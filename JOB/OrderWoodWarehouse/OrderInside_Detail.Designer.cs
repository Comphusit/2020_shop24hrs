﻿namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    partial class OrderInside_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderInside_Detail));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel_Emp = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_Wait = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Clos = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Head = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_DescriptionWait = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel_Rongmai = new Telerik.WinControls.UI.RadPanel();
            this.radDropDownList_Desc = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Descrongmai = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_SendtoAx = new Telerik.WinControls.UI.RadButton();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Printput = new Telerik.WinControls.UI.RadButton();
            this.radPanel_BillOut = new Telerik.WinControls.UI.RadPanel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radRadioButton_Noappbill = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Appbill = new Telerik.WinControls.UI.RadRadioButton();
            this.radTextBox_Descbillout = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Description = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radButton_FindCost = new Telerik.WinControls.UI.RadButton();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Cost = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_FindDpt = new Telerik.WinControls.UI.RadButton();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Func = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Dpt = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.PrintDialog1 = new System.Windows.Forms.PrintDialog();
            this.PrintDocument1 = new System.Drawing.Printing.PrintDocument();
            this.PrintDocument2 = new System.Drawing.Printing.PrintDocument();
            this.radLabel_EmpAppr = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Wait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Head)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DescriptionWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Rongmai)).BeginInit();
            this.radPanel_Rongmai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Descrongmai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SendtoAx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Printput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_BillOut)).BeginInit();
            this.radPanel_BillOut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Noappbill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Appbill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Descbillout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Description)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Cost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindDpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Func)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpAppr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Controls.Add(this.radLabel_Emp);
            this.radGroupBox_DB.Controls.Add(this.radCheckBox_Wait);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radButton_Clos);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Head);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_DescriptionWait);
            this.radGroupBox_DB.Controls.Add(this.radPanel_Rongmai);
            this.radGroupBox_DB.Controls.Add(this.radPanel_BillOut);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Description);
            this.radGroupBox_DB.Controls.Add(this.radLabel7);
            this.radGroupBox_DB.Controls.Add(this.radGridView_Show);
            this.radGroupBox_DB.Controls.Add(this.radButton_FindCost);
            this.radGroupBox_DB.Controls.Add(this.radLabel6);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Cost);
            this.radGroupBox_DB.Controls.Add(this.radButton_FindDpt);
            this.radGroupBox_DB.Controls.Add(this.radLabel5);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Docno);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Func);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Dpt);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(792, 570);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // radLabel_Emp
            // 
            this.radLabel_Emp.AutoSize = false;
            this.radLabel_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Emp.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Emp.Location = new System.Drawing.Point(447, 15);
            this.radLabel_Emp.Name = "radLabel_Emp";
            this.radLabel_Emp.Size = new System.Drawing.Size(323, 23);
            this.radLabel_Emp.TabIndex = 98;
            this.radLabel_Emp.Text = "ผู้เปิด -";
            // 
            // radCheckBox_Wait
            // 
            this.radCheckBox_Wait.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radCheckBox_Wait.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Wait.Location = new System.Drawing.Point(26, 335);
            this.radCheckBox_Wait.Name = "radCheckBox_Wait";
            this.radCheckBox_Wait.Size = new System.Drawing.Size(61, 19);
            this.radCheckBox_Wait.TabIndex = 0;
            this.radCheckBox_Wait.Text = "รอของ";
            this.radCheckBox_Wait.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Wait_ToggleStateChanged);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Cancel.Location = new System.Drawing.Point(347, 533);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(95, 32);
            this.radButton_Cancel.TabIndex = 97;
            this.radButton_Cancel.Text = "ยกเลิกทั้งบิล";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            // 
            // radButton_Clos
            // 
            this.radButton_Clos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Clos.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Clos.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Clos.Location = new System.Drawing.Point(447, 533);
            this.radButton_Clos.Name = "radButton_Clos";
            this.radButton_Clos.Size = new System.Drawing.Size(95, 32);
            this.radButton_Clos.TabIndex = 96;
            this.radButton_Clos.Text = "ปิด";
            this.radButton_Clos.ThemeName = "Fluent";
            this.radButton_Clos.Click += new System.EventHandler(this.RadButton_Clos_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Clos.GetChildAt(0))).Text = "ปิด";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Clos.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDropDownList_Head
            // 
            this.radDropDownList_Head.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Head.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Head.DropDownHeight = 124;
            this.radDropDownList_Head.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Head.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Head.Location = new System.Drawing.Point(25, 55);
            this.radDropDownList_Head.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Head.Name = "radDropDownList_Head";
            // 
            // 
            // 
            this.radDropDownList_Head.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Head.Size = new System.Drawing.Size(280, 25);
            this.radDropDownList_Head.TabIndex = 0;
            this.radDropDownList_Head.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Head.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Head.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radTextBox_DescriptionWait
            // 
            this.radTextBox_DescriptionWait.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_DescriptionWait.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_DescriptionWait.Location = new System.Drawing.Point(25, 359);
            this.radTextBox_DescriptionWait.Name = "radTextBox_DescriptionWait";
            this.radTextBox_DescriptionWait.Size = new System.Drawing.Size(747, 21);
            this.radTextBox_DescriptionWait.TabIndex = 6;
            // 
            // radPanel_Rongmai
            // 
            this.radPanel_Rongmai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel_Rongmai.AutoScroll = true;
            this.radPanel_Rongmai.Controls.Add(this.radDropDownList_Desc);
            this.radPanel_Rongmai.Controls.Add(this.radLabel10);
            this.radPanel_Rongmai.Controls.Add(this.radTextBox_Descrongmai);
            this.radPanel_Rongmai.Controls.Add(this.radButton_SendtoAx);
            this.radPanel_Rongmai.Controls.Add(this.radLabel11);
            this.radPanel_Rongmai.Controls.Add(this.radButton_Printput);
            this.radPanel_Rongmai.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel_Rongmai.Location = new System.Drawing.Point(25, 454);
            this.radPanel_Rongmai.Name = "radPanel_Rongmai";
            this.radPanel_Rongmai.Size = new System.Drawing.Size(747, 74);
            this.radPanel_Rongmai.TabIndex = 94;
            this.radPanel_Rongmai.Text = "นำของออก [เฉพาะโรงไม้]";
            this.radPanel_Rongmai.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radDropDownList_Desc
            // 
            this.radDropDownList_Desc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Desc.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Desc.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Desc.Location = new System.Drawing.Point(19, 42);
            this.radDropDownList_Desc.Name = "radDropDownList_Desc";
            this.radDropDownList_Desc.Size = new System.Drawing.Size(161, 20);
            this.radDropDownList_Desc.TabIndex = 1;
            this.radDropDownList_Desc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radDropDownList_Desc.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDownList_Desc_SelectedIndexChanged);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(324, 20);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(69, 19);
            this.radLabel10.TabIndex = 98;
            this.radLabel10.Text = "หมายเหตุ :";
            // 
            // radTextBox_Descrongmai
            // 
            this.radTextBox_Descrongmai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Descrongmai.BackColor = System.Drawing.Color.White;
            this.radTextBox_Descrongmai.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Descrongmai.Location = new System.Drawing.Point(324, 44);
            this.radTextBox_Descrongmai.Name = "radTextBox_Descrongmai";
            this.radTextBox_Descrongmai.Size = new System.Drawing.Size(301, 21);
            this.radTextBox_Descrongmai.TabIndex = 2;
            this.radTextBox_Descrongmai.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radButton_SendtoAx
            // 
            this.radButton_SendtoAx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_SendtoAx.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_SendtoAx.Location = new System.Drawing.Point(659, 40);
            this.radButton_SendtoAx.Name = "radButton_SendtoAx";
            this.radButton_SendtoAx.Size = new System.Drawing.Size(80, 25);
            this.radButton_SendtoAx.TabIndex = 4;
            this.radButton_SendtoAx.Text = "พิมพ์สั่งเบิก";
            this.radButton_SendtoAx.Click += new System.EventHandler(this.RadButton_SendtoAx_Click);
            this.radButton_SendtoAx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(19, 20);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(93, 19);
            this.radLabel11.TabIndex = 97;
            this.radLabel11.Text = "ระบุการรับของ :";
            // 
            // radButton_Printput
            // 
            this.radButton_Printput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Printput.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Printput.Location = new System.Drawing.Point(659, 12);
            this.radButton_Printput.Name = "radButton_Printput";
            this.radButton_Printput.Size = new System.Drawing.Size(80, 25);
            this.radButton_Printput.TabIndex = 3;
            this.radButton_Printput.Text = "พิมพ์สั่งจัด";
            this.radButton_Printput.Click += new System.EventHandler(this.RadButton_Printput_Click);
            this.radButton_Printput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radPanel_BillOut
            // 
            this.radPanel_BillOut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel_BillOut.Controls.Add(this.radLabel_EmpAppr);
            this.radPanel_BillOut.Controls.Add(this.radLabel8);
            this.radPanel_BillOut.Controls.Add(this.radRadioButton_Noappbill);
            this.radPanel_BillOut.Controls.Add(this.radRadioButton_Appbill);
            this.radPanel_BillOut.Controls.Add(this.radTextBox_Descbillout);
            this.radPanel_BillOut.Controls.Add(this.radLabel9);
            this.radPanel_BillOut.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel_BillOut.Location = new System.Drawing.Point(25, 384);
            this.radPanel_BillOut.Name = "radPanel_BillOut";
            this.radPanel_BillOut.Size = new System.Drawing.Size(747, 65);
            this.radPanel_BillOut.TabIndex = 93;
            this.radPanel_BillOut.Text = "อนุมัติเบิก [เฉพาะเบิกภานใน]";
            this.radPanel_BillOut.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(19, 30);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(87, 19);
            this.radLabel8.TabIndex = 98;
            this.radLabel8.Text = "ระบุความเห็น :";
            // 
            // radRadioButton_Noappbill
            // 
            this.radRadioButton_Noappbill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Noappbill.Location = new System.Drawing.Point(203, 31);
            this.radRadioButton_Noappbill.Name = "radRadioButton_Noappbill";
            this.radRadioButton_Noappbill.Size = new System.Drawing.Size(68, 19);
            this.radRadioButton_Noappbill.TabIndex = 2;
            this.radRadioButton_Noappbill.Text = "ไม่อนุมัติ";
            this.radRadioButton_Noappbill.CheckStateChanged += new System.EventHandler(this.RadRadioButton_Noappbill_CheckStateChanged);
            this.radRadioButton_Noappbill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radRadioButton_Appbill
            // 
            this.radRadioButton_Appbill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Appbill.Location = new System.Drawing.Point(127, 31);
            this.radRadioButton_Appbill.Name = "radRadioButton_Appbill";
            this.radRadioButton_Appbill.Size = new System.Drawing.Size(53, 19);
            this.radRadioButton_Appbill.TabIndex = 1;
            this.radRadioButton_Appbill.Text = "อนุมัติ";
            this.radRadioButton_Appbill.CheckStateChanged += new System.EventHandler(this.RadRadioButton_Appbill_CheckStateChanged);
            this.radRadioButton_Appbill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Descbillout
            // 
            this.radTextBox_Descbillout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Descbillout.BackColor = System.Drawing.Color.White;
            this.radTextBox_Descbillout.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Descbillout.Location = new System.Drawing.Point(324, 32);
            this.radTextBox_Descbillout.Name = "radTextBox_Descbillout";
            this.radTextBox_Descbillout.Size = new System.Drawing.Size(301, 21);
            this.radTextBox_Descbillout.TabIndex = 3;
            this.radTextBox_Descbillout.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(324, 9);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(69, 19);
            this.radLabel9.TabIndex = 87;
            this.radLabel9.Text = "หมายเหตุ :";
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.ForeColor = System.Drawing.Color.Blue;
            this.radLabel3.Location = new System.Drawing.Point(261, 10);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(81, 33);
            this.radLabel3.TabIndex = 89;
            this.radLabel3.Text = "แก้ไข";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(25, 282);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(258, 19);
            this.radLabel2.TabIndex = 41;
            this.radLabel2.Text = "รายละเอีดยดการเบิก";
            // 
            // radTextBox_Description
            // 
            this.radTextBox_Description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Description.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Description.Location = new System.Drawing.Point(25, 302);
            this.radTextBox_Description.Multiline = true;
            this.radTextBox_Description.Name = "radTextBox_Description";
            // 
            // 
            // 
            this.radTextBox_Description.RootElement.StretchVertically = true;
            this.radTextBox_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Description.Size = new System.Drawing.Size(747, 32);
            this.radTextBox_Description.TabIndex = 5;
            this.radTextBox_Description.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(25, 34);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(31, 19);
            this.radLabel7.TabIndex = 88;
            this.radLabel7.Text = "ช่าง";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(25, 152);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(747, 130);
            this.radGridView_Show.TabIndex = 40;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // radButton_FindCost
            // 
            this.radButton_FindCost.BackColor = System.Drawing.Color.Transparent;
            this.radButton_FindCost.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_FindCost.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_FindCost.Image = ((System.Drawing.Image)(resources.GetObject("radButton_FindCost.Image")));
            this.radButton_FindCost.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_FindCost.Location = new System.Drawing.Point(739, 101);
            this.radButton_FindCost.Name = "radButton_FindCost";
            // 
            // 
            // 
            this.radButton_FindCost.RootElement.AutoToolTip = true;
            this.radButton_FindCost.Size = new System.Drawing.Size(31, 26);
            this.radButton_FindCost.TabIndex = 4;
            this.radButton_FindCost.Text = "radButton3";
            this.radButton_FindCost.Click += new System.EventHandler(this.RadButton_FindCost_Click);
            this.radButton_FindCost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(447, 80);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(46, 19);
            this.radLabel6.TabIndex = 85;
            this.radLabel6.Text = "งบเบิก";
            // 
            // radDropDownList_Cost
            // 
            this.radDropDownList_Cost.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Cost.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Cost.DropDownHeight = 124;
            this.radDropDownList_Cost.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Cost.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Cost.Location = new System.Drawing.Point(447, 101);
            this.radDropDownList_Cost.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Cost.Name = "radDropDownList_Cost";
            // 
            // 
            // 
            this.radDropDownList_Cost.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Cost.Size = new System.Drawing.Size(286, 25);
            this.radDropDownList_Cost.TabIndex = 3;
            this.radDropDownList_Cost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Cost.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Cost.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radButton_FindDpt
            // 
            this.radButton_FindDpt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_FindDpt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_FindDpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_FindDpt.Image = ((System.Drawing.Image)(resources.GetObject("radButton_FindDpt.Image")));
            this.radButton_FindDpt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_FindDpt.Location = new System.Drawing.Point(311, 101);
            this.radButton_FindDpt.Name = "radButton_FindDpt";
            this.radButton_FindDpt.Size = new System.Drawing.Size(31, 26);
            this.radButton_FindDpt.TabIndex = 2;
            this.radButton_FindDpt.Text = "radButton3";
            this.radButton_FindDpt.Click += new System.EventHandler(this.RadButton_FindDpt_Click);
            this.radButton_FindDpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(25, 80);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(66, 19);
            this.radLabel5.TabIndex = 82;
            this.radLabel5.Text = "แผนกเบิก";
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(25, 20);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(280, 16);
            this.radLabel_Docno.TabIndex = 50;
            this.radLabel_Docno.Text = "MNOM200401000000";
            // 
            // radLabel_Func
            // 
            this.radLabel_Func.AutoSize = false;
            this.radLabel_Func.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Func.BorderVisible = true;
            this.radLabel_Func.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Func.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Func.Location = new System.Drawing.Point(314, 129);
            this.radLabel_Func.Name = "radLabel_Func";
            this.radLabel_Func.Size = new System.Drawing.Size(456, 19);
            this.radLabel_Func.TabIndex = 49;
            this.radLabel_Func.Text = "F1 แก้ไขข้อมูลสินค้า หรือแก้ไขจำนวนรับ F7 เปิดบิลใหม่กรณีรอของ F8 สั่งนอก";
            // 
            // radDropDownList_Dpt
            // 
            this.radDropDownList_Dpt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Dpt.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Dpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Dpt.Location = new System.Drawing.Point(25, 99);
            this.radDropDownList_Dpt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Dpt.Name = "radDropDownList_Dpt";
            // 
            // 
            // 
            this.radDropDownList_Dpt.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Dpt.Size = new System.Drawing.Size(280, 25);
            this.radDropDownList_Dpt.TabIndex = 1;
            this.radDropDownList_Dpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dpt.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dpt.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(247, 533);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(95, 32);
            this.radButton_Save.TabIndex = 8;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(25, 127);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(117, 19);
            this.radLabel1.TabIndex = 39;
            this.radLabel1.Text = "รายละเอียดสินค้า";
            // 
            // PrintDialog1
            // 
            this.PrintDialog1.UseEXDialog = true;
            // 
            // PrintDocument1
            // 
            this.PrintDocument1.EndPrint += new System.Drawing.Printing.PrintEventHandler(this.PrintDocument1_EndPrint);
            this.PrintDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // PrintDocument2
            // 
            this.PrintDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument2_PrintPage);
            // 
            // radLabel_EmpAppr
            // 
            this.radLabel_EmpAppr.AutoSize = false;
            this.radLabel_EmpAppr.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmpAppr.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmpAppr.Location = new System.Drawing.Point(422, 7);
            this.radLabel_EmpAppr.Name = "radLabel_EmpAppr";
            this.radLabel_EmpAppr.Size = new System.Drawing.Size(323, 23);
            this.radLabel_EmpAppr.TabIndex = 99;
            this.radLabel_EmpAppr.Text = "อนุมัติ -";
            // 
            // OrderInside_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 570);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "OrderInside_Detail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "สั่งของจากโรงไม้เชิงทะเล.";
            this.Load += new System.EventHandler(this.OrderInside_New_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Wait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Head)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DescriptionWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Rongmai)).EndInit();
            this.radPanel_Rongmai.ResumeLayout(false);
            this.radPanel_Rongmai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Descrongmai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SendtoAx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Printput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_BillOut)).EndInit();
            this.radPanel_BillOut.ResumeLayout(false);
            this.radPanel_BillOut.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Noappbill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Appbill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Descbillout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Description)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Cost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindDpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Func)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpAppr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Dpt;
        private Telerik.WinControls.UI.RadLabel radLabel_Func;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadButton radButton_FindCost;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadButton radButton_FindDpt;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Description;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Head;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Cost;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PrintDialog PrintDialog1;
        private System.Drawing.Printing.PrintDocument PrintDocument1;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadPanel radPanel_Rongmai;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Descrongmai;
        private Telerik.WinControls.UI.RadButton radButton_SendtoAx;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadButton radButton_Printput;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadPanel radPanel_BillOut;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Descbillout;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Noappbill;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Appbill;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_DescriptionWait;
        protected Telerik.WinControls.UI.RadButton radButton_Clos;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Wait;
        private System.Drawing.Printing.PrintDocument PrintDocument2;
        private Telerik.WinControls.UI.RadLabel radLabel_Emp;
        private Telerik.WinControls.UI.RadLabel radLabel_EmpAppr;
    }
}
