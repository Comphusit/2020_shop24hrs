﻿using System;
using System.Collections;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{

    #region "Var"
    public enum InputStatus
    {
        EditeItem = 0,
        InsertItem = 1,
        InsertQtyRecieve = 2,
        InsertItemForWait = 3,
        InsertItemForVend = 4,
        TypEdit = 5,
        TypRecieve = 6,
    }
    //main
    public class DATA_ORDERTABLE
    {
        private readonly string _DOCNO = "DOCNO";
        public string DOCNO { get; set; }
        private readonly string _DATECREATE = "DATECREATE";
        public string DATECREATE { get; set; }
        private readonly string _NAMECREATE = "NAMECREATE";
        public string NAMECREATE { get; set; }
        private readonly string _JOBGROUPSUB_ID = "JOBGROUPSUB_ID";
        public string JOBGROUPSUB_ID { get; set; }
        private readonly string _BRANCH_ID = "BRANCH_ID";
        public string BRANCH_ID { get; set; }
        private readonly string _COSTCENTERID = "COSTCENTERID";
        public string COSTCENTERID { get; set; }
        private readonly string _DESCRIPTION = "DESCRIPTION";
        public string DESCRIPTION { get; set; }
        private readonly string _STADOC = "STADOC";
        public string STADOC { get; set; }
        private readonly string _APPROVESTATUS = "APPROVESTATUS";
        public string APPROVESTATUS { get; set; }
        private readonly string _APPROVEWHOINS = "APPROVEWHOINS";
        public string APPROVEWHOINS { get; set; }
        private readonly string _APPROVENAMEINS = "APPROVENAMEINS";
        public string APPROVENAMEINS { get; set; }
        private readonly string _APPROVEDATEINS = "APPROVEDATEINS";
        public string APPROVEDATEINS { get; set; }
        private readonly string _APPROVE_DESCRIPTION = "APPROVE_DESCRIPTION";
        public string APPROVE_DESCRIPTION { get; set; }
        private readonly string _PRINTCOUNT = "PRINTCOUNT";
        public string PRINTCOUNT { get; set; }
        private readonly string _STATUSSENDTOAX = "STATUSSENDTOAX";
        public string STATUSSENDTOAX { get; set; }
        private readonly string _VENDID = "VENDID";
        public string VENDID { get; set; }
        private readonly string _PXEDOCNO = "PXEDOCNO";
        public string PXEDOCNO { get; set; }
        private readonly string _WAIT_STATUS = "WAIT_STATUS";
        public string WAIT_STATUS { get; set; }
        private readonly string _WAIT_DESCRIPTION = "WAIT_DESCRIPTION";
        public string WAIT_DESCRIPTION { get; set; }
        private readonly string _PAYMENT = "PAYMENT";
        public string PAYMENT { get; set; }

        public DATA_ORDERTABLE(string pBillDocNo)
        {
            if (pBillDocNo != null)
            {
                string sql = $@"
                    SELECT  [DOCNO],[DATECREATE],[USERCREATE],[NAMECREATE],[JOBGROUPSUB_ID],
                            [BRANCH_ID],[COSTCENTERID],[VENDID],[PAYMENT],[WHOUPD],[DATEUPD],[STADOC],[STATUS],[PRINTCOUNT],[DESCRIPTION],[WAIT_STATUS],[WAIT_DESCRIPTION],
                            [APPROVESTATUS],[APPROVEWHOINS],[APPROVENAMEINS],CONVERT(nvarchar,[APPROVEDATEINS],25) AS [APPROVEDATEINS],[APPROVE_DESCRIPTION],
                            [STATUSSENDTOAX],[WHOSENDTOAX],[NAMESENDTOAX],[DATESENDTOAX],[RECEIVER],[RECEIVEWHOINS],[RECIEVEDATEINS],[RECEIVER_DESCRIPTION],[PXEDOCNO]
                    FROM    [SHOP_MNOM_HD] WITH (NOLOCK) 
                    WHERE   [DOCNO] = '{pBillDocNo}' ";

                DataTable dtDocNo = Controllers.ConnectionClass.SelectSQL_Main(sql);
                if (dtDocNo.Rows.Count > 0)
                {
                    DOCNO = dtDocNo.Rows[0][_DOCNO].ToString();
                    DATECREATE = dtDocNo.Rows[0][_DATECREATE].ToString();
                    NAMECREATE = dtDocNo.Rows[0][_NAMECREATE].ToString();
                    JOBGROUPSUB_ID = dtDocNo.Rows[0][_JOBGROUPSUB_ID].ToString();
                    BRANCH_ID = dtDocNo.Rows[0][_BRANCH_ID].ToString();
                    COSTCENTERID = dtDocNo.Rows[0][_COSTCENTERID].ToString();
                    DESCRIPTION = dtDocNo.Rows[0][_DESCRIPTION].ToString();
                    STADOC = dtDocNo.Rows[0][_STADOC].ToString();
                    APPROVESTATUS = dtDocNo.Rows[0][_APPROVESTATUS].ToString();
                    APPROVEWHOINS = dtDocNo.Rows[0][_APPROVEWHOINS].ToString();
                    APPROVENAMEINS = dtDocNo.Rows[0][_APPROVENAMEINS].ToString();
                    APPROVEDATEINS = dtDocNo.Rows[0][_APPROVEDATEINS].ToString();
                    APPROVE_DESCRIPTION = dtDocNo.Rows[0][_APPROVE_DESCRIPTION].ToString();
                    PRINTCOUNT = dtDocNo.Rows[0][_PRINTCOUNT].ToString();
                    STATUSSENDTOAX = dtDocNo.Rows[0][_STATUSSENDTOAX].ToString();
                    VENDID = dtDocNo.Rows[0][_VENDID].ToString();
                    PXEDOCNO = dtDocNo.Rows[0][_PXEDOCNO].ToString();
                    WAIT_STATUS = dtDocNo.Rows[0][_WAIT_STATUS].ToString();
                    WAIT_DESCRIPTION = dtDocNo.Rows[0][_WAIT_DESCRIPTION].ToString();
                    PAYMENT = dtDocNo.Rows[0][_PAYMENT].ToString();
                }
            }
        }
    }
    public class DATAPRODUCT
    {
        private readonly string _ITEMBARCODE = "ITEMBARCODE";
        public string ITEMBARCODE { get; set; } = "";
        private readonly string _ITEMNAME = "ITEMNAME";
        public string ITEMNAME { get; set; } = "";
        private readonly string _ITEMID = "ITEMID";
        public string ITEMID { get; set; } = "";
        private readonly string _ITEMDIM = "ITEMDIM";
        public string ITEMDIM { get; set; } = "";
        private readonly string _UNIT = "UNIT";
        public string UNIT { get; set; } = "";
        private readonly string _QTY = "QTY";
        public double QTY { get; set; } = 0;
        private readonly string _QTYEDIT = "QTYEDIT";
        public double QTYEDIT { get; set; } = 0;
        private readonly string _PRICE = "PRICE";
        public double PRICE { get; set; } = 0;
        //private readonly string _AMOUNT = "AMOUNT";
        public double AMOUNT { get; set; } = 0;
        private FormShare.ShowData.Data_ITEMBARCODE Data_item { get; set; }
        public DATAPRODUCT(DataRow pItemrow)
        {
            if (pItemrow != null)
            {
                ITEMBARCODE = pItemrow[_ITEMBARCODE].ToString();
                ITEMNAME = pItemrow[_ITEMNAME].ToString();
                ITEMID = pItemrow[_ITEMID].ToString();
                ITEMDIM = pItemrow[_ITEMDIM].ToString();
                UNIT = pItemrow[_UNIT].ToString();
                QTY = double.Parse(pItemrow[_QTY].ToString());
                QTYEDIT = double.Parse(pItemrow[_QTYEDIT].ToString());
                PRICE = double.Parse(pItemrow[_PRICE].ToString());
                AMOUNT = QTY * PRICE;
            }
        }
        public DATAPRODUCT(string pItembacode, string pItemname, double pQty, double pQtyEdit, string pUnit)
        {
            if (pItembacode != null && pItembacode != "")
            {
                Data_item = new FormShare.ShowData.Data_ITEMBARCODE(pItembacode);
                ITEMBARCODE = Data_item.Itembarcode_ITEMBARCODE;
                ITEMNAME = Data_item.Itembarcode_SPC_ITEMNAME;
                ITEMID = Data_item.Itembarcode_ITEMID;
                ITEMDIM = Data_item.Itembarcode_INVENTDIMID;
                UNIT = Data_item.Itembarcode_UNITID;
                PRICE = Data_item.Itembarcode_SPC_PRICEGROUP13;
                QTY = pQty;
                QTYEDIT = pQtyEdit;
                AMOUNT = PRICE * QTY;
            }
            else
            {
                ITEMNAME = pItemname;
                UNIT = pUnit;
                QTY = pQty;
                QTYEDIT = pQtyEdit;
            }
        }
    }

    #endregion

    public class OrderWoodClass
    {
        internal static DataTable DataColumn()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ITEMBARCODE");
            dt.Columns.Add("ITEMNAME");
            dt.Columns.Add("QTY");
            dt.Columns.Add("QTYEDIT");
            dt.Columns.Add("UNIT");
            dt.Columns.Add("AMOUNT");
            dt.Columns.Add("ITEMID");
            dt.Columns.Add("ITEMDIM");
            dt.Columns.Add("PRICE");
            return dt;
        }
        internal static DataTable DataColumnCheck()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ITEMBARCODE");
            dt.Columns.Add("QTY");
            return dt;
        }

        #region OldCode
        // internal static DataTable QueryData_Top1000(string pStatus)
        // {
        //     string sql = string.Format(@" SELECT TOP 1000 SHOP_MNOM_HD.DOCNO,CAST(DATECREATE AS date) AS DATECREATE
        //                ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS)
        //  ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD
        //  ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS)
        //  ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT
        //                   ,USERCREATE,NAMECREATE,SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID
        //                ,JOBGROUPSUB_DESCRIPTION,BRANCH_ID,DIMENSIONS.DESCRIPTION AS DIMENSIONSNAME
        //                   ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME,VENDID,ISNULL(VENDTABLE.[NAME],'') AS ACCOUNTNAME
        //                   ,STADOC,SHOP_MNOM_HD.[STATUS] AS [STATUS],PRINTCOUNT,CASE WHEN PRINTCOUNT > 0 THEN 1 ELSE 0 END AS PRINTCOUNT2
        //                   ,WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION] AS [DESCRIPTION],CASE WHEN STADOC = '3' THEN '0' ELSE APPROVESTATUS END AS APPROVESTATUS  
        //                   ,STATUSSENDTOAX,NAMESENDTOAX,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
        //               FROM SHOP_MNOM_HD WITH (NOLOCK) 
        //               INNER JOIN [SHOP_MNOM_DT] WITH (NOLOCK) ON SHOP_MNOM_HD.DOCNO = [SHOP_MNOM_DT].DOCNO
        //               INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
        //               LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
        //               LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID
        //               LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON SHOP_MNOM_HD.VENDID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = 'SPC' 
        //               WHERE [STATUS] = '{0}' AND SHOP_MNOM_DT.QTY > 0 
        //               GROUP BY 
        //               SHOP_MNOM_HD.DOCNO,
        //DATECREATE,
        //DATECREATE,
        //SHOP_MNOM_HD.DATEUPD,
        //USERCREATE,NAMECREATE,
        //SHOP_MNOM_HD.JOBGROUPSUB_ID
        //,JOBGROUPSUB_DESCRIPTION
        //,BRANCH_ID,DIMENSIONS.DESCRIPTION 
        //               ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME 
        //               ,VENDID,VENDTABLE.[NAME]
        //               ,STADOC,SHOP_MNOM_HD.[STATUS]
        //               ,PRINTCOUNT
        //               ,WAIT_STATUS
        //               ,SHOP_MNOM_HD.[DESCRIPTION]
        //               ,STADOC,APPROVESTATUS
        //               ,STATUSSENDTOAX,NAMESENDTOAX
        //               ,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
        //                 ORDER BY SHOP_MNOM_HD.DATECREATE DESC", pStatus);

        //     DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
        //     return dt;
        // }

        //internal static DataTable GetdataAll_ByFilter(string _pTop, string _pCondition)
        //{
        //    string sql = string.Format(@"SELECT {0} DOCNO
        //                ,CAST(DATECREATE AS date) AS DATECREATE
        //               ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS)
        // ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD
        // ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS)
        // ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT
        //                  ,USERCREATE,NAMECREATE
        //                  ,SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID
        //               ,JOBGROUPSUB_DESCRIPTION
        //                  ,BRANCH_ID,DIMENSIONS.DESCRIPTION AS DIMENSIONSNAME
        //                  ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME
        //                  ,STADOC,SHOP_MNOM_HD.[STATUS] AS [STATUS]
        //                  ,PRINTCOUNT,SHOP_MNOM_HD.[DESCRIPTION] AS [DESCRIPTION]
        //                  ,APPROVESTATUS
        //                  ,APPROVEWHOINS
        //               ,APPROVENAMEINS
        //                  ,APPROVEDATEINS
        //                  ,APPROVE_DESCRIPTION
        //                  ,STATUSSENDTOAX
        //                  ,WHOSENDTOAX
        //               ,NAMESENDTOAX
        //                  ,DATESENDTOAX
        //              FROM SHOP24HRS.dbo.SHOP_MNOM_HD WITH (NOLOCK) 
        //              INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
        //              LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM  
        //              AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
        //              LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID 
        //              WHERE [STATUS] = '0' ", _pTop);

        //    if (_pCondition != "")
        //    {
        //        sql += string.Format(@" AND {0} ", _pCondition);
        //    }

        //    sql += string.Format(@" ORDER BY SHOP_MNOM_HD.DATECREATE DESC ");
        //    DataTable dtData = Controllers.ConnectionClass.SelectSQL_Main(sql);
        //    return dtData;
        //}

        // internal static DataTable QueryData_ByActiveA(string pStatus)
        // {
        //     string sql = string.Format(@" SELECT SHOP_MNOM_HD.DOCNO,CAST(DATECREATE AS date) AS DATECREATE
        //                ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS)
        //  ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD
        //  ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS)
        //  ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT
        //                   ,USERCREATE,NAMECREATE,SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID
        //                ,JOBGROUPSUB_DESCRIPTION,BRANCH_ID,DIMENSIONS.DESCRIPTION AS DIMENSIONSNAME
        //                   ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME,VENDID,ISNULL(VENDTABLE.[NAME],'') AS ACCOUNTNAME
        //                   ,STADOC,SHOP_MNOM_HD.[STATUS] AS [STATUS],PRINTCOUNT,CASE WHEN PRINTCOUNT > 0 THEN 1 ELSE 0 END AS PRINTCOUNT2
        //                   ,WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION] AS [DESCRIPTION],CASE WHEN STADOC = '3' THEN '0' ELSE APPROVESTATUS END AS APPROVESTATUS  
        //                   ,STATUSSENDTOAX,NAMESENDTOAX,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
        //               FROM SHOP_MNOM_HD WITH (NOLOCK) 
        //               INNER JOIN [SHOP_MNOM_DT] WITH (NOLOCK) ON SHOP_MNOM_HD.DOCNO = [SHOP_MNOM_DT].DOCNO
        //               INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
        //               LEFT OUTER JOIN  {0}DIMENSIONS WITH (NOLOCK)  ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
        //               LEFT OUTER JOIN {0}SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID
        //               LEFT OUTER JOIN {0}VENDTABLE WITH (NOLOCK) ON SHOP_MNOM_HD.VENDID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = 'SPC' 
        //               WHERE [STATUS] = '{1}' AND STADOC = '1'  AND  SHOP_MNOM_HD.[RECEIVEWHOINS] = ''  AND SHOP_MNOM_DT.QTY > 0
        //               GROUP BY 
        //SHOP_MNOM_HD.DOCNO,
        //DATECREATE,
        //DATECREATE,
        //SHOP_MNOM_HD.DATEUPD,
        //USERCREATE,NAMECREATE,
        //SHOP_MNOM_HD.JOBGROUPSUB_ID
        //,JOBGROUPSUB_DESCRIPTION
        //,BRANCH_ID,DIMENSIONS.DESCRIPTION 
        //               ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME 
        //               ,VENDID,VENDTABLE.[NAME]
        //               ,STADOC,SHOP_MNOM_HD.[STATUS]
        //               ,PRINTCOUNT
        //               ,WAIT_STATUS
        //               ,SHOP_MNOM_HD.[DESCRIPTION]
        //               ,STADOC,APPROVESTATUS
        //               ,STATUSSENDTOAX,NAMESENDTOAX
        //               ,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
        //               ORDER BY SHOP_MNOM_HD.DATECREATE DESC", "SHOP2013TMP.dbo.", pStatus);

        //     DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
        //     return dt;
        // }
        #endregion
              
        //Apv Bill
        public static string UpdateApprove(string BillRec, string UserID, string Username, string BillCheck, string StatusDoc, string Head, string Dept, string Costcenter, string RemarkBillOut, string Remark)
        {
            string sql = $@"
                UPDATE  dbo.SHOP_MNOM_HD 
                SET     JOBGROUPSUB_ID = '{Head}',BRANCH_ID = '{Dept}',COSTCENTERID = '{Costcenter}',STADOC = '{StatusDoc}',
                        APPROVESTATUS ='{BillCheck}',APPROVEWHOINS = '{UserID}',APPROVENAMEINS = '{Username}',APPROVEDATEINS = GETDATE(),
                        APPROVE_DESCRIPTION = '{RemarkBillOut}',DESCRIPTION = '{Remark}' 
                WHERE   DOCNO = '{BillRec}' ";
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //update Head Bill
        public static string UpdateDataHD(string BillRec, string UserID, string Head, string Dept, string Costcenter, string VendId, string Payment, string Remark)
        {
            string sql = $@"
                UPDATE  dbo.SHOP_MNOM_HD 
                SET     JOBGROUPSUB_ID = '{Head}',BRANCH_ID = '{Dept}',COSTCENTERID = '{Costcenter}',
                        VENDID = '{VendId}',PAYMENT = '{Payment}',DESCRIPTION = '{Remark}' ,WHOUPD = '{UserID}',DATEUPD = GETDATE()
                WHERE   DOCNO = '{BillRec}' ";
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Insert Bill OM
        public static string InsertbillWait_ORExternal(string BillNumber, DATAPRODUCT pdATAPRODUCT, string UserID, string UserName, string Description, string BillRec, int rowindex, string Wait_status, string Status)
        {
            string RemarkWait; string RemarkBill;
            DATA_ORDERTABLE dATA_ORDERTABLE = new DATA_ORDERTABLE(BillRec);
            DATAPRODUCT _dATAPRODUCT = pdATAPRODUCT;
            if (Wait_status == "1")
            {
                RemarkBill = dATA_ORDERTABLE.DESCRIPTION;
                RemarkWait = Description.Replace(",", " ");
            }
            else
            {
                RemarkBill = dATA_ORDERTABLE.DESCRIPTION + "//" + Description.Replace(",", " ");
                RemarkWait = "";
            }

            ArrayList sqlIn = new ArrayList {
                string.Format(@"INSERT INTO SHOP_MNOM_HD ( DOCNO,USERCREATE,NAMECREATE,JOBGROUPSUB_ID,BRANCH_ID 
                                  ,COSTCENTERID,DESCRIPTION,WHOUPD,WAIT_STATUS,WAIT_DESCRIPTION,[STATUS] 
                                  ,APPROVESTATUS,APPROVEWHOINS ,APPROVENAMEINS,APPROVEDATEINS,APPROVE_DESCRIPTION,REFERDOCNO) 
                        VALUES( '{0}','{1}','{2}','{3}','{4}'
                        ,'{5}','{6}','{1}','{7}','{8}','{9}','{10}'
                        ,'{11}','{12}','{13}','{14}','{15}')"
                        , BillNumber, UserID, UserName, dATA_ORDERTABLE.JOBGROUPSUB_ID, dATA_ORDERTABLE.BRANCH_ID
                        , dATA_ORDERTABLE.COSTCENTERID, RemarkBill, Wait_status, RemarkWait
                        , Status
                        , dATA_ORDERTABLE.APPROVESTATUS
                        , dATA_ORDERTABLE.APPROVEWHOINS
                        , dATA_ORDERTABLE.APPROVENAMEINS
                        , dATA_ORDERTABLE.APPROVEDATEINS
                        , dATA_ORDERTABLE.APPROVE_DESCRIPTION
                        ,BillRec + "_" + rowindex )
                };

            sqlIn.Add(string.Format(@"INSERT INTO SHOP_MNOM_DT ( DOCNO,SEQNO,ITEMID,ITEMDIM,ITEMBARCODE,ITEMNAME
                                  ,INVENTID,QTY,QTYEDIT,UNIT,PRICE,AMOUNT,WHOINS)
                            VALUES ('{0}','1','{1}','{2}','{3}','{4}'
                            ,'MN998','{5}','{6}','{7}','{8}','{9}','{10}')"
                            , BillNumber
                            , _dATAPRODUCT.ITEMID
                            , _dATAPRODUCT.ITEMDIM
                            , _dATAPRODUCT.ITEMBARCODE
                            , _dATAPRODUCT.ITEMNAME
                            , _dATAPRODUCT.QTY
                            , _dATAPRODUCT.QTYEDIT
                            , _dATAPRODUCT.UNIT
                            , _dATAPRODUCT.PRICE
                            , _dATAPRODUCT.AMOUNT
                            , UserID));

            return ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
        }
        //Update DT
        public static string UpdateDatarow_Qty(DATAPRODUCT pdATAPRODUCT, string UserID, string _pBill, int rowindex)
        {
            DATAPRODUCT _dATAPRODUCT = pdATAPRODUCT;
            string sql = string.Format(@"
                UPDATE  [SHOP_MNOM_DT] 
                SET     [QTY] = '{0}',[QTYEDIT] = '{1}',[DATEUPD] = GETDATE(),[WHOUPD]= '{2}'
                WHERE   [DOCNO] = '{3}' AND [SEQNO] = '{4}'"
                        , _dATAPRODUCT.QTY * (-1), _dATAPRODUCT.QTY * (-1), UserID, _pBill, rowindex);
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Update Rows
        public static string UpdateDataRow(DATAPRODUCT pdATAPRODUCT, string UserID, string _pBill, int rowindex)
        {
            DATAPRODUCT _dATAPRODUCT = pdATAPRODUCT;
            string sql = string.Format(@"
                UPDATE  [SHOP_MNOM_DT] 
                SET     [ITEMID] = '{2}',[ITEMDIM] = '{3}',[ITEMBARCODE] = '{4}',[ITEMNAME] = '{5}',[QTY] = '{6}',[QTYEDIT] = '{7}',
                        [UNIT] = '{8}',[PRICE] = '{9}',[AMOUNT] = '{10}',[DATEUPD] = GETDATE(),[WHOUPD] = '{11}'
                WHERE   [DOCNO] = '{0}' AND [SEQNO] = '{1}'"
                        , _pBill, rowindex, _dATAPRODUCT.ITEMID, _dATAPRODUCT.ITEMDIM
                        , _dATAPRODUCT.ITEMBARCODE, _dATAPRODUCT.ITEMNAME.Replace("'", " "), _dATAPRODUCT.QTY
                        , _dATAPRODUCT.QTYEDIT, _dATAPRODUCT.UNIT, _dATAPRODUCT.PRICE, _dATAPRODUCT.AMOUNT, UserID);
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Insert Rows
        public static string InsertDataRow(DATAPRODUCT pdATAPRODUCT, string UserID, string _pBill, int rowindex)
        {
            DATAPRODUCT _dATAPRODUCT = pdATAPRODUCT;
            string sql = string.Format(@"INSERT INTO [SHOP_MNOM_DT] (DOCNO,SEQNO,ITEMID,ITEMDIM,ITEMBARCODE,ITEMNAME,QTY,QTYEDIT,UNIT,PRICE,AMOUNT,WHOINS,WHOUPD)
                VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{11}')"
                    , _pBill
                    , rowindex
                    , _dATAPRODUCT.ITEMID
                    , _dATAPRODUCT.ITEMDIM
                    , _dATAPRODUCT.ITEMBARCODE
                    , _dATAPRODUCT.ITEMNAME
                    , _dATAPRODUCT.QTY
                    , _dATAPRODUCT.QTYEDIT
                    , _dATAPRODUCT.UNIT
                    , _dATAPRODUCT.PRICE
                    , _dATAPRODUCT.AMOUNT
                    , UserID);

            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Select item
        public static DataTable QueryRowsItem(string BillRec)
        {
            string sql = $@"
                SELECT  SHOP_MNOM_HD.DOCNO,SEQNO,ITEMID,ITEMDIM,ITEMBARCODE,ITEMNAME,INVENTID,QTY,QTYEDIT,UNIT,PRICE,AMOUNT 
                FROM    SHOP_MNOM_DT WITH (NOLOCK) 
                        INNER JOIN SHOP_MNOM_HD WITH (NOLOCK) ON SHOP_MNOM_HD.DOCNO = SHOP_MNOM_DT.DOCNO
                WHERE   SHOP_MNOM_HD.DOCNO = '{BillRec}' AND QTY > 0 ORDER BY SEQNO ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Update รอสินค้า
        public static string UpdateWait(string BillRec, string UserID, string Remark)
        {
            string sql = $@"
                UPDATE  dbo.SHOP_MNOM_HD 
                SET     WHOUPD = '{UserID}',DATEUPD = GETDATE(),WAIT_STATUS = '1',WAIT_DESCRIPTION = '{Remark}'
                WHERE DOCNO = '{BillRec}' ";

            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Update Bill Cancel
        public static string UpdateCancelBill(string BillRec, string stadoc, string UserID, string Remark, string typeRecive)
        {
            string sql = string.Format(@"UPDATE dbo.SHOP_MNOM_HD 
                    SET  
                    STADOC = '{0}'
                    ,[WHOUPD] = '{1}'
                    ,[DATEUPD] = GETDATE()
                    ,[RECEIVER] = '{2}'
                    ,[RECEIVEWHOINS] = '{1}'
				    ,[RECIEVEDATEINS] = GETDATE()
                    ,[RECEIVER_DESCRIPTION] = '{3}'
                    WHERE DOCNO = '{4}' ", stadoc, UserID, typeRecive, Remark, BillRec);

            string returnExc = Controllers.ConnectionClass.ExecuteSQL_Main(sql);
            return returnExc;
        }
        //ค้นหาข้อมูลการซือของใช้งานภายในบริษัท จากการไปซื้อเอง
        public static DataTable GetItemOrderExt(string date_beg, string date_end, DataTable _dtData)
        {
            string Condit = @"WHERE RECEIVEITEM = '0' "; ;
            if (!date_beg.Equals("")) Condit = $@" WHERE   Convert(varchar,DATEINS,23) between '{date_beg}' and '{date_end}' ";

            string sql = $@"SELECT  SEQNO,CASE WHEN PURCHASE = '1' THEN   DATEDIFF(day, DATEINS, PURCHDATEUP) ELSE 0 END AS TIMEOFPURCH,
                                    CASE WHEN  PURCHASE = '1'AND RECEIVEITEM = '1' THEN   DATEDIFF(day, PURCHDATEUP, RECEIVEDATE) ELSE 0 END AS TIMEOFRECEIVE,
                                    ITEMNAME, QTY, QTYEDIT, UNIT, REMARK, PURCHASE, PURCHDATEUP, PURCHWHOUP, ORDERACCOUNT, ACCOUNT, REMAIM,
                                    RECEIVEITEM, RECEIVEDATE, RECEIVEWHOUP, PAYMENT, WHOINS, WHOINSNAME, Convert(varchar,DATEINS,23) AS DATEINS,Convert(varchar,DATEINS,24) AS TIMEINS,WHOUP, WHOUPNAME, DATEUP
                            FROM    SHOP_MNOM_ITEM WITH (NOLOCK) 
                                    {Condit}   
                            ORDER BY DATEINS DESC";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            foreach (DataRow row in dt.Rows)
            {
                _dtData.Rows.Add(row["SEQNO"].ToString(),
                    row["TIMEOFPURCH"].ToString(),
                    row["TIMEOFRECEIVE"].ToString(),
                    row["DATEINS"].ToString() + " " + row["TIMEINS"].ToString(),
                    row["ITEMNAME"].ToString(),
                    Convert.ToDouble(row["QTY"].ToString()).ToString("N2"),
                    Convert.ToDouble(row["QTYEDIT"].ToString()).ToString("N2"),
                    row["UNIT"].ToString(),
                    row["REMARK"].ToString(),
                    row["ORDERACCOUNT"].ToString(),
                    row["ACCOUNT"].ToString(),
                    row["PAYMENT"].ToString(),
                    row["PURCHASE"].ToString(),
                    row["REMAIM"].ToString(),
                    row["RECEIVEITEM"].ToString(),
                    row["WHOINS"].ToString() + " " + row["WHOINSNAME"].ToString(),
                    row["DATEINS"].ToString() + " " + row["TIMEINS"].ToString(),
                    row["WHOUP"].ToString() + " " + row["WHOUPNAME"].ToString(),
                    String.Format("{0:yyyy-MM-dd HH:mm:ss}", row["DATEUP"])
                    );
            }
            return _dtData;
        }

        public static DataTable CheckDocNoUse(string pCase, string pDoc)//pCase 0 = [PXEDOCNO] 1= DOCNO
        {
            string pField = $@" WHERE [PXEDOCNO] = '{pDoc}' ";
            if (pCase == "1") pField = $@" WHERE [DOCNO] = '{pDoc}' ";

            string sql = $@"SELECT [DOCNO],[PXEDOCNO] FROM [SHOP_MNOM_HD]  WITH (NOLOCK)  {pField} ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable GetData_MNOM_HD(string docno)
        {
            string sql = $@"
                SELECT  [DOCNO],CONVERT(VARCHAR,[DATECREATE],23) AS [DATECREATE],
                        [USERCREATE],[NAMECREATE],[JOBGROUPSUB_ID],[BRANCH_ID],
                        [COSTCENTERID],[VENDID],[PAYMENT],[WHOUPD],CONVERT(VARCHAR,[DATEUPD],23) AS [DATEUPD],
                        [STADOC],[STATUS],[PRINTCOUNT],[DESCRIPTION],[WAIT_STATUS],[WAIT_DESCRIPTION],
                        [APPROVESTATUS],[APPROVEWHOINS],[APPROVENAMEINS],
                        CONVERT(VARCHAR,[APPROVEDATEINS],23) AS [APPROVEDATEINS],[APPROVE_DESCRIPTION],[STATUSSENDTOAX],[WHOSENDTOAX],[NAMESENDTOAX],
                        CONVERT(VARCHAR,[DATESENDTOAX],23) AS [DATESENDTOAX],
                        [RECEIVER],[RECEIVEWHOINS],[RECIEVEDATEINS],RECEIVER_DESCRIPTION,PXEDOCNO
                FROM    [SHOP_MNOM_HD] WITH (NOLOCK) 
                WHERE DOCNO = '{docno}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
