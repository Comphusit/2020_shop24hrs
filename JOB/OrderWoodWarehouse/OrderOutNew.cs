﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Linq;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderOutNew : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData = new DataTable();
        public OrderOutNew()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }
        #region Events 

        private void SetConditions()
        {
            DatagridClass.SetCellBackClolorByExpression("ITEMNAME", " PURCHASE = '0' ", Color.Red, radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("ITEMNAME", " PURCHASE = '1' ", ConfigClass.SetColor_Wheat(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("ITEMNAME", " REMAIM = '1' ", ConfigClass.SetColor_GrayPastel(), radGridView_Show);
        }
        private void OrderOutside_Report_Load(object sender, EventArgs e)
        {
            this.radStatusStrip_Zone.SizingGrip = false;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.ReadOnly = false;
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "บันทึกเป็น Excel";
            radButtonElement_Document.ShowBorder = true; radButtonElement_Document.ToolTipText = "คู่มือการใช้งาน";
            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Find.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderBegin, DateTime.Now.AddDays(-7), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderEnd, DateTime.Now, DateTime.Now);

            radDateTimePicker_OrderBegin.Enabled = false; radDateTimePicker_OrderEnd.Enabled = false;
            radCheckBox_CheckDate.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_group.ButtonElement.Font = SystemClass.SetFontGernaral;

            this.BindGrid();
            this.SetConditions();
            this.GetDataOrder();
            radGridView_Show.DataSource = dtData;
        }
        void BindGrid()
        {

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SEQNO", "SEQNO"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TIMEOFPURCH", "รวมซื้อ[D]", 60));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TIMEOFRECEIVE", "ช่างรับ[D]", 60));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนสั่ง", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYEDIT", "ค้างรับ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "รายละเอียดการสั่ง", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ORDERACCOUNT", "ผู้สั่ง", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ACCOUNT", "ชื่อร้าน", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("PAYMENT", "เงินสด", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("PURCHASE", "ซื้อแล้ว", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("REMAIM", "รอของ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("RECEIVEITEM", "ช่างรับแล้ว", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WHOINS", "ผู้สร้าง"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DATETIMEINS", "เวลาสร้าง"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WHOUP", "แก้ไข"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DATETIMEUP", "แก้ไขล่าสุด"));

            dtData = AddNewColumn(new string[] { "SEQNO", "TIMEOFPURCH", "TIMEOFRECEIVE", "DATEINS", "ITEMNAME"
                , "QTY", "QTYEDIT", "UNIT", "REMARK", "ORDERACCOUNT", "ACCOUNT", "PAYMENT", "PURCHASE", "REMAIM", "RECEIVEITEM", "WHOINS", "DATETIMEINS", "WHOUP", "DATETIMEUP"});

            foreach (RadLabel c in radPanel_Additem.Controls.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;   //c.ForeColor = ConfigClass.SetColor_Blue();
            }
            foreach (RadTextBoxBase c in radPanel_Additem.Controls.OfType<RadTextBoxBase>())
            {
                c.Font = SystemClass.SetFontGernaral; c.ForeColor = ConfigClass.SetColor_Blue();
            }
        }
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            foreach (RadTextBox radTextBox in radPanel_Additem.Controls.OfType<RadTextBox>())
            {
                if (string.IsNullOrEmpty(radTextBox.Text) && !radTextBox.Name.Equals("radTextBox_Account") && !radTextBox.Name.Equals("radTextBox_Remark"))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลให้เรียบร้อยก่อนบันทึก");
                    radTextBox.Focus();
                    return;
                }
            }
            string seqno = GetSeqno();
            string sqlIns = string.Format(@"INSERT INTO SHOP_MNOM_ITEM (SEQNO,ITEMNAME,QTY,UNIT,REMARK,ORDERACCOUNT,ACCOUNT,WHOINS,WHOINSNAME)
                            VALUES( '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')"
                            , seqno
                            , radTextBox_Itemname.Text
                            , Convert.ToDouble(radTextBox_Qty.Text)
                            , radTextBox_Unit.Text
                            , radTextBox_Remark.Text
                            , radTextBox_OwnOrder.Text
                            , radTextBox_Account.Text
                            , SystemClass.SystemUserID_M
                            , SystemClass.SystemUserName);

            string ret = ConnectionClass.ExecuteSQL_Main(sqlIns);
            MsgBoxClass.MsgBoxShow_SaveStatus(ret);
            if (ret.Equals(""))
            {
                string sql = @"SELECT   TOP 1 SEQNO, 
                                        CASE WHEN PURCHASE = '1' AND RECEIVEITEM = '0' THEN   DATEDIFF(day, DATEINS, PURCHDATEUP) ELSE 0 END AS TIMEOFPURCH,
		                                CASE WHEN  PURCHASE = '1'AND RECEIVEITEM = '1' THEN   DATEDIFF(day, PURCHDATEUP, RECEIVEDATE) ELSE 0 END AS TIMEOFRECEIVE,
                                        ITEMNAME,QTY,QTYEDIT,UNIT,REMARK,PURCHASE,PURCHDATEUP,PURCHWHOUP,ORDERACCOUNT,ACCOUNT,REMAIM,
                                        RECEIVEITEM,RECEIVEDATE,RECEIVEWHOUP,PAYMENT,WHOINS,WHOINSNAME,
                                        (Convert(varchar,DATEINS,23) + ' ' + Convert(varchar,DATEINS,24)) AS DATEINS,WHOUP,WHOUPNAME,DATEUP
                               FROM     SHOP_MNOM_ITEM WITH (NOLOCK)
                               WHERE    SEQNO = '" + seqno + @"'";

                DataTable dt = ConnectionClass.SelectSQL_Main(sql);
                foreach (DataRow row in dt.Rows)
                {
                    dtData.Rows.Add(row["SEQNO"].ToString(),
                        row["TIMEOFPURCH"].ToString(),
                        row["TIMEOFRECEIVE"].ToString(),
                        row["DATEINS"].ToString(),
                        row["ITEMNAME"].ToString(),
                        row["QTY"].ToString(),
                        row["QTYEDIT"].ToString(),
                        row["UNIT"].ToString(),
                        row["REMARK"].ToString(),
                        row["ORDERACCOUNT"].ToString(),
                        row["ACCOUNT"].ToString(),
                        row["PAYMENT"].ToString(),
                        row["PURCHASE"].ToString(),
                        row["REMAIM"].ToString(),
                        row["RECEIVEITEM"].ToString()
                        );
                }
                dtData.AcceptChanges();
                radTextBox_Itemname.Text = "";
                radTextBox_Qty.Text = "";
                radTextBox_Unit.Text = "";
                this.radTextBox_Itemname.Focus();
            }
            else
            {
                this.radTextBox_Itemname.SelectAll();
            }
        }
        private void RadButton_Cancle_Click(object sender, EventArgs e)
        {
            this.SetTextEmpty();
            radTextBox_Itemname.Focus();
        }
        private void RadTextBox_Qty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
            if ((e.KeyChar == '.') && ((sender as RadTextBox).Text.IndexOf('.') > -1)) e.Handled = true;
        }
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    if (radGridView_Show.CurrentRow.Cells["RECEIVEITEM"].Value.ToString().Equals("1")) { e.Cancel = true; }
                    else
                    {
                        switch (e.Column.FieldName)
                        {
                            case "SUMDATE":
                                e.Cancel = true;
                                break;
                            case "SUMDATEALL":
                                e.Cancel = true;
                                break;
                            case "DATEINS":
                                e.Cancel = true;
                                break;
                            case "RECEIVEITEM":
                                if (radGridView_Show.CurrentRow.Cells["PURCHASE"].Value.ToString().Equals("0"))
                                {
                                    e.Cancel = true;
                                }
                                if (Convert.ToDouble(radGridView_Show.CurrentRow.Cells["QTY"].Value) <= 0)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("จำนวนสั่งซื้อ น้อยกว่าหรือเท่ากับศูนย์ เช็คใหม่อีกครั้ง");
                                    e.Cancel = true;
                                }
                                if (string.IsNullOrEmpty(radGridView_Show.CurrentRow.Cells["REMARK"].Value.ToString()) || string.IsNullOrEmpty(radGridView_Show.CurrentRow.Cells["ORDERACCOUNT"].Value.ToString())
                                    || string.IsNullOrEmpty(radGridView_Show.CurrentRow.Cells["ACCOUNT"].Value.ToString()))
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุรายละเอียดให้เรียบร้อยก่อนรับ");
                                    e.Cancel = true;
                                }
                                break;
                            case "QTYEDIT":
                                if (radGridView_Show.CurrentRow.Cells["PURCHASE"].Value.ToString().Equals("0"))
                                {
                                    e.Cancel = true;
                                }
                                break;
                        }
                    }
                }
            }
            catch
            {

            }
        }
        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                try
                {
                    radGridView_Show.CurrentCell.Value = radGridView_Show.CurrentCell.Value.ToString();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void RadGridView_Show_CellValidating(object sender, CellValidatingEventArgs e)
        {
            //GridViewDataColumn column = e.Column as GridViewDataColumn;
            try
            {
                if (e.Row is GridViewDataRowInfo && e.Column != null && e.Column.Name == "ITEMNAME")
                {
                    if (string.IsNullOrEmpty((string)e.Value) || ((string)e.Value).Trim() == string.Empty)
                    {
                        e.Cancel = true;
                        ((GridViewDataRowInfo)e.Row).ErrorText = "Validation error!";
                    }
                    else
                    {
                        ((GridViewDataRowInfo)e.Row).ErrorText = string.Empty;
                    }
                }
            }
            catch
            {

            }
        }
        private void RadGridView_Show_CellValidated(object sender, CellValidatedEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    if (!radGridView_Show.CurrentRow.Cells["RECEIVEITEM"].Value.ToString().Equals("1"))
                    {
                        string segno = radGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString();
                        string checkstatus = "0", ret = null;
                        switch (e.Column.FieldName)
                        {
                            case "ITEMNAME":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value.ToString());
                                break;
                            case "QTY":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["QTY"].Value.ToString());
                                break;
                            case "QTYEDIT":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["QTYEDIT"].Value.ToString());
                                break;
                            case "UNIT":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["UNIT"].Value.ToString());
                                break;
                            case "REMARK":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["REMARK"].Value.ToString());
                                break;
                            case "ORDERACCOUNT":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["ORDERACCOUNT"].Value.ToString());
                                break;
                            case "ACCOUNT":
                                ret = UpdateCellValue(segno, e.Column.FieldName, radGridView_Show.CurrentRow.Cells["ACCOUNT"].Value.ToString());
                                break;
                            case "PURCHASE":
                                if (radGridView_Show.CurrentCell.Value.ToString() == "On") { checkstatus = "1"; } //else { checkstatus = "0"; }
                                string sqlUppurchase = string.Format(@"UPDATE SHOP_MNOM_ITEM SET 
                                          PURCHASE = '{0}'
                                          ,PURCHWHOUP = '{1}'
                                          ,PURCHDATEUP = GETDATE()
                                          ,WHOUP = '{1}'
                                          ,WHOUPNAME = '{2}'
                                          ,DATEUP = GETDATE()
                                          WHERE SEQNO = '{3}'", checkstatus, SystemClass.SystemUserID_M, SystemClass.SystemUserName, segno);
                                ret = ConnectionClass.ExecuteSQL_Main(sqlUppurchase);
                                break;
                            case "REMAIM":
                                if (radGridView_Show.CurrentCell.Value.ToString() == "On") checkstatus = "1";   //else { checkstatus = "0"; }
                                ret = UpdateCellValue(segno, e.Column.FieldName, checkstatus);
                                break;
                            case "RECEIVEITEM":
                                if (radGridView_Show.CurrentCell.Value.ToString() == "On") checkstatus = "1";   //else { checkstatus = "0"; }
                                string sqlUpreceive = string.Format(@"UPDATE SHOP_MNOM_ITEM SET 
                                          RECEIVEITEM = '{0}'
                                          ,RECEIVEWHOUP = '{1}'
                                          ,RECEIVEDATE = GETDATE()
                                          ,WHOUP = '{1}'
                                          ,WHOUPNAME = '{2}'
                                          ,DATEUP = GETDATE()
                                          WHERE SEQNO = '{3}'", checkstatus, SystemClass.SystemUserID_M, SystemClass.SystemUserName, segno);
                                ret = ConnectionClass.ExecuteSQL_Main(sqlUpreceive);
                                break;
                            case "PAYMENT":
                                if (radGridView_Show.CurrentCell.Value.ToString() == "On") checkstatus = "1";   //else { checkstatus = "0"; }
                                ret = UpdateCellValue(segno, e.Column.FieldName, checkstatus);
                                break;

                            default:
                                break;
                        }

                        if (!string.IsNullOrEmpty(ret)) MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                    }
                }
            }
            catch
            {

            }
        }
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            string ret = DatagridClass.ExportExcelGridView("รายละเอียดการสั่ง", radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(ret);
        }
        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            if (dtData.Rows.Count > 0) dtData.Rows.Clear();
            this.GetDataOrder();
        }
        private void RadCheckBox_group_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            //this.radGridView_Show.SummaryRowsTop.Clear();
            if (radCheckBox_group.Checked == true)
            {
                this.radGridView_Show.GroupDescriptors.Clear();
                this.radGridView_Show.GroupDescriptors.Add(new GridGroupByExpression("DATEINS AS DATEINS format \"{0}\" Group By DATEINS"));
            }
            else
            {
                this.radGridView_Show.GroupDescriptors.Clear();
            }
        }
        private void RadCheckBox_CheckDate_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_CheckDate.Checked == true)
            {
                radDateTimePicker_OrderBegin.Enabled = true;
                radDateTimePicker_OrderEnd.Enabled = true;
            }
            else
            {
                radDateTimePicker_OrderBegin.Enabled = false;
                radDateTimePicker_OrderEnd.Enabled = false;
            }
        }
        #endregion
        #region Methode
        void SetTextEmpty()
        {
            radTextBox_Itemname.Text = string.Empty;
            radTextBox_Qty.Text = string.Empty;
            radTextBox_Unit.Text = string.Empty;
            radTextBox_OwnOrder.Text = string.Empty;
            radTextBox_Account.Text = string.Empty;
            radTextBox_Remark.Text = string.Empty;
        }
        private void GetDataOrder()
        {
            if (radCheckBox_CheckDate.Checked == true)
            {
                dtData = OrderWoodClass.GetItemOrderExt(radDateTimePicker_OrderBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_OrderEnd.Value.ToString("yyyy-MM-dd"), dtData);
            }
            else
            {
                dtData = OrderWoodClass.GetItemOrderExt("", "", dtData);
            }
        }
        private string GetSeqno()
        {
            DataTable dt = ConnectionClass.SelectSQL_Main(@"SELECT(MAX(SEQNO)+1) AS SEQNO FROM SHOP_MNOM_ITEM WITH(NOLOCK)");
            string ret = "";
            if (dt.Rows.Count > 0) ret = dt.Rows[0]["SEQNO"].ToString();
            return ret;
        }
        private DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null)
            {
                return newColumn;
            }
            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];

                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }
            return newColumn;
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        private string UpdateCellValue(string SegNo, string FieldName, string Value)
        {
            string sqlUp = $@"
                UPDATE  SHOP_MNOM_ITEM 
                SET     {FieldName} = '{Value}',
                        WHOUP = '{SystemClass.SystemUserID_M}',WHOUPNAME = '{SystemClass.SystemUserName}',DATEUP = GETDATE()
                WHERE SEQNO = '{SegNo}' ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        #endregion

        private void RadButtonElement_Document_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
    }
}