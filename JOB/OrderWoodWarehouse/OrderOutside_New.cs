﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderOutside_New : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dt_data = new DataTable();// = new DataTable();
        private DataTable dtDept = new DataTable();// = new DataTable();
        private DataTable dtCost = new DataTable();// = new DataTable();
        private DataTable dtVend = new DataTable();// = new DataTable();
        readonly string pSave = "0";
        string BillNo = "";
        readonly string TypeBill = "MNOM";
        Data_ITEMBARCODE data_ITEMBARCODE;
        public ArrayList dataBillAdd = new ArrayList();
        string payment = "";
        public OrderOutside_New()
        {
            InitializeComponent();
            this.KeyPreview = true;

        }
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        #region Event

        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        private void OrderInside_New_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ราคารวม", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMDIM", "ITEMDIM"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRICE", "PRICE"));
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadDropDownList));
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            radTextBox_Description.Font = SystemClass.SetFontGernaral;
            radTextBox_Description.ForeColor = ConfigClass.SetColor_Blue();

            radButton_FindDpt.ButtonElement.ToolTipText = "ค้นหาแผนก"; radButton_FindDpt.ButtonElement.ShowBorder = true;
            radButton_FindCost.ButtonElement.ToolTipText = "ค้นหางบ"; radButton_FindCost.ButtonElement.ShowBorder = true;
            radButton_Claer.ButtonElement.ToolTipText = "เคลียร์"; radButton_Claer.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ToolTipText = "เพิ่มรายการสินค้า"; radButton_Add.ButtonElement.ShowBorder = true;

            radGridView_Show.EnableFiltering = false;

            dt_data.Columns.Add("ITEMBARCODE");
            dt_data.Columns.Add("ITEMNAME");
            dt_data.Columns.Add("QTY");
            dt_data.Columns.Add("UNIT");
            dt_data.Columns.Add("AMOUNT");
            dt_data.Columns.Add("ITEMID");
            dt_data.Columns.Add("ITEMDIM");
            dt_data.Columns.Add("PRICE");

            SetTextControls(radGroupBox_DB.Controls);
            GetDataDropDownList();
            ClearData();
        }

        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            ClearDataDT();
        }
        private string GetValueDropDownList(RadDropDownList radDropDownList)
        {
            if (radDropDownList.SelectedIndex > -1) return radDropDownList.SelectedValue.ToString(); else return "";
        }
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Qty.Text) || string.IsNullOrEmpty(radTextBox_Unit.Text))
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter(radLabel3.Text);
                radTextBox_Qty.Focus();
                return;
            }

            this.data_ITEMBARCODE.Itembarcode_QTY = double.Parse(radTextBox_Qty.Text);
            this.data_ITEMBARCODE.Itembarcode_UNITID = radTextBox_Unit.Text;
            InsertDt();
        }
        private void RadTextBox_Qty_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }

                    this.data_ITEMBARCODE = new Data_ITEMBARCODE(radTextBox_Item.Text);
                    if (string.IsNullOrEmpty(data_ITEMBARCODE.Itembarcode_ITEMBARCODE))
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }

                    if (data_ITEMBARCODE.Itembarcode_UNITID == "")
                    {
                        radTextBox_Unit.Text = "ชิ้น";
                    }
                    else
                    {
                        radTextBox_Unit.Text = this.data_ITEMBARCODE.Itembarcode_UNITID;
                    }

                    radTextBox_ItemName.Text = this.data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME;
                    radTextBox_Item.Enabled = false;
                    radTextBox_Qty.Enabled = true;
                    radTextBox_Qty.Focus();
                    radTextBox_Unit.Enabled = false;
                    break;
                case Keys.PageDown://ระบุชื่อสินค้า [PageDown] 
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ชื่อสินค้า");
                        radTextBox_Item.Focus();
                        return;
                    }
                    this.data_ITEMBARCODE = new Data_ITEMBARCODE(radTextBox_Item.Text);
                    radTextBox_ItemName.Text = radTextBox_Item.Text;
                    this.data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME = radTextBox_Item.Text;
                    this.data_ITEMBARCODE.Itembarcode_UNITID = radTextBox_Unit.Text;
                    radTextBox_Item.Text = "";
                    radTextBox_Item.Enabled = false;
                    radTextBox_Unit.Text = "ชิ้น";
                    radTextBox_Unit.Enabled = true;
                    radTextBox_Qty.Enabled = true;
                    radTextBox_Qty.Focus();
                    break;

                case Keys.F3://ค้าหาสินค้า [F3] 
                    ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(radTextBox_Item.Text);
                    if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                    {
                        this.data_ITEMBARCODE = ShowDataDGV_Itembarcode.items;
                        radTextBox_Item.Text = data_ITEMBARCODE.Itembarcode_ITEMBARCODE; //radTextBox_Cst.Focus();
                        radTextBox_Unit.Text = data_ITEMBARCODE.Itembarcode_UNITID;
                        radTextBox_ItemName.Text = data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME;
                        radTextBox_Item.Enabled = false;
                        radTextBox_Unit.Enabled = false;
                        radTextBox_Qty.Enabled = true;
                        radTextBox_Qty.Focus();
                    }
                    else
                    {
                        radTextBox_Item.Text = ""; radTextBox_Item.Focus();
                    }
                    break;
                default:
                    break;
            }
        }
        private void RadTextBox_Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Unit.Enabled == false)
                {
                    this.data_ITEMBARCODE.Itembarcode_QTY = double.Parse(radTextBox_Qty.Text);
                    InsertDt();
                    return;
                }
                radTextBox_Unit.Focus();
            }
        }
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.data_ITEMBARCODE.Itembarcode_QTY = double.Parse(radTextBox_Qty.Text);
                this.data_ITEMBARCODE.Itembarcode_UNITID = radTextBox_Unit.Text;
                InsertDt();
            }
        }
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            //F2
            if (e.KeyCode == Keys.F2)
            {
                if (pSave == "0")
                {
                    string desc = radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value.ToString() + "  จำนวน  " +
                    radGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() + "  " +
                    radGridView_Show.CurrentRow.Cells["UNIT"].Value.ToString();

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete(desc) == DialogResult.No) return;

                    radGridView_Show.Rows.Remove(radGridView_Show.CurrentRow);
                    ClearDataDT();
                }
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (GetValueDropDownList(radDropDownList_Head) == "" || GetValueDropDownList(radDropDownList_Dpt) == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการเบิก");
                radTextBox_Item.Focus();
                return;
            }
            if (radGridView_Show.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการเบิก");
                radTextBox_Item.Focus();
                return;
            }
            if (radTextBox_Description.Text == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("หมายเหตุ");
                radTextBox_Description.Focus();
                return;
            }

            SaveData();
        }
        private void RadDropDownList_Dpt_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Item.Focus();
        }
        private void RadButton_FindDpt_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtDept
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Dpt.SelectedValue = frm.pID; //radTextBox_Cst.Focus();
            }
            else
            {
                radDropDownList_Dpt.SelectedIndex = -1; //radTextBox_Branch.Focus();
            }
        }
        private void RadButton_FindCost_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtCost
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Cost.SelectedValue = frm.pID; //radTextBox_Cst.Focus();
            }
            else
            {
                radDropDownList_Cost.SelectedIndex = -1; //radTextBox_Branch.Focus();
            }

        }

        private void RadButton_Vend_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtVend
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Vend.SelectedValue = frm.pID; //radTextBox_Cst.Focus();
            }
            else
            {
                radDropDownList_Vend.SelectedIndex = -1; //radTextBox_Branch.Focus();
            }

        }
        #endregion

        #region Method
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;
            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
            }

            foreach (RadButton c in controlCollection.OfType<RadButton>())
            {
                c.ButtonElement.ShowBorder = true;
            }
        }
        private void ClearData()
        {
            radLabel_Docno.Text = "";
            radTextBox_Item.Text = "";
            radTextBox_ItemName.Text = "";
            radTextBox_Unit.Text = "";
            radTextBox_Qty.Text = "";
            radTextBox_Description.Text = "";

            radButton_Save.Enabled = true;
            radTextBox_Item.Enabled = true;
            radTextBox_ItemName.Enabled = true;
            radTextBox_Qty.Enabled = true;
            radTextBox_Unit.Enabled = true;
            radButton_Add.Enabled = true;
            radButton_Claer.Enabled = true;
            radTextBox_Description.Enabled = true;

            radTextBox_Item.Focus();
            if (dt_data.Rows.Count > 0) dt_data.Rows.Clear();
            if (radGridView_Show.Rows.Count > 0) radGridView_Show.Rows.Clear();
            radDropDownList_Head.SelectedIndex = -1;
            radDropDownList_Dpt.SelectedIndex = -1;
            radDropDownList_Cost.SelectedIndex = -1;
            radDropDownList_Vend.SelectedIndex = -1;

        }
        private void ClearDataDT()
        {
            radTextBox_Item.Text = ""; radTextBox_Item.Enabled = true; radTextBox_Item.Focus();
            radTextBox_ItemName.Text = "";
            radTextBox_Qty.Text = ""; radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Text = ""; radTextBox_Unit.Enabled = false;
        }
        private void ClearDataItem()
        {
            data_ITEMBARCODE.Itembarcode_ITEMBARCODE = "";
            data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME = "";
            data_ITEMBARCODE.Itembarcode_QTY = 0;
            data_ITEMBARCODE.Itembarcode_UNITID = "";
            data_ITEMBARCODE.Itembarcode_ITEMID = "";
            data_ITEMBARCODE.Itembarcode_INVENTDIMID = "";
            data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP3 = 0;
        }
        private void GetDataDropDownList()
        {
        
            radDropDownList_Head.DataSource = JOB_Class.GetJOB_GroupSub("00006"); //GetHead();
            radDropDownList_Head.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_Head.DisplayMember = "JOBGROUPSUB_DESCRIPTION";

            radDropDownList_Dpt.DataSource = GetDpt();
            radDropDownList_Dpt.ValueMember = "DATA_ID";
            radDropDownList_Dpt.DisplayMember = "DATA_DESC";
            radDropDownList_Dpt.SelectedIndex = -1;

            radDropDownList_Cost.DataSource = GetCostCenter();
            radDropDownList_Cost.ValueMember = "DATA_ID";
            radDropDownList_Cost.DisplayMember = "DATA_DESC";
            radDropDownList_Cost.SelectedIndex = -1;


            radDropDownList_Vend.DataSource = GetVend();
            radDropDownList_Vend.ValueMember = "DATA_ID";
            radDropDownList_Vend.DisplayMember = "DATA_DESC";
            radDropDownList_Vend.SelectedIndex = -1;
        }
        //private DataTable GetHead()
        //{
        //    string sql = string.Format(@"
        //        SELECT  JOBGROUPSUB_ID AS HEADID, JOBGROUPSUB_DESCRIPTION AS HEADDESCRIPTION 
        //        FROM    SHOP_JOBGROUPSUB WITH (NOLOCK)
        //                INNER JOIN SHOP_JOBGROUP WITH (NOLOCK) ON SHOP_JOBGROUPSUB.JOBGROUP_ID = SHOP_JOBGROUP.JOBGROUP_ID
        //        WHERE   SHOP_JOBGROUP.JOBGROUP_ID = '00006'
        //        ORDER BY JOBGROUPSUB_ID ");

        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        private DataTable GetDpt()
        {
            dtDept = Models.DptClass.GetDpt_All();
            return dtDept;
        }
        private DataTable GetCostCenter()
        {
            dtCost = BillOutClass.GetCostCenter();
            return dtCost;
        }
        private DataTable GetVend()
        {
            dtVend = Models.VendTableClass.GetVENDTABLE_All("", "");
            return dtVend;
        }
        private void InsertDt()
        {
            if (radTextBox_Qty.Text == "" || radTextBox_Unit.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("จำนวน");
                return;
            }

            for (int i = 0; i < dt_data.Rows.Count; i++)
            {
                DataRow[] rows = dt_data.Select(string.Format(@"ITEMBARCODE = '{0}'", radTextBox_Item.Text));
                if (rows.Length > 0)
                {
                    int iRow = dt_data.Rows.IndexOf(rows[0]);
                    dt_data.Rows[iRow]["QTY"] = double.Parse(dt_data.Rows[iRow]["QTY"].ToString()) + double.Parse(radTextBox_Qty.Text);
                    dt_data.Rows[iRow]["AMOUNT"] = double.Parse(dt_data.Rows[iRow]["QTY"].ToString()) * double.Parse(dt_data.Rows[iRow]["PRICE"].ToString());
                    dt_data.AcceptChanges();
                    ClearDataItem();
                    ClearDataDT();
                    return;
                }
            }

            dt_data.Rows.Add(data_ITEMBARCODE.Itembarcode_ITEMBARCODE
                , data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME
                , data_ITEMBARCODE.Itembarcode_QTY
                , data_ITEMBARCODE.Itembarcode_UNITID
                , ((data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP3) * data_ITEMBARCODE.Itembarcode_QTY)
                , data_ITEMBARCODE.Itembarcode_ITEMID
                , data_ITEMBARCODE.Itembarcode_INVENTDIMID
                , data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP3);

            radGridView_Show.DataSource = dt_data;
            ClearDataItem();
            ClearDataDT();
        }
        private void SaveData()
        {
            if (radRadioButton_Cr.IsChecked == true)
            {
                payment = "CR";
            }
            else if (radRadioButton_Dr.IsChecked == true)
            {
                payment = "DR";
            }
            else
            {
                payment = "DR";
            }
            ArrayList strSql = new ArrayList();
            BillNo = ConfigClass.GetMaxINVOICEID(TypeBill, "-", TypeBill, "1");

            if (BillNo != "")
            {
                strSql.Add(string.Format(@"INSERT INTO SHOP_MNOM_HD ( DOCNO,USERCREATE,NAMECREATE,JOBGROUPSUB_ID,BRANCH_ID
                                ,COSTCENTERID,VENDID,DESCRIPTION,STATUS,PAYMENT,WHOUPD)
                        VALUES( '{0}','{1}','{2}','{3}','{4}'
                        ,'{5}','{6}','{7}','1','{8}','{1}')"
                        , BillNo
                        , SystemClass.SystemUserID
                        , SystemClass.SystemUserName
                        , GetValueDropDownList(radDropDownList_Head)
                        , GetValueDropDownList(radDropDownList_Dpt)
                        , GetValueDropDownList(radDropDownList_Cost)
                        , GetValueDropDownList(radDropDownList_Vend)
                        , radTextBox_Description.Text.Replace(",", "")
                        , payment));

                for (int i = 0; i < dt_data.Rows.Count; i++)
                {
                    strSql.Add(string.Format(@"INSERT INTO SHOP_MNOM_DT ( DOCNO,SEQNO,ITEMID,ITEMDIM,ITEMBARCODE,ITEMNAME,INVENTID,QTY,QTYEDIT,UNIT,PRICE,AMOUNT,WHOINS)
                            VALUES( '{0}','{1}','{2}','{3}','{4}'
                            ,'{5}','MN998','{6}','{6}','{7}','{8}','{9}','{10}')"
                            , BillNo
                            , i + 1
                            , dt_data.Rows[i]["ITEMID"].ToString()
                            , dt_data.Rows[i]["ITEMDIM"].ToString()
                            , dt_data.Rows[i]["ITEMBARCODE"].ToString()
                            , dt_data.Rows[i]["ITEMNAME"].ToString()
                            , double.Parse(dt_data.Rows[i]["QTY"].ToString())
                            , dt_data.Rows[i]["UNIT"].ToString()
                            , double.Parse(dt_data.Rows[i]["PRICE"].ToString())
                            , double.Parse(dt_data.Rows[i]["AMOUNT"].ToString())
                            , SystemClass.SystemUserID));
                }

                string returnExecute = ConnectionClass.ExecuteSQL_ArrayMain(strSql);

                if (returnExecute == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation(string.Format(@"บันทึกเรียบร้อย {0}", BillNo));
                    dataBillAdd.Add(BillNo);
                    ClearData();
                    //this.Close();
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกข้อมูลได้{Environment.NewLine}{returnExecute}");
                    radTextBox_Description.Focus();
                    return;
                }
            }
        }
        #endregion
    }
}