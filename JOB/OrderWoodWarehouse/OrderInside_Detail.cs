﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using System.Collections;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderInside_Detail : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDept = new DataTable();
        DataTable dtCost = new DataTable();
        DataTable dtHd = new DataTable();
        DataTable dt = new DataTable();
        DataTable dt_data = new DataTable();
        DATAPRODUCT _dATAPRODUCT;
        readonly string _pBillNo;
        // readonly string Dept;
        int billCount;
        int indexRow;
        int indexRows;
        string billCheck; string statusDoc; private string statusSendtoAX;
        string pCopyColor = "";
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        public OrderInside_Detail(string pBillNo)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pBillNo = pBillNo;
            //Dept =;
        }

        #region SetFontInRadGridview
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion
        //Load
        private void OrderInside_New_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SEQNO", "SEQNO"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYEDIT", "จำนวนรับ", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ราคารวม", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMDIM", "ITEMDIM"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRICE", "PRICE"));

            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadDropDownList));
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadTextBoxBase));
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadCheckBox));

            radButton_Save.ButtonElement.Font = new Font(new FontFamily("Tahoma"), 11.0f, FontStyle.Bold);
            radButton_Clos.ButtonElement.Font = new Font(new FontFamily("Tahoma"), 11.0f, FontStyle.Bold);
            radButton_Cancel.ButtonElement.Font = new Font(new FontFamily("Tahoma"), 10.0f, FontStyle.Bold);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radPanel_BillOut.PanelElement.PanelBorder.ForeColor = ConfigClass.SetColor_PurplePastel();
            radPanel_Rongmai.PanelElement.PanelBorder.ForeColor = ConfigClass.SetColor_PurplePastel();

            radButton_FindDpt.ButtonElement.ToolTipText = "ค้นหาแผนก"; radButton_FindCost.ButtonElement.ToolTipText = "ค้นหางบ";


            dt_data = OrderWoodClass.DataColumn();
            radGridView_Show.EnableFiltering = false;


            SetTextControls(radGroupBox_DB.Controls);
            SetTextControls(radPanel_BillOut.Controls);
            SetTextControls(radPanel_Rongmai.Controls);

            GetHeadDropDownList();
            GetDptDropDownList();
            GetCostCenterDropDownList();
            GetDescReciveDropDownList();
            SetEnable();
            switch (SystemClass.SystemDptID)
            {
                case "D120":
                    radTextBox_Description.Enabled = true;
                    GetEnable_BillOutApprove();
                    break;
                case "D062":
                    radRadioButton_Appbill.ButtonElement.ReadOnly = false;
                    radRadioButton_Noappbill.ButtonElement.ReadOnly = false;
                    radTextBox_Descbillout.Enabled = true;
                    break;
                case "MN998":
                    radCheckBox_Wait.ButtonElement.ReadOnly = false;
                    radTextBox_DescriptionWait.Enabled = true;
                    break;
                default:
                    break;
            }
            GetDataTable(_pBillNo);

        }
        #region Method
        private void SetEnable()
        {
            //รอของ
            radCheckBox_Wait.ReadOnly = true;
            radTextBox_DescriptionWait.Enabled = false;
            //อนุมัติเบิก
            radRadioButton_Appbill.ButtonElement.ReadOnly = true;
            radRadioButton_Noappbill.ButtonElement.ReadOnly = true;
            radTextBox_Descbillout.Enabled = false;
            //โรงไม้
            radDropDownList_Desc.Enabled = false;
            radTextBox_Descrongmai.Enabled = false;
            radButton_Printput.Enabled = false;
            radButton_SendtoAx.Enabled = false;

            radLabel_EmpAppr.Text = "";
        }
        private void GetEnable_ByHD()
        {
            radDropDownList_Head.Enabled = false;
            radDropDownList_Dpt.Enabled = false;
            radDropDownList_Cost.Enabled = false;
            radButton_FindDpt.Enabled = false;
            radButton_FindCost.Enabled = false;
            radTextBox_Description.Enabled = false;
        }
        private void GetEnable_BillOutApprove()
        {
            radRadioButton_Appbill.ButtonElement.ReadOnly = true;
            radRadioButton_Noappbill.ButtonElement.ReadOnly = true;
            radTextBox_Descbillout.Enabled = false;
        }
        private void GetEnable_WaitLabel()
        {
            radCheckBox_Wait.ButtonElement.ReadOnly = true;
            radTextBox_DescriptionWait.Enabled = false;
        }
        private void GetEnable_Rongmai()
        {
            radDropDownList_Desc.Enabled = false;
            radTextBox_Descrongmai.Enabled = false;
            radButton_Printput.Enabled = false;
            radButton_SendtoAx.Enabled = false;
        }
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;

            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
                if (c.Name == "radLabel_Func") c.Font = SystemClass.SetFontGernaral;
            }
            foreach (RadButton c in controlCollection.OfType<RadButton>())
            {
                c.ButtonElement.ShowBorder = true;
            }
            foreach (RadDropDownList c in controlCollection.OfType<RadDropDownList>())
            {
                c.ListElement.Font = SystemClass.SetFontGernaral;
                c.DropDownListElement.Font = SystemClass.SetFontGernaral;
                c.DropDownListElement.ForeColor = Class.ConfigClass.SetColor_Blue();
            }
            foreach (RadCheckBox c in controlCollection.OfType<RadCheckBox>())
            {
                c.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral_Bold;
            }
            foreach (RadPanel c in controlCollection.OfType<RadPanel>())
            {
                c.PanelElement.Font = SystemClass.SetFontGernaral_Bold;
            }
            foreach (RadTextBox c in controlCollection.OfType<RadTextBox>())
            {
                c.Font = SystemClass.SetFontGernaral;
                c.ForeColor = Class.ConfigClass.SetColor_Blue();
            }
        }
        private void GetDataTable(string BillHd)
        {
            //    string sql = string.Format(@"SELECT [DOCNO],CONVERT(VARCHAR,[DATECREATE],23) AS [DATECREATE]
            //                  ,[USERCREATE],[NAMECREATE],[JOBGROUPSUB_ID],[BRANCH_ID]
            //                  ,[COSTCENTERID],[WHOUPD],CONVERT(VARCHAR,[DATEUPD],23) AS [DATEUPD]
            //                  ,[STADOC],[STATUS],[PRINTCOUNT],[DESCRIPTION],[WAIT_STATUS],[WAIT_DESCRIPTION]
            //                  ,[APPROVESTATUS],[APPROVEWHOINS],[APPROVENAMEINS]
            //               ,CONVERT(VARCHAR,[APPROVEDATEINS],23) AS [APPROVEDATEINS]
            //                  ,[APPROVE_DESCRIPTION],[STATUSSENDTOAX],[WHOSENDTOAX],[NAMESENDTOAX]
            //               ,CONVERT(VARCHAR,[DATESENDTOAX],23) AS [DATESENDTOAX]
            //,[RECEIVER],[RECEIVEWHOINS],[RECIEVEDATEINS],RECEIVER_DESCRIPTION
            //                FROM [SHOP24HRS].[dbo].[SHOP_MNOM_HD] WITH (NOLOCK) WHERE DOCNO = '{0}' ", BillHd);
            dtHd = OrderWoodClass.GetData_MNOM_HD(BillHd); //ConnectionClass.SelectSQL_Main(sql);
            if (dtHd.Rows.Count > 0)
            {
                radDropDownList_Head.SelectedValue = dtHd.Rows[0]["JOBGROUPSUB_ID"].ToString();
                radDropDownList_Dpt.SelectedValue = dtHd.Rows[0]["BRANCH_ID"].ToString();
                radDropDownList_Cost.SelectedValue = dtHd.Rows[0]["COSTCENTERID"].ToString();
                radTextBox_Description.Text = dtHd.Rows[0]["DESCRIPTION"].ToString();
                radTextBox_DescriptionWait.Text = dtHd.Rows[0]["WAIT_DESCRIPTION"].ToString();
                radTextBox_Descbillout.Text = dtHd.Rows[0]["APPROVE_DESCRIPTION"].ToString();
                radTextBox_Descrongmai.Text = dtHd.Rows[0]["RECEIVER_DESCRIPTION"].ToString();
                radLabel_Docno.Text = dtHd.Rows[0]["DOCNO"].ToString();
                radDropDownList_Desc.SelectedValue = dtHd.Rows[0]["RECEIVER"].ToString();

                billCount = int.Parse(dtHd.Rows[0]["PRINTCOUNT"].ToString());
                statusDoc = dtHd.Rows[0]["STADOC"].ToString();
                billCheck = dtHd.Rows[0]["APPROVESTATUS"].ToString();
                statusSendtoAX = dtHd.Rows[0]["STATUSSENDTOAX"].ToString();
                radLabel_Emp.Text = "ผู้เปิด " + dtHd.Rows[0]["NAMECREATE"].ToString();

                if (dtHd.Rows[0]["WAIT_STATUS"].ToString() == "1")
                {
                    radCheckBox_Wait.Checked = true;
                    radCheckBox_Wait.ReadOnly = true;
                    radTextBox_DescriptionWait.Enabled = false;
                }
                //bill
                if (dtHd.Rows[0]["APPROVESTATUS"].ToString() == "1")
                {
                    GetEnable_ByHD();
                    GetEnable_BillOutApprove();
                    radLabel_EmpAppr.Text = "อนุมัติ " + dtHd.Rows[0]["APPROVENAMEINS"].ToString() + " [ " + dtHd.Rows[0]["APPROVEDATEINS"].ToString() + " ]";
                    if (dtHd.Rows[0]["STADOC"].ToString() == "3")
                    {
                        radRadioButton_Noappbill.IsChecked = true;
                    }
                    else
                    {
                        radRadioButton_Appbill.IsChecked = true;
                        if (SystemClass.SystemDptID == "MN998") radButton_Printput.Enabled = true;
                    }
                }
                //ยกเลิก
                if (dtHd.Rows[0]["STADOC"].ToString() == "3")
                {
                    GetEnable_ByHD();
                    GetEnable_BillOutApprove();
                    GetEnable_WaitLabel();
                    GetEnable_Rongmai();
                    radButton_Save.Enabled = false;
                    radButton_Cancel.Enabled = false;
                }
                //Wood
                if (billCount > 0)
                {
                    radButton_SendtoAx.Enabled = true;
                    radDropDownList_Desc.Enabled = true;
                    radTextBox_Descrongmai.Enabled = true;

                    radTextBox_Description.ReadOnly = true;
                    radTextBox_DescriptionWait.ReadOnly = true;
                }
                //SendtoAX
                if (statusSendtoAX == "1")
                {
                    GetEnable_Rongmai();
                    GetEnable_WaitLabel();
                    radButton_Save.Enabled = false;
                    radButton_Cancel.Enabled = false;
                }

                if (dtHd.Rows[0]["STADOC"].ToString() == "3")
                {
                    GetEnable_ByHD();
                    GetEnable_BillOutApprove();
                    GetEnable_WaitLabel();
                    GetEnable_Rongmai();
                    radButton_Save.Enabled = false;
                    radButton_Cancel.Enabled = false;
                }
            }

            dt = OrderWoodClass.QueryRowsItem(BillHd);
            if (dt.Rows.Count > 0)
            {
                radGridView_Show.DataSource = dt;
                dt_data = dt;
            }
        }
        private void GetHeadDropDownList()
        {
            //string sql = string.Format(@"SELECT JOBGROUPSUB_ID AS HEADID, JOBGROUPSUB_DESCRIPTION AS HEADDESCRIPTION FROM SHOP_JOBGROUPSUB WITH (NOLOCK)
            //    INNER JOIN SHOP_JOBGROUP WITH (NOLOCK) ON SHOP_JOBGROUPSUB.JOBGROUP_ID = SHOP_JOBGROUP.JOBGROUP_ID
            //    WHERE SHOP_JOBGROUP.JOBGROUP_ID = '00006' ORDER BY JOBGROUPSUB_ID ");

            DataTable dtHead = JOB_Class.GetJOB_GroupSub("00006"); //ConnectionClass.SelectSQL_Main(sql);
            if (dtHead.Rows.Count > 0)
            {
                radDropDownList_Head.DataSource = dtHead;
                radDropDownList_Head.ValueMember = "JOBGROUPSUB_ID";
                radDropDownList_Head.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
                radDropDownList_Head.SelectedIndex = -1;
            }
        }
        private void GetDptDropDownList()
        {
            //string sql = string.Format(@"SELECT NUM AS DATA_ID, NUM +' - ' + DESCRIPTION AS DATA_DESC FROM
            //         (SELECT NUM AS NUM, DESCRIPTION AS DESCRIPTION
            //         FROM[SHOP2013TMP].[dbo].[DIMENSIONS] WHERE DATAAREAID = N'SPC' AND DIMENSIONCODE = '0' AND COMPANYGROUP != 'Cancel' AND NUM LIKE 'D%'
            //         UNION SELECT BRANCH_ID AS NUM, BRANCH_NAME AS[DESCRIPTION]
            //         FROM[SHOP24HRS].[dbo].[SHOP_BRANCH] WHERE BRANCH_ID LIKE 'MN%' AND BRANCH_ID != 'MN000') TMP ORDER BY NUM");

            dtDept = Models.DptClass.GetDpt_All();
            //ConnectionClass.SelectSQL_Main(sql);
            if (dtDept.Rows.Count > 0)
            {
                radDropDownList_Dpt.DataSource = dtDept;
                radDropDownList_Dpt.ValueMember = "DATA_ID";
                radDropDownList_Dpt.DisplayMember = "DATA_DESC";
                radDropDownList_Dpt.SelectedIndex = -1;
            }
        }
        private void GetCostCenterDropDownList()
        {
            //string sql = string.Format(@"SELECT 	SPC_INVENTCOSTCENTERID AS DATA_ID,SPC_INVENTCOSTCENTERID + ' - ' + SPC_INVENTCOSTCENTERNAME AS DATA_DESC 
            //    FROM 	SHOP2013TMP.dbo.SPC_InventCostCenter WITH (NOLOCK) WHERE DATAAREAID = 'SPC' ORDER BY  SPC_INVENTCOSTCENTERID ");

            dtCost = BillOutClass.GetCostCenter();
            //ConnectionClass.SelectSQL_Main(sql);
            if (dtCost.Rows.Count > 0)
            {
                radDropDownList_Cost.DataSource = dtCost;
                radDropDownList_Cost.ValueMember = "DATA_ID";
                radDropDownList_Cost.DisplayMember = "DATA_DESC";
                radDropDownList_Cost.SelectedIndex = -1;
            }
        }
        private void GetDescReciveDropDownList()
        {
            //string sql = string.Format(@"SELECT JOBGROUPSUB_ID AS ID, JOBGROUPSUB_DESCRIPTION AS DESCRIPTION FROM SHOP_JOBGROUPSUB WITH (NOLOCK)
            //    INNER JOIN SHOP_JOBGROUP WITH (NOLOCK) ON SHOP_JOBGROUPSUB.JOBGROUP_ID = SHOP_JOBGROUP.JOBGROUP_ID
            //    WHERE SHOP_JOBGROUP.JOBGROUP_ID = '00011' ORDER BY JOBGROUPSUB_ID ");

            DataTable dtDesc = JOB_Class.GetJOB_GroupSub("00011"); //ConnectionClass.SelectSQL_Main(sql);
            if (dtDesc.Rows.Count > 0)
            {
                radDropDownList_Desc.DataSource = dtDesc;
                radDropDownList_Desc.ValueMember = "JOBGROUPSUB_ID";
                radDropDownList_Desc.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
                radDropDownList_Desc.SelectedIndex = -1;
            }
        }
        private int CountPtint()
        {
            string sql = $@"SELECT ISNULL(MAX(ISNULL(PRINTCOUNT,0)),0)+1 AS PRINTCOUNT  FROM SHOP_MNOM_HD WITH (NOLOCK) WHERE DOCNO = '{_pBillNo}' ";

            DataTable dtCount = ConnectionClass.SelectSQL_Main(sql);
            if (dtCount.Rows.Count > 0) return int.Parse(dtCount.Rows[0][0].ToString()); else return 0;
        }
        private void PrinterAX()
        {
            DialogResult result = PrintDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                PrintDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument2.PrinterSettings = PrintDialog1.PrinterSettings;
                pCopyColor = "บิลชมพู"; PrintDocument2.Print();
                pCopyColor = "บิลฟ้า"; PrintDocument2.Print();
                pCopyColor = "สีเขียว"; PrintDocument2.Print();
            }
        }
        private string SendBillToAx(DataTable dtData_Rows, string BillRec, string Remark, string typeRecive)
        {
            //ArrayList InStrAX = new ArrayList
            //{
            //    string.Format(@"INSERT INTO SPC_INVENTJOURNALTABLE
            //           (DATAAREAID,RECVERSION,RECID,JOURNALID,DESCRIPTION
            //           ,TRANSDATE,JOURNALTYPE,EMPLID,REMARKS,INVENTLOCATIONIDTO
            //           ,INVENTLOCATIONIDFROM,LOG,INVENTCOSTCENTERID,JOBID
            //           ,REFJOURNALID,DIMENSION ,DIMENSION2_,DIMENSION3_,MANUALPOSTED) 
            //           VALUES 
            //                ('SPC','1','1','{0}','สมุดเบิกสินค้า-{1}'
            //                ,'{2}','0','{3}','{4}','{5}'
            //                ,'MN998','','{6}',''
            //                ,'','{7}','','','1')"
            //                , dtData_Rows.Rows[0]["DOCNO"].ToString()
            //                , dtData_Rows.Rows[0]["DEPT"].ToString()
            //                , dtData_Rows.Rows[0]["DATECREATE"].ToString()
            //                , SystemClass.SystemUserID_M
            //                , dtData_Rows.Rows[0]["DESCRIPTION"].ToString()
            //                , dtData_Rows.Rows[0]["DEPT"].ToString()
            //                , dtData_Rows.Rows[0]["COSTCENTERID"].ToString()
            //                , dtData_Rows.Rows[0]["DEPT"].ToString())
            //};

            //for (int i = 0; i < dtData_Rows.Rows.Count; i++)
            //{
            //    if (double.Parse(dtData_Rows.Rows[i]["QTYEDIT"].ToString()) > 0)
            //    {
            //        InStrAX.Add(string.Format(@"INSERT INTO SPC_INVENTJOURNALTRANS
            //            (DATAAREAID,RECVERSION,RECID,JOURNALID,LINENUM
            //            ,TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY
            //            ,INVENTTRANSID,INVENTSERIALID,INVENTBATCHID,COSTPRICE)
            //        VALUES(
            //            'SPC','1','1','{0}','{1}'
            //            ,'{2}','{3}','{4}','{5}','{6}'
            //            ,'{0}_{7}','','','0')"
            //            , dtData_Rows.Rows[i]["DOCNO"].ToString()
            //            , i + 1
            //            , dtData_Rows.Rows[i]["DATECREATE"].ToString()
            //            , dtData_Rows.Rows[i]["ITEMBARCODE"].ToString()
            //            , dtData_Rows.Rows[i]["ITEMID"].ToString()
            //            , dtData_Rows.Rows[i]["ITEMDIM"].ToString()
            //            , double.Parse(dtData_Rows.Rows[i]["QTYEDIT"].ToString()) * (-1)
            //            , (i + 1).ToString()));
            //    }
            //}

            ArrayList InStrAX = AX_SendData.Save_SPC_INVENTJOURNALTABLE_FixIS(dtData_Rows);
            ArrayList UpdateStr24 = new ArrayList {
                $@" UPDATE   dbo.SHOP_MNOM_HD 
                    SET     [RECEIVER] = '{typeRecive}',[RECEIVEWHOINS] = '{SystemClass.SystemUserID}',[RECIEVEDATEINS] = GETDATE(),
                            [RECEIVER_DESCRIPTION] = '{Remark}',[STATUSSENDTOAX] = '1',[WHOSENDTOAX] = '{SystemClass.SystemUserID_M}',
                            [NAMESENDTOAX] = '{SystemClass.SystemUserName}',[DATESENDTOAX] = GETDATE(),[WHOUPD] = '{SystemClass.SystemUserID_M}',[DATEUPD] = GETDATE()
                            WHERE DOCNO = '{BillRec}' "
            };

            return ConnectionClass.ExecuteMain_AX_24_SameTime(UpdateStr24, InStrAX);
        }
        #endregion
        #region Event

        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;

            if (statusDoc == "3")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถแก้ไขข้อมูลได้ทุกกรณี เนื่องจากบิลถูกยกเลิก");
                return;
            }

            if (statusSendtoAX == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถแก้ไขข้อมูลได้ทุกกรณี เนื่องจากส่งบิลเข้า AX แล้ว...");
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.F1:
                    indexRow = radGridView_Show.CurrentRow.Index;
                    indexRows = int.Parse(radGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString());
                    Editdatarow frmEdit = new Editdatarow((int)InputStatus.EditeItem, dt_data.Rows[indexRow], SystemClass.SystemDptID, "", billCount);

                    if (frmEdit.ShowDialog(this) == DialogResult.OK)
                    {
                        this._dATAPRODUCT = frmEdit.dATAPRODUCT;
                        string returnUpDate = OrderWoodClass.UpdateDataRow(_dATAPRODUCT, SystemClass.SystemUserID, _pBillNo, indexRows);
                        MsgBoxClass.MsgBoxShow_SaveStatus(returnUpDate);
                        if (returnUpDate == "")
                        {
                            radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value = _dATAPRODUCT.ITEMBARCODE;
                            radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value = _dATAPRODUCT.ITEMNAME;
                            radGridView_Show.CurrentRow.Cells["QTY"].Value = _dATAPRODUCT.QTY;
                            radGridView_Show.CurrentRow.Cells["QTYEDIT"].Value = _dATAPRODUCT.QTYEDIT;
                            radGridView_Show.CurrentRow.Cells["UNIT"].Value = _dATAPRODUCT.UNIT;
                            radGridView_Show.CurrentRow.Cells["AMOUNT"].Value = _dATAPRODUCT.AMOUNT;
                            radGridView_Show.CurrentRow.Cells["ITEMID"].Value = _dATAPRODUCT.ITEMID;
                            radGridView_Show.CurrentRow.Cells["ITEMDIM"].Value = _dATAPRODUCT.ITEMID;
                            radGridView_Show.CurrentRow.Cells["PRICE"].Value = _dATAPRODUCT.PRICE;
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    break;

                case Keys.F7:
                    indexRow = radGridView_Show.CurrentRow.Index;
                    indexRows = int.Parse(radGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString());
                    Editdatarow frmWait = new Editdatarow((int)InputStatus.InsertItemForWait, dt_data.Rows[indexRow], SystemClass.SystemDptID, _pBillNo, billCount);
                    if (frmWait.ShowDialog(this) == DialogResult.OK)
                    {
                        this._dATAPRODUCT = frmWait.dATAPRODUCT;
                        string Remark = frmWait.radTextBox_Desc.Text;
                        string returnExcet = OrderWoodClass.InsertbillWait_ORExternal(Class.ConfigClass.GetMaxINVOICEID("MNOM", "-", "MNOM", "1")
                       , _dATAPRODUCT, SystemClass.SystemUserID, SystemClass.SystemUserName, Remark, _pBillNo, indexRows, "1", "0");

                        if (returnExcet == "")
                        {
                            //string returnUpDate = OrderWoodClass.DeleteDatarow(indexRows, _pBillNo);
                            string returnUpDate = OrderWoodClass.UpdateDatarow_Qty(_dATAPRODUCT, SystemClass.SystemUserID, _pBillNo, indexRows);
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnUpDate);
                            if (returnUpDate == "")
                            {
                                GetDataTable(_pBillNo);
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnExcet);
                            return;
                        }
                    }
                    break;

                case Keys.F8:
                    indexRow = radGridView_Show.CurrentRow.Index;
                    indexRows = int.Parse(radGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString());
                    Editdatarow frmOutOrder = new Editdatarow((int)InputStatus.InsertItemForVend, dt_data.Rows[indexRow], SystemClass.SystemDptID, _pBillNo, billCount);
                    if (frmOutOrder.ShowDialog(this) == DialogResult.OK)
                    {
                        this._dATAPRODUCT = frmOutOrder.dATAPRODUCT;
                        string Remark = frmOutOrder.radTextBox_Desc.Text;
                        string returnExcet = OrderWoodClass.InsertbillWait_ORExternal(Class.ConfigClass.GetMaxINVOICEID("MNOM", "-", "MNOM", "1")
                       , _dATAPRODUCT, SystemClass.SystemUserID, SystemClass.SystemUserName, Remark, _pBillNo, indexRows, "0", "1");

                        if (returnExcet == "")
                        {
                            //string returnUpDate = OrderWoodClass.DeleteDatarow(indexRows, _pBillNo);
                            string returnUpDate = OrderWoodClass.UpdateDatarow_Qty(_dATAPRODUCT, SystemClass.SystemUserID, _pBillNo, indexRows);
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnUpDate);
                            if (returnUpDate == "")
                            {
                                GetDataTable(_pBillNo);
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnExcet);
                            return;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string _returnExc = "";
            if (billCheck == "0")
            {
                if (radRadioButton_Appbill.IsChecked == true || radRadioButton_Noappbill.IsChecked == true)
                {
                    billCheck = "1";
                    if (radRadioButton_Noappbill.IsChecked == true)
                    {
                        if (string.IsNullOrEmpty(radTextBox_Descbillout.Text))
                        {
                            MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการอนุมัติ");
                            radTextBox_Descbillout.Focus();
                            return;
                        }
                        else
                        {
                            statusDoc = "3";
                        }
                    }
                }

                if (radDropDownList_Head.SelectedIndex <= -1
                    || radDropDownList_Dpt.SelectedIndex <= -1
                    || radDropDownList_Cost.SelectedIndex <= -1)
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("การเบิก");
                    radTextBox_Description.Focus();
                    return;
                }
                if (SystemClass.SystemDptID == "D062")
                {
                    _returnExc = OrderWoodClass.UpdateApprove(_pBillNo, SystemClass.SystemUserID, SystemClass.SystemUserName
                        , billCheck, statusDoc, radDropDownList_Head.SelectedValue.ToString(), radDropDownList_Dpt.SelectedValue.ToString()
                        , radDropDownList_Cost.SelectedValue.ToString(), radTextBox_Descbillout.Text.Replace("'", " "), radTextBox_Description.Text.Replace("'", " "));
                }
                else if (SystemClass.SystemDptID == "D120")
                {
                    _returnExc = OrderWoodClass.UpdateDataHD(_pBillNo, SystemClass.SystemUserID, radDropDownList_Head.SelectedValue.ToString(), radDropDownList_Dpt.SelectedValue.ToString()
                            , radDropDownList_Cost.SelectedValue.ToString(), "", "", radTextBox_Description.Text.Replace("'", " "));
                }

                MsgBoxClass.MsgBoxShow_SaveStatus(_returnExc);
                if (_returnExc != "")
                {
                    radTextBox_Description.Focus();
                    return;
                }
                else
                {
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    return;
                }
            }
            else
            {
                if (radCheckBox_Wait.Checked == true)
                {
                    if (radTextBox_DescriptionWait.Text != "")
                    {
                        _returnExc = OrderWoodClass.UpdateWait(_pBillNo, SystemClass.SystemUserID, radTextBox_DescriptionWait.Text);
                        MsgBoxClass.MsgBoxShow_SaveStatus(_returnExc);
                        if (_returnExc != "")
                        {
                            radTextBox_Description.Focus();
                            return;
                        }
                        else
                        {
                            this.DialogResult = DialogResult.Yes;
                            this.Close();
                            return;
                        }
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการรอของ.");
                        radTextBox_DescriptionWait.Focus();
                        return;
                    }
                }
            }
        }
        private void RadButton_FindDpt_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtDept
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Dpt.SelectedValue = frm.pID;
            }
            else
            {
                radDropDownList_Dpt.SelectedIndex = -1;
            }
        }
        private void RadButton_FindCost_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtCost
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Cost.SelectedValue = frm.pID;
            }
            else
            {
                radDropDownList_Cost.SelectedIndex = -1;
            }

        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
        private void RadDropDownList_Desc_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Descrongmai.TextBoxElement.TextBoxItem.HostedControl.Select();
        }
        private void RadRadioButton_Appbill_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_Appbill.IsChecked == true) radTextBox_Descbillout.Focus();
        }
        private void RadRadioButton_Noappbill_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_Noappbill.IsChecked == true) radTextBox_Descbillout.TextBoxElement.TextBoxItem.HostedControl.Select();
        }

        private void RadButton_Printput_Click(object sender, EventArgs e)
        {
            if (billCheck == "1")
            {
                DialogResult result = PrintDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    PrintDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                    PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings;
                    PrintDocument1.Print();
                    GetDataTable(_pBillNo);
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("บิลยังไม่อนุมัติเบิกไม่สามารถพิมพ์จัดได้.");
                radTextBox_Descrongmai.Focus();
                return;
            }
        }
        private void RadButton_SendtoAx_Click(object sender, EventArgs e)
        {
            //CHECK.ITEMBARCODE
            int tempBacode = 0;
            int tempQty = 0;
            string remark;

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() == "")
                {
                    tempBacode += 1;
                    break;
                }
            }
            if (tempBacode > 0)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("บาร์โค้ดสินค้า");
                radTextBox_Descrongmai.Focus();
                return;
            }

            for (int j = 0; j < radGridView_Show.Rows.Count; j++)
            {
                if (double.Parse(radGridView_Show.Rows[j].Cells["QTYEDIT"].Value.ToString()) == double.Parse(radGridView_Show.Rows[j].Cells["QTYEDIT"].Value.ToString()))
                {
                    tempQty += 1;
                    break;
                }
            }
            if (tempQty > 0)
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันรับสินค้าเท่ากับจำนวนที่สั่ง ?") == DialogResult.No)
                {
                    radTextBox_Descrongmai.Focus();
                    return;
                }
            }

            if (billCount == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถยืนยันบิลเข้า AX ได้ เนื่องจากยังไม่จัดสินค้า.");
                radTextBox_Descrongmai.Focus();
                return;
            }

            if (radDropDownList_Desc.SelectedIndex <= -1)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("การรับของ");
                radTextBox_Descrongmai.Focus();
                return;
            }
            else if (radDropDownList_Desc.Text == "อื่นๆ" && string.IsNullOrEmpty(radTextBox_Descrongmai.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("หมายเหตุการรับของ");
                radTextBox_Descrongmai.Focus();
                return;
            }

            if (string.IsNullOrEmpty(radTextBox_Descrongmai.Text))
            {
                remark = radDropDownList_Desc.Text;
            }
            else
            {
                remark = radDropDownList_Desc.Text + " : " + radTextBox_Descrongmai.Text;
            }

            string sqlSelect = $@"
                SELECT  [SHOP_MNOM_HD].DOCNO,[SEQNO],[ITEMID],[ITEMDIM],[ITEMBARCODE],[ITEMNAME],[INVENTID],
                        [QTY],[QTYEDIT],[UNIT],CONVERT(VARCHAR, [SHOP_MNOM_HD].[DATECREATE], 23) AS[DATECREATE],
                        [SHOP_MNOM_HD].[BRANCH_ID]  AS DEPT ,[SHOP_MNOM_HD].[COSTCENTERID] ,[SHOP_MNOM_HD].[DESCRIPTION]
                FROM    [SHOP_MNOM_DT] WITH(NOLOCK)
                        INNER JOIN [SHOP_MNOM_HD] WITH(NOLOCK) ON[SHOP_MNOM_DT].DOCNO = [SHOP_MNOM_HD].DOCNO
                WHERE   [SHOP_MNOM_HD].DOCNO = '{_pBillNo}' ";

            string returnSendtoAx = SendBillToAx(ConnectionClass.SelectSQL_Main(sqlSelect), _pBillNo
                , remark, radDropDownList_Desc.SelectedValue.ToString());
            if (returnSendtoAx != "")
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(returnSendtoAx);
                radTextBox_Descrongmai.Focus();
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(returnSendtoAx);
                PrinterAX();
                this.DialogResult = DialogResult.Yes;
                this.Close();
                return;
            }
        }
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = _pBillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            billCount = CountPtint();
            //e.Graphics.DrawString("พิมพ์ครั้งที่ " + billCount.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            if (billCount > 1)
            {
                e.Graphics.DrawString("พิมพ์ครั้งที่ " + billCount.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
                Y += 20;
            }
            e.Graphics.DrawString("ใบจัดสินค้าโรงไม้เชิงทะเล.", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(radDropDownList_Dpt.SelectedValue.ToString() + "-" + Models.DptClass.GetDptName_ByDptID(radDropDownList_Dpt.SelectedValue.ToString()), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                Y += 20;
                if (radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() == "")
                {
                    e.Graphics.DrawString((i + 1).ToString() + ".(" + radGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                    radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + ")   " +
                                    radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                       SystemClass.printFont, Brushes.Black, 0, Y);
                }
                else
                {
                    e.Graphics.DrawString((i + 1).ToString() + ".(" + radGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                      radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + ")   " +
                                      radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                         SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString(" " + radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                        SystemClass.printFont, Brushes.Black, 0, Y);
                }
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("รวมจำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString() + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(radTextBox_Description.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            Y += 30;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        private void PrintDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = _pBillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            e.Graphics.DrawString("ใบเบิกสินค้าโรงไม้เชิงทะเล." + "[ " + pCopyColor + " ]", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(radDropDownList_Dpt.SelectedValue.ToString() + "-" + Models.DptClass.GetDptName_ByDptID(radDropDownList_Dpt.SelectedValue.ToString()), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ".(" + radGridView_Show.Rows[i].Cells["QTYEDIT"].Value.ToString() + " " +
                                  radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + ")   " +
                                  radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(" " + radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("รวมจำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString() + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(radTextBox_Description.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            Y += 30;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        private void RadButton_Clos_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //ยกเลิกทั้งบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันยกเลิกการสั่งของทั้งบิล ?") == DialogResult.No)
            {
                radTextBox_Description.Focus();
                return;
            }
            else
            {
                InputData inputfrm = new InputData("1", "ระบุหมายเหตุการยกเลิก", "", "");
                if (inputfrm.ShowDialog(this) == DialogResult.Yes)
                {
                    string returnExc = OrderWoodClass.UpdateCancelBill(_pBillNo, "3", SystemClass.SystemUserID, inputfrm.pInputData, "");
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);

                    if (returnExc != "")
                    {
                        radTextBox_Description.Focus();
                        return;
                    }
                    else
                    {
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                        return;
                    }
                }
            }
        }
        private void PrintDocument1_EndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string sql = $@"
                UPDATE  dbo.SHOP_MNOM_HD 
                SET     PRINTCOUNT = '{CountPtint()}',WHOUPD = '{SystemClass.SystemUserID}',DATEUPD = GETDATE()
                WHERE   DOCNO = '{_pBillNo}' ";

            ConnectionClass.ExecuteSQL_Main(sql);
        }
        private void RadCheckBox_Wait_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Wait.Checked == true)
            {
                radTextBox_DescriptionWait.Enabled = true;
                radTextBox_DescriptionWait.Focus();
            }
            else
            {
                radTextBox_DescriptionWait.Enabled = false;
            }
        }
        #endregion
    }
}