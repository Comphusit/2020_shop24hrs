﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderOutside_DetailNew : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDept = new DataTable();
        DataTable dtCost = new DataTable();
        readonly DataTable dtHd = new DataTable();
        private DataTable dt = new DataTable();
        DataTable dt_data = new DataTable();
        DataTable dtVend = new DataTable();
        DataTable dtRowBill = new DataTable();
        DataTable dtRowOld = new DataTable();
        DataTable dtPXE = new DataTable();
        DATAPRODUCT _dATAPRODUCT;
        DATA_ORDERTABLE dATA_ORDERTABLE;
        readonly string _pBillNo;
        //readonly string Dept;
        int indexRow;
        int indexRows;
        string Payment;
        string statusDoc;
        string statusPxe;
        public OrderOutside_DetailNew(string pBillNo)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pBillNo = pBillNo;
            //Dept = SystemClass.SystemDptID;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        #region SetFontInRadGridview
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion
        //Load
        private void OrderOutside_DetailNew_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SEQNO", "SEQNO"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTYEDIT", "จำนวนรับ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ราคารวม", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMDIM", "ITEMDIM"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRICE", "PRICE"));

            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadDropDownList));
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadTextBoxBase));
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadCheckBox));

            radButton_Save.ButtonElement.Font = new Font(new FontFamily("Tahoma"), 11.0f, FontStyle.Bold);
            radButton_Clos.ButtonElement.Font = new Font(new FontFamily("Tahoma"), 11.0f, FontStyle.Bold);
            radButton_Cancel.ButtonElement.Font = new Font(new FontFamily("Tahoma"), 10.0f, FontStyle.Bold);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_FindDpt.ButtonElement.ToolTipText = "ค้นหาแผนก";
            radButton_FindCost.ButtonElement.ToolTipText = "ค้นหางบ";



            radGroupBox1.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            DatagridClass.SetDefaultRadGridView(radGridView_Doc);
            radGridView_Doc.TableElement.RowHeight = 35;
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 200));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ราคารวม", 80));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMDIM", "ITEMDIM"));
            radGridView_Doc.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRICE", "PRICE"));
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadDropDownList));
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            dt_data = OrderWoodClass.DataColumn();
            radGridView_Show.EnableFiltering = false;

            radTextBox_DescriptionWait.Enabled = false;
            radButton_Addbill.Enabled = false;
            SetTextControls(radGroupBox_DB.Controls);
            GetDataDropDownList();
            GetDataTable(_pBillNo);

            radGridView_Doc.EnableFiltering = false;
            SetTextControls(radGroupBox_DB.Controls);
            ClearData();
            DataTable dts = OrderWoodClass.CheckDocNoUse("1", _pBillNo);
            if (dts.Rows[0]["PXEDOCNO"].ToString() != "")
            {
                dtPXE = PurchClass.OrderWood_GetPXE(dts.Rows[0]["PXEDOCNO"].ToString(), "0");
                LoadDataShow();
                SetEnablePxe();
            }
            else
            {
                dtRowBill = OrderWoodClass.QueryRowsItem(_pBillNo);
            }
            CheckItembacode();

        }
        #region Method
        private void GetEnable_ByHD()
        {
            radDropDownList_Head.Enabled = false;
            radDropDownList_Dpt.Enabled = false;
            radDropDownList_Cost.Enabled = false;
            radButton_FindDpt.Enabled = false;
            radButton_FindCost.Enabled = false;
            radTextBox_Description.Enabled = false;
        }
        private void GetEnable_WaitLabel()
        {
            radCheckBox_Wait.ButtonElement.ReadOnly = true;
            radTextBox_DescriptionWait.Enabled = false;
        }
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;

            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
                if (c.Name == "radLabel_Func")
                {
                    c.Font = SystemClass.SetFontGernaral;
                }
            }
            foreach (RadButton c in controlCollection.OfType<RadButton>())
            {
                c.ButtonElement.ShowBorder = true;
            }
            foreach (RadDropDownList c in controlCollection.OfType<RadDropDownList>())
            {
                c.ListElement.Font = SystemClass.SetFontGernaral;
                c.DropDownListElement.Font = SystemClass.SetFontGernaral;
                c.DropDownListElement.ForeColor = Class.ConfigClass.SetColor_Blue();
            }
            foreach (RadCheckBox c in controlCollection.OfType<RadCheckBox>())
            {
                c.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral_Bold;
            }
            foreach (RadPanel c in controlCollection.OfType<RadPanel>())
            {
                c.PanelElement.Font = SystemClass.SetFontGernaral_Bold;
            }
            foreach (RadTextBox c in controlCollection.OfType<RadTextBox>())
            {
                c.Font = SystemClass.SetFontGernaral;
                c.ForeColor = Class.ConfigClass.SetColor_Blue();
            }
        }
        private void GetDataTable(string BillHd)
        {
            dATA_ORDERTABLE = new DATA_ORDERTABLE(BillHd);
            radDropDownList_Head.SelectedValue = dATA_ORDERTABLE.JOBGROUPSUB_ID;
            radDropDownList_Dpt.SelectedValue = dATA_ORDERTABLE.BRANCH_ID;
            radDropDownList_Cost.SelectedValue = dATA_ORDERTABLE.COSTCENTERID;
            radDropDownList_Vend.SelectedValue = dATA_ORDERTABLE.VENDID;
            radTextBox_Description.Text = dATA_ORDERTABLE.DESCRIPTION;
            radTextBox_DescriptionWait.Text = dATA_ORDERTABLE.WAIT_DESCRIPTION;
            radLabel_Docno.Text = dATA_ORDERTABLE.DOCNO;
            radLabel_Emp.Text = dATA_ORDERTABLE.NAMECREATE;
            statusDoc = dATA_ORDERTABLE.STADOC;
            Payment = dATA_ORDERTABLE.PAYMENT;
            statusPxe = dATA_ORDERTABLE.PXEDOCNO;

            switch (Payment)
            {
                case "DR":
                    radRadioButton_Dr.IsChecked = true;
                    break;
                case "CR":
                    radRadioButton_Cr.IsChecked = true;
                    break;
                default:
                    radRadioButton_Dr.IsChecked = false; radRadioButton_Cr.IsChecked = false;
                    break;
            }

            if (dATA_ORDERTABLE.WAIT_STATUS.Equals("1"))
            {
                radCheckBox_Wait.Checked = true;
                radCheckBox_Wait.ReadOnly = true;
                radTextBox_DescriptionWait.Enabled = false;
            }
            //ยกเลิก
            if (statusDoc == "3")
            {
                GetEnable_ByHD();
                GetEnable_WaitLabel();
                radButton_Save.Enabled = false;
                radButton_Cancel.Enabled = false;
            }

            if (!statusPxe.Equals(""))
            {
                SetEnable();
                radButton_Addbill.Text = "ดูบิล";
            }

            dt = OrderWoodClass.QueryRowsItem(BillHd);
            if (dt.Rows.Count > 0)
            {
                radGridView_Show.DataSource = dt;
                dt_data = dt;
            }
        }
        private void GetDataDropDownList()
        {
            radDropDownList_Head.DataSource = JOB_Class.GetJOB_GroupSub("00006"); //GetHead();
            radDropDownList_Head.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_Head.DisplayMember = "JOBGROUPSUB_DESCRIPTION";

            radDropDownList_Dpt.DataSource = GetDpt();
            radDropDownList_Dpt.ValueMember = "DATA_ID";
            radDropDownList_Dpt.DisplayMember = "DATA_DESC";
            radDropDownList_Dpt.SelectedIndex = -1;

            radDropDownList_Cost.DataSource = GetCostCenter();
            radDropDownList_Cost.ValueMember = "DATA_ID";
            radDropDownList_Cost.DisplayMember = "DATA_DESC";
            radDropDownList_Cost.SelectedIndex = -1;


            radDropDownList_Vend.DataSource = GetVend();
            radDropDownList_Vend.ValueMember = "DATA_ID";
            radDropDownList_Vend.DisplayMember = "DATA_DESC";
            radDropDownList_Vend.SelectedIndex = -1;
        }
        //private DataTable GetHead()
        //{
        //    string sql = string.Format(@"SELECT JOBGROUPSUB_ID AS HEADID, JOBGROUPSUB_DESCRIPTION AS HEADDESCRIPTION FROM SHOP_JOBGROUPSUB WITH (NOLOCK)
        //        INNER JOIN SHOP_JOBGROUP WITH (NOLOCK) ON SHOP_JOBGROUPSUB.JOBGROUP_ID = SHOP_JOBGROUP.JOBGROUP_ID
        //        WHERE SHOP_JOBGROUP.JOBGROUP_ID = '00006' ORDER BY JOBGROUPSUB_ID ");

        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        private DataTable GetDpt()
        {
            //string sql = string.Format(@"SELECT NUM AS DATA_ID, NUM +' - ' + DESCRIPTION AS DATA_DESC FROM
            //         (SELECT NUM AS NUM, DESCRIPTION AS DESCRIPTION
            //         FROM[SHOP2013TMP].[dbo].[DIMENSIONS] WHERE DATAAREAID = N'SPC' AND DIMENSIONCODE = '0' AND COMPANYGROUP != 'Cancel' AND NUM LIKE 'D%'
            //         UNION SELECT BRANCH_ID AS NUM, BRANCH_NAME AS[DESCRIPTION]
            //         FROM[SHOP24HRS].[dbo].[SHOP_BRANCH] WHERE BRANCH_ID LIKE 'MN%' AND BRANCH_ID != 'MN000') TMP
            //      ORDER BY NUM");

            dtDept = Models.DptClass.GetDpt_All();
            //ConnectionClass.SelectSQL_Main(sql);
            return dtDept;
        }
        private DataTable GetCostCenter()
        {
            //string sql = string.Format(@"SELECT 	SPC_INVENTCOSTCENTERID AS DATA_ID,SPC_INVENTCOSTCENTERID + ' - ' + SPC_INVENTCOSTCENTERNAME AS DATA_DESC 
            //    FROM 	SHOP2013TMP.dbo.SPC_InventCostCenter WITH (NOLOCK) WHERE DATAAREAID = 'SPC' ORDER BY  SPC_INVENTCOSTCENTERID ");

            dtCost = BillOutClass.GetCostCenter();
            //ConnectionClass.SelectSQL_Main(sql);
            return dtCost;
        }
        private DataTable GetVend()
        {
            //string sql = string.Format(@"SELECT ACCOUNTNUM AS DATA_ID,ACCOUNTNUM + ' : ' + NAME AS DATA_DESC 
            //    FROM SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) WHERE DATAAREAID = 'SPC'  ");

            dtVend = Models.VendTableClass.GetVENDTABLE_All("", "");
            //ConnectionClass.SelectSQL_Main(sql);
            return dtVend;
        }
        private string Getpayment()
        {
            if (radRadioButton_Dr.IsChecked == true)
            {
                Payment = "DR";
            }
            else if (radRadioButton_Cr.IsChecked == true)
            {
                Payment = "CR";
            }
            return Payment;
        }
        private void SetEnable()
        {
            radDropDownList_Head.Enabled = false;
            radDropDownList_Dpt.Enabled = false;
            radDropDownList_Cost.Enabled = false;
            radDropDownList_Vend.Enabled = false;
            radButton_FindDpt.Enabled = false;
            radButton_FindCost.Enabled = false;
            radButton_Vend.Enabled = false;
            radTextBox_Description.Enabled = false;
            radTextBox_DescriptionWait.Enabled = false;
            radButton_Save.Enabled = false;
            radButton_Cancel.Enabled = false;
            radRadioButton_Dr.ReadOnly = true;
            radRadioButton_Cr.ReadOnly = true;
            radCheckBox_Wait.ReadOnly = true;

        }
        private void CheckItembacode()
        {
            for (int i = 0; i < dt_data.Rows.Count; i++)
            {
                if (!dt_data.Rows[i]["ITEMBARCODE"].ToString().Equals("")) continue;
                radTextBox_DocNo.Enabled = false;
            }
        }
        #endregion
        #region Event
        private void RadTextBox_Qty_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;

            if (statusDoc == "3")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถแก้ไขข้อมูลได้ทุกกรณี เนื่องจากบิลถูกยกเลิก");
                return;
            }
            if (statusPxe != "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถแก้ไขข้อมูลได้ทุกกรณี เนื่องจากบิลมีบิลแล้ว");
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.F1:
                    indexRow = radGridView_Show.CurrentRow.Index;
                    indexRows = int.Parse(radGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString());
                    Editdatarow frmEdit = new Editdatarow((int)InputStatus.EditeItem, dt_data.Rows[indexRow], SystemClass.SystemDptID, "", 0);
                    if (frmEdit.ShowDialog(this) == DialogResult.OK)
                    {
                        this._dATAPRODUCT = frmEdit.dATAPRODUCT;
                        string returnUpDate = OrderWoodClass.UpdateDataRow(_dATAPRODUCT, SystemClass.SystemUserID, _pBillNo, indexRows);
                        MsgBoxClass.MsgBoxShow_SaveStatus(returnUpDate);
                        if (returnUpDate == "")
                        {
                            radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value = _dATAPRODUCT.ITEMBARCODE;
                            radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value = _dATAPRODUCT.ITEMNAME;
                            radGridView_Show.CurrentRow.Cells["QTY"].Value = _dATAPRODUCT.QTY;
                            radGridView_Show.CurrentRow.Cells["QTYEDIT"].Value = _dATAPRODUCT.QTYEDIT;
                            radGridView_Show.CurrentRow.Cells["UNIT"].Value = _dATAPRODUCT.UNIT;
                            radGridView_Show.CurrentRow.Cells["AMOUNT"].Value = _dATAPRODUCT.AMOUNT;
                            radGridView_Show.CurrentRow.Cells["ITEMID"].Value = _dATAPRODUCT.ITEMID;
                            radGridView_Show.CurrentRow.Cells["ITEMDIM"].Value = _dATAPRODUCT.ITEMDIM;
                            radGridView_Show.CurrentRow.Cells["PRICE"].Value = _dATAPRODUCT.PRICE;
                        }
                        else
                        {
                            return;
                        }
                    }
                    break;
                case Keys.F4:
                    indexRow = radGridView_Show.RowCount;
                    indexRows = int.Parse(radGridView_Show.Rows[indexRow - 1].Cells["SEQNO"].Value.ToString());

                    Editdatarow frmAddrow = new Editdatarow((int)InputStatus.InsertItem, dt_data.Rows[indexRow - 1], SystemClass.SystemDptID, _pBillNo, 0);
                    if (frmAddrow.ShowDialog(this) == DialogResult.OK)
                    {
                        this._dATAPRODUCT = frmAddrow.dATAPRODUCT;
                        string returnUpDate = OrderWoodClass.InsertDataRow(_dATAPRODUCT, SystemClass.SystemUserID, _pBillNo, indexRows + 1);
                        MsgBoxClass.MsgBoxShow_SaveStatus(returnUpDate);
                        if (returnUpDate == "")
                        {
                            dt = OrderWoodClass.QueryRowsItem(_pBillNo);
                            if (dt.Rows.Count > 0)
                            {
                                radGridView_Show.DataSource = dt;
                                dt_data = dt;
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    break;

                case Keys.F7:
                    indexRow = radGridView_Show.CurrentRow.Index;
                    indexRows = int.Parse(radGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString());
                    Editdatarow frmWait = new Editdatarow((int)InputStatus.InsertItemForVend, dt_data.Rows[indexRow], SystemClass.SystemDptID, _pBillNo, 0);
                    if (frmWait.ShowDialog(this) == DialogResult.OK)
                    {
                        this._dATAPRODUCT = frmWait.dATAPRODUCT;
                        string Remark = frmWait.radTextBox_Desc.Text;
                        string returnExcet = OrderWoodClass.InsertbillWait_ORExternal(Class.ConfigClass.GetMaxINVOICEID("MNOM", "-", "MNOM", "1")
                       , _dATAPRODUCT, SystemClass.SystemUserID, SystemClass.SystemUserName, Remark, _pBillNo, indexRows, "1", "1");

                        if (returnExcet == "")
                        {
                            string returnUpDate = OrderWoodClass.UpdateDatarow_Qty(_dATAPRODUCT, SystemClass.SystemUserID, _pBillNo, indexRows);
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnUpDate);
                            if (returnUpDate == "")
                            {
                                GetDataTable(_pBillNo);
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnExcet);
                            return;
                        }
                    }
                    break;
                default:
                    break;
            }
            CheckItembacode();
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radDropDownList_Head.SelectedIndex <= -1 || radDropDownList_Dpt.SelectedIndex <= -1)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("การเบิก");
                radTextBox_Description.Focus();
                return;
            }

            if (radCheckBox_Wait.Checked == true)
            {
                if (string.IsNullOrEmpty(radTextBox_DescriptionWait.Text))
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการรอของ.");
                    radTextBox_DescriptionWait.Focus();
                    return;
                }
                else
                {
                    string _returnExc = OrderWoodClass.UpdateWait(_pBillNo, SystemClass.SystemUserID, radTextBox_DescriptionWait.Text);
                    MsgBoxClass.MsgBoxShow_SaveStatus(_returnExc);
                    if (_returnExc != "")
                    {
                        radTextBox_Description.Focus();
                        return;
                    }
                    else
                    {
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                        return;
                    }
                }
            }
            else
            {
                string _returnExc = OrderWoodClass.UpdateDataHD(_pBillNo, SystemClass.SystemUserID, radDropDownList_Head.SelectedValue.ToString(), radDropDownList_Dpt.SelectedValue.ToString()
                    , GetValueDropDownList(radDropDownList_Cost), radDropDownList_Vend.SelectedValue.ToString(), Getpayment(), radTextBox_Description.Text.Replace("'", " "));
                MsgBoxClass.MsgBoxShow_SaveStatus(_returnExc);
                if (_returnExc != "")
                {
                    radTextBox_Description.Focus();
                    return;
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกข้อมูลได้{Environment.NewLine}{_returnExc}");
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    return;
                }
            }

        }
        private void RadButton_FindDpt_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtDept
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Dpt.SelectedValue = frm.pID;
            }
            else
            {
                radDropDownList_Dpt.SelectedIndex = -1;
            }
        }
        private void RadButton_FindCost_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtCost
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Cost.SelectedValue = frm.pID;
            }
            else
            {
                radDropDownList_Cost.SelectedIndex = -1;
            }

        }
        private void RadButton_Vend_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtVend
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Vend.SelectedValue = frm.pID;
            }
            else
            {
                radDropDownList_Vend.SelectedIndex = -1;
            }
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        private void RadButton_Clos_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //ยกเลิกทั้งบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันยกเลิกการสั่งของทั้งบิล?") == DialogResult.No)
            {
                radTextBox_Description.Focus();
                return;
            }
            else
            {
                InputData inputfrm = new InputData("1", "ระบุหมายเหตุการยกเลิก", "", "");
                if (inputfrm.ShowDialog(this) == DialogResult.Yes)
                {
                    string returnExc = OrderWoodClass.UpdateCancelBill(_pBillNo, "3", SystemClass.SystemUserID, inputfrm.pInputData, "");
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);

                    if (returnExc != "")
                    {
                        radTextBox_Description.Focus();
                        return;
                    }
                    else
                    {
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                        return;
                    }
                }
            }
        }
        private void RadCheckBox_Wait_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Wait.Checked == true)
            {
                radTextBox_DescriptionWait.Enabled = true;
                radTextBox_DescriptionWait.Focus();
            }
            else
            {
                radTextBox_DescriptionWait.Enabled = false;
            }
        }

        private string GetValueDropDownList(RadDropDownList radDropDownList)
        {
            if (radDropDownList.SelectedIndex > -1)
            {
                return radDropDownList.SelectedValue.ToString();
            }
            else
            {
                return "";
            }
        }
        #endregion
        #region PXE
        private void RadButton_Update_Click(object sender, EventArgs e)
        {
            if (radTextBox_DocNo.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุเลขที่บิลทุกครั้งก่อนบันทึก.");
                radTextBox_DocNo.Focus();
                return;
            }

            if (radGridView_Doc.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกได้ ข้อมูลบิลไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                radTextBox_DocNo.Focus();
                return;
            }
            //SaveData();
            string sql = $@"
                UPDATE  dbo.SHOP_MNOM_HD 
                SET     WHOUPD = '{SystemClass.SystemUserID}',DATEUPD = GETDATE(),[RECEIVEWHOINS] = '{SystemClass.SystemUserID}',
                        [RECIEVEDATEINS] = GETDATE(),PXEDOCNO = '{radTextBox_DocNo.Text}'
                WHERE   DOCNO = '{_pBillNo}' ";

            string returnExc = Controllers.ConnectionClass.ExecuteSQL_Main(sql);

            MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
            if (returnExc == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกเรียบร้อย.");
                radButton_Update.Enabled = false;
                radButton_Clear.Enabled = false;
                radButton_Save.Enabled = false;
                radButton_Cancel.Enabled = false;
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถบันทึกได้ ลองใหม่อีกครั้ง.");
                radTextBox_DocNo.Focus();
                return;
            }
        }
        private void ClearData()
        {
            radLabel_Docno.Text = "";
            radTextBox_DocNo.Text = "";
            radLabel_Vend.Text = "";
            radLabel_Invoid.Text = "";
            radLabel_Amout.Text = "";

            radTextBox_DocNo.Enabled = true;
            radButton_Update.Enabled = false;
            radButton_Clear.Enabled = false;

            if (dtPXE.Rows.Count > 0) dtPXE.Rows.Clear();

            radGridView_Doc.DataSource = dtPXE;
        }
        private void SetEnablePxe()
        {
            radButton_Update.Enabled = false;
            radButton_Clear.Enabled = false;
            radTextBox_DocNo.Enabled = false;
        }
        private void FindDocumentPXE(string pDocPXE)
        {
            int count = 0;
            double Qty_checkrow;
            DataTable data = OrderWoodClass.DataColumnCheck();
            dtPXE = PurchClass.OrderWood_GetPXE(pDocPXE, "0");
            if (dtPXE.Rows.Count == 0) dtPXE = PurchClass.OrderWood_GetPXE(pDocPXE, "1");

            if (dtPXE.Rows.Count > 0)
            {
                if (dtPXE.Rows[0]["ORDERACCOUNT"].ToString() != dATA_ORDERTABLE.VENDID)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ผู้จำหน่ายไม่ถูกต้อง เช็คใหม่อีกครั้ง.");
                    radTextBox_DocNo.Focus();
                    return;
                }

                DataTable dtuse = OrderWoodClass.CheckDocNoUse("0", dtPXE.Rows[0]["PURCHID"].ToString());
                if (dtuse.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPXE.Rows.Count; i++)
                    {
                        DataRow[] rows = dtRowBill.Select(string.Format(@"ITEMBARCODE = '{0}'", dtPXE.Rows[i]["ITEMBARCODE"].ToString()));
                        if (rows.Length > 0)
                        {
                            count++;
                            Qty_checkrow = double.Parse(dtPXE.Rows[i]["QTY"].ToString()) - double.Parse(rows[0].ItemArray[8].ToString());
                            data.Rows.Add(dtPXE.Rows[i]["ITEMBARCODE"].ToString(), Qty_checkrow);
                        }
                    }

                    for (int j = 0; j < dtuse.Rows.Count; j++)
                    {
                        dtRowOld = OrderWoodClass.QueryRowsItem(dtuse.Rows[j]["DOCNO"].ToString());
                        for (int k = 0; k < data.Rows.Count; k++)
                        {
                            for (int l = 0; l < dtRowOld.Rows.Count; l++)
                            {

                                if (data.Rows[k]["ITEMBARCODE"].ToString() == dtRowOld.Rows[l]["ITEMBARCODE"].ToString())
                                {
                                    data.Rows[k]["QTY"] = double.Parse(data.Rows[k]["QTY"].ToString()) - double.Parse(dtRowOld.Rows[l]["QTY"].ToString());
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dtPXE.Rows.Count; i++)
                    {
                        DataRow[] rows = dtRowBill.Select(string.Format(@"ITEMBARCODE = '{0}'", dtPXE.Rows[i]["ITEMBARCODE"].ToString()));
                        if (rows.Length > 0)
                        {
                            count++;
                            Qty_checkrow = double.Parse(dtPXE.Rows[i]["QTY"].ToString()) - double.Parse(rows[0].ItemArray[8].ToString());
                            data.Rows.Add(dtPXE.Rows[i]["ITEMBARCODE"].ToString(), Qty_checkrow);
                        }
                    }
                }
                if (count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าไม่ตรงกัน เช็คข้อมูลใหม่อีกครั้ง.");
                    radTextBox_DocNo.Focus();
                    return;
                }
                //ok
                if (count > 0 && count <= dtPXE.Rows.Count)
                {
                    for (int ii = 0; ii < data.Rows.Count; ii++)
                    {
                        if (double.Parse(data.Rows[ii][1].ToString()) < 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("เลขที่เอกสารมีการใช้งานไปแล้ว จำนวนสินค้าบางตัวอาจไม่ถูกต้อง เช็คข้อมูลใหม่อีกครั้ง.");
                            radTextBox_DocNo.Focus();
                            return;
                        }
                        LoadDataShow();
                        radButton_Update.Enabled = true;
                        radButton_Clear.Enabled = true;
                    }
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบเลขที่บิล เช็คใหม่อีกครั้ง.");
                radTextBox_DocNo.Focus();
                return;
            }
        }
    
        //private DataTable CheckUseDoc(string pDoc)
        //{
        //    string sql = string.Format(@"SELECT [DOCNO],[PXEDOCNO] FROM[SHOP24HRS].[dbo].[SHOP_MNOM_HD]  WITH (NOLOCK)  WHERE [PXEDOCNO] = '{0}'", pDoc);
        //    DataTable dt = ConnectionClass.SelectSQL_Main(sql);
        //    return dt;
        //}
        //private DataTable PXEDoc(string pDoc)
        //{
        //    string sql = string.Format(@"SELECT [DOCNO],[PXEDOCNO] FROM[SHOP24HRS].[dbo].[SHOP_MNOM_HD]  WITH (NOLOCK)  WHERE [DOCNO] = '{0}'", pDoc);
        //    DataTable dt = ConnectionClass.SelectSQL_Main(sql);
        //    return dt;
        //}


        private void LoadDataShow(double Sum = 0)
        {
            radTextBox_DocNo.Text = dtPXE.Rows[0]["PURCHID"].ToString(); radTextBox_DocNo.Enabled = false;
            radLabel_Vend.Text = dtPXE.Rows[0]["ORDERACCOUNT"].ToString() + " - " + dtPXE.Rows[0]["PURCHNAME"].ToString();
            radLabel_Invoid.Text = dtPXE.Rows[0]["DOCUMENTNUM"].ToString();

            radGridView_Doc.DataSource = dtPXE;
            for (int i = 0; i <= dtPXE.Rows.Count - 1; i++)
            {
                Sum += double.Parse(dtPXE.Rows[i]["AMOUNT"].ToString());
            }

            radLabel_Amout.Visible = true;
            radLabel_Amout.Text = Sum.ToString("#,##0.00");
        }
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                    if (string.IsNullOrEmpty(radTextBox_DocNo.Text))
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุเลขที่ PXE ให้เรียบร้อยก่อน Enter");
                        radTextBox_DocNo.Focus();
                        return;
                    }

                    if (radGridView_Doc.Rows.Count > 0)
                    {
                        for (int i = 0; i <= radGridView_Doc.Rows.Count - 1; i++)
                            radGridView_Doc.Rows.RemoveAt(0);
                    }
                    FindDocumentPXE(radTextBox_DocNo.Text);
                    break;
                default:
                    break;
            }
        }
        private void RadButton_Cancle_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        #endregion
    }
}