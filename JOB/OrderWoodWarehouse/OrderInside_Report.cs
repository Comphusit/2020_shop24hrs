﻿//CheckOK
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderInside_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData = new DataTable();
        public OrderInside_Report()
        {
            InitializeComponent();
            this.KeyPreview = true;

        }
        #region ROWSNUMBER

        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //ToolTipText
        private void RadGridView_Show_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == radGridView_Show.Columns["BRANCH_ID"].Index)
                {
                    if (e.Row is GridViewDataRowInfo)
                    {
                        e.CellElement.ToolTipText = string.Format(@"แผนกเบิก : {0},{1}"
                            , radGridView_Show.Rows[e.RowIndex].Cells["BRANCH_ID"].Value.ToString()
                            , radGridView_Show.Rows[e.RowIndex].Cells["DIMENSIONSNAME"].Value.ToString());
                    }
                }
                else if (e.ColumnIndex == radGridView_Show.Columns["COSTCENTERID"].Index)
                {
                    if (e.Row is GridViewDataRowInfo)
                    {
                        e.CellElement.ToolTipText = string.Format(@"งบเบิก : {0},{1}"
                            , radGridView_Show.Rows[e.RowIndex].Cells["COSTCENTERID"].Value.ToString()
                            , radGridView_Show.Rows[e.RowIndex].Cells["COSTCENTERNAME"].Value.ToString());
                    }
                }
            }
        }
        #endregion

        #region Events 

        private void SetConditions()
        {
            ConditionalFormattingObject c1 = new ConditionalFormattingObject("Orange, applied to entire row", ConditionTypes.Equal, "3", "", true)
            {
                RowBackColor = Color.FromArgb(255, 219, 166),
                CellBackColor = Color.FromArgb(255, 219, 166),
                RowForeColor = Color.Black,
                CellForeColor = Color.Black
            };
            radGridView_Show.Columns["STADOC"].ConditionalFormattingObjectList.Add(c1);

            DatagridClass.SetCellBackClolorByExpression("QTY", " RECEIVEWHOINS = '' ", ConfigClass.SetColor_YellowPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("QTYEDIT", " RECEIVEWHOINS = '' ", ConfigClass.SetColor_YellowPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("QTYEDIT", " QTYEDIT > QTY ", Color.Tomato, radGridView_Show);
        }
        private void OrderInside_Report_Load(object sender, EventArgs e)
        {
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่ม";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButton_Find.ButtonElement.ShowBorder = true;

            radCheckBox_CheckDate.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Status.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Desc);
            radDropDownList_Desc.DropDownListElement.ForeColor = ConfigClass.SetColor_Blue();

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderBegin, DateTime.Today, DateTime.Now.Date);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderEnd, DateTime.Today, DateTime.Now.Date);

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATECREATE", "วันที่", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATERECORD", "ทั้งหมด[วัน]", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATERECORDEDIT", "แก้ไขล่าสุด[วัน]", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนสั่ง", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYEDIT", "จำนวนรับ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("UNIT", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ราคารวม", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "รายละเอียดการเบิก", 400));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOBGROUPSUB_ID", "ช่าง"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ช่างผู้สั่ง", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "แผนกเบิก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONSNAME", "แผนกเบิก", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COSTCENTERID", "งบเบิก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COSTCENTERNAME", "งบเบิก", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMECREATE", "ผู้บันทึก", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ใช้งาน"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("APPROVENAMEINS", "ผู้อนุมัติ", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("APPROVEDATEINS", "เวลาอนุมัติ", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RECEIVER_DESCRIPTION", "รายละเอียดการรับสินค้า", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RECEIVEWHOINS", "บันทึกรับสินค้า", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RECIEVEDATEINS", "เวลาบันทึกรับ", 150));

            radGridView_Show.Columns["DATECREATE"].FormatString = "{0:dd/MM/yyyy}";
            radGridView_Show.Columns["DOCNO"].IsPinned = true;
            radDateTimePicker_OrderBegin.Value = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            radDateTimePicker_OrderEnd.Value = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
            radGridView_Show.MasterTemplate.Columns["STADOC"].IsVisible = false;

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            this.SetConditions();
            radStatusStrip1.SizingGrip = false;

            SetEnable();
            GetdataAll();// FindData_ByCondition();
            GetGridViewSummary();
            GetDptDropDownList();
        }
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            OrderInside_New frmCam_New = new OrderInside_New();
            frmCam_New.ShowDialog();
            ArrayList arraybill = frmCam_New.dataBillAdd;
            if (arraybill.Count > 0)
            {
                GetdataAll();
            }
        }
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.CurrentRow.Index > -1)
            {
                OrderInside_Detail frmCam_Detail = new OrderInside_Detail(radGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString());
                if (frmCam_Detail.ShowDialog() == DialogResult.Yes) { }
                GetdataAll();
            }
        }
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0)
            {
                return;
            }
            else
            {
                string returnExportStatus = DatagridClass.ExportExcelGridView("สั่งของจากโรงไม้เชิงทะเล", radGridView_Show, "1");
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExportStatus);
            }
        }
        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            GetdataAll(); //FindData_ByCondition();
        }
        private void RadCheckBox_CheckDate_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_CheckDate.Checked == true)
            {
                radDateTimePicker_OrderBegin.Enabled = true;
                radDateTimePicker_OrderEnd.Enabled = true;
            }
            else
            {
                radDateTimePicker_OrderBegin.Enabled = false;
                radDateTimePicker_OrderEnd.Enabled = false;
            }
        }
        private void RadCheckBox_Status_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Status.Checked == true)
            {
                radDropDownList_Desc.Enabled = true;
                radDropDownList_Desc.SelectedIndex = 0;
            }
            else
            {
                radDropDownList_Desc.Enabled = false;
            }
        }
        #endregion

        #region Methode
        private void GetDptDropDownList()
        {
            DataTable NewData = new DataTable();
            NewData.Columns.Add("ID");
            NewData.Columns.Add("SHOW");

            NewData.Rows.Add("01", "ทั้งหมด");
            NewData.Rows.Add("02", "ใช้งาน");
            NewData.Rows.Add("03", "ยังไม่อนุมัติ");
            NewData.Rows.Add("04", "อนุมัติ");
            NewData.Rows.Add("05", "สั่งจัดแล้ว");
            NewData.Rows.Add("06", "รอของ");

            radDropDownList_Desc.DataSource = NewData;
            radDropDownList_Desc.ValueMember = "ID";
            radDropDownList_Desc.DisplayMember = "SHOW";
            radDropDownList_Desc.SelectedIndex = -1;

        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("ITEMNAME", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void SetEnable()
        {
            radDateTimePicker_OrderBegin.Enabled = false;
            radDateTimePicker_OrderEnd.Enabled = false;
            radDropDownList_Desc.Enabled = false;
            radCheckBox_CheckDate.Checked = true;
            radCheckBox_CheckDate.ReadOnly = true;
        }
        private void GetdataAll()
        {
            this.Cursor = Cursors.WaitCursor;
            dtData = BillOutClass.OrderWood_QueryDataReport("0", radDateTimePicker_OrderBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_OrderEnd.Value.ToString("yyyy-MM-dd"));
            radGridView_Show.DataSource = dtData;
            if (dtData.Rows.Count == 0) MsgBoxClass.MsgBoxShow_FindRecordNoData("ในเงื่อนไขที่กำหนด");
            this.Cursor = Cursors.Default;
        }

        //private void FindData_ByCondition()
        //{
        //    dtData = SQLORDERQUERY.QueryDataReport("0", radDateTimePicker_OrderBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_OrderEnd.Value.ToString("yyyy-MM-dd"));
        //    radGridView_Show.DataSource = dtData;

        //}
        #endregion


        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}