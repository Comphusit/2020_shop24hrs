﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderOutside_Addbill : Telerik.WinControls.UI.RadForm
    {
        DataTable dt = new DataTable();
        readonly string _pBill;
        readonly DataTable dt_data = new DataTable();
        readonly DATA_ORDERTABLE dATA_ORDERTABLE;
        DataTable dtRow = new DataTable();
        DataTable dtRowOld = new DataTable();
        double Sum;
        public OrderOutside_Addbill(string pBill)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pBill = pBill;
            dATA_ORDERTABLE = new DATA_ORDERTABLE(_pBill);
        }
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        #region Event

        private void OrderOutside_Addbill_Load(object sender, EventArgs e)
        {
            DatagridClass.SetTextControls(radGroupBox_DB.Controls, (int)(ComMinimart.Camera.CollectionControl.RadDropDownList));
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            radGroupBox1.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radButton_Claer.ButtonElement.ToolTipText = "เคลียร์";
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ราคารวม", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMDIM", "ITEMDIM"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRICE", "PRICE"));


            radGridView_Show.EnableFiltering = false;

            SetTextControls(radGroupBox_DB.Controls);
            ClearData();

            DataTable dts = OrderWoodClass.CheckDocNoUse("1", _pBill);
            if (dts.Rows[0]["PXEDOCNO"].ToString() != "")
            {
                dt = PurchClass.OrderWood_GetPXE(dts.Rows[0]["PXEDOCNO"].ToString(), "0");
                LoadDataShow();
                SetEnable();
            }
            else
            {
                dtRow = OrderWoodClass.QueryRowsItem(_pBill);
            }
        }

        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            ClearData();
            ClearDataDT();
        }

        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                    if (string.IsNullOrEmpty(radTextBox_Item.Text))
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุเลขที่ PXE ให้เรียบร้อยก่อน Enter");
                        radTextBox_Item.Focus();
                        return;
                    }

                    FindDocumentPXE(radTextBox_Item.Text);
                    break;
                default:
                    break;
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_Item.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุเลขที่บิลทุกครั้งก่อนบันทึก.");
                radTextBox_Item.Focus();
                return;
            }

            if (radGridView_Show.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกได้ ข้อมูลบิลไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                radTextBox_Item.Focus();
                return;
            }

            //SaveData();
            string sql = $@"
                UPDATE  dbo.SHOP_MNOM_HD 
                SET     WHOUPD = '{SystemClass.SystemUserID}',DATEUPD = GETDATE(),[RECEIVEWHOINS] = '{SystemClass.SystemUserID}',
                        [RECIEVEDATEINS] = GETDATE(),PXEDOCNO = '{ radTextBox_Item.Text}'
                WHERE   DOCNO = '{_pBill}' ";

            string returnExc = ConnectionClass.ExecuteSQL_Main(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
            if (returnExc == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                radTextBox_Item.Focus();
                return;
            }

        }
        #endregion
        #region Method
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;

            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
            }

            foreach (RadButton c in controlCollection.OfType<RadButton>())
            {
                c.ButtonElement.ShowBorder = true;
            }
        }
        private void ClearData()
        {
            radLabel_Docno.Text = "";
            radTextBox_Item.Text = "";
            radLabel_Vend.Text = "";
            radLabel_Invoid.Text = "";
            radLabel_Amout.Text = "";

            radButton_Save.Enabled = false;
            radTextBox_Item.Enabled = true;
            radButton_Claer.Enabled = true;
        }
        private void ClearDataDT()
        {
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            radGridView_Show.DataSource = dt;
        }
        private void SetEnable()
        {
            radButton_Save.Enabled = false;
            radTextBox_Item.Enabled = false;
            radButton_Claer.Enabled = false;
        }
        public void FindDocumentPXE(string pDocPXE)
        {
            int count = 0;
            double Qty_checkrow;

            DataTable data = OrderWoodClass.DataColumnCheck();
            if (radGridView_Show.Rows.Count > 0)
            {
                for (int i = 0; i <= radGridView_Show.Rows.Count - 1; i++)
                    radGridView_Show.Rows.RemoveAt(0);
            }

            dt = PurchClass.OrderWood_GetPXE(pDocPXE, "0");
            if (dt.Rows.Count == 0) dt = PurchClass.OrderWood_GetPXE(pDocPXE, "1");

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["ORDERACCOUNT"].ToString() != dATA_ORDERTABLE.VENDID)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ผู้จำหน่ายไม่ถูกต้อง เช็คใหม่อีกครั้ง.");
                    radTextBox_Item.Focus();
                    return;
                }

                DataTable dtuse = OrderWoodClass.CheckDocNoUse("0", dt.Rows[0]["PURCHID"].ToString());
                if (dtuse.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow[] rows = dtRow.Select(string.Format(@"ITEMBARCODE = '{0}'", dt.Rows[i]["ITEMBARCODE"].ToString()));
                        if (rows.Length > 0)
                        {
                            count++;
                            Qty_checkrow = double.Parse(dt.Rows[i]["QTY"].ToString()) - double.Parse(rows[0].ItemArray[8].ToString());
                            data.Rows.Add(dt.Rows[i]["ITEMBARCODE"].ToString(), Qty_checkrow);
                        }
                    }

                    for (int j = 0; j < dtuse.Rows.Count; j++)
                    {
                        dtRowOld = OrderWoodClass.QueryRowsItem(dtuse.Rows[j]["DOCNO"].ToString());
                        for (int k = 0; k < data.Rows.Count; k++)
                        {
                            for (int l = 0; l < dtRowOld.Rows.Count; l++)
                            {

                                if (data.Rows[k]["ITEMBARCODE"].ToString() == dtRowOld.Rows[l]["ITEMBARCODE"].ToString())
                                {
                                    data.Rows[k]["QTY"] = double.Parse(data.Rows[k]["QTY"].ToString()) - double.Parse(dtRowOld.Rows[l]["QTY"].ToString());
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow[] rows = dtRow.Select(string.Format(@"ITEMBARCODE = '{0}'", dt.Rows[i]["ITEMBARCODE"].ToString()));
                        if (rows.Length > 0)
                        {
                            count++;
                            Qty_checkrow = double.Parse(dt.Rows[i]["QTY"].ToString()) - double.Parse(rows[0].ItemArray[8].ToString());
                            data.Rows.Add(dt.Rows[i]["ITEMBARCODE"].ToString(), Qty_checkrow);
                        }
                    }
                }

                //ok
                if (count > 0 && count <= dt.Rows.Count)
                {
                    for (int ii = 0; ii < data.Rows.Count; ii++)
                    {
                        if (double.Parse(data.Rows[ii][1].ToString()) < 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("เลขที่เอกสารมีการใช้งานไปแล้ว จำนวนสินค้าบางตัวอาจไม่ถูกต้อง เช็คข้อมูลใหม่อีกครั้ง.");
                            radTextBox_Item.Focus();
                            return;
                        }
                        LoadDataShow();
                        radButton_Save.Enabled = true;
                    }
                }
            }
        }


        //private DataTable PXEDoc(string pDoc)
        //{
        //    string sql = $@"SELECT [DOCNO],[PXEDOCNO] FROM [SHOP_MNOM_HD]  WITH (NOLOCK)  WHERE [DOCNO] = '{pDoc}' ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        private void LoadDataShow()
        {
            radTextBox_Item.Text = dt.Rows[0]["PURCHID"].ToString(); radTextBox_Item.Enabled = false;
            radLabel_Vend.Text = dt.Rows[0]["ORDERACCOUNT"].ToString() + " - " + dt.Rows[0]["PURCHNAME"].ToString();
            radLabel_Invoid.Text = dt.Rows[0]["DOCUMENTNUM"].ToString();

            radGridView_Show.DataSource = dt;
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                Sum += double.Parse(dt.Rows[i]["AMOUNT"].ToString());
            }

            radLabel_Amout.Visible = true;
            radLabel_Amout.Text = Sum.ToString("#,##0.00");
        }
        #endregion
    }
}