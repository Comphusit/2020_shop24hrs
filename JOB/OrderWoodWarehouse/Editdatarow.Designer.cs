﻿namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    partial class Editdatarow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editdatarow));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Claer = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Unit = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Qty = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_ItemName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Item = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_QtyEdit = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Claer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_QtyEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(159, 184);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(255, 184);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 4;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radButton_Claer
            // 
            this.radButton_Claer.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Claer.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Claer.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Claer.Location = new System.Drawing.Point(12, 51);
            this.radButton_Claer.Name = "radButton_Claer";
            this.radButton_Claer.Size = new System.Drawing.Size(24, 24);
            this.radButton_Claer.TabIndex = 5;
            this.radButton_Claer.Text = "radButton1";
            this.radButton_Claer.Click += new System.EventHandler(this.RadButton_Claer_Click);
            // 
            // radTextBox_Unit
            // 
            this.radTextBox_Unit.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Unit.Location = new System.Drawing.Point(209, 100);
            this.radTextBox_Unit.Name = "radTextBox_Unit";
            this.radTextBox_Unit.Size = new System.Drawing.Size(124, 22);
            this.radTextBox_Unit.TabIndex = 2;
            this.radTextBox_Unit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Unit_KeyDown);
            // 
            // radTextBox_Qty
            // 
            this.radTextBox_Qty.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Qty.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Qty.Location = new System.Drawing.Point(44, 100);
            this.radTextBox_Qty.Name = "radTextBox_Qty";
            this.radTextBox_Qty.Size = new System.Drawing.Size(76, 22);
            this.radTextBox_Qty.TabIndex = 1;
            this.radTextBox_Qty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Qty_KeyDown);
            this.radTextBox_Qty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Qty_KeyPress);
            this.radTextBox_Qty.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Qty_KeyUp);
            // 
            // radTextBox_ItemName
            // 
            this.radTextBox_ItemName.AutoSize = false;
            this.radTextBox_ItemName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_ItemName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ItemName.Location = new System.Drawing.Point(229, 49);
            this.radTextBox_ItemName.Name = "radTextBox_ItemName";
            this.radTextBox_ItemName.Size = new System.Drawing.Size(267, 23);
            this.radTextBox_ItemName.TabIndex = 54;
            this.radTextBox_ItemName.Text = "-";
            // 
            // radLabel_1
            // 
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(12, 24);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(414, 19);
            this.radLabel_1.TabIndex = 53;
            this.radLabel_1.Text = "ระบุบาร์โค้ด [Enter] หรือ ระบุชื่อสินค้า [PageDown] หรือ F3 ค้นหา";
            // 
            // radTextBox_Item
            // 
            this.radTextBox_Item.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Item.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Item.Location = new System.Drawing.Point(44, 50);
            this.radTextBox_Item.Name = "radTextBox_Item";
            this.radTextBox_Item.Size = new System.Drawing.Size(174, 22);
            this.radTextBox_Item.TabIndex = 0;
            this.radTextBox_Item.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Item_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(209, 78);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(76, 19);
            this.radLabel4.TabIndex = 56;
            this.radLabel4.Text = "หน่วย";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(44, 78);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(76, 19);
            this.radLabel3.TabIndex = 55;
            this.radLabel3.Text = "จำนวนสั่ง";
            // 
            // radTextBox_QtyEdit
            // 
            this.radTextBox_QtyEdit.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_QtyEdit.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_QtyEdit.Location = new System.Drawing.Point(126, 100);
            this.radTextBox_QtyEdit.Name = "radTextBox_QtyEdit";
            this.radTextBox_QtyEdit.Size = new System.Drawing.Size(76, 22);
            this.radTextBox_QtyEdit.TabIndex = 57;
            this.radTextBox_QtyEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_QtyEdit_KeyPress);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(126, 78);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(76, 19);
            this.radLabel1.TabIndex = 58;
            this.radLabel1.Text = "จำนวนรับ";
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Desc.Location = new System.Drawing.Point(44, 152);
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            this.radTextBox_Desc.Size = new System.Drawing.Size(289, 22);
            this.radTextBox_Desc.TabIndex = 59;
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.AutoSize = false;
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Remark.Location = new System.Drawing.Point(44, 128);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(158, 19);
            this.radLabel_Remark.TabIndex = 60;
            this.radLabel_Remark.Text = "ระบุหมายเหตุ";
            // 
            // Editdatarow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(508, 225);
            this.Controls.Add(this.radLabel_Remark);
            this.Controls.Add(this.radTextBox_Desc);
            this.Controls.Add(this.radTextBox_QtyEdit);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radButton_Claer);
            this.Controls.Add(this.radTextBox_Unit);
            this.Controls.Add(this.radTextBox_Qty);
            this.Controls.Add(this.radTextBox_ItemName);
            this.Controls.Add(this.radLabel_1);
            this.Controls.Add(this.radTextBox_Item);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Editdatarow";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.Load += new System.EventHandler(this.Edittdatarow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Claer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_QtyEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadButton radButton_Claer;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Unit;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Qty;
        private Telerik.WinControls.UI.RadLabel radTextBox_ItemName;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Item;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_QtyEdit;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        public Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
    }
}
