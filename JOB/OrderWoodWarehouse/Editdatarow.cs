﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class Editdatarow : Telerik.WinControls.UI.RadForm
    {
        public DATAPRODUCT dATAPRODUCT;
        readonly int _pStatus;
        readonly string _pDeptUser;
        readonly string _pBillNo;
        readonly double _pBillCount;
        readonly DataRow _dataRow;
        Data_ITEMBARCODE data_ITEMBARCODE;  // ข้อมูลสินค้าที่ค้นหา
        DATAPRODUCT _dATAROWSITEM;          //ข้อมูลสินค้าที่ต้องการแก้ไข
        public Editdatarow(int pStatus, DataRow dataRow, string pDeptUser, string pBillNo, double pBillCount)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pStatus = pStatus;
            _pDeptUser = pDeptUser;
            _pBillNo = pBillNo;
            _dataRow = dataRow;
            _pBillCount = pBillCount;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        #region Event
        private void Edittdatarow_Load(object sender, EventArgs e)
        {
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            this._dATAROWSITEM = new DATAPRODUCT(_dataRow);
            radTextBox_Item.Text = _dATAROWSITEM.ITEMBARCODE;
            radTextBox_ItemName.Text = _dATAROWSITEM.ITEMNAME;
            radTextBox_Unit.Text = _dATAROWSITEM.UNIT;
            radTextBox_Qty.Text = _dATAROWSITEM.QTY.ToString();
            radTextBox_QtyEdit.Text = _dATAROWSITEM.QTYEDIT.ToString();

            if (radTextBox_Item.Text != "")
            {
                radTextBox_Unit.Enabled = false;
            }
            switch (_pStatus)
            {
                case 0:
                    this.Text = "แก้ไขข้อมูลสินค้า";
                    radLabel_Remark.Visible = false;
                    radTextBox_Desc.Visible = false;
                    break;
                case 1:
                    this.Text = "เพิ่มรายการสินค้า";
                    radTextBox_Item.Text = "";
                    radTextBox_ItemName.Text = "";
                    radTextBox_Unit.Text = "";
                    radTextBox_Qty.Text = "";
                    radTextBox_QtyEdit.Text = "";
                    radLabel_Remark.Visible = false;
                    radTextBox_Desc.Visible = false;
                    break;
                case 3:
                    this.Text = "เพิ่มสินค้ารอของ";
                    radTextBox_QtyEdit.Enabled = false;
                    radLabel_Remark.Visible = true;
                    radTextBox_Desc.Visible = true;
                    radTextBox_Desc.Focus();
                    break;
                case 4:
                    this.Text = "สั่งสินค้าจากนอก";
                    radTextBox_QtyEdit.Enabled = false;
                    radLabel_Remark.Visible = true;
                    radTextBox_Desc.Visible = true;
                    radTextBox_Desc.Focus();
                    break;
                default:
                    radTextBox_Item.Text = "";
                    radTextBox_ItemName.Text = "";
                    radTextBox_Unit.Text = "";
                    radTextBox_Qty.Text = "";
                    radTextBox_QtyEdit.Text = "";
                    radLabel_Remark.Visible = false;
                    radTextBox_Desc.Visible = false;
                    break;
            }

            switch (_pDeptUser)
            {
                case "D120":
                    radLabel1.Visible = false;
                    radTextBox_QtyEdit.Visible = false;
                    break;
                case "MN998":
                    if (_pBillCount <= 0) radTextBox_QtyEdit.Enabled = false; else radTextBox_QtyEdit.Focus();
                    break;
                default:
                    radTextBox_QtyEdit.Enabled = false;
                    break;
            }
        }
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }

                    this.data_ITEMBARCODE = new Data_ITEMBARCODE(radTextBox_Item.Text);
                    if (string.IsNullOrEmpty(data_ITEMBARCODE.Itembarcode_ITEMBARCODE))
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }

                    if (data_ITEMBARCODE.Itembarcode_UNITID == "") radTextBox_Unit.Text = "ชิ้น"; else radTextBox_Unit.Text = this.data_ITEMBARCODE.Itembarcode_UNITID;

                    radTextBox_ItemName.Text = this.data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME;
                    radTextBox_Item.Enabled = false;
                    radTextBox_Qty.Focus();
                    radTextBox_Unit.Enabled = false;
                    break;
                case Keys.PageDown://ระบุชื่อสินค้า [PageDown] 
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ชื่อสินค้า");
                        radTextBox_Item.Focus();
                        return;
                    }
                    this.data_ITEMBARCODE = new Data_ITEMBARCODE(radTextBox_Item.Text);
                    radTextBox_ItemName.Text = radTextBox_Item.Text;
                    this.data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME = radTextBox_Item.Text;
                    this.data_ITEMBARCODE.Itembarcode_UNITID = radTextBox_Unit.Text;
                    radTextBox_Item.Text = "";
                    radTextBox_Item.Enabled = false;
                    radTextBox_Unit.Text = "ชิ้น";
                    radTextBox_Unit.Enabled = true;
                    radTextBox_Qty.Enabled = true;
                    radTextBox_Qty.Focus();
                    break;

                case Keys.F3://ค้าหาสินค้า [F3] 
                    ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(radTextBox_Item.Text);
                    if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                    {
                        this.data_ITEMBARCODE = ShowDataDGV_Itembarcode.items;
                        radTextBox_Item.Text = data_ITEMBARCODE.Itembarcode_ITEMBARCODE; //radTextBox_Cst.Focus();
                        radTextBox_Unit.Text = data_ITEMBARCODE.Itembarcode_UNITID;
                        radTextBox_ItemName.Text = data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME;
                        radTextBox_Item.Enabled = false;
                        radTextBox_Unit.Enabled = false;
                        radTextBox_Qty.Focus();
                    }
                    else
                    {
                        radTextBox_Item.Text = ""; radTextBox_Item.Focus();
                    }
                    break;
                default:
                    break;
            }
        }
        private void RadTextBox_Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Unit.Enabled == false)
                {
                    radTextBox_QtyEdit.Text = radTextBox_Qty.Text;
                    radButton_Save.Focus();
                    return;
                }
                radTextBox_Unit.Focus();
            }
        }
        private void RadTextBox_QtyEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Save.Focus();
        }
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Unit.Text != "") radButton_Save.Focus();
            }
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Item.Text) && string.IsNullOrEmpty(radTextBox_ItemName.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("บาร์โค้ด หรือชื่อสินค้า");
                radTextBox_Item.Focus();
                return;
            }

            if (string.IsNullOrEmpty(radTextBox_Qty.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("จำนวน");
                radTextBox_Qty.Focus();
                return;
            }

            if (string.IsNullOrEmpty(radTextBox_Unit.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("หน่วย");
                radTextBox_Unit.Focus();
                return;
            }

            char[] ch = { '-', '_', '/' };
            for (int i = 0; i < ch.Length; i++)
            {
                if (radTextBox_Desc.Text == ch[i].ToString())
                {
                    MessageBox.Show("ไม่อนุญาติให้คีย์" + radTextBox_Desc.Text, Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    radTextBox_Desc.Focus();
                    return;
                }
            }

            this.dATAPRODUCT = new DATAPRODUCT(radTextBox_Item.Text, radTextBox_ItemName.Text, double.Parse(radTextBox_Qty.Text), double.Parse(radTextBox_QtyEdit.Text), radTextBox_Unit.Text);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            radTextBox_Item.Text = ""; radTextBox_Item.Enabled = true; radTextBox_Item.Focus();
            radTextBox_ItemName.Text = "";
            radTextBox_Qty.Text = ""; radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Text = ""; radTextBox_Unit.Enabled = false;
            radTextBox_QtyEdit.Text = "";
        }
        #endregion

        private void RadTextBox_Qty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
            if ((e.KeyChar == '.') && ((sender as RadTextBox).Text.IndexOf('.') > -1)) e.Handled = true;

            radTextBox_QtyEdit.Text = radTextBox_Qty.Text;
        }

        private void RadTextBox_QtyEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
            if ((e.KeyChar == '.') && ((sender as RadTextBox).Text.IndexOf('.') > -1)) e.Handled = true;
        }

        private void RadTextBox_Qty_KeyUp(object sender, KeyEventArgs e)
        {
            radTextBox_QtyEdit.Text = radTextBox_Qty.Text;
        }
    }
}
