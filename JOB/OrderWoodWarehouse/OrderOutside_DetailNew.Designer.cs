﻿namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    partial class OrderOutside_DetailNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderOutside_DetailNew));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Emp = new Telerik.WinControls.UI.RadLabel();
            this.radRadioButton_Cr = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Dr = new Telerik.WinControls.UI.RadRadioButton();
            this.radButton_Vend = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Vend = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_Wait = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton_Addbill = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Clos = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Head = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_DescriptionWait = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Description = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radButton_FindCost = new Telerik.WinControls.UI.RadButton();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Cost = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_FindDpt = new Telerik.WinControls.UI.RadButton();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Func = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Dpt = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Clear = new Telerik.WinControls.UI.RadButton();
            this.radButton_Update = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Amout = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Doc = new Telerik.WinControls.UI.RadGridView();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Vend = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Invoid = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_DocNo = new Telerik.WinControls.UI.RadTextBox();
            this.PrintDialog1 = new System.Windows.Forms.PrintDialog();
            this.PrintDocument1 = new System.Drawing.Printing.PrintDocument();
            this.PrintDocument2 = new System.Drawing.Printing.PrintDocument();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Cr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Dr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Vend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Vend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Wait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Addbill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Head)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DescriptionWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Description)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Cost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindDpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Func)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Amout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Doc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Doc.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Invoid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DocNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_DB.Controls.Add(this.radLabel9);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Emp);
            this.radGroupBox_DB.Controls.Add(this.radRadioButton_Cr);
            this.radGroupBox_DB.Controls.Add(this.radRadioButton_Dr);
            this.radGroupBox_DB.Controls.Add(this.radButton_Vend);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Vend);
            this.radGroupBox_DB.Controls.Add(this.radLabel4);
            this.radGroupBox_DB.Controls.Add(this.radCheckBox_Wait);
            this.radGroupBox_DB.Controls.Add(this.radButton_Addbill);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radButton_Clos);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Head);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_DescriptionWait);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Description);
            this.radGroupBox_DB.Controls.Add(this.radLabel7);
            this.radGroupBox_DB.Controls.Add(this.radGridView_Show);
            this.radGroupBox_DB.Controls.Add(this.radButton_FindCost);
            this.radGroupBox_DB.Controls.Add(this.radLabel6);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Cost);
            this.radGroupBox_DB.Controls.Add(this.radButton_FindDpt);
            this.radGroupBox_DB.Controls.Add(this.radLabel5);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Docno);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Func);
            this.radGroupBox_DB.Controls.Add(this.radDropDownList_Dpt);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(594, 570);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.ForeColor = System.Drawing.Color.Blue;
            this.radLabel9.Location = new System.Drawing.Point(462, 103);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(89, 21);
            this.radLabel9.TabIndex = 105;
            this.radLabel9.Text = "[ไม่บังคับ]";
            // 
            // radLabel_Emp
            // 
            this.radLabel_Emp.AutoSize = false;
            this.radLabel_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Emp.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Emp.Location = new System.Drawing.Point(311, 15);
            this.radLabel_Emp.Name = "radLabel_Emp";
            this.radLabel_Emp.Size = new System.Drawing.Size(269, 23);
            this.radLabel_Emp.TabIndex = 103;
            this.radLabel_Emp.Text = "ผู้เปิด -";
            // 
            // radRadioButton_Cr
            // 
            this.radRadioButton_Cr.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Cr.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_Cr.Location = new System.Drawing.Point(524, 51);
            this.radRadioButton_Cr.Name = "radRadioButton_Cr";
            this.radRadioButton_Cr.Size = new System.Drawing.Size(56, 19);
            this.radRadioButton_Cr.TabIndex = 102;
            this.radRadioButton_Cr.Text = "เครดิต";
            // 
            // radRadioButton_Dr
            // 
            this.radRadioButton_Dr.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Dr.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_Dr.Location = new System.Drawing.Point(462, 51);
            this.radRadioButton_Dr.Name = "radRadioButton_Dr";
            this.radRadioButton_Dr.Size = new System.Drawing.Size(56, 19);
            this.radRadioButton_Dr.TabIndex = 101;
            this.radRadioButton_Dr.Text = "เงินสด";
            // 
            // radButton_Vend
            // 
            this.radButton_Vend.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Vend.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Vend.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Vend.Image = ((System.Drawing.Image)(resources.GetObject("radButton_Vend.Image")));
            this.radButton_Vend.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Vend.Location = new System.Drawing.Point(549, 74);
            this.radButton_Vend.Name = "radButton_Vend";
            // 
            // 
            // 
            this.radButton_Vend.RootElement.AutoToolTip = true;
            this.radButton_Vend.Size = new System.Drawing.Size(31, 26);
            this.radButton_Vend.TabIndex = 100;
            this.radButton_Vend.Text = "radButton3";
            this.radButton_Vend.Click += new System.EventHandler(this.RadButton_Vend_Click);
            // 
            // radDropDownList_Vend
            // 
            this.radDropDownList_Vend.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Vend.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Vend.DropDownHeight = 124;
            this.radDropDownList_Vend.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Vend.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Vend.Location = new System.Drawing.Point(300, 74);
            this.radDropDownList_Vend.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Vend.Name = "radDropDownList_Vend";
            // 
            // 
            // 
            this.radDropDownList_Vend.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Vend.Size = new System.Drawing.Size(244, 25);
            this.radDropDownList_Vend.TabIndex = 98;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Vend.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Vend.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Vend.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(311, 51);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(67, 19);
            this.radLabel4.TabIndex = 99;
            this.radLabel4.Text = "ผู้จำหน่าย";
            // 
            // radCheckBox_Wait
            // 
            this.radCheckBox_Wait.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radCheckBox_Wait.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Wait.Location = new System.Drawing.Point(26, 433);
            this.radCheckBox_Wait.Name = "radCheckBox_Wait";
            this.radCheckBox_Wait.Size = new System.Drawing.Size(61, 19);
            this.radCheckBox_Wait.TabIndex = 0;
            this.radCheckBox_Wait.Text = "รอของ";
            this.radCheckBox_Wait.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Wait_ToggleStateChanged);
            // 
            // radButton_Addbill
            // 
            this.radButton_Addbill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Addbill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radButton_Addbill.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Addbill.Location = new System.Drawing.Point(508, 523);
            this.radButton_Addbill.Name = "radButton_Addbill";
            this.radButton_Addbill.Size = new System.Drawing.Size(72, 32);
            this.radButton_Addbill.TabIndex = 104;
            this.radButton_Addbill.Text = "เพิ่มบิล";
            this.radButton_Addbill.ThemeName = "Fluent";
            this.radButton_Addbill.Visible = false;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Addbill.GetChildAt(0))).Text = "เพิ่มบิล";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Addbill.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Addbill.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Addbill.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Addbill.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Addbill.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Addbill.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Addbill.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Cancel.Location = new System.Drawing.Point(307, 523);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(95, 32);
            this.radButton_Cancel.TabIndex = 97;
            this.radButton_Cancel.Text = "ยกเลิกทั้งบิล";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            // 
            // radButton_Clos
            // 
            this.radButton_Clos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Clos.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Clos.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Clos.Location = new System.Drawing.Point(407, 523);
            this.radButton_Clos.Name = "radButton_Clos";
            this.radButton_Clos.Size = new System.Drawing.Size(95, 32);
            this.radButton_Clos.TabIndex = 96;
            this.radButton_Clos.Text = "ปิด";
            this.radButton_Clos.ThemeName = "Fluent";
            this.radButton_Clos.Click += new System.EventHandler(this.RadButton_Clos_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Clos.GetChildAt(0))).Text = "ปิด";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Clos.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Clos.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDropDownList_Head
            // 
            this.radDropDownList_Head.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Head.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Head.DropDownHeight = 124;
            this.radDropDownList_Head.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Head.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Head.Location = new System.Drawing.Point(25, 72);
            this.radDropDownList_Head.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Head.Name = "radDropDownList_Head";
            // 
            // 
            // 
            this.radDropDownList_Head.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Head.Size = new System.Drawing.Size(233, 25);
            this.radDropDownList_Head.TabIndex = 0;
            this.radDropDownList_Head.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Head.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Head.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Head.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radTextBox_DescriptionWait
            // 
            this.radTextBox_DescriptionWait.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_DescriptionWait.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_DescriptionWait.Location = new System.Drawing.Point(25, 462);
            this.radTextBox_DescriptionWait.Multiline = true;
            this.radTextBox_DescriptionWait.Name = "radTextBox_DescriptionWait";
            // 
            // 
            // 
            this.radTextBox_DescriptionWait.RootElement.StretchVertically = true;
            this.radTextBox_DescriptionWait.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_DescriptionWait.Size = new System.Drawing.Size(556, 32);
            this.radTextBox_DescriptionWait.TabIndex = 6;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.ForeColor = System.Drawing.Color.Blue;
            this.radLabel3.Location = new System.Drawing.Point(234, 15);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(93, 23);
            this.radLabel3.TabIndex = 89;
            this.radLabel3.Text = "[แก้ไข]";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(25, 355);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(171, 19);
            this.radLabel2.TabIndex = 41;
            this.radLabel2.Text = "รายละเอีดยดการเบิก";
            // 
            // radTextBox_Description
            // 
            this.radTextBox_Description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Description.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Description.Location = new System.Drawing.Point(25, 375);
            this.radTextBox_Description.Multiline = true;
            this.radTextBox_Description.Name = "radTextBox_Description";
            // 
            // 
            // 
            this.radTextBox_Description.RootElement.StretchVertically = true;
            this.radTextBox_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Description.Size = new System.Drawing.Size(556, 45);
            this.radTextBox_Description.TabIndex = 5;
            this.radTextBox_Description.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(25, 51);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(31, 19);
            this.radLabel7.TabIndex = 88;
            this.radLabel7.Text = "ช่าง";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(25, 190);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(556, 159);
            this.radGridView_Show.TabIndex = 40;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // radButton_FindCost
            // 
            this.radButton_FindCost.BackColor = System.Drawing.Color.Transparent;
            this.radButton_FindCost.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_FindCost.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_FindCost.Image = ((System.Drawing.Image)(resources.GetObject("radButton_FindCost.Image")));
            this.radButton_FindCost.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_FindCost.Location = new System.Drawing.Point(549, 125);
            this.radButton_FindCost.Name = "radButton_FindCost";
            // 
            // 
            // 
            this.radButton_FindCost.RootElement.AutoToolTip = true;
            this.radButton_FindCost.Size = new System.Drawing.Size(31, 26);
            this.radButton_FindCost.TabIndex = 4;
            this.radButton_FindCost.Text = "radButton3";
            this.radButton_FindCost.Click += new System.EventHandler(this.RadButton_FindCost_Click);
            this.radButton_FindCost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(311, 104);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(46, 19);
            this.radLabel6.TabIndex = 85;
            this.radLabel6.Text = "งบเบิก";
            // 
            // radDropDownList_Cost
            // 
            this.radDropDownList_Cost.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Cost.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Cost.DropDownHeight = 124;
            this.radDropDownList_Cost.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Cost.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Cost.Location = new System.Drawing.Point(300, 125);
            this.radDropDownList_Cost.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Cost.Name = "radDropDownList_Cost";
            // 
            // 
            // 
            this.radDropDownList_Cost.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Cost.Size = new System.Drawing.Size(244, 25);
            this.radDropDownList_Cost.TabIndex = 3;
            this.radDropDownList_Cost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Cost.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Cost.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Cost.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radButton_FindDpt
            // 
            this.radButton_FindDpt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_FindDpt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_FindDpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_FindDpt.Image = ((System.Drawing.Image)(resources.GetObject("radButton_FindDpt.Image")));
            this.radButton_FindDpt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_FindDpt.Location = new System.Drawing.Point(263, 125);
            this.radButton_FindDpt.Name = "radButton_FindDpt";
            this.radButton_FindDpt.Size = new System.Drawing.Size(31, 26);
            this.radButton_FindDpt.TabIndex = 2;
            this.radButton_FindDpt.Text = "radButton3";
            this.radButton_FindDpt.Click += new System.EventHandler(this.RadButton_FindDpt_Click);
            this.radButton_FindDpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(25, 104);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(66, 19);
            this.radLabel5.TabIndex = 82;
            this.radLabel5.Text = "แผนกเบิก";
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(25, 20);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(203, 16);
            this.radLabel_Docno.TabIndex = 50;
            this.radLabel_Docno.Text = "MNOM200401000000";
            // 
            // radLabel_Func
            // 
            this.radLabel_Func.AutoSize = false;
            this.radLabel_Func.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Func.BorderVisible = true;
            this.radLabel_Func.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Func.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Func.Location = new System.Drawing.Point(219, 159);
            this.radLabel_Func.Name = "radLabel_Func";
            this.radLabel_Func.Size = new System.Drawing.Size(361, 19);
            this.radLabel_Func.TabIndex = 49;
            this.radLabel_Func.Text = "F1 แก้ไขข้อมูลสินค้า F4 เพิ่มรายการ F7 เปิดบิลใหม่กรณีรอของ";
            // 
            // radDropDownList_Dpt
            // 
            this.radDropDownList_Dpt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Dpt.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Dpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Dpt.Location = new System.Drawing.Point(25, 123);
            this.radDropDownList_Dpt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Dpt.Name = "radDropDownList_Dpt";
            // 
            // 
            // 
            this.radDropDownList_Dpt.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Dpt.Size = new System.Drawing.Size(233, 25);
            this.radDropDownList_Dpt.TabIndex = 1;
            this.radDropDownList_Dpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dpt.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dpt.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(207, 523);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(95, 32);
            this.radButton_Save.TabIndex = 8;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(25, 158);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(117, 19);
            this.radLabel1.TabIndex = 39;
            this.radLabel1.Text = "รายละเอียดสินค้า";
            // 
            // radButton_Clear
            // 
            this.radButton_Clear.Location = new System.Drawing.Point(292, 72);
            this.radButton_Clear.Name = "radButton_Clear";
            this.radButton_Clear.Size = new System.Drawing.Size(55, 24);
            this.radButton_Clear.TabIndex = 108;
            this.radButton_Clear.Text = "Cancel";
            this.radButton_Clear.Click += new System.EventHandler(this.RadButton_Cancle_Click);
            // 
            // radButton_Update
            // 
            this.radButton_Update.Location = new System.Drawing.Point(231, 72);
            this.radButton_Update.Name = "radButton_Update";
            this.radButton_Update.Size = new System.Drawing.Size(55, 24);
            this.radButton_Update.TabIndex = 107;
            this.radButton_Update.Text = "Update";
            this.radButton_Update.Click += new System.EventHandler(this.RadButton_Update_Click);
            // 
            // radLabel_Amout
            // 
            this.radLabel_Amout.AutoSize = false;
            this.radLabel_Amout.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Amout.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Amout.Location = new System.Drawing.Point(144, 160);
            this.radLabel_Amout.Name = "radLabel_Amout";
            this.radLabel_Amout.Size = new System.Drawing.Size(155, 16);
            this.radLabel_Amout.TabIndex = 56;
            this.radLabel_Amout.Text = "-";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(16, 132);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(117, 19);
            this.radLabel8.TabIndex = 55;
            this.radLabel8.Text = "เลขที่บิลผู้จำหน่าย";
            // 
            // radGridView_Doc
            // 
            this.radGridView_Doc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Doc.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Doc.Location = new System.Drawing.Point(5, 186);
            // 
            // 
            // 
            this.radGridView_Doc.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Doc.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Doc.Name = "radGridView_Doc";
            this.radGridView_Doc.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Doc.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Doc.Size = new System.Drawing.Size(424, 159);
            this.radGridView_Doc.TabIndex = 106;
            this.radGridView_Doc.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(16, 157);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(68, 19);
            this.radLabel10.TabIndex = 54;
            this.radLabel10.Text = "จำนวนเงิน";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(16, 107);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(67, 19);
            this.radLabel11.TabIndex = 53;
            this.radLabel11.Text = "ผู้จำหน่าย";
            // 
            // radLabel_Vend
            // 
            this.radLabel_Vend.AutoSize = false;
            this.radLabel_Vend.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Vend.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Vend.Location = new System.Drawing.Point(144, 101);
            this.radLabel_Vend.Name = "radLabel_Vend";
            this.radLabel_Vend.Size = new System.Drawing.Size(228, 25);
            this.radLabel_Vend.TabIndex = 52;
            this.radLabel_Vend.Text = "-";
            // 
            // radLabel_Invoid
            // 
            this.radLabel_Invoid.AutoSize = false;
            this.radLabel_Invoid.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Invoid.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Invoid.Location = new System.Drawing.Point(144, 135);
            this.radLabel_Invoid.Name = "radLabel_Invoid";
            this.radLabel_Invoid.Size = new System.Drawing.Size(181, 16);
            this.radLabel_Invoid.TabIndex = 51;
            this.radLabel_Invoid.Text = "-";
            // 
            // radLabel_1
            // 
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(16, 47);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(159, 19);
            this.radLabel_1.TabIndex = 50;
            this.radLabel_1.Text = "ระบุเลขที่เอกสาร [Enter]";
            // 
            // radTextBox_DocNo
            // 
            this.radTextBox_DocNo.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_DocNo.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_DocNo.Location = new System.Drawing.Point(16, 74);
            this.radTextBox_DocNo.MaxLength = 13;
            this.radTextBox_DocNo.Name = "radTextBox_DocNo";
            this.radTextBox_DocNo.Size = new System.Drawing.Size(174, 22);
            this.radTextBox_DocNo.TabIndex = 49;
            this.radTextBox_DocNo.Text = "PXE19-0001254";
            this.radTextBox_DocNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Item_KeyDown);
            // 
            // PrintDialog1
            // 
            this.PrintDialog1.UseEXDialog = true;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox1.Controls.Add(this.radGridView_Doc);
            this.radGroupBox1.Controls.Add(this.radLabel_1);
            this.radGroupBox1.Controls.Add(this.radButton_Clear);
            this.radGroupBox1.Controls.Add(this.radTextBox_DocNo);
            this.radGroupBox1.Controls.Add(this.radButton_Update);
            this.radGroupBox1.Controls.Add(this.radLabel_Invoid);
            this.radGroupBox1.Controls.Add(this.radLabel_Amout);
            this.radGroupBox1.Controls.Add(this.radLabel_Vend);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderText = "รายละเอียดบิล";
            this.radGroupBox1.Location = new System.Drawing.Point(600, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(434, 570);
            this.radGroupBox1.TabIndex = 109;
            this.radGroupBox1.Text = "รายละเอียดบิล";
            // 
            // OrderOutside_DetailNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 570);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "OrderOutside_DetailNew";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "สั่งของจากผู้จำหน่ายนอก.";
            this.Load += new System.EventHandler(this.OrderOutside_DetailNew_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Cr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Dr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Vend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Vend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Wait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Addbill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Head)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DescriptionWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Description)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Cost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_FindDpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Func)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Amout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Doc.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Doc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Invoid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DocNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Dpt;
        private Telerik.WinControls.UI.RadLabel radLabel_Func;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadButton radButton_FindCost;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadButton radButton_FindDpt;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Description;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Head;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Cost;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.PrintDialog PrintDialog1;
        private System.Drawing.Printing.PrintDocument PrintDocument1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_DescriptionWait;
        protected Telerik.WinControls.UI.RadButton radButton_Clos;
        private Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Wait;
        private System.Drawing.Printing.PrintDocument PrintDocument2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Cr;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Dr;
        private Telerik.WinControls.UI.RadButton radButton_Vend;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Vend;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_Emp;
        protected Telerik.WinControls.UI.RadButton radButton_Addbill;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_DocNo;
        private Telerik.WinControls.UI.RadLabel radLabel_Amout;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadGridView radGridView_Doc;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel_Vend;
        private Telerik.WinControls.UI.RadLabel radLabel_Invoid;
        private Telerik.WinControls.UI.RadButton radButton_Clear;
        private Telerik.WinControls.UI.RadButton radButton_Update;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
    }
}
