﻿namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    partial class OrderInside_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderInside_Main));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButton_Find = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Desc = new Telerik.WinControls.UI.RadDropDownList();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCheckBox_CheckDate = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Status = new Telerik.WinControls.UI.RadCheckBox();
            this.radDateTimePicker_OrderBegin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_OrderEnd = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Find)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CheckDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderEnd)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 539F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(864, 539);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(658, 533);
            this.radGridView_Show.TabIndex = 0;
            this.radGridView_Show.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_CellFormatting);
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_Show_CellBeginEdit);
            this.radGridView_Show.FilterChanging += new Telerik.WinControls.UI.GridViewCollectionChangingEventHandler(this.RadGridView_Show_FilterChanging);
            this.radGridView_Show.FilterExpressionChanged += new Telerik.WinControls.UI.GridViewFilterExpressionChangedEventHandler(this.RadGridView_Show_FilterExpressionChanged);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            this.radGridView_Show.DoubleClick += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radButton_Find);
            this.radPanel1.Controls.Add(this.radDropDownList_Desc);
            this.radPanel1.Controls.Add(this.radStatusStrip1);
            this.radPanel1.Controls.Add(this.radCheckBox_CheckDate);
            this.radPanel1.Controls.Add(this.radCheckBox_Status);
            this.radPanel1.Controls.Add(this.radDateTimePicker_OrderBegin);
            this.radPanel1.Controls.Add(this.radDateTimePicker_OrderEnd);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(667, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(194, 533);
            this.radPanel1.TabIndex = 9;
            // 
            // radButton_Find
            // 
            this.radButton_Find.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Find.Location = new System.Drawing.Point(10, 208);
            this.radButton_Find.Name = "radButton_Find";
            this.radButton_Find.Size = new System.Drawing.Size(175, 32);
            this.radButton_Find.TabIndex = 98;
            this.radButton_Find.Text = "ค้นหา";
            this.radButton_Find.ThemeName = "Fluent";
            this.radButton_Find.Click += new System.EventHandler(this.RadButton_Find_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Find.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Find.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDropDownList_Desc
            // 
            this.radDropDownList_Desc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Desc.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Desc.Location = new System.Drawing.Point(10, 161);
            this.radDropDownList_Desc.Name = "radDropDownList_Desc";
            this.radDropDownList_Desc.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Desc.TabIndex = 68;
            this.radDropDownList_Desc.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDownList_Desc_SelectedIndexChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.radButtonElement_Edit,
            this.commandBarSeparator2,
            this.radButtonElement_Excel,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 42);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "สร้าง";
            this.radButtonElement_Add.UseCompatibleTextRendering = false;
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.AutoSize = true;
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.UseCompatibleTextRendering = false;
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "";
            this.radButtonElement_Excel.ToolTipText = "Export";
            this.radButtonElement_Excel.UseCompatibleTextRendering = false;
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radCheckBox_CheckDate
            // 
            this.radCheckBox_CheckDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_CheckDate.ForeColor = System.Drawing.Color.Black;
            this.radCheckBox_CheckDate.Location = new System.Drawing.Point(10, 47);
            this.radCheckBox_CheckDate.Name = "radCheckBox_CheckDate";
            this.radCheckBox_CheckDate.Size = new System.Drawing.Size(47, 19);
            this.radCheckBox_CheckDate.TabIndex = 1;
            this.radCheckBox_CheckDate.Text = "วันที่";
            this.radCheckBox_CheckDate.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_CheckDate_ToggleStateChanged);
            // 
            // radCheckBox_Status
            // 
            this.radCheckBox_Status.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Status.ForeColor = System.Drawing.Color.Black;
            this.radCheckBox_Status.Location = new System.Drawing.Point(10, 136);
            this.radCheckBox_Status.Name = "radCheckBox_Status";
            this.radCheckBox_Status.Size = new System.Drawing.Size(61, 19);
            this.radCheckBox_Status.TabIndex = 97;
            this.radCheckBox_Status.Text = "สถานะ";
            this.radCheckBox_Status.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Status_ToggleStateChanged);
            // 
            // radDateTimePicker_OrderBegin
            // 
            this.radDateTimePicker_OrderBegin.CustomFormat = "";
            this.radDateTimePicker_OrderBegin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_OrderBegin.Location = new System.Drawing.Point(10, 73);
            this.radDateTimePicker_OrderBegin.Name = "radDateTimePicker_OrderBegin";
            this.radDateTimePicker_OrderBegin.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_OrderBegin.TabIndex = 94;
            this.radDateTimePicker_OrderBegin.TabStop = false;
            this.radDateTimePicker_OrderBegin.Text = "วันอังคารที่ 21 เมษายน 2020";
            this.radDateTimePicker_OrderBegin.Value = new System.DateTime(2020, 4, 21, 0, 0, 0, 0);
            // 
            // radDateTimePicker_OrderEnd
            // 
            this.radDateTimePicker_OrderEnd.CustomFormat = "";
            this.radDateTimePicker_OrderEnd.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_OrderEnd.Location = new System.Drawing.Point(10, 99);
            this.radDateTimePicker_OrderEnd.Name = "radDateTimePicker_OrderEnd";
            this.radDateTimePicker_OrderEnd.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_OrderEnd.TabIndex = 95;
            this.radDateTimePicker_OrderEnd.TabStop = false;
            this.radDateTimePicker_OrderEnd.Text = "วันอังคารที่ 21 เมษายน 2020";
            this.radDateTimePicker_OrderEnd.Value = new System.DateTime(2020, 4, 21, 0, 0, 0, 0);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(870, 570);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 548);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(864, 19);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>DoubleClick &gt;&gt; ดูบรรทัดรายการ | สีชมพู &gt;&gt; อนุมัติ | สีเขียว &gt" +
    ";&gt; สั่งจัด | สีเทา &gt;&gt; รอของ | สีเหลือง &gt;&gt; ยกเลิก</html>";
            // 
            // OrderInside_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 570);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrderInside_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "OrderInside_Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrderInside_Main_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Find)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CheckDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderEnd)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_OrderBegin;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_OrderEnd;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_CheckDate;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Status;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Desc;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected Telerik.WinControls.UI.RadButton radButton_Find;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}
