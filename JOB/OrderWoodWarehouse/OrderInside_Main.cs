﻿//CheckOK
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    public partial class OrderInside_Main : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData = new DataTable();
        public OrderInside_Main()
        {
            InitializeComponent();
            this.KeyPreview = true;

        }
        #region ROWSNUMBER

        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //ToolTipText
        private void RadGridView_Show_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == radGridView_Show.Columns["BRANCH_ID"].Index)
                {
                    if (e.Row is GridViewDataRowInfo)
                    {
                        e.CellElement.ToolTipText = string.Format(@"แผนกเบิก : {0},{1}"
                            , radGridView_Show.Rows[e.RowIndex].Cells["BRANCH_ID"].Value.ToString()
                            , radGridView_Show.Rows[e.RowIndex].Cells["DIMENSIONSNAME"].Value.ToString());
                    }
                }
                else if (e.ColumnIndex == radGridView_Show.Columns["COSTCENTERID"].Index)
                {
                    if (e.Row is GridViewDataRowInfo)
                    {
                        e.CellElement.ToolTipText = string.Format(@"งบเบิก : {0},{1}"
                            , radGridView_Show.Rows[e.RowIndex].Cells["COSTCENTERID"].Value.ToString()
                            , radGridView_Show.Rows[e.RowIndex].Cells["COSTCENTERNAME"].Value.ToString());
                    }
                }
            }
        }
        #endregion

        #region Enter Filler

        private void RadGridView_Show_FilterChanging(object sender, GridViewCollectionChangingEventArgs e)
        {
            //if (!EnterPress)
            //{
            //    e.Cancel = true;
            //}
            //EnterPress = false;
        }
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            //if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            ////try
            ////{
            //if (e.Row is GridViewFilteringRowInfo)
            //{
            //    //switch (e.ActiveEditor) {

            //    RadTextBoxEditor ed = e.ActiveEditor as RadTextBoxEditor;
            //    RadTextBoxEditorElement el = ed.EditorElement as RadTextBoxEditorElement;
            //    //}
            //    el.KeyDown -= Kel_KeyDown;
            //    el.KeyDown += Kel_KeyDown;
            //}
            ////}
            ////catch (Exception ex)
            ////{
            ////    return;
            ////}
        }
        private void RadGridView_Show_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {
            //string pCon = string.Empty;
            //foreach (var filterD in radGridView_Show.FilterDescriptors)
            //{
            //    if (filterD.Operator == Telerik.WinControls.Data.FilterOperator.Contains)
            //        pCon += $"{filterD.Expression.Replace(" ", "%").Replace("%LIKE%", " LIKE ")} ";
            //    else
            //        pCon += $"{filterD.Expression}";
            //}

            //string pTop = "";
            //if (pCon == "") { pTop = " TOP 100 "; pCon = ""; }

            //Cursor.Current = Cursors.WaitCursor;
            //dtData = GetdataAll_ByFilter(pTop, pCon);
            //radGridView_Show.DataSource = dtData;
            //Cursor.Current = Cursors.Default;
        }
        #endregion

        #region Events 
        //protected override void OnKeyDown(KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.Escape)
        //    {
        //        this.Close();
        //    }
        //    else
        //    {
        //        base.OnKeyDown(e);
        //    }
        //}
        private void SetConditions()
        {
            ConditionalFormattingObject c1 = new ConditionalFormattingObject("Orange, applied to entire row", ConditionTypes.Equal, "3", "", true)
            {
                RowBackColor = Color.FromArgb(255, 219, 166),
                CellBackColor = Color.FromArgb(255, 219, 166),
                RowForeColor = Color.Black,
                CellForeColor = Color.Black
            };
            radGridView_Show.Columns["STADOC"].ConditionalFormattingObjectList.Add(c1);

            DatagridClass.SetCellBackClolorByExpression("DOCNO", " APPROVESTATUS = '1' ", ConfigClass.SetColor_PinkPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", " WAIT_STATUS = '1' ", ConfigClass.SetColor_GrayPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", " PRINTCOUNT2 > '0' ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", " STATUSSENDTOAX = '1' ", Color.FromArgb(255, 255, 255), radGridView_Show);

        }
        private void OrderInside_Main_Load(object sender, EventArgs e)
        {

            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่ม";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Add.ToolTipText = "แก้ไข";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Add.ToolTipText = "Export Excel";
            radButton_Find.ButtonElement.ShowBorder = true; radButtonElement_Add.ToolTipText = "ค้นหา";

            radCheckBox_CheckDate.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Status.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Desc);
            radDropDownList_Desc.DropDownListElement.ForeColor = ConfigClass.SetColor_Blue();

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderBegin, DateTime.Today, DateTime.Now.Date);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderEnd, DateTime.Today, DateTime.Now.Date);

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATECREATE", "วันที่", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATERECORD", "ทั้งหมด[D]", 60));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATERECORDEDIT", "ล่าสุด[D]", 60));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOBGROUPSUB_ID", "ช่าง"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ช่างผู้สั่ง", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "แผนกเบิก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONSNAME", "แผนกเบิก", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COSTCENTERID", "งบเบิก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("COSTCENTERNAME", "งบเบิก"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "รายละเอียดการเบิก", 400));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMECREATE", "ผู้บันทึก", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STADOC", "ใช้งาน"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("APPROVESTATUS", "อนุมัติเบิกแล้ว"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRINTCOUNT2", "สั่งจัดแล้ว"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STATUSSENDTOAX", "สั่งเบิกแล้ว"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WAIT_STATUS", "รอของ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RECEIVER_DESCRIPTION", "รายละเอียดการรับสินค้า", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMESENDTOAX", "บันทึกรับสินค้า", 150));

            radGridView_Show.Columns["DATECREATE"].FormatString = "{0:dd/MM/yyyy}";
            radGridView_Show.Columns["DOCNO"].IsPinned = true;

            radDateTimePicker_OrderBegin.Value = DateTime.Now.Date.AddDays(-7);
            radGridView_Show.MasterTemplate.Columns["STADOC"].IsVisible = false;

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            this.SetConditions();

            radStatusStrip1.SizingGrip = false;

            SetEnable();
            GetdataAll();
            GetGridViewSummary();
            GetDptDropDownList();
        }
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            OrderInside_New frmCam_New = new OrderInside_New();
            frmCam_New.ShowDialog();
            ArrayList arraybill = frmCam_New.dataBillAdd;
            if (arraybill.Count > 0)
            {
                GetdataAll();
            }
        }
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.CurrentRow.Index > -1)
            {
                OrderInside_Detail frmCam_Detail = new OrderInside_Detail(radGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString());
                if (frmCam_Detail.ShowDialog() == DialogResult.Yes)
                {
                    GetdataAll();
                }
                else
                {
                    FindData_ByCondition();
                    return;
                }
            }
        }
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0)
            {
                return;
            }
            else
            {
                string returnExportStatus = DatagridClass.ExportExcelGridView("สั่งของจากโรงไม้เชิงทะเล", radGridView_Show, "1");
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExportStatus);
            }
        }
        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            FindData_ByCondition();
        }
        private void RadCheckBox_CheckDate_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_CheckDate.Checked == true)
            {
                radDateTimePicker_OrderBegin.Enabled = true;
                radDateTimePicker_OrderEnd.Enabled = true;
            }
            else
            {
                radDateTimePicker_OrderBegin.Enabled = false;
                radDateTimePicker_OrderEnd.Enabled = false;
            }
        }
        private void RadCheckBox_Status_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Status.Checked == true)
            {
                radDropDownList_Desc.Enabled = true;
                radDropDownList_Desc.SelectedIndex = 0;
            }
            else
            {
                radDropDownList_Desc.Enabled = false;
                this.Clearfilter();
            }
        }
        #endregion

        #region Methode
        private void GetDptDropDownList()
        {
            DataTable NewData = new DataTable();
            NewData.Columns.Add("ID");
            NewData.Columns.Add("SHOW");

            NewData.Rows.Add("01", "ทั้งหมด");
            NewData.Rows.Add("02", "ใช้งาน");
            NewData.Rows.Add("03", "ยังไม่อนุมัติ");
            NewData.Rows.Add("04", "อนุมัติ");
            NewData.Rows.Add("05", "สั่งจัดแล้ว");
            NewData.Rows.Add("06", "รอของ");

            radDropDownList_Desc.DataSource = NewData;
            radDropDownList_Desc.ValueMember = "ID";
            radDropDownList_Desc.DisplayMember = "SHOW";
            radDropDownList_Desc.SelectedIndex = -1;

        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("JOBGROUPSUB_DESCRIPTION", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void SetEnable()
        {
            radDateTimePicker_OrderBegin.Enabled = false;
            radDateTimePicker_OrderEnd.Enabled = false;
            radDropDownList_Desc.Enabled = false;
        }
        private void GetdataAll()
        {
            this.Cursor = Cursors.WaitCursor;
            dtData = BillOutClass.OrderWood_QueryDataOrder("0", "", "");// SQLORDERQUERY.QueryData_ByActive("0");
            radGridView_Show.DataSource = dtData;
            this.Cursor = Cursors.Default;
            if (dtData.Rows.Count == 0) MsgBoxClass.MsgBoxShow_FindRecordNoData("ในเงื่อนไขที่กำหนด");
        }
        private void GridViewfilter(GridViewDataColumn ColName, FilterOperator Operator, string Value)
        {
            FilterDescriptor filter = new FilterDescriptor(ColName.Name, Operator, Value);
            ColName.FilterDescriptor = filter;
        }
        private void Clearfilter()
        {
            radGridView_Show.FilterDescriptors.Clear();
        }
        private void FindData_ByCondition()
        {
            if (radCheckBox_CheckDate.Checked == true)
            {
                this.Cursor = Cursors.WaitCursor;
                dtData = BillOutClass.OrderWood_QueryDataOrder("0", radDateTimePicker_OrderBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_OrderEnd.Value.ToString("yyyy-MM-dd"));
                radGridView_Show.DataSource = dtData;
                this.Cursor = Cursors.Default;
                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("ในเงื่อนไขที่กำหนด");
                    return;
                }
            }
            else
            {
                GetdataAll();
            }

            if (radCheckBox_Status.Checked == true)
            {
                this.Clearfilter();
                switch (radDropDownList_Desc.SelectedValue.ToString())
                {
                    case "01":
                        this.Clearfilter();
                        break;
                    case "02":
                        GridViewfilter(radGridView_Show.Columns["STADOC"], FilterOperator.Contains, "1");
                        break;
                    case "03":
                        GridViewfilter(radGridView_Show.Columns["APPROVESTATUS"], FilterOperator.Contains, "0");
                        break;
                    case "04":
                        GridViewfilter(radGridView_Show.Columns["APPROVESTATUS"], FilterOperator.Contains, "1");
                        break;
                    case "05":
                        GridViewfilter(radGridView_Show.Columns["PRINTCOUNT2"], FilterOperator.IsGreaterThanOrEqualTo, "1");
                        break;
                    case "06":
                        GridViewfilter(radGridView_Show.Columns["WAIT_STATUS"], FilterOperator.Contains, "1");
                        break;
                    default:
                        this.Clearfilter();
                        break;
                }
            }
        }

        #endregion

        private void RadDropDownList_Desc_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (e.Position > -1)
            {
                if (radCheckBox_Status.Checked == true)
                {
                    this.Clearfilter();
                    switch (radDropDownList_Desc.SelectedValue.ToString())
                    {
                        case "01":
                            this.Clearfilter();
                            break;
                        case "02":
                            GridViewfilter(radGridView_Show.Columns["STADOC"], FilterOperator.Contains, "1");
                            break;
                        case "03":
                            GridViewfilter(radGridView_Show.Columns["APPROVESTATUS"], FilterOperator.Contains, "0");
                            break;
                        case "04":
                            GridViewfilter(radGridView_Show.Columns["APPROVESTATUS"], FilterOperator.Contains, "1");
                            break;
                        case "05":
                            GridViewfilter(radGridView_Show.Columns["PRINTCOUNT2"], FilterOperator.IsGreaterThanOrEqualTo, "1");
                            break;
                        case "06":
                            GridViewfilter(radGridView_Show.Columns["WAIT_STATUS"], FilterOperator.Contains, "1");
                            break;
                        default:
                            this.Clearfilter();
                            break;
                    }
                }
            }
        }
    }
}