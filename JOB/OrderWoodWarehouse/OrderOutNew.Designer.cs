﻿namespace PC_Shop24Hrs.JOB.OrderWoodWarehouse
{
    partial class OrderOutNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderOutNew));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radPanel_Additem = new Telerik.WinControls.UI.RadPanel();
            this.radButton_Add = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Account = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Qty = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Itemname = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Unit = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_OwnOrder = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel_Find = new Telerik.WinControls.UI.RadPanel();
            this.radCheckBox_group = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton_Find = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox_CheckDate = new Telerik.WinControls.UI.RadCheckBox();
            this.radDateTimePicker_OrderBegin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radStatusStrip_Zone = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Document = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radDateTimePicker_OrderEnd = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Additem)).BeginInit();
            this.radPanel_Additem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Account)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Itemname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_OwnOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Find)).BeginInit();
            this.radPanel_Find.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_group)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Find)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CheckDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel_Find, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(870, 570);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radGridView_Show);
            this.radPanel1.Controls.Add(this.radPanel_Additem);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(664, 539);
            this.radPanel1.TabIndex = 2;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Location = new System.Drawing.Point(0, 188);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.Size = new System.Drawing.Size(664, 351);
            this.radGridView_Show.TabIndex = 68;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_Show_CellBeginEdit);
            this.radGridView_Show.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellEndEdit);
            this.radGridView_Show.CellValidating += new Telerik.WinControls.UI.CellValidatingEventHandler(this.RadGridView_Show_CellValidating);
            this.radGridView_Show.CellValidated += new Telerik.WinControls.UI.CellValidatedEventHandler(this.RadGridView_Show_CellValidated);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radPanel_Additem
            // 
            this.radPanel_Additem.Controls.Add(this.radButton_Add);
            this.radPanel_Additem.Controls.Add(this.radButton_Cancel);
            this.radPanel_Additem.Controls.Add(this.radTextBox_Account);
            this.radPanel_Additem.Controls.Add(this.radLabel4);
            this.radPanel_Additem.Controls.Add(this.radLabel_Date);
            this.radPanel_Additem.Controls.Add(this.radTextBox_Qty);
            this.radPanel_Additem.Controls.Add(this.radTextBox_Itemname);
            this.radPanel_Additem.Controls.Add(this.radTextBox_Remark);
            this.radPanel_Additem.Controls.Add(this.radLabel3);
            this.radPanel_Additem.Controls.Add(this.radLabel1);
            this.radPanel_Additem.Controls.Add(this.radTextBox_Unit);
            this.radPanel_Additem.Controls.Add(this.radLabel5);
            this.radPanel_Additem.Controls.Add(this.radTextBox_OwnOrder);
            this.radPanel_Additem.Controls.Add(this.radLabel2);
            this.radPanel_Additem.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel_Additem.Location = new System.Drawing.Point(0, 0);
            this.radPanel_Additem.Name = "radPanel_Additem";
            this.radPanel_Additem.Size = new System.Drawing.Size(664, 188);
            this.radPanel_Additem.TabIndex = 2;
            // 
            // radButton_Add
            // 
            this.radButton_Add.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Add.Location = new System.Drawing.Point(427, 124);
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.Size = new System.Drawing.Size(114, 32);
            this.radButton_Add.TabIndex = 6;
            this.radButton_Add.Text = "บันทึก";
            this.radButton_Add.ThemeName = "Fluent";
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Cancel.Location = new System.Drawing.Point(547, 124);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(114, 32);
            this.radButton_Cancel.TabIndex = 7;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancle_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_Account
            // 
            this.radTextBox_Account.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Account.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Account.Location = new System.Drawing.Point(390, 52);
            this.radTextBox_Account.Name = "radTextBox_Account";
            this.radTextBox_Account.Size = new System.Drawing.Size(271, 21);
            this.radTextBox_Account.TabIndex = 4;
            this.radTextBox_Account.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(310, 52);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(35, 19);
            this.radLabel4.TabIndex = 81;
            this.radLabel4.Text = "ร้าน:";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(16, 23);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(54, 19);
            this.radLabel_Date.TabIndex = 71;
            this.radLabel_Date.Text = "สินค้า:*";
            // 
            // radTextBox_Qty
            // 
            this.radTextBox_Qty.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Qty.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Qty.Location = new System.Drawing.Point(83, 52);
            this.radTextBox_Qty.Name = "radTextBox_Qty";
            this.radTextBox_Qty.Size = new System.Drawing.Size(98, 21);
            this.radTextBox_Qty.TabIndex = 1;
            this.radTextBox_Qty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_Qty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Qty_KeyPress);
            // 
            // radTextBox_Itemname
            // 
            this.radTextBox_Itemname.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Itemname.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Itemname.Location = new System.Drawing.Point(83, 21);
            this.radTextBox_Itemname.Name = "radTextBox_Itemname";
            this.radTextBox_Itemname.Size = new System.Drawing.Size(211, 21);
            this.radTextBox_Itemname.TabIndex = 0;
            this.radTextBox_Itemname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(390, 82);
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            this.radTextBox_Remark.Size = new System.Drawing.Size(271, 21);
            this.radTextBox_Remark.TabIndex = 5;
            this.radTextBox_Remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(310, 84);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(69, 19);
            this.radLabel3.TabIndex = 77;
            this.radLabel3.Text = "หมายเหตุ:";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(16, 84);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(55, 19);
            this.radLabel1.TabIndex = 72;
            this.radLabel1.Text = "หน่วย:*";
            // 
            // radTextBox_Unit
            // 
            this.radTextBox_Unit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Unit.Location = new System.Drawing.Point(83, 82);
            this.radTextBox_Unit.Name = "radTextBox_Unit";
            this.radTextBox_Unit.Size = new System.Drawing.Size(98, 21);
            this.radTextBox_Unit.TabIndex = 2;
            this.radTextBox_Unit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(16, 54);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(61, 19);
            this.radLabel5.TabIndex = 73;
            this.radLabel5.Text = "จำนวน:*";
            // 
            // radTextBox_OwnOrder
            // 
            this.radTextBox_OwnOrder.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_OwnOrder.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_OwnOrder.Location = new System.Drawing.Point(390, 21);
            this.radTextBox_OwnOrder.Name = "radTextBox_OwnOrder";
            this.radTextBox_OwnOrder.Size = new System.Drawing.Size(271, 21);
            this.radTextBox_OwnOrder.TabIndex = 3;
            this.radTextBox_OwnOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(310, 23);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 19);
            this.radLabel2.TabIndex = 74;
            this.radLabel2.Text = "คนสั่ง:*";
            // 
            // radPanel_Find
            // 
            this.radPanel_Find.Controls.Add(this.radCheckBox_group);
            this.radPanel_Find.Controls.Add(this.radButton_Find);
            this.radPanel_Find.Controls.Add(this.radCheckBox_CheckDate);
            this.radPanel_Find.Controls.Add(this.radDateTimePicker_OrderBegin);
            this.radPanel_Find.Controls.Add(this.radDateTimePicker_OrderEnd);
            this.radPanel_Find.Controls.Add(this.radStatusStrip_Zone);
            this.radPanel_Find.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel_Find.Location = new System.Drawing.Point(673, 3);
            this.radPanel_Find.Name = "radPanel_Find";
            this.radPanel_Find.Size = new System.Drawing.Size(194, 539);
            this.radPanel_Find.TabIndex = 10;
            // 
            // radCheckBox_group
            // 
            this.radCheckBox_group.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_group.Location = new System.Drawing.Point(76, 54);
            this.radCheckBox_group.Name = "radCheckBox_group";
            this.radCheckBox_group.Size = new System.Drawing.Size(110, 19);
            this.radCheckBox_group.TabIndex = 73;
            this.radCheckBox_group.Text = "GroupByDate";
            this.radCheckBox_group.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_group_ToggleStateChanged);
            // 
            // radButton_Find
            // 
            this.radButton_Find.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Find.Location = new System.Drawing.Point(14, 142);
            this.radButton_Find.Name = "radButton_Find";
            this.radButton_Find.Size = new System.Drawing.Size(175, 32);
            this.radButton_Find.TabIndex = 101;
            this.radButton_Find.Text = "ค้นหา";
            this.radButton_Find.ThemeName = "Fluent";
            this.radButton_Find.Click += new System.EventHandler(this.RadButton_Find_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Find.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Find.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Find.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radCheckBox_CheckDate
            // 
            this.radCheckBox_CheckDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_CheckDate.ForeColor = System.Drawing.Color.Black;
            this.radCheckBox_CheckDate.Location = new System.Drawing.Point(14, 54);
            this.radCheckBox_CheckDate.Name = "radCheckBox_CheckDate";
            this.radCheckBox_CheckDate.Size = new System.Drawing.Size(47, 19);
            this.radCheckBox_CheckDate.TabIndex = 97;
            this.radCheckBox_CheckDate.Text = "วันที่";
            this.radCheckBox_CheckDate.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_CheckDate_ToggleStateChanged);
            // 
            // radDateTimePicker_OrderBegin
            // 
            this.radDateTimePicker_OrderBegin.CustomFormat = "";
            this.radDateTimePicker_OrderBegin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_OrderBegin.Location = new System.Drawing.Point(14, 80);
            this.radDateTimePicker_OrderBegin.Name = "radDateTimePicker_OrderBegin";
            this.radDateTimePicker_OrderBegin.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_OrderBegin.TabIndex = 98;
            this.radDateTimePicker_OrderBegin.TabStop = false;
            this.radDateTimePicker_OrderBegin.Text = "วันอังคารที่ 21 เมษายน 2020";
            this.radDateTimePicker_OrderBegin.Value = new System.DateTime(2020, 4, 21, 0, 0, 0, 0);
            // 
            // radStatusStrip_Zone
            // 
            this.radStatusStrip_Zone.AutoSize = false;
            this.radStatusStrip_Zone.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip_Zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip_Zone.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Excel,
            this.commandBarSeparator1,
            this.radButtonElement_Document,
            this.commandBarSeparator2});
            this.radStatusStrip_Zone.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip_Zone.Name = "radStatusStrip_Zone";
            this.radStatusStrip_Zone.Size = new System.Drawing.Size(194, 42);
            this.radStatusStrip_Zone.TabIndex = 80;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.UseCompatibleTextRendering = false;
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip_Zone.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Document
            // 
            this.radButtonElement_Document.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Document.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_Document.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement_Document.Name = "radButtonElement_Document";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Document, false);
            this.radButtonElement_Document.Text = "radButtonElement1";
            this.radButtonElement_Document.Click += new System.EventHandler(this.RadButtonElement_Document_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Zone.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radDateTimePicker_OrderEnd
            // 
            this.radDateTimePicker_OrderEnd.CustomFormat = "";
            this.radDateTimePicker_OrderEnd.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_OrderEnd.Location = new System.Drawing.Point(14, 108);
            this.radDateTimePicker_OrderEnd.Name = "radDateTimePicker_OrderEnd";
            this.radDateTimePicker_OrderEnd.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_OrderEnd.TabIndex = 99;
            this.radDateTimePicker_OrderEnd.TabStop = false;
            this.radDateTimePicker_OrderEnd.Text = "วันอังคารที่ 21 เมษายน 2020";
            this.radDateTimePicker_OrderEnd.Value = new System.DateTime(2020, 4, 21, 0, 0, 0, 0);
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 548);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(664, 19);
            this.radLabel_Detail.TabIndex = 57;
            this.radLabel_Detail.Text = resources.GetString("radLabel_Detail.Text");
            // 
            // OrderOutNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 570);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrderOutNew";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "OrderOutNew";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrderOutside_Report_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Additem)).EndInit();
            this.radPanel_Additem.ResumeLayout(false);
            this.radPanel_Additem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Account)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Itemname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_OwnOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Find)).EndInit();
            this.radPanel_Find.ResumeLayout(false);
            this.radPanel_Find.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_group)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Find)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CheckDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_OrderEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel_Find;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Itemname;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Qty;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Unit;
        private Telerik.WinControls.UI.RadTextBox radTextBox_OwnOrder;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Zone;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Account;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadPanel radPanel_Additem;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_CheckDate;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_OrderBegin;
        protected Telerik.WinControls.UI.RadButton radButton_Find;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Document;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_group;
        protected Telerik.WinControls.UI.RadButton radButton_Add;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_OrderEnd;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}
