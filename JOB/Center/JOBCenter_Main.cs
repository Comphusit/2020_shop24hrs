﻿//CheckOK
using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.JOB.Center
{
    public partial class JOBCenter_Main : RadForm
    {
        readonly string _pGROUPID;
        readonly string _pGROUP_DESC;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปืดจ๊อบ //1 มีสิทธิ์ปิด
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        readonly string tbl;
        readonly string billFirst;

        private DateTime dateAutoRefresh;

        //Main Load
        public JOBCenter_Main(string pGROUPID, string pGROUP_DESC, string pPermission,string pFixBch) // SHOP-SUPC
        {
            InitializeComponent();

            _pGROUPID = pGROUPID;
            _pGROUP_DESC = pGROUP_DESC;
            _pPerminssion = pPermission;
            _pFixBch = pFixBch;

            DataTable dtGroup = JOB_Class.GetJOB_Group(_pGROUPID);
            tbl = dtGroup.Rows[0]["TABLENAME"].ToString();
            billFirst = dtGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString();

        }
        private void JOBCenter_Main_Load(object sender, EventArgs e)
        {
            this.Text = _pGROUP_DESC + " : SHOP";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "ExportExcel";
            dateAutoRefresh = DateTime.Now.AddMinutes(3); timer_Auto.Start();

            radButtonElement_Excel.ShowBorder = true;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่ม";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข";
            radRadioButtonElement_Open.ShowBorder = true;
            radRadioButtonElement_ALL.ShowBorder = true;
            radStatusStrip_Main.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            // this.Text = "JOB SHOP";
            string pNameID = "สาขา";
            string pName = "ชื่อสาขา";

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATE", "S", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATEUPD", "Up", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_Number", "เลขที่ JOB", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("JOB_HEAD_STA", "หน.", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchID(pNameID));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchName(pName));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SHORTNAMEINS", "ผู้เปิด", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ประเภท", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SN", "หัวเรื่อง", 120));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_STACLOSE", "สถานะจ๊อบ"));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_HEAD_CLOSE", "สถานะจ๊อบ"));
            //Freeze Column
            for (int i = 0; i < 5; i++)
            {
                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;

            }

            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "JOB_STACLOSE = 1 ", false)
            //{ CellBackColor = ConfigClass.SetColor_GreenPastel() };
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj2);

            DatagridClass.SetCellBackClolorByExpression("JOB_HEAD_STA", " JOB_HEAD_STA = '/' ", Color.PeachPuff, radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_HEAD_STA = '/' ", Color.PeachPuff, radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_HEAD_STA = '/' ", Color.PeachPuff, radGridView_Show);
            //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition2", "JOB_HEAD_STA = '/' ", false)
            //{ CellBackColor = Color.PeachPuff };// ConfigClass.setColor_PinkPastel();
            //this.radGridView_Show.Columns["JOB_HEAD_STA"].ConditionalFormattingObjectList.Add(obj3);
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj3);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj3);

            //ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition4", "JOB_HEAD_CLOSE = 1 ", false)
            //{ CellBackColor = Color.Tomato };// ConfigClass.setColor_DarkPurplePastel();
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj4);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj4);
            //this.radGridView_Show.Columns["JOB_HEAD_STA"].ConditionalFormattingObjectList.Add(obj4);

            radGridView_Show.TableElement.SearchHighlightColor = Color.LightBlue;

            SetGDV();
        }
        //find Data
        void SetGDV()
        {
            string sta_JobAll = "   '0' ";
            if (radRadioButtonElement_Open.CheckState == CheckState.Unchecked) { sta_JobAll = "   '0','1' "; }

            string sql = $@" SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOBGROUPSUB_DESCRIPTION,
                            JOB_SN,JOB_DESCRIPTION,JOB_DESCRIPTION_UPD,
                            CASE ISNULL(JOB_BRANCHOUTPHUKET,'X') WHEN '0' THEN CONVERT(VARCHAR,'X') ELSE CONVERT(VARCHAR,'/') END  AS JOB_BRANCHOUTPHUKET,JOB_STACLOSE,
                            JOB_SHORTNAMEINS, JOB_HEAD, 
                            CASE ISNULL(JOB_HEAD, 'X') WHEN '0' THEN CONVERT(VARCHAR, 'X') ELSE CONVERT(VARCHAR, '/') END  AS JOB_HEAD_STA, 
                            DateDiff(Day, JOB_DATEINS, GETDATE()) AS SUMDATE   ,
                            CASE CONVERT(VARCHAR,JOB_DATEUPD,23) WHEN '1900-01-01' THEN  DateDiff(Day,JOB_DATEINS,GETDATE())
						    ELSE ISNULL(DATEDIFF(DAY,JOB_DATEUPD,GETDATE()),0) END AS SUMDATEUPD 
                        FROM    {tbl} WITH(NOLOCK)  
                        WHERE JOB_STACLOSE IN({sta_JobAll})  
                        AND JOB_TYPE = '{_pGROUPID}'";
            if (_pFixBch != "")
            {
                sql += " AND JOB_BRANCHID = '" + _pFixBch + "' ";
            }
            sql += "ORDER BY CONVERT(VARCHAR,JOB_DATEINS,23) DESC ";

            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            radGridView_Show.DataSource = dt;
        }

        //Open
        private void RadRadioButtonElement_Open_CheckStateChanged(object sender, EventArgs e)
        {
            SetGDV();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            OpenFormEdit();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            JOBCenter_SHOP_ADD frmJOB_ADD = new JOBCenter_SHOP_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC,_pFixBch);
            if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
            {
                SetGDV();
            }
        }
        //All
        private void RadRadioButtonElement_ALL_CheckStateChanged(object sender, EventArgs e)
        {
            SetGDV();
        }

        #region "ROWS"
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //ROWS NUMBERS
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        #endregion

        //Open Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            OpenFormEdit();
        }
        //Open From
        void OpenFormEdit()
        {
            if ((radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString() == "") ||
                (radGridView_Show.CurrentRow.Index == -1) ||
                (radGridView_Show.Rows.Count == 0))
            {
                return;
            }
            string jobID = radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString();
            JOBCenter_EDIT frmJOB_Edit = new JOBCenter_EDIT(tbl, _pGROUPID, _pGROUP_DESC, jobID, _pPerminssion);
            if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK)
            {
                SetGDV();
            }

        }
        //Auto Refresh
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now >= dateAutoRefresh)
            {
                timer_Auto.Stop();
                SetGDV();
                dateAutoRefresh = DateTime.Now.AddMinutes(3);
                timer_Auto.Start();
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("งาน " + _pGROUP_DESC, radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

        }
    }
}



