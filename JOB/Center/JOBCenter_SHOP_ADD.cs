﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Drawing;

namespace PC_Shop24Hrs.JOB.Center
{
    public partial class JOBCenter_SHOP_ADD : Telerik.WinControls.UI.RadForm
    {

        string bchId;

        readonly string _tblName;
        readonly string _billFirst;
        readonly string _grpId;
        readonly string _grpDesc;
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        public JOBCenter_SHOP_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc,string pFixBch)
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _pFixBch = pFixBch;
        }

        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }

        void Set_ClearData()
        {
            Set_GroupSub();
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            radTextBox_BranchID.Text = "";
            radTextBox_BranchID.Focus();
            radTextBox_SN.Text = "";
            radTextBox_Desc.Text = "";
            radLabel_BranchName.Text = "";
            //BCHID = "";
        }

        private void JOBCenter_SHOP_ADD_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);

            radDropDownList_GroupSUB.DropDownListElement.TextBox.Font = Controllers.SystemClass.SetFontGernaral;
            this.radDropDownList_GroupSUB.AutoSizeItems = true;
            this.radDropDownList_GroupSUB.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_GroupSUB.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = new Font("Tahoma", 12);
            this.radDropDownList_GroupSUB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_GroupSUB.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 50;
            radDropDownList_GroupSUB.DropDownListElement.DropDownWidth = 300;

            Set_ClearData();
            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SHOP";
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            if (_pFixBch != "")
            {
                radTextBox_BranchID.Text = SystemClass.SystemBranchID.Substring(2, 3);
                SetMN();
                radTextBox_BranchID.Enabled = false;
                radTextBox_SN.Focus();
            }


        }

        private void RadTextBox_BranchID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetMN();
            }
        }


        private void RadTextBox_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Desc.Focus();
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (bchId == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("สาขา");
                radTextBox_BranchID.Focus();
                return;
            }

            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("คำอธิบาย");
                radTextBox_Desc.Focus();
                return;
            }

            string descJOB ;
            descJOB = "- " + radTextBox_Desc.Text.Trim();
            descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = String.Format(@"
                            INSERT INTO " + _tblName + "([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],[JOB_GROUPSUB],[JOB_WHOINS],[JOB_NAMEINS]," +
                            "[JOB_SHORTNAMEINS],[JOB_DESCRIPTION],[JOB_SN],[JOB_DATEINS],[JOB_TYPE])  " +
                            "VALUES('" + billMaxNO + "', '" + bchId + "', '" + radLabel_BranchName.Text + "', '" + radDropDownList_GroupSUB.SelectedValue + "', " +
                            "'" + SystemClass.SystemUserID + "', '" + SystemClass.SystemUserName + "', '" + SystemClass.SystemUserNameShort + "', " +
                            "'" + descJOB + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']', '" + radTextBox_SN.Text.Trim() + "',GETDATE(),'" + _grpId + "') "
                            );
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);

            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void RadTextBox_BranchID_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_BranchID.Text.ToString().Length == 3)
            {
                if (radTextBox_BranchID.Text.ToString() != "000") { SetMN(); }
            }
            else
            {
                bchId = "";
                radLabel_BranchName.Text = "";
            }

        }

        void SetMN()
        {

            DataTable dtBch = BranchClass.GetDetailBranchByID("MN" + radTextBox_BranchID.Text.Trim());
            if (dtBch.Rows.Count == 0)
            {
                // MsgBoxClass.MsgBoxShow_FindRecordNoData("สาขา");
                bchId = "";
                radLabel_BranchName.Text = "";
                return;
            }
            bchId = dtBch.Rows[0]["BRANCH_ID"].ToString();
            radLabel_BranchName.Text = dtBch.Rows[0]["BRANCH_NAME"].ToString();
            radTextBox_SN.Focus();

        }

        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_SN.Focus();
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
    }
}
