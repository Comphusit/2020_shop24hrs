﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowImage;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.GeneralForm.Employee;
using PC_Shop24Hrs.GeneralForm.BillOut;

namespace PC_Shop24Hrs.JOB.Center
{
    public partial class JOBCenter_EDIT : Telerik.WinControls.UI.RadForm
    {
        // public string PJOBNUMBER;
        //public string PTYPE;

        //int GROUPSUB;
        string pBchID;
        string pChange;         //0 ไม่มีการเปลี่ยนเเปลงฟอร์ม 1 มีการเปลี่ยนแปลงเพื่อ reshesh ข้อมูลในตาราง main
        string pDate;
      //  string pConfigDB;
        readonly string _tblName;
        readonly string _grpId;
        readonly string _grpName;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปืดจ๊อบ //1 มีสิทธิ์ปิด
        readonly string _pJobNumber;


        #region "Rows"      
        private void RadGridView_Send_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion
        //Set Permission
        void SetPermission()
        {
            radCheckBox_STACLOSE.Enabled = false;
            radButton_Add.Enabled = false;
            Button_AX.Enabled = false;
            Button_MNIO.Enabled = false;
            Button_MNOT.Enabled = false;
        }

        public JOBCenter_EDIT(string pTbl, string pGrpID, string pGrpName, string pJobNumber, string pPermission)
        {
            InitializeComponent();
            _tblName = pTbl;
            _grpId = pGrpID;
            _grpName = pGrpName;
            _pPerminssion = pPermission;
            _pJobNumber = pJobNumber;
        }

        private void JOBCenter_EDIT_Load(object sender, EventArgs e)
        {

            //pConfigDB = ConfigClass.GetConfigDB_FORMNAME(this.Name);
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Bill.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล : " + _pJobNumber;

            this.Text = _grpName + " : SHOP";


            SetData();
            ////*****


            radButton_OpenAdd.ButtonElement.ShowBorder = true; radButton_OpenAdd.ButtonElement.ToolTipText = "แนบรูป";
            radButton_OpenView.ButtonElement.ShowBorder = true; radButton_OpenView.ButtonElement.ToolTipText = "ดูรูป";

            radButton_OpenCloseImage.ButtonElement.ShowBorder = true; radButton_OpenCloseImage.ButtonElement.ToolTipText = "ดูรูป จากสาขาถ่าย";
            RadButton_Empl.ButtonElement.ShowBorder = true; RadButton_Empl.ButtonElement.ToolTipText = "รายชื่อพนักงานที่อยู่สาขา";

            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Bill);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Desc);

            JOB_Class.SetHeadGrid_ForShowImage(radGridView_Show, "0");
            JOB_Class.SetHeadGrid_ForBillMNRZRB(radGridView_Send, "SHOP");

            SetData();

            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
            JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, "SHOP");

            radGridView_Show.ClearSelection();
            radGridView_Send.ClearSelection();

            this.Text = _grpName + " : SHOP";

        }
        //ประเภท
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }

        void SetData()
        {
            string sql = $@" SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOB_GROUPSUB,JOBGROUPSUB_DESCRIPTION,
                        JOB_SN,JOB_DESCRIPTION,JOB_STACLOSE,JOB_SHORTNAMEINS,JOB_DESCRIPTION_CLOSE,CONVERT(varchar,JOB_DATEINS,23) AS DATE_INS,
                        JOB_STAIMG_OPEN,JOB_STAIMG_CLOSE,JOB_HEAD      
                        FROM	{ _tblName} WITH (NOLOCK)   WHERE JOB_Number = '{ _pJobNumber}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            pBchID = dt.Rows[0]["BRANCH_ID"].ToString();
            radTextBox_BranchID.Text = dt.Rows[0]["BRANCH_ID"].ToString().Replace("MN", "").Replace("D", "");
            radLabel_BranchName.Text = dt.Rows[0]["BRANCH_NAME"].ToString();
            Set_GroupSub(); radDropDownList_GroupSUB.SelectedValue = dt.Rows[0]["JOB_GROUPSUB"].ToString();
            //            GROUPSUB = radDropDownList_GroupSUB.SelectedIndex;

            radTextBox_SN.Text = dt.Rows[0]["JOB_SN"].ToString();
            radTextBox_Desc.Text = dt.Rows[0]["JOB_DESCRIPTION"].ToString();
            pDate = dt.Rows[0]["DATE_INS"].ToString();

            radButton_OpenView.Enabled = false;
            if (dt.Rows[0]["JOB_STAIMG_OPEN"].ToString() == "1")
            {
                radButton_OpenView.Enabled = true;
            }

            radButton_OpenCloseImage.Enabled = false;
            if (dt.Rows[0]["JOB_STAIMG_CLOSE"].ToString() == "1")
            {
                radButton_OpenCloseImage.Enabled = true;
            }

            if (_grpId == "00004")
            {
                RadioButton_Bch.Enabled = false;
                RadioButton_Head.Enabled = false;
            }
            else
            {
                RadioButton_Bch.Enabled = true; RadioButton_Head.Enabled = true;
                if (dt.Rows[0]["JOB_HEAD"].ToString() == "1")
                {
                    RadioButton_Bch.Checked = false;
                    RadioButton_Head.Checked = true;
                }
                else
                {
                    RadioButton_Bch.Checked = true;
                    RadioButton_Head.Checked = false;
                }
            }

            radLabel_tel.Text = BranchClass.GetTel_ByBranchID(pBchID);

            if (dt.Rows[0]["JOB_STACLOSE"].ToString() == "1")
            {
                radCheckBox_STACLOSE.Checked = true;
                radTextBox_Update.Text = dt.Rows[0]["JOB_DESCRIPTION_CLOSE"].ToString();
                radTextBox_Update.ReadOnly = true;
                radButton_Save.Enabled = false;

                radDropDownList_GroupSUB.Enabled = false;
                radTextBox_SN.Enabled = false;
                radCheckBox_STACLOSE.Enabled = false;

                Button_MNIO.Enabled = false;

                Button_AX.Enabled = false;
                radButton_Add.Enabled = false;
                radButton_Cancel.Focus();
                radTextBox_Update.SelectionStart = radTextBox_Update.Text.Length;

                RadioButton_Bch.Enabled = false;
                RadioButton_Head.Enabled = false;
            }
            else
            {
                radCheckBox_STACLOSE.Checked = false;
                radTextBox_Update.Enabled = true;
                radButton_Save.Enabled = true;

                radTextBox_Update.Focus();
            }

            if (_pPerminssion != "1")
            {
                SetPermission();
            }

            pChange = "0";
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (pChange == "1")
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.No;
            }
            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string sta_Head = "0";
            if (RadioButton_Head.Checked == true)
            {
                sta_Head = "1";
            }

            if (radTextBox_Update.Text == "")
            {
                string strUpdGroupSUB;
                strUpdGroupSUB = $@"UPDATE {_tblName}
                    SET [JOB_GROUPSUB] = '{radDropDownList_GroupSUB.SelectedValue}',
                        [JOB_DATEUPD] = GETDATE(),
                        [JOB_SN] = '{radTextBox_SN.Text}',
                        [JOB_HEAD] = '{sta_Head}')
                    WHERE [JOB_Number] = '{_pJobNumber}' ";
                string TGroupSUB = ConnectionClass.ExecuteSQL_Main(strUpdGroupSUB);
                MsgBoxClass.MsgBoxShow_SaveStatus(TGroupSUB);
                if (TGroupSUB == "")
                {
                    //GROUPSUB = radDropDownList_GroupSUB.SelectedIndex;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
                else
                {
                    return;
                }
            }

            string strUser;
            strUser = "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

            string desc = radTextBox_Desc.Text;
            string descClose = "-" + radTextBox_Update.Text + Environment.NewLine + strUser;
            string strUpd;
            if (radCheckBox_STACLOSE.Checked == true)//ปิดจีอบ
            {
                strUpd = string.Format(@"UPDATE " + _tblName + " SET  [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'" +
                    ",[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "'" +
                    ",[JOB_DESCRIPTION_CLOSE] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'" +
                    ",[JOB_HEAD] = '" + sta_Head + "'" +
                    ",[JOB_STACLOSE] = '1'" +
                    ",[JOB_WHOCLOSE] = '" + SystemClass.SystemUserID + "' " +
                    ",[JOB_NAMECLOSE] = '" + SystemClass.SystemUserName + "'" +
                    ",[JOB_DATECLOSE] = GETDATE()" +
                    ",[JOB_DATEUPD] = GETDATE()  " +
                    " WHERE[JOB_Number] = '" + _pJobNumber + "' ");
            }
            else
            {
                desc = desc + Environment.NewLine + Environment.NewLine + descClose;
                strUpd = string.Format(@"UPDATE " + _tblName + "  SET	 [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'" +
                    ",[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "'" +
                    ",[JOB_HEAD] = '" + sta_Head + "'" +
                    ",[JOB_DESCRIPTION] = '" + desc + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'" +
                    ",[JOB_DESCRIPTION_UPD] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_DATEUPD] = GETDATE() " +
                    "WHERE[JOB_Number] = '" + _pJobNumber + "' ");
            }

            string T = ConnectionClass.ExecuteSQL_Main(strUpd);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        //Close
        private void RadCheckBox_STACLOSE_CheckedChanged(object sender, EventArgs e)
        {
           
            if (radCheckBox_STACLOSE.Checked == false)
            {
                return;
            }
            //radTextBox_Update.Focus();
            if (_grpId == "00004")//กรณีที่เป็น JOB ตรวจสาขาต้องมีสิดเพิ่ม
            {
                if ((SystemClass.SystemUserLevelMinimart == "0") && (SystemClass.SystemBranchID == "MN000"))
                {
                    radCheckBox_STACLOSE.Checked = false;
                    radTextBox_Update.Focus();
                    return;
                }
            }

            int cSpcType = 0;
            for (int iT = 0; iT < radGridView_Show.Rows.Count; iT++)
            {
                if (radGridView_Show.Rows[iT].Cells["Bill_SpcType"].Value.ToString() == "X")
                {
                    cSpcType += 1;
                }
            }

            if (cSpcType == 0)
            {
                radTextBox_Update.Focus();
                return;
            }
            //CheckBill
            int a = 0;
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["Bill_TypeBchStatus"].Value.ToString() == "X")
                {
                    a += 1;
                }
            }
            if (a == 0)
            {
                radTextBox_Update.Focus();
            }
            else
            {
                if (SystemClass.SystemComProgrammer == "1")
                {
                    if (ComfirmByProgrammer() == "1")
                    {
                        radTextBox_Update.Focus();
                    }
                    else
                    {
                        radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                        radTextBox_Update.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากสาขายังรับบิลไม่ครบทุกใบ" + Environment.NewLine + "จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                    radTextBox_Update.Focus();
                }
            }

        }
        //Comfirm
        string ComfirmByProgrammer()
        {
            if ((MessageBox.Show("ยันยันการปิดจ๊อบทั้งที่บิลยังไม่เรียบร้อย ?.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
            {
                return "0";
            }
            else
            {
                return "1";
            }
        }
        //Click
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Show.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                        //JOBClass.FindImageBillAll_ByJobID(BillAllClass.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD": //รูปสาขาส่งซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLD_Count"].Value.ToString(),
                           radGridView_Show.CurrentRow.Cells["Bill_OLD_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC": //รูปสาขาใหญ่รับซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                          radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    break;

            }

        }
        //MNIO
        private void Button_MNIO_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", "SHOP", _pJobNumber, _grpId, _grpName,"0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }

        //Add Image
        private void RadButton_OpenAdd_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMN;
            if (_grpId == "00004")
            {
                pPath = PathImageClass.pPathJOBHD;
            }

            pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            string pFileName = _pJobNumber + "-" + pBchID + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, _tblName, "JOB_STAIMG_OPEN", "JOB_Number", _pJobNumber, "1");
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.Yes)
            {
                radButton_OpenView.Enabled = true;
            }
            else if (frm.DialogResult == DialogResult.OK)
            {
                UploadImageSelectArea _UploadImageSelectArea = new UploadImageSelectArea("0", pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber);
                _UploadImageSelectArea.Show();
                radButton_OpenView.Enabled = true;
            }
        }
        //open Image
        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMN;
            if (_grpId == "00004")
            {
                pPath = PathImageClass.pPathJOBHD;
            }
            pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            { pathImg = pPath + @"|*.jpg" };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }
        //Click
        private void RadGridView_Send_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Send.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //MNOT
        private void Button_MNOT_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNOT", "SHOP", _pJobNumber, _grpId, _grpName,"0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //Import bill 
        private void Button_AX_Click(object sender, EventArgs e)
        {
            Bill.BillAXAddJOB frm = new Bill.BillAXAddJOB(pBchID, radLabel_BranchName.Text, "1",
                _pJobNumber, _grpId, _grpName, radTextBox_SN.Text, "", "", "");
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //Bch Send
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            DataTable dtJOB = BillOut_Class.FindBill_ForAddJOB("'D054'", pBchID);
            if (dtJOB.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("บิลทั้งหมด มี JOB เรียบร้อยแล้ว");
                return;
            }

            JOB.Bill.BillMN_ForAddJOB frmAddJOb = new Bill.BillMN_ForAddJOB("'D054'", pBchID, _pJobNumber, _grpId);
            if (frmAddJOb.ShowDialog(this) == DialogResult.Yes)
            {
                JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, "SHOP");
            }
        }

        private void RadButton_OpenCloseImage_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMN;
            if (_grpId == "00004")
            {
                pPath = PathImageClass.pPathJOBHDBCH;
            }
            pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            { pathImg = pPath + @"|*.jpg" };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void RadButton_Empl_Click(object sender, EventArgs e)
        {
            Employee_Report d054_Report_Employee = new Employee_Report("1", "SHOP", "MN" + radTextBox_BranchID.Text, DateTime.Now)
            {
                WindowState = FormWindowState.Maximized
            };
            d054_Report_Employee.ShowDialog();
        }
    }
}
