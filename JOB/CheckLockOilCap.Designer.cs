﻿namespace PC_Shop24Hrs.JOB
{
    partial class CheckLockOilCap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckLockOilCap));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DptCar = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_TagNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Car = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CarID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_sta = new Telerik.WinControls.UI.RadLabel();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptCar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_TagNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Car)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CarID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_sta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(67, 475);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(120, 32);
            this.radButton_Save.TabIndex = 2;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(191, 475);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(120, 32);
            this.radButton_Cancel.TabIndex = 3;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Input.Location = new System.Drawing.Point(24, 95);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(167, 19);
            this.radLabel_Input.TabIndex = 24;
            this.radLabel_Input.Text = "ระบุหมายเลข Tag [Enter]";
            // 
            // radLabel_DptCar
            // 
            this.radLabel_DptCar.AutoSize = false;
            this.radLabel_DptCar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_DptCar.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DptCar.Location = new System.Drawing.Point(24, 400);
            this.radLabel_DptCar.Name = "radLabel_DptCar";
            this.radLabel_DptCar.Size = new System.Drawing.Size(314, 56);
            this.radLabel_DptCar.TabIndex = 26;
            this.radLabel_DptCar.Text = "ข้อมูลรถ";
            // 
            // radTextBox_TagNumber
            // 
            this.radTextBox_TagNumber.AutoSize = false;
            this.radTextBox_TagNumber.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold);
            this.radTextBox_TagNumber.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_TagNumber.Location = new System.Drawing.Point(24, 120);
            this.radTextBox_TagNumber.MaxLength = 8;
            this.radTextBox_TagNumber.Name = "radTextBox_TagNumber";
            this.radTextBox_TagNumber.Size = new System.Drawing.Size(314, 119);
            this.radTextBox_TagNumber.TabIndex = 0;
            this.radTextBox_TagNumber.Tag = "";
            this.radTextBox_TagNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_TagNumber_KeyDown);
            this.radTextBox_TagNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_TagNumber_KeyPress);
            // 
            // radLabel_Car
            // 
            this.radLabel_Car.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Car.Location = new System.Drawing.Point(24, 246);
            this.radLabel_Car.Name = "radLabel_Car";
            this.radLabel_Car.Size = new System.Drawing.Size(146, 19);
            this.radLabel_Car.TabIndex = 27;
            this.radLabel_Car.Text = "ระบุทะเบียนรถ [Enter]";
            // 
            // radTextBox_CarID
            // 
            this.radTextBox_CarID.AutoSize = false;
            this.radTextBox_CarID.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_CarID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CarID.Location = new System.Drawing.Point(24, 269);
            this.radTextBox_CarID.Name = "radTextBox_CarID";
            this.radTextBox_CarID.Size = new System.Drawing.Size(314, 119);
            this.radTextBox_CarID.TabIndex = 1;
            this.radTextBox_CarID.Tag = "";
            this.radTextBox_CarID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CarID_KeyDown);
            // 
            // radLabel_sta
            // 
            this.radLabel_sta.AutoSize = false;
            this.radLabel_sta.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold);
            this.radLabel_sta.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_sta.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel_sta.Location = new System.Drawing.Point(24, 10);
            this.radLabel_sta.Name = "radLabel_sta";
            this.radLabel_sta.Size = new System.Drawing.Size(314, 70);
            this.radLabel_sta.TabIndex = 29;
            this.radLabel_sta.Text = "ปลดล็อก";
            this.radLabel_sta.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // CheckLockOilCap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(370, 525);
            this.Controls.Add(this.radLabel_sta);
            this.Controls.Add(this.radTextBox_CarID);
            this.Controls.Add(this.radLabel_Car);
            this.Controls.Add(this.radTextBox_TagNumber);
            this.Controls.Add(this.radLabel_DptCar);
            this.Controls.Add(this.radLabel_Input);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CheckLockOilCap";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุข้อมูล";
            this.Load += new System.EventHandler(this.InputData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptCar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_TagNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Car)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CarID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_sta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadLabel radLabel_DptCar;
        private Telerik.WinControls.UI.RadTextBox radTextBox_TagNumber;
        private Telerik.WinControls.UI.RadLabel radLabel_Car;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CarID;
        private Telerik.WinControls.UI.RadLabel radLabel_sta;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
    }
}
