﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowImage;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.GeneralForm.BillOut;

namespace PC_Shop24Hrs.JOB.General
{
    public partial class JOBGeneral_EDIT : Telerik.WinControls.UI.RadForm
    {
        // public string PJOBNUMBER;
        //public string STAOUT;
        //public string PTYPE;

        int GROUPSUB;
        string pBchID;
        string pDate;
        string pChange;         //0 ไม่มีการเปลี่ยนเเปลงฟอร์ม 1 มีการเปลี่ยนแปลงเพื่อ reshesh ข้อมูลในตาราง main
        //string pConfigDB;
        readonly string _tblName;
        readonly string _grpId;
        readonly string _grpName;
        readonly string _pPermission;//0 ไม่มีสิทธิ์ปิดจ๊อบ 1 มี
        readonly string _pJobNumber;
        readonly string _pType;


        #region "Rows"      
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //Rows
        private void RadGridView_Send_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //Set Permission
        void SetPermission()
        {
            radCheckBox_STAOUT.Enabled = false;
            radCheckBox_STACLOSE.Enabled = false;
            radButton_Add.Enabled = false;
            Button_AX.Enabled = false;
            if (SystemClass.SystemDptID == "D062") Button_MNIO.Enabled = true; else Button_MNIO.Enabled = false;
            Button_MNOT.Enabled = false;
            button_MNIO_stockID.Enabled = false;
        }
        //Load
        public JOBGeneral_EDIT(string pTbl, string pGrpID, string pGrpName, string pPermission, string pType, string pJobNumber)
        {
            InitializeComponent();
            _tblName = pTbl;
            _grpId = pGrpID;
            _grpName = pGrpName;
            _pPermission = pPermission;
            _pJobNumber = pJobNumber;
            _pType = pType;

            //pChange = "0";
        }
        //Head
        void Set_JOBSTA()
        {
            radDropDownList_STA.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_STA.DataSource = JOB_Class.GetJOBStatus();
            radDropDownList_STA.ValueMember = "STA_ID";
            radDropDownList_STA.DisplayMember = "STA_NAME";
        }
        //Load
        private void JOBGeneral_EDIT_Load(object sender, EventArgs e)
        {
            //pConfigDB = ConfigClass.GetConfigDB_FORMNAME(this.Name);

            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Bill.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล : " + _pJobNumber;


            RadButton_Car.ButtonElement.ShowBorder = true; RadButton_Car.ButtonElement.ToolTipText = "ระบุหมายเลขทะเบียนรถที่ใช้งาน";
            RadButton_OpenAdd.ButtonElement.ShowBorder = true; RadButton_OpenAdd.ButtonElement.ToolTipText = "แนบรูป สำหรับเปิดงานซ่อม";
            radButton_OpenView.ButtonElement.ShowBorder = true; radButton_OpenView.ButtonElement.ToolTipText = "ดูรูป สำหรับเปิดงานซ่อม";
            radButton_CloseAdd.ButtonElement.ShowBorder = true; radButton_CloseAdd.ButtonElement.ToolTipText = "แนบรูป สำหรับปิดงานซ่อม";
            radButton_CloseView.ButtonElement.ShowBorder = true; radButton_CloseView.ButtonElement.ToolTipText = "ดูรูป สำหรับปิดงานซ่อม";

            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Head);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Bill);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Desc);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_STA);

            JOB_Class.SetHeadGrid_ForShowImage(radGridView_Show, "0");
            JOB_Class.SetHeadGrid_ForBillMNRZRB(radGridView_Send, _pType);

            SetData();

            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
            JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);

            radGridView_Show.ClearSelection();
            radGridView_Send.ClearSelection();


            //FindBillAll();
            if (_pType == "SHOP")
            {
                radButton_Add.Text = "สาขาส่ง";
                radLabel_branch.Text = "สาขา";
            }
            else
            {
                radButton_Add.Text = "ส่งซ่อม";
                radLabel_branch.Text = "แผนก";
            }
            this.Text = _grpName + " : " + _pType;
        }
        //Group
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //Head
        void Set_Head()
        {
            radDropDownList_Head.DataSource = JOB_Class.GetJOB_GroupSub("00006");
            radDropDownList_Head.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_Head.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //Set Data
        void SetData()
        {
            string sql = $@" 
                        SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOB_GROUPSUB,JOBGROUPSUB_DESCRIPTION,
                                JOB_SN,JOB_DESCRIPTION,JOB_STACLOSE,JOB_STAOUT,JOB_SHORTNAMEINS,JOB_DESCRIPTION_CLOSE,JOB_HEADID,JOB_TELPHONE,JOB_CARID,    
                                JOB_STAIMG_OPEN,JOB_STAIMG_CLOSE,CONVERT(varchar,JOB_DATEINS,23) AS DATE_INS   ,ISNULL(JOB_STA,'01') AS JOB_STA   
                        FROM	{_tblName} WITH (NOLOCK)   
                        WHERE   JOB_Number = '{_pJobNumber}' ";
            DataTable Dt = Controllers.ConnectionClass.SelectSQL_Main(sql);

            pBchID = Dt.Rows[0]["BRANCH_ID"].ToString();
            radTextBox_BranchID.Text = Dt.Rows[0]["BRANCH_ID"].ToString().Replace("MN", "").Replace("D", "");
            radLabel_BranchName.Text = Dt.Rows[0]["BRANCH_NAME"].ToString();

            Set_GroupSub(); radDropDownList_GroupSUB.SelectedValue = Dt.Rows[0]["JOB_GROUPSUB"].ToString();
            Set_Head(); radDropDownList_Head.SelectedValue = Dt.Rows[0]["JOB_HEADID"].ToString();
            Set_JOBSTA(); radDropDownList_STA.SelectedValue = Dt.Rows[0]["JOB_STA"].ToString();

            GROUPSUB = radDropDownList_GroupSUB.SelectedIndex;
            radTextBox_SN.Text = Dt.Rows[0]["JOB_SN"].ToString();
            radTextBox_Desc.Text = Dt.Rows[0]["JOB_DESCRIPTION"].ToString();
            radTextBox_tel.Text = Dt.Rows[0]["JOB_TELPHONE"].ToString();
            radTextBox_Car.Text = Dt.Rows[0]["JOB_CARID"].ToString();

            pDate = Dt.Rows[0]["DATE_INS"].ToString();

            radButton_OpenView.Enabled = false;
            if (Dt.Rows[0]["JOB_STAIMG_OPEN"].ToString() == "1")
            {
                radButton_OpenView.Enabled = true;
            }

            radButton_CloseView.Enabled = false;
            if (Dt.Rows[0]["JOB_STAIMG_CLOSE"].ToString() == "1")
            {
                radButton_CloseView.Enabled = true;
            }

            radLabel_tel.Text = BranchClass.GetTel_ByBranchID(pBchID);

            if (Dt.Rows[0]["JOB_STAOUT"].ToString() == "1")
            { radCheckBox_STAOUT.Checked = true; }
            else { radCheckBox_STAOUT.Checked = false; }

            if (Dt.Rows[0]["JOB_STACLOSE"].ToString() == "1")
            {
                radCheckBox_STACLOSE.Checked = true;
                radTextBox_Update.Text = Dt.Rows[0]["JOB_DESCRIPTION_CLOSE"].ToString();
                radTextBox_Update.ReadOnly = true;
                radButton_Save.Enabled = false;

                radDropDownList_GroupSUB.Enabled = false;
                radTextBox_tel.Enabled = false;
                radTextBox_SN.Enabled = false;
                radCheckBox_STAOUT.Checked = false;
                radCheckBox_STACLOSE.Enabled = false;
                radCheckBox_STAOUT.Enabled = false;

                Button_MNIO.Enabled = false;
                Button_MNOT.Enabled = false;
                Button_AX.Enabled = false;
                radButton_Add.Enabled = false;

                radButton_CloseAdd.Enabled = false;
                RadButton_OpenAdd.Enabled = false;
                RadButton_Car.Enabled = false;
                radButton_Cancel.Focus();
                radTextBox_Update.SelectionStart = radTextBox_Update.Text.Length;
            }
            else
            {
                radCheckBox_STACLOSE.Checked = false;
                radTextBox_Update.Enabled = true;
                radButton_Save.Enabled = true;

                radTextBox_Update.Focus();
            }

            if (_pPermission != "1")
            {
                SetPermission();
            }

            if (_pType == "SUPC")
            {
                if (_pPermission == "1")
                {
                    radButton_Add.Enabled = false;
                }
                else
                {
                    radButton_Add.Enabled = true;
                }
            }

            pChange = "0";
        }
        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (pChange == "1")
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.No;
            }
            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //ในกรณีที่ไม่มีการ Update
            if (radTextBox_Update.Text == "")
            {
                string strUpdGroupSUB;
                strUpdGroupSUB = $@"UPDATE {_tblName}
                    SET [JOB_GROUPSUB] = '{radDropDownList_GroupSUB.SelectedValue}',
                        [JOB_DATEUPD] = GETDATE(),
                        JOB_SN = '{radTextBox_SN.Text}',
                        JOB_HEADID = '{radDropDownList_Head.SelectedValue}',
                        JOB_TELPHONE = '{radTextBox_tel.Text}',
                        JOB_STA = '{radDropDownList_STA.SelectedValue}'
                    WHERE [JOB_Number] = '{_pJobNumber}' ";
                string TGroupSUB = ConnectionClass.ExecuteSQL_Main(strUpdGroupSUB);
                MsgBoxClass.MsgBoxShow_SaveStatus(TGroupSUB);
                if (TGroupSUB == "")
                {
                    GROUPSUB = radDropDownList_GroupSUB.SelectedIndex;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
                else
                {
                    return;
                }
            }
            //ในกรณีที่มีการ Update

            //checkPhone
            if (_pType == "SUPC")
            {
                if (radTextBox_tel.Text.Length != 10)
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("เบอร์โทรผู้แจ้ง");
                    radTextBox_tel.Focus();
                    return;
                }
            }


            string strUser;
            strUser = "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";


            string desc = radTextBox_Desc.Text;
            string descClose = "-" + radTextBox_Update.Text + Environment.NewLine + strUser;
            string strUpd;
            if (radCheckBox_STACLOSE.Checked == true)//ปิดจีอบ
            {
                strUpd = string.Format(@"UPDATE " + _tblName + " SET  [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'," +
                    "[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "' " +
                    ",[JOB_DESCRIPTION_CLOSE] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'" +
                    ",[JOB_TELPHONE] = '" + radTextBox_tel.Text + "' " +
                    ",[JOB_HEADID] = '" + radDropDownList_Head.SelectedValue + "' " +
                    ",[JOB_STA] = '" + radDropDownList_STA.SelectedValue + "'   " +
                    ",[JOB_STACLOSE] = '1'" +
                    ",[JOB_WHOCLOSE] = '" + SystemClass.SystemUserID + "',[JOB_NAMECLOSE] = '" + SystemClass.SystemUserName + "',[JOB_DATECLOSE] = GETDATE()" +
                    ",[JOB_DATEUPD] = GETDATE()  " +
                    " WHERE  [JOB_Number] = '" + _pJobNumber + "' ");
            }
            else
            {
                string staout = "0";
                if (radCheckBox_STAOUT.Checked == true)
                {
                    staout = "1";
                }
                desc = desc + Environment.NewLine + Environment.NewLine + descClose;
                strUpd = string.Format(@"UPDATE " + _tblName + "  SET	 [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'" +
                    ",[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "' " +
                    ",[JOB_DESCRIPTION] = '" + desc + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_TELPHONE] = '" + radTextBox_tel.Text + "' " +
                    ",[JOB_HEADID] = '" + radDropDownList_Head.SelectedValue + "' " +
                    ",[JOB_STA] = '" + radDropDownList_STA.SelectedValue + "'   " +
                    ",[JOB_DESCRIPTION_UPD] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_DATEUPD] = GETDATE() " +
                    ",[JOB_STAOUT] = '" + staout + "'  " +
                    "WHERE  [JOB_Number] = '" + _pJobNumber + "' ");
            }

            string T = ConnectionClass.ExecuteSQL_Main(strUpd);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        //check change
        private void RadCheckBox_STAOUT_CheckedChanged(object sender, EventArgs e)
        {
            string staout = "0";
            if (radCheckBox_STAOUT.Checked == true)
            {
                staout = "1";
            }

            string strUpd;
            strUpd = $@"UPDATE  {_tblName}
                        SET     [JOB_STAOUT] = '{ staout}',
                                [JOB_DATEUPD] = GETDATE()
                        WHERE   [JOB_Number] = '{_pJobNumber}' ";
            ConnectionClass.ExecuteSQL_Main(strUpd);
            pChange = "1";

        }
        //OpenLink
        private void RadButton_openLink_Click(object sender, EventArgs e)
        {
            if (radDropDownList_GroupSUB.SelectedValue.ToString() == "00003")
            {
                try
                {
                    if (radTextBox_SN.Text.Trim() != "")
                    {
                        System.Diagnostics.Process.Start("iexplore.exe", radTextBox_SN.Text.ToString());
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void RadCheckBox_STACLOSE_CheckedChanged(object sender, EventArgs e)
        {
            if (radCheckBox_STACLOSE.Checked == true)
            {
                int cSpcType = 0;
                for (int iT = 0; iT < radGridView_Show.Rows.Count; iT++)
                {
                    if (radGridView_Show.Rows[iT].Cells["Bill_SpcType"].Value.ToString() == "X")
                    {
                        cSpcType += 1;
                    }
                }

                if (cSpcType == 0)
                {
                    radTextBox_Update.Focus();
                    return;
                }


                if (_pType == "SHOP")
                {
                    int a = 0;
                    for (int i = 0; i < radGridView_Show.Rows.Count; i++)
                    {
                        if (radGridView_Show.Rows[i].Cells["Bill_TypeBchStatus"].Value.ToString() == "X")
                        {
                            a += 1;
                        }
                    }
                    if (a == 0)
                    {
                        radTextBox_Update.Focus();
                    }
                    else
                    {
                        if (SystemClass.SystemComProgrammer == "1")
                        {
                            if (ComfirmByProgrammer() == "1")
                            {
                                radTextBox_Update.Focus();
                            }
                            else
                            {
                                radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                                radTextBox_Update.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากสาขายังรับบิลไม่ครบทุกใบ" + Environment.NewLine + "จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                        }
                    }
                }
                else // SUPC
                {
                    int aa = 0;
                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        if (radGridView_Show.Rows[ii].Cells["Bill_SPC_Count"].Value.ToString() == "0")
                        {
                            aa += 1;
                        }
                    }
                    if (aa == 0)
                    {
                        radTextBox_Update.Focus();
                    }
                    else
                    {
                        if (SystemClass.SystemComProgrammer == "1")
                        {
                            if (ComfirmByProgrammer() == "1")
                            {
                                radTextBox_Update.Focus();
                            }
                            else
                            {
                                radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                                radTextBox_Update.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากไม่มีรูปการถ่ายออก " + Environment.NewLine + " จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                        }
                    }
                }

            }
        }
        //Comfirm
        string ComfirmByProgrammer()
        {
            if ((MessageBox.Show("ยันยันการปิดจ๊อบทั้งที่บิลยังไม่เรียบร้อย ?.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
            {
                return "0";
            }
            else
            {
                return "1";
            }
        }
        //click

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Show.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                        //JOBClass.FindImageBillAll_ByJobID(BillAllClass.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD": //รูปสาขาส่งซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLD_Count"].Value.ToString(),
                           radGridView_Show.CurrentRow.Cells["Bill_OLD_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC": //รูปสาขาใหญ่รับซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                          radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    break;

            }

        }
        //MNIO
        private void Button_MNIO_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        // MNOT
        private void Button_MNOT_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNOT", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //Car Open
        private void RadButton_Car_Click(object sender, EventArgs e)
        {
            DataTable dt = Models.AssetClass.FindDataVEHICLE("", " AND VEHICLEGROUP !='MOTORCYCLE' "); //JOBClass.GetDataCar();
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV()
            { dtData = dt };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                string strUpCar = string.Format(@"Update " + _tblName + " " +
                    "SET JOB_CARID = '" + frm.pID + "' " +
                    "WHERE JOB_Number = '" + _pJobNumber + "' ");
                string T = ConnectionClass.ExecuteSQL_Main(strUpCar);
                if (T == "")
                {
                    radTextBox_Car.Text = frm.pID;
                    pChange = "1";
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    radTextBox_Car.Text = "";
                }
            }
            else
            {
                radTextBox_Car.Focus();
            }
        }
        //AddImage open JOB
        private void RadButton_OpenAdd_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMaintenance + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            string pFileName = _pJobNumber + "-" + pBchID + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, _tblName, "JOB_STAIMG_OPEN", "JOB_Number", _pJobNumber, "1");
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.Yes)
            {
                radButton_OpenView.Enabled = true;
            }
            else if (frm.DialogResult == DialogResult.OK)
            {
                UploadImageSelectArea _UploadImageSelectArea = new UploadImageSelectArea("0", pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber);
                _UploadImageSelectArea.Show();
                radButton_OpenView.Enabled = true;
            }

        }
        //ViewImage Open
        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMaintenance + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            { pathImg = pPath + @"|*.jpg" };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }
        //Close JOB Add omage
        private void RadButton_CloseAdd_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMaintenanceClose + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            string pFileName = _pJobNumber + "-" + pBchID + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, _tblName, "JOB_STAIMG_CLOSE", "JOB_Number", _pJobNumber, "1");
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.Yes)
            {
                radButton_CloseView.Enabled = true;
            }
            else if (frm.DialogResult == DialogResult.OK)
            {
                UploadImageSelectArea _UploadImageSelectArea = new UploadImageSelectArea("0", pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber);
                _UploadImageSelectArea.Show();
                radButton_CloseView.Enabled = true;
            }
        }
        //ViewImage Close
        private void RadButton_CloseView_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBMaintenanceClose + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            {
                pathImg = pPath + @"|*.jpg"
            };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }
        //บิลส่งของ
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            switch (_pType)
            {
                case "SUPC":
                    string pDesc = radTextBox_Desc.Text.Substring(1, radTextBox_Desc.Text.IndexOf("->") - 1).Replace(Environment.NewLine, "");
                    JOB.Bill.Bill_MNRZ frmMNRZ = new Bill.Bill_MNRZ(pBchID, radLabel_BranchName.Text, _pJobNumber, radLabel_tel.Text, pDesc);
                    frmMNRZ.ShowDialog(this);
                    if (frmMNRZ.pDialog == "1")
                    {
                        JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);
                    }
                    break;
                case "SHOP":
                    DataTable dtJOB = BillOut_Class.FindBill_ForAddJOB("'D041','D062'", pBchID);
                    if (dtJOB.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บิลทั้งหมด มี JOB เรียบร้อยแล้ว");
                        return;
                    }

                    JOB.Bill.BillMN_ForAddJOB frmAddJOb = new Bill.BillMN_ForAddJOB("'D041','D062'", pBchID, _pJobNumber, _grpId);
                    if (frmAddJOb.ShowDialog(this) == DialogResult.Yes)
                    {
                        JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);
                    }

                    break;
                default:
                    break;
            }
        }
        //Double Click
        private void RadGridView_Send_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Send.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }

        private void Button_AX_Click(object sender, EventArgs e)
        {
            Bill.BillAXAddJOB frm = new Bill.BillAXAddJOB(pBchID, radLabel_BranchName.Text, "1",
                _pJobNumber, _grpId, _grpName, radTextBox_SN.Text, "", "", "");
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //
        private void Button_MNIO_stockID_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", _pType, _pJobNumber, _grpId, _grpName, "1")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
    }
}
