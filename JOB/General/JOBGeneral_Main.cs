﻿//CheckOK
using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
 

namespace PC_Shop24Hrs.JOB.General
{
    public partial class JOBGeneral_Main : RadForm
    {
        readonly string _pType;
        readonly string _pGROUPID;
        readonly string _pGROUP_DESC;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปิดจ๊อบ 1 มี
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        readonly string tbl;
        readonly string billFirst;

        private DataTable Dt;
        private DateTime timeForRefresh;
        private void JOBGeneral_Main_Load(object sender, EventArgs e)
        {
            radButtonElement_Excel.ShowBorder = true;
            // this.Text = _pType + " - " + _pGROUP_DESC;
        }
        //Main Load
        public JOBGeneral_Main(string pType, string pGROUPID, string pGROUP_DESC, string pPerminssion, string pFixBch) // SHOP-SUPC
        {
            InitializeComponent();

            _pType = pType;
            _pGROUPID = pGROUPID;
            _pGROUP_DESC = pGROUP_DESC;
            _pPerminssion = pPerminssion;
            _pFixBch = pFixBch;

            // DataTable dtGroup = new DataTable();
            DataTable dtGroup = JOB_Class.GetJOB_Group(_pGROUPID);
            tbl = dtGroup.Rows[0]["TABLENAME"].ToString();
            billFirst = dtGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString();

            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            //this.Text = "JOB SHOP";
            string pNameID = "สาขา";
            string pName = "ชื่อสาขา";
            if (_pType == "SUPC")
            {
                pNameID = "แผนก";
                pName = "ชื่อแผนก";
                // this.Text = "JOB SUPC";
            }

            timeForRefresh = DateTime.Now.AddMinutes(3); timer_Auto.Start();

            radButtonElement_add.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;
            radRadioButtonElement_Open.ShowBorder = true;
            radRadioButtonElement_ALL.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATE", "S", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATEUPD", "Up", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_Number", "เลขที่ JOB", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_HeadName", "ช่างรับผิดชอบ", 250));
            
            if (_pType == "SUPC")
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_BRANCHOUTPHUKET", "อต"));
            }
            else
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("JOB_BRANCHOUTPHUKET", "อต", 50));
            }

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("JOB_STAOUT", "อน", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_CARID", "รถ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchID(pNameID));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchName(pName));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SHORTNAMEINS", "ผู้เปิด", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ประเภท", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SN", "ทะเบียน", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_IP", "เครื่อง", 120));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_STACLOSE", "สถานะจ๊อบ"));
            //Freeze Column
            for (int i = 0; i < 2; i++)
            {
                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;
            }

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_STAOUT = '/' ", ConfigClass.SetColor_PurplePastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_STAOUT = '/' ", ConfigClass.SetColor_PurplePastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("JOB_STAOUT", " JOB_STAOUT = '/' ", ConfigClass.SetColor_PurplePastel(), radGridView_Show);
            //ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "JOB_STAOUT = '/' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PurplePastel() };
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj1);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj1);
            //this.radGridView_Show.Columns["JOB_STAOUT"].ConditionalFormattingObjectList.Add(obj1);

            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "JOB_STACLOSE = 1 ", false)
            //{ CellBackColor = ConfigClass.SetColor_GreenPastel() };
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj2);

            DatagridClass.SetCellBackClolorByExpression("JOB_BRANCHOUTPHUKET", " JOB_BRANCHOUTPHUKET = '/' ", ConfigClass.SetColor_PinkPastel(), radGridView_Show);
            //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition2", "JOB_BRANCHOUTPHUKET = '/' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PinkPastel() };
            //this.radGridView_Show.Columns["JOB_BRANCHOUTPHUKET"].ConditionalFormattingObjectList.Add(obj3);

            radGridView_Show.TableElement.SearchHighlightColor = Color.LightBlue;

            SetDGV();

        }

        //find Data
        void SetDGV()
        {
            string sta_JobAll = "   '0' ";
            if (radRadioButtonElement_Open.CheckState == CheckState.Unchecked) { sta_JobAll = "   '0','1' "; }

            string sql = string.Format($@" SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOBGROUPSUB_DESCRIPTION,
                            JOB_HeadName,JOB_CARID, 
                            JOB_SN,JOB_DESCRIPTION,JOB_DESCRIPTION_UPD,
                            CASE ISNULL(JOB_BRANCHOUTPHUKET,'X') WHEN '0' THEN CONVERT(VARCHAR,'X') ELSE CONVERT(VARCHAR,'/') END  AS JOB_BRANCHOUTPHUKET,JOB_STACLOSE,
                            CASE [JOB_STA] WHEN '01' THEN 'เปิดจ๊อบ' WHEN '02' THEN 'รอซ่อม' WHEN '03' THEN 'กำลังซ่อม' WHEN '04' THEN 'ซ่อมเสร็จ'  END AS STA  ,
                            CASE JOB_STAOUT WHEN '0' THEN CONVERT(VARCHAR,'X') ELSE CONVERT(VARCHAR,'/') END AS JOB_STAOUT, JOB_SHORTNAMEINS, 
                            DateDiff(Day,JOB_DATEINS,GETDATE()) AS SUMDATE   , 
                            CASE CONVERT(VARCHAR,JOB_DATEUPD,23) WHEN '1900-01-01' THEN  DateDiff(Day,JOB_DATEINS,GETDATE())
						    ELSE ISNULL(DATEDIFF(DAY,JOB_DATEUPD,GETDATE()),0) END AS SUMDATEUPD
                        FROM	{tbl} WITH (NOLOCK)  
                        WHERE JOB_STACLOSE IN ({sta_JobAll})  
                        AND JOB_TYPE = '{_pType}'");

            if (_pFixBch != "")
            {
                sql += $@" AND JOB_BRANCHID = '{_pFixBch}' ";
            }

            sql += " ORDER BY CONVERT(VARCHAR,JOB_DATEINS,25) DESC ";

            Dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            radGridView_Show.DataSource = Dt;
        }

        #region "ROWS NUMBERS"      
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //ROWS NUMBERS
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        //Open
        private void RadRadioButtonElement_Open_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            OpenFormEdit();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            if (_pType == "SHOP")
            {
                JOBGeneral_SHOP_ADD frmJOB_ADD = new JOBGeneral_SHOP_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC, _pPerminssion,_pFixBch);
                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
                {
                    SetDGV();
                }
            }
            else
            {

                JOBGeneral_SUPC_ADD frmJOB_ADD = new JOBGeneral_SUPC_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC, _pPerminssion);
                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
                {
                    SetDGV();
                }
            }
        }
        //All
        private void RadRadioButtonElement_ALL_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Open for Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            OpenFormEdit();
        }
        //Open Form
        void OpenFormEdit()
        {
            if ((radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString() == "") ||
                (radGridView_Show.CurrentRow.Index == -1) ||
                (radGridView_Show.Rows.Count == 0))
            {
                return;
            }

            JOBGeneral_EDIT frmJOB_Edit = new JOBGeneral_EDIT(tbl, _pGROUPID, _pGROUP_DESC, _pPerminssion, _pType, radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString());
            if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK)
            {
                SetDGV();
            }
        }
        //Auto Refresh
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > timeForRefresh)
            {
                timer_Auto.Stop();
                SetDGV();
                timeForRefresh = DateTime.Now.AddMinutes(3);
                timer_Auto.Start();
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("งานช่างทั่วไป", radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}



