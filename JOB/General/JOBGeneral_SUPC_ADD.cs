﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.JOB.General
{
    public partial class JOBGeneral_SUPC_ADD : Telerik.WinControls.UI.RadForm
    {
        string bchID;
        string bchName;

        readonly string _tblName;
        readonly string _billFirst;
        readonly string _grpId;
        readonly string _grpDesc;
        readonly string _pPermission;
        public JOBGeneral_SUPC_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc, string pPermission)
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _pPermission = pPermission;
        }
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //set ค่าของ radCombo select branch before Use
        void SetCbDpt()
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            //Set RadDropDownList All
            this.radDropDownList_Dpt.DropDownListElement.ListElement.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.AutoSizeItems = true;
            this.radDropDownList_Dpt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 30;

            DataTable dtDpt = Models.DptClass.GetDpt_AllD();

            radDropDownList_Dpt.DataSource = dtDpt;
            radDropDownList_Dpt.ValueMember = "NUM";
            radDropDownList_Dpt.DisplayMember = "DESCRIPTION_SHOW";
        }
        void Set_ClearData()
        {
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            Set_GroupSub();
            SetCbDpt();
            radTextBox_SN.Text = "";
            radTextBox_Tel.Text = "";
            radTextBox_Desc.Text = "";
            bchID = "";
        }
        private void JOBGeneral_SUPC_ADD_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);

            Set_ClearData();

            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SUPC";

            bchID = radDropDownList_Dpt.SelectedValue.ToString();
            bchName = Models.DptClass.GetDptName_ByDptID(bchID);

            radTextBox_Tel.Text = Models.EmplClass.GetTelEmployeeByEmplID(SystemClass.SystemUserID);

            if (_pPermission == "0")
            {
                radDropDownList_Dpt.SelectedValue = SystemClass.SystemDptID;
            }
        }

        private void RadDropDownList_Dpt_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                bchID = radDropDownList_Dpt.SelectedValue.ToString();
                bchName = Models.DptClass.GetDptName_ByDptID(bchID);
                radDropDownList_GroupSUB.Focus();
            }
            catch (Exception)
            {
                MessageBox.Show("เช็คข้อมูลแผนกให้ถูกต้องอีกครั้ง.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radDropDownList_Dpt.Focus();
                return;
            }
        }

        private void RadTextBox_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Tel.Focus();
            }
        }

        private void RadTextBox_ip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Desc.Focus();
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_SN.Focus();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            try
            {
                bchID = radDropDownList_Dpt.SelectedValue.ToString();
                bchName = Models.DptClass.GetDptName_ByDptID(bchID);
            }
            catch (Exception)
            {
                MessageBox.Show("เช็คข้อมูลแผนกให้เรียบร้อยก่อนการบันทึก.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radDropDownList_Dpt.Focus();
                return;
            }

            if (bchID == "" || bchName == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("สาขา");
                radDropDownList_Dpt.Focus();
                return;
            }
            //รายละเอียด
            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียด");
                radTextBox_Desc.Focus();
                return;
            }
            //checkPhone
            if (radTextBox_Tel.Text.Length != 10)
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("เบอร์โทรผู้แจ้ง");
                radTextBox_Tel.Focus();
                return;
            }
            string descJOB;
            descJOB = "- " + radTextBox_Desc.Text.Trim();
            descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = String.Format(@"INSERT INTO " + _tblName + " ([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],[JOB_GROUPSUB] " +
                            ",[JOB_WHOINS],[JOB_NAMEINS],[JOB_SHORTNAMEINS],[JOB_DESCRIPTION],[JOB_SN],[JOB_TELPHONE],[JOB_DATEINS],[JOB_TYPE])  VALUES  " +
                            "('" + billMaxNO + "','" + bchID + "','" + bchName + "','" + radDropDownList_GroupSUB.SelectedValue + "'," +
                            "'" + SystemClass.SystemUserID + "','" + SystemClass.SystemUserName + "'," +
                            "'" + SystemClass.SystemUserNameShort + "','" + descJOB + "'+ ' [' + CONVERT(VARCHAR,GETDATE(),25) + ']'," +
                            "'" + radTextBox_SN.Text.Trim() + "','" + radTextBox_Tel.Text.Trim() + "',GETDATE(),'SUPC' ) ");
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
    }
}
