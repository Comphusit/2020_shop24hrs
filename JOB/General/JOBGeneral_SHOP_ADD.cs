﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.General
{
    public partial class JOBGeneral_SHOP_ADD : Telerik.WinControls.UI.RadForm
    {

        string bchId;
        string bchName;

        readonly string _tblName;
        readonly string _billFirst;
        readonly string _grpId;
        readonly string _grpDesc;
        readonly string _pPermission;
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        public JOBGeneral_SHOP_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc, string pPermission,string pFixBch)
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _pPermission = pPermission;
            _pFixBch = pFixBch;
        }
        //ประเภทงาน
        void Set_GroupSub()
        {

            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";

        }
        //set ค่าของ radCombo select branch before Use
        void SetCbDpt()
        {
            //Set RadDropDownList All
            this.radDropDownList_Dpt.DropDownListElement.ListElement.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.AutoSizeItems = true;
            this.radDropDownList_Dpt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 30;


            DataTable dtDpt = BranchClass.GetBranchAll("'1','4'","'0','1'");

            radDropDownList_Dpt.DataSource = dtDpt;
            radDropDownList_Dpt.ValueMember = "BRANCH_ID";
            radDropDownList_Dpt.DisplayMember = "NAME_BRANCH";
        }
        //Clear
        void Set_ClearData()
        {
            Set_GroupSub();
            SetCbDpt();
            bchId = ""; bchName = "";
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            radTextBox_SN.Text = "";
            radTextBox_Desc.Text = "";
        }

        private void JOBGeneral_SHOP_ADD_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            Set_ClearData();
            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SHOP";

            if (_pFixBch != "")
            {
                radDropDownList_Dpt.SelectedValue = SystemClass.SystemBranchID;
                radDropDownList_Dpt.Enabled = false;
                radTextBox_SN.Focus();
            }
        }

        private void RadTextBox_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_SN.Focus();
            }
        }

        //private void RadTextBox_ip_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        radTextBox_Desc.Focus();
        //    }
        //}

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            bchId = radDropDownList_Dpt.SelectedValue.ToString();
            bchName = BranchClass.GetBranchNameByID(bchId);

            if (bchId == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("สาขา");
                radDropDownList_Dpt.Focus();
                return;
            }

            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียด");
                radTextBox_Desc.Focus();
                return;
            }

            string descJOB;
            descJOB = "- " + radTextBox_Desc.Text.Trim();
            descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = String.Format(@"
                            INSERT INTO " + _tblName + "([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],[JOB_GROUPSUB],[JOB_WHOINS],[JOB_NAMEINS]," +
                            "[JOB_SHORTNAMEINS],[JOB_DESCRIPTION],[JOB_SN],[JOB_DATEINS])  " +
                            "VALUES('" + billMaxNO + "', '" + bchId + "', '" + bchName + "', '" + radDropDownList_GroupSUB.SelectedValue + "', " +
                            "'" + SystemClass.SystemUserID + "', '" + SystemClass.SystemUserName + "', '" + SystemClass.SystemUserNameShort + "', " +
                            "'" + descJOB + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']', '" + radTextBox_SN.Text.Trim() + "',GETDATE()) "
                            );
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);

            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_SN.Focus();
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
    }
}
