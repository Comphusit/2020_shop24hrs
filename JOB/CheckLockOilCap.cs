﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Data;
using System.Drawing.Printing;
using System.Collections;
using System.Drawing;
using PrintBarcode;
using System.Text;

namespace PC_Shop24Hrs.JOB
{
    public partial class CheckLockOilCap : Telerik.WinControls.UI.RadForm
    {
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        string TypeLock;//0 Lock  1 UnLock
        readonly string _pTypeCase; //0 tag ฝาถังน้ำมัน
        int qtyPrint;
        string barcodePrint;
        public CheckLockOilCap(string pTypeCase)
        {
            InitializeComponent();
            _pTypeCase = pTypeCase;
        }
        //ClearData
        void ClearData()
        {
            barcodePrint = "";
            radTextBox_TagNumber.Text = ""; radTextBox_TagNumber.Enabled = true;
            radTextBox_CarID.Text = ""; radTextBox_CarID.Enabled = false;
            radButton_Save.Enabled = false; radButton_Save.Text = "บันทึก";
            radButton_Cancel.Enabled = true;
            radLabel_sta.Text = "";
            radLabel_DptCar.Text = "";
            qtyPrint = 0;
            radTextBox_TagNumber.Focus();
        }
        //load
        private void InputData_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 38;
            barcode.LeftMargin = 0;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            ClearData();

            switch (_pTypeCase)
            {
                case "0":
                    radTextBox_TagNumber.MaxLength = 8;
                    break;
                case "1":
                    SetCase1_PrintBarcodeDiscount("0");
                    break;
                default:
                    break;
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            switch (_pTypeCase)
            {
                case "0"://Lock Car Oil
                    string tag = radTextBox_TagNumber.Text.Trim().Substring(0, 7);
                    string sql;
                    if (TypeLock == "0")
                    {
                        sql = $@"
                            UPDATE  SHOP_TAGCARTRUCK
                            SET     StaLock = '1',TagFullNumber = '{radTextBox_TagNumber.Text.Trim()}',LOGISTICID = '{radTextBox_CarID.Text}',
                                    BranchLock = 'MN000',BranchNameLock = '{radLabel_DptCar.Text}',WhoLock = '{SystemClass.SystemUserID}',
                                    WhoNameLock = '{SystemClass.SystemUserName}',DateLock= CONVERT(VARCHAR,GETDATE(),23),TimeLock = CONVERT(VARCHAR,GETDATE(),24)
                            WHERE   TagNumber = '{tag}' ";
                    }
                    else
                    {
                        sql = $@"
                            UPDATE  SHOP_TAGCARTRUCK
                            SET     StaUnlock = '1',BranchUnlock = 'MN000',BranchNameUnlock = 'ซุปเปอร์ชีป',WhoUnlock = '{SystemClass.SystemUserID}',
                                    WhoNameUnLock = '{SystemClass.SystemUserName}',DateUnlock= CONVERT(VARCHAR,GETDATE(),23),TimeUnlock = CONVERT(VARCHAR,GETDATE(),24)
                            WHERE   TagNumber = '{tag}' ";
                    }
                    string Stalock = ConnectionClass.ExecuteSQL_Main(sql);
                    MsgBoxClass.MsgBoxShow_SaveStatus(Stalock);
                    if (Stalock == "") ClearData();

                    break;
                case "1"://Barcode Discount
                    SetCase1_PrintBarcodeDiscount("2");
                    break;
                default:
                    break;
            }

        }

        //Enter TagNumber
        private void RadTextBox_TagNumber_KeyDown(object sender, KeyEventArgs e)
        {
            switch (_pTypeCase)
            {
                case "0":
                    switch (e.KeyCode)
                    {
                        case Keys.Enter:
                            if (radTextBox_TagNumber.Text.Trim() == "") return;
                            if (radTextBox_TagNumber.Text.Trim().Length != 8)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("tag ที่ระบุจะต้องมีจำนวน 8 หลัก เท่านั้น");
                                ClearData();
                                return;
                            }
                            string tag = radTextBox_TagNumber.Text.Trim().Substring(0, 7);
                            string sqltag = $@"
                                SELECT	TagNumber,BRANCH,BranchName,ISNULL(TagFullNumber,'') AS TagFullNumber,TagDpt,ISNULL(LOGISTICID,'') AS LOGISTICID,
		                                ISNULL(BranchLock,'') AS BranchLock,ISNULL(BranchNameLock,'') AS BranchNameLock,StaLock,ISNULL(WhoLock,'') AS WhoLock,ISNULL(WhoNameLock,'') AS WhoNameLock,
		                                ISNULL(CONVERT(VARCHAR,DateLock,23),'-') AS DateLock,ISNULL(TimeLock,'') AS TimeLock,
		                                ISNULL(BranchUnlock,'') AS BranchUnlock,StaUnlock,ISNULL(WhoUnlock,'') AS WhoUnlock,ISNULL(WhoNameUnLock,'') AS WhoNameUnLock,
		                                ISNULL(CONVERT(VARCHAR,DateUnlock,23),'-') AS DateUnlock,ISNULL(TimeUnlock,'') AS TimeUnlock
                                FROM	SHOP_TAGCARTRUCK WITH (NOLOCK) 
                                WHERE	TagNumber = '{tag}' ";
                            DataTable dtTag = ConnectionClass.SelectSQL_Main(sqltag);
                            if (dtTag.Rows.Count == 0)
                            {
                                MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ Tag ที่ระบุ");
                                ClearData();
                                return;
                            }

                            if (dtTag.Rows[0]["TagDpt"].ToString() != "D079")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"เลข Tag ที่ระบุ ไม่ใช่ Tag ใช้งานของแผนกยานยนต์" + Environment.NewLine + "ติดต่อข้อมูลเพิ่มเติมที่ ComMinimart 8570");
                                ClearData();
                                return;
                            }

                            if ((dtTag.Rows[0]["StaLock"].ToString() == "1") && (dtTag.Rows[0]["StaUnlock"].ToString() == "1"))
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"เลขที่ Tag ที่ระบุ ถูกล็อค/ปลดล็อค เรียบร้อยแล้ว{Environment.NewLine}ไม่สามารถใช้งานซ้ำได้");
                                ClearData();
                                return;
                            }

                            if (dtTag.Rows[0]["StaLock"].ToString() == "1")//ต้องปลดล็อก
                            {
                                TypeLock = "1";
                                radTextBox_CarID.Enabled = false;
                                radLabel_sta.Text = "UnLock";
                                radLabel_sta.ForeColor = ConfigClass.SetColor_Red();
                                radLabel_DptCar.Text = dtTag.Rows[0]["BranchNameLock"].ToString();
                                radTextBox_CarID.Text = dtTag.Rows[0]["LOGISTICID"].ToString();
                                radButton_Save.Enabled = true;
                                radButton_Save.Text = "UnLock";
                                radButton_Save.Focus();
                            }
                            else//ต้องล็อก
                            {
                                TypeLock = "0";
                                radTextBox_CarID.Enabled = true;
                                radLabel_sta.Text = "Lock";
                                radButton_Save.Text = "Lock";
                                radLabel_sta.ForeColor = ConfigClass.SetColor_Blue();
                                radTextBox_CarID.Focus();
                            }

                            radTextBox_TagNumber.Enabled = false;

                            break;
                        default:
                            break;
                    }
                    break;
                case "1":
                    if (e.KeyCode == Keys.Enter) SetCase1_PrintBarcodeDiscount("1");
                    break;
                default:
                    break;
            }

        }

        //keyNumbers
        private void RadTextBox_TagNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;//&& (e.KeyChar != '.')
        }

        private void RadTextBox_CarID_KeyDown(object sender, KeyEventArgs e)
        {
            switch (_pTypeCase)
            {
                case "0":
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (radTextBox_CarID.Text.Trim() == "") return;

                        //string sqlCar = $@"
                        //    SELECT	VEHICLEID AS DATA_ID,NAME,
                        //            CASE ISNULL(DIMENSIONS.DESCRIPTION,'') WHEN '' THEN NAME ELSE NAME+'/'+DIMENSIONS.DESCRIPTION END AS DATA_DESC,
                        //            ISNULL(SHOP_TAGCARTRUCK.LOGISTICID,'A') AS STANOLOCK 
                        //    FROM	SHOP2013TMP.[dbo].[SPC_VEHICLETABLE] WITH(NOLOCK) 
                        //      LEFT OUTER JOIN SHOP2013TMP.[dbo].DIMENSIONS WITH (NOLOCK) ON [SPC_VEHICLETABLE].DIMENSION = DIMENSIONS.NUM
                        //       AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                        //            LEFT OUTER JOIN SHOP_TAGCARTRUCK WITH (NOLOCK) ON [SPC_VEHICLETABLE].VEHICLEID = SHOP_TAGCARTRUCK.LOGISTICID AND StaLock = '1' AND StaUnlock = '0' 
                        //    WHERE	[SPC_VEHICLETABLE].DATAAREAID = N'SPC'  AND VEHICLEID LIKE '%{radTextBox_CarID.Text.Trim()}'
                        //    ";

                        DataTable dtCar = Models.AssetClass.FindDataVEHICLELogTagOil(radTextBox_CarID.Text.Trim()); //ConnectionClass.SelectSQL_Main(sqlCar);
                        if (dtCar.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลทะเบียนรถที่ระบุ");
                            radTextBox_CarID.SelectAll();
                            radTextBox_CarID.Focus();
                        }

                        string staNoLock;
                        if (dtCar.Rows.Count > 1)
                        {
                            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV() { dtData = dtCar };
                            if (frm.ShowDialog(this) == DialogResult.Yes)
                            {
                                radTextBox_CarID.Text = frm.pID;
                                radLabel_DptCar.Text = frm.pDesc;

                                DataRow[] result = dtCar.Select("DATA_ID = '" + radTextBox_CarID.Text + @"'  ");
                                staNoLock = result[0]["STANOLOCK"].ToString();
                            }
                            else
                            {
                                ClearData();
                                return;
                            }
                        }
                        else
                        {
                            radTextBox_CarID.Text = dtCar.Rows[0]["DATA_ID"].ToString();
                            radLabel_DptCar.Text = dtCar.Rows[0]["DATA_DESC"].ToString();
                            staNoLock = dtCar.Rows[0]["STANOLOCK"].ToString();
                        }

                        if (staNoLock != "A")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ทะเบียนรถที่ระบุ ถูกล็อคอยู่แล้ว{Environment.NewLine}ไม่สามารถล็อคซ้ำได้ต้องปลดล็อคก่อนเท่านั้น");
                            ClearData();
                            return;
                        }

                        radTextBox_CarID.Enabled = false;
                        radButton_Save.Enabled = true;
                        radButton_Save.Focus();
                    }
                    break;
                default:
                    break;
            }

        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            int Y = 0;
            barcode.Data = barcodePrint;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            e.Graphics.DrawString("*** สินค้าลดราคา ***", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect = new Rectangle(0, Y, 150, 20);
            e.Graphics.DrawString(barcodePrint, SystemClass.printFont, Brushes.Black, rect, stringFormat);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 60;
            Rectangle rect1 = new Rectangle(0, Y, 150, 20);
            e.Graphics.DrawString(SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
        }

        void SetCase1_PrintBarcodeDiscount(string pCase)
        {
            switch (pCase)
            {
                case "0"://Set Default
                    this.Text = "ระบุจำนวนบาร์โค้ดลดราคาที่ต้องการ";
                    radButton_Save.Text = "พิมพ์";

                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radTextBox_TagNumber.MaxLength = 3;
                        radLabel_Input.Text = "ระบุจำนวนบาร์โค้ด [ไม่เกิน 100 ดวง/ครั้ง]";
                    }
                    else
                    {
                        radTextBox_TagNumber.MaxLength = 2;
                        radLabel_Input.Text = "ระบุจำนวนบาร์โค้ด [ไม่เกิน 10 ดวง/ครั้ง]";
                    }

                    radLabel_Car.Visible = false;
                    radTextBox_CarID.Visible = false;
                    radLabel_DptCar.Visible = false;
                    radLabel_sta.Visible = false;
                    break;
                case "1"://Enter Qty Barcode For use print
                    int qty = int.Parse(radTextBox_TagNumber.Text);
                    int qtyMax = 10;
                    if (SystemClass.SystemBranchID == "MN000") qtyMax = 100;

                    if (qty > qtyMax)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"จำกัดการพิมพ์ครั้งละ {qtyMax} ดวง ต่อครั้งเท่านั้น.");
                        radTextBox_TagNumber.SelectAll();
                        radTextBox_TagNumber.Focus();
                        return;
                    }

                    qtyPrint = qty;
                    radTextBox_TagNumber.Enabled = false;
                    radButton_Save.Enabled = true;
                    radButton_Save.Focus();
                    break;
                case "2":
                    string fristDoc = SystemClass.SystemBranchID.Replace("MN", "") + DateTime.Now.ToString("yy-MM-dd").Replace("-", "");
                    string typeRuning = "5";
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        fristDoc = "MN" + DateTime.Now.ToString("yy-MM-dd").Replace("-", "");
                        typeRuning = "6";
                    }

                    string docno;

                    barcodePrint = "";
                    DataTable dtBarcodeDis = new DataTable(); dtBarcodeDis.Columns.Add("BARCODE");

                    int countBar = int.Parse(radTextBox_TagNumber.Text.Trim());
                    ArrayList sqlIn = new ArrayList();
                    for (int i = 0; i < countBar; i++)
                    {
                        docno = ConfigClass.GetMaxINVOICEID("BARCODEDISCOUNT", "-", fristDoc, typeRuning);
                        sqlIn.Add($@"
                        INSERT INTO [SHOP_BARCODEDISCOUNT]
                                   ([BRANCHID],[BRANCHNAME],[ITEMBARCODE_DIS],[WHOINS_DIS],[WHONAMEINS_DIS])
                        VALUES	    ('{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}','{docno}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')");
                        dtBarcodeDis.Rows.Add(docno);
                    }

                    string res = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
                    if (res == "")
                    {
                        DialogResult result = printDialog1.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            if (SystemClass.SystemBranchID == "MN000")
                            {
                                StringBuilder rowData = new StringBuilder();
                                for (int i = 0; i < dtBarcodeDis.Rows.Count; i++)
                                {
                                    rowData.Append(PrintClass.SetPrintBarcodeDiscountGodex(dtBarcodeDis.Rows[i]["BARCODE"].ToString()));
                                }
                                RawPrinterHelper.SendStringToPrinter(printDialog1.PrinterSettings.PrinterName, rowData.ToString());
                            }
                            else
                            {
                                printDocument1.PrintController = printController;
                                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                                for (int i = 0; i < dtBarcodeDis.Rows.Count; i++)
                                {
                                    barcodePrint = dtBarcodeDis.Rows[i]["BARCODE"].ToString();
                                    printDocument1.Print();
                                }
                            }

                        }
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    break;
                default:
                    break;
            }

        }
    }
}
