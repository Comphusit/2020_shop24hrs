﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.JOB.Stock
{
    public partial class Stock_Item : Telerik.WinControls.UI.RadForm
    {
        string staSave;
        readonly string _Pcase; // 1 : เคสสำหรับสต็ออะไหล่
        //   int iRow;
        readonly DataTable dt = new DataTable();
        //Load
        public Stock_Item(string PCase = "")
        {
            _Pcase = PCase;
            InitializeComponent();
            radStatusStrip1.SizingGrip = false;
            this.radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มสินค้า";
            this.radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขสินค้า";
            if (_Pcase == "00516")
            {
                this.radButtonElement_Add.Enabled = false;
                this.radButtonElement_Edit.Enabled = false;
                radButton_Cancel.Enabled = false;
            }
            this.radButtonElement_MNII.ShowBorder = true; radButtonElement_MNII.ToolTipText = "รับเข้าสินค้า";
            radButtonElement_Refresh.ShowBorder = true; radButtonElement_Refresh.ToolTipText = "ดึงข้อมูลใหม่";
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(radGridView_main);

            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "ใช้งาน"));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StockType", "ประเภท", 130));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StockTypeName", "ชื่อประเภท", 150));

            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StockID", "รหัสสินค้า", 130));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StockName", "ชื่อสินค้า", 300));
            //radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("StockQty", "จำนวนคงเหลือ", 130));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("StockQty", "จำนวนคงเหลือ", 130));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StockUnit", "หน่วย", 100));


            ClearData();

        }
        //Load Main
        private void Stock_Item_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            SetDGV();
        }
        //Set DGV
        void SetDGV()
        {
            string CaseFactory = "WHERE	StockType NOT LIKE '00516'";
            if (_Pcase == "00516") CaseFactory = " WHERE	StockType = '00516' ";
            string sql = $@"
                SELECT	[StockID],[StockName],[StockType],JOBGROUPSUB_DESCRIPTION AS StockTypeName,[StockQty],
                        [StockUnit],[STA],[WHOINS],[WHONAMEINS]
                FROM	[SHOP_STOCKITEM] WITH (NOLOCK)
			                INNER JOIN (SELECT JOBGROUPSUB_ID,JOBGROUPSUB_DESCRIPTION FROM [SHOP_JOBGROUPSUB] WITH (NOLOCK) WHERE JOBGROUP_ID = '00014')SHOP_JOBGROUPSUB 
            				    ON [SHOP_STOCKITEM].[StockType] = [SHOP_JOBGROUPSUB].JOBGROUPSUB_ID
                {CaseFactory}
                ORDER BY [StockID]";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            radGridView_main.DataSource = dt;
        }
        //sub grp
        void Set_GropMain()
        {
            radDropDownList_MainGroup.DataSource = JOB_Class.GetJOB_GroupSub("00014");
            radDropDownList_MainGroup.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_MainGroup.DisplayMember = "JOBGROUPSUB_DESCRIPTION";

            if (_Pcase == "00516")
            {
                string sql = $@"
                    SELECT	[JOBGROUPSUB_ID],[JOBGROUPSUB_DESCRIPTION],[REMARK],'แสดงคู่มือ' AS Doc 
                    FROM	SHOP_JOBGROUPSUB WITH (NOLOCK)
                    WHERE	JOBGROUP_ID = '00014'
                            AND JOBGROUPSUB_ID = '00516'";
                DataTable dt = ConnectionClass.SelectSQL_Main(sql);

                radDropDownList_MainGroup.DataSource = dt;
                radDropDownList_MainGroup.ValueMember = "JOBGROUPSUB_ID";
                radDropDownList_MainGroup.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
            }
        }

        //claer
        void ClearData()
        {
            staSave = "0";

            radTextBox_ID.Text = ""; radTextBox_ID.Enabled = false;
            radTextBox_Name.Text = ""; radTextBox_Name.Enabled = false;
            radTextBox_Unit.Text = ""; radTextBox_Unit.Enabled = false;

            radDropDownList_MainGroup.Enabled = false;

            radButton_Save.Enabled = false;
            radCheckBox_Sta.CheckState = CheckState.Checked; radCheckBox_Sta.Enabled = false;
            Set_GropMain();
        }

        #region RowsGrid
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        //เพิ่มสินค้า
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            staSave = "1";

            radTextBox_Name.Text = ""; radTextBox_Name.Enabled = true;
            radTextBox_Unit.Text = ""; radTextBox_Unit.Enabled = true;
            radDropDownList_MainGroup.Enabled = true;
            radCheckBox_Sta.Enabled = true;
            radButton_Save.Enabled = true;
            radTextBox_Name.Focus();
        }

        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }


        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if ((radTextBox_Name.Text == "") || (radTextBox_Unit.Text == ""))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูลชื่อสินค้าหรือหน่วยสินค้า"); return;
            }

            string sta;
            if (radCheckBox_Sta.Checked == true) sta = "1"; else sta = "0";


            string sql;
            switch (staSave)
            {
                case "1": //Insert
                    string pStockID = Class.ConfigClass.GetMaxINVOICEID("STOCKITEM", "-", "0", "5");
                    sql = $@"INSERT INTO [dbo].[SHOP_STOCKITEM]
                            ([StockID],[StockName],[StockType],[StockUnit],[STA],[WHOINS],[WHONAMEINS]) 
                            VALUES ('{pStockID}','{radTextBox_Name.Text}','{radDropDownList_MainGroup.SelectedValue}',
                            '{radTextBox_Unit.Text}','{sta}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}') ";
                    break;

                case "2"://Update

                    sql = $@"
                            UPDATE  SHOP_STOCKITEM 
                            SET     StockName = '{radTextBox_Name.Text}' ,
                                    StockType = '{radDropDownList_MainGroup.SelectedValue}' ,
                                    StockUnit = '{radTextBox_Unit.Text}', 
                                    STA = '{sta}',
                                    WHOIDUPD = '{SystemClass.SystemUserID}',
                                    WHONAMEUPD = '{SystemClass.SystemUserName}',
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25)  
                            WHERE   StockID = '{radTextBox_ID.Text}' ";
                    break;

                default:
                    // break;
                    return;
            }

            string T = ConnectionClass.ExecuteSQL_Main(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                switch (staSave)
                {
                    case "1":
                        SetDGV();
                        break;
                    case "2":
                        radGridView_main.CurrentRow.Cells["StockType"].Value = radDropDownList_MainGroup.SelectedValue.ToString();
                        radGridView_main.CurrentRow.Cells["StockTypeNAME"].Value = radDropDownList_MainGroup.SelectedItem.Text;
                        radGridView_main.CurrentRow.Cells["StockName"].Value = radTextBox_Name.Text;
                        radGridView_main.CurrentRow.Cells["StockUnit"].Value = radTextBox_Unit.Text;

                        radGridView_main.CurrentRow.Cells["STA"].Value = sta;
                        dt.AcceptChanges();
                        break;
                    default:
                        return;
                }
                ClearData();
            }
        }
        //SetData For Edit
        void SetDataForEdit()
        {
            if (radGridView_main.CurrentRow.Cells["StockID"].Value.ToString() == "") return;

            Set_GropMain();

            radTextBox_ID.Text = radGridView_main.CurrentRow.Cells["StockID"].Value.ToString();
            radTextBox_Name.Text = radGridView_main.CurrentRow.Cells["StockName"].Value.ToString();
            radTextBox_Unit.Text = radGridView_main.CurrentRow.Cells["StockUnit"].Value.ToString();

            radDropDownList_MainGroup.SelectedValue = radGridView_main.CurrentRow.Cells["StockType"].Value.ToString();

            if (radGridView_main.CurrentRow.Cells["STA"].Value.ToString() == "1") radCheckBox_Sta.Checked = true;
            else radCheckBox_Sta.Checked = false;

            radButton_Save.Enabled = true;
            radDropDownList_MainGroup.Enabled = true;
            radTextBox_Unit.Enabled = true;
            radTextBox_Name.Enabled = true;
            radCheckBox_Sta.Enabled = true;

            radTextBox_Name.Focus();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            SetDataForEdit();
            staSave = "2";
            radTextBox_Name.Focus();
        }
        //
        private void RadGridView_main_SelectionChanged(object sender, EventArgs e)
        {
            if (radButton_Save.Enabled == true) ClearData();
        }
        //Name
        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radTextBox_Unit.Focus();
        }
        //Unit
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Save.Focus();
        }
        //MNII
        private void RadButtonElement_MNII_Click(object sender, EventArgs e)
        {
            if (_Pcase != "00516")
            {
                Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNII", "SUPC", "", "00014", "สต็อกห้องช่าง", "0")
                {
                    pBchID = "D079",
                    pBchName = "แผนกยานยนต์"
                };
                if (frm.ShowDialog(this) == DialogResult.OK)
                {

                }
            }
            else
            {
                Bill.BillAXAddJOB frm = new Bill.BillAXAddJOB(SystemClass.SystemDptID, SystemClass.SystemDptName, "1",
                "", "00014", "เครื่องจักรโรงงาน", "", "", "", "", "1");
                if (frm.ShowDialog(this) == DialogResult.OK)
                {
                }
            }

            SetDGV();
        }
        //Refresh
        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
