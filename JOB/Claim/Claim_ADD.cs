﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using PC_Shop24Hrs.FormShare.ShowData;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.JOB.Claim
{


    public partial class Claim_ADD : Telerik.WinControls.UI.RadForm
    {

        public string pChange;// form มีการเปลี่ยนแปลงมั้ย
        readonly PrintController printController = new StandardPrintController();
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        DataTable dtHis = new DataTable();
        string pBillNo;
        private string pCase;//0 = Enter 1 = pgDown 2 = pgUp
        readonly string pTypeBillStatus; // 0 = เคลม 1 = ทำทิ้ง

        private string pSave; //0 = save 1 = print Again

        private readonly string _pTypeBill; //0 = เคลม 1 = ทำทิ้ง 2 ซ่อม ลูกค้าคอม 3 ซ่อลูกค้ามือถือ
        private readonly string _pGroupID;
        private readonly string _pGroupDesc;

        readonly string pTypeDesc;
        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        public Claim_ADD(string pTypeBill, string pGroupID, string pGroupDesc)
        {
            InitializeComponent();
            _pTypeBill = pTypeBill;
            _pGroupID = pGroupID;
            _pGroupDesc = pGroupDesc;


            switch (_pTypeBill)
            {
                case "0":
                    this.Text = "ส่งซ่อมอุปกรณ์ ภายในบริษัท";
                    pTypeBillStatus = "ส่งซ่อม";
                    pTypeDesc = "เอกสารส่งซ่อมอุปกรณ์";
                    radLabel_BarcodeDesc.Text = "ระบุทะเบียน/SN/SPC [Enter] หรือ ระบุชื่อสินค้า [PageDown] หรือ ระบุบาร์โค้ด [PageUp]";
                    break;
                case "1":
                    this.Text = "นำทิ้งอุปกรณ์ ภายในบริษัท";
                    pTypeBillStatus = "นำทิ้ง";
                    pTypeDesc = "เอกสารนำทิ้งอุปกรณ์";
                    radLabel_BarcodeDesc.Text = "ระบุทะเบียน/SN/SPC [Enter] หรือ ระบุชื่อสินค้า [PageDown] หรือ ระบุบาร์โค้ด [PageUp]";
                    break;
                case "2":
                    this.Text = "ส่งซ่อมอุปกรณ์ ลูกค้าคอมพิวเตอร์";
                    pTypeBillStatus = "ส่งซ่อม";
                    pTypeDesc = "เอกสารส่งซ่อมอุปกรณ์ ลูกค้าคอมพิวเตอร์";
                    radLabel_BarcodeDesc.Text = "ระบุบาร์โค้ด [Enter]";
                    break;
                case "3":
                    this.Text = "ส่งซ่อมอุปกรณ์ ลูกค้ามือถือ";
                    pTypeBillStatus = "ส่งซ่อม";
                    pTypeDesc = "เอกสารส่งซ่อมอุปกรณ์ ลูกค้ามือถือ";
                    radLabel_BarcodeDesc.Text = "ระบุบาร์โค้ด [Enter]";
                    break;
                default:
                    break;
            }
        }
        //ClearData
        void ClearData()
        {
            pBillNo = "";
            radLabel_Docno.Text = "";
            pSave = "0"; pChange = "0";
            radTextBox_Item.Text = "";
            radTextBox_ItemName.Text = "";
            radTextBox_Unit.Text = "";
            radTextBox_Qty.Text = "";
            radTextBox_Vender.Text = "";

            radLabel_DateStop.Text = ""; radLabel_DateStart.Text = "";
            radLabel_location.Text = ""; radLabel_SN.Text = ""; radLabel_SN.Enabled = false;
            radLabel_VenderName.Text = "";
            radLabel_DateDiffSend.Text = "";
            radLabel_Condition.Text = "";

            radButton_Save.Enabled = false;
            radTextBox_ItemName.Enabled = false;
            radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Enabled = false;
            radTextBox_Remark.Enabled = false;
            radTextBox_Item.Enabled = true;
            radTextBox_Vender.Enabled = false;

            if (dtHis.Rows.Count > 0)
            {
                dtHis.Rows.Clear();
            }
            radGridView_Show.DataSource = dtHis;
            dtHis.AcceptChanges();

            radTextBox_Item.Focus();
        }
        //Comfirm
        void CheckComfirm()
        {
            if (pCase == "1")
            {
                if ((radTextBox_ItemName.Text == "") || (radTextBox_Qty.Text == "") || (radTextBox_Unit.Text == ""))
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ข้อมูลที่ระบุทั้งหมด");
                    radTextBox_Item.Focus();
                    return;
                }
            }
            else
            {
                if ((radTextBox_Item.Text == "") || (radTextBox_ItemName.Text == "") || (radTextBox_Qty.Text == "") || (radTextBox_Unit.Text == ""))
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ข้อมูลที่ระบุทั้งหมด");
                    radTextBox_Item.Focus();
                    return;
                }
            }

            radTextBox_Item.Enabled = false;
            radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Enabled = false;
            if ((_pTypeBill == "0") || (_pTypeBill == "2") || (_pTypeBill == "3"))
            {
                radTextBox_Vender.Enabled = true;
                radTextBox_Vender.Focus();
            }
            else
            {
                radButton_findVender.Enabled = false;
                radTextBox_Vender.Enabled = false;
                radButton_Save.Enabled = true;
                radTextBox_Remark.Enabled = true;
                radTextBox_Remark.Focus();
            }

        }
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            CheckComfirm();
        }
        //Load
        private void Claim_ADD_Load(object sender, EventArgs e)
        {

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radButton_Claer.ButtonElement.ShowBorder = true;
            radButton_Claer.ButtonElement.ToolTipText = "ลบข้อมูลทั้งหมด";
            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ToolTipText = "ยืนยันข้อมูล";
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_findVender.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_INS", "วันที่ส่งซ่อม", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_RECIVE", "วันที่รับคืน", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "อาการ", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SQ_COST", "ค่าซ่อม", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SQ_DESCRIPTION", "รายละเอียดการซ่อม", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE"));

            radGridView_Show.EnableFiltering = false;

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            DatagridClass.SetCellBackClolorByExpression("DESCRIPTION", " STA_RECIVE =  '0' ", ConfigClass.SetColor_Red(), radGridView_Show);
            //ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "STA_RECIVE =  '0' ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //this.radGridView_Show.Columns["DESCRIPTION"].ConditionalFormattingObjectList.Add(obj1);

            ClearData();

        }
        //Claer DT
        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Check Num Only
        private void RadTextBox_Qty_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //Enter Data
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุทะเบียน/SN/SPC [Enter]  
                    if ((_pTypeBill == "2") || (_pTypeBill == "3"))
                    {
                        SetEnterForCst();
                    }
                    else
                    {
                        if (radTextBox_Item.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุทะเบียน/SN/SPC");
                            radTextBox_Item.Focus();
                            return;
                        }

                        //DataTable dtAsset = AssetClass.FindAsset_ByAllTypeNotTOOL(radTextBox_Item.Text);
                        DataTable dtAsset = Models.AssetClass.FindAsset_ByAllType(radTextBox_Item.Text, "", "");
                        if (dtAsset.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("ทะเบียน/SN/SPC");
                            radTextBox_Item.Focus();
                            return;
                        }

                        radTextBox_Item.Text = dtAsset.Rows[0]["ASSETID"].ToString();
                        radTextBox_ItemName.Text = dtAsset.Rows[0]["NAME"].ToString();
                        if (dtAsset.Rows[0]["ASSETID"].ToString() == "")
                        {
                            radTextBox_Unit.Text = "ชิ้น";
                        }
                        else
                        {
                            if (dtAsset.Rows[0]["UNITOFMEASURE"].ToString() == "") { radTextBox_Unit.Text = "Ea."; }
                            else { radTextBox_Unit.Text = dtAsset.Rows[0]["UNITOFMEASURE"].ToString(); }

                        }
                        pCase = "0";
                        radTextBox_Item.Enabled = false;
                        radLabel_VenderName.Text = dtAsset.Rows[0]["VenderNAME"].ToString();
                        radTextBox_Vender.Text = dtAsset.Rows[0]["VenderID"].ToString();

                        radLabel_DateStop.Text = dtAsset.Rows[0]["PolicyExpiration"].ToString();
                        radLabel_DateStart.Text = dtAsset.Rows[0]["GuaranteeDate"].ToString();
                        radLabel_SN.Text = dtAsset.Rows[0]["SERIALNUM"].ToString();
                        radLabel_location.Text = dtAsset.Rows[0]["LOCATION"].ToString() + " " + dtAsset.Rows[0]["NAMEALIAS"].ToString();
                        radLabel_Condition.Text = dtAsset.Rows[0]["CONDITION"].ToString();
                        radTextBox_Qty.Text = "1";
                        radTextBox_Qty.Enabled = false;
                        radTextBox_Unit.Enabled = false;
                        FindHistory(dtAsset.Rows[0]["ASSETID"].ToString(), dtAsset.Rows[0]["CONDITION"].ToString());
                    }
                    break;

                case Keys.PageDown://ระบุชื่อสินค้า [PageDown] 
                    if ((_pTypeBill == "2") || (_pTypeBill == "3")) { return; }
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ชื่อสินค้า");
                        radTextBox_Item.Focus();
                        return;
                    }

                    pCase = "1";
                    radTextBox_ItemName.Text = radTextBox_Item.Text;
                    radTextBox_Item.Text = "";
                    radTextBox_Item.Enabled = false;
                    radTextBox_Unit.Text = "ชิ้น";
                    radTextBox_Unit.Enabled = true;
                    radTextBox_Qty.Enabled = true;
                    radTextBox_Qty.Focus();

                    break;

                case Keys.PageUp://ระบุบาร์โค้ด [PageUp]
                    if ((_pTypeBill == "2") || (_pTypeBill == "3")) { return; }
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }

                    Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(radTextBox_Item.Text.Trim());
                    if (!(dtBarcode.GetItembarcodeStatus))
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                        ClearData();
                        return;
                    }

                    radTextBox_Item.Text = dtBarcode.Itembarcode_ITEMBARCODE;
                    radTextBox_ItemName.Text = dtBarcode.Itembarcode_SPC_ITEMNAME;
                    if (dtBarcode.Itembarcode_UNITID == "")
                    {
                        radTextBox_Unit.Text = "ชิ้น";
                    }
                    else
                    {
                        radTextBox_Unit.Text = dtBarcode.Itembarcode_UNITID;
                    }

                    pCase = "2";
                    radTextBox_Vender.Text = dtBarcode.Itembarcode_VENDORID;
                    radLabel_VenderName.Text = dtBarcode.Itembarcode_VENDORIDNAME;
                    radTextBox_Item.Enabled = false;
                    radTextBox_Unit.Enabled = false;
                    if ((_pTypeBill == "0") || (_pTypeBill == "1"))
                    {
                        radTextBox_Qty.Enabled = true;
                        radTextBox_Qty.Focus();
                    }
                    else
                    {
                        radLabel_SN.Enabled = true;
                        radLabel_SN.Focus();
                    }

                    break;
                default:
                    break;
            }

        }
        //SetEnter For CstSend
        void SetEnterForCst()
        {
            if (radTextBox_Item.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                radTextBox_Item.Focus();
                return;
            }

            Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(radTextBox_Item.Text.Trim());
            if (!(dtBarcode.GetItembarcodeStatus))
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                ClearData();
                return;
            }

            radTextBox_Item.Text = dtBarcode.Itembarcode_ITEMBARCODE;
            radTextBox_ItemName.Text = dtBarcode.Itembarcode_SPC_ITEMNAME;
            if (dtBarcode.Itembarcode_UNITID == "")
            {
                radTextBox_Unit.Text = "ชิ้น";
            }
            else
            {
                radTextBox_Unit.Text = dtBarcode.Itembarcode_UNITID;
            }

            pCase = "2";
            radTextBox_Vender.Text = dtBarcode.Itembarcode_VENDORID;
            radLabel_VenderName.Text = dtBarcode.Itembarcode_VENDORIDNAME;
            radTextBox_Item.Enabled = false;
            radTextBox_Unit.Enabled = false;

            radLabel_SN.Enabled = true;
            radLabel_SN.Focus();

        }
        //History
        void FindHistory(string pAssID, string pCondition)
        {
            dtHis = JOB_Class.GetData_HistoryClaimByAssID(pAssID);
            if (dtHis.Rows.Count > 0)
            {
                radGridView_Show.DataSource = dtHis;
                dtHis.AcceptChanges();

                string pDateDiff = dtHis.Rows[0]["DATELASTSEND"].ToString();
                if (pDateDiff == "-1")
                {
                    radLabel_DateDiffSend.Text = "สินค้าอยู่ระหว่างส่งซ่อม";
                    radLabel_DateDiffSend.ForeColor = Color.Red;
                    radTextBox_Vender.Enabled = false;
                    return;
                }
                else
                {
                    radLabel_DateDiffSend.Text = "จำนวนวันที่กลับจากซ่อม " + pDateDiff + " วัน";
                    radLabel_DateDiffSend.ForeColor = Color.Blue;
                }
            }
            else
            {
                radLabel_DateDiffSend.Text = "ไม่มีประวัติส่งซ่อม";
                radLabel_DateDiffSend.ForeColor = Color.Black;
            }
            //Check สถานะในกรณีที่จะส่งซ่อมหรือทิ้ง
            if ((_pTypeBill == "0") || (_pTypeBill == "2") || (_pTypeBill == "3"))
            {
                switch (pCondition)
                {
                    case "ใช้งาน":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = true;
                        radTextBox_Vender.Focus();
                        break;
                    case "พร้อมใช้งาน":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = true;
                        radTextBox_Vender.Focus();
                        break;
                    case "ไม่ใช้งาน":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = true;
                        radTextBox_Vender.Focus();
                        break;
                    default:
                        radLabel_DateDiffSend.Text = "ไม่สามารถส่งซ่อมอุปกรณ์ได้ เนื่องจากสถานะไม่ถูกต้อง";
                        radLabel_DateDiffSend.ForeColor = Color.Red;
                        radTextBox_Vender.Enabled = false;
                        break;
                }
            }
            else
            {
                switch (pCondition)
                {
                    case "ใช้งาน":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "พร้อมใช้งาน":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "ไม่ใช้งาน":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "ทำอะไหล่":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "ยกเลิกบริการ":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "รอทิ้ง":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "รอเปลี่ยนสถานะ":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "ส่งซ่อม":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    case "เสีย":
                        radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                        radTextBox_Vender.Enabled = false; radButton_Save.Enabled = true; radTextBox_Remark.Enabled = true;
                        radTextBox_Remark.Focus();
                        break;
                    default:
                        radLabel_DateDiffSend.Text = "ไม่สามารถนำทิ้งอุปกรณ์ได้ เนื่องจากสถานะไม่ถูกต้อง";
                        radLabel_DateDiffSend.ForeColor = Color.Red;
                        radTextBox_Vender.Enabled = false;
                        radTextBox_Remark.Enabled = false;
                        radButton_Save.Enabled = false;
                        break;
                }
            }

        }
        //Qty Enter
        private void RadTextBox_Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (pCase == "2") { CheckComfirm(); }
                else { radTextBox_Unit.Focus(); }
            }

        }
        //Unit Enter
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CheckComfirm();
            }
        }

        //Save 
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (pSave == "0") //check ว่า save หรือ print copy
            {
                if (radTextBox_Remark.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการส่งซ่อม");
                    return;
                }

                string T = SaveData();
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    pSave = "1";
                    pChange = "1";
                    //ClearEnable();
                    radLabel_Docno.Text = pBillNo;
                    radTextBox_Remark.Enabled = false;
                    PrintData();
                }
                else { return; }
            }
            else
            {
                PrintData();
                return;
            }
        }
        //Save
        string SaveData()
        {

            pBillNo = Class.ConfigClass.GetMaxINVOICEID("MNCM", "-", "MNCM", "1");
            string tAX = "";

            if (pCase == "0") AX_SendData.Save_EXTERNALLIST_AssetCodition(radTextBox_Item.Text, pTypeBillStatus);
            //{
            //    tAX = JOBClass.SendAX_AssetCodition(radTextBox_Item.Text, pTypeBillStatus);
            //}

            if (tAX != "") return tAX;
           
            string descJOB;
            descJOB = "- " + radTextBox_Remark.Text.Trim().Replace("'", "");
            descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss") + "]";


            string sqlClaim = $@"INSERT INTO [dbo].[SHOP_JOBCLAIM]  
                            (Claim_ID,JOBGROUP,JOBGROUP_DESCRIPTION,STA_TYPE,ASSETID,Name,SN,LOCATION_DESC,
                            VENDER_ID,VENDER_NAME,DATE_START,DATE_STOP,DESCRIPTION,WHOIDINS,WHONAMEINS,QTY, UNIT) 
                            VALUES ('{pBillNo}','{ _pGroupID}','{_pGroupDesc}','{ _pTypeBill }','{radTextBox_Item.Text}',
                            '{ radTextBox_ItemName.Text}','{ radLabel_SN.Text}','{ radLabel_location.Text}',
                            '{ radTextBox_Vender.Text}','{ radLabel_VenderName.Text}','{radLabel_DateStart.Text}',
                            '{ radLabel_DateStop.Text}',
                            '{descJOB}','{SystemClass.SystemUserID}','{ SystemClass.SystemUserName }',
                            '{ Convert.ToDecimal(radTextBox_Qty.Text)}','{ radTextBox_Unit.Text}')";

            string sqlLog = $@"INSERT INTO SHOP_ASSETTABLELOG 
                (ASSETID,NAME,CONDITION_OLD,CONDITION_NEW,
                UPDATE_WHOID,UPDATE_WHONAME ) values ( 
             '{ radTextBox_Item.Text}','{ radTextBox_ItemName.Text}',
             '{radLabel_Condition.Text}','{ pTypeBillStatus }',
             '{SystemClass.SystemUserID }','{ SystemClass.SystemUserName}')";

            if (pCase == "0")
            {
                return ConnectionClass.ExecuteSQL2Str_Main(sqlClaim, sqlLog);
            }
            else
            {
                return ConnectionClass.ExecuteSQL_Main(sqlClaim);
            }
        }
        //print
        void PrintData()
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                printDocument1.PrintController = printController;
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                printDocument1.Print();
                if (_pTypeBill != "1")
                {
                    printDocument1.Print();
                }
            }

        }
        //print
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = pBillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;



            e.Graphics.DrawString(pTypeDesc, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับผิดชอบ - " + _pGroupDesc, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);


            Y += 20;
            e.Graphics.DrawString("(" + radTextBox_Qty.Text + " X " +
                              radTextBox_Unit.Text + ")   " +
                              radTextBox_Item.Text,
                 SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString(radTextBox_ItemName.Text,
                SystemClass.printFont, Brushes.Black, 10, Y);
            if (pCase == "0")
            {
                Y += 15;
                e.Graphics.DrawString("S/N " + radLabel_SN.Text,
                    SystemClass.printFont, Brushes.Black, 10, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            if ((_pTypeBill == "0") || (_pTypeBill == "2") || (_pTypeBill == "3"))
            {
                Y += 20;
                e.Graphics.DrawString("ส่งซ่อมบริษัท " + radTextBox_Vender.Text + " " + radLabel_VenderName.Text, SystemClass.printFont, Brushes.Black, 10, Y);
            }
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            if ((pCase == "0") && (_pTypeBill == "0"))
            {
                Y += 20;
                e.Graphics.DrawString(" ********** ส่งซ่อมครั้งที่ " + Convert.ToString(radGridView_Show.Rows.Count + 1) + " ********** ", SystemClass.printFont, Brushes.Black, 0, Y);
            }
            Y += 30;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            StringFormat stringFormat = new StringFormat
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(radTextBox_Remark.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            e.Graphics.PageUnit = GraphicsUnit.Inch;

        }
        //find Vender
        void FindVender()
        {
            DataTable dtVender = ItembarcodeClass.GetVender_ByVenderID(radTextBox_Vender.Text);
            if (dtVender.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลผู้จำหน่าย");
                radTextBox_Vender.Text = "V";
                radTextBox_Vender.Focus();
                radTextBox_Vender.SelectionStart = radTextBox_Vender.Text.Length;
                return;
            }

            radTextBox_Vender.Text = dtVender.Rows[0]["ACCOUNTNUM"].ToString();
            radLabel_VenderName.Text = dtVender.Rows[0]["NAME"].ToString();
            radTextBox_Vender.Enabled = false;
            radTextBox_Remark.Enabled = true;
            radTextBox_Remark.Focus(); radButton_Save.Enabled = true;
        }
        //Chek Vender
        private void RadTextBox_Vender_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_Vender.Text.ToString().Length == 7)
            {
            }
            else
            {
                radLabel_VenderName.Text = "";
            }
        }
        // Enter
        private void RadTextBox_Vender_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FindVender();
            }
        }
        //find vender
        private void RadButton_findVender_Click(object sender, EventArgs e)
        {
            DataTable dt = Models.VendTableClass.GetVENDTABLE_All("", ""); //JOBClass.GetVender();
            ShowDataDGV frm = new ShowDataDGV()
            { dtData = dt };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radTextBox_Vender.Text = frm.pID;
                radLabel_VenderName.Text = frm.pDesc;
                radTextBox_Vender.Focus();
            }
            else
            {
                radTextBox_Vender.SelectAll();
                radTextBox_Vender.Focus();
            }
        }
        //Sn Enter
        private void RadLabel_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radLabel_SN.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูล SN ของสินค้า");
                    return;
                }
                radLabel_SN.Enabled = false;
                radTextBox_Qty.Enabled = true;
                radTextBox_Qty.Focus();
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, pTypeBillStatus);
        }
    }

}

