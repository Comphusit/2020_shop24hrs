﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.Claim
{
    public partial class Claim_DescPrint : Telerik.WinControls.UI.RadForm
    {
        readonly string _pPXE;//0 ไม่มีค่าใช้จ่าย 1 มีค่าใช้จ่าย
        string _pxeDesc;
        readonly string _pPathSPC;
        readonly string _pPathMN;
        readonly string _pPathEND;
        readonly string _pDocno;
        readonly string _pVender;

        public Claim_DescPrint(string pDocno, string pPXE, string pxeDesc, string pVender, string pPathSPC, string pPathMN, string pPathEND)
        {
            _pPathSPC = pPathSPC;
            _pPathMN = pPathMN;
            _pPathEND = pPathEND;
            _pDocno = pDocno;
            _pPXE = pPXE;
            _pVender = pVender;
            _pxeDesc = pxeDesc;

            InitializeComponent();
        }
        //โหลด
        private void Claim_DescPrint_Load(object sender, EventArgs e)
        {
            radButtonElement_Pxe.ShowBorder = true; radButtonElement_Pxe.ToolTipText = "ระบุ PXE";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร";
            radStatusStrip_Main.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("pPathSPC", "รูป รปภ.ถ่ายขาออก ", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("pPathMN", "รูป CN ถ่ายขาส่ง", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("pPathEND", "รูป CN ถ่ายขารับ", 250));

            radGridView_Show.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 320;
            radGridView_Show.TableElement.TableHeaderHeight = 100;


            switch (_pPXE)
            {
                case "0": //ไม่มีค่าใช้จ่าย
                    radButtonElement_Pxe.Enabled = false;
                    radButtonElement_Print.Enabled = true;
                    SetDGV();
                    break;
                case "1"://มีค่าใช้จ่ายแล้วยังไม่มี PXE
                    radButtonElement_Pxe.Enabled = true;
                    radButtonElement_Print.Enabled = false;
                    break;
                default: //มี PXE แล้ว
                    radButtonElement_Pxe.Enabled = false;
                    radButtonElement_Print.Enabled = true;
                    SetDGV();
                    break;
            }
        }
        //SetData
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;

            string[] strSPC = _pPathSPC.Split('|');
            DirectoryInfo DirInfoSPC = new DirectoryInfo(strSPC[0]);
            FileInfo[] FilesSPC = DirInfoSPC.GetFiles(strSPC[1], SearchOption.AllDirectories);

            string[] strMN = _pPathMN.Split('|');
            DirectoryInfo DirInfoMN = new DirectoryInfo(strMN[0]);
            FileInfo[] FilesMN = DirInfoMN.GetFiles(strMN[1], SearchOption.AllDirectories);

            string[] strEND = _pPathEND.Split('|');
            DirectoryInfo DirInfoEND = new DirectoryInfo(strEND[0]);
            FileInfo[] FilesEND = DirInfoEND.GetFiles(strEND[1], SearchOption.AllDirectories);

            radGridView_Show.Rows.Add(Image.FromFile(FilesSPC[0].FullName), Image.FromFile(FilesMN[0].FullName), Image.FromFile(FilesEND[0].FullName));

            radGridView_Show.Rows[0].Cells["pPathSPC"].ColumnInfo.HeaderText = _pxeDesc + Environment.NewLine + _pDocno +
                Environment.NewLine + "รูป รปภ.ถ่ายขาออก " +
                Environment.NewLine + File.GetCreationTime(FilesSPC[0].FullName).ToString("dd - MM - yyyy HH: mm: ss");

            radGridView_Show.Rows[0].Cells["pPathMN"].ColumnInfo.HeaderText = _pxeDesc + Environment.NewLine + _pDocno +
               Environment.NewLine + "รูป CN ถ่ายขาส่ง" +
               Environment.NewLine + File.GetCreationTime(FilesMN[0].FullName).ToString("dd - MM - yyyy HH: mm: ss");

            radGridView_Show.Rows[0].Cells["pPathEND"].ColumnInfo.HeaderText = _pxeDesc + Environment.NewLine + _pDocno +
               Environment.NewLine + "รูป CN ถ่ายขารับ" +
               Environment.NewLine + File.GetCreationTime(FilesEND[0].FullName).ToString("dd - MM - yyyy HH: mm: ss");

            this.Cursor = Cursors.Default;
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion
        //print
        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            this.radGridView_Show.PrintPreview();
        }
        //PXE
        private void RadButtonElement_Pxe_Click(object sender, EventArgs e)
        {
            FormShare.InputData _inputdata = new FormShare.InputData("1", _pDocno, "ระบุ PXE", "");
            if (_inputdata.ShowDialog(this) == DialogResult.Yes)
            {
                if (_inputdata.pInputData == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ต้องระบุ PXE ให้เรียบร้อยก่อนการบันทึก");
                    return;
                }

                DataTable dt = PurchClass.FindData_PurchTable(_inputdata.pInputData);
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ PXE"); return;
                }

                if (dt.Rows[0]["OrderAccount"].ToString() != _pVender)
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ข้อมูล PXE ที่ระบุ มีรหัสผู้จำหน่ายที่อ้างอิงในใบส่งซ่อมไม่ตรงกัน " + Environment.NewLine +
                        "ยืนยันการบันมึกข้อมูล ?") == DialogResult.No) return;
                }

                string sqlUp = $@"
                            UPDATE  [SHOP_JOBCLAIM] 
                            SET     PXE_ID = '{dt.Rows[0]["PurchId"]}',
                                    PXE_DocumentNum = '{dt.Rows[0]["DOCUMENTNUM"]}'  
                            WHERE   [Claim_ID] = '{ _pDocno }' ";
                string T = ConnectionClass.ExecuteSQL_Main(sqlUp);

                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    _pxeDesc = dt.Rows[0]["PurchId"].ToString();
                    radButtonElement_Print.Enabled = true;
                    radButtonElement_Pxe.Enabled = false;
                    SetDGV();
                }
                return;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pPXE);
        }
    }
}
