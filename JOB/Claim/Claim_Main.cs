﻿//CheckOK
using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.Claim
{
    public partial class Claim_Main : RadForm
    {
        readonly string _pType;//0 เคลม 1 ทำทิ้ง 2 ลูกค้าคอม 3 ลูกค้ามือถือ
        readonly string _pGROUPID;
        readonly string _pGROUP_DESC;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปืดจ๊อบ //1 มีสิทธิ์ปิด

        string pTypeDesc;
        DateTime pAutoRefresh;
        //Main Load
        public Claim_Main(string pType, string pGROUPID, string pGROUP_DESC, string pPermission) // SHOP-SUPC
        {
            InitializeComponent();

            _pGROUPID = pGROUPID;
            _pGROUP_DESC = pGROUP_DESC;
            _pPerminssion = pPermission;
            _pType = pType;

        }
        //load
        private void Claim_Main_Load(object sender, EventArgs e)
        {
            pAutoRefresh = DateTime.Now.AddMinutes(3);
            timer_Auto.Start();

            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radButtonElement_add.ShowBorder = true;
            radRadioButtonElement_Open.ShowBorder = true;
            radRadioButtonElement_All.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;
            radButtonElement_Excel.ShowBorder = true;
            radButtonElement_Excel.ToolTipText = "Export Excel";
            radButtonElement_Refresh.ShowBorder = true; radButtonElement_Refresh.ToolTipText = "ดึงข้อมูลใหม่";
            switch (_pType)
            {
                case "0":
                    radButtonElement_add.ToolTipText = "เพิ่มอุปกรณ์ส่งซ่อม";
                    radRadioButtonElement_Open.ToolTipText = "อุปกรณ์ส่งซ่อม ที่ยังไม่ได้รับคืน";
                    radRadioButtonElement_All.ToolTipText = "อุปกรณ์ส่งซ่อม ทั้งหมด";
                    radButtonElement_Edit.ToolTipText = "ข้อมูลอุปกรณ์ส่งซ่อม";
                    radLabel_F2.Text = "สีฟ้า>>ยังไม่ส่ง CN | สีเขียว >> CN ยังไม่ส่งผู้จำหน่าย | สีเขียวอ่อน >> ของอยู่ที่ผู้จำหน่าย | สีชมพู >> ของอยู่ที่ CN | Double Click >> ดูรายละเอียดบิล";
                    pTypeDesc = "อุปกรณ์ส่งเคลม";
                    break;
                case "1":
                    radButtonElement_add.ToolTipText = "เพิ่มอุปกรณ์นำทิ้ง";
                    radRadioButtonElement_Open.ToolTipText = "อุปกรณ์นำทิ้ง ที่ยังไม่ได้รับคืน";
                    radRadioButtonElement_All.ToolTipText = "อุปกรณ์นำทิ้ง ทั้งหมด";
                    radButtonElement_Edit.ToolTipText = "ข้อมูลอุปกรณ์นำทิ้ง";
                    radLabel_F2.Text = "Double Click >> ดูรายละเอียดบิล";
                    pTypeDesc = "อุปกรณ์นำทิ้ง";
                    break;
                case "2":
                    radButtonElement_add.ToolTipText = "เพิ่มอุปกรณ์ส่งซ่อม [ลูกค้าคอมพิวเตอร์]";
                    radRadioButtonElement_Open.ToolTipText = "อุปกรณ์ส่งซ่อม ที่ยังไม่ได้รับคืน";
                    radRadioButtonElement_All.ToolTipText = "อุปกรณ์ส่งซ่อม ทั้งหมด";
                    radButtonElement_Edit.ToolTipText = "ข้อมูลอุปกรณ์ส่งซ่อม";
                    radLabel_F2.Text = "สีฟ้า>>ยังไม่ส่ง CN | สีเขียว >> CN ยังไม่ส่งผู้จำหน่าย | สีเขียวอ่อน >> ของอยู่ที่ผู้จำหน่าย | สีชมพู >> ของอยู่ที่ CN | Double Click >> ดูรายละเอียดบิล";
                    pTypeDesc = "อุปกรณ์ส่งเคลม [ลูกค้าคอมพิวเตอร์]";
                    break;
                case "3":
                    radButtonElement_add.ToolTipText = "เพิ่มอุปกรณ์ส่งซ่อม [ลูกค้ามือถือ]";
                    radRadioButtonElement_Open.ToolTipText = "อุปกรณ์ส่งซ่อม ที่ยังไม่ได้รับคืน";
                    radRadioButtonElement_All.ToolTipText = "อุปกรณ์ส่งซ่อม ทั้งหมด";
                    radButtonElement_Edit.ToolTipText = "ข้อมูลอุปกรณ์ส่งซ่อม";
                    radLabel_F2.Text = "สีฟ้า>>ยังไม่ส่ง CN | สีเขียว >> CN ยังไม่ส่งผู้จำหน่าย | สีเขียวอ่อน >> ของอยู่ที่ผู้จำหน่าย | สีชมพู >> ของอยู่ที่ CN | Double Click >> ดูรายละเอียดบิล";
                    pTypeDesc = "อุปกรณ์ส่งเคลม [ลูกค้ามือถือ]";
                    break;
                default:
                    break;
            }



            radStatusStrip_Main.SizingGrip = false;
            if (_pPerminssion == "0")
            {
                radButtonElement_add.Enabled = false;
            }

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATE", "D", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUP_DESCRIPTION", "เปิดโดย", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Claim_ID", "เลขที่บิล", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ASSETID", "ทะเบียน", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อ", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SN", "S/N", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "อาการเสีย", 350));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_INS", "วันที่บิล", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_SENDCN", "ส่ง CN", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VENDER_ID", "รหัส", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VENDER_NAME", "บริษัท", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATION_DESC", "ที่ตั้ง", 200));

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_SEND", "สถานะส่งของ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_RECIVECN", "สถานะ CN รับของ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("RECIVE_STA", "แผนกรับของ"));

            //Freeze Column
            for (int i = 0; i < 3; i++)
            {
                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;
            }

            DatagridClass.SetCellBackClolorByExpression("Claim_ID", " DATE_SENDCN = '1900-01-01' ", ConfigClass.SetColor_SkyPastel(), radGridView_Show);
            //ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition2", "DATE_SENDCN = '1900-01-01' ", false)
            //{ CellBackColor = ConfigClass.SetColor_SkyPastel() };
            //this.radGridView_Show.Columns["Claim_ID"].ConditionalFormattingObjectList.Add(obj4);

            DatagridClass.SetCellBackClolorByExpression("Claim_ID", " DATE_SENDCN <> '1900-01-01' ", ConfigClass.SetColor_Green(), radGridView_Show);
            //ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition2", "  DATE_SENDCN <> '1900-01-01' ", false)
            //{ CellBackColor = ConfigClass.SetColor_Green() };
            //this.radGridView_Show.Columns["Claim_ID"].ConditionalFormattingObjectList.Add(obj1);

            DatagridClass.SetCellBackClolorByExpression("Claim_ID", " STA_SEND = '1' ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition2", "STA_SEND = '1' ", false)
            //{ CellBackColor = ConfigClass.SetColor_GreenPastel() };
            //this.radGridView_Show.Columns["Claim_ID"].ConditionalFormattingObjectList.Add(obj3);

            DatagridClass.SetCellBackClolorByExpression("Claim_ID", " STA_RECIVECN = '1' ", ConfigClass.SetColor_PinkPastel(), radGridView_Show);
            //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "STA_RECIVECN = '1' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PinkPastel() };
            //this.radGridView_Show.Columns["Claim_ID"].ConditionalFormattingObjectList.Add(obj2);

            DatagridClass.SetCellBackClolorByExpression("Claim_ID", " RECIVE_STA = '1' ", Color.Transparent, radGridView_Show);
            //ExpressionFormattingObject obj5 = new ExpressionFormattingObject("MyCondition2", "RECIVE_STA = '1' ", false)
            //{ CellBackColor = Color.Transparent };
            //this.radGridView_Show.Columns["Claim_ID"].ConditionalFormattingObjectList.Add(obj5);

            radGridView_Show.TableElement.SearchHighlightColor = Color.LightBlue;

            SetGDV();

        }

        //find Data
        void SetGDV()
        {
            string sta_JobAll = "   '0' ";
            if (radRadioButtonElement_Open.CheckState == CheckState.Unchecked) { sta_JobAll = "   '0','1' "; }

            string sql = $@" SELECT	JOBGROUP_DESCRIPTION,Claim_ID,CONVERT(VARCHAR,DATE_INS,23) AS DATE_INS,ASSETID,Name,SN
                        ,QTY,UNIT,LOCATION_DESC,VENDER_ID,VENDER_NAME
                        , WHOIDINS, WHONAMEINS, DESCRIPTION
                        , DateDiff(Day, DATE_INS, GETDATE()) AS SUMDATE, RECIVE_STA, STA_RECIVECN,  CONVERT(VARCHAR,ISNULL(DATE_SENDCN, '1900-01-01'),23)  AS DATE_SENDCN, STA_SEND 
                FROM SHOP_JOBCLAIM WITH(NOLOCK)
                WHERE JOBGROUP = '{_pGROUPID}' AND STA_TYPE = '{_pType}' AND STADOC = '1' 
                AND CASE WHEN RECIVE_STA = '1' THEN '1' ELSE CASE WHEN Bill_SpcType = '1' THEN '1' ELSE '0' END	END IN ({sta_JobAll}) 
                ORDER BY Claim_ID DESC  ";

            DataTable Dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            radGridView_Show.DataSource = Dt;
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            Claim_ADD frm = new Claim_ADD(_pType, _pGROUPID, _pGROUP_DESC);
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                if (frm.pChange == "1")
                {
                    SetGDV();
                }
            }
            else
            {
                if (frm.pChange == "1")
                {
                    SetGDV();
                }
            }
        }

        #region "ROWS"
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //ROWS NUMBERS
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        #endregion

        //Open Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            OpenFormEdit();
        }
        //Open From
        void OpenFormEdit()
        {
            if ((radGridView_Show.CurrentRow.Cells["Claim_ID"].Value.ToString() == "") || (radGridView_Show.CurrentRow.Index == -1))
            {
                return;
            }

            Claim_EDIT frm = new Claim_EDIT(_pType,
                radGridView_Show.CurrentRow.Cells["Claim_ID"].Value.ToString(), _pPerminssion,
                radGridView_Show.CurrentRow.Cells["JOBGROUP_DESCRIPTION"].Value.ToString());

            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                if (frm.pChange == "1")
                {
                    SetGDV();
                }
            }
            else
            {
                if (frm.pChange == "1")
                {
                    SetGDV();
                }
            }
        }
        //Auto Refresh
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > pAutoRefresh)
            {
                timer_Auto.Stop();
                SetGDV();
                pAutoRefresh = DateTime.Now.AddMinutes(3);
                timer_Auto.Start();
            }
        }
        //เฉพาะรายการค้างปิด
        private void RadRadioButtonElement_Open_CheckStateChanged_1(object sender, EventArgs e)
        {
            SetGDV();
        }
        //รายการทั้งหมด
        private void RadRadioButtonElement_All_CheckStateChanged_1(object sender, EventArgs e)
        {
            SetGDV();
        }
        //Open
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            OpenFormEdit();
        }
        //Export
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView(pTypeDesc, radGridView_Show,"1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            SetGDV();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pType);
        }
    }
}



