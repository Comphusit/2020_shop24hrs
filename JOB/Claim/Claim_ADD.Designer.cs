﻿namespace PC_Shop24Hrs.JOB.Claim 
{
    partial class Claim_ADD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Claim_ADD));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel_SN = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Condition = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DateDiffSend = new Telerik.WinControls.UI.RadLabel();
            this.radButton_findVender = new Telerik.WinControls.UI.RadButton();
            this.radLabel_location = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DateStop = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DateStart = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_VenderName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Vender = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Claer = new Telerik.WinControls.UI.RadButton();
            this.radButton_Add = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Unit = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Qty = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_ItemName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BarcodeDesc = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Item = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Condition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateDiffSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_findVender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_VenderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Claer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BarcodeDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Controls.Add(this.RadButton_pdt);
            this.radGroupBox_DB.Controls.Add(this.radLabel_SN);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Docno);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Condition);
            this.radGroupBox_DB.Controls.Add(this.radLabel9);
            this.radGroupBox_DB.Controls.Add(this.radLabel_DateDiffSend);
            this.radGroupBox_DB.Controls.Add(this.radButton_findVender);
            this.radGroupBox_DB.Controls.Add(this.radLabel_location);
            this.radGroupBox_DB.Controls.Add(this.radLabel12);
            this.radGroupBox_DB.Controls.Add(this.radLabel_DateStop);
            this.radGroupBox_DB.Controls.Add(this.radLabel_DateStart);
            this.radGroupBox_DB.Controls.Add(this.radLabel8);
            this.radGroupBox_DB.Controls.Add(this.radLabel7);
            this.radGroupBox_DB.Controls.Add(this.radLabel_VenderName);
            this.radGroupBox_DB.Controls.Add(this.radLabel5);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Vender);
            this.radGroupBox_DB.Controls.Add(this.radButton_Claer);
            this.radGroupBox_DB.Controls.Add(this.radButton_Add);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Remark);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Unit);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Qty);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_ItemName);
            this.radGroupBox_DB.Controls.Add(this.radLabel_BarcodeDesc);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Item);
            this.radGroupBox_DB.Controls.Add(this.radLabel4);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radGridView_Show);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Controls.Add(this.radLabel6);
            this.radGroupBox_DB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(740, 603);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // radLabel_SN
            // 
            this.radLabel_SN.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_SN.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_SN.Location = new System.Drawing.Point(460, 94);
            this.radLabel_SN.Name = "radLabel_SN";
            this.radLabel_SN.Size = new System.Drawing.Size(255, 25);
            this.radLabel_SN.TabIndex = 64;
            this.radLabel_SN.Tag = "";
            this.radLabel_SN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadLabel_SN_KeyDown);
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(25, 20);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(671, 16);
            this.radLabel_Docno.TabIndex = 63;
            this.radLabel_Docno.Text = "MNRO200401000000";
            // 
            // radLabel_Condition
            // 
            this.radLabel_Condition.AutoSize = false;
            this.radLabel_Condition.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Condition.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Condition.Location = new System.Drawing.Point(459, 158);
            this.radLabel_Condition.Name = "radLabel_Condition";
            this.radLabel_Condition.Size = new System.Drawing.Size(256, 23);
            this.radLabel_Condition.TabIndex = 58;
            this.radLabel_Condition.Text = "-";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(408, 161);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(174, 19);
            this.radLabel9.TabIndex = 62;
            this.radLabel9.Text = "สภาพ";
            // 
            // radLabel_DateDiffSend
            // 
            this.radLabel_DateDiffSend.AutoSize = false;
            this.radLabel_DateDiffSend.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DateDiffSend.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DateDiffSend.Location = new System.Drawing.Point(304, 261);
            this.radLabel_DateDiffSend.Name = "radLabel_DateDiffSend";
            this.radLabel_DateDiffSend.Size = new System.Drawing.Size(411, 19);
            this.radLabel_DateDiffSend.TabIndex = 61;
            this.radLabel_DateDiffSend.Text = "จำนวนวันที่กลับจากซ่อม 110 วัน";
            this.radLabel_DateDiffSend.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radButton_findVender
            // 
            this.radButton_findVender.BackColor = System.Drawing.Color.Transparent;
            this.radButton_findVender.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_findVender.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.radButton_findVender.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_findVender.Location = new System.Drawing.Point(26, 234);
            this.radButton_findVender.Name = "radButton_findVender";
            // 
            // 
            // 
            this.radButton_findVender.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_findVender.Size = new System.Drawing.Size(26, 26);
            this.radButton_findVender.TabIndex = 60;
            this.radButton_findVender.Click += new System.EventHandler(this.RadButton_findVender_Click);
            // 
            // radLabel_location
            // 
            this.radLabel_location.AutoSize = false;
            this.radLabel_location.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_location.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_location.Location = new System.Drawing.Point(459, 121);
            this.radLabel_location.Name = "radLabel_location";
            this.radLabel_location.Size = new System.Drawing.Size(269, 23);
            this.radLabel_location.TabIndex = 59;
            this.radLabel_location.Text = "-";
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel12.Location = new System.Drawing.Point(408, 124);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(140, 19);
            this.radLabel12.TabIndex = 58;
            this.radLabel12.Text = "ที่ตั้ง";
            // 
            // radLabel_DateStop
            // 
            this.radLabel_DateStop.AutoSize = false;
            this.radLabel_DateStop.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_DateStop.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DateStop.Location = new System.Drawing.Point(199, 123);
            this.radLabel_DateStop.Name = "radLabel_DateStop";
            this.radLabel_DateStop.Size = new System.Drawing.Size(165, 23);
            this.radLabel_DateStop.TabIndex = 57;
            this.radLabel_DateStop.Text = "-";
            // 
            // radLabel_DateStart
            // 
            this.radLabel_DateStart.AutoSize = false;
            this.radLabel_DateStart.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_DateStart.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DateStart.Location = new System.Drawing.Point(199, 96);
            this.radLabel_DateStart.Name = "radLabel_DateStart";
            this.radLabel_DateStart.Size = new System.Drawing.Size(203, 23);
            this.radLabel_DateStart.TabIndex = 55;
            this.radLabel_DateStart.Text = "-";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(57, 125);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(174, 19);
            this.radLabel8.TabIndex = 54;
            this.radLabel8.Text = "วันที่หมดประกันสินค้า";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(57, 98);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(146, 19);
            this.radLabel7.TabIndex = 53;
            this.radLabel7.Text = "วันที่รับประกันสินค้า";
            // 
            // radLabel_VenderName
            // 
            this.radLabel_VenderName.AutoSize = false;
            this.radLabel_VenderName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_VenderName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_VenderName.Location = new System.Drawing.Point(151, 235);
            this.radLabel_VenderName.Name = "radLabel_VenderName";
            this.radLabel_VenderName.Size = new System.Drawing.Size(564, 23);
            this.radLabel_VenderName.TabIndex = 51;
            this.radLabel_VenderName.Text = "-";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(25, 208);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(158, 19);
            this.radLabel5.TabIndex = 50;
            this.radLabel5.Text = "ผู้จำหน่าย [Enter]";
            // 
            // radTextBox_Vender
            // 
            this.radTextBox_Vender.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Vender.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Vender.Location = new System.Drawing.Point(57, 233);
            this.radTextBox_Vender.MaxLength = 7;
            this.radTextBox_Vender.Name = "radTextBox_Vender";
            this.radTextBox_Vender.Size = new System.Drawing.Size(88, 25);
            this.radTextBox_Vender.TabIndex = 49;
            this.radTextBox_Vender.Text = "V005450";
            this.radTextBox_Vender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Vender_KeyDown);
            this.radTextBox_Vender.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Vender_KeyUp);
            // 
            // radButton_Claer
            // 
            this.radButton_Claer.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Claer.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Claer.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Claer.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Claer.Location = new System.Drawing.Point(25, 64);
            this.radButton_Claer.Name = "radButton_Claer";
            this.radButton_Claer.Size = new System.Drawing.Size(26, 26);
            this.radButton_Claer.TabIndex = 48;
            this.radButton_Claer.Text = "radButton1";
            this.radButton_Claer.Click += new System.EventHandler(this.RadButton_Claer_Click);
            // 
            // radButton_Add
            // 
            this.radButton_Add.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Add.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Add.Location = new System.Drawing.Point(290, 179);
            this.radButton_Add.Name = "radButton_Add";
            // 
            // 
            // 
            this.radButton_Add.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_Add.Size = new System.Drawing.Size(26, 26);
            this.radButton_Add.TabIndex = 3;
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(362, 554);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(264, 554);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(25, 479);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(690, 63);
            this.radTextBox_Remark.TabIndex = 4;
            // 
            // radTextBox_Unit
            // 
            this.radTextBox_Unit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Unit.Location = new System.Drawing.Point(151, 179);
            this.radTextBox_Unit.Name = "radTextBox_Unit";
            this.radTextBox_Unit.Size = new System.Drawing.Size(133, 25);
            this.radTextBox_Unit.TabIndex = 2;
            this.radTextBox_Unit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Unit_KeyDown);
            // 
            // radTextBox_Qty
            // 
            this.radTextBox_Qty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Qty.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Qty.Location = new System.Drawing.Point(25, 179);
            this.radTextBox_Qty.Name = "radTextBox_Qty";
            this.radTextBox_Qty.Size = new System.Drawing.Size(120, 25);
            this.radTextBox_Qty.TabIndex = 1;
            this.radTextBox_Qty.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Qty_TextChanging);
            this.radTextBox_Qty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Qty_KeyDown);
            // 
            // radTextBox_ItemName
            // 
            this.radTextBox_ItemName.AutoSize = false;
            this.radTextBox_ItemName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_ItemName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ItemName.Location = new System.Drawing.Point(237, 68);
            this.radTextBox_ItemName.Name = "radTextBox_ItemName";
            this.radTextBox_ItemName.Size = new System.Drawing.Size(478, 23);
            this.radTextBox_ItemName.TabIndex = 36;
            this.radTextBox_ItemName.Text = "-";
            // 
            // radLabel_BarcodeDesc
            // 
            this.radLabel_BarcodeDesc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BarcodeDesc.Location = new System.Drawing.Point(25, 39);
            this.radLabel_BarcodeDesc.Name = "radLabel_BarcodeDesc";
            this.radLabel_BarcodeDesc.Size = new System.Drawing.Size(557, 19);
            this.radLabel_BarcodeDesc.TabIndex = 25;
            this.radLabel_BarcodeDesc.Text = "ระบุทะเบียน/SN/SPC [Enter] หรือ ระบุชื่อสินค้า [PageDown] หรือ ระบุบาร์โค้ด [Page" +
    "Up]";
            // 
            // radTextBox_Item
            // 
            this.radTextBox_Item.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Item.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Item.Location = new System.Drawing.Point(57, 66);
            this.radTextBox_Item.Name = "radTextBox_Item";
            this.radTextBox_Item.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Item.TabIndex = 0;
            this.radTextBox_Item.Tag = "";
            this.radTextBox_Item.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Item_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(160, 157);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(76, 19);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "หน่วย";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(25, 157);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(76, 19);
            this.radLabel3.TabIndex = 43;
            this.radLabel3.Text = "จำนวน";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(25, 282);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(690, 173);
            this.radGridView_Show.TabIndex = 40;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(25, 458);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(158, 19);
            this.radLabel2.TabIndex = 41;
            this.radLabel2.Text = "รายละเอียดการส่งซ่อม";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(25, 261);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(158, 19);
            this.radLabel1.TabIndex = 39;
            this.radLabel1.Text = "ประวัติการส่งซ่อม";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(408, 96);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(46, 19);
            this.radLabel6.TabIndex = 52;
            this.radLabel6.Text = "SN";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(704, 17);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 74;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // Claim_ADD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(740, 603);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Claim_ADD";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ส่งซ่อมอุปกรณ์.";
            this.Load += new System.EventHandler(this.Claim_ADD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Condition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateDiffSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_findVender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_VenderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Claer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BarcodeDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel_BarcodeDesc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Item;
        private Telerik.WinControls.UI.RadLabel radTextBox_ItemName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Unit;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Qty;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadButton radButton_Add;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton radButton_Claer;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Vender;
        private Telerik.WinControls.UI.RadLabel radLabel_VenderName;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel_DateStart;
        private Telerik.WinControls.UI.RadLabel radLabel_DateStop;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel_location;
        private Telerik.WinControls.UI.RadButton radButton_findVender;
        private Telerik.WinControls.UI.RadLabel radLabel_DateDiffSend;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel_Condition;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadTextBox radLabel_SN;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
    }
}
