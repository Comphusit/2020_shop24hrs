﻿namespace PC_Shop24Hrs.JOB.Claim 
{
    partial class Claim_EDIT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Claim_EDIT));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.radButton_printImg = new Telerik.WinControls.UI.RadButton();
            this.radLabel_SN = new Telerik.WinControls.UI.RadLabel();
            this.radButton_SendCN = new Telerik.WinControls.UI.RadButton();
            this.checkBox_ReciveDpt = new System.Windows.Forms.CheckBox();
            this.groupBox_SQ = new System.Windows.Forms.GroupBox();
            this.radButton_SaveSQ = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_SqDesc = new Telerik.WinControls.UI.RadTextBox();
            this.radioButton_NoSq = new System.Windows.Forms.RadioButton();
            this.radioButton_Sq = new System.Windows.Forms.RadioButton();
            this.radTextBox_SqpriceNoVat = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_SqNo = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox_CN = new System.Windows.Forms.GroupBox();
            this.radButton_CNPrint = new Telerik.WinControls.UI.RadButton();
            this.radButton_CnSaveSend = new Telerik.WinControls.UI.RadButton();
            this.radButton_CNSaveRecive = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_CNReciveDesc = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CnSendRmk = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CNSendBill = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CNSendBy = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CNWhoSend = new Telerik.WinControls.UI.RadTextBox();
            this.checkBox_CnRecive = new System.Windows.Forms.CheckBox();
            this.checkBox_CNSend = new System.Windows.Forms.CheckBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_ShowImage = new Telerik.WinControls.UI.RadGridView();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Vender = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Update = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Unit = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Qty = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_ItemName = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Item = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_ShowHistory = new Telerik.WinControls.UI.RadGridView();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.radButton_ExcelHistory = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SendCN)).BeginInit();
            this.groupBox_SQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SaveSQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SqDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SqpriceNoVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SqNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            this.groupBox_CN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CNPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CnSaveSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CNSaveRecive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNReciveDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CnSendRmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNSendBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNSendBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNWhoSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImage.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowHistory.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_ExcelHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Controls.Add(this.radButton_ExcelHistory);
            this.radGroupBox_DB.Controls.Add(this.RadButton_pdt);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Docno);
            this.radGroupBox_DB.Controls.Add(this.radButton_printImg);
            this.radGroupBox_DB.Controls.Add(this.radLabel_SN);
            this.radGroupBox_DB.Controls.Add(this.radButton_SendCN);
            this.radGroupBox_DB.Controls.Add(this.checkBox_ReciveDpt);
            this.radGroupBox_DB.Controls.Add(this.groupBox_SQ);
            this.radGroupBox_DB.Controls.Add(this.groupBox_CN);
            this.radGroupBox_DB.Controls.Add(this.radGridView_ShowImage);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Desc);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Vender);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Update);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Unit);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Qty);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_ItemName);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Item);
            this.radGroupBox_DB.Controls.Add(this.radLabel4);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radGridView_ShowHistory);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(952, 674);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(438, 23);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 74;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(191, 25);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(241, 23);
            this.radLabel_Docno.TabIndex = 65;
            this.radLabel_Docno.Text = "-";
            // 
            // radButton_printImg
            // 
            this.radButton_printImg.BackColor = System.Drawing.Color.Transparent;
            this.radButton_printImg.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_printImg.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_printImg.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_printImg.Location = new System.Drawing.Point(432, 522);
            this.radButton_printImg.Name = "radButton_printImg";
            this.radButton_printImg.Size = new System.Drawing.Size(28, 28);
            this.radButton_printImg.TabIndex = 64;
            this.radButton_printImg.Text = "radButton4";
            this.radButton_printImg.Click += new System.EventHandler(this.RadButton_printImg_Click);
            // 
            // radLabel_SN
            // 
            this.radLabel_SN.AutoSize = false;
            this.radLabel_SN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_SN.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_SN.Location = new System.Drawing.Point(173, 110);
            this.radLabel_SN.Name = "radLabel_SN";
            this.radLabel_SN.Size = new System.Drawing.Size(218, 19);
            this.radLabel_SN.TabIndex = 62;
            // 
            // radButton_SendCN
            // 
            this.radButton_SendCN.BackColor = System.Drawing.Color.Transparent;
            this.radButton_SendCN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radButton_SendCN.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_SendCN.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_SendCN.Location = new System.Drawing.Point(356, 155);
            this.radButton_SendCN.Name = "radButton_SendCN";
            this.radButton_SendCN.Size = new System.Drawing.Size(107, 26);
            this.radButton_SendCN.TabIndex = 61;
            this.radButton_SendCN.Text = "บันทึกส่ง CN";
            this.radButton_SendCN.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_SendCN.Click += new System.EventHandler(this.RadButton_SendCN_Click);
            // 
            // checkBox_ReciveDpt
            // 
            this.checkBox_ReciveDpt.AutoSize = true;
            this.checkBox_ReciveDpt.Location = new System.Drawing.Point(11, 525);
            this.checkBox_ReciveDpt.Name = "checkBox_ReciveDpt";
            this.checkBox_ReciveDpt.Size = new System.Drawing.Size(142, 20);
            this.checkBox_ReciveDpt.TabIndex = 56;
            this.checkBox_ReciveDpt.Text = "รับอุปกรณ์คืนแผนก";
            this.checkBox_ReciveDpt.UseVisualStyleBackColor = true;
            this.checkBox_ReciveDpt.CheckedChanged += new System.EventHandler(this.CheckBox_ReciveDpt_CheckedChanged);
            // 
            // groupBox_SQ
            // 
            this.groupBox_SQ.Controls.Add(this.radButton_SaveSQ);
            this.groupBox_SQ.Controls.Add(this.radTextBox_SqDesc);
            this.groupBox_SQ.Controls.Add(this.radioButton_NoSq);
            this.groupBox_SQ.Controls.Add(this.radioButton_Sq);
            this.groupBox_SQ.Controls.Add(this.radTextBox_SqpriceNoVat);
            this.groupBox_SQ.Controls.Add(this.radTextBox_SqNo);
            this.groupBox_SQ.Controls.Add(this.radLabel5);
            this.groupBox_SQ.Controls.Add(this.radLabel7);
            this.groupBox_SQ.Location = new System.Drawing.Point(11, 386);
            this.groupBox_SQ.Name = "groupBox_SQ";
            this.groupBox_SQ.Size = new System.Drawing.Size(455, 135);
            this.groupBox_SQ.TabIndex = 55;
            this.groupBox_SQ.TabStop = false;
            this.groupBox_SQ.Text = "ค่าใช้จ่าย";
            // 
            // radButton_SaveSQ
            // 
            this.radButton_SaveSQ.BackColor = System.Drawing.Color.Transparent;
            this.radButton_SaveSQ.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_SaveSQ.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButton_SaveSQ.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_SaveSQ.Location = new System.Drawing.Point(422, 102);
            this.radButton_SaveSQ.Name = "radButton_SaveSQ";
            this.radButton_SaveSQ.Size = new System.Drawing.Size(26, 26);
            this.radButton_SaveSQ.TabIndex = 48;
            this.radButton_SaveSQ.Text = "radButton3";
            this.radButton_SaveSQ.Click += new System.EventHandler(this.RadButton_SaveSQ_Click);
            // 
            // radTextBox_SqDesc
            // 
            this.radTextBox_SqDesc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_SqDesc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SqDesc.Location = new System.Drawing.Point(9, 65);
            this.radTextBox_SqDesc.Multiline = true;
            this.radTextBox_SqDesc.Name = "radTextBox_SqDesc";
            // 
            // 
            // 
            this.radTextBox_SqDesc.RootElement.StretchVertically = true;
            this.radTextBox_SqDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_SqDesc.Size = new System.Drawing.Size(407, 64);
            this.radTextBox_SqDesc.TabIndex = 47;
            // 
            // radioButton_NoSq
            // 
            this.radioButton_NoSq.AutoSize = true;
            this.radioButton_NoSq.ForeColor = System.Drawing.Color.Blue;
            this.radioButton_NoSq.Location = new System.Drawing.Point(9, 43);
            this.radioButton_NoSq.Name = "radioButton_NoSq";
            this.radioButton_NoSq.Size = new System.Drawing.Size(107, 20);
            this.radioButton_NoSq.TabIndex = 50;
            this.radioButton_NoSq.TabStop = true;
            this.radioButton_NoSq.Text = "ไม่มีค่าใช้จ่าย";
            this.radioButton_NoSq.UseVisualStyleBackColor = true;
            // 
            // radioButton_Sq
            // 
            this.radioButton_Sq.AutoSize = true;
            this.radioButton_Sq.ForeColor = System.Drawing.Color.Blue;
            this.radioButton_Sq.Location = new System.Drawing.Point(9, 22);
            this.radioButton_Sq.Name = "radioButton_Sq";
            this.radioButton_Sq.Size = new System.Drawing.Size(91, 20);
            this.radioButton_Sq.TabIndex = 49;
            this.radioButton_Sq.TabStop = true;
            this.radioButton_Sq.Text = "มีค่าใช้จ่าย";
            this.radioButton_Sq.UseVisualStyleBackColor = true;
            this.radioButton_Sq.CheckedChanged += new System.EventHandler(this.RadioButton_Sq_CheckedChanged);
            // 
            // radTextBox_SqpriceNoVat
            // 
            this.radTextBox_SqpriceNoVat.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_SqpriceNoVat.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SqpriceNoVat.Location = new System.Drawing.Point(279, 34);
            this.radTextBox_SqpriceNoVat.Name = "radTextBox_SqpriceNoVat";
            this.radTextBox_SqpriceNoVat.Size = new System.Drawing.Size(137, 21);
            this.radTextBox_SqpriceNoVat.TabIndex = 46;
            this.radTextBox_SqpriceNoVat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_SqpriceNoVat_KeyPress);
            // 
            // radTextBox_SqNo
            // 
            this.radTextBox_SqNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_SqNo.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SqNo.Location = new System.Drawing.Point(116, 34);
            this.radTextBox_SqNo.Name = "radTextBox_SqNo";
            this.radTextBox_SqNo.Size = new System.Drawing.Size(157, 21);
            this.radTextBox_SqNo.TabIndex = 45;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(279, 12);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(163, 19);
            this.radLabel5.TabIndex = 48;
            this.radLabel5.Text = "ราคา [ก่อน VAT]";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(116, 12);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(157, 19);
            this.radLabel7.TabIndex = 47;
            this.radLabel7.Text = "ใบเสนอราคาซ่อม";
            // 
            // groupBox_CN
            // 
            this.groupBox_CN.Controls.Add(this.radButton_CNPrint);
            this.groupBox_CN.Controls.Add(this.radButton_CnSaveSend);
            this.groupBox_CN.Controls.Add(this.radButton_CNSaveRecive);
            this.groupBox_CN.Controls.Add(this.radTextBox_CNReciveDesc);
            this.groupBox_CN.Controls.Add(this.radTextBox_CnSendRmk);
            this.groupBox_CN.Controls.Add(this.radTextBox_CNSendBill);
            this.groupBox_CN.Controls.Add(this.radTextBox_CNSendBy);
            this.groupBox_CN.Controls.Add(this.radTextBox_CNWhoSend);
            this.groupBox_CN.Controls.Add(this.checkBox_CnRecive);
            this.groupBox_CN.Controls.Add(this.checkBox_CNSend);
            this.groupBox_CN.Controls.Add(this.radLabel8);
            this.groupBox_CN.Controls.Add(this.radLabel9);
            this.groupBox_CN.Controls.Add(this.radLabel10);
            this.groupBox_CN.Location = new System.Drawing.Point(472, 386);
            this.groupBox_CN.Name = "groupBox_CN";
            this.groupBox_CN.Size = new System.Drawing.Size(451, 236);
            this.groupBox_CN.TabIndex = 54;
            this.groupBox_CN.TabStop = false;
            this.groupBox_CN.Text = "CN";
            // 
            // radButton_CNPrint
            // 
            this.radButton_CNPrint.BackColor = System.Drawing.Color.Transparent;
            this.radButton_CNPrint.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_CNPrint.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_CNPrint.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_CNPrint.Location = new System.Drawing.Point(418, 9);
            this.radButton_CNPrint.Name = "radButton_CNPrint";
            this.radButton_CNPrint.Size = new System.Drawing.Size(28, 28);
            this.radButton_CNPrint.TabIndex = 61;
            this.radButton_CNPrint.Text = "radButton4";
            this.radButton_CNPrint.Click += new System.EventHandler(this.RadButton_CNPrint_Click);
            // 
            // radButton_CnSaveSend
            // 
            this.radButton_CnSaveSend.BackColor = System.Drawing.Color.Transparent;
            this.radButton_CnSaveSend.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_CnSaveSend.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButton_CnSaveSend.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_CnSaveSend.Location = new System.Drawing.Point(421, 122);
            this.radButton_CnSaveSend.Name = "radButton_CnSaveSend";
            this.radButton_CnSaveSend.Size = new System.Drawing.Size(26, 26);
            this.radButton_CnSaveSend.TabIndex = 53;
            this.radButton_CnSaveSend.Text = "radButton1";
            this.radButton_CnSaveSend.Click += new System.EventHandler(this.RadButton_CnSaveSend_Click);
            // 
            // radButton_CNSaveRecive
            // 
            this.radButton_CNSaveRecive.BackColor = System.Drawing.Color.Transparent;
            this.radButton_CNSaveRecive.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_CNSaveRecive.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButton_CNSaveRecive.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_CNSaveRecive.Location = new System.Drawing.Point(420, 203);
            this.radButton_CNSaveRecive.Name = "radButton_CNSaveRecive";
            this.radButton_CNSaveRecive.Size = new System.Drawing.Size(26, 26);
            this.radButton_CNSaveRecive.TabIndex = 58;
            this.radButton_CNSaveRecive.Text = "radButton2";
            this.radButton_CNSaveRecive.Click += new System.EventHandler(this.RadButton_CNSaveRecive_Click);
            // 
            // radTextBox_CNReciveDesc
            // 
            this.radTextBox_CNReciveDesc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_CNReciveDesc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CNReciveDesc.Location = new System.Drawing.Point(6, 170);
            this.radTextBox_CNReciveDesc.Multiline = true;
            this.radTextBox_CNReciveDesc.Name = "radTextBox_CNReciveDesc";
            // 
            // 
            // 
            this.radTextBox_CNReciveDesc.RootElement.StretchVertically = true;
            this.radTextBox_CNReciveDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_CNReciveDesc.Size = new System.Drawing.Size(408, 60);
            this.radTextBox_CNReciveDesc.TabIndex = 57;
            // 
            // radTextBox_CnSendRmk
            // 
            this.radTextBox_CnSendRmk.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_CnSendRmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CnSendRmk.Location = new System.Drawing.Point(6, 91);
            this.radTextBox_CnSendRmk.Multiline = true;
            this.radTextBox_CnSendRmk.Name = "radTextBox_CnSendRmk";
            // 
            // 
            // 
            this.radTextBox_CnSendRmk.RootElement.StretchVertically = true;
            this.radTextBox_CnSendRmk.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_CnSendRmk.Size = new System.Drawing.Size(408, 57);
            this.radTextBox_CnSendRmk.TabIndex = 52;
            // 
            // radTextBox_CNSendBill
            // 
            this.radTextBox_CNSendBill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CNSendBill.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CNSendBill.Location = new System.Drawing.Point(68, 66);
            this.radTextBox_CNSendBill.Name = "radTextBox_CNSendBill";
            this.radTextBox_CNSendBill.Size = new System.Drawing.Size(342, 21);
            this.radTextBox_CNSendBill.TabIndex = 51;
            // 
            // radTextBox_CNSendBy
            // 
            this.radTextBox_CNSendBy.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CNSendBy.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CNSendBy.Location = new System.Drawing.Point(68, 41);
            this.radTextBox_CNSendBy.Name = "radTextBox_CNSendBy";
            this.radTextBox_CNSendBy.Size = new System.Drawing.Size(342, 21);
            this.radTextBox_CNSendBy.TabIndex = 50;
            // 
            // radTextBox_CNWhoSend
            // 
            this.radTextBox_CNWhoSend.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CNWhoSend.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CNWhoSend.Location = new System.Drawing.Point(141, 16);
            this.radTextBox_CNWhoSend.Name = "radTextBox_CNWhoSend";
            this.radTextBox_CNWhoSend.Size = new System.Drawing.Size(267, 21);
            this.radTextBox_CNWhoSend.TabIndex = 49;
            // 
            // checkBox_CnRecive
            // 
            this.checkBox_CnRecive.AutoSize = true;
            this.checkBox_CnRecive.ForeColor = System.Drawing.Color.Blue;
            this.checkBox_CnRecive.Location = new System.Drawing.Point(6, 150);
            this.checkBox_CnRecive.Name = "checkBox_CnRecive";
            this.checkBox_CnRecive.Size = new System.Drawing.Size(89, 20);
            this.checkBox_CnRecive.TabIndex = 1;
            this.checkBox_CnRecive.Text = "รับอุปกรณ์";
            this.checkBox_CnRecive.UseVisualStyleBackColor = true;
            this.checkBox_CnRecive.CheckedChanged += new System.EventHandler(this.CheckBox_CnRecive_CheckedChanged);
            // 
            // checkBox_CNSend
            // 
            this.checkBox_CNSend.AutoSize = true;
            this.checkBox_CNSend.ForeColor = System.Drawing.Color.Blue;
            this.checkBox_CNSend.Location = new System.Drawing.Point(6, 17);
            this.checkBox_CNSend.Name = "checkBox_CNSend";
            this.checkBox_CNSend.Size = new System.Drawing.Size(88, 20);
            this.checkBox_CNSend.TabIndex = 0;
            this.checkBox_CNSend.Text = "ส่งอุปกรณ์";
            this.checkBox_CNSend.UseVisualStyleBackColor = true;
            this.checkBox_CNSend.CheckedChanged += new System.EventHandler(this.CheckBox_CNSend_CheckedChanged);
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(4, 43);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(71, 19);
            this.radLabel8.TabIndex = 52;
            this.radLabel8.Text = "บ.จัดส่ง";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(105, 19);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(37, 19);
            this.radLabel9.TabIndex = 51;
            this.radLabel9.Text = "ผู้ส่ง";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(3, 69);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(72, 19);
            this.radLabel10.TabIndex = 55;
            this.radLabel10.Text = "เลขที่บิล";
            // 
            // radGridView_ShowImage
            // 
            this.radGridView_ShowImage.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_ShowImage.Location = new System.Drawing.Point(470, 23);
            // 
            // 
            // 
            this.radGridView_ShowImage.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_ShowImage.Name = "radGridView_ShowImage";
            // 
            // 
            // 
            this.radGridView_ShowImage.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_ShowImage.Size = new System.Drawing.Size(453, 143);
            this.radGridView_ShowImage.TabIndex = 53;
            this.radGridView_ShowImage.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowImage_ViewCellFormatting);
            this.radGridView_ShowImage.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowImage_CellDoubleClick);
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Black;
            this.radTextBox_Desc.Location = new System.Drawing.Point(11, 187);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            this.radTextBox_Desc.ReadOnly = true;
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Desc.Size = new System.Drawing.Size(453, 193);
            this.radTextBox_Desc.TabIndex = 52;
            // 
            // radLabel_Vender
            // 
            this.radLabel_Vender.AutoSize = false;
            this.radLabel_Vender.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Vender.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Vender.Location = new System.Drawing.Point(11, 139);
            this.radLabel_Vender.Name = "radLabel_Vender";
            this.radLabel_Vender.Size = new System.Drawing.Size(310, 23);
            this.radLabel_Vender.TabIndex = 51;
            this.radLabel_Vender.Text = "-";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(474, 628);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(375, 628);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_Update
            // 
            this.radTextBox_Update.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Update.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Update.Location = new System.Drawing.Point(10, 551);
            this.radTextBox_Update.Multiline = true;
            this.radTextBox_Update.Name = "radTextBox_Update";
            // 
            // 
            // 
            this.radTextBox_Update.RootElement.StretchVertically = true;
            this.radTextBox_Update.Size = new System.Drawing.Size(456, 71);
            this.radTextBox_Update.TabIndex = 4;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(470, 166);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(158, 19);
            this.radLabel1.TabIndex = 39;
            this.radLabel1.Text = "ประวัติการส่งซ่อม";
            // 
            // radTextBox_Unit
            // 
            this.radTextBox_Unit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Unit.Location = new System.Drawing.Point(91, 105);
            this.radTextBox_Unit.Name = "radTextBox_Unit";
            this.radTextBox_Unit.Size = new System.Drawing.Size(76, 25);
            this.radTextBox_Unit.TabIndex = 2;
            // 
            // radTextBox_Qty
            // 
            this.radTextBox_Qty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Qty.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Qty.Location = new System.Drawing.Point(11, 105);
            this.radTextBox_Qty.Name = "radTextBox_Qty";
            this.radTextBox_Qty.Size = new System.Drawing.Size(76, 25);
            this.radTextBox_Qty.TabIndex = 1;
            // 
            // radTextBox_ItemName
            // 
            this.radTextBox_ItemName.AutoSize = false;
            this.radTextBox_ItemName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_ItemName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ItemName.Location = new System.Drawing.Point(11, 54);
            this.radTextBox_ItemName.Name = "radTextBox_ItemName";
            this.radTextBox_ItemName.Size = new System.Drawing.Size(384, 23);
            this.radTextBox_ItemName.TabIndex = 36;
            this.radTextBox_ItemName.Text = "-";
            // 
            // radTextBox_Item
            // 
            this.radTextBox_Item.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Item.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Item.Location = new System.Drawing.Point(11, 23);
            this.radTextBox_Item.Name = "radTextBox_Item";
            this.radTextBox_Item.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Item.TabIndex = 0;
            this.radTextBox_Item.Tag = "";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(92, 83);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(76, 19);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "หน่วย";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(11, 83);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(76, 19);
            this.radLabel3.TabIndex = 43;
            this.radLabel3.Text = "จำนวน";
            // 
            // radGridView_ShowHistory
            // 
            this.radGridView_ShowHistory.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_ShowHistory.Location = new System.Drawing.Point(470, 186);
            // 
            // 
            // 
            this.radGridView_ShowHistory.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_ShowHistory.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_ShowHistory.Name = "radGridView_ShowHistory";
            this.radGridView_ShowHistory.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_ShowHistory.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_ShowHistory.Size = new System.Drawing.Size(453, 193);
            this.radGridView_ShowHistory.TabIndex = 40;
            this.radGridView_ShowHistory.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(11, 166);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(224, 19);
            this.radLabel2.TabIndex = 41;
            this.radLabel2.Text = "รายละเอียดการส่งซ่อม/ทำทิ้ง";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // radButton_ExcelHistory
            // 
            this.radButton_ExcelHistory.BackColor = System.Drawing.Color.Transparent;
            this.radButton_ExcelHistory.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_ExcelHistory.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButton_ExcelHistory.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_ExcelHistory.Location = new System.Drawing.Point(924, 162);
            this.radButton_ExcelHistory.Name = "radButton_ExcelHistory";
            this.radButton_ExcelHistory.Size = new System.Drawing.Size(24, 24);
            this.radButton_ExcelHistory.TabIndex = 75;
            this.radButton_ExcelHistory.Text = "radButton4";
            this.radButton_ExcelHistory.Click += new System.EventHandler(this.RadButton_ExcelHistory_Click);
            // 
            // Claim_EDIT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(952, 674);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Claim_EDIT";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ใบเช็คอุปกรณ์";
            this.Load += new System.EventHandler(this.Claim_EDIT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SendCN)).EndInit();
            this.groupBox_SQ.ResumeLayout(false);
            this.groupBox_SQ.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SaveSQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SqDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SqpriceNoVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SqNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            this.groupBox_CN.ResumeLayout(false);
            this.groupBox_CN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CNPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CnSaveSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CNSaveRecive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNReciveDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CnSendRmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNSendBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNSendBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CNWhoSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImage.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowHistory.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_ExcelHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Item;
        private Telerik.WinControls.UI.RadLabel radTextBox_ItemName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Unit;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Qty;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView radGridView_ShowHistory;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Update;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Vender;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private Telerik.WinControls.UI.RadGridView radGridView_ShowImage;
        private System.Windows.Forms.GroupBox groupBox_CN;
        private System.Windows.Forms.CheckBox checkBox_CNSend;
        private System.Windows.Forms.CheckBox checkBox_CnRecive;
        private System.Windows.Forms.GroupBox groupBox_SQ;
        private System.Windows.Forms.CheckBox checkBox_ReciveDpt;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SqpriceNoVat;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SqNo;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private System.Windows.Forms.RadioButton radioButton_NoSq;
        private System.Windows.Forms.RadioButton radioButton_Sq;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SqDesc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CNSendBy;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CNWhoSend;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CnSendRmk;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CNSendBill;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CNReciveDesc;
        private Telerik.WinControls.UI.RadButton radButton_CNSaveRecive;
        private Telerik.WinControls.UI.RadButton radButton_CnSaveSend;
        private Telerik.WinControls.UI.RadButton radButton_SaveSQ;
        private Telerik.WinControls.UI.RadButton radButton_CNPrint;
        private Telerik.WinControls.UI.RadButton radButton_SendCN;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private Telerik.WinControls.UI.RadLabel radLabel_SN;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.RadButton radButton_printImg;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadButton radButton_ExcelHistory;
    }
}
