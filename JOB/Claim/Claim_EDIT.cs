﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;


namespace PC_Shop24Hrs.JOB.Claim
{


    public partial class Claim_EDIT : Telerik.WinControls.UI.RadForm
    {
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        readonly string _pTypeBill;
        readonly string _pBillNo;
        readonly string _pPermission;
        readonly string _pGroupDesc;

        public string pChange;


        string descPrint;
        string whoIDIns;
        string whoNameIns;
        string vender;
        string pxe;
        //string _pGroupDesc;
        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowImage_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
        }
        #endregion

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        public Claim_EDIT(string pTypeBill, string pBillNo, string pPermission, string pGroupDesc)
        {
            InitializeComponent();

            _pTypeBill = pTypeBill;
            _pBillNo = pBillNo;
            _pPermission = pPermission;
            _pGroupDesc = pGroupDesc;
        }

        private void Claim_EDIT_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_printImg.ButtonElement.ShowBorder = true; radButton_printImg.ButtonElement.ToolTipText = "พิมพ์รูปภาพ";
            radButton_SendCN.ButtonElement.ShowBorder = true; radButton_SendCN.ButtonElement.ToolTipText = "บันทึกส่ง CN";
            radButton_SendCN.ButtonElement.Font = SystemClass.SetFontGernaral;
            radButton_SaveSQ.ButtonElement.ShowBorder = true; radButton_SaveSQ.ButtonElement.ToolTipText = "บันทึกใบเสนอราคา";
            radButton_CNPrint.ButtonElement.ShowBorder = true; radButton_CNPrint.ButtonElement.ToolTipText = "พิมพ์เอกสาร";
            radButton_CNSaveRecive.ButtonElement.ShowBorder = true; radButton_CNSaveRecive.ButtonElement.ToolTipText = "บันทึกรับคืน";
            radButton_CnSaveSend.ButtonElement.ShowBorder = true; radButton_CnSaveSend.ButtonElement.ToolTipText = "บันทึกส่ง";
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultRadGridView(radGridView_ShowHistory);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radGridView_ShowHistory.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_INS", "วันที่ส่งซ่อม", 120));
            radGridView_ShowHistory.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_RECIVE", "วันที่รับคืน", 120));
            radGridView_ShowHistory.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "อาการ", 250));
            radGridView_ShowHistory.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SQ_COST", "ค่าซ่อม", 120));
            radGridView_ShowHistory.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SQ_DESCRIPTION", "รายละเอียดการซ่อม", 250));

            radGridView_ShowHistory.EnableFiltering = false;

            pChange = "0";
            SetEnable();
            //if ((_pTypeBill == "0") || (_pTypeBill == "1"))
            //{
            SetFrom();

            //}


        }
        //Set SetCNSend 
        void SetCNSend()
        {
            if ((SystemClass.SystemDptID == "D035") || (SystemClass.SystemComProgrammer == "1"))
            {
                checkBox_CNSend.Enabled = true;
                radTextBox_CNWhoSend.Enabled = true; radTextBox_CNWhoSend.Text = "";
                radTextBox_CNSendBy.Enabled = true; radTextBox_CNSendBy.Text = "";
                radTextBox_CNSendBill.Enabled = true; radTextBox_CNSendBill.Text = "";
                radTextBox_CnSendRmk.Enabled = true; radTextBox_CnSendRmk.Text = "";

                radButton_CnSaveSend.Enabled = false;
            }
            else
            {
                checkBox_CNSend.Enabled = false;
                radTextBox_CNWhoSend.Enabled = false;
                radTextBox_CNSendBy.Enabled = false;
                radTextBox_CNSendBill.Enabled = false;
                radTextBox_CnSendRmk.Enabled = false;

                radButton_CNPrint.Enabled = false;
                radButton_CnSaveSend.Enabled = false;

            }

        }
        //Set SetCNRecive 
        void SetCNRecive()
        {
            if ((SystemClass.SystemDptID == "D035") || (SystemClass.SystemComProgrammer == "1"))
            {
                checkBox_CnRecive.Enabled = true;
                radTextBox_CNReciveDesc.Enabled = true; radTextBox_CNReciveDesc.Text = "";
                radButton_CNPrint.Enabled = true;
                radButton_CnSaveSend.Enabled = true;
                radButton_CNSaveRecive.Enabled = false;
                radButton_CNPrint.Enabled = true;
            }
            else
            {
                checkBox_CnRecive.Enabled = false;
                radTextBox_CNReciveDesc.Enabled = false;
                radButton_CNSaveRecive.Enabled = false;
            }

        }
        //Set SQ
        void SetSQ()
        {
            if (_pPermission == "1")
            {
                radTextBox_SqNo.Enabled = true; radTextBox_SqNo.Text = "";
                radTextBox_SqpriceNoVat.Enabled = true; radTextBox_SqpriceNoVat.Text = "";
                radioButton_Sq.Enabled = true; radioButton_Sq.Checked = false;
                radioButton_NoSq.Enabled = true; radioButton_NoSq.Checked = true;
                radTextBox_SqDesc.Enabled = true; radTextBox_SqDesc.Text = "";
                radButton_SaveSQ.Enabled = false;
            }
            else
            {
                radTextBox_SqNo.Enabled = false;
                radTextBox_SqpriceNoVat.Enabled = false;
                radioButton_Sq.Enabled = false;
                radioButton_NoSq.Enabled = false;
                radTextBox_SqDesc.Enabled = false;
                radButton_SaveSQ.Enabled = false;
            }
        }
        //Set Enable
        void SetEnable()
        {

            radTextBox_Item.Enabled = false;
            radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Enabled = false;
        }
        //Set Enable CloseJOB
        void SetEnableCloseJOB()
        {
            radTextBox_SqNo.Enabled = false;
            radTextBox_SqpriceNoVat.Enabled = false;
            radioButton_Sq.Enabled = false;
            radioButton_NoSq.Enabled = false;
            radTextBox_SqDesc.Enabled = false;
            radButton_SaveSQ.Enabled = false;

            checkBox_CNSend.Enabled = false;
            radTextBox_CNWhoSend.Enabled = false;
            radTextBox_CNSendBy.Enabled = false;
            radTextBox_CNSendBill.Enabled = false;
            radTextBox_CnSendRmk.Enabled = false;
            checkBox_CnRecive.Enabled = false;
            radTextBox_CNReciveDesc.Enabled = false;
            radButton_CNPrint.Enabled = false;
            radButton_CnSaveSend.Enabled = false;
            radButton_CNSaveRecive.Enabled = false;
            radButton_SendCN.Enabled = false;
            checkBox_ReciveDpt.Enabled = false;
            radButton_Save.Enabled = false;

        }
        //History
        void FindHistory(string pAssID)
        {
            DataTable dtHis = JOB_Class.GetData_HistoryClaimByAssID(pAssID);
            if (dtHis.Rows.Count > 0)
            {
                radGridView_ShowHistory.DataSource = dtHis;
                dtHis.AcceptChanges();
            }
        }
        //Set Form
        void SetFrom()
        {
            radButton_printImg.Enabled = false;

            string sql = $@"SELECT	ASSETID,Name,SN,LOCATION_DESC,VENDER_ID,VENDER_NAME,
                                CONVERT(VARCHAR,DATE_INS,23) AS DATE_INS,DESCRIPTION,QTY,UNIT,
		                        SQ_STA,SQ_INVOICEID,SQ_COST,SQ_DESCRIPTION,SQ_Date,
		                        PXE_ID,PXE_DocumentNum,PXE_Amount,
		                        STA_SEND,WHOID_SEND,WHONAME_SEND,DATE_SEND,Transport_SEND,TypeTransport_SEND,Track_SEND,REMARK_SEND,
		                        STA_RECIVECN,REMARK_RECIVECN,CONVERT(VARCHAR,ISNULL(DATE_SENDCN,'1900-01-01'),23) AS DATE_SENDCN,
		                        RECIVE_STA,RECIVE_DESC,RECIVE_WHOID,RECIVE_WHONAME,RECIVE_DATE,
                                CASE WHEN CHARINDEX('->' ,DESCRIPTION,1) = '1' THEN SUBSTRING( DESCRIPTION,1,CHARINDEX('->' ,DESCRIPTION,1)-1)  ELSE DESCRIPTION END  AS descPrint  ,
                                WHOIDINS,WHONAMEINS ,ISNULL(PXE_ID,'') AS PXE_ID    
                        FROM	SHOP_JOBCLAIM WITH(NOLOCK)
                        WHERE	Claim_ID = '{_pBillNo}'";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            radLabel_Docno.Text = _pBillNo;
            radTextBox_Item.Text = dt.Rows[0]["ASSETID"].ToString();
            radTextBox_ItemName.Text = dt.Rows[0]["Name"].ToString();
            radTextBox_Qty.Text = dt.Rows[0]["QTY"].ToString();
            radTextBox_Unit.Text = dt.Rows[0]["UNIT"].ToString();
            radLabel_Vender.Text = dt.Rows[0]["VENDER_ID"].ToString() + " " + dt.Rows[0]["VENDER_NAME"].ToString();
            vender = dt.Rows[0]["VENDER_ID"].ToString();
            pxe = dt.Rows[0]["PXE_ID"].ToString();
            radTextBox_Desc.Text = dt.Rows[0]["DESCRIPTION"].ToString();
            radLabel_SN.Text = dt.Rows[0]["SN"].ToString();
            descPrint = dt.Rows[0]["descPrint"].ToString();
            whoIDIns = dt.Rows[0]["WHOIDINS"].ToString();
            whoNameIns = dt.Rows[0]["WHONAMEINS"].ToString();

            if (dt.Rows[0]["ASSETID"].ToString() != "")
            {
                FindHistory(radTextBox_Item.Text);
            }
            //Send CN
            if (dt.Rows[0]["DATE_SENDCN"].ToString() == "1900-01-01")
            {
                radButton_SendCN.Enabled = true;
            }
            else
            {
                radButton_SendCN.Enabled = false;
                radButton_SendCN.Text = dt.Rows[0]["DATE_SENDCN"].ToString();
            }
            //SQ
            if (dt.Rows[0]["SQ_STA"].ToString() == "1")
            {
                radioButton_Sq.Checked = true; radioButton_Sq.Enabled = false;
                radioButton_NoSq.Checked = false; radioButton_NoSq.Enabled = false;

                radTextBox_SqNo.Text = dt.Rows[0]["SQ_INVOICEID"].ToString(); radTextBox_SqNo.Enabled = false;
                radTextBox_SqpriceNoVat.Text = dt.Rows[0]["SQ_COST"].ToString(); radTextBox_SqpriceNoVat.Enabled = false;
                radTextBox_SqDesc.Text = dt.Rows[0]["SQ_DESCRIPTION"].ToString(); radTextBox_SqDesc.Enabled = false;
                radButton_SaveSQ.Enabled = false;
            }
            else
            {
                SetSQ();
            }
            //CN Send
            if (dt.Rows[0]["STA_SEND"].ToString() == "1")
            {
                checkBox_CNSend.Checked = true; checkBox_CNSend.Enabled = false;
                radTextBox_CNWhoSend.Text = dt.Rows[0]["Transport_SEND"].ToString(); radTextBox_CNWhoSend.Enabled = false;
                radTextBox_CNSendBy.Text = dt.Rows[0]["TypeTransport_SEND"].ToString(); radTextBox_CNSendBy.Enabled = false;
                radTextBox_CNSendBill.Text = dt.Rows[0]["Track_SEND"].ToString(); radTextBox_CNSendBill.Enabled = false;
                radTextBox_CnSendRmk.Text = dt.Rows[0]["REMARK_SEND"].ToString(); radTextBox_CnSendRmk.Enabled = false;
                radButton_CnSaveSend.Enabled = false;
            }
            else
            {
                SetCNSend();
            }
            //CN Recive
            if (dt.Rows[0]["STA_RECIVECN"].ToString() == "1")
            {
                checkBox_CnRecive.Checked = true; checkBox_CnRecive.Enabled = false;
                radTextBox_CNReciveDesc.Text = dt.Rows[0]["REMARK_RECIVECN"].ToString(); radTextBox_CNReciveDesc.Enabled = false;
                radButton_CNSaveRecive.Enabled = false;
            }
            else
            {
                SetCNRecive();
            }

            JOB_Class.SetHeadGrid_Image_ForMNCM(radGridView_ShowImage, _pTypeBill, _pBillNo, dt.Rows[0]["DATE_INS"].ToString());

            //ปิดจ๊อบ
            if (dt.Rows[0]["RECIVE_STA"].ToString() == "1")
            {
                checkBox_ReciveDpt.Checked = true;
                radTextBox_Update.Text = dt.Rows[0]["RECIVE_DESC"].ToString();
                radTextBox_Update.Enabled = false;

                SetEnableCloseJOB();
                if (_pPermission == "1")
                {
                    radButton_printImg.Enabled = true;
                }
            }
            else
            {
                radTextBox_Update.Enabled = true;
                checkBox_ReciveDpt.Checked = false;
                if (_pPermission == "0")
                {
                    checkBox_ReciveDpt.Enabled = false;
                }
                else
                {
                    checkBox_ReciveDpt.Enabled = true;
                }
            }
            radTextBox_Desc.SelectionStart = radTextBox_Desc.Text.Length;
            radTextBox_Desc.Select(radTextBox_Desc.Text.Length, 0);

            if (_pTypeBill == "1")
            {
                groupBox_CN.Enabled = false;
                groupBox_SQ.Enabled = false;
                radButton_SendCN.Enabled = false;
                checkBox_ReciveDpt.Enabled = false;
            }



        }
        //SQ
        private void RadioButton_Sq_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Sq.Checked == true)
            {
                radButton_SaveSQ.Enabled = true;
                radTextBox_SqNo.Focus();
            }
            else
            {
                radButton_SaveSQ.Enabled = false;
            }
        }
        //CN Send
        private void CheckBox_CNSend_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_CNSend.Checked == true)
            {
                radButton_CnSaveSend.Enabled = true;
                radTextBox_CNWhoSend.Focus();
            }
            else
            {
                radButton_CnSaveSend.Enabled = false;
            }
        }
        //CN Recive
        private void CheckBox_CnRecive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_CnRecive.Checked == true)
            {
                radButton_CNSaveRecive.Enabled = true;
                radTextBox_CNReciveDesc.Focus();
            }
            else
            {
                radButton_CNSaveRecive.Enabled = false;
            }
        }
        //OpenImage
        private void RadGridView_ShowImage_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_ShowImage.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_ShowImage.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_ShowImage.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_ShowImage.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_ShowImage.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_ShowImage.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //Number Only
        private void RadTextBox_SqpriceNoVat_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Update SQ
        private void RadButton_SaveSQ_Click(object sender, EventArgs e)
        {

            if (radioButton_Sq.Checked == false)
            {
                return;
            }

            if ((radTextBox_SqNo.Text == "") || (radTextBox_SqpriceNoVat.Text == "") || (radTextBox_SqDesc.Text == ""))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูลใบเสนอราคาซ่อม");
                radTextBox_SqDesc.Focus();
                return;
            }

            if ((MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(_pBillNo, "ยืนยันการบันทึกใบเสนอราคา")) == DialogResult.No)
            {
                return;
            }

            string rmk = "- " + radTextBox_SqDesc.Text + Environment.NewLine +
                " -> BY " + SystemClass.SystemUserName + Environment.NewLine +
                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss") + "]";
            string sqlUp = $@" UPDATE   SHOP_JOBCLAIM 
                               SET      SQ_STA = '1', 
                                        SQ_INVOICEID = '{radTextBox_SqNo.Text}', 
                                        SQ_COST = '{radTextBox_SqpriceNoVat.Text}', 
                                        SQ_DESCRIPTION = '{rmk}', 
                                        SQ_Date = GETDATE() , 
                                        SQ_WHOID = '{SystemClass.SystemUserID}', 
                                        SQ_WHONAME = '{SystemClass.SystemUserName}' 
                                WHERE   Claim_ID = '{_pBillNo}'";
            string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                pChange = "1";
                radioButton_Sq.Enabled = false;
                radioButton_NoSq.Enabled = false;

                radTextBox_SqNo.Enabled = false;
                radTextBox_SqpriceNoVat.Enabled = false;
                radTextBox_SqDesc.Enabled = false; radTextBox_SqDesc.Text = rmk;
                radButton_SaveSQ.Enabled = false;

                radTextBox_Update.Focus();
            }

        }
        //Update Cn Send
        private void RadButton_CnSaveSend_Click(object sender, EventArgs e)
        {
            if (checkBox_CNSend.Checked == false) { return; }

            if ((radTextBox_CNWhoSend.Text == "") ||
                (radTextBox_CNSendBy.Text == "") || (radTextBox_CNSendBill.Text == "") ||
                (radTextBox_CnSendRmk.Text == ""))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูลการส่งสินค้า");
                radTextBox_CnSendRmk.Focus();
                return;
            }

            if ((MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(_pBillNo, "ยืนยันการบันทึกข้อมูลการส่ง")) == DialogResult.No)
            {
                return;
            }

            string rmk = radTextBox_CnSendRmk.Text + Environment.NewLine +
                " -> BY " + SystemClass.SystemUserName + Environment.NewLine +
                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss") + "]";
            string sqlUp = $@" UPDATE   SHOP_JOBCLAIM 
                               SET      STA_SEND = '1', 
                                        Transport_SEND = '{radTextBox_CNWhoSend.Text}', 
                                        TypeTransport_SEND = '{radTextBox_CNSendBy.Text}',
                                        Track_SEND = '{radTextBox_CNSendBill.Text}', 
                                        REMARK_SEND = '{rmk}',
                                        DATE_SEND = GETDATE() , 
                                        WHOID_SEND = '{SystemClass.SystemUserID}', 
                                        WHONAME_SEND = '{SystemClass.SystemUserName }' 
                               WHERE Claim_ID = '{_pBillNo}'";
            string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                pChange = "1";
                checkBox_CNSend.Enabled = false;

                radTextBox_CNWhoSend.Enabled = false;
                radTextBox_CNSendBy.Enabled = false;
                radTextBox_CNSendBill.Enabled = false;
                radTextBox_CnSendRmk.Enabled = false; radTextBox_CnSendRmk.Text = rmk;

                radButton_CnSaveSend.Enabled = false;

                radTextBox_Update.Focus();
            }
        }

        private void RadButton_CNSaveRecive_Click(object sender, EventArgs e)
        {
            if (checkBox_CnRecive.Checked == false) { return; }

            if (radTextBox_CNReciveDesc.Text == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูลการรับคืนสินค้า");
                radTextBox_CNReciveDesc.Focus();
                return;
            }

            if ((MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(_pBillNo, "ยืนยันการบันทึกรับคืนสินค้า")) == DialogResult.No)
            {
                return;
            }

            string rmk = "- " + radTextBox_CNReciveDesc.Text + Environment.NewLine +
                " -> BY " + SystemClass.SystemUserName + Environment.NewLine +
                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss") + "]";

            string sqlUp = $@" UPDATE   SHOP_JOBCLAIM 
                               SET      STA_RECIVECN = '1', 
                                        REMARK_RECIVECN = '{rmk}',
                                        DATE_RECIVECN = GETDATE() , 
                                        WHOID_RECIVECN = '{SystemClass.SystemUserID}', 
                                        WHONAME_RECIVECN = '{SystemClass.SystemUserName}' 
                               WHERE    Claim_ID = '{_pBillNo}' ";
            string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                pChange = "1";
                checkBox_CnRecive.Enabled = false;
                radTextBox_CNReciveDesc.Enabled = false; radTextBox_CNReciveDesc.Text = rmk;
                radButton_CNSaveRecive.Enabled = false;
                radTextBox_Update.Focus();
            }
        }
        //Close JOB
        private void CheckBox_ReciveDpt_CheckedChanged(object sender, EventArgs e)
        {
            if (_pPermission == "0") { return; }

            if (checkBox_ReciveDpt.Checked == true)
            {
                radTextBox_Update.Focus();
            }
            else
            {
                radTextBox_Update.Focus();
            }

        }
        //Update JOB
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_Update.Text == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียด");
                radTextBox_Update.Focus();
                return;
            }


            string desc = radTextBox_Desc.Text + Environment.NewLine + Environment.NewLine +
                   "- " + radTextBox_Update.Text + Environment.NewLine + " " +
                "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss") + "]";
            string pStaRecive = "0"; string recive_Dec = "";

            if (checkBox_ReciveDpt.Checked == true)
            {
                pStaRecive = "1";
                recive_Dec = "- " + radTextBox_Update.Text + Environment.NewLine + " " +
                "-> BY " + SystemClass.SystemUserID + " " + Environment.NewLine +
                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] [" + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss") + "]";
            }

            if ((pStaRecive == "1") && (radTextBox_Item.Text != ""))
            {
                //DataTable dt = AssetClass.FindAssetByID(radTextBox_Item.Text);
                DataTable dt = Models.AssetClass.FindAsset_ByAllType(radTextBox_Item.Text, "", "");
                if (dt.Rows.Count > 0)
                {
                    //string tAx = JOBClass.SendAX_AssetCodition(radTextBox_Item.Text, "ใช้งาน");
                    string tAx = AX_SendData.Save_EXTERNALLIST_AssetCodition(radTextBox_Item.Text, "ใช้งาน");
                    if (tAx != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(tAx);
                        return;
                    }
                }
            }

            string sqlUp = $@" UPDATE   SHOP_JOBCLAIM 
                               SET      RECIVE_STA = '{ pStaRecive}', 
                                        RECIVE_DESC = '{ recive_Dec }',
                                        DESCRIPTION = '{ desc }',
                                        RECIVE_DATE = GETDATE() , 
                                        RECIVE_WHOID = '{ SystemClass.SystemUserID }', 
                                        RECIVE_WHONAME = '{ SystemClass.SystemUserName }' 
                                WHERE   Claim_ID = '{ _pBillNo }'";

            string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                pChange = "1";
                this.DialogResult = DialogResult.OK;
                this.Close();
                return;
            }

        }
        //Send CN
        private void RadButton_SendCN_Click(object sender, EventArgs e)
        {
            if ((MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(_pBillNo, "ยืนยันการบันทึกส่ง CN")) == DialogResult.No)
            {
                return;
            }
            string sqlUp = $@" UPDATE SHOP_JOBCLAIM 
                               SET    DATE_SENDCN = GETDATE()  
                               WHERE  Claim_ID = '{ _pBillNo }'";

            string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                pChange = "1";
                radButton_SendCN.Enabled = false;
                radButton_SendCN.Text = DateTime.Now.ToString("yyyy-MM-dd");
                radTextBox_Update.Focus();
            }

        }
        //print
        private void RadButton_CNPrint_Click(object sender, EventArgs e)
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                printDocument1.Print();

            }
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = _pBillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            e.Graphics.DrawString("เอกสารส่งซ่อมอุปกรณ์", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับผิดชอบ - " + _pGroupDesc, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);


            Y += 20;
            e.Graphics.DrawString("(" + radTextBox_Qty.Text + " X " +
                              radTextBox_Unit.Text + ")   " +
                              radTextBox_Item.Text,
                 SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString(radTextBox_ItemName.Text,
                SystemClass.printFont, Brushes.Black, 10, Y);
            if (radLabel_SN.Text != "")
            {
                Y += 15;
                e.Graphics.DrawString("S/N " + radLabel_SN.Text,
                    SystemClass.printFont, Brushes.Black, 10, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("ส่งซ่อมบริษัท " + radLabel_Vender.Text, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + whoIDIns + "-" + whoNameIns, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            if (radTextBox_Item.Text != "")
            {
                Y += 20;
                e.Graphics.DrawString(" ********** ส่งซ่อมครั้งที่ " + Convert.ToString(radGridView_ShowHistory.Rows.Count) + " ********** ", SystemClass.printFont, Brushes.Black, 0, Y);
            }
            Y += 30;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            StringFormat stringFormat = new StringFormat
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(descPrint, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }

        private void RadButton_printImg_Click(object sender, EventArgs e)
        {
            if (_pTypeBill == "1") { return; }//กรณีทำทิ้ง ไม่อนุญาติให้ print

            int cImg = 0;
            if (radGridView_ShowImage.Rows[0].Cells["Bill_SPC_Count"].Value.ToString() != "0")
            { cImg += 1; }
            if (radGridView_ShowImage.Rows[0].Cells["Bill_MN_Count"].Value.ToString() != "0")
            { cImg += 1; }
            if (radGridView_ShowImage.Rows[0].Cells["Bill_END_Count"].Value.ToString() != "0")
            { cImg += 1; }

            if (cImg != 3)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถใช้งานฟังก์ชั่นพิมพ์ได้ เนื่องจากจำนวนรูปไม่ครบตามข้อกำหนดที่ระบุ [3 รูป]" + Environment.NewLine +
                    "[เช็ครูปถ่ายทั้งหมดใหม่อีกครั้ง]");
                return;
            }

            string staPXE, pxeDesc= "";
            if (pxe != "")
            {
                staPXE = pxe;
                pxeDesc = pxe;
            }
            else
            {
                if (radioButton_Sq.Checked == true)
                {
                    staPXE = "1";
                    pxeDesc = "1";
                }
                else
                {
                    staPXE = "0";
                    if (_pTypeBill == "0")
                    { pxeDesc = "ไม่มีค่าใช้จ่าย"; }
                    else if (_pTypeBill == "2")
                    { pxeDesc = "ส่งซ่อมลูกค้า คอมพิวเตอร์"; }
                    else if (_pTypeBill == "3")
                    { pxeDesc = "ส่งซ่อมลูกค้า มือถือ"; }
                }
            }



            Claim_DescPrint _print = new Claim_DescPrint(_pBillNo, staPXE, pxeDesc, vender,
                radGridView_ShowImage.Rows[0].Cells["Bill_SPC_Path"].Value.ToString(),
                radGridView_ShowImage.Rows[0].Cells["Bill_MN_Path"].Value.ToString(),
                radGridView_ShowImage.Rows[0].Cells["Bill_END_Path"].Value.ToString());
            _print.Show();

        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {

            Controllers.FormClass.Document_Check(this.Name, _pTypeBill);
        }

        private void RadButton_ExcelHistory_Click(object sender, EventArgs e)
        {
            if (radGridView_ShowHistory.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("ประวัติการส่งซ่อม", radGridView_ShowHistory, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }

}

