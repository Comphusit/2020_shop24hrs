﻿namespace PC_Shop24Hrs.JOB.Claim
{
    partial class Claim_DescPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Claim_DescPrint));
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radStatusStrip_Main = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Pxe = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radStatusStrip_Main
            // 
            this.radStatusStrip_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip_Main.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip_Main.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Pxe,
            this.commandBarSeparator5,
            this.radButtonElement_Print,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3});
            this.radStatusStrip_Main.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip_Main.Name = "radStatusStrip_Main";
            this.radStatusStrip_Main.Size = new System.Drawing.Size(883, 36);
            this.radStatusStrip_Main.TabIndex = 3;
            // 
            // radButtonElement_Pxe
            // 
            this.radButtonElement_Pxe.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Pxe.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Pxe.Name = "radButtonElement_Pxe";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Pxe, false);
            this.radButtonElement_Pxe.Text = "radButtonElement1";
            this.radButtonElement_Pxe.Click += new System.EventHandler(this.RadButtonElement_Pxe_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Print
            // 
            this.radButtonElement_Print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_Print.Name = "radButtonElement_Print";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Print, false);
            this.radButtonElement_Print.Text = "radButtonElement1";
            this.radButtonElement_Print.ToolTipText = "เพิ่ม";
            this.radButtonElement_Print.UseCompatibleTextRendering = false;
            this.radButtonElement_Print.Click += new System.EventHandler(this.RadButtonElement_Print_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.commandBarSeparator2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(883, 660);
            this.radGridView_Show.TabIndex = 2;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radStatusStrip_Main, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(889, 714);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip_Main.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.Text = "";
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // Claim_DescPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 714);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Claim_DescPrint";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายละเอียดรูปประกอบการส่งซ่อม";
            this.Load += new System.EventHandler(this.Claim_DescPrint_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Main;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Pxe;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
    }
}
