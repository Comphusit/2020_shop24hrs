﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;

namespace PC_Shop24Hrs.JOB.Claim
{
    public partial class Bill_Techout : Telerik.WinControls.UI.RadForm
    {
        private DataTable dt = new DataTable();
        readonly string _pTypeOpen;//SHOP-SUPC
        readonly string _pPrmission;// 0- ไม่มีสิด 1 - มีสิด

        string docno;
        //Load
        public Bill_Techout(string pTypeOpen, string pPrmission)
        {
            InitializeComponent();

            _pTypeOpen = pTypeOpen;
            _pPrmission = pPrmission;
        }
        //Load Main
        private void Bill_Techout_Load(object sender, EventArgs e)
        {
            radDateTimePicker_Bill.Value = DateTime.Now.AddDays(-7);
            radDateTimePicker1.Value = DateTime.Now.AddDays(0);

            radButton_Print.ShowBorder = true; radButton_Print.ToolTipText = "พิมพ์รูปภาพ";
            radButton_refresh.ShowBorder = true; radButton_refresh.ToolTipText = "ดึงข้อมูลใหม่";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            if (SystemClass.SystemBranchID == "MN000") radLabel_Detail.Text = "DoubleClick เลขที่บิลผู้จำหน่าย >> ยกเลิกทั้งรายการ  [ยกเลิกได้เฉพาะรายการที่ยังไม่มี MA เท่านั้น]";
            else radLabel_Detail.Text = "ต้องการยกเลิกบิลทั้งรายการ ติดต่อ CenterShop Tel.1022 [ยกเลิกได้เฉพาะรายการที่ยังไม่มี MA เท่านั้น]";


            radStatusStrip.SizingGrip = false;
            radButtonElement_OK.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchBill", "สาขา", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BillTrans", "เลขที่บิลรับ", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Docno", "เลขที่บิลผู้จำหน่าย", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RDateIn", "วันที่บิล", 110));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RemarkBill", "ผู้จำหน่าย", 500));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EmpReceive", "ผู้รับของ", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PXE", "เอกสาร PXE", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Numofprint", "ครั้งที่พิมพ์", 80));

            DatagridClass.SetDefaultRadGridView(radGridView_Image);
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESC", "คำอธิบาย", 120));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG1", "C_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG2", "C_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG3", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG3", "P_IMG3"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG3", "C_IMG3"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG4", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG4", "P_IMG4"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG4", "C_IMG4"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG5", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG5", "P_IMG5"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG5", "C_IMG5"));
            radGridView_Image.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Image.TableElement.RowHeight = 120;
            radGridView_Image.TableElement.TableHeaderHeight = 50;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Bill, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker1, DateTime.Now, DateTime.Now);
        }
        //Set Valus
        void SetDGV()
        {
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();

            this.Cursor = Cursors.WaitCursor;
            string pCon = "";
            if (_pTypeOpen == "SHOP") pCon = " AND BranchBill = '" + SystemClass.SystemBranchID + @"' ";

            string sql = $@"
                    select  Docno, EmpIDReceive + '-' + EmplNameReceive AS EmpReceive, Price, PrintBill,BranchBill,BRANCHNAME as  BranchName,  
		                    TypeBill,VenderName AS RemarkBill, BillTrans,CONVERT(varchar,RDateIn,23) AS RDateIn,ISNULL(StatusMP,0) AS Numofprint  ,'' AS PXE
                    FROM	Shop_ReciveDocument WITH (NOLOCK) 
                    WHERE	Rtype ='3'	AND typeBill='J' 
		                    AND RDateIn between '{radDateTimePicker_Bill.Value:yyyy-MM-dd}'  AND '{ radDateTimePicker1.Value:yyyy-MM-dd}' {pCon}
                    ORDER BY CONVERT(varchar,RDateIn,23) DESC ";
            dt = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string pxe = "";

                DataTable dtPXE = PurchClass.CheckStatusAX_SPC_SHIPCARRIERINVOICE(dt.Rows[i]["BillTrans"].ToString());  //ConnectionClass.SelectSQL_Main(sql0);
                if (dtPXE.Rows.Count > 0) pxe = dtPXE.Rows[0]["PURCHID"].ToString();

                RadGridView_Show.Rows[i].Cells["PXE"].Value = pxe;
            }

            if (RadGridView_Show.Rows.Count > 0)
            {
                RadGridView_Show.Rows[0].IsSelected = true;

                FindImage(RadGridView_Show.CurrentRow.Cells["RDateIn"].Value.ToString(),
                    RadGridView_Show.CurrentRow.Cells["BranchBill"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString());

                docno = RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString();
            }



            this.Cursor = Cursors.Default;
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //OK
        private void RadButtonElement_OK_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Selection Change
        private void RadGridView_Show_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            if (RadGridView_Show.CurrentRow.Index == -1) return;

            if (docno == RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString()) return;
            string pxe = "";
            try
            {
                pxe = RadGridView_Show.CurrentRow.Cells["PXE"].Value.ToString();
            }
            catch (Exception) { }

            try
            {
                radGridView_Image.Columns["DESC"].HeaderText = "พิมพ์ครั้งที่ " + Environment.NewLine +
                    (Convert.ToInt32(RadGridView_Show.CurrentRow.Cells["Numofprint"].Value.ToString()) + 1).ToString()
                    + Environment.NewLine + pxe;
                radGridView_Image.Columns["IMG1"].HeaderText = "สาขา " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["BranchBill"].Value.ToString() +
                    "-" + RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString();
                radGridView_Image.Columns["IMG2"].HeaderText = "เลขที่บิลรับ " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString();
                radGridView_Image.Columns["IMG3"].HeaderText = "เลขที่บิลผู้จำหน่าย  " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString();
                radGridView_Image.Columns["IMG4"].HeaderText = "ผู้จำหน่าย  " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["RemarkBill"].Value.ToString();
                radGridView_Image.Columns["IMG5"].HeaderText = "ผู้รับ  " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["EmpReceive"].Value.ToString();

                FindImage(RadGridView_Show.CurrentRow.Cells["RDateIn"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["BranchBill"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString());

                docno = RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString();
            }
            catch (Exception)
            {
                if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();
                return;
            }

        }
        //Find Image
        void FindImage(string pDate, string pBchID, string pBill)
        {
            radButton_Print.Enabled = false;


            if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();

            DirectoryInfo DirInfo = new DirectoryInfo(PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID);
            if (DirInfo.Exists == false) return;

            FileInfo[] FilesA = DirInfo.GetFiles("Sanyo-" + pBchID + @"*" + pBill + @"_A.JPG", SearchOption.AllDirectories);
            FileInfo[] FilesB = DirInfo.GetFiles("Sanyo-" + pBchID + @"*" + pBill + @"_B.JPG", SearchOption.AllDirectories);
            FileInfo[] FilesC = DirInfo.GetFiles("Sanyo-" + pBchID + @"*" + pBill + @"_C.JPG", SearchOption.AllDirectories);
            FileInfo[] FilesO = DirInfo.GetFiles("Sanyo-" + pBchID + @"*" + pBill + @"_O.JPG", SearchOption.AllDirectories);
            FileInfo[] FilesR = DirInfo.GetFiles("Sanyo-" + pBchID + @"*" + pBill + @"_R.JPG", SearchOption.AllDirectories);

            FileInfo[] FilesI = DirInfo.GetFiles("Sanyo-" + pBchID + @"*" + pBill + @"_I.JPG", SearchOption.AllDirectories);

            int A = FilesA.Length;
            if (A > 5) { A = 5; }
            switch (A)
            {
                case 1:
                    radGridView_Image.Rows.Add("ก่อนเปลี่ยน",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[0].Name, "1");
                    break;
                case 2:
                    radGridView_Image.Rows.Add("ก่อนเปลี่ยน",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[0].Name, "1",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[1].Name, "1");
                    break;
                case 3:
                    radGridView_Image.Rows.Add("ก่อนเปลี่ยน",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[2].Name, "1");
                    break;
                case 4:
                    radGridView_Image.Rows.Add("ก่อนเปลี่ยน",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[2].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[3].Name, "1");
                    break;
                case 5:
                    radGridView_Image.Rows.Add("ก่อนเปลี่ยน",
                          ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[0].Name, "1",
                          ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[1].Name, "1",
                          ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[2].Name, "1",
                          ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[3].Name, "1",
                          ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesA[4].FullName), PathImageClass.pPathRepairDevice + pDate + @"\" + pBchID + @"|" + FilesA[4].Name, "1");
                    break;
                default: break;
            }

            int B = FilesB.Length;
            if (B > 5) { B = 5; }
            switch (B)
            {
                case 1:
                    radGridView_Image.Rows.Add("ขณะเปลี่ยน",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[0].Name, "1");
                    break;
                case 2:
                    radGridView_Image.Rows.Add("ขณะเปลี่ยน",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[0].Name, "1",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[1].Name, "1");
                    break;
                case 3:
                    radGridView_Image.Rows.Add("ขณะเปลี่ยน",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[2].Name, "1");
                    break;
                case 4:
                    radGridView_Image.Rows.Add("ขณะเปลี่ยน",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[2].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[3].Name, "1");
                    break;
                case 5:
                    radGridView_Image.Rows.Add("ขณะเปลี่ยน",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[0].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[1].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[2].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[3].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesB[4].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesB[4].Name, "1");
                    break;
                default: break;
            }

            int C = FilesC.Length;
            if (C > 5) { C = 5; }
            switch (C)
            {
                case 1:
                    radGridView_Image.Rows.Add("หลังเปลี่ยน",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[0].Name, "1");
                    break;
                case 2:
                    radGridView_Image.Rows.Add("หลังเปลี่ยน",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[0].Name, "1",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[1].Name, "1");
                    break;
                case 3:
                    radGridView_Image.Rows.Add("หลังเปลี่ยน",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[2].Name, "1");
                    break;
                case 4:
                    radGridView_Image.Rows.Add("หลังเปลี่ยน",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[2].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[3].Name, "1");
                    break;
                case 5:
                    radGridView_Image.Rows.Add("หลังเปลี่ยน",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[0].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[1].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[2].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[3].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesC[4].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesC[4].Name, "1");
                    break;
                default: break;
            }

            int O = FilesO.Length;
            if (O > 5) { O = 5; }
            switch (O)
            {
                case 1:
                    radGridView_Image.Rows.Add("อะไหล่",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[0].Name, "1");
                    break;
                case 2:
                    radGridView_Image.Rows.Add("อะไหล่",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[0].Name, "1",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[1].Name, "1");
                    break;
                case 3:
                    radGridView_Image.Rows.Add("อะไหล่",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[2].Name, "1");
                    break;
                case 4:
                    radGridView_Image.Rows.Add("อะไหล่",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[2].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[3].Name, "1");
                    break;
                case 5:
                    radGridView_Image.Rows.Add("อะไหล่",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[0].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[1].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[2].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[3].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesO[4].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesO[4].Name, "1");
                    break;
                default: break;
            }


            int R = FilesR.Length;
            if (R > 5) { R = 5; }
            switch (R)
            {
                case 1:
                    radGridView_Image.Rows.Add("ซาก",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[0].Name, "1");
                    break;
                case 2:
                    radGridView_Image.Rows.Add("ซาก",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[0].Name, "1",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[1].Name, "1");
                    break;
                case 3:
                    radGridView_Image.Rows.Add("ซาก",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[2].Name, "1");
                    break;
                case 4:
                    radGridView_Image.Rows.Add("ซาก",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[2].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[3].Name, "1");
                    break;
                case 5:
                    radGridView_Image.Rows.Add("ซาก",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[0].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[1].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[2].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[3].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesR[4].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesR[4].Name, "1");
                    break;
                default: break;
            }

            int I = FilesI.Length;
            if (I > 5) { I = 5; }
            switch (I)
            {
                case 1:
                    radGridView_Image.Rows.Add("ภาพตรวจตัว",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[0].Name, "1");
                    break;
                case 2:
                    radGridView_Image.Rows.Add("ภาพตรวจตัว",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[0].Name, "1",
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[1].Name, "1");
                    break;
                case 3:
                    radGridView_Image.Rows.Add("ภาพตรวจตัว",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[2].Name, "1");
                    break;
                case 4:
                    radGridView_Image.Rows.Add("ภาพตรวจตัว",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[0].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[1].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[2].Name, "1",
                       ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[3].Name, "1");
                    break;
                case 5:
                    radGridView_Image.Rows.Add("ภาพตรวจตัว",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[0].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[0].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[1].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[1].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[2].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[2].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[3].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[3].Name, "1",
                      ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesI[4].FullName), PathImageClass.pPathRepairDevice + pDate + @"|" + FilesI[4].Name, "1");
                    break;
                default: break;
            }

            if (_pPrmission == "1") radButton_Print.Enabled = true;
        }
        //print
        private void RadButton_Print_Click(object sender, EventArgs e)
        {
            if (radGridView_Image.Rows.Count == 0) return;

            if (RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เลขที่บิล {RadGridView_Show.CurrentRow.Cells["Docno"].Value} ยังไม่มีการยืนยันรับจากสาขา {Environment.NewLine} ไม่สามารถพิมพ์ได้.");
                return;
            }

            if (Convert.ToInt32(RadGridView_Show.CurrentRow.Cells["Numofprint"].Value.ToString()) == 0)
            {
                string chkSql = $@"
                            Select  ISNULL([Docno],'') AS [Docno], Remark 
                            from    [Shop_DeviceRepair] WITH (NOLOCK) 
                            WHERE	[Docno] = '{RadGridView_Show.CurrentRow.Cells["Docno"].Value}' 
                            Group by [Docno],Remark  ";
                DataTable dtChk = ConnectionClass.SelectSQL_Main(chkSql);

                FormShare.InputData _inputData = new FormShare.InputData("1",
                    RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString() + " เลขที่ผู้จำหน่าย " + RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString() +
                    Environment.NewLine + RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString(),
                     "หมายเหตุการซ่อม", "");

                if (_inputData.ShowDialog(this) == DialogResult.Yes)
                {
                    String UpStr;
                    string strUp = _inputData.pInputData + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                                   " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

                    if (dtChk.Rows.Count > 0)
                    {
                        UpStr = string.Format(@"
                                        Update  Shop_DeviceRepair  
                                        SET     Remark = '" + strUp + @"'+
                                                CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']' ,
                                                WhoUp = '" + SystemClass.SystemUserID + @"',DateUp = CONVERT(VARCHAR,GETDATE(),23),
                                                TimeUp = CONVERT(VARCHAR,GETDATE(),24) 
                                        WHERE   Docno = '" + RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString() + @"'  ");
                    }
                    else
                    {
                        UpStr = string.Format(@"
                                        INSERT INTO  Shop_DeviceRepair  
                                        (Docno,SeqNo,BranchID,AssetName,Remark,WhoIns)  VALUES (
                                        '" + RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString() + @"','1',
                                        '" + RadGridView_Show.CurrentRow.Cells["BranchBill"].Value.ToString() + @"','ระบุอุปกรณ์ไม่มีทะเบียน','" + strUp + @"',
                                        '" + SystemClass.SystemUserID + @"')
                                        ");
                    }

                    string T = ConnectionClass.ExecuteSQL_Main(UpStr);
                    if (T != "") { MsgBoxClass.MsgBoxShow_SaveStatus(T); return; }
                }
                else return;
            }

            radGridView_Image.PrintPreview();
            int iR = Convert.ToInt32(RadGridView_Show.CurrentRow.Cells["Numofprint"].Value.ToString()) + 1;
            string upPrint = string.Format(@"
                    UPDATE  SHOP_RECIVEDOCUMENT 
                    SET     StatusMP = '" + iR.ToString() + @"'
                    WHERE   Docno = '" + RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString() + @"' 
                            AND BillTrans = '" + RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString() + @"'
                ");
            string Ta = ConnectionClass.ExecuteSQL_Main(upPrint);
            if (Ta == "") RadGridView_Show.CurrentRow.Cells["Numofprint"].Value = iR.ToString();
            return;
        }
        //Image Set Format
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //refresh
        private void RadButton_refresh_Click(object sender, EventArgs e)
        {
            //SetDGV();
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();

            if (radGridView_Image.Rows.Count > 0)
            {
                radGridView_Image.Rows.Clear();
                radGridView_Image.Columns["IMG1"].HeaderText = "สาขา";
                radGridView_Image.Columns["IMG2"].HeaderText = "เลขที่บิลรับ";
                radGridView_Image.Columns["IMG3"].HeaderText = "เลขที่บิลผู้จำหน่าย";
                radGridView_Image.Columns["IMG4"].HeaderText = "ผู้จำหน่าย";
                radGridView_Image.Columns["IMG5"].HeaderText = "ผู้รับ";
            }

        }
        //DoubleClick
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "IMG1":
                    try
                    {
                        ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG1"].Value.ToString(),
                           radGridView_Image.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    }
                    catch (Exception) { }
                    break;
                case "IMG2":
                    try
                    {
                        ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG2"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG2"].Value.ToString());
                    }
                    catch (Exception) { }
                    break;
                case "IMG3":
                    try
                    {
                        ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG3"].Value.ToString(),
                                                 radGridView_Image.CurrentRow.Cells["P_IMG3"].Value.ToString());
                    }
                    catch (Exception) { }
                    break;
                case "IMG4":
                    try
                    {
                        ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG4"].Value.ToString(),
                          radGridView_Image.CurrentRow.Cells["P_IMG4"].Value.ToString());
                    }
                    catch (Exception) { }
                    break;
                case "IMG5":
                    try
                    {
                        ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG5"].Value.ToString(),
                           radGridView_Image.CurrentRow.Cells["P_IMG5"].Value.ToString());
                    }
                    catch (Exception) { }
                    break;
                default:
                    break;
            }
        }

        private void RadDateTimePicker_Bill_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Bill, radDateTimePicker1);
        }

        private void RadDateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Bill, radDateTimePicker1);
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column.Name != "Docno") return;

            if (SystemClass.SystemBranchID != "MN000") return;
            if (RadGridView_Show.Rows.Count == 0) return;
            string docno = RadGridView_Show.CurrentRow.Cells["Docno"].Value.ToString();
            if (docno == "") return;

            if (RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString() != "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถยกเลิกบิลได้เนื่องจากมีออกบิล MA เรียบร้อยแล้ว");
                return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ต้องการยกเลิกเลขที่บิล {docno} {Environment.NewLine}สาขา {RadGridView_Show.CurrentRow.Cells["BranchBill"].Value} {RadGridView_Show.CurrentRow.Cells["BranchName"].Value}?") == DialogResult.No) return;


            string sqlUp = $@"
                UPDATE	[SHOP_RECIVEDOCUMENT]	
                SET     TypeBill = 'JJ',BranchBill = BranchBill+'0',Docno = Docno+'_0'	,
                        RDateUp = CONVERT(VARCHAR,GETDATE(),23),RTimeUp = CONVERT(VARCHAR,GETDATE(),24),RWhoUp = '{SystemClass.SystemUserID}'
                WHERE	Docno = '{docno}'
            ";

            string resualt = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "") SetDGV();

        }
    }
}
