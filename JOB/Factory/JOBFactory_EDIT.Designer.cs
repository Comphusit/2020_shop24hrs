﻿namespace PC_Shop24Hrs.JOB.Factory
{
    partial class JOBFactory_EDIT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JOBFactory_EDIT));
            this.radGroupBox_Desc = new Telerik.WinControls.UI.RadGroupBox();
            this.radTextBox_Machine = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Update = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radButton_OpenView = new Telerik.WinControls.UI.RadButton();
            this.RadButton_OpenAdd = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_STA = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_TECH = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_DptID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_DptName = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel_TECH = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_branch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Machine = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_STACLOSE = new System.Windows.Forms.CheckBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox_Bill = new Telerik.WinControls.UI.RadGroupBox();
            this.button_MNIO_stockID = new System.Windows.Forms.Button();
            this.radGridView_ShowImg = new Telerik.WinControls.UI.RadGridView();
            this.Button_AX = new System.Windows.Forms.Button();
            this.Button_MNIO = new System.Windows.Forms.Button();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f = new Telerik.WinControls.RootRadElement();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).BeginInit();
            this.radGroupBox_Desc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Machine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OpenAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_STA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_TECH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DptID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_TECH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Machine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Bill)).BeginInit();
            this.radGroupBox_Bill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImg.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_Desc
            // 
            this.radGroupBox_Desc.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Machine);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Update);
            this.radGroupBox_Desc.Controls.Add(this.radButton_pdt);
            this.radGroupBox_Desc.Controls.Add(this.radButton_OpenView);
            this.radGroupBox_Desc.Controls.Add(this.RadButton_OpenAdd);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_STA);
            this.radGroupBox_Desc.Controls.Add(this.radLabel6);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_TECH);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_DptID);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_DptName);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Save);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_TECH);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_branch);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_Machine);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Desc);
            this.radGroupBox_Desc.Controls.Add(this.radCheckBox_STACLOSE);
            this.radGroupBox_Desc.Controls.Add(this.radLabel5);
            this.radGroupBox_Desc.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Desc.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล";
            this.radGroupBox_Desc.Location = new System.Drawing.Point(3, 4);
            this.radGroupBox_Desc.Name = "radGroupBox_Desc";
            this.radGroupBox_Desc.Size = new System.Drawing.Size(551, 664);
            this.radGroupBox_Desc.TabIndex = 26;
            this.radGroupBox_Desc.Text = "รายละเอียดข้อมูล";
            // 
            // radTextBox_Machine
            // 
            this.radTextBox_Machine.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_Machine.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Machine.Location = new System.Drawing.Point(91, 62);
            this.radTextBox_Machine.Multiline = true;
            this.radTextBox_Machine.Name = "radTextBox_Machine";
            // 
            // 
            // 
            this.radTextBox_Machine.RootElement.StretchVertically = true;
            this.radTextBox_Machine.Size = new System.Drawing.Size(336, 27);
            this.radTextBox_Machine.TabIndex = 82;
            // 
            // radTextBox_Update
            // 
            this.radTextBox_Update.AcceptsReturn = true;
            this.radTextBox_Update.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Update.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_Update.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Update.Location = new System.Drawing.Point(9, 483);
            this.radTextBox_Update.Multiline = true;
            this.radTextBox_Update.Name = "radTextBox_Update";
            // 
            // 
            // 
            this.radTextBox_Update.RootElement.StretchVertically = true;
            this.radTextBox_Update.Size = new System.Drawing.Size(532, 134);
            this.radTextBox_Update.TabIndex = 0;
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(512, 21);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 73;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radButton_OpenView
            // 
            this.radButton_OpenView.BackColor = System.Drawing.Color.Transparent;
            this.radButton_OpenView.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_OpenView.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.radButton_OpenView.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_OpenView.Location = new System.Drawing.Point(512, 124);
            this.radButton_OpenView.Name = "radButton_OpenView";
            // 
            // 
            // 
            this.radButton_OpenView.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_OpenView.Size = new System.Drawing.Size(26, 26);
            this.radButton_OpenView.TabIndex = 54;
            this.radButton_OpenView.Click += new System.EventHandler(this.RadButton_OpenView_Click);
            // 
            // RadButton_OpenAdd
            // 
            this.RadButton_OpenAdd.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_OpenAdd.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_OpenAdd.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.RadButton_OpenAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_OpenAdd.Location = new System.Drawing.Point(482, 124);
            this.RadButton_OpenAdd.Name = "RadButton_OpenAdd";
            // 
            // 
            // 
            this.RadButton_OpenAdd.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.RadButton_OpenAdd.Size = new System.Drawing.Size(26, 26);
            this.RadButton_OpenAdd.TabIndex = 53;
            this.RadButton_OpenAdd.Click += new System.EventHandler(this.RadButton_OpenAdd_Click);
            // 
            // radDropDownList_STA
            // 
            this.radDropDownList_STA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_STA.BackColor = System.Drawing.Color.White;
            this.radDropDownList_STA.DropDownAnimationEnabled = false;
            this.radDropDownList_STA.DropDownHeight = 124;
            this.radDropDownList_STA.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_STA.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_STA.Location = new System.Drawing.Point(91, 126);
            this.radDropDownList_STA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_STA.Name = "radDropDownList_STA";
            // 
            // 
            // 
            this.radDropDownList_STA.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_STA.Size = new System.Drawing.Size(148, 25);
            this.radDropDownList_STA.TabIndex = 47;
            this.radDropDownList_STA.Text = "radDropDownList2";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_STA.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_STA.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList2";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel6.Location = new System.Drawing.Point(479, 97);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(48, 23);
            this.radLabel6.TabIndex = 40;
            this.radLabel6.Text = "รูปเปิด";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radDropDownList_TECH
            // 
            this.radDropDownList_TECH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_TECH.BackColor = System.Drawing.Color.White;
            this.radDropDownList_TECH.DropDownAnimationEnabled = false;
            this.radDropDownList_TECH.DropDownHeight = 124;
            this.radDropDownList_TECH.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_TECH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_TECH.Location = new System.Drawing.Point(91, 96);
            this.radDropDownList_TECH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_TECH.Name = "radDropDownList_TECH";
            // 
            // 
            // 
            this.radDropDownList_TECH.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_TECH.Size = new System.Drawing.Size(336, 25);
            this.radDropDownList_TECH.TabIndex = 34;
            this.radDropDownList_TECH.Text = "radDropDownList1";
            this.radDropDownList_TECH.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Reapair_SelectedValueChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_TECH.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_TECH.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_TECH.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radTextBox_DptID
            // 
            this.radTextBox_DptID.Enabled = false;
            this.radTextBox_DptID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_DptID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_DptID.Location = new System.Drawing.Point(91, 31);
            this.radTextBox_DptID.MaxLength = 3;
            this.radTextBox_DptID.Name = "radTextBox_DptID";
            this.radTextBox_DptID.Size = new System.Drawing.Size(69, 25);
            this.radTextBox_DptID.TabIndex = 3;
            this.radTextBox_DptID.Text = "012";
            // 
            // radLabel_DptName
            // 
            this.radLabel_DptName.AutoSize = false;
            this.radLabel_DptName.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radLabel_DptName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DptName.Location = new System.Drawing.Point(161, 31);
            this.radLabel_DptName.Name = "radLabel_DptName";
            this.radLabel_DptName.Size = new System.Drawing.Size(380, 23);
            this.radLabel_DptName.TabIndex = 18;
            this.radLabel_DptName.Text = "แผนก";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(255, 623);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(157, 623);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 1;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_TECH
            // 
            this.radLabel_TECH.AutoSize = false;
            this.radLabel_TECH.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel_TECH.Location = new System.Drawing.Point(9, 97);
            this.radLabel_TECH.Name = "radLabel_TECH";
            this.radLabel_TECH.Size = new System.Drawing.Size(86, 23);
            this.radLabel_TECH.TabIndex = 23;
            this.radLabel_TECH.Text = "ช่างผู้ซ่อม";
            // 
            // radLabel_branch
            // 
            this.radLabel_branch.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel_branch.Location = new System.Drawing.Point(11, 32);
            this.radLabel_branch.Name = "radLabel_branch";
            this.radLabel_branch.Size = new System.Drawing.Size(44, 22);
            this.radLabel_branch.TabIndex = 17;
            this.radLabel_branch.Text = "แผนก";
            // 
            // radLabel_Machine
            // 
            this.radLabel_Machine.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel_Machine.Location = new System.Drawing.Point(9, 64);
            this.radLabel_Machine.Name = "radLabel_Machine";
            this.radLabel_Machine.Size = new System.Drawing.Size(68, 22);
            this.radLabel_Machine.TabIndex = 20;
            this.radLabel_Machine.Text = "เครื่องจักร";
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Black;
            this.radTextBox_Desc.Location = new System.Drawing.Point(9, 158);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            this.radTextBox_Desc.ReadOnly = true;
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Desc.Size = new System.Drawing.Size(532, 291);
            this.radTextBox_Desc.TabIndex = 8;
            // 
            // radCheckBox_STACLOSE
            // 
            this.radCheckBox_STACLOSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radCheckBox_STACLOSE.AutoSize = true;
            this.radCheckBox_STACLOSE.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radCheckBox_STACLOSE.ForeColor = System.Drawing.Color.Black;
            this.radCheckBox_STACLOSE.Location = new System.Drawing.Point(5, 455);
            this.radCheckBox_STACLOSE.Name = "radCheckBox_STACLOSE";
            this.radCheckBox_STACLOSE.Size = new System.Drawing.Size(45, 22);
            this.radCheckBox_STACLOSE.TabIndex = 33;
            this.radCheckBox_STACLOSE.Text = "ปิด";
            this.radCheckBox_STACLOSE.UseVisualStyleBackColor = true;
            this.radCheckBox_STACLOSE.CheckedChanged += new System.EventHandler(this.RadCheckBox_STACLOSE_CheckedChanged);
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel5.Location = new System.Drawing.Point(11, 127);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(76, 23);
            this.radLabel5.TabIndex = 46;
            this.radLabel5.Text = "สถานะ";
            // 
            // radGroupBox_Bill
            // 
            this.radGroupBox_Bill.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Bill.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_Bill.Controls.Add(this.button_MNIO_stockID);
            this.radGroupBox_Bill.Controls.Add(this.radGridView_ShowImg);
            this.radGroupBox_Bill.Controls.Add(this.Button_AX);
            this.radGroupBox_Bill.Controls.Add(this.Button_MNIO);
            this.radGroupBox_Bill.Controls.Add(this.radGridView_Show);
            this.radGroupBox_Bill.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Bill.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Bill.HeaderText = "รูปเครื่องจักร";
            this.radGroupBox_Bill.Location = new System.Drawing.Point(560, 4);
            this.radGroupBox_Bill.Name = "radGroupBox_Bill";
            this.radGroupBox_Bill.Size = new System.Drawing.Size(645, 664);
            this.radGroupBox_Bill.TabIndex = 29;
            this.radGroupBox_Bill.Text = "รูปเครื่องจักร";
            // 
            // button_MNIO_stockID
            // 
            this.button_MNIO_stockID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            this.button_MNIO_stockID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_MNIO_stockID.ForeColor = System.Drawing.Color.Black;
            this.button_MNIO_stockID.Location = new System.Drawing.Point(314, 224);
            this.button_MNIO_stockID.Name = "button_MNIO_stockID";
            this.button_MNIO_stockID.Size = new System.Drawing.Size(150, 33);
            this.button_MNIO_stockID.TabIndex = 32;
            this.button_MNIO_stockID.Text = "MNIO สต็อกอะไหล่";
            this.button_MNIO_stockID.UseVisualStyleBackColor = false;
            this.button_MNIO_stockID.Click += new System.EventHandler(this.Button_MNIO_stockID_Click);
            // 
            // radGridView_ShowImg
            // 
            this.radGridView_ShowImg.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_ShowImg.Location = new System.Drawing.Point(2, 21);
            // 
            // 
            // 
            this.radGridView_ShowImg.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_ShowImg.Name = "radGridView_ShowImg";
            // 
            // 
            // 
            this.radGridView_ShowImg.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_ShowImg.Size = new System.Drawing.Size(640, 197);
            this.radGridView_ShowImg.TabIndex = 21;
            this.radGridView_ShowImg.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowImg_ViewCellFormatting);
            this.radGridView_ShowImg.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowImg_CellDoubleClick);
            // 
            // Button_AX
            // 
            this.Button_AX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.Button_AX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_AX.ForeColor = System.Drawing.Color.Black;
            this.Button_AX.Location = new System.Drawing.Point(2, 224);
            this.Button_AX.Name = "Button_AX";
            this.Button_AX.Size = new System.Drawing.Size(150, 33);
            this.Button_AX.TabIndex = 19;
            this.Button_AX.Text = "AX";
            this.Button_AX.UseVisualStyleBackColor = false;
            this.Button_AX.Click += new System.EventHandler(this.Button_AX_Click);
            // 
            // Button_MNIO
            // 
            this.Button_MNIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            this.Button_MNIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_MNIO.ForeColor = System.Drawing.Color.Black;
            this.Button_MNIO.Location = new System.Drawing.Point(158, 224);
            this.Button_MNIO.Name = "Button_MNIO";
            this.Button_MNIO.Size = new System.Drawing.Size(150, 33);
            this.Button_MNIO.TabIndex = 17;
            this.Button_MNIO.Text = "MNIO นำของออก";
            this.Button_MNIO.UseVisualStyleBackColor = false;
            this.Button_MNIO.Click += new System.EventHandler(this.Button_MNIO_Click);
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(2, 263);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(641, 399);
            this.radGridView_Show.TabIndex = 15;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f
            // 
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.AutoSize = false;
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.Name = "object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f";
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.StretchHorizontally = true;
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.StretchVertically = true;
            // 
            // JOBFactory_EDIT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(1215, 675);
            this.Controls.Add(this.radGroupBox_Bill);
            this.Controls.Add(this.radGroupBox_Desc);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JOBFactory_EDIT";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "แก้ไขข้อมูล.";
            this.Load += new System.EventHandler(this.JOBCar_EDIT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).EndInit();
            this.radGroupBox_Desc.ResumeLayout(false);
            this.radGroupBox_Desc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Machine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OpenAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_STA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_TECH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DptID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_TECH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Machine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Bill)).EndInit();
            this.radGroupBox_Bill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImg.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_ShowImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_DptID;
        private Telerik.WinControls.UI.RadLabel radLabel_DptName;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel_branch;
        private Telerik.WinControls.UI.RadLabel radLabel_Machine;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Update;
        private Telerik.WinControls.UI.RadLabel radLabel_TECH;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Bill;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        protected System.Windows.Forms.CheckBox radCheckBox_STACLOSE;
        private System.Windows.Forms.Button Button_MNIO;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_TECH;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.RootRadElement object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_STA;
        private System.Windows.Forms.Button Button_AX;
        private Telerik.WinControls.UI.RadButton radButton_OpenView;
        private Telerik.WinControls.UI.RadButton RadButton_OpenAdd;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Machine;
        private Telerik.WinControls.UI.RadGridView radGridView_ShowImg;
        private System.Windows.Forms.Button button_MNIO_stockID;
    }
}
