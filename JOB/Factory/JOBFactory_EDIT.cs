﻿
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.FormShare.ShowData;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowImage;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.GeneralForm.BillOut;
using System.IO;

namespace PC_Shop24Hrs.JOB.Factory
{
    public partial class JOBFactory_EDIT : Telerik.WinControls.UI.RadForm
    {

        string pBchID;
        string pDate;
        string pChange;  //0 ไม่มีการเปลี่ยนเเปลงฟอร์ม 1 มีการเปลี่ยนแปลงเพื่อ reshesh ข้อมูลในตาราง main

        readonly string _tblName;
        readonly string _grpId;
        readonly string _grpName;
        readonly string _pJobNumber;
        readonly string _pPermission;
        readonly string _pType;

        readonly string pTypeGps;

        DataTable dtSELECT = new DataTable();

        #region "Rows"      
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Set Permission สำหรับปิด JOB
        void SetPermission()
        {
            radCheckBox_STACLOSE.Enabled = false;
            Button_AX.Enabled = false;
            Button_MNIO.Enabled = false;
        }
        //Load
        public JOBFactory_EDIT(string pTbl, string pGrpID, string pGrpName, string pJobNumber, string pPermission, string pType)
        {
            InitializeComponent();
            _tblName = pTbl; //ตาราง
            _grpId = pGrpID; //JOBGROUP_ID = 00023 : SHOP_JOBGROUPSUB
            _grpName = pGrpName; //ชื่อFORM
            _pPermission = pPermission; //สิทธิ์ปิด JOB
            _pType = pType; //SET 0
            _pJobNumber = pJobNumber; //เลขที่ FJOB

            pChange = "0";
            pTypeGps = "";
        }
        //Load
        private void JOBCar_EDIT_Load(object sender, EventArgs e)
        {
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Bill.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล : " + _pJobNumber;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_TECH);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_STA);

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_OpenAdd.ButtonElement.ShowBorder = true; RadButton_OpenAdd.ButtonElement.ToolTipText = "แนบรูป สำหรับเปิดงานซ่อม";
            radButton_OpenView.ButtonElement.ShowBorder = true; radButton_OpenView.ButtonElement.ToolTipText = "ดูรูป สำหรับเปิดงานซ่อม";
            radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            JOB_Class.SetHeadGrid_ForShowImage(radGridView_Show, "0");



            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
            DatagridClass.SetDefaultRadGridView(radGridView_ShowImg);

            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("JOB_NUMBER", "JOB")));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("FillPath0", "Path")));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image0", "รูป", 80)));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("FillPath1", "Path")));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image1", "รูป", 80)));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("FillPath2", "Path")));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image2", "รูป", 80)));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("FillPath3", "Path")));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image3", "รูป", 80)));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("FillPath4", "Path")));
            radGridView_ShowImg.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image4", "รูป", 80)));
            radGridView_ShowImg.TableElement.RowHeight = 100;
            radGridView_ShowImg.MasterTemplate.EnableFiltering = false;
            radGridView_ShowImg.MasterTemplate.AllowRowResize = false;
            radGridView_ShowImg.MasterTemplate.AllowColumnResize = false;
            radGridView_Show.ClearSelection();
            radGridView_ShowImg.ClearSelection();

            this.Text = _grpName + " : " + _pType;

            SetData();
        }

        //MACHANIC
        void Set_GroupRepair()
        {
            radDropDownList_TECH.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_TECH.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("52", "", " ORDER BY SHOW_ID ", "1");
            radDropDownList_TECH.ValueMember = "SHOW_ID";
            radDropDownList_TECH.DisplayMember = "SHOW_NAME";
        }

        void Set_JOBSTA()
        {
            radDropDownList_STA.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_STA.DataSource = JOB_Class.GetJOBStatus();
            radDropDownList_STA.ValueMember = "STA_ID";
            radDropDownList_STA.DisplayMember = "STA_NAME";
        }
        //Set Data
        void SetData()
        {
            string sql = $@" SELECT JOB_NUMBER, JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,
		                                            JOB_MACHINEID, JOB_MACHINENAME, JOB_SN,
		                                            JOB_DESCRIPTION, JOB_STACLOSE,JOB_SHORTNAMEINS,
		                                            JOB_DESCRIPTION_CLOSE,JOB_STA,JOB_STAIMG_OPEN,
		                                            CONVERT(varchar,JOB_DATEINS,23) AS DATE_INS,
		                                            JOB_TECHID,JOB_TECHNAME,JOB_WHOINS
                              FROM  SHOP_JOBFactory WITH (NOLOCK)
                              WHERE	JOB_NUMBER = '{_pJobNumber}' ";

            dtSELECT = Controllers.ConnectionClass.SelectSQL_Main(sql);

            pBchID = dtSELECT.Rows[0]["BRANCH_ID"].ToString();
            radTextBox_DptID.Text = dtSELECT.Rows[0]["BRANCH_ID"].ToString().Replace("MN", "").Replace("D", "");
            radLabel_DptName.Text = dtSELECT.Rows[0]["BRANCH_NAME"].ToString();
            radTextBox_Machine.Text = $@"{dtSELECT.Rows[0]["JOB_MACHINEID"]} : {dtSELECT.Rows[0]["JOB_MACHINENAME"]}"; radTextBox_Machine.Enabled = false;
            Set_JOBSTA(); radDropDownList_STA.SelectedValue = dtSELECT.Rows[0]["JOB_STA"].ToString(); //set dropdown status job
            Set_GroupRepair(); radDropDownList_TECH.SelectedValue = dtSELECT.Rows[0]["JOB_TECHID"].ToString(); //set dropdown machanic

            if (dtSELECT.Rows[0]["JOB_STAIMG_OPEN"].ToString() == "1") Set_DGVImg(dtSELECT); //add img to GridView

            radTextBox_Desc.Text = dtSELECT.Rows[0]["JOB_DESCRIPTION"].ToString();
            pDate = dtSELECT.Rows[0]["DATE_INS"].ToString();

            radButton_OpenView.Enabled = false;
            if (dtSELECT.Rows[0]["JOB_STAIMG_OPEN"].ToString() == "1")
            {
                radButton_OpenView.Enabled = true;
            }

            if (dtSELECT.Rows[0]["JOB_STACLOSE"].ToString() == "1") //ปิดจ็อบไปแล้ว
            {
                radCheckBox_STACLOSE.Checked = true;
                radTextBox_Update.Text = dtSELECT.Rows[0]["JOB_DESCRIPTION_CLOSE"].ToString();
                radTextBox_Update.ReadOnly = true;
                radButton_Save.Enabled = false;
                radDropDownList_STA.Enabled = false;
                radDropDownList_TECH.Enabled = false;
                radCheckBox_STACLOSE.Enabled = false;
                Button_MNIO.Enabled = false;
                Button_AX.Enabled = false;
                button_MNIO_stockID.Enabled = false;
                radButton_OpenView.Enabled = false;
                RadButton_OpenAdd.Enabled = false;
                radButton_Cancel.Focus();
                radTextBox_Update.SelectionStart = radTextBox_Update.Text.Length;
            }
            else
            {
                radCheckBox_STACLOSE.Checked = false;
                radTextBox_Update.Enabled = true;
                radButton_Save.Enabled = true;
                radTextBox_Update.Focus();
            }

            if (_pPermission != "1")
            {
                SetPermission();
            }



            pChange = "0";
        }

        void Set_DGVImg(DataTable dt)
        {
            radGridView_ShowImg.DataSource = dtSELECT;
            //ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
            string FullPath = $@"{PathImageClass.pPathJOBFactory}{dt.Rows[0]["DATE_INS"].ToString().Substring(0, 7)}\{dt.Rows[0]["DATE_INS"]}\{dt.Rows[0]["JOB_NUMBER"]}";

            DirectoryInfo DirInfo = new DirectoryInfo(FullPath);
            FileInfo[] imageJob = DirInfo.GetFiles("*.*", SearchOption.AllDirectories);
            if (imageJob.Length == 0) return;
            int countImage = imageJob.Length;
            if (imageJob.Length > 5) countImage = 5;

            //radGridView_ShowImg.Rows.Add(ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply),
            //    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply),
            //    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply),
            //    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply),
            //    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply));

            for (int i = 0; i < 5; i++)
            {
                radGridView_ShowImg.Rows[0].Cells[$@"FillPath{i}"].Value = PathImageClass.pImageEmply;
                radGridView_ShowImg.Rows[0].Cells[$@"Image{i}"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
            }

            for (int i = 0; i < countImage; i++)
            {
                radGridView_ShowImg.Rows[0].Cells[$@"FillPath{i}"].Value = imageJob[i].FullName;
                radGridView_ShowImg.Rows[0].Cells[$@"Image{i}"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(imageJob[i].FullName);
            }
        }

        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (pChange == "1") this.DialogResult = DialogResult.OK; else this.DialogResult = DialogResult.No;

            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //ในกรณีที่ไม่มีการ Update
            if (radTextBox_Update.Text == "")
            {
                string strUpdGroupSUB = $@" UPDATE	SHOP_JOBFactory
                                            SET		JOB_DATEUPD = GETDATE(),   
		                                            JOB_TECHID = '{radDropDownList_TECH.SelectedValue}', 
		                                            JOB_TECHNAME = '{radDropDownList_TECH.SelectedItem}',
		                                            JOB_STA = '{radDropDownList_STA.SelectedValue}'
                                            WHERE	JOB_Number = '{_pJobNumber}' ";
                string TGroupSUB = ConnectionClass.ExecuteSQL_Main(strUpdGroupSUB);
                MsgBoxClass.MsgBoxShow_SaveStatus(TGroupSUB);
                if (TGroupSUB == "")
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
                else
                {
                    return;
                }
            }

            //ในกรณีที่มีการ Update
            string strUser;
            string dessc = SystemClass.SystemUserNameShort + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            strUser = "-> BY " + dessc;

            string desc = radTextBox_Desc.Text;
            string descClose = "-" + radTextBox_Update.Text + Environment.NewLine + strUser;
            string strUpd;
            string GETDATE = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            if (radCheckBox_STACLOSE.Checked == true)//ปิดจีอบ
            {
                if (radTextBox_Update.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการปิดจ๊อบ");
                    radTextBox_Update.Focus();
                    return;
                }
                strUpd = $@"  UPDATE	SHOP_JOBFactory 
                                            SET		[JOB_DESCRIPTION_CLOSE] = '{descClose} [ {GETDATE} ]',
                                                    [JOB_STA] = '{radDropDownList_STA.SelectedValue}',
		                                            [JOB_STACLOSE] = '1',
		                                            [JOB_WHOCLOSE] = '{SystemClass.SystemUserID}' ,
		                                            [JOB_NAMECLOSE] = '{SystemClass.SystemUserName}',
		                                            [JOB_DATECLOSE] = GETDATE(),
		                                            [JOB_DATEUPD] = GETDATE()   
                                            WHERE	[JOB_Number] = '{_pJobNumber}' ";
            }
            else
            {
                desc = desc + Environment.NewLine + Environment.NewLine + descClose;
                strUpd = $@"UPDATE	SHOP_JOBFactory 
                            SET		[JOB_DESCRIPTION] ='{desc} [ {GETDATE} ]',
		                            [JOB_DESCRIPTION_UPD] = '{descClose} [ {GETDATE} ]',
                                    [JOB_STA] = '{radDropDownList_STA.SelectedValue}',
		                            [JOB_DATEUPD] = GETDATE()
                            WHERE	[JOB_Number] = '{_pJobNumber}'";
            }

            string T = ConnectionClass.ExecuteSQL_Main(strUpd);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }


        }

        //Close
        private void RadCheckBox_STACLOSE_CheckedChanged(object sender, EventArgs e)
        {
            if (radCheckBox_STACLOSE.Checked == true)
            {
                if (radDropDownList_STA.SelectedValue.ToString() != "04")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สถานะจ๊อบไม่อยู่ในสถานะซ่อมเสร็จ ไม่สามารถปิดได้.");
                    radCheckBox_STACLOSE.Checked = false;
                    radDropDownList_STA.Focus();
                }
            }
        }

        //click
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Show.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                        //JOBClass.FindImageBillAll_ByJobID(BillAllClass.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD": //รูปสาขาส่งซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLD_Count"].Value.ToString(),
                           radGridView_Show.CurrentRow.Cells["Bill_OLD_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC": //รูปสาขาใหญ่รับซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                          radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //MNIO
        private void Button_MNIO_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_DptName.Text,
                //pRmk = radDropDownList_GroupSUB.SelectedText

            };
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }

        //Open Image Add
        private void RadButton_OpenAdd_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBFactory + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            string pFileName = _pJobNumber + "-" + pBchID + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" + SystemClass.SystemUserID + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, _tblName, "JOB_STAIMG_OPEN", "JOB_Number", _pJobNumber, "1");
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.Yes)
            {
                radButton_OpenView.Enabled = true;
            }
            else if (frm.DialogResult == DialogResult.OK)
            {
                UploadImageSelectArea _UploadImageSelectArea = new UploadImageSelectArea("0", pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber);
                _UploadImageSelectArea.Show();
                radButton_OpenView.Enabled = true;
            }
        }
        //Open Image View
        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBFactory + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            { pathImg = pPath + @"|*.jpg" };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }

        //MNOT
        private void Button_MNOT_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNOT", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_DptName.Text,
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //AX
        private void Button_AX_Click(object sender, EventArgs e)
        {
            Bill.BillAXAddJOB frm = new Bill.BillAXAddJOB(pBchID, radLabel_DptName.Text, "1",
                _pJobNumber, _grpId, _grpName, radTextBox_Machine.Text, "", "", "");
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }


        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
        //Select JOB
        private void RadDropDownList_Reapair_SelectedValueChanged(object sender, EventArgs e)
        {
            if (pTypeGps == "")
            {
                return;
            }
            if (pTypeGps != "00341")
            {
                if (radDropDownList_TECH.SelectedValue.ToString() == "00341")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("สำหรับประเภทการซ่อม ระบบ GPS มีปัญหา" + Environment.NewLine + "สามารถเลือกได้ตอนเปิดจ๊อบเท่านั้น" + Environment.NewLine +
                        "ถ้าเปิดจ๊อบผิดให้แจ้งปิดจ๊อบก่อนเปิดใหม่อีกครั้ง");
                    radDropDownList_TECH.SelectedValue = pTypeGps;
                    return;
                }
            }
        }



        private void Button_MNIO_stockID_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", _pType, _pJobNumber, _grpId, _grpName, "1", "1")
            {
                pBchID = pBchID,
                pBchName = radLabel_DptName.Text,
                pRmk = radTextBox_Machine.Text
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);

        }

        private void RadGridView_ShowImg_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowImg_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Image0":
                    System.Diagnostics.Process.Start(radGridView_ShowImg.CurrentRow.Cells["FillPath0"].Value.ToString());
                    break;
                case "Image1":
                    System.Diagnostics.Process.Start(radGridView_ShowImg.CurrentRow.Cells["FillPath1"].Value.ToString());
                    break;
                case "Image2":
                    System.Diagnostics.Process.Start(radGridView_ShowImg.CurrentRow.Cells["FillPath2"].Value.ToString());
                    break;
                case "Image3":
                    System.Diagnostics.Process.Start(radGridView_ShowImg.CurrentRow.Cells["FillPath3"].Value.ToString());
                    break;
                case "Image4":
                    System.Diagnostics.Process.Start(radGridView_ShowImg.CurrentRow.Cells["FillPath4"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
    }
}
