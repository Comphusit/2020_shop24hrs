﻿namespace PC_Shop24Hrs.JOB.Factory
{
    partial class JOBFactory_ADD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JOBFactory_ADD));
            this.panel1 = new System.Windows.Forms.Panel();
            this.radGroupBox_Desc = new Telerik.WinControls.UI.RadGroupBox();
            this.radButton_openMachine = new Telerik.WinControls.UI.RadButton();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_MACHINEID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_MACHINEName = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_branch = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).BeginInit();
            this.radGroupBox_Desc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_openMachine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MACHINEID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MACHINEName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radGroupBox_Desc);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(454, 461);
            this.panel1.TabIndex = 3;
            // 
            // radGroupBox_Desc
            // 
            this.radGroupBox_Desc.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Desc.Controls.Add(this.radButton_openMachine);
            this.radGroupBox_Desc.Controls.Add(this.RadButton_pdt);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_MACHINEID);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_MACHINEName);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Save);
            this.radGroupBox_Desc.Controls.Add(this.radLabel4);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_branch);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Desc);
            this.radGroupBox_Desc.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Desc.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Desc.HeaderText = "รายละเอียด";
            this.radGroupBox_Desc.Location = new System.Drawing.Point(18, 14);
            this.radGroupBox_Desc.Name = "radGroupBox_Desc";
            this.radGroupBox_Desc.Size = new System.Drawing.Size(419, 434);
            this.radGroupBox_Desc.TabIndex = 25;
            this.radGroupBox_Desc.Text = "รายละเอียด";
            // 
            // radButton_openMachine
            // 
            this.radButton_openMachine.BackColor = System.Drawing.Color.Transparent;
            this.radButton_openMachine.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_openMachine.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.radButton_openMachine.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_openMachine.Location = new System.Drawing.Point(153, 38);
            this.radButton_openMachine.Name = "radButton_openMachine";
            this.radButton_openMachine.Size = new System.Drawing.Size(26, 26);
            this.radButton_openMachine.TabIndex = 75;
            this.radButton_openMachine.Click += new System.EventHandler(this.RadButton_openMachine_Click);
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(387, 17);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 74;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radTextBox_MACHINEID
            // 
            this.radTextBox_MACHINEID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_MACHINEID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MACHINEID.Location = new System.Drawing.Point(102, 38);
            this.radTextBox_MACHINEID.MaxLength = 3;
            this.radTextBox_MACHINEID.Name = "radTextBox_MACHINEID";
            this.radTextBox_MACHINEID.Size = new System.Drawing.Size(45, 25);
            this.radTextBox_MACHINEID.TabIndex = 0;
            this.radTextBox_MACHINEID.Text = "0012";
            this.radTextBox_MACHINEID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BranchID_KeyDown);
            // 
            // radLabel_MACHINEName
            // 
            this.radLabel_MACHINEName.AutoSize = false;
            this.radLabel_MACHINEName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_MACHINEName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_MACHINEName.Location = new System.Drawing.Point(185, 39);
            this.radLabel_MACHINEName.Name = "radLabel_MACHINEName";
            this.radLabel_MACHINEName.Size = new System.Drawing.Size(192, 23);
            this.radLabel_MACHINEName.TabIndex = 18;
            this.radLabel_MACHINEName.Text = "เครื่องจักร";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(203, 394);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(105, 394);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(19, 72);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(85, 23);
            this.radLabel4.TabIndex = 23;
            this.radLabel4.Text = "รายละเอียด";
            // 
            // radLabel_branch
            // 
            this.radLabel_branch.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_branch.Location = new System.Drawing.Point(19, 43);
            this.radLabel_branch.Name = "radLabel_branch";
            this.radLabel_branch.Size = new System.Drawing.Size(74, 23);
            this.radLabel_branch.TabIndex = 17;
            this.radLabel_branch.Text = "เครื่องจักร";
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.AcceptsReturn = true;
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Desc.Location = new System.Drawing.Point(19, 101);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.Size = new System.Drawing.Size(379, 287);
            this.radTextBox_Desc.TabIndex = 4;
            // 
            // JOBFactory_ADD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(454, 461);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "JOBFactory_ADD";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "บันทึกข้อมูล JOB Factory.";
            this.Load += new System.EventHandler(this.JOBCenter_SHOP_ADD_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).EndInit();
            this.radGroupBox_Desc.ResumeLayout(false);
            this.radGroupBox_Desc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_openMachine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MACHINEID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MACHINEName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_branch;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MACHINEID;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_MACHINEName;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Desc;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadButton radButton_openMachine;
    }
}
