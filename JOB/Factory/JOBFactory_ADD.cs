﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.JOB.Factory
{
    public partial class JOBFactory_ADD : Telerik.WinControls.UI.RadForm
    {
        readonly string _tblName;
        readonly string _billFirst;
        readonly string _grpId;
        readonly string _grpDesc; //0 ไม่มีสิทธิ์ปิดจ๊อบ 1 มี
        readonly string _pFixBch; //กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น
        string machine;

        public JOBFactory_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc,string pFixBch)
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _pFixBch = pFixBch;
        }

        void Set_ClearData()
        {
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            radTextBox_MACHINEID.Text = "";
            radTextBox_MACHINEID.Focus();
            radTextBox_Desc.Text = "";
            radLabel_MACHINEName.Text = "";
        }

        private void JOBCenter_SHOP_ADD_Load(object sender, EventArgs e)
        {
            Set_ClearData();
            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SHOP";
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_openMachine.ButtonElement.ShowBorder = true; radButton_openMachine.ButtonElement.ToolTipText = "เครื่องจักรโรงงาน";
        }

        private void RadTextBox_BranchID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetMACHINE();
        }
         
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {

            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("คำอธิบาย");
                radTextBox_Desc.Focus();
                return;
            }

            string descJOB ;
            descJOB = "- " + radTextBox_Desc.Text.Trim();
            descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            string GETDATE = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = $@"
                INSERT INTO SHOP_JOBFactory ([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],
                        [JOB_MACHINEID],[JOB_MACHINENAME],
                        [JOB_TECHID],[JOB_TECHNAME],
                        [JOB_WHOINS],[JOB_WHONAMEINS],[JOB_SHORTNAMEINS],
                        [JOB_DESCRIPTION],[JOB_SN])
                VALUES  ('{billMaxNO}', '{SystemClass.SystemDptID}', '{SystemClass.SystemDptName}',
                        '{machine}', '{radLabel_MACHINEName.Text}',
                        '', '',
                        '{SystemClass.SystemUserID}', '{SystemClass.SystemUserName}', '{SystemClass.SystemUserNameShort}',
                        '{descJOB} [ {GETDATE} ]', '')";

            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        void SetMACHINE()
        {
            machine = $@"MC{radTextBox_MACHINEID.Text.PadLeft(4,'0')}";
            if (JOB_Class.GetJob_ByFactory(machine) > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"เครื่องจักร {machine} {Environment.NewLine} มีข้อมูลการซ่อมอยู่แล้ว {Environment.NewLine} ไม่สามารถเปิดจ๊อบซ้ำได้อีก");
                radTextBox_MACHINEID.Text = "";
                radButton_openMachine.Focus();
                return;
            }

            DataTable dtM = GetDetailMachine(machine);
            if (dtM.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("เครื่องจักร");
                radTextBox_MACHINEID.Text = "";
                return;
            }

            radLabel_MACHINEName.Text = dtM.Rows[0]["DATA_DESC"].ToString();
            radTextBox_Desc.Focus();

        }

        public static DataTable GetDetailMachine(string machineID)
        {
            string Machine = "";
            if (machineID != "") Machine = $@"WHERE	[NUM] = '{machineID}'";
            string sql = $@"
                    SELECT	[NUM] AS DATA_ID, [NAMEMACHINE] AS DATA_DESC
                    FROM	[SupcAndroid].[dbo].[DAILY_MACHINE] WITH (NOLOCK)
                    {Machine}";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_Desc.Focus();
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButton_openMachine_Click(object sender, EventArgs e)
        {
            DataTable dtM = GetDetailMachine("");
            ShowDataDGV frm = new ShowDataDGV()
            { dtData = dtM };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                if (JOB_Class.GetJob_ByFactory(frm.pID) > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"เครื่องจักร {frm.pID} {Environment.NewLine} มีข้อมูลการซ่อมอยู่แล้ว {Environment.NewLine} ไม่สามารถเปิดจ๊อบซ้ำได้อีก");
                    radTextBox_MACHINEID.Text = "";
                    radButton_openMachine.Focus();
                    return;
                }
                else
                {
                    machine = frm.pID;
                    radTextBox_MACHINEID.Text = frm.pID.Replace("MC","");
                    radLabel_MACHINEName.Text = frm.pDesc;
                    radTextBox_Desc.Focus();
                }
            }
            else
            {
                radTextBox_MACHINEID.Text = "";
                radButton_openMachine.Focus();
            }
        }
    }
}
