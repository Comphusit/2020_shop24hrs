﻿//CheckOK
using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.JOB.Factory
{
    public partial class JOBFactory_Main : RadForm
    {
        readonly string _pType;
        readonly string _pGROUPID;
        readonly string _pGROUP_DESC;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปิดจ๊อบ 1 มี
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        string tbl;
        string billFirst;

        private DateTime dateAutoRefresh;

        //Main Load
        public JOBFactory_Main(string pType, string pGROUPID, string pGROUP_DESC, string pPerminssion, string pFixBch) // SHOP-SUPC
        {
            InitializeComponent();

            _pType = pType;
            _pGROUPID = pGROUPID;
            _pGROUP_DESC = pGROUP_DESC;
            _pPerminssion = pPerminssion;
            _pFixBch = pFixBch;

        }
        private void JOBCar_Main_Load(object sender, EventArgs e)
        {
            DataTable dtGroup = JOB_Class.GetJOB_Group(_pGROUPID);
            tbl = dtGroup.Rows[0]["TABLENAME"].ToString();
            billFirst = dtGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString();

            dateAutoRefresh = DateTime.Now.AddMinutes(3); Timer_Auto.Start();

            this.Text = _pGROUP_DESC + " : " + _pType;

            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "ExportExcel";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่ม";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radRadioButtonElement_Open.ShowBorder = true;
            radRadioButtonElement_ALL.ShowBorder = true;
            radStatusStrip_Main.SizingGrip = false;

            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATE", "S", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATEUPD", "Up", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_NUMBER", "เลขที่ JOB", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_MACHINEID", "เครื่งจักร", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_MACHINENAME", "ชื่อเครื่งจักร", 170));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchID("แผนก"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchName("ชื่อแผนก"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SHORTNAMEINS", "ผู้เปิด", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_TECHID", "ช่างซ่อม", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_TECHNAME", "ชื่อช่างผู้ซ่อม", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_STACLOSE", "สถานะจ๊อบ"));

            //Freeze Column
            for (int i = 0; i < 4; i++)
            {
                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;
            }

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);

            radGridView_Show.TableElement.SearchHighlightColor = Color.LightBlue;

            SetGDV();
        }
        //find Data
        void SetGDV()
        {
            string sta_JobAll = "'0'";
            if (radRadioButtonElement_Open.CheckState == CheckState.Unchecked) { sta_JobAll = "   '0','1' "; }
            string sql = $@" 
                SELECT JOB_NUMBER, JOB_BRANCHID AS BRANCH_ID, JOB_BRANCHNAME AS BRANCH_NAME, JOB_MACHINEID, JOB_MACHINENAME,
                        JOB_SN, JOB_DESCRIPTION, JOB_DESCRIPTION_UPD, JOB_STACLOSE, JOB_SHORTNAMEINS,
                        DateDiff(Day, JOB_DATEINS, GETDATE()) AS SUMDATE,
                        CASE[JOB_STA] WHEN '01' THEN 'เปิดจ๊อบ' WHEN '02' THEN 'รอซ่อม' WHEN '03' THEN 'กำลังซ่อม' WHEN '04' THEN 'ซ่อมเสร็จ'  END AS STA,
                        CASE CONVERT(VARCHAR, JOB_DATEUPD,23) WHEN '1900-01-01' THEN DateDiff(Day, JOB_DATEINS, GETDATE()) ELSE ISNULL(DATEDIFF(DAY, JOB_DATEUPD, GETDATE()),0) END AS SUMDATEUPD,
            	        JOB_TECHID,JOB_TECHNAME  
                FROM   {tbl} WITH(NOLOCK)
                WHERE  JOB_STACLOSE IN({sta_JobAll}) 
                ORDER BY CONVERT(VARCHAR,JOB_DATEINS,25) DESC ";

            radGridView_Show.DataSource = Controllers.ConnectionClass.SelectSQL_Main(sql);
        }

        #region "ROWS NUMBERS"      
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        //Open
        private void RadRadioButtonElement_Open_CheckStateChanged(object sender, EventArgs e)
        {
            SetGDV();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            OpenFormEdit();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            JOBFactory_ADD frmJOB_ADD = new JOBFactory_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC, _pFixBch);
            if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
            {
                SetGDV();
            }
        }
        //All
        private void RadRadioButtonElement_ALL_CheckStateChanged(object sender, EventArgs e)
        {
            SetGDV();
        }
        //Open for Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            OpenFormEdit();
        }
        //Open Form
        void OpenFormEdit()
        {
            if ((radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString() == "") || (radGridView_Show.CurrentRow.Index == -1) || (radGridView_Show.Rows.Count == 0)) return;

            string jobID = radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString();

            JOBFactory_EDIT frmJOB_Edit = new JOBFactory_EDIT(tbl, _pGROUPID, _pGROUP_DESC, jobID, _pPerminssion, _pType);
            if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK)
            {
                SetGDV();
            }
        }
        //Auto Refresh
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now >= dateAutoRefresh)
            {
                Timer_Auto.Stop();
                SetGDV();
                dateAutoRefresh = DateTime.Now.AddMinutes(3);
                Timer_Auto.Start();
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("งานซ่อมรถ", radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}



