﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Collections;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.JOB.Bill
{
    public partial class Bill_MNIO_OT : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();

        public string pBchID;
        public string pBchName;
        public string pRmk;

        readonly DataTable dt_data = new DataTable();
        string pSave = "0";
        int pCopy = 0;
        string pUpPRINTCOUNT = "0";
        string pBillNo = "";
        string pCopyColor = "0";

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        readonly string tblNameHD;//ตารางที่บันทึก
        readonly string tblNameDT;//ตารางที่บันทึก
        readonly string pHeadBillPrint;//หัวบิล

        readonly string _pTypeBill;// MNOT หรือ MNIO
        readonly string _pTypeOpen; // 0 = SHOP,1 = SUPC
        readonly string _pJobNumber;//เลขที่ JOB
        readonly string _pGroupID;//แผนกที่เปิด โดยทีอ้างอิงจาก JOB
        readonly string _pGroupDesc;
        readonly string _pStockDelete; //1 เอาสต็อกไป - ด้วย 0 ไม่ต้องเอาสต็อกไป -
        readonly string _pCase; // 1 : เคสซ่อมเครื่องจักรโรงงาน

        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            //ในกรณีที่ MNII ก็ไม่ต้อง
            if ((_pJobNumber == "") || (_pTypeBill != "MNII"))
            {
                SetCbDpt();
                ClearDataHD();
                ClearDataDT();
            }
            else
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }
        //OpenF
        public Bill_MNIO_OT(string pTypeBill, string pTypeOpen, string pJobNumber, string pGroupID, string pGroupDesc, string pStockDelete, string pCase = "")
        {
            InitializeComponent();
            _pTypeBill = pTypeBill;
            _pTypeOpen = pTypeOpen;
            _pJobNumber = pJobNumber;
            _pGroupID = pGroupID;
            _pGroupDesc = pGroupDesc;
            _pStockDelete = pStockDelete;
            _pCase = pCase;
            radLabel_1.Text = "ระบุทะเบียน/SN/SPC [Enter] หรือ ระบุชื่อสินค้า [PageDown] หรือ ระบุบาร์โค้ด [PageUp]";
            switch (_pTypeBill)
            {
                case "MNIO":
                    tblNameHD = "SHOP_MNIO_HD"; tblNameDT = "SHOP_MNIO_DT"; pHeadBillPrint = "ใบนำของออก [ไม่มีค่าใช้จ่าย]";
                    if (_pStockDelete == "1")
                    {
                        radLabel_1.Text = "ระบุรหัสสินค้า [Enter]"; pHeadBillPrint = "ใบนำของห้องช่างออก [ไม่มีค่าใช้จ่าย]";
                    }
                    break;
                case "MNOT":
                    tblNameHD = "SHOP_MNOT_HD"; tblNameDT = "SHOP_MNOT_DT"; pHeadBillPrint = "ใบนำเครื่องมือออก";
                    break;
                case "MNII":
                    radLabel_1.Text = "ระบุรหัสสินค้า [Enter]";
                    tblNameHD = "SHOP_MNIO_HD"; tblNameDT = "SHOP_MNIO_DT"; pHeadBillPrint = "ใบนำของเข้าสต็อก [ห้องช่าง]";
                    break;
                default:
                    break;
            }
        }
        //Clear Open
        void ClearEnable()
        {
            radTextBox_Item.Enabled = false;
            radTextBox_ItemName.Enabled = false;
            radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Enabled = false;
            radButton_Add.Enabled = false;
            radButton_Claer.Enabled = false;
            radTextBox_Remark.Enabled = false;
            radRadioButton_SUPC.Enabled = false;
            radRadioButton_MN.Enabled = false;
        }
        //ClearDataHD
        void ClearDataHD()
        {
            radLabel_Docno.Text = "";
            radTextBox_Item.Text = "";
            radTextBox_ItemName.Text = "";
            radTextBox_Unit.Text = "";
            radTextBox_Qty.Text = "";
            radTextBox_Remark.Text = "";

            if (dt_data.Rows.Count > 0) dt_data.Rows.Clear();

            pSave = "0";
            pCopy = 0;
            pBillNo = "";
            radButton_Save.Enabled = true;
            radTextBox_Item.Enabled = true;
            radTextBox_ItemName.Enabled = true;
            radTextBox_Qty.Enabled = true;
            radTextBox_Unit.Enabled = true;
            radButton_Add.Enabled = true;
            radButton_Claer.Enabled = true;
            radTextBox_Remark.Enabled = true;
            radGridView_Show.Enabled = true;

            radTextBox_Item.Focus();
        }
        //ClearDataDT
        void ClearDataDT()
        {
            radTextBox_Qty.Text = ""; radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Text = ""; radTextBox_Unit.Enabled = false;
            radTextBox_ItemName.Text = "";
            radTextBox_Item.Text = ""; radTextBox_Item.Enabled = true; radTextBox_Item.Focus();
        }
        //Load
        private void Bill_MNIO_OT_Load(object sender, EventArgs e)
        {
            this.Text = pHeadBillPrint + " [" + _pTypeBill + "]";

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radButton_Claer.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEM", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 240));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 75));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.EnableFiltering = false;


            SetCbDpt();

            dt_data.Columns.Add("ITEM");
            dt_data.Columns.Add("ITEMNAME");
            dt_data.Columns.Add("QTY");
            dt_data.Columns.Add("UNIT");

            ClearDataHD();
            ClearDataDT();

            radTextBox_Remark.Text = pRmk;
        }
        void SetCbDpt()
        {
            //Set RadDropDownList All
            this.radDropDownList_Dpt.DropDownListElement.ListElement.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.AutoSizeItems = true;
            this.radDropDownList_Dpt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 30;

            DataTable dtDpt;// = new DataTable();
            switch (_pTypeOpen)
            {
                case "SHOP"://SHOP
                    radRadioButton_MN.CheckState = CheckState.Checked; radRadioButton_MN.Enabled = true;
                    radRadioButton_SUPC.CheckState = CheckState.Unchecked; radRadioButton_SUPC.Enabled = false;
                    if (_pJobNumber == "")
                    {
                        radDropDownList_Dpt.DropDownStyle = RadDropDownStyle.DropDown;
                        dtDpt = BranchClass.GetBranchAll("'1','4'", "'0','1'");
                    }
                    else
                    {
                        radRadioButton_MN.Enabled = false;
                        radRadioButton_SUPC.Enabled = false;
                        radDropDownList_Dpt.DropDownStyle = RadDropDownStyle.DropDownList;
                        dtDpt = BranchClass.GetDetailBranchByID(pBchID);
                    }
                    radDropDownList_Dpt.DataSource = dtDpt;
                    radDropDownList_Dpt.ValueMember = "BRANCH_ID";
                    radDropDownList_Dpt.DisplayMember = "NAME_BRANCH";
                    break;
                case "SUPC": //SUPC
                    radRadioButton_SUPC.CheckState = CheckState.Checked; radRadioButton_SUPC.Enabled = true;
                    radRadioButton_MN.CheckState = CheckState.Unchecked; radRadioButton_MN.Enabled = false;

                    if (_pStockDelete == "1")
                    {
                        radRadioButton_MN.Enabled = false;
                        radRadioButton_SUPC.Enabled = false;
                        radDropDownList_Dpt.DropDownStyle = RadDropDownStyle.DropDownList;
                        dtDpt = Models.DptClass.GetDptDetail_ByDptID(pBchID);
                    }
                    else
                    {
                        if (_pTypeBill == "MNII")
                        {
                            radRadioButton_MN.Enabled = false;
                            radRadioButton_SUPC.Enabled = false;
                            radDropDownList_Dpt.DropDownStyle = RadDropDownStyle.DropDownList;
                            dtDpt = Models.DptClass.GetDptDetail_ByDptID(pBchID);
                        }
                        else
                        {
                            if (_pJobNumber == "")
                            {
                                radDropDownList_Dpt.DropDownStyle = RadDropDownStyle.DropDown;
                                dtDpt = Models.DptClass.GetDpt_All();
                            }
                            else
                            {
                                radRadioButton_MN.Enabled = false;
                                radRadioButton_SUPC.Enabled = false;
                                radDropDownList_Dpt.DropDownStyle = RadDropDownStyle.DropDownList;
                                dtDpt = Models.DptClass.GetDptDetail_ByDptID(pBchID);
                            }
                        }

                    }

                    radDropDownList_Dpt.DataSource = dtDpt;
                    radDropDownList_Dpt.ValueMember = "NUM";
                    radDropDownList_Dpt.DisplayMember = "DESCRIPTION_SHOW";
                    break;
                default:
                    break;
            }
        }
        #region clearData
        //Claer DT
        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            ClearDataDT();
        }
        #endregion
        //Insert
        void InsertDt()
        {
            if (radTextBox_Qty.Text == "" || radTextBox_Unit.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("จำนวน");
                return;
            }

            dt_data.Rows.Add(radTextBox_Item.Text, radTextBox_ItemName.Text, radTextBox_Qty.Text, radTextBox_Unit.Text);
            radGridView_Show.DataSource = dt_data;

            ClearDataDT();
        }
        //Add DGV
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            InsertDt();
        }
        //Check Num Only
        private void RadTextBox_Qty_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //Enter Data
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            //ในกรณีบิล MNII
            if ((_pTypeBill == "MNII") || (_pStockDelete == "1"))
            {
                switch (e.KeyCode)
                {
                    case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                        if (radTextBox_Item.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุรหัสสินค้า");
                            radTextBox_Item.Focus();
                            return;
                        }

                        string condition = "";
                        if (_pCase == "1") condition = " AND StockType = '00516' ";

                        DataTable dtAsset = Models.AssetClass.FindStockItem_ByStockID(radTextBox_Item.Text, condition);//= new DataTable();
                        //dtAsset = AssetClass.Find_StockItem(radTextBox_Item.Text);

                        if (dtAsset.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสสินค้า");
                            radTextBox_Item.SelectAll();
                            radTextBox_Item.Focus();
                            return;
                        }

                        radTextBox_Item.Text = dtAsset.Rows[0]["ASSETID"].ToString();
                        radTextBox_ItemName.Text = dtAsset.Rows[0]["NAME"].ToString();
                        radTextBox_Unit.Text = dtAsset.Rows[0]["UNITOFMEASURE"].ToString();
                        radTextBox_Qty.Text = "1";
                        radTextBox_Qty.Enabled = true;
                        radTextBox_Unit.Enabled = false;
                        radTextBox_Qty.Focus();

                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (e.KeyCode)
                {
                    case Keys.Enter://ระบุทะเบียน/SN/SPC [Enter]  
                        if (radTextBox_Item.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุทะเบียน/SN/SPC");
                            radTextBox_Item.SelectAll();
                            radTextBox_Item.Focus();
                            return;
                        }

                        DataTable dtAsset;//= new DataTable();
                        if (_pTypeBill == "MNIO")
                        {
                            //dtAsset = AssetClass.FindAsset_ByAllType(radTextBox_Item.Text, " AND ASSETGROUP <> 'TOOL'  ");
                            dtAsset = Models.AssetClass.FindAsset_ByAllType(radTextBox_Item.Text, " AND ASSETGROUP <> 'TOOL'  ", "");
                        }
                        else
                        {
                            //dtAsset = AssetClass.FindAsset_ToolOnly(radTextBox_Item.Text);
                            dtAsset = Models.AssetClass.FindAsset_ByAllType(radTextBox_Item.Text, " AND ASSETGROUP = 'TOOL' ", "");
                        }

                        if (dtAsset.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("ทะเบียน/SN/SPC");
                            radTextBox_Item.SelectAll();
                            radTextBox_Item.Focus();
                            return;
                        }

                        radTextBox_Item.Text = dtAsset.Rows[0]["ASSETID"].ToString();
                        radTextBox_ItemName.Text = dtAsset.Rows[0]["NAME"].ToString();
                        if (dtAsset.Rows[0]["ASSETID"].ToString() == "")
                        {
                            radTextBox_Unit.Text = "ชิ้น";
                        }
                        else
                        {
                            if (dtAsset.Rows[0]["UNITOFMEASURE"].ToString() == "") { radTextBox_Unit.Text = "Ea."; }
                            else { radTextBox_Unit.Text = dtAsset.Rows[0]["UNITOFMEASURE"].ToString(); }

                        }

                        radTextBox_Qty.Text = "1";
                        radTextBox_Qty.Enabled = false;
                        radTextBox_Unit.Enabled = false;
                        radButton_Add.Focus();

                        break;
                    case Keys.PageDown://ระบุชื่อสินค้า [PageDown] 
                        if (radTextBox_Item.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ชื่อสินค้า");
                            radTextBox_Item.Focus();
                            return;
                        }

                        radTextBox_ItemName.Text = radTextBox_Item.Text;
                        radTextBox_Item.Text = "";
                        radTextBox_Item.Enabled = false;
                        radTextBox_Unit.Text = "ชิ้น";
                        radTextBox_Unit.Enabled = true;
                        radTextBox_Qty.Enabled = true;
                        radTextBox_Qty.Focus();

                        break;
                    case Keys.PageUp://ระบุบาร์โค้ด [PageUp]
                        if (radTextBox_Item.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                            radTextBox_Item.Focus();
                            return;
                        }

                        DataTable dtBarcode;// = new DataTable();
                        //dtBarcode = ItembarcodeClass.FindItembarcode_ByBarcode(radTextBox_Item.Text);
                        dtBarcode = ItembarcodeClass.GetItembarcode_ALLDeatil(radTextBox_Item.Text);
                        if (dtBarcode.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                            radTextBox_Item.Focus();
                            return;
                        }
                        radTextBox_Item.Text = dtBarcode.Rows[0]["ITEMBARCODE"].ToString();
                        radTextBox_ItemName.Text = dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString();
                        if (dtBarcode.Rows[0]["UNITID"].ToString() == "")
                        {
                            radTextBox_Unit.Text = "ชิ้น";
                        }
                        else
                        {
                            radTextBox_Unit.Text = dtBarcode.Rows[0]["UNITID"].ToString();
                        }
                        radTextBox_Item.Enabled = false;
                        radTextBox_Unit.Enabled = false;
                        radTextBox_Qty.Enabled = true;
                        radTextBox_Qty.Focus();

                        break;
                    default:
                        break;
                }

            }
        }
        //Qty Enter
        private void RadTextBox_Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Unit.Enabled == false)
                {
                    InsertDt();
                    return;
                }
                radTextBox_Unit.Focus();
            }

        }
        //Unit Enter
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                InsertDt();
            }
        }
        //Delete Rows in DGV
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0)
            {
                return;
            }
            //F2
            if (e.KeyCode == Keys.F2)
            {
                if (pSave == "0")
                {
                    string desc = radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value.ToString() + "  จำนวน  " +
                    radGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() + "  " +
                    radGridView_Show.CurrentRow.Cells["UNIT"].Value.ToString();

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete(desc) == DialogResult.No) return;

                    radGridView_Show.Rows.Remove(radGridView_Show.CurrentRow);
                    ClearDataDT();
                }
            }
        }
        //Save 
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radDropDownList_Dpt.SelectedIndex == -1)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave(" สาขาหรือแผนก");
                return;
            }
            if (_pJobNumber == "")
            {
                pBchID = radDropDownList_Dpt.SelectedValue.ToString();
                if (radRadioButton_MN.IsChecked == true) pBchName = BranchClass.GetBranchNameByID(pBchID);
                if (radRadioButton_SUPC.IsChecked == true) pBchName = Models.DptClass.GetDptName_ByDptID(pBchID);
            }

            if (pSave == "0") //check ว่า save หรือ print copy
            {
                if (radGridView_Show.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการเบิก");
                    radTextBox_Item.Focus();
                    return;
                }
                if (radTextBox_Remark.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("หมายเหตุ");
                    radTextBox_Remark.Focus();
                    return;
                }

                if (MsgBoxClass.MsgBoxShow_ConfirmInsert(pHeadBillPrint) == DialogResult.Yes)
                {
                    SaveData();
                }
            }
            else
            {
                PrintData();
                return;
            }
        }
        void SaveData()
        {
            string staBill_spcType = "0";
            //if (_pGroupID == "00023") staBill_spcType = "1";

            ArrayList strSql = new ArrayList();
            pBillNo = Class.ConfigClass.GetMaxINVOICEID(_pTypeBill, "-", _pTypeBill, "1");
            strSql.Add(string.Format(@"INSERT INTO [dbo].[" + tblNameHD + "]   " +
                            "([DOCNO],[REMARK],[JOBGROUP_ID],[JOBGROUP_DESCRIPTION],[JOB_Number] " +
                            ",[BRANCH_ID],[BRANCH_NAME],[WHOINS],[WHONAMEINS],Bill_SpcType ) " +
                            "VALUES ('" + pBillNo + "','" + radTextBox_Remark.Text + "','" + _pGroupID + "','" +
                            _pGroupDesc + "','" + _pJobNumber + "','" + pBchID + "','" + pBchName + "','" + SystemClass.SystemUserID +
                            "','" + SystemClass.SystemUserName + "','" + staBill_spcType + "')"));
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                strSql.Add(string.Format(@"INSERT INTO [dbo].[" + tblNameDT + "]   " +
                            "([DOCNO],[LINENUM],[ITEM]," +
                            "[ITEMNAME],[QTY] " +
                            ",[UNIT] )  " +
                            "VALUES ('" + pBillNo + "','" + (i + 1) + "','" +
                            radGridView_Show.Rows[i].Cells["ITEM"].Value.ToString() + "','" +
                            radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + "','" +
                            int.Parse(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + "','" +
                            radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + "')"));
            }

            if (_pTypeBill == "MNII")
            {
                strSql.Add(string.Format(@"
                    Update	[SHOP_STOCKITEM] Set StockQty = DocQty+(StockQty),WHOIDUPD = '" + SystemClass.SystemUserID + @"',
                            WHONAMEUPD = '" + SystemClass.SystemUserName + @"',DATEUPD = convert(varchar,getdate(),25)
                    FROM	[SHOP_STOCKITEM] WITH (NOLOCK)  INNER JOIN    
                             (SELECT ITEM AS DocItem,SUM(QTY) AS DocQty FROM [SHOP_MNIO_DT] WITH (NOLOCK) 
                                INNER JOIN [SHOP_MNIO_HD] WITH (NOLOCK) ON  [SHOP_MNIO_DT].DOCNO = [SHOP_MNIO_HD].DOCNO  
                    WHERE	[SHOP_MNIO_HD].DOCNO = '" + pBillNo + @"'  GROUP BY ITEM)TMP 
                            ON [SHOP_STOCKITEM].StockID = TMP.DocItem
                "));
            }

            if ((_pTypeBill == "MNIO") || (_pStockDelete == "1"))
            {
                strSql.Add(string.Format(@"
                    Update	[SHOP_STOCKITEM] Set StockQty = (StockQty)-(DocQty),WHOIDUPD = '" + SystemClass.SystemUserID + @"',
                            WHONAMEUPD = '" + SystemClass.SystemUserName + @"',DATEUPD = convert(varchar,getdate(),25)
                    FROM	[SHOP_STOCKITEM] WITH (NOLOCK)  INNER JOIN    
                             (SELECT ITEM AS DocItem,SUM(QTY) AS DocQty FROM [SHOP_MNIO_DT] WITH (NOLOCK) 
                                INNER JOIN [SHOP_MNIO_HD] WITH (NOLOCK) ON  [SHOP_MNIO_DT].DOCNO = [SHOP_MNIO_HD].DOCNO  
                    WHERE	[SHOP_MNIO_HD].DOCNO = '" + pBillNo + @"'  GROUP BY ITEM)TMP 
                            ON [SHOP_STOCKITEM].StockID = TMP.DocItem
                "));
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(strSql);

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = pBillNo;
                pSave = "1";
                pCopy = 1;
                ClearEnable();
                PrintData();
            }
            else { return; }

        }
        //print
        void PrintData()
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                PrintDocument1.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;

                pCopyColor = "บิลฟ้า"; pUpPRINTCOUNT = "1";
                PrintDocument1.Print();
                if (SystemClass.SystemDptID != "D179")
                {
                    pCopyColor = "บิลชมพู"; pUpPRINTCOUNT = "0";
                    PrintDocument1.Print();
                }
            }

        }
        //print
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = pBillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            e.Graphics.DrawString("พิมพ์ครั้งที่ " + pCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(pHeadBillPrint + " [" + pCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(pBchID + "-" + pBchName, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับผิดชอบ - " + _pGroupDesc, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ".(" + radGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                  radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + ")   " +
                                  radGridView_Show.Rows[i].Cells["ITEM"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(" " + radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("รวมจำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString() + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            string jobDesc;
            if (_pJobNumber == "")
            {
                jobDesc = "";
            }
            else
            {
                jobDesc = " [" + _pJobNumber + "] ";
            }
            e.Graphics.DrawString("หมายเหตุ : " + jobDesc + " ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };

            e.Graphics.DrawString(radTextBox_Remark.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            //Y += 30;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;


            if (pUpPRINTCOUNT == "1")
            {
                pCopy += 1;
                string upStr = string.Format(@"UPDATE [" + tblNameHD + "] SET PRINTCOUNT = '" + pCopy + "' WHERE DOCNO = '" + pBillNo + "' ");
                ConnectionClass.ExecuteSQL_Main(upStr);
            }

        }
        //Select Change
        private void RadDropDownList_Dpt_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Item.Focus();
        }
        //Number only
        private void RadTextBox_Qty_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }

}

