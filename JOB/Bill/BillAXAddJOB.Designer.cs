﻿namespace PC_Shop24Hrs.JOB.Bill
{
    partial class BillAXAddJOB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillAXAddJOB));
            this.radTextBox_BillID = new Telerik.WinControls.UI.RadTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_CreateName = new Telerik.WinControls.UI.RadLabel();
            this.radButton_print = new Telerik.WinControls.UI.RadButton();
            this.radToggleSwitch_24 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BillGroup = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radGridView_Detail = new Telerik.WinControls.UI.RadGridView();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel_WH = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Cost = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch_StaBillApv = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CreateID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.PrintDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CreateName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_print)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BillGroup)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_WH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_StaBillApv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CreateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radTextBox_BillID
            // 
            this.radTextBox_BillID.AutoSize = false;
            this.radTextBox_BillID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_BillID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_BillID.Location = new System.Drawing.Point(15, 37);
            this.radTextBox_BillID.Name = "radTextBox_BillID";
            this.radTextBox_BillID.Size = new System.Drawing.Size(174, 33);
            this.radTextBox_BillID.TabIndex = 1;
            this.radTextBox_BillID.Text = "MNIO192001000000";
            this.radTextBox_BillID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BillID_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_CreateName);
            this.panel1.Controls.Add(this.radButton_print);
            this.panel1.Controls.Add(this.radToggleSwitch_24);
            this.panel1.Controls.Add(this.radLabel_F2);
            this.panel1.Controls.Add(this.radLabel_BillGroup);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radLabel_WH);
            this.panel1.Controls.Add(this.radLabel_Cost);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel_1);
            this.panel1.Controls.Add(this.radToggleSwitch_StaBillApv);
            this.panel1.Controls.Add(this.radLabel_Remark);
            this.panel1.Controls.Add(this.radLabel8);
            this.panel1.Controls.Add(this.radLabel_CreateID);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_BranchName);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBox_BillID);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(886, 567);
            this.panel1.TabIndex = 2;
            // 
            // radLabel_CreateName
            // 
            this.radLabel_CreateName.AutoSize = false;
            this.radLabel_CreateName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CreateName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CreateName.Location = new System.Drawing.Point(233, 133);
            this.radLabel_CreateName.Name = "radLabel_CreateName";
            this.radLabel_CreateName.Size = new System.Drawing.Size(492, 23);
            this.radLabel_CreateName.TabIndex = 59;
            this.radLabel_CreateName.Text = "ผู้ทำเบิก";
            // 
            // radButton_print
            // 
            this.radButton_print.BackColor = System.Drawing.Color.Transparent;
            this.radButton_print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_print.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_print.Location = new System.Drawing.Point(195, 37);
            this.radButton_print.Name = "radButton_print";
            // 
            // 
            // 
            this.radButton_print.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_print.Size = new System.Drawing.Size(32, 32);
            this.radButton_print.TabIndex = 58;
            this.radButton_print.Click += new System.EventHandler(this.RadButton_print_Click);
            // 
            // radToggleSwitch_24
            // 
            this.radToggleSwitch_24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radToggleSwitch_24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch_24.Location = new System.Drawing.Point(755, 40);
            this.radToggleSwitch_24.Name = "radToggleSwitch_24";
            this.radToggleSwitch_24.OffText = "OK    Import";
            this.radToggleSwitch_24.OnText = "NOT Import";
            this.radToggleSwitch_24.ReadOnly = true;
            this.radToggleSwitch_24.Size = new System.Drawing.Size(117, 33);
            this.radToggleSwitch_24.TabIndex = 57;
            this.radToggleSwitch_24.Tag = "";
            this.radToggleSwitch_24.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.None;
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch_24.GetChildAt(0))).ThumbOffset = 97;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(49)))), ((int)(((byte)(77)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(73)))), ((int)(((byte)(97)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(110)))), ((int)(((byte)(116)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).Text = "NOT Import";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(1))).Text = "OK    Import";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch_24.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radLabel_F2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radLabel_F2.BorderVisible = true;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(756, 164);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(116, 43);
            this.radLabel_F2.TabIndex = 50;
            this.radLabel_F2.Text = "double click | F2\r\nปรับทะเบียน";
            this.radLabel_F2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel_F2.GetChildAt(0))).BorderVisible = true;
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel_F2.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel_F2.GetChildAt(0))).Text = "double click | F2\r\nปรับทะเบียน";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radLabel_F2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            // 
            // radLabel_BillGroup
            // 
            this.radLabel_BillGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_BillGroup.AutoSize = false;
            this.radLabel_BillGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_BillGroup.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BillGroup.Location = new System.Drawing.Point(780, 135);
            this.radLabel_BillGroup.Name = "radLabel_BillGroup";
            this.radLabel_BillGroup.Size = new System.Drawing.Size(92, 23);
            this.radLabel_BillGroup.TabIndex = 56;
            this.radLabel_BillGroup.Text = "หมวดบิล";
            this.radLabel_BillGroup.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radGridView_Detail);
            this.panel2.Location = new System.Drawing.Point(3, 210);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(880, 314);
            this.panel2.TabIndex = 54;
            // 
            // radGridView_Detail
            // 
            this.radGridView_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Detail.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Detail.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView_Detail.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Detail.Name = "radGridView_Detail";
            // 
            // 
            // 
            this.radGridView_Detail.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Detail.Size = new System.Drawing.Size(880, 314);
            this.radGridView_Detail.TabIndex = 18;
            this.radGridView_Detail.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Detail_ViewCellFormatting);
            this.radGridView_Detail.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Detail_CellDoubleClick);
            this.radGridView_Detail.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Detail_ConditionalFormattingFormShown);
            this.radGridView_Detail.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Detail_FilterPopupRequired);
            this.radGridView_Detail.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Detail_FilterPopupInitialized);
            this.radGridView_Detail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Detail_KeyDown);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(445, 530);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 53;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(347, 530);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 52;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_WH
            // 
            this.radLabel_WH.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_WH.AutoSize = false;
            this.radLabel_WH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_WH.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_WH.Location = new System.Drawing.Point(652, 106);
            this.radLabel_WH.Name = "radLabel_WH";
            this.radLabel_WH.Size = new System.Drawing.Size(221, 23);
            this.radLabel_WH.TabIndex = 51;
            this.radLabel_WH.Text = "คลังเบิก";
            // 
            // radLabel_Cost
            // 
            this.radLabel_Cost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_Cost.AutoSize = false;
            this.radLabel_Cost.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Cost.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Cost.Location = new System.Drawing.Point(651, 76);
            this.radLabel_Cost.Name = "radLabel_Cost";
            this.radLabel_Cost.Size = new System.Drawing.Size(221, 23);
            this.radLabel_Cost.TabIndex = 50;
            this.radLabel_Cost.Text = "งบเบิก";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(455, 108);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(55, 19);
            this.radLabel5.TabIndex = 49;
            this.radLabel5.Text = "คลังเบิก";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(455, 78);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(46, 19);
            this.radLabel4.TabIndex = 48;
            this.radLabel4.Text = "งบเบิก";
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(16, 12);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(132, 19);
            this.radLabel_1.TabIndex = 46;
            this.radLabel_1.Text = "ระบุเลขที่บิล [Enter]";
            // 
            // radToggleSwitch_StaBillApv
            // 
            this.radToggleSwitch_StaBillApv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radToggleSwitch_StaBillApv.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch_StaBillApv.Location = new System.Drawing.Point(755, 3);
            this.radToggleSwitch_StaBillApv.Name = "radToggleSwitch_StaBillApv";
            this.radToggleSwitch_StaBillApv.OffText = "อนุมัติแล้ว";
            this.radToggleSwitch_StaBillApv.OnText = "ยังไม่อนุมัติ";
            this.radToggleSwitch_StaBillApv.ReadOnly = true;
            this.radToggleSwitch_StaBillApv.Size = new System.Drawing.Size(117, 33);
            this.radToggleSwitch_StaBillApv.TabIndex = 45;
            this.radToggleSwitch_StaBillApv.Tag = "";
            this.radToggleSwitch_StaBillApv.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.None;
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0))).ThumbOffset = 97;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(49)))), ((int)(((byte)(77)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(73)))), ((int)(((byte)(97)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(110)))), ((int)(((byte)(116)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).Text = "ยังไม่อนุมัติ";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(1))).Text = "อนุมัติแล้ว";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch_StaBillApv.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.AutoSize = false;
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Remark.Location = new System.Drawing.Point(87, 161);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(519, 43);
            this.radLabel_Remark.TabIndex = 40;
            this.radLabel_Remark.Text = "หมายเหตุ";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(16, 172);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 19);
            this.radLabel8.TabIndex = 39;
            this.radLabel8.Text = "หมายเหตุ";
            // 
            // radLabel_CreateID
            // 
            this.radLabel_CreateID.AutoSize = false;
            this.radLabel_CreateID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CreateID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CreateID.Location = new System.Drawing.Point(87, 133);
            this.radLabel_CreateID.Name = "radLabel_CreateID";
            this.radLabel_CreateID.Size = new System.Drawing.Size(140, 23);
            this.radLabel_CreateID.TabIndex = 38;
            this.radLabel_CreateID.Text = "ผู้ทำเบิก";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.AutoSize = false;
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Date.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Date.Location = new System.Drawing.Point(87, 76);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(336, 23);
            this.radLabel_Date.TabIndex = 37;
            this.radLabel_Date.Text = "วันที่บิล";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(15, 78);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(51, 19);
            this.radLabel2.TabIndex = 34;
            this.radLabel2.Text = "วันที่บิล";
            // 
            // radLabel_BranchName
            // 
            this.radLabel_BranchName.AutoSize = false;
            this.radLabel_BranchName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_BranchName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BranchName.Location = new System.Drawing.Point(87, 104);
            this.radLabel_BranchName.Name = "radLabel_BranchName";
            this.radLabel_BranchName.Size = new System.Drawing.Size(336, 23);
            this.radLabel_BranchName.TabIndex = 36;
            this.radLabel_BranchName.Text = "แผนกเบิก";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(16, 135);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(56, 19);
            this.radLabel3.TabIndex = 35;
            this.radLabel3.Text = "ผู้ทำเบิก";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(15, 106);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(66, 19);
            this.radLabel1.TabIndex = 33;
            this.radLabel1.Text = "แผนกเบิก";
            // 
            // PrintDocument1
            // 
            this.PrintDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // BillAXAddJOB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(886, 567);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BillAXAddJOB";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "บันทีกบิลเบิกจาก AX";
            this.Load += new System.EventHandler(this.BillAXAddJOB_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CreateName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_print)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BillGroup)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_WH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_StaBillApv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CreateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox radTextBox_BillID;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel_CreateID;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchName;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_StaBillApv;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel_Cost;
        private Telerik.WinControls.UI.RadLabel radLabel_WH;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadGridView radGridView_Detail;
        private Telerik.WinControls.UI.RadLabel radLabel_BillGroup;
        private System.Drawing.Printing.PrintDocument PrintDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_24;
        private Telerik.WinControls.UI.RadButton radButton_print;
        private Telerik.WinControls.UI.RadLabel radLabel_CreateName;
    }
}
