﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.GeneralForm.BillOut;
using System.Data;
using System.Collections;

namespace PC_Shop24Hrs.JOB.Bill
{
    public partial class BillMN_ForAddJOB : Telerik.WinControls.UI.RadForm
    {
        DataTable dt;
        readonly string _pDptID;
        readonly string _pBch;
        readonly string _pJobNumber;
        readonly string _pGroupID;

        //Load
        public BillMN_ForAddJOB(string pDptID, string pBch, string pJobNumber, string pGroupID)
        {
            InitializeComponent();

            _pDptID = pDptID;
            _pBch = pBch;
            _pJobNumber = pJobNumber;
            _pGroupID = pGroupID;
        }
        //Load Main
        private void BillMN_ForAddJOB_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "Add JOB";
            radButtonElement_Clear.ShowBorder = true; radButtonElement_Clear.ToolTipText = "Clear JOB";
            radButtonElement_Refresh.ShowBorder = true; radButtonElement_Refresh.ToolTipText = "ดึงข้อมูลใหม่";
            BillOut_Class.SetHeadGrid_ForShowImageReport(RadGridView_Show, 3);

            if (_pJobNumber != "")
            {
                radButtonElement_Clear.Enabled = false;
                radButtonElement_Refresh.Enabled = false;
            }

            SetDGV();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //LoadData
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            dt = BillOut_Class.FindBill_ForAddJOB(_pDptID, _pBch);
            if (dt.Rows.Count == 0) MsgBoxClass.MsgBoxShow_FindRecordNoData("บิลทั้งหมด มี JOB เรียบร้อยแล้ว");
            else BillOut_Class.FindImageBill_ForShowImageReport(dt, RadGridView_Show, 3);
            this.Cursor = Cursors.Default;
        }
        //open
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_SPC":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;

                default:

                    BillOut_Detail frm = new BillOut_Detail(RadGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK) RadGridView_Show.CurrentRow.Cells["Bill_SpcType"].Value = 1;
                    break;
            }
        }
        //refresh
        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Add JOB
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            if (RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() == "") return;

            if (_pJobNumber == "")//ในกรณีที่เปิด จากหน้าไม่มี JOB
            {
                string sql = string.Empty;
                switch (_pGroupID)
                {
                    case "00001"://ComMinimart
                        sql = $@"SELECT	JOB_Number AS DATA_ID,JOBGROUPSUB_DESCRIPTION + ' : ' +JOB_DESCRIPTION AS DATA_DESC
                        FROM	SHOP_JOBComMinimart WITH (NOLOCK)
                        WHERE	JOB_STACLOSE = '0' AND JOB_BRANCHID = '{RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value}'    
                        Order By  JOB_Number DESC ";
                        break;
                    case "00002"://ComService
                        sql = $@"SELECT	JOB_Number AS DATA_ID,JOBGROUPSUB_DESCRIPTION + ' : ' +JOB_DESCRIPTION AS DATA_DESC
                        FROM	SHOP_JOBComService WITH (NOLOCK)
                        WHERE	JOB_STACLOSE = '0' AND JOB_BRANCHID = '{RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value}'    
                        Order By  JOB_Number DESC ";
                        break;
                    case "00003"://งานซ่อมทั่วไป
                        sql = $@"SELECT	JOB_Number AS DATA_ID,JOBGROUPSUB_DESCRIPTION + ' : ' +JOB_DESCRIPTION AS DATA_DESC
                        FROM	SHOP_JOBMaintenance WITH (NOLOCK)
                        WHERE	JOB_STACLOSE = '0' AND JOB_BRANCHID = '{RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value}'    
                        Order By  JOB_Number DESC ";
                        break;
                    case "00004"://งานตรวจสาขา
                        sql = $@"SELECT	JOB_Number AS DATA_ID,JOBGROUPSUB_DESCRIPTION + ' : ' +JOB_DESCRIPTION AS DATA_DESC
                        FROM	SHOP_JOBCenter WITH (NOLOCK)
                        WHERE	JOB_HEAD = '0' AND JOB_STACLOSE = '0' AND JOB_BRANCHID = '{RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value}'    
                        Order By  JOB_Number DESC ";
                        break;
                    case "00005"://งานสาขา
                        sql = $@"SELECT	JOB_Number AS DATA_ID,JOBGROUPSUB_DESCRIPTION + ' : ' +JOB_DESCRIPTION AS DATA_DESC
                        FROM	SHOP_JOBCenter WITH (NOLOCK)
                        WHERE	JOB_HEAD = '1' AND JOB_STACLOSE = '0' AND JOB_BRANCHID = '{RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value}'    
                        Order By  JOB_Number DESC ";
                        break;
                    case "00007"://งานซ่อมรถ-เครื่องจักร
                        sql = $@"SELECT	JOB_Number AS DATA_ID,JOBGROUPSUB_DESCRIPTION + ' : ' +JOB_DESCRIPTION AS DATA_DESC
                        FROM	Shop_JOBCarMachine WITH (NOLOCK)
                        WHERE	JOB_STACLOSE = '0' AND JOB_BRANCHID = '{RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value}'    
                        Order By  JOB_Number DESC ";
                        break;
                    default:
                        break;
                }

                DataTable dtShow = ConnectionClass.SelectSQL_Main(sql);
                if (dtShow.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("JOB สาขา " + RadGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString());
                    return;
                }
                else
                {
                    FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV { dtData = dtShow };
                    if (frm.ShowDialog(this) == DialogResult.Yes)
                    {
                        SetJOB(frm.pID, "0");
                    }
                    else return;
                }
            }
            else //have Job Allready
            {
                SetJOB(_pJobNumber, "0");
            }
        }
        //Clear JOB
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            if (RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() == "") return;
            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("บันทึกบิลที่เลือก") == DialogResult.No) return;

            SetJOB("", "1");
        }
        //Add + Clear JOB
        void SetJOB(string pJobID, string pType) //pType 0 คือ set JOB 1 คือ Clear Job
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            if (RadGridView_Show.CurrentRow.Cells["DocNo"].Value.ToString() == "") return;

            string docno = RadGridView_Show.CurrentRow.Cells["DocNo"].Value.ToString();

            if (pType == "1") pJobID = docno;

            ArrayList sql = new ArrayList {
            $@"Update SHOP_MNRB Set Bill_JOB = '{pJobID}' WHERE DocNo = '{docno}' ",
            $@"Update SHOP_MNRG Set Bill_JOB = '{pJobID}' WHERE DocNo = '{docno}' "
            };
            string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
            if (T == "")
            {
                if (_pJobNumber == "")
                {
                    dt.Rows[RadGridView_Show.CurrentRow.Index].Delete();
                    return;
                }
                else
                {
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                return;
            }

        }
    }
}
