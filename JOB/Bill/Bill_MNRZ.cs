﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Collections;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.JOB.Bill
{
    public partial class Bill_MNRZ : Telerik.WinControls.UI.RadForm
    {
        public string pDialog;

        readonly DataTable dt_data = new DataTable();// = new DataTable();
        string pSave = "0";
        int pCopy = 0;
        string pBillNo = "";

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        readonly string tblNameHD = "SHOP_MNRZ_HD";//ตารางที่บันทึก
        readonly string tblNameDT = "SHOP_MNRZ_DT";//ตารางที่บันทึก
        readonly string pHeadBillPrint = "บิลส่งของซ่อม";//หัวบิล

        readonly string _pDptID;
        readonly string _pDptName;
        readonly string _pJobNumber;//เลขที่ JOB
        readonly string _pPhoneID; // เบอร์โทร
        readonly string _pDesc;//รายละเอียด JOB

        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //OpenF
        public Bill_MNRZ(string pDptID, string pDptName, string pJobNumber, string pPhoneID, string pDesc)
        {
            InitializeComponent();
            _pDptID = pDptID;
            _pDptName = pDptName;
            _pJobNumber = pJobNumber;
            _pPhoneID = pPhoneID;
            _pDesc = pDesc;
        }
        //Clear Open
        void ClearEnable()
        {
            radTextBox_Item.Enabled = false;
            radTextBox_ItemName.Enabled = false;
            radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Enabled = false;
            radButton_Add.Enabled = false;
            radButton_Claer.Enabled = false;
        }
        //ClearDataHD
        void ClearDataHD()
        {
            pDialog = "0";
            radLabel_Docno.Text = "";
            radTextBox_Item.Text = "";
            radTextBox_ItemName.Text = "";
            radTextBox_Unit.Text = "";
            radTextBox_Qty.Text = "";
            //radLabel_Dpt.Text = "";
            radLabel_JobNumber.Text = "";

            if (dt_data.Rows.Count > 0) dt_data.Rows.Clear();

            pSave = "0";
            pCopy = 0;
            pBillNo = "";
            radButton_Save.Enabled = true;
            radTextBox_Item.Enabled = true;
            radTextBox_ItemName.Enabled = true;
            radTextBox_Qty.Enabled = true;
            radTextBox_Unit.Enabled = true;
            radButton_Add.Enabled = true;
            radButton_Claer.Enabled = true;
            radGridView_Show.Enabled = true;

            radTextBox_Item.Focus();
        }
        //ClearDataDT
        void ClearDataDT()
        {
            radTextBox_Qty.Text = ""; radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Text = ""; radTextBox_Unit.Enabled = false;
            radTextBox_ItemName.Text = "";
            radTextBox_Item.Text = ""; radTextBox_Item.Enabled = true; radTextBox_Item.Focus();
        }
        //Load
        private void Bill_MNRZ_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radButton_Claer.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEM", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 240));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 75));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.EnableFiltering = false;

            dt_data.Columns.Add("ITEM");
            dt_data.Columns.Add("ITEMNAME");
            dt_data.Columns.Add("QTY");
            dt_data.Columns.Add("UNIT");

            ClearDataHD();
            ClearDataDT();

            radLabel_JobNumber.Text = _pJobNumber;
            radLabel_Dpt.Text = _pDptID + "-" + _pDptName;
        }

        #region clearData
        //Claer DT
        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            ClearDataDT();
        }
        #endregion
        //Insert
        void InsertDt()
        {
            if (radTextBox_Qty.Text == "" || radTextBox_Unit.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("จำนวน");
                return;
            }

            dt_data.Rows.Add(radTextBox_Item.Text, radTextBox_ItemName.Text, radTextBox_Qty.Text, radTextBox_Unit.Text);
            radGridView_Show.DataSource = dt_data;

            ClearDataDT();
        }
        //Add DGV
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            InsertDt();
        }
        //Check Num Only
        private void RadTextBox_Qty_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //Enter Data
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุทะเบียน/SN/SPC [Enter]  
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุทะเบียน/SN/SPC");
                        radTextBox_Item.Focus();
                        return;
                    }

                    DataTable dtAsset = Models.AssetClass.FindAsset_ByAllType(radTextBox_Item.Text, " AND ASSETGROUP <> 'TOOL'  ", "");
                    if (dtAsset.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ทะเบียน/SN/SPC");
                        radTextBox_Item.Focus();
                        return;
                    }

                    radTextBox_Item.Text = dtAsset.Rows[0]["ASSETID"].ToString();
                    radTextBox_ItemName.Text = dtAsset.Rows[0]["NAME"].ToString();
                    if (dtAsset.Rows[0]["ASSETID"].ToString() == "")
                    {
                        radTextBox_Unit.Text = "ชิ้น";
                    }
                    else
                    {
                        if (dtAsset.Rows[0]["UNITOFMEASURE"].ToString() == "") { radTextBox_Unit.Text = "Ea."; }
                        else { radTextBox_Unit.Text = dtAsset.Rows[0]["UNITOFMEASURE"].ToString(); }

                    }

                    radTextBox_Qty.Text = "1";
                    radTextBox_Qty.Enabled = false;
                    radTextBox_Unit.Enabled = false;
                    radButton_Add.Focus();

                    break;
                case Keys.PageDown://ระบุชื่อสินค้า [PageDown] 
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ชื่อสินค้า");
                        radTextBox_Item.Focus();
                        return;
                    }

                    radTextBox_ItemName.Text = radTextBox_Item.Text;
                    radTextBox_Item.Text = "";
                    radTextBox_Item.Enabled = false;
                    radTextBox_Unit.Text = "ชิ้น";
                    radTextBox_Unit.Enabled = true;
                    radTextBox_Qty.Enabled = true;
                    radTextBox_Qty.Focus();

                    break;
                case Keys.PageUp://ระบุบาร์โค้ด [PageUp]
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }

                    DataTable dtBarcode = ItembarcodeClass.GetItembarcode_ALLDeatil(radTextBox_Item.Text);
                    if (dtBarcode.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }
                    radTextBox_Item.Text = dtBarcode.Rows[0]["ITEMBARCODE"].ToString();
                    radTextBox_ItemName.Text = dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString();

                    if (dtBarcode.Rows[0]["UNITID"].ToString() == "")
                    {
                        radTextBox_Unit.Text = "ชิ้น";
                    }
                    else
                    {
                        radTextBox_Unit.Text = dtBarcode.Rows[0]["UNITID"].ToString();
                    }
                    radTextBox_Item.Enabled = false;
                    radTextBox_Unit.Enabled = false;
                    radTextBox_Qty.Enabled = true;
                    radTextBox_Qty.Focus();

                    break;
                default:
                    break;
            }


        }
        //Qty Enter
        private void RadTextBox_Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Unit.Enabled == false)
                {
                    InsertDt();
                    return;
                }
                radTextBox_Unit.Focus();
            }

        }
        //Unit Enter
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                InsertDt();
            }
        }
        //Delete Rows in DGV
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;

            //F2
            if (e.KeyCode == Keys.F2)
            {
                if (pSave == "0")
                {
                    string desc = radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value.ToString() + "  จำนวน  " +
                    radGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() + "  " +
                    radGridView_Show.CurrentRow.Cells["UNIT"].Value.ToString();

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete(desc) == DialogResult.No) return;

                    radGridView_Show.Rows.Remove(radGridView_Show.CurrentRow);
                    ClearDataDT();
                }
            }
        }
        //Save 
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (pSave == "0") //check ว่า save หรือ print copy
            {
                if (radGridView_Show.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการเบิก");
                    radTextBox_Item.Focus();
                    return;
                }

                if (MsgBoxClass.MsgBoxShow_ConfirmInsert(pHeadBillPrint) == DialogResult.Yes)
                {
                    SaveData();
                }
            }
            else
            {
                PrintData();
                return;
            }
        }
        void SaveData()
        {
            ArrayList strSql = new ArrayList();
            pBillNo = Class.ConfigClass.GetMaxINVOICEID("MNRZ", "-", "MNRZ", "1");
            strSql.Add(string.Format(@"INSERT INTO [dbo].[" + tblNameHD + "]   " +
                            "([DOCNO],[REMARK],[JOB_Number] " +
                            ",[DPT_ID],[DPT_NAME],[WHOINS],[WHONAMEINS] ) " +
                            "VALUES ('" + pBillNo + "','" + "ส่งของซ่อม" + "','" +
                            _pJobNumber + "','" + _pDptID + "','" + _pDptName + "','" + SystemClass.SystemUserID +
                            "','" + SystemClass.SystemUserName + "')"));
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                strSql.Add(string.Format(@"INSERT INTO [dbo].[" + tblNameDT + "]   " +
                            "([DOCNO],[LINENUM],[ITEM]," +
                            "[ITEMNAME],[QTY] " +
                            ",[UNIT] )  " +
                            "VALUES ('" + pBillNo + "','" + (i + 1) + "','" +
                            radGridView_Show.Rows[i].Cells["ITEM"].Value.ToString() + "','" +
                            radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + "','" +
                            int.Parse(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + "','" +
                            radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + "')"));
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(strSql);

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = pBillNo;
                pSave = "1";
                pCopy = 1;
                pDialog = "1";
                ClearEnable();
                PrintData();
            }
            else return;
        }
        //print
        void PrintData()
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                PrintDocument1.PrintController = printController;
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;
                //pCopyColor = "บิลชมพู"; pUpPRINTCOUNT = "0";
                //PrintDocument1.Print();
                //pCopyColor = "บิลฟ้า"; pUpPRINTCOUNT = "1";
                PrintDocument1.Print();
            }

        }
        //print
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = pBillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            e.Graphics.DrawString("พิมพ์ครั้งที่ " + pCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(pHeadBillPrint + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(_pDptID + "-" + _pDptName, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(" " + _pPhoneID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ".(" + radGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                  radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + ")   " +
                                  radGridView_Show.Rows[i].Cells["ITEM"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(" " + radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("รวมจำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString() + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

            Y += 20;
            string jobDesc = _pJobNumber + " " + _pDesc;
            Rectangle rect1 = new Rectangle(0, Y, 310, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };

            e.Graphics.DrawString(jobDesc, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            e.Graphics.PageUnit = GraphicsUnit.Inch;

            pCopy += 1;
            string upStr = string.Format(@"UPDATE [" + tblNameHD + "] SET PRINTCOUNT = '" + pCopy + "' WHERE DOCNO = '" + pBillNo + "' ");
            ConnectionClass.ExecuteSQL_Main(upStr);


        }
        //Select Change
        private void RadDropDownList_Dpt_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Item.Focus();
        }
    }

}

