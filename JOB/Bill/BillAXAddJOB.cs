﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing.Printing;
using System.Collections;

namespace PC_Shop24Hrs.JOB.Bill
{
    public partial class BillAXAddJOB : Telerik.WinControls.UI.RadForm
    {
        public string sSendDialog;//ในกรณีที่เป็น 1 หมายถึงว่าได้บันทึกบิลไปแล้ว
        string _pBchID;
        string _pBchName;

        readonly string _pTypeReturn; //0 คือไม่ต้องคืนซาก 1 คือต้องคืนซาก
        readonly string _pJobNumber;
        readonly string _pGrpID; //กลุ่ม
        readonly string _pGrpName;//ชื่อกลุ่ม
        readonly string _pSN;
        readonly string _pIP;
        readonly string _pBillID;
        readonly string _pStaOpenImport;//กรณีที่ เปิดจากหน้า Import เบิกภายใน ก็สามารถ Import บิลได้เลย
                                        //readonly string _pCAMID;
                                        //readonly string _pCAMDESC;
        readonly string _pCase = ""; // 1 : เคสสำหรับ Job ซ่อมเครื่องจักรโรงงาน // 2 สำหรับการบันทึกรับบิลจัดส่งเพื่อไว้ขึ้นทะเบียนส่งของ SH

        string billOpenId;
        string billOpenName;

        string staAss = "0"; //0 ไม่ต้องปรับทะเบียน บิล I 1 ต้องปรับทะเบียนบิล F,FAL
        string typeBill = "I"; //ประเภทบิล I,F,FAL
        string staINVENTCOSTCENTERID = "0"; //สถานะงบเบิกควมคุม
        string pCopyColor = "";
        int pCopy = 0;
        string pUpPRINTCOUNT = "0";
        // DataTable dtINVENTCOSTCENTERID;// //งบเบิกควมคุม
        string DptID; //แผนกที่เบิกลง
        string DptName; //แผนกที่เบิกลง
        string pTypeBill;
        string HeadBill;
        DataTable dtBill = new DataTable();


        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();
        //clearData
        void ClearData()
        {
            billOpenId = ""; billOpenName = "";
            radLabel_BillGroup.Text = "";
            radLabel_BranchName.Text = "";
            radLabel_Cost.Text = "";
            radLabel_CreateID.Text = "";
            radLabel_CreateName.Text = "";
            radLabel_Date.Text = "";
            radLabel_Remark.Text = "";
            radLabel_WH.Text = "";
            staAss = "0"; typeBill = "I";
            radToggleSwitch_StaBillApv.SetToggleState(true);
            radToggleSwitch_24.SetToggleState(true);
            radTextBox_BillID.Text = "";
            radTextBox_BillID.Enabled = true;
            radButton_print.Enabled = false;
            radButton_Save.Enabled = false;
            radLabel_F2.Text = ""; radLabel_F2.Visible = false;

            radTextBox_BillID.Focus();

            if (dtBill.Rows.Count > 0) dtBill.Rows.Clear();

            if (_pJobNumber == "")
            {
                radTextBox_BillID.Enabled = true;
                radTextBox_BillID.Focus();
            }
            else radTextBox_BillID.Enabled = true;

            radGridView_Detail.DataSource = null;
            if (radGridView_Detail.Rows.Count > 0) radGridView_Detail.Rows.Clear();
           
        }
        public BillAXAddJOB(string pBchID, string pBchName, string pTypeReturn, string pJobNumber,
            string pGrpID, string pGrpName, string pSN, string pIP, string pBillID, string pStaOpenImport, string pCase = "")
        {
            InitializeComponent();

            _pBchID = pBchID;
            _pBchName = pBchName;
            _pTypeReturn = pTypeReturn;
            _pJobNumber = pJobNumber;
            _pGrpID = pGrpID;
            _pGrpName = pGrpName;
            _pSN = pSN;
            _pIP = pIP;
            _pBillID = pBillID;
            _pStaOpenImport = pStaOpenImport;
            _pCase = pCase;
        }
        //load
        private void BillAXAddJOB_Load(object sender, EventArgs e)
        {
            ClearData();

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radButton_print.ButtonElement.ShowBorder = true; radButton_print.ButtonElement.ToolTipText = "สั่งพิมพ์";
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Detail);
            //Bill Detail           
            radGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SerialNum", "S/N", 150));
            radGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค้ด", 140));
            radGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 240));
            radGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 90));
            radGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 90));
            radGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COST", "ราคารวม", 120));
            this.radGridView_Detail.EnableFiltering = false;

            //Freeze Column
            for (int i = 0; i < 2; i++)
            {
                this.radGridView_Detail.MasterTemplate.Columns[i].IsPinned = true;
            }

            if (_pBillID != "")
            {
                radTextBox_BillID.Enabled = false;
                radTextBox_BillID.Text = _pBillID;
                SetDATA();
            }
        }
        //Enter Load Data
        private void RadTextBox_BillID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_BillID.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("เลขที่บิล");
                    radTextBox_BillID.SelectAll();
                    radTextBox_BillID.Focus();
                    return;
                }

                SetDATA();
            }
        }
        //Set DATA
        void SetDATA()
        {

            DataTable dtPXE = new DataTable();
            dtBill = BillOutClass.FindBillAX_ByBillID(radTextBox_BillID.Text.Trim());

            if (dtBill.Rows.Count == 0)
            {
                if (_pCase == "1")
                {
                    dtPXE = PurchClass.GetDetailPD_ByDocno(radTextBox_BillID.Text.Trim());
                    if (dtPXE.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่บิล");
                        radTextBox_BillID.SelectAll();
                        radTextBox_BillID.Focus();
                        return;
                    }
                    HeadBill = "PXE";
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่บิล");
                    radTextBox_BillID.SelectAll();
                    radTextBox_BillID.Focus();
                    return;
                }
            }

            if (_pCase == "1")//เชคสำหรับการ +สต็อกอะไหล่ โรงงานเท่านั้น
            {
                if (HeadBill == "PXE")
                {
                    if (dtPXE.Rows[0]["PurchStatus"].ToString() != "3")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"บิล {radTextBox_BillID.Text.Trim()} จะต้องออกใบแจ้งหนี้แล้วเท่านั้น ไม่สามารถทำรายการได้");
                        radTextBox_BillID.SelectAll();
                        radTextBox_BillID.Focus();
                        return;
                    }
                }

                string SQL = $@"SELECT	*
                                    FROM	SHOP_MNIO_HD WITH (NOLOCK)
                                    WHERE	DOCNO = '{radTextBox_BillID.Text}'";
                DataTable dt = ConnectionClass.SelectSQL_Main(SQL);

                if (dt.Rows.Count > 0) //เช็คบิลซ้ำ
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เลขที่บิล : {radTextBox_BillID.Text}{Environment.NewLine}มีการอนุมัติแล้ว ไม่สามารถทำรายการซ้ำได้.");
                    radTextBox_BillID.Text = "";
                    radTextBox_BillID.Focus();
                    radButton_Save.Enabled = false;
                    return;
                }

                if (HeadBill == "PXE")
                {
                    if (dtPXE.Rows[0]["DIMENSION"].ToString() != "D177")//check แผนกเบิกก่อนว่าเป็น d177 มั้ย
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เลขที่บิล : {radTextBox_BillID.Text}{Environment.NewLine}ไม่ได้เบิกลงแผนกวัสดสิ้นเปลือง เช็คใหม่อีกครั้ง.");
                        radTextBox_BillID.Text = "";
                        radTextBox_BillID.Focus();
                        radButton_Save.Enabled = false;
                        return;
                    }
                }
                else
                {
                    if (dtBill.Rows[0]["BRANCH_ID"].ToString() != "D177")//check แผนกเบิกก่อนว่าเป็น d177 มั้ย
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เลขที่บิล : {radTextBox_BillID.Text}{Environment.NewLine}ไม่ได้เบิกลงแผนกวัสดสิ้นเปลือง เช็คใหม่อีกครั้ง.");
                        radTextBox_BillID.Text = "";
                        radTextBox_BillID.Focus();
                        radButton_Save.Enabled = false;
                        return;
                    }
                }
            }

            if (HeadBill != "PXE")
            {
                if (dtBill.Rows[0]["Bill24"].ToString() == "0") radToggleSwitch_24.SetToggleState(true); else radToggleSwitch_24.SetToggleState(false);

                DptID = dtBill.Rows[0]["BRANCH_ID"].ToString();
                DptName = dtBill.Rows[0]["BRANCH_NAME"].ToString();
                radTextBox_BillID.Text = dtBill.Rows[0]["Bill_ID"].ToString().ToUpper();
                pTypeBill = dtBill.Rows[0]["TYPE_BILL"].ToString();

                radTextBox_BillID.Enabled = false;

                radLabel_BranchName.Text = DptID + "-" + dtBill.Rows[0]["BRANCH_NAME"].ToString();
                radLabel_Cost.Text = dtBill.Rows[0]["INVENTCOSTCENTERID"].ToString();
                radLabel_CreateID.Text = dtBill.Rows[0]["EmplId"].ToString();
                radLabel_CreateName.Text = dtBill.Rows[0]["SPC_NAME"].ToString() +
                    "   [" + dtBill.Rows[0]["DPT_ID"].ToString() + "-" + dtBill.Rows[0]["DPT_NAME"].ToString() + "]";
                radLabel_Date.Text = dtBill.Rows[0]["DATEBill"].ToString();
                radLabel_Remark.Text = dtBill.Rows[0]["RMK"].ToString();
                radLabel_WH.Text = dtBill.Rows[0]["INVENTLOCATIONIDFROM"].ToString();

                radGridView_Detail.DataSource = dtBill;

                billOpenId = dtBill.Rows[0]["EmplId"].ToString();
                billOpenName = dtBill.Rows[0]["SPC_NAME"].ToString();

                if (radTextBox_BillID.Text.ToUpper().Substring(0, 3) == "IDM") typeBill = "IDM";
                if (radTextBox_BillID.Text.ToUpper().Substring(0, 1) == "F")
                {
                    staAss = "1"; typeBill = "F";
                    if (radTextBox_BillID.Text.ToUpper().Substring(0, 3) == "FAL") typeBill = "FAL";
                    radLabel_F2.Text = "double click | F2 " + Environment.NewLine + "ปรับทะเบียน";
                    radLabel_F2.Visible = true;
                }
                else
                {
                    radLabel_F2.Visible = true;
                    radLabel_F2.Text = "ไม่ต้องปรับทะเบียน";
                }

                if (dtBill.Rows[0]["Apps"].ToString() == "1")
                {
                    radToggleSwitch_StaBillApv.SetToggleState(false);
                    radButton_Save.Enabled = true;
                }
                else radToggleSwitch_StaBillApv.SetToggleState(true);

                staINVENTCOSTCENTERID = dtBill.Rows[0]["STA_INSTALL"].ToString();
            }
            else
            {
                radToggleSwitch_24.SetToggleState(true);

                DptID = dtPXE.Rows[0]["DIMENSION"].ToString();
                DptName = dtPXE.Rows[0]["DESCRIPTION"].ToString();
                radTextBox_BillID.Text = dtPXE.Rows[0]["PurchId"].ToString().ToUpper(); radTextBox_BillID.Enabled = false;
                pTypeBill = "PXE";

                radLabel_BranchName.Text = DptID + "-" + DptName;
                radLabel_Cost.Text = "-";
                radLabel3.Text = "ผู้จำหน่าย";  //ผู้ทำเบิก -> ผู้ขาย
                radLabel_CreateID.Text = dtPXE.Rows[0]["OrderAccount"].ToString();
                radLabel_CreateName.Text = dtPXE.Rows[0]["PurchName"].ToString();
                radLabel_Date.Text = dtPXE.Rows[0]["SPC_PurchDate"].ToString();
                radLabel_Remark.Text = dtPXE.Rows[0]["SPC_Remarks"].ToString();
                radLabel_WH.Text = "-";

                //radGridView_Detail.DataSource = dtBill;
                for (int i = 0; i < dtPXE.Rows.Count; i++)
                {
                    radGridView_Detail.Rows.Add("",
                                                dtPXE.Rows[i]["BarCode"].ToString(),
                                                dtPXE.Rows[i]["Name"].ToString(),
                                                dtPXE.Rows[i]["PurchQty"].ToString(),
                                                dtPXE.Rows[i]["PurchUnit"].ToString(),
                                                dtPXE.Rows[i]["LineAmount"].ToString());
                }

                billOpenId = dtPXE.Rows[0]["OrderAccount"].ToString();
                billOpenName = dtPXE.Rows[0]["PurchName"].ToString();

                radLabel_F2.Visible = true;
                radLabel_F2.Text = "ไม่ต้องปรับทะเบียน";
                radToggleSwitch_StaBillApv.SetToggleState(false);
                staINVENTCOSTCENTERID = "0";
            }


            if (_pCase == "")
            {
                if (_pJobNumber == "")
                {
                    _pBchID = DptID;
                    _pBchName = DptName;
                    radButton_Save.Enabled = false;
                    if (radToggleSwitch_24.Value == false)
                    {
                        if (staAss == "1")
                        {
                            _pBchID = DptID; _pBchName = DptName;
                            radButton_print.Enabled = true; pCopy = Convert.ToInt32(dtBill.Rows[0]["PRINTCOUNT"].ToString());
                            return;
                        }

                        radButton_print.Enabled = false;
                        return;
                    }
                    else
                    {
                        if ((_pStaOpenImport == "0") || (dtBill.Rows[0]["Apps"].ToString() == "1"))
                        { radButton_Save.Enabled = true; return; }
                        else { radButton_Save.Enabled = false; return; }
                    }
                }
                else
                {
                    if (radToggleSwitch_24.Value == false)//Import ok
                    {
                        radButton_Save.Enabled = false;
                        if (staAss == "1")
                        { radButton_print.Enabled = true; pCopy = Convert.ToInt32(dtBill.Rows[0]["PRINTCOUNT"].ToString()); return; }
                    }
                    else
                    {
                        if (dtBill.Rows[0]["Apps"].ToString() == "0") radButton_Save.Enabled = false;
                        else radButton_Save.Enabled = true;
                        return;
                    }
                }

                if ((typeBill.StartsWith("F")) & (radButton_Save.Enabled == true))
                {
                    for (int i = 0; i < radGridView_Detail.Rows.Count - 1; i++)
                    {
                        if (Convert.ToDouble(radGridView_Detail.Rows[i].Cells["COST"].Value) == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"รายการเบิกมีราคารวมเท่ากับ 0{Environment.NewLine}
                        ตั้งค่าราคาต้นทุนในรายการสินทรัพย์ให้เรียบร้อย{Environment.NewLine}ก่อนกดบันทึกอีกครั้ง");
                            radButton_Save.Enabled = false;
                            break;
                        }
                    }
                }

            }//กรณีบิลปกติ เมนูสำหรับนำเข้า shop_bilLOut

            else if (_pCase == "1")//สำหรับอะไหล่ โรงงานให้เปิดได้ถ้าผ่านการเช็ค D177 และยังไม่มีการ +สต็อกบิลนี้
            { radButton_Save.Enabled = true; }
            else if (_pCase == "2")//สำหรับจัดส่ง
            {
                radLabel_F2.Visible = false;
                radToggleSwitch_24.Visible = false;
                radButton_print.Visible = false;
                string sqlCheck = $@"
                    SELECT	*	FROM	SHOP_SHIPDOCNOTSEND WITH (NOLOCK)
                    WHERE	LO = '{radTextBox_BillID.Text}' ";

                if (ConnectionClass.SelectSQL_Main(sqlCheck).Rows.Count > 0) radButton_Save.Enabled = false; else radButton_Save.Enabled = true;

            }

        }

        #region "SetFontRows"
        private void RadGridView_Detail_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Detail_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Detail_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Detail_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        #endregion

        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (_pJobNumber == "") ClearData();
            else
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }

        }
        //save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (_pCase == "")
            {
                //check หัวบิล + สาขา
                if (DptID != _pBchID)
                {
                    if (_pGrpID == "00003")
                    {
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"บิลที่ระบุไม่ตรงกับแผนกของ JOB{Environment.NewLine}ยืนยันการเพิ่ม JOB บิลเบิกไม่ตรงกัน") == DialogResult.No)
                        {
                            radTextBox_BillID.SelectAll();
                            radTextBox_BillID.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("บิลที่ระบุไม่ตรงกับแผนกของ JOB");
                        radTextBox_BillID.SelectAll();
                        radTextBox_BillID.Focus();
                        return;
                    }
                }

                if (MsgBoxClass.MsgBoxShow_ConfirmInsert("บิลเบิก") == DialogResult.No) return;

                string staReturn = "0";
                string staReturnRecive;
                if ((_pTypeReturn == "1") || (_pStaOpenImport == "0"))
                {
                    FormShare.ChooseData frm = new FormShare.ChooseData("มีซาก", "ไม่มีซาก");
                    if (frm.ShowDialog(this) == DialogResult.Yes)
                    {
                        staReturn = frm.sSendData;
                    }
                }

                staReturnRecive = staReturn;
                string staRecive = "0";
                string staBchRecive = "1";
                if (_pBchID.StartsWith("M"))
                {
                    staRecive = "1"; staBchRecive = "0";
                }
                else
                {
                    staReturn = "0";
                }
                string sql;
                if (pTypeBill == "MNRS")
                {
                    sql = $@"UPDATE SHOP_RO_MNRSHD 
                        SET     JOB_NUMBER = '{_pJobNumber}',STA_INSTALL = '{staINVENTCOSTCENTERID}'   
                        WHERE   DOCNO = '{radTextBox_BillID.Text}' ";
                }
                else
                {
                    sql = $@" INSERT INTO [SHOP_BillOut] 
                  ( [Bill_ID],[Bill_INVENTCOSTCENTERID]
                  ,[JOB_NUMBER],[WH],[BRANCH_ID],[BRANCH_NAME]
                  ,[WHOOPENBILL_ID],[WHOOPENBILL_NAME]
                  ,[REMARK]
                  ,[WHOINS],[WHONAMEINS],[TYPEBILL],[JOBGROUP_ID],[JOBGROUP_DESCRIPTION],DATE
                  ,[STA_RECIVE],[STA_INSTALL],[STA_RETURN],[STA_RETURNRECIVE],Bill_TypeBchStatus
                  ) VALUES 
                  ('{radTextBox_BillID.Text}','{radLabel_Cost.Text}',
                  '{_pJobNumber}','{radLabel_WH.Text}','{_pBchID}','{_pBchName}',
                  '{billOpenId}','{billOpenName}','{ConfigClass.ChecKStringForImport(radLabel_Remark.Text)}',
                  '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}',
                  '{typeBill}','{_pGrpID}','{_pGrpName}','{radLabel_Date.Text}',
                  '{staRecive}','{staINVENTCOSTCENTERID}','{staReturn}','{staReturnRecive}','{staBchRecive}'  ) ";
                }
                string T = ConnectionClass.ExecuteSQL_Main(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    sSendDialog = "1";
                    if ((staAss == "0"))
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        radButton_Save.Enabled = false;
                        if ((typeBill == "F")) { radButton_print.Enabled = false; }
                        else { radButton_print.Enabled = true; pCopy = 0; }
                        radToggleSwitch_24.Value = false;
                    }
                }
            }
            else if (_pCase == "1")
            {
                string SELECTBardoce = BillOutClass.GetData_BillAXAddJOB();
                if (HeadBill == "PXE") SELECTBardoce = BillOutClass.GetData_BillAXAddJOB(HeadBill);
                ArrayList strSql = new ArrayList();
                string REMARK = radLabel_Remark.Text;
                if (radLabel_Remark.Text == "") REMARK = "สต็อกอะไหล่";

                strSql.Add($@"  DECLARE              @Docno AS NVARCHAR(50) = '{radTextBox_BillID.Text}';
                                                DECLARE              @UserID AS NVARCHAR(50) = '{SystemClass.SystemUserID}';
                                                DECLARE              @UserName AS NVARCHAR(500) = '{SystemClass.SystemUserName}';
                                                DECLARE              @Data table (BARCODE VARCHAR(50),ITEMNAME VARCHAR(500),QTY float,Unit VARCHAR(50) );
                                                --return Successfully = '' if error return mess
                                                DECLARE       @Msg AS NVARCHAR(500)

                                                BEGIN TRY

                                                              INSERT INTO @Data            
                                                                                  {SELECTBardoce}

                                                              DECLARE              @BARCODE AS NVARCHAR(50);
                                                              DECLARE              @ITEMNAME AS NVARCHAR(500);
                                                              DECLARE              @QTY AS float;
                                                              DECLARE              @Unit AS NVARCHAR(50);

                                                              WHILE EXISTS (SELECT 1 FROM @Data)
                                                              BEGIN
                                                                     SELECT TOP 1 @BARCODE = BARCODE from @Data        
                                                                     SELECT TOP 1 @ITEMNAME      = ITEMNAME from @Data       
                                                                     SELECT TOP 1 @QTY           = QTY from @Data     
                                                                     SELECT TOP 1 @Unit          = Unit from @Data        

                                                                     IF NOT EXISTS  (
                                                                           SELECT *      FROM   [SHOP_STOCKITEM] WITH (NOLOCK) WHERE  StockID = @BARCODE
                                                                     )
                                                                     BEGIN
                                                                           INSERT INTO  [SHOP_STOCKITEM]
                                                                                                  ([StockID],[StockName],[StockType],[StockQty],[StockUnit],[WHOINS],[WHONAMEINS]) 
                                                                           VALUES (@BARCODE,@ITEMNAME,'00516',@QTY,@Unit,@UserID,@UserName)
                                                                     END
                                                                     ELSE
                                                                     BEGIN
                                                                           UPDATE  [SHOP_STOCKITEM]
                                                                           SET     StockQty = @QTY+(StockQty),WHOIDUPD = @UserID,WHONAMEUPD = @UserName,DATEUPD = convert(varchar,getdate(),25)
                                                                           WHERE  [StockID] = @BARCODE
                                                                     END

                                                                     DELETE FROM @Data WHERE BARCODE = @BARCODE
                                                              END

                                                              SET @Msg=''
                                                END TRY
                                                BEGIN CATCH
                                                    SET @Msg = ERROR_MESSAGE()
                                                END CATCH

                                                SELECT @Msg AS RESUALT
                ");

                //string pBillNo = Class.ConfigClass.GetMaxINVOICEID("MNII", "-", "MNII", "1");
                strSql.Add($@"INSERT INTO   [dbo].[SHOP_MNIO_HD]
                                            ([DOCNO],[REMARK],[JOBGROUP_ID],
                                             [JOBGROUP_DESCRIPTION],[JOB_Number],[BRANCH_ID],
                                             [BRANCH_NAME],[WHOINS],[WHONAMEINS] )
                              VALUES        ('{radTextBox_BillID.Text}','{REMARK}','00014',
                                             'เครื่องจักรโรงงาน','','{DptID}',
                                             '{DptName}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')");
                for (int i = 0; i < radGridView_Detail.Rows.Count; i++)
                {
                    int QTY = (-1) * (int)float.Parse(radGridView_Detail.Rows[i].Cells["QTY"].Value.ToString());
                    if (HeadBill == "PXE") QTY = (int)float.Parse(radGridView_Detail.Rows[i].Cells["QTY"].Value.ToString());
                    strSql.Add($@"INSERT INTO   [dbo].[SHOP_MNIO_DT]
                                                ([DOCNO],[LINENUM],[ITEM],
                                                 [ITEMNAME],[QTY],[UNIT])
                                  VALUES        ('{radTextBox_BillID.Text}','{(i + 1)}','{radGridView_Detail.Rows[i].Cells["BARCODE"].Value}',
                                                 '{ConfigClass.ChecKStringForImport(radGridView_Detail.Rows[i].Cells["ITEMNAME"].Value.ToString())}',
                                                 '{QTY}','{radGridView_Detail.Rows[i].Cells["UNIT"].Value}')");
                }

                //strSql.Add($@"
                //    Update	[SHOP_STOCKITEM] Set StockQty = DocQty+(StockQty),WHOIDUPD = '{SystemClass.SystemUserID}',
                //            WHONAMEUPD = '{SystemClass.SystemUserName}',DATEUPD = convert(varchar,getdate(),25)
                //    FROM	[SHOP_STOCKITEM] WITH (NOLOCK)  INNER JOIN    
                //             (SELECT ITEM AS DocItem,SUM(QTY) AS DocQty FROM [SHOP_MNIO_DT] WITH (NOLOCK) 
                //                INNER JOIN [SHOP_MNIO_HD] WITH (NOLOCK) ON  [SHOP_MNIO_DT].DOCNO = [SHOP_MNIO_HD].DOCNO  
                //    WHERE	[SHOP_MNIO_HD].DOCNO = '{radTextBox_BillID}'  GROUP BY ITEM)TMP 
                //            ON [SHOP_STOCKITEM].StockID = TMP.DocItem
                //");

                string T = ConnectionClass.ExecuteSQL_ArrayMain(strSql);

                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    radButton_Save.Enabled = false;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else if (_pCase == "2")//สำหรับจัดส่ง
            {
                ArrayList sqlIn = new ArrayList() { $@" 
                    INSERT INTO [SHOP_SHIPDOCNOTSEND]
                               ([LO],[BranchID],[BranchName],[DriverID],[DriverName],
                                [Remark],[WhoIns],[WhoNameIns],[RemarkBill],[ProgramUse],[TYPEDATA])
                    VALUES	   ('{radTextBox_BillID.Text}','{DptID}','{DptName}','{billOpenId}','{billOpenName}',
                                '{this.Text}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','','PC','BILL') " };

                ArrayList sqlAX = new ArrayList() { AX_SendData.Save_EXTERNALLIST_SPC_SHIPMENTINVOICE(radTextBox_BillID.Text, radLabel_Date.Text, DptID, DptName) };
                string T = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn, sqlAX);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "") ClearData();
            }
        }
        //adjust asset
        private void RadGridView_Detail_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pCase != "") return;
            
            EditData();
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = radTextBox_BillID.Text;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            if (pUpPRINTCOUNT == "0") { pCopy += 1; }

            e.Graphics.DrawString("พิมพ์ครั้งที่ " + pCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("เอกสารเคลื่อนย้ายสินทรัพย์" + " [" + pCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(_pBchID + "-" + _pBchName, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่บิล " + Convert.ToDateTime(radLabel_Date.Text).ToString("dd-MM-yyyy"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            for (int i = 0; i < radGridView_Detail.Rows.Count; i++)
            {
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ". " + radGridView_Detail.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(radGridView_Detail.Rows[i].Cells["BARCODE"].Value.ToString() + "  [ S/N - " +
                    radGridView_Detail.Rows[i].Cells["SerialNum"].Value.ToString() + "]",
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("รวมจำนวนทั้งหมด " + radGridView_Detail.Rows.Count.ToString() + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้ทำเบิก : " + billOpenId + "-" + billOpenName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;

            string pJobRmk = "[" + _pJobNumber + "] ";
            if (_pJobNumber == "")
            {
                pJobRmk = "[" + dtBill.Rows[0]["JOB_NUMBER"].ToString();
            }
            e.Graphics.DrawString("หมายเหตุ : " + pJobRmk, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            //Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(radLabel_Remark.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            //Y += 30;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;

            if (pUpPRINTCOUNT == "1")
            {
                //pCopy = pCopy + 1;
                string upStr = string.Format(@"UPDATE  SHOP_BillOut SET PRINTCOUNT = '" + pCopy + "' WHERE Bill_ID = '" + radTextBox_BillID.Text + "' ");
                ConnectionClass.ExecuteSQL_Main(upStr);
            }

        }

        private void RadGridView_Detail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2) EditData();
        }
        //editData
        void EditData()
        {
            if (_pJobNumber == "")
            {
                if ((staAss == "0") || (radToggleSwitch_StaBillApv.Value == true) || (radToggleSwitch_24.Value == true)) return;
            }
            else
            {
                if ((staAss == "0") || (radToggleSwitch_StaBillApv.Value == true) || (radButton_Save.Enabled == true) || (radToggleSwitch_24.Value == true)) return;
            }

            if ((radGridView_Detail.CurrentRow.Cells["BARCODE"].Value.ToString() == "") || (radGridView_Detail.CurrentRow.Index == -1)) return;

            ASSET.AdjustAsset frm = new ASSET.AdjustAsset
                (radGridView_Detail.CurrentRow.Cells["BARCODE"].Value.ToString(),
                radTextBox_BillID.Text, _pJobNumber, _pBchID, _pBchName, _pSN, _pIP);
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                return;
            }
        }
        //Print
        private void RadButton_print_Click(object sender, EventArgs e)
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                PrintDocument1.PrintController = printController;
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;

                pCopyColor = "บิลฟ้า"; pUpPRINTCOUNT = "0";
                PrintDocument1.Print();

                if (SystemClass.SystemDptID != "D179")
                {
                    if (SystemClass.SystemDptID != "D156")
                    {
                        pCopyColor = "บิลชมพู"; pUpPRINTCOUNT = "1";
                        PrintDocument1.Print();
                    }
                }

            }
        }


    }
}
