﻿//CheckOK
using System;
using System.Data;
using System.IO;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB
{
    class JOB_Class
    {
        //MyJeeds ค้นหาประเภท Group JOB 
        public static DataTable GetJOB_Group(string pGrp)
        {
            string sql = $@"
                    SELECT	[SHOP_JOBGROUP].[JOBGROUP_ID],[JOBGROUP_DESCRIPTION],[DOCUMENTTYPE_ID] ,[TABLENAME] 
                    FROM    [SHOP_JOBGROUP] WITH(NOLOCK)
                    WHERE   [SHOP_JOBGROUP].[JOBGROUP_ID] = '{pGrp}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //MyJeeds ค้นหาประเภท GROUP SUB JOB 
        public static DataTable GetJOB_GroupSub(string _grpID)
        {
            string sql = $@"
                    SELECT	[JOBGROUPSUB_ID],[JOBGROUPSUB_DESCRIPTION],[REMARK],'แสดงคู่มือ' AS Doc 
                    FROM	SHOP_JOBGROUPSUB WITH (NOLOCK)
                    WHERE	JOBGROUP_ID = '{_grpID}'  ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        //MyJeeds ค้นหาชื่อช่างรับผิดชอบสาขา
        public static string GetTechnicianl_ByBranchID(string pCamIP)
        {
            string sql = $@" 
                        SELECT	[CAM_TECHNICID],[CAM_TECHNICNAME]   
                        FROM    [SHOP_JOBCAM] WITH (NOLOCK)
                        WHERE	[BRANCH_ID] = '{pCamIP}' OR [CAM_IP] = '{pCamIP}'";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) return "ช่างผู้รับผิดชอบ " + dt.Rows[0]["CAM_TECHNICID"].ToString() + "-" + dt.Rows[0]["CAM_TECHNICNAME"].ToString(); else return "";
        }
        //MyJeeds Set สถานะจ๊อบ
        public static DataTable GetJOBStatus()
        {
            DataTable dt = new DataTable("dt");
            dt.Columns.Add("STA_ID");
            dt.Columns.Add("STA_NAME");
            dt.Rows.Add("01", "เปิดจ๊อบ");
            dt.Rows.Add("02", "รอซ่อม");
            dt.Rows.Add("03", "กำลังซ่อม");
            dt.Rows.Add("04", "ซ่อมเสร็จ");
            return dt;
        }

        //Set Grid รูปตาม JOB
        public static void FindImageBillAll_ByJobID(DataTable dt, RadGridView radGridView_Show)
        {
            radGridView_Show.DataSource = dt;
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                System.String findDpt = dt.Rows[i]["BRANCH_ID"].ToString();
                if (findDpt.StartsWith("D"))
                {
                    findDpt = "MN000";
                }
                //ที่เก็บบิล
                string pPathMain = PathImageClass.pPathBillOut;
                string pTypeMNCM = "0";
                //string pConN = "_N";
                if (dt.Rows[i]["Bill_ID"].ToString().Substring(0, 3) == "IDM")
                {
                    pPathMain = PathImageClass.pPathBillOutIDM;
                }
                else if (dt.Rows[i]["Bill_ID"].ToString().Substring(0, 4) == "MNCM")
                {
                    pPathMain = PathImageClass.pPathAssetClaimLeave;
                    pTypeMNCM = "1";
                    //pConN = "_C*_N";
                }
                else if (dt.Rows[i]["Bill_ID"].ToString().Substring(0, 4) == "MNRD")
                {
                    pPathMain = PathImageClass.pPathCN;
                    pTypeMNCM = "1";
                    //pConN = "_C*_N";
                }
                string bill_Path = pPathMain + @"\" + dt.Rows[i]["Bill_Date"].ToString() + @"\" + findDpt;
                string bill_Find = "*_" + dt.Rows[i]["Bill_ID"].ToString();
                DirectoryInfo DirInfo = new DirectoryInfo(bill_Path);
                //ขาส่งของ S
                try
                {
                    string pConBill = "_S";
                    if (dt.Rows[i]["Bill_ID"].ToString().Substring(0, 4) == "MNRD")
                    {
                        pConBill = "";
                    }
                    FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + pConBill + @".*", SearchOption.AllDirectories);
                    string fullNameSPC = PathImageClass.pImageEmply;
                    if (FilesSPC.Length > 0)
                    {
                        fullNameSPC = FilesSPC[0].FullName;
                    }

                    radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + pConBill + ".*";
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                }
                //ขารับของ N

                try
                {
                    FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + "_N.*", SearchOption.AllDirectories);
                    string fullNameMN = PathImageClass.pImageEmply;
                    string pConN = "_N";
                    if ((FilesMN.Length == 0) && (pTypeMNCM == "1"))
                    {
                        FilesMN = DirInfo.GetFiles(bill_Find + "_C.*", SearchOption.AllDirectories);
                        pConN = "_C";
                    }

                    if (FilesMN.Length > 0)
                    {
                        fullNameMN = FilesMN[0].FullName;
                    }

                    radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                    radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + pConN + ".*";
                    radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = FilesMN.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = "";
                    radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = "0";
                }
                //ขารับของ E
                try
                {
                    FileInfo[] FilesEND = DirInfo.GetFiles(bill_Find + "_E.*", SearchOption.AllDirectories);
                    string fullNameEND = PathImageClass.pImageEmply;
                    if (FilesEND.Length > 0)
                    {
                        fullNameEND = FilesEND[0].FullName;
                    }

                    radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameEND);
                    radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = bill_Path + @"|" + bill_Find + "_E.*";
                    radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = FilesEND.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = "";
                    radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = "0";
                }
                //ขารับของ O
                try
                {
                    FileInfo[] FilesOLD = DirInfo.GetFiles(bill_Find + "_O.*", SearchOption.AllDirectories);
                    string fullNameOLD = PathImageClass.pImageEmply;
                    if (FilesOLD.Length > 0)
                    {
                        fullNameOLD = FilesOLD[0].FullName;
                    }

                    radGridView_Show.Rows[i].Cells["Bill_OLD"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameOLD);
                    radGridView_Show.Rows[i].Cells["Bill_OLD_Path"].Value = bill_Path + @"|" + bill_Find + "_O.*";
                    radGridView_Show.Rows[i].Cells["Bill_OLD_Count"].Value = FilesOLD.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[i].Cells["Bill_OLD"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[i].Cells["Bill_OLD_Path"].Value = "";
                    radGridView_Show.Rows[i].Cells["Bill_OLD_Count"].Value = "0";
                }
                //ขารับของ R
                try
                {
                    FileInfo[] FilesOLDSPC = DirInfo.GetFiles(bill_Find + "_R.*", SearchOption.AllDirectories);
                    string fullNameOLDSPC = PathImageClass.pImageEmply;
                    if (FilesOLDSPC.Length > 0)
                    {
                        fullNameOLDSPC = FilesOLDSPC[0].FullName;
                    }

                    radGridView_Show.Rows[i].Cells["Bill_OLDSPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameOLDSPC);
                    radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Path"].Value = bill_Path + @"|" + bill_Find + "_R.*";
                    radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Count"].Value = FilesOLDSPC.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[i].Cells["Bill_OLDSPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Path"].Value = "";
                    radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Count"].Value = "0";
                }
            }

        }
        //Set Grid
        public static void SetHeadGrid_ForShowImage(RadGridView radGridView_Show, string pType)//pType 0=ใส่หัวบิลด้วย 1=เอาเฉพาะรูปเท่านั้น
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("Bill_SpcType", "ตรวจ", 40)));
            if (pType == "0")
            {
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("Bill_TypeBchStatus", "รับ", 40)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Bill_ID", "เลขที่บิล", 150)));
            }
            else
            {
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_ID", "เลขที่บิล")));
            }
            // S
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
            // N
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", "รับของ", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));
            // E
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_END", "ติดตั้ง", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Count", "Bill_END_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Path", "Bill_END_Path")));
            // O
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_OLD", "ส่งซาก", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLD_Count", "Bill_OLD_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLD_Path", "Bill_OLD_Path")));
            // R
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_OLDSPC", "รับซาก", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLDSPC_Count", "Bill_OLDSPC_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLDSPC_Path", "Bill_OLDSPC_Path")));

            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TypeOpenBill", "TypeOpenBill")));

            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_INSTALL", "STA_INSTALL")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RETURN", "STA_RETURN")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RETURNRECIVE", "STA_RETURNRECIVE")));
            radGridView_Show.TableElement.RowHeight = 110;
            radGridView_Show.MasterTemplate.Columns[0].IsPinned = true;
            radGridView_Show.MasterTemplate.Columns[1].IsPinned = true;
            radGridView_Show.MasterTemplate.EnableFiltering = false;

            if (pType == "0")
            {

                //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "Bill_TypeBchStatus = 'X' ", false)
                { CellBackColor = ConfigClass.SetColor_Red() };
                radGridView_Show.Columns["Bill_TypeBchStatus"].ConditionalFormattingObjectList.Add(obj1);

                ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "TypeOpenBill = 'MNIO' ", false)
                { CellBackColor = ConfigClass.SetColor_PinkPastel() };
                radGridView_Show.Columns["Bill_ID"].ConditionalFormattingObjectList.Add(obj2);

                ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition1", "TypeOpenBill = 'MNOT' ", false)
                { CellBackColor = ConfigClass.SetColor_YellowPastel() };
                radGridView_Show.Columns["Bill_ID"].ConditionalFormattingObjectList.Add(obj3);

                ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition1", "TypeOpenBill IN ('I','F','FAL','MNRS') ", false)
                { CellBackColor = ConfigClass.SetColor_GreenPastel() };
                radGridView_Show.Columns["Bill_ID"].ConditionalFormattingObjectList.Add(obj4);

                ExpressionFormattingObject obj5 = new ExpressionFormattingObject("MyCondition1", "TypeOpenBill IN ('MNRB','MNRG','MNRH') ", false)
                { CellBackColor = ConfigClass.SetColor_PurplePastel() };
                radGridView_Show.Columns["Bill_ID"].ConditionalFormattingObjectList.Add(obj5);
            }

            radGridView_Show.MasterTemplate.AllowRowResize = false;
            radGridView_Show.MasterTemplate.AllowColumnResize = false;

            //Set Color
            ExpressionFormattingObject sBill_SPC = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC);

            ExpressionFormattingObject sSTA_RECIVE = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE);

            ExpressionFormattingObject sSTA_INSTALL = new ExpressionFormattingObject("MyCondition1", "Bill_END_Count = '0' AND STA_INSTALL = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_END"].ConditionalFormattingObjectList.Add(sSTA_INSTALL);

            ExpressionFormattingObject sSTA_RETURN = new ExpressionFormattingObject("MyCondition1", "Bill_OLD_Count = '0' AND STA_RETURN = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_OLD"].ConditionalFormattingObjectList.Add(sSTA_RETURN);

            ExpressionFormattingObject sSTA_RETURNRECIVE = new ExpressionFormattingObject("MyCondition1", " Bill_OLDSPC_Count = '0' AND STA_RETURNRECIVE = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_OLDSPC"].ConditionalFormattingObjectList.Add(sSTA_RETURNRECIVE);


        }
        //Set Grid For Bill MNRZ
        public static void SetHeadGrid_ForBillMNRZRB(RadGridView radGridView_Show, string pType)//pType SUPC คือบิลที่เอา 1 รูป ส่วน SHOP คือเอา 3 รูป
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Bill_ID", "เลขที่บิล", 150)));
            // S
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
            ExpressionFormattingObject sBill_SPC = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC);

            if (pType == "SHOP")
            {
                // N
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", "รับของ", 80)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));

                ExpressionFormattingObject sSTA_RECIVE = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0' AND STA_RECIVE = '1'  ", false)
                { CellBackColor = ConfigClass.SetColor_Red() };
                radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE);
                // E
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_END", "ติดตั้ง", 80)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Count", "Bill_END_Count")));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Path", "Bill_END_Path")));

                ExpressionFormattingObject sSTA_INSTALL = new ExpressionFormattingObject("MyCondition1", "Bill_END_Count = '0' AND STA_INSTALL = '1' ", false)
                { CellBackColor = ConfigClass.SetColor_Red() };
                radGridView_Show.Columns["Bill_END"].ConditionalFormattingObjectList.Add(sSTA_INSTALL);
            }



            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_INSTALL", "STA_INSTALL")));

            radGridView_Show.TableElement.RowHeight = 100;
            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.MasterTemplate.AllowRowResize = false;
            radGridView_Show.MasterTemplate.AllowColumnResize = false;

        }
        //Set รูป ใน กริด 1-3 รูป ตามของส่งซ่อม
        public static void FindImageBillMNRZRB_ByJobID(DataTable dt, RadGridView radGridView_Show, string pType)//pType =0 คือบิลที่เอา 1 รูป ส่วน 1 คือเอา 3 รูป
        {
            radGridView_Show.DataSource = dt;
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                System.String findDpt = dt.Rows[i]["BRANCH_ID"].ToString();
                if (findDpt.StartsWith("D"))
                {
                    findDpt = "MN000";
                }
                //ที่เก็บบิล
                string bill_Path = PathImageClass.pPathBillOut + @"\" + dt.Rows[i]["Bill_Date"].ToString() + @"\" + findDpt;
                string bill_Find = "*_" + dt.Rows[i]["Bill_ID"].ToString();
                DirectoryInfo DirInfo = new DirectoryInfo(bill_Path);
                //ขาส่งของ S
                try
                {
                    FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + "_S.*", SearchOption.AllDirectories);
                    string fullNameSPC = PathImageClass.pImageEmply;
                    if (FilesSPC.Length > 0)
                    {
                        fullNameSPC = FilesSPC[0].FullName;
                    }

                    radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + "_S.*";
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                    radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                }

                if (pType == "SHOP")
                {
                    //ขารับของ N
                    try
                    {
                        FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + "_N.*", SearchOption.AllDirectories);
                        string fullNameMN = PathImageClass.pImageEmply;
                        if (FilesMN.Length > 0)
                        {
                            fullNameMN = FilesMN[0].FullName;
                        }

                        radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                        radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + "_N.*";
                        radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = FilesMN.Length;
                    }
                    catch (Exception)
                    {
                        radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                        radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = "";
                        radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = "0";
                    }
                    //ขารับของ E
                    try
                    {
                        FileInfo[] FilesEND = DirInfo.GetFiles(bill_Find + "_E.*", SearchOption.AllDirectories);
                        string fullNameEND = PathImageClass.pImageEmply;
                        if (FilesEND.Length > 0)
                        {
                            fullNameEND = FilesEND[0].FullName;
                        }


                        radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameEND);
                        radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = bill_Path + @"|" + bill_Find + "_E.*";
                        radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = FilesEND.Length;
                    }
                    catch (Exception)
                    {
                        radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                        radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = "";
                        radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = "0";
                    }
                }


            }

        }

        //Set Grid
        public static void SetHeadGrid_Image_ForMNCM(RadGridView radGridView_Show, string pType, string pBillNo, string pDate)//pType 0=เคลม 1=ทิ้ง
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_ID", "เลขที่บิล")));

            // S
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
            string typeSta = "CN รับของ";
            if (pType == "1") { typeSta = "แม่บ้านรับ"; }
            // C
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", typeSta, 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));

            if ((pType == "0") || (pType == "2") || (pType == "3"))
            {
                // R
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_END", "ของกลับมา", 80)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Count", "Bill_END_Count")));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Path", "Bill_END_Path")));
            }


            radGridView_Show.TableElement.RowHeight = 110;
            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.MasterTemplate.AllowRowResize = false;
            radGridView_Show.MasterTemplate.AllowColumnResize = false;

            //Set Color
            ExpressionFormattingObject sBill_SPC = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC);

            ExpressionFormattingObject sSTA_RECIVE = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0'  ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE);

            if ((pType == "0") || (pType == "2") || (pType == "3"))
            {
                ExpressionFormattingObject sSTA_INSTALL = new ExpressionFormattingObject("MyCondition1", "Bill_END_Count = '0'   ", false)
                { CellBackColor = ConfigClass.SetColor_Red() };
                radGridView_Show.Columns["Bill_END"].ConditionalFormattingObjectList.Add(sSTA_INSTALL);

            }

            GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(radGridView_Show.MasterView);
            rowInfo.Cells[0].Value = pBillNo;
            radGridView_Show.Rows.Add(rowInfo);

            //ที่เก็บบิล
            string bill_Path = PathImageClass.pPathAssetClaimLeave + @"\" + pDate + @"\MN000";
            string bill_Find = "*_" + pBillNo;
            DirectoryInfo DirInfo = new DirectoryInfo(bill_Path);
            //ขาส่งของ S
            try
            {
                FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + "_S.*", SearchOption.AllDirectories);
                string fullNameSPC = PathImageClass.pImageEmply;
                if (FilesSPC.Length > 0)
                {
                    fullNameSPC = FilesSPC[0].FullName;
                }

                radGridView_Show.Rows[0].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                radGridView_Show.Rows[0].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + "_S.*";
                radGridView_Show.Rows[0].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
            }
            catch (Exception)
            {
                radGridView_Show.Rows[0].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                radGridView_Show.Rows[0].Cells["Bill_SPC_Path"].Value = "";
                radGridView_Show.Rows[0].Cells["Bill_SPC_Count"].Value = "0";
            }

            string pTypeN = "_N";
            if ((pType == "0") || (pType == "2") || (pType == "3"))
            {
                pTypeN = "_C";
            }
            //ขารับของ N
            try
            {
                FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + pTypeN + @".*", SearchOption.AllDirectories);
                string fullNameMN = PathImageClass.pImageEmply;
                if (FilesMN.Length > 0)
                {
                    fullNameMN = FilesMN[0].FullName;
                }


                radGridView_Show.Rows[0].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                radGridView_Show.Rows[0].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + pTypeN + @".*";
                radGridView_Show.Rows[0].Cells["Bill_MN_Count"].Value = FilesMN.Length;
            }
            catch (Exception)
            {
                radGridView_Show.Rows[0].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                radGridView_Show.Rows[0].Cells["Bill_MN_Path"].Value = "";
                radGridView_Show.Rows[0].Cells["Bill_MN_Count"].Value = "0";
            }

            if ((pType == "0") || (pType == "2") || (pType == "3"))
            {
                //ขารับของ R
                try
                {
                    FileInfo[] FilesEND = DirInfo.GetFiles(bill_Find + "_R.*", SearchOption.AllDirectories);
                    string fullNameEND = PathImageClass.pImageEmply;
                    if (FilesEND.Length > 0)
                    {
                        fullNameEND = FilesEND[0].FullName;
                    }

                    radGridView_Show.Rows[0].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameEND);
                    radGridView_Show.Rows[0].Cells["Bill_END_Path"].Value = bill_Path + @"|" + bill_Find + "_R.*";
                    radGridView_Show.Rows[0].Cells["Bill_END_Count"].Value = FilesEND.Length;
                }
                catch (Exception)
                {
                    radGridView_Show.Rows[0].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                    radGridView_Show.Rows[0].Cells["Bill_END_Path"].Value = "";
                    radGridView_Show.Rows[0].Cells["Bill_END_Count"].Value = "0";
                }
            }
        }

        //ประวัติการส่งเคลม
        public static DataTable GetData_HistoryClaimByAssID(string pAssID)
        {
            string sql = $@"
            SELECT 	ASSETID,CONVERT(VARCHAR,DATE_INS,23)AS DATE_INS
		            ,SUBSTRING( DESCRIPTION,1,CHARINDEX('->' ,DESCRIPTION,1)-1) AS DESCRIPTION     
		            ,ISNULL(RECIVE_STA,0) AS STA_RECIVE
		            ,CONVERT(VARCHAR,RECIVE_DATE,23) AS DATE_RECIVE,ISNULL(SQ_COST,0.00) AS SQ_COST
		            ,SQ_DESCRIPTION, 
		            CASE WHEN ISNULL(RECIVE_STA,'0') = '1' THEN DATEDIFF(DAY, CONVERT(VARCHAR,RECIVE_DATE,23), GETDATE())  ELSE '-1' END AS DATELASTSEND
             FROM   SHOP_JOBCLAIM with (nolock)  
             WHERE	ASSETID = '{pAssID}' AND STADOC ='1' AND STA_TYPE = '0'
             order by DATE_INS Desc ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //Check JOB TYPE
        public static DataTable GetJOBGROUP_ByJOBID(string pJobID)
        {
            return ConnectionClass.SelectSQL_Main($@"
            SELECT	JOBGROUP_ID,JOBGROUP_DESCRIPTION
            FROM	SHOP_JOBCenter WITH (NOLOCK) 
                    INNER JOIN SHOP_JOBGROUP WITH (NOLOCK) ON SHOP_JOBCenter.JOB_TYPE = SHOP_JOBGROUP.JOBGROUP_ID
            WHERE	JOB_Number = '{pJobID}' ");
        }

        //Get Camera For Open
        public static DataTable GetCam_ByID(string pCamID)
        {
            //CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN [DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
            //LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM
            string sqlCam = $@"
            SELECT  CAM_IP,CAM_ID,[SHOP_JOBCAM].BRANCH_ID,
                    CAM_DESC,SHOP_JOBCAM.CAM_DVR ,ISNULL(JOB_Number,'') AS JOB_Number ,Cam_CH,
                    ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD  
            FROM    [dbo].[SHOP_JOBCAM] WITH (NOLOCK)
                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON [SHOP_JOBCAM].BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                    LEFT OUTER JOIN 
                        (SELECT	JOB_Number,JOB_SN	
                        FROM	SHOP_JOBComMinimart WITH (NOLOCK) 
                        WHERE   JOB_STACLOSE = '0'  AND JOB_GROUPSUB = '00003')JOB  ON [SHOP_JOBCAM].CAM_IP = JOB.JOB_SN
                    LEFT OUTER JOIN 
                        (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '0' )CAM_PASS  ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
            WHERE	CAM_IP ='{pCamID}' OR CONVERT(VARCHAR,CAM_ID,100) = '{pCamID}' ";
            return ConnectionClass.SelectSQL_Main(sqlCam);
        }

        //GetJob ByCarID
        public static int GetJob_ByCar(string pCarID)
        {
            string sql = $@"
                SELECT	*	
                FROM	SHOP_JOBCarMachine WITH (NOLOCK)
                WHERE   JOB_CARID = '{pCarID}' AND JOB_STACLOSE = '0'";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }

        //Get Factory Job
        public static int GetJob_ByFactory(string pMarchineID)
        {
            string sql = $@"
                SELECT	*	
                FROM	SHOP_JOBFactory WITH (NOLOCK)
                WHERE   JOB_MACHINEID = '{pMarchineID}' AND JOB_STACLOSE = '0'";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }
    }

}
