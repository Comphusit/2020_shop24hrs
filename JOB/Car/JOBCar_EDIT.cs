﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.FormShare.ShowData;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowImage;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.GeneralForm.BillOut;

namespace PC_Shop24Hrs.JOB.Car
{
    public partial class JOBCar_EDIT : Telerik.WinControls.UI.RadForm
    {
        //public string PJOBNUMBER;
        //public string STAOUT;
        //public string PTYPE;

        //int GROUPSUB;
        //string pConfigDB;
        string pBchID;
        string pDate;
        string pChange;         //0 ไม่มีการเปลี่ยนเเปลงฟอร์ม 1 มีการเปลี่ยนแปลงเพื่อ reshesh ข้อมูลในตาราง main

        readonly string _tblName;
        readonly string _grpId;
        readonly string _grpName;
        readonly string _pJobNumber;
        readonly string _pPermission;
        readonly string _pType;

        string pTypeGps;

        #region "Rows"      
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion
        //Set Permission
        void SetPermission()
        {
            radCheckBox_STACLOSE.Enabled = false;
            Button_AX.Enabled = false;
            Button_MNIO.Enabled = false;
            Button_MNOT.Enabled = false;
            Bt_LockCar.Enabled = false;
            radTextBox_EmplID.Enabled = false;
        }
        //Load
        public JOBCar_EDIT(string pTbl, string pGrpID, string pGrpName, string pJobNumber, string pPermission, string pType)
        {
            InitializeComponent();
            _tblName = pTbl;
            _grpId = pGrpID;
            _grpName = pGrpName;
            _pPermission = pPermission;
            _pType = pType;
            _pJobNumber = pJobNumber;

            pChange = "0";
            pTypeGps = "";
        }
        //Load
        private void JOBCar_EDIT_Load(object sender, EventArgs e)
        {
            //pConfigDB = ConfigClass.GetConfigDB_FORMNAME(this.Name);

            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Bill.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล : " + _pJobNumber;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Reapair);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Garage);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_STA);

            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_OpenAdd.ButtonElement.ShowBorder = true; RadButton_OpenAdd.ButtonElement.ToolTipText = "แนบรูป สำหรับเปิดงานซ่อม";
            radButton_OpenView.ButtonElement.ShowBorder = true; radButton_OpenView.ButtonElement.ToolTipText = "ดูรูป สำหรับเปิดงานซ่อม";
            radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            JOB_Class.SetHeadGrid_ForShowImage(radGridView_Show, "0");
            JOB_Class.SetHeadGrid_ForBillMNRZRB(radGridView_Send, _pType);

            SetData();

            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
            JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);

            radGridView_Show.ClearSelection();
            radGridView_Send.ClearSelection();

            if (_pType == "SHOP")
            {
                radButton_Add.Text = "สาขาส่ง";
            }
            else
            {
                radButton_Add.Text = "ส่งซ่อม";
            }
            this.Text = _grpName + " : " + _pType;
        }
        //Group
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //Type Repair
        void Set_GroupRepair()
        {
            radDropDownList_Reapair.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_Reapair.DataSource = JOB_Class.GetJOB_GroupSub("00008");
            radDropDownList_Reapair.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_Reapair.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //Set_Garage
        void Set_Garage()
        {
            radDropDownList_Garage.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_Garage.DataSource = Models.AssetClass.FindDataVehicleRepairShop();
            radDropDownList_Garage.ValueMember = "CODE";
            radDropDownList_Garage.DisplayMember = "TXT";
        }
        //Head
        void Set_JOBSTA()
        {
            radDropDownList_STA.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_STA.DataSource = JOB_Class.GetJOBStatus();
            radDropDownList_STA.ValueMember = "STA_ID";
            radDropDownList_STA.DisplayMember = "STA_NAME";
        }
        //Set Data
        void SetData()
        {
            string sql = $@" SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOB_GROUPSUB,JOBGROUPSUB_DESCRIPTION,
                        JOB_SN,JOB_DESCRIPTION,JOB_STACLOSE,JOB_SHORTNAMEINS,JOB_DESCRIPTION_CLOSE,JOB_CARID,    
                        JOB_TYPEREPAIRID,JOB_TYPEREPAIRNAME,JOB_TYPEGARAGEID,JOB_TYPEGARAGENAME,JOB_STA,
                        JOB_STAIMG_OPEN,CONVERT(varchar,JOB_DATEINS,23) AS DATE_INS,JOB_LOCKCAR,JOB_TECHID,JOB_TECHNAME       
                        FROM	{_tblName} WITH (NOLOCK)   WHERE JOB_Number = '{_pJobNumber}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            pBchID = dt.Rows[0]["BRANCH_ID"].ToString();
            radTextBox_BranchID.Text = dt.Rows[0]["BRANCH_ID"].ToString().Replace("MN", "").Replace("D", "");
            radLabel_BranchName.Text = dt.Rows[0]["BRANCH_NAME"].ToString();

            Set_GroupSub(); radDropDownList_GroupSUB.SelectedValue = dt.Rows[0]["JOB_GROUPSUB"].ToString();
            Set_GroupRepair(); radDropDownList_Reapair.SelectedValue = dt.Rows[0]["JOB_TYPEREPAIRID"].ToString();
            Set_Garage(); radDropDownList_Garage.SelectedValue = dt.Rows[0]["JOB_TYPEGARAGEID"].ToString().Replace(" ", "");
            Set_JOBSTA(); radDropDownList_STA.SelectedValue = dt.Rows[0]["JOB_STA"].ToString();

            pTypeGps = dt.Rows[0]["JOB_TYPEREPAIRID"].ToString();

            radTextBox_SN.Text = dt.Rows[0]["JOB_SN"].ToString();
            radTextBox_Desc.Text = dt.Rows[0]["JOB_DESCRIPTION"].ToString();
            radTextBox_Car.Text = dt.Rows[0]["JOB_CARID"].ToString();

            pDate = dt.Rows[0]["DATE_INS"].ToString();

            radTextBox_EmplID.Text = dt.Rows[0]["JOB_TECHID"].ToString();
            radLabel_EmplName.Text = dt.Rows[0]["JOB_TECHNAME"].ToString();
            if (dt.Rows[0]["JOB_TECHID"].ToString() != "") radTextBox_EmplID.Enabled = false;

            radButton_OpenView.Enabled = false;
            if (dt.Rows[0]["JOB_STAIMG_OPEN"].ToString() == "1")
            {
                radButton_OpenView.Enabled = true;
            }

            radLabel_tel.Text = BranchClass.GetTel_ByBranchID(pBchID);

            if (dt.Rows[0]["JOB_STACLOSE"].ToString() == "1")
            {
                radCheckBox_STACLOSE.Checked = true;
                radTextBox_Update.Text = dt.Rows[0]["JOB_DESCRIPTION_CLOSE"].ToString();
                radTextBox_Update.ReadOnly = true;
                radButton_Save.Enabled = false;

                radDropDownList_GroupSUB.Enabled = false;
                radDropDownList_STA.Enabled = false;
                radDropDownList_Garage.Enabled = false;
                radDropDownList_Reapair.Enabled = false;

                radTextBox_SN.Enabled = false;
                radCheckBox_STACLOSE.Enabled = false;

                Button_MNIO.Enabled = false;
                Button_AX.Enabled = false;

                RadButton_Car.Enabled = false;

                radButton_OpenView.Enabled = false;
                Bt_LockCar.Enabled = false;

                radTextBox_EmplID.Enabled = false;

                radButton_Cancel.Focus();
                radTextBox_Update.SelectionStart = radTextBox_Update.Text.Length;
            }
            else
            {
                if (pTypeGps == "00341")
                {
                    radDropDownList_Reapair.Enabled = false;
                }

                radCheckBox_STACLOSE.Checked = false;
                radTextBox_Update.Enabled = true;
                radButton_Save.Enabled = true;

                if ((dt.Rows[0]["JOB_LOCKCAR"].ToString() == "0") && (radTextBox_Car.Text != ""))
                {
                    Bt_LockCar.Enabled = true;
                }
                else
                {
                    Bt_LockCar.Enabled = false;
                }
                radTextBox_Update.Focus();
            }

            if (_pPermission != "1")
            {
                SetPermission();
            }

            if (_pType == "SUPC")
            {
                if (_pPermission == "1")
                {
                    radButton_Add.Enabled = false;
                }
                else
                {
                    radButton_Add.Enabled = true;
                }
            }

            pChange = "0";
        }
        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (pChange == "1")
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.No;
            }
            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string pTypeRepairID;
            try
            {
                pTypeRepairID = radDropDownList_Reapair.SelectedValue.ToString();
            }
            catch (Exception)
            {
                pTypeRepairID = "";

            }
            //ในกรณีที่ไม่มีการ Update
            if (radTextBox_Update.Text == "")
            {
                string strUpdGroupSUB;
                strUpdGroupSUB = $@"UPDATE {_tblName}
                    SET [JOB_GROUPSUB] = '{radDropDownList_GroupSUB.SelectedValue}',
                    [JOB_DATEUPD] = GETDATE(),
                    JOB_SN = '{radTextBox_SN.Text}',
                    JOB_TYPEREPAIRID = '{pTypeRepairID}',
                    JOB_TYPEGARAGEID = '{ radDropDownList_Garage.SelectedValue}',
                    JOB_STA = '{radDropDownList_STA.SelectedValue}',
                    [JOB_TECHID] = '{radTextBox_EmplID.Text}',
                    [JOB_TECHNAME] = '{radLabel_EmplName.Text}'
                    WHERE [JOB_Number] = '{_pJobNumber}' ";
                string TGroupSUB = ConnectionClass.ExecuteSQL_Main(strUpdGroupSUB);
                MsgBoxClass.MsgBoxShow_SaveStatus(TGroupSUB);
                if (TGroupSUB == "")
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
                else
                {
                    return;
                }
            }
            //ในกรณีที่มีการ Update

            string strUser;
            string dessc = SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            strUser = "-> BY " + dessc;

            string desc = radTextBox_Desc.Text;
            string descClose = "-" + radTextBox_Update.Text + Environment.NewLine + strUser;
            string strUpd;
            string Garage;
            try
            { Garage = radDropDownList_Garage.SelectedValue.ToString(); }
            catch (Exception)
            { Garage = ""; }
            if (radCheckBox_STACLOSE.Checked == true)//ปิดจีอบ
            {
                //check ช่างผู้รับผิดชอบ
                if ((radTextBox_EmplID.Text == "") || (radLabel_EmplName.Text == ""))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุ ช่างผู้รับผิดชอบซ่อม" + Environment.NewLine + "ก่อนการปิดจ๊อบทุกกรณี");
                    radTextBox_EmplID.Focus();
                    return;
                }

                //Send AX
                String t_AX = "";
                if (radTextBox_Car.Text != "")
                {
                    string findRecord = FindRecordInAX(radTextBox_Car.Text);
                    if (findRecord != "")
                    {
                        String pDesc = _pJobNumber + Environment.NewLine + " วันที่ส่งซ่อม " + pDate + Environment.NewLine + " วันที่ซ่อมเสร็จ " + DateTime.Now.AddDays(0).ToString("yyyy-MM-dd");
                        //t_AX = JOBClass.SendAX_CloseJOBCarExternalList(findRecord, radTextBox_Car.Text, Garage, pDesc);
                        t_AX = AX_SendData.Save_EXTERNALLIST_CloseJOBCarExternalList(findRecord, radTextBox_Car.Text, Garage, pDesc);
                    }
                    else
                    {
                        t_AX = "";
                    }
                }
                if (t_AX != "")
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(t_AX);
                    return;
                }

                strUpd = string.Format(@"UPDATE " + _tblName + " SET  [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'," +
                    "[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "', " +
                    "[JOB_DESCRIPTION_CLOSE] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'," +
                    "[JOB_TYPEREPAIRID] = '" + pTypeRepairID + "', " +
                    "[JOB_TYPEGARAGEID] = '" + Garage + "'," +
                    "[JOB_STA] = '" + radDropDownList_STA.SelectedValue + "',   " +
                    "[JOB_STACLOSE] = '1'," +
                    "[JOB_WHOCLOSE] = '" + SystemClass.SystemUserID + "',[JOB_NAMECLOSE] = '" + SystemClass.SystemUserName + "',[JOB_DATECLOSE] = GETDATE()," +
                    "[JOB_DATEUPD] = GETDATE()  " +
                    ",[JOB_TECHID] = '" + radTextBox_EmplID.Text + "' " +
                    ",[JOB_TECHNAME] = '" + radLabel_EmplName.Text + "' " +
                    " WHERE   [JOB_Number] = '" + _pJobNumber + "' ");
            }
            else
            {
                desc = desc + Environment.NewLine + Environment.NewLine + descClose;
                strUpd = string.Format(@"UPDATE " + _tblName + "  SET	 [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'" +
                    ",[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "' " +
                    ",[JOB_DESCRIPTION] = '" + desc + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_TYPEREPAIRID] = '" + pTypeRepairID + "' " +
                    ",[JOB_TYPEGARAGEID] = '" + Garage + "' " +
                    ",[JOB_STA] = '" + radDropDownList_STA.SelectedValue + "'   " +
                    ",[JOB_DESCRIPTION_UPD] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_DATEUPD] = GETDATE() " +
                    ",[JOB_TECHID] = '" + radTextBox_EmplID.Text + "' " +
                    ",[JOB_TECHNAME] = '" + radLabel_EmplName.Text + "' " +
                    "WHERE   [JOB_Number] = '" + _pJobNumber + "' ");
            }

            string T = ConnectionClass.ExecuteSQL_Main(strUpd);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        //Find Recod AX
        string FindRecordInAX(string pCarID)
        {
            //string sql = $@"select	RECID
            //                from	SPC_VEHICLETRANS WITH (NOLOCK) 
            //                WHERE	VEHICLEID = '{pCarID}' 
            //                        AND     TRANSTYPE = '1' 
            //                        AND COMPLETEDATETIME = '1900-01-01' ";
            DataTable dt = Models.AssetClass.FindRecodJOBCar(pCarID);
                //ConnectionClass.SelectSQL_MainAX(sql);
            if (dt.Rows.Count == 0)
            {
                return "";
            }
            else
            {
                return dt.Rows[0]["RECID"].ToString();
            }
        }
        //Close
        private void RadCheckBox_STACLOSE_CheckedChanged(object sender, EventArgs e)
        {
            int cSpcType = 0;
            for (int iT = 0; iT < radGridView_Show.Rows.Count; iT++)
            {
                if (radGridView_Show.Rows[iT].Cells["Bill_SpcType"].Value.ToString() == "X")
                {
                    cSpcType += 1;
                }
            }

            if (cSpcType == 0)
            {
                radTextBox_Update.Focus();
                return;
            }

            if (radCheckBox_STACLOSE.Checked == true)
            {
                if (radDropDownList_STA.SelectedValue.ToString() == "04")
                {
                    CheckBill();
                    // radTextBox_Update.Focus();
                }
                else
                {
                    MessageBox.Show("สถานะจ๊อบไม่อยู่ในสถานะซ่อมเสร็จ ไม่สามารถปิดได้.",
                        SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radCheckBox_STACLOSE.Checked = false;
                    radDropDownList_STA.Focus();
                }

            }
        }
        //Comfirm
        void CheckBill()
        {
            if (_pType == "SHOP")
            {
                int a = 0;
                for (int i = 0; i < radGridView_Show.Rows.Count; i++)
                {
                    if (radGridView_Show.Rows[i].Cells["Bill_TypeBchStatus"].Value.ToString() == "X")
                    {
                        a += 1;
                    }
                }
                if (a == 0)
                {
                    radTextBox_Update.Focus();
                    return;
                }
                else
                {
                    if (SystemClass.SystemComProgrammer == "1")
                    {
                        if (ComfirmByProgrammer() == "1")
                        {
                            radTextBox_Update.Focus();
                        }
                        else
                        {
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากสาขายังรับบิลไม่ครบทุกใบ" + Environment.NewLine + "จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                        radTextBox_Update.Focus();
                    }
                }
            }
            else // SUPC
            {
                int aa = 0;
                for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                {
                    if (radGridView_Show.Rows[ii].Cells["Bill_SPC_Count"].Value.ToString() == "0")
                    {
                        aa += 1;
                    }
                }
                if (aa == 0)
                {
                    radTextBox_Update.Focus();
                    return;
                }
                else
                {
                    if (SystemClass.SystemComProgrammer == "1")
                    {
                        if (ComfirmByProgrammer() == "1")
                        {
                            radTextBox_Update.Focus();
                        }
                        else
                        {
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากไม่มีรูปการถ่ายออก " + Environment.NewLine + " จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                        radTextBox_Update.Focus();
                    }
                }
            }
        }
        string ComfirmByProgrammer()
        {
            if ((MessageBox.Show("ยันยันการปิดจ๊อบทั้งที่บิลยังไม่เรียบร้อย ?.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
            {
                return "0";
            }
            else
            {
                return "1";
            }
        }
        //click
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Show.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                        //JOBClass.FindImageBillAll_ByJobID(BillAllClass.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD": //รูปสาขาส่งซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLD_Count"].Value.ToString(),
                           radGridView_Show.CurrentRow.Cells["Bill_OLD_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC": //รูปสาขาใหญ่รับซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                          radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //MNIO
        private void Button_MNIO_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText

            };
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }

        //Open Image Add
        private void RadButton_OpenAdd_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBCar + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            string pFileName = _pJobNumber + "-" + pBchID + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, _tblName, "JOB_STAIMG_OPEN", "JOB_Number", _pJobNumber, "1");
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.Yes)
            {
                radButton_OpenView.Enabled = true;
            }
            else if (frm.DialogResult == DialogResult.OK)
            {
                UploadImageSelectArea _UploadImageSelectArea = new UploadImageSelectArea("0", pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber);
                _UploadImageSelectArea.Show();
                radButton_OpenView.Enabled = true;
            }
        }
        //Open Image View
        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOBCar + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            { pathImg = pPath + @"|*.jpg" };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }
        //Save CarID
        private void RadButton_Car_Click(object sender, EventArgs e)
        {
            if (radTextBox_Car.Text != "")
            {
                MessageBox.Show("เนื่องจาก JOB นี้มีการระบุทะเบียนรถไปเรียบร้อยแล้ว " + Environment.NewLine + "ไม่สามารถแก้ไขได้ ถ้าระบุทะเบียนรถผิด ให้ปิด JOB แล้วเปิดใหม่เท่านั้น."
                    , SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            DataTable dt = Models.AssetClass.FindDataVEHICLE("","AND VEHICLEGROUP !='MOTORCYCLE' ");
            ShowDataDGV frm = new ShowDataDGV()
            { dtData = dt };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            { 
                if (JOB_Class.GetJob_ByCar(frm.pID) > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ทะเบียนรถ {frm.pID} {Environment.NewLine} มีข้อมูลการซ่อมอยู่แล้ว {Environment.NewLine} ไม่สามารถเปิดจ๊อบซ้ำได้อีก");
                    return;
                }
                else
                {
                    if (_pType == "SUPC") if (_pPermission == "1") Bt_LockCar.Enabled = true;

                    string strUpCar = string.Format(@"Update " + _tblName + " " +
                        "SET JOB_CARID = '" + frm.pID + "' " +
                        "WHERE JOB_Number = '" + _pJobNumber + "' ");
                    string T = ConnectionClass.ExecuteSQL_Main(strUpCar);
                    if (T == "")
                    {
                        pChange = "1";
                        radTextBox_Car.Text = frm.pID;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        radTextBox_Car.Text = "";
                    }
                }
            }
            else
            {
                MessageBox.Show("ไม่มีการเลือกข้อมูลของรถ ไม่สามารถบันทึกข้อมูล ลองใหม่อีกครั้ง.",
                         SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radTextBox_Car.Focus();
            }
        }
        //ROWS
        private void RadGridView_Send_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //MNOT
        private void Button_MNOT_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNOT", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };

            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //AX
        private void Button_AX_Click(object sender, EventArgs e)
        {
            Bill.BillAXAddJOB frm = new Bill.BillAXAddJOB(pBchID, radLabel_BranchName.Text, "1",
                _pJobNumber, _grpId, _grpName, radTextBox_SN.Text, "", "", "");
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }

        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            switch (_pType)
            {
                case "SUPC":
                    string pDesc = radTextBox_Desc.Text.Substring(1, radTextBox_Desc.Text.IndexOf("->") - 1).Replace(Environment.NewLine, "");
                    JOB.Bill.Bill_MNRZ frmMNRZ = new Bill.Bill_MNRZ(pBchID, radLabel_BranchName.Text, _pJobNumber, radLabel_tel.Text, pDesc);
                    frmMNRZ.ShowDialog(this);
                    if (frmMNRZ.pDialog == "1")
                    {
                        JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);
                    }
                    break;
                case "SHOP":
                    DataTable dtJOB = BillOut_Class.FindBill_ForAddJOB("'D041'", pBchID);
                    if (dtJOB.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บิลทั้งหมด มี JOB เรียบร้อยแล้ว");
                        return;
                    }

                    JOB.Bill.BillMN_ForAddJOB frmAddJOb = new Bill.BillMN_ForAddJOB("'D041'", pBchID, _pJobNumber, _grpId);
                    if (frmAddJOb.ShowDialog(this) == DialogResult.Yes)
                    {
                        JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);
                    }

                    break;
                default:
                    break;
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
        //Select JOB
        private void RadDropDownList_Reapair_SelectedValueChanged(object sender, EventArgs e)
        {
            if (pTypeGps == "")
            {
                return;
            }
            if (pTypeGps != "00341")
            {
                if (radDropDownList_Reapair.SelectedValue.ToString() == "00341")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("สำหรับประเภทการซ่อม ระบบ GPS มีปัญหา" + Environment.NewLine + "สามารถเลือกได้ตอนเปิดจ๊อบเท่านั้น" + Environment.NewLine +
                        "ถ้าเปิดจ๊อบผิดให้แจ้งปิดจ๊อบก่อนเปิดใหม่อีกครั้ง");
                    radDropDownList_Reapair.SelectedValue = pTypeGps;
                    return;
                }
            }
        }
        //Lock Car AX
        private void Bt_LockCar_Click(object sender, EventArgs e)
        {
            if (radTextBox_Car.Text == "") return;
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการล็อกรถในระบบ AX ?") == DialogResult.No) return;

            DataTable dtSNCar = Models.AssetClass.FindDataVEHICLE(radTextBox_Car.Text, "");

            string carSN = "";
            if (dtSNCar.Rows.Count > 0) { carSN = dtSNCar.Rows[0]["SERIALNUMBER"].ToString(); }

            String t_AX = "";
            if (radDropDownList_Reapair.SelectedValue.ToString() != "00341")
                t_AX = AX_SendData.Save_EXTERNALLIST_OpenJOBCarExternalListLock(radTextBox_Car.Text, radDropDownList_Reapair.SelectedItem[0].ToString(), radTextBox_SN.Text, carSN);
            //{
            //    t_AX = JOBClass.SendAX_OpenJOBCarExternalListLock(radTextBox_Car.Text, radDropDownList_Reapair.SelectedItem[0].ToString(), radTextBox_SN.Text, carSN);
            //}

            if (t_AX != "")
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(t_AX); return;
            }
            else
            {
                string strUser = "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
                string desc = radTextBox_Desc.Text + Environment.NewLine + Environment.NewLine + "- รับกุญแจแล้ว" + Environment.NewLine + strUser;

                string strUpd = string.Format(@"UPDATE " + _tblName + "  SET " +
                    "JOB_LOCKCAR = '1' " +
                    ",[JOB_DESCRIPTION] = JOB_DESCRIPTION + CHAR(10) + CHAR(10) + '" + desc + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_DESCRIPTION_UPD] = '" + desc + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_DATEUPD] = GETDATE() " +
                    "WHERE   [JOB_Number] = '" + _pJobNumber + "' ");
                string T = ConnectionClass.ExecuteSQL_Main(strUpd);
                if (T == "")
                {
                    radTextBox_Desc.Text = desc;
                    Bt_LockCar.Enabled = false;
                }
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //ช่างซ่อม        
        private void RadTextBox_EmplID_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_EmplID.Text.ToString().Length == 7)
            {
                DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_EmplID.Text);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน");
                    radTextBox_EmplID.Text = "";
                    radLabel_EmplName.Text = "";
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                if (dtEmp.Rows[0]["NUM"].ToString() != "D084")
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("พนักงานที่ระบุ " + Environment.NewLine + "ไม่ได้เป็นพนักงานแผนกศูนย์ซ่อมรถยนต์" + Environment.NewLine +
                        "ยืนยันการบันทึกให้เป็นช่างผู้ซ่อมรถ ?") == DialogResult.No)
                    {
                        radTextBox_EmplID.Text = "";
                        radLabel_EmplName.Text = "";
                        radTextBox_EmplID.SelectAll();
                        radTextBox_EmplID.Focus();
                        return;
                    }
                }

                radLabel_EmplName.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                radTextBox_EmplID.Enabled = false;

            }
            else
            {
                radLabel_EmplName.Text = "";
            }

        }
        //CheckNumber
        private void RadTextBox_EmplID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
