﻿//CheckOK
using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.Car
{
    public partial class JOBCar_Main : RadForm
    {
        readonly string _pType;
        readonly string _pGROUPID;
        readonly string _pGROUP_DESC;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปิดจ๊อบ 1 มี
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        readonly string tbl;
        readonly string billFirst;

        private DateTime dateAutoRefresh;

        //Main Load
        public JOBCar_Main(string pType, string pGROUPID, string pGROUP_DESC, string pPerminssion, string pFixBch) // SHOP-SUPC
        {
            InitializeComponent();

            _pType = pType;
            _pGROUPID = pGROUPID;
            _pGROUP_DESC = pGROUP_DESC;
            _pPerminssion = pPerminssion;
            _pFixBch = pFixBch;


            DataTable dtGroup = JOB_Class.GetJOB_Group(_pGROUPID);
            tbl = dtGroup.Rows[0]["TABLENAME"].ToString();
            billFirst = dtGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString();

        }
        private void JOBCar_Main_Load(object sender, EventArgs e)
        {

            dateAutoRefresh = DateTime.Now.AddMinutes(3); Timer_Auto.Start();

            this.Text = _pGROUP_DESC + " : " + _pType;

            radButtonElement_Excel.ShowBorder = true;
            radButtonElement_Excel.ToolTipText = "ExportExcel";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่ม";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข";
            radRadioButtonElement_Open.ShowBorder = true;
            radRadioButtonElement_ALL.ShowBorder = true;
            radStatusStrip_Main.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            //this.Text = "JOB SHOP";
            string pNameID = "สาขา";
            string pName = "ชื่อสาขา";
            if (_pType == "SUPC")
            {
                pNameID = "แผนก";
                pName = "ชื่อแผนก";
                //this.Text = "JOB SUPC";
            }
            
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATE", "S", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATEUPD", "Up", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_Number", "เลขที่ JOB", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ประเภทรถ", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_TYPEREPAIRNAME", "ประเถทซ่อม", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_CARID", "รถ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SN", "ปัญหา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_TYPEGARAGENAME", "อู่ซ่อม", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchID(pNameID));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchName(pName));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 500));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SHORTNAMEINS", "ผู้เปิด", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_TECHID", "ช่างซ่อม", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_TECHNAME", "ชื่อช่างผู้ซ่อม", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_STACLOSE", "สถานะจ๊อบ"));
            //Freeze Column
            for (int i = 0; i < 4; i++)
            {

                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;
            }

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_GreenPastel(), radGridView_Show);
            //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "JOB_STACLOSE = 1 ", false)
            //{ CellBackColor = ConfigClass.SetColor_GreenPastel() };
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj2);

            radGridView_Show.TableElement.SearchHighlightColor = Color.LightBlue;

            SetGDV();
        }
        //find Data
        void SetGDV()
        {
            string sta_JobAll = "   '0' ";
            if (radRadioButtonElement_Open.CheckState == CheckState.Unchecked) { sta_JobAll = "   '0','1' "; }
            string sql = $@"SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOBGROUPSUB_DESCRIPTION,
                            JOB_TYPEREPAIRID, JOB_TYPEREPAIRNAME, JOB_CARID, JOB_STA, JOB_TYPEGARAGEID, JOB_TYPEGARAGENAME,
                            JOB_SN, JOB_DESCRIPTION, JOB_DESCRIPTION_UPD,
                            CASE ISNULL(JOB_BRANCHOUTPHUKET, 'X') WHEN '0' THEN CONVERT(VARCHAR, 'X') ELSE CONVERT(VARCHAR, '/') END  AS JOB_BRANCHOUTPHUKET, JOB_STACLOSE,
                            JOB_SHORTNAMEINS,
                            DateDiff(Day, JOB_DATEINS, GETDATE()) AS SUMDATE,
                            CASE [JOB_STA] WHEN '01' THEN 'เปิดจ๊อบ' WHEN '02' THEN 'รอซ่อม' WHEN '03' THEN 'กำลังซ่อม' WHEN '04' THEN 'ซ่อมเสร็จ'  END AS STA  ,
                            CASE CONVERT(VARCHAR,JOB_DATEUPD,23) WHEN '1900-01-01' THEN  DateDiff(Day,JOB_DATEINS,GETDATE())
						    ELSE ISNULL(DATEDIFF(DAY,JOB_DATEUPD,GETDATE()),0) END AS SUMDATEUPD,JOB_TECHID,JOB_TECHNAME  
                        FROM    {tbl} WITH(NOLOCK)
                        WHERE JOB_STACLOSE IN({ sta_JobAll})
                            AND JOB_TYPE = '{_pType}'";

            if (_pFixBch != "")
            {
                sql += " AND JOB_BRANCHID = '" + _pFixBch + "' ";
            }
            sql += "ORDER BY CONVERT(VARCHAR,JOB_DATEINS,25) DESC ";


            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            radGridView_Show.DataSource = dt;
        }

        #region "ROWS NUMBERS"      
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);

        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        //Open
        private void RadRadioButtonElement_Open_CheckStateChanged(object sender, EventArgs e)
        {
            SetGDV();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            OpenFormEdit();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            if (_pType == "SHOP")
            {
                JOBCar_SHOP_ADD frmJOB_ADD = new JOBCar_SHOP_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC,_pFixBch);
                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
                {
                    SetGDV();
                }
            }
            else
            {
                JOBCar_SUPC_ADD frmJOB_ADD = new JOBCar_SUPC_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC, _pPerminssion);
                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
                {
                    SetGDV();
                }
            }
        }
        //All
        private void RadRadioButtonElement_ALL_CheckStateChanged(object sender, EventArgs e)
        {
            SetGDV();
        }
        //Open for Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            OpenFormEdit();
        }
        //Open Form
        void OpenFormEdit()
        {
            if ((radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString() == "") ||
                (radGridView_Show.CurrentRow.Index == -1) ||
                (radGridView_Show.Rows.Count == 0))
            {
                return;
            }
            string jobID = radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString();

            JOBCar_EDIT frmJOB_Edit = new JOBCar_EDIT(tbl, _pGROUPID, _pGROUP_DESC, jobID, _pPerminssion, _pType);
            if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK)
            {
                SetGDV();
            }
        }
        //Auto Refresh
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now >= dateAutoRefresh)
            {
                Timer_Auto.Stop();
                SetGDV();
                dateAutoRefresh = DateTime.Now.AddMinutes(3);
                Timer_Auto.Start();
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {

            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("งานซ่อมรถ", radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}



