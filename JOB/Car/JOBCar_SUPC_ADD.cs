﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.JOB.Car
{
    public partial class JOBCar_SUPC_ADD : Telerik.WinControls.UI.RadForm
    {
        string bchID;
        string bchName;

        readonly string _tblName;
        readonly string _billFirst;
        readonly string _grpId;
        readonly string _grpDesc;
        readonly string _pPermission;
        //Supc
        public JOBCar_SUPC_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc, string pPermission)
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _pPermission = pPermission;
        }
        //Type
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //Type Repair
        void Set_GroupRepair()
        {
            radDropDownList_Reapair.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_Reapair.DataSource = JOB_Class.GetJOB_GroupSub("00008");
            radDropDownList_Reapair.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_Reapair.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //set ค่าของ radCombo select branch before Use
        void Set_CbDpt()
        {
            //Set RadDropDownList All
            this.radDropDownList_Dpt.DropDownListElement.ListElement.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.AutoSizeItems = true;
            this.radDropDownList_Dpt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 30;

            DataTable dtDpt = Models.DptClass.GetDpt_AllD();

            radDropDownList_Dpt.DataSource = dtDpt;
            radDropDownList_Dpt.ValueMember = "NUM";
            radDropDownList_Dpt.DisplayMember = "DESCRIPTION_SHOW";
        }
        void Set_ClearData()
        {
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            Set_GroupSub();
            Set_CbDpt();
            Set_GroupRepair();
            radTextBox_SN.Text = "";
            radTextBox_Desc.Text = "";
            bchID = "";
        }
        private void JOBCar_SUPC_ADD_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Reapair);

            radButton_openCar.ButtonElement.ShowBorder = true; radButton_openCar.ButtonElement.ToolTipText = "เพิ่มทะเบียนรถ";
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            Set_ClearData();

            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SUPC";

            bchID = radDropDownList_Dpt.SelectedValue.ToString();
            bchName = Models.DptClass.GetDptName_ByDptID(bchID);

            if (_pPermission == "0")
            {
                radDropDownList_Dpt.SelectedValue = SystemClass.SystemDptID;
            }
        }

        private void RadDropDownList_Dpt_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                bchID = radDropDownList_Dpt.SelectedValue.ToString();
                bchName = Models.DptClass.GetDptName_ByDptID(bchID);
                radDropDownList_GroupSUB.Focus();
            }
            catch (Exception)
            {
                MessageBox.Show("เช็คข้อมูลแผนกให้ถูกต้องอีกครั้ง.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radDropDownList_Dpt.Focus();
                return;
            }
        }
        //Focus
        private void RadTextBox_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radButton_openCar.Focus();
            }
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //Focus
        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {
            radDropDownList_Reapair.Focus();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //Check CarID
            if (radTextBox_CarID.Text == "")
            {
                if (MessageBox.Show("ยืนยันการไม่ใส่ทะเบียนรถในการเปิดจ๊อบซ่อม?.",
                    SystemClass.SystemBranchID, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ทะเบียนรถที่ส่งซ่อม");
                    radButton_openCar.Focus();
                    return;
                }
            }
            //Check Dpt
            try
            {
                bchID = radDropDownList_Dpt.SelectedValue.ToString();
                bchName = Models.DptClass.GetDptName_ByDptID(bchID);
            }
            catch (Exception)
            {
                MessageBox.Show("เช็คข้อมูลแผนกให้เรียบร้อยก่อนการบันทึก.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radDropDownList_Dpt.Focus();
                return;
            }

            if (bchID == "" || bchName == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("สาขา");
                radDropDownList_Dpt.Focus();
                return;
            }
            //รายละเอียด
            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียด");
                radTextBox_Desc.Focus();
                return;
            }
            //check Repair
            string typeRepair = radDropDownList_Reapair.SelectedValue.ToString();
            if (radDropDownList_Reapair.SelectedValue.ToString() == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ประเภทการซ่อม");
                radDropDownList_Reapair.Focus();
                return;
            }

            string descJOB;
            descJOB = "- " + radTextBox_Desc.Text.Trim();
            string dessc = SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            descJOB = descJOB + Environment.NewLine + "-> BY " + dessc;

            //String t_AX = "";
            //if (radTextBox_CarID.Text != "")
            //{
            //    if (radDropDownList_Reapair.SelectedValue.ToString() != "00341")
            //    {
            //        DataTable dtSNCar = JOBClass.GetVEHICLE_ByCarID(radTextBox_CarID.Text);
            //        string carSN = "";
            //        if (dtSNCar.Rows.Count > 0) { carSN = dtSNCar.Rows[0]["SERIALNUMBER"].ToString(); }

            //        t_AX = JOBClass.SendAX_OpenJOBCarExternalList(radTextBox_CarID.Text, radDropDownList_Reapair.SelectedItem[0].ToString(), radTextBox_SN.Text, carSN);
            //    }
            //}

            //if (t_AX != "")
            //{
            //    MsgBoxClass.MsgBoxShow_SaveStatus(t_AX);
            //    return;
            //}

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = String.Format(@"INSERT INTO " + _tblName + " ([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],[JOB_GROUPSUB] " +
                            ",[JOB_WHOINS],[JOB_NAMEINS],[JOB_SHORTNAMEINS],[JOB_DESCRIPTION],[JOB_SN],[JOB_TYPEREPAIRID],[JOB_DATEINS],[JOB_TYPE],[JOB_CARID])  VALUES  " +
                            "('" + billMaxNO + "','" + bchID + "','" + bchName + "','" + radDropDownList_GroupSUB.SelectedValue + "'," +
                            "'" + SystemClass.SystemUserID + "','" + SystemClass.SystemUserName + "'," +
                            "'" + SystemClass.SystemUserNameShort + "','" + descJOB + "'+ ' [' + CONVERT(VARCHAR,GETDATE(),25) + ']'," +
                            "'" + radTextBox_SN.Text.Trim() + "','" + typeRepair + "',GETDATE(),'SUPC','" + radTextBox_CarID.Text + "' ) ");
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void RadButton_openCar_Click(object sender, EventArgs e)
        {

            DataTable dt = Models.AssetClass.FindDataVEHICLE("", " AND VEHICLEGROUP !='MOTORCYCLE' ");
            ShowDataDGV frm = new ShowDataDGV()
            { dtData = dt };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                if (JOB_Class.GetJob_ByCar(frm.pID) > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ทะเบียนรถ {frm.pID} {Environment.NewLine} มีข้อมูลการซ่อมอยู่แล้ว {Environment.NewLine} ไม่สามารถเปิดจ๊อบซ้ำได้อีก");
                    radTextBox_CarID.Text = "";
                    radButton_openCar.Focus();
                    return;
                }
                else
                {
                    radTextBox_CarID.Text = frm.pID;
                    radTextBox_Desc.Focus();
                }
            }
            else
            {
                radTextBox_CarID.Text = "";
                radButton_openCar.Focus();
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
    }
}
