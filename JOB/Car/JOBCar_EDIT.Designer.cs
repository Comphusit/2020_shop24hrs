﻿namespace PC_Shop24Hrs.JOB.Car
{
    partial class JOBCar_EDIT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JOBCar_EDIT));
            this.radGroupBox_Desc = new Telerik.WinControls.UI.RadGroupBox();
            this.radTextBox_Update = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_EmplID = new Telerik.WinControls.UI.RadTextBox();
            this.Bt_LockCar = new System.Windows.Forms.Button();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Car = new Telerik.WinControls.UI.RadButton();
            this.radButton_OpenView = new Telerik.WinControls.UI.RadButton();
            this.RadButton_OpenAdd = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_STA = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Garage = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Car = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList_Reapair = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_BranchID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_BranchName = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_SN = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_branch = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_GroupSUB = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_STACLOSE = new System.Windows.Forms.CheckBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_tel = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox_Bill = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView_Send = new Telerik.WinControls.UI.RadGridView();
            this.radButton_Add = new Telerik.WinControls.UI.RadButton();
            this.Button_MNOT = new System.Windows.Forms.Button();
            this.Button_AX = new System.Windows.Forms.Button();
            this.Button_MNIO = new System.Windows.Forms.Button();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f = new Telerik.WinControls.RootRadElement();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).BeginInit();
            this.radGroupBox_Desc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Car)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OpenAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_STA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Garage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Car)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Reapair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BranchID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupSUB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Bill)).BeginInit();
            this.radGroupBox_Bill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_Desc
            // 
            this.radGroupBox_Desc.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Update);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_EmplName);
            this.radGroupBox_Desc.Controls.Add(this.radLabel9);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_EmplID);
            this.radGroupBox_Desc.Controls.Add(this.Bt_LockCar);
            this.radGroupBox_Desc.Controls.Add(this.radButton_pdt);
            this.radGroupBox_Desc.Controls.Add(this.RadButton_Car);
            this.radGroupBox_Desc.Controls.Add(this.radButton_OpenView);
            this.radGroupBox_Desc.Controls.Add(this.RadButton_OpenAdd);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_STA);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_Garage);
            this.radGroupBox_Desc.Controls.Add(this.radLabel7);
            this.radGroupBox_Desc.Controls.Add(this.radLabel6);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Car);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_Reapair);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_BranchID);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_BranchName);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_SN);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Save);
            this.radGroupBox_Desc.Controls.Add(this.radLabel4);
            this.radGroupBox_Desc.Controls.Add(this.radLabel3);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_branch);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_GroupSUB);
            this.radGroupBox_Desc.Controls.Add(this.radLabel1);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Desc);
            this.radGroupBox_Desc.Controls.Add(this.radCheckBox_STACLOSE);
            this.radGroupBox_Desc.Controls.Add(this.radLabel2);
            this.radGroupBox_Desc.Controls.Add(this.radLabel5);
            this.radGroupBox_Desc.Controls.Add(this.radLabel8);
            this.radGroupBox_Desc.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Desc.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล";
            this.radGroupBox_Desc.Location = new System.Drawing.Point(3, 4);
            this.radGroupBox_Desc.Name = "radGroupBox_Desc";
            this.radGroupBox_Desc.Size = new System.Drawing.Size(551, 664);
            this.radGroupBox_Desc.TabIndex = 26;
            this.radGroupBox_Desc.Text = "รายละเอียดข้อมูล";
            // 
            // radTextBox_Update
            // 
            this.radTextBox_Update.AcceptsReturn = true;
            this.radTextBox_Update.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Update.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_Update.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Update.Location = new System.Drawing.Point(9, 528);
            this.radTextBox_Update.Multiline = true;
            this.radTextBox_Update.Name = "radTextBox_Update";
            // 
            // 
            // 
            this.radTextBox_Update.RootElement.StretchVertically = true;
            this.radTextBox_Update.Size = new System.Drawing.Size(532, 91);
            this.radTextBox_Update.TabIndex = 0;
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel_EmplName.AutoSize = false;
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplName.Location = new System.Drawing.Point(180, 484);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(358, 23);
            this.radLabel_EmplName.TabIndex = 78;
            // 
            // radLabel9
            // 
            this.radLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel9.Location = new System.Drawing.Point(9, 482);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(70, 22);
            this.radLabel9.TabIndex = 77;
            this.radLabel9.Text = "ช่างผู้ซ่อม";
            // 
            // radTextBox_EmplID
            // 
            this.radTextBox_EmplID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radTextBox_EmplID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmplID.Location = new System.Drawing.Point(91, 483);
            this.radTextBox_EmplID.MaxLength = 7;
            this.radTextBox_EmplID.Name = "radTextBox_EmplID";
            this.radTextBox_EmplID.Size = new System.Drawing.Size(83, 21);
            this.radTextBox_EmplID.TabIndex = 76;
            this.radTextBox_EmplID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_EmplID_KeyPress);
            this.radTextBox_EmplID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmplID_KeyUp);
            // 
            // Bt_LockCar
            // 
            this.Bt_LockCar.BackColor = System.Drawing.Color.SkyBlue;
            this.Bt_LockCar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Bt_LockCar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bt_LockCar.ForeColor = System.Drawing.Color.Black;
            this.Bt_LockCar.Image = global::PC_Shop24Hrs.Properties.Resources.TagLO;
            this.Bt_LockCar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Bt_LockCar.Location = new System.Drawing.Point(432, 192);
            this.Bt_LockCar.Name = "Bt_LockCar";
            this.Bt_LockCar.Size = new System.Drawing.Size(106, 27);
            this.Bt_LockCar.TabIndex = 74;
            this.Bt_LockCar.Text = "   รับกุญแจ";
            this.Bt_LockCar.UseVisualStyleBackColor = false;
            this.Bt_LockCar.Click += new System.EventHandler(this.Bt_LockCar_Click);
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(512, 21);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 73;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // RadButton_Car
            // 
            this.RadButton_Car.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_Car.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_Car.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_Car.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_Car.Location = new System.Drawing.Point(91, 192);
            this.RadButton_Car.Name = "RadButton_Car";
            this.RadButton_Car.Size = new System.Drawing.Size(26, 26);
            this.RadButton_Car.TabIndex = 55;
            this.RadButton_Car.Click += new System.EventHandler(this.RadButton_Car_Click);
            // 
            // radButton_OpenView
            // 
            this.radButton_OpenView.BackColor = System.Drawing.Color.Transparent;
            this.radButton_OpenView.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_OpenView.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.radButton_OpenView.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_OpenView.Location = new System.Drawing.Point(501, 87);
            this.radButton_OpenView.Name = "radButton_OpenView";
            // 
            // 
            // 
            this.radButton_OpenView.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_OpenView.Size = new System.Drawing.Size(26, 26);
            this.radButton_OpenView.TabIndex = 54;
            this.radButton_OpenView.Click += new System.EventHandler(this.RadButton_OpenView_Click);
            // 
            // RadButton_OpenAdd
            // 
            this.RadButton_OpenAdd.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_OpenAdd.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_OpenAdd.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.RadButton_OpenAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_OpenAdd.Location = new System.Drawing.Point(471, 87);
            this.RadButton_OpenAdd.Name = "RadButton_OpenAdd";
            // 
            // 
            // 
            this.RadButton_OpenAdd.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.RadButton_OpenAdd.Size = new System.Drawing.Size(26, 26);
            this.RadButton_OpenAdd.TabIndex = 53;
            this.RadButton_OpenAdd.Click += new System.EventHandler(this.RadButton_OpenAdd_Click);
            // 
            // radDropDownList_STA
            // 
            this.radDropDownList_STA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_STA.BackColor = System.Drawing.Color.White;
            this.radDropDownList_STA.DropDownAnimationEnabled = false;
            this.radDropDownList_STA.DropDownHeight = 124;
            this.radDropDownList_STA.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_STA.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_STA.Location = new System.Drawing.Point(279, 194);
            this.radDropDownList_STA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_STA.Name = "radDropDownList_STA";
            // 
            // 
            // 
            this.radDropDownList_STA.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_STA.Size = new System.Drawing.Size(148, 25);
            this.radDropDownList_STA.TabIndex = 47;
            this.radDropDownList_STA.Text = "radDropDownList2";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_STA.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_STA.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList2";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_STA.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radDropDownList_Garage
            // 
            this.radDropDownList_Garage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Garage.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Garage.DropDownAnimationEnabled = false;
            this.radDropDownList_Garage.DropDownHeight = 124;
            this.radDropDownList_Garage.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Garage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Garage.Location = new System.Drawing.Point(91, 164);
            this.radDropDownList_Garage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Garage.Name = "radDropDownList_Garage";
            // 
            // 
            // 
            this.radDropDownList_Garage.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Garage.Size = new System.Drawing.Size(336, 25);
            this.radDropDownList_Garage.TabIndex = 45;
            this.radDropDownList_Garage.Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Garage.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Garage.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Garage.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel7.Location = new System.Drawing.Point(9, 164);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(86, 23);
            this.radLabel7.TabIndex = 44;
            this.radLabel7.Text = "อู่ซ่อม";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel6.Location = new System.Drawing.Point(468, 60);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(48, 23);
            this.radLabel6.TabIndex = 40;
            this.radLabel6.Text = "รูปเปิด";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radTextBox_Car
            // 
            this.radTextBox_Car.Enabled = false;
            this.radTextBox_Car.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_Car.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Car.Location = new System.Drawing.Point(122, 195);
            this.radTextBox_Car.Name = "radTextBox_Car";
            this.radTextBox_Car.Size = new System.Drawing.Size(95, 23);
            this.radTextBox_Car.TabIndex = 39;
            this.radTextBox_Car.Text = "80-8076";
            // 
            // radDropDownList_Reapair
            // 
            this.radDropDownList_Reapair.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Reapair.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Reapair.DropDownAnimationEnabled = false;
            this.radDropDownList_Reapair.DropDownHeight = 124;
            this.radDropDownList_Reapair.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Reapair.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Reapair.Location = new System.Drawing.Point(91, 91);
            this.radDropDownList_Reapair.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Reapair.Name = "radDropDownList_Reapair";
            // 
            // 
            // 
            this.radDropDownList_Reapair.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Reapair.Size = new System.Drawing.Size(336, 25);
            this.radDropDownList_Reapair.TabIndex = 34;
            this.radDropDownList_Reapair.Text = "radDropDownList1";
            this.radDropDownList_Reapair.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Reapair_SelectedValueChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Reapair.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Reapair.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Reapair.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radTextBox_BranchID
            // 
            this.radTextBox_BranchID.Enabled = false;
            this.radTextBox_BranchID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_BranchID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_BranchID.Location = new System.Drawing.Point(91, 31);
            this.radTextBox_BranchID.MaxLength = 3;
            this.radTextBox_BranchID.Name = "radTextBox_BranchID";
            this.radTextBox_BranchID.Size = new System.Drawing.Size(69, 25);
            this.radTextBox_BranchID.TabIndex = 3;
            this.radTextBox_BranchID.Text = "012";
            // 
            // radLabel_BranchName
            // 
            this.radLabel_BranchName.AutoSize = false;
            this.radLabel_BranchName.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radLabel_BranchName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BranchName.Location = new System.Drawing.Point(161, 31);
            this.radLabel_BranchName.Name = "radLabel_BranchName";
            this.radLabel_BranchName.Size = new System.Drawing.Size(380, 23);
            this.radLabel_BranchName.TabIndex = 18;
            this.radLabel_BranchName.Text = "สาขา";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(255, 623);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_SN
            // 
            this.radTextBox_SN.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_SN.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SN.Location = new System.Drawing.Point(91, 119);
            this.radTextBox_SN.Multiline = true;
            this.radTextBox_SN.Name = "radTextBox_SN";
            // 
            // 
            // 
            this.radTextBox_SN.RootElement.StretchVertically = true;
            this.radTextBox_SN.Size = new System.Drawing.Size(336, 42);
            this.radTextBox_SN.TabIndex = 5;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(157, 623);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 1;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel4.Location = new System.Drawing.Point(9, 92);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(86, 23);
            this.radLabel4.TabIndex = 23;
            this.radLabel4.Text = "ประเภทซ่อม";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel3.Location = new System.Drawing.Point(9, 117);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(71, 42);
            this.radLabel3.TabIndex = 22;
            this.radLabel3.Text = "อุปกรณ์ที่มีปัญหา";
            // 
            // radLabel_branch
            // 
            this.radLabel_branch.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel_branch.Location = new System.Drawing.Point(14, 32);
            this.radLabel_branch.Name = "radLabel_branch";
            this.radLabel_branch.Size = new System.Drawing.Size(40, 22);
            this.radLabel_branch.TabIndex = 17;
            this.radLabel_branch.Text = "สาขา";
            // 
            // radDropDownList_GroupSUB
            // 
            this.radDropDownList_GroupSUB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_GroupSUB.BackColor = System.Drawing.Color.White;
            this.radDropDownList_GroupSUB.DropDownAnimationEnabled = false;
            this.radDropDownList_GroupSUB.DropDownHeight = 124;
            this.radDropDownList_GroupSUB.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_GroupSUB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_GroupSUB.Location = new System.Drawing.Point(91, 62);
            this.radDropDownList_GroupSUB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_GroupSUB.Name = "radDropDownList_GroupSUB";
            // 
            // 
            // 
            this.radDropDownList_GroupSUB.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_GroupSUB.Size = new System.Drawing.Size(336, 25);
            this.radDropDownList_GroupSUB.TabIndex = 4;
            this.radDropDownList_GroupSUB.Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_GroupSUB.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_GroupSUB.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel1.Location = new System.Drawing.Point(9, 64);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(71, 22);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "ประเภทรถ";
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Black;
            this.radTextBox_Desc.Location = new System.Drawing.Point(9, 223);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            this.radTextBox_Desc.ReadOnly = true;
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Desc.Size = new System.Drawing.Size(532, 259);
            this.radTextBox_Desc.TabIndex = 8;
            // 
            // radCheckBox_STACLOSE
            // 
            this.radCheckBox_STACLOSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radCheckBox_STACLOSE.AutoSize = true;
            this.radCheckBox_STACLOSE.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radCheckBox_STACLOSE.ForeColor = System.Drawing.Color.Black;
            this.radCheckBox_STACLOSE.Location = new System.Drawing.Point(11, 510);
            this.radCheckBox_STACLOSE.Name = "radCheckBox_STACLOSE";
            this.radCheckBox_STACLOSE.Size = new System.Drawing.Size(45, 22);
            this.radCheckBox_STACLOSE.TabIndex = 33;
            this.radCheckBox_STACLOSE.Text = "ปิด";
            this.radCheckBox_STACLOSE.UseVisualStyleBackColor = true;
            this.radCheckBox_STACLOSE.CheckedChanged += new System.EventHandler(this.RadCheckBox_STACLOSE_CheckedChanged);
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel2.Location = new System.Drawing.Point(8, 195);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(76, 23);
            this.radLabel2.TabIndex = 37;
            this.radLabel2.Text = "ทะเบียนรถ";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radLabel5.Location = new System.Drawing.Point(230, 196);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(76, 23);
            this.radLabel5.TabIndex = 46;
            this.radLabel5.Text = "สถานะ";
            // 
            // radLabel8
            // 
            this.radLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel8.AutoSize = false;
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.ForeColor = System.Drawing.Color.Firebrick;
            this.radLabel8.Location = new System.Drawing.Point(122, 510);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(416, 23);
            this.radLabel8.TabIndex = 75;
            this.radLabel8.Text = "รถจะถูกล็อกในระบบ AX เมื่อยานยนต์บันทึกรับกุญแจแล้วเท่านั้น";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel_tel
            // 
            this.radLabel_tel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_tel.AutoSize = false;
            this.radLabel_tel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radLabel_tel.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_tel.Location = new System.Drawing.Point(95, 31);
            this.radLabel_tel.Name = "radLabel_tel";
            this.radLabel_tel.Size = new System.Drawing.Size(547, 23);
            this.radLabel_tel.TabIndex = 30;
            this.radLabel_tel.Text = "Tel 1818-1819";
            this.radLabel_tel.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radGroupBox_Bill
            // 
            this.radGroupBox_Bill.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Bill.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_Bill.Controls.Add(this.radGridView_Send);
            this.radGroupBox_Bill.Controls.Add(this.radButton_Add);
            this.radGroupBox_Bill.Controls.Add(this.Button_MNOT);
            this.radGroupBox_Bill.Controls.Add(this.Button_AX);
            this.radGroupBox_Bill.Controls.Add(this.Button_MNIO);
            this.radGroupBox_Bill.Controls.Add(this.radGridView_Show);
            this.radGroupBox_Bill.Controls.Add(this.radLabel_tel);
            this.radGroupBox_Bill.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Bill.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Bill.HeaderText = "รายละเอียด บิล";
            this.radGroupBox_Bill.Location = new System.Drawing.Point(560, 4);
            this.radGroupBox_Bill.Name = "radGroupBox_Bill";
            this.radGroupBox_Bill.Size = new System.Drawing.Size(645, 664);
            this.radGroupBox_Bill.TabIndex = 29;
            this.radGroupBox_Bill.Text = "รายละเอียด บิล";
            // 
            // radGridView_Send
            // 
            this.radGridView_Send.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Send.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Send.Location = new System.Drawing.Point(2, 63);
            // 
            // 
            // 
            this.radGridView_Send.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Send.Name = "radGridView_Send";
            // 
            // 
            // 
            this.radGridView_Send.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Send.Size = new System.Drawing.Size(640, 146);
            this.radGridView_Send.TabIndex = 22;
            this.radGridView_Send.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Send_ViewCellFormatting);
            // 
            // radButton_Add
            // 
            this.radButton_Add.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Add.Location = new System.Drawing.Point(5, 26);
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.Size = new System.Drawing.Size(84, 33);
            this.radButton_Add.TabIndex = 21;
            this.radButton_Add.Text = "สาขาส่ง";
            this.radButton_Add.ThemeName = "Fluent";
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).Text = "สาขาส่ง";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // Button_MNOT
            // 
            this.Button_MNOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.Button_MNOT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_MNOT.ForeColor = System.Drawing.Color.Black;
            this.Button_MNOT.Location = new System.Drawing.Point(317, 215);
            this.Button_MNOT.Name = "Button_MNOT";
            this.Button_MNOT.Size = new System.Drawing.Size(150, 33);
            this.Button_MNOT.TabIndex = 20;
            this.Button_MNOT.Text = "MNOT เครื่องมือ";
            this.Button_MNOT.UseVisualStyleBackColor = false;
            this.Button_MNOT.Click += new System.EventHandler(this.Button_MNOT_Click);
            // 
            // Button_AX
            // 
            this.Button_AX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.Button_AX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_AX.ForeColor = System.Drawing.Color.Black;
            this.Button_AX.Location = new System.Drawing.Point(5, 215);
            this.Button_AX.Name = "Button_AX";
            this.Button_AX.Size = new System.Drawing.Size(150, 33);
            this.Button_AX.TabIndex = 19;
            this.Button_AX.Text = "AX";
            this.Button_AX.UseVisualStyleBackColor = false;
            this.Button_AX.Click += new System.EventHandler(this.Button_AX_Click);
            // 
            // Button_MNIO
            // 
            this.Button_MNIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            this.Button_MNIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_MNIO.ForeColor = System.Drawing.Color.Black;
            this.Button_MNIO.Location = new System.Drawing.Point(161, 215);
            this.Button_MNIO.Name = "Button_MNIO";
            this.Button_MNIO.Size = new System.Drawing.Size(150, 33);
            this.Button_MNIO.TabIndex = 17;
            this.Button_MNIO.Text = "MNIO นำของออก";
            this.Button_MNIO.UseVisualStyleBackColor = false;
            this.Button_MNIO.Click += new System.EventHandler(this.Button_MNIO_Click);
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(2, 251);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(641, 411);
            this.radGridView_Show.TabIndex = 15;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f
            // 
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.AutoSize = false;
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.Name = "object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f";
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.StretchHorizontally = true;
            this.object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f.StretchVertically = true;
            // 
            // JOBCar_EDIT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(1215, 675);
            this.Controls.Add(this.radGroupBox_Bill);
            this.Controls.Add(this.radGroupBox_Desc);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JOBCar_EDIT";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "แก้ไขข้อมูล.";
            this.Load += new System.EventHandler(this.JOBCar_EDIT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).EndInit();
            this.radGroupBox_Desc.ResumeLayout(false);
            this.radGroupBox_Desc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Car)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OpenAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_STA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Garage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Car)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Reapair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BranchID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupSUB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Bill)).EndInit();
            this.radGroupBox_Bill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_BranchID;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchName;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SN;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_branch;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_GroupSUB;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Update;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_tel;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Bill;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        protected System.Windows.Forms.CheckBox radCheckBox_STACLOSE;
        private System.Windows.Forms.Button Button_MNIO;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Reapair;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Car;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.RootRadElement object_e25af3dc_7b8d_4cef_86bc_63e9f9f5e76f;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Garage;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_STA;
        private System.Windows.Forms.Button Button_MNOT;
        private System.Windows.Forms.Button Button_AX;
        private Telerik.WinControls.UI.RadGridView radGridView_Send;
        protected Telerik.WinControls.UI.RadButton radButton_Add;
        private Telerik.WinControls.UI.RadButton radButton_OpenView;
        private Telerik.WinControls.UI.RadButton RadButton_OpenAdd;
        private Telerik.WinControls.UI.RadButton RadButton_Car;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
        private System.Windows.Forms.Button Bt_LockCar;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
    }
}
