﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.JOB
{
    public partial class JOB_GROUPSUB : Telerik.WinControls.UI.RadForm
    {
        //String _grpID;
        private readonly string _grpID;
        private readonly string _sta;//1 คือ แสดงคู่มือการใช้

        string StatusSave = "";//0 = Insert , 1 = Update
        public JOB_GROUPSUB(string pGroupID, string sta = "0")
        {
            InitializeComponent();

            _grpID = pGroupID;
            _sta = sta;

        }
        //Load Main
        private void JOB_GROUPSUB_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_TYPEID("JOBGROUPSUB_ID", "รหัส"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "คำอธิบาย", 450));
            if (_sta == "1") RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("Doc", "แสดงคู่มือ", 150));

            radButton_Cancel.ButtonElement.ShowBorder = true; radButton_Save.ButtonElement.ShowBorder = true;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่ม";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radStatusStrip1.SizingGrip = false;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Branch);
            Set_ListBox();
            Set_DGV();
        }
        //listGroup
        void Set_ListBox()
        {
            radDropDownList_Branch.DataSource = JOB_Class.GetJOB_Group(_grpID);
            radDropDownList_Branch.ValueMember = "JOBGROUP_ID";
            radDropDownList_Branch.DisplayMember = "JOBGROUP_DESCRIPTION";
        }
        //Set DGV
        void Set_DGV()
        {
            try
            {
                RadGridView_Show.DataSource = JOB_Class.GetJOB_GroupSub(radDropDownList_Branch.SelectedValue.ToString());
            }
            catch (Exception)
            {

            }
            radTextBox_Desc.Enabled = false;
            radTextBox_Desc.Text = "";
            radTextBox_Desc.Focus();

            radButton_Save.Enabled = false;
        }
        //Reset All
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            Set_ListBox();
            Set_DGV();
            radTextBox_Desc.Focus();
        }


        //SetInsert
        void SetForInsert()
        {
            radTextBox_Desc.Enabled = true;
            StatusSave = "0";
            radButton_Save.Enabled = true;
            radTextBox_Desc.Text = "";
            radTextBox_Desc.Focus();
        }

        //Insert
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //check ข้อมูลก่อน Insert
            if (radTextBox_Desc.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("คำอธิบาย");
                return;
            }
            //CheckGroup ใหญ่
            string groupID = radDropDownList_Branch.SelectedValue.ToString();
            if (groupID == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("คำอธิบาย");
                return;
            }
            //save Data
            string T;
            switch (StatusSave)
            {
                case "0":// Insert
                    string billNO = Class.ConfigClass.GetMaxINVOICEID("JOBGROUPSUB", "-", "0", "2");
                    string sqlIn = $@"
                        INSERT INTO [dbo].[SHOP_JOBGROUPSUB] 
                            ([JOBGROUPSUB_ID],[JOBGROUPSUB_DESCRIPTION],[JOBGROUP_ID],[WHOINS] )
                        VALUES  ('{billNO}','{radTextBox_Desc.Text}','{groupID}','{SystemClass.SystemUserID}') ";
                    T = ConnectionClass.ExecuteSQL_Main(sqlIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    Set_DGV();
                    break;

                case "1": // Update  //checkGroupSub
                    string groupSubID = RadGridView_Show.CurrentRow.Cells[0].Value.ToString();
                    if (groupSubID == "")
                    {
                        MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รหัส");
                        return;
                    }

                    string sqlUp = $@"
                        UPDATE  [dbo].[SHOP_JOBGROUPSUB] 
                        SET     JOBGROUPSUB_DESCRIPTION = '{radTextBox_Desc.Text}',WHOUPD = '{SystemClass.SystemUserID}',DATEUPD = GETDATE() 
                        WHERE   JOBGROUPSUB_ID = '{groupSubID}' ";
                    T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    Set_DGV();
                    break;
                default:
                    break;
            }
        }
        //Cheange DGV
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            Set_DGV();
        }
        //SetEdit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if ((RadGridView_Show.Rows.Count == 0) || (RadGridView_Show.CurrentRow.Index == -1) || (RadGridView_Show.CurrentRow.Cells["JOBGROUPSUB_ID"].Value.ToString() == "")) return;

            if (RadGridView_Show.CurrentRow.Cells["JOBGROUPSUB_ID"].Value.ToString() == "00003")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"สำหรับประเภทงานนี้ ไม่ได้รับอนุญาติให้แก้ไขทุกกรณี{Environment.NewLine} [ถ้าต้องการแก้ไขให้ติดต่อ ComMinimart 8570 เท่านั้น]");
                return;
            }

            radTextBox_Desc.Text = RadGridView_Show.CurrentRow.Cells["JOBGROUPSUB_DESCRIPTION"].Value.ToString();
            radTextBox_Desc.Enabled = true;
            radButton_Save.Enabled = true;
            StatusSave = "1";
        }
        //ForInsert
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            SetForInsert();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _grpID);
        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "Doc":
                    if (RadGridView_Show.CurrentRow.Cells["JOBGROUPSUB_ID"].Value.ToString() == "") return;
                    Controllers.FormClass.Document_Check(RadGridView_Show.CurrentRow.Cells["JOBGROUPSUB_ID"].Value.ToString(), _grpID);
                    break;
                default:
                    break;
            }
        }
    }
}
