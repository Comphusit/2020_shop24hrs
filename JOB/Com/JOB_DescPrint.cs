﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.Drawing;
using System.IO;


namespace PC_Shop24Hrs.JOB.Com
{
    public partial class JOB_DescPrint : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable _pBchDesc;

        public JOB_DescPrint(DataTable pBchDesc)
        {
            _pBchDesc = pBchDesc;
            InitializeComponent();
        }

        private void JOB_DescPrint_Load(object sender, EventArgs e)
        {

            radButtonElement_Excel.ShowBorder = true;
            radStatusStrip_Main.SizingGrip = false;
            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH", "สาขา", 150));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("IP", "IP", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESC", "รายละเอียด", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMAGE1", "รูป ", 500));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMAGE2", "รูป 2 ", 500));
            // radGridView_Show.TableElement.RowHeight = 350;
            radGridView_Show.TableElement.MasterTemplate.EnableFiltering = false;

            SetDGV();

        }

        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < _pBchDesc.Rows.Count; i++)
            {
                string pPath = PathImageClass.pPathJOB;
                string pDate = _pBchDesc.Rows[i]["DATE"].ToString();
                string pJobNumber = _pBchDesc.Rows[i]["JOB"].ToString();
 
                pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + pJobNumber + @"\";
                DirectoryInfo DirInfo = new DirectoryInfo(pPath);
                try
                {
                    FileInfo[] FilesSPC = DirInfo.GetFiles("*" + pJobNumber + "*", SearchOption.AllDirectories);

                    for (int iR = 0; iR < FilesSPC.Length; iR++)
                    {
                        radGridView_Show.Rows.Add(_pBchDesc.Rows[i]["DESC"].ToString(), Image.FromFile(FilesSPC[iR].FullName));
                        radGridView_Show.Rows[i].Height = 320;
                    }
                }
                catch (Exception)
                {
                    radGridView_Show.Rows.Add(_pBchDesc.Rows[i]["DESC"].ToString());
                    radGridView_Show.Rows[i].Height = 100;
                }
            }
            this.Cursor = Cursors.Default;
        }

        //void SetDGV()
        //{
        //    this.Cursor = Cursors.WaitCursor;
        //    for (int i = 0; i < _pBchDesc.Rows.Count; i++)
        //    {
        //        string pPath = PathImageClass.pPathJOB;
        //        string pDate = _pBchDesc.Rows[i]["DATE"].ToString();
        //        string pJobNumber = _pBchDesc.Rows[i]["JOB"].ToString();

        //        //radGridView_Show.Rows.Add(_pBchDesc.Rows[i]["BRANCH"].ToString(), _pBchDesc.Rows[i]["IP"].ToString(), _pBchDesc.Rows[i]["DESC"].ToString());
        //        radGridView_Show.Rows.Add(_pBchDesc.Rows[i]["DESC"].ToString());

        //        pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + pJobNumber + @"\";
        //        DirectoryInfo DirInfo = new DirectoryInfo(pPath);
        //        try
        //        {
        //            FileInfo[] FilesSPC = DirInfo.GetFiles("*" + pJobNumber + "*", SearchOption.AllDirectories);

        //            if (FilesSPC.Length == 0)
        //            {
        //                //radGridView_Show.Rows[i].Cells["IMAGE1"].Value = "" Image.FromFile(PathImageClass.pImageEmply);
        //                //radGridView_Show.Rows[i].Cells["IMAGE2"].Value = Image.FromFile(PathImageClass.pImageEmply);
        //                radGridView_Show.Rows[i].Height = 100;
        //            }
        //            else if (FilesSPC.Length == 1)
        //            {
        //                radGridView_Show.Rows[i].Cells["IMAGE1"].Value = Image.FromFile(FilesSPC[0].FullName);
        //                radGridView_Show.Rows[i].Height = 320;
        //                //radGridView_Show.Rows[i].Cells["IMAGE2"].Value = Image.FromFile(PathImageClass.pImageEmply);
        //            }
        //            else if (FilesSPC.Length == 2)
        //            {
        //                radGridView_Show.Rows[i].Cells["IMAGE1"].Value = Image.FromFile(FilesSPC[0].FullName);
        //                radGridView_Show.Rows[i].Cells["IMAGE2"].Value = Image.FromFile(FilesSPC[1].FullName);
        //                radGridView_Show.Rows[i].Height = 320;
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            radGridView_Show.Rows[i].Height = 100;
        //            //radGridView_Show.Rows[i].Cells["IMAGE1"].Value = Image.FromFile(PathImageClass.pImageEmply);
        //            //radGridView_Show.Rows[i].Cells["IMAGE2"].Value = Image.FromFile(PathImageClass.pImageEmply);
        //        }
        //    }
        //    this.Cursor = Cursors.Default;
        //}



        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("ใบงานออกนอก", radGridView_Show,"2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
