﻿namespace PC_Shop24Hrs.JOB.Com
{
    partial class JOBCOM_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JOBCOM_Main));
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radRadioButtonElement_Open = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRadioButtonElement_ALL = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radStatusStrip_Main = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator8 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCheckBoxElement_PO = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radCheckBoxElement_Out = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement_Bch = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement_BchOOO = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement_krabi = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement_Surat = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement_Samui = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement_Nakorn = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.commandBarSeparator12 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator10 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator11 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.timer_Auto = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator9 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator13 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator14 = new Telerik.WinControls.UI.CommandBarSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Main)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 53);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(900, 577);
            this.radGridView_Show.TabIndex = 1;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(656, 522);
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "เพิ่ม";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.UseCompatibleTextRendering = false;
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radRadioButtonElement_Open
            // 
            this.radRadioButtonElement_Open.AutoSize = true;
            this.radRadioButtonElement_Open.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButtonElement_Open.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButtonElement_Open.Name = "radRadioButtonElement_Open";
            this.radRadioButtonElement_Open.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radRadioButtonElement_Open, false);
            this.radRadioButtonElement_Open.Text = "ค้างปิด";
            this.radRadioButtonElement_Open.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButtonElement_Open.UseCompatibleTextRendering = false;
            this.radRadioButtonElement_Open.CheckStateChanged += new System.EventHandler(this.RadRadioButtonElement_Open_CheckStateChanged);
            // 
            // radRadioButtonElement_ALL
            // 
            this.radRadioButtonElement_ALL.AutoSize = true;
            this.radRadioButtonElement_ALL.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButtonElement_ALL.Name = "radRadioButtonElement_ALL";
            this.radRadioButtonElement_ALL.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radRadioButtonElement_ALL, false);
            this.radRadioButtonElement_ALL.Text = "ทั้งหมด";
            this.radRadioButtonElement_ALL.UseCompatibleTextRendering = false;
            this.radRadioButtonElement_ALL.CheckStateChanged += new System.EventHandler(this.RadRadioButtonElement_ALL_CheckStateChanged);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radStatusStrip_Main
            // 
            this.radStatusStrip_Main.AutoSize = false;
            this.radStatusStrip_Main.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip_Main.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_add,
            this.commandBarSeparator1,
            this.radButtonElement_Edit,
            this.commandBarSeparator2,
            this.radButtonElement_Print,
            this.commandBarSeparator8,
            this.radRadioButtonElement_Open,
            this.commandBarSeparator3,
            this.radRadioButtonElement_ALL,
            this.commandBarSeparator4,
            this.radCheckBoxElement_PO,
            this.commandBarSeparator5,
            this.radCheckBoxElement_Out,
            this.commandBarSeparator11,
            this.radCheckBoxElement_Bch,
            this.commandBarSeparator6,
            this.radCheckBoxElement_BchOOO,
            this.commandBarSeparator7,
            this.radCheckBoxElement_krabi,
            this.commandBarSeparator9,
            this.radCheckBoxElement_Surat,
            this.commandBarSeparator14,
            this.radCheckBoxElement_Samui,
            this.commandBarSeparator13,
            this.radCheckBoxElement_Nakorn,
            this.commandBarSeparator12,
            this.radButtonElement_Excel,
            this.commandBarSeparator10,
            this.RadButtonElement_pdt});
            this.radStatusStrip_Main.Location = new System.Drawing.Point(3, 10);
            this.radStatusStrip_Main.Name = "radStatusStrip_Main";
            this.radStatusStrip_Main.Size = new System.Drawing.Size(900, 37);
            this.radStatusStrip_Main.TabIndex = 2;
            // 
            // radButtonElement_Print
            // 
            this.radButtonElement_Print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Print.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radButtonElement_Print.Name = "radButtonElement_Print";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Print, false);
            this.radButtonElement_Print.Text = "radButtonElement1";
            this.radButtonElement_Print.Click += new System.EventHandler(this.RadButtonElement_Print_Click);
            // 
            // commandBarSeparator8
            // 
            this.commandBarSeparator8.Name = "commandBarSeparator8";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator8, false);
            this.commandBarSeparator8.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radCheckBoxElement_PO
            // 
            this.radCheckBoxElement_PO.Checked = false;
            this.radCheckBoxElement_PO.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radCheckBoxElement_PO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBoxElement_PO.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radCheckBoxElement_PO.Name = "radCheckBoxElement_PO";
            this.radCheckBoxElement_PO.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_PO, false);
            this.radCheckBoxElement_PO.Text = "สั่งของ";
            this.radCheckBoxElement_PO.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_PO_CheckStateChanged);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radCheckBoxElement_Out
            // 
            this.radCheckBoxElement_Out.Checked = false;
            this.radCheckBoxElement_Out.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radCheckBoxElement_Out.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBoxElement_Out.Name = "radCheckBoxElement_Out";
            this.radCheckBoxElement_Out.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_Out, false);
            this.radCheckBoxElement_Out.Text = "อน.";
            this.radCheckBoxElement_Out.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_Out_CheckStateChanged);
            // 
            // radCheckBoxElement_Bch
            // 
            this.radCheckBoxElement_Bch.Checked = false;
            this.radCheckBoxElement_Bch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBoxElement_Bch.Name = "radCheckBoxElement_Bch";
            this.radCheckBoxElement_Bch.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_Bch, false);
            this.radCheckBoxElement_Bch.Text = "ภูเก็ต";
            this.radCheckBoxElement_Bch.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_Bch_CheckStateChanged);
            // 
            // radCheckBoxElement_BchOOO
            // 
            this.radCheckBoxElement_BchOOO.Checked = false;
            this.radCheckBoxElement_BchOOO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBoxElement_BchOOO.Name = "radCheckBoxElement_BchOOO";
            this.radCheckBoxElement_BchOOO.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_BchOOO, false);
            this.radCheckBoxElement_BchOOO.Text = "พังงา";
            this.radCheckBoxElement_BchOOO.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_BchOOO_CheckStateChanged);
            // 
            // radCheckBoxElement_krabi
            // 
            this.radCheckBoxElement_krabi.Checked = false;
            this.radCheckBoxElement_krabi.Name = "radCheckBoxElement_krabi";
            this.radCheckBoxElement_krabi.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_krabi, false);
            this.radCheckBoxElement_krabi.Text = "กระบี่";
            this.radCheckBoxElement_krabi.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_krabi_CheckStateChanged);
            // 
            // radCheckBoxElement_Surat
            // 
            this.radCheckBoxElement_Surat.Checked = false;
            this.radCheckBoxElement_Surat.Name = "radCheckBoxElement_Surat";
            this.radCheckBoxElement_Surat.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_Surat, false);
            this.radCheckBoxElement_Surat.Text = "สุราษ";
            this.radCheckBoxElement_Surat.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_Surat_CheckStateChanged);
            // 
            // radCheckBoxElement_Samui
            // 
            this.radCheckBoxElement_Samui.Checked = false;
            this.radCheckBoxElement_Samui.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBoxElement_Samui.Name = "radCheckBoxElement_Samui";
            this.radCheckBoxElement_Samui.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_Samui, false);
            this.radCheckBoxElement_Samui.Text = "เกาะสมุย";
            this.radCheckBoxElement_Samui.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_Samui_CheckStateChanged);
            // 
            // radCheckBoxElement_Nakorn
            // 
            this.radCheckBoxElement_Nakorn.Checked = false;
            this.radCheckBoxElement_Nakorn.Name = "radCheckBoxElement_Nakorn";
            this.radCheckBoxElement_Nakorn.ReadOnly = false;
            this.radStatusStrip_Main.SetSpring(this.radCheckBoxElement_Nakorn, false);
            this.radCheckBoxElement_Nakorn.Text = "นครศรี";
            this.radCheckBoxElement_Nakorn.CheckStateChanged += new System.EventHandler(this.RadCheckBoxElement_Nakorn_CheckStateChanged);
            // 
            // commandBarSeparator12
            // 
            this.commandBarSeparator12.Name = "commandBarSeparator12";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator12, false);
            this.commandBarSeparator12.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator10
            // 
            this.commandBarSeparator10.Name = "commandBarSeparator10";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator10, false);
            this.commandBarSeparator10.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip_Main.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator11
            // 
            this.commandBarSeparator11.Name = "commandBarSeparator11";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator11, false);
            this.commandBarSeparator11.VisibleInOverflowMenu = false;
            // 
            // timer_Auto
            // 
            this.timer_Auto.Enabled = true;
            this.timer_Auto.Tick += new System.EventHandler(this.Timer_Auto_Tick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Show, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip_Main, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(906, 633);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.Text = "";
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator7, false);
            this.commandBarSeparator7.Text = "";
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator9
            // 
            this.commandBarSeparator9.Name = "commandBarSeparator9";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator9, false);
            this.commandBarSeparator9.Text = "";
            this.commandBarSeparator9.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator13
            // 
            this.commandBarSeparator13.Name = "commandBarSeparator13";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator13, false);
            this.commandBarSeparator13.Text = "";
            this.commandBarSeparator13.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator14
            // 
            this.commandBarSeparator14.Name = "commandBarSeparator14";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator14, false);
            this.commandBarSeparator14.Text = "";
            this.commandBarSeparator14.VisibleInOverflowMenu = false;
            // 
            // JOBCOM_Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(906, 633);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JOBCOM_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JOB";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.JOBCOM_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Main)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Main;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadRadioButtonElement radRadioButtonElement_Open;
        private Telerik.WinControls.UI.RadRadioButtonElement radRadioButtonElement_ALL;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.Timer timer_Auto;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_PO;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_Out;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_Bch;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_BchOOO;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator8;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Print;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator11;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_Samui;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator12;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_krabi;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_Surat;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement_Nakorn;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator9;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator14;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator13;
    }
}
