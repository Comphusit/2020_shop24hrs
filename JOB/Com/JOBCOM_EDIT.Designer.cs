﻿namespace PC_Shop24Hrs.JOB.Com
{
    partial class JOBCOM_EDIT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JOBCOM_EDIT));
            this.radGroupBox_Desc = new Telerik.WinControls.UI.RadGroupBox();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radButton_openMap = new Telerik.WinControls.UI.RadButton();
            this.radButton_OpenView = new Telerik.WinControls.UI.RadButton();
            this.radButton_openLink = new Telerik.WinControls.UI.RadButton();
            this.radButton_OpenAdd = new Telerik.WinControls.UI.RadButton();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Tecnical = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Update = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_BranchID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_BranchName = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_SN = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_ip = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_branch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_GroupSUB = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_STAOUT = new System.Windows.Forms.CheckBox();
            this.radCheckBox_STACLOSE = new System.Windows.Forms.CheckBox();
            this.radLabel_tel = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox_Bill = new Telerik.WinControls.UI.RadGroupBox();
            this.button_MNRS = new System.Windows.Forms.Button();
            this.radGridView_Send = new Telerik.WinControls.UI.RadGridView();
            this.Button_MNOT = new System.Windows.Forms.Button();
            this.Button_MNIO = new System.Windows.Forms.Button();
            this.Button_AX = new System.Windows.Forms.Button();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radButton_Add = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Empl = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).BeginInit();
            this.radGroupBox_Desc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_openMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_openLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Tecnical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BranchID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupSUB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Bill)).BeginInit();
            this.radGroupBox_Bill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_Desc
            // 
            this.radGroupBox_Desc.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radGroupBox_Desc.Controls.Add(this.radButton_pdt);
            this.radGroupBox_Desc.Controls.Add(this.radButton_openMap);
            this.radGroupBox_Desc.Controls.Add(this.radButton_OpenView);
            this.radGroupBox_Desc.Controls.Add(this.radButton_openLink);
            this.radGroupBox_Desc.Controls.Add(this.radButton_OpenAdd);
            this.radGroupBox_Desc.Controls.Add(this.radLabel6);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Tecnical);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Update);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_BranchID);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_BranchName);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_SN);
            this.radGroupBox_Desc.Controls.Add(this.radButton_Save);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_ip);
            this.radGroupBox_Desc.Controls.Add(this.radLabel3);
            this.radGroupBox_Desc.Controls.Add(this.radLabel_branch);
            this.radGroupBox_Desc.Controls.Add(this.radLabel2);
            this.radGroupBox_Desc.Controls.Add(this.radDropDownList_GroupSUB);
            this.radGroupBox_Desc.Controls.Add(this.radLabel1);
            this.radGroupBox_Desc.Controls.Add(this.radTextBox_Desc);
            this.radGroupBox_Desc.Controls.Add(this.radCheckBox_STAOUT);
            this.radGroupBox_Desc.Controls.Add(this.radCheckBox_STACLOSE);
            this.radGroupBox_Desc.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Desc.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล";
            this.radGroupBox_Desc.Location = new System.Drawing.Point(3, 4);
            this.radGroupBox_Desc.Name = "radGroupBox_Desc";
            this.radGroupBox_Desc.Size = new System.Drawing.Size(551, 664);
            this.radGroupBox_Desc.TabIndex = 26;
            this.radGroupBox_Desc.Text = "รายละเอียดข้อมูล";
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(509, 25);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 72;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radButton_openMap
            // 
            this.radButton_openMap.BackColor = System.Drawing.Color.DarkSalmon;
            this.radButton_openMap.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_openMap.Image = global::PC_Shop24Hrs.Properties.Resources.settings;
            this.radButton_openMap.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_openMap.Location = new System.Drawing.Point(511, 62);
            this.radButton_openMap.Name = "radButton_openMap";
            // 
            // 
            // 
            this.radButton_openMap.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_openMap.Size = new System.Drawing.Size(24, 24);
            this.radButton_openMap.TabIndex = 50;
            this.radButton_openMap.Click += new System.EventHandler(this.RadButton_openMap_Click);
            // 
            // radButton_OpenView
            // 
            this.radButton_OpenView.BackColor = System.Drawing.Color.Transparent;
            this.radButton_OpenView.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_OpenView.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.radButton_OpenView.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_OpenView.Location = new System.Drawing.Point(512, 166);
            this.radButton_OpenView.Name = "radButton_OpenView";
            // 
            // 
            // 
            this.radButton_OpenView.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_OpenView.Size = new System.Drawing.Size(26, 26);
            this.radButton_OpenView.TabIndex = 49;
            this.radButton_OpenView.Click += new System.EventHandler(this.RadButton_OpenView_Click);
            // 
            // radButton_openLink
            // 
            this.radButton_openLink.BackColor = System.Drawing.Color.Transparent;
            this.radButton_openLink.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_openLink.Image = global::PC_Shop24Hrs.Properties.Resources.link;
            this.radButton_openLink.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_openLink.Location = new System.Drawing.Point(413, 120);
            this.radButton_openLink.Name = "radButton_openLink";
            // 
            // 
            // 
            this.radButton_openLink.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_openLink.Size = new System.Drawing.Size(24, 24);
            this.radButton_openLink.TabIndex = 48;
            this.radButton_openLink.Click += new System.EventHandler(this.RadButton_openLink_Click);
            // 
            // radButton_OpenAdd
            // 
            this.radButton_OpenAdd.BackColor = System.Drawing.Color.Transparent;
            this.radButton_OpenAdd.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_OpenAdd.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_OpenAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_OpenAdd.Location = new System.Drawing.Point(482, 166);
            this.radButton_OpenAdd.Name = "radButton_OpenAdd";
            // 
            // 
            // 
            this.radButton_OpenAdd.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_OpenAdd.Size = new System.Drawing.Size(26, 26);
            this.radButton_OpenAdd.TabIndex = 47;
            this.radButton_OpenAdd.Click += new System.EventHandler(this.RadButton_OpenAdd_Click);
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(482, 141);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(53, 23);
            this.radLabel6.TabIndex = 44;
            this.radLabel6.Text = "รูป";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radTextBox_Tecnical
            // 
            this.radTextBox_Tecnical.AutoSize = false;
            this.radTextBox_Tecnical.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Tecnical.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Tecnical.Location = new System.Drawing.Point(90, 148);
            this.radTextBox_Tecnical.Name = "radTextBox_Tecnical";
            this.radTextBox_Tecnical.Size = new System.Drawing.Size(385, 23);
            this.radTextBox_Tecnical.TabIndex = 34;
            this.radTextBox_Tecnical.Text = "-";
            // 
            // radTextBox_Update
            // 
            this.radTextBox_Update.AcceptsReturn = true;
            this.radTextBox_Update.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Update.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Update.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Update.Location = new System.Drawing.Point(9, 510);
            this.radTextBox_Update.Multiline = true;
            this.radTextBox_Update.Name = "radTextBox_Update";
            // 
            // 
            // 
            this.radTextBox_Update.RootElement.StretchVertically = true;
            this.radTextBox_Update.Size = new System.Drawing.Size(532, 109);
            this.radTextBox_Update.TabIndex = 0;
            // 
            // radTextBox_BranchID
            // 
            this.radTextBox_BranchID.Enabled = false;
            this.radTextBox_BranchID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_BranchID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_BranchID.Location = new System.Drawing.Point(91, 31);
            this.radTextBox_BranchID.MaxLength = 3;
            this.radTextBox_BranchID.Name = "radTextBox_BranchID";
            this.radTextBox_BranchID.Size = new System.Drawing.Size(69, 21);
            this.radTextBox_BranchID.TabIndex = 3;
            this.radTextBox_BranchID.Text = "012";
            // 
            // radLabel_BranchName
            // 
            this.radLabel_BranchName.AutoSize = false;
            this.radLabel_BranchName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_BranchName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BranchName.Location = new System.Drawing.Point(163, 31);
            this.radLabel_BranchName.Name = "radLabel_BranchName";
            this.radLabel_BranchName.Size = new System.Drawing.Size(342, 23);
            this.radLabel_BranchName.TabIndex = 18;
            this.radLabel_BranchName.Text = "สาขา";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(255, 624);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_SN
            // 
            this.radTextBox_SN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_SN.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SN.Location = new System.Drawing.Point(91, 121);
            this.radTextBox_SN.Name = "radTextBox_SN";
            this.radTextBox_SN.Size = new System.Drawing.Size(318, 21);
            this.radTextBox_SN.TabIndex = 5;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(157, 624);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 1;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radTextBox_ip
            // 
            this.radTextBox_ip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_ip.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ip.Location = new System.Drawing.Point(91, 91);
            this.radTextBox_ip.Name = "radTextBox_ip";
            this.radTextBox_ip.Size = new System.Drawing.Size(318, 21);
            this.radTextBox_ip.TabIndex = 6;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(19, 123);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(55, 19);
            this.radLabel3.TabIndex = 22;
            this.radLabel3.Text = "ทะเบียน";
            // 
            // radLabel_branch
            // 
            this.radLabel_branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_branch.Location = new System.Drawing.Point(19, 32);
            this.radLabel_branch.Name = "radLabel_branch";
            this.radLabel_branch.Size = new System.Drawing.Size(39, 19);
            this.radLabel_branch.TabIndex = 17;
            this.radLabel_branch.Text = "สาขา";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(19, 93);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 19);
            this.radLabel2.TabIndex = 21;
            this.radLabel2.Text = "อุปกรณ์";
            // 
            // radDropDownList_GroupSUB
            // 
            this.radDropDownList_GroupSUB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_GroupSUB.BackColor = System.Drawing.Color.White;
            this.radDropDownList_GroupSUB.DropDownAnimationEnabled = false;
            this.radDropDownList_GroupSUB.DropDownHeight = 124;
            this.radDropDownList_GroupSUB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_GroupSUB.Location = new System.Drawing.Point(91, 60);
            this.radDropDownList_GroupSUB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_GroupSUB.Name = "radDropDownList_GroupSUB";
            // 
            // 
            // 
            this.radDropDownList_GroupSUB.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_GroupSUB.Size = new System.Drawing.Size(318, 25);
            this.radDropDownList_GroupSUB.TabIndex = 4;
            this.radDropDownList_GroupSUB.Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_GroupSUB.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupSUB.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(19, 62);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(53, 19);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "ประเภท";
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Black;
            this.radTextBox_Desc.Location = new System.Drawing.Point(9, 198);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            this.radTextBox_Desc.ReadOnly = true;
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Desc.Size = new System.Drawing.Size(532, 284);
            this.radTextBox_Desc.TabIndex = 8;
            // 
            // radCheckBox_STAOUT
            // 
            this.radCheckBox_STAOUT.AutoSize = true;
            this.radCheckBox_STAOUT.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radCheckBox_STAOUT.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_STAOUT.Location = new System.Drawing.Point(21, 173);
            this.radCheckBox_STAOUT.Name = "radCheckBox_STAOUT";
            this.radCheckBox_STAOUT.Size = new System.Drawing.Size(93, 20);
            this.radCheckBox_STAOUT.TabIndex = 32;
            this.radCheckBox_STAOUT.Text = "รอออกนอก";
            this.radCheckBox_STAOUT.UseVisualStyleBackColor = true;
            this.radCheckBox_STAOUT.CheckedChanged += new System.EventHandler(this.RadCheckBox_STAOUT_CheckedChanged);
            // 
            // radCheckBox_STACLOSE
            // 
            this.radCheckBox_STACLOSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radCheckBox_STACLOSE.AutoSize = true;
            this.radCheckBox_STACLOSE.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_STACLOSE.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_STACLOSE.Location = new System.Drawing.Point(20, 486);
            this.radCheckBox_STACLOSE.Name = "radCheckBox_STACLOSE";
            this.radCheckBox_STACLOSE.Size = new System.Drawing.Size(45, 20);
            this.radCheckBox_STACLOSE.TabIndex = 33;
            this.radCheckBox_STACLOSE.Text = "ปิด";
            this.radCheckBox_STACLOSE.UseVisualStyleBackColor = true;
            this.radCheckBox_STACLOSE.CheckedChanged += new System.EventHandler(this.RadCheckBox_STACLOSE_CheckedChanged);
            // 
            // radLabel_tel
            // 
            this.radLabel_tel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_tel.AutoSize = false;
            this.radLabel_tel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_tel.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_tel.Location = new System.Drawing.Point(316, 29);
            this.radLabel_tel.Name = "radLabel_tel";
            this.radLabel_tel.Size = new System.Drawing.Size(318, 23);
            this.radLabel_tel.TabIndex = 30;
            this.radLabel_tel.Text = "Tel 1818-1819";
            this.radLabel_tel.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radGroupBox_Bill
            // 
            this.radGroupBox_Bill.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Bill.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_Bill.Controls.Add(this.RadButton_Empl);
            this.radGroupBox_Bill.Controls.Add(this.button_MNRS);
            this.radGroupBox_Bill.Controls.Add(this.radGridView_Send);
            this.radGroupBox_Bill.Controls.Add(this.Button_MNOT);
            this.radGroupBox_Bill.Controls.Add(this.Button_MNIO);
            this.radGroupBox_Bill.Controls.Add(this.Button_AX);
            this.radGroupBox_Bill.Controls.Add(this.radGridView_Show);
            this.radGroupBox_Bill.Controls.Add(this.radLabel_tel);
            this.radGroupBox_Bill.Controls.Add(this.radButton_Add);
            this.radGroupBox_Bill.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.radGroupBox_Bill.ForeColor = System.Drawing.Color.Gray;
            this.radGroupBox_Bill.HeaderText = "รายละเอียด บิล";
            this.radGroupBox_Bill.Location = new System.Drawing.Point(560, 4);
            this.radGroupBox_Bill.Name = "radGroupBox_Bill";
            this.radGroupBox_Bill.Size = new System.Drawing.Size(644, 664);
            this.radGroupBox_Bill.TabIndex = 29;
            this.radGroupBox_Bill.Text = "รายละเอียด บิล";
            // 
            // button_MNRS
            // 
            this.button_MNRS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.button_MNRS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_MNRS.ForeColor = System.Drawing.Color.Black;
            this.button_MNRS.Location = new System.Drawing.Point(472, 203);
            this.button_MNRS.Name = "button_MNRS";
            this.button_MNRS.Size = new System.Drawing.Size(150, 33);
            this.button_MNRS.TabIndex = 31;
            this.button_MNRS.Text = "MNRS นำของออก";
            this.button_MNRS.UseVisualStyleBackColor = false;
            this.button_MNRS.Click += new System.EventHandler(this.Button_MNRS_Click);
            // 
            // radGridView_Send
            // 
            this.radGridView_Send.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Send.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Send.Location = new System.Drawing.Point(2, 60);
            // 
            // 
            // 
            this.radGridView_Send.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Send.Name = "radGridView_Send";
            // 
            // 
            // 
            this.radGridView_Send.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Send.Size = new System.Drawing.Size(640, 140);
            this.radGridView_Send.TabIndex = 19;
            this.radGridView_Send.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Send_ViewCellFormatting);
            this.radGridView_Send.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Send_CellDoubleClick);
            // 
            // Button_MNOT
            // 
            this.Button_MNOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.Button_MNOT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_MNOT.ForeColor = System.Drawing.Color.Black;
            this.Button_MNOT.Location = new System.Drawing.Point(316, 203);
            this.Button_MNOT.Name = "Button_MNOT";
            this.Button_MNOT.Size = new System.Drawing.Size(150, 33);
            this.Button_MNOT.TabIndex = 18;
            this.Button_MNOT.Text = "MNOT เครื่องมือ";
            this.Button_MNOT.UseVisualStyleBackColor = false;
            this.Button_MNOT.Click += new System.EventHandler(this.Button_MNOT_Click);
            // 
            // Button_MNIO
            // 
            this.Button_MNIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            this.Button_MNIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_MNIO.Font = new System.Drawing.Font("Tahoma", 11.75F);
            this.Button_MNIO.ForeColor = System.Drawing.Color.Black;
            this.Button_MNIO.Location = new System.Drawing.Point(161, 203);
            this.Button_MNIO.Name = "Button_MNIO";
            this.Button_MNIO.Size = new System.Drawing.Size(150, 33);
            this.Button_MNIO.TabIndex = 17;
            this.Button_MNIO.Text = "MNIO นำของออก";
            this.Button_MNIO.UseVisualStyleBackColor = false;
            this.Button_MNIO.Click += new System.EventHandler(this.Button_MNIO_Click);
            // 
            // Button_AX
            // 
            this.Button_AX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.Button_AX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_AX.ForeColor = System.Drawing.Color.Black;
            this.Button_AX.Location = new System.Drawing.Point(5, 203);
            this.Button_AX.Name = "Button_AX";
            this.Button_AX.Size = new System.Drawing.Size(150, 33);
            this.Button_AX.TabIndex = 16;
            this.Button_AX.Text = "AX";
            this.Button_AX.UseVisualStyleBackColor = false;
            this.Button_AX.Click += new System.EventHandler(this.Button_AX_Click);
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(2, 239);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(640, 423);
            this.radGridView_Show.TabIndex = 15;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            // 
            // radButton_Add
            // 
            this.radButton_Add.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Add.Location = new System.Drawing.Point(5, 25);
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.Size = new System.Drawing.Size(150, 33);
            this.radButton_Add.TabIndex = 11;
            this.radButton_Add.Text = "ส่งซ่อม";
            this.radButton_Add.ThemeName = "Fluent";
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).Text = "ส่งซ่อม";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Add.GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Add.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Empl
            // 
            this.RadButton_Empl.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadButton_Empl.Location = new System.Drawing.Point(161, 25);
            this.RadButton_Empl.Name = "RadButton_Empl";
            this.RadButton_Empl.Size = new System.Drawing.Size(150, 33);
            this.RadButton_Empl.TabIndex = 32;
            this.RadButton_Empl.Text = "พนง ประจำกะ";
            this.RadButton_Empl.ThemeName = "Fluent";
            this.RadButton_Empl.Click += new System.EventHandler(this.RadButton_Empl_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Empl.GetChildAt(0))).Text = "พนง ประจำกะ";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Empl.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Empl.GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Empl.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(18)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Empl.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(18)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Empl.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(18)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Empl.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(156)))), ((int)(((byte)(18)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Empl.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Empl.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // JOBCOM_EDIT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(1206, 675);
            this.Controls.Add(this.radGroupBox_Bill);
            this.Controls.Add(this.radGroupBox_Desc);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JOBCOM_EDIT";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "แก้ไขข้อมูล.";
            this.Load += new System.EventHandler(this.JOBCOM_EDIT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Desc)).EndInit();
            this.radGroupBox_Desc.ResumeLayout(false);
            this.radGroupBox_Desc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_openMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_openLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Tecnical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BranchID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupSUB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Bill)).EndInit();
            this.radGroupBox_Bill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Send)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_BranchID;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchName;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SN;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ip;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_branch;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_GroupSUB;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Update;
        private Telerik.WinControls.UI.RadLabel radLabel_tel;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Bill;
        protected Telerik.WinControls.UI.RadButton radButton_Add;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        protected System.Windows.Forms.CheckBox radCheckBox_STAOUT;
        protected System.Windows.Forms.CheckBox radCheckBox_STACLOSE;
        private Telerik.WinControls.UI.RadLabel radTextBox_Tecnical;
        private System.Windows.Forms.Button Button_AX;
        private System.Windows.Forms.Button Button_MNIO;
        private System.Windows.Forms.Button Button_MNOT;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadButton radButton_OpenAdd;
        private Telerik.WinControls.UI.RadButton radButton_openLink;
        private Telerik.WinControls.UI.RadButton radButton_OpenView;
        private Telerik.WinControls.UI.RadGridView radGridView_Send;
        private Telerik.WinControls.UI.RadButton radButton_openMap;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
        private System.Windows.Forms.Button button_MNRS;
        protected Telerik.WinControls.UI.RadButton RadButton_Empl;
    }
}
