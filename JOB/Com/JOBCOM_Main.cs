﻿//CheckOK
using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
 
namespace PC_Shop24Hrs.JOB.Com
{
    public partial class JOBCOM_Main : RadForm
    {
        readonly string _pGROUPID;//หมวด 00001 ComMN,00002 Comervice
        readonly string _pGROUP_DESC; //รายละเอียดของหมวด
        readonly string _pType;// SHOP-SUPC
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปืดจ๊อบ //1 มีสิทธิ์ปิด
        readonly string _pFixBch;//กรณีที่เปิดจากสาขาให้ระบุ BCH เพราะจะได้ดูเฉพาะสาขาเองเท่านั้น

        readonly string tbl; //ตารางที่ Insert Update Delete
        readonly string billFirst; //เลขที่บิลขึ้นต้น

        private DateTime timeForRefresh;
        private void JOBCOM_Main_Load(object sender, EventArgs e)
        {
            this.Text = _pType + " - " + _pGROUP_DESC;
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "ExportExcel";
            radButtonElement_add.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;
            radRadioButtonElement_Open.ShowBorder = true;
            radRadioButtonElement_ALL.ShowBorder = true;
            radCheckBoxElement_PO.ShowBorder = true;
            radButtonElement_Print.ShowBorder = true;
            radCheckBoxElement_Out.ShowBorder = true;
            radCheckBoxElement_Bch.ShowBorder = true;
            radCheckBoxElement_BchOOO.ShowBorder = true;
            RadButtonElement_pdt.ShowBorder = true;
            radCheckBoxElement_Samui.ShowBorder = true;
            radCheckBoxElement_krabi.ShowBorder = true;
            radCheckBoxElement_Surat.ShowBorder = true;
            radCheckBoxElement_Nakorn.ShowBorder = true;
            radCheckBoxElement_Surat.Font = SystemClass.SetFontGernaral;
            radCheckBoxElement_krabi.Font = SystemClass.SetFontGernaral;
            radCheckBoxElement_Nakorn.Font = SystemClass.SetFontGernaral;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Print.ToolTipText = "สร้างใบออกนอก";
        }
        //Main Load
        public JOBCOM_Main(string pType, string pGROUPID, string pGROUP_DESC, string pPermission, string pFixBch) // SHOP-SUPC
        {
            InitializeComponent();

            _pType = pType;
            _pGROUPID = pGROUPID;
            _pGROUP_DESC = pGROUP_DESC;
            _pPerminssion = pPermission;
            _pFixBch = pFixBch;

            DataTable dtGroup = JOB_Class.GetJOB_Group(_pGROUPID);
            tbl = dtGroup.Rows[0]["TABLENAME"].ToString();
            billFirst = dtGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString();

            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            this.Text = "JOB SHOP";
            string pNameID = "สาขา";
            string pName = "ชื่อสาขา";
            radCheckBoxElement_PO.Enabled = false;
            if (_pType == "SUPC")
            {
                pNameID = "แผนก";
                pName = "ชื่อแผนก";
                radCheckBoxElement_PO.Enabled = true;
                radCheckBoxElement_Samui.Enabled = false;
                radCheckBoxElement_krabi.Enabled = false;
                radCheckBoxElement_Surat.Enabled = false;
                radCheckBoxElement_Nakorn.Enabled = false;
                radCheckBoxElement_Bch.Enabled = false;
                radCheckBoxElement_BchOOO.Enabled = false;
                this.Text = "JOB SUPC";
            }

            if (pGROUPID != "00001")
            {
                radCheckBoxElement_PO.Enabled = false;
            }

            timeForRefresh = DateTime.Now.AddMinutes(3); timer_Auto.Start();

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("CC", "/", 30));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATE", "S", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMDATEUPD", "Up", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_Number", "เลขที่ JOB", 135));

            if (_pType == "SUPC")
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_BRANCHOUTPHUKET", "อต"));
            }
            else
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("JOB_BRANCHOUTPHUKET", "อต", 50));
            }
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("JOB_STAOUT", "อน", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchID(pNameID));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_BranchName(pName));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SHORTNAMEINS", "ผู้เปิด", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ประเภท", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_IP", "อุปกรณ์", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_SN", "ทะเบียน", 150));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 400));
            //radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION_UPD", "Last Update", 400));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION", "รายละเอียด", 600));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_STACLOSE", "สถานะจ๊อบ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_DATEINS", "วันที่"));

            //Freeze Column
            for (int i = 0; i < 8; i++)
            {
                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;
            }

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            DatagridClass.SetCellBackClolorByExpression("JOB_STAOUT", " JOB_STAOUT = '/' AND JOB_STACLOSE = '0' ", ConfigClass.SetColor_Red(), radGridView_Show);
            //ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "JOB_STAOUT = '/' AND JOB_STACLOSE = '0' ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //this.radGridView_Show.Columns["JOB_STAOUT"].ConditionalFormattingObjectList.Add(obj1);

            DatagridClass.SetCellBackClolorByExpression("BRANCH_ID", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_DarkPurplePastel(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " JOB_STACLOSE = 1 ", ConfigClass.SetColor_DarkPurplePastel(), radGridView_Show);
            //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "JOB_STACLOSE = 1 ", false)
            //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
            //this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);
            //this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj2);

            DatagridClass.SetCellBackClolorByExpression("JOB_BRANCHOUTPHUKET", " JOB_BRANCHOUTPHUKET = '/' AND JOB_STACLOSE = '0' ", ConfigClass.SetColor_PurplePastel(), radGridView_Show);
            //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition2", "JOB_BRANCHOUTPHUKET = '/' AND JOB_STACLOSE = '0' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PurplePastel() };
            //this.radGridView_Show.Columns["JOB_BRANCHOUTPHUKET"].ConditionalFormattingObjectList.Add(obj3);

            radGridView_Show.TableElement.SearchHighlightColor = Color.LightBlue;

            this.radGridView_Show.Columns["CC"].ReadOnly = false;

            SetDGV();
        }

        //find Data
        void SetDGV()
        {
            string sta_JobAll = "   '0' ";
            string pDate = "";
            if (radRadioButtonElement_Open.CheckState == CheckState.Unchecked) { sta_JobAll = "   '0','1' "; pDate = " AND YEAR(CONVERT(VARCHAR,JOB_DATEINS,23)) BETWEEN YEAR(GETDATE())-1 AND YEAR(GETDATE()) "; }

            string sql = $@" 
                        SELECT  '0' AS CC,JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOB_GROUPSUB,JOBGROUPSUB_DESCRIPTION, 
                                JOB_IP,JOB_SN, JOB_DESCRIPTION, JOB_DESCRIPTION_UPD,
                                CASE ISNULL(JOB_BRANCHOUTPHUKET, 'X') WHEN '0' THEN CONVERT(VARCHAR, 'X') ELSE CONVERT(VARCHAR, '/') END  AS JOB_BRANCHOUTPHUKET, JOB_STACLOSE,
                                CASE JOB_STAOUT WHEN '0' THEN CONVERT(VARCHAR, 'X') ELSE CONVERT(VARCHAR, '/') END AS JOB_STAOUT, JOB_SHORTNAMEINS,
                                DateDiff(Day, JOB_DATEINS, GETDATE()) AS SUMDATE,CONVERT(VARCHAR,JOB_DATEINS,23) AS JOB_DATEINS  ,
                                CASE CONVERT(VARCHAR,JOB_DATEUPD,23) WHEN '1900-01-01' THEN  DateDiff(Day,JOB_DATEINS,GETDATE())
						        ELSE ISNULL(DATEDIFF(DAY,JOB_DATEUPD,GETDATE()),0) END AS SUMDATEUPD  
                        FROM    {tbl} WITH(NOLOCK) 
                        WHERE   JOB_STACLOSE IN ({sta_JobAll}) 
                                AND JOB_TYPE = '{_pType}' ";

            if (_pFixBch != "")
            {
                sql += " AND JOB_BRANCHID = '" + _pFixBch + "' ";
            }

            if (radCheckBoxElement_PO.Checked == true)
            {
                sql += " AND JOB_GROUPSUB = '00013' ";
            }
            else
            {
                sql += " AND JOB_GROUPSUB != '00013' ";
            }
            if (radCheckBoxElement_Out.CheckState == CheckState.Checked)
            {
                sql += " AND JOB_STAOUT = '1' ";
            }


            string chekProvince = "";
            //phuket
            if (radCheckBoxElement_Bch.CheckState == CheckState.Checked)
            {
                if (chekProvince.Length > 0) chekProvince += @",'A01' "; else chekProvince = " 'A01' ";
            }
            //พังงา
            if (radCheckBoxElement_BchOOO.CheckState == CheckState.Checked)
            {
                if (chekProvince.Length > 0) chekProvince += @",'A02' "; else chekProvince = " 'A02' ";
            }
            //กระบี่
            if (radCheckBoxElement_krabi.CheckState == CheckState.Checked)
            {
                if (chekProvince.Length > 0) chekProvince += @",'A03' "; else chekProvince = " 'A03' ";
            }
            //สุราษ
            if (radCheckBoxElement_Surat.CheckState == CheckState.Checked)
            {
                if (chekProvince.Length > 0) chekProvince += @",'A04' "; else chekProvince = " 'A04' ";
            }
            //สมุย
            if (radCheckBoxElement_Samui.CheckState == CheckState.Checked)
            {
                if (chekProvince.Length > 0) chekProvince += @",'A05' "; else chekProvince = " 'A05' ";
            }
            //นคร
            if (radCheckBoxElement_Nakorn.CheckState == CheckState.Checked)
            {
                if (chekProvince.Length > 0) chekProvince += @",'A06' "; else chekProvince = " 'A06' ";
            }
            //check provice
            if (chekProvince.Length > 0) sql += " AND JOB_BRANCHID IN (SELECT	BRANCH_ID	FROM	SHOP_BRANCH WITH (NOLOCK)	WHERE	BRANCH_PROVINCE IN (" + chekProvince + @") )  ";


            sql += pDate;
            sql += "ORDER BY CONVERT(VARCHAR,JOB_DATEINS,25) DESC ";

            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            radGridView_Show.DataSource = dt;
        }

        //Open
        private void RadRadioButtonElement_Open_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            OpenFormEdit();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            if (_pType == "SHOP")
            {
                JOBCOM_SHOP_ADD frmJOB_ADD = new JOBCOM_SHOP_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC, _pFixBch);
                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK) SetDGV();
            }
            else
            {

                JOBCOM_SUPC_ADD frmJOB_ADD = new JOBCOM_SUPC_ADD(tbl, billFirst, _pGROUPID, _pGROUP_DESC);
                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK) SetDGV();
            }
        }
        //All
        private void RadRadioButtonElement_ALL_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }
        // SetFontInRadGridview
        #region "SetFontInRadGridview"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        //Open Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            OpenFormEdit();
        }
        //Open From
        void OpenFormEdit()
        {
            if ((radGridView_Show.CurrentRow.Index == -1) || (radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString() == ""))
            {
                return;
            }
            JOBCOM_EDIT frmJOB_Edit = new JOBCOM_EDIT(tbl, _pGROUPID, _pGROUP_DESC, _pPerminssion, _pType, radGridView_Show.CurrentRow.Cells["JOB_Number"].Value.ToString());
            if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK) SetDGV();
        }
        //Auto Refresh
        private void Timer_Auto_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > timeForRefresh)
            {
                timer_Auto.Stop();
                SetDGV();
                timeForRefresh = DateTime.Now.AddMinutes(3);
                timer_Auto.Start();
            }
        }
        //เฉพาะรายการสั่งของใช้งาน
        private void RadCheckBoxElement_PO_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadCheckBoxElement_Out_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadCheckBoxElement_Bch_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadCheckBoxElement_BchOOO_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }

            DataTable dtDesc = new DataTable();
            dtDesc.Columns.Add("BRANCH", typeof(string));
            //dtDesc.Columns.Add("IP", typeof(string));
            dtDesc.Columns.Add("DESC", typeof(string));
            dtDesc.Columns.Add("DATE", typeof(string));
            dtDesc.Columns.Add("JOB", typeof(string));

            int k = 0;
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["CC"].Value.ToString() == "1")
                {
                    string desc = radGridView_Show.Rows[i].Cells["JOB_DESCRIPTION"].Value.ToString();
                    string pDesc = desc.Substring(1, desc.IndexOf(">") - 1).Replace(Environment.NewLine, "");
                    k += 1;
                    string bchID = radGridView_Show.Rows[i].Cells["BRANCH_ID"].Value.ToString();
                    dtDesc.Rows.Add(bchID,
                          bchID + "-" + radGridView_Show.Rows[i].Cells["BRANCH_NAME"].Value.ToString() +
                          Environment.NewLine + radGridView_Show.Rows[i].Cells["JOBGROUPSUB_DESCRIPTION"].Value.ToString() +
                          Environment.NewLine + radGridView_Show.Rows[i].Cells["JOB_SN"].Value.ToString() +
                          Environment.NewLine + radGridView_Show.Rows[i].Cells["JOB_IP"].Value.ToString() + Environment.NewLine +
                          pDesc,
                        radGridView_Show.Rows[i].Cells["JOB_DATEINS"].Value.ToString(),
                        radGridView_Show.Rows[i].Cells["JOB_Number"].Value.ToString());
                }
            }

            if (k == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData(" ไม่ได้เลือกจ๊อบ ");
                return;
            }

            JOB_DescPrint _JOB_DescPrint = new JOB_DescPrint(dtDesc);
            if (_JOB_DescPrint.ShowDialog(this) == DialogResult.OK) return;
        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "CC":
                    if (radGridView_Show.CurrentRow.Cells["JOB_STAOUT"].Value.ToString() == "X")
                    { return; }

                    if (radGridView_Show.CurrentCell.Value.ToString() == "0") { radGridView_Show.CurrentCell.Value = "1"; }
                    else { radGridView_Show.CurrentCell.Value = "0"; }

                    break;
                default:
                    break;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("งานซ่อม " + _pGROUP_DESC, radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadCheckBoxElement_Samui_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadCheckBoxElement_krabi_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadCheckBoxElement_Surat_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadCheckBoxElement_Nakorn_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV();

        }
    }
}



