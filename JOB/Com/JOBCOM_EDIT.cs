﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowImage;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.GeneralForm.BillOut;
using PC_Shop24Hrs.GeneralForm.Employee;

namespace PC_Shop24Hrs.JOB.Com
{
    public partial class JOBCOM_EDIT : Telerik.WinControls.UI.RadForm
    {

        //public string PJOBNUMBER;
        //public string STAOUT;
        //public string PTYPE;

        //int GROUPSUB;
        //string pChange;         //0 ไม่มีการเปลี่ยนเเปลงฟอร์ม 1 มีการเปลี่ยนแปลงเพื่อ reshesh ข้อมูลในตาราง main
        readonly ComMinimart.Camera.DATACAM_IP dATACAM_IP = new ComMinimart.Camera.DATACAM_IP();
        //string pConfigDB;
        string pChangeSTAOUT = "0";
        string pBchID;//สาขา
        string pDate;//วันที่ นำไปใช้หารูปในจ๊อบ

        readonly string _tblName;// table
        readonly string _grpId;//บิลขึ้นต้น
        readonly string _grpName;
        readonly string _pPerminssion; //0 ไม่มีสิทธิ์ปืดจ๊อบ //1 มีสิทธิ์ปิด
        readonly string _pType;//SHOP-SUPC
        readonly string _pJobNumber;

        #region "Rows"      
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Send_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion

        //Set Permission
        void SetPermission()
        {
            radCheckBox_STAOUT.Enabled = false;
            radCheckBox_STACLOSE.Enabled = false;
            radButton_Add.Enabled = true;
            Button_AX.Enabled = false;
            Button_MNIO.Enabled = false;
            Button_MNOT.Enabled = false;
            button_MNRS.Enabled = false;
        }

        public JOBCOM_EDIT(string pTbl, string pGrpID, string pGrpName, string pPermission, string pType, String pJobNumber)
        {
            InitializeComponent();
            _tblName = pTbl;
            _grpId = pGrpID;
            _grpName = pGrpName;
            _pPerminssion = pPermission;
            _pType = pType;
            _pJobNumber = pJobNumber;
        }

        //Load
        private void JOBCOM_EDIT_Load(object sender, EventArgs e)
        {
            //pConfigDB = ConfigClass.GetConfigDB_FORMNAME(this.Name);
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Bill.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral;
            radGroupBox_Desc.HeaderText = "รายละเอียดข้อมูล : " + _pJobNumber;

            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_openLink.ButtonElement.ShowBorder = true;
            radButton_OpenAdd.ButtonElement.ShowBorder = true;
            radButton_OpenView.ButtonElement.ShowBorder = true;
            radButton_openLink.ButtonElement.ToolTipText = "เปิดกล้อง";
            radButton_OpenAdd.ButtonElement.ToolTipText = "แนบรูป";
            radButton_OpenView.ButtonElement.ToolTipText = "เปิดรูป";
            radButton_openMap.ButtonElement.ToolTipText = "เปิดแพลนสาขา";
            radButton_openMap.ButtonElement.ShowBorder = true;
            radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Empl.ButtonElement.ShowBorder = true; RadButton_Empl.ButtonElement.ToolTipText = "รายชื่อพนักงานที่อยู่สาขา";
            if (_pType == "SHOP") RadButton_Empl.Enabled = true; else RadButton_Empl.Enabled = false;
            
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Bill);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Desc);

            JOB_Class.SetHeadGrid_ForShowImage(radGridView_Show, "0");
            JOB_Class.SetHeadGrid_ForBillMNRZRB(radGridView_Send, _pType);

            SetData();

            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
            JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);

            radGridView_Show.ClearSelection();
            radGridView_Send.ClearSelection();

            if (_pType == "SHOP")
            {
                radButton_Add.Text = "สาขาส่ง";
            }
            else
            {
                radButton_Add.Text = "ส่งซ่อม";
            }

            this.Text = _grpName + " : " + _pType;
        }
        //GroupSub
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //SetData
        void SetData()
        {
            string sql = $@" SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOB_GROUPSUB,JOBGROUPSUB_DESCRIPTION,
                        JOB_SN,JOB_IP,JOB_DESCRIPTION,JOB_STACLOSE,JOB_STAOUT,JOB_SHORTNAMEINS,JOB_DESCRIPTION_CLOSE,CONVERT(varchar,JOB_DATEINS,23) AS DATE_INS,
                        JOB_STAIMG   
                        FROM	{ _tblName} WITH (NOLOCK)   WHERE JOB_Number = '{ _pJobNumber}' ";

            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);

            pBchID = dt.Rows[0]["BRANCH_ID"].ToString();
            radTextBox_BranchID.Text = dt.Rows[0]["BRANCH_ID"].ToString().Replace("MN", "").Replace("D", "");
            radLabel_BranchName.Text = dt.Rows[0]["BRANCH_NAME"].ToString();
            Set_GroupSub(); radDropDownList_GroupSUB.SelectedValue = dt.Rows[0]["JOB_GROUPSUB"].ToString();
            //GROUPSUB = radDropDownList_GroupSUB.SelectedIndex;
            radTextBox_ip.Text = dt.Rows[0]["JOB_IP"].ToString();
            radTextBox_SN.Text = dt.Rows[0]["JOB_SN"].ToString();
            radTextBox_Desc.Text = dt.Rows[0]["JOB_DESCRIPTION"].ToString();
            pDate = dt.Rows[0]["DATE_INS"].ToString();



            radButton_OpenView.Enabled = false;
            if (dt.Rows[0]["JOB_STAIMG"].ToString() == "1")
            {
                radButton_OpenView.Enabled = true;
            }

            if (_pType == "SHOP")
            {
                radTextBox_Tecnical.Text = JOB_Class.GetTechnicianl_ByBranchID(pBchID);
                radLabel_branch.Text = "สาขา";
            }
            else
            {
                radTextBox_Tecnical.Text = JOB_Class.GetTechnicianl_ByBranchID(radTextBox_SN.Text);
                radLabel_branch.Text = "แผนก";
            }

            radLabel_tel.Text = BranchClass.GetTel_ByBranchID(pBchID);

            if (dt.Rows[0]["JOB_STAOUT"].ToString() == "1")
            { radCheckBox_STAOUT.Checked = true; }
            else { radCheckBox_STAOUT.Checked = false; }

            if (dt.Rows[0]["JOB_GROUPSUB"].ToString() == "00003")
            {
                //radTextBox_SN.Enabled = false; radTextBox_ip.Enabled = false;
                radTextBox_SN.ReadOnly = true; radTextBox_ip.ReadOnly = true;
                radDropDownList_GroupSUB.Enabled = false; radButton_openLink.Enabled = true;
            }
            else { radButton_openLink.Enabled = false; }

            if (dt.Rows[0]["JOB_STACLOSE"].ToString() == "1")
            {
                radCheckBox_STACLOSE.Checked = true;
                radTextBox_Update.Text = dt.Rows[0]["JOB_DESCRIPTION_CLOSE"].ToString();
                radTextBox_Update.ReadOnly = true;
                radButton_Save.Enabled = false;

                radDropDownList_GroupSUB.Enabled = false;
                radTextBox_ip.Enabled = false;
                radTextBox_SN.Enabled = false;
                radCheckBox_STAOUT.Checked = false;
                radCheckBox_STACLOSE.Enabled = false;
                radCheckBox_STAOUT.Enabled = false;

                Button_MNIO.Enabled = false;
                Button_MNOT.Enabled = false;
                button_MNRS.Enabled = false;
                Button_AX.Enabled = false;
                radButton_Add.Enabled = false;
                radButton_Cancel.Focus();
                radTextBox_Update.SelectionStart = radTextBox_Update.Text.Length;
                radButton_OpenAdd.Enabled = false;
                radButton_openLink.Enabled = false;


            }
            else
            {
                radCheckBox_STACLOSE.Checked = false;
                radTextBox_Update.Enabled = true;
                radButton_Save.Enabled = true;

                radTextBox_Update.Focus();
            }

            if (_pPerminssion != "1")
            {
                SetPermission();
            }
            radTextBox_Desc.SelectionStart = radTextBox_Desc.Text.Length;
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (pChangeSTAOUT == "1")
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.No;
            }
            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_Update.Text == "")
            {
                if (radCheckBox_STACLOSE.Checked == true)
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการปิดจ๊อบ");
                    return;
                }
                string strUpdGroupSUB = $@"UPDATE {_tblName}  
                                           SET  [JOB_GROUPSUB] = '{radDropDownList_GroupSUB.SelectedValue}',
                                                [JOB_DATEUPD] = GETDATE(),   
                                                [JOB_SN] = '{radTextBox_SN.Text} ',    
                                                [JOB_IP] = '{radTextBox_ip.Text} '  
                                           WHERE [JOB_Number] = '{_pJobNumber} ' ";
                string TGroupSUB = ConnectionClass.ExecuteSQL_Main(strUpdGroupSUB);
                MsgBoxClass.MsgBoxShow_SaveStatus(TGroupSUB);
                if (TGroupSUB == "")
                {
                    //GROUPSUB = radDropDownList_GroupSUB.SelectedIndex;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
                else
                {
                    return;
                }
            }

            string strUser;
            if (SystemClass.SystemComMinimart == "1")
            {
                strUser = "-> BY " + SystemClass.SystemUserNameShort + " [ComMinimart] ";
            }
            else if (SystemClass.SystemDptID == "D179")
            {
                strUser = "-> BY " + SystemClass.SystemUserNameShort + " [ComService] ";
            }
            else
            {
                strUser = "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            }

            string desc = radTextBox_Desc.Text.Trim().Replace("'", "");
            string descClose = "-" + radTextBox_Update.Text + Environment.NewLine + strUser;
            string strUpd;
            if (radCheckBox_STACLOSE.Checked == true)//ปิดจีอบ
            {
                if (radTextBox_Update.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดการปิดจ๊อบ"); return;
                }
                strUpd = string.Format(@"UPDATE " + _tblName + " SET  [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'" +
                    ",[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "'" +
                    ",[JOB_IP] = '" + radTextBox_ip.Text.Trim() + "' " +
                    ",[JOB_DESCRIPTION_CLOSE] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'" +
                    ",[JOB_STACLOSE] = '1'" +
                    ",[JOB_WHOCLOSE] = '" + SystemClass.SystemUserID + "' " +
                    ",[JOB_NAMECLOSE] = '" + SystemClass.SystemUserName + "'" +
                    ",[JOB_DATECLOSE] = GETDATE()" +
                    ",[JOB_DATEUPD] = GETDATE()  " +
                    " WHERE[JOB_Number] = '" + _pJobNumber + "' ");
            }
            else
            {
                string staout = "0";
                if (radCheckBox_STAOUT.Checked == true)
                {
                    staout = "1";
                }
                desc = desc + Environment.NewLine + Environment.NewLine + descClose;
                strUpd = string.Format(@"UPDATE " + _tblName + "  SET	 [JOB_GROUPSUB] = '" + radDropDownList_GroupSUB.SelectedValue + "'" +
                    ",[JOB_SN] = '" + radTextBox_SN.Text.Trim() + "'" +
                    ",[JOB_IP] = '" + radTextBox_ip.Text.Trim() + "'" +
                    ",[JOB_DESCRIPTION] = '" + desc + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'" +
                    ",[JOB_DESCRIPTION_UPD] = '" + descClose + "' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']' " +
                    ",[JOB_DATEUPD] = GETDATE() " +
                    ",[JOB_STAOUT] = '" + staout + "'  " +
                    "WHERE[JOB_Number] = '" + _pJobNumber + "' ");
            }

            string T = ConnectionClass.ExecuteSQL_Main(strUpd);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        //check change
        private void RadCheckBox_STAOUT_CheckedChanged(object sender, EventArgs e)
        {
            string staout = "0";
            if (radCheckBox_STAOUT.Checked == true)
            {
                staout = "1";
            }


            string strUpd;
            strUpd = $@"UPDATE     {_tblName}
                        SET  [JOB_STAOUT] = '{staout}',
                             [JOB_DATEUPD] = GETDATE()
                        WHERE [JOB_Number] = '{_pJobNumber}' ";
            ConnectionClass.ExecuteSQL_Main(strUpd);
            pChangeSTAOUT = "1";
        }
        //OpenLink
        private void RadButton_openLink_Click(object sender, EventArgs e)
        {
            try
            {
                if (radTextBox_SN.Text.Trim() != "")
                {
                    //System.Diagnostics.Process.Start("iexplore.exe", radTextBox_SN.Text.ToString());
                    string pPath = PathImageClass.pPathJOB;
                    if (_tblName == "SHOP_JOBComService")
                    {
                        pPath = PathImageClass.pPathJOBComService;
                    }
                    pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
                    string pFileName = _pJobNumber + "-" + pBchID + "-";


                    DataTable dtCam = JOB_Class.GetCam_ByID(radTextBox_SN.Text.ToString());
                    if (dtCam.Rows.Count == 0) { return; }

                    dATACAM_IP.IPCam = dtCam.Rows[0]["CAM_IP"].ToString();
                    dATACAM_IP.Userpermis = "0";
                    dATACAM_IP.CamID = dtCam.Rows[0]["CAM_ID"].ToString();
                    dATACAM_IP.FormName = this.Name;
                    dATACAM_IP.JobNumber = _pJobNumber;
                    dATACAM_IP.CamCH = int.Parse(dtCam.Rows[0]["CAM_CH"].ToString());
                    //dATACAM_IP.Path = pPath;
                    //dATACAM_IP.FileName = pFileName;
                    dATACAM_IP.BranchID = dtCam.Rows[0]["BRANCH_ID"].ToString();
                    dATACAM_IP.CamDesc = dtCam.Rows[0]["CAM_DESC"].ToString(); //radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value.ToString();
                    //dATACAM_IP.BranchName = dtCam.Rows[0]["DESCRIPTION"].ToString();// radGridView_Show.CurrentRow.Cells["DESCRIPTION"].Value.ToString();
                    dATACAM_IP.CamDVR = dtCam.Rows[0]["CAM_DVR"].ToString();

                    ComMinimart.Camera.Camera_IEMain _cameraIE = new ComMinimart.Camera.Camera_IEMain(dATACAM_IP, "2",
                        dtCam.Rows[0]["SET_USER"].ToString(), dtCam.Rows[0]["SET_PASSWORD"].ToString())
                    {
                        pCAM_REMARKCHANNEL = "",
                        ptypeCamOpen = "JOB"
                    };
                    //D029.Camera.Camera_IE _cameraIE = new D029.Camera.Camera_IE(radTextBox_SN.Text.ToString(), "0", "", "", this.Name, _pJobNumber, pPath, pFileName);
                    if (_cameraIE.ShowDialog(this) == DialogResult.Yes)
                    {
                        // radButton_OpenView.Enabled = true;
                    }
                    radButton_OpenView.Enabled = true;
                    //else
                    //{
                    //    if (_cameraIE.pChange == "1")
                    //    {
                    //        radButton_OpenView.Enabled = true;
                    //    }
                    //}
                }
            }
            catch (Exception)
            {
                System.Diagnostics.Process.Start("iexplore.exe", radTextBox_SN.Text.ToString());
            }
        }
        //Close
        private void RadCheckBox_STACLOSE_CheckedChanged(object sender, EventArgs e)
        {
            if (radCheckBox_STACLOSE.Checked == true)
            {
                int cSpcType = 0;
                for (int iT = 0; iT < radGridView_Show.Rows.Count; iT++)
                {
                    if (radGridView_Show.Rows[iT].Cells["Bill_SpcType"].Value.ToString() == "X")
                    {
                        cSpcType += 1;
                    }
                }

                if (cSpcType == 0)
                {
                    radTextBox_Update.Focus();
                    return;
                }

                if (_pType == "SHOP")
                {
                    int a = 0;

                    DataTable dtOpen = BranchClass.GetDetailBranchByID(pBchID);
                    if (dtOpen.Rows[0]["BRANCH_STAOPEN"].ToString() == "0")
                    {
                        if ((MessageBox.Show("สำหรับสาขาที่ยังไม่เปิด สามารถปิดจ๊อบได้ โดยที่ไม่ต้องรับบิล" + Environment.NewLine +
                            "รบกวนตรวจสอบรูปภาพให้เรียบร้อยก่อนปิดจ๊อบ" + Environment.NewLine + "ยืนยันการปิดจ๊อบหรือไม่ ?",
                            SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
                        {
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                            return;
                        }
                        else
                        {
                            radTextBox_Update.Focus(); return;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < radGridView_Show.Rows.Count; i++)
                        {
                            if (radGridView_Show.Rows[i].Cells["Bill_TypeBchStatus"].Value.ToString() == "X")
                            {
                                a += 1;
                            }
                        }
                    }

                    if (a == 0)
                    {
                        radTextBox_Update.Focus();
                    }
                    else
                    {

                        if (SystemClass.SystemComProgrammer == "1")
                        {
                            if (ComfirmByProgrammer() == "1")
                            {
                                radTextBox_Update.Focus();
                            }
                            else
                            {
                                radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                                radTextBox_Update.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากสาขายังรับบิลไม่ครบทุกใบ" + Environment.NewLine + "จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                        }
                    }
                }
                else
                {
                    int aa = 0;
                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        if (radGridView_Show.Rows[ii].Cells["Bill_SPC_Count"].Value.ToString() == "0")
                        {
                            aa += 1;
                        }
                    }
                    if (aa == 0)
                    {
                        radTextBox_Update.Focus();
                    }
                    else
                    {
                        if (SystemClass.SystemComProgrammer == "1")
                        {
                            if (ComfirmByProgrammer() == "1")
                            {
                                radTextBox_Update.Focus();
                            }
                            else
                            {
                                radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                                radTextBox_Update.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("ไม่สามารถปิดจ๊อบได้เนื่องจากไม่มีรูปการถ่ายออก " + Environment.NewLine + " จัดการบิลให้เรียบร้อยก่อนดำเนินการอีกครั้ง.", SystemClass.SystemHeadprogram,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            radCheckBox_STACLOSE.CheckState = CheckState.Unchecked;
                            radTextBox_Update.Focus();
                        }
                    }
                }

            }
        }
        //Comfirm
        string ComfirmByProgrammer()
        {
            if ((MessageBox.Show("ยันยันการปิดจ๊อบทั้งที่บิลยังไม่เรียบร้อย ?.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.No)
            {
                return "0";
            }
            else
            {
                return "1";
            }
        }

        //DoubleClick
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Show.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    { }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD": //รูปสาขาส่งซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLD_Count"].Value.ToString(),
                           radGridView_Show.CurrentRow.Cells["Bill_OLD_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC": //รูปสาขาใหญ่รับซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                          radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    break;
            }

        }
        //MNIO
        private void Button_MNIO_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNIO", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText

            };
            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }

        //MNOT
        private void Button_MNOT_Click(object sender, EventArgs e)
        {
            Bill.Bill_MNIO_OT frm = new Bill.Bill_MNIO_OT("MNOT", _pType, _pJobNumber, _grpId, _grpName, "0")
            {
                pBchID = pBchID,
                pBchName = radLabel_BranchName.Text,
                pRmk = radDropDownList_GroupSUB.SelectedText
            };
            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //Import AX
        private void Button_AX_Click(object sender, EventArgs e)
        {
            Bill.BillAXAddJOB frm = new Bill.BillAXAddJOB(pBchID, radLabel_BranchName.Text, "0",
                _pJobNumber, _grpId, _grpName, radTextBox_SN.Text, radTextBox_ip.Text, "", "");
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);
        }
        //openImage
        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            string pPath = PathImageClass.pPathJOB;
            if (_tblName == "SHOP_JOBComService")
            {
                pPath = PathImageClass.pPathJOBComService;
            }
            pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            ShowImage frmSPC = new ShowImage()
            { pathImg = pPath + @"|*.jpg" };
            if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
        }
        //add Image
        private void RadButton_OpenAdd_Click(object sender, EventArgs e)
        {

            string pPath = PathImageClass.pPathJOB;
            if (_tblName == "SHOP_JOBComService")
            {
                pPath = PathImageClass.pPathJOBComService;
            }

            pPath = pPath + pDate.Substring(0, 7) + @"\" + pDate + @"\" + _pJobNumber + @"\";
            string pFileName = _pJobNumber + "-" + pBchID + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber, "1");
            frm.ShowDialog(this);
            if (frm.DialogResult == DialogResult.Yes)
            {
                radButton_OpenView.Enabled = true;
            }
            else if (frm.DialogResult == DialogResult.OK)
            {
                UploadImageSelectArea _UploadImageSelectArea = new UploadImageSelectArea("0", pPath, pFileName, _tblName, "JOB_STAIMG", "JOB_Number", _pJobNumber);
                _UploadImageSelectArea.Show();
                radButton_OpenView.Enabled = true;
            }
        }
        //ส่งของซอม
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            switch (_pType)
            {
                case "SUPC":
                    string pDesc = radTextBox_Desc.Text.Substring(1, radTextBox_Desc.Text.IndexOf("->") - 1).Replace(Environment.NewLine, "");
                    JOB.Bill.Bill_MNRZ frmMNRZ = new Bill.Bill_MNRZ(pBchID, radLabel_BranchName.Text, _pJobNumber, radLabel_tel.Text, pDesc);
                    frmMNRZ.ShowDialog(this);
                    if (frmMNRZ.pDialog == "1")
                    {
                        JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);
                    }
                    break;
                case "SHOP":
                    DataTable dtJOB = BillOut_Class.FindBill_ForAddJOB("'D179','D156'", pBchID);
                    if (dtJOB.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บิลทั้งหมด มี JOB เรียบร้อยแล้ว");
                        return;
                    }

                    JOB.Bill.BillMN_ForAddJOB frmAddJOb = new Bill.BillMN_ForAddJOB("'D156','D179'", pBchID, _pJobNumber, _grpId);
                    if (frmAddJOb.ShowDialog(this) == DialogResult.Yes)
                    {
                        JOB_Class.FindImageBillMNRZRB_ByJobID(BillOut_Class.FindAllBillMNRZRB_ByJobNumberID(_pJobNumber), radGridView_Send, _pType);
                    }

                    break;
                default:
                    break;
            }
        }
        //Open Detail Bill
        private void RadGridView_Send_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_ID":
                    BillOut_Detail frm = new BillOut_Detail(radGridView_Send.CurrentRow.Cells["Bill_ID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK)
                    {
                    }
                    break;
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Send.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Send.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //Open PlanMN
        private void RadButton_openMap_Click(object sender, EventArgs e)
        {
            ShowImage _showImage = new ShowImage
            {
                pathImg = PathImageClass.pPathPLANMN + pBchID + @"|*.jpg"
            };
            _showImage.StartPosition = FormStartPosition.CenterScreen;
            _showImage.WindowState = FormWindowState.Maximized;
            _showImage.Show();
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void Button_MNRS_Click(object sender, EventArgs e)
        {
            string dpt = "SHOP";
            if (pBchID.Contains("D"))
            {
                dpt = "SUPC";
            }
            GeneralForm.BillOut.MNRS_Main frm = new GeneralForm.BillOut.MNRS_Main(dpt, "COM-MN", pBchID, radLabel_BranchName.Text, _pJobNumber)
            {
                WindowState = FormWindowState.Normal
            };
            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
            JOB_Class.FindImageBillAll_ByJobID(BillOut_Class.FindAllBill_ByJobNumberID(_pJobNumber), radGridView_Show);

        }
        //พนักงานที่อยู่ที่สาขา
        private void RadButton_Empl_Click(object sender, EventArgs e)
        {
            Employee_Report d054_Report_Employee = new Employee_Report("1", "SHOP", "MN" + radTextBox_BranchID.Text, DateTime.Now)
            {
                WindowState = FormWindowState.Maximized
            };
            d054_Report_Employee.ShowDialog();
        }
    }
}
