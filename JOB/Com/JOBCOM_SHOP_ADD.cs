﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.JOB.Com
{
    public partial class JOBCOM_SHOP_ADD : Telerik.WinControls.UI.RadForm
    {

        string bchId;

        readonly string _tblName; //ชื่อตาราง
        readonly string _billFirst; //เลขที่บิล
        readonly string _grpId;//หมวดการใช้
        readonly string _grpDesc;//รายละเอียด
        readonly string _pFixBch;// fix Bch
        readonly string _radTextBox_ip;
        readonly string _radTextBox_SN;

        public JOBCOM_SHOP_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc, string pFixBch, string radTextBox_ip = "", string radTextBox_SN = "")
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _pFixBch = pFixBch;
            _radTextBox_ip = radTextBox_ip;
            _radTextBox_SN = radTextBox_SN;
        }
        //กลุ่ม
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //Clear
        void Set_ClearData()
        {
            Set_GroupSub();
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            radTextBox_BranchID.Text = "";
            radTextBox_BranchID.Focus();
            radTextBox_SN.Text = "";
            radTextBox_ip.Text = "";
            radTextBox_Desc.Text = "";
            radLabel_BranchName.Text = "";
        }
        //load
        private void JOBCOM_SHOP_ADD_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            Set_ClearData();
            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SHOP";

            if (_pFixBch != "")
            {
                radTextBox_BranchID.Text = _pFixBch.Replace("MN", "");// SystemClass.SystemBranchID.Substring(2, 3);
                SetMN(radTextBox_BranchID.Text.Trim());
                radTextBox_ip.Text = _radTextBox_ip;
                radTextBox_SN.Text = _radTextBox_SN;
                radTextBox_BranchID.Enabled = false;
                if (radTextBox_SN.Text == "")
                {
                    radTextBox_SN.Focus();
                }
                else
                {
                    radTextBox_Desc.Focus();
                }
                if (_radTextBox_SN.Contains("http") == true) radDropDownList_GroupSUB.SelectedValue = "00003";

            }
        }
        //Enter Bch
        private void RadTextBox_BranchID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetMN(radTextBox_BranchID.Text.Trim());
            }
        }
        //Set Focus
        private void RadTextBox_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Desc.Focus();
            }
        }
        //Set Focus
        private void RadTextBox_ip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_SN.Focus();
            }
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (bchId == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("สาขา");
                radTextBox_BranchID.Focus();
                return;
            }

            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดงานที่ต้องการซ่อม");
                radTextBox_Desc.Focus();
                return;
            }

            string descJOB;
            descJOB = "- " + radTextBox_Desc.Text.Trim().Replace("'", "");
            if (SystemClass.SystemComMinimart == "1")
            {
                descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComMinimart] ";
            }
            else if ((SystemClass.SystemDptID == "D179"))
            {
                descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComService] ";
            }
            else
            {
                descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            }

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = String.Format(@"
                            INSERT INTO " + _tblName + @"([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],[JOB_GROUPSUB],[JOB_WHOINS],[JOB_NAMEINS],
                            [JOB_SHORTNAMEINS],[JOB_DESCRIPTION],[JOB_SN],[JOB_IP],[JOB_DATEINS])  
                            VALUES('" + billMaxNO + @"', '" + bchId + @"', '" + radLabel_BranchName.Text + @"', '" + radDropDownList_GroupSUB.SelectedValue + @"', 
                            '" + SystemClass.SystemUserID + @"', '" + SystemClass.SystemUserName + "', '" + SystemClass.SystemUserNameShort + @"',  
                            '" + descJOB + @"' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']', '" + radTextBox_SN.Text.Trim() + @"', 
                            '" + radTextBox_ip.Text.Trim() + @"', GETDATE()) "
                            );
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);

            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        //check Length
        private void RadTextBox_BranchID_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_BranchID.Text.ToString().Length == 3)
            {
                if (radTextBox_BranchID.Text.ToString() != "000") { SetMN(radTextBox_BranchID.Text.Trim()); }
            }
            else
            {
                bchId = "";
                radLabel_BranchName.Text = "";
            }

        }
        //Set MN
        void SetMN(string pBch)
        {
            if (radTextBox_BranchID.Text.ToString() != "000")
            {
                DataTable dtBch = BranchClass.GetDetailBranchByID("MN" + pBch);
                if (dtBch.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("สาขา");
                    bchId = ""; radLabel_BranchName.Text = "";
                    radTextBox_BranchID.SelectAll();
                    radTextBox_BranchID.Focus();
                    return;
                }
                bchId = dtBch.Rows[0]["BRANCH_ID"].ToString();
                radLabel_BranchName.Text = dtBch.Rows[0]["BRANCH_NAME"].ToString();
                radTextBox_ip.Focus();
            }
            else
            {
                bchId = ""; radLabel_BranchName.Text = "";
            }

        }
        //Select Type fix กล้อง
        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {

            if (radDropDownList_GroupSUB.SelectedValue.ToString() == "00003")
            {
                if (_radTextBox_SN.Contains("http") == true)
                {
                    radTextBox_SN.Enabled = false; radTextBox_ip.Enabled = false;
                    radDropDownList_GroupSUB.Enabled = false;
                    radTextBox_Desc.Focus();
                    return;
                }
                else
                {
                    string str = $@"SELECT	CAM_IP AS DATA_ID,CAM_DESC AS DATA_DESC
                                    FROM	SHOP_JOBCAM WITH (NOLOCK)
                                    WHERE	BRANCH_ID = '{bchId}' 
                                    ORDER BY CAM_IP ";

                    DataTable dt = ConnectionClass.SelectSQL_Main(str);
                    if (dt.Rows.Count > 0)
                    {
                        FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV() { dtData = dt };
                        if (frm.ShowDialog(this) == DialogResult.Yes)
                        {
                            radTextBox_SN.Text = frm.pID;
                            radTextBox_ip.Text = frm.pDesc;
                            radTextBox_SN.Enabled = false; radTextBox_ip.Enabled = false;
                            radTextBox_Desc.Focus();
                            return;
                        }
                    }

                    MessageBox.Show("ไม่มีข้อมูลของกล้อง ของสาขาที่ระบุ ลองใหม่อีกครั้ง.",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radDropDownList_GroupSUB.SelectedIndex = 0;
                    radDropDownList_GroupSUB.Focus();
                }
                
            }
            else
            {
                radTextBox_SN.Focus();
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
    }
}
