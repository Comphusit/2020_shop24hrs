﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.JOB.Com
{
    public partial class JOBCOM_SUPC_ADD : Telerik.WinControls.UI.RadForm
    {

        private string BCHID;
        private string BCHNAME;

        readonly string _tblName; // table
        readonly string _billFirst;//เลขที่บิลขึ้นต้น
        readonly string _grpId;//กลุ่ม
        readonly string _grpDesc;//ชื่อกลุ่ม

        readonly string _DptID;
        readonly string _radTextBox_ip;
        readonly string _radTextBox_SN;
        public JOBCOM_SUPC_ADD(string pTblName, string pBillFirst, string pGrpID, string pGrpDesc, string pDptID = "", string radTextBox_ip = "", string radTextBox_SN = "")
        {
            InitializeComponent();

            _tblName = pTblName;
            _billFirst = pBillFirst;
            _grpId = pGrpID;
            _grpDesc = pGrpDesc;
            _DptID = pDptID;
            _radTextBox_ip = radTextBox_ip;
            _radTextBox_SN = radTextBox_SN;
        }
        void Set_GroupSub()
        {
            radDropDownList_GroupSUB.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_GroupSUB.DataSource = JOB_Class.GetJOB_GroupSub(_grpId);
            radDropDownList_GroupSUB.ValueMember = "JOBGROUPSUB_ID";
            radDropDownList_GroupSUB.DisplayMember = "JOBGROUPSUB_DESCRIPTION";
        }
        //set ค่าของ radCombo select branch before Use
        void SetCbDpt()
        {
            //Set RadDropDownList All
            this.radDropDownList_Dpt.DropDownListElement.ListElement.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.AutoSizeItems = true;
            this.radDropDownList_Dpt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = SystemClass.SetFont12;
            this.radDropDownList_Dpt.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 30;


            DataTable dtDpt = Models.DptClass.GetDpt_AllD();

            radDropDownList_Dpt.DataSource = dtDpt;
            radDropDownList_Dpt.ValueMember = "NUM";
            radDropDownList_Dpt.DisplayMember = "DESCRIPTION_SHOW";

        }
        void Set_ClearData()
        {
            radGroupBox_Desc.GroupBoxElement.Header.Font = SystemClass.SetFont12;
            Set_GroupSub();
            SetCbDpt();
            radTextBox_SN.Text = "";
            radTextBox_ip.Text = "";
            radTextBox_Desc.Text = "";
            BCHID = "";
        }
        private void JOBCOM_SUPC_ADD_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupSUB);

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            Set_ClearData();

            radGroupBox_Desc.Text = _grpDesc + " : " + SystemClass.SystemUserName;
            this.Text = _grpDesc + " : SUPC";

            BCHID = radDropDownList_Dpt.SelectedValue.ToString();
            BCHNAME = Models.DptClass.GetDptName_ByDptID(BCHID);
            if (_DptID != "")
            {
                radDropDownList_Dpt.SelectedValue = _DptID; radDropDownList_Dpt.Enabled = false;
                radTextBox_ip.Text = _radTextBox_ip;
                radTextBox_SN.Text = _radTextBox_SN;
                if (_radTextBox_SN.Contains("http") == true) radDropDownList_GroupSUB.SelectedValue = "00003";
            }
          
        }

        private void RadDropDownList_Dpt_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                BCHID = radDropDownList_Dpt.SelectedValue.ToString();
                BCHNAME = Models.DptClass.GetDptName_ByDptID(BCHID);
                radDropDownList_GroupSUB.Focus();
            }
            catch (Exception)
            {
                MessageBox.Show("เช็คข้อมูลแผนกให้ถูกต้องอีกครั้ง.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radDropDownList_Dpt.Focus();
                return;
            }
        }
        //Focus
        private void RadTextBox_SN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Desc.Focus();
            }
        }
        //Focus
        private void RadTextBox_ip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_SN.Focus();
            }
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadDropDownList_GroupSUB_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_GroupSUB.SelectedValue.ToString() == "00003")
            {
                if (_DptID == "")
                {
                    string str = $@"SELECT	CAM_IP AS DATA_ID,CAM_DESC AS DATA_DESC
                                    FROM	SHOP_JOBCAM WITH (NOLOCK)
                                    WHERE	CAM_DEPT = '{BCHID}' 
                                            AND BRANCH_ID = 'MN000' 
                                    ORDER BY CAM_DESC  ";

                    DataTable dt = ConnectionClass.SelectSQL_Main(str);
                    if (dt.Rows.Count > 0)
                    {
                        FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV() { dtData = dt };

                        if (frm.ShowDialog(this) == DialogResult.Yes)
                        {
                            radTextBox_SN.Text = frm.pID;
                            radTextBox_ip.Text = frm.pDesc;
                            radTextBox_SN.Enabled = false; radTextBox_ip.Enabled = false;
                            radTextBox_Desc.Focus();
                            return;
                        }
                    }

                    MessageBox.Show("ไม่มีข้อมูลของกล้อง ไม่สามารถเปิดจ๊อบกล้องได้ ลองใหม่อีกครั้ง." + Environment.NewLine +
                        "[กล้องทั้งหมดจะต้องเปิดจ๊อบในส่วนของแผนก D999-แผนกส่วนกลาง เท่านั้น]" + Environment.NewLine +
                        "สามารถติดต่อสอบถามข้อมูลเพิ่มเติมได้ที่ ComMinimart Tel.8570",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radDropDownList_GroupSUB.SelectedIndex = 0;
                    radDropDownList_GroupSUB.Focus();

                }
                else
                {
                    radTextBox_SN.Enabled = false; radTextBox_ip.Enabled = false;
                    radDropDownList_GroupSUB.Enabled = false;
                    radTextBox_Desc.Focus();
                    return;
                }
            }
            else
            {
                radTextBox_ip.Focus();
            }
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            try
            {
                BCHID = radDropDownList_Dpt.SelectedValue.ToString();
                BCHNAME = Models.DptClass.GetDptName_ByDptID(BCHID);
            }
            catch (Exception)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูลแผนก");
                radDropDownList_Dpt.Focus();
                return;
            }

            if (BCHID == "" || BCHNAME == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("สาขา");
                radDropDownList_Dpt.Focus();
                return;
            }

            if (radTextBox_Desc.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดงานที่ต้องการซ่อม");
                radTextBox_Desc.Focus();
                return;
            }

            string descJOB;
            descJOB = "- " + radTextBox_Desc.Text.Trim();
            if (SystemClass.SystemComMinimart == "1")
            {
                descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComMinimart] ";
            }
            else if (SystemClass.SystemDptID == "D179")
            {
                descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComService] ";
            }
            else
            {
                descJOB = descJOB + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
            }

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID(_billFirst, "-", _billFirst, "1");
            string sqlIn = String.Format(@"INSERT INTO " + _tblName + " ([JOB_Number],[JOB_BRANCHID],[JOB_BRANCHNAME],[JOB_GROUPSUB] " +
                            ",[JOB_WHOINS],[JOB_NAMEINS],[JOB_SHORTNAMEINS],[JOB_DESCRIPTION],[JOB_SN],[JOB_IP],[JOB_DATEINS],[JOB_TYPE])  VALUES  " +
                            "('" + billMaxNO + "','" + BCHID + "','" + BCHNAME + "','" + radDropDownList_GroupSUB.SelectedValue + "'," +
                            "'" + SystemClass.SystemUserID + "','" + SystemClass.SystemUserName + "'," +
                            "'" + SystemClass.SystemUserNameShort + "','" + descJOB + "'+ ' [' + CONVERT(VARCHAR,GETDATE(),25) + ']'," +
                            "'" + radTextBox_SN.Text.Trim() + "','" + radTextBox_ip.Text.Trim() + "',GETDATE(),'SUPC' ) ");
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
    }
}
