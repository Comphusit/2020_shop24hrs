﻿namespace PC_Shop24Hrs.JOB.ASSET
{
    partial class AdjustAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdjustAsset));
            this.radTextBox_BillID = new Telerik.WinControls.UI.RadTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_rmk = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker_Date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Condition = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_MaintenanceInfo3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Notes = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Lease = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_LocationMemo = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Reference = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Location = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_NameAlias = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_SN = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_MaintenanceInfo3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Notes = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Reference = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_LocationMemo = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Condition = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel_GuaranteeDate = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Lease = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Location = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_SN = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_NameAlias = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_rmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Condition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MaintenanceInfo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Notes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Lease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LocationMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Reference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_NameAlias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MaintenanceInfo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Notes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Reference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_LocationMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Condition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GuaranteeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Lease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NameAlias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radTextBox_BillID
            // 
            this.radTextBox_BillID.AutoSize = false;
            this.radTextBox_BillID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_BillID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_BillID.Location = new System.Drawing.Point(12, 38);
            this.radTextBox_BillID.Name = "radTextBox_BillID";
            this.radTextBox_BillID.Size = new System.Drawing.Size(252, 25);
            this.radTextBox_BillID.TabIndex = 0;
            this.radTextBox_BillID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BillID_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radTextBox_rmk);
            this.panel1.Controls.Add(this.radDateTimePicker_Date);
            this.panel1.Controls.Add(this.radLabel_Name);
            this.panel1.Controls.Add(this.radDropDownList_Condition);
            this.panel1.Controls.Add(this.radTextBox_MaintenanceInfo3);
            this.panel1.Controls.Add(this.radTextBox_Notes);
            this.panel1.Controls.Add(this.radTextBox_Lease);
            this.panel1.Controls.Add(this.radTextBox_LocationMemo);
            this.panel1.Controls.Add(this.radTextBox_Reference);
            this.panel1.Controls.Add(this.radTextBox_Location);
            this.panel1.Controls.Add(this.radTextBox_NameAlias);
            this.panel1.Controls.Add(this.radTextBox_SN);
            this.panel1.Controls.Add(this.radLabel_MaintenanceInfo3);
            this.panel1.Controls.Add(this.radLabel_Notes);
            this.panel1.Controls.Add(this.radLabel14);
            this.panel1.Controls.Add(this.radLabel_Reference);
            this.panel1.Controls.Add(this.radLabel12);
            this.panel1.Controls.Add(this.radLabel_LocationMemo);
            this.panel1.Controls.Add(this.radLabel10);
            this.panel1.Controls.Add(this.radLabel_Condition);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radLabel_GuaranteeDate);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel_1);
            this.panel1.Controls.Add(this.radLabel_Lease);
            this.panel1.Controls.Add(this.radLabel8);
            this.panel1.Controls.Add(this.radLabel_Location);
            this.panel1.Controls.Add(this.radLabel_SN);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_NameAlias);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBox_BillID);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(674, 677);
            this.panel1.TabIndex = 2;
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(13, 614);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(112, 19);
            this.radLabel6.TabIndex = 65;
            this.radLabel6.Text = "หมายเหตุเพิ่มเติม";
            // 
            // radTextBox_rmk
            // 
            this.radTextBox_rmk.AutoSize = false;
            this.radTextBox_rmk.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_rmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_rmk.Location = new System.Drawing.Point(380, 600);
            this.radTextBox_rmk.Multiline = true;
            this.radTextBox_rmk.Name = "radTextBox_rmk";
            this.radTextBox_rmk.Size = new System.Drawing.Size(281, 33);
            this.radTextBox_rmk.TabIndex = 11;
            // 
            // radDateTimePicker_Date
            // 
            this.radDateTimePicker_Date.Enabled = false;
            this.radDateTimePicker_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDateTimePicker_Date.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_Date.Location = new System.Drawing.Point(380, 489);
            this.radDateTimePicker_Date.Name = "radDateTimePicker_Date";
            this.radDateTimePicker_Date.Size = new System.Drawing.Size(281, 21);
            this.radDateTimePicker_Date.TabIndex = 16;
            this.radDateTimePicker_Date.TabStop = false;
            this.radDateTimePicker_Date.Text = "วันเสาร์ที่ 4 เมษายน 2020";
            this.radDateTimePicker_Date.Value = new System.DateTime(2020, 4, 4, 0, 0, 0, 0);
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(13, 68);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(648, 23);
            this.radLabel_Name.TabIndex = 64;
            this.radLabel_Name.Text = "Name";
            // 
            // radDropDownList_Condition
            // 
            this.radDropDownList_Condition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Condition.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Condition.DropDownAnimationEnabled = false;
            this.radDropDownList_Condition.DropDownHeight = 124;
            this.radDropDownList_Condition.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Condition.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Condition.Location = new System.Drawing.Point(382, 181);
            this.radDropDownList_Condition.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Condition.Name = "radDropDownList_Condition";
            // 
            // 
            // 
            this.radDropDownList_Condition.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Condition.Size = new System.Drawing.Size(279, 25);
            this.radDropDownList_Condition.TabIndex = 63;
            this.radDropDownList_Condition.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Condition_SelectedValueChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Condition.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Condition.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Condition.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radTextBox_MaintenanceInfo3
            // 
            this.radTextBox_MaintenanceInfo3.AcceptsReturn = true;
            this.radTextBox_MaintenanceInfo3.AutoSize = false;
            this.radTextBox_MaintenanceInfo3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_MaintenanceInfo3.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MaintenanceInfo3.Location = new System.Drawing.Point(380, 515);
            this.radTextBox_MaintenanceInfo3.Multiline = true;
            this.radTextBox_MaintenanceInfo3.Name = "radTextBox_MaintenanceInfo3";
            this.radTextBox_MaintenanceInfo3.Size = new System.Drawing.Size(281, 81);
            this.radTextBox_MaintenanceInfo3.TabIndex = 10;
            // 
            // radTextBox_Notes
            // 
            this.radTextBox_Notes.AcceptsReturn = true;
            this.radTextBox_Notes.AutoSize = false;
            this.radTextBox_Notes.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Notes.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Notes.Location = new System.Drawing.Point(380, 405);
            this.radTextBox_Notes.Multiline = true;
            this.radTextBox_Notes.Name = "radTextBox_Notes";
            this.radTextBox_Notes.Size = new System.Drawing.Size(281, 81);
            this.radTextBox_Notes.TabIndex = 8;
            // 
            // radTextBox_Lease
            // 
            this.radTextBox_Lease.AcceptsReturn = true;
            this.radTextBox_Lease.AutoSize = false;
            this.radTextBox_Lease.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Lease.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Lease.Location = new System.Drawing.Point(380, 322);
            this.radTextBox_Lease.Multiline = true;
            this.radTextBox_Lease.Name = "radTextBox_Lease";
            this.radTextBox_Lease.Size = new System.Drawing.Size(281, 81);
            this.radTextBox_Lease.TabIndex = 7;
            // 
            // radTextBox_LocationMemo
            // 
            this.radTextBox_LocationMemo.AcceptsReturn = true;
            this.radTextBox_LocationMemo.AutoSize = false;
            this.radTextBox_LocationMemo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_LocationMemo.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_LocationMemo.Location = new System.Drawing.Point(381, 210);
            this.radTextBox_LocationMemo.Multiline = true;
            this.radTextBox_LocationMemo.Name = "radTextBox_LocationMemo";
            this.radTextBox_LocationMemo.Size = new System.Drawing.Size(281, 81);
            this.radTextBox_LocationMemo.TabIndex = 5;
            // 
            // radTextBox_Reference
            // 
            this.radTextBox_Reference.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Reference.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Reference.Location = new System.Drawing.Point(381, 294);
            this.radTextBox_Reference.Name = "radTextBox_Reference";
            this.radTextBox_Reference.Size = new System.Drawing.Size(281, 21);
            this.radTextBox_Reference.TabIndex = 6;
            // 
            // radTextBox_Location
            // 
            this.radTextBox_Location.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Location.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Location.Location = new System.Drawing.Point(381, 151);
            this.radTextBox_Location.Name = "radTextBox_Location";
            this.radTextBox_Location.Size = new System.Drawing.Size(281, 21);
            this.radTextBox_Location.TabIndex = 3;
            // 
            // radTextBox_NameAlias
            // 
            this.radTextBox_NameAlias.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_NameAlias.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_NameAlias.Location = new System.Drawing.Point(381, 123);
            this.radTextBox_NameAlias.Name = "radTextBox_NameAlias";
            this.radTextBox_NameAlias.Size = new System.Drawing.Size(281, 21);
            this.radTextBox_NameAlias.TabIndex = 2;
            // 
            // radTextBox_SN
            // 
            this.radTextBox_SN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_SN.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SN.Location = new System.Drawing.Point(381, 96);
            this.radTextBox_SN.Name = "radTextBox_SN";
            this.radTextBox_SN.Size = new System.Drawing.Size(281, 21);
            this.radTextBox_SN.TabIndex = 1;
            // 
            // radLabel_MaintenanceInfo3
            // 
            this.radLabel_MaintenanceInfo3.AutoSize = false;
            this.radLabel_MaintenanceInfo3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_MaintenanceInfo3.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_MaintenanceInfo3.Location = new System.Drawing.Point(93, 515);
            this.radLabel_MaintenanceInfo3.Name = "radLabel_MaintenanceInfo3";
            this.radLabel_MaintenanceInfo3.Size = new System.Drawing.Size(281, 81);
            this.radLabel_MaintenanceInfo3.TabIndex = 62;
            this.radLabel_MaintenanceInfo3.Text = "MaintenanceInfo3";
            this.radLabel_MaintenanceInfo3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel_Notes
            // 
            this.radLabel_Notes.AutoSize = false;
            this.radLabel_Notes.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Notes.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Notes.Location = new System.Drawing.Point(93, 403);
            this.radLabel_Notes.Name = "radLabel_Notes";
            this.radLabel_Notes.Size = new System.Drawing.Size(281, 83);
            this.radLabel_Notes.TabIndex = 61;
            this.radLabel_Notes.Text = "Notes";
            this.radLabel_Notes.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel14.Location = new System.Drawing.Point(12, 403);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(65, 43);
            this.radLabel14.TabIndex = 60;
            this.radLabel14.Text = "บิลที่เคยเบิก";
            // 
            // radLabel_Reference
            // 
            this.radLabel_Reference.AutoSize = false;
            this.radLabel_Reference.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Reference.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Reference.Location = new System.Drawing.Point(94, 294);
            this.radLabel_Reference.Name = "radLabel_Reference";
            this.radLabel_Reference.Size = new System.Drawing.Size(280, 23);
            this.radLabel_Reference.TabIndex = 59;
            this.radLabel_Reference.Text = "Reference";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(12, 296);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(81, 19);
            this.radLabel12.TabIndex = 58;
            this.radLabel12.Text = "บิลเบิกล่าสุด";
            // 
            // radLabel_LocationMemo
            // 
            this.radLabel_LocationMemo.AutoSize = false;
            this.radLabel_LocationMemo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_LocationMemo.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_LocationMemo.Location = new System.Drawing.Point(93, 210);
            this.radLabel_LocationMemo.Name = "radLabel_LocationMemo";
            this.radLabel_LocationMemo.Size = new System.Drawing.Size(281, 81);
            this.radLabel_LocationMemo.TabIndex = 57;
            this.radLabel_LocationMemo.Text = "LocationMemo";
            this.radLabel_LocationMemo.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(12, 210);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(70, 19);
            this.radLabel10.TabIndex = 56;
            this.radLabel10.Text = "บันทึกที่ตั้ง";
            // 
            // radLabel_Condition
            // 
            this.radLabel_Condition.AutoSize = false;
            this.radLabel_Condition.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Condition.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Condition.Location = new System.Drawing.Point(94, 183);
            this.radLabel_Condition.Name = "radLabel_Condition";
            this.radLabel_Condition.Size = new System.Drawing.Size(280, 23);
            this.radLabel_Condition.TabIndex = 55;
            this.radLabel_Condition.Text = "Condition";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(12, 185);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(43, 19);
            this.radLabel7.TabIndex = 54;
            this.radLabel7.Text = "สภาพ";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(378, 639);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(130, 32);
            this.radButton_Cancel.TabIndex = 12;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(243, 639);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(130, 32);
            this.radButton_Save.TabIndex = 11;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_GuaranteeDate
            // 
            this.radLabel_GuaranteeDate.AutoSize = false;
            this.radLabel_GuaranteeDate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_GuaranteeDate.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_GuaranteeDate.Location = new System.Drawing.Point(93, 490);
            this.radLabel_GuaranteeDate.Name = "radLabel_GuaranteeDate";
            this.radLabel_GuaranteeDate.Size = new System.Drawing.Size(281, 23);
            this.radLabel_GuaranteeDate.TabIndex = 50;
            this.radLabel_GuaranteeDate.Text = "GuaranteeDate";
            this.radLabel_GuaranteeDate.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(12, 515);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 19);
            this.radLabel5.TabIndex = 49;
            this.radLabel5.Text = "หมายเหตุ";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(12, 491);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(48, 19);
            this.radLabel4.TabIndex = 48;
            this.radLabel4.Text = "ประกัน";
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(12, 13);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(362, 19);
            this.radLabel_1.TabIndex = 46;
            this.radLabel_1.Text = "ระบุเลขที่สินทรัพย์/SN/คำค้นหา [Enter]";
            // 
            // radLabel_Lease
            // 
            this.radLabel_Lease.AutoSize = false;
            this.radLabel_Lease.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Lease.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Lease.Location = new System.Drawing.Point(93, 321);
            this.radLabel_Lease.Name = "radLabel_Lease";
            this.radLabel_Lease.Size = new System.Drawing.Size(281, 81);
            this.radLabel_Lease.TabIndex = 40;
            this.radLabel_Lease.Text = "Lease";
            this.radLabel_Lease.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(12, 321);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 43);
            this.radLabel8.TabIndex = 39;
            this.radLabel8.Text = "สถานที่เคยใช้";
            // 
            // radLabel_Location
            // 
            this.radLabel_Location.AutoSize = false;
            this.radLabel_Location.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Location.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Location.Location = new System.Drawing.Point(93, 153);
            this.radLabel_Location.Name = "radLabel_Location";
            this.radLabel_Location.Size = new System.Drawing.Size(280, 23);
            this.radLabel_Location.TabIndex = 38;
            this.radLabel_Location.Text = "Location";
            // 
            // radLabel_SN
            // 
            this.radLabel_SN.AutoSize = false;
            this.radLabel_SN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_SN.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_SN.Location = new System.Drawing.Point(93, 97);
            this.radLabel_SN.Name = "radLabel_SN";
            this.radLabel_SN.Size = new System.Drawing.Size(280, 23);
            this.radLabel_SN.TabIndex = 37;
            this.radLabel_SN.Text = "SN";
            // 
            // radLabel2
            // 
            this.radLabel2.AccessibleRole = System.Windows.Forms.AccessibleRole.Animation;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(10, 99);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(33, 19);
            this.radLabel2.TabIndex = 34;
            this.radLabel2.Text = "S/N";
            // 
            // radLabel_NameAlias
            // 
            this.radLabel_NameAlias.AutoSize = false;
            this.radLabel_NameAlias.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_NameAlias.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_NameAlias.Location = new System.Drawing.Point(93, 125);
            this.radLabel_NameAlias.Name = "radLabel_NameAlias";
            this.radLabel_NameAlias.Size = new System.Drawing.Size(280, 23);
            this.radLabel_NameAlias.TabIndex = 36;
            this.radLabel_NameAlias.Text = "NameAlias";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(11, 155);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(73, 19);
            this.radLabel3.TabIndex = 35;
            this.radLabel3.Text = "สถานที่เก็บ";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(10, 127);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(60, 19);
            this.radLabel1.TabIndex = 33;
            this.radLabel1.Text = "ชื่อค้นหา";
            // 
            // AdjustAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(676, 679);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdjustAsset";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ปรับทะเบียนสินทรัพย์";
            this.Load += new System.EventHandler(this.AdjustAsset_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_rmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Condition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MaintenanceInfo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Notes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Lease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LocationMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Reference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_NameAlias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MaintenanceInfo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Notes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Reference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_LocationMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Condition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GuaranteeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Lease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NameAlias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox radTextBox_BillID;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Lease;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel_Location;
        private Telerik.WinControls.UI.RadLabel radLabel_SN;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_NameAlias;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel_GuaranteeDate;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel_Condition;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel_LocationMemo;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel_Reference;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel_Notes;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel_MaintenanceInfo3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SN;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Location;
        private Telerik.WinControls.UI.RadTextBox radTextBox_NameAlias;
        private Telerik.WinControls.UI.RadTextBox radTextBox_LocationMemo;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Reference;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Lease;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MaintenanceInfo3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Notes;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Condition;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Date;
        private Telerik.WinControls.UI.RadTextBox radTextBox_rmk;
        private Telerik.WinControls.UI.RadLabel radLabel6;
    }
}
