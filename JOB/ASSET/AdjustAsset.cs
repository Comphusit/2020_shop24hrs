﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Collections;

namespace PC_Shop24Hrs.JOB.ASSET
{
    public partial class AdjustAsset : Telerik.WinControls.UI.RadForm
    {
        readonly string _pAssetID;
        readonly string _pBillID;
        readonly string _pJobNumber;
        readonly string _pBchID;
        readonly string _pBchName;
        readonly string _pSN;
        readonly string _pIP;

        string stanbyDpt;
        string pDate;
        //clearData
        void ClearData()
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Condition);

            radTextBox_BillID.Text = "";
            radTextBox_rmk.Text = "";
            radTextBox_SN.Text = "";
            radTextBox_Lease.Text = "";
            radTextBox_Location.Text = "";
            radTextBox_LocationMemo.Text = "";
            radTextBox_MaintenanceInfo3.Text = "";
            radTextBox_NameAlias.Text = "";
            radTextBox_Notes.Text = "";
            radTextBox_Reference.Text = "";

            radLabel_Condition.Text = "";
            radLabel_GuaranteeDate.Text = "";
            radLabel_Lease.Text = "";
            radLabel_Location.Text = "";
            radLabel_LocationMemo.Text = "";
            radLabel_MaintenanceInfo3.Text = "";
            radLabel_NameAlias.Text = "";
            radLabel_Notes.Text = "";
            radLabel_Reference.Text = "";
            radLabel_SN.Text = "";
            radLabel_Name.Text = "";
            pDate = DateTime.Now.ToString("yyyy-MM-dd");

            if (_pBillID == "")
            {
                radTextBox_BillID.Enabled = true;
                radTextBox_BillID.Focus();
            }
            else
            {
                radTextBox_BillID.Enabled = false;
            }
        }
        //condition
        void Set_Condition()
        {
            radDropDownList_Condition.DropDownListElement.Font = SystemClass.SetFont12;
            radDropDownList_Condition.DataSource = Models.AssetClass.FindConditionAsset();
            radDropDownList_Condition.ValueMember = "CODE_ID";
            radDropDownList_Condition.DisplayMember = "CODE_NAME";
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (_pBillID == "")
            {
                ClearData();
                return;
            }
            else
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }

        public AdjustAsset(string pAssetID, string pBillID, string pJobNumber, string pBchID, string pBchName, string pSN, string pIP)
        {
            InitializeComponent();

            _pAssetID = pAssetID;
            _pBillID = pBillID;
            _pJobNumber = pJobNumber;
            _pBchID = pBchID;
            _pBchName = pBchName;
            _pSN = pSN;
            _pIP = pIP;
        }
        //load
        private void AdjustAsset_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            ClearData();
            if (SystemClass.SystemComMinimart == "1") stanbyDpt = "StandBy ComMinimart"; else stanbyDpt = "StandBy";

            if (_pAssetID != "")
            {
                radTextBox_BillID.Enabled = false;
                radTextBox_BillID.Text = _pAssetID.ToUpper();
                FindData();
            }
            else
            {
                radTextBox_BillID.Enabled = true; radTextBox_BillID.Focus();
            }
        }
        //Set Data
        void FindData()
        {
            DataTable dtAsset = Models.AssetClass.FindAsset_ByAllType(radTextBox_BillID.Text, "", "");
            if (dtAsset.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ทะเบียนสินทรัพย์");
                radTextBox_BillID.SelectAll();
                radTextBox_BillID.Focus();
                return;
            }

            radLabel_Name.Text = dtAsset.Rows[0]["NAME"].ToString();
            radTextBox_BillID.Text = dtAsset.Rows[0]["AssetId"].ToString();
            radLabel_Condition.Text = dtAsset.Rows[0]["Condition"].ToString();
            radLabel_GuaranteeDate.Text = dtAsset.Rows[0]["GuaranteeDate"].ToString();
            radLabel_Lease.Text = dtAsset.Rows[0]["Lease"].ToString();
            radLabel_Location.Text = dtAsset.Rows[0]["LOCATION"].ToString();
            radLabel_LocationMemo.Text = dtAsset.Rows[0]["LocationMemo"].ToString();
            radLabel_MaintenanceInfo3.Text = dtAsset.Rows[0]["MaintenanceInfo3"].ToString();
            radLabel_NameAlias.Text = dtAsset.Rows[0]["NameAlias"].ToString();
            radLabel_Notes.Text = dtAsset.Rows[0]["Notes"].ToString();
            radLabel_Reference.Text = dtAsset.Rows[0]["Reference"].ToString();
            radLabel_SN.Text = dtAsset.Rows[0]["SerialNum"].ToString();
            Set_Condition(); radDropDownList_Condition.SelectedValue = dtAsset.Rows[0]["Condition"].ToString();
            radTextBox_BillID.Enabled = false;
            //กรณีที่ปรับทะเบียนแบบผูกจ๊อบ
            if (_pBillID == "") SetDataForStanby(); else SetDataForApvFAL();
        }

        //  Set Data For F/FAL
        void SetDataForApvFAL()
        {
            radTextBox_SN.Text = radLabel_SN.Text;

            if ((radTextBox_BillID.Text.Substring(0, 3) == "SPC") || (radTextBox_BillID.Text.Substring(0, 3) == "SRV")) radTextBox_NameAlias.Text = radLabel_NameAlias.Text;// กรณีที่ต้องใส่ชื่อเครื่องคอม
            else radTextBox_NameAlias.Text = _pBchName;

            radTextBox_Location.Text = _pBchID;
            radDropDownList_Condition.SelectedValue = "ใช้งาน"; radDropDownList_Condition.Enabled = false;

            if (radTextBox_BillID.Text.Substring(0, 3) == "CAM") radTextBox_LocationMemo.Text = _pIP + " [" + _pSN + "]";
            else
            {
                if (radTextBox_Location.Text == "D999") radTextBox_LocationMemo.Text = "";
                else radTextBox_LocationMemo.Text = _pBchName;
            }

            radTextBox_Reference.Text = _pBillID + " [" + _pJobNumber + "]";
            radTextBox_MaintenanceInfo3.Text = radLabel_MaintenanceInfo3.Text;

            radDateTimePicker_Date.Value = Convert.ToDateTime(radLabel_GuaranteeDate.Text);
            radDateTimePicker_Date.Format = DateTimePickerFormat.Custom;
            radDateTimePicker_Date.CustomFormat = "yyyy-MM-dd";

            if (radLabel_Reference.Text.Contains(_pBillID) == true)
            {
                radTextBox_Notes.Text = radLabel_Notes.Text; radTextBox_Lease.Text = radLabel_Lease.Text;
            }
            else
            {
                radTextBox_Notes.Text = radLabel_Notes.Text + Environment.NewLine + radLabel_Reference.Text;
                radTextBox_Lease.Text = radLabel_Lease.Text + Environment.NewLine + radLabel_Location.Text + "-" + radLabel_LocationMemo.Text;
            }

            radTextBox_LocationMemo.SelectionStart = radTextBox_LocationMemo.Text.Length;
            radTextBox_Lease.SelectionStart = radTextBox_Lease.Text.Length;
            radTextBox_Notes.SelectionStart = radTextBox_Notes.Text.Length;
            radTextBox_rmk.Text = _pBillID;
        }
        //Set Data For StanBy
        void SetDataForStanby()
        {
            radTextBox_SN.Text = radLabel_SN.Text;
            radDropDownList_Condition.SelectedValue = "พร้อมใช้งาน"; radDropDownList_Condition.Enabled = true;
            if ((radTextBox_BillID.Text).Contains("SPC") || (radTextBox_BillID.Text.Substring(0, 3) == "SRV"))
            {
                radTextBox_NameAlias.Text = radLabel_NameAlias.Text;
                if (SystemClass.SystemComMinimart == "1") radTextBox_LocationMemo.Text = stanbyDpt;
                else radTextBox_LocationMemo.Text = stanbyDpt;
            }
            else
            {
                if (SystemClass.SystemComMinimart == "1")
                {
                    radTextBox_LocationMemo.Text = stanbyDpt;
                    radTextBox_NameAlias.Text = stanbyDpt;
                }
                else
                {
                    radTextBox_LocationMemo.Text = stanbyDpt;
                    radTextBox_NameAlias.Text = stanbyDpt;
                }
            }

            radDateTimePicker_Date.Value = Convert.ToDateTime(radLabel_GuaranteeDate.Text);
            radDateTimePicker_Date.Format = DateTimePickerFormat.Custom;
            radDateTimePicker_Date.CustomFormat = "yyyy-MM-dd";
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_BillID.Text == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("เลขที่สินทรัพย์");
                radTextBox_BillID.Focus();
                return;
            }
            if (radTextBox_NameAlias.Text.Length > 20) radTextBox_NameAlias.Text = radTextBox_NameAlias.Text.Substring(0, 20);

            if (radTextBox_LocationMemo.Text == "")
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("บันทึกที่ตั้ง");
                radTextBox_LocationMemo.Focus();
                return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ปรับทะเบียน") == DialogResult.No) return;

            SaveData();
        }
        //SAVE
        void SaveData()
        {
            ArrayList In24 = new ArrayList { $@"
                INSERT INTO SHOP_ASSETTABLELOG 
                    (ASSETID,NAME,
                    SERIALNUM_OLD,SERIALNUM_NEW,NAMEALIAS_OLD,NAMEALIAS_NEW,
                    LOCATION_OLD,LOCATION_NEW,CONDITION_OLD,CONDITION_NEW,
                    LOCATIONMEMO_OLD,LOCATIONMEMO_NEW,REFERENCE_OLD,REFERENCE_NEW,
                    LEASE_OLD,LEASE_NEW,NOTES_OLD,NOTES_NEW,
                    UPDATE_WHOID,UPDATE_WHONAME,
                    MaintenanceInfo3_OLD,MaintenanceInfo3_NEW,
                    GuaranteeDate_OLD,GuaranteeDate_NEW,REMARK) 
                values ('{radTextBox_BillID.Text}','{radLabel_Name.Text }',
                    '{radLabel_SN.Text}','{radTextBox_SN.Text}','{ radLabel_NameAlias.Text}','{radTextBox_NameAlias.Text}',
                    '{radLabel_Location.Text}','{radTextBox_Location.Text}','{radLabel_Condition.Text}','{radDropDownList_Condition.SelectedValue}',
                    '{radLabel_LocationMemo.Text}','{radTextBox_LocationMemo.Text}','{radLabel_Reference.Text}','{radTextBox_Reference.Text}',
                    '{radLabel_Lease.Text}','{radTextBox_Lease.Text}','{radLabel_Notes.Text}','{radTextBox_Notes.Text}',
                    '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}',
                    '{radLabel_MaintenanceInfo3.Text}','{radTextBox_MaintenanceInfo3.Text}',
                    '{radLabel_GuaranteeDate.Text}','{radDateTimePicker_Date.Value}','{radTextBox_rmk.Text}')" };

            string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "") + "0";
            ArrayList InAX = new ArrayList
                {
                    AX_SendData.Save_EXTERNALLIST("UPDATE", "AssetTable", "AssetId|DATAAREAID", $@"{radTextBox_BillID.Text}|SPC",
                    "NameAlias|LOCATION|Condition|LocationMemo|Reference|Lease|Notes|LastMaintenance|SerialNum|MaintenanceInfo3",
                    $@"{radTextBox_NameAlias.Text}|{radTextBox_Location.Text}|{ radDropDownList_Condition.SelectedValue}|{radTextBox_LocationMemo.Text}|{radTextBox_Reference.Text}|
                        {radTextBox_Lease.Text}|{radTextBox_Notes.Text}|{pDate}|{radTextBox_SN.Text}|{radTextBox_MaintenanceInfo3.Text}", recID)
                };

            if (radDropDownList_Condition.SelectedValue.ToString() == "พร้อมใช้งาน")
            {
                DataTable dtRec = Models.AssetClass.FindDataAssetLending(radTextBox_BillID.Text);
                if (dtRec.Rows.Count > 0)
                {
                    recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "") + "1";
                    InAX.Add(AX_SendData.Save_EXTERNALLIST("UPDATE", "AssetLending", "RECID|DATAAREAID", $@"{dtRec.Rows[0]["RECID"]}|SPC",
                       "ExpectedReturn|ActualReturn", $@"{DateTime.Now:yyyy-MM-dd}|{DateTime.Now:yyyy-MM-dd}", recID));
                }
            }

            string TAX = ConnectionClass.ExecuteMain_AX_24_SameTime(In24, InAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(TAX);
            if ((TAX == "") && (_pJobNumber != ""))
            {
                this.DialogResult = DialogResult.OK; this.Close(); return;
            }
            if (TAX == "")
            {
                ClearData(); return;
            }
            if (TAX != "") MsgBoxClass.MsgBoxShow_SaveStatus(TAX);
                
        }
        //AssetID Enter
        private void RadTextBox_BillID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) FindData();
        }
        //Set Value
        private void RadDropDownList_Condition_SelectedValueChanged(object sender, EventArgs e)
        {
            switch (radDropDownList_Condition.SelectedValue)
            {
                case "พร้อมใช้งาน":
                    radTextBox_Location.Text = "D999";
                    if ((radTextBox_BillID.Text.Substring(0, 3) == "SPC") || (radTextBox_BillID.Text.Substring(0, 3) == "SRV"))
                    {
                        radTextBox_NameAlias.Text = radLabel_NameAlias.Text;// กรณีที่ต้องใส่ชื่อเครื่องคอม

                        if (SystemClass.SystemComMinimart == "1") radTextBox_LocationMemo.Text = stanbyDpt;
                        else radTextBox_NameAlias.Text = stanbyDpt;
                    }
                    else
                    {

                        if (SystemClass.SystemComMinimart == "1")
                        {
                            radTextBox_LocationMemo.Text = stanbyDpt;
                            radTextBox_NameAlias.Text = stanbyDpt;
                        }
                        else
                        {
                            radTextBox_NameAlias.Text = stanbyDpt;
                            radTextBox_LocationMemo.Text = stanbyDpt;
                        }
                    }
                    radTextBox_LocationMemo.Text = SystemClass.SystemDptID + "-" + SystemClass.SystemDptName;
                    radTextBox_Reference.Text = "";
                    radTextBox_MaintenanceInfo3.Text = radLabel_MaintenanceInfo3.Text;
                    radTextBox_Notes.Text = radLabel_Notes.Text + Environment.NewLine + radLabel_Reference.Text;
                    radTextBox_Lease.Text = radLabel_Lease.Text + Environment.NewLine + radLabel_Location.Text + "-" + radLabel_LocationMemo.Text;

                    radTextBox_LocationMemo.SelectionStart = radTextBox_LocationMemo.Text.Length;
                    radTextBox_Lease.SelectionStart = radTextBox_Lease.Text.Length;
                    radTextBox_Notes.SelectionStart = radTextBox_Notes.Text.Length;
                    break;
                case "ใช้งาน":
                    if (_pBillID == "")
                    {
                        radTextBox_Location.Text = radLabel_Location.Text;
                        radTextBox_NameAlias.Text = radLabel_NameAlias.Text;
                        radTextBox_LocationMemo.Text = radLabel_LocationMemo.Text;
                        radTextBox_Reference.Text = radLabel_Reference.Text;
                        radTextBox_MaintenanceInfo3.Text = radLabel_MaintenanceInfo3.Text;
                        radTextBox_Notes.Text = radLabel_Notes.Text;
                        radTextBox_Lease.Text = radLabel_Lease.Text;

                        radTextBox_LocationMemo.SelectionStart = radTextBox_LocationMemo.Text.Length;
                        radTextBox_Lease.SelectionStart = radTextBox_Lease.Text.Length;
                        radTextBox_Notes.SelectionStart = radTextBox_Notes.Text.Length;
                    }
                    break;
                default:
                    radTextBox_Location.Text = radLabel_Location.Text;
                    radTextBox_NameAlias.Text = radLabel_NameAlias.Text;
                    radTextBox_LocationMemo.Text = radLabel_LocationMemo.Text;
                    radTextBox_Reference.Text = radLabel_Reference.Text;
                    radTextBox_MaintenanceInfo3.Text = radLabel_MaintenanceInfo3.Text;
                    radTextBox_Notes.Text = radLabel_Notes.Text;
                    radTextBox_Lease.Text = radLabel_Lease.Text;

                    radTextBox_LocationMemo.SelectionStart = radTextBox_LocationMemo.Text.Length;
                    radTextBox_Lease.SelectionStart = radTextBox_Lease.Text.Length;
                    radTextBox_Notes.SelectionStart = radTextBox_Notes.Text.Length;

                    break;
            }
        }


    }
}
