﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.JOB.ASSET
{
    public partial class Asset_Main : RadForm
    {
        DataTable dt = new DataTable();
        readonly string _pPermission;
        //Main Load
        public Asset_Main(string pPermission) //0 = ผู้ที่มีสิทธิ์เข้าหน้าจอแก้ไขข้อมูล สินทรัพย์ 1 = ผู้ที่เข้าดูได้อย่างเดียว
        {
            InitializeComponent();
            _pPermission = pPermission;

        }
        private void Asset_Main_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radButton_Excel.ButtonElement.ShowBorder = true; radButton_Excel.ButtonElement.ToolTipText = "Export Excel.";

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("AssetId", "หมายเลขสินทรัพย์", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินทรัพย์", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMEALIAS", "ชื่อค้นหา", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATION", "ที่เก็บ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERIALNUM", "S/N", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CONDITION", "สภาพ", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATIONMEMO", "บันทึกที่ตั้ง", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REFERENCE", "บิลเบิก", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TECHINFO1", "รายละเอียด 1", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TECHINFO2", "รายละเอียด 2", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TECHINFO3", "รายละเอียด 3", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LEASE", "สถานที่เคยใช้", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MAKE", "ผู้จำหน่าย", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GUARANTEEDATE", "วันที่รับประกัน", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MAINTENANCEINFO3", "หมายเหตุ", 200));
            

            if (_pPermission == "0") radLabel_rmk.Text = "ระบุข้อความในช่องที่ต้องการค้นหาแล้วกด Enter || Double Click เพื่อปรับทะเบียน";
            else radLabel_rmk.Text = "ระบุข้อความในช่องที่ต้องการค้นหาแล้วกด Enter";

            //Freeze Column
            for (int i = 0; i < 5; i++)
            {
                this.radGridView_Show.MasterTemplate.Columns[i].IsPinned = true;
            }

            SetGDV();
        }


        //find Data
        void SetGDV()
        {
            this.Cursor = Cursors.WaitCursor;
            dt = Class.Models.AssetClass.FindAsset_ByAllType("", "", " TOP 100 ");
            radGridView_Show.DataSource = dt;
            this.Cursor = Cursors.Default;
        }

        #region "ROWS NUMBERS"      
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        //ROWS NUMBERS
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion


        private void RadGridView_Show_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {
            this.radGridView_Show.MasterView.TableFilteringRow.Cells[1].BeginEdit();
            string _pCon = " AND " + radGridView_Show.FilterDescriptors.Expression.ToString().Trim().Replace(" ", "");
            dt = Class.Models.AssetClass.FindAsset_ByAllType("", _pCon, "");
            radGridView_Show.DataSource = dt;
            dt.AcceptChanges();
        }

        #region "ค้นหาในกริดเมื่อ Enter"
        private bool EnterPress = false;

        private void Kel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) EnterPress = true;
        }

        private void RadGridView_Show_FilterChanging(object sender, GridViewCollectionChangingEventArgs e)
        {
            if (!EnterPress) e.Cancel = true;

            EnterPress = false;
        }

        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Row is GridViewFilteringRowInfo)
            {
                RadTextBoxEditor ed = e.ActiveEditor as RadTextBoxEditor;
                RadTextBoxEditorElement el = ed.EditorElement as RadTextBoxEditorElement;
                el.KeyDown -= Kel_KeyDown;
                el.KeyDown += Kel_KeyDown;
            }
        }

        #endregion

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pPermission == "1") return;
            
            if ((radGridView_Show.CurrentRow.Index == -1) || (radGridView_Show.CurrentRow.Cells["AssetId"].Value.ToString() == "")) return;
            
            AdjustAsset frm = new AdjustAsset(radGridView_Show.CurrentRow.Cells["AssetId"].Value.ToString(), "", "", "", "", "", "");
            if (frm.ShowDialog(this) == DialogResult.OK)
            {

            }
        }
        //Export
        private void RadButton_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายการสินทรัพย์.", radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}



