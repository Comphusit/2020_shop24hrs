﻿namespace PC_Shop24Hrs.Class
{
    class EncryptDataClass
    {
        //เข้ารหัสผ่าน  password ของพนักงาน
        public static string EncodePassword(string pass)
        {
            string res = "";
            for (int i = 0; i <= pass.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) - 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) + 4);
            }
            return res;
        }
        // ถอดรหัส password ของพนักงาน
        public static string DecodePassword(string pass)
        {
            string res = "";
            for (int i = 0; i <= pass.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) + 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) - 4);
            }
            return res;
        }

    }
}
