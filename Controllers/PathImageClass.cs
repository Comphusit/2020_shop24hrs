﻿using System;
using System.IO;

namespace PC_Shop24Hrs.Controllers
{
    class PathImageClass
    {
        // public static string SystemVLC = string.Format(@"C:\Program Files\VideoLAN\VLC\VLC.EXE");

        //public static string pPathBillOut = "";
        public static string pImageEmply = @"\\192.168.100.27\ImageMinimark\emply.png";
        public static string pImageCamera = @"\\192.168.100.27\ImageMinimark\takeimg.png";
        public static string pPathBillOut = "";
        public static string pPathJOBMaintenance = "";
        public static string pPathJOBMaintenanceClose = "";
        public static string pPathJOB = "";// ComMinimart
        public static string pPathJOBMN = ""; // Center
        public static string pPathJOBComService = "";
        public static string pServerIpImg = "";
        public static string pServerIpImgVender = "";
        public static string pPathJOBCar = "";
        public static string pPathJOBHD = "";// Head
        public static string pPathJOBHDBCH = "";// Branch Reply Head
        public static string pPathBillOutIDM = ""; // IDM
        public static string pPathAssetClaimLeave = "";//Claim
        public static string pPathSH_CarCloseNewLO = "";//รูปของ LO ทั้งหมด
        public static string pPathCAMERA = "";//กล้องหลัก
        public static string pPathRepairRA = "";// ซ่อมรถอู่นอก
        public static string pPathBOXBillEdit = "";//บิลแก้ไข
        public static string pPathBOXNotScan = "";//บิลพาเลท
        public static string pPathSupc = "";// ถ่ายรูปการรีบสินค้า LO
        public static string pPathCarwash = "";//รูปล้างรถ
        public static string pPathBillUNew = "";//รูปบิลเปลี่ยน
        public static string pPathVDOSendMoneyNew = "";// VDO ส่งซองเงิน
        public static string pPathVDOFront = "";// VDO หน้าร้าน
        public static string pPathRepairDevice = "";//ช่างนอกเข้าสาขา
        public static string pPathAllDay = "";//รูปประจำวัน
        public static string pPathAllDayChangeServer = "";//รูปประจำวัน
        public static string pPathCN = "";//รูปถ่าย MNRD
        public static string pPathShelfNew = "";//รูปจัดสินค้า
        public static string pPathVender = "";//รูปรับสินค้าผู้จำหน่าย
        public static string pPathVDOVender = ""; //รูป VDO ลงของผู้จำหน่าย
        public static string pPathEmpVDO = ""; //VDOตรวจตัวพนักงาน
        public static string pPathPLANMN = ""; //planmn
        public static string pPathCheckPriceMN = ""; //File รูปสำหรับเครื่องเช็คราคา มินิมาร์ท
        public static string pPathCheckPriceSUPC = ""; //File รูปสำหรับเครื่องเช็คราคา สาขาใหญ่
        public static string pPathMP = "";//File รูปรับบิล MP
        public static string pPathCheckItemRecive = "";//สินค้าควบคุม

        public static string pPathSendStock = "";//ส่งสต๊อก
        public static string pPathEmplSorting = "";//งานจัดเรียงสินค้า
        public static string pPathMNSV = "";//งานทีมจัดร้าน

        public static string pPathDocument = "";
        public static string pPathPromotion = ""; //รูปป้ายโปรโมชัน
        public static string pPathJOBFactory = "";//รูป JOB ซ่อมเครื่องจักรโรงงาน


        public static string CheckDirectory(string pathFolder)
        {
            string Desc = "";
            DirectoryInfo DirInfoVDO = new DirectoryInfo(pathFolder);
            if (DirInfoVDO.Exists == false)
            {
                Desc = "ไม่พบข้อมูลที่เก็บ File" + Environment.NewLine + DirInfoVDO.FullName;
            }
            return Desc;
        }

        public static FileInfo[] CheckFileWant(string pathFolder, string fileWant)
        {
            DirectoryInfo DirInfoVDO = new DirectoryInfo(pathFolder);
            FileInfo[] FilesInvoice = DirInfoVDO.GetFiles(fileWant + @"_*", SearchOption.AllDirectories);
            return FilesInvoice;
        }

    }



}
