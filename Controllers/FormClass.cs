﻿using System;
using System.IO;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI.Docking;

namespace PC_Shop24Hrs.Controllers
{
    class FormClass
    {
        public static Form OpenForm(Form activeFrm, Form mdiFrm, string caption, string Type, RadDock radDock1)
        {
            foreach (Form childFrm in radDock1.MdiChildren)
            {
                if (childFrm.GetType() == activeFrm.GetType())
                {
                    childFrm.Activate();
                    return childFrm;
                }
            }
            try
            {
                activeFrm.Tag = Type;
                activeFrm.MdiParent = mdiFrm;
                activeFrm.WindowState = FormWindowState.Normal;
                activeFrm.Text = caption;
                activeFrm.Show();
            }
            catch (Exception)
            {

            }
            return activeFrm;
        }

        public static DialogResult ShowDialog(Form frm, string caption)
        {
            frm.Text = caption;
            frm.WindowState = FormWindowState.Normal;
            frm.StartPosition = FormStartPosition.CenterScreen;
            return frm.ShowDialog();
        }
        public static void ShowCheckForm(Form frm, string caption)
        {
            if (!CheckFormUse(frm.Name))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เมนูนี้ยังไม่เปิดใช้งาน ให้ใช้งานใน Shop24Hrs เดิม [ตะกร้าแดง]{Environment.NewLine}สอบถามข้อมูลเพิ่มเติม ติดต่อ ComMinimart Tel.8570 ");
                return;
            }

            frm.Text = caption;
            frm.WindowState = FormWindowState.Maximized;
            frm.StartPosition = FormStartPosition.CenterScreen;
            if (CheckShowForm(frm.Name))
            {
                frm.Activate();
            }
            else
            {
                frm.Show();
            }
        }

        public static void ShowCheckFormNormal(Form frm, string caption)
        {
            if (!CheckFormUse(frm.Name))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เมนูนี้ยังไม่เปิดใช้งาน ให้ใช้งานใน Shop24Hrs เดิม [ตะกร้าแดง]{Environment.NewLine}สอบถามข้อมูลเพิ่มเติม ติดต่อ ComMinimart Tel.8570 ");
                return;
            }

            frm.Text = caption;
            frm.WindowState = FormWindowState.Normal;
            frm.StartPosition = FormStartPosition.CenterScreen;
            if (CheckShowForm(frm.Name))
            {
                frm.Activate();
            }
            else
            {
                frm.Show();
            }
        }
        public static void ShowNotCheckForm(Form frm, string caption)
        {
            if (!CheckFormUse(frm.Name))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เมนูนี้ยังไม่เปิดใช้งาน ให้ใช้งานใน Shop24Hrs เดิม [ตะกร้าแดง]{Environment.NewLine}สอบถามข้อมูลเพิ่มเติม ติดต่อ ComMinimart Tel.8570 ");
                return;
            }

            frm.Text = caption;
            frm.WindowState = FormWindowState.Maximized;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }
        public static void ShowNotCheckFormNormal(Form frm, string caption)
        {
            if (!CheckFormUse(frm.Name))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เมนูนี้ยังไม่เปิดใช้งาน ให้ใช้งานใน Shop24Hrs เดิม [ตะกร้าแดง]{Environment.NewLine}สอบถามข้อมูลเพิ่มเติม ติดต่อ ComMinimart Tel.8570 ");
                return;
            }

            frm.Text = caption;
            frm.WindowState = FormWindowState.Normal;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }
        public static bool CheckShowForm(string _formName)
        {
            bool ret = false;

            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == _formName)
                {
                    frm.Activate();
                    ret = true;
                }
            }

            return ret;
        }

        //Check ว่าฟอร์มนี้เปืดใช้หรือยัง
        public static bool CheckFormUse(string _formName)
        {
            Boolean checkOpen = false;
            string T = Class.ConfigClass.GetConfigUSE_FORMNAME(_formName);
            switch (T)
            {
                case "0":
                    if (SystemClass.SystemComProgrammer == "1") { checkOpen = true; }
                    break;
                case "1":
                    if (SystemClass.SystemComMinimart == "1") { checkOpen = true; }
                    break;
                case "2":
                    checkOpen = true; break;
                default:
                    break;
            }
            return checkOpen;
        }

        //check Document
        public static void Document_Check(string pFORMNAME, string pFORMTYPE) // 1 open 0 upload
        {
            string pStaOpen = "1"; //1 open 0 upload
            string dptName = " ComMinimart 8570";

            string open = "SHOP";
            if (SystemClass.SystemBranchID == "MN000") open = "SUPC";
            if (pFORMTYPE == "00016") open = "SHOP";
            if ((pFORMTYPE == "00017") || (pFORMTYPE == "00018") || (pFORMTYPE == "00019") || (pFORMTYPE == "00020") || (pFORMTYPE == "00021"))
            {
                open = "CENTER"; //[CenterShop]
                dptName = "CenterShop 1022";
            }


            if (SystemClass.SystemBranchID == "MN000")
            {
                FormShare.ChooseData chsData = new FormShare.ChooseData("เปิดคู่มือ", "อัพเดตคู่มือ");
                if (chsData.ShowDialog() == DialogResult.Yes)
                {
                    pStaOpen = chsData.sSendData;
                }
            }
            else if (SystemClass.SystemComMinimart == "1")
            {
                FormShare.ChooseData chsData = new FormShare.ChooseData("เปิดคู่มือ", "อัพเดตคู่มือ");
                if (chsData.ShowDialog() == DialogResult.Yes)
                {
                    pStaOpen = chsData.sSendData;
                }
            }


            string fileDocument = "";
            string sql = $@"
                SELECT	FILENAME	FROM	SHOP_DOCUMENT WITH (NOLOCK)
                WHERE	FORMNAME = '{pFORMNAME}' AND FORMTYPE = '{pFORMTYPE}' AND OPENBY = '{open}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            if (dt.Rows.Count > 0) //เช็คไฟล์ในเบส
            {
                //เช็คไฟล์ในโฟร์เดอร์
                if (File.Exists(dt.Rows[0]["FILENAME"].ToString()) == true) fileDocument = dt.Rows[0]["FILENAME"].ToString();
            }

            if (pStaOpen == "1") //เลือกเปิดคู่มือ
            {
                if (fileDocument == "") //ไม่เจอไฟล์ในเบส
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบคู่มือการใช้งานภายในระบบ{ Environment.NewLine}ติดต่อ แผนก {dptName}");
                    return;
                }
                else
                {
                    try
                    {
                        System.Diagnostics.Process.Start(fileDocument); //เปิดไฟล์
                        return;
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถเปิดเอกสารคู่มือได้ ลองใหม่อีกครั้ง{ Environment.NewLine}{ex.Message}{ Environment.NewLine}หรือติอต่อแผนก {dptName}");
                        return;
                    }
                }
            }



            //เลือกอัพเดตคู่มือ
            if (SystemClass.SystemComMinimart != "1") //เช็คสิทธิ์ไม่ใช่Minimart
            {
                if ((pFORMTYPE == "00017") || (pFORMTYPE == "00018") || (pFORMTYPE == "00019") || (pFORMTYPE == "00020") || (pFORMTYPE == "00021")) //เช็คฟอร์มที่ใช้
                {
                    if (SystemClass.SystemDptID != "D054") //เช็คสิทธิ์Center
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีสิทธ์ในการจัดการ ติดต่อแผนกที่รับผิดชอบ");
                        return;
                    }
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีสิทธ์ในการจัดการ ติดต่อแผนกที่รับผิดชอบ");
                    return;
                }
            }

            string fileName = "";
            OpenFileDialog openDlg = new OpenFileDialog { Title = "เลือกเอกสารคู่มือ" }; //เลือกไฟล์ที่จะอัพ
            if (openDlg.ShowDialog() == DialogResult.OK) fileName = openDlg.FileName;

            if (fileName != "")
            {
                string targetPath = fileName;
                if ((pFORMTYPE == "00017") || (pFORMTYPE == "00018") || (pFORMTYPE == "00019") || (pFORMTYPE == "00020") || (pFORMTYPE == "00021"))
                {
                    string lastFile = fileName.Substring(fileName.LastIndexOf(@"."));
                    targetPath = $@"{PathImageClass.pPathDocument}{pFORMTYPE}\{pFORMNAME}{lastFile}";
                    if (File.Exists(targetPath) == true) File.Delete(targetPath); //ถ้ามีไฟล์อยู่ในโฟร์เดอร์ให้ลบไฟล์เดิมออกก่อน
                    File.Copy(fileName, targetPath);
                }
                string T = Document_Save(pFORMNAME, pFORMTYPE, targetPath, open, "GETDATE()", ""); //บันทึกลงเบส
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
            else MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกเอกสารคู่มือให้เรียบร้อยก่อนการบันทึก");


            
        }
        //check Document
        public static string Document_Save(string pFORMNAME, string pFORMTYPE, string pFILENAME, string pOPENBY,string pDateIns,string pRemark)//string pSta,
        {
            string sql = $@"
                DECLARE @DIMTABLE TABLE (DESC_STATUS NVARCHAR(500));
                DECLARE @pFORMNAME AS NVARCHAR(50) = '{pFORMNAME}';
                DECLARE @pFORMTYPE AS NVARCHAR(200) = '{pFORMTYPE}';
                DECLARE @pOPENBY AS NVARCHAR(200) = '{pOPENBY}';
                DECLARE @pFileName AS NVARCHAR(200) = '{pFILENAME}';
                DECLARE @pRemark AS NVARCHAR(200) = '{pRemark}';
                DECLARE @pDateIns AS DATETIME = CONVERT(VARCHAR,{pDateIns},25) ;

                DECLARE @pWHOINS AS NVARCHAR(200) = '{SystemClass.SystemUserID}';
                DECLARE @pWHONAME AS NVARCHAR(200) = '{SystemClass.SystemUserName}';
 
                INSERT INTO @DIMTABLE VALUES ('ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง')
                IF NOT EXISTS  (
			                SELECT	*	FROM	SHOP_DOCUMENT WITH (NOLOCK) WHERE	FORMNAME = @pFORMNAME AND FORMTYPE = @pFORMTYPE AND OPENBY = @pOPENBY )
                BEGIN
	                 INSERT INTO  SHOP_DOCUMENT
	                  (FORMNAME,FORMTYPE,OPENBY,FILENAME,WHOINS,WHONAME,REMARK,DATEINS) VALUES (
		                @pFORMNAME,@pFORMTYPE,@pOPENBY,@pFileName,@pWHOINS,@pWHONAME,@pRemark,@pDateIns)
		            DELETE 	@DIMTABLE  
	                INSERT INTO @DIMTABLE VALUES ('')
                END
                ELSE
                BEGIN
                    UPDATE  SHOP_DOCUMENT
	                SET     DATEUPD = GETDATE() ,FILENAME = @pFileName,WHOINS = @pWHOINS,WHONAME = @pWHONAME
                    WHERE   FORMNAME = @pFORMNAME AND FORMTYPE = @pFORMTYPE  AND OPENBY = @pOPENBY
                    DELETE 	@DIMTABLE  
	                INSERT INTO @DIMTABLE VALUES ('')
                END

                SELECT * FROM @DIMTABLE
                 ";
           
            return ConnectionClass.SelectSQL_Main(sql).Rows[0]["DESC_STATUS"].ToString();
        }

        //SELECT ป้ายโปรโมชัน
        public static DataTable DataFormSHOP_DOCUMENT(string FORMNAME, string OPENBY)
        {
            string LastFile = "";
            if (FORMNAME != "") LastFile = $@"AND FORMNAME = '{FORMNAME}'";
            string sql = $@"SELECT	FORMNAME, CONVERT(nvarchar,DATEINS,23) AS DATEINS, FILENAME, REMARK,
                                    CASE WHEN CONVERT(nvarchar,DATEINS,23) >= CONVERT(nvarchar,GETDATE(),23) THEN '1' ELSE '0' END AS CDATE,
                                    WHOINS + char(10) + WHONAME AS WHOINS
                            FROM	SHOP_DOCUMENT WITH (NOLOCK)
                            WHERE	FORMTYPE = '00022'
                                    AND OPENBY = '{OPENBY}'
                                    {LastFile}
                            ORDER BY DATEINS DESC";
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
