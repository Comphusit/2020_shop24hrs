﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PC_Shop24Hrs.Controllers
{
    class ImageClass
    {
        //open ShowImage
        public static DialogResult OpenShowImage(string _pCount, string pPath)
        {
            if (_pCount == "0") return DialogResult.No;
            FormShare.ShowImage.ShowImage frmSPC = new FormShare.ShowImage.ShowImage() { pathImg = pPath };
            if (frmSPC.ShowDialog() == DialogResult.OK) return DialogResult.OK; else return DialogResult.No;
        }
        //Check Path File
        public static bool CheckFileExit(string Path)
        {
            FileInfo FilePaht = new FileInfo(Path);
            bool ExitPath = false;
            if (FilePaht.Exists == true) ExitPath = true;
            return ExitPath;
        }
        //SHowImageInDGV
        public static Image ScaleImageDataGridView150_200_ForDelete_SendPath(string imagePath)
        {
            try
            {
                Bitmap image = new Bitmap(imagePath);
                int maxWidth = 150;
                int maxHeight = 200;
                var ratioX = (double)maxWidth / image.Width;
                var ratioY = (double)maxHeight / image.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);

                var newImage = new Bitmap(newWidth, newHeight);

                using (var graphics = Graphics.FromImage(newImage))
                    graphics.DrawImage(image, 0, 0, newWidth, newHeight);

                return newImage;
            }
            catch (Exception)
            {
                return new Bitmap(PathImageClass.pImageEmply);
            }
        }
        //Send Scale input text in image
        public static Image ScaleImageDataGridViewSendScaleDrawTextInImage(int w, int h, string imagePath, string textInImage)
        {
            try
            {
                Bitmap image = new Bitmap(imagePath);
                int maxWidth = w;
                int maxHeight = h;

                var ratioX = (double)maxWidth / image.Width;
                var ratioY = (double)maxHeight / image.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);

                string firstLine = textInImage.Replace(".000", "");
                // string secondLine = "Test";

                // Create the Font object for the image text drawing.
                Font objFont = new Font("Tahoma", 18, FontStyle.Bold, GraphicsUnit.Pixel);

                // Create a graphics object to measure the text's width and height.
                Graphics objGraphics = Graphics.FromImage(image);

                // Create the bmpImage again with the correct size for the text and font.
                image = new Bitmap(image, new Size(newWidth, newHeight));

                // Add the colors to the new bitmap.
                objGraphics = Graphics.FromImage(image);

                objGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias; //  <-- This is the correct value to use. ClearTypeGridFit is better yet!
                objGraphics.DrawString(firstLine, objFont, new SolidBrush(System.Drawing.Color.White), 10, (newHeight / 2), StringFormat.GenericDefault);
                //objGraphics.DrawString(secondLine, objFont, new SolidBrush(System.Drawing.Color.Black), 0, newHeight / 2, StringFormat.GenericDefault);

                objGraphics.Flush();

                return (image);
            }
            catch (Exception)
            {
                return new Bitmap(PathImageClass.pImageEmply);
            }
        }
        //Image from Path Not Set Size
        public static Image ScaleImageData_SendPath(string imagePath)
        {
            try
            {
                Bitmap image = new Bitmap(imagePath);
                return image;
            }
            catch (Exception)
            {
                return new Bitmap(PathImageClass.pImageEmply);
            }
        }
    }
}
