﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using QRCoder;
using Newtonsoft.Json;
using System.Net;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.Controllers
{
    //ตัวแปรสำหรับการพิมพ์บิล MNPT+MNPF
    public class Var_Print_MNPT_MNPF
    {
        public string Docno { get; set; } = "";
        public string BranchID { get; set; } = "";
        public string BranchName { get; set; } = "";
        public string InventLocationID { get; set; } = "";
        public string Route { get; set; } = "";
        public string BOX { get; set; } = "";// Sytle 1/2,2/2
        public string EmplIDPacking { get; set; } = "";
        public string EmplNamePacking { get; set; } = "";
    }
    //ตัวแปร สำหรับการพิมพ์บิล MNRS+BOX
    public class Var_Print_MNRS
    {
        public string SDocno { get; set; } = "";
        public string SBranch { get; set; } = "";
        public string SNumBox { get; set; } = "";
        public string SLocation { get; set; } = "";
        public string SpCopyColor { get; set; } = "";
        public string SpRmk { get; set; } = "";
        public string SpUpPRINTCOUNT { get; set; } = "";
        public string SPersonSave { get; set; } = "";
        public string SPersonPacking { get; set; } = "";
        public int IBox { get; set; } = 0;
        public int SpCopy { get; set; } = 0;
        public double SumLINEAMOUNT { get; set; } = 0.00;
        public DataTable SdataDetail = new DataTable();


    }
    class PrintClass
    {
        //printBarcode Discount Godex
        public static StringBuilder SetPrintBarcodeDiscountGodex(string barcode)
        {
            #region CodeGodex
            //^Q40,3
            //^W40
            //^H6
            //^P1
            //^S6
            //^AD
            //^C1
            //^R0
            //~Q+0
            //^O0
            //^D0
            //^E16
            //~R200
            //^XSET,ROTATION,0
            //^L
            //Dy2-me-dd
            //Th:m:s
            //BQ,13,75,2,6,50,0,3,MN22091700002
            //ATA,15,5,48,48,0,0,A,0,### สินค้าลดราคา ###
            //ATA,193,183,73,73,0,0,A,0,บาท
            //Lo,6,171,311,182
            //ATA,35,262,34,45,0,0,A,0,*ซื้อแล้วไม่รับคืนทุกกรณี*
            //E
            #endregion

            StringBuilder rowData = new StringBuilder();
            rowData.Append("^Q40,3\n");
            rowData.Append("^W40\n");
            rowData.Append("^H6\n");
            rowData.Append("^P1\n");
            rowData.Append("^S6\n");
            rowData.Append("^AD\n");
            rowData.Append("^C1\n");
            rowData.Append("^R0\n");
            rowData.Append("~Q+0\n");
            rowData.Append("^O0\n");
            rowData.Append("^D0\n");
            rowData.Append("^E16\n");
            rowData.Append("~R200\n");
            rowData.Append("^XSET,ROTATION,0\n");
            rowData.Append("^L\n");
            rowData.Append("Dy2-me-dd\n");
            rowData.Append("Th:m:s\n");

            rowData.AppendFormat("BQ,13,75,2,6,50,0,3,{0}\n", barcode);
            rowData.AppendFormat("ATA,15,5,48,48,0,0,A,0,### สินค้าลดราคา ###\n");
            rowData.AppendFormat("Lo,6,171,311,182\n");
            rowData.AppendFormat("ATA,193,183,73,73,0,0,A,0,บาท\n");
            rowData.AppendFormat("ATA,35,262,34,45,0,0,A,0,*ซื้อแล้วไม่รับคืนทุกกรณี*\n");
            rowData.Append("E\n");
            return rowData;
        }
        //printBarcode Container Godex
        public static StringBuilder SetPrintBarcodeContainer_Goldex(string barcode, string emplID)
        {
            StringBuilder rowData = new StringBuilder();
            rowData.Append("^Q40,3\n");
            rowData.Append("^W30\n");
            rowData.Append("^H6\n");
            rowData.Append("^P1\n");
            rowData.Append("^S6\n");
            rowData.Append("^AD\n");
            rowData.Append("^C1\n");
            rowData.Append("^R0\n");
            rowData.Append("~Q+0\n");
            rowData.Append("^O0\n");
            rowData.Append("^D0\n");
            rowData.Append("^E16\n");
            rowData.Append("~R200\n");
            rowData.Append("^XSET,ROTATION,0\n");
            rowData.Append("^L\n");
            rowData.Append("Dy2-me-dd\n");
            rowData.Append("Th:m:s\n");
            rowData.AppendFormat("BQ,189,46,2,6,95,1,1,{0}\n", barcode);
            rowData.AppendFormat("ATA,64,32,34,34,0,1E,A,0,{0}\n", DateTime.Now.ToString("dd-MM-yyy"));
            rowData.AppendFormat("ATA,64,190,34,34,0,1E,A,0,{0}\n", "0 kg.");
            rowData.AppendFormat("ATA,240,184,34,34,0,1E,A,0,{0}\n", emplID);
            rowData.Append("E\n");
            //RawPrinterHelper.SendStringToPrinter(printerName, rowData.ToString());
            return rowData;
        }
        //printBarcode Container Epson TM
        public static void SetPrintBarcodeContainer_TM(System.Drawing.Printing.PrintPageEventArgs e, string Barcode, string EmplID, string EmplName)
        {
            BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear
            {
                Type = BarcodeLib.Barcode.BarcodeType.CODE128,
                UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL,
                BarWidth = 1,//1
                BarHeight = 60,//38
                LeftMargin = 0,
                RightMargin = 0,
                TopMargin = 0,
                BottomMargin = 0,

                Data = Barcode
            };
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            Y += 20;
            e.Graphics.DrawString(EmplID + "-" + EmplName, SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 85;
            e.Graphics.DrawString(DateTime.Now.ToString("dd-MM-yyyy"), SystemClass.printFont, Brushes.Black, 5, Y);
            e.Graphics.DrawString("0.00 Kg.", SystemClass.printFont, Brushes.Black, 95, Y);
            Y += 20;
            //e.Graphics.DrawString(".", SystemClass.printFont, Brushes.Black, 5, Y);
            e.Graphics.DrawString("- Container Number - ", SystemClass.printFont, Brushes.Black, 40, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        //Create QrCode สำหรับการจัดของสาขาใหญ่
        public static Bitmap SetQeCode_MNPF(string docno)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            string srcodeStr = $@"https://spcwebapp.azurewebsites.net/Shop24HrsWebVUE/#/mnpfmain?docno={docno}";
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(srcodeStr, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            return qrCode.GetGraphic(3);
        }
        //การพิมพ์บิลจัดสินค้า MNPF + MNPT
        public static void Print_MNPT_MNPF(string pTypePrint, System.Drawing.Printing.PrintPageEventArgs e, Var_Print_MNPT_MNPF var, string staContainer, string staQRPrint) //pTypePrint = MNPT/MNPF  staContainer = 0 ไม่พิม containner 1 = พิมพ์
        {
            BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear
            {
                Type = BarcodeLib.Barcode.BarcodeType.CODE128,
                UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL,
                BarWidth = 1,//1
                BarHeight = 60,//38
                LeftMargin = 0,
                RightMargin = 0,
                TopMargin = 0,
                BottomMargin = 0,

                Data = var.Docno
            };
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            Y += 20;
            e.Graphics.DrawString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont15, Brushes.Black, 20, Y);
            Y += 30;
            e.Graphics.DrawImage(barcodeInBitmap, 20, Y);
            Y += 80;
            e.Graphics.DrawString(var.Docno, new Font(new FontFamily("Tahoma"), 20), Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString(var.Route, new Font(new FontFamily("Tahoma"), 20), Brushes.Black, 0, Y);
            Y += 55;
            e.Graphics.DrawString(var.BranchID, new Font(new FontFamily("Tahoma"), 50), Brushes.Black, 0, Y);
            Y += 60;
            e.Graphics.DrawString(var.BranchName, new Font(new FontFamily("Tahoma"), 35), Brushes.Black, 0, Y);
            Y += 55;
            e.Graphics.DrawString("ตัดสต็อกที่", new Font(new FontFamily("Tahoma"), 35), Brushes.Black, 0, Y);
            Y += 55;
            e.Graphics.DrawString(var.InventLocationID, new Font(new FontFamily("Tahoma"), 35), Brushes.Black, 0, Y);

            switch (pTypePrint)
            {
                case "MNPF":
                    Y += 60;
                    e.Graphics.DrawString(var.BOX, new Font(new FontFamily("Tahoma"), 75), Brushes.Black, 0, Y);
                    Y += 50;
                    //e.Graphics.DrawString("-", SystemClass.printFont15, Brushes.Black, 0, Y);
                    if (staQRPrint == "1")
                    {
                        Y += 60;
                        e.Graphics.DrawImage(PrintClass.SetQeCode_MNPF(var.Docno.Replace("MNPF", "")), 70, Y);
                        Y += 160;
                        e.Graphics.DrawString("ยิงสินค้าจัดใน Shop24Hrs.WEB เท่านั้น", SystemClass.SetFont12, Brushes.Black, 0, Y);
                    }
                    else
                    {
                        Y += 180;
                        e.Graphics.DrawString("จัดผ่านระบบ Container เท่านั้น", SystemClass.printFont15, Brushes.Black, 0, Y);
                        Y += 25;
                        e.Graphics.DrawString(var.EmplIDPacking + "-" + var.EmplNamePacking, SystemClass.SetFont12, Brushes.Black, 0, Y);
                    }

                    break;
                case "MNPT":
                    //เอาโค้ดจาก Ice
                    if (staContainer == "0")
                    {
                        Y += 180;//250
                        //e.Graphics.DrawString(".", new Font(new FontFamily("Tahoma"), 10), Brushes.Black, 0, Y);
                        e.Graphics.DrawString(var.EmplIDPacking + "-" + var.EmplNamePacking, SystemClass.printFont15, Brushes.Black, 0, Y);
                    }
                    else
                    {
                        string numberContainer = Class.ConfigClass.GetMaxINVOICEID("", "", "", "13");
                        string staSaveAX = AX_SendData.Save_WHSContainer(numberContainer, var.EmplIDPacking, var.Docno);
                        if (staSaveAX != "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกข้อมูล Container ได้{Environment.NewLine}ลองใหม่อีกครั้ง หรือ พิมพ์ Container แยกที่เมนูด้านบน{Environment.NewLine}{staSaveAX}");
                            Y += 180;//250
                            e.Graphics.DrawString(var.EmplIDPacking + "-" + var.EmplNamePacking, SystemClass.printFont15, Brushes.Black, 0, Y);
                        }
                        else
                        {
                            barcode.Data = numberContainer;
                            Bitmap barcodeInBitmapBox = new Bitmap(barcode.drawBarcode());
                            Y += 65;
                            e.Graphics.DrawString(var.EmplIDPacking + "-" + var.EmplNamePacking, SystemClass.printFont, Brushes.Black, 5, Y);
                            Y += 20;
                            e.Graphics.DrawImage(barcodeInBitmapBox, 40, Y);
                            Y += 85;
                            e.Graphics.DrawString("- Container Number - ", SystemClass.printFont, Brushes.Black, 40, Y);
                            Y += 20;
                            e.Graphics.DrawString(DateTime.Now.ToString("dd-MM-yyyy"), SystemClass.printFont, Brushes.Black, 5, Y);
                            e.Graphics.DrawString("0.00 Kg.", SystemClass.printFont, Brushes.Black, 95, Y);
                            Y += 20;
                            e.Graphics.DrawString(".", SystemClass.printFont, Brushes.Black, 5, Y);
                            e.Graphics.PageUnit = GraphicsUnit.Inch;
                        }
                        ////MsgRecive msgRecive = (MsgRecive)JsonConvert.DeserializeObject(PO_Class.CreatContainerNumberTypeNotWeight(var.EmplIDPacking), (typeof(MsgRecive)));
                        //if (!msgRecive.Success)
                        //{
                        //    Y += 180;//250
                        //    e.Graphics.DrawString(var.EmplIDPacking + "-" + var.EmplNamePacking, SystemClass.printFont15, Brushes.Black, 0, Y);
                        //}
                        //else
                        //{
                        //    barcode.Data = numberContainer;
                        //    Bitmap barcodeInBitmapBox = new Bitmap(barcode.drawBarcode());
                        //    Y += 80;
                        //    e.Graphics.DrawString(var.EmplIDPacking + "-" + var.EmplNamePacking, SystemClass.printFont, Brushes.Black, 5, Y);
                        //    Y += 20;
                        //    e.Graphics.DrawImage(barcodeInBitmapBox, 40, Y);
                        //    Y += 85;
                        //    e.Graphics.DrawString("- Container Number - ", SystemClass.printFont, Brushes.Black, 40, Y);
                        //    e.Graphics.DrawString(DateTime.Now.ToString("dd-MM-yyyy"), SystemClass.printFont, Brushes.Black, 5, Y);
                        //    e.Graphics.DrawString("0.00 Kg.", SystemClass.printFont, Brushes.Black, 95, Y);
                        //    Y += 20;
                        //    e.Graphics.DrawString(".", SystemClass.printFont, Brushes.Black, 5, Y);
                        //    e.Graphics.PageUnit = GraphicsUnit.Inch;
                        //}
                    }
                    break;
                default:
                    break;
            }

        }

        //สำหรับการพิมพ์บิลเบิก MNRS และ Tag ติดลัง MNRS
        public static void Print_MNRS(string pTypePrint, System.Drawing.Printing.PrintPageEventArgs e, Var_Print_MNRS var) //pTypePrint box,bill
        {
            BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear
            {
                Type = BarcodeLib.Barcode.BarcodeType.CODE128,
                UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL,
                BarWidth = 1,//1
                BarHeight = 60,//38
                LeftMargin = 0,
                RightMargin = 0,
                TopMargin = 0,
                BottomMargin = 0,

                Data = var.SDocno
            };
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            switch (pTypePrint)
            {
                case "bill":
                    e.Graphics.DrawString("พิมพ์ครั้งที่ " + var.SpCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + var.SpCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString(var.SBranch, SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
                    Y += 80;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString("คลังเบิก " + var.SLocation, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

                    for (int i = 0; i < var.SdataDetail.Rows.Count; i++)
                    {
                        Y += 20;
                        e.Graphics.DrawString((i + 1).ToString() + ".(" + (Convert.ToDouble(var.SdataDetail.Rows[i]["QTY"].ToString()).ToString("#,#0.00")).ToString() + " X " +
                                          var.SdataDetail.Rows[i]["UNITID"].ToString() + ")   " +
                                          var.SdataDetail.Rows[i]["ITEMBARCODE"].ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y);
                        Y += 15;
                        e.Graphics.DrawString(var.SdataDetail.Rows[i]["SPC_ITEMNAME"].ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y);
                    }

                    Y += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + var.IBox.ToString() + "  ลัง", SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 15;
                    e.Graphics.DrawString("รวม " + var.SdataDetail.Rows.Count.ToString() + "  รายการ/" +
                        "ยอดเงิน " + (var.SumLINEAMOUNT.ToString("#,#0.00")).ToString() + "  บาท", SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 20;
                    e.Graphics.DrawString("ผู้บันทึก : " + var.SPersonSave, SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 15;
                    e.Graphics.DrawString("ผู้จัดบิล : " + var.SPersonPacking, SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 30;
                    e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    Rectangle rect1 = new Rectangle(0, Y, 280, 100);
                    StringFormat stringFormat = new StringFormat()
                    {
                        Alignment = StringAlignment.Near,
                        LineAlignment = StringAlignment.Near
                    };
                    e.Graphics.DrawString(var.SpRmk, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
                    //Y += 30
                    //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;
                case "box":
                    Y += 20;
                    e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + var.SLocation + "]", SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString(var.SBranch, SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 15;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
                    Y += 80;
                    e.Graphics.DrawString(var.SNumBox, SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 15;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;
                default:
                    break;
            }




        }
        //สำหรับพิมพ์ สลิป รหัส+ชื่อสาขา
        public static void Print_Bch(System.Drawing.Printing.PrintPageEventArgs e, string BchID, string BchName)
        {
            int Y = 0;
            Y += 20;

            e.Graphics.DrawString(BchID, new Font(new FontFamily("Tahoma"), 50), Brushes.Black, 0, Y);
            Y += 70;

            Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(BchName, new Font(new FontFamily("Tahoma"), 40), Brushes.Black, rect1, stringFormat);
        }

    }
}
