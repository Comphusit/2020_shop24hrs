﻿using System;
using System.Data;
using System.IO;
using System.Collections;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.Controllers
{
    class RunJOBClass
    {
        //ORDER BY WEB
        public static void RunOrderByWeb(string pBchId, string pBchName)
        {
            DataTable dtData = ConnectionClass.SelectSQL_Main(" RunJob_FindData '0','" + pBchId + @"' ");
            if (dtData.Rows.Count == 0) { return; }

            ArrayList sqlAX = new ArrayList();
            ArrayList sql24 = new ArrayList();

            int II = 1;//SeqNo = 0, 
            string WH = "-", maxDocno = "";

            try
            {
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    if (WH != dtData.Rows[i]["WHSoure"].ToString())
                    {
                        WH = dtData.Rows[i]["WHSoure"].ToString();
                        //SeqNo += 1;
                        II = 1;
                        maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNPO", "-", "MNPO", "1");
                        //HD
                        sqlAX.Add(string.Format(@"
                    insert into SPC_POSTOTABLE10 
                        (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                        INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
                    values('98','" + dtData.Rows[i]["EMPLID"].ToString() + @"','0','SPC','1','1','0',
                        '" + dtData.Rows[i]["WHDestination"].ToString() + @"', 
                        '" + dtData.Rows[i]["WHSoure"].ToString() + @"',
                        '" + maxDocno + @"',
                        '" + dtData.Rows[i]["DateIns"].ToString() + @"',
                        '" + maxDocno + @"', '',
                        '" + dtData.Rows[i]["EMPLID"].ToString() + @"-WEB" + @"' ) 
                    "));

                        //DT  
                        sqlAX.Add(string.Format(@"
                    insert into SPC_POSTOLINE10 
                                (RECID,REFBOXTRANS,DATAAREAID,
                                VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTDIMID,INVENTTRANSID,RECVERSION) 
                    values('1','0','SPC','" + maxDocno + @"',
                                '" + dtData.Rows[i]["DateIns"].ToString() + @"',
                                '" + II + @"',
                                '" + Convert.ToDouble(dtData.Rows[i]["QtyOrder"].ToString()) + @"',
                                '0',
                                '" + dtData.Rows[i]["ITEMID"].ToString() + @"',
                                '" + dtData.Rows[i]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                                '" + dtData.Rows[i]["UnitID"].ToString() + @"',
                                '" + dtData.Rows[i]["ITEMBARCODE"].ToString() + @"',
                                '" + dtData.Rows[i]["INVENTDIM"].ToString() + @"',
                                '" + maxDocno + "-" + Convert.ToString(i + 1) + @"',
                                '1')
                    "));
                        //'" + dtData.Rows[i]["TRANSID"].ToString() + @"',
                        sql24.Add(string.Format(@"
                        UPDATE	SHOP_WEB_PDTTRANSORDER
                        SET		ORDERSTATUS = '2',STATUSCHANGEDATE = CONVERT(VARCHAR,GETDATE(),25),REFMNPO = '" + maxDocno + @"'
                        WHERE	TRANSID = '" + dtData.Rows[i]["TRANSID"].ToString() + @"'
                    "));

                        II += 1;
                    }
                    else
                    {
                        //DT  
                        sqlAX.Add(string.Format(@"
                    insert into SPC_POSTOLINE10 
                                (RECID,REFBOXTRANS,DATAAREAID,
                                VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTDIMID,INVENTTRANSID,RECVERSION) 
                    values('1','0','SPC','" + maxDocno + @"',
                                '" + dtData.Rows[i]["DateIns"].ToString() + @"',
                                '" + II + @"',
                                '" + Convert.ToDouble(dtData.Rows[i]["QtyOrder"].ToString()) + @"',
                                '0',
                                '" + dtData.Rows[i]["ITEMID"].ToString() + @"',
                                '" + dtData.Rows[i]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                                '" + dtData.Rows[i]["UnitID"].ToString() + @"',
                                '" + dtData.Rows[i]["ITEMBARCODE"].ToString() + @"',
                                '" + dtData.Rows[i]["INVENTDIM"].ToString() + @"',
                                  '" + maxDocno + "-" + Convert.ToString(i + 1) + @"',
                                '1')
                    "));

                        sql24.Add(string.Format(@"
                        UPDATE	SHOP_WEB_PDTTRANSORDER
                        SET		ORDERSTATUS = '2',STATUSCHANGEDATE = CONVERT(VARCHAR,GETDATE(),25),REFMNPO = '" + maxDocno + @"'
                        WHERE	TRANSID = '" + dtData.Rows[i]["TRANSID"].ToString() + @"'
                    "));

                        II += 1;
                    }
                }

                string T = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX);
                if (T != "")
                {
                    Class.ConfigClass.SendEmail("RunOrderByWeb", T, dtData.Rows.Count, pBchId, pBchName);
                }
            }
            catch (Exception ex)
            {
                Class.ConfigClass.SendEmail("RunOrderByWeb", ex.Message, dtData.Rows.Count, pBchId, pBchName);
            }
        }

        ////ORDER BY MNPO
        //public static void RunOrderByMNPO(string tblHD, string pBchId, string pBchName)
        //{
        //    //DataTable dtData = ConnectionClass.SelectSQL_Main(" RunJob_FindData '1','" + pBchId + @"' ");
        //    //if (dtData.Rows.Count == 0) return;


        //    //for (int i = 0; i < dtData.Rows.Count; i++)
        //    //{
        //    //    DataTable dtForSend = Class.POClass.GetDataDetailMNPO_ForSendAX(dtData.Rows[i]["MNPODocNo"].ToString());
        //    //    if (dtForSend.Rows.Count == 0)
        //    //    {
        //    //        return;
        //    //    }
        //    //    ArrayList sqlUp = new ArrayList
        //    //    {
        //    //      string.Format(@" 
        //    //                UPDATE  "+ tblHD + @"  
        //    //                SET     MNPOStaPrcDoc = '1',MNPOStaApvDoc = '1',
        //    //                        MNPODateApv = CONVERT(VARCHAR,GETDATE(),23),MNPOTimeApv = CONVERT(VARCHAR,GETDATE(),24),
        //    //                        MNPOWhoApv = MNPOUserCode ,MNPOWhoNameApv = MNPOUserNameCode 
        //    //                WHERE   MNPODocNo = '" + dtData.Rows[i]["MNPODocNo"].ToString() + @"'   ")
        //    //    };
        //    //    string T = AX_SendData.Save_POSTOLINE10(sqlUp, dtForSend, "98");
        //    //    if (T != "")
        //    //    {
        //    //        Class.ConfigClass.SendEmail("RunOrderByMNPO", T, dtData.Rows.Count, pBchId, pBchName);
        //    //    }
        //    //}

        //}

        //RECIVE BOX
        public static void RunReciveBox(string pBchID, string pBchName)
        {
            //Update ลังซ้ำก่อน จะได้ไม่มีปัญหาในการ Insert
            string sqlUpBox = $@"UPDATE  SHOP_RECIVEBOX SET Stadoc_Process = '1',Stadoc_AX = '1',ProcessDATEIN = CONVERT(VARCHAR,GETDATE(),23),ProcessTIMEIN = CONVERT(VARCHAR,GETDATE(),24)
                WHERE   BOXNUMBER IN 
                (
                SELECT	BOXNUMBER 
                FROM	SHOP_RECIVEBOX WITH (NOLOCK) 
                WHERE	Stadoc_Process = '0' AND Stadoc_AX = '0'
		                AND BOXBRANCH = '{pBchID}'
		                AND BOXNUMBER	IN (SELECT	BOXID	FROM	SPC708SRV.AX50SP1_SPC.dbo.SPC_TRANSFERUPDRECEIVE WITH (NOLOCK) )
                GROUP BY BOXNUMBER 
                )";
            ConnectionClass.ExecuteSQL_Main(sqlUpBox);

            //ดูจำนวนลังที่ยังไม่ได้ส่งสต็อก
            DataTable dtData = ConnectionClass.SelectSQL_Main(" RunJob_FindData '2','" + pBchID + @"' ");
            if (dtData.Rows.Count == 0) { return; }

            ArrayList sqlAx = new ArrayList();
            ArrayList sql24 = new ArrayList();
            try
            {
                foreach (DataRow item in dtData.Rows)
                {
                    sqlAx.Add(@"
                    INSERT INTO SPC_TRANSFERUPDRECEIVE (
                                RECVERSION,RECID,DATAAREAID,VOUCHERID, BOXID, EMPLID,RECEIVEDATE, REMARKS, INVENTLOCATIONIDTO) 
                    VALUES      ('1','" + item["BOXNUMBER"].ToString() + @"','SPC','" + item["DOCUMENTNUM"].ToString() + @"',
                                '" + item["BOXNUMBER"].ToString() + @"',
                                '" + item["WHOIN"].ToString() + @"','" + item["DATEIN"].ToString() + @"',
                                '" + item["REMARKS"].ToString() + @"-PC','" + item["BOXBRANCH"].ToString() + @"')
                    ");

                    sql24.Add(@"
                    UPDATE  SHOP_RECIVEBOX
                    SET     Stadoc_Process = '1',Stadoc_AX = '1',ProcessDATEIN = CONVERT(VARCHAR,GETDATE(),23),ProcessTIMEIN = CONVERT(VARCHAR,GETDATE(),24)
                    WHERE   BOXNUMBER = '" + item["BOXNUMBER"].ToString() + @"'
                    ");
                    // AND DOCUMENTNUM = '" + item["DOCUMENTNUM"].ToString() + @"'
                }

                string result = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAx);
                if (result != "")
                {
                    ConfigClass.SendEmail("RunReciveBox", result, dtData.Rows.Count, pBchID, pBchName);
                }
            }
            catch (Exception ex)
            {
                ConfigClass.SendEmail("RunReciveBox", ex.Message, dtData.Rows.Count, pBchID, pBchName);
            }

        }

        ////โอนลังจาก Txt เข้า ReciveBoxOffline
        //public static void RunTxtToReciveBox(string pBchID, string pBchName)
        //{
        //    string pathTxt = @"\\192.168.100.27\Minimart\SHOP24HRS\";

        //    DirectoryInfo DirInfo = new DirectoryInfo(pathTxt);
        //    if (DirInfo.Exists == false) return;

        //    FileInfo[] FilesTxt = DirInfo.GetFiles(pBchID + @"*.txt", SearchOption.AllDirectories);
        //    if (FilesTxt.Length == 0) { return; }

        //    DataTable dtTxt = new DataTable();
        //    dtTxt.Columns.Add("FullName"); dtTxt.Columns.Add("TxtName");

        //    try
        //    {
        //        foreach (var item in FilesTxt)
        //        {
        //            long length = new System.IO.FileInfo(item.FullName).Length;
        //            if (length == 0)
        //            {
        //                try
        //                {
        //                    File.Delete(item.FullName);
        //                }
        //                catch (Exception exDeleteFileSize0)
        //                {
        //                    ConfigClass.SendEmail("RunTxtToReciveBox", $@"DeleteFileSize0-{ exDeleteFileSize0.Message}{Environment.NewLine}{item.FullName}", 1, pBchID, pBchName);
        //                }
        //            }
        //            else
        //            {
        //                dtTxt.Rows.Add(item.FullName, item.Name);
        //            }
        //        }
        //    }
        //    catch (Exception exInsertFileToDataTable)
        //    {
        //        ConfigClass.SendEmail("RunTxtToReciveBox", "InsertFileTxtToDataTable-" + exInsertFileToDataTable.Message, FilesTxt.Length, pBchID, pBchName);
        //    }


        //    //ในกรณีไม่มี File หรือ File เสียหมด
        //    if (dtTxt.Rows.Count == 0) return;

        //    ArrayList sqlInUp = new ArrayList();
        //    try
        //    {
        //        foreach (DataRow itemInput in dtTxt.Rows)
        //        {
        //            using (StreamReader sr = new StreamReader(itemInput["FullName"].ToString()))
        //            {
        //                while (sr.Peek() >= 0)
        //                {
        //                    try
        //                    {
        //                        string[] AA = sr.ReadLine().Split(',');
        //                        //check ลังซ้ำก่อน
        //                        if (CheckReciveBox(AA[0]) == 0)
        //                        {
        //                            DataTable dtBox = BOXClass.Find_BoxDetail(AA[0]);
        //                            if (dtBox.Rows.Count > 0)
        //                            {
        //                                string staRecive = "D";//กรณีรับลังสาขาตัวเอง D แต่ถ้ารับลังสาขาอื่นคือ C เปลี่ยนเปน Y
        //                                if (dtBox.Rows[0]["INVOICEACCOUNT"].ToString() != AA[1])
        //                                {
        //                                    staRecive = "Y";
        //                                }
        //                                sqlInUp.Add(@"INSERT INTO  SHOP_RECIVEBOX (
        //                                     BOXBRANCH,BOXNUMBER,DOCUMENTNUM,
        //                                     SHIPMENTID,MACHINENAME, Stadoc_Recive, Stadoc_Process, 
        //                                     BOXTYPE,BOXBARCODE,
        //                                     BOXSPCITEMNAME,BOXQTY,
        //                                     BOXUITID,REMARK,
        //                                     DATEIN,TIMEIN,WHOIN,BOXBRANCHBILL,BOXQTY_Send) values (
        //                                     '" + AA[1] + @"','" + AA[0] + @"','" + dtBox.Rows[0]["INVOICEID"].ToString() + @"',
        //                                     '" + dtBox.Rows[0]["SHIPMENTID"].ToString() + @"','" + AA[2] + @"','" + staRecive + @"','0',
        //                                     '" + dtBox.Rows[0]["BOXTYPE"].ToString() + @"','" + dtBox.Rows[0]["ITEMBARCODE"].ToString() + @"',
        //                                     '" + dtBox.Rows[0]["ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"','" + dtBox.Rows[0]["QTY"].ToString() + @"',  
        //                                     '" + dtBox.Rows[0]["SALESUNIT"].ToString() + @"','BoxOffline',
        //                                     '" + AA[3] + @"','" + AA[4] + @"','" + SystemClass.SystemUserID_M + @"','" + dtBox.Rows[0]["INVOICEACCOUNT"].ToString() + @"','" + dtBox.Rows[0]["QTY"].ToString() + @"'
        //                                    )");
        //                            }
        //                        }

        //                    }
        //                    catch (Exception exStreamReader)
        //                    {
        //                        ConfigClass.SendEmail("RunTxtToReciveBox", $@"ReadFile-{exStreamReader.Message}{Environment.NewLine}{ sr.ReadLine()}", dtTxt.Rows.Count, pBchID, pBchName);
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception exReadFile)
        //    {
        //        ConfigClass.SendEmail("RunTxtToReciveBox", "ReadFile-" + exReadFile.Message, dtTxt.Rows.Count, pBchID, pBchName);
        //    }

        //    if (sqlInUp.Count > 0)
        //    {
        //        string result = ConnectionClass.ExecuteSQL_ArrayMain(sqlInUp);
        //        if (result == "")
        //        {
        //            foreach (DataRow itemDel in dtTxt.Rows)
        //            {
        //                try
        //                {
        //                    File.Copy(itemDel[0].ToString(), @"\\192.168.100.27\Minimart\SHOP24HRS\TextOldFile\R_" + itemDel[1].ToString(), true);
        //                    File.Delete(itemDel[0].ToString());
        //                }
        //                catch (Exception exDeleteFileCopy)
        //                {
        //                    ConfigClass.SendEmail("RunTxtToReciveBox", "DeleteFileCopy-" + exDeleteFileCopy.Message + Environment.NewLine + itemDel[0].ToString(),
        //                        dtTxt.Rows.Count, pBchID, pBchName);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ConfigClass.SendEmail("RunTxtToReciveBox", "InsertDataDB" + result, sqlInUp.Count, pBchID, pBchName);
        //        }
        //    }
        //    else
        //    {
        //        foreach (DataRow itemDel in dtTxt.Rows)
        //        {
        //            try
        //            {
        //                File.Copy(itemDel[0].ToString(), @"\\192.168.100.27\Minimart\SHOP24HRS\TextOldFile\R_" + itemDel[1].ToString(), true);
        //                File.Delete(itemDel[0].ToString());
        //            }
        //            catch (Exception exDeleteFileCopy)
        //            {
        //                ConfigClass.SendEmail("RunTxtToReciveBox",
        //                    $@"DeleteFileCopy-" + exDeleteFileCopy.Message + Environment.NewLine + itemDel[0].ToString(),
        //                    dtTxt.Rows.Count, pBchID, pBchName);
        //            }
        //        }
        //    }
        //}

        //check box
        public static int CheckReciveBox(string boxID)
        {
            string sql = $@"
                SELECT	*	FROM	SHOP_RECIVEBOX WITH (NOLOCK)	WHERE	BOXNUMBER = '{boxID}'
            ";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }

        ////Get Taff
        //public static void GetTaff()
        //{
        //    FileInfo[] FilesImage;
        //    DirectoryInfo DirInfo = new DirectoryInfo($@"\\spc160bck\Photos\20210828");

        //    FilesImage = DirInfo.GetFiles("*.JPG", SearchOption.AllDirectories);
        //    if (FilesImage.Length == 0)
        //    {
        //        return;
        //    }
        //    ArrayList sql = new ArrayList();
        //    //01525_02_180312
        //    //1703030  210828 0015 986 I
        //    foreach (FileInfo item in FilesImage)
        //    {
        //        string[] AA = item.Name.Replace(".jpg", "").Split('_');
        //        sql.Add($@"
        //        INSERT INTO AAA(A,B,C,D,E)
        //        values ('{AA[0]}','210828','{AA[2].Substring(0, 4)}','913','O')
        //        ");
        //    }
        //    string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
        //    Class.MsgBoxClass.MsgBoxShow_SaveStatus(T);
        //}
        ////
        //public static void RunCstCountBill()
        //{
        //    ArrayList sqlIn = new ArrayList();
        //    string sql = string.Format(@"
        //        SELECT	ACCOUNTNUM,PW_IDCARD,PW_PWCARD,PW_QTYBill,PW_BRANCH_IN,CONVERT(VARCHAR,PW_DATETIME_IN,25) AS PW_DATETIME_IN,CONVERT(VARCHAR,PW_DATETIME_OUT,25) AS PW_DATETIME_OUT
        //        FROM	SHOP2013.dbo.Shop_TimeKeeper WITH (NOLOCK)
        //           INNER JOIN SHOP2013TMP.dbo.CUSTTABLE WITH (NOLOCK) ON Shop_TimeKeeper.PW_PWCARD = CUSTTABLE.NameAlias
        //        WHERE	PW_DATE_IN = '2021-01-29'
        //          AND PW_BRANCH_IN BETWEEN 'MN001' AND 'MN998'
        //        ORDER BY PW_DATETIME_IN
        //    ");
        //    DataTable dtEmp = ConnectionClass.SelectSQL_Main(sql);
        //    for (int i = 0; i < dtEmp.Rows.Count; i++)
        //    {
        //        string cst = dtEmp.Rows[i]["ACCOUNTNUM"].ToString();
        //        string D1 = dtEmp.Rows[i]["PW_DATETIME_IN"].ToString();
        //        string D2 = dtEmp.Rows[i]["PW_DATETIME_OUT"].ToString();
        //        string Emp = dtEmp.Rows[i]["PW_IDCARD"].ToString();

        //        //string sqlPos = string.Format(@"
        //        //    SELECT	ISNULL(COUNT(INVOICEACCOUNT),0) AS COUNT_BILL
        //        //    FROM	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK) 
        //        //    WHERE	INVOICEACCOUNT = '" + cst + @"'
        //        //      AND CREATEDDATETIME BETWEEN '" + D1 + @"' and '" + D2 + @"'
        //        //");
        //        // DataTable dtPos = ConnectionClass.SelectSQL_POSRetail707(sqlPos);

        //        string sqlPos = string.Format(@"
        //            SELECT	ISNULL(COUNT(INVOICEACCOUNT),0) AS COUNT_BILL
        //            FROM	XXX_POSTABLE WITH (NOLOCK) 
        //            WHERE	INVOICEACCOUNT = '" + cst + @"'
        //              AND CREATEDDATETIME BETWEEN '" + D1 + @"' and '" + D2 + @"'
        //        ");
        //        DataTable dtPos = ConnectionClass.SelectSQL_POSRetail707(sqlPos);
        //        if (dtPos.Rows.Count > 0)
        //        {
        //            if (Convert.ToDouble(dtPos.Rows[0]["COUNT_BILL"].ToString()) > 0)
        //            {
        //                sqlIn.Add(string.Format(@"
        //                UPDATE	SHOP2013.dbo.Shop_TimeKeeper	
        //                SET	    PW_QTYBill = '" + dtPos.Rows[0]["COUNT_BILL"].ToString() + @"'
        //                WHERE   PW_IDCARD = '" + Emp + @"'
        //            "));
        //            }
        //        }
        //    }

        //    if (sqlIn.Count > 0)
        //    {
        //        Class.MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_ArrayMain(sqlIn));
        //    }


        //}
    }
}
