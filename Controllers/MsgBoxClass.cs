﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Controllers
{
    class MsgBoxClass
    {
        //MyJeeds ข้อความแสดงการบันทึกเรียบร้อยหรือไม่เรียบร้อย : _pStr  = ข้อความที่ error
        public static void MsgBoxShow_SaveStatus(string _pStr)
        {
            string T;
            if (_pStr == "")
            {
                T = "บันทึกข้อมูลเรียบร้อย.";
                MessageBox.Show(T,
                                 SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                T = "ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง. " + Environment.NewLine + " [" + _pStr + "] ";
                MessageBox.Show(T,
                                 SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        //MyJeeds ให้เช็คข้อมูลก่อนการบันทึก
        public static void MsgBoxShow_CheckDataBeforeSave(string pFieldWarning)
        {
            MessageBox.Show("เช็คข้อมูล [" + pFieldWarning + "] ให้เรียบร้อย ก่อนการบันทึก ลองใหม่อีกครั้ง.",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        //MyJeeds  แสดงข้อความค้นหาข้อมูลไม่เจอ
        public static void MsgBoxShow_FindRecordNoData(string _pStr)
        {
            MessageBox.Show("ไม่พบข้อมูลที่ค้นหา" + Environment.NewLine + _pStr + Environment.NewLine + "ลองใหม่อีกครั้ง.",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        //MyJeeds  ระบุุข้อมูลก่อนค้นหา แบบ Enter
        public static void MsgBoxShow_InputDataBeforeEnter(string pFieldWarning)
        {
            MessageBox.Show("ระบุข้อมูล [" + pFieldWarning + "] ที่ต้องการค้นหา ก่อน Enter ลองใหม่อีกครั้ง.",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        //MyJeeds ระบุข้อมูลก่อนการบันทึก  
        public static void MsgBoxShow_InputDataBeforeForInsert(string pFieldWarning)
        {
            MessageBox.Show("ระบุข้อมูล [" + pFieldWarning + "] ให้เรียบร้อยก่อนการบันทึก ลองใหม่อีกครั้ง.",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        //MyJeeds ระบุข้อมูลเพื่อยืนยันการลบ
        public static DialogResult MsgBoxShow_ConfirmDelete(string _pStr)
        {
            return
           MessageBox.Show("ยืนยันการลบข้อมูล [" + _pStr + "] ที่ระบุ ?", SystemClass.SystemHeadprogram,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        }
        //MyJeeds ระบุข้อมูลเพื่อยืนยันการบันทึก
        public static DialogResult MsgBoxShow_ConfirmInsert(string _pStr)
        {
            return
           MessageBox.Show("ยืนยันการบันทึกข้อมูล [" + _pStr + "] ที่ระบุ ?", SystemClass.SystemHeadprogram,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        }
        //MyJeeds ระบุข้อมูลเพื่อยืนยันการแก้ไข
        public static DialogResult MsgBoxShow_ConfirmEdit(string _pStr)
        {
            return
           MessageBox.Show("ยืนยันการแก้ไขข้อมูล [" + _pStr + "] ที่ระบุ ?", SystemClass.SystemHeadprogram,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        }
        //MyJeeds ระบุข้อมูลเพื่อยืนยัน+อนุมัติบิล+ยกเลิก
        public static DialogResult MsgBoxShow_Bill_ComfirmStatus(string _pBillDocno, string _pStatus)
        {
            return
           MessageBox.Show("ต้องการ" + _pStatus + " บิลเลขที่ " + _pBillDocno + Environment.NewLine + "เมื่อดำเนินการแล้วไม่สามารถแก้ไขข้อมูลใดๆได้อีก ?", SystemClass.SystemHeadprogram,
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        }
        //MyJeeds ข้อความแสดงการ ยืนยัน+อนุมัติบิล+ยกเลิก เรียบร้อยหรือไม่ _pStr คือ ข้อความที่ error _pBillDocno คือ เลขที่บิล
        public static void MsgBoxShow_Bill_SaveStatus(string _pStr, string _pBillDocno, string _pStatus)
        {
            string T;
            if (_pStr == "")
            {
                T = _pStatus + "บิลเลขที่ " + _pBillDocno + @" เรียบร้อย.";
                MessageBox.Show(T,
                                 SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                T = "ไม่สามารถ" + _pStatus + @"บิลเลขที่ " + _pBillDocno + @" ได้ ลองใหม่อีกครั้ง. " + Environment.NewLine + " [" + _pStr + "] ";
                MessageBox.Show(T,
                                 SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        //MyJeeds เลือกข้อมูลให้เรียบร้อย
        public static void MsgBoxShow_ChooseDataWarning(string pFieldWarning)
        {
            MessageBox.Show("เลือกข้อมูล [" + pFieldWarning + "] ให้เรียบร้อยก่อนกดเลือกทุกครั้ง ลองใหม่อีกครั้ง.",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        //MyJeeds เตือนการ Input Numbers
        public static void MsgBoxShow_InputNumbersOnly(string pFieldWarning)
        {
            MessageBox.Show("ข้อมูลที่ช่องที่ระบุ ต้องเป็นตัวเลขเท่านั้น เช็คใหม่อีกครั้ง. " + Environment.NewLine + " [" + pFieldWarning + "]",
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }



        public static void MsgBoxShowButtonOk_Warning(string Message)
        {
            MessageBox.Show(Message,
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
        public static void MsgBoxShowButtonOk_Error(string Message)
        {
            MessageBox.Show(Message,
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);

        }
        public static void MsgBoxShowButtonOk_Imformation(string Message)
        {
            MessageBox.Show(Message,
                             SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        public static DialogResult MsgBoxShowYesNo_DialogResult(string Message)
        {
            DialogResult dialogResult = MessageBox.Show(Message, SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            return dialogResult;

        }

        public static DialogResult MsgBoxShowYesNoCencel_DialogResult(string Message)
        {
            DialogResult dialogResult = MessageBox.Show(Message, SystemClass.SystemHeadprogram, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            return dialogResult;

        }


    }
}
