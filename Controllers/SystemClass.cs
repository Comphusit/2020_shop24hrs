﻿
using System.Drawing;

namespace PC_Shop24Hrs.Controllers
{
    class SystemClass
    {
        // เกี่ยวกับ user
        public static string SystemUserID = "";
        public static string SystemUserID_M = "";
        public static string SystemUserName = "";
        public static string SystemUserNameShort = "";
        public static string SystemUserPositionID = "";
        public static string SystemUserPositionName = "";
        public static string SystemDptID = "";
        public static string SystemDptName = "";
        public static string SystemUserLevelMinimart = "0";//สิทธิ์เพิ่มเติมของมินิมาร์ท EMP_TRANSTOCK

        // เกี่ยวกัยสาขา
        public static string SystemBranchID = "";// รหัสสาขาที่เข้าใช้งาน
        public static string SystemBranchName = "";// ชื่อสาขาที่ใช้งาน
        public static string SystemBranchPrice = ""; // ระดับราคาของสาขาที่เข้าใช้งาน

        public static string SystemBranchServer = ""; // ชื่อ server สาขาที่ใช้
        public static string SystemBranchOutPhuket = ""; // สาขาในจังหวัด 1 สาขาต่างจังหวัด 0
        public static string SystemBranchInventID = "";// คลังของสาขาที่ใช้งาน
        public static string SystemBranchStaShelf = "0";// คลังของสาขาที่ใช้งาน
        //public static string SystemServerIP = ""; // IP เครื่อง


        // เกี่ยวกับเครื่อง+IP
        public static string SystemPcName = ""; // ชื่อ SPC
        public static string SystemPcIp = ""; // IP เครื่อง

        //เกี่ยวกับการ Update โปรแกรม
        public static string SystemVersionShop24Hrs_SUPC = ""; // เวอร์ชั่นการใช้งาน SUPC
        public static string SystemVersionShop24Hrs_SHOP = ""; // เวอร์ชั่นการใช้งาน SHOP
        public static string SystemPathUpdateShop24Hrs_SUPC = ""; // เวอร์ชั่นการใช้งาน SUPC
        public static string SystemPathUpdateShop24Hrs_SHOP = ""; // เวอร์ชั่นการใช้งาน SHOP
        public static string SystemVersion = "2.0.0.8";//Version
        public static string SystemVersionShop24HrsCurrent_SUPC = "240"; // เวอร์ชั่นการใช้งาน SUPC Current
        public static string SystemVersionShop24HrsCurrent_SHOP = "133"; // เวอร์ชั่นการใช้งาน SHOP Current
        public static string SystemAX2012 = "0";

        // เกี่ยวกับการตั้งค่าพิเศษ 
        public static string SystemComMinimart = "";// สำหรับ D179 ComMN
        public static string SystemComService = ""; // สำหรับ D179 COmService
        public static string SystemComProgrammer = ""; // ในกรณีที่เป็นโปรแกรมเมอร์
        //การตั้งค่าหลักที่ไม่เปลี่ยนแปลง
        public static string SystemHeadprogram = "Shop24Hrs."; // ชื่อโปแกรมหลัก คือ Shop24Hrs.
        public static string TmpAX = "AX60CU13.dbo."; // Database AX ใน SPC119SRV

        //Set Font DGV
        public static Font SetFontGernaral = new Font(new FontFamily("Tahoma"), 10.0f);
        public static Font SetFontGernaral_Bold = new Font(new FontFamily("Tahoma"), 10.0f, FontStyle.Bold);
        public static Font SetFont12 = new Font(new FontFamily("Tahoma"), 12.0f);
        public static Font printFont = new Font("Tahoma", 10, System.Drawing.FontStyle.Regular);
        public static Font printFont15 = new Font(new FontFamily("Tahoma"), 15);

        //แผนกจัดซื้ออาหารสด MNOG
        public static string SystemPurchaseFresh = "";
        //บาร์โค้ดลดราคาอาหารกล่อง
        public static string SystemBarcodeType17 = "";
        public static string SystemBarcodeType19 = "";
        public static string SystemBarcodeType20 = "";
        public static string SystemBarcodeType32 = "";
       

    }
}
