﻿using System.Data;

namespace PC_Shop24Hrs.Controllers
{
    class ConnectionControl
    {
        //MyJeeds set connection ทั้งหมด 
        public static void GetConnectionString()
        {
            string sql = @" SELECT	*	FROM	SHOP_CONFIGDB WITH (NOLOCK)  ";

            DataTable dtConnect = ConnectionClass.SelectSQL_Main(sql);
            if (dtConnect.Rows.Count > 0)
            {
                //SERVER AX SPC708SRV
                DataRow[] ConMainAX = dtConnect.Select("VARIABLENAME = 'ConMainAX' ");
                if (ConMainAX.Length > 0)
                {

                    IpServerConnectClass.ConMainAX = @"Data Source=" + ConMainAX[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConMainAX[0]["CATALOG"] + "; " +
                        "User Id=" + ConMainAX[0]["USERNAME"] + ";Password=" + ConMainAX[0]["PASSWORD"];
                }

                //SERVER POSRETAIL SPC707SRV
                DataRow[] ConMainRatail707 = dtConnect.Select("VARIABLENAME = 'ConMainRatail707' ");
                if (ConMainRatail707.Length > 0)
                {

                    IpServerConnectClass.ConMainRatail707 = @"Data Source=" + ConMainRatail707[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConMainRatail707[0]["CATALOG"] + "; " +
                        "User Id=" + ConMainRatail707[0]["USERNAME"] + ";Password=" + ConMainRatail707[0]["PASSWORD"];
                }

                //SERVER FINGER SPC803SRV [Ditital]
                DataRow[] ConFinger_Digital = dtConnect.Select("VARIABLENAME = 'ConFinger_Digital' ");
                if (ConFinger_Digital.Length > 0)
                {
                    IpServerConnectClass.ConFinger_Digital = @"Data Source=" + ConFinger_Digital[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConFinger_Digital[0]["CATALOG"] + "; " +
                        "User Id=" + ConFinger_Digital[0]["USERNAME"] + ";Password=" + ConFinger_Digital[0]["PASSWORD"];
                }

                //SERVER HR-ONLINE SPC160SRV
                DataRow[] Con160HROS = dtConnect.Select("VARIABLENAME = 'ConHROS' ");
                if (Con160HROS.Length > 0)
                {
                    IpServerConnectClass.ConHROS = @"Data Source=" + Con160HROS[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + Con160HROS[0]["CATALOG"] + "; " +
                        "User Id=" + Con160HROS[0]["USERNAME"] + ";Password=" + Con160HROS[0]["PASSWORD"];
                }

                //SERVER AX REPORT SPC704SRV
                DataRow[] ConMainReportAX = dtConnect.Select("VARIABLENAME = 'ConMainReportAX' ");
                if (ConMainReportAX.Length > 0)
                {
                    IpServerConnectClass.ConMainReportAX = @"Data Source=" + ConMainReportAX[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConMainReportAX[0]["CATALOG"] + "; " +
                        "User Id=" + ConMainReportAX[0]["USERNAME"] + ";Password=" + ConMainReportAX[0]["PASSWORD"];
                }

                //SERVER SEND SCALE SUPC SPC707SRV
                DataRow[] ConScaleSUPC = dtConnect.Select("VARIABLENAME = 'ConScaleSUPC' ");
                if (ConScaleSUPC.Length > 0)
                {
                    IpServerConnectClass.ConScaleSUPC = @"Data Source=" + ConScaleSUPC[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConScaleSUPC[0]["CATALOG"] + "; " +
                        "User Id=" + ConScaleSUPC[0]["USERNAME"] + ";Password=" + ConScaleSUPC[0]["PASSWORD"];
                }

                //SERVER VENSALE MP SPC805SRV
                DataRow[] ConMP = dtConnect.Select("VARIABLENAME = 'ConMP' ");
                if (ConMP.Length > 0)
                {
                    IpServerConnectClass.ConMP = @"Data Source=" + ConMP[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConMP[0]["CATALOG"] + "; " +
                        "User Id=" + ConMP[0]["USERNAME"] + ";Password=" + ConMP[0]["PASSWORD"];
                }

                //SERVER YA N'COP ALLBRANH SPC196SRV
                DataRow[] ConMYA = dtConnect.Select("VARIABLENAME = 'ConMYA' ");
                if (ConMYA.Length > 0)
                {
                    IpServerConnectClass.ConMYA = @"Data Source=" + ConMYA[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConMYA[0]["CATALOG"] + "; " +
                        "User Id=" + ConMYA[0]["USERNAME"] + ";Password=" + ConMYA[0]["PASSWORD"];
                }

                //SERVER TRANSPORT RA-MA SPC112SRV
                DataRow[] ConRA = dtConnect.Select("VARIABLENAME = 'ConRA' ");
                if (ConRA.Length > 0)
                {
                    IpServerConnectClass.ConRA = @"Data Source=" + ConRA[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConRA[0]["CATALOG"] + "; " +
                        "User Id=" + ConRA[0]["USERNAME"] + ";Password=" + ConRA[0]["PASSWORD"];
                }

                //SERVER REPORT AX PROCESS SPC705SRV
                DataRow[] ConMainReportProcess = dtConnect.Select("VARIABLENAME = 'ConMainReportProcess' ");
                if (ConMainReportProcess.Length > 0)
                {
                    IpServerConnectClass.ConMainReportProcess = @"Data Source=" + ConMainReportProcess[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConMainReportProcess[0]["CATALOG"] + "; " +
                        "User Id=" + ConMainReportProcess[0]["USERNAME"] + ";Password=" + ConMainReportProcess[0]["PASSWORD"];
                }


                //SERVER TRANSPORT RA-MA SPC112SRV
                DataRow[] Con803SRV = dtConnect.Select("VARIABLENAME = 'Con803SRV' ");
                if (Con803SRV.Length > 0)
                {
                    IpServerConnectClass.Con803SRV = @"Data Source=" + Con803SRV[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + Con803SRV[0]["CATALOG"] + "; " +
                        "User Id=" + Con803SRV[0]["USERNAME"] + ";Password=" + Con803SRV[0]["PASSWORD"];
                }

                //SERVER เงินเดือน
                DataRow[] Con701SRV = dtConnect.Select("VARIABLENAME = 'Con701SRV' ");
                if (Con701SRV.Length > 0)
                {
                    IpServerConnectClass.Con701SRV = @"Data Source=" + Con701SRV[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + Con701SRV[0]["CATALOG"] + "; " +
                        "User Id=" + Con701SRV[0]["USERNAME"] + ";Password=" + Con701SRV[0]["PASSWORD"];
                }
                //SERVER SUPC2014 CheckAsset
                DataRow[] ConSUPC2014 = dtConnect.Select("VARIABLENAME = 'ConSUPC2014' ");
                if (ConSUPC2014.Length > 0)
                {
                    IpServerConnectClass.ConSUPC2014 = @"Data Source=" + ConSUPC2014[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConSUPC2014[0]["CATALOG"] + "; " +
                        "User Id=" + ConSUPC2014[0]["USERNAME"] + ";Password=" + ConSUPC2014[0]["PASSWORD"];
                }
                //SERVER SupcAndroid
                DataRow[] ConSupcAndroid = dtConnect.Select("VARIABLENAME = 'ConSupcAndroid' ");
                if (ConSupcAndroid.Length > 0)
                {
                    IpServerConnectClass.ConSupcAndroid = @"Data Source=" + ConSupcAndroid[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + ConSupcAndroid[0]["CATALOG"] + "; " +
                        "User Id=" + ConSupcAndroid[0]["USERNAME"] + ";Password=" + ConSupcAndroid[0]["PASSWORD"];
                }

                //SERVER SPC_Transport
                DataRow[] Con112SRV = dtConnect.Select("VARIABLENAME = 'Con112SRV' ");
                if (Con112SRV.Length > 0)
                {
                    IpServerConnectClass.Con112SRV = @"Data Source=" + Con112SRV[0]["SERVERIP"] + "; " +
                        "Initial Catalog=" + Con112SRV[0]["CATALOG"] + "; " +
                        "User Id=" + Con112SRV[0]["USERNAME"] + ";Password=" + Con112SRV[0]["PASSWORD"];
                }

                //VERSION SHOP24HRS SUPC. 
                DataRow[] SystemVersionShop24Hrs_SUPC = dtConnect.Select("VARIABLENAME = 'SystemVersionShop24Hrs_SUPC' ");
                if (SystemVersionShop24Hrs_SUPC.Length > 0)
                {
                    SystemClass.SystemVersionShop24Hrs_SUPC = SystemVersionShop24Hrs_SUPC[0]["USERNAME"].ToString();
                    SystemClass.SystemPathUpdateShop24Hrs_SUPC = SystemVersionShop24Hrs_SUPC[0]["CATALOG"].ToString();
                }

                //VERSION SHOP24HRS SUPC. 
                DataRow[] SystemVersionShop24Hrs_SHOP = dtConnect.Select("VARIABLENAME = 'SystemVersionShop24Hrs_SHOP' ");
                if (SystemVersionShop24Hrs_SHOP.Length > 0)
                {
                    SystemClass.SystemVersionShop24Hrs_SHOP = SystemVersionShop24Hrs_SHOP[0]["USERNAME"].ToString();
                    SystemClass.SystemPathUpdateShop24Hrs_SHOP = SystemVersionShop24Hrs_SHOP[0]["CATALOG"].ToString();
                }

                //Set Server Image Gernaral
                DataRow[] pServerIpImg = dtConnect.Select("VARIABLENAME = 'pServerIpImg' ");
                if (pServerIpImg.Length > 0)
                {

                    PathImageClass.pServerIpImg = pServerIpImg[0]["SERVERIP"].ToString();
                }

                //Set Server Image Vender
                DataRow[] pServerIpImgVender = dtConnect.Select("VARIABLENAME = 'pServerIpImgVender' ");
                if (pServerIpImgVender.Length > 0)
                {

                    PathImageClass.pServerIpImgVender = pServerIpImgVender[0]["SERVERIP"].ToString();
                }
                //PATH pPathCAMERA
                DataRow[] pPathCAMERA = dtConnect.Select("VARIABLENAME = 'pPathCAMERA' ");
                if (pPathCAMERA.Length > 0)
                {
                    PathImageClass.pPathCAMERA = pPathCAMERA[0]["SERVERIP"].ToString();
                }

                //PATH pPathRepairRA  
                DataRow[] pPathRepairRA = dtConnect.Select("VARIABLENAME = 'pPathRepairRA' ");
                if (pPathRepairRA.Length > 0)
                {
                    PathImageClass.pPathRepairRA = pPathRepairRA[0]["SERVERIP"].ToString();
                }
                //pPathBOXBillEdit
                DataRow[] pPathBOXBillEdit = dtConnect.Select("VARIABLENAME = 'pPathBOXBillEdit' ");
                if (pPathBOXBillEdit.Length > 0)
                {
                    PathImageClass.pPathBOXBillEdit = pPathBOXBillEdit[0]["SERVERIP"].ToString();
                }
                //pPathBOXNotScan บิลพาเลท
                DataRow[] pPathBOXNotScan = dtConnect.Select("VARIABLENAME = 'pPathBOXNotScan' ");
                if (pPathBOXNotScan.Length > 0)
                { PathImageClass.pPathBOXNotScan = pPathBOXNotScan[0]["SERVERIP"].ToString(); }

                //pPathSupc บิลส่งของ LO
                DataRow[] pPathSupc = dtConnect.Select("VARIABLENAME = 'pPathSupc' ");
                if (pPathSupc.Length > 0)
                { PathImageClass.pPathSupc = pPathSupc[0]["SERVERIP"].ToString(); }

                //PathImageClass.pPathSH_CarCloseNewLO = PathImageClass.pServerIpImg + @"\ImageMinimark\LO\";//รูปของ LO ทั้งหมด
                DataRow[] pPathSH_CarCloseNewLO = dtConnect.Select("VARIABLENAME = 'pPathSH_CarCloseNewLO' ");
                if (pPathSH_CarCloseNewLO.Length > 0)
                { PathImageClass.pPathSH_CarCloseNewLO = pPathSH_CarCloseNewLO[0]["SERVERIP"].ToString(); }

                //pPathCarwash
                DataRow[] pPathCarwash = dtConnect.Select("VARIABLENAME = 'pPathCarwash' ");
                if (pPathCarwash.Length > 0)
                { PathImageClass.pPathCarwash = pPathCarwash[0]["SERVERIP"].ToString(); }

                //pPathBillUNew
                DataRow[] pPathBillUNew = dtConnect.Select("VARIABLENAME = 'pPathBillUNew' ");
                if (pPathBillUNew.Length > 0)
                { PathImageClass.pPathBillUNew = pPathBillUNew[0]["SERVERIP"].ToString(); }

                //pPathVDOSendMoneyNew
                DataRow[] pPathVDOSendMoneyNew = dtConnect.Select("VARIABLENAME = 'pPathVDOSendMoneyNew' ");
                if (pPathVDOSendMoneyNew.Length > 0)
                { PathImageClass.pPathVDOSendMoneyNew = pPathVDOSendMoneyNew[0]["SERVERIP"].ToString(); }

                //pPathVDOFront
                DataRow[] pPathVDOFront = dtConnect.Select("VARIABLENAME = 'pPathVDOFront' ");
                if (pPathVDOFront.Length > 0)
                { PathImageClass.pPathVDOFront = pPathVDOFront[0]["SERVERIP"].ToString(); }

                //pPathRepairDevice
                DataRow[] pPathRepairDevice = dtConnect.Select("VARIABLENAME = 'pPathRepairDevice' ");
                if (pPathRepairDevice.Length > 0)
                { PathImageClass.pPathRepairDevice = pPathRepairDevice[0]["SERVERIP"].ToString(); }

                //pPathAllDay
                DataRow[] pPathAllDay = dtConnect.Select("VARIABLENAME = 'pPathAllDay' ");
                if (pPathAllDay.Length > 0)
                { PathImageClass.pPathAllDay = pPathAllDay[0]["SERVERIP"].ToString(); }

                //pPathAllDayChangeServer
                DataRow[] pPathAllDayChangeServer = dtConnect.Select("VARIABLENAME = 'pPathAllDay' ");
                if (pPathAllDayChangeServer.Length > 0)
                { PathImageClass.pPathAllDayChangeServer = pPathAllDayChangeServer[0]["USERNAME"].ToString(); }

                //pPathCN
                DataRow[] pPathCN = dtConnect.Select("VARIABLENAME = 'pPathCN' ");
                if (pPathCN.Length > 0)
                { PathImageClass.pPathCN = pPathCN[0]["SERVERIP"].ToString(); }

                //pPathBillOutIDM
                //PathImageClass.pPathBillOutIDM = PathImageClass.pServerIpImgVender + @"\ImageMinimark\IDM\";
                DataRow[] pPathBillOutIDM = dtConnect.Select("VARIABLENAME = 'pPathBillOutIDM' ");
                if (pPathBillOutIDM.Length > 0)
                { PathImageClass.pPathBillOutIDM = pPathBillOutIDM[0]["SERVERIP"].ToString(); }

                //pPathBillOut
                //PathImageClass.pPathBillOut = PathImageClass.pServerIpImgVender + @"\ImageMinimark\BillSupc\";
                DataRow[] pPathBillOut = dtConnect.Select("VARIABLENAME = 'pPathBillOut' ");
                if (pPathBillOut.Length > 0)
                { PathImageClass.pPathBillOut = pPathBillOut[0]["SERVERIP"].ToString(); }

                //pPathJOBMaintenance
                // PathImageClass.pPathJOBMaintenance = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBMaintenance\";
                DataRow[] pPathJOBMaintenance = dtConnect.Select("VARIABLENAME = 'pPathJOBMaintenance' ");
                if (pPathJOBMaintenance.Length > 0)
                { PathImageClass.pPathJOBMaintenance = pPathJOBMaintenance[0]["SERVERIP"].ToString(); }

                //pPathJOBMaintenanceClose
                //PathImageClass.pPathJOBMaintenanceClose = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBMaintenanceClose\";
                DataRow[] pPathJOBMaintenanceClose = dtConnect.Select("VARIABLENAME = 'pPathJOBMaintenanceClose' ");
                if (pPathJOBMaintenanceClose.Length > 0)
                { PathImageClass.pPathJOBMaintenanceClose = pPathJOBMaintenanceClose[0]["SERVERIP"].ToString(); }

                //pPathJOB
                //PathImageClass.pPathJOB = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOB\"; //ComMinimart
                DataRow[] pPathJOB = dtConnect.Select("VARIABLENAME = 'pPathJOB' ");
                if (pPathJOB.Length > 0)
                { PathImageClass.pPathJOB = pPathJOB[0]["SERVERIP"].ToString(); }

                //pPathJOBComService
                //PathImageClass.pPathJOBComService = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBComService\";
                DataRow[] pPathJOBComService = dtConnect.Select("VARIABLENAME = 'pPathJOBComService' ");
                if (pPathJOBComService.Length > 0)
                { PathImageClass.pPathJOBComService = pPathJOBComService[0]["SERVERIP"].ToString(); }

                //pPathJOBCar
                //PathImageClass.pPathJOBCar = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBCar\";
                DataRow[] pPathJOBCar = dtConnect.Select("VARIABLENAME = 'pPathJOBCar' ");
                if (pPathJOBCar.Length > 0)
                { PathImageClass.pPathJOBCar = pPathJOBCar[0]["SERVERIP"].ToString(); }

                //pPathJOBMN
                //PathImageClass.pPathJOBMN = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBMN\"; //center
                DataRow[] pPathJOBMN = dtConnect.Select("VARIABLENAME = 'pPathJOBMN' ");
                if (pPathJOBMN.Length > 0)
                { PathImageClass.pPathJOBMN = pPathJOBMN[0]["SERVERIP"].ToString(); }

                //pPathJOBHD
                //PathImageClass.pPathJOBHD = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBHD\"; //head
                DataRow[] pPathJOBHD = dtConnect.Select("VARIABLENAME = 'pPathJOBHD' ");
                if (pPathJOBHD.Length > 0)
                { PathImageClass.pPathJOBHD = pPathJOBHD[0]["SERVERIP"].ToString(); }

                //pPathJOBHDBCH
                //PathImageClass.pPathJOBHDBCH = PathImageClass.pServerIpImgVender + @"\ImageMinimark\JOBHDBCH\"; //branch reply Head
                DataRow[] pPathJOBHDBCH = dtConnect.Select("VARIABLENAME = 'pPathJOBHDBCH' ");
                if (pPathJOBHDBCH.Length > 0)
                { PathImageClass.pPathJOBHDBCH = pPathJOBHDBCH[0]["SERVERIP"].ToString(); }

                //pPathAssetClaimLeave
                //PathImageClass.pPathAssetClaimLeave = PathImageClass.pServerIpImgVender + @"\ImageMinimark\AssetClaimLeave\";//ส่งเคลม+นำทิ้ง
                DataRow[] pPathAssetClaimLeave = dtConnect.Select("VARIABLENAME = 'pPathAssetClaimLeave' ");
                if (pPathAssetClaimLeave.Length > 0)
                { PathImageClass.pPathAssetClaimLeave = pPathAssetClaimLeave[0]["SERVERIP"].ToString(); }

                // pPathShelfNew
                DataRow[] pPathShelfNew = dtConnect.Select("VARIABLENAME = 'pPathShelfNew' ");
                if (pPathShelfNew.Length > 0)
                { PathImageClass.pPathShelfNew = pPathShelfNew[0]["SERVERIP"].ToString(); }

                // pPathVender
                DataRow[] pPathVender = dtConnect.Select("VARIABLENAME = 'pPathVender' ");
                if (pPathVender.Length > 0)
                { PathImageClass.pPathVender = pPathVender[0]["SERVERIP"].ToString(); }

                // pPathEmpVDO
                DataRow[] pPathEmpVDO = dtConnect.Select("VARIABLENAME = 'pPathEmpVDO' ");
                if (pPathEmpVDO.Length > 0)
                { PathImageClass.pPathEmpVDO = pPathEmpVDO[0]["SERVERIP"].ToString(); }

                // pPathPLANMN
                DataRow[] pPathPLANMN = dtConnect.Select("VARIABLENAME = 'pPathPLANMN' ");
                if (pPathPLANMN.Length > 0)
                { PathImageClass.pPathPLANMN = pPathPLANMN[0]["SERVERIP"].ToString(); }

                //pPathCheckPriceMN
                DataRow[] pPathCheckPriceMN = dtConnect.Select("VARIABLENAME = 'pPathCheckPriceMN' ");
                if (pPathCheckPriceMN.Length > 0)
                { PathImageClass.pPathCheckPriceMN = pPathCheckPriceMN[0]["SERVERIP"].ToString(); }

                //pPathCheckPriceSUPC
                DataRow[] pPathCheckPriceSUPC = dtConnect.Select("VARIABLENAME = 'pPathCheckPriceSUPC' ");
                if (pPathCheckPriceSUPC.Length > 0)
                { PathImageClass.pPathCheckPriceSUPC = pPathCheckPriceSUPC[0]["SERVERIP"].ToString(); }

                //pPathVDOVender
                DataRow[] pPathVDOVender = dtConnect.Select("VARIABLENAME = 'pPathVDOVender' ");
                if (pPathVDOVender.Length > 0)
                { PathImageClass.pPathVDOVender = pPathVDOVender[0]["SERVERIP"].ToString(); }

                //pPathMP
                DataRow[] pPathMP = dtConnect.Select("VARIABLENAME = 'pPathMP' ");
                if (pPathMP.Length > 0)
                { PathImageClass.pPathMP = pPathMP[0]["SERVERIP"].ToString(); }

                //pPathCheckItemRecive
                DataRow[] pPathCheckItemRecive = dtConnect.Select("VARIABLENAME = 'pPathCheckItemRecive' ");
                if (pPathCheckItemRecive.Length > 0)
                { PathImageClass.pPathCheckItemRecive = pPathCheckItemRecive[0]["SERVERIP"].ToString(); }

                //SystemPurchaseFresh
                DataRow[] SystemPurchaseFresh = dtConnect.Select("VARIABLENAME = 'SystemPurchaseFresh' ");
                if (SystemPurchaseFresh.Length > 0)
                { SystemClass.SystemPurchaseFresh = SystemPurchaseFresh[0]["SERVERIP"].ToString(); }

                //SystemBarcodeType17
                DataRow[] SystemBarcodeType17 = dtConnect.Select("VARIABLENAME = 'SystemBarcodeType17' ");
                if (SystemBarcodeType17.Length > 0)
                { SystemClass.SystemBarcodeType17 = SystemBarcodeType17[0]["SERVERIP"].ToString(); }

                //SystemBarcodeType19
                DataRow[] SystemBarcodeType19 = dtConnect.Select("VARIABLENAME = 'SystemBarcodeType19' ");
                if (SystemBarcodeType19.Length > 0)
                { SystemClass.SystemBarcodeType19 = SystemBarcodeType19[0]["SERVERIP"].ToString(); }

                //SystemBarcodeType20
                DataRow[] SystemBarcodeType20 = dtConnect.Select("VARIABLENAME = 'SystemBarcodeType20' ");
                if (SystemBarcodeType20.Length > 0)
                { SystemClass.SystemBarcodeType20 = SystemBarcodeType20[0]["SERVERIP"].ToString(); }

                //SystemBarcodeType32
                DataRow[] SystemBarcodeType32 = dtConnect.Select("VARIABLENAME = 'SystemBarcodeType32' ");
                if (SystemBarcodeType32.Length > 0)
                { SystemClass.SystemBarcodeType32 = SystemBarcodeType32[0]["SERVERIP"].ToString(); }

                //pPathSendStock
                DataRow[] pPathSendStock = dtConnect.Select("VARIABLENAME = 'pPathSendStock' ");
                if (pPathSendStock.Length > 0)
                { PathImageClass.pPathSendStock = pPathSendStock[0]["SERVERIP"].ToString(); }

                //pPathEmplSorting
                DataRow[] pPathEmplSorting = dtConnect.Select("VARIABLENAME = 'pPathEmplSorting' ");
                if (pPathEmplSorting.Length > 0)
                { PathImageClass.pPathEmplSorting = pPathEmplSorting[0]["SERVERIP"].ToString(); }

                //pPathMNSV
                DataRow[] pPathMNSV = dtConnect.Select("VARIABLENAME = 'pPathMNSV' ");
                if (pPathMNSV.Length > 0)
                { PathImageClass.pPathMNSV = pPathMNSV[0]["SERVERIP"].ToString(); }

                //pPathDocument
                DataRow[] pPathDocument = dtConnect.Select("VARIABLENAME = 'pPathDocument' ");
                if (pPathDocument.Length > 0)
                { PathImageClass.pPathDocument = pPathDocument[0]["SERVERIP"].ToString(); }

                //pPathPromotion
                DataRow[] pPathPromotion = dtConnect.Select("VARIABLENAME = 'pPathPromotion' ");
                if (pPathPromotion.Length > 0)
                { PathImageClass.pPathPromotion = pPathPromotion[0]["SERVERIP"].ToString(); }

                //pPathJOBFactory
                DataRow[] pPathJOBFactory = dtConnect.Select("VARIABLENAME = 'pPathJOBFactory' ");
                if (pPathJOBFactory.Length > 0)
                { PathImageClass.pPathJOBFactory = pPathJOBFactory[0]["SERVERIP"].ToString(); }
            }

        }

 
    }
}
