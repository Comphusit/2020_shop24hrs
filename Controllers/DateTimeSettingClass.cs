﻿using System;
using System.Collections.Generic;
using System.Data;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs.Controllers
{
    class DateTimeSettingClass
    {
        // วันเวลา-วัน-เวลา
        public static DataTable GetDateTime()
        {
            string sql = @"SELECT getdate() AS Datetime, convert(varchar,getdate(),23) AS Date,convert(varchar,getdate(),24) AS Time ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        // ปี
        public static DataTable Config_YY()
        {
            string sqlStr = @"SELECT	YEAR(GETDATE()) AS Y ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sqlStr);
            string yearY = dt.Rows[0]["Y"].ToString();

            DataTable dtY = new DataTable("dtY");
            dtY.Columns.Add("Y1");
            dtY.Columns.Add("Y2");
            for (int i = 0; i < 4; i++)
            {
                int yy = Convert.ToInt32(yearY) - i;
                dtY.Rows.Add(Convert.ToString(yy), Convert.ToString(yy));
            }
            return dtY;
        }
        // ค้นหาวันในการ running Bill
        public static string GetDateShortForRunningBill()
        {
            string sql = @"SELECT  SUBSTRING(REPLACE(CAST(convert(varchar,getdate(),23) AS nvarchar(10)),'-',''),3,6) AS DateShort ";            
            return ConnectionClass.SelectSQL_Main(sql).Rows[0]["DateShort"].ToString();
        }
        //ค้นหาปี คศ 2 หลักหลัง 21
        public static string GetShortYear()
        {
            DataTable dt = ConnectionClass.SelectSQL_Main(" SELECT	SUBSTRING(CAST(convert(varchar,getdate(),23) as nvarchar(10)),3,2) AS Y ");
            return dt.Rows[0]["Y"].ToString();
        }
        //Check Month เป็นการคำนวณเดือนจากค่าที่เราต่งเข้าไป
        public static DataTable GetMonthYearByDate(string dateStart, string dateEnd)
        {
            string sql = $@"
                DECLARE @start DATE = '{dateStart}'
                DECLARE @end DATE = '{dateEnd}'

                ;with months (date)
                AS
                (
                    SELECT @start
                    UNION ALL
                    SELECT DATEADD(DAY, 1, date)
                    FROM months
                    WHERE DATEADD(DAY, 1, date) <= @end
                )
                SELECT		[MonthName]    = DATENAME(mm, date),
                            [MonthNumber]  = DATEPART(mm, date),  
                            [MonthNumber2]  = FORMAT(date,'MM'),
                            [LastDayOfMonth]  = DATEPART(dd, EOMONTH(date)),
                            [MonthYear]    = DATEPART(yy, date),
                            [MMYY] = CONVERT(VARCHAR,FORMAT(date,'MM')) + '-' +CONVERT(VARCHAR,DATEPART(yy, date)),
		                    CASE DATEPART(mm, date)
		                    WHEN '01' THEN 'มกราคม'
                            WHEN '02' THEN 'กุมภาพันธ์'
                            WHEN '03' THEN 'มีนาคม'
                            WHEN '04' THEN 'เมษายน'
                            WHEN '05' THEN 'พฤษภาคม'
                            WHEN '06' THEN 'มิถุนายน'
                            WHEN '07' THEN 'กรกฎาคม'
                            WHEN '08' THEN 'สิงหาคม'
                            WHEN '09' THEN 'กันยายน'
                            WHEN '10' THEN 'ตุลาคม'
                            WHEN '11' THEN 'พฤศจิกายน'
                                ELSE 'ธันวาคม' END AS ThaiMonth
                FROM	months
                GROUP BY DATENAME(mm, date),DATEPART(mm, date),FORMAT(date,'MM'),  DATEPART(dd, EOMONTH(date)),DATEPART(yy, date),
		                CASE DATEPART(mm, date)
		                    WHEN '01' THEN 'มกราคม'
                            WHEN '02' THEN 'กุมภาพันธ์'
                            WHEN '03' THEN 'มีนาคม'
                            WHEN '04' THEN 'เมษายน'
                            WHEN '05' THEN 'พฤษภาคม'
                            WHEN '06' THEN 'มิถุนายน'
                            WHEN '07' THEN 'กรกฎาคม'
                            WHEN '08' THEN 'สิงหาคม'
                            WHEN '09' THEN 'กันยายน'
                            WHEN '10' THEN 'ตุลาคม'
                            WHEN '11' THEN 'พฤศจิกายน'
                                ELSE 'ธันวาคม' END,CONVERT(VARCHAR,FORMAT(date,'MM')) + '-' +CONVERT(VARCHAR,DATEPART(yy, date))
                ORDER BY  DATEPART(yy, date) DESC,DATEPART(mm, date) DESC
                option (maxrecursion 0) 
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Check Month เป็นการคำนวณเดือนจากค่าที่เราต่งเข้าไป
        public static DataTable GetDayByDate(string dateStart, string dateEnd)
        {
            string sql = $@"
                DECLARE @start DATE = '{dateStart}'
                DECLARE @end DATE = '{dateEnd}'

                ;with months (date)
                AS
                (
                    SELECT @start
                    UNION ALL
                    SELECT DATEADD(DAY, 1, date)
                    FROM months
                    WHERE DATEADD(DAY, 1, date) <= @end
                )
                SELECT		Day = CONVERT(VARCHAR,date,23),
			                [MonthName]    = DATENAME(mm, date),
                            [MonthNumber]  = DATEPART(mm, date),  
                            [LastDayOfMonth]  = DATEPART(dd, EOMONTH(date)),
                            [MonthYear]    = DATEPART(yy, date) 
                FROM	months
 
                option (maxrecursion 0)  
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Check Month เป็นการคำนวณเดือนจากค่าที่เราต่งเข้าไป
        public static DataTable GetHour()
        {
            string sql = $@"
                with dataHour as
                (
                    SELECT 0 AS MyHour
                    UNION ALL
                    SELECT	MyHour + 1
                    FROM	dataHour 
                    WHERE	MyHour + 1 < 24
                )
                SELECT	dataHour.MyHour,
                        CASE MyHour
		                    WHEN '0' THEN '00.00-00.59'
		                    WHEN '1' THEN '01.00-01.59'
		                    WHEN '2' THEN '02.00-02.59'
		                    WHEN '3' THEN '03.00-03.59'
		                    WHEN '4' THEN '04.00-04.59'
		                    WHEN '5' THEN '05.00-05.59'
		                    WHEN '6' THEN '06.00-06.59'
		                    WHEN '7' THEN '07.00-07.59'
		                    WHEN '8' THEN '08.00-08.59'
		                    WHEN '9' THEN '09.00-09.59'
		                    WHEN '10' THEN '10.00-10.59'
		                    WHEN '11' THEN '11.00-11.59'
		                    WHEN '12' THEN '12.00-12.59'
		                    WHEN '13' THEN '13.00-13.59'
		                    WHEN '14' THEN '14.00-14.59'
		                    WHEN '15' THEN '15.00-15.59'
		                    WHEN '16' THEN '16.00-16.59'
		                    WHEN '17' THEN '17.00-17.59'
		                    WHEN '18' THEN '18.00-18.59'
		                    WHEN '19' THEN '19.00-19.59'
		                    WHEN '20' THEN '20.00-20.59'
		                    WHEN '21' THEN '21.00-21.59'
		                    WHEN '22' THEN '22.00-22.59'
		                    WHEN '23' THEN '23.00-23.59'
                        END AS MyHourName
                FROM	dataHour  
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Set max min DatetimePicker
        public static void SetDateTime_MinMaxValue(RadDateTimePicker beginDate, RadDateTimePicker endDate)
        {
            beginDate.MaxDate = endDate.Value;
            endDate.MinDate = beginDate.Value;
        }

    }
}

