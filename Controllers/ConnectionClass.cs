﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace PC_Shop24Hrs.Controllers
{
    class ConnectionClass
    {
        #region "SELECT"
        //MyJeeds ดึงข้อมูลจาก Server หลัก 24
        public static DataTable SelectSQL_Main(string _sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConSelectMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 90;
                adp.Fill(dt);
                sqlConnection.Close();
            }
            catch (Exception)
            {
                //throw;
            }

            return dt;
        }
        //MyJeeds ดึงข้อมูลจาก Server POS
        public static DataTable SelectSQL_POSRetail707(string _sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConMainRatail707);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception)
                    {
                        //  return dt;
                    }
                }
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 30;
                adp.Fill(dt);
                sqlConnection.Close();
            }
            catch (Exception)
            {
                //throw;
            }

            return dt;
        }
        //MyJeeds ดึงข้อมูลจาก Server POS
        public static DataTable SelectSQL_MainAX(string _sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConMainAX);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception)
                    {
                    }
                }
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 30;
                adp.Fill(dt);
                sqlConnection.Close();
            }
            catch (Exception)
            {
                //throw;
            }

            return dt;
        }
        //MyJeeds ดึงข้อมูลจาก server ที่ส่งค่ามา
        public static DataTable SelectSQL_SentServer(string _sql, string _ServerName, int timeout = 30)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                SqlConnection sqlConnection = new SqlConnection(_ServerName);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception)
                    {
                    }
                }
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = timeout;
                adp.Fill(dt);
                sqlConnection.Close();
            }
            catch (Exception)
            {
                //throw;
            }

            return dt;
        }

        #endregion

        #region "EXECUTE"
        //MyJeeds save data server main 24
        public static string ExecuteSQL_Main(string _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConSelectMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    sqlCommand.CommandText = _sql;
                    sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }
        //MyJeds save Sql For sql > 1
        public static string ExecuteSQL_ArrayMain(ArrayList _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConSelectMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    for (int i = 0; i < _sql.Count; i++)
                    {
                        sqlCommand.CommandText = Convert.ToString(_sql[i]);
                        sqlCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        //MyJeeds save data server 708 AX
        public static string ExecuteSQL_MainAX(string _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConMainAX);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception ex2)
                    {
                        return $@"ระบบ AX-SPC708SRV หลักมีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                    }
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    sqlCommand.CommandText = _sql;
                    sqlCommand.CommandTimeout = 10;
                    sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        //MyJeeds save data server POS 
        public static string ExecuteSQL_POSRetail707(string _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConMainRatail707);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception ex2)
                    {
                        return $@"ระบบ POS-SPC707SRV หลักมีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                    }
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    sqlCommand.CommandText = _sql;
                    sqlCommand.CommandTimeout = 10;
                    sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        //MyJeeds save Sql For sql > 1
        public static string ExecuteSQL_ArrayMainAX(ArrayList _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConMainAX);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception ex2)
                    {
                        return $@"ระบบ AX-SPC708SRV หลักมีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                    }
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    for (int i = 0; i < _sql.Count; i++)
                    {
                        sqlCommand.CommandText = Convert.ToString(_sql[i]);
                        sqlCommand.CommandTimeout = 10;
                        sqlCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        //Insert 24 + AX 
        public static string ExecuteMain_AX_24_SameTime(ArrayList str24, ArrayList strAX)
        {
            //24
            SqlConnection sqlConnection24 = new SqlConnection(IpServerConnectClass.ConSelectMain);
            if (sqlConnection24.State == ConnectionState.Closed)
            {
                sqlConnection24.Open();
            }
            //  SqlCommand sqlCommand24 = new SqlCommand();
            SqlCommand sqlCommand24 = sqlConnection24.CreateCommand();
            SqlTransaction transaction24 = sqlConnection24.BeginTransaction("SampleTransaction");
            sqlCommand24.Connection = sqlConnection24;
            sqlCommand24.Transaction = transaction24;
            //AX
            SqlConnection sqlConnectionAX = new SqlConnection(IpServerConnectClass.ConMainAX);
            if (sqlConnectionAX.State == ConnectionState.Closed)
            {
                try
                {
                    sqlConnectionAX.Open();
                }
                catch (Exception ex2)
                {
                    return $@"ระบบ AX-SPC708SRV หลักมีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                }

            }
            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommandAX = sqlConnectionAX.CreateCommand();
            SqlTransaction transactionAX = sqlConnectionAX.BeginTransaction("SampleTransaction");
            sqlCommandAX.Connection = sqlConnectionAX;
            sqlCommandAX.Transaction = transactionAX;

            try
            {
                for (int i24 = 0; i24 < str24.Count; i24++)
                {
                    sqlCommand24.CommandText = Convert.ToString(str24[i24]);
                    sqlCommand24.ExecuteNonQuery();
                }

                for (int i = 0; i < strAX.Count; i++)
                {
                    sqlCommandAX.CommandText = Convert.ToString(strAX[i]);
                    sqlCommandAX.CommandTimeout = 10;
                    sqlCommandAX.ExecuteNonQuery();
                }

                transaction24.Commit();
                transactionAX.Commit();

                sqlConnection24.Close();
                sqlConnectionAX.Close();
                return "";
            }
            catch (Exception ex)
            {
                transaction24.Rollback();
                transactionAX.Rollback();
                return "ไม่สามารถบันทึกข้อมูลเข้าระบบได้ ลองใหม่อีกครั้ง." + Environment.NewLine + ex.Message;
            }
        }

        //Insert To Server
        public static string Execute_SameTime_2Server(ArrayList str1, string _ServerName1, ArrayList str2, string _ServerName2)
        {

            //1
            SqlConnection sqlConnection1 = new SqlConnection(_ServerName1);
            if (sqlConnection1.State == ConnectionState.Closed)
            {
                //sqlConnection1.Open();
                try
                {
                    sqlConnection1.Open();
                }
                catch (Exception ex1)
                {
                    return $@"ระบบ Server {Environment.NewLine}{_ServerName1}{Environment.NewLine}มีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex1.Message}";
                }
            }
            //  SqlCommand sqlCommand24 = new SqlCommand();
            SqlCommand sqlCommand1 = sqlConnection1.CreateCommand();
            SqlTransaction transaction1 = sqlConnection1.BeginTransaction("SampleTransaction");
            sqlCommand1.Connection = sqlConnection1;
            sqlCommand1.Transaction = transaction1;
            //2
            SqlConnection sqlConnection2 = new SqlConnection(_ServerName2);
            if (sqlConnection2.State == ConnectionState.Closed)
            {
                //sqlConnection2.Open();
                try
                {
                    sqlConnection2.Open();
                }
                catch (Exception ex2)
                {
                    return $@"ระบบ Server {Environment.NewLine}{_ServerName2}{Environment.NewLine}มีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                }
            }
            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommand2 = sqlConnection2.CreateCommand();
            SqlTransaction transaction2 = sqlConnection2.BeginTransaction("SampleTransaction");
            sqlCommand2.Connection = sqlConnection2;
            sqlCommand2.Transaction = transaction2;

            try
            {
                for (int i24 = 0; i24 < str1.Count; i24++)
                {
                    sqlCommand1.CommandText = Convert.ToString(str1[i24]);
                    sqlCommand1.CommandTimeout = 10;
                    sqlCommand1.ExecuteNonQuery();
                }

                for (int i = 0; i < str2.Count; i++)
                {
                    sqlCommand2.CommandText = Convert.ToString(str2[i]);
                    sqlCommand2.CommandTimeout = 10;
                    sqlCommand2.ExecuteNonQuery();
                }

                transaction1.Commit();
                transaction2.Commit();
                sqlConnection1.Close();
                sqlConnection2.Close();
                return "";
            }
            catch (Exception ex)
            {
                transaction1.Rollback();
                transaction2.Rollback();

                return ex.Message;
            }
        }

        //MyJeds save Sql For sql > 1
        public static string ExecuteSQL_ArrayMain_SentServer(ArrayList _sql, string _ServerName)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(_ServerName);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception ex2)
                    {
                        return $@"ระบบ Server {Environment.NewLine}{_ServerName}{Environment.NewLine}มีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                    }
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    for (int i = 0; i < _sql.Count; i++)
                    {
                        sqlCommand.CommandText = Convert.ToString(_sql[i]);
                        sqlCommand.CommandTimeout = 10;
                        sqlCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }
        //MyJeeds save data server main 24
        public static string ExecuteSQL2Str_Main(string _sql1, string _sql2)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(IpServerConnectClass.ConSelectMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    sqlCommand.CommandText = _sql1;
                    sqlCommand.CommandTimeout = 10;
                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.CommandText = _sql2;
                    sqlCommand.CommandTimeout = 10;
                    sqlCommand.ExecuteNonQuery();

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        //MyJeeds save server ที่ส่งค่ามา
        public static string ExecuteSQL_SentServer(string _sql, string _ServerName)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(_ServerName);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception ex2)
                    {
                        return $@"ระบบ Server {Environment.NewLine}{_ServerName}{Environment.NewLine}มีปัญหาไม่สามารถส่งข้อมูลได้{Environment.NewLine}{ex2.Message}";
                    }
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    sqlCommand.CommandText = _sql;
                    sqlCommand.CommandTimeout = 10;
                    sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        #endregion




    }
}
