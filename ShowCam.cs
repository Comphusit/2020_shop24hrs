﻿using NVRCsharpDemo;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace HikvisionPreview
{
    public partial class ShowCam : Form
    {
        public DataTable dtCam;
        public string typeCam;

        public string pSwitch;
        private bool m_bInitSDK = false;
        //private uint iLastErr = 0;

        #region "m_lUserID"
        //สำคัญ
        Int32 m_lUserID1 = -1;
        Int32 m_lUserID2 = -1;
        Int32 m_lUserID3 = -1;
        Int32 m_lUserID4 = -1;
        Int32 m_lUserID5 = -1;
        Int32 m_lUserID6 = -1;
        Int32 m_lUserID7 = -1;
        Int32 m_lUserID8 = -1;
        Int32 m_lUserID9 = -1;
        Int32 m_lUserID10 = -1;
        Int32 m_lUserID11 = -1;
        Int32 m_lUserID12 = -1;
        Int32 m_lUserID13 = -1;
        Int32 m_lUserID14 = -1;
        Int32 m_lUserID15 = -1;
        Int32 m_lUserID16 = -1;
        #endregion

        #region "m_lRealHandle"
        //ช่อง
        Int32 m_lRealHandle1 = -1;
        Int32 m_lRealHandle2 = -1;
        Int32 m_lRealHandle3 = -1;
        Int32 m_lRealHandle4 = -1;
        Int32 m_lRealHandle5 = -1;
        Int32 m_lRealHandle6 = -1;
        Int32 m_lRealHandle7 = -1;
        Int32 m_lRealHandle8 = -1;
        Int32 m_lRealHandle9 = -1;
        Int32 m_lRealHandle10 = -1;
        Int32 m_lRealHandle11 = -1;
        Int32 m_lRealHandle12 = -1;
        Int32 m_lRealHandle13 = -1;
        Int32 m_lRealHandle14 = -1;
        Int32 m_lRealHandle15 = -1;
        Int32 m_lRealHandle16 = -1;
        #endregion

        #region "dvrIp"
        //DVR IP
        string dvrIp1 = "";
        string dvrIp2 = "";
        string dvrIp3 = "";
        string dvrIp4 = "";
        string dvrIp5 = "";
        string dvrIp6 = "";
        string dvrIp7 = "";
        string dvrIp8 = "";
        string dvrIp9 = "";
        string dvrIp10 = "";
        string dvrIp11 = "";
        string dvrIp12 = "";
        string dvrIp13 = "";
        string dvrIp14 = "";
        string dvrIp15 = "";
        string dvrIp16 = "";
        #endregion

        #region "bch"
        //DVR IP
        string bch1 = "";
        string bch2 = "";
        string bch3 = "";
        string bch4 = "";
        string bch5 = "";
        string bch6 = "";
        string bch7 = "";
        string bch8 = "";
        string bch9 = "";
        string bch10 = "";
        string bch11 = "";
        string bch12 = "";
        string bch13 = "";
        string bch14 = "";
        string bch15 = "";
        string bch16 = "";
        #endregion

        #region "DVR_passWord"
        //DVR passWord
        string passWord1 = "";
        string passWord2 = "";
        string passWord3 = "";
        string passWord4 = "";
        string passWord5 = "";
        string passWord6 = "";
        string passWord7 = "";
        string passWord8 = "";
        string passWord9 = "";
        string passWord10 = "";
        string passWord11 = "";
        string passWord12 = "";
        string passWord13 = "";
        string passWord14 = "";
        string passWord15 = "";
        string passWord16 = "";
        #endregion
        #region "DVR_User"
        //DVR passWord
        string DVR_User1 = "";
        string DVR_User2 = "";
        string DVR_User3 = "";
        string DVR_User4 = "";
        string DVR_User5 = "";
        string DVR_User6 = "";
        string DVR_User7 = "";
        string DVR_User8 = "";
        string DVR_User9 = "";
        string DVR_User10 = "";
        string DVR_User11 = "";
        string DVR_User12 = "";
        string DVR_User13 = "";
        string DVR_User14 = "";
        string DVR_User15 = "";
        string DVR_User16 = "";
        #endregion
        //public string DVR_IP; // IP
        //public string DVR_BchID; //รหัสสาขา
        //public string DVR_BchName; // ชื่อสาขา
        //public string DVR_TypeCam; //ประเภทกล้อง

        // public string DVR_User = "shop"; //User เข้า DVR
        //public string DVR_Password = "shop1234"; // Password เข้า DVR


        private CHCNetSDK.NET_DVR_PICCFG_V40 m_struPicCfgV40;
        public CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo;
        public CHCNetSDK.NET_DVR_TIME m_struTimeCfg;

        // private Int32 m_lFindHandle = -1;
        //private uint dwAChanTotalNum = 0;

        //private uint iLastErr = 0;
        //
        public ShowCam()
        {
            InitializeComponent();

            this.FormClosing += ShowCam_FormClosing;

            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom; pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox3.SizeMode = PictureBoxSizeMode.Zoom; pictureBox4.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox5.SizeMode = PictureBoxSizeMode.Zoom; pictureBox6.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox7.SizeMode = PictureBoxSizeMode.Zoom; pictureBox8.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox9.SizeMode = PictureBoxSizeMode.Zoom; pictureBox10.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox11.SizeMode = PictureBoxSizeMode.Zoom; pictureBox12.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox13.SizeMode = PictureBoxSizeMode.Zoom; pictureBox14.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox15.SizeMode = PictureBoxSizeMode.Zoom; pictureBox16.SizeMode = PictureBoxSizeMode.Zoom;
        }
        //Close
        private void ShowCam_FormClosing(object sender, FormClosingEventArgs e)
        {
            SetLogout();
        }

        public void Login(string dvrIP, string bchID, string bchName, int iCH, Int16 Channel, string dvr_User, string dvrPassWord)
        {
            this.Cursor = Cursors.WaitCursor;
            m_bInitSDK = CHCNetSDK.NET_DVR_Init();
            if (m_bInitSDK == false)
            {
                SetPathEmp(iCH);
                this.Cursor = Cursors.Default;
                return;
            }

            //Int32 m_lUserID = CHCNetSDK.NET_DVR_Login_V30(dvrIP, ClassShow.DVRPortNumber, DVR_User, DVR_Password, ref DeviceInfo);
            Int32 m_lUserID = CHCNetSDK.NET_DVR_Login_V30(dvrIP, ClassShow.DVRPortNumber, dvr_User, dvrPassWord, ref DeviceInfo);
            //ในกรณีที่ Login ไม่ได้
            if (m_lUserID < 0)
            {
                SetPathEmp(iCH);
                this.Cursor = Cursors.Default;
                return;
            }
            else
            {
                SetPlay(iCH, m_lUserID, bchID, bchName, Channel, dvrIP, dvr_User, dvrPassWord);
                this.Cursor = Cursors.Default;
                return;
            }
        }

        //Load
        public void ShowCam_Load(object sender, EventArgs e)
        {
            this.Text = typeCam;

            for (int i = 0; i < 16; i++)
            {
                SetPathEmp(i + 1);
            }

            if ((pSwitch == "1") || (dtCam is null))
            {
                SetLogout();
            }
            else
            {
                //if ((DVR_User == "") || (DVR_User is null))
                //{
                //    DVR_User = "admin";
                //    //DVR_Password = "admin8570";
                //}

                int iCh = 1;
                foreach (DataRow item in dtCam.Rows)
                {
                    string dvr_IP = item[1].ToString().Replace("http://", "").Replace("/", "");
                    string bchID = item[2].ToString();
                    string bchName = item[3].ToString();
                    Int16 channel = Int16.Parse(item[4].ToString());
                    //string dvrPassword = "shop1234";
                    //if (item[5].ToString() == "2") dvrPassword = "Shop1234";
                    Login(dvr_IP, bchID, bchName, iCh, channel, item["SET_USER"].ToString(), item["SET_PASSWORD"].ToString());
                    iCh++;
                }
            }

        }
        //SetPathEmp16
        void SetPathEmp(int iCH)
        {
            switch (iCH)
            {
                case 1:
                    label_1.Text = "EMP";
                    label_1.Visible = false; pictureBox1.Visible = false;
                    break;
                case 2:
                    label_2.Text = "EMP";
                    label_2.Visible = false; pictureBox2.Visible = false;
                    break;
                case 3:
                    label_3.Text = "EMP";
                    label_3.Visible = false; pictureBox3.Visible = false;
                    break;
                case 4:
                    label_4.Text = "EMP";
                    label_4.Visible = false; pictureBox4.Visible = false;
                    break;
                case 5:
                    label_5.Text = "EMP";
                    label_5.Visible = false; pictureBox5.Visible = false;
                    break;
                case 6:
                    label_6.Text = "EMP";
                    label_6.Visible = false; pictureBox6.Visible = false;
                    break;
                case 7:
                    label_7.Text = "EMP";
                    label_7.Visible = false; pictureBox7.Visible = false;
                    break;
                case 8:
                    label_8.Text = "EMP";
                    label_8.Visible = false; pictureBox8.Visible = false;
                    break;
                case 9:
                    label_9.Text = "EMP";
                    label_9.Visible = false; pictureBox9.Visible = false;
                    break;
                case 10:
                    label_10.Text = "EMP";
                    label_10.Visible = false; pictureBox10.Visible = false;
                    break;
                case 11:
                    label_11.Text = "EMP";
                    label_11.Visible = false; pictureBox11.Visible = false;
                    break;
                case 12:
                    label_12.Text = "EMP";
                    label_12.Visible = false; pictureBox12.Visible = false;
                    break;
                case 13:
                    // ClassShow.PathEmp(pictureBox13); 
                    label_13.Text = "EMP";
                    label_13.Visible = false; pictureBox13.Visible = false;
                    break;
                case 14:
                    //ClassShow.PathEmp(pictureBox14); 
                    label_14.Text = "EMP";
                    label_14.Visible = false; pictureBox14.Visible = false;
                    break;
                case 15:
                    // ClassShow.PathEmp(pictureBox15); 
                    label_15.Text = "EMP";
                    label_15.Visible = false; pictureBox15.Visible = false;
                    break;
                case 16:
                    // ClassShow.PathEmp(pictureBox16);
                    label_16.Text = "EMP";
                    label_16.Visible = false; pictureBox16.Visible = false;
                    break;
                default:
                    break;
            }

        }
        void SetPlay(int iCH, Int32 m_lUserID, string DVR_BchID, string DVR_BchName, Int16 Channel, string dvrIP, string dvrUser, string dvrPassword)
        {
            switch (iCH)
            {
                case 1:
                    m_lUserID1 = m_lUserID;
                    dvrIp1 = dvrIP + "|" + Channel;
                    bch1 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle1 = ClassShow.Preview(Channel, pictureBox1, m_lUserID);
                    label_1.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_1.Visible = true; pictureBox1.Visible = true;
                    passWord1 = dvrPassword;
                    DVR_User1 = dvrUser;
                    break;
                case 2:
                    m_lUserID2 = m_lUserID;
                    dvrIp2 = dvrIP + "|" + Channel;
                    bch2 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle2 = ClassShow.Preview(Channel, pictureBox2, m_lUserID);
                    label_2.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_2.Visible = true; pictureBox2.Visible = true;
                    passWord2 = dvrPassword;
                    DVR_User2 = dvrUser;
                    break;
                case 3:
                    m_lUserID3 = m_lUserID;
                    dvrIp3 = dvrIP + "|" + Channel;
                    bch3 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle3 = ClassShow.Preview(Channel, pictureBox3, m_lUserID);
                    label_3.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_3.Visible = true; pictureBox3.Visible = true;
                    passWord3 = dvrPassword;
                    DVR_User3 = dvrUser;
                    break;
                case 4:
                    m_lUserID4 = m_lUserID;
                    dvrIp4 = dvrIP + "|" + Channel;
                    bch4 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle4 = ClassShow.Preview(Channel, pictureBox4, m_lUserID);
                    label_4.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_4.Visible = true; pictureBox4.Visible = true;
                    passWord4 = dvrPassword;
                    DVR_User4 = dvrUser;
                    break;
                case 5:
                    m_lUserID5 = m_lUserID;
                    dvrIp5 = dvrIP + "|" + Channel;
                    bch5 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle5 = ClassShow.Preview(Channel, pictureBox5, m_lUserID);
                    label_5.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_5.Visible = true; pictureBox5.Visible = true;
                    passWord5 = dvrPassword;
                    DVR_User5 = dvrUser;
                    break;
                case 6:
                    m_lUserID6 = m_lUserID;
                    dvrIp6 = dvrIP + "|" + Channel;
                    bch6 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle6 = ClassShow.Preview(Channel, pictureBox6, m_lUserID);
                    label_6.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_6.Visible = true; pictureBox6.Visible = true;
                    passWord6 = dvrPassword;
                    DVR_User6 = dvrUser;
                    break;
                case 7:
                    m_lUserID7 = m_lUserID;
                    dvrIp7 = dvrIP + "|" + Channel;
                    bch7 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle7 = ClassShow.Preview(Channel, pictureBox7, m_lUserID);
                    label_7.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_7.Visible = true; pictureBox7.Visible = true;
                    passWord7 = dvrPassword;
                    DVR_User7 = dvrUser;
                    break;
                case 8:
                    m_lUserID8 = m_lUserID;
                    dvrIp8 = dvrIP + "|" + Channel;
                    bch8 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle8 = ClassShow.Preview(Channel, pictureBox8, m_lUserID);
                    label_8.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_8.Visible = true; pictureBox8.Visible = true;
                    passWord8 = dvrPassword;
                    DVR_User8 = dvrUser;
                    break;
                case 9:
                    m_lUserID9 = m_lUserID;
                    dvrIp9 = dvrIP + "|" + Channel;
                    bch9 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle9 = ClassShow.Preview(Channel, pictureBox9, m_lUserID);
                    label_9.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_9.Visible = true; pictureBox9.Visible = true;
                    passWord9 = dvrPassword;
                    DVR_User9 = dvrUser;
                    break;
                case 10:
                    m_lUserID10 = m_lUserID;
                    dvrIp10 = dvrIP + "|" + Channel;
                    bch10 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle10 = ClassShow.Preview(Channel, pictureBox10, m_lUserID);
                    label_10.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_10.Visible = true; pictureBox10.Visible = true;
                    passWord10 = dvrPassword;
                    DVR_User10 = dvrUser;
                    break;
                case 11:
                    m_lUserID11 = m_lUserID;
                    dvrIp11 = dvrIP + "|" + Channel;
                    bch11 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle11 = ClassShow.Preview(Channel, pictureBox11, m_lUserID);
                    label_11.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_11.Visible = true; pictureBox11.Visible = true;
                    passWord11 = dvrPassword;
                    DVR_User11 = dvrUser;
                    break;
                case 12:
                    m_lUserID12 = m_lUserID;
                    dvrIp12 = dvrIP + "|" + Channel;
                    bch12 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle12 = ClassShow.Preview(Channel, pictureBox12, m_lUserID);
                    label_12.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_12.Visible = true; pictureBox12.Visible = true;
                    passWord12 = dvrPassword;
                    DVR_User12 = dvrUser;
                    break;
                case 13:
                    m_lUserID13 = m_lUserID;
                    dvrIp13 = dvrIP + "|" + Channel;
                    bch13 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle13 = ClassShow.Preview(Channel, pictureBox13, m_lUserID);
                    label_13.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_13.Visible = true; pictureBox13.Visible = true;
                    passWord13 = dvrPassword;
                    DVR_User13 = dvrUser;
                    break;
                case 14:
                    m_lUserID14 = m_lUserID;
                    dvrIp14 = dvrIP + "|" + Channel;
                    bch14 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle14 = ClassShow.Preview(Channel, pictureBox14, m_lUserID);
                    label_14.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_14.Visible = true; pictureBox14.Visible = true;
                    passWord14 = dvrPassword;
                    DVR_User14 = dvrUser;
                    break;
                case 15:
                    m_lUserID15 = m_lUserID;
                    dvrIp15 = dvrIP + "|" + Channel;
                    bch15 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle15 = ClassShow.Preview(Channel, pictureBox15, m_lUserID);
                    label_15.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_15.Visible = true; pictureBox15.Visible = true;
                    passWord15 = dvrPassword;
                    DVR_User15 = dvrUser;
                    break;
                case 16:
                    m_lUserID16 = m_lUserID;
                    dvrIp16 = dvrIP + "|" + Channel;
                    bch16 = DVR_BchID + "|" + DVR_BchName;
                    m_lRealHandle16 = ClassShow.Preview(Channel, pictureBox16, m_lUserID);
                    label_16.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ListAnalogChannel(m_lUserID, Channel);
                    label_16.Visible = true; pictureBox16.Visible = true;
                    passWord16 = dvrPassword;
                    DVR_User16 = dvrUser;
                    break;
                default:
                    break;
            }


        }

        public string ListAnalogChannel(Int32 m_lUserID, Int16 m_lChannel)
        {
            string str2;
            UInt32 dwReturn = 0;
            Int32 nSize = Marshal.SizeOf(m_struPicCfgV40);
            IntPtr ptrPicCfg = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(m_struPicCfgV40, ptrPicCfg, false);
            if (!CHCNetSDK.NET_DVR_GetDVRConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_PICCFG_V40, m_lChannel, ptrPicCfg, (UInt32)nSize, ref dwReturn))
            {
                str2 = "Cam " + m_lChannel.ToString();
            }
            else
            {
                m_struPicCfgV40 = (CHCNetSDK.NET_DVR_PICCFG_V40)Marshal.PtrToStructure(ptrPicCfg, typeof(CHCNetSDK.NET_DVR_PICCFG_V40));
                if (m_struPicCfgV40.dwShowChanName == 1)
                {
                    str2 = "/ ";
                }
                else
                {
                    str2 = "X ";
                }
                str2 += System.Text.Encoding.GetEncoding("GBK").GetString(m_struPicCfgV40.sChanName);
            }
            Marshal.FreeHGlobal(ptrPicCfg);
            return str2;
        }

        void OpenLarge(string pEmp, Int32 m_lRealHandle, Int32 m_lUserID, string dvrIP, string pBch, string dvrUser, string dvrPassword)
        {
            string[] bch = pBch.Split('|');
            string[] dvr = dvrIP.Split('|');
            if (m_lUserID < 0) { return; }
            if (pEmp.ToUpper().Contains("EMP"))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับกล้องว่าง ไม่สามารถเปิดหน้าจอใหญ่ได้.");
                return;
            }
            ShowSingleLarge _ShowSingleLarge = new ShowSingleLarge
            {

                DVR_TypeCam = typeCam,
                DVR_BchID = bch[0],
                DVR_BchName = bch[1],
                DVR_CH = int.Parse(dvr[1]),
                DVR_IP = dvr[0],
                pJOBMN = "",
                DVR_User = dvrUser,
                DVR_Password = dvrPassword,
                pCase = "1",
                m_lUserID = m_lUserID,
                m_lRealHandle = m_lRealHandle
            };
            //DVR_Password,
            _ShowSingleLarge.Show();

        }

        #region "OpenLarge-DoubleClick"
        private void PictureBox1_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_1.Text, m_lRealHandle1, m_lUserID1, dvrIp1, bch1, DVR_User1, passWord1);
        }

        private void PictureBox2_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_2.Text, m_lRealHandle2, m_lUserID2, dvrIp2, bch2, DVR_User2, passWord2);
        }

        private void PictureBox3_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_3.Text, m_lRealHandle3, m_lUserID3, dvrIp3, bch3, DVR_User3, passWord3);
        }

        private void PictureBox4_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_4.Text, m_lRealHandle4, m_lUserID4, dvrIp4, bch4, DVR_User4, passWord4);
        }

        private void PictureBox5_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_5.Text, m_lRealHandle5, m_lUserID5, dvrIp5, bch5, DVR_User5, passWord5);
        }

        private void PictureBox6_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_6.Text, m_lRealHandle6, m_lUserID6, dvrIp6, bch6, DVR_User6, passWord6);
        }

        private void PictureBox7_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_7.Text, m_lRealHandle7, m_lUserID7, dvrIp7, bch7, DVR_User7, passWord7);
        }

        private void PictureBox8_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_8.Text, m_lRealHandle8, m_lUserID8, dvrIp8, bch8, DVR_User8, passWord8);
        }

        private void PictureBox9_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_9.Text, m_lRealHandle9, m_lUserID9, dvrIp9, bch9, DVR_User9, passWord9);
        }

        private void PictureBox10_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_10.Text, m_lRealHandle10, m_lUserID10, dvrIp10, bch10, DVR_User10, passWord10);
        }

        private void PictureBox11_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_11.Text, m_lRealHandle11, m_lUserID11, dvrIp11, bch11, DVR_User11, passWord11);
        }

        private void PictureBox12_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_12.Text, m_lRealHandle12, m_lUserID12, dvrIp12, bch12, DVR_User12, passWord12);
        }

        private void PictureBox13_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_13.Text, m_lRealHandle13, m_lUserID13, dvrIp13, bch13, DVR_User13, passWord13);
        }

        private void PictureBox14_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_14.Text, m_lRealHandle14, m_lUserID14, dvrIp14, bch14, DVR_User14, passWord14);
        }

        private void PictureBox15_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_15.Text, m_lRealHandle15, m_lUserID15, dvrIp15, bch15, DVR_User15, passWord15);
        }

        private void PictureBox16_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_16.Text, m_lRealHandle16, m_lUserID16, dvrIp16, bch16, DVR_User16, passWord16);
        }

        #endregion


        void SetLogout()
        {
            if (m_lRealHandle1 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle1); m_lRealHandle1 = -1; }
            if (m_lRealHandle2 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle2); m_lRealHandle2 = -1; }
            if (m_lRealHandle3 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle3); m_lRealHandle3 = -1; }
            if (m_lRealHandle4 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle4); m_lRealHandle4 = -1; }
            if (m_lRealHandle5 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle5); m_lRealHandle5 = -1; }
            if (m_lRealHandle6 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle6); m_lRealHandle6 = -1; }
            if (m_lRealHandle7 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle7); m_lRealHandle7 = -1; }
            if (m_lRealHandle8 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle8); m_lRealHandle8 = -1; }
            if (m_lRealHandle9 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle9); m_lRealHandle9 = -1; }
            if (m_lRealHandle10 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle10); m_lRealHandle10 = -1; }
            if (m_lRealHandle11 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle11); m_lRealHandle11 = -1; }
            if (m_lRealHandle12 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle12); m_lRealHandle12 = -1; }
            if (m_lRealHandle13 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle13); m_lRealHandle13 = -1; }
            if (m_lRealHandle14 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle14); m_lRealHandle14 = -1; }
            if (m_lRealHandle15 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle15); m_lRealHandle15 = -1; }
            if (m_lRealHandle16 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle16); m_lRealHandle16 = -1; }

            //注销登录
            if (m_lUserID1 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID1); m_lUserID1 = -1; }
            if (m_lUserID2 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID2); m_lUserID2 = -1; }
            if (m_lUserID3 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID3); m_lUserID3 = -1; }
            if (m_lUserID4 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID4); m_lUserID4 = -1; }
            if (m_lUserID5 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID5); m_lUserID5 = -1; }
            if (m_lUserID6 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID6); m_lUserID6 = -1; }
            if (m_lUserID7 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID7); m_lUserID7 = -1; }
            if (m_lUserID8 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID8); m_lUserID8 = -1; }
            if (m_lUserID9 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID9); m_lUserID9 = -1; }
            if (m_lUserID10 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID10); m_lUserID10 = -1; }
            if (m_lUserID11 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID11); m_lUserID11 = -1; }
            if (m_lUserID12 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID12); m_lUserID12 = -1; }
            if (m_lUserID13 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID13); m_lUserID13 = -1; }
            if (m_lUserID14 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID14); m_lUserID14 = -1; }
            if (m_lUserID15 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID15); m_lUserID15 = -1; }
            if (m_lUserID16 >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID16); m_lUserID16 = -1; }

            dvrIp1 = "";
            dvrIp2 = "";
            dvrIp3 = "";
            dvrIp4 = "";
            dvrIp5 = "";
            dvrIp6 = "";
            dvrIp7 = "";
            dvrIp8 = "";
            dvrIp9 = "";
            dvrIp10 = "";
            dvrIp11 = "";
            dvrIp12 = "";
            dvrIp13 = "";
            dvrIp14 = "";
            dvrIp15 = "";
            dvrIp16 = "";

            bch1 = "";
            bch2 = "";
            bch3 = "";
            bch4 = "";
            bch5 = "";
            bch6 = "";
            bch7 = "";
            bch8 = "";
            bch9 = "";
            bch10 = "";
            bch11 = "";
            bch12 = "";
            bch13 = "";
            bch14 = "";
            bch15 = "";

            passWord1 = "";
            passWord2 = "";
            passWord3 = "";
            passWord4 = "";
            passWord5 = "";
            passWord6 = "";
            passWord7 = "";
            passWord8 = "";
            passWord9 = "";
            passWord10 = "";
            passWord11 = "";
            passWord12 = "";
            passWord13 = "";
            passWord14 = "";
            passWord15 = "";
            passWord16 = "";

            DVR_User1 = "";
            DVR_User2 = "";
            DVR_User3 = "";
            DVR_User4 = "";
            DVR_User5 = "";
            DVR_User6 = "";
            DVR_User7 = "";
            DVR_User8 = "";
            DVR_User9 = "";
            DVR_User10 = "";
            DVR_User11 = "";
            DVR_User12 = "";
            DVR_User13 = "";
            DVR_User14 = "";
            DVR_User15 = "";
            DVR_User16 = "";

            CHCNetSDK.NET_DVR_Cleanup();
            for (int i = 0; i < 16; i++)
            {
                SetPathEmp(i + 1);
            }

        }

    }
}

