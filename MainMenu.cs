﻿using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs
{
    public partial class MainMenu : Form
    {
        private DateTime pTimeClose;

        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {
            using (Login Login = new Login())
            {
                DialogResult dr = Login.ShowDialog();
                if (dr == DialogResult.OK)
                {

                    LoadForm();
                    LoadMenus(Controllers.SystemClass.SystemDptID);
                }
                else
                {
                    this.Close();
                }
            }


        }
        private void LoadForm()
        {
            if (Controllers.SystemClass.SystemBranchID == "MN000")
            {
                this.Text = Controllers.SystemClass.SystemVersion + " {" + Controllers.SystemClass.SystemVersionShop24HrsCurrent_SUPC + "} " +
                    Controllers.SystemClass.SystemHeadprogram + " [" +
                    Controllers.SystemClass.SystemBranchID + " " + Controllers.SystemClass.SystemBranchName + "]";
                pTimeClose = DateTime.Now.AddHours(10);
            }
            else
            {
                this.Text = Controllers.SystemClass.SystemVersion + " {" + Controllers.SystemClass.SystemVersionShop24HrsCurrent_SHOP + "} " +
                    Controllers.SystemClass.SystemHeadprogram + " [" +
                    Controllers.SystemClass.SystemBranchID + " " + Controllers.SystemClass.SystemBranchName + "]";
                pTimeClose = DateTime.Now.AddHours(2);
            }

            this.Font = Controllers.SystemClass.SetFontGernaral;
            this.Icon = PC_Shop24Hrs.Properties.Resources.Shop24Hrs2020;

        }

        private void LoadMenus(string _dept)
        {
            DataTable DtuserMenus;

            if (!string.IsNullOrEmpty(_dept))
            {
                DtuserMenus = Class.MenuPermissionClass.GetMenu(_dept, Controllers.SystemClass.SystemBranchID);
                if (DtuserMenus.Rows.Count > 0)
                {
                    foreach (DataRow row in DtuserMenus.Rows)
                    {
                        RadListDataItem listItem = new RadListDataItem();
                        if (row["MENU_ID"].ToString() == "SETTING")
                        {
                            listItem.Text = row["MENU_NAME"].ToString();
                        }
                        else
                        {
                            listItem.Text = row["MENU_NAME"].ToString();
                        }
                        listItem.Tag = row["MENU_ID"].ToString();
                        radListControl_Menu.Items.Add(listItem);
                    }

                    if (Controllers.SystemClass.SystemDptID == "D001")
                    { radListControl_Menu.Items[radListControl_Menu.Items.Count - 1].Selected = true; }
                    else { radListControl_Menu.Items[0].Selected = true; }

                    Timer_Close.Start();
                }

                else
                {
                    MessageBox.Show("ผู้ใช้งานไม่มีสิทธิ์ในการใช้งานโปรแกรม " + Environment.NewLine + "รบกวนติดต่อ ComMinimart Tel 8570 หรือ Mail Com-Minimark " + Environment.NewLine + "เพื่อแจ้งรหัสแผนกในการเพิ่มสิทธิ์.",
                        Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();

                }
            }

        }

        private void RadListControl_Menu_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (radListControl_Menu.SelectedItem != null)
            {
                switch (radListControl_Menu.SelectedItem.Tag.ToString())
                {
                    case "D000": // ผู้ดูแลระบบ
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D000_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "ผู้ดูแลระบบ.";
                        break;
                    case "D003": // แผนก ร.ป.ภ.
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D003_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนก ร.ป.ภ..";
                        break;
                    case "D004": //แผนกข้อมูลคอมพิวเตอร์ และสถิติ
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D004_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกข้อมูลคอมพิวเตอร์ และสถิติ.";
                        break;
                    case "D010": //แผนกแคชเชียร์เงินสด
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D010_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกแคชเชียร์เงินสด.";
                        break;
                    case "D011": //แผนกแคชเชียร์เครดิต
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D011_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกแคชเชียร์เครดิต.";
                        break;
                    case "D020": // แผนกขายส่งและลูกค้าสัมพันธ์
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D020_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกขายส่งและลูกค้าสัมพันธ์.";
                        break;
                    case "D023": // แผนกเช็คสินค้าขึ้นรถ
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D023_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกเช็คสินค้าขึ้นรถ.";
                        break;
                    case "D027": // จัดซื้อ-คอนซูเมอร์(อร่อยดี,ฟาร์มเฮ้าส์)
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D027_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "จัดซื้อ-คอนซูเมอร์(อร่อยดี,ฟาร์มเฮ้าส์).";
                        break;
                    case "D029": // แผนกจัดซื้อ
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D029_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกจัดซื้อ-คอมพิวเตอร์.";
                        break;
                    case "D033": // แผนกคลังสินค้าอาคาร CD
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D033_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกคลังสินค้าอาคาร CD.";
                        break;
                    case "D035": // แผนกรับคืนสินค้า C/N
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D035_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกรับคืนสินค้า C/N.";
                        break;
                    case "D038"://แผนกโปรโมชั่น ของแถม
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D038_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกโปรโมชั่น ของแถม.";
                        break;
                    case "D041"://ฝ่ายอาคารและสถานที่
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D041_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "ฝ่ายอาคารและสถานที่.";
                        break;
                    case "D054": //แผนกดูแลระบบงานมินิมาร์ทสาขา
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D054_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกดูแลระบบงานมินิมาร์ทสาขา.";
                        break;
                    case "D057": //แผนกหน่วยรถขาออก
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D057_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกหน่วยรถขาออก.";
                        break;
                    case "D062"://แผนกเบิกสินค้าภายใน
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D062_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกเบิกสินค้าภายใน.";
                        break;
                    case "D070"://ฝ่ายผลิต - ครัว
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D070_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "ฝ่ายผลิต - ครัว.";
                        break;
                    case "D088"://แผนกตรวจสินค้าส่วนหน้า
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D088_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกตรวจสินค้าส่วนหน้า.";
                        break;
                    case "D100": // แผนกจัดซื้อ-มือถือ
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D100_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกจัดซื้อ-มือถือ.";
                        break;
                    case "D103": // แผนกบุคคล
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D103_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกบุคคล.";
                        break;
                    case "D147": //แผนกคลังสินค้าอาคาร ช้าง
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D147_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกคลังสินค้าอาคาร ช้าง.";
                        break;
                    case "D150": //ระบบเครดิต มินิมาร์ท
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D150_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "ระบบเครดิต มินิมาร์ท.";
                        break;
                    case "D153": // แผนกบัญชีลูกหนี้
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D153_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกบัญชีลูกหนี้.";
                        break;
                    case "D162": //ฝ่ายผลิต-โรงน้ำแข็ง
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D162_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกฝ่ายผลิต-โรงน้ำแข็ง.";
                        break;
                    case "D168": //แผนกจัดสินค้าเครดิต
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D168_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกจัดสินค้าเครดิต.";
                        break;
                    case "D177": //แผนกฝ่ายผลิต-วัสดุสิ้นเปลือง
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D177_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกฝ่ายผลิต-วัสดุสิ้นเปลือง.";
                        break;
                    case "D179"://แผนกคอมพิวเตอร์-ช่างดูแลระบบคอมฯ
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D179_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกคอมพิวเตอร์-ช่างดูแลระบบคอมฯ.";
                        break;
                    case "D194"://แผนกแผนกตรวจบิลขาเข้ามินิมาร์ท
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D194_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกตรวจบิลขาเข้ามินิมาร์ท.";
                        break;
                    case "D204"://แผนกจัดส่งสินค้ามินิมาร์ท
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D204_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกจัดส่งสินค้ามินิมาร์ท.";
                        break;
                    case "D999"://แผนกงานแจ้งซ่อม&กล้องวงจรปิด
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D999_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "งานแจ้งซ่อม&กล้องวงจรปิด.";
                        break;
                    case "D9BS"://แผนกกล้องวงจรปิด
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.D9BS_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "กล้องวงจรปิด [โก้].";
                        break;
                    case "Purchase": //แผนกจัดซื้อทั่วไป
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.Purchase_MainMenu(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกจัดซื้อทั่วไป.";
                        break;
                    case "Purchase_F": //แผนกจัดซื้อ อาหารสด
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.Purchase_FreshFood(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "แผนกจัดซื้อ อาหารสด.";
                        break;


                    case "Genaral"://SHOP - งานทั่วไป
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.SHOP_Genaral(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "งานทั่วไป.";
                        break;
                    case "GenaralAdd"://SHOP - งานเพิ่มเติม
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.SHOP_GenaralAddOn(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "งานเพิ่มเติม.";
                        break;
                    case "ITEM"://SHOP - สินค้า
                        radDock2.ActivateMdiChild(Controllers.FormClass.OpenForm(new Mainmenu_Dpt.SHOP_Item(), this, "-e-", "Form", radDock2));
                        radCollapsiblePanel2.HeaderText = "Shop24Hrs. - " + "สินค้า.";
                        break;


                    default:
                        break;
                }
            }
        }

        private void Timer_Close_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > pTimeClose)
            {
                Timer_Close.Stop();
                MessageBox.Show("โปรแกรมเปิดใช้งานนานเกินกว่าเวลาที่กำหนด " + Environment.NewLine + " ให้ปิดโปรแกรมแล้วเปิดใช้งานใหม่อีกครั้ง ",
                      Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
    }
}
