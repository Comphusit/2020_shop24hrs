﻿using PC_Shop24Hrs.GeneralForm.TimeKeeper;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Net.Sockets;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using System.Collections;

namespace PC_Shop24Hrs
{
    public partial class Login : Telerik.WinControls.UI.RadForm
    {
        string versionUse;
        DialogResult dr;
        //สำหรับการ Setting ค่าเข้า AX2012
        void SetAX2012()
        {
            if (checkBox_ax2012.Checked == true)
            {
                SystemClass.SystemAX2012 = "1";
                Class.Models.AssetClass = new Class.Asset.AssetClass2012();
                Class.Models.EmplClass = new Class.Empl.EmplClass2012();
                Class.Models.VendTableClass = new Class.Vend.VendTableClass2012();
                Class.Models.DptClass = new Class.Dpt.DptClass2012();
                Class.Models.InventClass = new Class.Invent.InventClass2012();
                Class.Models.CustomerClass = new Class.Cust.CustomerClass2012();
                Class.Models.ScaleClass = new Class.Scale.ScaleClass2012();
                Class.Models.ARClass = new Class.AR.ARClass2012();
                Class.Models.ManageCameraClass = new Class.ManageCamera.ManageCameraClass2012();
            }
        }

        public Login()
        {
            InitializeComponent();
        }
        //Clear Form
        private void ClearFrom()
        {
            //Set RadDropDownList All
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Branch);
            radDropDownList_Branch.DropDownListElement.TextBox.Font = SystemClass.SetFont12;
            this.radDropDownList_Branch.DropDownListElement.ListElement.Font = new Font("Tahoma", 12);
            this.radDropDownList_Branch.AutoSizeItems = true;
            this.radDropDownList_Branch.DropDownListElement.AutoCompleteSuggest.SuggestMode = Telerik.WinControls.UI.SuggestMode.Contains;
            this.radDropDownList_Branch.DropDownListElement.AutoCompleteSuggest.DropDownList.Popup.Font = new Font("Tahoma", 12);
            this.radDropDownList_Branch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Branch.DropDownListElement.AutoCompleteSuggest.DropDownList.ListElement.ItemHeight = 50;

            this.Font = SystemClass.SetFontGernaral;
        }
        //GetIP
        private string GetIP()
        {
            string ipA = "";
            var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipA = ip.ToString();
                }
            }
            return ipA;
        }
        //Main Load
        private void Login_Load(object sender, EventArgs e)
        {

            RadButton_ChangePassword.ButtonElement.ToolTipText = "จัดการรหัสผ่าน [ระบุรหัสพนักงานก่อนกด]";

            SystemClass.SystemPcIp = GetIP();// System.Net.Dns.GetHostName();
            SystemClass.SystemPcName = System.Environment.MachineName;

            //Main Load All Connection
            ConnectionControl.GetConnectionString();
            ClearFrom();

            //config_MachineShop24Hrs
            DataTable DtTypeCom = ConnectionClass.SelectSQL_Main(" config_PERMISSIONMachine '" + SystemClass.SystemPcName + "','A01' ");
            SetCbBranch(DtTypeCom);
            CheckVersion();

            switch (SystemClass.SystemPcName)
            {
                case "SPCN1467": textBox_EmplID.Text = "0604049"; textBox_Password.Text = "8002"; checkBox_ax2012.Visible = true; break;
                case "SPCN1468": textBox_EmplID.Text = "2203025"; textBox_Password.Text = "260342"; checkBox_ax2012.Visible = true; break;
                case "SPCN1824": textBox_EmplID.Text = "2210067"; textBox_Password.Text = "46220"; checkBox_ax2012.Visible = true; break;
                //เครื่องโก้
                case "SPC1252": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;
                case "SPCN435": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;
                case "SPCN1464": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;
                case "SPCN1463": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;
                case "SPCN1517": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;
                case "SPCN1541": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;//เครื่องคุณกิ๊ก
                case "SPCN2317": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;
                case "SPCN2319": textBox_EmplID.Text = "9901999"; textBox_Password.Text = "1"; CheckAllDataItem(); break;//เครื่องคุณกิ๊ก
                default:
                    textBox_EmplID.Text = ""; textBox_Password.Text = ""; textBox_EmplID.Focus();
                    break;
            }

            try
            {
                if (radDropDownList_Branch.SelectedValue.ToString() == "MN000")
                    label_Version.Text = $@"เวอร์ชั่น  {SystemClass.SystemVersion}-{SystemClass.SystemVersionShop24HrsCurrent_SUPC}";
                else
                    label_Version.Text = $@"เวอร์ชั่น  {SystemClass.SystemVersion}-{SystemClass.SystemVersionShop24HrsCurrent_SHOP}";
            }
            catch (Exception)
            {
                label_Version.Text = $@"เวอร์ชั่น  {SystemClass.SystemVersion}-{SystemClass.SystemVersionShop24HrsCurrent_SUPC}";
            }
        }
        //Check Values
        void CheckVersion()
        {
            string bchSelect;
            try
            {
                bchSelect = radDropDownList_Branch.SelectedValue.ToString();
            }
            catch (Exception) { throw; }

            switch (bchSelect)
            {
                case "MN000"://กรณีสาขาใหญ่
                    versionUse = SystemClass.SystemVersion;
                    if (Convert.ToInt16(SystemClass.SystemVersionShop24HrsCurrent_SUPC) <
                            Convert.ToInt16(SystemClass.SystemVersionShop24Hrs_SUPC))
                    {
                        //MessageBox.Show("ไม่สามารถใช้งานโปรแกรมได้ เนื่องจากเวอร์ชั่นไม่ Update ให้ Update โปรแกรม ตาม Link ที่เปิดไว้ก่อนใช้งาน.", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถใช้งานโปรแกรมได้ เนื่องจากเวอร์ชั่นไม่ Update ให้ Update โปรแกรม ตาม Link ที่เปิดไว้ก่อนใช้งาน.");
                        ProcessStartInfo startInfo = new ProcessStartInfo("IExplore.exe")
                        {
                            WindowStyle = ProcessWindowStyle.Maximized,
                            Arguments = SystemClass.SystemPathUpdateShop24Hrs_SUPC

                        };
                        Process.Start(startInfo);
                        Application.Exit();
                    }
                    break;
                default://กรณีสาขา
                    versionUse = SystemClass.SystemVersion;
                    if (Convert.ToInt16(SystemClass.SystemVersionShop24HrsCurrent_SHOP) <
                        Convert.ToInt16(SystemClass.SystemVersionShop24Hrs_SHOP))
                    {
                        //MessageBox.Show("ไม่สามารถใช้งานโปรแกรมได้ เนื่องจากเวอร์ชั่นไม่ Update ให้ Update โปรแกรม ตาม Link ที่เปิดไว้ก่อนใช้งาน.",SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถใช้งานโปรแกรมได้ เนื่องจากเวอร์ชั่นไม่ Update ให้ Update โปรแกรม ตาม Link ที่เปิดไว้ก่อนใช้งาน.");
                        ProcessStartInfo startInfo = new ProcessStartInfo("IExplore.exe")
                        {
                            WindowStyle = ProcessWindowStyle.Maximized,
                            Arguments = SystemClass.SystemPathUpdateShop24Hrs_SHOP
                        };
                        Process.Start(startInfo);
                        Application.Exit();
                    }
                    break;
            }
        }
        //set ค่าของ radCombo select branch before Use
        void SetCbBranch(DataTable DtBranch)
        {
            radDropDownList_Branch.DataSource = DtBranch;
            radDropDownList_Branch.ValueMember = "BRANCH_ID";
            radDropDownList_Branch.DisplayMember = "BRANCH_NAME";
        }
        // click OK
        private void Button_singin_Click(object sender, EventArgs e)
        {
            if (CheckPassword(textBox_Password.Text)) return; else CheckAllDataItem();

        }
        //textbox user enter
        private void TextBox_EmplID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) textBox_Password.Focus();
        }
        //textbox password enter
        private void TextBox_Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (CheckPassword(textBox_Password.Text)) return; else CheckAllDataItem();
            }
        }
        //Exit Program
        private void Button_singout_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Select change Branch
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            textBox_EmplID.Focus();
        }
        //Check user+Pass+Branch
        private void CheckAllDataItem()
        {
            //เชค รหัสพนักงาน
            if (String.IsNullOrEmpty(textBox_EmplID.Text))
            {
                //MessageBox.Show("ระบุรหัสผู้ใช้งานก่อนเข้าใช้โปรแกรม." + Environment.NewLine + "[ไม่ต้องใส่ M หรือ D]",
                //    SystemClass.SystemHeadprogram,
                //    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุรหัสผู้ใช้งานก่อนเข้าใช้โปรแกรม." + Environment.NewLine + "[ไม่ต้องใส่ M หรือ D]");
                textBox_Password.SelectAll();
                return;
            }
            //เชครหัสผ่าน
            if (string.IsNullOrEmpty(textBox_Password.Text))
            {
                //MessageBox.Show("ระบุรหัสผ่านผู้ใช้งานก่อนเข้าใช้โปรแกรม.",Controllers.SystemClass.SystemHeadprogram,MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุรหัสผ่านผู้ใช้งานก่อนเข้าใช้โปรแกรม.");
                textBox_Password.SelectAll();
                return;
            }
            //เชค สาขา เชค ผู้ใช้

            GetBch();

        }

        //Check user+Pass+Brannh
        private bool CheckPassword(string Password)
        {
            bool Rsu = false;
            if (textBox_EmplID.Text == Password)
            {
                //MessageBox.Show("กรุณาเปลี่ยนรหัสผ่านก่อนใช้งาน.",Controllers.SystemClass.SystemHeadprogram,MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                MsgBoxClass.MsgBoxShowButtonOk_Error("กรุณาเปลี่ยนรหัสผ่านก่อนใช้งาน.");

                DataTable dtEmpAX = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(textBox_EmplID.Text);
                if (dtEmpAX.Rows.Count == 0)
                {
                    //MessageBox.Show("รหัสพนักงานไม่ถูกต้อง ลองใหม่อีกครั้ง.",Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสพนักงานไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                    textBox_EmplID.Text = "";
                    textBox_EmplID.Focus();
                    Rsu = true;
                }

                if (SetPassword(dtEmpAX.Rows[0]["ALTNUM"].ToString(), dtEmpAX.Rows[0]["SPC_NAME"].ToString())) Rsu = true;
            }
            return Rsu;
        }

        //Check Branch
        private void GetBch()
        {
            SetAX2012();

            DataTable DtEmpl = Class.Models.EmplClass.GetEmployee(textBox_EmplID.Text);
            if (DtEmpl.Rows.Count == 0)
            {
                DataTable dtEmpAX = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(textBox_EmplID.Text);
                if (dtEmpAX.Rows.Count == 0)
                {
                    //MessageBox.Show("รหัสพนักงานไม่ถูกต้อง ลองใหม่อีกครั้ง.",Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสพนักงานไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                    textBox_EmplID.Text = "";
                    textBox_EmplID.Focus();
                    return;
                }
                else
                {
                    SetPassword(dtEmpAX.Rows[0]["ALTNUM"].ToString(), dtEmpAX.Rows[0]["SPC_NAME"].ToString());
                    return;
                }
            }

            string Pass = Class.EncryptDataClass.DecodePassword(DtEmpl.Rows[0]["EMP_PASSWORD"].ToString());
            if (Pass != textBox_Password.Text)
            {
                //MessageBox.Show("รหัสผ่านไม่ถูกต้อง ลองใหม่อีกครั้ง.", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสผ่านไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                textBox_Password.Text = "";
                textBox_Password.Focus();
                return;
            }

            if (DtEmpl.Rows[0]["IVZ_HRPAResignationDate"].ToString() != "1900-01-01")
            {
                //MessageBox.Show("พนักงานทีสถานะลาออกเรียบร้อยแล้ว ไม่สามารถเข้าใช้งานโปรแกรมได้.",SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานทีสถานะลาออกเรียบร้อยแล้ว ไม่สามารถเข้าใช้งานโปรแกรมได้.");
                textBox_Password.Text = "";
                textBox_Password.Focus();
                return;
            }

            SystemClass.SystemUserID = DtEmpl.Rows[0]["EMPLID"].ToString();
            SystemClass.SystemUserName = DtEmpl.Rows[0]["SPC_NAME"].ToString();
            SystemClass.SystemUserID_M = DtEmpl.Rows[0]["EMPLID_M"].ToString();
            SystemClass.SystemDptID = DtEmpl.Rows[0]["NUM"].ToString();
            SystemClass.SystemDptName = DtEmpl.Rows[0]["DESCRIPTION"].ToString();
            SystemClass.SystemUserNameShort = DtEmpl.Rows[0]["IVZ_HRPANickName"].ToString();
            SystemClass.SystemUserPositionName = DtEmpl.Rows[0]["POSSITION"].ToString();
            SystemClass.SystemUserPositionID = DtEmpl.Rows[0]["IVZ_HROMPositionTitle"].ToString();
            SystemClass.SystemComMinimart = DtEmpl.Rows[0]["EMP_STACOMMN"].ToString();
            SystemClass.SystemUserLevelMinimart = DtEmpl.Rows[0]["EMP_TRANSTOCK"].ToString();

            DataTable DtBranch;
            DtBranch = Class.BranchClass.GetBranchSettingBranch(radDropDownList_Branch.SelectedValue.ToString());
            if (DtBranch.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การตั้งค่าสาขาที่ใช้งานยังไม่เรียบร้อย{Environment.NewLine}ติดต่อ ComMinimart Tel.8570 เพื่อดำเนินการให้เรียบร้อย{Environment.NewLine}[SHOP_BranchConfig]");
                ClearFrom();
                return;
            }
            else if (DtBranch.Rows[0]["BRANCH_CHANNEL"].ToString() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("การตั้งค่าคลังสินค้าสาขาที่ใช้งานยังไม่เรียบร้อย " + Environment.NewLine +
                   "ติดต่อ ComMinimart Tel.8570 เพื่อดำเนินการให้เรียบร้อย " + Environment.NewLine +
                   "[SHOP_BranchConfig]");
                ClearFrom();
                return;
            }
            else if (DtBranch.Rows[0]["BRANCH_SERVERNAME"].ToString() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("การตั้งค่า Server POS สาขาที่ใช้งานยังไม่เรียบร้อย " + Environment.NewLine +
                   "ติดต่อ ComMinimart Tel.8570 เพื่อดำเนินการให้เรียบร้อย " + Environment.NewLine +
                   "[SHOP_BranchConfig]");
                ClearFrom();
                return;
            }
            else if (DtBranch.Rows[0]["BRANCH_PATHPOS"].ToString() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("การตั้งค่า Server VDO POS สาขาที่ใช้งานยังไม่เรียบร้อย " + Environment.NewLine +
                   "ติดต่อ ComMinimart Tel.8570 เพื่อดำเนินการให้เรียบร้อย " + Environment.NewLine +
                   "[SHOP_BranchConfig]");
                ClearFrom();
                return;
            }

            SystemClass.SystemBranchID = DtBranch.Rows[0]["BRANCH_ID"].ToString();
            SystemClass.SystemBranchName = DtBranch.Rows[0]["BRANCH_NAME"].ToString();
            SystemClass.SystemBranchPrice = DtBranch.Rows[0]["BRANCH_PRICE"].ToString();
            SystemClass.SystemBranchServer = DtBranch.Rows[0]["BRANCH_SERVERNAME"].ToString();
            SystemClass.SystemBranchOutPhuket = DtBranch.Rows[0]["BRANCH_OutPhuket"].ToString();
            SystemClass.SystemBranchInventID = DtBranch.Rows[0]["BRANCH_CHANNEL"].ToString();
            SystemClass.SystemBranchStaShelf = DtBranch.Rows[0]["BRANCH_STATUSSHELF"].ToString();

            IpServerConnectClass.ConMainRatailChannel = @"Data Source=" + DtBranch.Rows[0]["BRANCH_SERVERNAME"].ToString() +
                            ";Initial Catalog=" + DtBranch.Rows[0]["BRANCH_DATABASENAME"].ToString() +
                            ";User Id=" + DtBranch.Rows[0]["BRANCH_USERNAME"].ToString() +
                            ";Password=" + DtBranch.Rows[0]["BRANCH_PASSWORDNAME"].ToString() + " ";

            switch (SystemClass.SystemUserID)
            {
                case "0604049": SystemClass.SystemComProgrammer = "1"; break;
                default:
                    SystemClass.SystemComProgrammer = "0";
                    break;
            }

            UpdateLastUse();
            //RunJob();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        //Number Only
        private void TextBox_EmplID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Update การใช้งาน ทั้งหมด
        void UpdateLastUse()
        {
            ArrayList sql = new ArrayList {
             $@"UPDATE  SHOP_BRANCH_PERMISSION SET VERSIONUPD = '{versionUse}',BRANCHUPD = '{ SystemClass.SystemBranchID}',
                        DATEUPD = GETDATE(),WHOIDUPD = '{SystemClass.SystemUserID}',
                        WHONAMEUPD = '{SystemClass.SystemUserName}',REMARK = '{DateTime.Now}'
                WHERE   COM_NAME = '{SystemClass.SystemPcName}' AND SHOW_ID = 'A01' ",
             $@"UPDATE SHOP_EMPLOYEE SET EMP_LASTUSED = GETDATE(),EMP_LASTUSEDSPC = '{SystemClass.SystemPcName}'
                WHERE   EMP_ID = '{SystemClass.SystemUserID}'    " };

            ConnectionClass.ExecuteSQL_ArrayMain(sql);

        }
        //เปลี่ยนรหัสผ่าน
        private void RadButton_ChangePassword_Click(object sender, EventArgs e)
        {
            if (!(textBox_EmplID.Text.Length == 7 || textBox_EmplID.Text.Length == 5)) { return; }
            DataTable dtEmpAX = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(textBox_EmplID.Text);
            if (dtEmpAX.Rows.Count == 0)
            {
                //MessageBox.Show("รหัสพนักงานไม่ถูกต้อง ลองใหม่อีกครั้ง.", Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Error);
                MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสพนักงานไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                textBox_EmplID.Text = "";
                textBox_EmplID.Focus();
                return;
            }
            else
            {
                SetPassword(dtEmpAX.Rows[0]["ALTNUM"].ToString(), dtEmpAX.Rows[0]["SPC_NAME"].ToString());
                return;
            }
        }
        //Set PassWord
        private bool SetPassword(string _EmplID, string _EmplName)
        {
            bool Rsu = true;
            using (ComMinimart.Manage.ChangePassword ChangePassword = new ComMinimart.Manage.ChangePassword())
            {
                ChangePassword._EmplID = _EmplID;
                ChangePassword._EmplName = _EmplName;
                dr = ChangePassword.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    textBox_Password.SelectAll(); Rsu = false;
                }
                return Rsu;
            }
        }
        ////runJOB
        //void RunJob()
        //{
        //    if (SystemClass.SystemBranchID != "MN000")
        //    {
        //        this.Cursor = Cursors.WaitCursor;
        //        if (SystemClass.SystemComMinimart == "1") return;
        //        //Controllers.RunJOBClass.RunOrderByWeb(Controllers.SystemClass.SystemBranchID, Controllers.SystemClass.SystemBranchName);
        //        //RunJOBClass.RunOrderByMNPO("SHOP_MNPO_HD", SystemClass.SystemBranchID, SystemClass.SystemBranchName);
        //        //RunJOBClass.RunReciveBox(SystemClass.SystemBranchID, SystemClass.SystemBranchName);
        //        //RunJOBClass.RunTxtToReciveBox(SystemClass.SystemBranchID, SystemClass.SystemBranchName);
        //        this.Cursor = Cursors.Default;
        //    }
        //    else
        //    {
        //        if (SystemClass.SystemComMinimart == "1") return;
        //        this.Cursor = Cursors.WaitCursor;
        //        //RunJOBClass.RunOrderByMNPO("SHOP_MNPO_HD", "MN098", "ฟาร์มาซี");
        //        //RunJOBClass.RunReciveBox("MN098", "ฟาร์มาซี");
        //        //RunJOBClass.RunTxtToReciveBox("MN098", "ฟาร์มาซี");
        //        this.Cursor = Cursors.Default;
        //    }

        //}

        //Scan เข้าแผนก
        private void PictureBox1_Click(object sender, EventArgs e)
        {
            if ((System.Environment.MachineName == "SPCN1467") || (System.Environment.MachineName == "SPCN1468") || (System.Environment.MachineName == "SPCN1155"))
            {
                EmplScan emplScan = new EmplScan { };
                FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.ScanDptTime("0", "", emplScan), "สแกนนิ้วเข้าแผนก.");
            }
        }
    }
}
