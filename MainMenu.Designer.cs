﻿namespace PC_Shop24Hrs
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radCollapsiblePanel2 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radListControl_Menu = new Telerik.WinControls.UI.RadListControl();
            this.radDock2 = new Telerik.WinControls.UI.Docking.RadDock();
            this.documentContainer2 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.Timer_Close = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            this.radDock1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).BeginInit();
            this.radCollapsiblePanel2.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radListControl_Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock2)).BeginInit();
            this.radDock2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer2)).BeginInit();
            this.SuspendLayout();
            // 
            // radDock1
            // 
            this.radDock1.AutoDetectMdiChildren = true;
            this.radDock1.Controls.Add(this.documentContainer1);
            this.radDock1.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.radDock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock1.DocumentTabsVisible = false;
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(280, 0);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            this.radDock1.Padding = new System.Windows.Forms.Padding(0);
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radDock1.Size = new System.Drawing.Size(200, 200);
            this.radDock1.TabIndex = 0;
            this.radDock1.TabStop = false;
            // 
            // documentContainer1
            // 
            this.documentContainer1.Name = "documentContainer1";
            this.documentContainer1.Padding = new System.Windows.Forms.Padding(0);
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentContainer1.SizeInfo.AbsoluteSize = new System.Drawing.Size(486, 200);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.SizeInfo.SplitterCorrection = new System.Drawing.Size(200, 0);
            this.documentContainer1.SplitterWidth = 8;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radCollapsiblePanel1.HeaderText = "Shop24Hrs.";
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(247, 559);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(280, 561);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Tag = "333";
            this.radCollapsiblePanel1.ThemeName = "Fluent";
            // 
            // radCollapsiblePanel2
            // 
            this.radCollapsiblePanel2.AnimationFrames = 10;
            this.radCollapsiblePanel2.AnimationInterval = 10;
            this.radCollapsiblePanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            this.radCollapsiblePanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radCollapsiblePanel2.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radCollapsiblePanel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radCollapsiblePanel2.HeaderText = "Shop24Hrs.";
            this.radCollapsiblePanel2.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel2.Name = "radCollapsiblePanel2";
            // 
            // radCollapsiblePanel2.PanelContainer
            // 
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.radListControl_Menu);
            this.radCollapsiblePanel2.PanelContainer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel2.PanelContainer.Size = new System.Drawing.Size(212, 559);
            this.radCollapsiblePanel2.ShowHeaderLine = false;
            this.radCollapsiblePanel2.Size = new System.Drawing.Size(248, 561);
            this.radCollapsiblePanel2.TabIndex = 0;
            this.radCollapsiblePanel2.ThemeName = "ControlDefault";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel2.GetChildAt(0))).ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel2.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel2.GetChildAt(0))).AnimationFrames = 10;
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1))).ShowHeaderLine = false;
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1))).HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Left;
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1))).Orientation = System.Windows.Forms.Orientation.Vertical;
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BorderLeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BorderTopColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BorderRightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BorderBottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).TextOrientation = System.Windows.Forms.Orientation.Vertical;
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(0))).StretchVertically = false;
            ((Telerik.WinControls.Primitives.LinePrimitive)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(2))).SeparatorOrientation = Telerik.WinControls.SepOrientation.Vertical;
            ((Telerik.WinControls.Primitives.LinePrimitive)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(2))).FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentPadding;
            ((Telerik.WinControls.Primitives.LinePrimitive)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(2))).GradientAngle = 0F;
            ((Telerik.WinControls.Primitives.LinePrimitive)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(2))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(192)))), ((int)(((byte)(249)))));
            ((Telerik.WinControls.Primitives.LinePrimitive)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.LinePrimitive)(this.radCollapsiblePanel2.GetChildAt(0).GetChildAt(1).GetChildAt(2))).StretchHorizontally = false;
            // 
            // radListControl_Menu
            // 
            this.radListControl_Menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radListControl_Menu.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radListControl_Menu.ItemHeight = 30;
            this.radListControl_Menu.Location = new System.Drawing.Point(0, 0);
            this.radListControl_Menu.Name = "radListControl_Menu";
            this.radListControl_Menu.Size = new System.Drawing.Size(212, 559);
            this.radListControl_Menu.TabIndex = 0;
            this.radListControl_Menu.ThemeName = "Fluent";
            this.radListControl_Menu.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadListControl_Menu_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadListElement)(this.radListControl_Menu.GetChildAt(0))).ItemHeight = 30;
            ((Telerik.WinControls.UI.RadScrollBarElement)(this.radListControl_Menu.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadScrollBarElement)(this.radListControl_Menu.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // radDock2
            // 
            this.radDock2.AutoDetectMdiChildren = true;
            this.radDock2.Controls.Add(this.documentContainer2);
            this.radDock2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock2.DocumentTabsVisible = false;
            this.radDock2.IsCleanUpTarget = true;
            this.radDock2.Location = new System.Drawing.Point(248, 0);
            this.radDock2.MainDocumentContainer = this.documentContainer2;
            this.radDock2.Name = "radDock2";
            // 
            // 
            // 
            this.radDock2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radDock2.Size = new System.Drawing.Size(536, 561);
            this.radDock2.SplitterWidth = 8;
            this.radDock2.TabIndex = 1;
            this.radDock2.TabStop = false;
            this.radDock2.ThemeName = "Fluent";
            // 
            // documentContainer2
            // 
            this.documentContainer2.Name = "documentContainer2";
            // 
            // 
            // 
            this.documentContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentContainer2.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer2.SplitterWidth = 8;
            this.documentContainer2.ThemeName = "Fluent";
            // 
            // Timer_Close
            // 
            this.Timer_Close.Tick += new System.EventHandler(this.Timer_Close_Tick);
            // 
            // MainMenu
            // 
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.radDock2);
            this.Controls.Add(this.radCollapsiblePanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainMenu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel2.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radListControl_Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock2)).EndInit();
            this.radDock2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel2;
        private Telerik.WinControls.UI.Docking.RadDock radDock2;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer2;
        private Telerik.WinControls.UI.RadListControl radListControl_Menu;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private System.Windows.Forms.Timer Timer_Close;
    }
}