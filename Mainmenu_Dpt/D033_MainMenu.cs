﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D033_MainMenu : Form
    {
        public D033_MainMenu()
        {
            InitializeComponent();
        }

        private void D033_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        //งานทั่วไป
        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "D010_ReportCash":
                    break;

                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CountStk":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Main("2"), "รายการนับสต็อก."); break;
                case "StockExcel":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_ImportItem("2"), "ปรับสต็อกสินค้าจาก Excel."); break;
                case "Batch":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_ClearBatch(), "เคลียร์สต็อกชุดงาน."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNCS_ReportItemByJournalID":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Report("0", "2"), "รายงานตรวจนับสต๊อกสินค้า."); break;
                case "SPCN_SumInventCountingMNByVoucherByDate_FindJournalId":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Report("1", "2"), "รายงานเปรียบเทียบยอดขาย/จำนวนสต็อกคงเหลือ."); break;
                case "CountStkByEmp":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Report("2", "2"), "รายงานจำนวนรายการเช็คสต๊อก."); break;
                case "CountStkByEmpHour":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Report("3", "2"), "รายงานจำนวนรายการเช็คสต๊อก[รายชั่วโมง]"); break;
                case "ReportWeight":
                    FormClass.ShowCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("6", "0", "0"), "สินค้าที่รับเข้าไม่มีน้ำหนัก/ปริมาตร[รายชั่วโมง]"); break;
                case "notApv":
                    FormClass.ShowCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("7", "1", "0"), "รายงานสินค้าที่รับเข้าคลังแต่ไม่ลงรายการบัญชี"); break;
                case "ReportDiffBA":
                    FormClass.ShowCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("8", "0", "0"), "รายงานสรุปวันรับสินค้าและวันรับเข้าคลัง"); break;
                default:

                    break;
            }
        }
        //general
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "DROID":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("0", "1", "1"), "รับสินค้าเข้าคลัง."); break;
                case "DROID_D177":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("1", "1", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                case "DROID_LINENUM":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("3", "1", "0"), "รับสินค้าเข้าคลัง จัดเรียงบรรทัดตามบิลผู้จำหน่าย."); break;
                case "DROID_D177NEW":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                default:
                    break;
            }
        }

    }
}
