﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D038_MainMenu : Form
    {
        public D038_MainMenu()
        {
            InitializeComponent();
        }

        private void D038_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "BCH":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                default:
                    break;
            }
        }
        //เบิกกระดาษ
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D038_SmartRO_LoadOrder":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRO_LoadOrder(), "ดึงข้อมูลจัดสินค้าส่งมินิมาร์ท."); break;
                case "D038_SmartRO_Main":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SHOP", ""), "เบิกสินค้าส่งมินิมาร์ท."); break;
                case "reciveOrderMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_FindData("0"), "รับออเดอร์ของสดมินิมาร์ท [MNOR].");
                    break;
                case "MNOR":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_LoadOrder(), "ดึงออเดอร์จัด [MNOR].");
                    break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Rpt_MNPZ":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNPZ"), "นำของรางวัลออก [MNPZ]."); break;
                case "D038_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "RETAILAREA", "17"), "ข้อมูลการรับข้าวกล่อง."); break;
                case "D038_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "17", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง."); break;
                case "Report_Customers":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Report_Customers("1"), "รายงานสมัครสมาชิก"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("7", "0"), "รายงานสมัครสมาชิก"); break;
                case "ReportScan130":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกโปรโมชั่น]."); break;
                case "Rpt_Free":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_Report("0", "0"), "รายงานรายการสินค้าแถมมินิมาร์ท."); break;
                case "Pro24":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_Report("1", "0"), "รายงานการเปิด-ปิด โปรโมชั่น [ของแถม] ผ่านระบบ SHOP24HRS."); break;
                case "MNFR_Detail":
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("4", "0"), "รายงานรายละเอียดการแลกแต้มของลูกค้ามินิมาร์ท."); break;
                case "LineCouponControl":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("4", "0"), "การใช้คูปอง Line@ ร้านเช่า."); break;
                case "SUPC_MNPO_CheckOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("11", "SUPC", "", "", ""), "เช็คสถานะออเดอร์ ระบบจัดสินค้าไปมินิมาร์ททั้งหมด."); break;

                default:
                    break;
            }
        }
        //งานทั่วไป


        private void RadTreeView_Cust_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CstMain":
                    FormClass.ShowCheckForm(new GeneralForm.Customers.Customer_Detail("SUPC", "1"), "รายละเอียดลูกค้า"); break;
                case "AddCustType0":
                    //FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomer("SUPC"), "สมัครสมาชิก ลูกค้าบุคคล."); break;
                    FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomerPerson("SUPC", "CRET"), "สมัครสมาชิก ลูกค้าบุคคล."); break;
                case "AddCustType1":
                    FormClass.ShowNotCheckFormNormal(new GeneralForm.Customers.AddCustomerOther("Company", "SUPC"), "สมัครสมาชิก ลูกค้าองค์กร."); break;
                case "AddCustType2":
                    FormClass.ShowNotCheckFormNormal(new GeneralForm.Customers.AddCustomerOther("", "SUPC"), "สมัครสมาชิก ลูกค้าต่างชาติ."); break;
                default:
                    break;
            }
            //งานทั้วไป

        }

        private void RadTreeView_TabGernaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPZ":
                    FormClass.ShowCheckForm(new GeneralForm.MNPZ.MNPZ(), "ใบรับของรางวัล PZ."); break;
                case "Scan130":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                case "Cust_Rent":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_Report("2", "0"), "รายชื่อผู้เช่าสำหรับจ่ายคูปอง Line@."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Upload_SPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_UpFile(PathImageClass.pPathCheckPriceSUPC), "Upload รูป แสดงผลเครื่องเช็คราคา สาขาใหญ่."); break;

                case "Upload_MN":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_UpFile(PathImageClass.pPathCheckPriceMN), "Upload รูป แสดงผลเครื่องเช็คราคา มินิมาร์ท."); break;
                case "Fix_Group":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroup("02", "กลุ่มสินค้าแลกแต้ม"), "การตั้งค่ากลุ่มสินค้าแลกแต้ม.");
                    break;
                case "Fix_Item":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroupBarcode("02"), "การตั้งค่าสินค้าแลกแต้มตามกลุ่ม.");
                    break;
                case "GrpOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroup("01", "กลุ่มสินค้าสั่ง"), "การตั้งค่ากลุ่มสินค้าสั่ง.");
                    break;
                case "ItemOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroupBarcode("01"), "การตั้งค่าสินค้าสั่งตามกลุ่ม.");
                    break;
                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "promotion":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_Data("1"), "รายละเอียดข้อมูลโปรโมชั่น [ของแถม]."); break;
                case "Img_SUPC":
                    FormClass.ShowNotCheckForm(
                        new FormShare.ShowImage.ShowImage { pathImg = PathImageClass.pPathCheckPriceSUPC + "|*.jpg" }, "รูปที่แสดงในเครื่องเช็คราคาสาขาใหญ่ทั้งหมด."); break;
                case "Img_MN":
                    FormClass.ShowNotCheckForm(
                        new FormShare.ShowImage.ShowImage { pathImg = PathImageClass.pPathCheckPriceMN + "|*.jpg" }, "รูปที่แสดงในเครื่องเช็คราคามินิมาร์ททั้งหมด."); break;
                case "labelPromo":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_AddImg("SHOP","0"), "ป้ายโปรโมชั่น [มินิมารืท]."); break;
                case "labelPromoSupc":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_AddImg("SUPC", "0"), "ป้ายโปรโมชั่น [สาขาใหญ่]."); break;
                default:
                    break;
            }
        }
    }
}
