﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.Logistic;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D003_MainMenu : Form
    {
        public D003_MainMenu()
        {
            InitializeComponent();
        }

        private void D003_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "Camera_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("2", "",""), "กล้องวงจรปิด."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Camera_Check":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Check("2"), "ตรวจกล้องประจำวัน."); break;
                case "D003_scan1_30":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_Scan":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนก ร.ป.ภ.]."); break;
                case "LO":
                    FormClass.ShowCheckForm(new D204_Report("4","0"), "รายงาน LO ส่งสินค้า."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป

    }
}
