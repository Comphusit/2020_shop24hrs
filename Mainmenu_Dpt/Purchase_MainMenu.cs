﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.PhoneBook;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class Purchase_MainMenu : Form
    {
        public Purchase_MainMenu()
        {
            InitializeComponent();
        }

        private void Purchase_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }

        //ข้อมูลทั่วไป

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_DetailProduct_ByDate":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("5"), "รายงานจำนวนสินค้า ขาย-เบิก-คืน."); break;
                case "Report_DetailProduct_TO":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("6"), "รายงานจำนวนสินค้าเบิก."); break;
                case "Report_DetailProduct_CN":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("7"), "รายงานจำนวนสินค้าคืน."); break;
                case "Report_DetailProduct_Sale":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("8"), "รายงานจำนวนสินค้าขาย."); break;
                case "Report_MNPCItems_Dept":
                    //Controllers.FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCItems_Dept("SUPC"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "3"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                case "SUPC_MNPO_CheckOrder1":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "", "", ""), "เช็คออเดอร์."); break;
                case "PriceChangeMain":
                    FormClass.ShowNotCheckForm(new Itembarcode.PriceChange_Report("0"), "รายงานสินค้าที่เปลี่ยนแปลงราคา."); break;
                case "PriceChangeLabel":
                    FormClass.ShowNotCheckForm(new Itembarcode.PriceChange_Report("1"), "รายงานการเปลี่ยนป้ายราคา [ดูแลสินค้า]."); break;
                case "ReportScan130":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกจัดซื้อทั่วไป]."); break;
                case "MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG"), "ส่งของไม่มีทะเบียนคืน [MNRG]."); break;
                case "Detail_MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG", "0"), "รายละเอียดส่งของไม่มีทะเบียน [MNRG]."); break;
                case "MNTF_ReportD044":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNTF.MNTF_Report("0"), "จำนวนการเบิกของจัดซื้อยา."); break;
                case "ReportText18":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("18"), "จำนวนการขายสินค้า ย้อนหลัง 3 เดือน ตามกลุ่มสินค้า [ประเมินการขาย]."); break;
                case "LineCoupon":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("2", "0"), "การใช้ไลน์คูปอง [ละเอียด]."); break;
                case "LineCouponSUM":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("3", "0"), "การใช้ไลน์คูปอง [สรุป]."); break;
                case "DetailBill_POSRetail":
                    FormClass.ShowCheckForm(new ComMinimart.DetailBill_POSRetail(), "รายละเอียดบิลขาย."); break;
                case "StatusOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("2", "SUPC", "", "", ""), "สถานะออร์เดอร์รวม [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "StatusDMS":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("3", "SUPC", "", "", ""), "สถานะออร์เดอร์ตามแผนก [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "StatusMN":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("4", "SUPC", "", "", ""), "จำนวนออเดอร์ตามมินิมาร์ท [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "QtyRowByEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("7", "SUPC", "", "", ""), "จำนวนจัดสินค้าตามพนักงานจัดบิล [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "RptItemNoPacking":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("8", "SUPC", "", "", ""), "รายการสินค้าที่จัดทั้งหมด [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "Performance":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("9", "SUPC", "", "", ""), "ประเมินการจัดบิล ตามพนักงานจัดบิล [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "PerformanceMM":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("10", "SUPC", "", "", ""), "ประเมินการจัดบิล ตามพนักงานจัดบิล รายเดือน [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]."); break;
                case "SUPC_MNPO_CheckOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("11", "SUPC", "", "", ""), "เช็คสถานะออเดอร์ ระบบจัดสินค้าไปมินิมาร์ททั้งหมด."); break;
                case "MachineCountLine":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("12", "SUPC", "", "", ""), "อัตราการใช้งานเครื่อง รายเดือน ระบบจัดสินค้าไปมินิมาร์ททั้งหมด."); break;
                case "RptByTypeByEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("13", "SUPC", "", "", ""), "รายงานการสั่ง ตามประเภทการสั่ง ตามพนักงาน."); break;
                case "RptContainer":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("14", "SUPC", "", "", ""), "รายงาน รายการรอทำบิลในระบบ Container."); break;
                default:
                    break;
            }
        }
        //รูปถ่าย MN
        private void RadTreeView_TabImage_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ImageAllDays_Date":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "0", "0"), "รูปถ่ายประจำวัน ระบุวันที่ ทุกสาขา."); break;
                case "ImageAllDays_M":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "1", "0"), "รูปถ่ายประจำวัน ระบุสาขา ตามช่วงวันที่."); break;
                case "MP":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.MP.MP_Main("SUPC", "0"), "รายละเอียดการรับสินค้าเซลล์ส่ง [MP]."); break;
                case "vender":
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("0"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                case "ImageAllDay3":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "3", "0"), "รูปถ่ายประจำวัน ตามช่วงวันที่ ทุกสาขา."); break;
                case "MA":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("7", "0"), "รับสินค้าที่มินิมาร์ท [MA]."); break;
                default:
                    break;
            }

        }
        //MNTF
        private void RadTreeView_MNTF_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNTF":
                    FormClass.ShowCheckForm(new GeneralForm.MNTF.MNTF_Order("0"), "เบิกสินค้าลงหน้าร้าน [ผ่านระบบ AX]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabScan_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

        }

        private void RadTreeView_Config_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

            switch (e.Node.Name)
            {
                case "ConfigGroup":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroup("01", "กลุ่มสินค้าสั่ง"), "การตั้งค่ากลุ่มสินค้าสั่ง");
                    break;
                case "ConfigItembarcode":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroupBarcode("01"), "การตั้งค่าสินค้าสั่งตามกลุ่มสินค้า");
                    break;
                case "CustYA":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D044"))
                        FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomerPerson("SUPC", "CPHA"), "สมัครสมาชิก ลูกค้าร้านยา [บัตรเหลือง].");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "RouteBeer":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D034"))
                        FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("49", "เส้นทางส่งสินค้าเหล้า-เบียร์", "0", "1", "1", "", "5"), "เส้นทางส่งสินค้าเหล้า-เบียร์.");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "ConfigV_NotPackingOrderWeb":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D014") || (SystemClass.SystemDptID == "D021") || (SystemClass.SystemDptID == "D034") || (SystemClass.SystemDptID == "D048") || (SystemClass.SystemDptID == "D129"))
                        FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("61", "กำหนดผู้จำหน่ายไม่จัดออเดอร์แผนก [แผนกจัดสินค้าเครดิตจัดบิล]", "1", "1", "1", "", ""), "กำหนดผู้จำหน่ายไม่จัดออเดอร์แผนก [แผนกจัดสินค้าเครดิตจัดบิล].");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                default:
                    break;
            }
        }

        private void RadTreeView_MNOR_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "reciveOrderMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_FindData("0"), "รับออเดอร์ของสดมินิมาร์ท [MNOR].");
                    break;
                case "LoadOrderMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_LoadOrder(), "ดึงออเดอร์จัด [MNOR].");
                    break;
                case "MNPR":
                    if ((SystemClass.SystemComMinimart == "1") || (Controllers.SystemClass.SystemDptID == "D034"))
                        FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_Report("0"), "สรุปใบสั่งซื้อ D034 จัดซื้อ-เหล้าบุหรี่.");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabScale_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "ScaleSUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.Scale.ScaleMain("SUPC", "SUPC"), "กำหนดสินค้าชั่ง สาขาใหญ่."); break;
                default:
                    break;
            }
        }
        //แผนกอะไหล่


        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                case "Branch":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                default:
                    break;
            }
        }
        //งานทั่วไป
        private void RadTreeView_GenaralWork_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "BillV014483":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D014"))
                        FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_ExcelToMNPR("V014483"), "ทำบิลใบสั่งซื้อ V014483 บริษัท นวสร ดิสทรีบิวชั่น จำกัด [Import Form Excel].");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "FarmHousePB":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D014"))
                        FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_PBPD("PB", "0", "V014483"), "ทำใบวางบิล [PB]  V014483 บริษัท นวสร ดิสทรีบิวชั่น จำกัด.");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "labelPromoSupc":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_AddImg("SUPC", "1"), "ป้ายโปรโมชั่น [สาขาใหญ่]."); break;

                default:
                    break;
            }
        }
        //การจัดสินค้า
        private void RadTreeView_OrderPicking_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderPicking":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("5", "SUPC", "", "", ""), "การจัดสินค้าตามแผนก [อ้างอิงการสั่ง]."); break;
                case "OrderPickingNoOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("6", "SUPC", "", "", ""), "การจัดสินค้าตามแผนก [ไม่อ้างอิงการสั่ง]."); break;
                default:
                    break;
            }
        }
        //รับสินค้าเข้าคลัง
        private void RadTreeView_WH_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ReciveWH":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("0", "0", "1"), "รับสินค้าเข้าคลัง."); break;
                //case "DROID_D177":
                //    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("1", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                //case "DROID_D177_Recheck":
                //    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("2", "0", "0"), "ตรวจสอบ จำนวนการส่งโรงงาน กับ จำนวนการรับเข้าคลัง [เฉพาะสินค้าโรงงาน]."); break;
                case "ReciveD015":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D015"))
                        FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("0", "1", "1"), "รับสินค้าเข้าคลัง [เฉพาะโรงหมี่].");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะแผนก D015-ฝ่ายผลิต-โรงหมี่ เท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "notApv":
                    FormClass.ShowCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("7", "0", "0"), "รายงานสินค้าที่รับเข้าคลังแต่ไม่ลงรายการบัญชี"); break;
                //case "DROID_D177NEW":
                //    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "1", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                //case "FacAll":
                //    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("5", "1", "0"), "จำนวน ผลิต/ส่ง/รับ[สินค้าโรงงาน]."); break;
                case "ReportDiffBA":
                    FormClass.ShowCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("8", "0", "0"), "รายงานสรุปวันรับสินค้าและวันรับเข้าคลัง"); break;
                default:
                    break;
            }
        }

        private void RadTreeView_FactotyItem_SelectedNodeChanged(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ITEMGroupSet":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOI.ItemsGroupVEND(), "การตั้งค่ากลุ่มสินค้าโรงงาน."); break;
                case "ITEMBarcodeSet":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOI.ItemsGroupBarcodeORDER("01"), "การตั้งค่าสินค้าตามกลุ่มสินค้าโรงงาน."); break;
                case "OrderMNOI":
                    FormClass.ShowCheckForm(new GeneralForm.MNOI.MNOI_ORDER(), "สั่งสินค้าผลิตสินค้าโรงงาน."); break;
                case "LoadOrderMNOI":
                    FormClass.ShowCheckForm(new GeneralForm.MNOI.MNOI_LoadOrder(), "ข้อมูลการสั่งสินค้าโรงงาน [MNOI]."); break;
                case "DROID_D177NEW":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "1", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                case "FacAll":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("5", "1", "0"), "จำนวน ผลิต/ส่ง/รับ[สินค้าโรงงาน]."); break;
                case "DROID_D177":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("1", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                case "DROID_D177_Recheck":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("2", "0", "0"), "ตรวจสอบ จำนวนการส่งโรงงาน กับ จำนวนการรับเข้าคลัง [เฉพาะสินค้าโรงงาน]."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_Scan_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Scan130":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                default:
                    break;
            }
        }
    }
}
