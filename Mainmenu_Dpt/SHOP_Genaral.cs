﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.SendMoney;
using PC_Shop24Hrs.GeneralForm.Report;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class SHOP_Genaral : Form
    {
        public SHOP_Genaral()
        {
            InitializeComponent();
        }

        private void SHOP_Genaral_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "1", ""), "รายละเอียดสินค้า."); break;

                default:
                    break;
            }
        }


        //ขอเบิกสินค้า
        private void RadTreeView_TabMNRO_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SmartRO_ORDER":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRO_ORDER(), "ขอเบิกสินค้า [MNRO]"); break;
                case "MNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("0", "MNRS_MN", "0"), "บิลเบิกสินค้า [MNRS]"); break;
                case "MNRR":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("0", "MNRR", "0"), "เบิกของในมินิมาร์ท [MNRR]"); break;
                default:
                    break;
            }
        }

        //Report
        private void RadTreeView_Report_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

            switch (e.Node.Name)
            {
                case "MNHS":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNHS.MNHS_Report("0", "SHOP"), "รายงานการซื้อสินค้าระบบบิลเครดิต[MNHS]."); break;
                case "Bill_Techout":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Bill_Techout("SHOP", "0"), "รายงานช่างนอกเข้าสาขา."); break;
                case "ReportBillU":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("0", "SHOP"), "รายงานบิลเปลี่ยน."); break;
                case "FrontVDO":
                    FormClass.ShowNotCheckForm(new Report_Recheck("1", "0"), "รายงานการตรวจหน้าร้าน [VDO]."); break;
                case "ReportMNPMVDO":
                    FormClass.ShowNotCheckForm(new Report_Recheck("0", "0"), "รายงานการส่งซองเงิน [VDO]."); break;
                case "ReportSlipLOImage":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImage("0"), "รายงานรูปปิดท้ายและกุญแจรถ 1 ประตู."); break;
                case "ReportSlipLOImageCar":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImageForCar("0", ""), "รายงานรูปปิดท้ายและกุญแจรถ > 1 ประตู."); break;

                case "Report_ARAtBranch_Lak":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash_Report("LackOfMoney", "0"), "รายงานเงินขาดตามแคชเชียร์."); break;
                case "Report_ARAtBranch_Deduct":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash_Report("DeductedMoney", "0"), "รายงานยอดหักตามแคชเชียร์."); break;
                case "MNBC":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("2"), "รายงานการรับซื้อสินค้าลูกค้า [MNBC]."); break;
                case "Rpt_Free":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_Report("0", "0"), "รายงานรายการสินค้าแถมมินิมาร์ท."); break;
                case "Pro24":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_Report("1", "0"), "รายงานการเปิด-ปิด โปรโมชั่น ผ่านระบบ SHOP24HRS."); break;

                case "Report_Customers":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Report_Customers("2"), "รายงานจำนวนการสมัครสมาชิก"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("8", "0"), "รายงานจำนวนการสมัครสมาชิก"); break;
                case "Report_CustomersDetail":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Report_Customers("1"), "รายงานการสมัครสมาชิก [รายละเอียด]"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("7", "0"), "รายงานการสมัครสมาชิก [รายละเอียด]"); break;
                case "Report_ItemsGroupBarcode":
                    //FormClass.ShowNotCheckForm(new FormShare.ItemGroup.Report_ItemsGroupBarcode("02", "D038"), "รายละเอียดสินค้าแลกแต้ม."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("4"), "รายงานรายละเอียดสินค้าแลกแต้ม"); break;
                case "MNFR_Detail":
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("4", "0"), "รายงานรายละเอียดการแลกแต้มของลูกค้ามินิมาร์ท."); break;
                case "MNFR_SUM":
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("5", "0"), "รายงานจำนวนแต้มที่แลกไปทั้งหมดของมินิมาร์ท."); break;
                case "MNOR_SUM":
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("6", "0"), "รายงานจำนวนรับออเดอร์ลูกค้าของมินิมาร์ท."); break;
                case "MNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRS"), "เบิกสินค้าส่งสาขา [MNRS]."); break;
                case "MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNIO"), "เบิกสินค้าไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "MNOT":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNOT"), "เบิกเครื่องมือ [MNOT]."); break;
                case "I-F":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("IMN"), "เบิกสินค้า MN [I-F]."); break;
                case "FAL":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("FAL"), "เบิกสินค้า [FAL]."); break;
                case "MNRR":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRR"), "เบิกสินค้าที่มินิมาร์ท [MNRR]."); break;
                case "EMPLOYEE_ListInCorrect":
                    //FormClass.ShowNotCheckForm(new FormShare.Report_EMP_ListInCorrect("D062", ""), "สรุปรายการหัก."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("5", "D062"), "สรุปรายการหัก [แผนกเบิกสินค้าภายใน]."); break;
                case "EMPLOYEE_ListInCorrect_tag":
                //FormClass.ShowNotCheckForm(new FormShare.Report_EMP_ListInCorrect("D179", "Tag"), "สรุปรายการหัก Tag."); break;
                case "EMPLOYEE_ListInCorrect_com":
                    //FormClass.ShowNotCheckForm(new FormShare.Report_EMP_ListInCorrect("D179", "COM-MN"), "สรุปรายการหักอุปกรณ์ต่างๆ."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("5", "D179"), "สรุปรายการหัก [แผนกคอมพิวเตอร์]."); break;
                case "Detail_MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("Detail_MNIO", "0"), "รายละเอียดเบิกไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "Detail_MNOT":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("Detail_MNOT", "0"), "รายละเอียดเบิกเครื่องมือ [MNOT]."); break;
                case "MNRM":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRM"), "โอนย้ายระหว่างสาขา [MNRM]."); break;
                default:
                    break;
            }
        }
        //Image
        private void RadTreeView_TabImage_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ImageAllDays_Date":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SHOP", "0", "0"), "รูปถ่ายประจำวัน ระบุวันที่ ทุกสาขา."); break;
                case "ImageAllDays_M":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SHOP", "1", "0"), "รูปถ่ายประจำวัน ระบุสาขา ตามช่วงวันที่."); break;
                case "ImageAllDays_C":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SHOP", "2", "0"), "รูปถ่ายประจำวัน จำนวนรูป ช่วงวันที่ ทุกสาขา."); break;
                case "ImageAllDays_ByGrp":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SHOP", "3", "0"), "รูปถ่ายประจำวัน ตามช่วงวันที่ เรียงตามกลุ่มการถ่าย."); break;
                case "ReportText_13":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("13"), "รายงานรูปถ่ายจัดสินค้า [สรุป]."); break;
                case "ReportText_14":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("14"), "รายงานรูปถ่ายจัดสินค้า [ละเอียด]."); break;
                case "EmplSH":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("2", "SHOP"), "รายงานพนักงานจัดสินค้าตามชั้นวาง."); break;
                case "EmpSorting":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("3", "SHOP"), "รายงานการจัดเรียงสินค้าตามพนักงาน."); break;
                case "TeamAB":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("4", "SHOP"), "รายงานทีมจัดร้านเข้าสาขา."); break;
                default:
                    break;
            }
        }
        //Genaral
        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_ARCashReason":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash("0"), "ยอดส่งเงิน."); break;

                case "promotion":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_Data("0"), "รายละเอียดข้อมูลโปรโมชั่น"); break;
                //case "Report_ARCashReason":
                //    FormClass.ShowCheckForm(new D153.ARMoneyCash("BRANCH"), "ยอดเงินขาด-เกิน"); break;
                case "labelPromo":
                    Controllers.FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_AddImg("SHOP", "1"), "ป้ายโปรโมชั่น."); break;
                case "MNEC_MN":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("23"), "แลกเหรียญ [MNEC]."); break;
                default:
                    break;
            }
        }


        private void RadTreeView_Cst_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CstDetail":
                    FormClass.ShowCheckForm(new GeneralForm.Customers.Customer_Detail("SHOP"), "รายละเอียดลูกค้า"); break;
                case "AddCustType0":
                    //FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomer("SHOP"), "สมัครสมาชิก ลูกค้าบุคคล."); break;
                    FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomerPerson("SHOP", "CRET_MN"), "สมัครสมาชิก ลูกค้าบุคคล."); break;
                case "AddCustType1":
                    FormClass.ShowNotCheckFormNormal(new GeneralForm.Customers.AddCustomerOther("Company", "SHOP"), "สมัครสมาชิก ลูกค้าองค์กร."); break;
                case "AddCustType2":
                    FormClass.ShowNotCheckFormNormal(new GeneralForm.Customers.AddCustomerOther("", "SHOP"), "สมัครสมาชิก ลูกค้าต่างชาติ."); break;
                case "AddCustYa":
                    if ((SystemClass.SystemBranchID == "MN080") || (SystemClass.SystemBranchID == "MN098"))
                        FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomerPerson("SHOP", "CPHA"), "สมัครสมาชิก ลูกค้าร้านยา [บัตรเหลือง].");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"สำหรับสาขา {SystemClass.SystemBranchID} {SystemClass.SystemBranchName} ยังไม่ได้เปิดให้ใช้งาน.");
                    break;
                default:
                    break;
            }
        }
        //คู่มือและการแก้ไขปัญหา
        private void RadTreeView_Doc_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

            switch (e.Node.Name)
            {
                case "Doc":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00016", "1"), "คู่มือและการแก้ไขปัญหา [COM-MM]."); break;
                case "DocumentCenter":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00017", "1"), "คู่มือและการแก้ไขปัญหา [CenterShop]."); break;
                case "DocumentFormCenter":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00018", "1"), "แบบฟอร์มต่างๆ [CenterShop]."); break;
                case "DocumentProduct":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00019", "1"), "แบบฟอร์มต่างๆ [Product Planning]."); break;
                case "DocumentEmplData":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00020", "1"), "แบบฟอร์มต่างๆ [ดูแลข้อมูลพนักงาน]."); break;
                case "DocumentRule":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00021", "1"), "แบบฟอร์มต่างๆ [ดูแลกฎระเบียบ]."); break;
                default:
                    break;
            }
        }
    }
}
