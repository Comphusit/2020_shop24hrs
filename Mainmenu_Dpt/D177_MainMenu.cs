﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D177_MainMenu : Form
    {
        public D177_MainMenu()
        {
            InitializeComponent();
        }

        private void D177_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "BCH":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                default:
                    break;
            }
        }
        //เบิกกระดาษ
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D038_SmartRO_LoadOrder":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRO_LoadOrder(), "ดึงข้อมูลจัดสินค้าส่งมินิมาร์ท."); break;
                case "D038_SmartRO_Main":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SHOP", ""), "เบิกสินค้าส่งมินิมาร์ท."); break;
                case "reciveOrderMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_FindData("0"), "รับออเดอร์ของสดมินิมาร์ท [MNOR].");
                    break;
                case "MNOR":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_LoadOrder(), "ดึงออเดอร์จัด [MNOR].");
                    break;
                default:
                    break;
            }
        }

        //รายงาน
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ReportScan130":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกโปรโมชั่น]."); break;
                case "TagReport":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImageForCar("1", "027"), "รายงานรูปปิดท้ายรถ โรงงานป่าคลอก."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Upload_SPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_UpFile(PathImageClass.pPathCheckPriceSUPC), "Upload รูป แสดงผลเครื่องเช็คราคา สาขาใหญ่."); break;

                case "Upload_MN":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_UpFile(PathImageClass.pPathCheckPriceMN), "Upload รูป แสดงผลเครื่องเช็คราคา มินิมาร์ท."); break;
                case "configJOBFactory":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("52", "ตั้งค่าช่างซ่อมเครื่องจักรโรงงาน", "1", "1", "1", "0", "0"), "ตั้งค่าช่างซ่อมเครื่องจักรโรงงาน."); break;
                default:
                    break;
            }
        }

        //ระบบโปรโมชัน
        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "promotion":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_Data("1"), "รายละเอียดข้อมูลโปรโมชั่น [ของแถม]."); break;
                case "Img_SUPC":
                    FormClass.ShowNotCheckForm(
                        new FormShare.ShowImage.ShowImage { pathImg = PathImageClass.pPathCheckPriceSUPC + "|*.jpg" }, "รูปที่แสดงในเครื่องเช็คราคาสาขาใหญ่ทั้งหมด."); break;
                case "Img_MN":
                    FormClass.ShowNotCheckForm(
                        new FormShare.ShowImage.ShowImage { pathImg = PathImageClass.pPathCheckPriceMN + "|*.jpg" }, "รูปที่แสดงในเครื่องเช็คราคามินิมาร์ททั้งหมด."); break;
                case "labelPromo":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_AddImg("SHOP", "0"), "ป้ายโปรโมชั่น [มินิมารืท]."); break;
                case "labelPromoSupc":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_AddImg("SUPC", "0"), "ป้ายโปรโมชั่น [สาขาใหญ่]."); break;
                default:
                    break;
            }
        }

        //งานซ่อมโรงงาน
        private void RadTreeView4_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "JOBFACTORY":
                    FormClass.ShowNotCheckForm(new JOB.Factory.JOBFactory_Main("SUPC", "00023", "งานซ่อมเครื่องจักรโรงงาน.", "1", ""), "แจ้งซ่อมเครื่องจักรของโรงงาน"); break;
                case "StockFactoy":
                    FormClass.ShowNotCheckForm(new JOB.Stock.Stock_Item("00516"), "สต็อกอะไหล่."); break;
                default:
                    break;
            }
        }

        //งานทั่วไป
        private void RadTreeView3_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Scan130":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                case "ReciveWH":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("0", "0", "1"), "รับสินค้าเข้าคลัง."); break;
                case "DROID_D177":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                case "DROID_D177_Recheck":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("2", "0", "0"), "ตรวจสอบ จำนวนการส่งโรงงาน กับ จำนวนการรับเข้าคลัง [เฉพาะสินค้าโรงงาน]."); break;
                case "FacAll":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("5", "1", "0"), "จำนวน ผลิต/ส่ง/รับ[สินค้าโรงงาน]."); break;

                default:
                    break;
            }
        }
    }
}
