﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Logistic;
using PC_Shop24Hrs.GeneralForm.MNEC;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.Report;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D153_MainMenu : Form
    {
        public D153_MainMenu()
        {
            InitializeComponent();
        }

        private void D153_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ARCashReason":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash("0"), "ยอดส่งเงิน ขาด-เกิน."); break;
                //ARSendMoney
                case "ARSendMoney":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARSendMoney(), "ส่งเงินสด"); break;
                case "CoinsExchange":
                    FormClass.ShowNotCheckForm(new MNEC_Main("22"), "แลกเหรียญสาขา."); break;
                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SendMoney_Rpt":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_ReportSum("1"), "รายงานซองส่งเงินค้างส่ง."); break;
                case "ReportStaPack":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("5", "1"), "รายงานสถานะการรับซองส่งเงิน."); break;
                case "ReportMNSM":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("6", "0"), "รายงานซองส่งเงิน พนักงานบัญชีลูกหนี้."); break;
                case "ReportSendMNPMDetail":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("7", "1"), "รายงานรายละเอียดการส่งซองเงิน."); break;
                case "Report_ARByMonth":
                    //FormClass.ShowNotCheckForm(new D153.Report_ARByMonth(), "รายงานยอดส่งเงินขาด-เกิน."); break; 
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash_Report("SummaryMoney", "0"), "รายงานยอดส่งเงินขาด-เกิน."); break;
                case "Report_ARMoneyCash":
                    //FormClass.ShowNotCheckForm(new D153.Report_ARCashReason(), "รายงาน ยอดส่งเงิน."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash("1"), "รายงานยอดส่งเงิน."); break;
                case "MNBC":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("2"), "รายงานการซื้อสินค้าของลูกค้ามินิมาร์ท"); break;
                case "MP":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.MP.MP_Main("SUPC", "0"), "รายละเอียดการรับสินค้าเซลล์ส่ง [MP]."); break;
                case "MNECBag":
                    FormClass.ShowNotCheckForm(new MNEC_Main("25"), "ซองเงินแลกเหรียญ."); break;
                case "MNECSUM":
                    FormClass.ShowNotCheckForm(new MNEC_Main("26"), "สรุปเงินระบบแลกเหรียญ."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

            switch (e.Node.Name)
            {
                case "DroidD":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D051") || (SystemClass.SystemDptID == "D151"))
                        FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("0", "1", "1"), "รับสินค้าเข้าคลัง.");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะแผนก D051-แผนกบัญชีลูกหนี้ (กมลศรี) และ D151-แผนกบัญชีลูกหนี้สาย B (เรวดี) เท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "notApv":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D051") || (SystemClass.SystemDptID == "D151"))
                        FormClass.ShowCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("7", "1", "0"), "รายงานสินค้าที่รับเข้าคลังแต่ไม่ลงรายการบัญชี");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะแผนก D051-แผนกบัญชีลูกหนี้ (กมลศรี) และ D151-แผนกบัญชีลูกหนี้สาย B (เรวดี) เท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                default:
                    break;
            }
        }
    }
}
