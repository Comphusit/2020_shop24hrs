﻿namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    partial class D168_MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode8 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode9 = new Telerik.WinControls.UI.RadTreeNode();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel3 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabConfig = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel4 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabJobGenaral = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabGenaral = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel5 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabReport = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel_Dept = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_Dpt = new Telerik.WinControls.UI.RadLabel();
            this.object_843f177e_c01b_4318_8746_cb64b97fb147 = new Telerik.WinControls.RootRadElement();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).BeginInit();
            this.radCollapsiblePanel3.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).BeginInit();
            this.radCollapsiblePanel4.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabJobGenaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).BeginInit();
            this.radCollapsiblePanel5.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).BeginInit();
            this.radPanel_Dept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(921, 580);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.radCollapsiblePanel3);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel3.Location = new System.Drawing.Point(325, 3);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(316, 574);
            this.radPanel3.TabIndex = 4;
            // 
            // radCollapsiblePanel3
            // 
            this.radCollapsiblePanel3.AnimationFrames = 10;
            this.radCollapsiblePanel3.AnimationInterval = 10;
            this.radCollapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel3.HeaderText = "ตั้งค่าระบบ";
            this.radCollapsiblePanel3.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel3.Name = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 207);
            // 
            // radCollapsiblePanel3.PanelContainer
            // 
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.radTreeView_TabConfig);
            this.radCollapsiblePanel3.PanelContainer.Size = new System.Drawing.Size(314, 177);
            this.radCollapsiblePanel3.Size = new System.Drawing.Size(316, 207);
            this.radCollapsiblePanel3.TabIndex = 0;
            this.radCollapsiblePanel3.Visible = false;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabConfig
            // 
            this.radTreeView_TabConfig.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabConfig.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabConfig.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabConfig.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabConfig.ItemHeight = 28;
            this.radTreeView_TabConfig.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabConfig.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabConfig.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabConfig.Name = "radTreeView_TabConfig";
            this.radTreeView_TabConfig.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabConfig.ShowLines = true;
            this.radTreeView_TabConfig.Size = new System.Drawing.Size(314, 177);
            this.radTreeView_TabConfig.SpacingBetweenNodes = -5;
            this.radTreeView_TabConfig.TabIndex = 2;
            this.radTreeView_TabConfig.ThemeName = "Fluent";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radCollapsiblePanel4);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(316, 574);
            this.radPanel1.TabIndex = 2;
            this.radPanel1.ThemeName = "Fluent";
            // 
            // radCollapsiblePanel4
            // 
            this.radCollapsiblePanel4.AnimationFrames = 10;
            this.radCollapsiblePanel4.AnimationInterval = 10;
            this.radCollapsiblePanel4.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel4.HeaderText = "งานทั่วไป";
            this.radCollapsiblePanel4.Location = new System.Drawing.Point(0, 89);
            this.radCollapsiblePanel4.Name = "radCollapsiblePanel4";
            this.radCollapsiblePanel4.OwnerBoundsCache = new System.Drawing.Rectangle(0, 138, 316, 231);
            // 
            // radCollapsiblePanel4.PanelContainer
            // 
            this.radCollapsiblePanel4.PanelContainer.Controls.Add(this.radTreeView_TabJobGenaral);
            this.radCollapsiblePanel4.PanelContainer.Size = new System.Drawing.Size(314, 455);
            this.radCollapsiblePanel4.Size = new System.Drawing.Size(316, 485);
            this.radCollapsiblePanel4.TabIndex = 2;
            this.radCollapsiblePanel4.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabJobGenaral
            // 
            this.radTreeView_TabJobGenaral.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabJobGenaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabJobGenaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabJobGenaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabJobGenaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabJobGenaral.ItemHeight = 28;
            this.radTreeView_TabJobGenaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabJobGenaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabJobGenaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabJobGenaral.Name = "radTreeView_TabJobGenaral";
            radTreeNode1.Expanded = true;
            radTreeNode1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode1.Name = "MNPO_SUPC";
            radTreeNode1.Text = "สั่งสินค้าให้สาขา";
            radTreeNode2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode2.Name = "OrderPicking";
            radTreeNode2.Text = "การจัดสินค้าตามแผนก";
            this.radTreeView_TabJobGenaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1,
            radTreeNode2});
            this.radTreeView_TabJobGenaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabJobGenaral.ShowLines = true;
            this.radTreeView_TabJobGenaral.Size = new System.Drawing.Size(314, 455);
            this.radTreeView_TabJobGenaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabJobGenaral.TabIndex = 2;
            this.radTreeView_TabJobGenaral.ThemeName = "Fluent";
            this.radTreeView_TabJobGenaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabJobGenaral_NodeMouseClick);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationFrames = 10;
            this.radCollapsiblePanel1.AnimationInterval = 10;
            this.radCollapsiblePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel1.HeaderText = "ข้อมูลทั่วไป";
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 138);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radTreeView_TabGenaral);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(314, 59);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(316, 89);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabGenaral
            // 
            this.radTreeView_TabGenaral.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabGenaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabGenaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabGenaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabGenaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabGenaral.ItemHeight = 28;
            this.radTreeView_TabGenaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabGenaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabGenaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabGenaral.Name = "radTreeView_TabGenaral";
            radTreeNode3.Expanded = true;
            radTreeNode3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode3.Name = "PhoneBook";
            radTreeNode3.Text = "เบอร์โทรภายใน";
            radTreeNode4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode4.Name = "ItembarcodeDetail";
            radTreeNode4.Text = "รายละเอียดสินค้า";
            this.radTreeView_TabGenaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode3,
            radTreeNode4});
            this.radTreeView_TabGenaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabGenaral.ShowLines = true;
            this.radTreeView_TabGenaral.Size = new System.Drawing.Size(314, 59);
            this.radTreeView_TabGenaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabGenaral.TabIndex = 3;
            this.radTreeView_TabGenaral.ThemeName = "Fluent";
            this.radTreeView_TabGenaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabGenaral_NodeMouseClick);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radCollapsiblePanel5);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel2.Location = new System.Drawing.Point(647, 3);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(271, 574);
            this.radPanel2.TabIndex = 3;
            // 
            // radCollapsiblePanel5
            // 
            this.radCollapsiblePanel5.AnimationFrames = 10;
            this.radCollapsiblePanel5.AnimationInterval = 10;
            this.radCollapsiblePanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel5.HeaderText = "รายงาน";
            this.radCollapsiblePanel5.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel5.Name = "radCollapsiblePanel5";
            this.radCollapsiblePanel5.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 271, 309);
            // 
            // radCollapsiblePanel5.PanelContainer
            // 
            this.radCollapsiblePanel5.PanelContainer.Controls.Add(this.radTreeView_TabReport);
            this.radCollapsiblePanel5.PanelContainer.Size = new System.Drawing.Size(269, 544);
            this.radCollapsiblePanel5.Size = new System.Drawing.Size(271, 574);
            this.radCollapsiblePanel5.TabIndex = 1;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabReport
            // 
            this.radTreeView_TabReport.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView_TabReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabReport.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabReport.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabReport.ItemHeight = 28;
            this.radTreeView_TabReport.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_TabReport.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabReport.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabReport.Name = "radTreeView_TabReport";
            radTreeNode5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode5.Name = "SUPC_MNPO_CheckOrder";
            radTreeNode5.Text = "เช็คออเดอร์";
            radTreeNode6.Expanded = true;
            radTreeNode6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode6.Name = "OrderFromPicture";
            radTreeNode7.Expanded = true;
            radTreeNode7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode7.Name = "StatusAllOrder";
            radTreeNode7.Text = "สถานะออร์เดอร์รวม";
            radTreeNode8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode8.Name = "StatusOrderDMS";
            radTreeNode8.Text = "สถานะออร์เดอร์ตามแผนก";
            radTreeNode9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode9.Name = "StatusOrderMN";
            radTreeNode9.Text = "สถานะออร์เดอร์ตามมินิมาร์ท";
            radTreeNode6.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode7,
            radTreeNode8,
            radTreeNode9});
            radTreeNode6.Text = "สินค้าที่สั่งผ่านรูป ";
            this.radTreeView_TabReport.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode5,
            radTreeNode6});
            this.radTreeView_TabReport.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabReport.ShowLines = true;
            this.radTreeView_TabReport.Size = new System.Drawing.Size(269, 544);
            this.radTreeView_TabReport.SpacingBetweenNodes = -5;
            this.radTreeView_TabReport.TabIndex = 0;
            this.radTreeView_TabReport.ThemeName = "Fluent";
            this.radTreeView_TabReport.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabReport_NodeMouseClick);
            // 
            // radPanel_Dept
            // 
            this.radPanel_Dept.Controls.Add(this.radLabel_Dpt);
            this.radPanel_Dept.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel_Dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel_Dept.ForeColor = System.Drawing.Color.Purple;
            this.radPanel_Dept.Location = new System.Drawing.Point(0, 0);
            this.radPanel_Dept.Name = "radPanel_Dept";
            this.radPanel_Dept.Size = new System.Drawing.Size(921, 29);
            this.radPanel_Dept.TabIndex = 3;
            this.radPanel_Dept.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).AutoSize = true;
            // 
            // radLabel_Dpt
            // 
            this.radLabel_Dpt.AutoSize = false;
            this.radLabel_Dpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            this.radLabel_Dpt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Dpt.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Dpt.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Dpt.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Dpt.Name = "radLabel_Dpt";
            this.radLabel_Dpt.Size = new System.Drawing.Size(921, 29);
            this.radLabel_Dpt.TabIndex = 19;
            this.radLabel_Dpt.Text = "User";
            this.radLabel_Dpt.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // object_843f177e_c01b_4318_8746_cb64b97fb147
            // 
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.Name = "object_843f177e_c01b_4318_8746_cb64b97fb147";
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchHorizontally = true;
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchVertically = true;
            // 
            // radTreeView1
            // 
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.Size = new System.Drawing.Size(241, 172);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 0;
            // 
            // D168_MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 608);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.radPanel_Dept);
            this.Name = "D168_MainMenu";
            this.ShowIcon = false;
            this.Text = "D168_แผนกจัดสินค้าเครดิต";
            this.Load += new System.EventHandler(this.D168_MainMenu_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radCollapsiblePanel3.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radCollapsiblePanel4.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabJobGenaral)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radCollapsiblePanel5.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).EndInit();
            this.radPanel_Dept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel2;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabReport;
        protected Telerik.WinControls.UI.RadPanel radPanel_Dept;
        private Telerik.WinControls.RootRadElement object_843f177e_c01b_4318_8746_cb64b97fb147;
        private Telerik.WinControls.UI.RadLabel radLabel_Dpt;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel4;
        protected Telerik.WinControls.UI.RadPanel radPanel3;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel3;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabJobGenaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabGenaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabConfig;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel5;
    }
}