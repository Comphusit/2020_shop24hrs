﻿namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    partial class D038_MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode8 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode9 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode10 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode11 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode12 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode13 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode14 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode15 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode16 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode17 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode18 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode19 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode20 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode21 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode22 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode23 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode24 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode25 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode26 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode27 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode28 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode29 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode30 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode31 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode32 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode33 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode34 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode35 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode36 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode37 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode38 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode39 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode40 = new Telerik.WinControls.UI.RadTreeNode();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel3 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabConfig = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel7 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView2 = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel2 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_Cust = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel6 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabGernaral = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel4 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabJobGenaral = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabGenaral = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel5 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabReport = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel_Dept = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_Dpt = new Telerik.WinControls.UI.RadLabel();
            this.object_843f177e_c01b_4318_8746_cb64b97fb147 = new Telerik.WinControls.RootRadElement();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).BeginInit();
            this.radCollapsiblePanel3.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel7)).BeginInit();
            this.radCollapsiblePanel7.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).BeginInit();
            this.radCollapsiblePanel2.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel6)).BeginInit();
            this.radCollapsiblePanel6.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGernaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).BeginInit();
            this.radCollapsiblePanel4.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabJobGenaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).BeginInit();
            this.radCollapsiblePanel5.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).BeginInit();
            this.radPanel_Dept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(921, 580);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.radCollapsiblePanel3);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel3.Location = new System.Drawing.Point(325, 3);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(316, 574);
            this.radPanel3.TabIndex = 4;
            // 
            // radCollapsiblePanel3
            // 
            this.radCollapsiblePanel3.AnimationFrames = 10;
            this.radCollapsiblePanel3.AnimationInterval = 10;
            this.radCollapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel3.HeaderText = "ตั้งค่าระบบ";
            this.radCollapsiblePanel3.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel3.Name = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 207);
            // 
            // radCollapsiblePanel3.PanelContainer
            // 
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.radTreeView_TabConfig);
            this.radCollapsiblePanel3.PanelContainer.Size = new System.Drawing.Size(314, 544);
            this.radCollapsiblePanel3.Size = new System.Drawing.Size(316, 574);
            this.radCollapsiblePanel3.TabIndex = 0;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabConfig
            // 
            this.radTreeView_TabConfig.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabConfig.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabConfig.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabConfig.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabConfig.ItemHeight = 28;
            this.radTreeView_TabConfig.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabConfig.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabConfig.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabConfig.Name = "radTreeView_TabConfig";
            radTreeNode1.Expanded = true;
            radTreeNode1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode1.Name = "Node1";
            radTreeNode2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode2.Name = "Upload_SPC";
            radTreeNode2.Text = "สาขาใหญ่";
            radTreeNode3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode3.Name = "Upload_MN";
            radTreeNode3.Text = "มินิมาร์ท";
            radTreeNode1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode2,
            radTreeNode3});
            radTreeNode1.Text = "Upload รูป เครื่องเช็คราคา";
            radTreeNode4.Expanded = true;
            radTreeNode4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode4.Name = "Node9";
            radTreeNode5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode5.Name = "Fix_Group";
            radTreeNode5.Text = "กลุ่ม";
            radTreeNode6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode6.Name = "Fix_Item";
            radTreeNode6.Text = "สินค้า";
            radTreeNode4.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode5,
            radTreeNode6});
            radTreeNode4.Text = "กำหนดแลกแต้ม";
            radTreeNode7.Expanded = true;
            radTreeNode7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode7.Name = "Node6";
            radTreeNode8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode8.Name = "GrpOrder";
            radTreeNode8.Text = "กลุ่ม";
            radTreeNode9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode9.Name = "ItemOrder";
            radTreeNode9.Text = "สินค้า";
            radTreeNode7.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode8,
            radTreeNode9});
            radTreeNode7.Text = "กำหนดสั่ง MNOR";
            this.radTreeView_TabConfig.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1,
            radTreeNode4,
            radTreeNode7});
            this.radTreeView_TabConfig.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabConfig.ShowLines = true;
            this.radTreeView_TabConfig.Size = new System.Drawing.Size(314, 544);
            this.radTreeView_TabConfig.SpacingBetweenNodes = -5;
            this.radTreeView_TabConfig.TabIndex = 2;
            this.radTreeView_TabConfig.ThemeName = "Fluent";
            this.radTreeView_TabConfig.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabConfig_NodeMouseClick);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radCollapsiblePanel7);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel2);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel6);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel4);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(316, 574);
            this.radPanel1.TabIndex = 2;
            this.radPanel1.ThemeName = "Fluent";
            // 
            // radCollapsiblePanel7
            // 
            this.radCollapsiblePanel7.AnimationFrames = 10;
            this.radCollapsiblePanel7.AnimationInterval = 10;
            this.radCollapsiblePanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel7.HeaderText = "ระบบโปรโมชั่น";
            this.radCollapsiblePanel7.Location = new System.Drawing.Point(0, 489);
            this.radCollapsiblePanel7.Name = "radCollapsiblePanel7";
            this.radCollapsiblePanel7.OwnerBoundsCache = new System.Drawing.Rectangle(0, 309, 271, 200);
            // 
            // radCollapsiblePanel7.PanelContainer
            // 
            this.radCollapsiblePanel7.PanelContainer.Controls.Add(this.radTreeView2);
            this.radCollapsiblePanel7.PanelContainer.Size = new System.Drawing.Size(314, 55);
            this.radCollapsiblePanel7.Size = new System.Drawing.Size(316, 85);
            this.radCollapsiblePanel7.TabIndex = 5;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel7.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel7.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel7.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView2
            // 
            this.radTreeView2.BackColor = System.Drawing.Color.White;
            this.radTreeView2.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView2.ForeColor = System.Drawing.Color.Black;
            this.radTreeView2.ItemHeight = 28;
            this.radTreeView2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView2.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView2.Location = new System.Drawing.Point(0, 0);
            this.radTreeView2.Name = "radTreeView2";
            radTreeNode10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode10.Name = "promotion";
            radTreeNode10.Text = "ข้อมูลโปรโมชั่น [ของแถม]";
            radTreeNode11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode11.Name = "Img_SUPC";
            radTreeNode11.Text = "รูป เครื่องเช็คราคา สาขาใหญ่";
            radTreeNode12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode12.Name = "Img_MN";
            radTreeNode12.Text = "รูป เครื่องเช็คราคา มินิมาร์ท";
            radTreeNode13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(17)))), ((int)(((byte)(173)))));
            radTreeNode13.Name = "labelPromo";
            radTreeNode13.Text = "ป้ายโปรโมชั่น [มินิมาร์ท]";
            radTreeNode14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode14.Name = "labelPromoSupc";
            radTreeNode14.Text = "ป้ายโปรโมชั่น [สาขาใหญ่]";
            this.radTreeView2.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode10,
            radTreeNode11,
            radTreeNode12,
            radTreeNode13,
            radTreeNode14});
            this.radTreeView2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView2.ShowLines = true;
            this.radTreeView2.Size = new System.Drawing.Size(314, 55);
            this.radTreeView2.SpacingBetweenNodes = -5;
            this.radTreeView2.TabIndex = 4;
            this.radTreeView2.ThemeName = "Fluent";
            this.radTreeView2.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView2_NodeMouseClick);
            // 
            // radCollapsiblePanel2
            // 
            this.radCollapsiblePanel2.AnimationFrames = 10;
            this.radCollapsiblePanel2.AnimationInterval = 10;
            this.radCollapsiblePanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel2.HeaderText = "ระบบสมาชิก";
            this.radCollapsiblePanel2.Location = new System.Drawing.Point(0, 359);
            this.radCollapsiblePanel2.Name = "radCollapsiblePanel2";
            this.radCollapsiblePanel2.OwnerBoundsCache = new System.Drawing.Rectangle(0, 309, 271, 200);
            // 
            // radCollapsiblePanel2.PanelContainer
            // 
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.radTreeView_Cust);
            this.radCollapsiblePanel2.PanelContainer.Size = new System.Drawing.Size(314, 100);
            this.radCollapsiblePanel2.Size = new System.Drawing.Size(316, 130);
            this.radCollapsiblePanel2.TabIndex = 4;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel2.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel2.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel2.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_Cust
            // 
            this.radTreeView_Cust.BackColor = System.Drawing.Color.White;
            this.radTreeView_Cust.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_Cust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_Cust.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_Cust.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_Cust.ItemHeight = 28;
            this.radTreeView_Cust.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_Cust.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_Cust.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_Cust.Name = "radTreeView_Cust";
            radTreeNode15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode15.Name = "CstMain";
            radTreeNode15.Text = "รายละเอียดลูกค้า";
            radTreeNode16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode16.Name = "AddCustType0";
            radTreeNode16.Text = "ลูกค้าบุคคล";
            radTreeNode17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode17.Name = "AddCustType1";
            radTreeNode17.Text = "ลูกค้าองค์กร";
            radTreeNode18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode18.Name = "AddCustType2";
            radTreeNode18.Text = "ลูกค้าต่างชาติ";
            this.radTreeView_Cust.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode15,
            radTreeNode16,
            radTreeNode17,
            radTreeNode18});
            this.radTreeView_Cust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_Cust.ShowLines = true;
            this.radTreeView_Cust.Size = new System.Drawing.Size(314, 100);
            this.radTreeView_Cust.SpacingBetweenNodes = -5;
            this.radTreeView_Cust.TabIndex = 3;
            this.radTreeView_Cust.ThemeName = "Fluent";
            this.radTreeView_Cust.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_Cust_NodeMouseClick);
            // 
            // radCollapsiblePanel6
            // 
            this.radCollapsiblePanel6.AnimationFrames = 10;
            this.radCollapsiblePanel6.AnimationInterval = 10;
            this.radCollapsiblePanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel6.HeaderText = "งานทั่วไป";
            this.radCollapsiblePanel6.Location = new System.Drawing.Point(0, 244);
            this.radCollapsiblePanel6.Name = "radCollapsiblePanel6";
            this.radCollapsiblePanel6.OwnerBoundsCache = new System.Drawing.Rectangle(0, 309, 271, 200);
            // 
            // radCollapsiblePanel6.PanelContainer
            // 
            this.radCollapsiblePanel6.PanelContainer.Controls.Add(this.radTreeView_TabGernaral);
            this.radCollapsiblePanel6.PanelContainer.Size = new System.Drawing.Size(314, 85);
            this.radCollapsiblePanel6.Size = new System.Drawing.Size(316, 115);
            this.radCollapsiblePanel6.TabIndex = 3;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel6.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel6.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel6.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabGernaral
            // 
            this.radTreeView_TabGernaral.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView_TabGernaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabGernaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabGernaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabGernaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabGernaral.ItemHeight = 28;
            this.radTreeView_TabGernaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_TabGernaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabGernaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabGernaral.Name = "radTreeView_TabGernaral";
            radTreeNode19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode19.Name = "MNPZ";
            radTreeNode19.Text = "ใบรับของรางวัล [PZ]";
            radTreeNode20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode20.Name = "Scan130";
            radTreeNode20.Text = "สแกนนิ้ว 1.30 ชม.";
            radTreeNode21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode21.Name = "Cust_Rent";
            radTreeNode21.Text = "รายชื่อผู้เช่าสำหรับจ่ายคูปอง Line@";
            this.radTreeView_TabGernaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode19,
            radTreeNode20,
            radTreeNode21});
            this.radTreeView_TabGernaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabGernaral.ShowLines = true;
            this.radTreeView_TabGernaral.Size = new System.Drawing.Size(314, 85);
            this.radTreeView_TabGernaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabGernaral.TabIndex = 2;
            this.radTreeView_TabGernaral.ThemeName = "Fluent";
            this.radTreeView_TabGernaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabGernaral_NodeMouseClick);
            // 
            // radCollapsiblePanel4
            // 
            this.radCollapsiblePanel4.AnimationFrames = 10;
            this.radCollapsiblePanel4.AnimationInterval = 10;
            this.radCollapsiblePanel4.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel4.HeaderText = "กระดาษและสินค้าแลกแต้มส่งมินิมาร์ท";
            this.radCollapsiblePanel4.Location = new System.Drawing.Point(0, 88);
            this.radCollapsiblePanel4.Name = "radCollapsiblePanel4";
            this.radCollapsiblePanel4.OwnerBoundsCache = new System.Drawing.Rectangle(0, 138, 316, 231);
            // 
            // radCollapsiblePanel4.PanelContainer
            // 
            this.radCollapsiblePanel4.PanelContainer.Controls.Add(this.radTreeView_TabJobGenaral);
            this.radCollapsiblePanel4.PanelContainer.Size = new System.Drawing.Size(314, 126);
            this.radCollapsiblePanel4.Size = new System.Drawing.Size(316, 156);
            this.radCollapsiblePanel4.TabIndex = 2;
            this.radCollapsiblePanel4.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabJobGenaral
            // 
            this.radTreeView_TabJobGenaral.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabJobGenaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabJobGenaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabJobGenaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabJobGenaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabJobGenaral.ItemHeight = 28;
            this.radTreeView_TabJobGenaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabJobGenaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabJobGenaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabJobGenaral.Name = "radTreeView_TabJobGenaral";
            radTreeNode22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode22.Name = "D038_SmartRO_LoadOrder";
            radTreeNode22.Text = "ดึงข้อมูลจัดสินค้า";
            radTreeNode23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode23.Name = "D038_SmartRO_Main";
            radTreeNode23.Text = "เบิกสินค้าส่งมินิมาร์ท";
            radTreeNode24.Expanded = true;
            radTreeNode24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode24.Name = "Node4";
            radTreeNode25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode25.Name = "reciveOrderMN";
            radTreeNode25.Text = "รับออเดอร์ [MNOR]";
            radTreeNode26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode26.Name = "MNOR";
            radTreeNode26.Text = "ดึงออเดอร์จัดสินค้า [MNOR]";
            radTreeNode24.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode25,
            radTreeNode26});
            radTreeNode24.Text = "สินค้าแลกแต้ม";
            this.radTreeView_TabJobGenaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode22,
            radTreeNode23,
            radTreeNode24});
            this.radTreeView_TabJobGenaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabJobGenaral.ShowLines = true;
            this.radTreeView_TabJobGenaral.Size = new System.Drawing.Size(314, 126);
            this.radTreeView_TabJobGenaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabJobGenaral.TabIndex = 2;
            this.radTreeView_TabJobGenaral.ThemeName = "Fluent";
            this.radTreeView_TabJobGenaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabJobGenaral_NodeMouseClick);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationFrames = 10;
            this.radCollapsiblePanel1.AnimationInterval = 10;
            this.radCollapsiblePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel1.HeaderText = "ข้อมูลทั่วไป";
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 138);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radTreeView_TabGenaral);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(314, 58);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(316, 88);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabGenaral
            // 
            this.radTreeView_TabGenaral.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabGenaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabGenaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabGenaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabGenaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabGenaral.ItemHeight = 28;
            this.radTreeView_TabGenaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabGenaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabGenaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabGenaral.Name = "radTreeView_TabGenaral";
            radTreeNode27.Expanded = true;
            radTreeNode27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode27.Name = "PhoneBook";
            radTreeNode27.Text = "เบอร์โทรภายใน";
            radTreeNode28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode28.Name = "BCH";
            radTreeNode28.Text = "ข้อมูลสาขา";
            this.radTreeView_TabGenaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode27,
            radTreeNode28});
            this.radTreeView_TabGenaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabGenaral.ShowLines = true;
            this.radTreeView_TabGenaral.Size = new System.Drawing.Size(314, 58);
            this.radTreeView_TabGenaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabGenaral.TabIndex = 3;
            this.radTreeView_TabGenaral.ThemeName = "Fluent";
            this.radTreeView_TabGenaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabGenaral_NodeMouseClick);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radCollapsiblePanel5);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel2.Location = new System.Drawing.Point(647, 3);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(271, 574);
            this.radPanel2.TabIndex = 3;
            // 
            // radCollapsiblePanel5
            // 
            this.radCollapsiblePanel5.AnimationFrames = 10;
            this.radCollapsiblePanel5.AnimationInterval = 10;
            this.radCollapsiblePanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel5.HeaderText = "รายงาน";
            this.radCollapsiblePanel5.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel5.Name = "radCollapsiblePanel5";
            this.radCollapsiblePanel5.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 271, 309);
            // 
            // radCollapsiblePanel5.PanelContainer
            // 
            this.radCollapsiblePanel5.PanelContainer.Controls.Add(this.radTreeView_TabReport);
            this.radCollapsiblePanel5.PanelContainer.Size = new System.Drawing.Size(269, 544);
            this.radCollapsiblePanel5.Size = new System.Drawing.Size(271, 574);
            this.radCollapsiblePanel5.TabIndex = 1;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabReport
            // 
            this.radTreeView_TabReport.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView_TabReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabReport.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabReport.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabReport.ItemHeight = 28;
            this.radTreeView_TabReport.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_TabReport.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabReport.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabReport.Name = "radTreeView_TabReport";
            radTreeNode29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode29.Name = "Rpt_MNPZ";
            radTreeNode29.Text = "นำของรางวัลออก [MNPZ]";
            radTreeNode30.Expanded = true;
            radTreeNode30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode30.Name = "Node3";
            radTreeNode31.Expanded = true;
            radTreeNode31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode31.Name = "D038_TranferItem_BranchRecive";
            radTreeNode31.Text = "ข้อมูลการรับข้าวกล่อง";
            radTreeNode32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode32.Name = "D038_TranferDelivary_Report";
            radTreeNode32.Text = "ข้อมูลการรับ-ส่งข้าวกล่อง";
            radTreeNode30.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode31,
            radTreeNode32});
            radTreeNode30.Text = "ครัวชญานันทน์";
            radTreeNode33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode33.Name = "Report_Customers";
            radTreeNode33.Text = "รายงานสมัครสมาชิก";
            radTreeNode34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode34.Name = "ReportScan130";
            radTreeNode34.Text = "สแกนนิ้ว 1.30 ชม.";
            radTreeNode35.Expanded = true;
            radTreeNode35.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode35.Name = "Node8";
            radTreeNode36.Expanded = true;
            radTreeNode36.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode36.Name = "Rpt_Free";
            radTreeNode36.Text = "รายการสินค้าแถมมินิมาร์ท";
            radTreeNode37.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode37.Name = "Pro24";
            radTreeNode37.Text = "เปิด-ปิด [ผ่านระบบ SHOP24HRS]";
            radTreeNode35.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode36,
            radTreeNode37});
            radTreeNode35.Text = "โปรโมชั่น [ของแถม]";
            radTreeNode38.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode38.Name = "MNFR_Detail";
            radTreeNode38.Text = "รายละเอียดแลกแต้ม";
            radTreeNode39.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode39.Name = "LineCouponControl";
            radTreeNode39.Text = "การใช้คูปอง Line@ ร้านเช่า";
            radTreeNode40.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode40.Name = "SUPC_MNPO_CheckOrder";
            radTreeNode40.Text = "ระบบจัดสินค้าไปมินิมาร์ททั้งหมด ";
            this.radTreeView_TabReport.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode29,
            radTreeNode30,
            radTreeNode33,
            radTreeNode34,
            radTreeNode35,
            radTreeNode38,
            radTreeNode39,
            radTreeNode40});
            this.radTreeView_TabReport.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabReport.ShowLines = true;
            this.radTreeView_TabReport.Size = new System.Drawing.Size(269, 544);
            this.radTreeView_TabReport.SpacingBetweenNodes = -5;
            this.radTreeView_TabReport.TabIndex = 0;
            this.radTreeView_TabReport.ThemeName = "Fluent";
            this.radTreeView_TabReport.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabReport_NodeMouseClick);
            // 
            // radPanel_Dept
            // 
            this.radPanel_Dept.Controls.Add(this.radLabel_Dpt);
            this.radPanel_Dept.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel_Dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel_Dept.ForeColor = System.Drawing.Color.Purple;
            this.radPanel_Dept.Location = new System.Drawing.Point(0, 0);
            this.radPanel_Dept.Name = "radPanel_Dept";
            this.radPanel_Dept.Size = new System.Drawing.Size(921, 29);
            this.radPanel_Dept.TabIndex = 3;
            this.radPanel_Dept.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).AutoSize = true;
            // 
            // radLabel_Dpt
            // 
            this.radLabel_Dpt.AutoSize = false;
            this.radLabel_Dpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            this.radLabel_Dpt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Dpt.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Dpt.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Dpt.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Dpt.Name = "radLabel_Dpt";
            this.radLabel_Dpt.Size = new System.Drawing.Size(921, 29);
            this.radLabel_Dpt.TabIndex = 19;
            this.radLabel_Dpt.Text = "User";
            this.radLabel_Dpt.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // object_843f177e_c01b_4318_8746_cb64b97fb147
            // 
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.Name = "object_843f177e_c01b_4318_8746_cb64b97fb147";
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchHorizontally = true;
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchVertically = true;
            // 
            // radTreeView1
            // 
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.Size = new System.Drawing.Size(241, 172);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 0;
            // 
            // D038_MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 608);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.radPanel_Dept);
            this.Name = "D038_MainMenu";
            this.ShowIcon = false;
            this.Text = "D038_แผนกโปรโมชั่น ของแถม";
            this.Load += new System.EventHandler(this.D038_MainMenu_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radCollapsiblePanel3.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radCollapsiblePanel7.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView2)).EndInit();
            this.radCollapsiblePanel2.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_Cust)).EndInit();
            this.radCollapsiblePanel6.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGernaral)).EndInit();
            this.radCollapsiblePanel4.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabJobGenaral)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radCollapsiblePanel5.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).EndInit();
            this.radPanel_Dept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel2;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabReport;
        protected Telerik.WinControls.UI.RadPanel radPanel_Dept;
        private Telerik.WinControls.RootRadElement object_843f177e_c01b_4318_8746_cb64b97fb147;
        private Telerik.WinControls.UI.RadLabel radLabel_Dpt;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel4;
        protected Telerik.WinControls.UI.RadPanel radPanel3;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel3;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabJobGenaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabGenaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabConfig;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel5;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel6;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel2;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabGernaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_Cust;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel7;
        protected Telerik.WinControls.UI.RadTreeView radTreeView2;
    }
}