﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Logistic;
using PC_Shop24Hrs.GeneralForm.BoxRecive;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.MNEC;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D204_MainMenu : Form
    {
        public D204_MainMenu()
        {
            InitializeComponent();
        }

        private void D204_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Recive_SH_Detail":
                    FormClass.ShowCheckForm(new CheckBOX_Detail("0", "", "0", ""), "รายละเอียดการส่งของ."); break;
                case "Recive_SH_Sum":
                    FormClass.ShowCheckForm(new CheckBOX_SH_SUM("0"), "ใบขนส่ง [สรุป]."); break;
                case "CheckBox_All":
                    FormClass.ShowCheckForm(new CheckBOX_All("0", "0"), "จำนวนลังค้างรับ."); break;

                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "BillEdit":
                    FormClass.ShowNotCheckForm(new BillEdit("0"), "รายงานบิลแก้ไข."); break;
                case "ReciveBoxOth":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("0"), "รายงานรับลังสาขาอื่น."); break;
                case "BillPallets":
                    FormClass.ShowNotCheckForm(new BillPallets("0"), "รายงานบิลพาเลท."); break;
                case "ReciveBox_P":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("1"), "รายงานรับลังค้างรับ."); break;
                case "ReciveBox_NotSH":
                    FormClass.ShowNotCheckForm(new ReciveBox_NotSH(), "ลังที่รับไม่ขึ้นทะเบียน."); break;
                case "SendMoney_Rpt":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_ReportSum("0"), "รายงานซองส่งเงินค้างส่ง."); break;
                case "D204_Report_0":
                    FormClass.ShowNotCheckForm(new D204_Report("0", "0"), "รายงานน้ำหนักรถต่างจังหวัด."); break;
                case "D204_Report_1":
                    FormClass.ShowNotCheckForm(new D204_Report("1", "0"), "รายงานการตรวจรถ 4 ด้าน."); break;
                case "CarCare0":
                    FormClass.ShowNotCheckForm(new CarCare_Report("0"), "รายงานการล้างรถ [ละเอียด]."); break;
                case "CarCare1":
                    FormClass.ShowNotCheckForm(new CarCare_Report("1"), "รายงานการล้างรถ [สรุป]."); break;
                case "BookCarCare":
                    FormClass.ShowNotCheckForm(new D204_Report("5", "0"), "รายงานการจองล้างรถ."); break;
                case "MNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRS"), "เบิกสินค้าส่งสาขา [MNRS]."); break;
                case "ReportSendLO":
                    FormClass.ShowNotCheckForm(new D204_Report("2", "0"), "รายงานการส่งคืนอุปกรณ์ตาม LO."); break;
                case "ReportNotSend":
                    FormClass.ShowNotCheckForm(new D204_Report("3", "0"), "รายงานการไม่เก็บ-ไม่มี อุปกรณ์."); break;
                case "ReportMNPMBYLO":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("4", "0"), "รายงานจำนวนซองส่งตามรอบ LO."); break;
                case "ReportStaPack":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("5", "0"), "รายงานสถานะการรับซองส่งเงิน."); break;

                case "Report_TagImage":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImage("0"), "รายงานรูปปิดท้ายและกุญแจรถ 1 ประตู."); break;
                case "Report_TagImageForCar":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImageForCar("0",""), "รายงานรูปปิดท้ายและกุญแจรถ > 1 ประตู."); break;
                case "DriverQ":
                    //FormClass.ShowNotCheckForm(new D204.D204_Report("6", "0"), "รายงานคิวพนักงานขับรถ."); break;
                    FormClass.ShowNotCheckForm(new Report_Queue(), "คิวพนักงาน ขับรถ/เด็กท้าย."); break;
                case "D204_Report_7":
                    FormClass.ShowNotCheckForm(new D204_Report("7", "0"), "รายงานฝาถังน้ำมัน."); break;
                case "SHOP_SHIPDOCNOTSEND":
                    FormClass.ShowNotCheckForm(new D204_Report("8", "0"), "รายงาน LO ที่ไม่เก็บ CN/ขยะ."); break;
                case "BILLIF":
                    FormClass.ShowNotCheckForm(new D204_Report("9", "0"), "รายงาน รายการรับบิลเบิก I/F/FAL."); break;
                default:
                    break;
            }

        }
        //งานทั้วไป
        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Lo_SlipMNRD":
                    FormClass.ShowNotCheckFormNormal(new LO_PrintSlip("0"), "พิมพ์สลิปเก็บ CN."); break;
                case "Lo_SlipMNPM":
                    FormClass.ShowNotCheckFormNormal(new LO_PrintSlip("1"), "พิมพ์สลิปเก็บซองเงิน."); break;
                //case "Box_MainLogistic":
                //    FormClass.ShowNotCheckFormNormal(new D204.Device.Box_MainLogistic("1"), "รับอุปกรณ์."); break;
                case "CarCare":
                    FormClass.ShowNotCheckForm(new CarCare("0", "1"), "การจองคิวล้างรถ."); break;
                case "InputQ":
                    FormClass.ShowCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("3"), "ลงคิวพนักงานขับรถ[จัดส่งมินิมาร์ท]."); break;
                case "PrintBillT":
                    FormClass.ShowCheckForm(new GeneralForm.OrderToAx.OrderToAx_PrintT(), "พิมพ์บิลผลไม้."); break;
                case "CarIDMNEC":
                    FormClass.ShowNotCheckForm(new MNEC_Main("24"), "ใส่ป้ายทะเบียนรถ แลกเหรียญสาขา."); break;
                case "SaveIFFAL":
                    FormClass.ShowCheckFormNormal(new JOB.Bill.BillAXAddJOB("", "", "", "", "", "", "", "", "", "","2"), "บันทึกรับบิลเบิก [I/F/FAL]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

            switch (e.Node.Name)
            {
                case "Config_ReturnDevice":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("22", "อุปกรณ์ส่งคืน", "0", "1", "1", "DV", "4"), "ข้อมูลอุปกรณ์ส่งคืน"); break;
                case "TagCar"://จำนวน Tag ท้ายรถ
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("40", "จำนวน Tag ท้ายรถ", "1", "1", "0", "", ""), "จำนวน Tag ท้ายรถ"); break;
                default:
                    break;
            }
        }
        //FarmHouse
        private void RadTreeView_farmHouse_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "FarmHouse_SendMN":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_BoxSend("24", "0"), "ส่งมินิมาร์ท [ระบุจำนวนลัง]."); break;
                case "FarmHouse_Recive":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_BranchRecive("SUPC", "0"), "รายละเอียดการรับสินค้า ฟาร์มเฮ้าส์."); break;

                default:
                    break;
            }
        }
    }
}
