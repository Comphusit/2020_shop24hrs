﻿using System;
using PC_Shop24Hrs.Controllers;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.Report;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D070_MainMenu : Form
    {
        public D070_MainMenu()
        {
            InitializeComponent();
        }

        private void D070_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }
        //ทั่วไป
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D070_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1", "17", 11), "ส่งออเดอร์ข้าวกล่องประจำวัน [ครัวชญานันทน์]."); break;
                case "D036_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1","19",11), "ส่งออเดอร์ข้าวกล่องประจำวัน [ครัวมาลินี]."); break;
                case "D047_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1", "20", 11), "ส่งออเดอร์ข้าวกล่องประจำวัน [ครัวศรัณยา]."); break;
                case "D025_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1", "25", 11), "ส่งออเดอร์เบเกอรี่."); break;
                case "Scan130":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                default:
                    break;
            }
        }
        //ตั้งค่า
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ConfigItemSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("17", "ข้าวกล่องส่งสาขา [ครัวชญานันทน์]", "1", "1","1", "0", "0"), "กำหนดข้าวกล่องส่งสาขา [ครัวชญานันทน์]."); break;
                case "ConfigBranchSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("17"), "กำหนดสาขาที่ลงข้าวกล่อง [ครัวชญานันทน์]."); break;
                case "D036_ConfigItemSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("19", "ข้าวกล่องส่งสาขา [ครัวมาลินี]", "1", "1", "1", "0", "0"), "กำหนดข้าวกล่องส่งสาขา [ครัวมาลินี]."); break;
                case "D036_ConfigBranchSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("19"), "กำหนดสาขาที่ลงข้าวกล่อง [ครัวมาลินี]."); break;
                case "D047_ConfigItemSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("20", "ข้าวกล่องส่งสาขา [ครัวศรัณยา]", "1", "1", "1", "0", "0"), "กำหนดข้าวกล่องส่งสาขา [ครัวศรัณยา]."); break;
                case "D047_ConfigBranchSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("20"), "กำหนดสาขาที่ลงข้าวกล่อง [ครัวศรัณยา]."); break;
                case "D025_ConfigItemSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("25", "เบเกอรี่ส่งสาขา", "1", "1", "1", "0", "0"), "กำหนดเบเกอรี่ส่งสาขา."); break;
                case "D025_ConfigBranchSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("25"), "กำหนดสาขาที่ลงเบเกอรี่."); break;
                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D070_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0","SUPC", "MNPI", "RETAILAREA","17"), "ข้อมูลการรับข้าวกล่อง [ครัวชญานันทน์]."); break;
                case "D070_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC","17", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง [ครัวชญานันทน์]."); break;
                case "D036_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "RETAILAREA","19"), "ข้อมูลการรับข้าวกล่อง [ครัวมาลินี]."); break;
                case "D036_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "19", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง [ครัวมาลินี]."); break;
                case "D047_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "RETAILAREA", "20"), "ข้อมูลการรับข้าวกล่อง [ครัวศรัณยา]."); break;
                case "D047_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "20", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง [ครัวศรัณยา]."); break;
                case "D025_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "RETAILAREA", "25"), "ข้อมูลการรับเบเกอรี่."); break;
                case "D025_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "25", "RETAILAREA"), "ข้อมูลการรับ-ส่งเบเกอรี่."); break;
                case "Report_MNPCItems_Dept":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCItems_Dept("SUPC"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "3"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                case "RptScan130":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกฝ่ายผลิตและครัว]."); break;
                case "LineCoupon":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("2", "0"), "การใช้ไลน์คูปอง [ละเอียด]."); break;
                case "LineCouponSUM":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("3", "0"), "การใช้ไลน์คูปอง [สรุป]."); break;
                case "BarcodeDisprice":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("9"), "รายงานสินค้าลดราคาตามบาร์โค๊ด."); break;
                case "FoodCourt":
                    FormClass.ShowNotCheckForm(new GeneralForm.FoodCourt.FoodCourt_Main("1","0"), "รายงานยอดขายระบบ FoodCourt."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป
        private void RadTreeView_TabScale_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "ScaleSUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.Scale.ScaleMain("SUPC", "SUPC"), "กำหนดสินค้าชั่ง."); break;
                default: break;
            }
        }
       

    }
}
