﻿namespace PC_Shop24Hrs.Mainmenu_Dpt 
{
    partial class D179_MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode8 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode9 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode10 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode11 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode12 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode13 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode14 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode15 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode16 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode17 = new Telerik.WinControls.UI.RadTreeNode();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel3 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabConfig = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel4 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabJOB = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabGenaral = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel5 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabReport = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel_Dept = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_Dpt = new Telerik.WinControls.UI.RadLabel();
            this.object_843f177e_c01b_4318_8746_cb64b97fb147 = new Telerik.WinControls.RootRadElement();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).BeginInit();
            this.radCollapsiblePanel3.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).BeginInit();
            this.radCollapsiblePanel4.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabJOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).BeginInit();
            this.radCollapsiblePanel5.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).BeginInit();
            this.radPanel_Dept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(921, 580);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.radCollapsiblePanel3);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel3.Location = new System.Drawing.Point(325, 3);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(316, 574);
            this.radPanel3.TabIndex = 4;
            // 
            // radCollapsiblePanel3
            // 
            this.radCollapsiblePanel3.AnimationFrames = 10;
            this.radCollapsiblePanel3.AnimationInterval = 10;
            this.radCollapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel3.HeaderText = "ตั้งค่าระบบ";
            this.radCollapsiblePanel3.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel3.Name = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 207);
            // 
            // radCollapsiblePanel3.PanelContainer
            // 
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.radTreeView_TabConfig);
            this.radCollapsiblePanel3.PanelContainer.Size = new System.Drawing.Size(314, 544);
            this.radCollapsiblePanel3.Size = new System.Drawing.Size(316, 574);
            this.radCollapsiblePanel3.TabIndex = 0;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabConfig
            // 
            this.radTreeView_TabConfig.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabConfig.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabConfig.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabConfig.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabConfig.ItemHeight = 28;
            this.radTreeView_TabConfig.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabConfig.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabConfig.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabConfig.Name = "radTreeView_TabConfig";
            radTreeNode1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode1.Name = "JOB_GROUPSUB";
            radTreeNode1.Text = "ประเภท JOB";
            radTreeNode2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode2.Name = "Machine_Manager";
            radTreeNode2.Text = "สิทธิ์เครื่องเช็คราคา";
            this.radTreeView_TabConfig.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1,
            radTreeNode2});
            this.radTreeView_TabConfig.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabConfig.ShowLines = true;
            this.radTreeView_TabConfig.Size = new System.Drawing.Size(314, 544);
            this.radTreeView_TabConfig.SpacingBetweenNodes = -5;
            this.radTreeView_TabConfig.TabIndex = 2;
            this.radTreeView_TabConfig.ThemeName = "Fluent";
            this.radTreeView_TabConfig.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabConfig_NodeMouseClick);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radCollapsiblePanel4);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(316, 574);
            this.radPanel1.TabIndex = 2;
            this.radPanel1.ThemeName = "Fluent";
            // 
            // radCollapsiblePanel4
            // 
            this.radCollapsiblePanel4.AnimationFrames = 10;
            this.radCollapsiblePanel4.AnimationInterval = 10;
            this.radCollapsiblePanel4.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel4.HeaderText = "งานทั่วไป";
            this.radCollapsiblePanel4.Location = new System.Drawing.Point(0, 160);
            this.radCollapsiblePanel4.Name = "radCollapsiblePanel4";
            this.radCollapsiblePanel4.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 138);
            // 
            // radCollapsiblePanel4.PanelContainer
            // 
            this.radCollapsiblePanel4.PanelContainer.Controls.Add(this.radTreeView_TabJOB);
            this.radCollapsiblePanel4.PanelContainer.Size = new System.Drawing.Size(314, 384);
            this.radCollapsiblePanel4.Size = new System.Drawing.Size(316, 414);
            this.radCollapsiblePanel4.TabIndex = 1;
            this.radCollapsiblePanel4.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabJOB
            // 
            this.radTreeView_TabJOB.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabJOB.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabJOB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabJOB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabJOB.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabJOB.ItemHeight = 28;
            this.radTreeView_TabJOB.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabJOB.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabJOB.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabJOB.Name = "radTreeView_TabJOB";
            radTreeNode3.Expanded = true;
            radTreeNode3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            radTreeNode3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode3.Name = "Node4";
            radTreeNode4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode4.Name = "JOB_Main";
            radTreeNode4.Text = "JOB SHOP";
            radTreeNode5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode5.Name = "JOB_Main_SUPC";
            radTreeNode5.Text = "JOB SUPC";
            radTreeNode6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode6.Name = "BillMN_ADDJOB";
            radTreeNode6.Text = "บิลสาขาส่งคืน";
            radTreeNode3.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode4,
            radTreeNode5,
            radTreeNode6});
            radTreeNode3.Text = "JOB ComService";
            radTreeNode7.Expanded = true;
            radTreeNode7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode7.Name = "Node8";
            radTreeNode8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode8.Name = "Asset_Detail";
            radTreeNode8.Text = "รายการสินทรัพย์";
            radTreeNode9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode9.Name = "Asset_Adjust";
            radTreeNode9.Text = "ปรับทะเบียนสินทรัพย์";
            radTreeNode10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode10.Name = "Asset_FAL";
            radTreeNode10.Text = "บิลเบิก I/F/FAL";
            radTreeNode11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode11.Name = "ClaimMain";
            radTreeNode11.Text = "ส่งซ่อมอุปกรณ์";
            radTreeNode12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode12.Name = "DiscardMain";
            radTreeNode12.Text = "นำทิ้งอุปกรณ์";
            radTreeNode7.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode8,
            radTreeNode9,
            radTreeNode10,
            radTreeNode11,
            radTreeNode12});
            radTreeNode7.Text = "อุปกรณ์สินทรัพย์";
            this.radTreeView_TabJOB.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode3,
            radTreeNode7});
            this.radTreeView_TabJOB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabJOB.ShowLines = true;
            this.radTreeView_TabJOB.Size = new System.Drawing.Size(314, 384);
            this.radTreeView_TabJOB.SpacingBetweenNodes = -5;
            this.radTreeView_TabJOB.TabIndex = 5;
            this.radTreeView_TabJOB.ThemeName = "Fluent";
            this.radTreeView_TabJOB.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabJOB_NodeMouseClick);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationFrames = 10;
            this.radCollapsiblePanel1.AnimationInterval = 10;
            this.radCollapsiblePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel1.HeaderText = "ข้อมูลทั่วไป";
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 138);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radTreeView_TabGenaral);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(314, 130);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(316, 160);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabGenaral
            // 
            this.radTreeView_TabGenaral.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabGenaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabGenaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabGenaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabGenaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabGenaral.ItemHeight = 28;
            this.radTreeView_TabGenaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabGenaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabGenaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabGenaral.Name = "radTreeView_TabGenaral";
            radTreeNode13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode13.Name = "Branch_Main";
            radTreeNode13.Text = "ข้อมูลสาขา";
            radTreeNode14.Expanded = true;
            radTreeNode14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode14.Name = "PhoneBook";
            radTreeNode14.Text = "เบอร์โทรภายใน";
            radTreeNode15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode15.Name = "Camera_Main";
            radTreeNode15.Text = "กล้องวงจรปิด";
            radTreeNode16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode16.Name = "programeUse";
            radTreeNode16.Text = "ข้อมูลโปรแกรมภายในบริษัท";
            radTreeNode17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode17.Name = "server";
            radTreeNode17.Text = "ข้อมูล Server/VM";
            this.radTreeView_TabGenaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode13,
            radTreeNode14,
            radTreeNode15,
            radTreeNode16,
            radTreeNode17});
            this.radTreeView_TabGenaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabGenaral.ShowLines = true;
            this.radTreeView_TabGenaral.Size = new System.Drawing.Size(314, 130);
            this.radTreeView_TabGenaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabGenaral.TabIndex = 4;
            this.radTreeView_TabGenaral.ThemeName = "Fluent";
            this.radTreeView_TabGenaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabGenaral_NodeMouseClick);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radCollapsiblePanel5);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel2.Location = new System.Drawing.Point(647, 3);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(271, 574);
            this.radPanel2.TabIndex = 3;
            // 
            // radCollapsiblePanel5
            // 
            this.radCollapsiblePanel5.AnimationFrames = 10;
            this.radCollapsiblePanel5.AnimationInterval = 10;
            this.radCollapsiblePanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel5.HeaderText = "รายงาน";
            this.radCollapsiblePanel5.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel5.Name = "radCollapsiblePanel5";
            this.radCollapsiblePanel5.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 271, 309);
            // 
            // radCollapsiblePanel5.PanelContainer
            // 
            this.radCollapsiblePanel5.PanelContainer.Controls.Add(this.radTreeView_TabReport);
            this.radCollapsiblePanel5.PanelContainer.Size = new System.Drawing.Size(269, 544);
            this.radCollapsiblePanel5.Size = new System.Drawing.Size(271, 574);
            this.radCollapsiblePanel5.TabIndex = 1;
            this.radCollapsiblePanel5.Visible = false;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabReport
            // 
            this.radTreeView_TabReport.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView_TabReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabReport.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabReport.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabReport.ItemHeight = 28;
            this.radTreeView_TabReport.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_TabReport.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabReport.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabReport.Name = "radTreeView_TabReport";
            this.radTreeView_TabReport.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabReport.ShowLines = true;
            this.radTreeView_TabReport.Size = new System.Drawing.Size(269, 544);
            this.radTreeView_TabReport.SpacingBetweenNodes = -5;
            this.radTreeView_TabReport.TabIndex = 0;
            this.radTreeView_TabReport.ThemeName = "Fluent";
            // 
            // radPanel_Dept
            // 
            this.radPanel_Dept.Controls.Add(this.radLabel_Dpt);
            this.radPanel_Dept.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel_Dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel_Dept.ForeColor = System.Drawing.Color.Purple;
            this.radPanel_Dept.Location = new System.Drawing.Point(0, 0);
            this.radPanel_Dept.Name = "radPanel_Dept";
            this.radPanel_Dept.Size = new System.Drawing.Size(921, 29);
            this.radPanel_Dept.TabIndex = 3;
            this.radPanel_Dept.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).AutoSize = true;
            // 
            // radLabel_Dpt
            // 
            this.radLabel_Dpt.AutoSize = false;
            this.radLabel_Dpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            this.radLabel_Dpt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Dpt.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Dpt.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Dpt.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Dpt.Name = "radLabel_Dpt";
            this.radLabel_Dpt.Size = new System.Drawing.Size(921, 29);
            this.radLabel_Dpt.TabIndex = 19;
            this.radLabel_Dpt.Text = "User";
            this.radLabel_Dpt.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // object_843f177e_c01b_4318_8746_cb64b97fb147
            // 
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.Name = "object_843f177e_c01b_4318_8746_cb64b97fb147";
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchHorizontally = true;
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchVertically = true;
            // 
            // radTreeView1
            // 
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.Size = new System.Drawing.Size(241, 172);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 0;
            // 
            // D179_MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 608);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.radPanel_Dept);
            this.Name = "D179_MainMenu";
            this.ShowIcon = false;
            this.Text = "D179_แผนกคอมพิวเตอร์-ช่างดูแลระบบคอมฯ";
            this.Load += new System.EventHandler(this.D179_MainMenu_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radCollapsiblePanel3.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radCollapsiblePanel4.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabJOB)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radCollapsiblePanel5.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).EndInit();
            this.radPanel_Dept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel2;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabReport;
        protected Telerik.WinControls.UI.RadPanel radPanel_Dept;
        private Telerik.WinControls.RootRadElement object_843f177e_c01b_4318_8746_cb64b97fb147;
        private Telerik.WinControls.UI.RadLabel radLabel_Dpt;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        protected Telerik.WinControls.UI.RadPanel radPanel3;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel3;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabConfig;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel5;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel4;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabGenaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabJOB;
    }
}