﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D010_MainMenu : Form
    {
        public D010_MainMenu()
        {
            InitializeComponent();
        }

        private void D010_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        //งานทั่วไป
        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "D010_ReportCash":
                    break;

                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SumMoney":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash_Report("SummaryMoney", "1"), "รายงานยอดส่งเงินขาด-เกิน."); break;
                case "FoodCourt":
                    FormClass.ShowNotCheckForm(new GeneralForm.FoodCourt.FoodCourt_Main("1", "0"), "รายงานยอดขายระบบ FoodCourt."); break;
                default:
                    break;
            }


        }
    }
}
