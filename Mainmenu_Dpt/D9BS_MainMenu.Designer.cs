﻿namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    partial class D9BS_MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode8 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode9 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode10 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode11 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode12 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode13 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode14 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode15 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode16 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode17 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode18 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode19 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode20 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode21 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode22 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode23 = new Telerik.WinControls.UI.RadTreeNode();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel3 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_cam = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel_Cam_BS = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabCam = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel4 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_JOB = new Telerik.WinControls.UI.RadTreeView();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView_TabGenaral = new Telerik.WinControls.UI.RadTreeView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radCollapsiblePanel5 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radPanel_Dept = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_Dpt = new Telerik.WinControls.UI.RadLabel();
            this.object_843f177e_c01b_4318_8746_cb64b97fb147 = new Telerik.WinControls.RootRadElement();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).BeginInit();
            this.radCollapsiblePanel3.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_cam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel_Cam_BS)).BeginInit();
            this.radCollapsiblePanel_Cam_BS.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).BeginInit();
            this.radCollapsiblePanel4.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_JOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).BeginInit();
            this.radPanel_Dept.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(921, 580);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.radCollapsiblePanel3);
            this.radPanel3.Controls.Add(this.radCollapsiblePanel_Cam_BS);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel3.Location = new System.Drawing.Point(325, 3);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(316, 574);
            this.radPanel3.TabIndex = 4;
            // 
            // radCollapsiblePanel3
            // 
            this.radCollapsiblePanel3.AnimationFrames = 10;
            this.radCollapsiblePanel3.AnimationInterval = 10;
            this.radCollapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel3.HeaderText = "กล้องประจำจุด";
            this.radCollapsiblePanel3.Location = new System.Drawing.Point(0, 136);
            this.radCollapsiblePanel3.Name = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.OwnerBoundsCache = new System.Drawing.Rectangle(0, 309, 271, 200);
            // 
            // radCollapsiblePanel3.PanelContainer
            // 
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.radTreeView_cam);
            this.radCollapsiblePanel3.PanelContainer.Size = new System.Drawing.Size(314, 408);
            this.radCollapsiblePanel3.Size = new System.Drawing.Size(316, 438);
            this.radCollapsiblePanel3.TabIndex = 3;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel3.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_cam
            // 
            this.radTreeView_cam.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView_cam.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_cam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_cam.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_cam.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_cam.ItemHeight = 28;
            this.radTreeView_cam.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_cam.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_cam.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_cam.Name = "radTreeView_cam";
            radTreeNode1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode1.Name = "CamOnline";
            radTreeNode1.Text = "กล้องออนไลน์ [ตามจุด]";
            radTreeNode2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode2.Name = "CAM_F";
            radTreeNode2.Text = "กล้องตู้แช่";
            radTreeNode3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode3.Name = "CAM_U";
            radTreeNode3.Text = "กล้องเพดาน";
            radTreeNode4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode4.Name = "CAM_D";
            radTreeNode4.Text = "กล้องส่องถนน";
            radTreeNode5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode5.Name = "CAM_C";
            radTreeNode5.Text = "กล้องส่องแคชเชียร์";
            radTreeNode6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode6.Name = "CAM_R";
            radTreeNode6.Text = "กล้องลงสินค้า";
            this.radTreeView_cam.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1,
            radTreeNode2,
            radTreeNode3,
            radTreeNode4,
            radTreeNode5,
            radTreeNode6});
            this.radTreeView_cam.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_cam.ShowLines = true;
            this.radTreeView_cam.Size = new System.Drawing.Size(314, 408);
            this.radTreeView_cam.SpacingBetweenNodes = -5;
            this.radTreeView_cam.TabIndex = 1;
            this.radTreeView_cam.ThemeName = "Fluent";
            this.radTreeView_cam.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_cam_NodeMouseClick);
            // 
            // radCollapsiblePanel_Cam_BS
            // 
            this.radCollapsiblePanel_Cam_BS.AnimationFrames = 10;
            this.radCollapsiblePanel_Cam_BS.AnimationInterval = 10;
            this.radCollapsiblePanel_Cam_BS.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel_Cam_BS.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel_Cam_BS.HeaderText = "กล้องวงจรปิด [ทั้งหมด]";
            this.radCollapsiblePanel_Cam_BS.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel_Cam_BS.Name = "radCollapsiblePanel_Cam_BS";
            this.radCollapsiblePanel_Cam_BS.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 207);
            // 
            // radCollapsiblePanel_Cam_BS.PanelContainer
            // 
            this.radCollapsiblePanel_Cam_BS.PanelContainer.Controls.Add(this.radTreeView_TabCam);
            this.radCollapsiblePanel_Cam_BS.PanelContainer.Size = new System.Drawing.Size(314, 106);
            this.radCollapsiblePanel_Cam_BS.Size = new System.Drawing.Size(316, 136);
            this.radCollapsiblePanel_Cam_BS.TabIndex = 0;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel_Cam_BS.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel_Cam_BS.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel_Cam_BS.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabCam
            // 
            this.radTreeView_TabCam.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabCam.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabCam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabCam.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabCam.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabCam.ItemHeight = 28;
            this.radTreeView_TabCam.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabCam.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabCam.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabCam.Name = "radTreeView_TabCam";
            radTreeNode7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode7.Name = "Camera_Main_MN";
            radTreeNode7.Text = "กล้องมินิมาร์ท";
            radTreeNode8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode8.Name = "Camera_Main_RET";
            radTreeNode8.Text = "กล้องพื้นที่ขาย";
            radTreeNode9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode9.Name = "Camera_Main_SPC";
            radTreeNode9.Text = "กล้องอาคารและพื้นที่ส่วนหลัง";
            radTreeNode10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            radTreeNode10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode10.Name = "Camera_Main_OTH";
            radTreeNode10.Text = "กล้องพื้นที่ส่วนนอก";
            this.radTreeView_TabCam.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode7,
            radTreeNode8,
            radTreeNode9,
            radTreeNode10});
            this.radTreeView_TabCam.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabCam.ShowLines = true;
            this.radTreeView_TabCam.Size = new System.Drawing.Size(314, 106);
            this.radTreeView_TabCam.SpacingBetweenNodes = -5;
            this.radTreeView_TabCam.TabIndex = 2;
            this.radTreeView_TabCam.ThemeName = "Fluent";
            this.radTreeView_TabCam.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabCam_NodeMouseClick);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radCollapsiblePanel4);
            this.radPanel1.Controls.Add(this.radCollapsiblePanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(316, 574);
            this.radPanel1.TabIndex = 2;
            this.radPanel1.ThemeName = "Fluent";
            // 
            // radCollapsiblePanel4
            // 
            this.radCollapsiblePanel4.AnimationFrames = 10;
            this.radCollapsiblePanel4.AnimationInterval = 10;
            this.radCollapsiblePanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel4.HeaderText = "งานซ่อม [JOB]";
            this.radCollapsiblePanel4.Location = new System.Drawing.Point(0, 68);
            this.radCollapsiblePanel4.Name = "radCollapsiblePanel4";
            this.radCollapsiblePanel4.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 271, 309);
            // 
            // radCollapsiblePanel4.PanelContainer
            // 
            this.radCollapsiblePanel4.PanelContainer.Controls.Add(this.radTreeView_JOB);
            this.radCollapsiblePanel4.PanelContainer.Size = new System.Drawing.Size(314, 476);
            this.radCollapsiblePanel4.Size = new System.Drawing.Size(316, 506);
            this.radCollapsiblePanel4.TabIndex = 2;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel4.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_JOB
            // 
            this.radTreeView_JOB.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView_JOB.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_JOB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_JOB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_JOB.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_JOB.ItemHeight = 28;
            this.radTreeView_JOB.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView_JOB.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_JOB.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_JOB.Name = "radTreeView_JOB";
            radTreeNode11.Expanded = true;
            radTreeNode11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode11.Name = "Node2";
            radTreeNode12.Expanded = true;
            radTreeNode12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode12.Name = "SPC_JOB_Maintanace";
            radTreeNode12.Text = "งานซ่อมทั่วไป";
            radTreeNode13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode13.Name = "SPC_JOB_CAR";
            radTreeNode13.Text = "งานซ่อมรถยนต์-เครื่องจักร";
            radTreeNode14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode14.Name = "SPC_COMService";
            radTreeNode14.Text = "งานซ่อม ComService";
            radTreeNode15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode15.Name = "SPC_ComMN";
            radTreeNode15.Text = "งานซ่อม ComMinimart";
            radTreeNode11.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode12,
            radTreeNode13,
            radTreeNode14,
            radTreeNode15});
            radTreeNode11.Text = "งานสาขาใหญ่";
            radTreeNode16.Expanded = true;
            radTreeNode16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode16.Name = "Node6";
            radTreeNode17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode17.Name = "MN_JOB_Maintanace";
            radTreeNode17.Text = "งานซ่อมทั่วไป";
            radTreeNode18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode18.Name = "MN_JOB_CAR";
            radTreeNode18.Text = "งานซ่อมรถยนต์-เครื่องจักร";
            radTreeNode19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode19.Name = "MN_COMService";
            radTreeNode19.Text = "งานซ่อม ComService";
            radTreeNode20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode20.Name = "MN_ComMN";
            radTreeNode20.Text = "งานซ่อม ComMinimart";
            radTreeNode21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode21.Name = "MN_Center";
            radTreeNode21.Text = "งาน CenterShop";
            radTreeNode22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode22.Name = "MN_Head";
            radTreeNode22.Text = "งานตรวจสาขา";
            radTreeNode16.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode17,
            radTreeNode18,
            radTreeNode19,
            radTreeNode20,
            radTreeNode21,
            radTreeNode22});
            radTreeNode16.Text = "งานมินิมาร์ท";
            this.radTreeView_JOB.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode11,
            radTreeNode16});
            this.radTreeView_JOB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_JOB.ShowLines = true;
            this.radTreeView_JOB.Size = new System.Drawing.Size(314, 476);
            this.radTreeView_JOB.SpacingBetweenNodes = -5;
            this.radTreeView_JOB.TabIndex = 0;
            this.radTreeView_JOB.ThemeName = "Fluent";
            this.radTreeView_JOB.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_JOB_NodeMouseClick);
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationFrames = 10;
            this.radCollapsiblePanel1.AnimationInterval = 10;
            this.radCollapsiblePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel1.HeaderText = "ข้อมูลทั่วไป";
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 316, 138);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radTreeView_TabGenaral);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(314, 38);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(316, 68);
            this.radCollapsiblePanel1.TabIndex = 0;
            this.radCollapsiblePanel1.Tag = "";
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radTreeView_TabGenaral
            // 
            this.radTreeView_TabGenaral.BackColor = System.Drawing.Color.White;
            this.radTreeView_TabGenaral.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView_TabGenaral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView_TabGenaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTreeView_TabGenaral.ForeColor = System.Drawing.Color.Black;
            this.radTreeView_TabGenaral.ItemHeight = 28;
            this.radTreeView_TabGenaral.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(206)))), ((int)(((byte)(255)))));
            this.radTreeView_TabGenaral.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView_TabGenaral.Location = new System.Drawing.Point(0, 0);
            this.radTreeView_TabGenaral.Name = "radTreeView_TabGenaral";
            radTreeNode23.Expanded = true;
            radTreeNode23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radTreeNode23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(16)))), ((int)(((byte)(174)))));
            radTreeNode23.Name = "PhoneBook";
            radTreeNode23.Text = "เบอร์โทรภายใน";
            this.radTreeView_TabGenaral.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode23});
            this.radTreeView_TabGenaral.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView_TabGenaral.ShowLines = true;
            this.radTreeView_TabGenaral.Size = new System.Drawing.Size(314, 38);
            this.radTreeView_TabGenaral.SpacingBetweenNodes = -5;
            this.radTreeView_TabGenaral.TabIndex = 3;
            this.radTreeView_TabGenaral.ThemeName = "Fluent";
            this.radTreeView_TabGenaral.NodeMouseClick += new Telerik.WinControls.UI.RadTreeView.TreeViewEventHandler(this.RadTreeView_TabGenaral_NodeMouseClick);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radCollapsiblePanel5);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radPanel2.Location = new System.Drawing.Point(647, 3);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(271, 574);
            this.radPanel2.TabIndex = 3;
            // 
            // radCollapsiblePanel5
            // 
            this.radCollapsiblePanel5.AnimationFrames = 10;
            this.radCollapsiblePanel5.AnimationInterval = 10;
            this.radCollapsiblePanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel5.HeaderText = "รายงาน";
            this.radCollapsiblePanel5.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel5.Name = "radCollapsiblePanel5";
            this.radCollapsiblePanel5.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 271, 309);
            // 
            // radCollapsiblePanel5.PanelContainer
            // 
            this.radCollapsiblePanel5.PanelContainer.Size = new System.Drawing.Size(269, 544);
            this.radCollapsiblePanel5.Size = new System.Drawing.Size(271, 574);
            this.radCollapsiblePanel5.TabIndex = 1;
            this.radCollapsiblePanel5.Visible = false;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).IsExpanded = true;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationInterval = 10;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel5.GetChildAt(0))).AnimationFrames = 10;
            // 
            // radPanel_Dept
            // 
            this.radPanel_Dept.Controls.Add(this.radLabel_Dpt);
            this.radPanel_Dept.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel_Dept.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel_Dept.ForeColor = System.Drawing.Color.Purple;
            this.radPanel_Dept.Location = new System.Drawing.Point(0, 0);
            this.radPanel_Dept.Name = "radPanel_Dept";
            this.radPanel_Dept.Size = new System.Drawing.Size(921, 29);
            this.radPanel_Dept.TabIndex = 3;
            this.radPanel_Dept.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.UI.RadPanelElement)(this.radPanel_Dept.GetChildAt(0))).AutoSize = true;
            // 
            // radLabel_Dpt
            // 
            this.radLabel_Dpt.AutoSize = false;
            this.radLabel_Dpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            this.radLabel_Dpt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Dpt.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Dpt.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Dpt.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Dpt.Name = "radLabel_Dpt";
            this.radLabel_Dpt.Size = new System.Drawing.Size(921, 29);
            this.radLabel_Dpt.TabIndex = 19;
            this.radLabel_Dpt.Text = "User";
            this.radLabel_Dpt.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // object_843f177e_c01b_4318_8746_cb64b97fb147
            // 
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.Name = "object_843f177e_c01b_4318_8746_cb64b97fb147";
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchHorizontally = true;
            this.object_843f177e_c01b_4318_8746_cb64b97fb147.StretchVertically = true;
            // 
            // radTreeView1
            // 
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.Size = new System.Drawing.Size(241, 172);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 0;
            // 
            // D9BS_MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 608);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.radPanel_Dept);
            this.Name = "D9BS_MainMenu";
            this.ShowIcon = false;
            this.Text = "D9BS_โก้";
            this.Load += new System.EventHandler(this.D9BS_MainMenu_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radCollapsiblePanel3.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_cam)).EndInit();
            this.radCollapsiblePanel_Cam_BS.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel_Cam_BS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radCollapsiblePanel4.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_JOB)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView_TabGenaral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Dept)).EndInit();
            this.radPanel_Dept.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        protected Telerik.WinControls.UI.RadPanel radPanel2;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_JOB;
        protected Telerik.WinControls.UI.RadPanel radPanel_Dept;
        private Telerik.WinControls.RootRadElement object_843f177e_c01b_4318_8746_cb64b97fb147;
        private Telerik.WinControls.UI.RadLabel radLabel_Dpt;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        protected Telerik.WinControls.UI.RadPanel radPanel3;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel_Cam_BS;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabGenaral;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_TabCam;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel5;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel3;
        protected Telerik.WinControls.UI.RadTreeView radTreeView_cam;
        protected Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel4;
    }
}