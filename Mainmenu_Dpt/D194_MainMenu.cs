﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.BoxRecive;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D194_MainMenu : Form
    {
        public D194_MainMenu()
        {
            InitializeComponent();
        }

        private void D194_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Recive_BOX":
                    FormClass.ShowCheckForm(new ReciveBOX_BoxGroup("0"), "รับลังสินค้า ตามเลขที่ลัง."); break;
                case "Recive_SH_Detail":
                    FormClass.ShowCheckForm(new CheckBOX_Detail("1", "", "0", ""), "รายละเอียดการส่งของ."); break;
                case "Recive_SH_Sum":
                    FormClass.ShowCheckForm(new CheckBOX_SH_SUM("1"), "ใบขนส่ง [สรุป]."); break;
                case "CheckBox_All":
                    FormClass.ShowCheckForm(new CheckBOX_All("1", "0"), "จำนวนลังค้างรับ."); break;
                case "MNPI18":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPI.MNPI_ReciveInvent("18", "น้ำแข็งส่งสาขา", "1"), "บิลรับน้ำแข็งเฉพาะสาขาภายในจังหวัดภูเก็ต [MNPI]."); break;
                case "MNPI19":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPI.MNPI_ReciveInvent("19", "อาหารกล่อง", "1"), "บิลรับอาหารกล่องประจำวัน [MNPI]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "BillEdit":
                    FormClass.ShowNotCheckForm(new BillEdit("1"), "รายงานบิลแก้ไข."); break;
                case "BillPallets":
                    FormClass.ShowNotCheckForm(new BillPallets("1"), "รายงานบิลพาเลท."); break;
                case "ReciveBoxOth":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("0"), "รายงานรับลังสาขาอื่น."); break;
                case "ReciveBox_P":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("1"), "รายงานรับลังค้างรับ."); break;
                case "ReciveBox_A":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("2"), "สแกนลังไม่ตรงกับ LO ที่ขึ้นทะเบียน."); break;
                case "ReciveBox_NotSH":
                    FormClass.ShowNotCheckForm(new ReciveBox_NotSH(), "ลังที่รับไม่ขึ้นทะเบียน."); break;
                case "Report_MNPC":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCDetail("SUPC"), "รายงานการคืนสินค้า."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_FindData("SUPC", "1"), "รายงานการคืนสินค้า แบบละเอียด."); break;
                case "BoxNotScan":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("3"), "รับลังไม่ผ่านการสแกน."); break;
                case "MNCS_ReportItemByJournalID":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("0", "1"), "รายงานตรวจนับสต๊อกสินค้า."); break;
                case "SPCN_SumInventCountingMNByVoucherByDate_FindJournalId":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("1", "1"), "รายงานเปรียบเทียบยอดขายกับจำนวนสต็อกคงเหลือ."); break;
                case "CountStkByEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("2", "2"), "รายงานจำนวนรายการเช็คสต๊อก."); break;
                case "CountStkByEmpHour":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("3", "2"), "รายงานจำนวนรายการเช็คสต๊อก[รายชั่วโมง]."); break;
                case "ItemHistoryCheckStock":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("4", "1"), "รายงานประวัติการเช็คสต็อกสินค้า [หน่วยย่อย]."); break;
                case "CheckBox_AllDetail":
                    FormClass.ShowCheckForm(new CheckBOX_All("1", "1"), "รายละเอียดลังค้างรับ."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป
        private void RadTreeView_TabImage_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "vender":
                    //FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("SUPC", "0"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("0"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CountStk":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Main("1"), "รายการนับสต็อก."); break;
                case "StockExcel":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_ImportItem("1"), "ปรับสต็อกสินค้าจาก Excel."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Config_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ReciveBox":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownProcess("SUPC", "1"), "ลบข้อมูลการรับลังสินค้าของมินิมาร์ท."); break;
                default:
                    break;
            }
        }
    }
}
