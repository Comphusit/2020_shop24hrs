﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Employee;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D088_MainMenu : Form
    {
        public D088_MainMenu()
        {
            InitializeComponent();
        }

        private void D088_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "Report_EmployeeVDO":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("3", "SUPC"), "ตรวจตัวเข้า-ออกงาน."); break;
                case "Report_EmployeeBreak":
                    FormClass.ShowNotCheckForm(new Employee_Report("4", "SUPC", "", DateTime.Now), "ลงเวลาพัก."); break;
                case "Scan1_30":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "สแกนนิ้ว 1.30 ชม [แผนกตรวจสินค้าส่วนหน้า]."); break;
                case "Report_Employee_CheckScan":
                    FormClass.ShowNotCheckForm(new Employee_Report("2", "SUPC", "", DateTime.Now), "รายงาน ตรวจสอบพนักงาน."); break;
                default:
                    break;
            }

        }
    }
}
