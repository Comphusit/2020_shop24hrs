﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D029_MainMenu : Form
    {
        public D029_MainMenu()
        {
            InitializeComponent();
        }

        private void D029_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D029_Claim":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("2", "00012", "ส่งซ่อม ลูกค้าคอมพิวเตอร์", "1"), "ส่งซ่อม ลูกค้าคอมพิวเตอร์."); break;
                case "D029_ClaimMain":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00002", "ComService", "1"), "ส่งซ่อมอุปกรณ์ ภายในบริษัท."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D029_ReportBill":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNCM_2"), "รายงานส่งซ่อมสินค้าลูกค้า [คอมพิวเตอร์]."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป

    }
}
