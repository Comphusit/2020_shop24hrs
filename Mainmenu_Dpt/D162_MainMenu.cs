﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D162_MainMenu : Form
    {
        public D162_MainMenu()
        {
            InitializeComponent();
        }

        private void D162_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }

        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }
        //ทั่วไป
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "D162_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1", "18", 11), "ส่งออเดอร์น้ำแข็ง."); break;               
                default:
                    break;
            }
        }
        //ตั้งค่า
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ConfigItemSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("18", "น้ำแข็งส่งสาขา", "1", "1","1", "0", "0"), "กำหนดน้ำแข็งส่งสาขา."); break;
                case "ConfigBranchSend":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("18"), "กำหนดสาขาที่ลงน้ำแข็ง]."); break;
                case "ConfigEmpIceDrive":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("45", "พนักงานขับรถ [ส่งน้ำแข็ง]", "1", "1", "1", "0", "0"), "กำหนดตั้งค่าพนักงานขับรถส่งน้ำแข็ง."); break;
                case "ConfigEmpIce":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("46", "พนักงานทัายรถ [ส่งน้ำแข็ง]", "1", "1", "1", "0", "0"), "กำหนดตั้งค่าพนักงานท้ายรถส่งน้ำแข็ง."); break;
                default:
                    break;
            }

        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D162_TranferItem_BranchReciveAX":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "WH-ICE","18"), "ข้อมูลการรับน้ำแข็ง."); break;
                case "D162_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "18", "WH-ICE"), "ข้อมูลการรับ-ส่งน้ำแช็ง."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabMNRS_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNRS": 
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SUPC", "WH-ICE"), "ทำรายการเบิกน้ำแข็ง."); break;
                case "MNRS_MN":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SHOP", "WH-ICE"), "ทำรายการเบิกน้ำแข็ง."); break;
                case "MNRS_D":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("1", "MNRS_D","0", "WH-ICE"), "ข้อมูลการเบิกน้ำแข็งแผนก [MNRS]."); break;
                case "MNRS_DMN":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("1", "MNRS_MN", "0","WH-ICE"), "ข้อมูลการเบิกน้ำแข็งมินิมาร์ท [MNRS]."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป

    }
}
