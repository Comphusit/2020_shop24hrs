﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D179_MainMenu : Form
    {
        public D179_MainMenu()
        {
            InitializeComponent();
        }

        private void D179_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";

            this.radTreeView_TabJOB.TreeViewElement.Scroller.ScrollMode = ItemScrollerScrollModes.Smooth;
            this.radTreeView_TabJOB.TreeViewElement.FitItemsToSize = true;
            //this.radCollapsiblePanel4.ElementTree.RootElement.FitToSizeMode = true;

        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Branch_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;               
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("0"), "เบอร์โทรภายใน."); break;
                case "Camera_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("0","1",""), "กล้องวงจรปิด."); break;
                case "programeUse":
                    FormClass.ShowNotCheckForm(new PhoneBook("2"), "ข้อมูลโปรแกรมภายในบริษัท."); break;
                case "server":
                    FormClass.ShowNotCheckForm(new ComMinimart.Config_Server_Main(), "รายละเอียดการใช้งาน Server."); break;
                default:
                    break;
            }
        }
        //JOB
        private void RadTreeView_TabJOB_NodeMouseClick(object sender, RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "JOB_Main":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00002", "ComService", "1",""), "ComService SHOP"); break;
                case "JOB_Main_SUPC":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SUPC", "00002", "ComService", "1",""), "ComService SUPC"); break;
                case "Asset_FAL":
                    FormClass.ShowCheckFormNormal(new JOB.Bill.BillAXAddJOB("", "", "", "", "", "", "", "","",""), "บิล I/F/FAL."); break;
                case "Asset_Detail":
                    FormClass.ShowNotCheckForm(new JOB.ASSET.Asset_Main("0"), "รายละเอียดสินทรัพย์ถาวร."); break;
                case "Asset_Adjust":
                    FormClass.ShowCheckFormNormal(new JOB.ASSET.AdjustAsset("", "", "", "", "", "", ""), "ปรับทะเบียนสินทรัพย์."); break;
                //case "JOB_GROUPSUB":
                //    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00002"), "ประเภทงาน."); break; 
                case "BillMN_ADDJOB":
                    FormClass.ShowCheckForm(new JOB.Bill.BillMN_ForAddJOB("'D156','D179'", "MN%", "", "00002"), "ข้อมูลการส่งซ่อม-ส่งคืนอุปกรณ์ต่างๆ."); break;
                case "ClaimMain":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00002", "ComService", "1"), "ส่งซ่อมอุปกรณ์ ภายในบริษัท."); break;
                case "DiscardMain":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("1", "00002", "ComService", "1"), "นำทิ้งอุปกรณ์ ภายในบริษัท."); break;
                default:
                    break;

            }
        }
        //config

        private void RadTreeView_TabConfig_NodeMouseClick(object sender, RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "JOB_GROUPSUB":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00002"), "ประเภทงาน."); break;
                case "Machine_Manager":
                    FormClass.ShowCheckForm(new ComMinimart.Manage.Machine_Manager("1"), "สิทธิ์เครื่อง."); break;
                default:
                    break;

            }
        }



        //private void RadTreeView_TabJOB_NodeExpandedChanged(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        //{
        //    int maxRight = 0;// radTreeView_TabJOB.ClientSize.Width;

        //    //  if (e.Node.Nodes != null)
        //    //    foreach (TreeNode node in e.Node.Nodes)
        //    //    {
        //    //        maxRight = Math.Max(maxRight, node.Bounds.Right);
        //    //    }

        //    int y = e.Node.Nodes.Count;
        //    for (int i = 0; i < y - 1; i++)

        //    {
        //        maxRight += e.Node.Nodes[i].ChildrenSize.Height;
        //    }

        //    radTreeView_TabJOB.Height = maxRight;//new Size(maxRight, radTreeView_TabJOB.ClientSize.Height);
        //}


        //readonly Dictionary <object, State> nodeStates = new Dictionary<object, State>();

        // struct State
        // {
        //     public bool Expanded { get; set; }

        //     public bool Selected { get; set; }

        //     public State(bool expanded, bool selected) : this()
        //     {
        //         this.Expanded = expanded;
        //         this.Selected = selected;
        //     }
        // }

        // private void SaveExpandedStates(RadTreeNode nodeToSave)
        // {
        //     {
        //         if (nodeToSave != null && nodeToSave.DataBoundItem != null)
        //         {
        //             if (!nodeStates.ContainsKey(nodeToSave.DataBoundItem))
        //             {
        //                 nodeStates.Add(nodeToSave.DataBoundItem, new State(nodeToSave.Expanded, nodeToSave.Selected));
        //             }
        //             else
        //             {
        //                 nodeStates[nodeToSave.DataBoundItem] = new State(nodeToSave.Expanded, nodeToSave.Selected);
        //             }
        //         }
        //         foreach (RadTreeNode childNode in nodeToSave.Nodes)
        //         {
        //             SaveExpandedStates(childNode);
        //         }
        //     }
        // }

        // private void RestoreExpandedStates(RadTreeNode nodeToRestore)
        // {
        //     if (nodeToRestore != null && nodeToRestore.DataBoundItem != null &&
        //         nodeStates.ContainsKey(nodeToRestore.DataBoundItem))
        //     {
        //         nodeToRestore.Expanded = nodeStates[nodeToRestore.DataBoundItem].Expanded;
        //         nodeToRestore.Selected = nodeStates[nodeToRestore.DataBoundItem].Selected;
        //     }

        //     foreach (RadTreeNode childNode in nodeToRestore.Nodes)
        //     {
        //         RestoreExpandedStates(childNode);
        //     }
        // }
    }
}
