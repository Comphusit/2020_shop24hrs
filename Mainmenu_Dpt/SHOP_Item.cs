﻿using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.BoxRecive;
using PC_Shop24Hrs.GeneralForm.SendMoney;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.MNEC;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class SHOP_Item : Form
    {
        public SHOP_Item()
        {
            InitializeComponent();
        }

        private void SHOP_Item_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }
        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ItembarcodeDetail":
                    FormClass.ShowCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "1", ""), "รายละเอียดสินค้า."); break;
                case "PriceChange":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("4"), "รายงานการเปลี่ยนเเปลงราคา."); break;
                case "ScaleShopCommit":
                    FormClass.ShowNotCheckForm(new GeneralForm.Scale.ScaleMain("SHOP", "SHOP"), "ส่งราคาตาชั่ง."); break;
                default:
                    break;
            }
        }
        //สั่งสินค้า
        private void RadTreeView_TabMNPO_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPO_SHOP":
                    FormClass.ShowCheckForm(new GeneralForm.MNPO.MNPO_Order("SHOP", "สาขาเปิด [PC]"), "เปิดออเดอร์ทั่วไป [MNPO]."); break;
                case "MNOR":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_ORDER(), "เปิดออเดอร์แผนก [MNOR]."); break;
                case "MNOG":
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNOG_Order("Minimart"), "สั่งสินค้าของสดให้ลูกค้า [MNOG]"); break;
                case "MNPD":
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNPD_Order("Minimart"), "สั่งสินค้าขอทั่วไปให้ลูกค้า [MNPD]"); break;
                case "reciveMNOG"://สถานะรับออเดอร์ลูกค้าของสด [MNOG]
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNOG_MNPD_ReciveOrder("MNOG", "0"), "สถานะรับออเดอร์ลูกค้าของสด [MNOG]."); break;
                default:
                    break;
            }
        }


        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SHOP_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("11", "SHOP", "", "", ""), "เช็คสถานะออเดอร์ ระบบจัดสินค้าทั้งหมด."); break;
                case "Qty_Web":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("1", "SHOP", "", "", ""), "จำนวนออเดอร์ผ่าน WEB [เฉพาะสั่งตามบาร์โค้ด]."); break;
                case "Order_picture":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("4", "SHOP", "", "", ""), "จำนวนออเดอร์ [เฉพาะสั่งผ่านรูป]."); break;
                case "MNPO_WEBSum":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.MNPO_Report("0", "SHOP"), "ออเดอร์สั่งสินค้าผ่านมือถือ [สรุป]."); break;
                case "MNPO_WEBDetail":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.MNPO_Report("1", "SHOP"), "ออเดอร์สั่งสินค้าผ่านมือถือ [ละเอียด]."); break;
                case "SHOP_TranferItemDpt":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("5", "SHOP", "", "", ""), "เช็คสถานะออเดอร์ แผนกจัดสินค้าจัดให้เฉพาะที่สั่ง."); break;
                case "SHOP_TranferItemDptAll":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("8", "SHOP", "", "", ""), "เช็คสถานะออเดอร์ ระบบจัดซื้อจัดสินค้าเอง."); break;
                case "Report_MNPCItems_Dept":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCItems_Dept("SHOP"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "3"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                case "Report_DetailProduct_ByDate":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("5"), "รายงานจำนวนสินค้า ขาย-เบิก-คืน."); break;
                case "Report_DetailProduct_TO":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("6"), "รายงานจำนวนสินค้าเบิก."); break;
                case "Report_DetailProduct_CN":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("7"), "รายงานจำนวนสินค้าคืน."); break;
                case "Report_DetailProduct_Sale":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("8"), "รายงานจำนวนสินค้าขาย."); break;
                case "ReportSumGrandBYBch":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("9"), "รายงานมูลค่า เบิก-ขาย-คืน-สต็อก ตามสาขา"); break;
                case "ReportSumGrandBYDate":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("10"), "รายงานมูลค่า เบิก-ขาย-คืน-สต็อก ตามวันที่."); break;
                case "MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG"), "ส่งของไม่มีทะเบียนคืน [MNRG]."); break;
                case "Detail_MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG", "0"), "รายละเอียดส่งของไม่มีทะเบียน [MNRG]."); break;
                case "MNRB":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRB"), "ส่งของมีทะเบียนคืน [MNRB]."); break;
                case "MNRH":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRH"), "ส่งคืนเอกสาร [MNRH]."); break;
                case "ReportMNRD":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRD"), "ส่งคืนขยะ-กระดาษลัง [MNRD]."); break;
                case "ReportSendMNPMDetail":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("7", "0"), "รายงานรายละเอียดการส่งซองเงิน."); break;
                case "TransferMNPM":
                    FormClass.ShowNotCheckForm(new Report_Recheck("2", "0"), "รายงานการโอนเงินเข้าบัญชี [ส่งซองเงิน]."); break;
                case "SPC_TransferBlockedItemTable":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("0"), "รายงานสินค้าหยุดส่งมินิมาร์ท [ห้ามสาขาขาย]."); break;
                case "SPC_RETAILPLANORDERITEMBLOCKED":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("1"), "รายงานหยุดแผนใบสั่ง-สินค้า [ห้ามสาขาสั่ง]."); break;
                case "ReportText17":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("17"), "รายงานอัตราเปรียบเทียบจำนวนการขายสินค้า ย้อนหลัง 3 เดือน.."); break;
                case "ReportText18":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("18"), "จำนวนการขายสินค้า ย้อนหลัง 3 เดือน ตามกลุ่มสินค้า [ประเมินการขาย]."); break;
                case "SUPC-SEND-RPT":
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.ControlProduct.ControlProduct_Main("1", "SHOP", "0"), "รายงานรายละเอียดการรับสินค้าควบคุม [ส่งจาก SUPC]."); break;
                case "BillEdit":
                    FormClass.ShowNotCheckForm(new BillEdit("0"), "รายงานบิลแก้ไข."); break;
                case "BillPallets":
                    FormClass.ShowNotCheckForm(new BillPallets("0"), "รายงานบิลพาเลท."); break;
                case "ReciveBoxOth":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("0"), "รายงานรับลังสาขาอื่น."); break;
                case "ReciveBox_P":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("1"), "รายงานรับลังค้างรับ."); break;
                case "ReciveBox_A":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("2"), "สแกนลังไม่ตรงกับ LO ที่ขึ้นทะเบียน."); break;
                case "ReciveBox_NotSH":
                    FormClass.ShowNotCheckForm(new ReciveBox_NotSH(), "ลังที่รับไม่ขึ้นทะเบียน."); break;
                case "ShelfReportSale":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfReportSale("SHOP"), "ยอดขายสินค้าตามชั้นวาง."); break;
                //case "SPC_RETAILINVENTBLOCKSCHED":
                //    FormClass.ShowNotCheckForm(new D054.Config_AlcoholSale("0"), "การตั้งค่าการขายสินค้าประเภทเหล้า-เบียร์."); break;
                case "BoxNotScan":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("3"), "รับลังไม่ผ่านการสแกน."); break;
                case "MNCS_ReportItemByJournalID":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Report("0", "0"), "รายงานตรวจนับสต๊อกสินค้า."); break;
                case "SPCN_SumInventCountingMNByVoucherByDate_FindJournalId":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("1", "0"), "รายงานเปรียบเทียบยอดขายกับจำนวนสต็อกคงเหลือ."); break;
                case "CountStkByEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("2", "0"), "รายงานจำนวนรายการเช็คสต๊อก."); break;
                case "CountStkByEmpHour":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("3", "0"), "รายงานจำนวนรายการเช็คสต๊อก[รายชั่วโมง]."); break;
                case "ItemHistoryCheckStock":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("4", "0"), "รายงานประวัติการเช็คสต็อกสินค้า [หน่วยย่อย]."); break;
                case "CountStkByEmpYear":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("5", "0"), "รายงานจำนวนรายการเช็คสต๊อก[รายปี]."); break;
                case "StockHis":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("6", "0"), "รายงานประวัติการเช็คสต็อกสินค้า [สรุป]."); break;
                case "barcodeDiscount164":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("7"), "สินค้าที่ขายตามบาร์โค้ดทั้งหมด."); break;
                case "SumDiscount":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("8"), "รายงานสินค้าลดราคาตามยอด/จำนวน."); break;
                case "SumDiscount_ByBarcode":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("9"), "รายงานสินค้าลดราคาตามบาร์โค๊ด."); break;
                case "ReportScanSUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("16", "SHOP", "", "", ""), "รายงานการจัดสินค้าสาขาใหญ่."); break;
                default:
                    break;
            }

        }
        //ซองส่งเงิน
        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SaveCashReciveAR":
                    FormClass.ShowNotCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("1"), "บันทึกข้อมูล การรับซองเงิน [บัญชีลูกหนี้]."); break;
                case "SaveSlipEMV":
                    FormClass.ShowNotCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("2"), "บันทึกข้อมูล สลิปโอนยอดบัตรสวัสดิการ [EMV]."); break;
                case "SaveSendMoney":
                    FormClass.ShowCheckForm(new GeneralForm.MNPM.MNPM_Main(), "บันทึก ส่งซองเงิน."); break;
                case "SaveChangeCoin":
                    //FormClass.ShowNotCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("4"), "บันทึกข้อมูล ซองแลกเหรียญมินิมาร์ท."); break;
                    FormClass.ShowNotCheckForm(new MNEC_Main("22"), "แลกเหรียญ [MNEC]."); break;
                case "SaveSteal":
                    FormClass.ShowNotCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("5"), "บันทึกข้อมูล ซองค่าปรับขโมย."); break;
                case "SaveChaneCash":
                    FormClass.ShowNotCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("6"), "บันทึกข้อมูล เงินทอนของแคชเชียร์."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Recive_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPI18":
                    System.Data.DataTable dt = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    if (dt.Rows[0]["BRANCH_OUTPHUKET"].ToString() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("เมนูนี้เปิดใช้งานเฉพาะสาขาภายในจังหวัดภูเก็ตเท่านั้น");
                        return;
                    }

                    FormClass.ShowCheckForm(new GeneralForm.MNPI.MNPI_ReciveInvent("18", "น้ำแข็งส่งสาขา", "0"), "บันทึกรับน้ำแข็งเฉพาะสาขาภายในจังหวัดภูเก็ต."); break;
                case "MNPI19":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPI.MNPI_ReciveInvent("19", "อาหารกล่อง", "0"), "บันทึกรับอาหารกล่องประจำวัน."); break;

                case "FarmHouse":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_BranchRecive("SHOP", "1"), "รายละเอียดการรับสินค้า ฟาร์มเฮ้าส์."); break;
                case "Vender":
                    //FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("SHOP", "0"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("0"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                case "MP":
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.MP.MP_Main("SHOP", "0"), "รายละเอียดการรับสินค้าเซลล์ส่ง [MP]."); break;
                case "SUPC-SEND":
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.ControlProduct.ControlProduct_Main("0", "SHOP", "0"), "รายละเอียดการรับสินค้าควบคุม [ส่งจาก SUPC]."); break;
                case "LO":
                    FormClass.ShowCheckForm(new CheckBOX_SH_SUM("0"), "ใบขนส่งสินค้า SUPC."); break;
                default:
                    break;
            }
        }
        //งานทั่วไป
        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "StockD032":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("16"), "รายงานจำนวนรายการส่งสต็อกหมู-ไก่."); break;
                case "StockFruit":
                    FormClass.ShowNotCheckForm(new GeneralForm.SendStock.ReportSendStock("SHOP"), "รายงานส่งสต๊อกผลไม้มินิมาร์ท"); break;
                case "CountStk":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Main("0"), "รายการนับสต็อก."); break;
                case "BarcodeDiscount":
                    //FormClass.ShowCheckFormNormal(new JOB.CheckLockOilCap("1"), "การพิมพ์บาร์โค้ดลดราคา."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("6"), "การพิมพ์บาร์โค้ดลดราคา."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_CN_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPC_SHOPDocument":
                    FormClass.ShowCheckForm(new GeneralForm.MNPC.MNPC_SHOPDocument("SHOP"), "คินสินค้า [MNPC]."); break;

                default:
                    break;
            }
        }
    }
}
