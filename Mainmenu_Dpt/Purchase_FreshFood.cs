﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.BoxRecive;
using PC_Shop24Hrs.GeneralForm.Report;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class Purchase_FreshFood : Form
    {
        public Purchase_FreshFood()
        {
            InitializeComponent();
        }

        private void Purchase_FreshFood_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Branch":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabScale_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "ScaleSUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.Scale.ScaleMain("SUPC", "SUPC"), "กำหนดสินค้าชั่ง สาขาใหญ่."); break;
                case "ScaleMN":
                    FormClass.ShowNotCheckForm(new GeneralForm.Scale.ScaleMain("SUPC", "SHOP"), "กำหนดสินค้าชั่ง มินิมาร์ท."); break;
                default:
                    break;
            }
        }


        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ConfigGroup":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroup("01", "กลุ่มสินค้าสั่ง"), "การตั้งค่ากลุ่มสินค้าสั่ง");
                    break;
                case "ConfigItembarcode":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroupBarcode("01"), "การตั้งค่าสินค้าสั่งตามกลุ่มสินค้า");
                    break;
                case "ConfigItembarcodeStock":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOR.ItemsGroupStock("01", "กลุ่มสินค้าสั่ง"), "การตั้งค่าสินค้าสั่งส่งสต๊อก");
                    break;
                case "D032_Item":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("32", "อาหารกล่อง [จัดซื้อหมู-ไก่]", "1", "1", "1", "0", "0"), "กำหนดอาหารกล่อง [จัดซื้อหมู-ไก่] ส่งสาขา."); break;
                case "D032_Bch":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("32"), "กำหนดสาขาที่อาหารกล่อง [จัดซื้อหมู-ไก่]."); break;
                case "D144_Item":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("33", "อาหารกล่อง [จัดซื้ออาหารทะเล]", "1", "1", "1", "0", "0"), "กำหนดอาหารกล่อง [จัดซื้ออาหารทะเล] ส่งสาขา."); break;
                case "D144_Bch":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("33"), "กำหนดสาขาที่อาหารกล่อง [จัดซื้ออาหารทะเล]."); break;
                case "configScanDpt":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("43", "ตั้งค่าพนักงานสำหรับผู้ช่วยสแกนแทนได้", "1", "1", "1", "0", "0"), "กำหนดผู้จำหน่ายลงสินค้าสาขา."); break;
                case "ConfigDesc":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("44", "ตั้งค่าปลายทางสำหรับสแกนนิ้วเข้าแผนก", "0", "1", "1", "Q", "3"), "กำหนดผู้จำหน่ายลงสินค้าสาขา."); break;
                case "OrderToAx_ConfigBranch":
                //FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAx_ConfigBranch(SystemClass.SystemComMinimart), "เส้นทาง-สาขา"); break;
                //FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAX_ConfigRoute(), "จัดการการตั้งค่า เส้นทาง-สาขา"); break;
                case "ConfigD110_Grp":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("48", "ตั้งค่ากลุ่มหลักของแผนกผัก OrderToAX", "0", "1", "1", "D110_", "3"), "กลุ่มหลักของแผนกผัก OrderToAX"); break;
                //case "A":
                //    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("43", "ตั้งค่าพนักงานสำหรับผู้ช่วยสแกนแทนได้", "1", "1", "1", "", "", "1"), "ตั้งค่าพนักงานสำหรับผู้ช่วยสแกนแทนได้"); break;
                //case "B":
                //    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("44", "ตั้งค่าปลายทางสำหรับสแกนนิ้วเข้าแผนก", "0", "1", "1", "L", "3", "1"), "ตั้งค่าปลายทางสำหรับสแกนนิ้วเข้าแผนก"); break;

                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "StockFruit":
                    FormClass.ShowNotCheckForm(new GeneralForm.SendStock.ReportSendStock("SUPC"), "รายงานสต๊อกมินิมาร์ทส่ง"); break;
                case "MNBC":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("2"), "รายงานการซื้อสินค้าของลูกค้ามินิมาร์ท"); break;
                case "D032_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "RETAILAREA", "32"), "ข้อมูลการรับอาหารกล่อง [จัดซื้อหมู-ไก่]."); break;
                case "D032_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "32", "RETAILAREA"), "ข้อมูลการรับ-ส่งอาหารกล่อง [จัดซื้อหมู-ไก่]."); break;
                case "D144_TranferItem_BranchRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "MNPI", "RETAILAREA", "33"), "ข้อมูลการรับอาหารกล่อง [จัดซื้ออาหารทะเล]."); break;
                case "D144_TranferDelivary_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "33", "RETAILAREA"), "ข้อมูลการรับ-ส่งอาหารกล่อง [จัดซื้ออาหารทะเล]."); break;
                case "LineCoupon":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("2", "0"), "การใช้ไลน์คูปอง [ละเอียด]."); break;
                case "LineCouponSUM":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("3", "0"), "การใช้ไลน์คูปอง [สรุป]."); break;
                case "DetailBill_POSRetail":
                    FormClass.ShowCheckForm(new ComMinimart.DetailBill_POSRetail(), "รายละเอียดบิลขาย."); break;
                case "ScanHalf":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานสแกนนิ้ว 1.30 ชม [แผนกจัดซื้อาหารสด]."); break;
                case "ScanDept":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("D110"), "รายงานสแกนนิ้วเข้าแผนก."); break;
                case "BarcodeDisprice":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("9"), "รายงานสินค้าลดราคาตามบาร์โค๊ด."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Recive_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "reciveOrderMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_FindData("0"), "รับออเดอร์ของสดมินิมาร์ท [MNOR].");
                    break;
                case "LoadOrderMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_LoadOrder(), "ดึงออเดอร์จัด [MNOR].");
                    break;
                case "reciveMNOG"://รับออเดอร์ลูกค้า
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNOG_MNPD_ReciveOrder("MNOG", "1"), "รับออเดอร์ลูกค้าของสด [MNOG]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_OrderOR_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNOR":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_ORDER(), "สั่งสินค้าแผนกให้สาขา [MNOR]."); break;
                case "MNOG"://คีย์ออเดอร์ลูกค้าให้สาขา
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNOG_Order("Purchase"), "สั่งสินค้าของสดให้ลูกค้า [MNOG]"); break;
                default:
                    break;
            }
        }
        //งานทั่วไป
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D032_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1", "32", 11), "ส่งออเดอร์อาหารกล่อง [จัดซื้อหมูไก่]."); break;
                case "D144_TranferDelivary_Template":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Template("SUPC", "1", "33", 11), "ส่งออเดอร์อาหารกล่อง [จัดซื้ออาหารทะเล]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_OrderToAX_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderToAx_ConfigBranch":
                //FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAx_ConfigBranch(SystemClass.SystemComMinimart), "เส้นทาง-สาขา"); break;
                //FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAX_ConfigRoute(), "จัดการการตั้งค่า เส้นทาง-สาขา"); break;
                case "OrderItemsExcel":
                //FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderItemsExcel(SystemClass.SystemComMinimart), "เอกสารตั้งต้น."); break;
                case "OrderToAx_MRTDocument":
                    FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAx_MRTDocument(), "เอกสารจัดสินค้า."); break;
                case "OrderToAx_Main":
                    FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAx_Main(), "จัดสินค้า."); break;
                case "DeleBox":
                    FormClass.ShowNotCheckForm(new ReciveBOX_BoxGroup("1"), "ลบลังส่งของ."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Scan_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Scan130":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                case "ScanDpt":
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ยังไม่เปิดให้ใช้งาน ต้องใช้งานที่โปรแกรม ScanDept ตัวเก่าก่อนเท่านั้น");
                    //FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.ScanDptTime("0",""), "สแกนนิ้วเข้าแผนก.");
                    break;
                default:
                    break;
            }
        }


    }
}
