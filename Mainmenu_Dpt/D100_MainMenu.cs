﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.PhoneBook;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D100_MainMenu : Form
    {
        public D100_MainMenu()
        {
            InitializeComponent();
        }

        private void D100_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "BchDetail":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                case "ItemDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D029_Claim":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("3", "00013", "ส่งซ่อม ลูกค้ามือถือ", "1"), "ส่งซ่อม ลูกค้ามือถือ."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D029_ReportBill":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNCM_3"), "รายงานส่งซ่อมสินค้าลูกค้า [มือถือ]."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป

    }
}
