﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.BillOut;
using PC_Shop24Hrs.GeneralForm.BoxRecive;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D011_MainMenu : Form
    {
        public D011_MainMenu()
        {
            InitializeComponent();
        }

        private void D011_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                //case "Recive_BOX":
                //    Controllers.FormClass.ShowCheckForm(new D194.ReciveBOX_BoxGroup(), "รับลังสินค้า ตามเลขที่ลัง."); break;
                case "Recive_SH_Detail":
                    FormClass.ShowCheckForm(new CheckBOX_Detail("0", "", "0", ""), "รายละเอียดการส่งของ."); break;
                //case "Recive_SH_Sum":
                //    Controllers.FormClass.ShowCheckForm(new D194.CheckBOX_SH_SUM("0"), "ใบขนส่ง [สรุป]."); break;
                //case "CheckBox_All":
                //    Controllers.FormClass.ShowCheckForm(new D194.CheckBOX_All("0"), "จำนวนลังค้างรับ."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                //case "BillEdit":
                //    Controllers.FormClass.ShowNotCheckForm(new D194.BillEdit("0"), "รายงานบิลแก้ไข."); break;
                //case "BillPallets":
                //    Controllers.FormClass.ShowNotCheckForm(new D194.BillPallets("0"), "รายงานบิลพาเลท."); break;
                //case "ReciveBoxOth":
                //    Controllers.FormClass.ShowNotCheckForm(new D194.ReciveBox_STA("0"), "รายงานรับลังสาขาอื่น."); break;
                //case "ReciveBox_P":
                //    Controllers.FormClass.ShowNotCheckForm(new D194.ReciveBox_STA("1"), "รายงานรับลังค้างรับ."); break;
                //case "ReciveBox_A":
                //    Controllers.FormClass.ShowNotCheckForm(new D194.ReciveBox_STA("2"), "สแกนลังไม่ตรงกับ LO ที่ขึ้นทะเบียน."); break;
                //case "ReciveBox_NotSH":
                //    Controllers.FormClass.ShowNotCheckForm(new D194.ReciveBox_NotSH(), "ลังที่รับไม่ขึ้นทะเบียน."); break;
                case "ReportMNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRS"), "เบิกสินค้าส่งสาขา [MNRS]."); break;
                default:
                    break;
            }
        }
        //MNRS
        private void RadTreeView_TabMNRS_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D038_SmartRO_LoadOrder":
                    FormClass.ShowCheckForm(new MNRO_LoadOrder(), "ดึงข้อมูลจัดสินค้าส่งมินิมาร์ท."); break;
                case "MNRS":
                    FormClass.ShowCheckForm(new MNRS_Main("SHOP", "WH-A"), "ทำรายการเบิกสินค้า."); break;
                case "MNRS_D":
                    FormClass.ShowNotCheckForm(new BillOut_FindBill_APV("1", "MNRS_MN", "0", "WH-A"), "ข้อมูลการเบิกสินค้ามินิมาร์ท [MNRS]."); break;
                case "MNRS_D177":
                    FormClass.ShowCheckForm(new MNRS_Main("SUPC", "WH-A", "D177", "ฝ่ายผลิต-วัสดุสิ้นเปลือง", ""), "ทำรายการเบิกวัตถุดิบเฉพาะฝ่ายผลิต-โรงงาน D177."); break;
                default:
                    break;
            }
        }


    }
}
