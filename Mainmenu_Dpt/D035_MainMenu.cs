﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D035_MainMenu : Form
    {
        public D035_MainMenu()
        {
            InitializeComponent();
        }

        private void D035_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }

        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า.");
                    break;
                default:
                    break;
            }
        }
        //Gernaral
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SUPC_MNIO":
                    FormClass.ShowCheckFormNormal(new JOB.Bill.Bill_MNIO_OT("MNIO", "SUPC", "", "00010", "รับคืนสินค้า", "0"), "บิลนำออกไม่มีค่าใช้จ่าย SUPC [MNIO]."); break;
                case "ComMN_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00001", "ComMinimart", "0"), "ส่งซ่อมอุปกรณ์ [ComMinimart]."); break;
                case "ComService_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00002", "ComService", "0"), "ส่งซ่อมอุปกรณ์ [ComService]."); break;
                case "Main_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00003", "งานซ่อมทั่วไป", "0"), "ส่งซ่อมอุปกรณ์ [ช่างซ่อมบำรุง]."); break;
                case "Center_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00005", "งานสาขา", "0"), "ส่งซ่อมอุปกรณ์ [CenterShop]."); break;
                case "Center_D029":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("2", "00012", "ส่งซ่อม ลูกค้าคอมพิวเตอร์", "0"), "ส่งซ่อมสินค้าลูกค้า [แผนกคอมพิวเตอร์]."); break;
                case "Center_D100":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("3", "00013", "ส่งซ่อม ลูกค้ามือถือ", "0"), "ส่งซ่อมสินค้าลูกค้า [แผนกมือถือ]."); break;
                default:
                    break;
            }
        }
        //ตั้งค่า
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPC_Reason":
                    FormClass.ShowCheckForm(new FormShare.ConfigBranch_GenaralDetail("16", "เหตุผลการทำคืนสินค้า", "0", "0", "1", "R", "5"), "เหตุผลการทำคืนสินค้า."); break;

                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_MNPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "0"), "รายงานการรับคืนสินค้า."); break;
                case "Report_MNPCEditDT":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCEditDT("SUPC"), "แก้ไขรายการสินค้าคืน."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "1"), "รายงานการแก้ไขรายการสินค้าคืน."); break;
                case "Report_MNPCItems":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCItems("SUPC"), "คืนสินค้าตามบาร์โค๊ด."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "4"), "รายงานการคืนสินค้าตามบาร์โค๊ด."); break;
                case "Report_MNPCItems_Dept":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCItems_Dept("SUPC"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "3"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                case "Report_MNPCDetail":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCDetail("SUPC"), "รายงานการคืนสินค้า แบบละเอียด."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_FindData("SUPC", "1"), "รายงานการคืนสินค้า แบบละเอียด."); break;
                case "Report_MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNIO"), "เบิกสินค้าไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "D035_IDM":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("IDM"), "ทำลายสินค้าทิ้ง [IDM]."); break;
                case "MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG"), "ส่งของไม่มีทะเบียนคืน [MNRG]."); break;
                case "MNRB":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRB"), "ส่งของมีทะเบียนคืน [MNRB]."); break;
                case "MNRH":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRH"), "ส่งคืนเอกสาร [MNRH]."); break;
                case "SUPC_MNPO_CheckOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("11", "SUPC", "", "", ""), "เช็คสถานะสินค้าส่งมินิมาร์ท ระบบจัดสินค้าทั้งหมด."); break;
                case "OrderWeb":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("8", "SUPC", "", "", ""), "เช็คสถานะสินค้าส่งมินิมาร์ท ระบบจัดซื้อจัดสินค้าเอง."); break;
                case "SumPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "5"), "รายงานการคืนสินค้า สรุปรวมตามช่วงเวลา."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_CN_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_SHOPDocument("SUPC"), "คินสินค้า [MNPC]."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_Document_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

            switch (e.Node.Name)
            {
                case "MNPD":
                    FormClass.ShowCheckForm(new GeneralForm.ReceiveDocument.ReceivePD_Main(), "การรับเอกสาร [PD]."); break;
                case "MNRL":
                    FormClass.ShowCheckForm(new GeneralForm.ReceiveDocument.MNRL_Main(), "การส่งคืนเอกสาร [MNRL]."); break;
                default:
                    break;
            }
        }
    }
}
