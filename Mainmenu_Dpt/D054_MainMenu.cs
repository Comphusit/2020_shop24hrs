﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.JOB.Center;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.Logistic;
using PC_Shop24Hrs.GeneralForm.BoxRecive;
using PC_Shop24Hrs.GeneralForm.SendMoney;
using PC_Shop24Hrs.GeneralForm.Employee;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.MNEC;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D054_MainMenu : Form
    {
        public D054_MainMenu()
        {
            InitializeComponent();
        }

        private void D054_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "Asset_Detail":
                    FormClass.ShowNotCheckForm(new JOB.ASSET.Asset_Main("1"), "รายละเอียดสินทรัพย์ถาวร."); break;
                case "ItembarcodeDetail":
                    //FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                case "Camera_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("1", "MN", ""), "กล้องวงจรปิด."); break;
                case "CamOnline":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Show(), "กล้องออนไลน์ตามจุดต่างๆ."); break;
                default:
                    break;
            }
        }
        //JOB
        private void RadTreeView_TabJOB_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Camera_Check":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Check("1"), "ตรวจกล้องประจำวัน."); break;
                case "JOBMN_Main":
                    FormClass.ShowNotCheckForm(new JOBCenter_Main("00005", "JOB Center", "1", ""), "งานดูแลสาขา SHOP"); break;
                case "JOB_Head":
                    FormClass.ShowNotCheckForm(new JOBCenter_Main("00004", "JOB ตรวจสาขา", "1", ""), "งานเข้าตรวจสาขา SHOP"); break;

                case "JOB_General":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SHOP", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป [8560-8561]."); break;
                case "JOB_Car":
                    FormClass.ShowNotCheckForm(new JOB.Car.JOBCar_Main("SHOP", "00007", "งานซ่อมรถ-เครื่องจักร", "0", ""), "งานซ่อมรถยนต์-เครื่องจักร [3060,8580]."); break;
                case "JOB_Comservice":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00002", "ComService", "0", ""), "งานซ่อม ComService [8555]."); break;
                case "JOB_MN":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00001", "ComMinimart.", "0", ""), "งานซ่อม ComMinimart [8570]."); break;
                case "BillRB-RG-RH":
                    FormClass.ShowCheckForm(new JOB.Bill.BillMN_ForAddJOB("'D054'", "MN%", "", "00005"), "ข้อมูลการส่งซ่อม-ส่งคืนอุปกรณ์ต่างๆ [Center]."); break;
                case "BillRB-RG-RH_041":
                    FormClass.ShowCheckForm(new JOB.Bill.BillMN_ForAddJOB("'D041'", "MN%", "", "00003"), "ข้อมูลการส่งซ่อม-ส่งคืนอุปกรณ์ต่างๆ [ซ่อมทั่วไป]."); break;

                case "D054_CLIAM":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00005", "งานสาขา", "1"), "ส่งซ่อมอุปกรณ์ ภายในบริษัท."); break;
                case "D054_DESCARD":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("1", "00005", "งานสาขา", "1"), "นำทิ้งอุปกรณ์ ภายในบริษัท."); break;
                case "MNRH":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRH"), "ส่งคืนเอกสาร [MNRH]."); break;
                default:
                    break;
            }
        }
        //Config
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Branch_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("1"), "ข้อมูลสาขา"); break;
                case "ConfigBranch11":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("11", "Size Minimart", "0", "1", "1", "S", "4"), "ขนาดมินิมาร์ท"); break;
                case "ConfigBranch12":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("12", "ช่างนอกเข้าสาขา", "1", "1", "1", "0", "0"), "ช่างนอกเข้าสาขา."); break;
                case "ConfigBranch2":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("2", "ผู้จำหน่ายลงสินค้าสาขา", "1", "1", "0", "0", "0"), "กำหนดผู้จำหน่ายลงสินค้าสาขา."); break;
                case "JOB_GROUP_Center":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00005"), "ประเภท JOB Center."); break;
                case "JOB_GROUP_Type":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00004"), "หัวข้อตรวจสาขา."); break;
                case "ConfigBranch_Vendor":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("2"), "กำหนดสาขาที่ผู้จำหน่ายลงสินค้า."); break;
                case "D054_CamMian":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("21", "กล้องประจำจุด", "0", "1", "0", "C", "3"), "ตั้งค่ากล้องประจำจุด."); break;
                case "Config_ReturnDevice":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("22", "อุปกรณ์ส่งคืน", "0", "1", "1", "DV", "4"), "ข้อมูลอุปกรณ์ส่งคืน"); break;
                case "ImageAllDays_Group":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("26", "รูปถ่ายประจำวัน", "0", "1", "0", "A", "4"), "กำหนดกลุ่มรูปถ่ายประจำวัน"); break;
                case "ImageAllDays_Take":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("26"), "กำหนดถ่ายรูปประจำวัน"); break;
                case "Config_BillU":
                    FormClass.ShowNotCheckForm(new ComMinimart.Config_BillU("0"), "ตั้งค่ายอดการคืนเงิน บิลเปลี่ยน."); break;
                case "D054_ReportGroup":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("28", "รายงานประจำวัน", "0", "1", "0", "A", "3"), "ตั้งค่ากลุ่มรายงานประจำวัน."); break;
                case "D054_ReportGroupBch":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("28"), "กำหนดสาขารายงานประจำวัน."); break;
                case "Config_Reason29":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("29", "รูปท้ายรถและกุญแจรถ", "0", "1", "1", "R", "3"), "เหตุผลการตรวจสอบรูปท้ายรถและกุญแจรถ"); break;
                case "Config_Reason30":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("30", "เหตุผลการตรวจสอบ VDO ส่งซองเงิน", "0", "1", "1", "R", "3"), "เหตุผลการตรวจสอบ VDO ส่งซองเงิน"); break;
                case "Config_PC":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("31", "PC จัดสินค้า", "0", "1", "1", "PCA", "4"), "ข้อมูล PC จัดสินค้า"); break;
                case "TypeCN":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("34", "ประเภทของสินค้าคืน", "0", "1", "1", "", "7", "0"), "ประเภทของสินค้าคืน"); break;
                case "Zone":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfMain(""), "ตั้งค่าโซนสินค้าและชั้นวางสินค้า"); break;
                case "Lock_Emp":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfEmpl(), "ตั้งค่าพนักงานดูแลชั้นวางสินค้า"); break;
                case "Lock_Item":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfItems(), "ตั้งค่าสินค้าประจำชั้นวาง"); break;
                case "UploadCheckPrice":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_UpFile(PathImageClass.pPathCheckPriceMN), "Upload รูป แสดงผลเครื่องเช็คราคา มินิมาร์ท."); break;
                case "ItemAllZone":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("3"), "สินค้าทั้งหมดตามชั้นวาง."); break;
                case "configTeam":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("37", "ตั้งค่าทีมของส่วนงานทีมจัดร้าน", "0", "1", "1", "T", "3"), "ตั้งค่าทีมของส่วนงานทีมจัดร้าน"); break;
                case "configEmpT":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("38", "ตั้งค่าพนักงานทีมจัดร้าน", "1", "1", "0", "0", "0"), "ตั้งค่าพนักงานทีมจัดร้าน"); break;
                case "configEmpTeam":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("38"), "กำหนดทีมพนักงาน."); break;
                //case "SPC_RETAILINVENTBLOCKSCHED":
                //    FormClass.ShowNotCheckForm(new D054.Config_AlcoholSale("1"), "การตั้งค่าการขายสินค้าประเภทเหล้า-เบียร์."); break;
                case "OTP":
                    FormClass.ShowCheckFormNormal(new FormShare.OTPGenerator(), "OTP Generator [Void]"); break;
                case "ConfigCommissionRedim":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("39", "ตั้งค่าอัตราค่าคอมแลกแต้ม", "0", "1", "0", "", "5", "1"), "ตั้งค่าอัตราค่าคอมแลกแต้ม"); break;
                case "EmpCheckStock":
                    FormClass.ShowNotCheckForm(new ComMinimart.Config_BillU("2"), "ตั้งค่าพนักงานนับสต็อก/ลดราคาสินค้า"); break;
                case "Adverstise_Manage":
                    FormClass.ShowNotCheckForm(new GeneralForm.Advertise.Adverstise_Manage(), "FlashPlay."); break;
                case "SumSaleGroup":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("55", "ตั้งค่าการดึงยอดขายตามกลุ่ม", "0", "1", "0", "R", "4"), "การตั้งค่ากลุ่มการดึงยอดขาย."); break;
                case "SumSaleBch":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("55"), "กำหนดสาขาที่การดึงยอดขายตามกลุ่ม."); break;
                case "SumSaleBarcode":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("56", "ตั้งค่าบาร์โค้ดสำหรับการดึงยอดขายตามกลุ่ม", "1", "1", "1", "", ""), "การตั้งค่าบาร์โค้ดสำหรับการดึงยอดขายตามกลุ่ม."); break;
                case "SumSaleBarcodeFroup":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("56"), "กำหนดบาร์โค้ดสำหรับกลุ่มยอดขาย."); break;
                case "MNExchangeCoins":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("59"), "ตั้งค่าแลกเหรียญสาขา."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_JOBAdd_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "D054_scan1_30":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                case "SaveMoneyCash":
                    FormClass.ShowCheckFormNormal(new FormShare.InputDataGeneral_FromEmplID("0"), "บันทึกข้อมูล การนับเงินในลิ้นชักแคชเชียร์."); break;
                case "AddCustType0":
                    //FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomer("SHOP"), "สมัครสมาชิก ลูกค้าบุคคล"); break;
                    FormClass.ShowCheckFormNormal(new GeneralForm.Customers.AddCustomerPerson("SHOP", "CRET_MN"), "สมัครสมาชิก ลูกค้าบุคคล."); break;
                case "AddCustType1":
                    FormClass.ShowNotCheckFormNormal(new GeneralForm.Customers.AddCustomerOther("Company", "SUPC"), "สมัครสมาชิก ลูกค้าองค์กร"); break;
                case "AddCustType2":
                    FormClass.ShowNotCheckFormNormal(new GeneralForm.Customers.AddCustomerOther("", "SUPC"), "สมัครสมาชิก ลูกค้าต่างชาติ"); break;
                case "Cust_Detail":
                    FormClass.ShowCheckForm(new GeneralForm.Customers.Customer_Detail("SUPC"), "รายละเอียดลูกค้า"); break;
                case "promotion":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_Data("1"), "รายละเอียดข้อมูลโปรโมชั่น [ของแถม]"); break;
                case "MNSV_Main":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNSV.MNSV_Main(), "รายละเอียดงานทีมจัดร้าน"); break;
                case "ChangePoint":
                    FormClass.ShowCheckFormNormal(new GeneralForm.Customers.ChangePoint_BillSale("0"), "เปลี่ยนลูกค้าในบิลขาย [วันนี้เท่านั้น]"); break;
                case "CountStk":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Main("1"), "รายการนับสต็อก."); break;
                //case "DocumentCenter":
                //    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00017","1"), "คู่มือและการแก้ไขปัญหา [CenterShop]."); break;
                //case "DocumentFormCenter":
                //    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00018","1"), "แบบฟอร์มต่างๆ [CenterShop]."); break;
                case "ScanInSPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("D054"), "สแกนนิ้วเข้าสาขาใหญ่."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_Scan":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกดูแลระบบงานมินิมาร์ทสาขา]."); break;
                case "Report_CHECKCAM":
                    FormClass.ShowCheckForm(new ComMinimart.Camera.Camera_Report(), "รายงานการตรวจกล้องประจำจุด."); break;
                case "BillPallets":
                    FormClass.ShowCheckFormNormal(new BillPallets("1"), "บิลพาเลท."); break;
                case "SendMoney_Rpt":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_ReportSum("1"), "รายงานซองส่งเงินค้างส่ง."); break;
                case "ReportMNPMBYLO":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("4", "1"), "รายงานจำนวนซองส่งตามรอบ LO."); break;
                case "ReportStaPack":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_Report("5", "1"), "รายงานสถานะการรับซองส่งเงิน."); break;
                case "ReportSaleAgain":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("0"), "รายงานการพิมพ์บิลขายซ้ำ."); break;
                case "ReportVoid_Detail":
                    //FormClass.ShowNotCheckForm(new Report_TextCenterShop("1"), "รายงานการ Void สินค้าแบบละเอียด."); break;
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("1", "SUPC"), "รายงานการ Void สินค้าแบบละเอียด."); break;
                case "ReportVoid_Sum":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("2"), "รายงานการ Void สินค้าแบบสรุป."); break;
                case "SumVoidByEmp":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("20"), "รายงานการ Void สินค้าแบบสรุป/คน/วัน."); break;
                case "ReportQtyLess1":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("3"), "รายงานการเป็นทศนิยม."); break;
                case "ReportPriceChange":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("4"), "รายงานการเปลี่ยนเเปลงราคา."); break;
                case "Report_DetailProduct_ByDate":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("5"), "รายงานจำนวนสินค้า ขาย-เบิก-คืน."); break;
                case "Report_DetailProduct_TO":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("6"), "รายงานจำนวนสินค้าเบิก."); break;
                case "Report_DetailProduct_CN":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("7"), "รายงานจำนวนสินค้าคืน."); break;
                case "Report_DetailProduct_Sale":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("8"), "รายงานจำนวนสินค้าขาย."); break;
                case "ReportSumGrandBYBch":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("9"), "รายงานมูลค่า เบิก-ขาย-คืน-สต็อก ตามสาขา"); break;
                case "ReportSumGrandBYDate":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("10"), "รายงานมูลค่า เบิก-ขาย-คืน-สต็อก ตามวันที่."); break;
                case "ReportBillU":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("0", "SUPC"), "รายงานบิลเปลี่ยน."); break;
                case "Report_MNPCItems_Dept":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCItems_Dept("SUPC"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "3"), "รายงานการคืนสินค้า ตามแผนกจัดซื้อ."); break;
                case "ReportSendLO":
                    FormClass.ShowNotCheckForm(new D204_Report("2", "1"), "รายงานการส่งคืนอุปกรณ์ตาม LO."); break;
                case "ReportNotSend":
                    FormClass.ShowNotCheckForm(new D204_Report("3", "0"), "รายงานการไม่เก็บ-ไม่มี อุปกรณ์."); break;
                case "Bill_Techout":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Bill_Techout("SUPC", "1"), "รายงานช่างนอกเข้าสาขา."); break;
                case "SUPC_MNPO_CheckOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("11", "SUPC", "", "", ""), "เช็คสถานะออเดอร์ ระบบจัดสินค้าทั้งหมด."); break;
                case "OrderWeb":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("8", "SUPC", "", "", ""), "เช็คสถานะออเดอร์ ระบบจัดซื้อจัดสินค้าเอง."); break;
                case "Qty_Web":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("1", "SUPC", "", "", ""), "จำนวนออเดอร์ผ่าน WEB [ตามบาร์โค้ด]."); break;
                case "OrderPic":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("4", "SUPC", "", "", ""), "จำนวนออเดอร์ผ่าน WEB [ผ่านรูป]."); break;
                case "MNPO_WEBSum":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.MNPO_Report("0", "SUPC"), "ออเดอร์สั่งสินค้าผ่านมือถือ [สรุป]."); break;
                case "MNPO_WEBDetail":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.MNPO_Report("1", "SUPC"), "ออเดอร์สั่งสินค้าผ่านมือถือ [ละเอียด]."); break;
                case "D054_Report_Employee0":
                    FormClass.ShowNotCheckForm(new Employee_Report("0", "SUPC", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [สรุป]."); break;
                case "D054_Report_Employee1":
                    FormClass.ShowNotCheckForm(new Employee_Report("1", "SUPC", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [ละเอียด]."); break;
                case "D054_Report_Employee2":
                    FormClass.ShowNotCheckForm(new Employee_Report("2", "SUPC", "", DateTime.Now), "รายงาน ตรวจสอบพนักงาน."); break;
                case "D054_Report_Employee3":
                    FormClass.ShowNotCheckForm(new Employee_Report("3", "SUPC", "", DateTime.Now), "รายงาน มาตรฐานพนักงาน."); break;
                case "MNHS":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNHS.MNHS_Report("0", "SUPC"), "รายงานการซื้อสินค้าระบบบิลเครดิตมินิมาร์ท [MNHS]."); break;

                case "VDO_CheckEmp_MN_0":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("0", "SUPC"), "รายงานการตรวจตัว สาขา ระบุวัน."); break;
                case "VDO_CheckEmp_MN_1":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("1", "SUPC"), "รายงานการตรวจตัว สาขา ตามช่วงเวลา."); break;
                case "VDO_CheckEmp_D054":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("2", "SUPC"), "รายงานการตรวจตัว ทีมจัดร้าน."); break;
                case "VDO_CheckEmp_D088":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("3", "SUPC"), "รายงานการตรวจตัว ตรวจสินค้าส่วนหน้า."); break;
                case "VDO_CheckEmp_OTH":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("4", "SUPC"), "รายงานการตรวจตัว แผนกต่างๆ."); break;
                case "VDO_PC":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("7", "SUPC"), "รายงานการตรวจตัว PC จัดสินค้า."); break;
                case "VDO_CheckBill":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("5", "SUPC"), "รายงานการตรวจบิล."); break;
                case "VDO_CheckCar":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("6", "SUPC"), "รายงานการตรวจก่อนกลับบ้าน ."); break;

                case "D054_Report_Employee4":
                    FormClass.ShowNotCheckForm(new Employee_Report("4", "SUPC", "", DateTime.Now), "รายงาน การลงเวลาเข้าพัก ."); break;

                case "ReportType28_D":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("11"), "รายงานประจำวัน ข้อมูลละเอียด."); break;
                case "ReportType28_M":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("12"), "รายงานประจำวัน ข้อมูลสรุป."); break;
                case "ReportText15":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("15"), "รายงานยอดนับเงินในลิ้นชักแคชเชียร์."); break;
                case "Report_ARCashReason":
                    //FormClass.ShowNotCheckForm(new D153.Report_ARCashReason(), "รายงานส่งเงิน."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash("1"), "รายงานยอดส่งเงิน."); break;
                case "Report_ARAtBranch_Lak":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash_Report("LackOfMoney", "0"), "รายงานเงินขาดตามแคชเชียร์."); break;
                case "Report_ARAtBranch_Deduct":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.ARMoneyCash_Report("DeductedMoney", "0"), "รายงานยอดหักตามแคชเชียร์."); break;
                case "StockPig":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("16"), "รายงานจำนวนรายการส่งสต็อก หมู-ไก่/ผัก/ผลไม้."); break;
                case "Report_Customers":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Report_Customers("2"), "รายงานสมัครสมาชิก"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("8", ""), "รายงานสมัครสมาชิก"); break;
                case "SPC_TransferBlockedItemTable":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("0"), "รายงานสินค้าหยุดส่งมินิมาร์ท [ห้ามสาขาขาย]"); break;
                case "SPC_RETAILPLANORDERITEMBLOCKED":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("1"), "รายงานหยุดแผนใบสั่ง-สินค้า [ห้ามสาขาสั่ง]"); break;
                case "MNBC":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("2"), "รายงานการซื้อสินค้าของลูกค้ามินิมาร์ท"); break;
                case "MNPI17":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "17", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง [ครัวชญานันทน์]."); break;
                case "MNPI19":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "19", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง [ครัวมาลินี]."); break;
                case "MNPI20":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "20", "RETAILAREA"), "ข้อมูลการรับ-ส่งข้าวกล่อง [ครัวศรัณยา]."); break;
                case "MNPI25":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "25", "RETAILAREA"), "ข้อมูลการรับ-ส่งเบเกอรี่."); break;
                case "Recive_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SUPC", "0"), "ค่าคอมของพนักงานลงสินค้าต่างจังหวัด [สรุป]."); break;
                case "Recive_Process":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownProcess("SUPC", "0"), "ค่าคอมของพนักงานลงสินค้าต่างจังหวัด [ประมวลผล]."); break;
                case "SUPC-SEND-RPT":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.ControlProduct.ControlProduct_Main("1", "SUPC", "1"), "รายงานรายละเอียดการรับสินค้าควบคุม [ส่งจาก SUPC]."); break;
                case "FreeItem":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_Report("0", "0"), "รายงานรายการสินค้าแถมมินิมาร์ท."); break;
                case "Pro24":
                    FormClass.ShowNotCheckForm(new GeneralForm.Promotion.Promotion_Report("1", "0"), "รายงานการเปิด-ปิด โปรโมชั่น [ของแถม] ผ่านระบบ SHOP24HRS."); break;
                case "Rpt_Tel":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("0", "0"), "รายงานรายละเอียดการโทรหาลูกค้า."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("0", "0"), "รายงานรายละเอียดการโทรหาลูกค้า."); break;
                case "OrderSum":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("1", "0"), "รายงานรายละเอียดการสั่งของลูกค้า."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("1", "0"), "รายงานรายละเอียดการสั่งของลูกค้า."); break;
                case "TelEmp":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("2", "0"), "รายงานรายละเอียดการโทรหาลูกค้าของพนักงาน."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("2", "0"), "รายงานรายละเอียดการโทรหาลูกค้าของพนักงาน."); break;
                case "TelOrderEmp":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("3", "0"), "รายงานรายละเอียดการโทรต่อการสั่งของลูกค้า."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("3", "0"), "รายงานรายละเอียดการโทรต่อการสั่งของลูกค้า."); break;
                case "MNFR_Detail":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("4", "0"), "รายงานรายละเอียดการแลกแต้มของลูกค้ามินิมาร์ท."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("4", "0"), "รายงานรายละเอียดการแลกแต้มของลูกค้ามินิมาร์ท."); break;
                case "MNFR_SUM":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("5", "0"), "รายงานจำนวนแต้มที่แลกไปทั้งหมดของมินิมาร์ท."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("5", "0"), "รายงานจำนวนแต้มที่แลกไปทั้งหมดของมินิมาร์ท."); break;
                case "MNOR_SUM":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_CenterReport("6", "0"), "รายงานจำนวนรับออเดอร์ลูกค้าของมินิมาร์ท."); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Customers.Customer_Report("6", "0"), "รายงานจำนวนรับออเดอร์ลูกค้าของมินิมาร์ท."); break;
                case "Commission_OrderRecive":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.OrderCustomers.CommissionOrderCust(), "รายงานค่าคอมออร์เดอร์ลูกค้า."); break;
                case "RedempCommission":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.ReportRedeem.RedempCommission("SUPC", "0"), "รายงานค่าคอมของการแลกแต้มสินค้ามินิมาร์ท."); break;
                case "RedempCommission_Detail":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Commission.ReportRedeem.ReportRedempCommission("SUPC", "2"), "รายละเอียดตามสาขา"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.ReportRedeem.RedempCommission("SUPC", "2"), "รายละเอียดตามสาขา"); break;
                //case "RedempCommission_Report":
                //  FormClass.ShowNotCheckForm(new GeneralForm.Commission.ReportRedeem.ReportRedempCommission("SUPC", "1"), "รายงานค่าคอมของการแลกแต้มสินค้ามินิมาร์ทย้อนหลัง."); break;
                case "RptText17":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("17"), "รายงานอัตราเปรียบเทียบจำนวนการขายสินค้า ย้อนหลัง 3 เดือน ตามจัดซื้อ."); break;
                case "RptText18":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("18"), "จำนวนการขายสินค้า ย้อนหลัง 3 เดือน ตามกลุ่มสินค้า [ประเมินการขาย]."); break;
                case "MNPI32":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "32", "RETAILAREA"), "ข้อมูลการรับ-ส่งอาหารกล่อง [จัดซื้อหมู-ไก่]."); break;
                case "MNPI33":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferDelivary_Report("SUPC", "33", "RETAILAREA"), "ข้อมูลการรับ-ส่งอาหารกล่อง [จัดซื้ออาหารทะเล]."); break;
                case "rpt_MNOG":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOG_PD.MNOG_MNPD_ReciveOrder("MNOG", "2"), "เช็คออเดอร์ลูกค้าของสด [MNOG]."); break;
                case "rpt_MNPD":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNOG_PD.MNOG_MNPD_ReciveOrder("MNPD", "2"), "เช็คออเดอร์ลูกค้าของทั่วไป [MNPD]."); break;
                case "Report_Use":
                    FormClass.ShowNotCheckForm(new Report_MenuUse(), "การใช้งานโปรแกรมเมนูต่างๆ."); break;
                case "StockFruit":
                    FormClass.ShowNotCheckForm(new GeneralForm.SendStock.ReportSendStock("SUPC"), "รายงานส่งสต๊อกผลไม้มินิมาร์ท"); break;
                case "cnType":
                    //FormClass.ShowNotCheckForm(new GeneralForm.MNPC.Report_MNPCType("SUPC"), "รายงานคืนสินค้าตามประเภทการคืน"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPC.MNPC_Report("SUPC", "2"), "รายงานคืนสินค้าตามประเภทการคืน."); break;
                case "ShelfReportSale":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfReportSale("SUPC"), "ยอดขายสินค้าตามชั้นวาง."); break;
                case "MNSV_Report_SUMALL":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNSV.MNSV_Report("0"), "ยอดรวมค่าคอมของทีมจัดร้าน."); break;
                case "CommissionEmpShelf":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfReportEmp("SUPC"), "รายงานค่าคอมของพนักงานดูแลสินค้า."); break;
                case "PrtChangeCstBillsale":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("19"), "รายการการปรับเปลี่ยนลูกค้าในบิลขาย."); break;
                case "MNCS_ReportItemByJournalID":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("0", "1"), "รายงานตรวจนับสต๊อกสินค้า."); break;
                case "SPCN_SumInventCountingMNByVoucherByDate_FindJournalId":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("1", "1"), "รายงานเปรียบเทียบยอดขาย/จำนวนสต็อกคงเหลือ."); break;
                case "CountStkByEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("2", "1"), "รายงานจำนวนรายการเช็คสต๊อก."); break;
                case "CountStkByEmpHour":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("3", "1"), "รายงานจำนวนรายการเช็คสต๊อก[รายชั่วโมง]."); break;
                case "ItemHistoryCheckStock":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("4", "1"), "รายงานประวัติการเช็คสต็อกสินค้า [หน่วยย่อย]."); break;
                case "CountStkByEmpYear":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("5", "1"), "รายงานจำนวนรายการเช็คสต๊อก[รายปี]."); break;
                case "StockHis":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("6", "1"), "รายงานประวัติการเช็คสต็อกสินค้า [สรุป]."); break;
                case "CountLogin":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("6", "0"), "สรุปการ Login เข้าใช้งานเครื่อง."); break;
                case "TechRptVDO":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("5", "0"), "รายงาน ช่างเข้าซ่อมสาขา [ตรวจรถ]."); break;
                case "TechRptImage":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("6", "0"), "รายงาน ช่างเข้าซ่อมสาขา [ตรวจตัว]."); break;
                case "ComCheckStock":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("7", "1"), "ค่าคอมของพนักงานเเช็คสต๊อก."); break;
                case "JOB":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("6", "SUPC"), "รายงาน JOB เข้าตรวจสาขา."); break;
                case "DetailBill_POSRetail":
                    FormClass.ShowCheckForm(new ComMinimart.DetailBill_POSRetail(), "รายละเอียดบิลขาย."); break;
                case "D054_Report_Employee5":
                    FormClass.ShowNotCheckForm(new Employee_Report("5", "SUPC", "", DateTime.Now), "รายงานการสแกนนิ้วเข้าสาขาใหญ่."); break;
                case "barcodeDiscount":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("6"), "สินค้าที่ขายลดราคา."); break;
                case "SumDiscount":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("8"), "รายงานสินค้าลดราคาตามยอด/จำนวน."); break;
                case "barcodeDiscount164":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("7"), "สินค้าที่ขายตามบาร์โค้ดทั้งหมด."); break;
                case "D054_Report_EmployeePartTime":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("8", "SUPC"), "รายงานการตรวจตัว PartTime."); break;
                case "D054_Report_EmployeeOT":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("9", "SUPC"), "รายงานการตรวจตัว OT วันหยุด."); break;
                case "Report_EmployeePartTime":
                    FormClass.ShowNotCheckForm(new Employee_Report("6", "SUPC", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [PartTime]."); break;
                case "Report_EmployeeOT":
                    FormClass.ShowNotCheckForm(new Employee_Report("7", "SUPC", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [OT วันหยุด]."); break;
                case "CommissionPartTime":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownProcess("SUPC", "2"), "ค่าคอมแคชเชียร์ PartTime [ประมวลผล]."); break;
                case "PartTime_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SUPC", "1"), "ค่าคอมของพนักงานแคชเชียร์ PartTime."); break;
                case "OT_Report":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SUPC", "2"), "ค่าคอมของพนักงาน OT วันหยุด."); break;
                case "SumDiscount_ByBarcode":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("9"), "รายงานสินค้าลดราคาตามบาร์โค๊ด."); break;
                case "ItemSaleFixGroup":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("10"), "รายงานยอดขายสินค้าแสดงตามวันที่ [กลุ่มที่ตั้งไว้/บาร์โค้ด/รหัส]."); break;
                case "CommissionLocation":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SUPC", "3"), "ค่าเบี้ยเลี้ยง+ค่าที่พัก."); break;
                case "CommissionCashierAddON":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SUPC", "4"), "ค่าคอมของแคชเชียร์มินิมาร์ทรวม [OnTop]."); break;
                case "MNRR_Detail":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("21"), "รายงานการละเอียดการเบิกสินค้าของมินิมาร์ท [ละเอียด]."); break;
                case "MNRR_SUM":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRR"), "รายงานการละเอียดการเบิกสินค้าของมินิมาร์ท [สรุป]."); break;
                case "D054_Report_Employee6":
                    if ((SystemClass.SystemUserID_M == "M0904073")
                         || (SystemClass.SystemUserID_M == "M0804002") || (SystemClass.SystemUserID_M == "M0211064") || (SystemClass.SystemUserID_M == "M1308066")
                         || (SystemClass.SystemUserID_M == "M0604049") || (SystemClass.SystemUserID_M == "M0111029") || (SystemClass.SystemUserID_M == "M2203025"))
                        FormClass.ShowNotCheckForm(new Employee_Report("8", "SUPC", "", DateTime.Now), "รายงาน การประเมินพนักงาน.");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะผู้ที่มีสิทธิ์เท่านั้น {Environment.NewLine}สำหรับ {SystemClass.SystemUserID_M}  {SystemClass.SystemUserName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "ImportEvaluate":
                    if ((SystemClass.SystemUserID_M == "M0904073")
                       || (SystemClass.SystemUserID_M == "M0804002") || (SystemClass.SystemUserID_M == "M0211064") || (SystemClass.SystemUserID_M == "M1308066")
                       || (SystemClass.SystemUserID_M == "M0604049") || (SystemClass.SystemUserID_M == "M0111029") || (SystemClass.SystemUserID_M == "M2203025"))
                        FormClass.ShowNotCheckForm(new Employee_EvaluateImport(), "Import File ประเมินพนักงาน.");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะผู้ที่มีสิทธิ์เท่านั้น {Environment.NewLine}สำหรับ {SystemClass.SystemUserID_M}  {SystemClass.SystemUserName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                case "PAYScanDptSCN":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("D054_1"), "รายงานค่าใช้จ่ายในการมาสาขาใหญ่."); break;
                case "ReportScanSUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("16", "SUPC", "", "", ""), "รายงานการจัดสินค้าสาขาใหญ่."); break;
                default:
                    break;
            }
        }
        //Image
        private void RadTreeView_Image_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "FrontVDO":
                    FormClass.ShowNotCheckForm(new Report_Recheck("1", "1"), "รายงานการตรวจหน้าร้าน [VDO]."); break;
                case "ReportMNPMVDO":
                    FormClass.ShowNotCheckForm(new Report_Recheck("0", "1"), "รายงานการส่งซองเงิน [VDO]."); break;
                case "TransferMNPM":
                    FormClass.ShowNotCheckForm(new Report_Recheck("2", "1"), "รายงานการโอนเงินเข้าบัญชี [ส่งซองเงิน]."); break;
                case "ReportSlipLOImage":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImage("1"), "รายงานรูปปิดท้ายและกุญแจรถ 1 ประตู."); break;
                case "ReportSlipLOImageCar":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.Report_TagImageForCar("1", ""), "รายงานรูปปิดท้ายและกุญแจรถ > 1 ประตู."); break;
                case "ImageAllDays_Date":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "0", "1"), "รูปถ่ายประจำวัน ระบุวันที่ ทุกสาขา."); break;
                case "ImageAllDays_M":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "1", "1"), "รูปถ่ายประจำวัน ระบุสาขา ตามช่วงวันที่."); break;
                case "ImageAllDays_C":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "2", "1"), "รูปถ่ายประจำวัน จำนวนรูป ช่วงวันที่ ทุกสาขา."); break;
                case "ImageAllDays_B":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "3", "1"), "รูปถ่ายประจำวัน ตามช่วงวันที่ ทุกสาขา."); break;
                case "ReportText_13":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("13"), "รายงานรูปถ่ายจัดสินค้า [สรุป]."); break;
                case "ReportText_14":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("14"), "รายงานรูปถ่ายจัดสินค้า [ละเอียด]."); break;
                case "FarmHouse":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_BranchRecive("SUPC", "1"), "รายละเอียดการรับสินค้า ฟาร์มเฮ้าส์."); break;
                case "Vender":
                    //FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("SUPC", "1"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.Vender.ImageVender_Show("1"), "รายละเอียดการรับสินค้าตามผู้จำหน่าย."); break;
                case "MP":
                    FormClass.ShowCheckForm(new GeneralForm.ImageShow.MP.MP_Main("SUPC", "1"), "รายละเอียดการรับสินค้าเซลล์ส่ง [MP]."); break;
                case "SUPC-SEND":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.ControlProduct.ControlProduct_Main("0", "SUPC", "1"), "รายละเอียดการรับสินค้าควบคุม [ส่งจาก SUPC]."); break;
                case "EmplSH":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("2", "SUPC"), "รายงานพนักงานจัดสินค้าตามชั้นวาง."); break;
                case "EmpSorting":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("3", "SUPC"), "รายงานการจัดเรียงสินค้าตามพนักงาน."); break;
                case "TeamAB":
                    FormClass.ShowNotCheckForm(new Report_ImageCenterShop("4", "SUPC"), "รายงานทีมจัดร้านเข้าสาขา."); break;
                case "MA":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("7", "0"), "รับสินค้าที่มินิมาร์ท [MA]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Bch_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNOR":
                    FormClass.ShowCheckForm(new GeneralForm.MNOR.MNOR_ORDER(), "สั่งสินค้าแผนกให้สาขา [MNOR]."); break;
                case "MNPO_SUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.MNPO_Order("SUPC", "CenterShop"), "สั่งสินค้าให้สาขา [MNPO]."); break;
                case "MNPD_Order":
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNPD_Order("CenterShop"), "สั่งสินค้าของทั่วไปให้ลูกค้า [MNPD]"); break;
                case "MNOG_Order":
                    FormClass.ShowCheckForm(new GeneralForm.MNOG_PD.MNOG_Order("CenterShop"), "สั่งสินค้าของสดให้ลูกค้า [MNOG]"); break;
                case "MNPI18":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPI.MNPI_ReciveInvent("18", "น้ำแข็งส่งสาขา", "0"), "บันทึกรับน้ำแข็งเฉพาะสาขาภายในจังหวัดภูเก็ต [MNPI]."); break;
                case "MNPI19":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPI.MNPI_ReciveInvent("19", "อาหารกล่อง", "0"), "บันทึกรับอาหารกล่องประจำวัน [MNPI]."); break;
                case "DocumentCenter":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00017", "1"), "คู่มือและการแก้ไขปัญหา [CenterShop]."); break;
                case "DocumentFormCenter":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00018", "1"), "แบบฟอร์มต่างๆ [CenterShop]."); break;
                case "DocumentProduct":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00019", "1"), "แบบฟอร์มต่างๆ [Product Planning]."); break;
                case "DocumentEmplData":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00020", "1"), "แบบฟอร์มต่างๆ [ดูแลข้อมูลพนักงาน]."); break;
                case "DocumentRule":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00021", "1"), "แบบฟอร์มต่างๆแบบฟอร์มต่างๆ [ดูแลกฎระเบียบ]."); break;
                case "CoinsExchange":
                    FormClass.ShowNotCheckForm(new MNEC_Main("22"), "แลกเหรียญสาขา."); break;
                default: break;
            }
        }

        private void RadTreeView_Manual_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "DocumentCenter":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00017", "1"), "คู่มือและการแก้ไขปัญหา [CenterShop]."); break;
                case "DocumentFormCenter":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00018", "1"), "แบบฟอร์มต่างๆ [CenterShop]."); break;
                case "DocumentProduct":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00019", "1"), "แบบฟอร์มต่างๆ [Product Planning]."); break;
                case "DocumentEmplData":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00020", "1"), "แบบฟอร์มต่างๆ [ดูแลข้อมูลพนักงาน]."); break;
                case "DocumentRule":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00021", "1"), "แบบฟอร์มต่างๆแบบฟอร์มต่างๆ [ดูแลกฎระเบียบ]."); break;
                default:
                    break;
            }
        }
    }
}
