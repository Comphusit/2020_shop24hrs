﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.JOB.Com;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.Employee;
using PC_Shop24Hrs.GeneralForm.Report;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D000_MainMenu : Form
    {

        public D000_MainMenu()
        {
            InitializeComponent();
        }

        private void D000_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("0"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                case "programeUse":
                    FormClass.ShowNotCheckForm(new PhoneBook("2"), "ข้อมูลโปรแกรมภายในบริษัท."); break;
                case "server":
                    FormClass.ShowNotCheckForm(new ComMinimart.Config_Server_Main(), "รายละเอียดการใช้งาน Server."); break;
                default:
                    break;
            }
        }
        //JOB
        private void RadTreeView_TabJOB_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Document":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00015", "1"), "คู่มือการแก้ไขปัญหา."); break;
                case "DocumentSHOP":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00016", "1"), "คู่มือการแก้ไขปัญหา."); break;
                case "Camera_Check":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Check("0"), "ตรวจกล้องประจำวัน."); break;
                case "JOBMN_Main":
                    FormClass.ShowNotCheckForm(new JOBCOM_Main("SHOP", "00001", "ComMinimart.", "1", ""), "ComMinimart SHOP"); break;
                case "JOBMN_Main_SUPC":
                    FormClass.ShowNotCheckForm(new JOBCOM_Main("SUPC", "00001", "ComMinimart.", "1", ""), "ComMinimart SUPC"); break;
                case "Asset_FAL":
                    FormClass.ShowCheckFormNormal(new JOB.Bill.BillAXAddJOB("", "", "", "", "", "", "", "", "", ""), "บิล I/F/FAL."); break;
                case "Asset_Detail":
                    FormClass.ShowNotCheckForm(new JOB.ASSET.Asset_Main("0"), "รายละเอียดสินทรัพย์ถาวร."); break;
                case "Asset_Adjust":
                    FormClass.ShowCheckFormNormal(new JOB.ASSET.AdjustAsset("", "", "", "", "", "", ""), "ปรับทะเบียนสินทรัพย์."); break;
                case "ClaimMain":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00001", "ComMinimart", "1"), "ส่งซ่อมอุปกรณ์ ภายในบริษัท."); break;
                case "DiscardMain":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("1", "00001", "ComMinimart", "1"), "นำทิ้งอุปกรณ์ ภายในบริษัท."); break;
                case "TagDetail":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.TagDetail("0", "1"), "รายละเอียด Tag [ส่งของ]."); break;
                case "TagDetailCar":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.TagDetail("1", "1"), "รายละเอียด Tag [ฝาถังน้ำมัน]."); break;
                case "TagSumAll":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.TagSumAll(), "สรุป Tag [กด F5 >> Refresh ข้อมูล]."); break;
                case "MNRB_MNRG":
                    FormClass.ShowCheckForm(new JOB.Bill.BillMN_ForAddJOB("'D156','D179'", "MN%", "", "00001"), "ข้อมูลการส่งซ่อม-ส่งคืนอุปกรณ์ต่างๆ."); break;
                case "EmpList_Com":
                    FormClass.ShowCheckForm(new FormShare.EmpListInCorrect("", "COM-MN", "D179", ""), "บันทึกรายการหักอุปกรณ์ต่างๆ."); break;
                case "MNRS":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SHOP", "COM-MN"), "ทำรายการเบิกสินค้า MNRS [MN]."); break;
                case "MNRS_D":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SUPC", "COM-MN"), "ทำรายการเบิกสินค้า MNRS [D]."); break;
                case "ChangeCstBill":
                    FormClass.ShowCheckFormNormal(new GeneralForm.Customers.ChangePoint_BillSale("1"), "เปลี่ยนลูกค้าในบิลขาย"); break;
                case "Coupon":
                    FormClass.ShowCheckForm(new GeneralForm.LineCoupon.LineCoupon_Main(), "การออกคูปองและส่วนลด."); break;
                case "ScanDpt":
                    //  FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.ScanDpt("0"), "สแกนนิ้วเข้าแผนก.");
                    break;
                case "D054_Report_Employee1":
                    FormClass.ShowNotCheckForm(new Employee_Report("1", "SUPC", "", DateTime.Now), "พนักงานลงเวลาเข้างาน [มินิมาร์ท]."); break;

                default:
                    break;
            }
        }
        //Config
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PROVINCE":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("50", "จังหวัดที่เปิดสาขา", "0", "0", "1", "A", "3"), "จังหวัดที่เปิดสาขา."); break;
                case "Branch_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("0"), "ข้อมูลสาขา."); break;
                case "AddProgram":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("15", "ตั้งค่าโปรแกรมใช้งาน", "0", "0", "1", "A", "3"), "ตั้งค่าโปรแกรมใช้งาน."); break;
                case "Menu_manager":
                    FormClass.ShowCheckForm(new ComMinimart.Manage.Menu_Manager(), "จัดการเมนู."); break;
                case "DepPermission_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.DepPermission_Main("SUPC"), "กำหนดแผนกเพื่อใช้งานโปรแกรม SHOP24HRS_PC."); break;
                case "DepPermission_WEB":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.DepPermission_Main("WEB"), "กำหนดแผนกเพื่อใช้งานโปรแกรม SHOP24HRS_WEB."); break;
                case "Machine_Manager":
                    FormClass.ShowCheckForm(new ComMinimart.Manage.Machine_Manager("0"), "สิทธิ์เครื่อง."); break;
                case "Empl_Manager":
                    FormClass.ShowCheckForm(new ComMinimart.Manage.Empl_Manager(), "สิทธิ์พนักงาน."); break;
                case "Config_BillU":
                    FormClass.ShowCheckForm(new ComMinimart.Config_BillU("0"), "ตั้งค่ายอดการคืนเงิน บิลเปลี่ยน."); break;
                case "RegisterTag":
                    FormClass.ShowCheckFormNormal(new ComMinimart.Tag.TagRegister(), "TagRegister."); break;
                case "JOB_GROUPSUB":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00001"), "ประเภทงาน."); break;
                case "Camera_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("0", "", ""), "กล้องทั้งหมด."); break;
                case "Branch_Wireless":
                    FormClass.ShowCheckForm(new ComMinimart.Manage.Branch_Wireless(), "Wireless."); break;
                case "FixDate":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("27", "วันถ่ายรูป", "0", "1", "1", "", "5"), "กำหนดวันถ่ายรูป."); break;
                case "TypeCN":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("34", "ประเภทของสินค้าคืน", "0", "1", "1", "", "7"), "ประเภทของสินค้าคืน."); break;
                case "CountRevice":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("36", "เครื่องทำบิลที่ต้องนับรายการ", "1", "1", "1", "", "7"), "เครื่องทำบิลที่ต้องนับรายการ."); break;
                case "QR":
                    FormClass.ShowCheckForm(new ComMinimart.Config_BillU("1"), "ตั้งค่าปิด-เปิด QR CODE."); break;
                case "CAMIP":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_IP("41", "กล้องก่อสร้าง", "CAM", "1"), "กล้องช่างก่อสร้าง."); break;
                case "CAMMK":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_IP("42", "กล้องแมคโคร", "CAMMK", "1"), "กล้องแมคโคร."); break;
                case "CAMBKK":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_IP("58", "กล้องขนส่ง1999", "CAMBKK", "1"), "กล้องขนส่ง1999."); break;
                case "Camera_Main_Online":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Show(), "กล้องออนไลน์ตามจุดต่างๆ."); break;
                case "Adverstise_Manage":
                    FormClass.ShowNotCheckForm(new GeneralForm.Advertise.Adverstise_Manage(), "FlashPlay."); break;
                case "ConfigDptMNOR":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("47", "ตั้งค่าแผนกเปิดสั่งระบบ MNOR", "1", "1", "1", "0", "0"), "การตั้งค่าแผนกเปิดสั่งระบบ MNOR."); break;
                case "Sta_ProductContainer_1":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("53", "สถานะจัดสินค้า ProductContainer", "1", "1", "1", "", ""), "สถานะจัดสินค้า ProductContainer."); break;
                case "Sta_ProductContainer_2":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("54", "สถานะออเดอร์ ProductContainer", "1", "1", "1", "", ""), "สถานะออเดอร์ ProductContainer."); break;
                case "DptConfigPO":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("57", "กำหนดผู้จำหน่ายเพื่อผูก PO [รับสินค้าเข้าคลัง]", "1", "1", "1", "", ""), "กำหนดผู้จำหน่ายเพื่อผูก PO [รับสินค้าเข้าคลัง]."); break;
                case "DptFatory":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("60", "กำหนดผู้จำหน่ายสินค้าโรงงาน [รับสินค้าเข้าคลัง]", "1", "1", "1", "", ""), "กำหนดผู้จำหน่ายสินค้าโรงงาน [รับสินค้าเข้าคลัง]."); break;
                case "CoinsSetting":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("59", "แลกเหรียญ", "1", "1", "1", "", ""), "ตั้งค่าเหรียญ."); break;
                case "HeadEmp":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("62", "หัวข้อประเมินพนักงาน", "0", "1", "1", "H", "3"), "หัวข้อประเมินพนักงาน."); break;
                case "HeadEmpImport":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("63", "หัวข้อประเมินนำเข้า File Excel", "0", "1", "1", "HH", "3"), "หัวข้อประเมินนำเข้า File Excel."); break;
                case "ReciveFactory":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("65", "ตั้งค่าแผนกรับสินค้าจากโรงงาน", "1", "1", "1", "0", "0"), "การตั้งค่าแผนกที่รับสินค้าจากโรงงาน."); break;
                case "ConfigDptMNOI":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("64", "ตั้งค่าแผนกเปิดสั่งระบบ MNOI", "1", "1", "1", "0", "0"), "การตั้งค่าแผนกเปิดสั่งระบบ MNOI."); break;
                case "D_Wight":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("67"), "ตั้งค่าแผนกจัดซื้อที่ต้องเก็บน้ำหนัก/ปริมาตร."); break;
                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "DetailBill_POSRetail":
                    FormClass.ShowCheckForm(new ComMinimart.DetailBill_POSRetail(), "รายละเอียดบิลขาย."); break;
                case "DetailFigerprint":
                    FormClass.ShowCheckForm(new ComMinimart.DetailFingerprint(), "ตรวจสอบลายนิ้วมือ."); break;
                //case "Tag_SHOP_EMPLOYEE_ListInCorrect":
                //FormClass.ShowNotCheckForm(new FormShare.Report_EMP_ListInCorrect("D179", "Tag"), "สรุปรายการหัก Tag."); break;
                case "Tag_SHOP_EMPLOYEE_ListInCorrect_ComMN":
                    //FormClass.ShowNotCheckForm(new FormShare.Report_EMP_ListInCorrect("D179", "COM-MN"), "สรุปรายการหักอุปกรณ์ต่างๆ."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("5", "D179"), "สรุปรายการหักอุปกรณ์ต่างๆ."); break;
                case "PDA_Count":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.QuantityPDA), "จำนวนเครื่องต่อสาขา."); break;
                case "PDA_Login":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.Login), "การเข้าใช้งานเครื่อง."); break;
                case "PDA_Use":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.ReportLogin), "สรุปการใช้งาน."); break;
                case "PDA_File":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.FileinPDA), "File ค้าง."); break;
                case "PDA_Menu":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.LogShop24Hrs_SUPC), "การเข้าใช้งานเมนู."); break;
                case "RunJOB":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.ErrorLogRunjob), "RunJOB."); break;
                case "CheckReciveBox":
                    FormClass.ShowNotCheckForm(new Report_LoginPDA(Report_LoginPDA.PDAReport.CheckReciveBox), "เช็คลังที่ยังไม่เข้า AX."); break;
                case "SendAX":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("0", "1"), "ข้อมูลค้างเข้า AX."); break;
                case "RptAsset":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("1", "1"), "ตรวจเช็ครายการสินทรัพย์."); break;
                case "LineCoupon":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("2", "1"), "การใช้ไลน์คูปอง [ละเอียด]."); break;
                case "LineCouponSUM":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("3", "1"), "การใช้ไลน์คูปอง [สรุป]."); break;
                case "LineCouponControl":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("4", "1"), "การใช้ไลน์คูปอง [ควบคุม]."); break;
                case "Retail POS Monitor":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("5", "1"), "Retail POS Monitor."); break;
                case "CountLogin":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("6", "1"), "สรุปการ Login เข้าใช้งานเครื่อง."); break;
                case "ScanDept":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("D110"), "รายงานสแกนนิ้วเข้าแผนก."); break;
                case "monitorJOB":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("8", "1"), "Monitor JOB."); break;
                case "ReportCPW":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("15", "SUPC", "", "", ""), "รายงาน การใช้งานเครื่องล่าสุด [PDA/TAB/CPW]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {

        }


    }
}
