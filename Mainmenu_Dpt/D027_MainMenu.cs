﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D027_MainMenu : Form
    {
        public D027_MainMenu()
        {
            InitializeComponent();
        }

        private void D027_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                //case "Report_DetailProduct_ByDate":
                //   FormClass.ShowNotCheckForm(new D054.D054_Report_Text("5"), "รายงานจำนวนสินค้า ขาย-เบิก-คืน."); break;
                //case "Report_DetailProduct_TO":
                //   FormClass.ShowNotCheckForm(new D054.D054_Report_Text("6"), "รายงานจำนวนสินค้าเบิก."); break;
                //case "Report_DetailProduct_CN":
                //   FormClass.ShowNotCheckForm(new D054.D054_Report_Text("7"), "รายงานจำนวนสินค้าคืน."); break;
                //case "Report_DetailProduct_Sale":
                //   FormClass.ShowNotCheckForm(new D054.D054_Report_Text("8"), "รายงานจำนวนสินค้าขาย."); break;
                default:
                    break;
            }
        }
        //FarmHouse
        private void RadTreeView_FarmHouse_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "FarmHouseBill":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_ExcelToMNPR("V005450"), "FarmHouse [Import Form Excel]."); break;
                case "FarmHousePB":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_PBPD("PB", "0", "V005450"), "FarmHouse ใบวางบิล [PB]."); break;
                case "FarmHousePD":
                    //Controllers.FormClass.ShowNotCheckForm(new GeneralForm.FarmHouse.FarmHouse_PD("0"), "FarmHouse ใบลดหนี้ [PD]."); break;
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_PBPD("PD", "0", "V005450"), "FarmHouse ใบลดหนี้ [PD]."); break;
                case "FarmHouse_BoxSend":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_BoxSend("24", "1"), "ส่งมินิมาร์ท [ระบุจำนวนลัง]."); break;
                case "ReciveMN":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_BranchRecive("SUPC", "0"), "รายละเอียดการรับสินค้า ฟาร์มเฮ้าส์."); break;
                default:
                    break;
            }
        }
        //Config
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ConfigBox":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("24", "ลังส่งมินิมาร์ท [FarmHouse]", "0", "1", "1", "FH", "3"), "กำหนดลังส่งสาขา [FarmHouse]."); break;
                case "ConfigBoxBranch":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_Detail("24"), "กำหนดสาขาที่ส่ง [FarmHouse]."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                case "Branch":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "YakultBill051670": //FarmHouseBill
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_ExcelToMNPR("V051670"), "Yakult [Import Form Excel]."); break;
                default:
                    break;
            }
        }
    }
}
