﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D150_MainMenu : Form
    {
        public D150_MainMenu()
        {
            InitializeComponent();
        }

        private void D150_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                 default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNHS":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNHS.MNHS_Report("0","SUPC"), "รายงานการซื้อสินค้าระบบบิลเครดิตมินิมาร์ท [MNHS]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        { 
            switch (e.Node.Name)
            {
                case "Cust_MNHS":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNHS.MNHS_Main(), "ลูกค้าเครดิตมินิมาร์ท [MNHS]."); break;
                default:
                    break;
            }
        }
    }
}
