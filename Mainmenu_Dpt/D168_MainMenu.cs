﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D168_MainMenu : Form
    {
        public D168_MainMenu()
        {
            InitializeComponent();
        }

        private void D168_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0",""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNPO_SUPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.MNPO_Order("SUPC", "จัดสินค้าเครดิต"), "สั่งสินค้าให้สาขา."); break;
                case "OrderPicking":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("5", "SUPC", "", "", ""), "การจัดสินค้าตามแผนก [อ้างอิงการสั่ง]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SUPC_MNPO_CheckOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("0", "SUPC", "", "", ""), "เช็คออเดอร์."); break;
                case "StatusAllOrder":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("2", "SUPC", "", "", ""), "สถานะออร์เดอร์รวม [สินค้าที่สั่งผ่านรูป]."); break;
                case "StatusOrderDMS":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("3", "SUPC", "", "", ""), "สถานะออร์เดอร์ตามแผนก [สินค้าที่สั่งผ่านรูป]."); break;
                case "StatusOrderMN":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPO.TranferItem_BranchReciveAX("4", "SUPC", "", "", ""), "จำนวนออเดอร์ตามมินิมาร์ท [สินค้าที่สั่งผ่านรูป]."); break;
                default:
                    break;
            }
        }
    }
}
