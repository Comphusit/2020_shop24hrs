﻿using System;
using PC_Shop24Hrs.GeneralForm.Logistic;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.BoxRecive;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D147_MainMenu : Form
    {
        public D147_MainMenu()
        {
            InitializeComponent();
        }

        private void D147_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0",""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                //case "Recive_SH_Detail":
                //    FormClass.ShowCheckForm(new D194.CheckBOX_Detail("0", "", "0"), "รายละเอียดการส่งของ."); break;
                case "Recive_SH_Sum":
                    FormClass.ShowCheckForm(new CheckBOX_SH_SUM("0"), "ใบขนส่ง [สรุป]."); break;
                //case "CheckBox_All":
                //    FormClass.ShowCheckForm(new D194.CheckBOX_All("0"), "จำนวนลังค้างรับ."); break;

                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ReportSendLO":
                    FormClass.ShowNotCheckForm(new D204_Report("2", "1"), "รายงานการส่งคืนอุปกรณ์ตาม LO."); break;
                default:
                    break;
            }
        }
        //Order To AX
        private void RadTreeView_Dpt_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderToAx_MRTDocument":
                    FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAx_MRTDocument(), "เอกสารจัดสินค้า."); break;
                case "OrderToAx_Main":
                    FormClass.ShowNotCheckForm(new GeneralForm.OrderToAx.OrderToAx_Main(), "จัดสินค้า."); break;
                default:
                    break;
            }
        }
    }
}
