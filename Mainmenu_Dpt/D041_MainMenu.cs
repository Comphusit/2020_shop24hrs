﻿using System;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.JOB.General;
using PC_Shop24Hrs.JOB.Car;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D041_MainMenu : Telerik.WinControls.UI.RadForm
    {
        public D041_MainMenu()
        {
            InitializeComponent();
        }

        private void D041_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }

        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Branch_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                case "D041_PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน.");
                    break;
                default:
                    break;
            }
        }
        //JOB
        private void RadTreeView_TabJOB_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "JOBMN_Main":
                    FormClass.ShowNotCheckForm(new JOBGeneral_Main("SHOP", "00003", "งานซ่อมทั่วไป", "1", ""), "งานซ่อมทั่วไป SHOP"); break;
                case "JOBMN_Main_SUPC":
                    FormClass.ShowNotCheckForm(new JOBGeneral_Main("SUPC", "00003", "งานซ่อมทั่วไป", "1", ""), "งานซ่อมทั่วไป SUPC"); break;
                case "CarMN":
                    FormClass.ShowNotCheckForm(new JOBCar_Main("SHOP", "00007", "งานซ่อมรถ-เครื่องจักร", "1", ""), "งานซ่อมรถ-เครื่องจักร SHOP"); break;
                case "CarSUPC":
                    FormClass.ShowNotCheckForm(new JOBCar_Main("SUPC", "00007", "งานซ่อมรถ-เครื่องจักร", "1", ""), "งานซ่อมรถ-เครื่องจักร SUPC"); break;
                case "BillRB-RG-RH":
                    FormClass.ShowCheckForm(new JOB.Bill.BillMN_ForAddJOB("'D041'", "MN%", "", "00003"), "ข้อมูลการส่งซ่อม-ส่งคืนอุปกรณ์ต่างๆ."); break;
                case "D041_CLIAM":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00003", "งานซ่อมทั่วไป", "1"), "ส่งซ่อมอุปกรณ์ ภายในบริษัท."); break;
                case "D041_DESCARD":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("1", "00003", "งานซ่อมทั่วไป", "1"), "นำทิ้งอุปกรณ์ ภายในบริษัท."); break;
                case "D079_Item":
                    FormClass.ShowNotCheckForm(new JOB.Stock.Stock_Item(), "รายการสต็อกห้องช่าง."); break;
                case "OilCap":
                    FormClass.ShowCheckFormNormal(new JOB.CheckLockOilCap("0"), "การล็อคฝาถังน้ำมัน."); break;
                default:
                    break;
            }
        }
        //Config
        private void RadTreeView_TabConfug_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D041_JOBGROUPSUB":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00003"), "ประเภทงาน [ช่างทั่วไป]."); break;
                case "D041_JOBGROUPSUB_HEAD":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00006"), "หัวหน้าช่าง [ทั่วไป]."); break;
                case "CarType":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00007"), "ประเภทรถ-เครื่องจักร."); break;
                case "CarType_Repair":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00008"), "ประเภทการซ่อมรถ-เครื่องจักร."); break;
                case "D079_GROUPSUB":
                    FormClass.ShowNotCheckForm(new JOB.JOB_GROUPSUB("00014"), "ประเภทสินค้าสต็อกห้องช่าง."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNII":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNII"), "รับสินค้าเข้าสต็อกห้องช่าง [MNII]."); break;
                case "OrderOutside_Report":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderOutside_Report(), "รายงานสั่งของใช้ช่างจากนอก."); break;
                case "OrderInside_Report":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderInside_Report(), "รายงานสั่งของจากโรงไม้เชิงทะเล."); break;
                case "BillRA":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.Bill_RA(), "ซ่อมรถอู่นอก [RA]."); break;
                case "Bill_Techout":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Bill_Techout("SUPC", "1"), "รายงานช่างนอกเข้าสาขา."); break;
                case "Detail_MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG", "0"), "รายละเอียดส่งของไม่มีทะเบียน [MNRG]."); break;
                case "Detail_MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("Detail_MNIO", "0"), "รายละเอียดเบิกไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "Detail_MNOT":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("Detail_MNOT", "0"), "รายละเอียดเบิกเครื่องมือ [MNOT]."); break;
                case "MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNIO"), "เบิกสินค้าไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "MNOT":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNOT"), "เบิกเครื่องมือ [MNOT]."); break;
                case "IF":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("ISPC"), "เบิกสินค้า SPC [I-F]."); break;
                case "IF_MN":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("IMN"), "เบิกสินค้า MN [I-F]."); break;
                case "FAL":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("FAL"), "เบิกสินค้า [FAL]."); break;
                case "MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG"), "ส่งของไม่มีทะเบียนคืน [MNRG]."); break;
                case "MNRB":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRB"), "ส่งของมีทะเบียนคืน [MNRB]."); break;
                case "RptLockOilCap":
                    FormClass.ShowNotCheckForm(new ComMinimart.Tag.TagDetail("1", "0"), "รายละเอียด Tag [ฝาถังน้ำมัน]."); break;
                case "Report_MNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRS"), "เบิกสินค้าส่งสาขา [MNRS]."); break;
                default:
                    break;
            }
        }
        //order
        private void RadTreeView_Order_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderInside_Main":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderInside_Main(), "สั่งของจากโรงไม้เชิงทะเล."); break;
                case "OrderOutside_Main":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderOutside_Main(), "สั่งของใช้ช่างจากนอก."); break;
                case "OrderD041":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderOutNew(), "ซื้อของใช้งานภายในบริษัท."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_BOQ_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            //switch (e.Node.Name)
            //{
            //    case "BOQ_Type":
            //        Controllers.FormClass.ShowNotCheckForm(new GeneralForm.BOQ.BOQ_TypeMain(), "หมวดงานหลักและหมวดงานย่อย."); break;
            //    case "BOQ_Item":
            //        Controllers.FormClass.ShowNotCheckForm(new GeneralForm.BOQ.BOQ_Item(), "กำหนดสินค้าตั้งต้น."); break;
            //    case "BchRate":
            //        Controllers.FormClass.ShowNotCheckForm(new GeneralForm.BOQ.BOQ_Main(), "ประมาณการก่อสร้าง."); break;
            //    default:
            //        break;
            //}
        }

        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CAMIP":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_IP("41", "กล้องก่อสร้าง", "CAM", "0"), "กล้องช่างก่อสร้าง."); break;
                case "CAMMK":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_IP("42", "กล้องแมคโคร", "CAMMK", "0"), "กล้องแมคโคร."); break;
                case "ProductRecive1":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("1", "1", "MN%"), "รับสินค้าระบบ TRK [มินิมาร์ท]."); break;
                case "ProductRecive2":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("1", "1", "D%"), "รับสินค้าระบบ TRK [แผนกทั่วไป]."); break;
                case "ProductRecive_BRP":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("4", "0", ""), "เอกสารการรับสินค้า BRP."); break;
                default:
                    break;
            }
        }
    }
}
