﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D103_MainMenu : Form
    {
        public D103_MainMenu()
        {
            InitializeComponent();
        }

        private void D103_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                   FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
              default:
                    break;
            }
        }

        //งานทั่วไป
        private void RadTreeView_Genaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "D103_Cam":
                   FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Show(" AND [SHOW_ID] IN ('C13') "), "กล้องออนไลน์ตามจุดต่างๆ."); break;

                default:
                    break;
            }
        }
    }
}
