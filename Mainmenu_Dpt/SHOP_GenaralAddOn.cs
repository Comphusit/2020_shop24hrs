﻿using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Employee;
using System;
using PC_Shop24Hrs.GeneralForm.Report;
using System.Windows.Forms;
using System.Data;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class SHOP_GenaralAddOn : Form
    {
        DataTable dtBch = new DataTable();
        public SHOP_GenaralAddOn()
        {
            InitializeComponent();
        }

        private void SHOP_GenaralAddOn_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
            dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
        }


        //Config
        private void RadTreeView_TabConfug_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderInside_Main":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderInside_Main(), "สั่งของจากโรงไม้เชิงทะเล."); break;
                case "OrderOutside_Main":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderOutside_Main(), "สั่งของใช้ช่างจากนอก."); break;
                default:
                    break;
            }
        }
        ////ขอเบิกสินค้า
        //private void RadTreeView_TabMNRO_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        //{
        //    switch (e.Node.Name)
        //    {
        //        case "SmartRO_ORDER":
        //            FormClass.ShowCheckForm(new _24SHOP.Genaral.MNRO.MNRO_ORDER(), "ขอเบิกสินค้า [MNRO]"); break;
        //        case "MNRS":
        //            FormClass.ShowNotCheckForm(new D062.BillOut_FindBill_APV("0", "MNRS"), "บิลเบิกสินค้า [MNRS]"); break;
        //        case "MNRR":
        //            FormClass.ShowNotCheckForm(new D062.BillOut_FindBill_APV("0", "MNRR"), "เบิกของในมินิมาร์ท [MNRR]"); break;
        //        default:
        //            break;
        //    }
        //}

        private void RadTreeView_JOB_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "JOB_General":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SHOP", "00003", "งานซ่อมทั่วไป", "0", SystemClass.SystemBranchID), "งานซ่อมทั่วไป [8560-8561]."); break;
                case "JOB_Car":
                    FormClass.ShowNotCheckForm(new JOB.Car.JOBCar_Main("SHOP", "00007", "งานซ่อมรถ-เครื่องจักร", "0", SystemClass.SystemBranchID), "งานซ่อมรถยนต์-เครื่องจักร [3060,8580]."); break;
                case "JOB_Comservice":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00002", "ComService", "0", SystemClass.SystemBranchID), "งานซ่อม ComService [8555]."); break;
                case "JOB_MN":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00001", "ComMinimart.", "0", SystemClass.SystemBranchID), "งานซ่อม ComMinimart [8570]."); break;
                case "JOB_Center":
                    FormClass.ShowNotCheckForm(new JOB.Center.JOBCenter_Main("00005", "JOB Center", "0", SystemClass.SystemBranchID), "งานดูแลสาขา SHOP"); break;
                case "JOB_Head":
                    FormClass.ShowNotCheckForm(new JOB.Center.JOBCenter_Main("00004", "JOB ตรวจสาขา", "0", SystemClass.SystemBranchID), "งานเข้าตรวจสาขา SHOP"); break;
                default:
                    break;
            }
        }

        private void RadTreeViewTime_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            //switch (e.Node.Name)
            //{
            //    case "TimeKeeperForm1":
            //        FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("1"), "ลงเวลาเข้างาน."); break;
            //    case "TimeKeeperForm2":
            //        FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("2"), "ลงเวลาพัก."); break;
            //    case "TimeKeeperForm3":
            //        FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกน 1.30 ชม."); break;
            //    case "TimeKeeperForm4":
            //        FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("4"), "ตรวจสอบปริมาณพนักงาน."); break;
            //    case "ReportType28":
            //        FormClass.ShowNotCheckForm(new D054.D054_Report_Text("11"), "รายงานประจำวัน."); break;
            //    default:
            //        break;
            //}
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "TimeKeeperForm1":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("1"), "ลงเวลาเข้า-ออกงาน."); break;
                case "TimeKeeperForm2":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("2"), "ลงเวลาพัก."); break;
                case "TimeKeeperForm3":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกน 1.30 ชม."); break;
                case "TimeKeeperForm4":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("4"), "ตรวจสอบปริมาณพนักงาน."); break;
                case "TimeKeeperForm5":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("5"), "PC เข้าสาขา."); break;
                case "CheckSaraly":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.CheckSalary(), "เช็ครายได้ล่าสุด."); break;
                case "ScanD041":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("6"), "ช่างก่อสร้าง."); break;
                case "ScanTachnicain":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("7"), "ช่างซ่อมบริษัทเข้า-ออก."); break;
                case "ReportType28":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("11"), "รายงานประจำวัน ข้อมูลละเอียด."); break;
                case "MNSV_Main":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNSV.MNSV_Main(), "งานทีมจัดร้าน."); break;
                case "TimeKeeperForm12":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("1", "2"), "ลงเวลาเข้า-ออกงาน [เฉพาะ PartTime]."); break;
                case "TimeKeeperForm13":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("1", "3"), "ลงเวลาเข้า-ออกงาน [เฉพาะ OT วันหยุด]."); break;
                default:
                    break;
            }
        }


        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderInside_Report":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderInside_Report(), "รายงานสั่งของจากโรงไม้เชิงทะเล."); break;
                case "D054_Report_Employee1":
                    FormClass.ShowNotCheckForm(new Employee_Report("1", "SHOP", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [ละเอียด]."); break;
                case "D054_Report_Employee2":
                    FormClass.ShowNotCheckForm(new Employee_Report("2", "SHOP", "", DateTime.Now), "รายงาน ตรวจสอบพนักงาน."); break;
                case "D054_Report_Employee4":
                    FormClass.ShowNotCheckForm(new Employee_Report("4", "SHOP", "", DateTime.Now), "รายงาน การลงเวลาพัก."); break;
                case "Report_Scan":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม."); break;
                case "ReportType28_M":
                    FormClass.ShowNotCheckForm(new Report_TextCenterShop("12"), "รายงานประจำวัน ข้อมูลสรุป."); break;
                case "Recive_Report":
                    if (dtBch.Rows[0]["BRANCH_OUTPHUKET"].ToString() == "1")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("เมนูนี้เปิดใช้งานเฉพาะสาขาภายต่างจังหวัดเท่านั้น");
                        return;
                    }

                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SHOP", "0"), "ค่าคอมของพนักงานลงสินค้าต่างจังหวัด [สรุป]."); break;

                case "D054_Report_EmployeeVDO":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("0", "SHOP"), "รายงานการตรวจตัว สาขา ระบุวัน."); break;
                case "D054_Report_EmployeeVDOSum":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("1", "SHOP"), "รายงานการตรวจตัว สาขา ตามช่วงเวลา."); break;
                case "D054_Report_EmployeeVDO054":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("2", "SHOP"), "รายงานการตรวจตัว ทีมจัดร้าน."); break;
                case "D054_Report_EmployeeVDOChecker":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("3", "SHOP"), "รายงานการตรวจตัว ตรวจสินค้าส่วนหน้า."); break;
                case "D054_Report_EmployeeVDOOth":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("4", "SHOP"), "รายงานการตรวจตัว แผนกต่างๆ."); break;
                case "D054_Report_EmployeeVDOPc":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("7", "SHOP"), "รายงานการตรวจตัว PC จัดสินค้า."); break;
                case "D054_Report_EmployeeVDOBill":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("5", "SHOP"), "รายงานการตรวจบิล."); break;
                case "D054_Report_EmployeeVDOCar":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("6", "SHOP"), "รายงานการตรวจก่อนกลับบ้าน ."); break;
                case "D054_Report_EmployeeBreak":
                    FormClass.ShowNotCheckForm(new Employee_Report("4", "SHOP", "", DateTime.Now), "รายงาน การลงเวลาเข้าพัก ."); break;
                case "Report_ScanD041":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("D041"), "รายงานการสแกนนิ้วช่างก่อสร้าง"); break;

                case "Commission_Redim":
                    //FormClass.ShowNotCheckForm(new GeneralForm.Commission.ReportRedeem.ReportRedempCommission("SHOP", "0"), "รายละเอียดการคอมแลกแต้ม ตามสาขา"); break;
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.ReportRedeem.RedempCommission("SHOP", "0"), "รายงานค่าคอมของการแลกแต้มสินค้ามินิมาร์ท."); break;
                //case "Commission_ReciveOrder":
                //    FormClass.ShowNotCheckForm(new GeneralForm.Commission.OrderCustomers.CommissionOrderCust(), "รายงานค่าคอมออร์เดอร์ลูกค้า."); break;

                case "Report_Employee0":
                    FormClass.ShowNotCheckForm(new Employee_Report("0", "SHOP", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [สรุป]."); break;
                case "Report_Employee1":
                    FormClass.ShowNotCheckForm(new Employee_Report("1", "SHOP", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [ละเอียด]."); break;
                case "Report_Employee2":
                    FormClass.ShowNotCheckForm(new Employee_Report("2", "SHOP", "", DateTime.Now), "รายงาน ตรวจสอบพนักงาน."); break;
                case "Report_Employee3":
                    FormClass.ShowNotCheckForm(new Employee_Report("3", "SHOP", "", DateTime.Now), "รายงาน มาตรฐานพนักงาน."); break;
                case "CommissionEmpShelf":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfReportEmp("SHOP"), "รายงานค่าคอมของพนักงานดูแลสินค้า."); break;
                case "ComCheckStock":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("7", "0"), "ค่าคอมของพนักงานเเช็คสต๊อก."); break;
                case "D054_Report_EmployeePartTime":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("8", "SHOP"), "รายงานการตรวจตัว PartTime."); break;
                case "D054_Report_EmployeeOT":
                    FormClass.ShowNotCheckForm(new Employee_ReportVDO("9", "SHOP"), "รายงานการตรวจตัว OT วันหยุด."); break;
                case "Report_EmployeePartTime":
                    FormClass.ShowNotCheckForm(new Employee_Report("6", "SHOP", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [PartTime]."); break;
                case "Report_EmployeeOT":
                    FormClass.ShowNotCheckForm(new Employee_Report("7", "SHOP", "", DateTime.Now), "รายงาน ลงเวลาเข้า-ออกงาน [OT วันหยุด]."); break;

                case "CashPartTime":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SHOP", "1"), "ค่าคอมของพนักงานแคชเชียร์ PartTime."); break;
                case "OT":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SHOP", "2"), "ค่าคอมของพนักงาน OT วันหยุด."); break;
                case "comlocation":
                    if (dtBch.Rows[0]["BRANCH_PROVINCE"].ToString() != "A05")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("เมนูนี้เปิดใช้งานเฉพาะสาขาเกาะสมุย-พะงันเท่านั้น");
                        return;
                    }
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SHOP", "3"), "ค่าเบี้ยเลี้ยง+ค่าที่พัก[เฉพาะสาขาเกาะสมุย-พะงัน]."); break;
                case "cashSum":
                    FormClass.ShowNotCheckForm(new GeneralForm.Commission.Commission_EmpDownReport("SHOP", "4"), "ค่าคอมของแคชเชียร์มินิมาร์ทรวม [OnTop]."); break;
                default:
                    break;
            }

        }
        private void RadTreeView1_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Camera_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("5", "", ""), "กล้องวงจรปิด."); break;
                case "Camera_Main_Position":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Show(), "กล้องออนไลน์ตามจุดต่างๆ."); break;
                default:
                    break;
            }
        }
        private void RadTreeView3_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Zone":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfMain("SHOP"), "ตั้งค่าโซนสินค้าและชั้นวางสินค้า"); break;
                case "Lock_Emp":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfEmpl(), "ตั้งค่าพนักงานดูแลชั้นวางสินค้า"); break;
                case "Lock_Item":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfItems(), "ตั้งค่าสินค้าประจำชั้นวาง"); break;
                case "ItemAllZone":
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("3"), "สินค้าทั้งหมดตามชั้นวาง."); break;
                case "ImportExcel":
                    FormClass.ShowNotCheckForm(new GeneralForm.ItemShelf.ShelfItems_Manage(), "Import ข้อมูลสินค้าเข้าชั้นวาง."); break;
                default:
                    break;
            }
        }
    }
}
