﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Logistic;
using PC_Shop24Hrs.GeneralForm.BoxRecive;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D023_MainMenu : Form
    {
        public D023_MainMenu()
        {
            InitializeComponent();
        }

        private void D023_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Recive_SH_Detail":
                    FormClass.ShowCheckForm(new CheckBOX_Detail("0", "", "0", ""), "รายละเอียดการส่งของ."); break;
                case "Recive_SH_Sum":
                    FormClass.ShowCheckForm(new CheckBOX_SH_SUM("0"), "ใบขนส่ง [สรุป]."); break;

                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "BillEdit":
                    FormClass.ShowNotCheckForm(new BillEdit("0"), "รายงานบิลแก้ไข."); break;
                case "ReciveBoxOth":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("0"), "รายงานรับลังสาขาอื่น."); break;
                case "BillPallets":
                    FormClass.ShowNotCheckForm(new BillPallets("0"), "รายงานบิลพาเลท."); break;
                case "ReciveBox_P":
                    FormClass.ShowNotCheckForm(new ReciveBox_STA("1"), "รายงานรับลังค้างรับ."); break;
                case "ReciveBox_NotSH":
                    FormClass.ShowNotCheckForm(new ReciveBox_NotSH(), "ลังที่รับไม่ขึ้นทะเบียน."); break;
                case "SendMoney_Rpt":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNPM.MNPM_ReportSum("0"), "รายงานซองส่งเงินค้างส่ง."); break;
                case "D204_Report_0":
                    FormClass.ShowNotCheckForm(new D204_Report("0", "0"), "รายงานน้ำหนักรถต่างจังหวัด."); break;
                case "D204_Report_1":
                    FormClass.ShowNotCheckForm(new D204_Report("1", "0"), "รายงานการตรวจรถ 4 ด้าน."); break;
                //case "CarCare":
                //    Controllers.FormClass.ShowNotCheckForm(new D204.CarCare("0"), "รายงานการล้างรถ."); break;
                case "D204_Report_7":
                    FormClass.ShowNotCheckForm(new D204_Report("7", "0"), "รายงานฝาถังน้ำมัน."); break;
                case "Report_ISPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("ISPC"), "เบิกสินค้า SPC [I-F]."); break;
                default:
                    break;
            }
        }


    }
}
