﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Report;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D020_MainMenu : Form
    {
        public D020_MainMenu()
        {
            InitializeComponent();
        }

        private void D020_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "D003_scan1_30":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                case "Cust_Rent":
                    FormClass.ShowCheckForm(new GeneralForm.Promotion.Promotion_Report("2","1"), "รายชื่อสำหรับจ่ายคูปอง Line@."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_Scan":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกขายส่งและลูกค้าสัมพันธ์]."); break;
                case "LineCouponControl":
                    FormClass.ShowNotCheckForm(new Report_TextComMN("4", "0"), "การใช้คูปอง Line@ ร้านเช่า."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Coupon":
                    FormClass.ShowCheckForm(new GeneralForm.LineCoupon.LineCoupon_Main(), "การออกคูปองและส่วนลด."); break;
                default:
                    break;
            }
        }
        //งานทั้วไป

    }
}
