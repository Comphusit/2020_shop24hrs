﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D004_MainMenu : Form
    {
        public D004_MainMenu()
        {
            InitializeComponent();
        }

        private void D004_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }

        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "BCH":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                default:
                    break;
            }
        }
        //Gernaral
        private void RadTreeView_TabJobGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {

                case "ComMN_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00001", "ComMinimart", "0"), "ส่งซ่อมอุปกรณ์ [ComMinimart]."); break;
                case "ComService_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00002", "ComService", "0"), "ส่งซ่อมอุปกรณ์ [ComService]."); break;
                case "Main_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00003", "งานซ่อมทั่วไป", "0"), "ส่งซ่อมอุปกรณ์ [ช่างซ่อมบำรุง]."); break;
                case "Center_cliam":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("0", "00005", "งานสาขา", "0"), "ส่งซ่อมอุปกรณ์ [CenterShop]."); break;
                case "Center_D029":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("2", "00012", "ส่งซ่อม ลูกค้าคอมพิวเตอร์", "0"), "ส่งซ่อมสินค้าลูกค้า [แผนกคอมพิวเตอร์]."); break;
                case "Center_D100":
                    FormClass.ShowNotCheckForm(new JOB.Claim.Claim_Main("3", "00013", "ส่งซ่อม ลูกค้ามือถือ", "0"), "ส่งซ่อมสินค้าลูกค้า [แผนกมือถือ]."); break;
                case "JOBMN_Maintanace":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SHOP", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป มินิมาร์ท."); break;
                case "MNPZ":
                    FormClass.ShowCheckForm(new GeneralForm.MNPZ.MNPZ(), "ใบรับของรางวัล PZ."); break;
                case "FarmHousePD":
                    FormClass.ShowCheckForm(new GeneralForm.FarmHouse.FarmHouse_PBPD("PD", "1", "V005450"), "FarmHouse ใบลดหนี้ [PD]."); break;
                case "ProductRecive3":
                    FormClass.ShowCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("3","0",""), "เอกสารการขายดิน."); break;
                case "Image_allBch":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "0", "0"), "รูปถ่ายประจำวัน ระบุวันที่ ทุกสาขา."); break;
                case "Image_allDate":
                    FormClass.ShowNotCheckForm(new GeneralForm.ImageShow.AllDays.ImageAllDays_Show("SUPC", "1", "0"), "รูปถ่ายประจำวัน ระบุสาขา ตามช่วงวันที่."); break;
                default:
                    break;
            }
        }
        //ตั้งค่า
        private void RadTreeView_TabConfig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "DOCTYPE":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("35", "ประเภทเอกสารส่งคืนจากสาขา","0", "1", "1", "10_", "3"), "ประเภทเอกสารส่งคืนจากสาขา.");
                    break;
                default:
                    break;
            }
        }
        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_MNPZ":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNPZ"), "นำของรางวัลออก [MNPZ]."); break;
                case "Report_MNRH":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRH"), "ส่งคืนเอกสาร [MNRH]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Document_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "MNRH":
                    FormClass.ShowCheckForm(new GeneralForm.ReceiveDocument.MNRH_Receive(), "การรับเอกสาร [MNRH]."); break;
                case "MNRL":
                    FormClass.ShowCheckForm(new GeneralForm.ReceiveDocument.MNRL_Main(), "การส่งคืนเอกสาร [MNRL]."); break;
                default:
                    break;
            }
        }
    }
}
