﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Logistic;
using PC_Shop24Hrs.GeneralForm.PhoneBook;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D999_MainMenu : Form
    {
        public D999_MainMenu()
        {
            InitializeComponent();
        }

        private void D999_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Branch":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                case "ItembarcodeDetail":
                    FormClass.ShowNotCheckForm(new Itembarcode.ItembarcodeDetail("0", "", "0", "0", ""), "รายละเอียดสินค้า."); break;
                case "programeUse":
                    FormClass.ShowNotCheckForm(new PhoneBook("2"), "ข้อมูลโปรแกรมภายในบริษัท."); break;
                default:
                    break;
            }
        }

        //กลัองทั้งหมด 
        private void RadTreeView_CamPermission_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Camera_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("3", SystemClass.SystemUserID, ""), "กล้องทั้งหมด."); break;
                case "CamC20":
                    if ((SystemClass.SystemComMinimart == "1") || (SystemClass.SystemDptID == "D025"))
                        FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Show(" AND [SHOW_ID] IN ('C20') "), "กล้องมินิมาร์ททั้งหมด [เบเกอรี่].");
                    else
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะแผนก D025-ฝ่ายผลิต-เบเกอรี่ เท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                    break;
                default:
                    break;
            }
        }

        private void RadTreeView_TabJob_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "JOB_General":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SUPC", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป [8560]."); break;
                case "JOB_Car":
                    FormClass.ShowNotCheckForm(new JOB.Car.JOBCar_Main("SUPC", "00007", "งานซ่อมรถ-เครื่องจักร", "0", ""), "งานซ่อมรถยนต์-เครื่องจักร [3060,8580]."); break;
                case "JOB_Comservice":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SUPC", "00002", "ComService", "0", ""), "งานซ่อมคอมพิวเตอร์และระบบ [8555]."); break;
                case "JOB_MN":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SUPC", "00001", "ComMinimart.", "0", ""), "งานซ่อมกล้องและเครื่องสแกนนิ้ว [8570]."); break;
                case "QCarCare":
                    FormClass.ShowNotCheckForm(new CarCare("0", "0"), "การจองคิวล้างรถ."); break;
                default:
                    break;
            }

        }

        private void RadTreeView_Scan_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Scan130":
                    FormClass.ShowCheckFormNormal(new GeneralForm.TimeKeeper.TimeKeeperForm("3"), "สแกนนิ้ว 1.30 ชม."); break;
                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_Scan":
                    FormClass.ShowNotCheckForm(new GeneralForm.TimeKeeper.TimeKeeper_Report("130HR"), "รายงานการสแกนนิ้ว 1.30 ชม [แผนกขายส่งและลูกค้าสัมพันธ์]."); break;
                case "FoodCourt":
                    FormClass.ShowNotCheckForm(new GeneralForm.FoodCourt.FoodCourt_Main("0", "0"), "รายงานการใช้จ่ายในระบบ FoodCourt."); break;
                default:
                    break;
            }
        }
        //โรงหมี่
        private void RadTreeView_D015_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "ProductRecive1":
                    if (SystemClass.SystemComMinimart == "1")
                    {
                        FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("1", "1", "D%"), "รับสินค้าระบบ TRK [แผนกทั่วไป].");
                    }
                    else
                    {
                        switch (SystemClass.SystemDptID)
                        {
                            case "D007":
                                FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("1", "0", "D015"), "รับสินค้าระบบ TRK [ฝ่ายผลิต-โรงหมี่]."); break;
                            case "D015":
                                FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("1", "1", "D015"), "รับสินค้าระบบ TRK [ฝ่ายผลิต-โรงหมี่]."); break;
                            case "D186":
                                FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.TRK.ProductRecive_TRK("1", "1", "D186"), "รับสินค้าระบบ TRK [จัดซื้อ-ศูนย์น้ำเนสท์เล่กระบี่]."); break;
                            default:
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                                break;
                        }
                    }
                    break;
                case "Droid":
                    FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("0", "0", "0"), "รับสินค้าเข้าคลัง."); break;
                case "DROID_D177NEW":
                    if (SystemClass.SystemComMinimart == "1")
                    {
                        FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                    }
                    else
                    {
                        switch (SystemClass.SystemDptID)
                        {
                            case "D062":
                                FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                            case "D111":
                                FormClass.ShowNotCheckForm(new GeneralForm.ProductRecive.DROID.ProductRecive_DROID("4", "0", "0"), "รับสินค้าเข้าคลัง[เฉพาะสินค้าโรงงาน]."); break;
                            default:
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"เมนูนี้เปิดใข้งานเฉพาะบางแผนกเท่านั้น{Environment.NewLine}สำหรับแผนก {SystemClass.SystemDptID}  {SystemClass.SystemDptName} ยังไม่ได้เปิดให้ใช้งาน");
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
