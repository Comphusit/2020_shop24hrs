﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D9BS_MainMenu : Form
    {
        public D9BS_MainMenu()
        {
            InitializeComponent();
        }

        private void D9BS_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน."); break;
                default:
                    break;
            }
        }
        //กลัองทั้งหมด BS
        private void RadTreeView_TabCam_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Camera_Main_MN":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "MN",""), "กล้องมินิมาร์ท."); break;
                case "Camera_Main_RET":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "RET",""), "กล้องพื้นที่ขาย."); break;
                case "Camera_Main_SPC":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "SPC",""), "กล้องอาคารและพื้นที่ส่วนหลัง."); break;
                case "Camera_Main_OTH":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "OTH",""), "กล้องพื้นที่ส่วนนอก."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_cam_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CAM_F":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "MN","C01"), "กล้องตู้แช่."); break;
                case "CAM_U":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "MN","C02"), "กล้องเพดาน."); break;
                case "CAM_D":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "MN","C03"), "กล้องส่องถนน."); break;
                case "CAM_C":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "MN","C05"), "กล้องแคชเชียร์."); break;
                case "CAM_R":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Main("4", "MN","C04"), "กล้องลงสินค้า."); break;
                case "CamOnline":
                    FormClass.ShowNotCheckForm(new ComMinimart.Camera.Camera_Show(), "กล้องออนไลน์ตามจุดต่าง."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_JOB_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "SPC_JOB_Maintanace":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SUPC", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป [สาขาใหญ่]."); break;
                case "SPC_JOB_CAR":
                    FormClass.ShowNotCheckForm(new JOB.Car.JOBCar_Main("SUPC", "00007", "งานซ่อมรถ-เครื่องจักร", "0", ""), "งานซ่อมรถยนต์-เครื่องจักร [สาขาใหญ่]."); break;
                case "SPC_COMService":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SUPC", "00002", "ComService", "0", ""), "งานซ่อมคอมพิวเตอร์และระบบ [สาขาใหญ่]."); break;
                case "SPC_ComMN":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SUPC", "00001", "ComMinimart.", "0", ""), "งานซ่อมกล้องและเครื่องสแกนนิ้ว [สาขาใหญ่]."); break;
                case "MN_JOB_Maintanace":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SHOP", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป [มินิมาร์ท]."); break;
                case "MN_JOB_CAR":
                    FormClass.ShowNotCheckForm(new JOB.Car.JOBCar_Main("SHOP", "00007", "งานซ่อมรถ-เครื่องจักร", "0", ""), "งานซ่อมรถยนต์-เครื่องจักร [มินิมาร์ท]."); break;
                case "MN_COMService":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00002", "ComService", "0", ""), "งานซ่อมคอมพิวเตอร์และระบบ [มินิมาร์ท]."); break;
                case "MN_ComMN":
                    FormClass.ShowNotCheckForm(new JOB.Com.JOBCOM_Main("SHOP", "00001", "ComMinimart.", "0", ""), "งานซ่อมกล้องและเครื่องสแกนนิ้ว [มินิมาร์ท]."); break;
                case "MN_Center":
                    FormClass.ShowNotCheckForm(new JOB.Center.JOBCenter_Main("00005", "JOB Center.", "0", ""), "งาน CenterShop[มินิมาร์ท]."); break;
                case "MN_Head":
                    FormClass.ShowNotCheckForm(new JOB.Center.JOBCenter_Main("00004", "JOB ตรวจสาขา.", "0", ""), "งานตรวจสาขา [มินิมาร์ท]."); break;
                default:
                    break;
            }
        }
    }
}
