﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.GeneralForm.PhoneBook;
using PC_Shop24Hrs.GeneralForm.BillOut;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Mainmenu_Dpt
{
    public partial class D062_MainMenu : Form
    {
        public D062_MainMenu()
        {
            InitializeComponent();
        }

        private void D062_MainMenu_Load(object sender, EventArgs e)
        {
            radLabel_Dpt.Text = $@"{SystemClass.SystemUserID} : {SystemClass.SystemUserName} [{SystemClass.SystemDptName}]";
        }


        //ข้อมูลทั่วไป
        private void RadTreeView_TabGenaral_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Branch_Main":
                    FormClass.ShowNotCheckForm(new ComMinimart.Manage.Branch_Main("2"), "ข้อมูลสาขา"); break;
                case "PhoneBook":
                    FormClass.ShowNotCheckForm(new PhoneBook("1"), "เบอร์โทรภายใน.");
                    break;
                default:
                    break;
            }
        }
        //JOB
        private void RadTreeView_TabJOB_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "GroupMain":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.I_GroupMain(), "กำหนดกลุ่มเบิก."); break;
                case "GroupItem":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.I_Item(), "กำหนดสินค้าเบิก."); break;
                case "SmartRO_Main":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRS_Main("SHOP", ""), "ทำรายการเบิกสินค้า."); break;
                case "SmartRO_ReciveBill":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRO_FindData("D062"), "อนุมัติบิลขอเบิกสินค้า."); break;
                case "SmartRO_LoadOrder":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.MNRO_LoadOrder("D062"), "ดึงข้อมูลจัดสินค้า."); break;
                case "SmartRO_MNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("1", "MNRS_MN", "1"), "ข้อมูลการเบิกสินค้าสาขา [MNRS]."); break;
                case "MNRS_D":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("1", "MNRS_D", "1"), "ข้อมูลการเบิกสินค้าแผนก [MNRS]."); break;
                case "MNRR":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_FindBill_APV("1", "MNRR", "1"), "ข้อมูลการเบิกสินค้า [MNRR]."); break;
                default:
                    break;
            }
        }


        //Report
        private void RadTreeView_TabReport_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Report_MNRS":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRS"), "เบิกสินค้าส่งสาขา [MNRS]."); break;
                case "Report_MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNIO"), "เบิกสินค้าไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "Report_MNOT":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNOT"), "เบิกเครื่องมือ [MNOT]."); break;
                case "Report_IMN":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("IMN"), "เบิกสินค้า MN [I-F]."); break;
                case "Report_ISPC":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("ISPC"), "เบิกสินค้า SPC [I-F]."); break;
                case "FAL":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("FAL"), "เบิกสินค้า [FAL]."); break;
                case "MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG"), "ส่งของไม่มีทะเบียนคืน [MNRG]."); break;
                case "MNRB":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRB"), "ส่งของมีทะเบียนคืน [MNRB]."); break;
                case "MNRH":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRH"), "ส่งคืนเอกสาร [MNRH]."); break;
                case "MNRZ":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRZ"), "ส่งซ่อมอุปกรณ์ภายใน [MNRZ]."); break;
                case "MNRR":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRR"), "เบิกสินค้าที่มินิมาร์ท [MNRR]."); break;
                case "MNPZ":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNPZ"), "นำของรางวัลออก [MNPZ]."); break;
                case "IDM":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("IDM"), "ทำลายสินค้าทิ้ง [IDM]."); break;
                case "MNCM_0":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNCM_0"), "ส่งซ่อมอุปกรณ์ [MNCM]."); break;
                case "MNCM_1":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNCM_1"), "นำทิ้งอุปกรณ์ [MNCM]."); break;
                case "MNCM_2":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNCM_2"), "ส่งซ่อมสินค้าลูกค้า [คอมพิวเตอร์]."); break;
                case "MNCM_3":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNCM_3"), "ส่งซ่อมสินค้าลูกค้า [มือถือ]."); break;
                case "BillRA":
                    FormClass.ShowCheckForm(new GeneralForm.BillOut.Bill_RA(), "ซ่อมรถอู่นอก [RA]."); break;
                case "EMPLOYEE_ListInCorrect":
                    //FormClass.ShowNotCheckForm(new FormShare.Report_EMP_ListInCorrect("D062", ""), "สรุปรายการหัก."); break;
                    FormClass.ShowNotCheckForm(new Itembarcode.Item_Report("5", "D062"), "สรุปรายการหัก."); break;
                case "Detail_MNRG":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRG", "0"), "รายละเอียดส่งของไม่มีทะเบียน [MNRG]."); break;
                case "Detail_MNIO":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("Detail_MNIO", "0"), "รายละเอียดเบิกไม่มีค่าใช้จ่าย [MNIO]."); break;
                case "Detail_MNOT":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("Detail_MNOT", "0"), "รายละเอียดเบิกเครื่องมือ [MNOT]."); break;
                case "MNRM":
                    FormClass.ShowNotCheckForm(new GeneralForm.BillOut.BillOut_Report("MNRM"), "โอนย้ายระหว่างสาขา [MNRM]."); break;
                case "MNCS_ReportItemByJournalID":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("0", "2"), "รายงานตรวจนับสต๊อกสินค้า."); break;
                case "SPCN_SumInventCountingMNByVoucherByDate_FindJournalId":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("1", "2"), "รายงานเปรียบเทียบยอดขาย/จำนวนสต็อกคงเหลือ."); break;
                case "CountStkByEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("2", "2"), "รายงานจำนวนรายการเช็คสต๊อก."); break;
                case "CountStkByEmpHour":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("3", "2"), "รายงานจำนวนรายการเช็คสต๊อก[รายชั่วโมง]"); break;
                case "CountStkByEmpYear":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("5", "2"), "รายงานจำนวนรายการเช็คสต๊อก[รายปี]."); break;
                case "CountStockEmp":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("8", "2"), "รายงานรายละเอียดการนับสต๊อก[พนักงาน]."); break;
                case "CountStock50per":
                    FormClass.ShowNotCheckForm(new GeneralForm.MNCS.MNCS_Report("9", "2"), "รายการเช็คสต๊อกที่ไม่ลงรายการบัญชี."); break;
                default:
                    break;
            }
        }
        //tabBill
        private void RadTreeView_TabBill_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "BillOutDetail":
                    FormClass.ShowCheckFormNormal(new BillOut_Detail(""), "รายละเอียดบิล."); break;
                case "BillOutNotApv":
                    FormClass.ShowCheckForm(new BillOut_NotApv(), "รายการบิลค้างตรวจทั้งหมด."); break;
                case "BillOutImport":
                    FormClass.ShowCheckForm(new BillOut_Import(), "รายการบิลสำหรับ Import."); break;
                case "JOBMN_Main":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SUPC", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป SUPC"); break;
                case "JOBMN_Main_SUPC":
                    FormClass.ShowNotCheckForm(new JOB.General.JOBGeneral_Main("SHOP", "00003", "งานซ่อมทั่วไป", "0", ""), "งานซ่อมทั่วไป SHOP"); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Cofig_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CONFIGBRANCH_GenaralDetail":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("13", "เหตุผลการเบิกสินค้าที่สาขา", "0", "0", "1", "R", "5"), "เหตุผลการเบิกสินค้าที่สาขา."); break;
                case "CONFIGBRANCH_GenaralDetail_IS":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("14", "งบเบิกควบคุม", "1", "0", "1", "0", "0"), "งบเบิกควมคุม [IS]."); break;
                case "PERCENTSTOCK":
                    FormClass.ShowNotCheckForm(new FormShare.ConfigBranch_GenaralDetail("51", "ตั้งค่าเปอร์เซนต์ขาด-เกินสำหรับนับสต็อก", "1", "1", "1", "0", "0"), "ตั้งค่าเปอร์เซนต์ขาด-เกินสำหรับนับสต็อก."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Bill_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "Bill_MNIO_SUPC":
                    FormClass.ShowCheckFormNormal(new JOB.Bill.Bill_MNIO_OT("MNIO", "SUPC", "", "00009", "เบิกภายใน", "0"), "บิลนำออกไม่มีค่าใช้จ่าย SUPC [MNIO]."); break;
                case "Bill_MNIO_SHOP":
                    FormClass.ShowCheckFormNormal(new JOB.Bill.Bill_MNIO_OT("MNIO", "SHOP", "", "00009", "เบิกภายใน", "0"), "บิลนำออกไม่มีค่าใช้จ่าย SHOP [MNIO]."); break;
                default:
                    break;
            }
        }

        private void RadTreeView_Order_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "OrderInside_Main":
                    FormClass.ShowNotCheckForm(new JOB.OrderWoodWarehouse.OrderInside_Main(), "สั่งของจากโรงไม้เชิงทะเล."); break;

                default:
                    break;
            }
        }

        private void RadTreeView2_NodeMouseClick(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            switch (e.Node.Name)
            {
                case "CountStk":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Main("2"), "รายการนับสต็อก."); break;
                case "CountStkMN":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_Main("3"), "รายการนับสต็อก MN."); break;
                case "StockExcel":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_ImportItem("2"), "ปรับสต็อกสินค้าจาก Excel."); break;
                case "Batch":
                    FormClass.ShowCheckForm(new GeneralForm.MNCS.MNCS_ClearBatch(), "เคลียร์สต็อกชุดงาน."); break;
                default:
                    break;
            }
        }
    }
}
