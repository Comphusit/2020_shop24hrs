﻿namespace PC_Shop24Hrs.FormShare
{
    partial class InputDataGeneral_FromEmplID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputDataGeneral_FromEmplID));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radDateTimePicker_D1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remaek = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Unit = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_InputData = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_ForSave = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1Desc = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_2Desc = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_EmplID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Head = new Telerik.WinControls.UI.RadLabel();
            this.PrintDocument_LO = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_InputData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ForSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_2Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Controls.Add(this.RadButton_pdt);
            this.radGroupBox_DB.Controls.Add(this.radDateTimePicker_D1);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Name);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Remaek);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Unit);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_InputData);
            this.radGroupBox_DB.Controls.Add(this.radLabel_ForSave);
            this.radGroupBox_DB.Controls.Add(this.radLabel_1Desc);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radLabel_2Desc);
            this.radGroupBox_DB.Controls.Add(this.radLabel_2);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_EmplID);
            this.radGroupBox_DB.Controls.Add(this.radLabel_1);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Head);
            this.radGroupBox_DB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(455, 383);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(424, 12);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 72;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radDateTimePicker_D1
            // 
            this.radDateTimePicker_D1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radDateTimePicker_D1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D1.Location = new System.Drawing.Point(13, 91);
            this.radDateTimePicker_D1.Name = "radDateTimePicker_D1";
            this.radDateTimePicker_D1.Size = new System.Drawing.Size(175, 25);
            this.radDateTimePicker_D1.TabIndex = 59;
            this.radDateTimePicker_D1.TabStop = false;
            this.radDateTimePicker_D1.Text = "22/05/2020";
            this.radDateTimePicker_D1.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(95, 91);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(335, 23);
            this.radLabel_Name.TabIndex = 70;
            this.radLabel_Name.Text = ":";
            // 
            // radTextBox_Remaek
            // 
            this.radTextBox_Remaek.AcceptsReturn = true;
            this.radTextBox_Remaek.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Remaek.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remaek.Location = new System.Drawing.Point(31, 284);
            this.radTextBox_Remaek.Multiline = true;
            this.radTextBox_Remaek.Name = "radTextBox_Remaek";
            // 
            // 
            // 
            this.radTextBox_Remaek.RootElement.StretchVertically = true;
            this.radTextBox_Remaek.Size = new System.Drawing.Size(399, 50);
            this.radTextBox_Remaek.TabIndex = 3;
            this.radTextBox_Remaek.Tag = "";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(12, 259);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(192, 19);
            this.radLabel3.TabIndex = 68;
            this.radLabel3.Text = "หมายเหตุ";
            // 
            // radLabel_Unit
            // 
            this.radLabel_Unit.AutoSize = false;
            this.radLabel_Unit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Unit.Location = new System.Drawing.Point(385, 245);
            this.radLabel_Unit.Name = "radLabel_Unit";
            this.radLabel_Unit.Size = new System.Drawing.Size(45, 19);
            this.radLabel_Unit.TabIndex = 67;
            this.radLabel_Unit.Text = "บาท";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(12, 70);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(367, 19);
            this.radLabel2.TabIndex = 66;
            this.radLabel2.Text = "ระบุรหัสพนักงาน [Enter]";
            // 
            // radTextBox_InputData
            // 
            this.radTextBox_InputData.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_InputData.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_InputData.Location = new System.Drawing.Point(31, 226);
            this.radTextBox_InputData.MaxLength = 30;
            this.radTextBox_InputData.Name = "radTextBox_InputData";
            this.radTextBox_InputData.Size = new System.Drawing.Size(348, 25);
            this.radTextBox_InputData.TabIndex = 2;
            this.radTextBox_InputData.Text = "0054450";
            this.radTextBox_InputData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_InputData_KeyDown);
            this.radTextBox_InputData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_InputData_KeyPress);
            // 
            // radLabel_ForSave
            // 
            this.radLabel_ForSave.AutoSize = false;
            this.radLabel_ForSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_ForSave.Location = new System.Drawing.Point(12, 197);
            this.radLabel_ForSave.Name = "radLabel_ForSave";
            this.radLabel_ForSave.Size = new System.Drawing.Size(192, 19);
            this.radLabel_ForSave.TabIndex = 64;
            this.radLabel_ForSave.Text = "หัวข้อการบันทึก";
            // 
            // radLabel_1Desc
            // 
            this.radLabel_1Desc.AutoSize = false;
            this.radLabel_1Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_1Desc.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_1Desc.Location = new System.Drawing.Point(95, 125);
            this.radLabel_1Desc.Name = "radLabel_1Desc";
            this.radLabel_1Desc.Size = new System.Drawing.Size(335, 23);
            this.radLabel_1Desc.TabIndex = 62;
            this.radLabel_1Desc.Text = ":";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(104, 342);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(120, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(230, 342);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(120, 32);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_2Desc
            // 
            this.radLabel_2Desc.AutoSize = false;
            this.radLabel_2Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_2Desc.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_2Desc.Location = new System.Drawing.Point(95, 155);
            this.radLabel_2Desc.Name = "radLabel_2Desc";
            this.radLabel_2Desc.Size = new System.Drawing.Size(335, 23);
            this.radLabel_2Desc.TabIndex = 63;
            this.radLabel_2Desc.Text = ":";
            // 
            // radLabel_2
            // 
            this.radLabel_2.AutoSize = false;
            this.radLabel_2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_2.Location = new System.Drawing.Point(12, 156);
            this.radLabel_2.Name = "radLabel_2";
            this.radLabel_2.Size = new System.Drawing.Size(74, 19);
            this.radLabel_2.TabIndex = 60;
            this.radLabel_2.Text = "แผนก";
            // 
            // radTextBox_EmplID
            // 
            this.radTextBox_EmplID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmplID.Location = new System.Drawing.Point(12, 91);
            this.radTextBox_EmplID.MaxLength = 7;
            this.radTextBox_EmplID.Name = "radTextBox_EmplID";
            this.radTextBox_EmplID.Size = new System.Drawing.Size(74, 25);
            this.radTextBox_EmplID.TabIndex = 0;
            this.radTextBox_EmplID.Text = "0054450";
            this.radTextBox_EmplID.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_LO2_TextChanging);
            this.radTextBox_EmplID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmplID_KeyDown);
            this.radTextBox_EmplID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_EmplID_KeyPress);
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(12, 126);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(74, 19);
            this.radLabel_1.TabIndex = 39;
            this.radLabel_1.Text = "ตำแหน่ง";
            // 
            // radLabel_Head
            // 
            this.radLabel_Head.AutoSize = false;
            this.radLabel_Head.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Head.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Head.Location = new System.Drawing.Point(12, 24);
            this.radLabel_Head.Name = "radLabel_Head";
            this.radLabel_Head.Size = new System.Drawing.Size(406, 40);
            this.radLabel_Head.TabIndex = 44;
            this.radLabel_Head.Text = "หัวข้อการบันทึก";
            // 
            // PrintDocument_LO
            // 
            this.PrintDocument_LO.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_LO_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // InputDataGeneral_FromEmplID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(455, 383);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputDataGeneral_FromEmplID";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "บันทึกข้อมูล";
            this.Load += new System.EventHandler(this.InputDataGeneral_FromEmplID_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_InputData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ForSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_2Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadLabel radLabel_Head;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel_2;
        private Telerik.WinControls.UI.RadLabel radLabel_1Desc;
        private Telerik.WinControls.UI.RadLabel radLabel_2Desc;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private System.Drawing.Printing.PrintDocument PrintDocument_LO;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.RadLabel radLabel_ForSave;
        private Telerik.WinControls.UI.RadTextBox radTextBox_InputData;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Unit;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remaek;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D1;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
    }
}
