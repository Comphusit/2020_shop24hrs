﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using FingerScanURU4500;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.FormShare
{
    public partial class FingerScan : Telerik.WinControls.UI.RadForm
    {
        private MainVerificationForm Verification;
        private AppData Data;

        readonly string _pEmplID, _pEmpName, _pEmpPosition, _pEmpDpt;

        public FingerScan(string pEmplID, string pPageDesc, string pEmpName, string pEmpPosition, string pEmpDpt)
        {
            InitializeComponent();
            _pEmplID = ConfigClass.ReplaceMDPForEmlpID(pEmplID);//.Replace("M", "").Replace("P", "").Replace("D", "");
            _pEmpName = pEmpName;
            _pEmpPosition = pEmpPosition;
            _pEmpDpt = pEmpDpt;

            radLabel_Page.Text = pPageDesc;
            radLabel_EmplID.Text = _pEmplID;
            radLabel_EmplNAME.Text = _pEmpName;
            radLabel_Possion.Text = _pEmpPosition;
            radLabel_Dept.Text = _pEmpDpt;

            ImageEmp(_pEmplID);

        }

        private void FingerScan_Load(object sender, EventArgs e)
        {
            try
            {
                SQLQUERY.SetupSqlConnection();
                Data = new AppData();                               // Create the application data object
                Data.OnChange += delegate { ExchangeData(false); }; // Track data changes to keep the form synchronized    
                Data.OnClickPicBox += delegate { ButtonVerrificationPanel_Click(null, null); };

                ExchangeData(false);

                if (Verification != null && Verification.Visible)
                    Verification.Close();

                Verification = new MainVerificationForm
                {
                    TopLevel = false,
                    AutoScroll = true
                };

                panel5.Controls.Clear();
                panel5.Controls.Add(Verification);
                Verification.Dock = DockStyle.Fill;
                Verification.FormBorderStyle = FormBorderStyle.None;
                Verification.Show();

                ButtonVerrificationPanel_Click(null, null);
            }
            catch
            {
                Invoke(new Action(() => { this.DialogResult = DialogResult.No; Close(); }));
                return;
            }
        }
        //scan 
        private void ExchangeData(Boolean read)
        {
            if (read)
            {
                Data.EnrolledFingersMask = 0;
                Data.MaxEnrollFingerCount = 10;
                Data.IsFeatureSetMatched = false;
                Data.IsEventHandlerSucceeds = true;
                Data.DataTableFingerPrint = new DataTable();
                Data.Update();
            }
            else
            {

                if (Data.IsFeatureSetMatched)
                {
                    Invoke(new Action(() => { this.DialogResult = DialogResult.Yes; Close(); }));
                }
            }
        }

        private void ButtonVerrificationPanel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Verification != null && Verification.Visible)
                {
                    Data.EMPLID = this.radLabel_EmplID.Text;
                    ExchangeData(true);
                    Verification.Data = this.Data;
                    Verification.LoadFingerPrint();
                    if (Data.DataTableFingerPrint.Rows.Count == 0)
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบลายนิ้วมือของพนักงาน{Environment.NewLine}ลองใหม่อีกครั้ง");
                }
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error(ex.Message);
            }
        }


        //Set Image for Show
        public static Image Image_SpcByteArrayToAxCompanyImage(byte[] byteArrayImage)
        {
            int startIndex = 7;
            if (SystemClass.SystemAX2012 == "1") startIndex = 0;
            MemoryStream ms = new MemoryStream(byteArrayImage, startIndex, byteArrayImage.Length - startIndex);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        //load Image and Show
        public void ImageEmp(string pEmpID)
        {
            DataTable DtEmpImg = Class.Models.EmplClass.GetImage_byALTNUM(pEmpID);
            try
            {
                if (DtEmpImg.Rows.Count > 0)
                {
                    byte[] datas = (byte[])DtEmpImg.Rows[0]["IMAGE"];
                    pictureBox_EmplImg.Image = Image_SpcByteArrayToAxCompanyImage(datas);
                    pictureBox_EmplImg.SizeMode = PictureBoxSizeMode.Zoom;
                }
                else
                {
                    //System.IO.FileStream fs;
                    System.IO.FileStream fs = new System.IO.FileStream(PathImageClass.pImageEmply, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    pictureBox_EmplImg.Image = System.Drawing.Image.FromStream(fs);
                    pictureBox_EmplImg.SizeMode = PictureBoxSizeMode.Zoom;
                    fs.Close();
                }
            }
            catch (Exception)
            {
                //System.IO.FileStream fs;
                System.IO.FileStream fs = new System.IO.FileStream(PathImageClass.pImageEmply, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                pictureBox_EmplImg.Image = System.Drawing.Image.FromStream(fs);
                pictureBox_EmplImg.SizeMode = PictureBoxSizeMode.Zoom;
                fs.Close();
            }
        }
        //close Form
        private void FingerScan_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Verification != null && Verification.Visible)
                Verification.Close();
        }
    }
}
