﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.FormShare
{
    public partial class OTPGenerator : Telerik.WinControls.UI.RadForm
    {
        public string pInputData;
        public OTPGenerator()
        {
            InitializeComponent();
        }
        //load
        private void OTPGenerator_Load(object sender, EventArgs e)
        {
            radButton_Clear.ButtonElement.ShowBorder = true;
           
            radTextBox_PIN.SelectAll();
            radTextBox_PIN.Focus();
            radTextBox_OTP.ReadOnly = true;
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            DecrypteOTP(radTextBox_PIN.Text.Trim());
        }

        private void DecrypteOTP(string _pin)
        {
            Int64 cryptbase1, cryptbase2, decrypted;
            bool success = Int64.TryParse(_pin, out long encrypted);
            if (success)
            {
                cryptbase1 = Int64.Parse(DateTime.Now.Date.ToString("yyMMdd", new System.Globalization.CultureInfo("en-US")));
                cryptbase2 = Int64.Parse(DateTime.Now.Date.ToString("ddMMyy", new System.Globalization.CultureInfo("en-US")));
                decrypted = (encrypted ^ cryptbase2) - cryptbase1;
                radTextBox_OTP.Text = decrypted.ToString();
                radTextBox_OTP.Focus();
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("รหัส PIN ต้องเป็นหม่ายเลขเท่านั้น!");
                radTextBox_PIN.Focus();
            }
        }
        //Enter
        private void RadTextBox_PIN_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (!string.IsNullOrEmpty(radTextBox_OTP.Text)) {
                        radTextBox_OTP.Text = "";
                    }
                    DecrypteOTP(radTextBox_PIN.Text.Trim());
                    break;
                default:
                    break;
            }
        }

        private void RadButton_Clear_Click(object sender, EventArgs e)
        {
            
            radTextBox_PIN.Text = "";
            radTextBox_OTP.Text = "";
            radTextBox_PIN.Focus();

        }
    }
}
