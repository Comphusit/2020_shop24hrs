﻿//CheckOK
using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.FormShare
{
    public partial class EmpListInCorrect : Telerik.WinControls.UI.RadForm
    {
        public string pEmplID;
        public string pEmplName;
        public string pEmplDptID;
        public string pEmplDptNAME;
        public string pMoney;
        public string pRmk;

        readonly string _pType;//ประเภทรายการหัก
        readonly string _pDocno;//เลขที่บิลหัก
        readonly string _pDptID;//หักในแผนกไหน
        readonly string _pRemark;
        //readonly System.Data.DataTable DtItem = new System.Data.DataTable();
        readonly DataTable DtItem = new DataTable();
        private int sum = 0;
        public EmpListInCorrect(string pDocno, string pType, string pDptID, string pRemark)
        {
            InitializeComponent();

            _pType = pType;
            _pDocno = pDocno;
            _pDptID = pDptID;
            _pRemark = pRemark;

        }
        //Clear
        void ClearData()
        {
            radTextBox_EmplID.Text = "";
            radTextBox_Money.Text = "";
            radLabel_EmplName.Text = "";
            radLabel_DptID.Text = "";
            radLabel_DptName.Text = "";
            radTextBox_Money.Enabled = false;
            radTextBox_Bill.Focus();
        }
        //load
        private void EmpListInCorrect_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_AddEmpFormExcel.ButtonElement.ShowBorder = true; radButton_AddEmpFormExcel.ButtonElement.ToolTipText = "นำเข้ารายการจาก Excel";


            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน"; RadButton_pdt.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.ReadOnly = false;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_ID", "รหัส", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_NAME", "ชื่อพนักงาน", 240));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("GRAND", "ยอดหัก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_Dpt", "แผนก", 90));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_DptName", "ชื่อแผนก", 250));
            radGridView_Show.MasterTemplate.EnableFiltering = false;

            radGridView_Show.Columns["EMP_ID"].ReadOnly = true;
            radGridView_Show.Columns["EMP_NAME"].ReadOnly = true;
            radGridView_Show.Columns["EMP_Dpt"].ReadOnly = true;
            radGridView_Show.Columns["EMP_DptName"].ReadOnly = true;

            ConditionalFormattingObject c1 = new ConditionalFormattingObject("MyCondition1", ConditionTypes.Equal, "", "", true)
            {
                RowBackColor = ConfigClass.SetColor_Red(),
                CellBackColor = ConfigClass.SetColor_Red()
            };
            radGridView_Show.Columns["EMP_NAME"].ConditionalFormattingObjectList.Add(c1);

            //การแสดงยอดรวม แถวบนสุด
            GridViewSummaryItem summaryItem = new GridViewSummaryItem
            {
                Name = "GRAND",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };
            //summaryRowItem.Add(summaryItem);
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);

            ClearData();

            radTextBox_Type.Text = _pType; radTextBox_Type.Enabled = false;
            radTextBox_Remark.Text = _pRemark;
            radButton_Save.Enabled = false;
            radTextBox_Remark.Enabled = false;

            if (_pDocno != "")
            {
                radTextBox_Bill.Text = _pDocno; radTextBox_Bill.Enabled = false;
                radTextBox_EmplID.Focus();
            }
        }
        //ปิด
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //check Number
        #region "CheckInputNumber"
        //check Number
        private void RadTextBox_EmplID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
        //check Number
        private void RadTextBox_Money_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
        #endregion
        //Enter
        private void RadTextBox_EmplID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_EmplID.Text.Length != 7)
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("รหัสพนักงาน");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                {
                    if (radTextBox_EmplID.Text == radGridView_Show.Rows[ii].Cells["EMP_ID"].Value.ToString())
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานนี้มีในรายการหักเรียบร้อยแล้ว ไม่สามารถบันทึกเพิ่มได้.");
                        radTextBox_EmplID.SelectAll(); radTextBox_EmplID.Focus();
                        return;
                    }
                }

                //DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_EmplID.Text);
                DataTable dtEmp = Models.EmplClass.GetEmployee_Altnum(radTextBox_EmplID.Text);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                if (dtEmp.Rows[0]["IVZ_HRPAResignationDate"].ToString() != "1900-01-01")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานนี้ลาออกแล้ว ไม่สามารถทำรายการหักได้.");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                radLabel_EmplName.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                radLabel_DptID.Text = dtEmp.Rows[0]["NUM"].ToString();
                radLabel_DptName.Text = dtEmp.Rows[0]["DESCRIPTION"].ToString();
                radTextBox_Money.Enabled = true;
                radTextBox_Money.Focus();
            }
        }
        //Enter Money
        private void RadTextBox_Money_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Money.Text == "") { return; }

                if (int.Parse(radTextBox_Money.Text) > 0)
                {
                    if (String.IsNullOrEmpty(this.radTextBox_EmplID.Text))
                    {
                        int divide = int.Parse(radTextBox_Money.Text) / radGridView_Show.Rows.Count; //หารยอดหักที่รัยมา

                        foreach (GridViewRowInfo row in radGridView_Show.MasterView.Rows)
                        {
                            row.Cells["GRAND"].Value = divide; //นำค่าที่ได้ใส่ในยอดหักของแต่ละคน
                        }

                        radButton_Save.Enabled = true;
                        radTextBox_Remark.Enabled = true;

                        ClearData();
                    }
                    else
                    {
                        radButton_Save.Enabled = true;
                        radTextBox_Remark.Enabled = true;

                        radGridView_Show.Rows.Add(radTextBox_EmplID.Text, radLabel_EmplName.Text, int.Parse(radTextBox_Money.Text),
                                radLabel_DptID.Text, radLabel_DptName.Text);

                        // radGridView_Show.Rows[0]

                        ClearData();

                        radTextBox_EmplID.Focus();
                    }
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุยอดเงินหักต้องมากกว่า 0 เท่านั้น.");
                    return;
                }
            }
        }
        //save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_Bill.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("เลขที่บิลอ้างอิง");
                radTextBox_Bill.Focus();
                return;
            }
            if (radTextBox_Remark.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("หมายเหตุ");
                radTextBox_Remark.Focus();
                return;
            }

            if (radGridView_Show.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรายชื่อพนักงานที่หักให้เรียบร้อยก่อนบันทึก");
                radTextBox_EmplID.Focus();
                return;
            }

            SaveData();
        }

        //บันทึก
        private void SaveData()
        {
            ArrayList sql = new ArrayList();

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                sql.Add(string.Format(@"
                INSERT INTO [SHOP_EMPLOYEE_ListInCorrect] 
                ([DOCNO],[DPTIDINS],[TYPE],[WHOID],[WHONAME]
                ,[GRAND],[WHOIDINS],[WHONAMEINS],[REMARK],[DPTID],[DPTNAME]) VALUES 
                ('" + radTextBox_Bill.Text + @"','" + _pDptID + @"','" + _pType + @"',
                '" + radGridView_Show.Rows[i].Cells["EMP_ID"].Value.ToString() + @"',
                '" + radGridView_Show.Rows[i].Cells["EMP_NAME"].Value.ToString() + @"',
                '" + float.Parse(radGridView_Show.Rows[i].Cells["GRAND"].Value.ToString()) + @"',
                '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + "'," +
                "'" + radTextBox_Remark.Text + "'," +
                "'" + radGridView_Show.Rows[i].Cells["EMP_Dpt"].Value.ToString() + @"',
                '" + radGridView_Show.Rows[i].Cells["EMP_DptName"].Value.ToString() + @"') "));
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                if (_pDocno != "")
                {
                    pEmplID = radGridView_Show.Rows[0].Cells["EMP_ID"].Value.ToString();
                    pMoney = radGridView_Show.Rows[0].Cells["GRAND"].Value.ToString();
                    pEmplName = radGridView_Show.Rows[0].Cells["EMP_NAME"].Value.ToString();
                    pEmplDptID = radGridView_Show.Rows[0].Cells["EMP_Dpt"].Value.ToString();
                    pEmplDptNAME = radGridView_Show.Rows[0].Cells["EMP_DptName"].Value.ToString();
                    pRmk = radTextBox_Remark.Text;

                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
                else
                {
                    ClearData();
                    if (radGridView_Show.Rows.Count > 0) radGridView_Show.Rows.Clear();

                    radButton_Save.Enabled = false;
                    radTextBox_Remark.Text = "";
                    radTextBox_Bill.Text = ""; radTextBox_Bill.Focus();
                }
            }
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //F2 remove
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmDelete($@"พนักงาน {radGridView_Show.CurrentRow.Cells["EMP_NAME"].Value}") == DialogResult.Yes)
                {
                    radGridView_Show.Rows.RemoveAt(radGridView_Show.CurrentRow.Index);
                    if (sum == 0)
                    {
                        radTextBox_Money.Focus();
                        return;
                    }
                    radTextBox_EmplID.Focus();
                }
                else
                {
                    return;
                }
            }
        }
        //Set Enter
        private void RadTextBox_Bill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radTextBox_EmplID.Focus();
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pType);
        }

        private void RadButton_AddEmpFormExcel_Click(object sender, EventArgs e)
        {
            //string filePath = @"C:\SSO18-00061_2018_7-1.xls";
            OpenFileDialog fileDialog = new OpenFileDialog
            {
                FileName = @"",
                //fileDialog.Filter = "Excel Worksheets 2003(*.xls)|*.xls|Excel Worksheets 2007(*.xlsx)|*.xlsx|Word Documents(*.doc)|*.doc"
                Filter = "Excel Files|*.xls;*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (fileDialog.ShowDialog() != DialogResult.OK) //เลือกไฟล์ Excel
            {
                radTextBox_Bill.Focus();
                return;
            }

            try
            {
                ImportExcel(@fileDialog.FileName);
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Import ไฟล์ excel ได้{Environment.NewLine}{ex.Message}");
                radTextBox_Bill.Focus();
            }
        }

        public void ImportExcel(string excelFilename)
        {
            DataTable DtSet = new DataTable();
            int redcount = 0;
            string sheetName;

            System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
               ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + excelFilename +
               @"';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

            MyConnection.Open();
            DataTable dtExcelSheet = MyConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);

            if (dtExcelSheet == null)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลใน File Excel ที่ระบุ{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง");
                return;
            }

            if (dtExcelSheet.Rows.Count == 1) //ดึงข้อมูลใน Excel ในกรณีมีแค่ 1 เอกสารในไฟล์
            {
                System.Data.OleDb.OleDbDataAdapter MyCommand1 = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{dtExcelSheet.Rows[0]["TABLE_NAME"]}]  ", MyConnection);
                MyCommand1.TableMappings.Add("Table", "Net-informations.com");
                MyCommand1.Fill(DtSet);
            }
            else //ดึงข้อมูลใน Excel ในกรณีมีเอกสารมากกว่า 1 
            {
                ShowDataDGV frm = new ShowDataDGV("2")
                { dtData = dtExcelSheet };

                if (frm.ShowDialog(this) == DialogResult.Yes) sheetName = frm.pSheet; else return;

                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{sheetName}]  ", MyConnection);
                MyCommand.TableMappings.Add("Table", "Net-informations.com");
                MyCommand.Fill(DtSet);
            }


            //ไม่พบรายการหัก
            if (DtSet.Rows.Count == 0) { MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลสำหรับ File Excel ที่เลือก{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง"); return; }


            for (int i = 0; i < DtSet.Rows.Count; i++)
            {
                string EMPname, DptID, DptName;
                string id = DtSet.Rows[i][0].ToString();
                string EMPid = id.Replace("M", "").Replace("D", "").Replace("P", "");
                DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(EMPid);

                if (dtEmp.Rows.Count == 0) //ไม่พบรหัสพนักงาน, รหัสพนักงานผิด
                {
                    EMPname = "";
                    DptID = "";
                    DptName = "";
                    redcount++;
                }
                else
                {
                    EMPname = dtEmp.Rows[0]["SPC_NAME"].ToString();
                    DptID = dtEmp.Rows[0]["NUM"].ToString();
                    DptName = dtEmp.Rows[0]["DESCRIPTION"].ToString();
                }

                radGridView_Show.Rows.Add(EMPid, EMPname, DtSet.Rows[i][3].ToString(), DptID, DptName); //เพิ่มข้อมูลDGV

                sum += Convert.ToInt32(DtSet.Rows[i][3].ToString()); //ยอดหักรวม


            }

            if (redcount > 0) //กรณีมีรายการหักที่ รหัสพนักงานผิด
            {
                radTextBox_EmplID.Enabled = true;
                radButton_Save.Enabled = false;
                radTextBox_Remark.Enabled = false;
                radTextBox_EmplID.Focus();

                if (sum == 0) //และยอดหักรวมเท่ากับ 0
                {
                    radTextBox_Money.Enabled = true;
                    radButton_Save.Enabled = false;
                    radTextBox_Remark.Enabled = false;
                }
                return;
            }

            if (sum == 0) //กรณีรายการหักมียอดรวมเท่ากับ 0
            {
                radTextBox_Money.Enabled = true;
                radButton_Save.Enabled = false;
                radTextBox_Remark.Enabled = false;
                radTextBox_Money.Focus();
                return;
            }

            radTextBox_Bill.Focus();
            radButton_Save.Enabled = true;
            radTextBox_Remark.Enabled = true;
        }

    }
}
