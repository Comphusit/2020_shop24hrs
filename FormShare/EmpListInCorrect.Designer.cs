﻿namespace PC_Shop24Hrs.FormShare
{
    partial class EmpListInCorrect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmpListInCorrect));
            this.radLabel_Desc = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_EmplID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Money = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DptID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DptName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Bill = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Type = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radButton_AddEmpFormExcel = new Telerik.WinControls.UI.RadButton();
            this.radOpenFileDialog1 = new Telerik.WinControls.UI.RadOpenFileDialog();
            this.radOpenFolderDialog1 = new Telerik.WinControls.UI.RadOpenFolderDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Money)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_AddEmpFormExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel_Desc
            // 
            this.radLabel_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Desc.Location = new System.Drawing.Point(12, 155);
            this.radLabel_Desc.Name = "radLabel_Desc";
            this.radLabel_Desc.Size = new System.Drawing.Size(65, 19);
            this.radLabel_Desc.TabIndex = 26;
            this.radLabel_Desc.Text = "หมายเหตุ";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(12, 178);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(657, 86);
            this.radTextBox_Remark.TabIndex = 4;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(213, 591);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(120, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(338, 591);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(120, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_EmplID
            // 
            this.radTextBox_EmplID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmplID.Location = new System.Drawing.Point(131, 54);
            this.radTextBox_EmplID.MaxLength = 7;
            this.radTextBox_EmplID.Name = "radTextBox_EmplID";
            this.radTextBox_EmplID.Size = new System.Drawing.Size(147, 21);
            this.radTextBox_EmplID.TabIndex = 2;
            this.radTextBox_EmplID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmplID_KeyDown);
            this.radTextBox_EmplID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_EmplID_KeyPress);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(14, 54);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(116, 21);
            this.radLabel1.TabIndex = 30;
            this.radLabel1.Text = "<html>รหัส พนง [Enter]</html>";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(12, 121);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(104, 19);
            this.radLabel2.TabIndex = 31;
            this.radLabel2.Text = "ยอดหัก [Enter]";
            // 
            // radTextBox_Money
            // 
            this.radTextBox_Money.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Money.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Money.Location = new System.Drawing.Point(131, 119);
            this.radTextBox_Money.MaxLength = 7;
            this.radTextBox_Money.Name = "radTextBox_Money";
            this.radTextBox_Money.Size = new System.Drawing.Size(147, 21);
            this.radTextBox_Money.TabIndex = 3;
            this.radTextBox_Money.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Money_KeyDown);
            this.radTextBox_Money.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Money_KeyPress);
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(284, 121);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(79, 19);
            this.radLabel3.TabIndex = 33;
            this.radLabel3.Text = "บาท";
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.AutoSize = false;
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplName.Location = new System.Drawing.Point(284, 54);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(385, 23);
            this.radLabel_EmplName.TabIndex = 34;
            this.radLabel_EmplName.Text = "ชื่อผู้ถูกหัก";
            // 
            // radLabel_DptID
            // 
            this.radLabel_DptID.AutoSize = false;
            this.radLabel_DptID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_DptID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DptID.Location = new System.Drawing.Point(131, 85);
            this.radLabel_DptID.Name = "radLabel_DptID";
            this.radLabel_DptID.Size = new System.Drawing.Size(147, 23);
            this.radLabel_DptID.TabIndex = 35;
            this.radLabel_DptID.Text = "D156";
            // 
            // radLabel_DptName
            // 
            this.radLabel_DptName.AutoSize = false;
            this.radLabel_DptName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_DptName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DptName.Location = new System.Drawing.Point(284, 85);
            this.radLabel_DptName.Name = "radLabel_DptName";
            this.radLabel_DptName.Size = new System.Drawing.Size(382, 23);
            this.radLabel_DptName.TabIndex = 36;
            this.radLabel_DptName.Text = "D156";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(12, 85);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(104, 23);
            this.radLabel5.TabIndex = 37;
            this.radLabel5.Text = "แผนก";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(12, 270);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = false;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(657, 315);
            this.radGridView_Show.TabIndex = 41;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(14, 20);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(116, 21);
            this.radLabel4.TabIndex = 42;
            this.radLabel4.Text = "<html>บิลหักอ้างอิง</html>";
            // 
            // radTextBox_Bill
            // 
            this.radTextBox_Bill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Bill.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Bill.Location = new System.Drawing.Point(131, 20);
            this.radTextBox_Bill.MaxLength = 500;
            this.radTextBox_Bill.Name = "radTextBox_Bill";
            this.radTextBox_Bill.Size = new System.Drawing.Size(147, 21);
            this.radTextBox_Bill.TabIndex = 0;
            this.radTextBox_Bill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Bill_KeyDown);
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(284, 21);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(116, 21);
            this.radLabel6.TabIndex = 44;
            this.radLabel6.Text = "ประเภทการหัก";
            // 
            // radTextBox_Type
            // 
            this.radTextBox_Type.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Type.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Type.Location = new System.Drawing.Point(396, 21);
            this.radTextBox_Type.MaxLength = 7;
            this.radTextBox_Type.Name = "radTextBox_Type";
            this.radTextBox_Type.Size = new System.Drawing.Size(162, 21);
            this.radTextBox_Type.TabIndex = 1;
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(646, 9);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 72;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radButton_AddEmpFormExcel
            // 
            this.radButton_AddEmpFormExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_AddEmpFormExcel.BackColor = System.Drawing.Color.Transparent;
            this.radButton_AddEmpFormExcel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_AddEmpFormExcel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_AddEmpFormExcel.Image = global::PC_Shop24Hrs.Properties.Resources.add_user;
            this.radButton_AddEmpFormExcel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_AddEmpFormExcel.Location = new System.Drawing.Point(640, 146);
            this.radButton_AddEmpFormExcel.Name = "radButton_AddEmpFormExcel";
            this.radButton_AddEmpFormExcel.Size = new System.Drawing.Size(26, 26);
            this.radButton_AddEmpFormExcel.TabIndex = 73;
            this.radButton_AddEmpFormExcel.Text = "radButton3";
            this.radButton_AddEmpFormExcel.Click += new System.EventHandler(this.RadButton_AddEmpFormExcel_Click);
            // 
            // EmpListInCorrect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(678, 630);
            this.Controls.Add(this.radButton_AddEmpFormExcel);
            this.Controls.Add(this.RadButton_pdt);
            this.Controls.Add(this.radTextBox_Type);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radTextBox_Bill);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radGridView_Show);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel_DptName);
            this.Controls.Add(this.radLabel_DptID);
            this.Controls.Add(this.radLabel_EmplName);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radTextBox_Money);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_EmplID);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.radLabel_Desc);
            this.Controls.Add(this.radTextBox_Remark);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmpListInCorrect";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ยืนยันการบันทึกรายการหัก";
            this.Load += new System.EventHandler(this.EmpListInCorrect_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Money)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_AddEmpFormExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Money;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
        private Telerik.WinControls.UI.RadLabel radLabel_DptID;
        private Telerik.WinControls.UI.RadLabel radLabel_DptName;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Bill;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Type;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadButton radButton_AddEmpFormExcel;
        private Telerik.WinControls.UI.RadOpenFileDialog radOpenFileDialog1;
        private Telerik.WinControls.UI.RadOpenFolderDialog radOpenFolderDialog1;
    }
}
