﻿namespace PC_Shop24Hrs.FormShare.ShowImage
{
    partial class UploadImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadImage));
            this.pictureBox_Show = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radBrowseEditor_choose = new Telerik.WinControls.UI.RadBrowseEditor();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_OpenView = new Telerik.WinControls.UI.RadButton();
            this.radButton_Upload = new Telerik.WinControls.UI.RadButton();
            this.RadButton_cap = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Show)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBrowseEditor_choose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Upload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_cap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_Show
            // 
            this.pictureBox_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_Show.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_Show.Name = "pictureBox_Show";
            this.pictureBox_Show.Size = new System.Drawing.Size(790, 525);
            this.pictureBox_Show.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Show.TabIndex = 0;
            this.pictureBox_Show.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.pictureBox_Show);
            this.panel1.Location = new System.Drawing.Point(1, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(790, 525);
            this.panel1.TabIndex = 1;
            // 
            // radBrowseEditor_choose
            // 
            this.radBrowseEditor_choose.Font = new System.Drawing.Font("Tahoma", 11F);
            this.radBrowseEditor_choose.ForeColor = System.Drawing.Color.Blue;
            this.radBrowseEditor_choose.Location = new System.Drawing.Point(39, 10);
            this.radBrowseEditor_choose.Name = "radBrowseEditor_choose";
            this.radBrowseEditor_choose.Size = new System.Drawing.Size(367, 24);
            this.radBrowseEditor_choose.TabIndex = 2;
            this.radBrowseEditor_choose.ValueChanging += new Telerik.WinControls.UI.ValueChangingEventHandler(this.RadBrowseEditor_choose_ValueChanging);
            this.radBrowseEditor_choose.ValueChanged += new System.EventHandler(this.RadBrowseEditor_choose_ValueChanged);
            this.radBrowseEditor_choose.Click += new System.EventHandler(this.RadBrowseEditor_choose_Click);
            ((Telerik.WinControls.UI.RadBrowseEditorElement)(this.radBrowseEditor_choose.GetChildAt(0))).Text = "(none)";
            ((Telerik.WinControls.UI.BrowseEditorButton)(this.radBrowseEditor_choose.GetChildAt(0).GetChildAt(3).GetChildAt(1))).Text = "...";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radBrowseEditor_choose.GetChildAt(0).GetChildAt(3).GetChildAt(1).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radBrowseEditor_choose.GetChildAt(0).GetChildAt(3).GetChildAt(1).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(688, 11);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 24);
            this.radButton_Cancel.TabIndex = 46;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Visible = false;
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_OpenView
            // 
            this.radButton_OpenView.BackColor = System.Drawing.Color.Transparent;
            this.radButton_OpenView.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_OpenView.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_OpenView.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_OpenView.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_OpenView.Location = new System.Drawing.Point(7, 9);
            this.radButton_OpenView.Name = "radButton_OpenView";
            this.radButton_OpenView.Size = new System.Drawing.Size(26, 26);
            this.radButton_OpenView.TabIndex = 63;
            this.radButton_OpenView.Text = "radButton3";
            this.radButton_OpenView.Click += new System.EventHandler(this.RadButton_OpenView_Click);
            // 
            // radButton_Upload
            // 
            this.radButton_Upload.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Upload.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Upload.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Upload.Location = new System.Drawing.Point(448, 9);
            this.radButton_Upload.Name = "radButton_Upload";
            this.radButton_Upload.Size = new System.Drawing.Size(98, 26);
            this.radButton_Upload.TabIndex = 64;
            this.radButton_Upload.Text = "Upload รูป ";
            this.radButton_Upload.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton_Upload.Click += new System.EventHandler(this.RadButton_Upload_Click);
            // 
            // RadButton_cap
            // 
            this.RadButton_cap.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_cap.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_cap.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_cap.Image = global::PC_Shop24Hrs.Properties.Resources.screenshot;
            this.RadButton_cap.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_cap.Location = new System.Drawing.Point(409, 9);
            this.RadButton_cap.Name = "RadButton_cap";
            this.RadButton_cap.Size = new System.Drawing.Size(26, 26);
            this.RadButton_cap.TabIndex = 65;
            this.RadButton_cap.Text = "radButton3";
            this.RadButton_cap.Click += new System.EventHandler(this.RadButton_cap_Click);
            // 
            // UploadImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(792, 570);
            this.Controls.Add(this.RadButton_cap);
            this.Controls.Add(this.radButton_Upload);
            this.Controls.Add(this.radButton_OpenView);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.radBrowseEditor_choose);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UploadImage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upload Image";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UploadImage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Show)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radBrowseEditor_choose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_OpenView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Upload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_cap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Show;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadBrowseEditor radBrowseEditor_choose;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadButton radButton_OpenView;
        private Telerik.WinControls.UI.RadButton radButton_Upload;
        private Telerik.WinControls.UI.RadButton RadButton_cap;
    }
}
