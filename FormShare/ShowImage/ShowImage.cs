﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.IO;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.FormShare.ShowImage
{
    public partial class ShowImage : Telerik.WinControls.UI.RadForm
    {
        public string pathImg;

        string pathFolder;
        string pathFileWant;
        public ShowImage()
        {
            InitializeComponent();
        }

        #region "Rows"      
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        private void ShowImage_Load(object sender, EventArgs e)
        {
            radLabel_Desc.Text = "";
            radLabel_DateTime.Text = "";

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("ex", "ตัวอย่าง", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("FullPath", "path")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("NameFile", "NameFile")));
            radGridView_Show.TableElement.RowHeight = 90;

            radGridView_Show.MasterTemplate.EnableFiltering = false;

            FindImage();
        }

        void FindImage()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dt = new DataTable();
            dt.Columns.Add("ex", typeof(Image));
            dt.Columns.Add("FullPath", typeof(string));
            dt.Columns.Add("NameFile", typeof(string));

            string[] str = pathImg.Split('|');
            pathFolder = str[0];
            pathFileWant = str[1];
            DirectoryInfo DirInfo = new DirectoryInfo(str[0]);
            try
            {
                FileInfo[] FilesSPC = DirInfo.GetFiles(str[1], SearchOption.AllDirectories);

                for (int i = 0; i < FilesSPC.Length; i++)
                {
                    dt.Rows.Add(ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesSPC[i].FullName), FilesSPC[i].FullName, FilesSPC[i].Name);
                }
                radGridView_Show.DataSource = dt;

                Bitmap img = new Bitmap(radGridView_Show.Rows[0].Cells[1].Value.ToString());
                pictureBox_Full.Image = img;
                FindNameByPath(radGridView_Show.Rows[0].Cells[1].Value.ToString(), radGridView_Show.Rows[0].Cells[2].Value.ToString());
                this.Cursor = Cursors.Default;
            }
            catch (Exception)
            {
                this.Cursor = Cursors.Default;
                return;
            }
        }


        private void PictureBox_Full_DoubleClick(object sender, EventArgs e)
        {
            if (pictureBox_Full.Image == null) return;
            else System.Diagnostics.Process.Start(radGridView_Show.CurrentRow.Cells[1].Value.ToString());
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                Bitmap img = new Bitmap(radGridView_Show.CurrentRow.Cells[1].Value.ToString());
                pictureBox_Full.Image = img;
                FindNameByPath(radGridView_Show.CurrentRow.Cells[1].Value.ToString(), radGridView_Show.CurrentRow.Cells[2].Value.ToString());
            }
            catch (Exception)
            {
                pictureBox_Full.Image.Dispose();
                return;
            }
        }

        //NameTakeImage

        void FindNameByPath(string pNameFullName, string pFileName)
        {
            //radLabel_DateTime.Text = "วันที่-เวลา ถ่ายรูป : " + File.GetCreationTimeUtc(pNameFullName).ToString("dd-MM-yyyy HH:mm:ss");
            radLabel_DateTime.Text = "วันที่-เวลา ถ่ายรูป : " + File.GetCreationTime(pNameFullName).ToString("dd-MM-yyyy HH:mm:ss");

            string whoIns = "", toolTake = "";

            if ((pathFolder.Contains("BillSupc")) || (pathFolder.Contains("AssetClaimLeave")) || (pathFolder.Contains("IDM")))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    string[] C = B[5].Split('(');
                    whoIns = B[6];
                    toolTake = BillOutClass.FindTab_ByIME(C[1].Replace(")", ""));
                    if (toolTake == "")
                    { toolTake = C[1].Replace(")", ""); }
                }
                catch (Exception) { }
            }
            //MJOB190507000005-MN018-20190507-114752-M1104146
            if (pathFolder.Contains("JOB"))
            {
                try
                {
                    string[] A = pFileName.Split('-');
                    //string[] B = A[0].Split('-');
                    //string[] C = B[5].Split('(');
                    whoIns = A[4].ToUpper().Replace(".JPG", "").Replace("@", "");
                    toolTake = "JOB";

                }
                catch (Exception) { }
            }
            //
            if (pathFolder.Contains("LO"))
            {//CarO-MN033-2020-07-01-141507-M1705090_LO20-0069844,B@03052921
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    //string[] C = B[5].Split('(');
                    whoIns = B[6];
                    toolTake = "TAG";

                }
                catch (Exception) { }
            }
            //HJOB200702000007-MN069-2020-07-02-144418-M1502046 
            if (pathFolder.Contains("JOBHDBCH"))
            {
                try
                {
                    string[] A = pFileName.Split('-');
                    whoIns = A[6].ToUpper().Replace(".JPG", "");
                    toolTake = "PDA ประจำสาขา";
                }
                catch (Exception) { }
            }
            //RA-RAINVOICE-2020-07-12-131922(354343094265663)-M1803025_RA2007-00033_S
            if (pathFolder.Contains("RAINVOICE"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    string[] C = B[5].Split('(');
                    string[] D = B[6].Split('_');
                    whoIns = D[0];
                    toolTake = BillOutClass.FindTab_ByIME(C[1].Replace(")", ""));
                    if (toolTake == "")
                    { toolTake = C[1].Replace(")", ""); }
                }
                catch (Exception) { }
            }
            //Edit-MN059-2020-07-16-121509-M1907069_47,8850298110002,T2007281-0001848
            if (pathFolder.Contains("BillEdit"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    //string[] C = B[5].Split('(');
                    //string[] D = B[6].Split('_');
                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //RBox-MN001-2020-07-19-131831-M1410031_20353-0057351-027774133989-T2007774-0002404-Q
            if (pathFolder.Contains("ReciveBOX"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //SUPC-MN002-2020-07-21-135430-M1807068_LO20-0077410
            if (pathFolder.Contains("SUPC"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //UBill-MN005-2020-07-06-140821-M1810137_U2007Z05-0000009
            if (pathFolder.Contains("BillU"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //All-0000-MN003-2019-11-03-001151-M1711005_A005
            if (pathFolder.Contains("All"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    whoIns = B[7];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //CN-MN003-2020-08-31-033207-M1605078_MNRD200831000036
            if (pathFolder.Contains("CN"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //Shelf-MN022-2020-09-01-093643-M1603058
            if (pathFolder.Contains("Shelf"))
            {
                try
                {
                    string[] A = pFileName.Split('.');
                    string[] B = A[0].Split('-');
                    whoIns = B[6];
                    if (whoIns.Contains("_"))
                    {
                        string[] C = whoIns.Split('_');
                        whoIns = C[0];
                    }
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //Vender-MN003-2020-09-10-145330-M1707074_V013037#18854788000368,A,10,[F]
            if (pathFolder.Contains("Vender"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');
                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";

                }
                catch (Exception) { }
            }
            //MN086-20200925-174131-1707105
            if (pathFolder.Contains("PLANMN"))
            {
                try
                {
                    string[] A = pFileName.Split('-');
                    string[] B = A[3].Split('.');
                    whoIns = B[0];
                    toolTake = "UPLOAD PC";
                }
                catch (Exception) { }
            }
            //MP-MN045-2020-09-18-092401-M1302001_MP140200918001,08851123233026,3,[F]
            if (pathFolder.Contains("MP"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');

                    whoIns = B[6];
                    toolTake = "UPLOAD PC";
                }
                catch (Exception) { }
            }
            //ChkI-MN022-2020-10-16-042652-M1510093_030779005631-T2010779-0000478#8850170100794
            if (pathFolder.Contains("CheckItemRecive"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');

                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";
                }
                catch (Exception) { }
            }
            //2020-10-31-20201021155338_M060409.jpg
            if (pathFolder.Contains("CheckPrice"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    whoIns = A[1].Replace(".jpg", "");
                    toolTake = "Upload PC";
                    radLabel_DateTime.Text = "วันที่สิ้นสุดการเล่น File >> " + A[0].Substring(0, 10);
                }
                catch (Exception) { radLabel_DateTime.Text = ""; radLabel_Desc.Text = ""; }
            }
            //P1110202_80-8668_2021-07-15,WCB-AF
            if (pathFolder.Contains("VEHICLEWASH"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    whoIns = A[0];
                    toolTake = "Upload CARCARE";
                }
                catch (Exception) { radLabel_DateTime.Text = ""; radLabel_Desc.Text = ""; }
            }
            ////StkMN-MN003-2022-03-01-074056-M1809144_MNOS220301000031#2007077
            if (pathFolder.Contains("SendSTK"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');

                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";
                }
                catch (Exception) { radLabel_DateTime.Text = ""; radLabel_Desc.Text = ""; }
            }
            //Sanyo-MN010-2022-07-20-203837(นาบอน)-M1907100_159929_C.jpg
            if (pathFolder.Contains("Sanyo"))
            {
                try
                {
                    string[] A = pFileName.Split('_');
                    string[] B = A[0].Split('-');

                    whoIns = B[6];
                    toolTake = "PDA ประจำสาขา";
                }
                catch (Exception) { radLabel_DateTime.Text = ""; radLabel_Desc.Text = ""; }
            }

            FindNameTake(whoIns, toolTake);
        }

        void FindNameTake(string pEmpID, string pToolTake)
        {
            if (pEmpID != "")
            {
                DataTable dt = Class.Models.EmplClass.GetEmployeeDetail_ByEmplID(pEmpID.Replace("M", "").Replace("D", "").Replace("P", ""));
                if (dt.Rows.Count == 0)
                {
                    radLabel_Desc.Text = "{" + pToolTake + "} ไม่สามารถระบุผู้ถ่ายได้.";
                }
                else
                {
                    radLabel_Desc.Text = "{" + pToolTake + "} ผู้ถ่ายรูป - " + dt.Rows[0]["EMPLID"].ToString() + " " + dt.Rows[0]["SPC_NAME"].ToString() +
                   " [" + dt.Rows[0]["NUM"].ToString() + " " + dt.Rows[0]["DESCRIPTION"].ToString() + "]";
                }
            }
            else
            {
                radLabel_Desc.Text = "{" + pToolTake + "} ไม่สามารถระบุผู้ถ่ายได้.";
            }
        }

    }
}
