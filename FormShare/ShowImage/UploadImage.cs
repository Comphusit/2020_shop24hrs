﻿//CheckOK
using System;
using System.IO;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.FormShare.ShowImage
{
    public partial class UploadImage : Telerik.WinControls.UI.RadForm
    {
        readonly string _pPathImage;
        readonly string _pFileName;
        readonly string _ptblName;
        readonly string _pFiledNameUp;
        readonly string _pFiledNameCondition;
        readonly string _pValueCondition;

        public UploadImage(string pPathImage, string pFileName, string ptblName,
            string pFiledNameUp, string pFiledNameCondition, string pValueCondition, string pShowCap) //pShowCap = 1 คือ ให้ Open Capture ได้ 0 ไม่ได้
        {
            InitializeComponent();

            _pPathImage = pPathImage;
            _pFileName = pFileName;
            _ptblName = ptblName;
            _pFiledNameUp = pFiledNameUp;
            _pFiledNameCondition = pFiledNameCondition;
            _pValueCondition = pValueCondition;

            if (pShowCap == "1") RadButton_cap.Enabled = true; else RadButton_cap.Enabled = false;
        }
        //clear
        void ClearData()
        {
            radButton_Upload.Enabled = false;
            radBrowseEditor_choose.Enabled = true;
            if (!(radBrowseEditor_choose is null)) radBrowseEditor_choose.Value = null;

            if (pictureBox_Show.Image != null)
            {
                pictureBox_Show.Image.Dispose();
                pictureBox_Show.Image = null;
            }
        }
        //Load
        private void UploadImage_Load(object sender, EventArgs e)
        {
            ClearData();

            radBrowseEditor_choose.BrowseElement.Font = SystemClass.SetFontGernaral;
            radBrowseEditor_choose.BrowseElement.BrowseButton.Font = SystemClass.SetFontGernaral;

            this.radBrowseEditor_choose.DialogType = BrowseEditorDialogType.OpenFileDialog;
            this.radBrowseEditor_choose.ReadOnly = true;

            radBrowseEditor_choose.BrowseElement.ToolTipText = "เลือกรูปที่ต้องการ";
            radButton_OpenView.ButtonElement.ShowBorder = true; radButton_OpenView.ButtonElement.ToolTipText = "เปลี่ยนรูปใหม่";
            radButton_Upload.ButtonElement.ShowBorder = true; radButton_Upload.ButtonElement.ToolTipText = "Upload";
            RadButton_cap.ButtonElement.ShowBorder = true; RadButton_cap.ButtonElement.ToolTipText = "Capture";
            radButton_Upload.ButtonElement.Font = SystemClass.SetFontGernaral;

        }

        //Value Change
        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;

            try
            {
                this.pictureBox_Show.Image = ImageClass.ScaleImageData_SendPath(this.radBrowseEditor_choose.Value);
                radBrowseEditor_choose.Enabled = false;
                radButton_Upload.Enabled = true;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถแสดงรูปที่เลือกได้ ลองใหม่อีกครั้ง [{ex.Message}]");
                return;
            }

        }
        //value change
        private void RadBrowseEditor_choose_ValueChanging(object sender, Telerik.WinControls.UI.ValueChangingEventArgs e)
        {
            try
            {
                e.Cancel = !File.Exists(e.NewValue.ToString());
                return;
            }
            catch (Exception)
            {
                return;
            }
        }
        //browse
        private void RadBrowseEditor_choose_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = (OpenFileDialog)radBrowseEditor_choose.Dialog;
            dialog.Filter = "JPG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|GIF Files (*.gif)|*.gif|JPEG Files (*.jpeg)|*.jpeg";
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        ////Upload
        private void RadButton_Upload_Click(object sender, EventArgs e)
        {
            //Check File 
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == ""))
            {
                MessageBox.Show("เลือกรูปให้เรียบร้อยก่อนการ Upload.", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //Upload
            try
            {
                string DelSta = "0";
                string DelText = "ไม่ได้ลบรูปต้นทาง";
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("เมื่อทำการ Upload รูปเรียบร้อยแล้ว ต้องการลบรูปต้นทางหรือไม่ ?.") == DialogResult.Yes)
                {
                    DelSta = "1";
                    DelText = "ลบรูปต้นทางเรียบร้อยแล้ว";
                }
                //check Path
                if (Directory.Exists(_pPathImage) == false) Directory.CreateDirectory(_pPathImage);

                File.Copy(radBrowseEditor_choose.Value.ToString(), _pPathImage + _pFileName);

                pictureBox_Show.Image.Dispose();
                pictureBox_Show.Image = null;

                if (DelSta == "1")
                {
                    File.Delete(radBrowseEditor_choose.Value.ToString());
                }

                if (_ptblName != string.Empty)
                {
                    string upStr = string.Format(@"Update " + _ptblName + "  " +
                        "SET " + _pFiledNameUp + " =  '1'   " +
                        "WHERE " + _pFiledNameCondition + " = '" + _pValueCondition + "'");
                    string UpStatus = ConnectionClass.ExecuteSQL_Main(upStr);
                    if (UpStatus == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"Upload รูปเสร็จสมบูรณ์ [{DelText} ].");
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                        return;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สาามารถ Upload รูปได้ ลองใหม่อีกครั้ง [ {UpStatus} ].");
                        return;
                    }
                }
                else
                {

                    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"Upload รูปเสร็จสมบูรณ์ [ {DelText} ].");
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    return;
                }

            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สาามารถ Upload รูปได้ ลองใหม่อีกครั้ง [ { ex.Message } ].");
                return;
            }
        }

        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //
        private void RadButton_cap_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
            return;
        }

    }
}

