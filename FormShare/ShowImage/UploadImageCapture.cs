﻿//CheckOK
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.FormShare.ShowImage
{
    public partial class UploadImageCapture : Telerik.WinControls.UI.RadForm
    {
        readonly string _pPathImage;
        readonly string _pFileName;
        readonly string _ptblName;
        readonly string _pFiledNameUp;
        readonly string _pFiledNameCondition;
        readonly string _pValueCondition;
        readonly Bitmap bmp;
        //Load
        public UploadImageCapture(Int32 x, Int32 y, Int32 w, Int32 h, Size s, string pPathImage, string pFileName, string ptblName,
          string pFiledNameUp, string pFiledNameCondition, string pValueCondition)
        {
            InitializeComponent();

            Rectangle rect = new Rectangle(x, y, w, h);
            bmp = new Bitmap(rect.Width, rect.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen(rect.Left, rect.Top, 0, 0, s, CopyPixelOperation.SourceCopy);
            pictureBox_Show.Image = bmp;

            _pPathImage = pPathImage;
            _pFileName = pFileName;
            _ptblName = ptblName;
            _pFiledNameUp = pFiledNameUp;
            _pFiledNameCondition = pFiledNameCondition;
            _pValueCondition = pValueCondition;
        }
        //Load
        private void UploadImageCapture_Load(object sender, EventArgs e)
        {
            radButton_Upload.ButtonElement.ShowBorder = true;
            radButton_Upload.ButtonElement.ToolTipText = "Update รูป";
            radButton_Upload.ButtonElement.Font = SystemClass.SetFontGernaral;
        }
        //Up Image
        private void RadButton_Upload_Click(object sender, EventArgs e)
        {
            //Upload
            try
            {
                //check Path
                if (Directory.Exists(_pPathImage) == false)
                {
                    Directory.CreateDirectory(_pPathImage);
                }

                bmp.Save(_pPathImage + _pFileName);

                pictureBox_Show.Image.Dispose();
                pictureBox_Show.Image = null;

                if (_ptblName != string.Empty)
                {
                    string upStr = string.Format(@"Update " + _ptblName + "  " +
                        "SET " + _pFiledNameUp + " =  '1'   " +
                        "WHERE " + _pFiledNameCondition + " = '" + _pValueCondition + "'");
                    string UpStatus = ConnectionClass.ExecuteSQL_Main(upStr);
                    if (UpStatus == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("Upload รูปเสร็จสมบูรณ์.");

                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                        return;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Upload รูปได้ ลองใหม่อีกครั้ง.{Environment.NewLine}{UpStatus}");
                        return;
                    }
                }
                else
                {

                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("Upload รูปเสร็จสมบูรณ์ .");

                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    return;
                }

            }
            catch (Exception ex)
            {
               MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Upload รูปได้ ลองใหม่อีกครั้ง.{Environment.NewLine}{ ex.Message}");
                return;
            }
        }
    }
}

