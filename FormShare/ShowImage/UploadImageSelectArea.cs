﻿//CheckOK
using System;
using System.Drawing;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using System.Runtime.InteropServices;

namespace PC_Shop24Hrs.FormShare.ShowImage
{
    public partial class UploadImageSelectArea : Telerik.WinControls.UI.RadForm
    {
        
        public Int32 X;
        public Int32 Y;
        public Int32 W;
        public Int32 H;
        public Size S;
        public string aa;

        readonly string _pType;//สำหรับกรณีที่ Save Auto = 1 ส่วน 0 คือการเข้าหน้า Upload รูป
        readonly string _pPathImage;
        readonly string _pFileName;
        readonly string _ptblName;
        readonly string _pFiledNameUp;
        readonly string _pFiledNameCondition;
        readonly string _pValueCondition;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HTCAPTION = 0x2;

        [DllImport("User32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);


        public UploadImageSelectArea(string pType, string pPathImage, string pFileName, string ptblName, string pFiledNameUp, string pFiledNameCondition, string pValueCondition)
        {
            InitializeComponent();

            _pType = pType;
            _pPathImage = pPathImage;
            _pFileName = pFileName;
            _ptblName = ptblName;
            _pFiledNameUp = pFiledNameUp;
            _pFiledNameCondition = pFiledNameCondition;
            _pValueCondition = pValueCondition;


            this.Opacity = .5D;
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw, true); // this is to avoid visual artifacts
        }

        protected override void OnPaint(PaintEventArgs e) // you can safely omit this method if you want
        {
            e.Graphics.FillRectangle(Brushes.Green, Top);
            e.Graphics.FillRectangle(Brushes.Green, Left);
            e.Graphics.FillRectangle(Brushes.Green, Right);
            e.Graphics.FillRectangle(Brushes.Green, Bottom);
        }

        private const int
            HTLEFT = 10,
            HTRIGHT = 11,
            HTTOP = 12,
            HTTOPLEFT = 13,
            HTTOPRIGHT = 14,
            HTBOTTOM = 15,
            HTBOTTOMLEFT = 16,
            HTBOTTOMRIGHT = 17;

        const int _ = 10; // you can rename this variable if you like

        new Rectangle Top { get { return new Rectangle(0, 0, this.ClientSize.Width, _); } }
        new Rectangle Left { get { return new Rectangle(0, 0, _, this.ClientSize.Height); } }
        new Rectangle Bottom { get { return new Rectangle(0, this.ClientSize.Height - _, this.ClientSize.Width, _); } }
        new Rectangle Right { get { return new Rectangle(this.ClientSize.Width - _, 0, _, this.ClientSize.Height); } }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
            }
        }

        private void UploadImageSelectArea_Load(object sender, EventArgs e)
        {
      
        }
        //Cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Cap_Click(object sender, EventArgs e)
        {
            if (_pType == "0")
            {
                this.Hide();
                UploadImageCapture save = new UploadImageCapture(this.Location.X, this.Location.Y, this.Width, this.Height, this.Size,
                     _pPathImage, _pFileName, _ptblName, _pFiledNameUp, _pFiledNameCondition, _pValueCondition);
                if (save.ShowDialog(this) == DialogResult.Yes)
                {
                    aa = "1";
                }
                this.Close();
    
            }
            else
            {
                try
                {
                    Rectangle form = this.Bounds;
                    Bitmap bitmap = new Bitmap(form.Width, form.Height);
                    Graphics graphic = Graphics.FromImage(bitmap);
                    graphic.CopyFromScreen(form.Location, Point.Empty, form.Size);
                    bitmap.Save(_pPathImage + _pFileName) ;
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่สามารถ Upload รูปได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}.");
                    this.Close();
                 
                }
                

            }
         
        }

        Rectangle TopLeft { get { return new Rectangle(0, 0, _, _); } }
        Rectangle TopRight { get { return new Rectangle(this.ClientSize.Width - _, 0, _, _); } }
        Rectangle BottomLeft { get { return new Rectangle(0, this.ClientSize.Height - _, _, _); } }
        Rectangle BottomRight { get { return new Rectangle(this.ClientSize.Width - _, this.ClientSize.Height - _, _, _); } }

        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);

            if (message.Msg == 0x84) // WM_NCHITTEST
            {
                var cursor = this.PointToClient(Cursor.Position);

                if (TopLeft.Contains(cursor)) message.Result = (IntPtr)HTTOPLEFT;
                else if (TopRight.Contains(cursor)) message.Result = (IntPtr)HTTOPRIGHT;
                else if (BottomLeft.Contains(cursor)) message.Result = (IntPtr)HTBOTTOMLEFT;
                else if (BottomRight.Contains(cursor)) message.Result = (IntPtr)HTBOTTOMRIGHT;

                else if (Top.Contains(cursor)) message.Result = (IntPtr)HTTOP;
                else if (Left.Contains(cursor)) message.Result = (IntPtr)HTLEFT;
                else if (Right.Contains(cursor)) message.Result = (IntPtr)HTRIGHT;
                else if (Bottom.Contains(cursor)) message.Result = (IntPtr)HTBOTTOM;
            }
        }

    }
}


