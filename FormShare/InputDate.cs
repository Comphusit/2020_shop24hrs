﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Data;
using System.Windows.Forms;

namespace PC_Shop24Hrs.FormShare
{
    public partial class InputDate : Telerik.WinControls.UI.RadForm
    {
        public string pInputDate;
        // readonly string sTANumberOtText; //0 ต้องการเปนตัวเลข 1 ต้องการเป็นข้อความ

        readonly string _formartReturn;
        readonly string _case;
        public InputDate(string sShowInput, string BT_Ok, string BT_Cancle, string formartReturn, string Case)
        {
            InitializeComponent();
            radLabel_Input.Text = sShowInput;
            radButton_Save.Text = BT_Ok;
            radButton_Cancel.Text = BT_Cancle;
            _formartReturn = formartReturn;
            _case = Case;
        }
        //load
        private void InputDate_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radDateTimePicker_Send.Visible = false;
            RadDropDownList_Location.Visible = false;

            if (_case == "EMP")
            {              
                RadDropDownList_Location.Visible = true;
                DatagridClass.SetDefaultFontDropDown(RadDropDownList_Location);

                DataTable dtDropDown = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("62", "", "ORDER BY SHOW_ID", "1");
                
                DataTable dtAdd = new DataTable();
                dtAdd.Columns.Add("SHOW_ID");
                dtAdd.Columns.Add("DESCSHOW");
                
                dtAdd.Rows.Add("ALL-1", "ALL-ทั้งหมด [File เดียว]");
                dtAdd.Rows.Add("ALL-2", "ALL-ทั้งหมด [แยก File]");

                for (int i = 0; i < dtDropDown.Rows.Count; i++)
                {
                    dtAdd.Rows.Add(dtDropDown.Rows[i]["SHOW_ID"].ToString(), dtDropDown.Rows[i]["DESCSHOW"].ToString());
                }

                RadDropDownList_Location.DataSource = dtAdd;
                RadDropDownList_Location.DisplayMember = "DESCSHOW";
                RadDropDownList_Location.ValueMember = "SHOW_ID";
            }
            else
            {
                radDateTimePicker_Send.Visible = true;                
                radDateTimePicker_Send.Value = DateTime.Now.AddDays(1);
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            EnterData();
        }

        void EnterData()
        {
            if (_case == "EMP")
            {
                this.DialogResult = DialogResult.Yes;
                pInputDate = RadDropDownList_Location.SelectedValue.ToString();
                this.Close();
                return;
            }
            pInputDate = radDateTimePicker_Send.Value.ToString(_formartReturn).ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

    }
}
