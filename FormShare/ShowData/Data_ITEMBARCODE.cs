﻿//CheckOK
using System;
using System.Data;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public partial class Data_ITEMBARCODE
    {
        //data Items
        private readonly string _Itembarcode_SPC_ITEMACTIVE = "SPC_ITEMACTIVE";
        public string Itembarcode_SPC_ITEMACTIVE { get; set; }

        private readonly string _Itembarcode_ITEMID = "ITEMID";
        public string Itembarcode_ITEMID { get; set; }

        private readonly string _Itembarcode_INVENTDIMID = "INVENTDIMID";
        public string Itembarcode_INVENTDIMID { get; set; }

        private readonly string _Itembarcode_ITEMBARCODE = "ITEMBARCODE";
        public string Itembarcode_ITEMBARCODE { get; set; }

        private readonly string _Itembarcode_SPC_ITEMNAME = "SPC_ITEMNAME";
        public string Itembarcode_SPC_ITEMNAME { get; set; }

        private readonly string _Itembarcode_QTY = "QTY";
        public Double Itembarcode_QTY { get; set; }

        private readonly string _Itembarcode_UNITID = "UNITID";
        public string Itembarcode_UNITID { get; set; }


        //price items
        private readonly string _Itembarcode_SPC_PRICEGROUP3 = "SPC_PRICEGROUP3";
        public Double Itembarcode_SPC_PRICEGROUP3 { get; set; }


        private readonly string _Itembarcode_SPC_PRICEGROUP13 = "SPC_PRICEGROUP13";
        public Double Itembarcode_SPC_PRICEGROUP13 { get; set; }


        private readonly string _Itembarcode_SPC_PRICEGROUP14 = "SPC_PRICEGROUP14";
        public Double Itembarcode_SPC_PRICEGROUP14 { get; set; }


        private readonly  string _Itembarcode_SPC_PRICEGROUP15 = "SPC_PRICEGROUP15";
        public Double Itembarcode_SPC_PRICEGROUP15 { get; set; }

        private readonly string _Itembarcode_SPC_PRICEGROUP17 = "SPC_PRICEGROUP17";
        public Double Itembarcode_SPC_PRICEGROUP17 { get; set; }

        private readonly string _Itembarcode_SPC_PRICEGROUP18 = "SPC_PRICEGROUP18";
        public Double Itembarcode_SPC_PRICEGROUP18 { get; set; }

        private readonly string _Itembarcode_SPC_PRICEGROUP19 = "SPC_PRICEGROUP19";
        public Double Itembarcode_SPC_PRICEGROUP19 { get; set; }

        //Detail items
        private readonly string _Itembarcode_DIMENSION = "DIMENSION";
        public string Itembarcode_DIMENSION { get; set; }


        private readonly string _Itembarcode_DESCRIPTION = "DESCRIPTION";
        public string Itembarcode_DESCRIPTION { get; set; }


        private readonly string _Itembarcode_VENDORID = "PRIMARYVENDORID";
        public string Itembarcode_VENDORID { get; set; }


        private readonly string _Itembarcode_VENDORIDNAME = "PRIMARYVENDORIDNAME";
        public string Itembarcode_VENDORIDNAME { get; set; }



        private readonly string _Itembarcode_SPC_SalesPriceType = "SPC_SalesPriceType";
        public string Itembarcode_SPC_SalesPriceType { get; set; }


        private readonly string _Itembarcode_SPC_SalesPriceTypeNAME = "SPC_SalesPriceTypeNAME";
        public string Itembarcode_SPC_SalesPriceTypeNAME { get; set; }


        //tax items
        private readonly string _Itembarcode_GRPID0 = "GRPID0";
        public string Itembarcode_GRPID0 { get; set; }


        private readonly  string _Itembarcode_TAX0 = "TAX0";
        public string Itembarcode_TAX0 { get; set; }


        private readonly string _Itembarcode_GRPID1 = "GRPID1";
        public string Itembarcode_GRPID1 { get; set; }


        private readonly string _Itembarcode_TAX1 = "TAX1";
        public string Itembarcode_TAX1 { get; set; }


        private readonly string _Itembarcode_GRPID2 = "GRPID2";
        public string Itembarcode_GRPID2 { get; set; }


        private readonly string _Itembarcode_TAX2 = "TAX2";
        public string Itembarcode_TAX2 { get; set; }


        private readonly string _Itembarcode_SPC_IMAGEPATH = "SPC_IMAGEPATH";
        public string Itembarcode_SPC_IMAGEPATH { get; set; }

        //ราคาสินค้าแต่ละมินิมาร์ท
        public Double Itembarcode_PriceUnit_Branch { get; set; }

        //price minimart
        public Double Itembarcode_weight { get; set; }
        //price supercheap
        public Double Itembarcode_weight_SupercheapPrice { get; set; }
        public Double Itembarcode_priceNet { get; set; }
        //private string _Itembarcode_ { get; set; }
        //public string Itembarcode_ { get; set; } 


        private readonly string _Itembarcode_SPC_GiftPoint = "SPC_GiftPoint";
        public string Itembarcode_SPC_GiftPoint { get; set; }

        //search Itembarcode currenrow in gridview :: ShowDataDGV_Itembarcode
        public Data_ITEMBARCODE(DataRow items)
        {
            if (items != null)
            {

                //data Items
                Itembarcode_SPC_ITEMACTIVE = items[_Itembarcode_SPC_ITEMACTIVE].ToString();
                Itembarcode_ITEMID = items[_Itembarcode_ITEMID].ToString();
                Itembarcode_INVENTDIMID = items[_Itembarcode_INVENTDIMID].ToString();
                Itembarcode_ITEMBARCODE = items[_Itembarcode_ITEMBARCODE].ToString();
                Itembarcode_SPC_ITEMNAME = items[_Itembarcode_SPC_ITEMNAME].ToString();
                Itembarcode_QTY = Double.Parse(items[_Itembarcode_QTY].ToString());
                Itembarcode_UNITID = items[_Itembarcode_UNITID].ToString();

                //price items
                Itembarcode_SPC_PRICEGROUP3 = Double.Parse(items[_Itembarcode_SPC_PRICEGROUP3].ToString());
                Itembarcode_SPC_PRICEGROUP13 = Double.Parse(items[_Itembarcode_SPC_PRICEGROUP13].ToString());
                Itembarcode_SPC_PRICEGROUP14 = Double.Parse(items[_Itembarcode_SPC_PRICEGROUP14].ToString());
                Itembarcode_SPC_PRICEGROUP15 = Double.Parse(items[_Itembarcode_SPC_PRICEGROUP15].ToString());

                //Detail items
                Itembarcode_DIMENSION = items[_Itembarcode_DIMENSION].ToString();
                Itembarcode_DESCRIPTION = items[_Itembarcode_DESCRIPTION].ToString();
                Itembarcode_VENDORID = items[_Itembarcode_VENDORID].ToString();
                Itembarcode_VENDORIDNAME = items[_Itembarcode_VENDORIDNAME].ToString();
                Itembarcode_SPC_SalesPriceType = items[_Itembarcode_SPC_SalesPriceType].ToString();
                Itembarcode_SPC_SalesPriceTypeNAME = items[_Itembarcode_SPC_SalesPriceTypeNAME].ToString();

                //tax items
                Itembarcode_GRPID0 = items[_Itembarcode_GRPID0].ToString();
                Itembarcode_TAX0 = items[_Itembarcode_TAX0].ToString();
                Itembarcode_GRPID1 = items[_Itembarcode_GRPID1].ToString();
                Itembarcode_TAX1 = items[_Itembarcode_TAX1].ToString();
                Itembarcode_GRPID2 = items[_Itembarcode_GRPID2].ToString();
                Itembarcode_TAX2 = items[_Itembarcode_TAX2].ToString();

                //ImagePath Items 
                Itembarcode_SPC_IMAGEPATH = items[_Itembarcode_SPC_IMAGEPATH].ToString();

                //PriceBranch
                Itembarcode_PriceUnit_Branch = Price_Minimart(Controllers.SystemClass.SystemBranchPrice);

                Itembarcode_SPC_GiftPoint = items[_Itembarcode_SPC_GiftPoint].ToString();

            }
        }


        public Boolean GetItembarcodeStatus { get; set; }
        //search itembarcode from barcode
        public Data_ITEMBARCODE(string ItemBarcode)
        {
            // DataTable dtItemBarcode = new DataTable();
            DataTable dtItemBarcode = Class.ItembarcodeClass.GetItembarcode_ALLDeatil(ItemBarcode);
            if ((dtItemBarcode.Rows.Count == 0) && (ItemBarcode.Length == 13))
            {
                dtItemBarcode = Class.ItembarcodeClass.GetItembarcode_ALLDeatil(ItemBarcode.Substring(0, 7));
            }

            if (dtItemBarcode.Rows.Count > 0)
            {
                GetItembarcodeStatus = true;
                DataColumnCollection columns = dtItemBarcode.Columns;
                //data Items
                //Itembarcode_SPC_ITEMACTIVE = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMACTIVE].ToString();
                if (columns.Contains(_Itembarcode_SPC_ITEMACTIVE))
                { Itembarcode_SPC_ITEMACTIVE = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMACTIVE].ToString(); }
                else { Itembarcode_SPC_ITEMACTIVE = ""; } 
                
                //Itembarcode_ITEMID = dtItemBarcode.Rows[0][_Itembarcode_ITEMID].ToString();
                if (columns.Contains(_Itembarcode_ITEMID))
                { Itembarcode_ITEMID = dtItemBarcode.Rows[0][_Itembarcode_ITEMID].ToString(); }
                else { Itembarcode_ITEMID = ""; }

                //Itembarcode_INVENTDIMID = dtItemBarcode.Rows[0][_Itembarcode_INVENTDIMID].ToString();
                if (columns.Contains(_Itembarcode_INVENTDIMID))
                { Itembarcode_INVENTDIMID = dtItemBarcode.Rows[0][_Itembarcode_INVENTDIMID].ToString(); }
                else { Itembarcode_INVENTDIMID = ""; }

                //Itembarcode_ITEMBARCODE = dtItemBarcode.Rows[0][_Itembarcode_ITEMBARCODE].ToString();
                if (columns.Contains(_Itembarcode_ITEMBARCODE))
                { Itembarcode_ITEMBARCODE = dtItemBarcode.Rows[0][_Itembarcode_ITEMBARCODE].ToString(); }
                else { Itembarcode_ITEMBARCODE = ""; }

                //Itembarcode_SPC_ITEMNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMNAME].ToString();
                if (columns.Contains(_Itembarcode_SPC_ITEMNAME))
                { Itembarcode_SPC_ITEMNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMNAME].ToString(); }
                else { Itembarcode_SPC_ITEMNAME = ""; }

                //Itembarcode_QTY = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_QTY].ToString());
                if (columns.Contains(_Itembarcode_QTY))
                { Itembarcode_QTY = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_QTY].ToString()); }
                else { Itembarcode_QTY = 0; }
                 
                //Itembarcode_UNITID = dtItemBarcode.Rows[0][_Itembarcode_UNITID].ToString();
                if (columns.Contains(_Itembarcode_UNITID))
                { Itembarcode_UNITID = dtItemBarcode.Rows[0][_Itembarcode_UNITID].ToString(); }
                else { Itembarcode_UNITID = ""; }


                //price items
                //Itembarcode_SPC_PRICEGROUP3 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP3].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP3))
                { Itembarcode_SPC_PRICEGROUP3 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP3].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP3 = 0; }

                //Itembarcode_SPC_PRICEGROUP13 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP13].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP13))
                { Itembarcode_SPC_PRICEGROUP13 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP13].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP13 = 0; }

                //Itembarcode_SPC_PRICEGROUP14 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP14].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP14))
                { Itembarcode_SPC_PRICEGROUP14 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP14].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP14 = 0; }

                //Itembarcode_SPC_PRICEGROUP15 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP15].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP15))
                { Itembarcode_SPC_PRICEGROUP15 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP15].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP15 = 0; }


                //Detail items
                // Itembarcode_DIMENSION = dtItemBarcode.Rows[0][_Itembarcode_DIMENSION].ToString();
                if (columns.Contains(_Itembarcode_DIMENSION))
                { Itembarcode_DIMENSION = dtItemBarcode.Rows[0][_Itembarcode_DIMENSION].ToString(); }
                else { Itembarcode_DIMENSION = ""; }

                //Itembarcode_DESCRIPTION = dtItemBarcode.Rows[0][_Itembarcode_DESCRIPTION].ToString();
                if (columns.Contains(_Itembarcode_DESCRIPTION))
                { Itembarcode_DESCRIPTION = dtItemBarcode.Rows[0][_Itembarcode_DESCRIPTION].ToString(); }
                else { Itembarcode_DESCRIPTION = ""; }

                //Itembarcode_VENDORID = dtItemBarcode.Rows[0][_Itembarcode_VENDORID].ToString();
                if (columns.Contains(_Itembarcode_VENDORID))
                { Itembarcode_VENDORID = dtItemBarcode.Rows[0][_Itembarcode_VENDORID].ToString(); }
                else { Itembarcode_VENDORID = ""; }

                //Itembarcode_VENDORIDNAME = dtItemBarcode.Rows[0][_Itembarcode_VENDORIDNAME].ToString();
                if (columns.Contains(_Itembarcode_VENDORIDNAME))
                { Itembarcode_VENDORIDNAME = dtItemBarcode.Rows[0][_Itembarcode_VENDORIDNAME].ToString(); }
                else { Itembarcode_VENDORIDNAME = ""; }

                //Itembarcode_SPC_SalesPriceType = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceType].ToString();
                if (columns.Contains(_Itembarcode_SPC_SalesPriceType))
                { Itembarcode_SPC_SalesPriceType = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceType].ToString(); }
                else { Itembarcode_SPC_SalesPriceType = ""; }

                //Itembarcode_SPC_SalesPriceTypeNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceTypeNAME].ToString();
                if (columns.Contains(_Itembarcode_SPC_SalesPriceTypeNAME))
                { Itembarcode_SPC_SalesPriceTypeNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceTypeNAME].ToString(); }
                else { Itembarcode_SPC_SalesPriceTypeNAME = ""; }


                //tax items
                //Itembarcode_GRPID0 = dtItemBarcode.Rows[0][_Itembarcode_GRPID0].ToString();
                if (columns.Contains(_Itembarcode_GRPID0))
                { Itembarcode_GRPID0 = dtItemBarcode.Rows[0][_Itembarcode_GRPID0].ToString(); }
                else { Itembarcode_GRPID0 = ""; }

                //Itembarcode_TAX0 = dtItemBarcode.Rows[0][_Itembarcode_TAX0].ToString();
                if (columns.Contains(_Itembarcode_TAX0))
                { Itembarcode_TAX0 = dtItemBarcode.Rows[0][_Itembarcode_TAX0].ToString(); }
                else { Itembarcode_TAX0 = ""; }

                //Itembarcode_GRPID1 = dtItemBarcode.Rows[0][_Itembarcode_GRPID1].ToString();
                if (columns.Contains(_Itembarcode_GRPID1))
                { Itembarcode_GRPID1 = dtItemBarcode.Rows[0][_Itembarcode_GRPID1].ToString(); }
                else { Itembarcode_GRPID1 = ""; }

                //Itembarcode_TAX1 = dtItemBarcode.Rows[0][_Itembarcode_TAX1].ToString();
                if (columns.Contains(_Itembarcode_TAX1))
                { Itembarcode_TAX1 = dtItemBarcode.Rows[0][_Itembarcode_TAX1].ToString(); }
                else { Itembarcode_TAX1 = ""; }

                //Itembarcode_GRPID2 = dtItemBarcode.Rows[0][_Itembarcode_GRPID2].ToString();
                if (columns.Contains(_Itembarcode_GRPID2))
                { Itembarcode_GRPID2 = dtItemBarcode.Rows[0][_Itembarcode_GRPID2].ToString(); }
                else { Itembarcode_GRPID2 = ""; }

                //Itembarcode_TAX2 = dtItemBarcode.Rows[0][_Itembarcode_TAX2].ToString();
                if (columns.Contains(_Itembarcode_TAX2))
                { Itembarcode_TAX2 = dtItemBarcode.Rows[0][_Itembarcode_TAX2].ToString(); }
                else { Itembarcode_TAX2 = ""; }
                 

                //ImagePath Items 
                //Itembarcode_SPC_IMAGEPATH = dtItemBarcode.Rows[0][_Itembarcode_SPC_IMAGEPATH].ToString();
                if (columns.Contains(_Itembarcode_SPC_IMAGEPATH))
                { Itembarcode_SPC_IMAGEPATH = dtItemBarcode.Rows[0][_Itembarcode_SPC_IMAGEPATH].ToString(); }
                else { Itembarcode_SPC_IMAGEPATH = ""; }


                if (columns.Contains(_Itembarcode_SPC_GiftPoint))
                { Itembarcode_SPC_GiftPoint = dtItemBarcode.Rows[0][_Itembarcode_SPC_GiftPoint].ToString(); }
                else { Itembarcode_SPC_GiftPoint = "0"; }

                //PriceBranch
                Itembarcode_PriceUnit_Branch = Price_Minimart(Controllers.SystemClass.SystemBranchPrice);


                GetitemBarcode_weight(ItemBarcode, Itembarcode_SPC_SalesPriceType);

                GetitemBarcode_weight_SupercheapPrice(ItemBarcode, Itembarcode_SPC_SalesPriceType);

            }
            else
            {
                GetItembarcodeStatus = false;
            }
        }

        public Data_ITEMBARCODE(DataTable dtItemBarcode)
        {
            // DataTable dtItemBarcode = new DataTable();
            //DataTable dtItemBarcode = Class.ItembarcodeClass.GetItembarcode_ALLDeatil(ItemBarcode);
            //if ((dtItemBarcode.Rows.Count == 0) && (ItemBarcode.Length == 13))
            //{
            //    dtItemBarcode = Class.ItembarcodeClass.GetItembarcode_ALLDeatil(ItemBarcode.Substring(0, 7));
            //}

            if (dtItemBarcode.Rows.Count > 0)
            {
                GetItembarcodeStatus = true;
                DataColumnCollection columns = dtItemBarcode.Columns;
                //data Items
                //Itembarcode_SPC_ITEMACTIVE = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMACTIVE].ToString();
                if (columns.Contains(_Itembarcode_SPC_ITEMACTIVE))
                { Itembarcode_SPC_ITEMACTIVE = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMACTIVE].ToString(); }
                else { Itembarcode_SPC_ITEMACTIVE = ""; }

                //Itembarcode_ITEMID = dtItemBarcode.Rows[0][_Itembarcode_ITEMID].ToString();
                if (columns.Contains(_Itembarcode_ITEMID))
                { Itembarcode_ITEMID = dtItemBarcode.Rows[0][_Itembarcode_ITEMID].ToString(); }
                else { Itembarcode_ITEMID = ""; }

                //Itembarcode_INVENTDIMID = dtItemBarcode.Rows[0][_Itembarcode_INVENTDIMID].ToString();
                if (columns.Contains(_Itembarcode_INVENTDIMID))
                { Itembarcode_INVENTDIMID = dtItemBarcode.Rows[0][_Itembarcode_INVENTDIMID].ToString(); }
                else { Itembarcode_INVENTDIMID = ""; }

                //Itembarcode_ITEMBARCODE = dtItemBarcode.Rows[0][_Itembarcode_ITEMBARCODE].ToString();
                if (columns.Contains(_Itembarcode_ITEMBARCODE))
                { Itembarcode_ITEMBARCODE = dtItemBarcode.Rows[0][_Itembarcode_ITEMBARCODE].ToString(); }
                else { Itembarcode_ITEMBARCODE = ""; }

                //Itembarcode_SPC_ITEMNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMNAME].ToString();
                if (columns.Contains(_Itembarcode_SPC_ITEMNAME))
                { Itembarcode_SPC_ITEMNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_ITEMNAME].ToString(); }
                else { Itembarcode_SPC_ITEMNAME = ""; }

                //Itembarcode_QTY = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_QTY].ToString());
                if (columns.Contains(_Itembarcode_QTY))
                { Itembarcode_QTY = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_QTY].ToString()); }
                else { Itembarcode_QTY = 0; }

                //Itembarcode_UNITID = dtItemBarcode.Rows[0][_Itembarcode_UNITID].ToString();
                if (columns.Contains(_Itembarcode_UNITID))
                { Itembarcode_UNITID = dtItemBarcode.Rows[0][_Itembarcode_UNITID].ToString(); }
                else { Itembarcode_UNITID = ""; }


                //price items
                //Itembarcode_SPC_PRICEGROUP3 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP3].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP3))
                { Itembarcode_SPC_PRICEGROUP3 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP3].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP3 = 0; }

                //Itembarcode_SPC_PRICEGROUP13 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP13].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP13))
                { Itembarcode_SPC_PRICEGROUP13 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP13].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP13 = 0; }

                //Itembarcode_SPC_PRICEGROUP14 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP14].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP14))
                { Itembarcode_SPC_PRICEGROUP14 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP14].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP14 = 0; }

                //Itembarcode_SPC_PRICEGROUP15 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP15].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP15))
                { Itembarcode_SPC_PRICEGROUP15 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP15].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP15 = 0; }

                //Itembarcode_SPC_PRICEGROUP15 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP15].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP17))
                { Itembarcode_SPC_PRICEGROUP17 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP17].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP17 = 0; }

                //Itembarcode_SPC_PRICEGROUP18 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP18].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP18))
                { Itembarcode_SPC_PRICEGROUP18 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP18].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP18 = 0; }

                //Itembarcode_SPC_PRICEGROUP19 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP19].ToString());
                if (columns.Contains(_Itembarcode_SPC_PRICEGROUP19))
                { Itembarcode_SPC_PRICEGROUP19 = Double.Parse(dtItemBarcode.Rows[0][_Itembarcode_SPC_PRICEGROUP19].ToString()); }
                else { Itembarcode_SPC_PRICEGROUP19 = 0; }


                //Detail items
                // Itembarcode_DIMENSION = dtItemBarcode.Rows[0][_Itembarcode_DIMENSION].ToString();
                if (columns.Contains(_Itembarcode_DIMENSION))
                { Itembarcode_DIMENSION = dtItemBarcode.Rows[0][_Itembarcode_DIMENSION].ToString(); }
                else { Itembarcode_DIMENSION = ""; }

                //Itembarcode_DESCRIPTION = dtItemBarcode.Rows[0][_Itembarcode_DESCRIPTION].ToString();
                if (columns.Contains(_Itembarcode_DESCRIPTION))
                { Itembarcode_DESCRIPTION = dtItemBarcode.Rows[0][_Itembarcode_DESCRIPTION].ToString(); }
                else { Itembarcode_DESCRIPTION = ""; }

                //Itembarcode_VENDORID = dtItemBarcode.Rows[0][_Itembarcode_VENDORID].ToString();
                if (columns.Contains(_Itembarcode_VENDORID))
                { Itembarcode_VENDORID = dtItemBarcode.Rows[0][_Itembarcode_VENDORID].ToString(); }
                else { Itembarcode_VENDORID = ""; }

                //Itembarcode_VENDORIDNAME = dtItemBarcode.Rows[0][_Itembarcode_VENDORIDNAME].ToString();
                if (columns.Contains(_Itembarcode_VENDORIDNAME))
                { Itembarcode_VENDORIDNAME = dtItemBarcode.Rows[0][_Itembarcode_VENDORIDNAME].ToString(); }
                else { Itembarcode_VENDORIDNAME = ""; }

                //Itembarcode_SPC_SalesPriceType = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceType].ToString();
                if (columns.Contains(_Itembarcode_SPC_SalesPriceType))
                { Itembarcode_SPC_SalesPriceType = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceType].ToString(); }
                else { Itembarcode_SPC_SalesPriceType = ""; }

                //Itembarcode_SPC_SalesPriceTypeNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceTypeNAME].ToString();
                if (columns.Contains(_Itembarcode_SPC_SalesPriceTypeNAME))
                { Itembarcode_SPC_SalesPriceTypeNAME = dtItemBarcode.Rows[0][_Itembarcode_SPC_SalesPriceTypeNAME].ToString(); }
                else { Itembarcode_SPC_SalesPriceTypeNAME = ""; }


                //tax items
                //Itembarcode_GRPID0 = dtItemBarcode.Rows[0][_Itembarcode_GRPID0].ToString();
                if (columns.Contains(_Itembarcode_GRPID0))
                { Itembarcode_GRPID0 = dtItemBarcode.Rows[0][_Itembarcode_GRPID0].ToString(); }
                else { Itembarcode_GRPID0 = ""; }

                //Itembarcode_TAX0 = dtItemBarcode.Rows[0][_Itembarcode_TAX0].ToString();
                if (columns.Contains(_Itembarcode_TAX0))
                { Itembarcode_TAX0 = dtItemBarcode.Rows[0][_Itembarcode_TAX0].ToString(); }
                else { Itembarcode_TAX0 = ""; }

                //Itembarcode_GRPID1 = dtItemBarcode.Rows[0][_Itembarcode_GRPID1].ToString();
                if (columns.Contains(_Itembarcode_GRPID1))
                { Itembarcode_GRPID1 = dtItemBarcode.Rows[0][_Itembarcode_GRPID1].ToString(); }
                else { Itembarcode_GRPID1 = ""; }

                //Itembarcode_TAX1 = dtItemBarcode.Rows[0][_Itembarcode_TAX1].ToString();
                if (columns.Contains(_Itembarcode_TAX1))
                { Itembarcode_TAX1 = dtItemBarcode.Rows[0][_Itembarcode_TAX1].ToString(); }
                else { Itembarcode_TAX1 = ""; }

                //Itembarcode_GRPID2 = dtItemBarcode.Rows[0][_Itembarcode_GRPID2].ToString();
                if (columns.Contains(_Itembarcode_GRPID2))
                { Itembarcode_GRPID2 = dtItemBarcode.Rows[0][_Itembarcode_GRPID2].ToString(); }
                else { Itembarcode_GRPID2 = ""; }

                //Itembarcode_TAX2 = dtItemBarcode.Rows[0][_Itembarcode_TAX2].ToString();
                if (columns.Contains(_Itembarcode_TAX2))
                { Itembarcode_TAX2 = dtItemBarcode.Rows[0][_Itembarcode_TAX2].ToString(); }
                else { Itembarcode_TAX2 = ""; }


                //ImagePath Items 
                //Itembarcode_SPC_IMAGEPATH = dtItemBarcode.Rows[0][_Itembarcode_SPC_IMAGEPATH].ToString();
                if (columns.Contains(_Itembarcode_SPC_IMAGEPATH))
                { Itembarcode_SPC_IMAGEPATH = dtItemBarcode.Rows[0][_Itembarcode_SPC_IMAGEPATH].ToString(); }
                else { Itembarcode_SPC_IMAGEPATH = ""; }

                if (columns.Contains(_Itembarcode_SPC_GiftPoint))
                { Itembarcode_SPC_GiftPoint = dtItemBarcode.Rows[0][_Itembarcode_SPC_GiftPoint].ToString(); }
                else { Itembarcode_SPC_GiftPoint = "0"; }

                //PriceBranch
                Itembarcode_PriceUnit_Branch = Price_Minimart(Controllers.SystemClass.SystemBranchPrice);
                 
            }
            else
            {
                GetItembarcodeStatus = false;
            }
        }

        //Branch Price
        public Double Price_Minimart(string SystemBranchPrice)
        {
            double PriceGroup;
            switch (SystemBranchPrice)
            {
                case "SPC_PriceGroup3":
                    PriceGroup = Itembarcode_SPC_PRICEGROUP3;
                    break;
                case "SPC_PriceGroup13":
                    PriceGroup = Itembarcode_SPC_PRICEGROUP13;
                    break;
                case "SPC_PriceGroup14":
                    PriceGroup = Itembarcode_SPC_PRICEGROUP14;
                    break;
                case "SPC_PriceGroup15":
                    PriceGroup = Itembarcode_SPC_PRICEGROUP15;
                    break;
                case "SPC_PriceGroup17":
                    PriceGroup = Itembarcode_SPC_PRICEGROUP17;
                    break;
                default:
                    PriceGroup = Itembarcode_SPC_PRICEGROUP3;
                    break;
            }

            return PriceGroup;
        }

        //weigth items
        public void GetitemBarcode_weight(string ItemBarocde, string typeItemBarcode)
        {
            switch (typeItemBarcode)
            {
                case "1":
                    Itembarcode_weight = 1;
                    Itembarcode_priceNet = Itembarcode_PriceUnit_Branch;
                    break;
                case "2":
                    Itembarcode_weight = 1;
                    Itembarcode_priceNet = Itembarcode_PriceUnit_Branch;
                    break;
                case "3":

                    if (ItemBarocde.Length < 8)
                    {
                        Itembarcode_weight = 1;
                        Itembarcode_priceNet = Itembarcode_PriceUnit_Branch;
                    }
                    else
                    {
                        Itembarcode_weight = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3)) / Itembarcode_PriceUnit_Branch;
                        Itembarcode_priceNet = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3));
                    }
                  
                    break;
                case "4":
                    if (ItemBarocde.Length < 8)
                    {
                        Itembarcode_weight = 1;
                        Itembarcode_priceNet = Itembarcode_PriceUnit_Branch;
                    }
                    else
                    {
                        Itembarcode_weight = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3));
                        Itembarcode_priceNet = Itembarcode_weight * Itembarcode_PriceUnit_Branch;
                    } 
                    break;
                case "5":
                    if (ItemBarocde.Length < 8)
                    {
                        Itembarcode_priceNet = 1;
                    }
                    else
                    {
                        Itembarcode_priceNet = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3));
                    } 
                    Itembarcode_weight = 1;
                    break;
            }
        }

        //weigth items
        public void GetitemBarcode_weight_SupercheapPrice(string ItemBarocde, string typeItemBarcode)
        {
            switch (typeItemBarcode)
            {
                case "1":
                    Itembarcode_weight_SupercheapPrice = 1;
                    Itembarcode_priceNet = Itembarcode_SPC_PRICEGROUP3;
                    break;
                case "2":
                    Itembarcode_weight_SupercheapPrice = 1;
                    Itembarcode_priceNet = Itembarcode_SPC_PRICEGROUP3;
                    break;
                case "3":

                    if (ItemBarocde.Length < 8)
                    {
                        Itembarcode_weight_SupercheapPrice = 1;
                        Itembarcode_priceNet = Itembarcode_SPC_PRICEGROUP3;
                    }
                    else
                    {
                        Itembarcode_weight_SupercheapPrice = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3)) / Itembarcode_SPC_PRICEGROUP3;
                        Itembarcode_priceNet = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3));
                    }
                
                    break;
                case "4":
                    if (ItemBarocde.Length < 8)
                    {
                        Itembarcode_weight_SupercheapPrice = 1;
                        Itembarcode_priceNet = Itembarcode_SPC_PRICEGROUP3;
                    }
                    else
                    {
                        Itembarcode_weight_SupercheapPrice = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3));
                        Itembarcode_priceNet = Itembarcode_weight_SupercheapPrice * Itembarcode_SPC_PRICEGROUP3;
                    }
                  
                    break;
                case "5":
                    if (ItemBarocde.Length < 8)
                    {
                        Itembarcode_priceNet = Itembarcode_SPC_PRICEGROUP3;
                    }
                    else
                    {
                        Itembarcode_priceNet = Convert.ToDouble(ItemBarocde.Substring(7, 3) + '.' + ItemBarocde.Substring(10, 3));
                    } 
                    Itembarcode_weight_SupercheapPrice = 1;
                    break;
            }

        }

    }
}
