﻿//CheckOK
using System.Data;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public   class Data_VENDTABLE
    {
        readonly  private  string _VENDTABLE_ACCOUNTNUM = "ACCOUNTNUM";
        public  string VENDTABLE_ACCOUNTNUM { get; set; }

        readonly private string _VENDTABLE_NAME = "NAME";
        public string VENDTABLE_NAME { get; set; }

        readonly private string _VENDTABLE_ADDRESS = "ADDRESS";
        public string VENDTABLE_ADDRESS { get; set; }

        readonly private string _VENDTABLE_PHONE = "PHONE";
        public string VENDTABLE_PHONE { get; set; }

        readonly private string _VENDTABLE_ITEMBUYERGROUPID = "ITEMBUYERGROUPID";
        public string VENDTABLE_ITEMBUYERGROUPID { get; set; }
        


        //search employee currenrow in gridview :: ShowDataDGV_VEND
        public Data_VENDTABLE(DataRow rows)
        {
            if (rows != null)
            {
                VENDTABLE_ACCOUNTNUM = rows[_VENDTABLE_ACCOUNTNUM].ToString();
                VENDTABLE_NAME = rows[_VENDTABLE_NAME].ToString();
                VENDTABLE_ADDRESS = rows[_VENDTABLE_ADDRESS].ToString();
                VENDTABLE_PHONE = rows[_VENDTABLE_PHONE].ToString(); 
                VENDTABLE_ITEMBUYERGROUPID = rows[_VENDTABLE_ITEMBUYERGROUPID].ToString();
            }
        }
         
        public Data_VENDTABLE(DataTable DtVend)
        {  
            if (DtVend.Rows.Count > 0)
            {
                DataColumnCollection columns = DtVend.Columns;
                if (columns.Contains(_VENDTABLE_ACCOUNTNUM))
                { VENDTABLE_ACCOUNTNUM = DtVend.Rows[0][_VENDTABLE_ACCOUNTNUM].ToString(); }
                else { VENDTABLE_ACCOUNTNUM = ""; }

                if (columns.Contains(_VENDTABLE_NAME))
                { VENDTABLE_NAME = DtVend.Rows[0][_VENDTABLE_NAME].ToString(); }
                else { VENDTABLE_NAME = ""; }

                if (columns.Contains(_VENDTABLE_ADDRESS))
                { VENDTABLE_ADDRESS = DtVend.Rows[0][_VENDTABLE_ADDRESS].ToString(); }
                else { VENDTABLE_ADDRESS = ""; }

                if (columns.Contains(_VENDTABLE_PHONE))
                { VENDTABLE_PHONE = DtVend.Rows[0][_VENDTABLE_PHONE].ToString(); }
                else { VENDTABLE_PHONE = ""; }

                if (columns.Contains(_VENDTABLE_ITEMBUYERGROUPID))
                { VENDTABLE_ITEMBUYERGROUPID = DtVend.Rows[0][_VENDTABLE_ITEMBUYERGROUPID].ToString(); }
                else { VENDTABLE_ITEMBUYERGROUPID = ""; }

            } 
        } 
    } 
}
