﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs.FormShare.ShowData
{
    public partial class ShowDataDGV_Itembarcode : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData;

        //public DataRow[] pItembarcode;
        public static Data_ITEMBARCODE items;

        readonly string _pCon;

        public ShowDataDGV_Itembarcode(string pCon)
        {
            InitializeComponent();
            _pCon = pCon;
        }

        private void ShowDataDGV_Itembarcode_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;


            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("SPC_ITEMACTIVE", "ใช้งาน")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMID", "รหัสสินค้า", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTDIMID", "มิติสินค้า", 140)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "อัตราส่วน", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาสาขาใหญ่", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP13", "ราคาขาย MN_1", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP14", "ราคาขาย MN_2", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP15", "ราคาขาย MN_3", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP17", "ราคาขาย MN_4", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP18", "ราคาขาย MN_5", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP19", "ราคาขาย MN_6", 105)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนก", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "จัดซื้อ", 150)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PRIMARYVENDORID", "ผู้จำหน่าย", 120)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อผู้จำหน่าย", 250)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_SalesPriceTypeNAME", "ประเภทราคา", 120)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GRPID0", "กลุ่มหลัก", 90)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TAX0", "ชื่อหลัก", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GRPID1", "กลุ่มย่อย1", 90)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TAX1", "ชื่อย่อย1", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GRPID2", "กลุ่มย่อย2", 90)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TAX2", "ชื่อย่อย2", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_IMAGEPATH", "PathImage")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("SPC_GiftPoint", "ให้แต้ม")));
            
            radGridView_Show.Columns["SPC_ITEMACTIVE"].IsPinned = true;
            radGridView_Show.Columns["ITEMID"].IsPinned = true;
            radGridView_Show.Columns["INVENTDIMID"].IsPinned = true;
            radGridView_Show.Columns["ITEMBARCODE"].IsPinned = true;

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radGridView_Show.MasterTemplate.EnableCustomFiltering = true; //บังคับให้ code ต้องใส่ เมื่อ ใช้การกรองแบบ Enter

            Cursor.Current = Cursors.WaitCursor;
            dtData = Class.ItembarcodeClass.FindItembarcodeAll(" TOP 1000 ", _pCon);
            radGridView_Show.DataSource = dtData;
            Cursor.Current = Cursors.Default;
        }
        //close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //choose
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่มีข้อมูลที่เลือก ลองใหม่อีกครั้ง.");
                return;
            }

            if (radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "")
            {
                MsgBoxClass.MsgBoxShow_ChooseDataWarning("ข้อมูลบาร์โค้ด");
                return;
            }


            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        #region "ROWS DGV"

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        #region "Enter Filler"
        private bool EnterPress = false;
        private void Kel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EnterPress = true;
            }
        }

        private void RadGridView_Show_FilterChanging(object sender, GridViewCollectionChangingEventArgs e)
        {

            if (!EnterPress)
            {
                e.Cancel = true;
            }

            EnterPress = false;

        }

        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Row is GridViewFilteringRowInfo)
            {
                RadTextBoxEditor ed = e.ActiveEditor as RadTextBoxEditor;
                RadTextBoxEditorElement el = ed.EditorElement as RadTextBoxEditorElement;
                el.KeyDown -= Kel_KeyDown;
                el.KeyDown += Kel_KeyDown;
            }

        }

        private void RadGridView_Show_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {
                        string pCon = _pCon;
            foreach (var filterD in radGridView_Show.FilterDescriptors)
            {
                if (filterD.Operator == Telerik.WinControls.Data.FilterOperator.Contains)
                    pCon += $" AND {filterD.Expression.Replace(" ", "%").Replace("%LIKE%", " LIKE ")} ";
                else
                    pCon += $" AND {filterD.Expression}";
            }

            string pTop = "";
            if (pCon == "") { pTop = " TOP 1000 "; pCon = ""; }

            Cursor.Current = Cursors.WaitCursor;
            dtData = Class.ItembarcodeClass.FindItembarcodeAll(pTop, pCon);
            radGridView_Show.DataSource = dtData;
            Cursor.Current = Cursors.Default;
        }
        #endregion

        private void RadGridView_Show_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) { return; }
            if (e.CurrentRow.Index > -1)
            {
                DataRow rowItem = ((DataRowView)e.CurrentRow.DataBoundItem).Row;
                items = new Data_ITEMBARCODE(rowItem);
            }


        }
    }
}
