﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public partial class ShowDataDGV_Customer : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData;


        public Data_CUSTOMER cust;
        readonly string _pCon;

        public ShowDataDGV_Customer(string pCon)
        {
            InitializeComponent();
            _pCon = pCon;
        }

        private void ShowDataDGV_Customer_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "รหัส", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 300)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PHONE", "เบอร์โทร", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEALIAS", "ชื่อค้นหา", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("IDENTIFICATIONNUMBER", "บัตร ปชช/เลขเสียภาษี", 150)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PartyTypeName", "สถานะ", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_REMAINPOINT", "แต้มสะสม", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PriceGroup", "ระดับราคาส่ง", 60)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CREDITMAX", "วงเงินเครดิต", 100)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ADDRESS", "ที่อยู่", 400)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_IMAGEPATH", "รูป")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PartyType", "PartyType")));

            dtData = Models.CustomerClass.FindCust_ByCustID("TOP 1000 ", _pCon);
            radGridView_Show.DataSource = dtData;
        }
        //close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //choose
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.CurrentRow.Cells["ACCOUNTNUM"].Value.ToString() == "")
            {
                MsgBoxClass.MsgBoxShow_ChooseDataWarning("ข้อมูลลูกค้า");
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        #region "ROWS DGV"


        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        #region "Enter Filler"
        private bool EnterPress = false;
        private void Kel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) EnterPress = true;
        }

        private void RadGridView_Show_FilterChanging(object sender, GridViewCollectionChangingEventArgs e)
        {

            if (!EnterPress) e.Cancel = true;
         
            EnterPress = false;

        }


        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {

            string pCon = string.Empty;
            foreach (var filterD in radGridView_Show.FilterDescriptors)
            {
                if (filterD.Operator == Telerik.WinControls.Data.FilterOperator.Contains)
                    pCon += $" AND {filterD.Expression.Replace(" ", "%").Replace("%LIKE%", " LIKE ")} ";
                else
                    pCon += $" AND {filterD.Expression}";
            }

            string pTop = "";
            if (pCon == "") { pTop = " TOP 1000 "; pCon = ""; }

            Cursor.Current = Cursors.WaitCursor;
            dtData = Models.CustomerClass.FindCust_ByCustID(pTop, pCon);
            radGridView_Show.DataSource = dtData;
            Cursor.Current = Cursors.Default;

        }



        #endregion

        private void RadGridView_Show_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow.Index > -1)
            {
                DataRow rows = ((DataRowView)e.CurrentRow.DataBoundItem).Row;
                cust = new Data_CUSTOMER(rows);
            }

        }

        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Row is GridViewFilteringRowInfo)
            {
                RadTextBoxEditor ed = e.ActiveEditor as RadTextBoxEditor;
                RadTextBoxEditorElement el = ed.EditorElement as RadTextBoxEditorElement;
                el.KeyDown -= Kel_KeyDown;
                el.KeyDown += Kel_KeyDown;
            }
        }
    }
}
