﻿//CheckOK
using System;
using System.Data;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public partial class Data_CUSTOMER
    {
        private readonly string _Customer_ACCOUNTNUM = "ACCOUNTNUM";
        public string Customer_ACCOUNTNUM { get; set; }

        private readonly string _Customer_NAME = "NAME";
        public string Customer_NAME { get; set; }

        private readonly string _Customer_PHONE = "PHONE";
        public string Customer_PHONE { get; set; }

        private readonly string _Customer_NAMEALIAS = "NAMEALIAS";
        public string Customer_NAMEALIAS { get; set; }

        private readonly string _Customer_IDENTIFICATIONNUMBER = "IDENTIFICATIONNUMBER";
        public string Customer_IDENTIFICATIONNUMBER { get; set; }

        private readonly string _Customer_SPC_REMAINPOINT = "SPC_REMAINPOINT";
        public string Customer_SPC_REMAINPOINT { get; set; }

        private readonly string _Customer_ADDRESS = "ADDRESS";
        public string Customer_ADDRESS { get; set; }

        private readonly string _Customer_CREDITMAX = "CREDITMAX";
        public string Customer_CREDITMAX { get; set; }

        private readonly string _Customer_PriceGroup = "PriceGroup";
        public string Customer_PriceGroup { get; set; }


        private readonly string _Customer_SPC_IMAGEPATH = "SPC_IMAGEPATH";
        public string Customer_SPC_IMAGEPATH { get; set; }


        private readonly string _Customer_PartyType = "PartyType";
        public string Customer_PartyType { get; set; }

        private readonly string _Customer_PartyTypeName = "PartyTypeName";
        public string Customer_PartyTypeName { get; set; }


        //search Customer currenrow in gridview :: ShowDataDGV_Customer
        public Data_CUSTOMER(DataRow rows)
        {
            if (rows != null)
            {
                Customer_ACCOUNTNUM = rows[_Customer_ACCOUNTNUM].ToString();
                Customer_NAME = rows[_Customer_NAME].ToString();
                Customer_PHONE = rows[_Customer_PHONE].ToString();
                Customer_NAMEALIAS = rows[_Customer_NAMEALIAS].ToString();
                Customer_IDENTIFICATIONNUMBER = rows[_Customer_IDENTIFICATIONNUMBER].ToString();
                Customer_SPC_REMAINPOINT = rows[_Customer_SPC_REMAINPOINT].ToString();
                Customer_ADDRESS = rows[_Customer_ADDRESS].ToString();
                Customer_CREDITMAX = rows[_Customer_CREDITMAX].ToString();
                Customer_PriceGroup = rows[_Customer_PriceGroup].ToString();
                Customer_SPC_IMAGEPATH = rows[_Customer_SPC_IMAGEPATH].ToString();
                Customer_PartyType = rows[_Customer_PartyType].ToString();
                Customer_PartyTypeName = rows[_Customer_PartyTypeName].ToString();
            }
        }

        public Data_CUSTOMER(String cstID)
        {
            // DataTable dtCst = Class.CustomerClass.FindCust_ByCustID("", " AND ACCOUNTNUM = '" + cstID + @"'");
            DataTable dtCst = Models.CustomerClass.FindCust_ByCustID("", " AND ACCOUNTNUM = '" + cstID + @"'  ");
            if (dtCst.Rows.Count == 0) dtCst = Models.CustomerClass.FindCust_ByCustID("", " AND NameAlias = '" + cstID + @"' OR IDENTIFICATIONNUMBER = '" + cstID + @"' OR PHONE = '" + cstID + @"' ");

            if (dtCst.Rows.Count > 0)
            {
                Customer_ACCOUNTNUM = dtCst.Rows[0]["ACCOUNTNUM"].ToString();
                Customer_NAME = dtCst.Rows[0]["NAME"].ToString();
                Customer_PHONE = dtCst.Rows[0]["PHONE"].ToString();
                Customer_NAMEALIAS = dtCst.Rows[0]["NAMEALIAS"].ToString();
                Customer_IDENTIFICATIONNUMBER = dtCst.Rows[0]["IDENTIFICATIONNUMBER"].ToString();
                Customer_SPC_REMAINPOINT = dtCst.Rows[0]["SPC_REMAINPOINT"].ToString();
                Customer_ADDRESS = dtCst.Rows[0]["ADDRESS"].ToString();
                Customer_CREDITMAX = dtCst.Rows[0]["CREDITMAX"].ToString();
                Customer_PriceGroup = dtCst.Rows[0]["PriceGroup"].ToString();
                Customer_SPC_IMAGEPATH = dtCst.Rows[0]["SPC_IMAGEPATH"].ToString();

                Customer_PartyType = dtCst.Rows[0]["PartyType"].ToString();
                Customer_PartyTypeName = dtCst.Rows[0]["PartyTypeName"].ToString();
            }

        }


    }
}
