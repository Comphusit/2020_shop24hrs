﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Collections;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public partial class ShowDataDGV : Telerik.WinControls.UI.RadForm
    {
        public DataTable dtData;

        public string pID;
        public string pID2;
        public string pDesc;
        public string pDesc2;
        public string pSheet;
        public string pID3;
        public string pDesc3;
        
        public string pBarcode;
        public double pFactor;
        public double pQtyBeforeRecive;

        readonly string _pShow;

        string case3_TransID, case3_QtyRecive;
        public ShowDataDGV(string pShow = "0") //pShow 3 = ใบสั่งซื้อ 4 = พิมพ์ใบจัดซื้อ MNPT
        {
            InitializeComponent();
            _pShow = pShow;

        }

        private void ShowDataDGV_Load(object sender, EventArgs e)
        {
            radLabel_Desc.Visible = false;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_split.ButtonElement.ShowBorder = true; radButton_split.Visible = false;

            if (_pShow == "2")
            {
                this.Size = new Size(366, 300);
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TABLE_NAME", "ชื่อเอกสาร", 295)));
                //radGridView_Show.DataSource = dtData;
            }
            else if (_pShow == "3")
            {
                //this.Size = new Size(1024, 768);
                case3_TransID = pID3;
                case3_QtyRecive = pDesc3;
                radLabel_Desc.Visible = true;
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_PURCHDATE", "วันที่ใบสั่งซื้อ", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่ใบสั่งซื้อ", 120)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค้ด", 120)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 300)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PURCHQTY", "จำนวนสั่ง", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHUNIT", "หน่วย", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYORDERED", "จำนวนทั้งหมด", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTTRANSID", "INVENTTRANSID")));//, 130
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVEALL", "จำนวนที่รับแล้ว", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("STADESC", "สถานะใบสั่งซื้อ", 120)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBUYERGROUPID", "รหัสจัดซื้อ", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDACCOUNT", "ผู้จำหน่าย", 100)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHNAME", "ชื่อผู้จำหน่าย", 200)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("FACTOR", "อัตราส่วน", 120)));

                radGridView_Show.Columns["SPC_PURCHDATE"].IsPinned = true;
                radGridView_Show.Columns["PURCHID"].IsPinned = true;
                if (pID2 == "0") radButton_Save.Enabled = false;
                if ((SystemClass.SystemDptID == "D030") || (SystemClass.SystemDptID == "D117") || (SystemClass.SystemDptID == "D158")
                    || (SystemClass.SystemDptID == "D156") || (SystemClass.SystemDptID == "D179")
                    || (SystemClass.SystemDptID == "D151") || (SystemClass.SystemDptID == "D015") || (SystemClass.SystemDptID == "D051"))
                {
                    radButton_split.Visible = true;
                }
            }
            else if (_pShow == "4")
            {
                //this.Size = new Size(800, 600);
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่บิล", 120)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDERNUM", "เลขที่บิล", 140)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขา", 80)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTONAME", "ชื่อสาขา", 180)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลัง", 120)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "พนักงานจัด", 80)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงานจัดสินค้า", 250)));

            }
            else
            {
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATA_ID", "รหัส", 170)));
                radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATA_DESC", "คำอธิบาย", 550)));

                if (_pShow == "1")
                {
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATA_ID2", "แผนก", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATA_DESC2", "ชื่อแผนก", 300)));

                }
                else
                {
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DATA_ID2", "รหัส2")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DATA_DESC2", "คำอธิบาย2")));

                }
            }
            radGridView_Show.DataSource = dtData;
        }
        //close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //choose
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (_pShow == "2")
            {
                if (radGridView_Show.CurrentRow.Cells["TABLE_NAME"].Value.ToString() == "")
                {
                    MsgBoxClass.MsgBoxShow_ChooseDataWarning("ข้อมูล"); return;
                }

                try
                {
                    pSheet = radGridView_Show.CurrentRow.Cells["TABLE_NAME"].Value.ToString(); //เก็บค่าชื่อ Sheet ที่เลือก
                }
                catch (Exception)
                { pSheet = ""; }

                this.DialogResult = DialogResult.Yes;
                this.Close();
                return;
            }

            if (_pShow == "3")
            {
                if (radGridView_Show.CurrentRow.Cells["INVENTTRANSID"].Value.ToString() == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลใบสั่งซื้อก่อนกดตกลง"); return;
                }

                if (pDesc != "")
                {
                    if ((pDesc == radGridView_Show.CurrentRow.Cells["INVENTTRANSID"].Value.ToString()) && (pID == radGridView_Show.CurrentRow.Cells["PURCHID"].Value.ToString()))
                    {
                        this.DialogResult = DialogResult.No;
                        this.Close();
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"รายการนี้มีการอ้าง PO เรียบร้อยแล้ว{Environment.NewLine}ต้องการแก้ไขข้อมูลหรือไม่ ?") != DialogResult.Yes) return;
                    string sqlDelLog = $@"
                                DELETE  [ProductRecive_LOGPO] 
                                WHERE   TRANSID = '{pID3}' ";
                    string T = ConnectionClass.ExecuteSQL_SentServer(sqlDelLog, IpServerConnectClass.ConSupcAndroid);
                    if (T != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T); return;
                    }
                }

                double checkReciveOld = double.Parse(radGridView_Show.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString());
                if (pDesc == radGridView_Show.CurrentRow.Cells["INVENTTRANSID"].Value.ToString()) checkReciveOld -= double.Parse(pSheet);

                string rmk = "";
                double checkQtySum = (double.Parse(pSheet) + checkReciveOld);
                double checkQtyDiff = checkQtySum - double.Parse(radGridView_Show.CurrentRow.Cells["QTYORDERED"].Value.ToString());
                if (checkQtyDiff != 0)
                {
                    string checkStr = "มากกว่า";
                    if (checkQtyDiff < 0)
                    {
                        checkStr = "น้อยกว่า";
                        checkQtyDiff *= -1;
                    }

                    ShowRemark _showRemark = new ShowRemark("1")
                    {
                        pDesc = $@"หมายเหตุ : การรับสินค้าเข้าคลัง{Environment.NewLine}ที่จำนวนรับ {checkStr} PO อยู่ {checkQtyDiff:N2} ชิ้น"
                    };

                    if (_showRemark.ShowDialog(this) != DialogResult.Yes)
                    { MsgBoxClass.MsgBoxShowButtonOk_Error("ต้องระบุหมายเหตุก่อนบันทึกเท่านั้น"); return; }
                    rmk = _showRemark.pRmk;
                }

                pID = radGridView_Show.CurrentRow.Cells["PURCHID"].Value.ToString();
                pID2 = radGridView_Show.CurrentRow.Cells["PURCHQTY"].Value.ToString();
                pDesc = radGridView_Show.CurrentRow.Cells["INVENTTRANSID"].Value.ToString();
                pDesc2 = radGridView_Show.CurrentRow.Cells["QTYORDERED"].Value.ToString();
                pID3 = radGridView_Show.CurrentRow.Cells["VENDACCOUNT"].Value.ToString();
                pDesc3 = radGridView_Show.CurrentRow.Cells["PURCHNAME"].Value.ToString();

                pBarcode = radGridView_Show.CurrentRow.Cells["BARCODE"].Value.ToString();
                pFactor = Double.Parse(radGridView_Show.CurrentRow.Cells["FACTOR"].Value.ToString());
                pQtyBeforeRecive = Double.Parse(radGridView_Show.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString());

                pSheet = rmk;

                this.DialogResult = DialogResult.Yes;
                this.Close();
                return;
            }

            if (_pShow == "4")
            {
                if (radGridView_Show.CurrentRow.Cells["ORDERNUM"].Value.ToString() == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลใบโอนย้ายก่อนกดตกลง"); return;
                }

                pID = radGridView_Show.CurrentRow.Cells["ORDERNUM"].Value.ToString();

                this.DialogResult = DialogResult.Yes;
                this.Close();
                return;
            }


            if (radGridView_Show.CurrentRow.Cells["DATA_ID"].Value.ToString() == "")
            {
                MsgBoxClass.MsgBoxShow_ChooseDataWarning("ข้อมูล");
                return;
            }
            pID = radGridView_Show.CurrentRow.Cells["DATA_ID"].Value.ToString();
            pDesc = radGridView_Show.CurrentRow.Cells["DATA_DESC"].Value.ToString();

            try
            {
                if (radGridView_Show.CurrentRow.Cells["DATA_ID2"].Value != null) pID2 = radGridView_Show.CurrentRow.Cells["DATA_ID2"].Value.ToString(); else pID2 = "";
            }
            catch (Exception) { pID2 = ""; }

            try
            {
                if (radGridView_Show.CurrentRow.Cells["DATA_DESC2"].Value != null) pDesc2 = radGridView_Show.CurrentRow.Cells["DATA_DESC2"].Value.ToString(); else pDesc2 = "";
            }
            catch (Exception) { pDesc2 = ""; }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //แยกรายการ จากการรับ
        private void RadButton_split_Click(object sender, EventArgs e)
        {
            if (_pShow != "3") return;
            if (pDesc != "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"รายการนี้มีการอ้าง PO เรียบร้อยแล้ว{Environment.NewLine}ไม่สามารถแยกรายการได้");
                return;
            }
            InputData inputData = new InputData("0", "จำนวนที่ต้องการแยกรายการ", "", "");
            if (inputData.ShowDialog() == DialogResult.Yes)
            {
                double newQty = double.Parse(inputData.pInputData);
                double oldQty = double.Parse(case3_QtyRecive);
                if (newQty >= oldQty)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"จำนวนที่แยกรายการจะต้องไม่มากกว่าจำนวนที่มีอยู่เดิม{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง"); return;
                }
                double oldUpdate = oldQty - newQty;

                string newTransID = $@"{SystemClass.SystemUserID}_{DateTime.Now:yy-MM-dd}{DateTime.Now:HH:mm:ss}".Replace("-", "").Replace(":", "");

                ArrayList sqlSplit = new ArrayList() {
                $@"
                INSERT INTO [dbo].[ProductRecive_DROID]
                           ([TRANSID],[DATETIMERECIVE]
                           ,[INVENTLOCATIONFROM],[INVENTLOCATIONTO],[EMPLIDRECIVE],[EMPLNAMERECIVE],[DIMENSION],[DIMENSIONNAME]
                           ,[DEVICENAME]
                           ,[VENDID],[VENDNAME],[BUYERID],[BUYERNAME]
		                   ,[ITEMID],[INVENTDIMID],[ITEMBARCODE],[SPCITEMNAME],[QTY],[UNITID]
                           ,[QTYRECIVE]
                           ,[REMARK],[REMARKDIFF]         
                           ,[EMPLIDUPDATE],[EMPLNAMEUPDATE],[DATETIMEUPDATE]
                           )

                SELECT	    '{newTransID}' AS [TRANSID],[DATETIMERECIVE]
                           ,[INVENTLOCATIONFROM],[INVENTLOCATIONTO],[EMPLIDRECIVE],[EMPLNAMERECIVE],[DIMENSION],[DIMENSIONNAME]
                           ,'{SystemClass.SystemPcName}' AS [DEVICENAME]
		                   ,[VENDID],[VENDNAME],BUYERID,BUYERNAME
                           ,[ITEMID],[INVENTDIMID],[ITEMBARCODE],[SPCITEMNAME],[QTY],[UNITID]
                           ,'{newQty}' AS [QTYRECIVE]
                           ,'แยกรายการ' AS [REMARK],'{case3_TransID}' AS REMARKDIFF
		                   ,'{SystemClass.SystemUserID}' AS [EMPLIDUPDATE],'{SystemClass.SystemUserName}' AS [EMPLNAMEUPDATE],GETDATE() AS [DATETIMEUPDATE]
                FROM	ProductRecive_DROID WITH (NOLOCK)
                WHERE	TRANSID = '{case3_TransID}'
                ",
                $@"
                UPDATE	[ProductRecive_DROID]
                SET		QTYRECIVE = '{oldUpdate}'
                        ,[EMPLIDUPDATE] = '{SystemClass.SystemUserID}',[EMPLNAMEUPDATE] = '{SystemClass.SystemUserName}',[DATETIMEUPDATE] = GETDATE()
                WHERE	TRANSID =  '{case3_TransID}' "
                };

                string resualt = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlSplit, IpServerConnectClass.ConSupcAndroid);
                if (resualt == "")
                {
                    pDesc3 = "R";
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    return;
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                    return;
                }
            }
        }
    }
}
