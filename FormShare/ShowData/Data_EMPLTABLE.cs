﻿//CheckOK
using System;
using System.Data;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public   class Data_EMPLTABLE
    {
        readonly  private  string _EMPLTABLE_EMPLID = "EMPLID";
        public  string EMPLTABLE_EMPLID { get; set; }

        readonly private string _EMPLTABLE_EMPLID_M = "EMPLID_M";
        public string EMPLTABLE_EMPLID_M { get; set; }

        readonly private string _EMPLTABLE_ALTNUM = "ALTNUM";
        public string EMPLTABLE_ALTNUM { get; set; }

        readonly private string _EMPLTABLE_SPC_NAME = "SPC_NAME";
        public string EMPLTABLE_SPC_NAME { get; set; }

        readonly private string _EMPLTABLE_NUM = "NUM";
        public string EMPLTABLE_NUM { get; set; }

        readonly private string _EMPLTABLE_DESCRIPTION = "DESCRIPTION";
        public string EMPLTABLE_DESCRIPTION { get; set; }

        readonly private string _EMPLTABLE_NickName = "IVZ_HRPANickName";
        public string EMPLTABLE_NickName { get; set; }

        readonly private string _EMPLTABLE_PositionTitle = "IVZ_HROMPositionTitle";
        public string EMPLTABLE_PositionTitle { get; set; }

        readonly private string _EMPLTABLE_POSSITION = "POSSITION";
        public string EMPLTABLE_POSSITION { get; set; }

        readonly private string _EMPLTABLE_SPC_PHONEMOBILE = "SPC_PHONEMOBILE";
        public string EMPLTABLE_SPC_PHONEMOBILE { get; set; }

        readonly private string _EMPLTABLE_EMP_PASSWORD = "EMP_PASSWORD";
        public string EMPLTABLE_EMP_PASSWORD { get; set; }

        readonly private string _EMPLTABLE_EMP_AccountNum = "AccountNum";
        public string EMPLTABLE_EMP_AccountNum { get; set; }

        readonly private string _EMPLTABLE_STA_SHOP24 = "STA_SHOP24";
        public string EMPLTABLE_STA_SHOP24 { get; set; }

       
        //ส่ง string EMPLID เข้ามาหาพนักงาน
        //case 1 ค้นหารหัสพนักงานใน  ที่มีการบันทึกข้อมูลใน shop24
        //case 2 ค้นหารหัสพนักงานจาก EPMLTABLE
        public Data_EMPLTABLE(String Empl)
        {
            DataTable DtEmpl  = Class.Models.EmplClass.GetEmployee( Empl );
           
            if (DtEmpl.Rows.Count > 0)
            {
                EMPLTABLE_EMPLID = DtEmpl.Rows[0][_EMPLTABLE_EMPLID].ToString();
                EMPLTABLE_EMPLID_M = DtEmpl.Rows[0][_EMPLTABLE_EMPLID_M].ToString();

                EMPLTABLE_ALTNUM = DtEmpl.Rows[0][_EMPLTABLE_ALTNUM].ToString(); 
                EMPLTABLE_SPC_NAME = DtEmpl.Rows[0][_EMPLTABLE_SPC_NAME].ToString();
                EMPLTABLE_NUM = DtEmpl.Rows[0][_EMPLTABLE_NUM].ToString();
                EMPLTABLE_DESCRIPTION = DtEmpl.Rows[0][_EMPLTABLE_DESCRIPTION].ToString();
                EMPLTABLE_NickName = DtEmpl.Rows[0][_EMPLTABLE_NickName].ToString();
                EMPLTABLE_PositionTitle = DtEmpl.Rows[0][_EMPLTABLE_PositionTitle].ToString();
                EMPLTABLE_POSSITION = DtEmpl.Rows[0][_EMPLTABLE_POSSITION].ToString();
                EMPLTABLE_SPC_PHONEMOBILE = DtEmpl.Rows[0][_EMPLTABLE_SPC_PHONEMOBILE].ToString();
                EMPLTABLE_EMP_PASSWORD = DtEmpl.Rows[0][_EMPLTABLE_EMP_PASSWORD].ToString();
                EMPLTABLE_EMP_AccountNum = DtEmpl.Rows[0][_EMPLTABLE_EMP_AccountNum].ToString();
                EMPLTABLE_STA_SHOP24 = DtEmpl.Rows[0][_EMPLTABLE_ALTNUM].ToString();
            }
            else 
            {
                DtEmpl = Class.Models.EmplClass.GetEmployee_Altnum(Empl);
                if (DtEmpl.Rows.Count > 0)
                {
                    EMPLTABLE_EMPLID = DtEmpl.Rows[0][_EMPLTABLE_EMPLID].ToString();
                    EMPLTABLE_EMPLID_M = DtEmpl.Rows[0][_EMPLTABLE_EMPLID].ToString();

                    EMPLTABLE_ALTNUM = DtEmpl.Rows[0][_EMPLTABLE_ALTNUM].ToString();
                    EMPLTABLE_SPC_NAME = DtEmpl.Rows[0][_EMPLTABLE_SPC_NAME].ToString();
                    EMPLTABLE_NUM = DtEmpl.Rows[0][_EMPLTABLE_NUM].ToString();
                    EMPLTABLE_DESCRIPTION = DtEmpl.Rows[0][_EMPLTABLE_DESCRIPTION].ToString();
                    EMPLTABLE_NickName = DtEmpl.Rows[0][_EMPLTABLE_NickName].ToString();
                    EMPLTABLE_PositionTitle = DtEmpl.Rows[0][_EMPLTABLE_PositionTitle].ToString();
                    EMPLTABLE_POSSITION = DtEmpl.Rows[0][_EMPLTABLE_POSSITION].ToString();
                    EMPLTABLE_SPC_PHONEMOBILE = DtEmpl.Rows[0][_EMPLTABLE_SPC_PHONEMOBILE].ToString();
                    EMPLTABLE_EMP_AccountNum = DtEmpl.Rows[0][_EMPLTABLE_EMP_AccountNum].ToString();
                    EMPLTABLE_STA_SHOP24   = DtEmpl.Rows[0][_EMPLTABLE_STA_SHOP24].ToString();
                }
            }
        }

 
        public Data_EMPLTABLE(DataTable DtEmpl)
        { 
             
            if (DtEmpl.Rows.Count > 0)
            {
                DataColumnCollection columns = DtEmpl.Columns;
                if (columns.Contains(_EMPLTABLE_EMPLID))
                { EMPLTABLE_EMPLID = DtEmpl.Rows[0][_EMPLTABLE_EMPLID].ToString(); }
                else { EMPLTABLE_EMPLID = ""; }
                
                if (columns.Contains(_EMPLTABLE_EMPLID_M))
                { EMPLTABLE_EMPLID_M = DtEmpl.Rows[0][_EMPLTABLE_EMPLID_M].ToString(); }
                else { EMPLTABLE_EMPLID_M = ""; }

                if (columns.Contains(_EMPLTABLE_ALTNUM))
                { EMPLTABLE_ALTNUM = DtEmpl.Rows[0][_EMPLTABLE_ALTNUM].ToString(); }
                else { EMPLTABLE_ALTNUM = ""; }

                if (columns.Contains(_EMPLTABLE_SPC_NAME))
                { EMPLTABLE_SPC_NAME = DtEmpl.Rows[0][_EMPLTABLE_SPC_NAME].ToString(); }
                else { _EMPLTABLE_SPC_NAME = ""; }

                if (columns.Contains(_EMPLTABLE_NUM))
                { EMPLTABLE_NUM = DtEmpl.Rows[0][_EMPLTABLE_NUM].ToString(); }
                else { _EMPLTABLE_NUM = ""; }

                if (columns.Contains(_EMPLTABLE_DESCRIPTION))
                { EMPLTABLE_DESCRIPTION = DtEmpl.Rows[0][_EMPLTABLE_DESCRIPTION].ToString(); }
                else { _EMPLTABLE_DESCRIPTION = ""; }

                if (columns.Contains(_EMPLTABLE_NickName))
                { EMPLTABLE_NickName = DtEmpl.Rows[0][_EMPLTABLE_NickName].ToString(); }
                else { _EMPLTABLE_NickName = ""; }

                if (columns.Contains(_EMPLTABLE_PositionTitle))
                { EMPLTABLE_PositionTitle = DtEmpl.Rows[0][_EMPLTABLE_PositionTitle].ToString(); }
                else { _EMPLTABLE_PositionTitle = ""; }

                if (columns.Contains(_EMPLTABLE_POSSITION))
                { EMPLTABLE_POSSITION = DtEmpl.Rows[0][_EMPLTABLE_POSSITION].ToString(); }
                else { _EMPLTABLE_POSSITION = ""; }

                if (columns.Contains(_EMPLTABLE_SPC_PHONEMOBILE))
                { EMPLTABLE_SPC_PHONEMOBILE = DtEmpl.Rows[0][_EMPLTABLE_SPC_PHONEMOBILE].ToString(); }
                else { _EMPLTABLE_SPC_PHONEMOBILE = ""; }

                if (columns.Contains(_EMPLTABLE_STA_SHOP24))
                { EMPLTABLE_STA_SHOP24 = DtEmpl.Rows[0][_EMPLTABLE_STA_SHOP24].ToString(); }
                else { _EMPLTABLE_STA_SHOP24 = ""; }

                //EMPLTABLE_SPC_PHONEMOBILE = DtEmpl.Rows[0][_EMPLTABLE_SPC_PHONEMOBILE].ToString();
                //EMPLTABLE_EMP_PASSWORD = DtEmpl.Rows[0][_EMPLTABLE_EMP_PASSWORD].ToString();
                //EMPLTABLE_EMP_AccountNum = DtEmpl.Rows[0][_EMPLTABLE_EMP_AccountNum].ToString();


                //EMPLTABLE_ALTNUM = DtEmpl.Rows[0][_EMPLTABLE_ALTNUM].ToString(); 
                //EMPLTABLE_SPC_NAME = DtEmpl.Rows[0][_EMPLTABLE_SPC_NAME].ToString();
                //EMPLTABLE_NUM = DtEmpl.Rows[0][_EMPLTABLE_NUM].ToString();
                //EMPLTABLE_DESCRIPTION = DtEmpl.Rows[0][_EMPLTABLE_DESCRIPTION].ToString(); 
                //EMPLTABLE_NickName = DtEmpl.Rows[0][_EMPLTABLE_NickName].ToString();
                //EMPLTABLE_PositionTitle = DtEmpl.Rows[0][_EMPLTABLE_PositionTitle].ToString();
                //EMPLTABLE_POSSITION = DtEmpl.Rows[0][_EMPLTABLE_POSSITION].ToString();
                //EMPLTABLE_SPC_PHONEMOBILE = DtEmpl.Rows[0][_EMPLTABLE_SPC_PHONEMOBILE].ToString(); 
            } 
        }

      
    }
     
}
