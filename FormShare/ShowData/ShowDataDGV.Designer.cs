﻿namespace PC_Shop24Hrs.FormShare.ShowData
{
    partial class ShowDataDGV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowDataDGV));
            this.panel1 = new System.Windows.Forms.Panel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07 = new Telerik.WinControls.RootRadElement();
            this.radButton_split = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Desc = new Telerik.WinControls.UI.RadLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_split)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.radGridView_Show);
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(831, 529);
            this.panel1.TabIndex = 0;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(831, 529);
            this.radGridView_Show.TabIndex = 16;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Cancel.Location = new System.Drawing.Point(415, 535);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 8;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Save.Location = new System.Drawing.Point(317, 535);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 7;
            this.radButton_Save.Text = "เลือก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "เลือก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07
            // 
            this.object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07.Name = "object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07";
            this.object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07.StretchHorizontally = true;
            this.object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07.StretchVertically = true;
            // 
            // radButton_split
            // 
            this.radButton_split.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_split.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_split.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_split.Location = new System.Drawing.Point(732, 537);
            this.radButton_split.Name = "radButton_split";
            this.radButton_split.Size = new System.Drawing.Size(99, 32);
            this.radButton_split.TabIndex = 9;
            this.radButton_split.Text = "แยกรายการ";
            this.radButton_split.ThemeName = "Fluent";
            this.radButton_split.Click += new System.EventHandler(this.RadButton_split_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_split.GetChildAt(0))).Text = "แยกรายการ";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_split.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_split.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_split.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_split.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_split.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_split.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_Desc
            // 
            this.radLabel_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel_Desc.AutoSize = false;
            this.radLabel_Desc.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_Desc.BorderVisible = true;
            this.radLabel_Desc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Desc.ForeColor = System.Drawing.Color.Red;
            this.radLabel_Desc.Location = new System.Drawing.Point(0, 535);
            this.radLabel_Desc.Name = "radLabel_Desc";
            this.radLabel_Desc.Size = new System.Drawing.Size(291, 32);
            this.radLabel_Desc.TabIndex = 71;
            this.radLabel_Desc.Text = "สินค้าที่เกิน PO จะคิดเป็นสินค้าเพิ่ม จ/ซ นำไปจ่ายบิลไม่ได้ทุกกรณี (สินค้าแถมต้อง" +
    "แยกรายการก่อนทุกครั้ง)";
            this.radLabel_Desc.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // ShowDataDGV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(833, 570);
            this.Controls.Add(this.radLabel_Desc);
            this.Controls.Add(this.radButton_split);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "ShowDataDGV";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แสดงข้อมูลให้เลือก.";
            this.Load += new System.EventHandler(this.ShowDataDGV_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_split)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.RootRadElement object_21aa9bbb_2bac_42e5_a6be_63649b9e3f07;
        protected Telerik.WinControls.UI.RadButton radButton_split;
        private Telerik.WinControls.UI.RadLabel radLabel_Desc;
    }
}
