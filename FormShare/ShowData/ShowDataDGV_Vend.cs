﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.FormShare.ShowData
{
    public partial class ShowDataDGV_Vend : Telerik.WinControls.UI.RadForm
    {
        public DataTable dtData;

        public DataRowView rowsVend;

        public static Data_VENDTABLE Vend;
        private readonly string DimensionID;

        public ShowDataDGV_Vend(string _dimensionID)
        {
            InitializeComponent();
            DimensionID = _dimensionID;
        }

        private void ShowDataDGV_Vend_Load(object sender, EventArgs e)
        {

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "รหัส", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อ", 180)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ADDRESS", "ที่อยู่", 300)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PHONE", "เบอร์โทร", 180)));
             
            dtData = Class.Models.VendTableClass.GetVENDTABLE_All("", DimensionID);
            radGridView_Show.DataSource = dtData;

        }
        //close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {

            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //choose
        private void RadButton_Save_Click(object sender, EventArgs e)
        {


            this.DialogResult = DialogResult.Yes;
            this.Close();

        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        private void RadGridView_Show_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow.Index > -1)
            {
                DataRow rowItem = ((DataRowView)e.CurrentRow.DataBoundItem).Row;
                Vend = new Data_VENDTABLE(rowItem);
            }
        }
    }
}
