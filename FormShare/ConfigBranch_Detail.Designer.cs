﻿namespace PC_Shop24Hrs.FormShare
{
    partial class ConfigBranch_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigBranch_Detail));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel1.Controls.Add(this.radStatusStrip1);
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(817, 42);
            this.radPanel1.TabIndex = 4;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(817, 42);
            this.radStatusStrip1.TabIndex = 8;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.UseCompatibleTextRendering = false;
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(817, 574);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_Show_CellBeginEdit);
            this.RadGridView_Show.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellEndEdit);
            this.RadGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(823, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // ConfigBranch_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigBranch_Detail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "กำหนดสาขาที่ผู้จำหน่ายลงสินค้า.";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ConfigBranch_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
    }
}
