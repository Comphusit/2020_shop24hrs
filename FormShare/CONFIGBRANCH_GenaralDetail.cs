﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Data;
using PC_Shop24Hrs.FormShare.ShowData;
using System.Collections;

namespace PC_Shop24Hrs.FormShare
{
    public partial class ConfigBranch_GenaralDetail : Telerik.WinControls.UI.RadForm
    {
        DataTable dt;
        string StatusSave = "";//0 = Insert , 1 = Update

        readonly string _pTypeConfig;//id TypeConfig
        readonly string _pTypeConfigName;//name TypeConfig
        readonly string _pRunIDAuto; // ID ให้กำหนดเอง หรือ rum Auo จาก DB -- 0 Auto , 1 กำหนดเอง
        readonly string _pIDDiffName; // ID และ Name ให้เหมือนกันมั้ย -- 0 เหมือนกัน ,1 ไม่เหมือนกัน
        readonly string _pStaImage; // มีรูปมาเกี่ยวข้องมั้ย -- 0 มี , 1 ไม่มี
        readonly string _pFirst; //ขึ้นต้นด้วยอะไร
        readonly string _pCase; // case อะไรที่ใช้ในการค้นหาเลขที่บิล [ถ้าไม่ run id auto ตรงนี้ไม่มีผล]
        readonly string _pPermission;// 0 ไม่มีสิด เพิ่ม แก้ไข 1 มีสิด
        string pLock;

        readonly string pDescForImage = "จำนวนรูปสูงสุด";

        public ConfigBranch_GenaralDetail(string pTypeConfig, string pTypeConfigName,
            string pRunIDAuto0, string pIDDiffName1, string pStaImage0, string pFirst, string pCase, string pPermission = "1")
        {
            InitializeComponent();
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "ใช้"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ชื่อ", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "คำอธิบาย", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 500));
            
            _pTypeConfig = pTypeConfig;
            _pTypeConfigName = pTypeConfigName;

            _pRunIDAuto = pRunIDAuto0;
            _pIDDiffName = pIDDiffName1;
            _pStaImage = pStaImage0;
            _pFirst = pFirst;
            _pCase = pCase;
            _pPermission = pPermission;

            this.Text = "การตั้งค่า " + _pTypeConfigName;

            if (_pStaImage == "0")
            {
                switch (_pTypeConfig)
                {
                    case "28":
                        pDescForImage = "ประเภทรายงาน";
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage",
                                    pDescForImage + Environment.NewLine + "0=ข้อความ 1=ตัวเลข", 170));
                        radLabel_MaxImage.Text = pDescForImage;
                        break;
                    case "38":
                        pDescForImage = "ค่าคอม / งาน";
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage", pDescForImage, 150));
                        radLabel_MaxImage.Text = pDescForImage;
                        break;
                    case "39":
                        pDescForImage = "อัตราค่าคอม";
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage", pDescForImage, 150));
                        radLabel_MaxImage.Text = pDescForImage;
                        break;
                    case "40":
                        pDescForImage = "จำนวน Tag";
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage", pDescForImage, 150));
                        radLabel_MaxImage.Text = pDescForImage;
                        break;
                    case "55":
                        pDescForImage = "แสดงบาร์โค้ด";
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage",
                                    pDescForImage + Environment.NewLine + "0=รวมบาร์โค้ด 1=แยกบาร์โค้ด", 190));
                        radLabel_MaxImage.Text = pDescForImage;
                        break;
                    default:
                        pDescForImage = "จำนวนรูปสูงสุด";
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage", pDescForImage, 120));
                        radLabel_MaxImage.Text = pDescForImage;
                        break;
                }
            }
            else
            {
                pDescForImage = "จำนวนรูปสูงสุด";
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("MaxImage", "จำนวนรูปสูงสุด"));
            }

            #region BeforeChangeToSwitch
            //if (_pStaImage == "0")
            //{
            //    if (_pTypeConfig == "28") pDescForImage = "ประเภทรายงาน";
            //    else if (_pTypeConfig == "38") pDescForImage = "ค่าคอม/งาน";
            //    else if (_pTypeConfig == "39") pDescForImage = "อัตราค่าคอม";
            //    else if (_pTypeConfig == "40") pDescForImage = "จำนวน Tag";
            //    else if (_pTypeConfig == "55") pDescForImage = "แสดงบาร์โค้ด";
            //    else pDescForImage = "จำนวนรูปสูงสุด";
            //}
            //else pDescForImage = "จำนวนรูปสูงสุด";

            //if (_pStaImage == "0")
            //{
            //    if (_pTypeConfig == "28")
            //    {
            //        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage",
            //        pDescForImage + Environment.NewLine + "0=ข้อความ 1=ตัวเลข", 170));
            //        radLabel_MaxImage.Text = pDescForImage;
            //    }
            //    else if (_pTypeConfig == "55")
            //    {
            //        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage",
            //        pDescForImage + Environment.NewLine + "0=รวมบาร์โค้ด 1=แยกบาร์โค้ด", 190));
            //        radLabel_MaxImage.Text = pDescForImage;
            //    }
            //    else
            //    {
            //        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("MaxImage",
            //       pDescForImage, 150));
            //        radLabel_MaxImage.Text = pDescForImage;
            //    }
            //}
            //else
            //{
            //    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("MaxImage", "จำนวนรูปสูงสุด"));
            //}

            #endregion

            ClearData();
            if (_pPermission == "0") radStatusStrip1.Enabled = false;
            if (_pTypeConfig == "51")
            {
                radButtonElement_Add.Enabled = false;
                radCheckBox_Sta.Text = "ล็อครายการลงบัญชี";
            }

        }
        //ClearData
        void ClearData()
        {
            radTextBox_MaxImage.Text = "";
            if (_pStaImage == "0")
            {
                radLabel_MaxImage.Visible = true; radTextBox_MaxImage.Visible = true; radTextBox_MaxImage.Enabled = false;
            }
            else
            {
                radLabel_MaxImage.Visible = false; radTextBox_MaxImage.Visible = false;
            }

            if (_pTypeConfig == "21") radLabel_MaxImage.Text = "จำนวนรอบตรวจ";

            radTextBox_ID.Enabled = false; radTextBox_ID.Text = "";
            radTextBox_Name.Enabled = false; radTextBox_Name.Text = "";
            radTextBox_Desc.Enabled = false; radTextBox_Desc.Text = "";
            radTextBox_Rmk.Enabled = false; radTextBox_Rmk.Text = "";
            radCheckBox_Sta.Enabled = false;
            radButton_Save.Enabled = false;
            pLock = "0";
        }
        //Load Main
        private void ConfigBranch_GenaralDetail_Load(object sender, EventArgs e)
        {
            Set_DGV();
        }

        //Set DGV
        void Set_DGV()
        {
            dt = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(_pTypeConfig, "", " ORDER BY SHOW_ID,SHOW_NAME", "0,1");
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();
        }
        //Reset All
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        //SetInsert
        void SetForInsert()
        {
            StatusSave = "0";

            if (_pRunIDAuto == "0") // id Auto
            {
                radTextBox_ID.Enabled = false;
                if (_pIDDiffName == "0")//เหมือน ID=NAME
                {
                    radTextBox_Name.Enabled = false;
                    radLabel_Desc.Text = "คำอธิบาย [Enter]";
                    radTextBox_Desc.Enabled = true;
                    radTextBox_Desc.Focus();
                    radCheckBox_Sta.CheckState = CheckState.Checked; radCheckBox_Sta.Enabled = true;
                }
                else
                {
                    radLabel_Name.Text = "ชื่อ [Enter]";
                    radTextBox_Name.Enabled = true;
                    radTextBox_Name.Focus();
                }
            }
            else
            {
                radLabel_ID.Text = "รหัส [Enter]";
                radTextBox_ID.Enabled = true;
                radTextBox_ID.Focus();
            }
            if (_pStaImage == "0") radTextBox_MaxImage.Enabled = true; else radTextBox_MaxImage.Enabled = false;
        }
        //Insert
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //check ข้อมูลก่อน Insert
            if (radTextBox_Desc.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("คำอธิบาย");
                return;
            }

            int maximg;
            try
            {
                maximg = Convert.ToInt32(radTextBox_MaxImage.Text);
            }
            catch (Exception)
            {

                maximg = 0;
            }

            switch (_pTypeConfig)
            {
                case "2":
                    if ((radTextBox_MaxImage.Text != "1"))
                    {
                        if ((radTextBox_MaxImage.Text != "3"))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("การระบุจำนวนรูปต้องเป็น แค่ 1 หรือ 3 เท่านั้น");
                            return;
                        }
                    }
                    break;
                case "28":
                    if (maximg > 1)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การระบุข้อมูลประเภทรายงาน จะกำหนดเป็น {Environment.NewLine}0 คือรายงานที่เป็นข้อความ {Environment.NewLine}1 คือรายงานที่เป็นตัวเลข {Environment.NewLine}เท่านั้น ลองใหม่อีกครั้ง.");
                        return;
                    }
                    break;
                case "38":
                    if (maximg == 0)
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
                        radTextBox_MaxImage.Focus();
                        return;
                    }
                    break;
                case "39":
                    if (maximg == 0)
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
                        radTextBox_MaxImage.Focus();
                        return;
                    }
                    break;
                case "40":
                    if (maximg == 0)
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
                        radTextBox_MaxImage.Focus();
                        return;
                    }
                    break;
                case "55":
                    if (maximg > 1)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การระบุการดึงรายงาน จะกำหนดเป็น {Environment.NewLine}0 คือรายงานที่รวมบาร์โค้ด {Environment.NewLine}1 คือรายงานที่แยกบาร์โค้ด {Environment.NewLine}เท่านั้น ลองใหม่อีกครั้ง.");
                        return;
                    }
                    break;
                default:
                    if ((_pStaImage == "0") && (maximg == 0))
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
                        radTextBox_MaxImage.Focus();
                        return;
                    }
                    break;
            }

            #region BeforeChangeToSwitch
            //if (_pTypeConfig == "28")
            //{
            //    if (maximg > 1)
            //    {
            //        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การระบุข้อมูลประเภทรายงาน จะกำหนดเป็น {Environment.NewLine}0 คือรายงานที่เป็นข้อความ {Environment.NewLine}1 คือรายงานที่เป็นตัวเลข {Environment.NewLine}เท่านั้น ลองใหม่อีกครั้ง.");
            //        return;
            //    }

            //}
            //else if (_pTypeConfig == "38")
            //{
            //    if (maximg == 0)
            //    {
            //        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
            //        radTextBox_MaxImage.Focus();
            //        return;
            //    }
            //}
            //else if (_pTypeConfig == "39")
            //{
            //    if (maximg == 0)
            //    {
            //        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
            //        radTextBox_MaxImage.Focus();
            //        return;
            //    }
            //}
            //else if (_pTypeConfig == "40")
            //{
            //    if (maximg == 0)
            //    {
            //        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
            //        radTextBox_MaxImage.Focus();
            //        return;
            //    }
            //}
            //else if (_pTypeConfig == "55")
            //{
            //    if (maximg > 1)
            //    {
            //        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การระบุการดึงรายงาน จะกำหนดเป็น {Environment.NewLine}0 คือรายงานที่รวมบาร์โค้ด {Environment.NewLine}1 คือรายงานที่แยกบาร์โค้ด {Environment.NewLine}เท่านั้น ลองใหม่อีกครั้ง.");
            //        return;
            //    }
            //}
            //else
            //{
            //    if ((_pStaImage == "0") && (maximg == 0))
            //    {
            //        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert(pDescForImage);
            //        radTextBox_MaxImage.Focus();
            //        return;
            //    }
            //}

            //if (_pTypeConfig == "2")
            //{

            //    if ((radTextBox_MaxImage.Text != "1"))
            //    {
            //        if ((radTextBox_MaxImage.Text != "3"))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Warning("การระบุจำนวนรูปต้องเป็น แค่ 1 หรือ 3 เท่านั้น");
            //            return;
            //        }
            //    }
            //}
            #endregion


            string aSta = "0", UpAX = "-";
            if (radCheckBox_Sta.Checked == true)
            { aSta = "1"; UpAX = "PO_WH"; }

            string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "");
            ArrayList strAX0 = new ArrayList() { AX_SendData.Save_EXTERNALLIST("UPDATE", "VENDTABLE", "DATAAREAID|ACCOUNTNUM", $@"SPC|{radTextBox_ID.Text}", "PURCHPOOLID", $@"{UpAX}", $@"{recID}") };

            //save Data
            string T;
            switch (StatusSave)
            {
                case "0":// Insert
                    string aID;
                    if (_pRunIDAuto == "0") aID = Class.ConfigClass.GetMaxINVOICEID(_pTypeConfig, "-", _pFirst, _pCase); else aID = radTextBox_ID.Text;

                    string aName;
                    if (_pIDDiffName == "0") aName = aID; else aName = radTextBox_Name.Text;

                    ArrayList str240 = new ArrayList()
                    {
                        ConfigClass.Save_CONFIGBRANCH_GenaralDetail(aID, aName, radTextBox_Desc.Text, _pTypeConfig, _pTypeConfigName,radTextBox_Rmk.Text, aSta, maximg, pLock)
                    };

                    if (_pTypeConfig == "57") T = ConnectionClass.ExecuteMain_AX_24_SameTime(str240, strAX0); else T = ConnectionClass.ExecuteSQL_ArrayMain(str240);

                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        Set_DGV();
                        ClearData();
                    }
                    break;

                case "1": // Update
                    string bName;
                    if (_pIDDiffName == "0") bName = radTextBox_ID.Text; else bName = radTextBox_Name.Text;

                    ArrayList str241 = new ArrayList()
                    {
                        ConfigClass.Save_CONFIGBRANCH_GenaralDetail(radTextBox_ID.Text, bName, radTextBox_Desc.Text,_pTypeConfig, "", radTextBox_Rmk.Text, aSta, maximg, pLock)
                    };

                    if (_pTypeConfig == "57") T = ConnectionClass.ExecuteMain_AX_24_SameTime(str241, strAX0); else T = ConnectionClass.ExecuteSQL_ArrayMain(str241);

                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        Set_DGV();
                        ClearData();
                    }
                    break;
                default:
                    break;
            }
        }

        //SetEdit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            StatusSave = "1";
            radTextBox_ID.Text = RadGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString();
            radTextBox_Name.Text = RadGridView_Show.CurrentRow.Cells["SHOW_NAME"].Value.ToString();
            radTextBox_Desc.Text = RadGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value.ToString(); radTextBox_Desc.Enabled = true;
            radTextBox_Rmk.Text = RadGridView_Show.CurrentRow.Cells["REMARK"].Value.ToString(); radTextBox_Rmk.Enabled = true;
            if (_pTypeConfig == "28")
            {
                if (RadGridView_Show.CurrentRow.Cells["MaxImage"].Value.ToString() == "ตัวเลข") radTextBox_MaxImage.Text = "1";
                else radTextBox_MaxImage.Text = "0";

            }
            else if (_pTypeConfig == "55")
            {
                if (RadGridView_Show.CurrentRow.Cells["MaxImage"].Value.ToString() == "1") radTextBox_MaxImage.Text = "1";
                else radTextBox_MaxImage.Text = "0";
            }
            else
            {
                radTextBox_MaxImage.Text = RadGridView_Show.CurrentRow.Cells["MaxImage"].Value.ToString();
            }

            radCheckBox_Sta.Enabled = true;

            if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "1") radCheckBox_Sta.CheckState = CheckState.Checked;
            else radCheckBox_Sta.CheckState = CheckState.Unchecked;

            if (_pStaImage == "0")
            {
                radTextBox_MaxImage.Enabled = true;
                radTextBox_MaxImage.Visible = true;
            }
            else
            {
                radTextBox_MaxImage.Enabled = false;
                radTextBox_MaxImage.Visible = false;
            }
            if (_pIDDiffName == "0")
            {
                radTextBox_Name.Enabled = false;
            }
            else
            {
                radTextBox_Name.Enabled = true;
            }
            radButton_Save.Enabled = true;
            radTextBox_Desc.Focus();
            radTextBox_Desc.SelectionStart = radTextBox_Desc.Text.Length;
        }
        //ForInsert
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            SetForInsert();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //ID Enter
        private void RadTextBox_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_ID.Text == "") return;

                //check ข้อมูลก่อนว่ามีแล้วหรือยัง
                if (dt.Rows.Count > 0)
                {

                    DataRow[] dr = dt.Select(" SHOW_ID = '" + radTextBox_ID.Text + @"' ");
                    if (dr.Length > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ข้อมูลนี้มีอยู่แล้ว ไม่ต้องเพิ่มซ้ำอีก.");
                        ClearData();
                        return;
                    }
                }

                //check Type ข้าวกล่องก่อน
                if ((_pTypeConfig == "17") || (_pTypeConfig == "18") || (_pTypeConfig == "19") || (_pTypeConfig == "20") || (_pTypeConfig == "25") || (_pTypeConfig == "32") || (_pTypeConfig == "33") || (_pTypeConfig == "56"))
                {
                    Data_ITEMBARCODE _dataItemBarcode = new Data_ITEMBARCODE(radTextBox_ID.Text);
                    if (_dataItemBarcode.Itembarcode_ITEMBARCODE is null)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลบาร์โค้ดที่ระบุ");
                        ClearData();
                        return;
                    }
                    radTextBox_Name.Enabled = false; radTextBox_Name.Text = _dataItemBarcode.Itembarcode_SPC_ITEMNAME;
                    radLabel_Desc.Text = "คำอธิบาย";
                    radTextBox_Desc.Enabled = false;
                    if (_pTypeConfig == "17") { radTextBox_Desc.Text = "RETAILAREA"; }
                    else if (_pTypeConfig == "19") { radTextBox_Desc.Text = "RETAILAREA"; }
                    else if (_pTypeConfig == "20") { radTextBox_Desc.Text = "RETAILAREA"; }
                    else if (_pTypeConfig == "25") { radTextBox_Desc.Text = "RETAILAREA"; }
                    else if (_pTypeConfig == "32") { radTextBox_Desc.Text = "RETAILAREA"; }
                    else if (_pTypeConfig == "33") { radTextBox_Desc.Text = "RETAILAREA"; }
                    else if (_pTypeConfig == "56") { radTextBox_Desc.Text = _dataItemBarcode.Itembarcode_SPC_ITEMNAME; }
                    else { radTextBox_Desc.Text = "WH-ICE"; }
                    radCheckBox_Sta.CheckState = CheckState.Checked;
                    radCheckBox_Sta.Enabled = true; radTextBox_Rmk.Enabled = true;
                    radButton_Save.Enabled = true;
                    radTextBox_Rmk.Focus();
                    return;
                }

                if ((_pTypeConfig == "2") || (_pTypeConfig == "57") || (_pTypeConfig == "60") || (_pTypeConfig == "61"))
                {

                    DataTable dt = Class.Models.VendTableClass.GetVENDTABLE_All(radTextBox_ID.Text, "");
                    if (dt.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสผู้จำหน่ายที่ระบุ");
                        radTextBox_ID.SelectAll();
                        radTextBox_ID.Focus();
                        return;
                    }

                    radTextBox_ID.Text = dt.Rows[0]["ACCOUNTNUM"].ToString();
                    radTextBox_Name.Text = dt.Rows[0]["Name"].ToString();
                    pLock = dt.Rows[0]["ITEMBUYERGROUPID"].ToString();
                    radTextBox_Rmk.Text = pLock;
                }

                if ((_pTypeConfig == "38") || (_pTypeConfig == "43") || (_pTypeConfig == "45") || (_pTypeConfig == "46") || (_pTypeConfig == "52"))
                {
                    DataTable dtEmp = Class.Models.EmplClass.GetEmployee_Altnum(radTextBox_ID.Text);
                    if (dtEmp.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลพนักงาน");
                        ClearData();
                        return;
                    }

                    //check ข้อมูลก่อนว่ามีแล้วหรือยัง
                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] dr = dt.Select(" SHOW_ID = '" + dtEmp.Rows[0]["EMPLID"].ToString() + @"' ");
                        if (dr.Length > 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลนี้มีอยู่แล้ว ไม่ต้องเพิ่มซ้ำอีก.");
                            ClearData();
                            return;
                        }
                    }
                    //
                    if ((dtEmp.Rows[0]["NUM"].ToString() != "D054") && (_pTypeConfig == "38"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลพนักงานไม่ได้อยู่แผนก D054 ดูแลระบบงานมินิมาร์ทสาขา" + Environment.NewLine + " เช็คข้อมูลใหม่อีกครั้ง" +
                            Environment.NewLine + "[พนักงานอยู่แผนก " + dtEmp.Rows[0]["NUM"].ToString() + " " + dtEmp.Rows[0]["DESCRIPTION"].ToString() + @"]");
                        ClearData();
                        return;
                    }

                    //
                    if ((dtEmp.Rows[0]["NUM"].ToString() != "D162") && (_pTypeConfig == "45"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลพนักงานไม่ได้อยู่แผนก D162 ฝ่ายผลิต-โรงน้ำแข็ง" + Environment.NewLine + " เช็คข้อมูลใหม่อีกครั้ง" +
                            Environment.NewLine + "[พนักงานอยู่แผนก " + dtEmp.Rows[0]["NUM"].ToString() + " " + dtEmp.Rows[0]["DESCRIPTION"].ToString() + @"]");
                        ClearData();
                        return;
                    }


                    //
                    if ((dtEmp.Rows[0]["NUM"].ToString() != "D162") && (_pTypeConfig == "46"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลพนักงานไม่ได้อยู่แผนก D162 ฝ่ายผลิต-โรงน้ำแข็ง" + Environment.NewLine + " เช็คข้อมูลใหม่อีกครั้ง" +
                            Environment.NewLine + "[พนักงานอยู่แผนก " + dtEmp.Rows[0]["NUM"].ToString() + " " + dtEmp.Rows[0]["DESCRIPTION"].ToString() + @"]");
                        ClearData();
                        return;
                    }

                    radTextBox_ID.Text = dtEmp.Rows[0]["EMPLID"].ToString();
                    radTextBox_Name.Enabled = false; radTextBox_Name.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                    radLabel_Desc.Text = "คำอธิบาย";
                    radTextBox_Desc.Text = dtEmp.Rows[0]["DESCRIPTION"].ToString();
                    radTextBox_Desc.Enabled = false;
                    radCheckBox_Sta.CheckState = CheckState.Checked;
                    radCheckBox_Sta.Enabled = true; radTextBox_Rmk.Enabled = true;
                    radButton_Save.Enabled = true; radTextBox_ID.Enabled = false;
                    radTextBox_Rmk.Focus();
                    return;
                }

                if (_pTypeConfig == "40")
                {
                    DataTable dt40 = Models.AssetClass.FindDataVEHICLE(radTextBox_ID.Text, "");
                    if (dt40.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ทะเบียนรถที่ระบุ");
                        radTextBox_ID.SelectAll();
                        radTextBox_ID.Focus();
                        return;
                    }

                    radTextBox_ID.Text = dt40.Rows[0]["VEHICLEID"].ToString();
                    radTextBox_Name.Text = dt40.Rows[0]["BRAND"].ToString() + "-" + dt40.Rows[0]["Name"].ToString();
                }

                if ((_pTypeConfig == "47") || (_pTypeConfig == "64") || (_pTypeConfig == "65"))//ค้นหาแผนกสำหรับการสั่งสินค้าผ่านระบบ MNOR
                {
                    DataTable dtDpt = ConnectionClass.SelectSQL_Main($@" DptClass_GetDpt_All '{radTextBox_ID.Text}' ");
                    if (dtDpt.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("แผนกที่ระบุ");
                        radTextBox_ID.SelectAll();
                        radTextBox_ID.Focus();
                        return;
                    }

                    radTextBox_ID.Text = dtDpt.Rows[0]["NUM"].ToString();
                    radTextBox_Name.Text = dtDpt.Rows[0]["DESCRIPTION_SHOW"].ToString();
                    // pLock = dt40.Rows[0]["ITEMBUYERGROUPID"].ToString();
                }


                if (_pIDDiffName == "0")//เหมือน ID=NAME
                {
                    radTextBox_Name.Enabled = false; radTextBox_Name.Text = radTextBox_ID.Text;
                    radLabel_Desc.Text = "คำอธิบาย [Enter]";
                    radTextBox_Desc.Enabled = true;
                    radTextBox_Desc.Focus();
                    radCheckBox_Sta.CheckState = CheckState.Checked;
                    radCheckBox_Sta.Enabled = true;
                }
                else
                {
                    radTextBox_ID.Enabled = false;
                    radLabel_Name.Text = "ชื่อ [Enter]";
                    radTextBox_Name.Enabled = true;
                    radTextBox_Name.Focus();
                }
            }
        }
        //Name Enter
        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Name.Text == "") return;
                radLabel_Desc.Text = "คำอธิบาย [Enter]";
                radTextBox_Desc.Enabled = true;
                radTextBox_Desc.Focus();
                radCheckBox_Sta.CheckState = CheckState.Checked;
                radCheckBox_Sta.Enabled = true;
                radTextBox_Name.Enabled = false;

            }
        }

        private void RadTextBox_Desc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Desc.Text == "") return;
                radTextBox_MaxImage.Enabled = true; radTextBox_MaxImage.Focus();
                radTextBox_Desc.Enabled = false;
                radTextBox_Rmk.Enabled = true;// radTextBox_Rmk.Focus();
                radButton_Save.Enabled = true; radButton_Save.Focus();
            }
        }
        //Number Only
        private void RadTextBox_MaxImage_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeConfigName);
        }

        private void RadTextBox_Name_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_pTypeConfig == "51") e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar); //เฉพาะล็อครายการลงบัญชี
        }
    }
}
