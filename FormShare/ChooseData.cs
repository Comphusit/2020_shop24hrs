﻿//CheckOK
using System;
using System.Windows.Forms;

namespace PC_Shop24Hrs.FormShare
{
    public partial class ChooseData : Telerik.WinControls.UI.RadForm
    {
        readonly string _pCase1;
        readonly string _pCase2;
        readonly string _pCase3;

        public string sSendData;

        public ChooseData(string pCase1, string pCase2, string pCase3 = "")
        {
            InitializeComponent();
            _pCase1 = pCase1;
            _pCase2 = pCase2;
            _pCase3 = pCase3;
        }
        //load
        private void ChooseData_Load(object sender, EventArgs e)
        {
            radButton_case1.ButtonElement.ShowBorder = true;
            radButton_case2.ButtonElement.ShowBorder = true;

            radButton_case1.Text = _pCase1;
            radButton_case2.Text = _pCase2;
            radLabel_text.Text = _pCase3;
        }

        private void RadButton_case1_Click(object sender, EventArgs e)
        {
            sSendData = "1";
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void RadButton_case2_Click(object sender, EventArgs e)
        {
            sSendData = "0";
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

    }
}
