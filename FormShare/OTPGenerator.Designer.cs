﻿namespace PC_Shop24Hrs.FormShare
{
    partial class OTPGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OTPGenerator));
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Show = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_OTP = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_PIN = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Clear = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_OTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Input.Location = new System.Drawing.Point(24, 107);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(72, 19);
            this.radLabel_Input.TabIndex = 24;
            this.radLabel_Input.Text = "รหัส OTP :";
            // 
            // radLabel_Show
            // 
            this.radLabel_Show.AutoSize = false;
            this.radLabel_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Show.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Show.Location = new System.Drawing.Point(24, 36);
            this.radLabel_Show.Name = "radLabel_Show";
            this.radLabel_Show.Size = new System.Drawing.Size(85, 53);
            this.radLabel_Show.TabIndex = 25;
            this.radLabel_Show.Text = "รหัส PIN :";
            // 
            // radTextBox_OTP
            // 
            this.radTextBox_OTP.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_OTP.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_OTP.Location = new System.Drawing.Point(115, 102);
            this.radTextBox_OTP.Multiline = true;
            this.radTextBox_OTP.Name = "radTextBox_OTP";
            // 
            // 
            // 
            this.radTextBox_OTP.RootElement.StretchVertically = true;
            this.radTextBox_OTP.Size = new System.Drawing.Size(121, 34);
            this.radTextBox_OTP.TabIndex = 1;
            this.radTextBox_OTP.Tag = "";
            // 
            // radTextBox_PIN
            // 
            this.radTextBox_PIN.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_PIN.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_PIN.Location = new System.Drawing.Point(115, 48);
            this.radTextBox_PIN.Multiline = true;
            this.radTextBox_PIN.Name = "radTextBox_PIN";
            // 
            // 
            // 
            this.radTextBox_PIN.RootElement.StretchVertically = true;
            this.radTextBox_PIN.Size = new System.Drawing.Size(121, 34);
            this.radTextBox_PIN.TabIndex = 0;
            this.radTextBox_PIN.Tag = "";
            this.radTextBox_PIN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_PIN_KeyDown);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Blue;
            this.radLabel1.Location = new System.Drawing.Point(115, 19);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(133, 23);
            this.radLabel1.TabIndex = 28;
            this.radLabel1.Text = "ระบุ PIN [Enter]";
            // 
            // radButton_Clear
            // 
            this.radButton_Clear.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Clear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Clear.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_Clear.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Clear.Location = new System.Drawing.Point(242, 48);
            this.radButton_Clear.Name = "radButton_Clear";
            // 
            // 
            // 
            this.radButton_Clear.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.radButton_Clear.Size = new System.Drawing.Size(30, 34);
            this.radButton_Clear.TabIndex = 49;
            this.radButton_Clear.Click += new System.EventHandler(this.RadButton_Clear_Click);
            // 
            // OTPGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 167);
            this.Controls.Add(this.radButton_Clear);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_PIN);
            this.Controls.Add(this.radTextBox_OTP);
            this.Controls.Add(this.radLabel_Show);
            this.Controls.Add(this.radLabel_Input);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "OTPGenerator";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OTP Generator";
            this.Load += new System.EventHandler(this.OTPGenerator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_OTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Clear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadLabel radLabel_Show;
        private Telerik.WinControls.UI.RadTextBox radTextBox_OTP;
        private Telerik.WinControls.UI.RadTextBox radTextBox_PIN;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton radButton_Clear;
    }
}
