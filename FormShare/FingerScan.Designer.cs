﻿namespace PC_Shop24Hrs.FormShare
{
    partial class FingerScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FingerScan));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox_EmplImg = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radLabel_Page = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Possion = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmplNAME = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmplID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Show = new Telerik.WinControls.UI.RadLabel();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_EmplImg)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Page)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplNAME)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 350);
            this.panel1.TabIndex = 33;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(547, 350);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(300, 123);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(244, 224);
            this.panel5.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pictureBox_EmplImg);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 123);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(291, 224);
            this.panel4.TabIndex = 2;
            // 
            // pictureBox_EmplImg
            // 
            this.pictureBox_EmplImg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_EmplImg.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_EmplImg.Name = "pictureBox_EmplImg";
            this.pictureBox_EmplImg.Size = new System.Drawing.Size(291, 224);
            this.pictureBox_EmplImg.TabIndex = 0;
            this.pictureBox_EmplImg.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radLabel_Page);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(300, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(244, 114);
            this.panel3.TabIndex = 1;
            // 
            // radLabel_Page
            // 
            this.radLabel_Page.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Page.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Page.ForeColor = System.Drawing.Color.Red;
            this.radLabel_Page.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Page.Name = "radLabel_Page";
            this.radLabel_Page.Size = new System.Drawing.Size(75, 30);
            this.radLabel_Page.TabIndex = 31;
            this.radLabel_Page.Text = "สถานะ";
            this.radLabel_Page.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radLabel_Dept);
            this.panel2.Controls.Add(this.radLabel5);
            this.panel2.Controls.Add(this.radLabel_Possion);
            this.panel2.Controls.Add(this.radLabel3);
            this.panel2.Controls.Add(this.radLabel_EmplNAME);
            this.panel2.Controls.Add(this.radLabel_EmplID);
            this.panel2.Controls.Add(this.radLabel1);
            this.panel2.Controls.Add(this.radLabel_Show);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(291, 114);
            this.panel2.TabIndex = 0;
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dept.Location = new System.Drawing.Point(66, 87);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(43, 19);
            this.radLabel_Dept.TabIndex = 33;
            this.radLabel_Dept.Text = "แผนก";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(6, 87);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(43, 19);
            this.radLabel5.TabIndex = 32;
            this.radLabel5.Text = "แผนก";
            // 
            // radLabel_Possion
            // 
            this.radLabel_Possion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Possion.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Possion.Location = new System.Drawing.Point(66, 60);
            this.radLabel_Possion.Name = "radLabel_Possion";
            this.radLabel_Possion.Size = new System.Drawing.Size(57, 19);
            this.radLabel_Possion.TabIndex = 31;
            this.radLabel_Possion.Text = "ตำแหน่ง";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(6, 60);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(57, 19);
            this.radLabel3.TabIndex = 30;
            this.radLabel3.Text = "ตำแหน่ง";
            // 
            // radLabel_EmplNAME
            // 
            this.radLabel_EmplNAME.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplNAME.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplNAME.Location = new System.Drawing.Point(66, 33);
            this.radLabel_EmplNAME.Name = "radLabel_EmplNAME";
            this.radLabel_EmplNAME.Size = new System.Drawing.Size(25, 19);
            this.radLabel_EmplNAME.TabIndex = 29;
            this.radLabel_EmplNAME.Text = "ชื่อ";
            // 
            // radLabel_EmplID
            // 
            this.radLabel_EmplID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplID.Location = new System.Drawing.Point(66, 6);
            this.radLabel_EmplID.Name = "radLabel_EmplID";
            this.radLabel_EmplID.Size = new System.Drawing.Size(32, 19);
            this.radLabel_EmplID.TabIndex = 28;
            this.radLabel_EmplID.Text = "รหัส";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(6, 33);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(25, 19);
            this.radLabel1.TabIndex = 27;
            this.radLabel1.Text = "ชื่อ";
            // 
            // radLabel_Show
            // 
            this.radLabel_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Show.Location = new System.Drawing.Point(6, 6);
            this.radLabel_Show.Name = "radLabel_Show";
            this.radLabel_Show.Size = new System.Drawing.Size(32, 19);
            this.radLabel_Show.TabIndex = 26;
            this.radLabel_Show.Text = "รหัส";
            // 
            // FingerScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.ClientSize = new System.Drawing.Size(547, 350);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FingerScan";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "สแกนนิ้ว";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FingerScan_FormClosing);
            this.Load += new System.EventHandler(this.FingerScan_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_EmplImg)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Page)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplNAME)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox_EmplImg;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplNAME;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Show;
        private Telerik.WinControls.UI.RadLabel radLabel_Page;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel_Possion;
        private Telerik.WinControls.UI.RadLabel radLabel3;
    }
}
