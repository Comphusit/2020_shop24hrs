﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.FormShare
{
    public partial class ConfigBranch_Detail : Telerik.WinControls.UI.RadForm
    {

        readonly DataTable dt_DataAll = new DataTable();

        readonly string _pTypeConfig;
        int CoinsBeforeChange;
        public ConfigBranch_Detail(string pTypeConfig)
        {
            InitializeComponent();
            _pTypeConfig = pTypeConfig;
        }

        //Load Main
        private void ConfigBranch_Detail_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            DataTable dtBch;//= new DataTable();

            int w_column = 80;
            switch (_pTypeConfig)
            {
                case "26":
                    dtBch = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("27", "", "ORDER BY SHOW_ID", "1");
                    break;
                case "38":
                    dtBch = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("37", "", "ORDER BY SHOW_ID", "1");
                    break;
                case "56":
                    dtBch = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("55", "", "ORDER BY SHOW_ID", "1");
                    w_column = 200;
                    break;
                case "67":
                    dtBch = Models.DptClass.GetDpt_Purchase();
                    break;
                default:
                    dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                    break;
            }

            //if (_pTypeConfig == "26") dtBch = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("27", "", "ORDER BY SHOW_ID", "1");
            //else if (_pTypeConfig == "38") dtBch = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("37", "", "ORDER BY SHOW_ID", "1");
            //else dtBch = BranchClass.GetBranchAll("'1'", "'1'");

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 110)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ชื่อ", 250)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "คำอธิบาย", 200)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("REMARK", "หมายเหตุ")));
            RadGridView_Show.MasterTemplate.Columns["SHOW_ID"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns["SHOW_NAME"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns["SHOW_DESC"].IsPinned = true;

            dt_DataAll.Columns.Add("SHOW_ID");
            dt_DataAll.Columns.Add("SHOW_NAME");
            dt_DataAll.Columns.Add("SHOW_DESC");
            dt_DataAll.Columns.Add("REMARK");

            if (_pTypeConfig == "59")
            {
                RadGridView_Show.ReadOnly = false;
                RadGridView_Show.MasterTemplate.Columns["SHOW_ID"].ReadOnly = true;
                RadGridView_Show.MasterTemplate.Columns["SHOW_NAME"].ReadOnly = true;
                RadGridView_Show.MasterTemplate.Columns["SHOW_DESC"].ReadOnly = true;

                foreach (DataRow row in dtBch.Rows)
                {
                    string headText = row["BRANCH_ID"].ToString() + System.Environment.NewLine + row["BRANCH_NAME"].ToString();
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(row["BRANCH_ID"].ToString(), headText, w_column)));
                    dt_DataAll.Columns.Add(row["BRANCH_ID"].ToString());
                    DatagridClass.SetCellBackClolorByExpression(row["BRANCH_ID"].ToString(), $@" {row["BRANCH_ID"]} = '0.00'", ConfigClass.SetColor_Red(), RadGridView_Show);
                }

                DataTable dtVendor = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(_pTypeConfig, "", "ORDER BY SHOW_ID", "1");
                foreach (DataRow row1 in dtVendor.Rows)
                {
                    dt_DataAll.Rows.Add(row1["SHOW_ID"].ToString(), row1["SHOW_NAME"].ToString(), row1["SHOW_DESC"].ToString(), row1["REMARK"].ToString());
                }

                DataTable dtBranchVendor = ConfigClass.FindData_CONFIGBRANCH_DETAIL(_pTypeConfig); //GetAllBranch();
                for (int iR = 0; iR < dt_DataAll.Rows.Count; iR++)
                {
                    for (int iC = 4; iC < dt_DataAll.Columns.Count; iC++)
                    {
                        DataRow[] dr = dtBranchVendor.Select(" SHOW_ID = '" + dt_DataAll.Rows[iR]["SHOW_ID"] + "'" +
                            " AND  BRANCH_ID = '" + dt_DataAll.Columns[iC] + @"' ");

                        dt_DataAll.Rows[iR][dt_DataAll.Columns[iC]] = "0.00";
                        if (dr.Length > 0)
                        {
                            double cCouins = double.Parse(dr[0]["REMARK"].ToString());
                            dt_DataAll.Rows[iR][dt_DataAll.Columns[iC]] = cCouins.ToString("N2");
                        }
                    }
                }

                RadGridView_Show.MasterTemplate.AllowAddNewRow = false;
            }
            else if (_pTypeConfig == "67")
            {
                RadGridView_Show.Columns["SHOW_DESC"].IsVisible = false;
                RadGridView_Show.Columns["REMARK"].IsVisible = false;
                //Add Column
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("FIX", "บังคับ", 80)));
                dt_DataAll.Columns.Add("FIX");

                //Set column
                foreach (DataRow row1 in dtBch.Rows)
                {
                    dt_DataAll.Rows.Add(row1["SHOW_ID"].ToString(), row1["SHOW_NAME"].ToString(), "", "");
                }

                DataTable dtBranchVendor = ConfigClass.FindData_CONFIGBRANCH_DETAIL(_pTypeConfig); //GetAllBranch();
                for (int iR = 0; iR < dt_DataAll.Rows.Count; iR++)
                {
                    for (int iC = 3; iC < dt_DataAll.Columns.Count; iC++)
                    {
                        DataRow[] dr = dtBranchVendor.Select(" SHOW_ID = '" + dt_DataAll.Rows[iR]["SHOW_ID"] + "'" +
                            " AND  BRANCH_ID = '" + dt_DataAll.Columns[iC] + @"' ");
                        dt_DataAll.Rows[iR][dt_DataAll.Columns[iC]] = "0";
                        if (dr.Length > 0)
                        { dt_DataAll.Rows[iR][dt_DataAll.Columns[iC]] = "1"; }
                    }
                }
            }
            else
            {
                foreach (DataRow row in dtBch.Rows)
                {
                    string headText = row["BRANCH_ID"].ToString() + System.Environment.NewLine + row["BRANCH_NAME"].ToString();
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual(row["BRANCH_ID"].ToString(), headText, w_column)));
                    dt_DataAll.Columns.Add(row["BRANCH_ID"].ToString());
                }

                DataTable dtVendor = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(_pTypeConfig, "", "ORDER BY SHOW_ID", "1");
                foreach (DataRow row1 in dtVendor.Rows)
                {
                    dt_DataAll.Rows.Add(row1["SHOW_ID"].ToString(), row1["SHOW_NAME"].ToString(), row1["SHOW_DESC"].ToString());
                }

                DataTable dtBranchVendor = ConfigClass.FindData_CONFIGBRANCH_DETAIL(_pTypeConfig); //GetAllBranch();
                for (int iR = 0; iR < dt_DataAll.Rows.Count; iR++)
                {
                    for (int iC = 3; iC < dt_DataAll.Columns.Count; iC++)
                    {
                        DataRow[] dr = dtBranchVendor.Select(" SHOW_ID = '" + dt_DataAll.Rows[iR]["SHOW_ID"] + "'" +
                            " AND  BRANCH_ID = '" + dt_DataAll.Columns[iC] + @"' ");
                        dt_DataAll.Rows[iR][dt_DataAll.Columns[iC]] = "0";
                        if (dr.Length > 0)
                        { dt_DataAll.Rows[iR][dt_DataAll.Columns[iC]] = "1"; }

                    }
                }
            }

            RadGridView_Show.DataSource = dt_DataAll;
            dt_DataAll.AcceptChanges();

            this.Cursor = Cursors.Default;
        }


        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (_pTypeConfig == "59") return;

            if (RadGridView_Show.ColumnCount > 0 && RadGridView_Show.RowCount > 0)
            {
                if (RadGridView_Show.CurrentColumn.Index < 3) return;
                string sqlIn;
                if ((RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString() == "0"))
                {
                    sqlIn = ConfigClass.Save_CONFIGBRANCH_DETAIL(RadGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString(),
                                   RadGridView_Show.CurrentRow.Cells["SHOW_NAME"].Value.ToString(),
                                   RadGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value.ToString(),
                                   _pTypeConfig, RadGridView_Show.Columns[e.ColumnIndex].Name);
                    RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "1";
                }
                else
                {
                    sqlIn = ConfigClass.Save_CONFIGBRANCH_DETAIL(RadGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString(),
                                  "", "", _pTypeConfig, RadGridView_Show.Columns[e.ColumnIndex].Name);
                    RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "0";
                }
                string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
                if (T != "") MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }

        }
        //set font
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //pdt
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeConfig);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (_pTypeConfig != "59") return;
            CoinsBeforeChange = Convert.ToInt32(double.Parse(RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString()));

        }

        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (_pTypeConfig != "59") return;

            if (RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString() == "")
            {
                RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = "0.00"; return;
            } //ค่าที่ใส่ไม่เท่ากับ ""

            bool isNumerical = double.TryParse(RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString(), out _); //out double number
            if (isNumerical == false)
            {
                RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = "0.00"; return;
            } //ค่าที่ใส่ไม่เท่ากับตัวเลข 

            string QtyInput = RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString();
            int QTYIn = Convert.ToInt32(double.Parse(RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString()));
            string Branch = RadGridView_Show.Columns[RadGridView_Show.CurrentCell.ColumnIndex].Name.ToString();

            if (double.TryParse(QtyInput, out _)) RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = QTYIn.ToString("N2");

            //int index = RadGridView_Show.CurrentCell.RowIndex;
            string rowValue = RadGridView_Show.CurrentRow.Cells[0].Value.ToString();

            DataTable dtData = ConfigClass.FindData_CONFIGBRANCH_DETAIL(_pTypeConfig, $@" AND SHOW_ID = '{rowValue}' AND BRANCH_ID = '{Branch}' ");
            //DataTable dtData = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("59", $@" AND SHOW_ID = '{rowValue}' AND BRANCH_ID = '{Branch}' ", "", "1");

            string sqlIn;
            if (dtData.Rows.Count == 0) //insert
            {
                sqlIn = ConfigClass.Save_CONFIGBRANCH_DETAIL(RadGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString(),
                                   RadGridView_Show.CurrentRow.Cells["REMARK"].Value.ToString(),
                                   RadGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value.ToString(),
                                   _pTypeConfig, RadGridView_Show.Columns[e.ColumnIndex].Name,
                                   QTYIn.ToString());
                RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "1";
            }
            else
            {
                sqlIn = ConfigClass.Update_CONFIGBRANCH_DETAIL(RadGridView_Show.Columns[e.ColumnIndex].Name,
                                    RadGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString(),
                                    QTYIn.ToString());
            }
            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            if (T != "") RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = CoinsBeforeChange.ToString("N2");
            else RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = QTYIn.ToString("N2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

        }
    }
}
