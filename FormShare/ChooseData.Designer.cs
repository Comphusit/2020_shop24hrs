﻿namespace PC_Shop24Hrs.FormShare
{
    partial class ChooseData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radButton_case1 = new Telerik.WinControls.UI.RadButton();
            this.radButton_case2 = new Telerik.WinControls.UI.RadButton();
            this.radLabel_text = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_case1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_case2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_text)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_case1
            // 
            this.radButton_case1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_case1.Location = new System.Drawing.Point(24, 77);
            this.radButton_case1.Name = "radButton_case1";
            this.radButton_case1.Size = new System.Drawing.Size(218, 72);
            this.radButton_case1.TabIndex = 1;
            this.radButton_case1.Text = "บันทึก";
            this.radButton_case1.ThemeName = "Fluent";
            this.radButton_case1.Click += new System.EventHandler(this.RadButton_case1_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_case1.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_case1.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_case1.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case1.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case1.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case1.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case1.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_case2
            // 
            this.radButton_case2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_case2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_case2.Location = new System.Drawing.Point(248, 77);
            this.radButton_case2.Name = "radButton_case2";
            this.radButton_case2.Size = new System.Drawing.Size(218, 72);
            this.radButton_case2.TabIndex = 2;
            this.radButton_case2.Text = "ยกเลิก";
            this.radButton_case2.ThemeName = "Fluent";
            this.radButton_case2.Click += new System.EventHandler(this.RadButton_case2_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_case2.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_case2.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case2.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case2.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case2.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_case2.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_text
            // 
            this.radLabel_text.AutoSize = false;
            this.radLabel_text.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_text.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_text.Location = new System.Drawing.Point(24, 31);
            this.radLabel_text.Name = "radLabel_text";
            this.radLabel_text.Size = new System.Drawing.Size(442, 23);
            this.radLabel_text.TabIndex = 67;
            this.radLabel_text.Text = ":";
            // 
            // ChooseData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_case2;
            this.ClientSize = new System.Drawing.Size(499, 194);
            this.Controls.Add(this.radLabel_text);
            this.Controls.Add(this.radButton_case1);
            this.Controls.Add(this.radButton_case2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChooseData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.Load += new System.EventHandler(this.ChooseData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_case1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_case2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_text)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_case1;
        protected Telerik.WinControls.UI.RadButton radButton_case2;
        private Telerik.WinControls.UI.RadLabel radLabel_text;
    }
}
