﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;



namespace PC_Shop24Hrs.FormShare
{
    public partial class ShowRemark : Telerik.WinControls.UI.RadForm
    {
        public string pRmk;
        public string pDesc;

        readonly string pType; //0 ไม่ยังคับให้ใส่หมายเหตุ 1 บังคับใส่หมายเหตุ
        public ShowRemark(string typeSend)
        {
            InitializeComponent();
            pType = typeSend;
        }
        //load
        private void ShowRemark_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true; radButton_Cancel.ButtonElement.ShowBorder = true;

            if (pDesc == "") radLabel_Desc.Text = "หมายเหตุ"; else { radLabel_Desc.Text = pDesc; radLabel_Desc.ForeColor = ConfigClass.SetColor_Blue(); }
            if (pRmk == "") radTextBox_Remark.Text = ""; else radTextBox_Remark.Text = pRmk;

            //radTextBox_Remark.Text = "";
            radTextBox_Remark.Focus();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (pType == "1")
            {
                if (radTextBox_Remark.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("หมายเหตุ");
                    return;
                }
            }

            pRmk = radTextBox_Remark.Text;
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
