﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Drawing;
using System.Drawing.Printing;
using PC_Shop24Hrs.GeneralForm.Logistic;

namespace PC_Shop24Hrs.FormShare
{
    public partial class InputDataGeneral_FromEmplID : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();

        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly string _pTypeInput;
        // 0 บันทึกข้อมูล การนับเงินในลิ้นชักแคชเชียร์.
        // 1 บันทึกข้อมูล การรับซองเงิน [บัญชีลูกหนี้]
        // 2 บันทึกข้อมูล สลิปโอนยอดบัตรสวัสดิการ [EMV].
        // 3 บันทึก ลงคิวพนักงานขับรถ/เก็บขยะ/สถานะรถรอขึ้นของ
        // 4 บันทึก ซองแลกเหรียญมินิมาร์ท
        // 5 บันทึก ซองค่าปรับขโมย
        // 6 บันทึก เงินทอนของแคชเชียร์

        string mnsm;
        string HeadBill;
        string pSave;//0=Notsave 1=save
        string strNum;
        string pStaCase3; // 3 เกบขยะ 4 รถรอขึ้นของ 7 ลงคิวพนักงานขับ

        //string pTypeInput
        public InputDataGeneral_FromEmplID(string pTypeInput)
        {
            InitializeComponent();
            _pTypeInput = pTypeInput;
        }

        //Load
        private void InputDataGeneral_FromEmplID_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            ClearTxt();
            radDateTimePicker_D1.Visible = false;

            switch (_pTypeInput)
            {
                case "0"://นับเงินในลิ้นชัก
                    this.Text = "บันทึกข้อมูล การนับเงินในลิ้นชักแคชเชียร์";
                    radLabel_Head.Text = "บันทึกนับเงินในลิ้นชักแคชเชียร์ [ไม่รวมเงินทอน]";
                    radLabel_ForSave.Text = "จำนวนเงินที่นับได้ [Enter]";
                    radLabel_Unit.Text = "บาท";
                    radLabel_1.Text = "เครื่องขาย";
                    radLabel_2.Text = "เปิดขาย";
                    break;
                case "1"://ส่งซองเงิน
                    this.Text = "บันทึกข้อมูล การรับซองเงิน [บัญชีลูกหนี้]";
                    radLabel_Head.Text = "รับซองเงิน [บัญชีลูกหนี้]";
                    radLabel_ForSave.Text = "จำนวนเงินที่รับต่อซอง [Enter]";
                    radLabel_Unit.Text = "บาท";
                    radLabel_1.Text = "ตำแหน่ง";
                    radLabel_2.Text = "แผนก";
                    break;
                case "2"://ซอง EMV
                    SaveDataMNSM("0");
                    radLabel_1Desc.Visible = false; radLabel_2Desc.Visible = false;
                    this.Text = "บันทึกข้อมูล สลิปโอนยอดบัตรสวัสดิการ [EMV]";
                    radLabel2.Text = "ระบุวันที่ส่งยอดบัตรสวัสดิการ [EMV]";
                    radLabel_Head.Text = "สลิปโอนยอดบัตรสวัสดิการ [EMV]";
                    radLabel_ForSave.Text = "จำนวนเงิน [Enter]";
                    HeadBill = "สลิปโอนยอดบัตรสวัสดิการEMV";
                    break;
                case "3"://ลงคิวพนักงานขับรถ/เก็บขยะ/สถานะรถรอขึ้นของ
                    this.Text = "บันทึกข้อมูล ลงคิวพนักงานขับรถ/เด็กท้าย/เก็บขยะ";///สถานะรถรอขึ้นของ
                    radLabel_Head.Text = "ลงคิวพนักงานขับรถ/เด็กท้าย/เก็บขยะ";//สถานะรถรอขึ้นของ
                    radLabel2.Text = "ระบุ รหัสพนักงาน/สาขา [Enter]";///ทะเบียนรถ
                    radLabel_ForSave.Visible = false;
                    radLabel_Unit.Visible = false;
                    radTextBox_InputData.Visible = false;
                    radLabel3.Visible = false;
                    radTextBox_Remaek.Visible = false;
                    radLabel_1.Text = "";
                    radLabel_2.Text = "";
                    break;
                case "4":
                    SaveDataMNSM("0");
                    this.Text = "บันทึกข้อมูล ซองแลกเหรียญมินิมาร์ท";
                    radLabel2.Text = "ระบุวันที่ส่งซองแลกเหรียญมินิมาร์ท";
                    radLabel_Head.Text = "ซองแลกเหรียญมินิมาร์ท";
                    radLabel_ForSave.Text = "จำนวนเงิน [Enter]";
                    HeadBill = "สลิป ซองแลกเหรียญมินิมาร์ท";
                    break;
                case "5":
                    SaveDataMNSM("0");
                    this.Text = "บันทึกข้อมูล ซองค่าปรับขโมย";
                    radLabel2.Text = "ระบุวันที่ส่งซองค่าปรับขโมย";
                    radLabel_Head.Text = "ซองค่าปรับขโมย";
                    radLabel_ForSave.Text = "จำนวนเงิน [Enter]";
                    HeadBill = "สลิป ซองค่าปรับขโมย";
                    break;
                case "6":
                    SaveDataMNSM("0");
                    this.Text = "บันทึกข้อมูล เงินทอนของแคชเชียร์";
                    radLabel2.Text = "ระบุวันที่ส่งเงินทอนของแคชเชียร์";
                    radLabel_Head.Text = "เงินทอนของแคชเชียร์";
                    radLabel_ForSave.Text = "จำนวนเงิน [Enter]";
                    HeadBill = "สลิป เงินทอนของแคชเชียร์";
                    break;
                default:
                    break;
            }

            radTextBox_EmplID.Focus();
        }
        //ClearData
        void ClearTxt()
        {
            if (_pTypeInput == "3")
            {
                radLabel_1.Text = "";
                radLabel_2.Text = "";
            }

            pStaCase3 = "0";
            mnsm = ""; pSave = "0"; strNum = "";
            radLabel_Name.Text = "";
            radLabel_1Desc.Text = "";
            radLabel_2Desc.Text = "";
            radTextBox_InputData.Text = "";
            radLabel_1Desc.Text = "";
            radLabel_2Desc.Text = "";
            radTextBox_Remaek.Text = "";
            radTextBox_EmplID.Text = "";
            radButton_Save.Enabled = false;
            radTextBox_InputData.Enabled = false;
            radTextBox_Remaek.Enabled = false;
            radTextBox_EmplID.Enabled = true;
            radDateTimePicker_D1.Value = DateTime.Now;
            radTextBox_EmplID.Focus();
        }

        #region "CheckNumOnly"

        //private void RadTextBox_Tag_TextChanging(object sender, TextChangingEventArgs e)
        //{
        //    e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        //}

        private void RadTextBox_LO2_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        //EmplID Enter
        private void RadTextBox_EmplID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;

        }
        //data Enter
        private void RadTextBox_InputData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }

        #endregion

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            switch (_pTypeInput)
            {
                case "0":
                    if (radTextBox_EmplID.Text == "")
                    { return; }

                    DataTable dtAmount = PosSaleClass.Get_POSPAYMSUM(radTextBox_EmplID.Text);
                    double LINEAMOUNT = 0;
                    if (dtAmount.Rows.Count > 0)
                    {
                        LINEAMOUNT = double.Parse(dtAmount.Rows[0]["LINEAMOUNT"].ToString());
                    }

                    string[] A = radLabel_1Desc.Text.Split('-');
                    string[] B = radLabel_2Desc.Text.Split('-');
                    string POSNUMBER = A[0];
                    string LOCATIONID = A[1];
                    string POSGROUP = B[0];
                    string ZONEID = B[1];

                    double countAmount = Double.Parse(radTextBox_InputData.Text);
                    string sqlIn0 = $@"
                        INSERT INTO SHOP_COUNTMONEY (
                                    Transdate,EMPLID,NAME,POSGROUP,ZONEID,POSNUMBER,LOCATIONID,LINEAMOUNT,CountAMOUNT,WhoIns,WhoNameIns,Remark)  values  (
                                    convert(varchar,getdate(),23),'" + radTextBox_EmplID.Text + @"','" + radLabel_Name.Text + @"',
                                    '" + POSGROUP + @"','" + ZONEID + @"','" + POSNUMBER + @"','" + LOCATIONID + @"',
                                    '" + LINEAMOUNT + @"','" + countAmount + @"',
                                    '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + radTextBox_Remaek.Text + @"')";

                    string T0 = ConnectionClass.ExecuteSQL_Main(sqlIn0);
                    if (T0 == "") ClearTxt();
                    MsgBoxClass.MsgBoxShow_SaveStatus(T0);
                    break;
                case "1":
                    if (radTextBox_EmplID.Text == "") return;
                    if (pSave == "0")
                    {
                        string[] B1 = radLabel_2Desc.Text.Split('-');
                        double countAmountRecive = Double.Parse(radTextBox_InputData.Text);
                        mnsm = Class.ConfigClass.GetMaxINVOICEID("MNSM", "-", "MNSM", "1");

                        string T1 = GeneralForm.MNPM.AR_Class.Save_MNSM(mnsm, radTextBox_EmplID.Text, radLabel_Name.Text, B1[0], B1[1], 
                            countAmountRecive, radTextBox_Remaek.Text, "รับซองเงิน [บัญชีลูกหนี้]", _pTypeInput); //ConnectionClass.ExecuteSQL_Main(sqlIn1);
                        if (T1 == "")
                        {
                            pSave = "1";
                            radTextBox_Remaek.Enabled = false;
                            PrintData();
                        }
                        MsgBoxClass.MsgBoxShow_SaveStatus(T1);
                    }
                    else
                    {
                        PrintData();
                    }

                    break;
                case "2"://EMV
                    SaveDataMNSM("1");
                    break;
                case "3"://บันทึกข้อมูลต่างของจัดส่ง พนักงานขับรถ [จัดส่ง]
                    if (radTextBox_EmplID.Text == "") return;
                    if (pSave == "0")
                    {
                        switch (pStaCase3)//pStaCase3; // 3 เกบขยะ 4 รถรอขึ้นของ 7 ลงคิวพนักงานขับ
                        {
                            case "3"://เกบขยะ

                                MsgBoxClass.MsgBoxShow_SaveStatus(Logistic_Class.Insert_Garbage(radLabel_1Desc.Text, radLabel_2Desc.Text));
                                ClearTxt();
                                break;
                            case "4"://รถรอขึ้นของ
                                MsgBoxClass.MsgBoxShowButtonOk_Error("ฟังก์ชั่นนี้ ยังไม่เปิดให้ใช้งาน");
                                break;
                            case "7"://ลงคิวพนักงานขับ
                                string staTYPEQ = "1";
                                if (radLabel_2Desc.Text.Contains("ขับรถ") == true) staTYPEQ = "0";
                                MsgBoxClass.MsgBoxShow_SaveStatus(Logistic_Class.Insert_DRIVERQUEUE(radTextBox_EmplID.Text, radLabel_Name.Text, strNum,
                                    radLabel_1Desc.Text, radLabel_2Desc.Text, staTYPEQ));
                                ClearTxt();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case "4":
                    SaveDataMNSM("1"); break;
                case "5":
                    SaveDataMNSM("1"); break;
                case "6":
                    SaveDataMNSM("1"); break;
                default:
                    break;
            }
        }
        //Save EMV
        void SaveDataMNSM(string pType)
        {
            if (pType == "0")
            {
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
                radDateTimePicker_D1.Visible = true;
                radDateTimePicker_D1.Value = DateTime.Now;
                radDateTimePicker_D1.Size = new Size(261, 25);
                radLabel_Name.Visible = false; radTextBox_EmplID.Visible = false;
                radLabel_1.Visible = false; radLabel_2.Visible = false;
                radLabel2.Visible = false; radDateTimePicker_D1.Visible = false;
                radLabel_1Desc.Visible = false; radLabel_2Desc.Visible = false;
                radLabel_Unit.Text = "บาท";
                radTextBox_InputData.Enabled = true;
                radTextBox_InputData.Focus();
            }
            else
            {
                if (pSave == "0")
                {
                    double countAmountRecive = Double.Parse(radTextBox_InputData.Text);
                    mnsm = ConfigClass.GetMaxINVOICEID("MNSM", "-", "MNSM", "1");

                    string T1 = GeneralForm.MNPM.AR_Class.Save_MNSM(mnsm, SystemClass.SystemUserID, SystemClass.SystemUserName,
                                    SystemClass.SystemBranchID, SystemClass.SystemBranchName, countAmountRecive, radTextBox_Remaek.Text, HeadBill, _pTypeInput);
                    if (T1 == "")
                    {
                        pSave = "1";
                        radTextBox_Remaek.Enabled = false;
                        PrintData();
                    }
                    MsgBoxClass.MsgBoxShow_SaveStatus(T1);
                }
                else
                {
                    PrintData();
                }
            }
        }
        //print
        void PrintData()
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                PrintDocument_LO.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument_LO.PrinterSettings = printDialog1.PrinterSettings;
                PrintDocument_LO.Print();
            }

        }
        //Print Bill
        void Print_ฺMNSV(System.Drawing.Printing.PrintPageEventArgs e, string pCase) //0 บิลปกติ 1 ซองเงินที่ไม่เข้าลูกหนี้
        {
            if (pCase == "0")
            {
                int Y = 0;
                barcode.Data = mnsm;
                Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
                e.Graphics.DrawString(HeadBill, SystemClass.printFont15, Brushes.Black, 0, Y);
                Y += 25;
                e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
                Y += 67;
                e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
                Y += 20;
                e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("ยอดเงิน [บาท] " + radTextBox_InputData.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("หมายเหตุ " + radTextBox_Remaek.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("ผู้บันทึก " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 25;
                e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

                e.Graphics.PageUnit = GraphicsUnit.Inch;
            }
            else
            {
                int Y = 0;
                //barcode.Data = mnsm;
                //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
                //e.Graphics.DrawString(HeadBill, SystemClass.printFont15, Brushes.Black, 0, Y);
                //Y += 25;
                //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                //Y += 20;
                //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
                Rectangle rect1 = new Rectangle(0, Y, 350, 400);
                StringFormat stringFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Near,
                    LineAlignment = StringAlignment.Near
                };
                e.Graphics.DrawString(HeadBill, new Font(new FontFamily("Tahoma"), 30), Brushes.Black, rect1, stringFormat);

                Y += 110;
                e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 0, Y);
                Y += 30;
                e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString(mnsm, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("ยอดเงิน [บาท] " + radTextBox_InputData.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("หมายเหตุ " + radTextBox_Remaek.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("ผู้บันทึก " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 25;
                e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

                e.Graphics.PageUnit = GraphicsUnit.Inch;
            }
        }

        private void PrintDocument_LO_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int Y = 0;
            switch (_pTypeInput)
            {
                case "1":
                    barcode.Data = mnsm;
                    Bitmap barcodeInBitmap0 = new Bitmap(barcode.drawBarcode());

                    e.Graphics.DrawString(" รับซองเงิน บ/ช ลูกหนี้ ", SystemClass.printFont15, Brushes.Black, 0, Y);
                    Y += 25;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawImage(barcodeInBitmap0, 5, Y);
                    Y += 67;
                    e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
                    Y += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("รหัสพนักงาน " + radTextBox_EmplID.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString(radLabel_Name.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString(radLabel_2Desc.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("ยอดเงิน [บาท] " + radTextBox_InputData.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("หมายเหตุ " + radTextBox_Remaek.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("ผู้รับ " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 25;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;
                case "2":
                    Print_ฺMNSV(e,"0");
                    //barcode.Data = mnsm;
                    //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
                    //e.Graphics.DrawString("สลิปโอนยอดบัตรสวัสดิการEMV", SystemClass.printFont15, Brushes.Black, 0, Y);
                    //Y += 25;
                    //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    //Y += 20;
                    //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
                    //Y += 67;
                    //e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, 0, Y);
                    //Y += 20;
                    //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
                    //Y += 20;
                    //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    //Y += 20;
                    //e.Graphics.DrawString("ยอดเงิน [บาท] " + radTextBox_InputData.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    //Y += 20;
                    //e.Graphics.DrawString("หมายเหตุ " + radTextBox_Remaek.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    //Y += 20;
                    //e.Graphics.DrawString("ผู้บันทึก " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
                    //Y += 25;
                    //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    //e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;
                case "4":
                    Print_ฺMNSV(e,"1"); break;
                case "5":
                    Print_ฺMNSV(e, "1"); break;
                case "6":
                    Print_ฺMNSV(e, "1"); break;
                default:
                    break;
            }

        }

        private void RadTextBox_EmplID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_EmplID.Text == "") return;

                switch (_pTypeInput)
                {
                    case "0"://นับเงินในลิ้นชัก
                        DataTable dtEmp = PosSaleClass.Get_POSLOGINOPEN(radTextBox_EmplID.Text);
                        if (dtEmp.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสแคชเชียร์");
                            radTextBox_EmplID.SelectAll();
                            radTextBox_EmplID.Focus();
                            return;
                        }
                        radTextBox_EmplID.Enabled = false;
                        radLabel_Name.Text = dtEmp.Rows[0]["NAME"].ToString();
                        radLabel_1Desc.Text = dtEmp.Rows[0]["POSNUMBER"].ToString() + "-" + dtEmp.Rows[0]["LOCATIONID"].ToString();
                        radLabel_2Desc.Text = dtEmp.Rows[0]["POSGROUP"].ToString() + "-" + dtEmp.Rows[0]["ZONEID"].ToString();
                        radTextBox_InputData.Enabled = true;
                        radTextBox_InputData.Focus();
                        break;
                    case "1"://ซองเงิน ลูกหนี้
                        DataTable dtEmpAR = Models.EmplClass.GetEmployee_Altnum(radTextBox_EmplID.Text);
                        if (dtEmpAR.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน");
                            radTextBox_EmplID.SelectAll();
                            radTextBox_EmplID.Focus();
                            return;
                        }
                        radTextBox_EmplID.Enabled = false;
                        radLabel_Name.Text = dtEmpAR.Rows[0]["SPC_NAME"].ToString();
                        radLabel_1Desc.Text = dtEmpAR.Rows[0]["POSSITION"].ToString();
                        radLabel_2Desc.Text = dtEmpAR.Rows[0]["NUM"].ToString() + "-" + dtEmpAR.Rows[0]["DESCRIPTION"].ToString();
                        radTextBox_InputData.Enabled = true;
                        radTextBox_InputData.Focus();
                        break;

                    case "3"://บันทึกข้อมูล ลงคิวพนักงานขับรถ/เก็บขยะ/สถานะรถรอขึ้นของ

                        switch (radTextBox_EmplID.Text.Length)
                        {
                            case 3://เก็บขยะ
                                DataTable dtBch3 = BranchClass.GetDetailBranchByID("MN" + radTextBox_EmplID.Text);
                                if (dtBch3.Rows.Count == 0)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบสาขาที่ระบุ เช็ครหัสสาขาใหม่อีกครั้ง.");
                                    radTextBox_EmplID.SelectAll();
                                    Cursor.Current = Cursors.Default;
                                    return;
                                }
                                radTextBox_EmplID.Enabled = false;
                                radTextBox_EmplID.Text = dtBch3.Rows[0]["BRANCH_ID"].ToString();
                                radLabel_1.Text = "รหัส";
                                radLabel_1Desc.Text = dtBch3.Rows[0]["BRANCH_ID"].ToString();
                                radLabel_2.Text = "ชื่อ";
                                radLabel_2Desc.Text = dtBch3.Rows[0]["BRANCH_NAME"].ToString();
                                pStaCase3 = "3";
                                radLabel_Name.Text = "สาขาแจ้งเก็บขยะ";
                                radButton_Save.Enabled = true;
                                radButton_Save.Focus();
                                break;
                            //case 4://สถานะรถรอขึ้นของ
                            //    DataTable dtCar4 = ConnectionClass.SelectSQL_Main($@"
                            //        SELECT	VEHICLEID,NAME,VEHICLESTATUS
                            //        FROM	SHOP2013TMP.[dbo].[SPC_VEHICLETABLE] WITH(NOLOCK) 
                            //        WHERE	VEHICLEID LIKE '%{radTextBox_EmplID.Text}'
                            //    ");
                            //    if (dtCar4.Rows.Count == 0)
                            //    {
                            //        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบทะเบียนรถที่ระบุ เช็คใหม่อีกครั้ง.");
                            //        radTextBox_EmplID.SelectAll();
                            //        Cursor.Current = Cursors.Default;
                            //        return;
                            //    }

                            //    radTextBox_EmplID.Enabled = false;
                            //    radLabel_1.Text = "ทะเบียน";
                            //    radLabel_1Desc.Text = dtCar4.Rows[0]["VEHICLEID"].ToString();
                            //    radLabel_2.Text = "ชื่อรถ";
                            //    radLabel_2Desc.Text = dtCar4.Rows[0]["NAME"].ToString();
                            //    pStaCase3 = "4";
                            //    //radLabel_Name.Text = "รถที่รอสถานะขึ้นของ";
                            //    radLabel_Name.Text = "สถานะ - " + StaLogistic(dtCar4.Rows[0]["VEHICLESTATUS"].ToString());
                            //    radButton_Save.Enabled = true;
                            //    radButton_Save.Focus();
                            //    break;

                            case 7://ลงคิวพนักงานขับรถ
                                DataTable dtEmp7 = Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_EmplID.Text);
                                if (dtEmp7.Rows.Count == 0)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบพนักงานที่ระบุ เช็ครหัสใหม่อีกครั้ง.");
                                    radTextBox_EmplID.SelectAll();
                                    Cursor.Current = Cursors.Default;
                                    return;
                                }
                                radTextBox_EmplID.Enabled = false;
                                strNum = dtEmp7.Rows[0]["NUM"].ToString();
                                radLabel_1.Text = "แผนก";
                                radLabel_2.Text = "ตำแหน่ง";
                                radLabel_Name.Text = dtEmp7.Rows[0]["SPC_NAME"].ToString();
                                radLabel_1Desc.Text = dtEmp7.Rows[0]["DESCRIPTION"].ToString();
                                radLabel_2Desc.Text = dtEmp7.Rows[0]["POSSITION"].ToString();
                                radButton_Save.Enabled = true;
                                pStaCase3 = "7";
                                radButton_Save.Focus();
                                break;
                            default:
                                MsgBoxClass.MsgBoxShowButtonOk_Error("ข้อมูลที่ระบุไม่ถูกต้อง" + Environment.NewLine +
                                    "ต้องระบุ รหัสพนักงาน/สาขา/ทะเบียนรถ เท่านั้น" + Environment.NewLine + "ระบุใหม่อีกครั้ง");
                                ClearTxt();
                                break;
                        }
                        break;

                    default:
                        break;
                }

            }
        }


        private void RadTextBox_InputData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    var iNput = Convert.ToDouble(radTextBox_InputData.Text);
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ข้อมูลที่ระบุต้องเป็นตัวเลข ลองใหม่อีกครั้ง.{Environment.NewLine}[{ex.Message}]");
                    radTextBox_InputData.SelectAll();
                    radTextBox_InputData.Focus();
                    return;
                }

                switch (_pTypeInput)
                {
                    case "0":
                        if (radTextBox_InputData.Text == "") return;
                        radButton_Save.Enabled = true;
                        radTextBox_Remaek.Enabled = true;
                        radButton_Save.Focus();
                        break;
                    case "1":
                        if (radTextBox_InputData.Text == "") return;
                        radButton_Save.Enabled = true;
                        radTextBox_Remaek.Enabled = true;
                        radTextBox_Remaek.Focus();
                        break;
                    case "2":
                        radButton_Save.Enabled = true;
                        radTextBox_Remaek.Enabled = true;
                        radTextBox_Remaek.Focus();
                        break;
                    case "4":
                        radButton_Save.Enabled = true;
                        radTextBox_Remaek.Enabled = true;
                        radTextBox_Remaek.Focus();
                        break;
                    case "5":
                        radButton_Save.Enabled = true;
                        radTextBox_Remaek.Enabled = true;
                        radTextBox_Remaek.Focus();
                        break;
                    case "6":
                        radButton_Save.Enabled = true;
                        radTextBox_Remaek.Enabled = true;
                        radTextBox_Remaek.Focus();
                        break;
                    default:
                        break;
                }
            }


        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeInput);
        }
    }
}