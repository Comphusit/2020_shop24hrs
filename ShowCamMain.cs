﻿using NVRCsharpDemo;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;


namespace HikvisionPreview
{
    public partial class ShowCamMain : Form
    {
        //public DataTable dtCam;
        private Int32 m_lFindHandle = -1;
        private bool m_bInitSDK = false;
        //private uint iLastErr = 0;


        #region "m_lUserID"
        //สำคัญ
        Int32 m_lUserID = -1;
        //Int32 m_lUserID2 = -1;
        //Int32 m_lUserID3 = -1;
        //Int32 m_lUserID4 = -1;
        //Int32 m_lUserID5 = -1;
        //Int32 m_lUserID6 = -1;
        //Int32 m_lUserID7 = -1;
        //Int32 m_lUserID8 = -1;
        //Int32 m_lUserID9 = -1;
        //Int32 m_lUserID10 = -1;
        //Int32 m_lUserID11 = -1;
        //Int32 m_lUserID12 = -1;
        //Int32 m_lUserID13 = -1;
        //Int32 m_lUserID14 = -1;
        //Int32 m_lUserID15 = -1;
        //Int32 m_lUserID16 = -1;
        #endregion

        #region "m_lRealHandle"
        //ช่อง
        Int32 m_lRealHandle1 = -1;
        Int32 m_lRealHandle2 = -1;
        Int32 m_lRealHandle3 = -1;
        Int32 m_lRealHandle4 = -1;
        Int32 m_lRealHandle5 = -1;
        Int32 m_lRealHandle6 = -1;
        Int32 m_lRealHandle7 = -1;
        Int32 m_lRealHandle8 = -1;
        Int32 m_lRealHandle9 = -1;
        Int32 m_lRealHandle10 = -1;
        Int32 m_lRealHandle11 = -1;
        Int32 m_lRealHandle12 = -1;
        Int32 m_lRealHandle13 = -1;
        Int32 m_lRealHandle14 = -1;
        Int32 m_lRealHandle15 = -1;
        Int32 m_lRealHandle16 = -1;
        #endregion

        #region "CH"
        int CH1 = 1;
        int CH2 = 2;
        int CH3 = 3;
        int CH4 = 4;
        int CH5 = 5;
        int CH6 = 6;
        int CH7 = 7;
        int CH8 = 8;
        int CH9 = 9;
        int CH10 = 10;
        int CH11 = 11;
        int CH12 = 12;
        int CH13 = 13;
        int CH14 = 14;
        int CH15 = 15;
        int CH16 = 16;

        #endregion

        public string typeCam;
        public string pSwitch;
        public string dvrIp = "";
        public string bchID = "";
        public string bchName = "";
        public string DVR_User; //User เข้า DVR
        public string DVR_Password; // Password เข้า DVR
        public string pJOBMN;

        public int CAM_CH;//จำนวนกล้องทั้งหมด
        public string pCAM_REMARKCHANNEL; //กล้องประจำจุด
        // public DataTable dtCamChanel;

        private CHCNetSDK.NET_DVR_PICCFG_V40 m_struPicCfgV40;
        public CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo;
        public CHCNetSDK.NET_DVR_TIME m_struTimeCfg;

        public string Desc;
        public string DescDateTime;
        // private Int32 m_lFindHandle = -1;
        //private uint dwAChanTotalNum = 0;

        //private uint iLastErr = 0;
        //
        public ShowCamMain()
        {
            InitializeComponent();

            this.FormClosing += ShowCamMain_FormClosing;

            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom; pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox3.SizeMode = PictureBoxSizeMode.Zoom; pictureBox4.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox5.SizeMode = PictureBoxSizeMode.Zoom; pictureBox6.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox7.SizeMode = PictureBoxSizeMode.Zoom; pictureBox8.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox9.SizeMode = PictureBoxSizeMode.Zoom; pictureBox10.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox11.SizeMode = PictureBoxSizeMode.Zoom; pictureBox12.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox13.SizeMode = PictureBoxSizeMode.Zoom; pictureBox14.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox15.SizeMode = PictureBoxSizeMode.Zoom; pictureBox16.SizeMode = PictureBoxSizeMode.Zoom;
        }
        //Close
        private void ShowCamMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SetLogout();
        }

        public void Login()
        {
            this.Cursor = Cursors.WaitCursor;
            m_bInitSDK = CHCNetSDK.NET_DVR_Init();
            if (m_bInitSDK == false)
            {
                for (int i = 0; i < 16; i++) { SetPathEmp(i + 1); }

                this.Cursor = Cursors.Default;
                return;
            }

            m_lUserID = CHCNetSDK.NET_DVR_Login_V30(dvrIp, ClassShow.DVRPortNumber, DVR_User, DVR_Password, ref DeviceInfo);
            //ในกรณีที่ Login ไม่ได้
            if (m_lUserID < 0)
            {
                for (int i = 0; i < 16; i++) { SetPathEmp(i + 1); }


                string ip = dvrIp.Replace("http://", "").Replace("/", "").Replace(":81", "").Replace(":82", "").Replace("ie.htm", "");
                try
                {
                    System.Net.NetworkInformation.Ping myPing = new System.Net.NetworkInformation.Ping();
                    System.Net.NetworkInformation.PingReply reply = myPing.Send(ip, 1000);
                    if (reply.Status.ToString() == "TimedOut")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถเข้าใช้งานได้{Environment.NewLine}เนื่องจาก DVR มีปัญหา [ping ไม่เจอ]{Environment.NewLine}แจ้งซ่อมที่ ComMinimart Tel 8570.");
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    //pingStatus = "ไม่พบข้อมูล" + ex.Message;
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Login เข้า DVR ได้{Environment.NewLine}ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
                    this.Cursor = Cursors.Default;
                    return;
                }

                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Login เข้า DVR ได้{Environment.NewLine}เช็ค User และ Password เข้าใช้งานอาจจะไม่ถูกต้อง{Environment.NewLine}แจ้งเช็ค User/Password ที่ ComMinimart Tel 8570{Environment.NewLine}แล้วลองใหม่อีกครั้ง.");
                this.Cursor = Cursors.Default;
                return;
            }
            else
            {
                DataTable dtCamChanel = new DataTable();
                dtCamChanel.Columns.Add("CH");
                if (pCAM_REMARKCHANNEL == "")
                {
                    for (int i = 0; i < CAM_CH; i++)
                    {
                        dtCamChanel.Rows.Add((i + 1));
                    }
                }
                else
                {
                    if (!pCAM_REMARKCHANNEL.Contains("/"))
                    {
                        dtCamChanel.Rows.Add(pCAM_REMARKCHANNEL);
                    }
                    else
                    {
                        string[] ch = pCAM_REMARKCHANNEL.Split('/');
                        for (int i = 0; i < ch.Length; i++)
                        {
                            dtCamChanel.Rows.Add(ch[i].ToString());
                        }
                    }
                }

                int iCH = 1;
                for (int i = 0; i < dtCamChanel.Rows.Count; i++)
                {
                    SetPlay(iCH, bchName, Int16.Parse(dtCamChanel.Rows[i]["CH"].ToString()));
                    iCH++;
                }
                FindRecode();
                GetDateTimeDVR();
                this.Cursor = Cursors.Default;
                return;
            }
        }

        //Load
        public void ShowCamMain_Load(object sender, EventArgs e)
        {
            this.Text = typeCam;

            for (int i = 0; i < 16; i++)
            {
                SetPathEmp(i + 1);
            }

            if ((pSwitch == "1"))
            {
                SetLogout();
            }
            else
            {
                //if ((DVR_User == "") || (DVR_User is null))
                //{
                //    DVR_User = "admin";
                //    DVR_Password = "admin8570";
                //}
                Login();
            }

        }
        //SetPathEmp16
        void SetPathEmp(int iCH)
        {
            switch (iCH)
            {
                case 1:
                    label_1.Text = "EMP";
                    label_1.Visible = false; pictureBox1.Visible = false;
                    break;
                case 2:
                    label_2.Text = "EMP";
                    label_2.Visible = false; pictureBox2.Visible = false;
                    break;
                case 3:
                    label_3.Text = "EMP";
                    label_3.Visible = false; pictureBox3.Visible = false;
                    break;
                case 4:
                    label_4.Text = "EMP";
                    label_4.Visible = false; pictureBox4.Visible = false;
                    break;
                case 5:
                    label_5.Text = "EMP";
                    label_5.Visible = false; pictureBox5.Visible = false;
                    break;
                case 6:
                    label_6.Text = "EMP";
                    label_6.Visible = false; pictureBox6.Visible = false;
                    break;
                case 7:
                    label_7.Text = "EMP";
                    label_7.Visible = false; pictureBox7.Visible = false;
                    break;
                case 8:
                    label_8.Text = "EMP";
                    label_8.Visible = false; pictureBox8.Visible = false;
                    break;
                case 9:
                    label_9.Text = "EMP";
                    label_9.Visible = false; pictureBox9.Visible = false;
                    break;
                case 10:
                    label_10.Text = "EMP";
                    label_10.Visible = false; pictureBox10.Visible = false;
                    break;
                case 11:
                    label_11.Text = "EMP";
                    label_11.Visible = false; pictureBox11.Visible = false;
                    break;
                case 12:
                    label_12.Text = "EMP";
                    label_12.Visible = false; pictureBox12.Visible = false;
                    break;
                case 13:
                    // ClassShow.PathEmp(pictureBox13); 
                    label_13.Text = "EMP";
                    label_13.Visible = false; pictureBox13.Visible = false;
                    break;
                case 14:
                    //ClassShow.PathEmp(pictureBox14); 
                    label_14.Text = "EMP";
                    label_14.Visible = false; pictureBox14.Visible = false;
                    break;
                case 15:
                    // ClassShow.PathEmp(pictureBox15); 
                    label_15.Text = "EMP";
                    label_15.Visible = false; pictureBox15.Visible = false;
                    break;
                case 16:
                    // ClassShow.PathEmp(pictureBox16);
                    label_16.Text = "EMP";
                    label_16.Visible = false; pictureBox16.Visible = false;
                    break;
                default:
                    break;
            }

        }
        void SetPlay(int iCH, string DVR_BchName, Int16 Channel)
        {
            string ch_Name = ListAnalogChannel(m_lUserID, Channel);
            switch (iCH)
            {
                case 1:
                    CH1 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_1.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox1);
                    }
                    else
                    {
                        label_1.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                        m_lRealHandle1 = ClassShow.Preview(Channel, pictureBox1, m_lUserID);
                    }
                    label_1.Visible = true; pictureBox1.Visible = true;
                    break;
                case 2:
                    CH2 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_2.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox2);
                    }
                    else
                    {
                        m_lRealHandle2 = ClassShow.Preview(Channel, pictureBox2, m_lUserID);
                        label_2.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_2.Visible = true; pictureBox2.Visible = true;
                    break;
                case 3:
                    CH3 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_3.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox3);
                    }
                    else
                    {
                        m_lRealHandle3 = ClassShow.Preview(Channel, pictureBox3, m_lUserID);
                        label_3.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_3.Visible = true; pictureBox3.Visible = true;
                    break;
                case 4:
                    CH4 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_4.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox4);
                    }
                    else
                    {
                        m_lRealHandle4 = ClassShow.Preview(Channel, pictureBox4, m_lUserID);
                        label_4.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_4.Visible = true; pictureBox4.Visible = true;
                    break;
                case 5:
                    CH5 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_5.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox5);
                    }
                    else
                    {
                        m_lRealHandle5 = ClassShow.Preview(Channel, pictureBox5, m_lUserID);
                        label_5.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_5.Visible = true; pictureBox5.Visible = true;
                    break;
                case 6:
                    CH6 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_6.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox6);
                    }
                    else
                    {
                        m_lRealHandle6 = ClassShow.Preview(Channel, pictureBox6, m_lUserID);
                        label_6.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_6.Visible = true; pictureBox6.Visible = true;
                    break;
                case 7:
                    CH7 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_7.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox7);
                    }
                    else
                    {
                        m_lRealHandle7 = ClassShow.Preview(Channel, pictureBox7, m_lUserID);
                        label_7.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_7.Visible = true; pictureBox7.Visible = true;
                    break;
                case 8:
                    CH8 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_8.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox8);
                    }
                    else
                    {
                        m_lRealHandle8 = ClassShow.Preview(Channel, pictureBox8, m_lUserID);
                        label_8.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_8.Visible = true; pictureBox8.Visible = true;
                    break;
                case 9:
                    CH9 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_9.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox9);
                    }
                    else
                    {
                        m_lRealHandle9 = ClassShow.Preview(Channel, pictureBox9, m_lUserID);
                        label_9.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_9.Visible = true; pictureBox9.Visible = true;
                    break;
                case 10:
                    CH10 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_10.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox10);
                    }
                    else
                    {
                        m_lRealHandle10 = ClassShow.Preview(Channel, pictureBox10, m_lUserID);
                        label_10.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_10.Visible = true; pictureBox10.Visible = true;
                    break;
                case 11:
                    CH11 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_11.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox11);
                    }
                    else
                    {
                        m_lRealHandle11 = ClassShow.Preview(Channel, pictureBox11, m_lUserID);
                        label_11.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_11.Visible = true; pictureBox11.Visible = true;
                    break;
                case 12:
                    CH12 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_12.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox12);
                    }
                    else
                    {
                        m_lRealHandle12 = ClassShow.Preview(Channel, pictureBox12, m_lUserID);
                        label_12.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_12.Visible = true; pictureBox12.Visible = true;
                    break;
                case 13:
                    CH13 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_13.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox13);
                    }
                    else
                    {
                        m_lRealHandle13 = ClassShow.Preview(Channel, pictureBox13, m_lUserID);
                        label_13.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_13.Visible = true; pictureBox13.Visible = true;
                    break;
                case 14:
                    CH14 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_14.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox14);
                    }
                    else
                    {
                        m_lRealHandle14 = ClassShow.Preview(Channel, pictureBox14, m_lUserID);
                        label_14.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }

                    label_14.Visible = true; pictureBox14.Visible = true;
                    break;
                case 15:
                    CH15 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_15.Text = "EMP - ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox15);
                    }
                    else
                    {
                        m_lRealHandle15 = ClassShow.Preview(Channel, pictureBox15, m_lUserID);
                        label_15.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_15.Visible = true; pictureBox15.Visible = true;
                    break;
                case 16:
                    CH16 = Channel;
                    if (ch_Name.ToUpper().Contains("EMP"))
                    {
                        label_16.Text = "ไม่มีกล้อง";
                        ClassShow.PathEmp(pictureBox16);
                    }
                    else
                    {
                        m_lRealHandle16 = ClassShow.Preview(Channel, pictureBox16, m_lUserID);
                        label_16.Text = DVR_BchName + " [" + Channel.ToString() + @"] " + ch_Name;
                    }
                    label_16.Visible = true; pictureBox16.Visible = true;
                    break;
                default:
                    break;
            }
        }


        public string ListAnalogChannel(Int32 m_lUserID, Int16 m_lChannel)
        {
            string str2;
            UInt32 dwReturn = 0;
            Int32 nSize = Marshal.SizeOf(m_struPicCfgV40);
            IntPtr ptrPicCfg = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(m_struPicCfgV40, ptrPicCfg, false);
            if (!CHCNetSDK.NET_DVR_GetDVRConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_PICCFG_V40, m_lChannel, ptrPicCfg, (UInt32)nSize, ref dwReturn))
            {
                str2 = "Cam " + m_lChannel.ToString();
            }
            else
            {
                m_struPicCfgV40 = (CHCNetSDK.NET_DVR_PICCFG_V40)Marshal.PtrToStructure(ptrPicCfg, typeof(CHCNetSDK.NET_DVR_PICCFG_V40));
                if (m_struPicCfgV40.dwShowChanName == 1)
                {
                    str2 = "/ ";
                }
                else
                {
                    str2 = "X ";
                }
                str2 += System.Text.Encoding.GetEncoding("GBK").GetString(m_struPicCfgV40.sChanName);
            }
            Marshal.FreeHGlobal(ptrPicCfg);
            return str2;
        }

        void OpenLarge(string pEmp, Int32 m_lRealHandle, int iCH)
        {
            if (m_lUserID < 0) { return; }

            if (pEmp.ToUpper().Contains("EMP"))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับกล้องว่าง ไม่สามารถเปิดหน้าจอใหญ่ได้.");
                return;
            }
            ShowSingleLarge _ShowSingleLarge = new ShowSingleLarge
            {

                DVR_TypeCam = typeCam,
                DVR_BchID = bchID,
                DVR_BchName = bchName,
                DVR_CH = iCH,
                DVR_IP = dvrIp,
                pJOBMN = pJOBMN,
                DVR_User = DVR_User,
                DVR_Password = DVR_Password,
                pCase = "1",
                m_lUserID = m_lUserID,
                m_lRealHandle = m_lRealHandle
            };

            _ShowSingleLarge.Show();
        }

        #region "OpenLarge-DoubleClick"
        private void PictureBox1_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_1.Text, m_lRealHandle1, CH1);
        }

        private void PictureBox2_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_2.Text, m_lRealHandle2, CH2);
        }

        private void PictureBox3_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_3.Text, m_lRealHandle3, CH3);
        }

        private void PictureBox4_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_4.Text, m_lRealHandle4, CH4);
        }

        private void PictureBox5_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_5.Text, m_lRealHandle5, CH5);
        }

        private void PictureBox6_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_6.Text, m_lRealHandle6, CH6);
        }

        private void PictureBox7_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_7.Text, m_lRealHandle7, CH7);
        }

        private void PictureBox8_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_8.Text, m_lRealHandle8, CH8);
        }

        private void PictureBox9_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_9.Text, m_lRealHandle9, CH9);
        }

        private void PictureBox10_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_10.Text, m_lRealHandle10, CH10);
        }

        private void PictureBox11_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_11.Text, m_lRealHandle11, CH11);
        }

        private void PictureBox12_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_12.Text, m_lRealHandle12, CH12);
        }

        private void PictureBox13_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_13.Text, m_lRealHandle13, CH13);
        }

        private void PictureBox14_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_14.Text, m_lRealHandle14, CH14);
        }

        private void PictureBox15_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_15.Text, m_lRealHandle15, CH15);
        }

        private void PictureBox16_DoubleClick(object sender, EventArgs e)
        {
            OpenLarge(label_16.Text, m_lRealHandle16, CH16);
        }

        #endregion

        public void SetLogout()
        {

            if (m_lRealHandle1 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle1); m_lRealHandle1 = -1; }
            if (m_lRealHandle2 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle2); m_lRealHandle2 = -1; }
            if (m_lRealHandle3 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle3); m_lRealHandle3 = -1; }
            if (m_lRealHandle4 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle4); m_lRealHandle4 = -1; }
            if (m_lRealHandle5 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle5); m_lRealHandle5 = -1; }
            if (m_lRealHandle6 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle6); m_lRealHandle6 = -1; }
            if (m_lRealHandle7 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle7); m_lRealHandle7 = -1; }
            if (m_lRealHandle8 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle8); m_lRealHandle8 = -1; }
            if (m_lRealHandle9 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle9); m_lRealHandle9 = -1; }
            if (m_lRealHandle10 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle10); m_lRealHandle10 = -1; }
            if (m_lRealHandle11 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle11); m_lRealHandle11 = -1; }
            if (m_lRealHandle12 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle12); m_lRealHandle12 = -1; }
            if (m_lRealHandle13 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle13); m_lRealHandle13 = -1; }
            if (m_lRealHandle14 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle14); m_lRealHandle14 = -1; }
            if (m_lRealHandle15 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle15); m_lRealHandle15 = -1; }
            if (m_lRealHandle16 >= 0) { CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle16); m_lRealHandle16 = -1; }

            //注销登录
            if (m_lUserID >= 0) { CHCNetSDK.NET_DVR_Logout(m_lUserID); m_lUserID = -1; }

            CHCNetSDK.NET_DVR_Cleanup();
            for (int i = 0; i < 16; i++)
            {
                SetPathEmp(i + 1);
            }

        }
        //Reboot
        public Boolean Reboot()
        {
            if (m_lUserID < 0)
            {
                MessageBox.Show("ยังไมได้ Login อุปกรณ์ ไม่สามารถ Reboot DVR ได้.");
                return false;
            }
            if (MessageBox.Show("ยืนยันการ Reboot DVR ?", "SHOP24HRS.", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                == DialogResult.No)
            {
                return false;
            }

            Boolean result = CHCNetSDK.NET_DVR_RebootDVR(m_lUserID);
            if (result == true)
            {
                MessageBox.Show("Reboot DVR เรียบร้อย.", "SHOP24HRS.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetLogout();
                return true;
            }
            else
            {
                MessageBox.Show("ไม่สามารถ Reboot DVR ได้ ลองใหม่อีกครั้ง.", "SHOP24HRS.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        //จำนวนวันบันทึก
        void FindRecode()
        {
            int chNumber = 2;
            if (label_1.Text != "") chNumber = 1;

            CHCNetSDK.NET_DVR_FILECOND_V40 struFileCond_V40 = new CHCNetSDK.NET_DVR_FILECOND_V40
            {
                lChannel = chNumber, //Channel number
                dwFileType = 0xff, //0xff-All，0-Timing record，1-Motion detection，2-Alarm trigger，...
                dwIsLocked = 0xff //0-unfixed file，1-fixed file，0xff means all files（including fixed and unfixed files）
            };

            //Set the starting time to search video files
            struFileCond_V40.struStartTime.dwYear = (uint)DateTime.Now.AddYears(-2).Year;
            struFileCond_V40.struStartTime.dwMonth = (uint)DateTime.Now.AddYears(-2).Month;
            struFileCond_V40.struStartTime.dwDay = (uint)DateTime.Now.AddYears(-2).Day;
            struFileCond_V40.struStartTime.dwHour = (uint)DateTime.Now.AddYears(-2).Hour;
            struFileCond_V40.struStartTime.dwMinute = (uint)DateTime.Now.AddYears(-2).Minute;
            struFileCond_V40.struStartTime.dwSecond = (uint)DateTime.Now.AddYears(-2).Second;

            //Set the stopping time to search video files
            struFileCond_V40.struStopTime.dwYear = (uint)DateTime.Now.Year;
            struFileCond_V40.struStopTime.dwMonth = (uint)DateTime.Now.Month;
            struFileCond_V40.struStopTime.dwDay = (uint)DateTime.Now.Day;
            struFileCond_V40.struStopTime.dwHour = (uint)DateTime.Now.Hour;
            struFileCond_V40.struStopTime.dwMinute = (uint)DateTime.Now.Minute;
            struFileCond_V40.struStopTime.dwSecond = (uint)DateTime.Now.Second;


            //Start to search video files 
            m_lFindHandle = CHCNetSDK.NET_DVR_FindFile_V40(m_lUserID, ref struFileCond_V40);

            if (m_lFindHandle < 0)
            {
                Desc = "ไม่สามารถค้นหาข้อมูลการบันทึกย้อนหลังได้ ";
            }
            else
            {
                CHCNetSDK.NET_DVR_FINDDATA_V30 struFileData = new CHCNetSDK.NET_DVR_FINDDATA_V30(); ;
                while (true)
                {
                    //Get file information one by one.
                    int result = CHCNetSDK.NET_DVR_FindNextFile_V30(m_lFindHandle, ref struFileData);
                    if (result == CHCNetSDK.NET_DVR_ISFINDING)  //Searching, please wait
                    {
                        continue;
                    }
                    else if (result == CHCNetSDK.NET_DVR_FILE_SUCCESS) //Get the file information successfully
                    {

                        string str2 = Convert.ToString(struFileData.struStartTime.dwYear) + "-" +
                             struFileData.struStartTime.dwMonth.ToString("00") + "-" +
                              struFileData.struStartTime.dwDay.ToString("00") + "  " +
                               struFileData.struStartTime.dwHour.ToString("00") + ":" +
                               struFileData.struStartTime.dwMinute.ToString("00") + ":" +
                               struFileData.struStartTime.dwSecond.ToString("00");

                        Desc = "ย้อนหลังได้ถึงวันที่ " + str2; break;
                    }
                    else if (result == CHCNetSDK.NET_DVR_FILE_NOFIND || result == CHCNetSDK.NET_DVR_NOMOREFILE)
                    {
                        Desc = "File Not Found"; break; //No file found or no more file found, searching is finished 
                    }
                    else
                    {
                        Desc = "File Not Found"; break;
                    }
                }

            }
        }
        //วันเวลา DVR
        void GetDateTimeDVR()
        {
            UInt32 dwReturn = 0;
            Int32 nSize = Marshal.SizeOf(m_struTimeCfg);
            IntPtr ptrTimeCfg = Marshal.AllocHGlobal(nSize);
            Marshal.StructureToPtr(m_struTimeCfg, ptrTimeCfg, false);
            if (!CHCNetSDK.NET_DVR_GetDVRConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_TIMECFG, -1, ptrTimeCfg, (UInt32)nSize, ref dwReturn))
            {
                DescDateTime = "ไม่สามารถดึงวันที่ของ DVR ได้";
            }
            else
            {
                m_struTimeCfg = (CHCNetSDK.NET_DVR_TIME)Marshal.PtrToStructure(ptrTimeCfg, typeof(CHCNetSDK.NET_DVR_TIME));
                DescDateTime = "วันที่ของ DVR ";
                DescDateTime += m_struTimeCfg.dwYear.ToString() + "-" + m_struTimeCfg.dwMonth.ToString("00") + "-" + m_struTimeCfg.dwDay.ToString("00") + "  ";
                DescDateTime += m_struTimeCfg.dwHour.ToString() + ":" + m_struTimeCfg.dwMinute.ToString("00") + ":" + m_struTimeCfg.dwMinute.ToString("00");
            }
            Marshal.FreeHGlobal(ptrTimeCfg);
        }

    }
}

