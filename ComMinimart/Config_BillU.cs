﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Diagnostics;

namespace PC_Shop24Hrs.ComMinimart
{
    public partial class Config_BillU : Telerik.WinControls.UI.RadForm
    {
        private DataTable dt = new DataTable();
        readonly string _pTypeReport;
        //0 เปลี่ยนค่าเงินในบิลเปลี่ยน
        //1 ปิด-เปิด Qr code
        //2 เพิ่มพนักงานนับสต็อกสาขา/ลดราคา
        //Load
        public Config_BillU(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load Main
        private void Config_BillU_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_CheckWeb.ShowBorder = true; radButtonElement_CheckWeb.ToolTipText = "เช็คสถานะ WebService";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขยอดเงินในบิลเปลี่ยน";
            radStatusStrip1.SizingGrip = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            switch (_pTypeReport)
            {
                case "0"://เปลี่ยนค่าเงินในบิลเปลี่ยน

                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_TYPEID("POSGROUP", "รหัส"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "สาขา", 300));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Amount", "ยอดเงิน", 150));

                    //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                    ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "Amount > '3000' ", false)
                    { CellBackColor = ConfigClass.SetColor_PurplePastel() };
                    this.RadGridView_Show.Columns["POSGROUP"].ConditionalFormattingObjectList.Add(obj1);
                    this.RadGridView_Show.Columns["NAME"].ConditionalFormattingObjectList.Add(obj1);
                    this.RadGridView_Show.Columns["Amount"].ConditionalFormattingObjectList.Add(obj1);

                    radButtonElement_CheckWeb.Enabled = false;
                    radLabel_Detail.Text = "DoubleClick รายการ >> ปรับเพิ่ม-ลดยอดเงิน";
                    break;

                case "1"://ปิด-เปิด Qr code

                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StoreId", "รหัสกลุ่มขาย", 100));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("StoreId_DESC", "ชื่อกลุ่มขาย", 300));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GatewayId", "รหัสประเภท", 100));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GatewayId_DESC", "ชื่อประเภท", 300));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("Block_DESC", "สถานะ", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("Block", "สถานะ", 100));

                    //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "Block = '1' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_Show.Columns["StoreId"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_Show.Columns["StoreId_DESC"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_Show.Columns["GatewayId"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_Show.Columns["GatewayId_DESC"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_Show.Columns["Block_DESC"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_Show.Columns["Block"].ConditionalFormattingObjectList.Add(obj2);
                    radButtonElement_Edit.Enabled = false;
                    radLabel_Detail.Text = "Click สถานะ >> เปิด-ปิด QR CODE";
                    break;

                case "2"://เพิ่มพนักงานนับสต็อกสาขา
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_ID", "รหัส", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 250));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 120));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("IVZ_HROMPositionTitle", "รหัส"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 220));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("EMP_TRANSTOCK", "สิทธิ์การนับ", 120));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("EMP_STADISCOUNT", "สิทธิ์ลดราคา", 120));
                                        
                    DatagridClass.SetCellBackClolorByExpression("EMP_TRANSTOCK", "EMP_TRANSTOCK = '1' ", ConfigClass.SetColor_SkyPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("EMP_STADISCOUNT", "EMP_STADISCOUNT = '1' ", ConfigClass.SetColor_PinkPastel(), RadGridView_Show);

                    radButtonElement_Edit.Enabled = false;
                    radButtonElement_CheckWeb.Enabled = false;
                    radLabel_Detail.Text = "Click สิทธิ์การนับ >> ปรับเพิ่ม-ลดสิทธิ์การนับสต็อก [สีฟ้า] | Click สิทธิ์ลดราคา >> ปรับเพิ่ม-ลดสิทธิ์การลดราคา [สีชมพู]";
                    break;

                default:
                    break;
            }


            switch (_pTypeReport)
            {
                case "0":
                    Set_DGV();
                    break;

                case "1"://QR CODE
                    Set_QR();
                    break;

                case "2"://CheckStock
                    Set_EmpChekStock();
                    break;

                default:
                    break;
            }

        }

        //Set DGV
        void Set_DGV()
        {
            dt = PosSaleClass.GetAmount_BillU();
            RadGridView_Show.DataSource = dt;
        }
        // QR
        void Set_QR()
        {
            dt = PosSaleClass.GetQRCODE();
            RadGridView_Show.DataSource = dt;
        }
        //Set Emp
        void Set_EmpChekStock()
        {
            this.Cursor = Cursors.WaitCursor;
            dt = Models.EmplClass.GetEmpAll_SHOP24("MN%");
            RadGridView_Show.DataSource = dt;
            this.Cursor = Cursors.Default;
        }
        //SetEdit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (_pTypeReport == "0") SetEditAmount();
        }
        //set Edit U
        void SetEditAmount()
        {
            if ((RadGridView_Show.CurrentRow.Cells["POSGROUP"].Value.ToString() == "") || (RadGridView_Show.Rows.Count == 0)) return;

            FormShare.InputData f = new FormShare.InputData("0",
                RadGridView_Show.CurrentRow.Cells["POSGROUP"].Value.ToString() + "-" + RadGridView_Show.CurrentRow.Cells["NAME"].Value.ToString(),
                "จำนวนเงิน.", "บาท");
            if (f.ShowDialog(this) == DialogResult.Yes)
            {
                string T = PosSaleClass.UpdateAmountBillU(RadGridView_Show.CurrentRow.Cells["POSGROUP"].Value.ToString(), double.Parse(f.pInputData));
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    DataRow[] rows = dt.Select("POSGROUP = '" + RadGridView_Show.CurrentRow.Cells["POSGROUP"].Value.ToString() + "'");
                    dt.Rows[dt.Rows.IndexOf(rows[0])]["Amount"] = f.pInputData;
                    dt.AcceptChanges();
                }
            }
            else return;
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //double Click
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            // SetEditAmount();
            switch (_pTypeReport)
            {
                case "0":
                    SetEditAmount();
                    break;
                default:
                    break;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {

            switch (_pTypeReport)
            {
                case "1"://QR CODE
                    if ((RadGridView_Show.CurrentRow.Cells["GatewayId"].Value.ToString() == "") || (RadGridView_Show.Rows.Count == 0)) return;
                    switch (e.Column.Name)
                    {
                        case "Block":
                            string descPay = RadGridView_Show.CurrentRow.Cells["GatewayId_DESC"].Value.ToString();
                            string descMN = RadGridView_Show.CurrentRow.Cells["StoreId_DESC"].Value.ToString();
                            string sta = RadGridView_Show.CurrentRow.Cells["Block"].Value.ToString(); //1 close 0 open

                            string staNew, staNewDesc;
                            if (sta == "1")//ถ้าปิดอยู่
                            {
                                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการเปิดใช้งานระบบ " + descPay + Environment.NewLine + descMN + "?") == DialogResult.No) return;
                                staNew = "0"; staNewDesc = "เปิด";
                            }
                            else if (sta == "0")//ถ้าเปิดอยู่
                            {
                                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการปิดใช้งานระบบ " + descPay + Environment.NewLine + descMN + "?") == DialogResult.No) return;
                                staNew = "1"; staNewDesc = "ปิด";
                            }
                            else
                            {
                                staNew = "2"; staNewDesc = "";
                            }
                            if (staNew == "2") return;

                            string result = PosSaleClass.UpdateQRCode(RadGridView_Show.CurrentRow.Cells["StoreId"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["GatewayId"].Value.ToString(), staNew);
                            MsgBoxClass.MsgBoxShow_SaveStatus(result);
                            if (result == "")
                            {
                                RadGridView_Show.CurrentRow.Cells["Block_DESC"].Value = staNewDesc;
                                RadGridView_Show.CurrentRow.Cells["Block"].Value = staNew;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "2"://Update CheclStock
                    if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                    if ((RadGridView_Show.CurrentRow.Cells["EMP_ID"].Value.ToString() == "") || (RadGridView_Show.Rows.Count == 0)) return;

                    switch (e.Column.Name)
                    {
                        case "EMP_TRANSTOCK":
                            CheckEmpUpdate("2", "EMP_TRANSTOCK", "ลดราคาสินค้า");

                            break;
                        case "EMP_STADISCOUNT":
                            CheckEmpUpdate("3", "EMP_STADISCOUNT", "ลดราคาสินค้า");
                            break;
                        default:
                            break;
                    }

                    break;
                default:
                    break;
            }


        }
        //CheckEmp
        void CheckEmpUpdate(string pCase, string fileName, string desc)
        {
            string staOld = RadGridView_Show.CurrentRow.Cells[fileName].Value.ToString();
            string staNew, strDesc;
            if (staOld == "1")
            {
                strDesc = $@"ไม่ให้{desc}ได้อีก ?";
                staNew = "0";
            }
            else
            {
                strDesc = $@"ให้สามารถ{desc}ได้ ?";
                staNew = "1";
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการปรับพนักงาน" +
                  Environment.NewLine + RadGridView_Show.CurrentRow.Cells["EMP_ID"].Value.ToString() + " " +
                                        RadGridView_Show.CurrentRow.Cells["SPC_NAME"].Value.ToString() +
                  Environment.NewLine + strDesc) == DialogResult.No) return;


            if ((RadGridView_Show.CurrentRow.Cells["IVZ_HROMPositionTitle"].Value.ToString() == "03") && (staOld == "0"))
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการกำหนดสิทธิ์ให้พนักงานที่ระบุ" + Environment.NewLine +
                    "เนื่องจากพนักงานมีตำแหน่ง เป็น " + RadGridView_Show.CurrentRow.Cells["POSSITION"].Value.ToString() + " ?") == DialogResult.No) return;
            }

            string result = ConnectionClass.ExecuteSQL_Main(Models.EmplClass.Save_EMPLOYEE(pCase, RadGridView_Show.CurrentRow.Cells["EMP_ID"].Value.ToString(), staNew, ""));

            if (result == "") RadGridView_Show.CurrentRow.Cells[fileName].Value = staNew; else MsgBoxClass.MsgBoxShow_SaveStatus(result);

        }
        //check Web
        private void RadButtonElement_CheckWeb_Click(object sender, EventArgs e)
        {
            Process.Start("chrome.exe", "https://www.supercheapphuket.com/TestLink.php");
        }
    }
}
