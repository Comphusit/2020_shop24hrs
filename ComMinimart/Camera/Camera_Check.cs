﻿//CheckOK
using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System.Drawing;


namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_Check : Form
    {
        //ตรวจกล้อง --admin 0--center1 --รปภ2
        DataTable dt = new DataTable();
        private string Selectdate;
        readonly string _pUserpermis;
        private int iRows = 0;
        private DataRow[] rRows = new DataRow[1];
        readonly DATACAM_IP dATACAM_IP = new DATACAM_IP();
        DataTable dtStation = new DataTable();
        int checkDataActive = 0;
        string[] items;
        public Camera_Check(string pUserpermis)
        {
            InitializeComponent();
            this.KeyPreview = true;
            this.Text = "ตรวจกล้องประจำวัน";
            _pUserpermis = pUserpermis;
            radButton_Search.ButtonElement.ShowBorder = true;
            radButtonElement_excel.ShowBorder = true;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_ID", "CAMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_DEPT", "สาขา", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อสาขา", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_DESC", "คำอธิบาย", 230));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_IP", "LINK CAM", 230));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_CH", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_AMORN", "AMORN"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_TYPECAM", "พื้นที่"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("CAM_CHECK", "ตรวจ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_TECHNICID", "รหัส"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_TECHNICNAME", "ช่าง"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CHECK_EMPID", "พนักงาน"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_EMPNICKNAME", "ผู้ตรวจ", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_DATETIME", "เวลาตรวจ", 170));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_RECORD", "จำนวนวันบันทึก", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_DATERECORD", "บันทึกย้อนหลัง", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_CHECKJOBID", "หมายเลขจ๊อบ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_DATEINS", "JOB_DATEINS"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_REMARK", "หมายเหตุ", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_DVR", "CAM_DVR"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SHOW_ID", "SHOW_ID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("COUNT", "COUNT"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PATH_IMAGE", "PATH_IMAGE"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMAGE", "รูป", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_REMARKCHANNEL", "CAM_REMARKCHANNEL"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SET_USER", "USER", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SET_PASSWORD", "PASSWORD", 100));

            ////Freeze Column
            this.radGridView_Show.MasterTemplate.Columns["CAM_DEPT"].IsPinned = true;
            this.radGridView_Show.MasterTemplate.Columns["DESCRIPTION"].IsPinned = true;
            //radDropDownList Font
            DatagridClass.SetDefaultFontDropDown(radDropDown_Area);
            DatagridClass.SetDefaultFontDropDown(radDropDown_Cam);

            this.radCheckBox_Check.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral;
            this.radCheckBox_Check.ButtonElement.TextElement.ForeColor = ConfigClass.SetColor_Blue();

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Check, DateTime.Today, DateTime.Now.Date);
            Selectdate = this.radDateTimePicker_Check.Value.ToString("yyyy-MM-dd");

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            DatagridClass.SetCellBackClolorByExpression("CAM_DESC", "CAM_CHECKJOBID LIKE 'MJOB%' ", ConfigClass.SetColor_Red(), radGridView_Show);


            DatagridClass.SetDefaultFontDropDown(radDomainUpDown_Round);
            if (_pUserpermis == "1" || _pUserpermis == "2")
            {
                radGridView_Show.MasterTemplate.Columns["CAM_TECHNICNAME"].IsVisible = false;
            }
            if (_pUserpermis == "0" || _pUserpermis == "2")
            {
                radGridView_Show.MasterTemplate.Columns["IMAGE"].IsVisible = false;
            }
            this.GetGridViewSummary();
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void CreateBindingList(int count)
        {
            items = new string[count];
            for (int i = 0; i < count; i++)
            {
                items[i] = (i + 1).ToString();
            }
        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("DESCRIPTION", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        #region "ROWSNUMBER"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        private void Camera_Check_Load(object sender, EventArgs e)
        {

            //-----new
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            this.GetDropDownList();
            radLabel_Round.Enabled = false;
            radDomainUpDown_Round.Enabled = false;

            switch (_pUserpermis)
            {
                //admin
                case "0":
                    //return;
                    break;
                //center
                case "1":
                    radDropDown_Area.SelectedValue = "MN";
                    radDropDown_Area.Enabled = false;
                    break;
                //รภป
                case "2":
                    radDropDown_Cam.Enabled = false;
                    break;
                default:
                    break;
            }

            dt = this.Getdatacheck();
            dtStation = this.GetCamStation();
            radGridView_Show.DataSource = dt;

            GridViewfilter(radGridView_Show.Columns["CAM_CHECK"], CheckStatus());
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
            //-----new
        }
        private void GetDropDownList()
        {
            radDropDown_Area.DataSource = GetCamArea();
            radDropDown_Area.DisplayMember = "CAMAREA";
            radDropDown_Area.ValueMember = "CAM_TYPECAM";
            radDropDown_Area.SelectedIndex = 0;

            radDropDown_Cam.DataSource = GetCamtype();
            radDropDown_Cam.ValueMember = "SHOW_ID";
            radDropDown_Cam.DisplayMember = "SHOW_NAME";
            radDropDown_Cam.SelectedIndex = 0;

        }
        private DataTable GetCamtype()
        {
            string sql = string.Format(@"SELECT 'C00' AS [SHOW_ID],'ทั้งหมด' AS [SHOW_NAME]
                        UNION ( SELECT [SHOW_ID],[SHOW_NAME] FROM  [SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK) WHERE [TYPE_CONFIG] = '21')");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetCamArea()
        {
            string sql = string.Format(@"SELECT 'ALL' AS CAM_TYPECAM ,'ทั้งหมด' AS CAMAREA
	              UNION SELECT CAM_TYPECAM,
		          CASE CAM_TYPECAM
		          WHEN 'MN' THEN 'มินิมาร์ท'
		          WHEN 'RET' THEN 'สาขาใหญ่ RET'
		          WHEN 'SPC' THEN 'สาขาใหญ่ SPC'
		          WHEN 'OTH' THEN 'พื้นที่ส่วนนอก OTH'
				  END AS CAMAREA
				  FROM SHOP_JOBCAM WITH (NOLOCK)
				  GROUP BY CAM_TYPECAM");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable Getdatacheck()
        {
            string Condition = "";

            //int CheckDay = (int)(radDateTimePicker_Check.Value).DayOfWeek;
            if (_pUserpermis == "0") //admin
            {
                Condition = $@" WHERE ISNULL(CAM_CHECKDAY,0) in ('0',datepart(WEEKDAY,GETDATE())) ";
            }
            else if (_pUserpermis == "2") //secur
            {
                Condition = $@" WHERE SHOP_JOBCAM.BRANCH_ID = 'MN000' AND CAM_CHECKUSER = '0'";
            }

            if (radDropDown_Area.SelectedIndex != 0)
            {
                if (string.IsNullOrEmpty(Condition))
                {
                    Condition = $@"WHERE CAM_TYPECAM = '{radDropDown_Area.SelectedValue}' ";
                }
                else
                {
                    Condition = $@" {Condition} AND CAM_TYPECAM = '{radDropDown_Area.SelectedValue}' ";
                }
            }

            //return ManageAndCameraClass.Getdatacheck(Condition, radDateTimePicker_Check.Value.ToString("yyyy-MM-dd"), _pUserpermis);
            return Models.ManageCameraClass.Getdatacheck(Condition, radDateTimePicker_Check.Value.ToString("yyyy-MM-dd"), _pUserpermis);

            //string sql = $@" SELECT SHOP_JOBCAM.CAM_ID,
            //                SHOP_JOBCAM.CAM_IP,
            //                Shop_JOBCAM.CAM_DESC,
            //                CAM_CH,
            //                SHOP_JOBCAM.BRANCH_ID,
            //                BRANCH_NAME,
            //                CAM_AMORN,
            //                CAM_TYPECAM,
            //                CASE WHEN SHOP_JOBCAM.BRANCH_ID = 'MN000' THEN CAM_DEPT ELSE SHOP_JOBCAM.BRANCH_ID END AS CAM_DEPT,
            //                CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN [DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
            //                CHECK_EMPNICKNAME,
            //                CHECK_EMPID,
            //                CHECK_EMPNAME,
            //                CHECK_DATETIME,
            //                CAM_RECORD,
            //                CONVERT(VARCHAR,CAM_DATERECORD,23) AS CAM_DATERECORD,
            //                ISNULL(JOB_Number,'') AS CAM_CHECKJOBID,
            //                CONVERT(VARCHAR,JOB_DATEINS,23) AS JOB_DATEINS,
            //                CASE WHEN CHECK_EMPID != '' THEN '1' ELSE '0' END AS CAM_CHECK,'{0}' AS CHECK_USERTYPE,
            //                CHECK_REMARK,
            //                CAM_TECHNICID,
            //                CAM_TECHNICNAME,
            //                SHOP_JOBCAM.CAM_DVR,
            //        [CHECK_STATION] as SHOW_ID,[COUNT],PATH_IMAGE,'' AS CAM_REMARKCHANNEL
            //        ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
            //FROM    SHOP_JOBCAM WITH (NOLOCK) 
            //        LEFT OUTER JOIN  SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
            //        LEFT OUTER JOIN  
            //               ( 
            //               select ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) as Seq ,*
            //               from SHOP_JOBComMinimart where JOB_STACLOSE='0' and  JOB_GROUPSUB='00003' 
            //                )JOB   
            //                ON JOB.JOB_SN = SHOP_JOBCAM.CAM_IP AND JOB_STACLOSE = '0'  AND Seq='1'
            //        LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM  AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = 'SPC'
            //        LEFT OUTER JOIN  
            //                 ( 
            //                SELECT *,CONVERT(VARCHAR,CHECK_DATEINS,25) AS CHECK_DATETIME 
            //                FROM SHOP_JOBCAMCHECK  WITH (NOLOCK) 
            //                WHERE CAM_DATECHECK = '{radDateTimePicker_Check.Value:yyyy-MM-dd}' )TMP ON SHOP_JOBCAM.CAM_ID = TMP.CAM_ID AND CHECK_USERTYPE = '{_pUserpermis}' 
            //                AND [CHECK_STATION] = '' 
            //        LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
            //       ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
            //{Condition} ORDER BY CAM_DESC";

            //return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetCamStation()
        {
            //       string sql = $@"
            //       SELECT SHOP_JOBCAM.CAM_ID,SHOP_JOBCAM.CAM_IP,SHOP_JOBCAM.CAM_DESC,
            //                       CAM_CH,SHOP_JOBCAM.BRANCH_ID,BRANCH_NAME,CAM_AMORN,CAM_TYPECAM,
            //                       CASE WHEN SHOP_JOBCAM.BRANCH_ID = 'MN000' THEN CAM_DEPT ELSE SHOP_JOBCAM.BRANCH_ID END AS CAM_DEPT,
            //                       CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN [DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
            //                       CHECK_EMPNICKNAME,CHECK_EMPID,CHECK_EMPNAME,CHECK_DATETIME,CAM_RECORD,
            //                       CONVERT(VARCHAR,CAM_DATERECORD,23) AS CAM_DATERECORD,ISNULL(JOB_Number,'') AS CAM_CHECKJOBID,
            //                       CONVERT(VARCHAR,JOB_DATEINS,23) AS JOB_DATEINS,
            //                       CASE WHEN CHECK_EMPID != '' THEN '1' ELSE '0' END AS CAM_CHECK,'1' AS CHECK_USERTYPE,
            //                       CHECK_REMARK,CAM_TECHNICID,CAM_TECHNICNAME,
            //                       SHOP_JOBCAM.CAM_DVR,SHOW_ID,ISNULL([CHANNEL] ,'') AS CAM_REMARKCHANNEL,[COUNT],[PATH_IMAGE]
            //                ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
            //       FROM    SHOP_JOBCAM WITH (NOLOCK) 
            //               LEFT OUTER JOIN  SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
            //INNER JOIN  [SHOP_JOBCAMSTATION] WITH (NOLOCK) ON SHOP_JOBCAM.CAM_ID = [SHOP_JOBCAMSTATION].CAM_ID
            //               LEFT OUTER JOIN  
            //                      ( 
            //                      select ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) as Seq ,*
            //                      from SHOP_JOBComMinimart where JOB_STACLOSE='0' and  JOB_GROUPSUB='00003' 
            //                       )JOB    ON JOB.JOB_SN = SHOP_JOBCAM.CAM_IP  AND JOB_STACLOSE = '0'  AND Seq='1'
            //               LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM  
            //                               AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = 'SPC'
            //               LEFT OUTER JOIN  
            //                        ( 
            //                       SELECT *,CONVERT(VARCHAR,CHECK_DATEINS,25) AS CHECK_DATETIME 
            //                       FROM SHOP_JOBCAMCHECK  WITH (NOLOCK) 
            //                       WHERE CAM_DATECHECK = '{radDateTimePicker_Check.Value:yyyy-MM-dd}' AND [CHECK_STATION] = '{ radDropDown_Cam.SelectedValue}' AND [COUNT] = '{int.Parse(radDomainUpDown_Round.Text.ToString())}'

            //		)TMP ON SHOP_JOBCAM.CAM_ID = TMP.CAM_ID 
            //		AND CHECK_USERTYPE = '1' AND SHOW_ID = '{ radDropDown_Cam.SelectedValue}' AND [COUNT] = '{int.Parse(radDomainUpDown_Round.Text.ToString())}'
            //               LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
            //              ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR

            //       WHERE CAM_TYPECAM = 'MN'  
            //       ORDER BY CAM_DESC  ";

            //return ConnectionClass.SelectSQL_Main(sql);

            //return ManageAndCameraClass.GetCamStation(radDateTimePicker_Check.Value.ToString("yyyy-MM-dd"), radDropDown_Cam.SelectedValue.ToString(), _pUserpermis, int.Parse(radDomainUpDown_Round.Text.ToString()));
            return Models.ManageCameraClass.GetCamStation(radDateTimePicker_Check.Value.ToString("yyyy-MM-dd"), radDropDown_Cam.SelectedValue.ToString(), _pUserpermis, int.Parse(radDomainUpDown_Round.Text.ToString()));
        }
        private string GetSafeString(object value)
        {
            if (value == null) return string.Empty; else return value.ToString();
        }
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            radGridView_Show.FilterDescriptors.Clear();
            if (this.radDateTimePicker_Check.Value.ToString("yyyy-MM-dd") != Selectdate)
            {
                Selectdate = this.radDateTimePicker_Check.Value.ToString("yyyy-MM-dd");
                dt = this.Getdatacheck();
                dtStation = this.GetCamStation();
            }
            //ประจำจุด
            if (radDropDown_Cam.SelectedIndex > 0)
            {
                checkDataActive = 1;
                dtStation = this.GetCamStation();
                radGridView_Show.DataSource = dtStation;
                for (int i = 0; i < radGridView_Show.Rows.Count; i++)
                {
                    if (dtStation.Rows[i]["PATH_IMAGE"].ToString() != "")
                    {
                        radGridView_Show.Rows[i].Height = 150;
                        radGridView_Show.Rows[i].Cells["IMAGE"].Value = Image.FromFile(dtStation.Rows[i]["PATH_IMAGE"].ToString());
                    }
                }

                GridViewfilter(radGridView_Show.Columns["SHOW_ID"], radDropDown_Cam.SelectedValue.ToString());
            }
            else
            {
                checkDataActive = 0;
                dt = this.Getdatacheck();
                radGridView_Show.DataSource = dt;
            }

            //'MN','SPC','RET'
            if (radDropDown_Area.SelectedIndex > 0)
            {
                GridViewfilter(radGridView_Show.Columns["CAM_TYPECAM"], radDropDown_Area.SelectedValue.ToString());
            }
            else
            {
                GridViewfilter(radGridView_Show.Columns["CAM_TYPECAM"], "");
            }

            GridViewfilter(radGridView_Show.Columns["CAM_CHECK"], CheckStatus());
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (this.radGridView_Show.CurrentRow != null && !(this.radGridView_Show.CurrentRow is GridViewNewRowInfo))
            {
                try
                {
                    if (GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_AMORN"].Value) == "1")
                    {
                        if (MessageBox.Show("ยืนยันการเข้าดู " + GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value) + " " + GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value)
                          , SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Process.Start("iexplore.exe", GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value));
                        }
                    }
                    else
                    {
                        switch (e.Column.Name)
                        {
                            case "CAM_DESC":
                                try
                                {
                                    System.Net.NetworkInformation.Ping myPing = new System.Net.NetworkInformation.Ping();
                                    System.Net.NetworkInformation.PingReply reply = myPing.Send(GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString()).Replace("http://", "").Replace("/", ""), 1000);
                                    if (reply.Status == System.Net.NetworkInformation.IPStatus.Success)
                                    {
                                        rRows = dt.Select("CAM_ID = '" + this.radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString() + "' ");
                                        iRows = dt.Rows.IndexOf(rRows[0]);

                                        string pPath = ""; string pFileName = ""; //string PathI = @"\\192.168.100.60\ImageMinimark\CAMERA\";
                                        string _JOB_DATEINS = this.radGridView_Show.CurrentRow.Cells["JOB_DATEINS"].Value.ToString();
                                        string _CAM_CHECKJOBID = this.radGridView_Show.CurrentRow.Cells["CAM_CHECKJOBID"].Value.ToString();

                                        if (_CAM_CHECKJOBID != "")
                                        {
                                            pPath = PathImageClass.pPathJOB + _JOB_DATEINS.Substring(0, 7) +
                                            @"\" + _JOB_DATEINS + @"\" + _CAM_CHECKJOBID + @"\";

                                            pFileName = _CAM_CHECKJOBID + "-" +
                                            this.radGridView_Show.CurrentRow.Cells["CAM_DEPT"].Value.ToString() + "-";
                                        }
                                        else
                                        {
                                            pPath = PathImageClass.pPathCAMERA + radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                                            pFileName = string.Format(@"\{0}-{1}-{2}.jpg"
                                                , radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString()
                                                , radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString()
                                                , radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString().Replace("http://", "").Replace(".", "_").Replace("/", "").Replace(":81", "").Replace(":82", ""));
                                        }

                                        dATACAM_IP.IPCam = radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString();
                                        dATACAM_IP.Userpermis = _pUserpermis;
                                        dATACAM_IP.CamID = GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_ID"].Value);
                                        dATACAM_IP.FormName = this.Name;
                                        dATACAM_IP.JobNumber = GetSafeString(_CAM_CHECKJOBID);
                                        //dATACAM_IP.Path = pPath;

                                        //dATACAM_IP.FileName = pFileName;
                                        dATACAM_IP.BranchID = radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                                        dATACAM_IP.CamDesc = radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value.ToString();
                                        dATACAM_IP.BranchName = radGridView_Show.CurrentRow.Cells["DESCRIPTION"].Value.ToString();
                                        dATACAM_IP.CamDVR = radGridView_Show.CurrentRow.Cells["CAM_DVR"].Value.ToString();
                                        dATACAM_IP.CamStation = radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString();
                                        dATACAM_IP.CamCount = int.Parse(radDomainUpDown_Round.Text.ToString());
                                        dATACAM_IP.CamDate = radDateTimePicker_Check.Value.ToString("yyyy-MM-dd");
                                        dATACAM_IP.CamCH = int.Parse(radGridView_Show.CurrentRow.Cells["CAM_CH"].Value.ToString());

                                        //Camera_IE frm = new Camera_IE(dATACAM_IP, "0");
                                        Camera_IEMain frm = new Camera_IEMain(dATACAM_IP, "0",
                                                radGridView_Show.CurrentRow.Cells["SET_USER"].Value.ToString(),
                                                radGridView_Show.CurrentRow.Cells["SET_PASSWORD"].Value.ToString())
                                        {
                                            pCAM_REMARKCHANNEL = radGridView_Show.CurrentRow.Cells["CAM_REMARKCHANNEL"].Value.ToString(),
                                            ptypeCamOpen = "ตรวจกล้องประจำวัน"
                                        };
                                        frm.Show();
                                    }
                                    else
                                    {
                                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"DVR มีปัญหา Ping ไม่เจอ{Environment.NewLine}ลองใหม่อีกครั้ง");
                                        return;
                                        //Process.Start("iexplore.exe", GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Error(ex.Message);
                                    Process.Start("iexplore.exe", GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value));
                                }

                                break;
                            case "IMAGE":
                                if (GetSafeString(this.radGridView_Show.CurrentRow.Cells["PATH_IMAGE"].Value) == "") return;
                                else Process.Start(GetSafeString(this.radGridView_Show.CurrentRow.Cells["PATH_IMAGE"].Value));

                                break;
                            default:
                                Process.Start("iexplore.exe", GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value));
                                break;
                        }
                    }
                }
                catch (Exception)
                {
                    Process.Start("iexplore.exe", GetSafeString(this.radGridView_Show.CurrentRow.Cells["CAM_IP"].Value));
                }
            }
        }
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.ColumnIndex == radGridView_Show.MasterTemplate.Columns["CAM_CHECK"].Index && e.RowIndex > -1)
            {
                Camera_CheckDetail frmCheckDetail = new Camera_CheckDetail(_pUserpermis
                    , radDateTimePicker_Check.Value.ToString("yyyy-MM-dd")
                    , this.radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString()
                    , this.radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString()
                    , int.Parse(radDomainUpDown_Round.Text.ToString()));

                if (frmCheckDetail.ShowDialog(this) == DialogResult.OK)
                {

                    if (checkDataActive == 0)
                    {
                        dt = this.Getdatacheck();
                        radGridView_Show.DataSource = dt;
                    }
                    else
                    {
                        dtStation = this.GetCamStation();
                        radGridView_Show.DataSource = dtStation;
                    }
                }
            }
        }
        private void GridViewfilter(GridViewDataColumn ColName, string Value)
        {
            FilterDescriptor filter = new FilterDescriptor(ColName.Name, FilterOperator.Contains, Value);
            ColName.FilterDescriptor = filter;
        }
        private string CheckStatus()
        {
            if (radCheckBox_Check.Checked == true) return "0"; else return "";
        }
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            else
            {
                string returnExportStatus = DatagridClass.ExportExcelGridView("ตรวจกล้องประจำวัน", radGridView_Show, "1");
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExportStatus);
            }
        }
        private void RadDropDown_Cam_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (radDropDown_Cam.SelectedIndex > 0)
            {
                radLabel_Round.Enabled = true;
                radDomainUpDown_Round.Enabled = true;

                this.CreateBindingList(Checkcount(radDropDown_Cam.SelectedValue.ToString()));
                this.radDomainUpDown_Round.DataSource = items;
            }
            else
            {
                radLabel_Round.Enabled = false;
                radDomainUpDown_Round.Enabled = false;
                radDomainUpDown_Round.Text = "0";
            }
        }
        private int Checkcount(string pStation)
        {
            string sql = string.Format(@"SELECT  ISNULL([MaxImage],0) AS [MaxImage] 
                                  FROM SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                                  WHERE [TYPE_CONFIG] = '21' and [SHOW_ID] = '{0}'", pStation);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return int.Parse(dt.Rows[0][0].ToString());
        }


    }
}