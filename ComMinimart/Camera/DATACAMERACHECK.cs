﻿//CheckOK
using System.Data;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    //public partial class DATACAMERACHECK
    //{
    //    public string CAMID { get; set; }
    //    public string CAMIP { get; set; }
    //    public string CAMDESC { get; set; }
    //    public string CAMCH { get; set; }
    //    public string CAMDEPT { get; set; }
    //    public string CAMDEPTNAME { get; set; }
    //    public int CAMRECODR { get; set; }
    //    public string CAMDATRECODR { get; set; }
    //    public string CAMJOBID { get; set; }
    //    public string CAMREMARK { get; set; }
    //    public string CAMCHECKSTATUS { get; set; }
    //    public string CAMCHECKEMP { get; set; }
    //    public string CAMCHECKEMPSHORT { get; set; }
    //    public string CAMCHECKDATETIME { get; set; }
    //}
    public class DATACAMERADETAIL
    {
        private readonly string _CAMID = "CAM_ID";
        public string CAMID { get; set; }
        private readonly string _CAMIP = "CAM_IP";
        public string CAMIP { get; set; }
        private readonly string _CAMDESC = "CAM_DESC";
        public string CAMDESC { get; set; }
        private readonly string _CAMCH = "CAM_CH";
        public int CAMCH { get; set; }
        private readonly string _CAMDEPT = "CAM_DEPT";
        public string CAMDEPT { get; set; }
        private readonly string _CAMDEPTNAME = "DESCRIPTION";
        public string CAMDEPTNAME { get; set; }
        private readonly string _CAMRECODR = "CAM_RECORD";
        public int CAMRECODR { get; set; }
        private readonly string _CAMDATRECODR = "CAM_DATERECORD";
        public string CAMDATRECODR { get; set; }
        private readonly string _CAMJOBID = "CAM_CHECKJOBID";
        public string CAMJOBID { get; set; } = "";
        public string CAMREMARK { get; set; }
        private readonly string _CAMCHECKSTATUS = "CAM_CHECK";
        public string CAMCHECKSTATUS { get; set; }
        private readonly string _CAMCHECKEMP = "CHECK_EMPID";
        public string CAMCHECKEMP { get; set; }
        private readonly string _CAMCHECKEMPSHORT = "CHECK_EMPNICKNAME";
        public string CAMCHECKEMPSHORT { get; set; } = "";
        private readonly string _CAMCHECKDATETIME = "CHECK_DATETIME";
        public string CAMCHECKDATETIME { get; set; }
        public string CAMAMORN { get; set; } = "0";
        private readonly string _CAMTECHNICID = "CAM_TECHNICID";
        public string CAMTECHNICID { get; set; }
        private readonly string _CAMTECHNICINAME = "CAM_TECHNICNAME";
        public string CAMTECHNICINAME { get; set; }
        private readonly string _CAMTYPECAM = "CAM_TYPECAM";
        public string CAMTYPECAM { get; set; }
        public string CAMFREEZ { get; set; } = "0";
        public string CAMUP { get; set; } = "0";
        public string CAMDOWN { get; set; } = "0";
        private readonly string _CAMCHECKREMARK = "CHECK_REMARK";
        public string CAMCHECKREMARK { get; set; }
        private readonly string _CAMJOBDATE = "JOB_DATEINS";
        public string CAMJOBDATE { get; set; }
        private readonly string _CAMREMARKCHANNEL = "CAM_REMARKCHANNEL";
        public string CAMREMARKCHANNEL { get; set; }
        public DATACAMERADETAIL(string pUserpermis, string pDateCheck, string pCamID, string pStation, int pCount)
        {
            string sql;
            if (pCamID != null && pStation != "")
            {
                sql = string.Format(@"SELECT SHOP_JOBCAM.CAM_ID,
                            SHOP_JOBCAM.CAM_IP,Shop_JOBCAM.CAM_DESC,
                            CAM_CH,SHOP_JOBCAM.BRANCH_ID,
                            BRANCH_NAME,CAM_AMORN,CAM_TYPECAM,
                            CASE WHEN SHOP_JOBCAM.BRANCH_ID = 'MN000' THEN CAM_DEPT ELSE SHOP_JOBCAM.BRANCH_ID END AS CAM_DEPT,
                            CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN[DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS[DESCRIPTION],
                            CHECK_EMPNICKNAME,CHECK_EMPID,CHECK_EMPNAME,CHECK_DATETIME,CAM_RECORD,
                            CONVERT(VARCHAR, CAM_DATERECORD, 23) AS CAM_DATERECORD,
                            ISNULL(JOB_Number, '') AS CAM_CHECKJOBID,
                            CONVERT(VARCHAR, JOB_DATEINS, 23) AS JOB_DATEINS,
                            CASE WHEN CHECK_EMPID != '' THEN '1' ELSE '0' END AS CAM_CHECK,'1' AS CHECK_USERTYPE,
                            CHECK_REMARK,
                            CAM_TECHNICID,
                            CAM_TECHNICNAME,
                            CAM_DVR,
                            [CHECK_STATION] as SHOW_ID,
                            [COUNT],PATH_IMAGE,
							[CHANNEL] AS CAM_REMARKCHANNEL
                            FROM SHOP_JOBCAM WITH(NOLOCK)
                            LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                            INNER JOIN [SHOP_JOBCAMSTATION] WITH (NOLOCK) ON SHOP_JOBCAM.CAM_ID = [SHOP_JOBCAMSTATION].CAM_ID
                            LEFT OUTER JOIN
                                  (
                                  select ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) as Seq ,*
			                        from SHOP_JOBComMinimart where JOB_STACLOSE = '0' and JOB_GROUPSUB = '00003' 
			                         )JOB
                                        ON JOB.JOB_SN = SHOP_JOBCAM.CAM_IP
                                        AND JOB_STACLOSE = '0'  AND Seq = '1'
                            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM
                            LEFT OUTER JOIN
                             (
                            SELECT*, CONVERT(VARCHAR, CHECK_DATEINS,25) AS CHECK_DATETIME
                            FROM SHOP_JOBCAMCHECK  WITH(NOLOCK)
                            WHERE CAM_DATECHECK = '{3}' )TMP ON SHOP_JOBCAM.CAM_ID = TMP.CAM_ID
                            AND CHECK_USERTYPE = '{4}'  
                            AND [CHECK_STATION] = '{1}' 
							and [COUNT] = '{2}'
                            WHERE SHOP_JOBCAM.CAM_ID = '{0}'  
							ORDER BY CAM_DESC ", pCamID, pStation, pCount, pDateCheck, pUserpermis);
            }
            else
            {
                sql = string.Format(@"SELECT SHOP_JOBCAM.CAM_ID,
                            SHOP_JOBCAM.CAM_IP,
                            Shop_JOBCAM.CAM_DESC,
                            CAM_CH,
                            SHOP_JOBCAM.BRANCH_ID,
                            BRANCH_NAME,
                            CAM_AMORN,
                            CAM_TYPECAM,
                            CASE WHEN SHOP_JOBCAM.BRANCH_ID = 'MN000' THEN CAM_DEPT ELSE SHOP_JOBCAM.BRANCH_ID END AS CAM_DEPT,
                            CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN [DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
                            CHECK_EMPNICKNAME,
                            CHECK_EMPID,
                            CHECK_EMPNAME,
                            CHECK_DATETIME,
                            CAM_RECORD,
                            CONVERT(VARCHAR,CAM_DATERECORD,23) AS CAM_DATERECORD,
                            ISNULL(JOB_Number,'') AS CAM_CHECKJOBID,
                            CONVERT(VARCHAR,JOB_DATEINS,23) AS JOB_DATEINS,
                            CASE WHEN CHECK_EMPID != '' THEN '1' ELSE '0' END AS CAM_CHECK,'1' AS CHECK_USERTYPE,
                            CHECK_REMARK,
                            CAM_TECHNICID,
                            CAM_TECHNICNAME,
                            CAM_DVR,
                            [CHECK_STATION] as SHOW_ID,
							[COUNT],PATH_IMAGE,'' AS CAM_REMARKCHANNEL
                            FROM SHOP_JOBCAM WITH (NOLOCK) 
                            LEFT OUTER JOIN  SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                               LEFT OUTER JOIN  
			                        ( 
			                        select ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) as Seq ,*
			                        from SHOP_JOBComMinimart where JOB_STACLOSE='0' and  JOB_GROUPSUB='00003' 
			                         )JOB   
				                        ON JOB.JOB_SN = SHOP_JOBCAM.CAM_IP 
				                        AND JOB_STACLOSE = '0'  AND Seq='1'
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM  
                            LEFT OUTER JOIN  
                             ( 
                            SELECT *,CONVERT(VARCHAR,CHECK_DATEINS,25) AS CHECK_DATETIME 
                            FROM SHOP_JOBCAMCHECK  WITH (NOLOCK) 
                            WHERE CAM_DATECHECK = '{1}' )TMP ON SHOP_JOBCAM.CAM_ID = TMP.CAM_ID 
							AND CHECK_USERTYPE = '{2}' 
                            AND [CHECK_STATION] = '' 
                            WHERE SHOP_JOBCAM.CAM_ID = '{0}'  
							ORDER BY CAM_DESC", pCamID, pDateCheck, pUserpermis);
            }

            DataTable dtDetail = Controllers.ConnectionClass.SelectSQL_Main(sql);
            if (dtDetail.Rows.Count > 0)
            {
                CAMID = dtDetail.Rows[0][_CAMID].ToString();
                CAMIP = dtDetail.Rows[0][_CAMIP].ToString();
                CAMDESC = dtDetail.Rows[0][_CAMDESC].ToString();
                CAMCH = int.Parse(dtDetail.Rows[0][_CAMCH].ToString());
                CAMRECODR = int.Parse(dtDetail.Rows[0][_CAMRECODR].ToString());
                CAMDATRECODR = dtDetail.Rows[0][_CAMDATRECODR].ToString();
                CAMDEPT = dtDetail.Rows[0][_CAMDEPT].ToString();
                CAMDEPTNAME = dtDetail.Rows[0][_CAMDEPTNAME].ToString();
                CAMTYPECAM = dtDetail.Rows[0][_CAMTYPECAM].ToString();
                CAMJOBID = dtDetail.Rows[0][_CAMJOBID].ToString();
                CAMJOBDATE = dtDetail.Rows[0][_CAMJOBDATE].ToString();
                CAMCHECKSTATUS = dtDetail.Rows[0][_CAMCHECKSTATUS].ToString();
                CAMCHECKEMP = dtDetail.Rows[0][_CAMCHECKEMP].ToString();
                CAMCHECKEMPSHORT = dtDetail.Rows[0][_CAMCHECKEMPSHORT].ToString();
                CAMCHECKDATETIME = dtDetail.Rows[0][_CAMCHECKDATETIME].ToString();
                CAMCHECKREMARK = dtDetail.Rows[0][_CAMCHECKREMARK].ToString();
                CAMTECHNICID = dtDetail.Rows[0][_CAMTECHNICID].ToString();
                CAMTECHNICINAME = dtDetail.Rows[0][_CAMTECHNICINAME].ToString();
                CAMREMARKCHANNEL = dtDetail.Rows[0][_CAMREMARKCHANNEL].ToString();
            }

        }
    }
    public class DATACAMERA
    {
        public string CAMID { get; set; }
        public string CAMIP { get; set; }
        public string CAMDESC { get; set; }
        public int CAMCH { get; set; }
        public int CAMRECODR { get; set; }
        public string CAMAMORN { get; set; } = "0";
        public string CAMTECHNICID { get; set; }
        public string CAMTECHNICINAME { get; set; }
        public string CAMDEPT { get; set; }
        public string CAMTYPECAM { get; set; }
        public string CAMFREEZ { get; set; } = "0";
        public string CAMUP { get; set; } = "0";
        public string CAMDOWN { get; set; } = "0";
        public string CAMREMARK { get; set; }
        public string CAM_MYANMAR { get; set; } = "";
        public string SET_USER { get; set; } = "";
        public string SET_PASSWORD { get; set; } = "";
    }
    public class DATACAM_IP
    {
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public string CamDesc { get; set; }
        public string IPCam { get; set; }
        public string Userpermis { get; set; }
        public string Type { get; set; }
        public string CamID { get; set; }
        public string FormName { get; set; }
        public string JobNumber { get; set; }
        public string TypeCam { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string CamDVR { get; set; }
        public string CamStation { get; set; }
        //public string CamTypeComservice { get; set; }
        public int CamCount { get; set; }
        public string CamDate { get; set; }
        public int CamCH { get; set; } //จำนวนกล้องทั้งหมดใน DVR
    }
    public enum CollectionControl
    {
        RadLabel = 0,
        RadTextBoxBase = 1,
        RadCheckBox = 2,
        RadRadioButton = 3,
        RadDropDownList = 4
    }
    //public enum UserPermission
    //{
    //    Admin,      //or comser 0
    //    Center,     //1
    //    Security,   //2
    //    General,    //3
    //    Koh         //4
    //}
    //public class UserPerson
    //{
    //    public string[] EmpID = { "1604001", "1908021", "1911143", "2008014", "2101008", "2102065" };
    //}
}
