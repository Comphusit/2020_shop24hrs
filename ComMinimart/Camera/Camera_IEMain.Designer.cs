﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{ 
    public partial class Camera_IEMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_IEMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_live = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_loginAdmin = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_loginPhusit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Link = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_Check = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Cap = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_CapJOB = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator8 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator9 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.commandBarSeparator10 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Reboot = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator11 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_LOGOUT = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator12 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator13 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.object_c8561900_4434_4397_870f_4df1521b5ad4 = new Telerik.WinControls.RootRadElement();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.webBrowser1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(3, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(889, 616);
            this.panel1.TabIndex = 5;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(887, 614);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.Visible = false;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_live,
            this.commandBarSeparator6,
            this.radButtonElement_loginAdmin,
            this.commandBarSeparator1,
            this.radButtonElement_loginPhusit,
            this.commandBarSeparator3,
            this.radButtonElement_Link,
            this.commandBarSeparator2,
            this.RadButtonElement_Check,
            this.commandBarSeparator4,
            this.radButtonElement_Cap,
            this.commandBarSeparator5,
            this.radButtonElement_CapJOB,
            this.commandBarSeparator7,
            this.radButtonElement_Edit,
            this.commandBarSeparator8,
            this.commandBarSeparator9,
            this.commandBarSeparator10,
            this.radButtonElement_Reboot,
            this.commandBarSeparator11,
            this.radButtonElement_LOGOUT,
            this.commandBarSeparator12,
            this.RadButtonElement_pdt,
            this.commandBarSeparator13});
            this.radStatusStrip1.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(889, 34);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_live
            // 
            this.radButtonElement_live.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_live.Image = global::PC_Shop24Hrs.Properties.Resources.live;
            this.radButtonElement_live.Name = "radButtonElement_live";
            this.radStatusStrip1.SetSpring(this.radButtonElement_live, false);
            this.radButtonElement_live.Text = "radButtonElement1";
            this.radButtonElement_live.Click += new System.EventHandler(this.RadButtonElement_live_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_loginAdmin
            // 
            this.radButtonElement_loginAdmin.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_loginAdmin.Image = global::PC_Shop24Hrs.Properties.Resources.add_user;
            this.radButtonElement_loginAdmin.Name = "radButtonElement_loginAdmin";
            this.radStatusStrip1.SetSpring(this.radButtonElement_loginAdmin, false);
            this.radButtonElement_loginAdmin.Text = "radButtonElement1";
            this.radButtonElement_loginAdmin.ToolTipText = "เพิ่มเอกสาร";
            this.radButtonElement_loginAdmin.Click += new System.EventHandler(this.RadButtonElement_loginAdmin_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_loginPhusit
            // 
            this.radButtonElement_loginPhusit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_loginPhusit.Image = global::PC_Shop24Hrs.Properties.Resources.contacts;
            this.radButtonElement_loginPhusit.Name = "radButtonElement_loginPhusit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_loginPhusit, false);
            this.radButtonElement_loginPhusit.Text = "radButtonElement1";
            this.radButtonElement_loginPhusit.ToolTipText = "Export To Excel";
            this.radButtonElement_loginPhusit.Click += new System.EventHandler(this.RadButtonElement_loginPhusit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Link
            // 
            this.radButtonElement_Link.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Link.Image = global::PC_Shop24Hrs.Properties.Resources.link;
            this.radButtonElement_Link.Name = "radButtonElement_Link";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Link, false);
            this.radButtonElement_Link.Text = "radButtonElement1";
            this.radButtonElement_Link.Click += new System.EventHandler(this.RadButtonElement_Link_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_Check
            // 
            this.RadButtonElement_Check.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_Check.Image = global::PC_Shop24Hrs.Properties.Resources.job;
            this.RadButtonElement_Check.Name = "RadButtonElement_Check";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_Check, false);
            this.RadButtonElement_Check.Text = "radButtonElement1";
            this.RadButtonElement_Check.ToolTipText = "ค้นหาเอกสาร";
            this.RadButtonElement_Check.Click += new System.EventHandler(this.RadButtonElement_Check_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Cap
            // 
            this.radButtonElement_Cap.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Cap.Image = global::PC_Shop24Hrs.Properties.Resources.settings;
            this.radButtonElement_Cap.Name = "radButtonElement_Cap";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Cap, false);
            this.radButtonElement_Cap.Text = "radButtonElement1";
            this.radButtonElement_Cap.Click += new System.EventHandler(this.RadButtonElement_Cap_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_CapJOB
            // 
            this.radButtonElement_CapJOB.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_CapJOB.Image = global::PC_Shop24Hrs.Properties.Resources.screenshot;
            this.radButtonElement_CapJOB.Name = "radButtonElement_CapJOB";
            this.radStatusStrip1.SetSpring(this.radButtonElement_CapJOB, false);
            this.radButtonElement_CapJOB.Text = "radButtonElement1";
            this.radButtonElement_CapJOB.Click += new System.EventHandler(this.RadButtonElement_CapJOB_Click);
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator7, false);
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement1";
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator8
            // 
            this.commandBarSeparator8.Name = "commandBarSeparator8";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator8, false);
            this.commandBarSeparator8.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator9
            // 
            this.commandBarSeparator9.Name = "commandBarSeparator9";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator9, false);
            this.commandBarSeparator9.VisibleInOverflowMenu = false;
            // 
            // commandBarSeparator10
            // 
            this.commandBarSeparator10.Name = "commandBarSeparator10";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator10, false);
            this.commandBarSeparator10.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Reboot
            // 
            this.radButtonElement_Reboot.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Reboot.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radButtonElement_Reboot.Image = global::PC_Shop24Hrs.Properties.Resources.reboot;
            this.radButtonElement_Reboot.Name = "radButtonElement_Reboot";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Reboot, false);
            this.radButtonElement_Reboot.Text = "Reboot";
            this.radButtonElement_Reboot.Click += new System.EventHandler(this.RadButtonElement_Reboot_Click);
            // 
            // commandBarSeparator11
            // 
            this.commandBarSeparator11.Name = "commandBarSeparator11";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator11, false);
            this.commandBarSeparator11.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_LOGOUT
            // 
            this.radButtonElement_LOGOUT.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_LOGOUT.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButtonElement_LOGOUT.Image = global::PC_Shop24Hrs.Properties.Resources.logout;
            this.radButtonElement_LOGOUT.Name = "radButtonElement_LOGOUT";
            this.radStatusStrip1.SetSpring(this.radButtonElement_LOGOUT, false);
            this.radButtonElement_LOGOUT.Text = "LOGOUT";
            this.radButtonElement_LOGOUT.Click += new System.EventHandler(this.RadButtonElement_LOGOUT_Click);
            // 
            // commandBarSeparator12
            // 
            this.commandBarSeparator12.Name = "commandBarSeparator12";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator12, false);
            this.commandBarSeparator12.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator13
            // 
            this.commandBarSeparator13.Name = "commandBarSeparator13";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator13, false);
            this.commandBarSeparator13.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_F2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radStatusStrip1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(895, 695);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 51);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(889, 19);
            this.radLabel_F2.TabIndex = 53;
            // 
            // object_c8561900_4434_4397_870f_4df1521b5ad4
            // 
            this.object_c8561900_4434_4397_870f_4df1521b5ad4.Name = "object_c8561900_4434_4397_870f_4df1521b5ad4";
            this.object_c8561900_4434_4397_870f_4df1521b5ad4.StretchHorizontally = true;
            this.object_c8561900_4434_4397_870f_4df1521b5ad4.StretchVertically = true;
            // 
            // Camera_IEMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 695);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Camera_IEMain";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "กล้องวงจรปิด";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Camera_IE_FormClosing);
            this.Load += new System.EventHandler(this.Camera_IEMain_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_loginAdmin;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_Check;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_loginPhusit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Link;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Cap;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_live;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_CapJOB;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator8;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator9;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Reboot;
        private Telerik.WinControls.RootRadElement object_c8561900_4434_4397_870f_4df1521b5ad4;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_LOGOUT;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator12;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator13;
    }
}
 