﻿using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_Permission : Form
    {
        string Updatestatus = "0";
        DataTable dt = new DataTable();
        readonly string _pCAMID;
        public Camera_Permission(string pCAMID)
        {
            InitializeComponent();
            this.KeyPreview = true;
            this.Text = "สิทธิ์ดูกล้องวงจรปิด";
            _pCAMID = pCAMID;

        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void Camera_Permission_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_ID", "CAMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_ID", "รหัส", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERNAME", "ชื่อพนักงาน", 350));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERDVR", "USER", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PASSDVR", "PASSWORD", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTCODE", "แผนก", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "ชื่อแผนก", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 150));

            ////Freeze Column
            this.radGridView_Show.MasterTemplate.Columns["CAM_ID"].IsPinned = true;
            this.radGridView_Show.MasterTemplate.Columns["EMP_ID"].IsPinned = true;

            DatagridClass.SetTextControls(radPanel_PermissInfor.Controls, (int)(CollectionControl.RadTextBoxBase));
            radLabel_EmpName.Font = SystemClass.SetFontGernaral_Bold;
            radLabel_EmpName.ForeColor = Class.ConfigClass.SetColor_Blue();
            radLabel_DptName.Font = SystemClass.SetFontGernaral_Bold;
            radLabel_DptName.ForeColor = Class.ConfigClass.SetColor_Blue();
            //diabel
            radStatusStrip.SizingGrip = false;

            radButton_Delet.ShowBorder = true; radButton_Delet.ToolTipText = "ลบผู้ใช้";
            radButton_Edit.ShowBorder = true; radButton_Edit.ToolTipText = "แก้ไขผู้ใช้";
            radButton_AddUser.ShowBorder = true; radButton_AddUser.ToolTipText = "เพิ่มผู้ใช้";

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radTextBox_Spc.Enabled = false;

            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            SetDefaultPanal();
            FinddataPermission();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        //ROWS NUMBERS
        #region "ROWSNUMBERS"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        private void RadGridView_Show_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            SetDefaultPanal();
        }
        //Emp_KeyDown
        private void RadTextBox_Emp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(radTextBox_Emp.Text))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสพนักงาน...");
                    radTextBox_Emp.SelectAll();
                    radTextBox_Emp.Focus();
                    radLabel_EmpName.Text = string.Empty;
                }

                else
                {
                    DataTable dt = CheckUserPermission(radTextBox_Emp.Text.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("มีข้อมูลพนักงาน เรียบร้อยแล้ว ต้องการเพิ่ม SPC เครื่องแต่ผู้ใช้เดิม ?. ") == DialogResult.Yes)
                        {
                            radTextBox_Emp.Text = dt.Rows[0]["EMP_ID"].ToString(); //dt.Rows[0]["ALTNUM"].ToString();   //EMPID
                            radLabel_EmpName.Text = dt.Rows[0]["USERNAME"].ToString();// dt.Rows[0]["SPC_NAME"].ToString();
                            radTextBox_Dpt.Text = dt.Rows[0]["DEPTCODE"].ToString();//dt.Rows[0]["NUM"].ToString();
                            radLabel_DptName.Text = dt.Rows[0]["DEPTNAME"].ToString();//dt.Rows[0]["DESCRIPTION"].ToString();
                            radTextBox_Pass.Focus();
                            radTextBox_User.Text = radTextBox_Dpt.Text;
                            radTextBox_Emp.Enabled = false;
                            radTextBox_User.Enabled = false;
                            radTextBox_Pass.Enabled = true;
                            radTextBox_Spc.Enabled = false;
                            radTextBox_Remark.Enabled = true;
                            radButton_Save.Enabled = true;
                            radButton_Cancel.Enabled = true;
                            radTextBox_Pass.Focus();
                        }
                        else
                        {
                            radTextBox_Emp.Focus();
                            radTextBox_Emp.SelectAll();
                        }
                    }

                    else
                    {
                        dt = Models.EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_Emp.Text.ToString());
                        if (dt.Rows.Count > 0)
                        {
                            radTextBox_Emp.Text = ConfigClass.ReplaceMDPForEmlpID(dt.Rows[0]["EMPLID"].ToString());   //EMPID
                            radLabel_EmpName.Text = dt.Rows[0]["SPC_NAME"].ToString();
                            radTextBox_Dpt.Text = dt.Rows[0]["NUM"].ToString();
                            radLabel_DptName.Text = dt.Rows[0]["DESCRIPTION"].ToString();
                            radTextBox_User.Text = radTextBox_Dpt.Text;
                            radTextBox_Pass.Focus();
                            radTextBox_Emp.Enabled = false;
                            radTextBox_User.Enabled = false;
                            radTextBox_Pass.Enabled = true;
                            radTextBox_Spc.Enabled = false;
                            radTextBox_Remark.Enabled = true;
                            radButton_Save.Enabled = true;
                            radButton_Cancel.Enabled = true;
                            radTextBox_Pass.Focus();
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีข้อมูลพนักงาน กรุณาติดต่อฝ่ายบุคคล");
                            radTextBox_Emp.SelectAll();
                            radTextBox_Emp.Focus();
                        }
                    }
                }
            }
        }
        private void RadButton_AddUser_Click(object sender, EventArgs e)
        {
            Updatestatus = "1";
            SetDefaultPanal();
            radTextBox_Emp.Enabled = true;
            radTextBox_Emp.TextBoxElement.TextBoxItem.HostedControl.Select();
        }
        private void RadButton_Delet_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ต้องการลบข้อมูล ผู้ใช้ {Environment.NewLine}{this.radGridView_Show.CurrentRow.Cells["EMP_ID"].Value} - {this.radGridView_Show.CurrentRow.Cells["USERNAME"].Value} ?") == DialogResult.Yes)
            {
                string sqlIn = $@"DELETE FROM SHOP24HRS.dbo.SHOP_JOBCAM_PERMISSION WHERE CAM_ID = '{_pCAMID}' AND EMP_ID = '{this.radGridView_Show.CurrentRow.Cells["EMP_ID"].Value}' ";
                MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sqlIn));
                FinddataPermission();
            }
            else
            {
                radTextBox_Emp.Focus();
            }
        }
        private void RadButton_Edit_Click(object sender, EventArgs e)
        {
            Updatestatus = "0";
            UpdatePanelInfo(this.radGridView_Show.CurrentRow);
            radTextBox_Pass.Enabled = true;
            radTextBox_Spc.Enabled = false;
            radTextBox_Remark.Enabled = true;
            radButton_Save.Enabled = true;
            radButton_Cancel.Enabled = true;
            radTextBox_Pass.TextBoxElement.TextBoxItem.HostedControl.Select();
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            SetDefaultPanal();
            radTextBox_Emp.Enabled = true;
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Emp.Text) || string.IsNullOrEmpty(radLabel_EmpName.Text) || string.IsNullOrEmpty(radTextBox_Dpt.Text)
               || string.IsNullOrEmpty(radLabel_DptName.Text) || string.IsNullOrEmpty(radTextBox_User.Text) || string.IsNullOrEmpty(radTextBox_Pass.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ผู้ใช้งาน");
                radTextBox_Emp.Focus();
            }
            else
            {
                DataRow[] rows; int iRow = 0;
                if (radGridView_Show.Rows.Count > 0)
                {
                    rows = dt.Select("EMP_ID = '" + this.radGridView_Show.CurrentRow.Cells["EMP_ID"].Value.ToString() + "' ");
                    iRow = dt.Rows.IndexOf(rows[0]);
                }

                string StatusExc = GetExecut();
                MsgBoxClass.MsgBoxShow_SaveStatus(StatusExc);
                if (StatusExc == "")
                {
                    if (Updatestatus == "1")
                    {
                        dt.Rows.Add(_pCAMID,
                            radTextBox_Emp.Text,
                            radLabel_EmpName.Text,
                            radTextBox_User.Text,
                            radTextBox_Pass.Text,
                            radTextBox_Dpt.Text,
                            radLabel_DptName.Text,
                            radTextBox_Remark.Text);
                    }
                    else
                    {

                        dt.Rows[iRow]["PASSDVR"] = radTextBox_Pass.Text; // Change
                        dt.Rows[iRow]["REMARK"] = radTextBox_Remark.Text;
                    }
                    dt.AcceptChanges();
                }
                else
                {
                    radTextBox_Remark.Focus();
                }
            }
        }
        private void SetDefaultPanal()
        {
            radTextBox_Emp.Enabled = false;
            radTextBox_Dpt.Enabled = false;
            radTextBox_User.Enabled = false;
            radTextBox_Pass.Enabled = false;
            radTextBox_Spc.Enabled = false;
            radTextBox_Remark.Enabled = false;
            radButton_Save.Enabled = false;
            radButton_Cancel.Enabled = false;

            radTextBox_Emp.Text = string.Empty;
            radLabel_EmpName.Text = string.Empty;
            radTextBox_Dpt.Text = string.Empty;
            radLabel_DptName.Text = string.Empty;
            radTextBox_User.Text = string.Empty;
            radTextBox_Pass.Text = string.Empty;
            radTextBox_Spc.Text = string.Empty;
            radTextBox_Remark.Text = string.Empty;
        }
        private string GetSafeString(object value)
        {
            if (value == null) return string.Empty; else return value.ToString();
        }
        private void FinddataPermission()
        {
            // string sql = $@"
            // SELECT  SHOP_JOBCAM_PERMISSION.CAM_ID,EMP_ID,EMPLTABLE.SPC_NAME AS USERNAME,USERDVR,PASSDVR,
            //         DEPTCODE,DIMENSIONS.[DESCRIPTION] AS DEPTNAME,WHONAMEINS,REMARK
            // FROM    SHOP_JOBCAM_PERMISSION WITH (NOLOCK)
            //         INNER JOIN SHOP_JOBCAM WITH (NOLOCK) ON SHOP_JOBCAM_PERMISSION.CAM_ID =  SHOP_JOBCAM.CAM_ID
            //LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_JOBCAM_PERMISSION.EMP_ID = EMPLTABLE.ALTNUM   
            //LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM   
            // WHERE   SHOP_JOBCAM_PERMISSION.CAM_ID = '{_pCAMID}' ";
            //dt = ManageAndCameraClass.GetDataPermissioByCamID(_pCAMID); //ConnectionClass.SelectSQL_Main(sql);
            //dt = ManageAndCameraClass.GetDataPermissioByEmpID("", $@" AND CAM_ID = '{_pCAMID}' ");
            dt = Models.ManageCameraClass.GetDataPermissioByEmpID("", $@" AND CAM_ID = '{_pCAMID}' ");
            radGridView_Show.DataSource = dt;
        }
        private void UpdatePanelInfo(GridViewRowInfo currentRow)
        {
            if (currentRow != null && !(currentRow is GridViewNewRowInfo))
            {
                this.radTextBox_Emp.Text = this.GetSafeString(currentRow.Cells["EMP_ID"].Value);
                this.radLabel_EmpName.Text = this.GetSafeString(currentRow.Cells["USERNAME"].Value);
                this.radTextBox_Dpt.Text = this.GetSafeString(currentRow.Cells["DEPTCODE"].Value);
                this.radLabel_DptName.Text = this.GetSafeString(currentRow.Cells["DEPTNAME"].Value);
                this.radTextBox_User.Text = this.GetSafeString(currentRow.Cells["USERDVR"].Value);
                this.radTextBox_Pass.Text = this.GetSafeString(currentRow.Cells["PASSDVR"].Value);
                this.radTextBox_Remark.Text = this.GetSafeString(currentRow.Cells["REMARK"].Value);
                radTextBox_Emp.Enabled = false;
                radTextBox_Dpt.Enabled = false;
                radTextBox_User.Enabled = false;
                radTextBox_Pass.Enabled = false;
                radTextBox_Spc.Enabled = false;
                radTextBox_Remark.Enabled = false;
                radButton_Save.Enabled = false;
            }
            else
            {
                SetDefaultPanal();
            }
        }
        private string GetExecut()
        {
            string sqlIn;
            if (Updatestatus == "1")
            {
                sqlIn = $@"INSERT INTO dbo.SHOP_JOBCAM_PERMISSION
                            ( CAM_ID
                              ,[EMP_ID]
                              ,[USERNAME]
                              ,[REMARK]
                              ,[DEPTCODE]
                              ,[DEPTNAME]
                              ,[USERDVR]
                              ,[PASSDVR]
                              ,[WHOINS]
                              ,[WHONAMEINS] )
                            VALUES  ('" + _pCAMID + @"',
                            '" + radTextBox_Emp.Text.Trim().ToUpper() + @"',      
                            '" + radLabel_EmpName.Text + @"',  
                            '" + radTextBox_Remark.Text + @"',
                            '" + radTextBox_Dpt.Text.ToUpper() + @"',
                            '" + radLabel_DptName.Text + @"',
                            '" + radTextBox_User.Text.ToUpper() + @"',
                            '" + radTextBox_Pass.Text.Trim() + @"',
                            '" + SystemClass.SystemUserID + @"',
                            '" + SystemClass.SystemUserName + @"')";
            }
            else
            {
                sqlIn = $@"
                UPDATE  SHOP_JOBCAM_PERMISSION
                SET     USERDVR = '{radTextBox_User.Text.Trim().ToUpper()}',PASSDVR = '{radTextBox_Pass.Text.Trim()}',
                        REMARK = '{radTextBox_Remark.Text}'
                WHERE   CAM_ID = '{_pCAMID}' AND EMP_ID = '{radTextBox_Emp.Text.Trim()}' ";
            }
            return ConnectionClass.ExecuteSQL_Main(sqlIn);
        }
        private DataTable CheckUserPermission(string pEmp)
        {
            //return ManageAndCameraClass.CheckUserPermission(pEmp, _pCAMID);  
            //return ManageAndCameraClass.GetDataPermissioByEmpID("", $@" AND CAM_ID = '{_pCAMID}' AND EMP_ID = '{pEmp}' "); //ConnectionClass.SelectSQL_Main(stringsql);
            return Models.ManageCameraClass.GetDataPermissioByEmpID("", $@" AND CAM_ID = '{_pCAMID}' AND EMP_ID = '{pEmp}' "); //ConnectionClass.SelectSQL_Main(stringsql);
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
    }
}
