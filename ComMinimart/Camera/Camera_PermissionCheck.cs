﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_PermissionCheck : Form
    {

        DataTable dtData;
        public Camera_PermissionCheck()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        private void Camera_PermissionCheck_Load(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            this.Text = "เช็คสิทธิ์ดูกล้องวงจรปิด";
         
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_ID", "CAMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_ID", "รหัส", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERNAME", "ชื่อพนักงาน", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_DESC", "คำอธิบาย", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERDVR", "USER", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PASSDVR", "PASSWORD", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTCODE", "แผนก", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "ชื่อแผนก", 230));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 150));

            ////Freeze Column
            this.radGridView_Show.MasterTemplate.Columns["CAM_ID"].IsPinned = true;
            this.radGridView_Show.MasterTemplate.Columns["EMP_ID"].IsPinned = true;

            dtData = Find_ByID("TOP 100 ", "");
            radGridView_Show.DataSource = dtData;
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
         }
        //close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        #region "Enter Filler"
        private bool EnterPress = false;
        private void Kel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) EnterPress = true;
        }

        private void RadGridView_Show_FilterChanging(object sender, GridViewCollectionChangingEventArgs e)
        {
            if (!EnterPress)
            {
                e.Cancel = true;
            }

            EnterPress = false;

        }
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {
            string pCon = string.Empty;
            foreach (var filterD in radGridView_Show.FilterDescriptors)
            {
                if (filterD.Operator == Telerik.WinControls.Data.FilterOperator.Contains)
                    pCon += $" AND {filterD.Expression.Replace(" ", "%").Replace("%LIKE%", " LIKE ")} ";
                else
                    pCon += $" AND {filterD.Expression}";
            }

            string pTop = "";
            if (pCon == "") { pTop = " TOP 100 "; pCon = ""; }

            Cursor.Current = Cursors.WaitCursor;
            dtData = Find_ByID(pTop, pCon);
            radGridView_Show.DataSource = dtData;
            Cursor.Current = Cursors.Default;

        }
        #endregion
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Row is GridViewFilteringRowInfo)
            {
                RadTextBoxEditor ed = e.ActiveEditor as RadTextBoxEditor;
                RadTextBoxEditorElement el = ed.EditorElement as RadTextBoxEditorElement;
                el.KeyDown -= Kel_KeyDown;
                el.KeyDown += Kel_KeyDown;
            }
        }

        public static DataTable Find_ByID(string _pTopID, string _pEmpID)
        {
            //string sql = string.Format(@"SELECT {0} SHOP_JOBCAM_PERMISSION.CAM_ID
            //                ,EMP_ID,EMPLTABLE.SPC_NAME AS USERNAME
            //                ,SHOP_JOBCAM.CAM_DESC
            //                ,USERDVR
            //                ,PASSDVR
            //                ,DEPTCODE
            //                ,DIMENSIONS.[DESCRIPTION] AS DEPTNAME
            //                ,WHONAMEINS
            //                ,REMARK
            //    FROM    SHOP_JOBCAM_PERMISSION WITH (NOLOCK)
            //            INNER JOIN SHOP_JOBCAM WITH (NOLOCK) ON SHOP_JOBCAM_PERMISSION.CAM_ID =  SHOP_JOBCAM.CAM_ID
            //            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_JOBCAM_PERMISSION.EMP_ID = EMPLTABLE.ALTNUM   
            //            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM  {1} ", _pTopID, _pEmpID);

            //DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            //return ManageAndCameraClass.GetDataPermissioByEmpID(_pTopID, _pEmpID);
            return Models.ManageCameraClass.GetDataPermissioByEmpID(_pTopID, _pEmpID);
        }
    }
}
