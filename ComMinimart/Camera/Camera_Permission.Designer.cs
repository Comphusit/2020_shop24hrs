﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_Permission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_Permission));
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radPanel_PermissInfor = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_DptName = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_AddUser = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Delet = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBar = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_User = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Dpt = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Spc = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Pass = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_EmpName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Emp = new Telerik.WinControls.UI.RadTextBox();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_PermissInfor)).BeginInit();
            this.radPanel_PermissInfor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_User)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Spc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Pass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Location = new System.Drawing.Point(101, 410);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 7;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(5, 135);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(39, 19);
            this.radLabel3.TabIndex = 5;
            this.radLabel3.Text = "แผนก";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Location = new System.Drawing.Point(3, 410);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 6;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radPanel_PermissInfor
            // 
            this.radPanel_PermissInfor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radPanel_PermissInfor.Controls.Add(this.radLabel_DptName);
            this.radPanel_PermissInfor.Controls.Add(this.radStatusStrip);
            this.radPanel_PermissInfor.Controls.Add(this.radTextBox_Remark);
            this.radPanel_PermissInfor.Controls.Add(this.radTextBox_User);
            this.radPanel_PermissInfor.Controls.Add(this.radTextBox_Dpt);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel4);
            this.radPanel_PermissInfor.Controls.Add(this.radTextBox_Spc);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel9);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel7);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel8);
            this.radPanel_PermissInfor.Controls.Add(this.radTextBox_Pass);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel_EmpName);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel1);
            this.radPanel_PermissInfor.Controls.Add(this.radTextBox_Emp);
            this.radPanel_PermissInfor.Controls.Add(this.radButton_Save);
            this.radPanel_PermissInfor.Controls.Add(this.radLabel3);
            this.radPanel_PermissInfor.Controls.Add(this.radButton_Cancel);
            this.radPanel_PermissInfor.Location = new System.Drawing.Point(581, 3);
            this.radPanel_PermissInfor.Name = "radPanel_PermissInfor";
            this.radPanel_PermissInfor.Size = new System.Drawing.Size(194, 549);
            this.radPanel_PermissInfor.TabIndex = 1;
            // 
            // radLabel_DptName
            // 
            this.radLabel_DptName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DptName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DptName.Location = new System.Drawing.Point(5, 187);
            this.radLabel_DptName.Name = "radLabel_DptName";
            this.radLabel_DptName.Size = new System.Drawing.Size(64, 19);
            this.radLabel_DptName.TabIndex = 20;
            this.radLabel_DptName.Text = "ชื่อแแผนก";
            this.radLabel_DptName.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.radButton_AddUser,
            this.commandBarSeparator1,
            this.radButton_Edit,
            this.commandBarSeparator3,
            this.radButton_Delet,
            this.commandBar});
            this.radStatusStrip.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(194, 42);
            this.radStatusStrip.TabIndex = 0;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).AutoSize = true;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Alignment = System.Drawing.ContentAlignment.BottomRight;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Enabled = false;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButton_AddUser
            // 
            this.radButton_AddUser.AutoSize = true;
            this.radButton_AddUser.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_AddUser.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_AddUser.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_AddUser.Name = "radButton_AddUser";
            this.radButton_AddUser.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_AddUser, false);
            this.radButton_AddUser.Text = "เพิ่ม";
            this.radButton_AddUser.ToolTipText = "เพิ่มสิทธิ์";
            this.radButton_AddUser.UseCompatibleTextRendering = false;
            this.radButton_AddUser.Click += new System.EventHandler(this.RadButton_AddUser_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButton_Edit
            // 
            this.radButton_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButton_Edit.Name = "radButton_Edit";
            this.radStatusStrip.SetSpring(this.radButton_Edit, false);
            this.radButton_Edit.Text = "radButtonElement1";
            this.radButton_Edit.ToolTipText = "แก้ไข";
            this.radButton_Edit.UseCompatibleTextRendering = false;
            this.radButton_Edit.Click += new System.EventHandler(this.RadButton_Edit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButton_Delet
            // 
            this.radButton_Delet.AutoSize = true;
            this.radButton_Delet.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Delet.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Delet.Name = "radButton_Delet";
            this.radButton_Delet.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Delet, false);
            this.radButton_Delet.Text = "ลบ";
            this.radButton_Delet.ToolTipText = "ลบ";
            this.radButton_Delet.UseCompatibleTextRendering = false;
            this.radButton_Delet.Click += new System.EventHandler(this.RadButton_Delet_Click);
            // 
            // commandBar
            // 
            this.commandBar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBar.Name = "commandBar";
            this.radStatusStrip.SetSpring(this.commandBar, false);
            this.commandBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBar.UseCompatibleTextRendering = false;
            this.commandBar.VisibleInOverflowMenu = false;
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.Location = new System.Drawing.Point(5, 348);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(175, 53);
            this.radTextBox_Remark.TabIndex = 5;
            this.radTextBox_Remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_User
            // 
            this.radTextBox_User.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_User.Location = new System.Drawing.Point(5, 240);
            this.radTextBox_User.Name = "radTextBox_User";
            this.radTextBox_User.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_User.TabIndex = 2;
            this.radTextBox_User.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Dpt
            // 
            this.radTextBox_Dpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Dpt.Location = new System.Drawing.Point(5, 160);
            this.radTextBox_Dpt.Name = "radTextBox_Dpt";
            this.radTextBox_Dpt.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Dpt.TabIndex = 1;
            this.radTextBox_Dpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(5, 323);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(60, 19);
            this.radLabel4.TabIndex = 15;
            this.radLabel4.Text = "หมายเหตุ";
            // 
            // radTextBox_Spc
            // 
            this.radTextBox_Spc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Spc.Location = new System.Drawing.Point(5, 523);
            this.radTextBox_Spc.Name = "radTextBox_Spc";
            this.radTextBox_Spc.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Spc.TabIndex = 4;
            this.radTextBox_Spc.Visible = false;
            this.radTextBox_Spc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(8, 498);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(61, 19);
            this.radLabel9.TabIndex = 13;
            this.radLabel9.Text = "*ระบุ SPC";
            this.radLabel9.Visible = false;
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(3, 267);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(83, 19);
            this.radLabel7.TabIndex = 11;
            this.radLabel7.Text = "*PASSWORD";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(5, 215);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(50, 19);
            this.radLabel8.TabIndex = 9;
            this.radLabel8.Text = "*USER ";
            // 
            // radTextBox_Pass
            // 
            this.radTextBox_Pass.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Pass.Location = new System.Drawing.Point(5, 294);
            this.radTextBox_Pass.Name = "radTextBox_Pass";
            this.radTextBox_Pass.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Pass.TabIndex = 3;
            this.radTextBox_Pass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel_EmpName
            // 
            this.radLabel_EmpName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmpName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmpName.Location = new System.Drawing.Point(5, 110);
            this.radLabel_EmpName.Name = "radLabel_EmpName";
            this.radLabel_EmpName.Size = new System.Drawing.Size(54, 19);
            this.radLabel_EmpName.TabIndex = 4;
            this.radLabel_EmpName.Text = "นายคอม";
            this.radLabel_EmpName.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(3, 58);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(122, 19);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "รหัสพนักงาน [Enter]";
            // 
            // radTextBox_Emp
            // 
            this.radTextBox_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Emp.Location = new System.Drawing.Point(5, 83);
            this.radTextBox_Emp.Name = "radTextBox_Emp";
            this.radTextBox_Emp.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Emp.TabIndex = 0;
            this.radTextBox_Emp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Emp_KeyDown);
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.Size = new System.Drawing.Size(572, 549);
            this.radGridView_Show.TabIndex = 0;
            this.radGridView_Show.ThemeName = "Fluent";
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.RadGridView_Show_CurrentRowChanged);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel_PermissInfor, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 555F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 555);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // Camera_Permission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Camera_Permission";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Camera_Permission";
            this.Load += new System.EventHandler(this.Camera_Permission_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_PermissInfor)).EndInit();
            this.radPanel_PermissInfor.ResumeLayout(false);
            this.radPanel_PermissInfor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_User)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Spc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Pass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadPanel radPanel_PermissInfor;
        protected Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel_EmpName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Dpt;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Emp;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Spc;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox_User;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Pass;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButton_AddUser;
        private Telerik.WinControls.UI.RadButtonElement radButton_Delet;
        private Telerik.WinControls.UI.CommandBarSeparator commandBar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadButtonElement radButton_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_DptName;
    }
}