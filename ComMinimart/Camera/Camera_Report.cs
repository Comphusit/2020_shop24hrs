﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Drawing;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();

        //int pInsert;
        //Load
        public Camera_Report()
        {
            InitializeComponent();
        }
        //Load
        private void Camera_Report_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_Clear.ToolTipText = "ล้างข้อมูล"; radButtonElement_Clear.ShowBorder = true;

            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_type);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Count);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ประเภทกล้อง", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "ข้อมูล", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("COUNT", "รอบที่", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CAM_IP", "CAM_IP", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CAM_CH", "ช่อง", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CHECK_REMARK", "รายละเอียดการตรวจ", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CHECK_EMPNAME", "ผู้ตรวจ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATE_CHECK", "วันที่ตรวจ", 110)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIME_CHECK", "เวลาที่ตรวจ", 90)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 400)));
            RadGridView_ShowHD.TableElement.RowHeight = 350;

            RadButton_Search.ButtonElement.ShowBorder = true;

            SetCamType();
            ClearTxt();
        }
        //set Camera
        void SetCamType()
        {
            string sql = string.Format(@"
                            SELECT	SHOW_ID,SHOW_NAME + '-' + SHOW_DESC AS SHOW_NAME
                            FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                            WHERE	TYPE_CONFIG = '21'
                            ORDER BY SHOW_ID ");
            RadDropDownList_type.DataSource = ConnectionClass.SelectSQL_Main(sql);
            RadDropDownList_type.DisplayMember = "SHOW_NAME";
            RadDropDownList_type.ValueMember = "SHOW_ID";
        }
        //Set HD
        void SetDGV_HD()
        {

            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string type = "", round = "";
            try
            {
                type = RadDropDownList_type.SelectedValue.ToString();
                round = radDropDownList_Count.SelectedValue.ToString();
            }
            catch (Exception) { }

            if ((type == "") || (round == ""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่ได้ระบุเงื่อนไขการถึงข้อมูลให้เรียบร้อย ลองใหม่อีกครั้ง");
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            string sqlSelect = string.Format(@"
                    SELECT	SHOP_JOBCAM.CAM_IP, CAM_CH,SHOP_JOBCAM.BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME
		                    ,TMP.*
                    FROM	(   
			                    SELECT  'รอบที่ '+  CONVERT(VARCHAR,COUNT) AS  COUNT,CAM_ID,SHOP_JOBCAMCHECK.CAM_IP,SHOP_JOBCAMCHECK.CAM_DESC,SHOP_JOBCAMCHECK.CHECK_REMARK,SHOP_JOBCAMCHECK.PATH_IMAGE
					                    ,SHOP_JOBCAMCHECK.CHECK_EMPID + CHAR(10) + SHOP_JOBCAMCHECK.CHECK_EMPNAME + ' [' + SHOP_JOBCAMCHECK.CHECK_EMPNICKNAME + ']' AS CHECK_EMPNAME,
					                    CONVERT(varchar,SHOP_JOBCAMCHECK.CHECK_DATEINS,23) AS DATE_CHECK,CONVERT(varchar,SHOP_JOBCAMCHECK.CHECK_DATEINS,24) AS TIME_CHECK 
                                        ,SHOW_ID,SHOW_NAME,SHOW_DESC,MaxImage 
			                    FROM	SHOP_JOBCAMCHECK  WITH (NOLOCK) 
                                        INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_JOBCAMCHECK.CHECK_STATION =  SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID  AND TYPE_CONFIG = '21'   AND STA = '1' 
                                WHERE	CHECK_STATION = '" + type + @"' AND CAM_DATECHECK = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND COUNT = '" + round + @"'  
		                    )TMP 
		                    INNER JOIN SHOP_JOBCAM WITH (NOLOCK) ON TMP.CAM_ID = SHOP_JOBCAM.CAM_ID 
		                    LEFT OUTER JOIN  SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                    ORDER BY SHOP_JOBCAM.BRANCH_ID,TMP.CAM_IP 
            ");

            dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pathFullName = dt_Data.Rows[i]["PATH_IMAGE"].ToString();
                if (pathFullName == "") pathFullName = PathImageClass.pImageEmply;
                RadGridView_ShowHD.Rows[i].Cells["Image"].Value = Image.FromFile(pathFullName);
            }
            this.Cursor = Cursors.Default;
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            //pInsert = 0;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }


        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0)   return;  
            string T = DatagridClass.ExportExcelGridView("รอบที่ " + radDropDownList_Count.SelectedItem[0].ToString() + " [ " +
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + " ]", RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Clear
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Select Change
        private void RadDropDownList_type_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            string type = "";
            try
            {
                type = RadDropDownList_type.SelectedValue.ToString();
            }
            catch (Exception) { }

            DataTable dtR = new DataTable();
            dtR.Columns.Add("VALUES");
            dtR.Columns.Add("VALUES_NAME");

            if (type == "")
            {
                dtR.Rows.Add("1", "1");
            }
            else
            {
                string sql = string.Format(@"
                    SELECT	SHOW_ID,SHOW_NAME,SHOW_DESC,MaxImage
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                    WHERE	SHOW_ID = '" + type + @"'
                    ORDER BY SHOW_ID 
                ");
                DataTable dt = ConnectionClass.SelectSQL_Main(sql);

                if (dt.Rows.Count == 0)
                {
                    dtR.Rows.Add("1", "1");
                }
                else
                {
                    for (int i = 1; i < Convert.ToInt32(dt.Rows[0]["MaxImage"].ToString()); i++)
                    {
                        dtR.Rows.Add(Convert.ToString(i), Convert.ToString(i));
                    }
                    dtR.Rows.Add(dt.Rows[0]["MaxImage"].ToString(), dt.Rows[0]["MaxImage"].ToString());
                }
            }

            radDropDownList_Count.DataSource = dtR;
            radDropDownList_Count.DisplayMember = "VALUES_NAME";
            radDropDownList_Count.ValueMember = "VALUES";
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}

