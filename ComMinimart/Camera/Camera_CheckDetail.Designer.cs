﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_CheckDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_CheckDetail));
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_Check = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radTextBox_Remark2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_Job = new Telerik.WinControls.UI.RadCheckBox();
            this.radRadioButton_Check = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Nocheck = new Telerik.WinControls.UI.RadRadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e = new Telerik.WinControls.RootRadElement();
            this.radLabel_MN = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_LinkCam = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Desc = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CH = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Record = new Telerik.WinControls.UI.RadLabel();
            this.radButton_IE = new Telerik.WinControls.UI.RadButton();
            this.radButton_Job = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Channelremark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Chanel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Nocheck)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_LinkCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Record)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_IE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Channelremark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Chanel)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(362, 156);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(23, 19);
            this.radLabel7.TabIndex = 27;
            this.radLabel7.Text = "วัน";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(54, 158);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(100, 19);
            this.radLabel8.TabIndex = 25;
            this.radLabel8.Text = "บันทึกย้อนหลัง ";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(54, 123);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(85, 19);
            this.radLabel6.TabIndex = 23;
            this.radLabel6.Text = "จำนวนกล้อง ";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(54, 55);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(73, 19);
            this.radLabel2.TabIndex = 19;
            this.radLabel2.Text = "Link Cam ";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(54, 22);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(43, 19);
            this.radLabel1.TabIndex = 17;
            this.radLabel1.Text = "สาขา ";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(54, 91);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(69, 19);
            this.radLabel3.TabIndex = 21;
            this.radLabel3.Text = "คำอธิบาย ";
            // 
            // radDateTimePicker_Check
            // 
            this.radDateTimePicker_Check.CustomFormat = "";
            this.radDateTimePicker_Check.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_Check.Location = new System.Drawing.Point(163, 156);
            this.radDateTimePicker_Check.Name = "radDateTimePicker_Check";
            this.radDateTimePicker_Check.Size = new System.Drawing.Size(146, 21);
            this.radDateTimePicker_Check.TabIndex = 0;
            this.radDateTimePicker_Check.TabStop = false;
            this.radDateTimePicker_Check.Text = "Tuesday, April 21, 2020";
            this.radDateTimePicker_Check.Value = new System.DateTime(2020, 4, 21, 0, 0, 0, 0);
            this.radDateTimePicker_Check.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Check_ValueChanged);
            // 
            // radTextBox_Remark2
            // 
            this.radTextBox_Remark2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox_Remark2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark2.Location = new System.Drawing.Point(3, 212);
            this.radTextBox_Remark2.Multiline = true;
            this.radTextBox_Remark2.Name = "radTextBox_Remark2";
            // 
            // 
            // 
            this.radTextBox_Remark2.RootElement.StretchVertically = true;
            this.radTextBox_Remark2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Remark2.Size = new System.Drawing.Size(506, 109);
            this.radTextBox_Remark2.TabIndex = 1;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(204, 123);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(72, 19);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "CHANNEL";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.Location = new System.Drawing.Point(3, 3);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Remark.Size = new System.Drawing.Size(506, 153);
            this.radTextBox_Remark.TabIndex = 0;
            // 
            // radCheckBox_Job
            // 
            this.radCheckBox_Job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Job.Location = new System.Drawing.Point(304, 14);
            this.radCheckBox_Job.Name = "radCheckBox_Job";
            this.radCheckBox_Job.Size = new System.Drawing.Size(219, 19);
            this.radCheckBox_Job.TabIndex = 2;
            this.radCheckBox_Job.Text = "ต้องการปิดงาน [ระบุรายละเอียด]";
            this.radCheckBox_Job.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Job_ToggleStateChanged);
            // 
            // radRadioButton_Check
            // 
            this.radRadioButton_Check.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Check.Location = new System.Drawing.Point(12, 14);
            this.radRadioButton_Check.Name = "radRadioButton_Check";
            this.radRadioButton_Check.Size = new System.Drawing.Size(48, 19);
            this.radRadioButton_Check.TabIndex = 0;
            this.radRadioButton_Check.TabStop = false;
            this.radRadioButton_Check.Text = "ปกติ";
            // 
            // radRadioButton_Nocheck
            // 
            this.radRadioButton_Nocheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Nocheck.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Nocheck.Location = new System.Drawing.Point(63, 14);
            this.radRadioButton_Nocheck.Name = "radRadioButton_Nocheck";
            this.radRadioButton_Nocheck.Size = new System.Drawing.Size(172, 19);
            this.radRadioButton_Nocheck.TabIndex = 1;
            this.radRadioButton_Nocheck.Text = "ไม่ปกติ [ระบุรายละเอียด]";
            this.radRadioButton_Nocheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Nocheck.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Nocheck_ToggleStateChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radTextBox_Remark, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radTextBox_Remark2, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 194);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 58.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 41.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(512, 324);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radRadioButton_Nocheck);
            this.radPanel1.Controls.Add(this.radRadioButton_Check);
            this.radPanel1.Controls.Add(this.radCheckBox_Job);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(3, 162);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(506, 44);
            this.radPanel1.TabIndex = 45;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Location = new System.Drawing.Point(158, 524);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(106, 32);
            this.radButton_Save.TabIndex = 1;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.Location = new System.Drawing.Point(270, 524);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(106, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e
            // 
            this.object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e.Name = "object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e";
            this.object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e.StretchHorizontally = true;
            this.object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e.StretchVertically = true;
            // 
            // radLabel_MN
            // 
            this.radLabel_MN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_MN.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_MN.Location = new System.Drawing.Point(163, 22);
            this.radLabel_MN.Name = "radLabel_MN";
            this.radLabel_MN.Size = new System.Drawing.Size(55, 19);
            this.radLabel_MN.TabIndex = 47;
            this.radLabel_MN.Text = "MN001";
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(214, 22);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Name.TabIndex = 48;
            this.radLabel_Name.Text = "เทศบาล";
            // 
            // radLabel_LinkCam
            // 
            this.radLabel_LinkCam.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_LinkCam.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_LinkCam.Location = new System.Drawing.Point(163, 55);
            this.radLabel_LinkCam.Name = "radLabel_LinkCam";
            this.radLabel_LinkCam.Size = new System.Drawing.Size(40, 19);
            this.radLabel_LinkCam.TabIndex = 49;
            this.radLabel_LinkCam.Text = "http:";
            // 
            // radLabel_Desc
            // 
            this.radLabel_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Desc.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Desc.Location = new System.Drawing.Point(163, 91);
            this.radLabel_Desc.Name = "radLabel_Desc";
            this.radLabel_Desc.Size = new System.Drawing.Size(33, 19);
            this.radLabel_Desc.TabIndex = 50;
            this.radLabel_Desc.Text = "SPC";
            // 
            // radLabel_CH
            // 
            this.radLabel_CH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CH.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CH.Location = new System.Drawing.Point(163, 123);
            this.radLabel_CH.Name = "radLabel_CH";
            this.radLabel_CH.Size = new System.Drawing.Size(24, 19);
            this.radLabel_CH.TabIndex = 51;
            this.radLabel_CH.Text = "01";
            // 
            // radLabel_Record
            // 
            this.radLabel_Record.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Record.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Record.Location = new System.Drawing.Point(322, 156);
            this.radLabel_Record.Name = "radLabel_Record";
            this.radLabel_Record.Size = new System.Drawing.Size(24, 19);
            this.radLabel_Record.TabIndex = 52;
            this.radLabel_Record.Text = "01";
            // 
            // radButton_IE
            // 
            this.radButton_IE.BackColor = System.Drawing.Color.Transparent;
            this.radButton_IE.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_IE.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_IE.Image = global::PC_Shop24Hrs.Properties.Resources.link;
            this.radButton_IE.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_IE.Location = new System.Drawing.Point(391, 151);
            this.radButton_IE.Name = "radButton_IE";
            this.radButton_IE.Size = new System.Drawing.Size(28, 26);
            this.radButton_IE.TabIndex = 54;
            this.radButton_IE.Text = "radButton3";
            this.radButton_IE.Click += new System.EventHandler(this.RadButton_IE_Click);
            // 
            // radButton_Job
            // 
            this.radButton_Job.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Job.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Job.Image = global::PC_Shop24Hrs.Properties.Resources.job;
            this.radButton_Job.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Job.Location = new System.Drawing.Point(425, 151);
            this.radButton_Job.Name = "radButton_Job";
            this.radButton_Job.Size = new System.Drawing.Size(28, 26);
            this.radButton_Job.TabIndex = 55;
            this.radButton_Job.Click += new System.EventHandler(this.RadButton_Job_Click);
            // 
            // radLabel_Channelremark
            // 
            this.radLabel_Channelremark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Channelremark.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Channelremark.Location = new System.Drawing.Point(363, 123);
            this.radLabel_Channelremark.Name = "radLabel_Channelremark";
            this.radLabel_Channelremark.Size = new System.Drawing.Size(56, 19);
            this.radLabel_Channelremark.TabIndex = 57;
            this.radLabel_Channelremark.Text = "1/12/4";
            // 
            // radLabel_Chanel
            // 
            this.radLabel_Chanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Chanel.Location = new System.Drawing.Point(322, 123);
            this.radLabel_Chanel.Name = "radLabel_Chanel";
            this.radLabel_Chanel.Size = new System.Drawing.Size(33, 19);
            this.radLabel_Chanel.TabIndex = 56;
            this.radLabel_Chanel.Text = "ช่อง";
            // 
            // Camera_CheckDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 561);
            this.Controls.Add(this.radLabel_Channelremark);
            this.Controls.Add(this.radLabel_Chanel);
            this.Controls.Add(this.radButton_Job);
            this.Controls.Add(this.radButton_IE);
            this.Controls.Add(this.radLabel_Record);
            this.Controls.Add(this.radLabel_CH);
            this.Controls.Add(this.radLabel_Desc);
            this.Controls.Add(this.radLabel_LinkCam);
            this.Controls.Add(this.radLabel_Name);
            this.Controls.Add(this.radLabel_MN);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radDateTimePicker_Check);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radLabel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Camera_CheckDetail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "บันทึกตรวจกล้อง";
            this.Load += new System.EventHandler(this.Camera_CheckDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Nocheck)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_LinkCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Record)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_IE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Channelremark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Chanel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Check;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Job;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Check;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Nocheck;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.RootRadElement object_ac66e58a_cbde_4d1b_87af_b8a28c3a648e;
        private Telerik.WinControls.UI.RadLabel radLabel_MN;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadLabel radLabel_LinkCam;
        private Telerik.WinControls.UI.RadLabel radLabel_Desc;
        private Telerik.WinControls.UI.RadLabel radLabel_CH;
        private Telerik.WinControls.UI.RadLabel radLabel_Record;
        private Telerik.WinControls.UI.RadButton radButton_IE;
        private Telerik.WinControls.UI.RadButton radButton_Job;
        private Telerik.WinControls.UI.RadLabel radLabel_Channelremark;
        private Telerik.WinControls.UI.RadLabel radLabel_Chanel;
    }
}