﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.JOB;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_CheckDetail : Form
    {
        public string returnExecut;
        private string billMaxNO;
        readonly DATACAMERADETAIL dATACAMERADETAIL;
        readonly string _pUserpermis;
        readonly string _pDateselect;
        readonly string _pCamID;
        readonly string _pStation;
        readonly int _pCount;
        string Permission;
        public Camera_CheckDetail(string pUserpermis, string pDateselect, string pCamID, string pStation, int pCount)
        {
            InitializeComponent();
            this.KeyPreview = true;
            this.Text = "รายละเอียด";
            _pUserpermis = pUserpermis;
            _pDateselect = pDateselect;
            _pCamID = pCamID;
            _pStation = pStation;
            _pCount = pCount;
            this.dATACAMERADETAIL = new DATACAMERADETAIL(_pUserpermis, _pDateselect, _pCamID, _pStation, _pCount);

            SetTextControls(this.Controls);
            SetTextControls(tableLayoutPanel1.Controls);
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_IE.ButtonElement.ShowBorder = true;
            radButton_Job.ButtonElement.ShowBorder = true;
            radButton_IE.ButtonElement.ToolTipText = "เปิด IE";
            radButton_Job.ButtonElement.ToolTipText = "ดูจ็อบ";
            radCheckBox_Job.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Job.ButtonElement.TextElement.ForeColor = ConfigClass.SetColor_Blue();

            radTextBox_Remark.BackColor = Color.LightGray;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Check, DateTime.Today, DateTime.Now.Date);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;

            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
            }
            foreach (RadTextBoxBase c in controlCollection.OfType<RadTextBoxBase>())
            {
                c.Font = SystemClass.SetFontGernaral;
                c.ForeColor = ConfigClass.SetColor_Blue();
            }
            foreach (RadCheckBox c in controlCollection.OfType<RadCheckBox>())
            {
                c.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral;
                c.ButtonElement.TextElement.ForeColor = ConfigClass.SetColor_Blue();
            }
        }
        private void Camera_CheckDetail_Load(object sender, EventArgs e)
        {
            this.LabelColorDatashow();
            GetdataCamera();
            this.Clear();

            if (_pDateselect != DateTime.Now.ToString("yyyy-MM-dd") || dATACAMERADETAIL.CAMCHECKSTATUS == "1")
            {
                radButton_Save.Enabled = false;
                radDateTimePicker_Check.Enabled = false;
                foreach (var control in tableLayoutPanel1.Controls.Cast<Control>())
                {
                    if (control is RadTextBoxBase)
                    {
                        RadTextBoxBase tb = control as RadTextBoxBase;
                        tb.ReadOnly = true;       // controls like TextBox and RichTextBox
                    }
                    else
                    {
                        control.Enabled = false;  // all other controls
                    }
                }
            }

            if (dATACAMERADETAIL.CAMJOBID == "") radButton_Job.Enabled = false;

            if (_pUserpermis == "0") Permission = "1";

            if (_pUserpermis == "1" && _pStation != "")
            {
                radLabel_Chanel.Visible = true;
                radLabel_Channelremark.Visible = true;
            }
        }
        private void LabelColorDatashow()
        {
            radLabel_MN.ForeColor = ConfigClass.SetColor_Blue();
            radLabel_Name.ForeColor = ConfigClass.SetColor_Blue();
            radLabel_LinkCam.ForeColor = ConfigClass.SetColor_Blue();
            radLabel_Desc.ForeColor = ConfigClass.SetColor_Blue();
            radLabel_CH.ForeColor = ConfigClass.SetColor_Blue();
            radLabel_Record.ForeColor = ConfigClass.SetColor_Blue();
        }
        private void GetdataCamera()
        {
            radLabel_MN.Text = dATACAMERADETAIL.CAMDEPT;
            radLabel_Name.Text = dATACAMERADETAIL.CAMDEPTNAME;
            radLabel_LinkCam.Text = dATACAMERADETAIL.CAMIP;
            radLabel_Desc.Text = dATACAMERADETAIL.CAMDESC;
            radLabel_CH.Text = dATACAMERADETAIL.CAMCH.ToString();
            radLabel_Record.Text = dATACAMERADETAIL.CAMRECODR.ToString();
            radDateTimePicker_Check.Value = DateTime.Now.AddDays(-(dATACAMERADETAIL.CAMRECODR));
            radTextBox_Remark.Text = GetDescJob(dATACAMERADETAIL.CAMJOBID);
            radTextBox_Remark2.Focus();
            radTextBox_Remark2.Text = dATACAMERADETAIL.CAMCHECKREMARK;
            billMaxNO = dATACAMERADETAIL.CAMJOBID;
            radLabel_Channelremark.Text = dATACAMERADETAIL.CAMREMARKCHANNEL;
            if (radTextBox_Remark2.Text == "") radRadioButton_Check.IsChecked = true;
        }
        private string GetDescJob(string pJobID)
        {
            string sql = string.Format(@"SELECT ISNULL(JOB_DESCRIPTION,'') AS JOB_DESCRIPTION FROM SHOP_JOBComMinimart WITH (NOLOCK) WHERE JOB_Number = '{0}'", pJobID);
            DataTable dtDescJob = ConnectionClass.SelectSQL_Main(sql);
            if (dtDescJob.Rows.Count > 0) return dtDescJob.Rows[0]["JOB_DESCRIPTION"].ToString(); else return string.Empty;
        }
        private void Clear()
        {
            radTextBox_Remark.ReadOnly = true;
            radCheckBox_Job.ReadOnly = true;
            if (_pUserpermis != "0")
            {
                radCheckBox_Job.Visible = false;
                radDateTimePicker_Check.Enabled = false;

                if ((dATACAMERADETAIL.CAMCHECKSTATUS == "1") && (dATACAMERADETAIL.CAMREMARK == "")) radRadioButton_Check.IsChecked = true;
            }
            else
            {
                if (dATACAMERADETAIL.CAMJOBID != "") radCheckBox_Job.ReadOnly = false;

                radRadioButton_Check.Visible = false;
                radRadioButton_Nocheck.Visible = false;
            }
            radTextBox_Remark2.Focus();
            radLabel_Chanel.Visible = false;
            radLabel_Channelremark.Visible = false;
        }
        private void RadCheckBox_Job_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (radCheckBox_Job.Checked == true)
            {
                radTextBox_Remark2.Enabled = true;
                radTextBox_Remark2.Focus();
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            ArrayList sqlIn = new ArrayList();

            if (_pUserpermis == "1" && _pStation != "")
            {

                if (radRadioButton_Check.IsChecked == false && radRadioButton_Nocheck.IsChecked == false)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุรายละเอียดการตรวจทุกครั้งก่อนบันทึก.");
                    radTextBox_Remark2.Focus();
                    return;
                }
                if (radRadioButton_Nocheck.IsChecked == true && radTextBox_Remark2.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุรายละเอียดการตรวจทุกครั้งก่อนบันทึก.");
                    radTextBox_Remark2.Focus();
                    return;
                }

                if (radTextBox_Remark2.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุรายละเอียดการตรวจทุกครั้งก่อนบันทึก.");
                    radTextBox_Remark2.Focus();
                    return;
                }

                sqlIn.Add(string.Format(@"INSERT INTO SHOP_JOBCAMCHECK(CAM_ID, CAM_IP, CAM_DESC, CAM_DATECHECK
                    , CHECK_EMPID, CHECK_EMPNAME,CHECK_EMPNICKNAME, CHECK_USERTYPE, CHECK_REMARK,CHECK_STATION,[COUNT])
                    VALUES( '{0}','{1}','{2}',CONVERT(VARCHAR, GETDATE(), 23)
                    ,'{3}','{4}','{5}','{6}','{7}','{8}','{9}')"
                   , dATACAMERADETAIL.CAMID
                   , dATACAMERADETAIL.CAMIP
                   , dATACAMERADETAIL.CAMDESC
                   , SystemClass.SystemUserID
                   , SystemClass.SystemUserName
                   , SystemClass.SystemUserNameShort
                   , _pUserpermis
                   , radTextBox_Remark2.Text
                   , _pStation
                   , _pCount));
            }

            else
            {
                if (_pUserpermis != "0")
                {
                    if (radRadioButton_Check.IsChecked == false && radRadioButton_Nocheck.IsChecked == false)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุรายละเอียดการตรวจทุกครั้งก่อนบันทึก.");
                        radTextBox_Remark2.Focus();
                        return;
                    }
                    if (radRadioButton_Nocheck.IsChecked == true && radTextBox_Remark2.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุรายละเอียดการตรวจทุกครั้งก่อนบันทึก.");
                        radTextBox_Remark2.Focus();
                        return;
                    }
                }
                else
                {
                    if (dATACAMERADETAIL.CAMDATRECODR == radDateTimePicker_Check.Value.ToString("yyyy-MM-dd"))
                    {
                        if (MessageBox.Show(string.Format(@"ยืนยันบันทึกย้อนหลัง เป็นวันที่ {0} ?", radDateTimePicker_Check.Value.ToString("dd-MM-yyyy"))
                            , SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            radTextBox_Remark2.Focus();
                            return;
                        }
                    }
                }

                //ArrayList sqlIn = new ArrayList {
                sqlIn.Add(string.Format(@"INSERT INTO SHOP_JOBCAMCHECK(CAM_ID, CAM_IP, CAM_DESC, CAM_DATECHECK
                    , CHECK_EMPID, CHECK_EMPNAME,CHECK_EMPNICKNAME, CHECK_USERTYPE, CHECK_REMARK)
                    VALUES( '{0}','{1}','{2}',CONVERT(VARCHAR, GETDATE(), 23)
                    ,'{3}','{4}','{5}','{6}','{7}')"
                    , dATACAMERADETAIL.CAMID
                    , dATACAMERADETAIL.CAMIP
                    , dATACAMERADETAIL.CAMDESC
                    , SystemClass.SystemUserID
                    , SystemClass.SystemUserName
                    , SystemClass.SystemUserNameShort
                    , _pUserpermis
                    , radTextBox_Remark2.Text));
                //};

                if (_pUserpermis == "0")
                {
                    sqlIn.Add(string.Format(@"UPDATE SHOP_JOBCAM SET 
                          CAM_RECORD = '{0}',
                          CAM_DATERECORD = '{1}',
                          USERUPD = '{2}',
                          DATEUPD = GETDATE()
                          WHERE CAM_ID = '{3}'"
                     , int.Parse(radLabel_Record.Text)
                     , radDateTimePicker_Check.Value.ToString("yyyy-MM-dd")
                     , SystemClass.SystemUserID
                     , dATACAMERADETAIL.CAMID));
                }
                if (radTextBox_Remark2.Text != "")
                {
                    DataTable dtJobGroup = JOB_Class.GetJOB_Group("00001");
                    //string descJOB = Environment.NewLine;
                    string typJOB;
                    string descJOB = "- " + radTextBox_Remark2.Text.Trim();
                    if (SystemClass.SystemComMinimart == "1")
                    {
                        descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComMinimart] ";
                    }
                    else if (SystemClass.SystemDptID == "D179")
                    {
                        descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComService] ";
                    }
                    else
                    {
                        descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + @" [" + SystemClass.SystemDptName + @"/" + SystemClass.SystemUserNameShort + "] ";
                    }
                    descJOB += @" [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ff") + "] ";

                    if (dATACAMERADETAIL.CAMTYPECAM == "MN") typJOB = "SHOP"; else typJOB = "SUPC";
                    

                    if (dATACAMERADETAIL.CAMJOBID == "")
                    {
                        DataTable dtJobGroupSub = JOB_Class.GetJOB_GroupSub("00001");
                        DataRow[] dr = dtJobGroupSub.Select("JOBGROUPSUB_ID = '00003'");
                        //savejod
                        billMaxNO = ConfigClass.GetMaxINVOICEID(dtJobGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString(), "-", dtJobGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString(), "1");

                        sqlIn.Add(string.Format(@"INSERT INTO {0} (JOB_Number,JOB_BRANCHID,JOB_BRANCHNAME,JOB_GROUPSUB,JOB_WHOINS
                                ,JOB_NAMEINS,JOB_SHORTNAMEINS,JOB_DESCRIPTION,JOB_SN,JOB_IP
                                ,JOB_DATEINS,JOB_TYPE)  
                            VALUES ( '{1}','{2}','{3}','{4}','{5}'
                                ,'{6}','{7}','{8}','{9}','{10}'
                                , GETDATE(),'{11}' )"
                                    , dtJobGroup.Rows[0]["TABLENAME"].ToString()
                                    , billMaxNO
                                    , radLabel_MN.Text
                                    , radLabel_Name.Text
                                    , dr[0].ItemArray[0].ToString()
                                    , SystemClass.SystemUserID
                                    , SystemClass.SystemUserName
                                    , SystemClass.SystemUserNameShort
                                    , descJOB
                                    , radLabel_LinkCam.Text
                                    , radLabel_Desc.Text
                                    , typJOB));
                    }
                    else if (dATACAMERADETAIL.CAMJOBID != "")
                    {
                        //editjob
                        string descJOBUpdate = radTextBox_Remark.Text + Environment.NewLine + Environment.NewLine + descJOB;
                        if (radCheckBox_Job.Checked == true)
                        {
                            sqlIn.Add(string.Format(@"UPDATE {0} 
                            SET JOB_DESCRIPTION_CLOSE = '{1}'
                            ,JOB_STACLOSE = '1'
                            ,JOB_WHOCLOSE = '{2}' 
                            ,JOB_NAMECLOSE = '{3}'
                            ,JOB_DATECLOSE = GETDATE() 
                            ,JOB_DATEUPD = GETDATE()   
                            WHERE JOB_Number = '{4}' "
                                , dtJobGroup.Rows[0]["TABLENAME"].ToString()
                                , descJOB
                                , SystemClass.SystemUserID
                                , SystemClass.SystemUserName
                                , dATACAMERADETAIL.CAMJOBID));
                        }
                        else
                        {
                            if (MessageBox.Show("ต้องการอัพเดทงานใช่ไหม?", SystemClass.SystemHeadprogram,
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {

                                sqlIn.Add(string.Format(@"UPDATE {0} 
                            SET JOB_DESCRIPTION = '{1}'
                            ,JOB_DESCRIPTION_UPD = '{2}' 
                            ,JOB_DATEUPD = GETDATE()  
                            WHERE JOB_Number = '{3}'"
                                , dtJobGroup.Rows[0]["TABLENAME"].ToString()
                                , Environment.NewLine + descJOBUpdate
                                , descJOB
                                , dATACAMERADETAIL.CAMJOBID));
                            }
                        }
                    }
                }
            }

            returnExecut = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(returnExecut);
            this.DialogResult = DialogResult.OK;
            this.Close();
            return;
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            return;
        }
        private void RadDateTimePicker_Check_ValueChanged(object sender, EventArgs e)
        {
            radLabel_Record.Text = (((TimeSpan)(DateTime.Now - radDateTimePicker_Check.Value)).Days).ToString();
        }
        private void RadRadioButton_Nocheck_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (radRadioButton_Nocheck.IsChecked == true)
            {
                radTextBox_Remark2.Enabled = true;
                radTextBox_Remark2.Focus();
            }
            else
            {
                radTextBox_Remark2.Enabled = true;
                radTextBox_Remark2.Focus();
            }
        }
        private void RadButton_IE_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("iexplore.exe", radLabel_LinkCam.Text);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ไม่สามารถเปิดลิ้งค์ได้ ลองใหม่อีกครั้ง" + ex, SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่สามารถเปิดลิ้งค์ได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
            }
        }
        private void RadButton_Job_Click(object sender, EventArgs e)
        {
            if (dATACAMERADETAIL.CAMJOBID != "")
            {
                JOB.Com.JOBCOM_EDIT frmJob = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComMinimart", "00001", "ComMinimart", Permission, "", dATACAMERADETAIL.CAMJOBID);
                frmJob.ShowDialog();
            }
        }
    }
}
