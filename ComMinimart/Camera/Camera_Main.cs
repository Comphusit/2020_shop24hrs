﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using System.Diagnostics;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System.Collections;
using PC_Shop24Hrs.JOB;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_Main : Form
    {
        DataTable dt = new DataTable();
        DataTable dtStation = new DataTable();
        string _pUserpermis, _pType, _checkD041;   //admin,comser = 0,center = 1,รปภ = 2,ทั่วไป = 3,koh = 4 
        readonly string _pConditions;
        private DATACAMERA DATACAMERAUPDATE;
        readonly DATACAM_IP dATACAM_IP = new DATACAM_IP();
        public enum Userpermis
        {
            admin,         //0
            center,        //1
            security,      //2
            general,       //3
            executive,     //4 โก้
            minimart        // minimart
        }
        public Camera_Main(string pUserpermis, string pType, string pCondition)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pUserpermis = pUserpermis;
            _pType = pType;
            _pConditions = pCondition;

        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        void SetGridView()
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_ID", "CAMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_DESC", "คำอธิบาย", 320));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_IP", "ลิ้งค์", 170));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("CAM_CH", "จำนวน[กล้อง]", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("CAM_RECORD", "บันทึกย้อนหลัง[วัน]", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_AMORN", "อมร"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SHOW_ID", "ประจำจุด"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("CAM_REMARKCHANNEL", "ช่อง", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_REMARK", "หมายเหตุ", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_TECHNICID", "รหัส"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_TECHNICNAME", "ช่าง", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_MYANMAR", "ช่างพม่า", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CAM_DEPT", "แผนก", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CAM_TYPECAM", "พื้นที่"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SET_USER", "USER", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SET_PASSWORD", "PASSWORD", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PING", "PING", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOB_NUMBER", "JOB"));

            if (_pUserpermis != "0")
            {
                this.radGridView_Show.MasterTemplate.Columns["CAM_TECHNICNAME"].IsVisible = false;
                this.radGridView_Show.MasterTemplate.Columns["CAM_MYANMAR"].IsVisible = false;
                this.radGridView_Show.MasterTemplate.Columns["CAM_DEPT"].IsVisible = false;
                this.radGridView_Show.MasterTemplate.Columns["PING"].IsVisible = false;
            }
            //Freeze Column
            this.radGridView_Show.MasterTemplate.Columns["CAM_ID"].IsPinned = true;
            this.radGridView_Show.MasterTemplate.Columns["CAM_DESC"].IsPinned = true;
        }
        void SetDropDownList()
        {
            DatagridClass.SetDefaultFontDropDown(radDropDown_Cam);
            radDropDown_Cam.DropDownListElement.ForeColor = ConfigClass.SetColor_Blue();
            DatagridClass.SetDefaultFontDropDown(radDropDown_Area);
            radDropDown_Area.DropDownListElement.ForeColor = ConfigClass.SetColor_Blue();
        }
        void SetButton()
        {
            radButton_Add.ShowBorder = true;
            radButton_Edit.ShowBorder = true;
            radButton_Delet.ShowBorder = true;
            radButton_Addpermiss.ShowBorder = true;
            radButtonElement_Checkpermiss.ShowBorder = true;
            radButtonElement_Ping.ShowBorder = true;
            radButtonElement_Ping.ToolTipText = "Ping";
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
        }
        void SetExpression()
        {
            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            //ExpressionFormattingObject obj1 = new ExpressionFormattingObject("CAM_RECORD", "CAM_RECORD < '15' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PurplePastel() };
            //this.radGridView_Show.Columns["CAM_RECORD"].ConditionalFormattingObjectList.Add(obj1);
            DatagridClass.SetCellBackClolorByExpression("CAM_RECORD", "CAM_RECORD < '15' ", ConfigClass.SetColor_PurplePastel(), radGridView_Show);

            //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("CAM_RECORD", "CAM_RECORD >= '15' AND CAM_RECORD < '30'", false)
            //{ CellBackColor = ConfigClass.SetColor_YellowPastel() };
            //this.radGridView_Show.Columns["CAM_RECORD"].ConditionalFormattingObjectList.Add(obj2);
            DatagridClass.SetCellBackClolorByExpression("CAM_RECORD", "CAM_RECORD >= '15' AND CAM_RECORD < '30'", ConfigClass.SetColor_YellowPastel(), radGridView_Show);

            //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("PING", "PING = 'TimedOut'", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //this.radGridView_Show.Columns["PING"].ConditionalFormattingObjectList.Add(obj3);
            DatagridClass.SetCellBackClolorByExpression("PING", "PING = 'TimedOut'", ConfigClass.SetColor_Red(), radGridView_Show);


            ExpressionFormattingObject obj4 = new ExpressionFormattingObject("JOB_NUMBER", "JOB_NUMBER <> ''", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.radGridView_Show.Columns["CAM_IP"].ConditionalFormattingObjectList.Add(obj4);
            this.radGridView_Show.Columns["CAM_CH"].ConditionalFormattingObjectList.Add(obj4);
        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("CAM_DESC", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        //ROWS NUMBERS
        #region "ROWSNUMBER"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        private void Camera_Main_Load(object sender, EventArgs e)
        {
            this.SetGridView();
            this.SetDropDownList();
            this.SetButton();
            this.SetExpression();
            this.GetGridViewSummary();
            this.radButtonElement_Ping.Click += new System.EventHandler(this.RadButtonElement_Ping_Click);

            radLabel_Detail.Text = "สีม่วง >> บันทึกย้อนหลังน้อยกว่า 15 วัน | สีเหลือง >> บันทึกย้อนหลังมากกว่า 15 วัน แต่ไม่เกิน 30 วัน | สีแดงที่ลิ้งค์และจำนวนกล้อง >> มี Job | สีแดงที่Ping >> offline | ดับเบิ้ลคลิกคำอธิบายเพื่อดูผ่านโปรแกรม หรือดับเบิ้ลคลิกลิ้งค์เพื่อดูผ่านIE";


            this.GetDropDownList();
            this.GetEnebleNotAdmin();
            this.CheckEmpID_D041();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            dt = this.GetCamAll();
            dtStation = this.GetCamStation();
            radGridView_Show.DataSource = dt;
            this.CheckUserPermission();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        #region Method
        void CheckEmpID_D041()
        {
            if (SystemClass.SystemDptID.Equals("D041"))
            {
                if (SystemClass.SystemUserPositionName.Equals("วิศวกร") || SystemClass.SystemUserPositionName.Equals("ช่างเขียนแบบ") || SystemClass.SystemUserPositionName.Equals("สถาปนิก"))
                {
                    _pUserpermis = "1";
                    _pType = "MN";
                    _checkD041 = _pType;
                }
            }

        }
        void CheckUserPermission()
        {
            switch (Convert.ToInt32(_pUserpermis))
            {
                //comservice
                case (int)Userpermis.admin:
                    if (_pType == "1")
                    {
                        radDropDown_Area.Enabled = true;
                    }
                    else
                    {
                        radButton_Add.Enabled = true; radButton_Edit.Enabled = true;
                        radButton_Delet.Enabled = true; radButton_Addpermiss.Enabled = true;
                        radButtonElement_Checkpermiss.Enabled = true;
                        radDropDown_Area.Enabled = true; radDropDown_Cam.Enabled = true;
                        radButtonElement_Ping.Enabled = true;
                    }
                    break;
                //center
                case (int)Userpermis.center:
                    if (!string.IsNullOrEmpty(_checkD041))
                    {
                        radButton_Edit.Enabled = false;
                        radDropDown_Cam.Enabled = false;
                    }
                    else
                    {
                        radButton_Edit.Enabled = true;
                        radDropDown_Cam.Enabled = true;
                        radDropDown_Area.SelectedValue = _pType;
                    }
                    break;
                //รภป
                case (int)Userpermis.security:
                    radDropDown_Area.Enabled = true;
                    break;
                //ทั่วไป
                case (int)Userpermis.general:
                    break;
                //koh
                case (int)Userpermis.executive:
                    radDropDown_Area.SelectedValue = _pType;
                    if (_pConditions != "") radDropDown_Cam.SelectedValue = _pConditions;

                    break;
                default:

                    break;
            }
        }
        private void GetDropDownList()
        {
            radDropDown_Area.DataSource = GetCamArea();
            radDropDown_Area.DisplayMember = "CAMAREA";
            radDropDown_Area.ValueMember = "CAM_TYPECAM";
            radDropDown_Area.SelectedIndex = 0;

            radDropDown_Cam.DataSource = GetCamtype();
            radDropDown_Cam.ValueMember = "SHOW_ID";
            radDropDown_Cam.DisplayMember = "SHOW_NAME";
            radDropDown_Cam.SelectedIndex = 0;

        }
        private void GetEnebleNotAdmin()
        {
            radButton_Add.Enabled = false;
            radButton_Edit.Enabled = false;
            radButton_Delet.Enabled = false;
            radButton_Addpermiss.Enabled = false;
            radButtonElement_Checkpermiss.Enabled = false;
            radDropDown_Area.Enabled = false;
            radDropDown_Cam.Enabled = false;
            radButtonElement_Ping.Enabled = false;
        }
        private DataTable GetCamtype()
        {
            string sql = $@"SELECT 'C00' AS [SHOW_ID],'ทั้งหมด' AS [SHOW_NAME]
                        UNION ( SELECT [SHOW_ID],[SHOW_NAME] FROM [SHOP_CONFIGBRANCH_GenaralDetail]  WHERE [TYPE_CONFIG] = '21') ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetCamArea()
        {
            string sql = $@"
                    SELECT 'ALL' AS CAM_TYPECAM ,'ทั้งหมด' AS CAMAREA UNION 
                            SELECT CAM_TYPECAM,CASE CAM_TYPECAM
		                    WHEN 'MN' THEN 'มินิมาร์ท'
		                    WHEN 'RET' THEN 'สาขาใหญ่ RET'
		                    WHEN 'SPC' THEN 'สาขาใหญ่ SPC'
		                    WHEN 'OTH' THEN 'พื้นที่ส่วนนอก OTH'   END AS CAMAREA
				    FROM    SHOP_JOBCAM WITH (NOLOCK)
				    GROUP BY CAM_TYPECAM ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetCamAll()
        {
            string sql = "";
            switch (_pUserpermis)
            {
                case "0":
                    sql = $@"
                    SELECT  CAM_ID, CAM_DESC, CAM_IP, CAM_CH, CAM_RECORD, CAM_AMORN, CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME,
                            ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT, CAM_TYPECAM,CAM_REMARK,'' AS SHOW_ID,'' AS CAM_REMARKCHANNEL,'' AS PING
		                    ,ISNULL(JOB_Number,'') AS JOB_NUMBER,CAM_MYANMAR
                            ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                    FROM    SHOP_JOBCAM WITH(NOLOCK)  
                            LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                            LEFT OUTER JOIN (  
					            SELECT ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) AS RowID,[JOB_Number],[JOB_SN]
					            FROM	[dbo].[SHOP_JOBComMinimart] WITH(NOLOCK)
					            WHERE   [JOB_STACLOSE] = '0'
							            AND JOB_GROUPSUB ='00003')J ON J.[JOB_SN] = SHOP_JOBCAM.CAM_IP  
				                AND RowID ='1'
                             LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                    ORDER BY CAM_DESC 
                    ";
                    break;
                //D041Build
                case "1":
                    if (!string.IsNullOrEmpty(_checkD041))
                    {
                        sql = $@"
                        SELECT  CAM_ID, CAM_DESC, CAM_IP, CAM_CH, CAM_RECORD, CAM_AMORN, CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME,
                                ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT, CAM_TYPECAM, CAM_REMARK,'' AS SHOW_ID,'' AS CAM_REMARKCHANNEL,CAM_MYANMAR,
                                ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                        FROM    SHOP_JOBCAM WITH(NOLOCK) 
                                LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                                LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                        WHERE   CAM_TYPECAM = 'MN' OR CAM_DEPT in ('D042','D015','D177') 
                        ORDER BY CAM_DESC";
                    }
                    else
                    {
                        sql = $@"
                        SELECT  CAM_ID, CAM_DESC, CAM_IP, CAM_CH, CAM_RECORD, CAM_AMORN, CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME,
                                ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT, CAM_TYPECAM,CAM_REMARK,'' AS SHOW_ID,'' AS CAM_REMARKCHANNEL,CAM_MYANMAR,
                                ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                        FROM    SHOP_JOBCAM WITH(NOLOCK)  
                                LEFT OUTER JOIN  SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                                LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                        ORDER BY CAM_DESC ";
                    }
                    break;
                //รภป
                case "2":
                    sql = $@"
                    SELECT  CAM_ID, CAM_DESC, CAM_IP, CAM_CH, CAM_RECORD, CAM_AMORN, CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME, 
                            ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT, CAM_TYPECAM, CAM_REMARK,'' AS SHOW_ID,'' AS CAM_REMARKCHANNEL,CAM_MYANMAR,
                            ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                    FROM    SHOP_JOBCAM WITH(NOLOCK) 
                            LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                            LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                    WHERE   [CAM_CHECKUSER] = '0' AND CAM_TYPECAM ! = 'MN' 
                    ORDER BY CAM_DESC";
                    break;
                //ทั่วไป
                case "3":
                    sql = $@"
                    SELECT  SHOP_JOBCAM.CAM_ID,CAM_DESC,CAM_IP,CAM_CH,CAM_RECORD,CAM_AMORN, CAM_TYPECAM,USERDVR AS CAM_TECHNICNAME,
                            PASSDVR AS CAM_DEPT,'' AS CAM_REMARKCHANNEL,CAM_MYANMAR,CAM_REMARK,
                            ISNULL(USERDVR,'') AS  SET_USER,ISNULL(PASSDVR,'') AS  SET_PASSWORD
                    FROM    SHOP_JOBCAM WITH(NOLOCK)  
                            LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID   
                            INNER JOIN  SHOP_JOBCAM_PERMISSION WITH(NOLOCK) ON SHOP_JOBCAM.CAM_ID = SHOP_JOBCAM_PERMISSION.CAM_ID
                    WHERE   EMP_ID = '{SystemClass.SystemUserID}' ORDER BY CAM_DESC ";
                    break;
                case "4":
                    sql = $@"
                    SELECT  CAM_ID, CAM_DESC, CAM_IP, CAM_CH, CAM_RECORD, CAM_AMORN, CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME,
                            ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT, CAM_TYPECAM,CAM_REMARK,'' AS SHOW_ID,'' AS CAM_REMARKCHANNEL,'' AS PING
		                    ,ISNULL(JOB_Number,'') AS JOB_NUMBER,CAM_MYANMAR
                            ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                    FROM    SHOP_JOBCAM WITH(NOLOCK)  
                            LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                            LEFT OUTER JOIN (  
					            SELECT ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) AS RowID,[JOB_Number],[JOB_SN]
					            FROM	[dbo].[SHOP_JOBComMinimart] WITH(NOLOCK)
					            WHERE   [JOB_STACLOSE] = '0'
							            AND JOB_GROUPSUB ='00003')J ON J.[JOB_SN] = SHOP_JOBCAM.CAM_IP  
				                AND RowID ='1'
                             LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                    ORDER BY CAM_DESC 
                    ";
                    break;
                case "5":
                    sql = $@"
                    SELECT  CAM_ID, CAM_DESC, CAM_IP, CAM_CH, CAM_RECORD, CAM_AMORN, CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME,
                            ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT, CAM_TYPECAM,CAM_REMARK,'' AS SHOW_ID,'' AS CAM_REMARKCHANNEL,'' AS PING
		                    ,ISNULL(JOB_Number,'') AS JOB_NUMBER,CAM_MYANMAR
                            ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                    FROM    SHOP_JOBCAM WITH(NOLOCK)  
                            LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                            LEFT OUTER JOIN (  
					            SELECT ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) AS RowID,[JOB_Number],[JOB_SN]
					            FROM	[dbo].[SHOP_JOBComMinimart] WITH(NOLOCK)
					            WHERE   [JOB_STACLOSE] = '0'
							            AND JOB_GROUPSUB ='00003')J ON J.[JOB_SN] = SHOP_JOBCAM.CAM_IP  
				                AND RowID ='1'
                             LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                    WHERE   SHOP_BRANCH.BRANCH_ID = '{SystemClass.SystemBranchID}'
                    ORDER BY CAM_DESC 
                    ";
                    break;
                default:
                    break;
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetCamStation()
        {
            string sql = $@"
                    SELECT  [SHOP_JOBCAMSTATION].[CAM_ID],CAM_DESC,CAM_IP,CAM_CH,CAM_RECORD,CAM_AMORN,ISNULL(CAM_DEPT, 'D999') AS CAM_DEPT,CAM_TYPECAM,CAM_TECHNICID, ISNULL(CAM_TECHNICNAME, '') AS CAM_TECHNICNAME
                            ,CAM_REMARK,[SHOW_ID],[CHANNEL] AS CAM_REMARKCHANNEL,CAM_MYANMAR  
                            ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                    FROM    [SHOP_JOBCAMSTATION] WITH (NOLOCK)   
                            INNER JOIN  SHOP_JOBCAM ON SHOP_JOBCAM.CAM_ID = [SHOP_JOBCAMSTATION].CAM_ID
                            LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                    ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                    ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        void CheckPingIp()
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            foreach (DataRow row in dt.Rows)
            {
                string ip = row["CAM_IP"].ToString().Replace("http://", "").Replace("/", "").Replace(":81", "").Replace(":82", "").Replace("ie.htm", "");
                try
                {
                    System.Net.NetworkInformation.Ping myPing = new System.Net.NetworkInformation.Ping();
                    System.Net.NetworkInformation.PingReply reply = myPing.Send(ip, 1000);
                    row["PING"] = reply.Status.ToString();
                }
                catch (Exception)
                {
                    //pingStatus = "ไม่พบข้อมูล" + ex.Message;
                }
            }
            dt.AcceptChanges();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        #endregion
        private void RadButtonElement_Ping_Click(object sender, EventArgs e)
        {
            this.CheckPingIp();
        }
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            Camera_Detail frmCam_New = new Camera_Detail(this.Text + " :: เพิ่มกล้อง", "", _pUserpermis);
            if (frmCam_New.ShowDialog(this) == DialogResult.OK)
            {
                if (frmCam_New.ReturnExecut == "")
                {
                    this.DATACAMERAUPDATE = frmCam_New.DATAROWCAM;
                    dt.Rows.Add(this.DATACAMERAUPDATE.CAMID,
                        this.DATACAMERAUPDATE.CAMDESC,
                        this.DATACAMERAUPDATE.CAMIP,
                        this.DATACAMERAUPDATE.CAMCH,
                        this.DATACAMERAUPDATE.CAMRECODR,
                        this.DATACAMERAUPDATE.CAMAMORN,
                         this.DATACAMERAUPDATE.CAMTECHNICID,
                        this.DATACAMERAUPDATE.CAMTECHNICINAME,
                        this.DATACAMERAUPDATE.CAMDEPT,
                        this.DATACAMERAUPDATE.CAMTYPECAM,
                        this.DATACAMERAUPDATE.CAMREMARK,
                        "", "");
                    dt.AcceptChanges();
                }
            }
        }
        private void RadButton_Edit_Click(object sender, EventArgs e)
        {
            if (this.radGridView_Show.CurrentRow != null)
            {
                DataRow[] rows = dt.Select("CAM_ID = '" + this.radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString() + "' ");
                int iRow = dt.Rows.IndexOf(rows[0]);

                Camera_Detail Camera_Detail = new Camera_Detail(this.Text + " :: แก้ไข", radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString(), _pUserpermis);
                if (Camera_Detail.ShowDialog(this) == DialogResult.OK)
                {
                    if (Camera_Detail.ReturnExecut == "")
                    {
                        this.DATACAMERAUPDATE = Camera_Detail.DATAROWCAM;
                        dt.Rows[iRow]["CAM_IP"] = this.DATACAMERAUPDATE.CAMIP;
                        dt.Rows[iRow]["CAM_DESC"] = this.DATACAMERAUPDATE.CAMDESC;
                        dt.Rows[iRow]["CAM_CH"] = this.DATACAMERAUPDATE.CAMCH;
                        dt.Rows[iRow]["CAM_AMORN"] = this.DATACAMERAUPDATE.CAMAMORN;
                        dt.Rows[iRow]["CAM_TECHNICID"] = this.DATACAMERAUPDATE.CAMTECHNICID;
                        dt.Rows[iRow]["CAM_TECHNICNAME"] = this.DATACAMERAUPDATE.CAMTECHNICINAME;
                        dt.Rows[iRow]["CAM_MYANMAR"] = this.DATACAMERAUPDATE.CAM_MYANMAR;
                        dt.Rows[iRow]["CAM_DEPT"] = this.DATACAMERAUPDATE.CAMDEPT;
                        dt.Rows[iRow]["CAM_TYPECAM"] = this.DATACAMERAUPDATE.CAMTYPECAM;
                        dt.Rows[iRow]["CAM_REMARK"] = this.DATACAMERAUPDATE.CAMREMARK;
                        dt.AcceptChanges();
                    }
                }
            }
        }
        private void RadButton_Delet_Click(object sender, EventArgs e)
        {
            //ms conferm
            if (this.radGridView_Show.CurrentRow != null)
            {
                //if (MessageBox.Show("ยืนยันการลบข้อมูล ? " + radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString(), SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบข้อมูล ?{Environment.NewLine}{radGridView_Show.CurrentRow.Cells["CAM_IP"].Value}") == DialogResult.Yes)
                {
                    DataRow[] rows = dt.Select("CAM_ID = '" + this.radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString() + "' ");
                    int iRow = dt.Rows.IndexOf(rows[0]);

                    ArrayList sqlDel = new ArrayList
                    {
                        $@" DELETE FROM SHOP_JOBCAM WHERE CAM_ID = '{radGridView_Show.CurrentRow.Cells["CAM_ID"].Value}' ",
                        $@" DELETE FROM SHOP_JOBCAMSTATION WHERE CAM_ID = '{radGridView_Show.CurrentRow.Cells["CAM_ID"].Value}' "
                    };

                    string returnstr = ConnectionClass.ExecuteSQL_ArrayMain(sqlDel);
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);

                    if (returnstr == "")
                    {
                        dt.Rows[iRow].Delete();   //Delete
                        dt.AcceptChanges();
                    }
                }
            }
        }
        private void RadButton_Addpermiss_Click(object sender, EventArgs e)
        {
            if (this.radGridView_Show.CurrentRow != null && radGridView_Show.CurrentRow.Cells["CAM_ID"].Value != null)
            {
                Camera_Permission frmCamera_Permission = new Camera_Permission(radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString());
                if (frmCamera_Permission.ShowDialog(this) == DialogResult.OK)
                {
                    this.Clearfilter();
                }
            }
        }
        private void RadDropDownList_Area_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDown_Area.SelectedIndex > 0) GridViewfilter(radGridView_Show.Columns["CAM_TYPECAM"], radDropDown_Area.SelectedValue.ToString()); else this.Clearfilter();
        }
        private void RadDropDownList_Cam_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDown_Cam.SelectedIndex > 0)
            {
                radDropDown_Area.SelectedIndex = 1;
                radDropDown_Area.Enabled = false;
                this.Clearfilter();

                if (radGridView_Show.DataSource != dtStation) radGridView_Show.DataSource = dtStation;

                GridViewfilter(radGridView_Show.Columns["SHOW_ID"], radDropDown_Cam.SelectedValue.ToString());
            }
            else
            {
                if (radGridView_Show.DataSource != dt) radGridView_Show.DataSource = dt;

                this.Clearfilter();
                if (_pUserpermis == "0") { radDropDown_Area.Enabled = true; }
                if (radDropDown_Area.SelectedValue.ToString() != "ALL") GridViewfilter(radGridView_Show.Columns["CAM_TYPECAM"], radDropDown_Area.SelectedValue.ToString());
            }
        }
        private void GridViewfilter(GridViewDataColumn ColName, string Value)
        {
            FilterDescriptor filter = new FilterDescriptor(ColName.Name, FilterOperator.Contains, Value);
            ColName.FilterDescriptor = filter;
        }
        private void Clearfilter()
        {
            radGridView_Show.FilterDescriptors.Clear();
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pUserpermis);
        }

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                if (e.Column.Name.Equals("CAM_DESC"))
                {
                    DataTable dtCam = JOB_Class.GetCam_ByID(radGridView_Show.CurrentRow.Cells["CAM_ID"].Value.ToString());
                    if (dtCam.Rows.Count == 0) { return; }

                    dATACAM_IP.IPCam = dtCam.Rows[0]["CAM_IP"].ToString();
                    dATACAM_IP.CamID = dtCam.Rows[0]["CAM_ID"].ToString();
                    dATACAM_IP.FormName = this.Name;
                    dATACAM_IP.JobNumber = dtCam.Rows[0]["JOB_Number"].ToString();
                    dATACAM_IP.BranchID = dtCam.Rows[0]["BRANCH_ID"].ToString();
                    dATACAM_IP.CamDesc = dtCam.Rows[0]["CAM_DESC"].ToString();
                    dATACAM_IP.CamDVR = dtCam.Rows[0]["CAM_DVR"].ToString();
                    dATACAM_IP.Userpermis = _pUserpermis;
                    dATACAM_IP.CamCH = int.Parse(dtCam.Rows[0]["Cam_CH"].ToString());
                    Camera_IEMain frm = new Camera_IEMain(dATACAM_IP, "1",
                                radGridView_Show.CurrentRow.Cells["SET_USER"].Value.ToString(), radGridView_Show.CurrentRow.Cells["SET_PASSWORD"].Value.ToString())
                    {
                        pCAM_REMARKCHANNEL = radGridView_Show.CurrentRow.Cells["CAM_REMARKCHANNEL"].Value.ToString(),
                        ptypeCamOpen = "กล้องวงจรปิดหลัก"
                    };
                    frm.Show();
                }
                else if (e.Column.Name.Equals("CAM_IP"))
                {
                    if (radGridView_Show.CurrentRow.Cells["CAM_AMORN"].ToString() == "1")
                    {
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนการเข้าใช้งานกล้องอมร {radGridView_Show.CurrentRow.Cells["CAM_IP"].Value} {radGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value}") == DialogResult.Yes)
                        {
                            Process.Start("iexplore.exe", radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString());
                        }
                    }
                    else
                    {
                        Process.Start("iexplore.exe", radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString());
                    }
                }
                else if (e.Column.Name.Equals("CAM_CH"))
                {
                    //เปิดJob
                    if (Convert.ToInt32(_pUserpermis) == (int)Userpermis.admin)
                    {
                        string branchid = radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value.ToString().Substring(0, 2).Equals("MN") ? radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value.ToString().Substring(0, 5) : radGridView_Show.CurrentRow.Cells["CAM_DEPT"].Value.ToString();
                        string typeJob = branchid.Substring(0, 2).Equals("MN") ? "SHOP" : "SUPC";
                        string jobNumber = radGridView_Show.CurrentRow.Cells["JOB_NUMBER"].Value.ToString();
                        if (jobNumber.Equals(""))
                        {
                            if (typeJob.Equals("SHOP"))
                            {
                                JOB.Com.JOBCOM_SHOP_ADD frmJOB_ADD = new JOB.Com.JOBCOM_SHOP_ADD("SHOP_JOBComMinimart",
                                    "MJOB", "00001", "ComMinimart.", branchid,
                                    radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value.ToString()
                                    , radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString());

                                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK) { }
                            }
                            else
                            {
                                JOB.Com.JOBCOM_SUPC_ADD frmJOB_ADD = new JOB.Com.JOBCOM_SUPC_ADD("SHOP_JOBComMinimart",
                                   "MJOB", "00001", "ComMinimart.", radGridView_Show.CurrentRow.Cells["CAM_DEPT"].Value.ToString(),
                                   radGridView_Show.CurrentRow.Cells["CAM_DESC"].Value.ToString(),
                                   radGridView_Show.CurrentRow.Cells["CAM_IP"].Value.ToString());
                                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
                                {

                                }
                            }
                        }
                        else
                        {
                            JOB.Com.JOBCOM_EDIT frmJOB_Edit = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComMinimart", "00001", "ComMinimart", "1", typeJob, jobNumber);
                            if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK) { }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถเปิดลิ้งค์ได้ ลองใหม่อีกครั้ง [{ex.Message}]");
            }
        }
        private void RadButtonElement_Checkpermiss_Click(object sender, EventArgs e)
        {
            Camera_PermissionCheck frmCamera_PermissionCheck = new Camera_PermissionCheck
            {
                WindowState = FormWindowState.Maximized
            };
            frmCamera_PermissionCheck.Show();
            //if (this.radGridView_Show.CurrentRow != null)
            //{
            //    Camera_PermissionCheck frmCamera_PermissionCheck = new Camera_PermissionCheck();
            //    if (frmCamera_PermissionCheck.ShowDialog(this) == DialogResult.OK) return; else return;

            //}
        }
    }
}