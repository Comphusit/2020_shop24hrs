﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using HikvisionPreview;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_Show : Telerik.WinControls.UI.RadForm
    {
        ShowCam singleForm;
        string pSwitch;//0 open 1 close
        readonly string _pCase;
        private readonly DataTable dtCam = new DataTable();

        int count_CH;
        public Camera_Show(string pCase = "")
        {
            InitializeComponent();
            _pCase = pCase;
        }


        private void Camera_Show_Load(object sender, EventArgs e)
        {
            radButton_Open.ButtonElement.ShowBorder = true;
            radButton_Open.ButtonElement.Font = SystemClass.SetFontGernaral;
            pSwitch = "0";

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Type);

            dtCam.Columns.Add("CAM_DESC");
            dtCam.Columns.Add("CAM_IP");
            dtCam.Columns.Add("BRANCH_ID");
            dtCam.Columns.Add("BRANCH_NAME");
            dtCam.Columns.Add("CHANNEL");
            dtCam.Columns.Add("CAM_DVR");
            dtCam.Columns.Add("SET_USER");
            dtCam.Columns.Add("SET_PASSWORD");
            dtCam.DefaultView.Sort = "BRANCH_ID ASC";

            DatagridClass.SetDefaultRadGridView(RadGridView_Bch);

            RadGridView_Bch.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก"));
            RadGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CAM_DESC", "คำอธิบาย")));
            RadGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 60)));
            RadGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 80)));
            RadGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CHANNEL", "ช่อง", 80)));
            RadGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CAM_IP", "IP", 120)));
            RadGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CAM_DVR", "CAM_DVR")));
            RadGridView_Bch.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SET_USER", "USER", 80));
            RadGridView_Bch.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SET_PASSWORD", "PASSWORD", 100));
            RadGridView_Bch.MasterTemplate.Columns["C"].IsPinned = true;

            SetTypeCam();
            ClearData();

        }
        //ClearData
        void ClearData()
        {
            count_CH = 0;
            if (dtCam.Rows.Count > 0) dtCam.Rows.Clear();

            radDropDownList_Type.Enabled = true;

            radButton_Open.Text = "OpenAll";
            radButton_Open.ButtonElement.ButtonFillElement.BackColor = Class.ConfigClass.SetColor_PurplePastel();
            radButton_Open.ButtonElement.ToolTipText = "เปิดทั้งหมดที่เลือกไว้";

            for (int i = 0; i < RadGridView_Bch.Rows.Count; i++)
            {
                RadGridView_Bch.Rows[i].Cells["C"].Value = "0";
            }
            pSwitch = "0";
            SetDataGrid();
        }
        //Set Type
        void SetTypeCam()
        {
            string sql = string.Format(@"
                    SELECT	[SHOW_ID],[SHOW_NAME] 
                    FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  
                    WHERE	[TYPE_CONFIG] = '21' AND STA = '1' " + _pCase);
            radDropDownList_Type.DataSource = ConnectionClass.SelectSQL_Main(sql);
            radDropDownList_Type.DisplayMember = "SHOW_NAME";
            radDropDownList_Type.ValueMember = "SHOW_ID";
        }
        //DataChange
        void SetDataGrid()
        {
            count_CH = 0;
            if (dtCam.Rows.Count > 0) dtCam.Rows.Clear();

            string pBch = "";
            if (SystemClass.SystemBranchID != "MN000")
            {
                pBch = " AND SHOP_JOBCAM.BRANCH_ID = '" + SystemClass.SystemBranchID + @"' ";

            }
            //SHOP_JOBCAM.CAM_DVR IN ('0','2') AND 
            string sql = string.Format(@"
                SELECT	'0' AS C,CAM_DESC,CAM_IP,SHOP_JOBCAM.BRANCH_ID,BRANCH_NAME,CHANNEL,SHOP_JOBCAM.CAM_DVR
                        ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
                FROM	SHOP_JOBCAMSTATION WITH (NOLOCK) 
		                INNER JOIN SHOP_JOBCAM WITH (NOLOCK) ON SHOP_JOBCAMSTATION.CAM_ID = SHOP_JOBCAM.CAM_ID
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                        LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '0' )CAM_PASS 
			                ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
                WHERE	SHOW_ID = '" + radDropDownList_Type.SelectedValue.ToString() + @"'  " + pBch + @"
                ORDER BY SHOP_JOBCAM.BRANCH_ID
            ");

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_Bch.DataSource = dt;
            dt.AcceptChanges();
        }

        //Select Change
        private void RadDropDownList_Type_SelectedValueChanged(object sender, EventArgs e)
        {
            SetDataGrid();
        }

        #region "Formatting"
        private void RadGridView_Bch_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Bch_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Bch_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Bch_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //Delete Rows In Datarow
        void DeleteRows(string pCon)
        {
            this.Cursor = Cursors.WaitCursor;
            DataRow[] dtr = dtCam.Select(" CAM_IP = '" + pCon + @"'");
            foreach (DataRow row in dtr)
                dtCam.Rows.Remove(row);
            dtCam.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //เลือก
        private void RadGridView_Bch_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (e.Column.Name)
            {
                case "C":
                    if (pSwitch == "1")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"มีการเลือกกล้องค้างไว้ ไม่สามารถเปลี่ยนแปลงได้{Environment.NewLine}ให้ปิดกล้องที่ดูอยู่ก่อนแล้วเลือกใหม่อีกครั้ง");
                        return;
                    }

                    if (RadGridView_Bch.CurrentRow.Cells["CHANNEL"].Value.ToString() == "")
                    {
                        RadGridView_Bch.CurrentRow.Cells["C"].Value = "0";
                        return;
                    }

                    int cR;
                    DataTable dtCH = new DataTable();
                    dtCH.Columns.Add("CH");

                    if (!RadGridView_Bch.CurrentRow.Cells["CHANNEL"].Value.ToString().Contains("/"))
                    {
                        cR = 1;
                        dtCH.Rows.Add(RadGridView_Bch.CurrentRow.Cells["CHANNEL"].Value.ToString());
                    }
                    else
                    {
                        string[] ch = RadGridView_Bch.CurrentRow.Cells["CHANNEL"].Value.ToString().Split('/');
                        cR = ch.Length;
                        for (int i = 0; i < ch.Length; i++)
                        {
                            dtCH.Rows.Add(ch[i].ToString());
                        }
                    }

                    if (RadGridView_Bch.CurrentRow.Cells["C"].Value.ToString() == "0")
                    {
                        if ((count_CH + cR) > 16)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่สามารถเลือกกล้องเกิน 16 ช่องได้{Environment.NewLine}ลองใหม่อีกครั้ง");
                            RadGridView_Bch.CurrentRow.Cells["C"].Value = "0";
                            return;
                        }

                        foreach (DataRow item in dtCH.Rows)
                        {
                            dtCam.Rows.Add(RadGridView_Bch.CurrentRow.Cells["CAM_DESC"].Value.ToString(),
                                RadGridView_Bch.CurrentRow.Cells["CAM_IP"].Value.ToString(),
                                RadGridView_Bch.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                                RadGridView_Bch.CurrentRow.Cells["BRANCH_NAME"].Value.ToString(), item[0].ToString(),
                                RadGridView_Bch.CurrentRow.Cells["CAM_DVR"].Value.ToString(),
                                RadGridView_Bch.CurrentRow.Cells["SET_USER"].Value.ToString(),
                                RadGridView_Bch.CurrentRow.Cells["SET_PASSWORD"].Value.ToString());
                        }

                        count_CH += cR;

                        RadGridView_Bch.CurrentRow.Cells["C"].Value = "1";

                    }
                    else
                    {
                        DeleteRows(RadGridView_Bch.CurrentRow.Cells["CAM_IP"].Value.ToString());
                        count_CH -= cR;
                        RadGridView_Bch.CurrentRow.Cells["C"].Value = "0";
                        return;
                    }

                    break;
                default:
                    break;
            }
        }
        //open & Close
        private void RadButton_Open_Click(object sender, EventArgs e)
        {
            switch (pSwitch)
            {
                case "0": // Open
                    this.Cursor = Cursors.WaitCursor;

                    singleForm = new ShowCam
                    {
                        dtCam = dtCam,
                        typeCam = radDropDownList_Type.SelectedItem.ToString(),

                        TopLevel = false,
                        AutoScroll = true,
                        Dock = DockStyle.Fill,
                        FormBorderStyle = FormBorderStyle.None
                    };

                    panel_Add.Controls.Clear();
                    panel_Add.Controls.Add(singleForm);
                    singleForm.Show();

                    radButton_Open.Text = "CloseAll";
                    radButton_Open.ButtonElement.ButtonFillElement.BackColor = Class.ConfigClass.SetColor_PinkPastel();
                    radButton_Open.ButtonElement.ToolTipText = "ปิดทั้งหมด";
                    radDropDownList_Type.Enabled = false;
                    pSwitch = "1";

                    this.Cursor = Cursors.Default;
                    break;

                case "1": //Close
                    ClearData();
                    try
                    {
                        singleForm.Close();
                    }
                    catch (Exception) { }
                    panel_Add.Controls.Clear();
                    break;
                default:
                    break;
            }

        }
        //Close Form
        private void Camera_Show_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                singleForm.Close();
            }
            catch (Exception)
            {
                return;
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
