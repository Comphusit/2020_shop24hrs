﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_Show
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_Show));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.radDropDownList_Type = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_Open = new Telerik.WinControls.UI.RadButton();
            this.RadGridView_Bch = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_Add = new Telerik.WinControls.UI.RadPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Open)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Bch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Bch.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.RadGridView_Bch, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(300, 687);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.Controls.Add(this.radDropDownList_Type, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radButton_Open, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.pictureBox1, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(294, 32);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // radDropDownList_Type
            // 
            this.radDropDownList_Type.AutoSize = false;
            this.radDropDownList_Type.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList_Type.DropDownHeight = 150;
            this.radDropDownList_Type.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Type.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Type.Location = new System.Drawing.Point(3, 3);
            this.radDropDownList_Type.Name = "radDropDownList_Type";
            this.radDropDownList_Type.Size = new System.Drawing.Size(186, 26);
            this.radDropDownList_Type.TabIndex = 0;
            this.radDropDownList_Type.Text = "radDropDownList1";
            this.radDropDownList_Type.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Type_SelectedValueChanged);
            // 
            // radButton_Open
            // 
            this.radButton_Open.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton_Open.Location = new System.Drawing.Point(195, 3);
            this.radButton_Open.Name = "radButton_Open";
            this.radButton_Open.Size = new System.Drawing.Size(64, 26);
            this.radButton_Open.TabIndex = 0;
            this.radButton_Open.Text = "CloseAll";
            this.radButton_Open.Click += new System.EventHandler(this.RadButton_Open_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Open.GetChildAt(0))).Text = "CloseAll";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(1).GetChildAt(1))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Open.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RadGridView_Bch
            // 
            this.RadGridView_Bch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Bch.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Bch.Location = new System.Drawing.Point(3, 41);
            // 
            // 
            // 
            this.RadGridView_Bch.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Bch.Name = "RadGridView_Bch";
            // 
            // 
            // 
            this.RadGridView_Bch.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Bch.Size = new System.Drawing.Size(294, 643);
            this.RadGridView_Bch.TabIndex = 18;
            this.RadGridView_Bch.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Bch_ViewCellFormatting);
            this.RadGridView_Bch.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Bch_CellClick);
            this.RadGridView_Bch.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Bch_ConditionalFormattingFormShown);
            this.RadGridView_Bch.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Bch_FilterPopupRequired);
            this.RadGridView_Bch.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Bch_FilterPopupInitialized);
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.panel_Add);
            this.splitContainer1.Size = new System.Drawing.Size(970, 687);
            this.splitContainer1.SplitterDistance = 300;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel_Add
            // 
            this.panel_Add.BackColor = System.Drawing.Color.Transparent;
            this.panel_Add.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Add.Location = new System.Drawing.Point(0, 0);
            this.panel_Add.Name = "panel_Add";
            this.panel_Add.Size = new System.Drawing.Size(665, 687);
            this.panel_Add.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.pictureBox1.Location = new System.Drawing.Point(265, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 26);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // Camera_Show
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 687);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Camera_Show";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "กล้องวงจรปิด";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Camera_Show_FormClosing);
            this.Load += new System.EventHandler(this.Camera_Show_Load);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Open)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Bch.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Bch)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Type;
        private Telerik.WinControls.UI.RadGridView RadGridView_Bch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadButton radButton_Open;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Telerik.WinControls.UI.RadPanel panel_Add;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
