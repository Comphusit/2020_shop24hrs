﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_Main));
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.radDropDown_Cam = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDown_Area = new Telerik.WinControls.UI.RadDropDownList();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Delet = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Addpermiss = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Checkpermiss = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator8 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Ping = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator10 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel_Area = new Telerik.WinControls.UI.RadLabelElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator9 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            this.radStatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Cam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Area)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(240)))), ((int)(((byte)(249)))));
            this.radGridView_Show.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.ForeColor = System.Drawing.Color.Black;
            this.radGridView_Show.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView_Show.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.AllowColumnReorder = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            this.radGridView_Show.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView_Show.Size = new System.Drawing.Size(907, 482);
            this.radGridView_Show.TabIndex = 67;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Controls.Add(this.radDropDown_Cam);
            this.radStatusStrip.Controls.Add(this.radDropDown_Area);
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButton_Add,
            this.commandBarSeparator5,
            this.radButton_Edit,
            this.commandBarSeparator6,
            this.radButton_Delet,
            this.commandBarSeparator4,
            this.radButton_Addpermiss,
            this.commandBarSeparator7,
            this.radButtonElement_Checkpermiss,
            this.commandBarSeparator8,
            this.radButtonElement_Ping,
            this.commandBarSeparator10,
            this.radLabel_Camall,
            this.commandBarSeparator1,
            this.radLabel_Area,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator9});
            this.radStatusStrip.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(907, 42);
            this.radStatusStrip.TabIndex = 1;
            // 
            // radDropDown_Cam
            // 
            this.radDropDown_Cam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Cam.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Cam.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDown_Cam.Location = new System.Drawing.Point(288, 7);
            this.radDropDown_Cam.Name = "radDropDown_Cam";
            this.radDropDown_Cam.Size = new System.Drawing.Size(150, 21);
            this.radDropDown_Cam.TabIndex = 67;
            this.radDropDown_Cam.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Cam_SelectedValueChanged);
            // 
            // radDropDown_Area
            // 
            this.radDropDown_Area.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Area.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Area.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDown_Area.Location = new System.Drawing.Point(522, 7);
            this.radDropDown_Area.Name = "radDropDown_Area";
            this.radDropDown_Area.Size = new System.Drawing.Size(150, 21);
            this.radDropDown_Area.TabIndex = 1;
            this.radDropDown_Area.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Area_SelectedValueChanged);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButton_Add
            // 
            this.radButton_Add.AutoSize = true;
            this.radButton_Add.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Add, false);
            this.radButton_Add.Text = "เพิ่ม";
            this.radButton_Add.ToolTipText = "เพิ่มกล้อง";
            this.radButton_Add.UseCompatibleTextRendering = false;
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButton_Edit
            // 
            this.radButton_Edit.AutoSize = true;
            this.radButton_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButton_Edit.Name = "radButton_Edit";
            this.radButton_Edit.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Edit, false);
            this.radButton_Edit.Text = "แก้ไขข้อมูล";
            this.radButton_Edit.ToolTipText = "แก้ไข";
            this.radButton_Edit.Click += new System.EventHandler(this.RadButton_Edit_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // radButton_Delet
            // 
            this.radButton_Delet.AutoSize = true;
            this.radButton_Delet.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Delet.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Delet.Name = "radButton_Delet";
            this.radButton_Delet.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Delet, false);
            this.radButton_Delet.Text = "ลบ";
            this.radButton_Delet.ToolTipText = "ลบ";
            this.radButton_Delet.Click += new System.EventHandler(this.RadButton_Delet_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButton_Addpermiss
            // 
            this.radButton_Addpermiss.AutoSize = true;
            this.radButton_Addpermiss.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Addpermiss.Image = global::PC_Shop24Hrs.Properties.Resources.add_user;
            this.radButton_Addpermiss.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton_Addpermiss.Name = "radButton_Addpermiss";
            this.radButton_Addpermiss.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Addpermiss, false);
            this.radButton_Addpermiss.Text = "เพิ่มสิทธิ์";
            this.radButton_Addpermiss.ToolTipText = "เพิ่มสิทธิ์ดูกล้อง";
            this.radButton_Addpermiss.Click += new System.EventHandler(this.RadButton_Addpermiss_Click);
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.radStatusStrip.SetSpring(this.commandBarSeparator7, false);
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Checkpermiss
            // 
            this.radButtonElement_Checkpermiss.AutoSize = true;
            this.radButtonElement_Checkpermiss.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Checkpermiss.Image = global::PC_Shop24Hrs.Properties.Resources.contacts;
            this.radButtonElement_Checkpermiss.Name = "radButtonElement_Checkpermiss";
            this.radStatusStrip.SetSpring(this.radButtonElement_Checkpermiss, false);
            this.radButtonElement_Checkpermiss.Text = "เช็คสิทธิ์ดูกล้อง";
            this.radButtonElement_Checkpermiss.ToolTipText = "เช็คสิทธิ์ดูกล้อง";
            this.radButtonElement_Checkpermiss.Click += new System.EventHandler(this.RadButtonElement_Checkpermiss_Click);
            // 
            // commandBarSeparator8
            // 
            this.commandBarSeparator8.Name = "commandBarSeparator8";
            this.radStatusStrip.SetSpring(this.commandBarSeparator8, false);
            this.commandBarSeparator8.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Ping
            // 
            this.radButtonElement_Ping.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Ping.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButtonElement_Ping.Name = "radButtonElement_Ping";
            this.radStatusStrip.SetSpring(this.radButtonElement_Ping, false);
            this.radButtonElement_Ping.Text = "radButtonElement1";
            // 
            // commandBarSeparator10
            // 
            this.commandBarSeparator10.Name = "commandBarSeparator10";
            this.radStatusStrip.SetSpring(this.commandBarSeparator10, false);
            this.commandBarSeparator10.VisibleInOverflowMenu = false;
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radStatusStrip.SetSpring(this.radLabel_Camall, false);
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radLabel_Area
            // 
            this.radLabel_Area.AutoSize = false;
            this.radLabel_Area.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Area.Name = "radLabel_Area";
            this.radStatusStrip.SetSpring(this.radLabel_Area, false);
            this.radLabel_Area.Text = "พื้นที่ : ";
            this.radLabel_Area.TextWrap = true;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator9
            // 
            this.commandBarSeparator9.Name = "commandBarSeparator9";
            this.radStatusStrip.SetSpring(this.commandBarSeparator9, false);
            this.commandBarSeparator9.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Show, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(913, 561);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 539);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(907, 19);
            this.radLabel_Detail.TabIndex = 68;
            // 
            // Camera_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 561);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Camera_Main";
            this.Text = "Camera_Main";
            this.Load += new System.EventHandler(this.Camera_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            this.radStatusStrip.ResumeLayout(false);
            this.radStatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Cam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Area)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.RadButtonElement radButton_Add;
        private Telerik.WinControls.UI.RadButtonElement radButton_Edit;
        private Telerik.WinControls.UI.RadButtonElement radButton_Delet;
        private Telerik.WinControls.UI.RadButtonElement radButton_Addpermiss;
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadLabelElement radLabel_Area;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Area;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Cam;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Checkpermiss;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator8;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator9;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Ping;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator10;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}