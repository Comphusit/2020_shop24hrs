﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_IP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_IP));
            this.radLabel_Sim = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radTextBox_Wifi = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Sim = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Wifi = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Password = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_SN = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_NameShow = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CamIP = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_MaxImage = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Name = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CamID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_ID = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radCheckBox_Sta = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Sim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Wifi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Sim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Wifi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NameShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CamIP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MaxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CamID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel_Sim
            // 
            this.radLabel_Sim.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Sim.Location = new System.Drawing.Point(6, 278);
            this.radLabel_Sim.Name = "radLabel_Sim";
            this.radLabel_Sim.Size = new System.Drawing.Size(78, 19);
            this.radLabel_Sim.TabIndex = 17;
            this.radLabel_Sim.Text = "ซิม [Enter]";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(3, 424);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(98, 424);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radCheckBox_Sta);
            this.panel1.Controls.Add(this.radTextBox_Wifi);
            this.panel1.Controls.Add(this.radTextBox_Sim);
            this.panel1.Controls.Add(this.radLabel_Wifi);
            this.panel1.Controls.Add(this.radLabel_Password);
            this.panel1.Controls.Add(this.radLabel_SN);
            this.panel1.Controls.Add(this.radLabel_NameShow);
            this.panel1.Controls.Add(this.radTextBox_CamIP);
            this.panel1.Controls.Add(this.radLabel_MaxImage);
            this.panel1.Controls.Add(this.radTextBox_Name);
            this.panel1.Controls.Add(this.radLabel_Name);
            this.panel1.Controls.Add(this.radTextBox_CamID);
            this.panel1.Controls.Add(this.radLabel_ID);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radLabel_Sim);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 5;
            // 
            // radTextBox_Wifi
            // 
            this.radTextBox_Wifi.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Wifi.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Wifi.Location = new System.Drawing.Point(8, 362);
            this.radTextBox_Wifi.Name = "radTextBox_Wifi";
            this.radTextBox_Wifi.Size = new System.Drawing.Size(174, 21);
            this.radTextBox_Wifi.TabIndex = 70;
            this.radTextBox_Wifi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Wifi_KeyDown);
            // 
            // radTextBox_Sim
            // 
            this.radTextBox_Sim.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Sim.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Sim.Location = new System.Drawing.Point(8, 303);
            this.radTextBox_Sim.Name = "radTextBox_Sim";
            this.radTextBox_Sim.Size = new System.Drawing.Size(174, 21);
            this.radTextBox_Sim.TabIndex = 69;
            this.radTextBox_Sim.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Sim_KeyDown);
            // 
            // radLabel_Wifi
            // 
            this.radLabel_Wifi.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Wifi.Location = new System.Drawing.Point(8, 337);
            this.radLabel_Wifi.Name = "radLabel_Wifi";
            this.radLabel_Wifi.Size = new System.Drawing.Size(136, 19);
            this.radLabel_Wifi.TabIndex = 68;
            this.radLabel_Wifi.Text = "Pocket Wifi [Enter]";
            // 
            // radLabel_Password
            // 
            this.radLabel_Password.AutoSize = false;
            this.radLabel_Password.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Password.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Password.Location = new System.Drawing.Point(7, 147);
            this.radLabel_Password.Name = "radLabel_Password";
            this.radLabel_Password.Size = new System.Drawing.Size(173, 19);
            this.radLabel_Password.TabIndex = 67;
            this.radLabel_Password.Text = "Password";
            // 
            // radLabel_SN
            // 
            this.radLabel_SN.AutoSize = false;
            this.radLabel_SN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_SN.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_SN.Location = new System.Drawing.Point(7, 115);
            this.radLabel_SN.Name = "radLabel_SN";
            this.radLabel_SN.Size = new System.Drawing.Size(173, 19);
            this.radLabel_SN.TabIndex = 66;
            this.radLabel_SN.Text = "SN";
            // 
            // radLabel_NameShow
            // 
            this.radLabel_NameShow.AutoSize = false;
            this.radLabel_NameShow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_NameShow.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_NameShow.Location = new System.Drawing.Point(8, 227);
            this.radLabel_NameShow.Name = "radLabel_NameShow";
            this.radLabel_NameShow.Size = new System.Drawing.Size(173, 45);
            this.radLabel_NameShow.TabIndex = 65;
            this.radLabel_NameShow.Text = "SPC";
            this.radLabel_NameShow.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radTextBox_CamIP
            // 
            this.radTextBox_CamIP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_CamIP.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CamIP.Location = new System.Drawing.Point(7, 88);
            this.radTextBox_CamIP.Name = "radTextBox_CamIP";
            this.radTextBox_CamIP.Size = new System.Drawing.Size(174, 21);
            this.radTextBox_CamIP.TabIndex = 63;
            this.radTextBox_CamIP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CamIP_KeyDown);
            // 
            // radLabel_MaxImage
            // 
            this.radLabel_MaxImage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_MaxImage.Location = new System.Drawing.Point(8, 63);
            this.radLabel_MaxImage.Name = "radLabel_MaxImage";
            this.radLabel_MaxImage.Size = new System.Drawing.Size(140, 19);
            this.radLabel_MaxImage.TabIndex = 64;
            this.radLabel_MaxImage.Text = "ทะเบียนกล้อง [Enter]";
            // 
            // radTextBox_Name
            // 
            this.radTextBox_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Name.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Name.Location = new System.Drawing.Point(7, 200);
            this.radTextBox_Name.Name = "radTextBox_Name";
            this.radTextBox_Name.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Name.TabIndex = 1;
            this.radTextBox_Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Name_KeyDown);
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.Location = new System.Drawing.Point(7, 182);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(102, 19);
            this.radLabel_Name.TabIndex = 61;
            this.radLabel_Name.Text = "ชื่อช่าง [Enter]";
            // 
            // radTextBox_CamID
            // 
            this.radTextBox_CamID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_CamID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CamID.Location = new System.Drawing.Point(6, 36);
            this.radTextBox_CamID.Name = "radTextBox_CamID";
            this.radTextBox_CamID.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_CamID.TabIndex = 0;
            this.radTextBox_CamID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID_KeyDown);
            // 
            // radLabel_ID
            // 
            this.radLabel_ID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_ID.Location = new System.Drawing.Point(6, 18);
            this.radLabel_ID.Name = "radLabel_ID";
            this.radLabel_ID.Size = new System.Drawing.Size(65, 19);
            this.radLabel_ID.TabIndex = 18;
            this.radLabel_ID.Text = "รหัสกล้อง";
            // 
            // radPanel1
            // 
            this.radPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel1.Controls.Add(this.RadGridView_Show);
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(3, 51);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(617, 574);
            this.radPanel1.TabIndex = 4;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 574);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.radButtonElement_Edit,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3});
            this.radStatusStrip1.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(617, 42);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่ม";
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.AutoSize = true;
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radCheckBox_Sta
            // 
            this.radCheckBox_Sta.AutoSize = true;
            this.radCheckBox_Sta.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_Sta.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_Sta.Location = new System.Drawing.Point(8, 393);
            this.radCheckBox_Sta.Name = "radCheckBox_Sta";
            this.radCheckBox_Sta.Size = new System.Drawing.Size(88, 20);
            this.radCheckBox_Sta.TabIndex = 71;
            this.radCheckBox_Sta.Text = "เปิดใช้งาน";
            this.radCheckBox_Sta.UseVisualStyleBackColor = true;
            // 
            // Camera_IP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Camera_IP";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "การตั้งค่าต่างๆ.";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Camera_IP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Sim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Wifi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Sim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Wifi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NameShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CamIP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MaxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CamID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel_Sim;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadLabel radLabel_ID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CamID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Name;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CamIP;
        private Telerik.WinControls.UI.RadLabel radLabel_MaxImage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadLabel radLabel_NameShow;
        private Telerik.WinControls.UI.RadLabel radLabel_SN;
        private Telerik.WinControls.UI.RadLabel radLabel_Password;
        private Telerik.WinControls.UI.RadLabel radLabel_Wifi;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Sim;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Wifi;
        private System.Windows.Forms.CheckBox radCheckBox_Sta;
    }
}
