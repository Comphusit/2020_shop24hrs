﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;


namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_IP : Telerik.WinControls.UI.RadForm
    {
        DataTable dt;
        string StatusSave = "";//0 = Insert , 1 = Update

        readonly string _pTypeConfig;//id TypeConfig
        readonly string _pTypeConfigName;//name TypeConfig
        readonly string _pFirst; //ขึ้นต้นด้วยอะไร
        readonly string _pPermission;// 0 ไม่มีสิด เพิ่ม แก้ไข 1 มีสิด

        public Camera_IP(string pTypeConfig, string pTypeConfigName, string pFirst, string pPermission)
        {
            InitializeComponent();

            _pTypeConfig = pTypeConfig;
            _pTypeConfigName = pTypeConfigName;
            _pFirst = pFirst;
            _pPermission = pPermission;

        }
        //ClearData
        void ClearData()
        {
            radTextBox_CamID.Enabled = false; radTextBox_CamID.Text = "";
            radTextBox_CamIP.Enabled = false; radTextBox_CamIP.Text = "";
            radLabel_SN.Text = "";
            radLabel_Password.Text = "";
            radTextBox_Name.Enabled = false; radTextBox_Name.Text = "";
            radLabel_NameShow.Text = "";
            radTextBox_Sim.Enabled = false; radTextBox_Sim.Text = "";
            radTextBox_Wifi.Enabled = false; radTextBox_Wifi.Text = "";
            radButton_Save.Enabled = false;
            radCheckBox_Sta.Enabled = false;
        }
        //Load Main
        private void Camera_IP_Load(object sender, EventArgs e)
        {
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "ใช้"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัสกล้อง", 130));
            if (_pTypeConfig != "58")
            {
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ช่าง", 130));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อช่าง", 250));
            }
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "ทะเบียนกล้อง", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Condition", "สภาพ", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERIALNUM", "SN", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MaintenanceInfo3", "รหัสผ่าน", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "ซิม", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCK", "Pocket Wifi", 150));

            ClearData();
            if (_pPermission == "0")
            {
                radButtonElement_Edit.Enabled = false; radButtonElement_Add.Enabled = false;
                this.Text = "ข้อมูล " + _pTypeConfigName;
            }
            else this.Text = "การตั้งค่า " + _pTypeConfigName;

            Set_DGV();
        }

        //Set DGV
        void Set_DGV()
        {
            dt = Models.AssetClass.FindDetailAsset_ByTypeConfig(_pTypeConfig);
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();
        }
        //Reset All
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //SetInsert
        void SetForInsert()
        {
            StatusSave = "0";

            radTextBox_CamID.Enabled = false;
            radTextBox_CamIP.Enabled = true; radTextBox_CamIP.Text = "";
            radTextBox_CamIP.Focus();
        }
        //Insert
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //check ข้อมูลก่อน Insert
            if (radTextBox_CamIP.Text == "") { MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ทะเบียนกล้อง"); return; }

            if (radTextBox_Sim.Text == "") { MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ทะเบียนซิม"); return; }

            if (radTextBox_Name.Text == "") { MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ชื่อช่างรับผิดชอบ"); return; }

            if (radTextBox_Wifi.Text == "") { MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ทะเบียน Pocket Wifi"); return; }

            string aSta;
            if (radCheckBox_Sta.Checked == true) aSta = "1"; else aSta = "0";

            //save Data
            string T;
            switch (StatusSave)
            {
                case "0":// Insert
                    string aID;
                    aID = ConfigClass.GetMaxINVOICEID(_pTypeConfig, "-", _pFirst, "3");

                    //string sqlIn = String.Format(@"INSERT INTO [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] 
                    //        ([SHOW_ID],[SHOW_NAME],[SHOW_DESC],[TYPE_CONFIG],[TYPE_CONFIG_DESC],[REMARK],[STA],[MaxImage],[WHOIDINS],[WHONAMEINS],[LOCK] )
                    //        VALUES  ('" + aID + @"','" + radTextBox_Name.Text + @"','" + radTextBox_CamIP.Text + @"',
                    //        '" + _pTypeConfig + @"','" + _pTypeConfigName + @"','" + radTextBox_Sim.Text + @"',
                    //        '" + aSta + @"','0','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + "','" + radTextBox_Wifi.Text + @"')");

                    string sqlIn = $@"
                            INSERT INTO [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] 
                                    ([SHOW_ID],[SHOW_NAME],[SHOW_DESC],
                                    [TYPE_CONFIG],[TYPE_CONFIG_DESC],[REMARK],
                                    [STA],[MaxImage],[WHOIDINS],
                                    [WHONAMEINS],[LOCK] )
                            VALUES  ('{aID}','{radTextBox_Name.Text}','{radTextBox_CamIP.Text}',
                                    '{_pTypeConfig}','{_pTypeConfigName}','{radTextBox_Sim.Text}',
                                    '{aSta }','0','{SystemClass.SystemUserID}',
                                    '{SystemClass.SystemUserName}','{radTextBox_Wifi.Text}') ";

                    T = ConnectionClass.ExecuteSQL_Main(sqlIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        Set_DGV();
                        ClearData();
                    }
                    break;

                case "1": // Update
                    //string sqlUp = String.Format(@"UPDATE [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] SET SHOW_NAME = '" + radTextBox_Name.Text + @"',
                    //        SHOW_DESC = '" + radTextBox_CamIP.Text + @"',REMARK = '" + radTextBox_Sim.Text + @"',
                    //        STA = '" + aSta + @"',
                    //        LOCK = '" + radTextBox_Wifi.Text + @"',
                    //        DATEUPD = GETDATE(),WHOIDUPD = '" + SystemClass.SystemUserID + @"',
                    //        WHONAMEUPD = '" + SystemClass.SystemUserName + @"' 
                    //    WHERE TYPE_CONFIG = '" + _pTypeConfig + @"' AND SHOW_ID = '" + radTextBox_CamID.Text + @"' ");

                    string sqlUp = $@"
                            UPDATE  [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] 
                            SET     SHOW_NAME = '{ radTextBox_Name.Text}',
                                    SHOW_DESC = '{radTextBox_CamIP.Text}',
                                    REMARK = '{radTextBox_Sim.Text}',
                                    STA = '{aSta}',
                                    LOCK = '{radTextBox_Wifi.Text}',
                                    DATEUPD = GETDATE(),
                                    WHOIDUPD = '{SystemClass.SystemUserID}',
                                    WHONAMEUPD = '{SystemClass.SystemUserName}' 
                            WHERE   TYPE_CONFIG = '{_pTypeConfig}' 
                                    AND SHOW_ID = '{radTextBox_CamID.Text}' ";

                    T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        Set_DGV();
                        ClearData();
                    }
                    break;
                default:
                    break;
            }
        }

        //SetEdit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;

            StatusSave = "1";
            radTextBox_CamID.Text = RadGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString(); radTextBox_CamID.Enabled = false;
            radTextBox_CamIP.Text = RadGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value.ToString(); radTextBox_CamIP.Enabled = true;
            radLabel_SN.Text = RadGridView_Show.CurrentRow.Cells["SERIALNUM"].Value.ToString();
            radLabel_Password.Text = RadGridView_Show.CurrentRow.Cells["MaintenanceInfo3"].Value.ToString();
            if (_pTypeConfig != "58")
            {
                radTextBox_Name.Text = RadGridView_Show.CurrentRow.Cells["SHOW_NAME"].Value.ToString(); radTextBox_Name.Enabled = true;
                radLabel_NameShow.Text = RadGridView_Show.CurrentRow.Cells["SPC_NAME"].Value.ToString();
            }
            radTextBox_Sim.Text = RadGridView_Show.CurrentRow.Cells["REMARK"].Value.ToString(); radTextBox_Sim.Enabled = true;
            radTextBox_Wifi.Text = RadGridView_Show.CurrentRow.Cells["LOCK"].Value.ToString(); radTextBox_Wifi.Enabled = true;
            radCheckBox_Sta.Enabled = true;
            if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "1") radCheckBox_Sta.CheckState = CheckState.Checked;
            else radCheckBox_Sta.CheckState = CheckState.Unchecked;
            radButton_Save.Enabled = true;
            radTextBox_CamID.Focus();
            if (_pTypeConfig == "58")
            {
                radTextBox_Name.Text = "ขนส่ง1999";
                radTextBox_CamIP.Focus();
            }
        }
        //ForInsert
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
            SetForInsert();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //ID Enter
        private void RadTextBox_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CamID.Text == "") return;

                //check ข้อมูลก่อนว่ามีแล้วหรือยัง
                if (dt.Rows.Count > 0)
                {
                    DataRow[] dr = dt.Select(" SHOW_ID = '" + radTextBox_CamID.Text + @"' ");
                    if (dr.Length > 0)
                    {
                        MessageBox.Show("ข้อมูลนี้มีอยู่แล้ว ไม่ต้องเพิ่มซ้ำอีก.",
                            SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        ClearData();
                        return;
                    }
                }

                radTextBox_CamID.Enabled = false;
                radLabel_Name.Text = "ชื่อ [Enter]";
                radTextBox_Name.Enabled = true;
                radTextBox_Name.Focus();
            }
        }
        //Name Enter
        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Name.Text == "") return;
                DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_Name.Text);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("พนักงาน");
                    radTextBox_Name.SelectAll();
                    radTextBox_Name.Focus();
                    return;
                }
                radLabel_NameShow.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                radTextBox_Name.Enabled = false;
                radTextBox_Sim.Enabled = true;
                radTextBox_Sim.Focus();
            }
        }

        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeConfigName);
        }

        //EnterCheck
        private void RadTextBox_CamIP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CamIP.Text == "") return;

                //DataTable dtAsset = AssetClass.FindAssetByID(radTextBox_CamIP.Text);
                DataTable dtAsset = Models.AssetClass.FindAsset_ByAllType(radTextBox_CamIP.Text, "", "");
                if (dtAsset.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("ทะเบียนสินทรัพย์");
                    radTextBox_CamIP.SelectAll();
                    radTextBox_CamIP.Focus();
                    return;
                }

                radLabel_SN.Text = dtAsset.Rows[0]["SerialNum"].ToString();
                radLabel_Password.Text = dtAsset.Rows[0]["MaintenanceInfo3"].ToString();
                radTextBox_CamIP.Enabled = false;
                radTextBox_Name.Enabled = true;
                radTextBox_Name.Focus();

                if (_pTypeConfig == "58")
                {
                    if (StatusSave != "1")
                    {
                        //check ข้อมูลก่อนว่ามีแล้วหรือยัง
                        if (dt.Rows.Count > 0)
                        {
                            DataRow[] dr = dt.Select($@" SHOW_DESC = '{radTextBox_CamIP.Text}' ");
                            if (dr.Length > 0)
                            {
                                MessageBox.Show("ข้อมูลนี้มีอยู่แล้ว ไม่ต้องเพิ่มซ้ำอีก.",
                                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                ClearData();
                                return;
                            }
                        }
                    }
                    radTextBox_Name.Text = "ขนส่ง1999"; radTextBox_Name.Enabled = false;
                    radLabel_NameShow.Text = "ขนส่ง1999";
                    radTextBox_Sim.Enabled = true; radTextBox_Sim.Focus();
                }
            }
        }
        //Sim
        private void RadTextBox_Sim_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Sim.Text == "") return;

                radTextBox_Sim.Enabled = false;
                radTextBox_Wifi.Enabled = true;
                radTextBox_Wifi.Focus();
            }
        }
        //Wifi
        private void RadTextBox_Wifi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Wifi.Text == "") return;

                radTextBox_Wifi.Enabled = false;
                radCheckBox_Sta.Enabled = true;
                radButton_Save.Enabled = true;
                radButton_Save.Focus();
            }
        }
    }
}
