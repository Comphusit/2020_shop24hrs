﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_Check
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_Check));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_Round = new Telerik.WinControls.UI.RadLabel();
            this.radDomainUpDown_Round = new Telerik.WinControls.UI.RadDomainUpDown();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radDropDown_Area = new Telerik.WinControls.UI.RadDropDownList();
            this.radCheckBox_Check = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_Check = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDown_Cam = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Round)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDomainUpDown_Round)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Area)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Cam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radLabel_Round);
            this.radPanel1.Controls.Add(this.radDomainUpDown_Round);
            this.radPanel1.Controls.Add(this.radStatusStrip1);
            this.radPanel1.Controls.Add(this.radDropDown_Area);
            this.radPanel1.Controls.Add(this.radCheckBox_Check);
            this.radPanel1.Controls.Add(this.radButton_Search);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.radDateTimePicker_Check);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.radDropDown_Cam);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(587, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(194, 555);
            this.radPanel1.TabIndex = 8;
            // 
            // radLabel_Round
            // 
            this.radLabel_Round.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Round.Location = new System.Drawing.Point(10, 243);
            this.radLabel_Round.Name = "radLabel_Round";
            this.radLabel_Round.Size = new System.Drawing.Size(57, 19);
            this.radLabel_Round.TabIndex = 57;
            this.radLabel_Round.Text = "รอบตรวจ";
            // 
            // radDomainUpDown_Round
            // 
            this.radDomainUpDown_Round.Location = new System.Drawing.Point(10, 273);
            this.radDomainUpDown_Round.Name = "radDomainUpDown_Round";
            this.radDomainUpDown_Round.Size = new System.Drawing.Size(61, 20);
            this.radDomainUpDown_Round.TabIndex = 56;
            this.radDomainUpDown_Round.Text = "0";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_excel,
            this.commandBarSeparator2});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.ToolTipText = "Export To Excel";
            this.radButtonElement_excel.UseCompatibleTextRendering = false;
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radDropDown_Area
            // 
            this.radDropDown_Area.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Area.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Area.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDown_Area.Location = new System.Drawing.Point(10, 154);
            this.radDropDown_Area.Name = "radDropDown_Area";
            this.radDropDown_Area.Size = new System.Drawing.Size(175, 21);
            this.radDropDown_Area.TabIndex = 39;
            this.radDropDown_Area.Text = "ทั้งหมด";
            // 
            // radCheckBox_Check
            // 
            this.radCheckBox_Check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox_Check.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Check.Location = new System.Drawing.Point(22, 45);
            this.radCheckBox_Check.Name = "radCheckBox_Check";
            this.radCheckBox_Check.Size = new System.Drawing.Size(126, 19);
            this.radCheckBox_Check.TabIndex = 42;
            this.radCheckBox_Check.Text = " เฉพาะที่ยังไม่ตรวจ";
            this.radCheckBox_Check.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radButton_Search
            // 
            this.radButton_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Search.Location = new System.Drawing.Point(6, 311);
            this.radButton_Search.Name = "radButton_Search";
            this.radButton_Search.Size = new System.Drawing.Size(185, 33);
            this.radButton_Search.TabIndex = 41;
            this.radButton_Search.Text = "ค้นหา";
            this.radButton_Search.ThemeName = "Fluent";
            this.radButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(10, 129);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(34, 19);
            this.radLabel1.TabIndex = 40;
            this.radLabel1.Text = "พื้นที่";
            // 
            // radDateTimePicker_Check
            // 
            this.radDateTimePicker_Check.CustomFormat = "";
            this.radDateTimePicker_Check.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_Check.Location = new System.Drawing.Point(10, 209);
            this.radDateTimePicker_Check.Name = "radDateTimePicker_Check";
            this.radDateTimePicker_Check.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_Check.TabIndex = 38;
            this.radDateTimePicker_Check.TabStop = false;
            this.radDateTimePicker_Check.Text = "วันอังคารที่ 21 เมษายน 2020";
            this.radDateTimePicker_Check.Value = new System.DateTime(2020, 4, 21, 0, 0, 0, 0);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(8, 72);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(38, 19);
            this.radLabel3.TabIndex = 36;
            this.radLabel3.Text = "กล้อง";
            // 
            // radDropDown_Cam
            // 
            this.radDropDown_Cam.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Cam.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Cam.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDown_Cam.Location = new System.Drawing.Point(10, 97);
            this.radDropDown_Cam.Name = "radDropDown_Cam";
            this.radDropDown_Cam.Size = new System.Drawing.Size(175, 21);
            this.radDropDown_Cam.TabIndex = 33;
            this.radDropDown_Cam.Text = "ทั้งหมด";
            this.radDropDown_Cam.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDown_Cam_SelectedIndexChanged);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(8, 184);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(59, 19);
            this.radLabel4.TabIndex = 32;
            this.radLabel4.Text = "วันที่ตรวจ";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(240)))), ((int)(((byte)(249)))));
            this.radGridView_Show.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.ForeColor = System.Drawing.Color.Black;
            this.radGridView_Show.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.AllowColumnReorder = false;
            this.radGridView_Show.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            this.radGridView_Show.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView_Show.Size = new System.Drawing.Size(578, 555);
            this.radGridView_Show.TabIndex = 0;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // Camera_Check
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Camera_Check";
            this.Text = "Camera_Check";
            this.Load += new System.EventHandler(this.Camera_Check_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Round)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDomainUpDown_Round)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Area)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Cam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Area;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Check;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Cam;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        protected Telerik.WinControls.UI.RadButton radButton_Search;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Check;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadLabel radLabel_Round;
        private Telerik.WinControls.UI.RadDomainUpDown radDomainUpDown_Round;
    }
}