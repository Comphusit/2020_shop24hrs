﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using HikvisionPreview;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_IEMain : Telerik.WinControls.UI.RadForm
    {
        //string pLogOut;
        public string pCAM_REMARKCHANNEL;
        public string ptypeCamOpen;

        public string pChange;
        //string Userid ;
        //string Password ;
        string pSwitch;
        readonly string _pTypeOpen; // เปิดจากหน้าจอตรวจกล้อง หรือ JOB หรือกล้องหลัก 0/ตรวจกล้อง       1/กล้องหลัก /JOB
        readonly string _pUserpermis;
        readonly string _pUser;
        readonly string _pPassword;

        readonly DATACAM_IP _pdATACAM_IP = new DATACAM_IP();
        DATACAMERADETAIL dATACAMERADETAIL;

        ShowCamMain singleForm;// = new ShowCam();

        public Camera_IEMain(DATACAM_IP pdATACAM_IP, string pTypeOpen, string pUser, string pPassword)
        {
            InitializeComponent();
            _pdATACAM_IP = pdATACAM_IP;
            _pUserpermis = _pdATACAM_IP.Userpermis;
            _pTypeOpen = pTypeOpen;
            _pUser = pUser;
            _pPassword = pPassword;
        }
        //Load Main
        private void Camera_IEMain_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";


            //SetUserPassword();
            radStatusStrip1.SizingGrip = false;
            radButtonElement_loginAdmin.ShowBorder = true; radButtonElement_loginAdmin.ToolTipText = "Login หน้าขาว [Enter 2 ครั้ง]";
            radButtonElement_Link.ShowBorder = true; radButtonElement_Link.ToolTipText = "เปิดกับ IE";
            radButtonElement_loginPhusit.ShowBorder = true; radButtonElement_loginPhusit.ToolTipText = "Login หน้าดำ [Enter 1 ครั้ง]";
            RadButtonElement_Check.ShowBorder = true; RadButtonElement_Check.ToolTipText = "เช็คจ๊อบ";
            radButtonElement_Cap.Enabled = true; radButtonElement_Cap.ShowBorder = true; radButtonElement_Cap.ToolTipText = "จับภาพสำหรับบันทึกข้อมูลหน้ากล้อง";
            radButtonElement_live.ShowBorder = true; radButtonElement_live.ToolTipText = "เปิดกล้องออนไลน์";
            radButtonElement_CapJOB.ShowBorder = true; radButtonElement_CapJOB.ToolTipText = "จับภาพเข้า JOB";

            radButtonElement_Edit.Enabled = true; radButtonElement_Edit.ToolTipText = "ตั้งค่ากล้อง";
            radButtonElement_Edit.ShowBorder = true;

            radButtonElement_Reboot.ShowBorder = true; radButtonElement_Reboot.ToolTipText = "Reboot";
            radButtonElement_LOGOUT.ShowBorder = true; radButtonElement_LOGOUT.ToolTipText = "ออก | เข้า ระบบ";


            this.CheckCamChackday();
            if (_pTypeOpen == "0")
            {
                RadButtonElement_Check.Enabled = true; RadButtonElement_Check.ToolTipText = "ตรวจกล้อง";
            }
            else
            {
                RadButtonElement_Check.Enabled = false;
            }

            if (_pdATACAM_IP.JobNumber == "")
            {
                radButtonElement_CapJOB.Enabled = false;
            }
            else
            {
                radButtonElement_CapJOB.Enabled = true;
                RadButtonElement_Check.Enabled = true;
            }

            switch (_pUserpermis)
            {
                case "0":
                    if (SystemClass.SystemComMinimart != "1")
                    {
                        radButtonElement_Cap.Enabled = false;
                        radButtonElement_CapJOB.Enabled = false;
                        radButtonElement_Edit.Enabled = false;
                    }
                    break;
                case "1": //center
                    if (_pTypeOpen == "0")
                    {
                        if (_pdATACAM_IP.CamStation != "")
                        {
                            radButtonElement_CapJOB.ToolTipText = "จับภาพเข้าบันทึกกล้องประจำจุด";
                            if (dATACAMERADETAIL.CAMCHECKEMP != "") radButtonElement_CapJOB.Enabled = true;
                            else radButtonElement_CapJOB.Enabled = false;
                        }

                    }
                    radButtonElement_Edit.Enabled = true;
                    radButtonElement_Cap.Enabled = false;

                    break;
                case "2": //รปภ 
                    radButtonElement_Cap.Enabled = false;
                    radButtonElement_CapJOB.Enabled = false;
                    radButtonElement_Edit.Enabled = false;
                    break;
                case "3": //ทั่วไป
                    radButtonElement_Cap.Enabled = false;
                    radButtonElement_CapJOB.Enabled = false;
                    radButtonElement_Edit.Enabled = false;
                    break;
                case "4": //โก้
                    RadButtonElement_Check.Enabled = false;
                    radButtonElement_CapJOB.Enabled = false;
                    radButtonElement_Cap.Enabled = false;
                    radButtonElement_Edit.Enabled = false;
                    break;
            }

            if ((_pdATACAM_IP.CamDVR == "0") || (_pdATACAM_IP.CamDVR == "2") || (_pdATACAM_IP.CamDVR == "3"))
            {
                radButtonElement_loginAdmin.Enabled = false;
                radButtonElement_loginPhusit.Enabled = false;
                radButtonElement_live.Enabled = true;
                pSwitch = "0";
                ShowLive();
            }
            else // หน้าดำ
            {
                radLabel_F2.Text = _pdATACAM_IP.CamDesc + "  " + _pdATACAM_IP.BranchID + "-" + _pdATACAM_IP.BranchName + " [" + _pdATACAM_IP.IPCam + "] ";
                radButtonElement_Reboot.Enabled = false;
                radButtonElement_LOGOUT.Enabled = false;

                radButtonElement_loginAdmin.Enabled = false;
                radButtonElement_loginPhusit.Enabled = true;
                radButtonElement_live.Enabled = false;
                webBrowser1.Visible = true;
                try
                {
                    panel1.Controls.Add(webBrowser1);
                    webBrowser1.Navigate(_pdATACAM_IP.IPCam);
                    webBrowser1.Dock = DockStyle.Fill;
                    this.WindowState = FormWindowState.Maximized;
                }
                catch (Exception)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถเปิดกล้องได้ เช็คระบบใหม่อีกครั้ง");
                    return;
                }
            }

            if (SystemClass.SystemBranchID != "MN000")
            {
                radButtonElement_Reboot.Enabled = false;
                radButtonElement_Edit.Enabled = false;
                radButtonElement_CapJOB.Enabled = false;
                radButtonElement_Cap.Enabled = false;
            }

        }
        //
        void ShowLive()
        {
            this.Cursor = Cursors.WaitCursor;
            if (pSwitch == "0")
            {
                string pPath = ""; string pFileName = ""; string sqlUp = "";
                switch (_pUserpermis)
                {
                    case "0":
                        if (_pdATACAM_IP.JobNumber != "")
                        {
                            string pM = "20" + _pdATACAM_IP.JobNumber.Substring(4, 2) + "-" + _pdATACAM_IP.JobNumber.Substring(6, 2);
                            string pD = "20" + _pdATACAM_IP.JobNumber.Substring(4, 2) + "-" + _pdATACAM_IP.JobNumber.Substring(6, 2) + "-" + _pdATACAM_IP.JobNumber.Substring(8, 2);
                            pPath = PathImageClass.pPathJOB + pM + @"\" + pD + @"\" + _pdATACAM_IP.JobNumber + @"\";
                            pFileName = _pdATACAM_IP.JobNumber + "-" + _pdATACAM_IP.BranchID + "-" +
                                   Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                                   Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";
                            sqlUp = " UPDATE  SHOP_JOBComMinimart SET JOB_STAIMG = '1' WHERE JOB_Number = '" + _pdATACAM_IP.JobNumber + "' ";
                        }
                        break;
                    case "1":
                        if (_pTypeOpen == "0" && _pdATACAM_IP.CamStation != "")
                        {
                            pPath = string.Format(@"\\192.168.100.77\ImageMinimark\CAMERACHECK\{0}", DateTime.Now.ToString("yyyy-MM-dd"));
                            pFileName = string.Format(@"\{0}-{1}-{2}-{3}-{4:yyyyMMdd}-{4:HHmmss}-{5}.jpg"
                                            , _pdATACAM_IP.CamID
                                            , _pdATACAM_IP.BranchID
                                            , _pdATACAM_IP.CamStation
                                            , _pdATACAM_IP.CamCount
                                            , DateTime.Now
                                            , SystemClass.SystemUserID);

                            sqlUp = string.Format(@"UPDATE  SHOP_JOBCAMCHECK
                                        SET [PATH_IMAGE] = '{0}',[PATH_WHOINS] = '{1}',[PATH_DATEINS] = GETDATE() 
                                            WHERE [CAM_ID] = '{2}' AND [CAM_DATECHECK] = '{3}' AND [CHECK_STATION] = '{4}' AND [COUNT] = '{5}'"
                                    , pPath + pFileName
                                    , SystemClass.SystemUserID
                                    , _pdATACAM_IP.CamID
                                    , DateTime.Now.ToString("yyyy-MM-dd")
                                    , _pdATACAM_IP.CamStation
                                    , _pdATACAM_IP.CamCount);
                        }
                        break;
                    default:
                        break;
                }

                singleForm = new ShowCamMain
                {
                    pJOBMN = sqlUp + "|" + pPath + "|" + pFileName,
                    typeCam = ptypeCamOpen,
                    dvrIp = _pdATACAM_IP.IPCam.ToString().Replace("http://", "").Replace("/", ""),
                    bchID = _pdATACAM_IP.BranchID,
                    bchName = _pdATACAM_IP.BranchName,
                    DVR_User = _pUser,// Userid,
                    DVR_Password = _pPassword,// Password,
                    CAM_CH = _pdATACAM_IP.CamCH,
                    pCAM_REMARKCHANNEL = pCAM_REMARKCHANNEL,
                    TopLevel = false,
                    AutoScroll = true,
                    Dock = DockStyle.Fill,
                    FormBorderStyle = FormBorderStyle.None
                };

                panel1.Controls.Clear();
                panel1.Controls.Add(singleForm);
                panel1.Dock = DockStyle.Fill;
                singleForm.Show();

                radLabel_F2.Text = _pdATACAM_IP.BranchID + "-" + _pdATACAM_IP.BranchName + " [" + _pdATACAM_IP.IPCam + "] " +
                    " >> " + singleForm.Desc + " >> " + singleForm.DescDateTime;

                radButtonElement_loginAdmin.Enabled = false;
                radButtonElement_live.ToolTipText = "Live Preview IE"; pSwitch = "1";
                this.Cursor = Cursors.Default;
            }
            else // ต้องการดูแบบ IE
            {
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    try
                    {
                        singleForm.Close();
                    }
                    catch (Exception) { }
                    panel1.Controls.Clear();
                    panel1.Controls.Add(webBrowser1);
                    webBrowser1.Navigate(_pdATACAM_IP.IPCam);
                    webBrowser1.Visible = true;
                    webBrowser1.Dock = DockStyle.Fill;
                    radButtonElement_live.Enabled = true;

                    radButtonElement_loginAdmin.Enabled = true;
                    radButtonElement_live.ToolTipText = "Live Preview "; pSwitch = "0";
                }
                catch (Exception)
                { return; }
                this.Cursor = Cursors.Default;
            }

        }
        //หน้าขาว
        private void RadButtonElement_loginAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                webBrowser1.Document.GetElementById("username").InnerText = _pUser;// Userid;
                webBrowser1.Document.GetElementById("password").SetAttribute("value", _pPassword);
                webBrowser1.Document.GetElementById("password").Focus();
                webBrowser1.Document.InvokeScript("login");
            }
            catch (Exception)
            { return; }
        }
        //หน้าดำ
        private void RadButtonElement_loginPhusit_Click(object sender, EventArgs e)
        {
            try
            {
                webBrowser1.Document.GetElementById("username").InnerText = _pUser;
                webBrowser1.Document.GetElementById("password").SetAttribute("value", _pPassword);
                webBrowser1.Document.GetElementById("password").Focus();
                webBrowser1.Document.InvokeScript("login");
            }
            catch (Exception)
            { return; }
        }
        //open IE
        private void RadButtonElement_Link_Click(object sender, EventArgs e)
        {
            try
            { Process.Start("iexplore.exe", _pdATACAM_IP.IPCam); }
            catch (Exception)
            { return; }
        }

        private void RadButtonElement_Check_Click(object sender, EventArgs e)
        {
            if (_pTypeOpen == "0")
            {
                Camera_CheckDetail frmCam_CheckDetail = new Camera_CheckDetail(_pUserpermis, DateTime.Now.ToString("yyyy-MM-dd")
                    , _pdATACAM_IP.CamID, _pdATACAM_IP.CamStation, _pdATACAM_IP.CamCount);
                if (frmCam_CheckDetail.ShowDialog(this) == DialogResult.OK)
                {
                    this.CheckCamChackday();
                    if (dATACAMERADETAIL.CAMCHECKEMP != "")
                    {
                        radButtonElement_CapJOB.Enabled = true;
                    }
                    return;
                }
            }
            else
            {
                string TypeOpenJOB = "SHOP";
                if (_pdATACAM_IP.BranchID == "MN000") TypeOpenJOB = "SUPC";

                JOB.Com.JOBCOM_EDIT frmJOB_Edit = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComMinimart", "00001", "ComMinimart", _pUserpermis, TypeOpenJOB, _pdATACAM_IP.JobNumber);
                if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK)
                {

                }
            }

        }
        //Cap
        private void RadButtonElement_Cap_Click(object sender, EventArgs e)
        {
            if (_pUserpermis == "0") CapCamMain(); 
        }
        void CapCamMain()
        {
            try
            {
                string pPath = PathImageClass.pPathCAMERA + _pdATACAM_IP.BranchID;
                string pFileName = string.Format(@"\{0}-{1}-{2}.jpg"
                    , _pdATACAM_IP.CamID
                    , _pdATACAM_IP.BranchID
                    , _pdATACAM_IP.IPCam.Replace("http://", "").Replace(".", "_").Replace("/", "").Replace(":81", "").Replace(":82", ""));

                if (Directory.Exists(pPath) == false) { Directory.CreateDirectory(pPath); }

                if (File.Exists(pPath + pFileName) == true) File.Delete(pPath + pFileName);

                Rectangle form = this.Bounds;
                Bitmap bitmap = new Bitmap(form.Width, form.Height);
                Graphics graphic = Graphics.FromImage(bitmap);
                graphic.CopyFromScreen(form.Location, Point.Empty, form.Size);

                bitmap.Save(pPath + pFileName);

                //resized picture
                FitImage(pPath + pFileName, 1200, 600, pFileName);
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกหน้าจอกล้องหลักเรียบร้อย");
            }
            catch (Exception ex)
            {

                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถใช้งาน ฟังก์ชั่นนี้ ได้ใหม่อีกครั้ง{Environment.NewLine }[{ex.Message }]");
                return;
            }
        }
        void CapStation()
        {
            try
            {
                string pPath = string.Format(@"\\192.168.100.77\ImageMinimark\CAMERACHECK\{0}", DateTime.Now.ToString("yyyy-MM-dd"));
                string pFileName = string.Format(@"\{0}-{1}-{2}-{3}-{4:yyyyMMdd}-{4:HHmmss}-{5}.jpg"
                                , _pdATACAM_IP.CamID
                                , _pdATACAM_IP.BranchID
                                , _pdATACAM_IP.CamStation
                                , _pdATACAM_IP.CamCount
                                , DateTime.Now
                                , SystemClass.SystemUserID);

                if (Directory.Exists(pPath) == false) { Directory.CreateDirectory(pPath); }

                Rectangle form = this.Bounds;
                Bitmap bitmap = new Bitmap(form.Width, form.Height);
                Graphics graphic = Graphics.FromImage(bitmap);
                graphic.CopyFromScreen(form.Location, Point.Empty, form.Size);
                bitmap.Save(pPath + pFileName);

                //resized picture
                FitImage(pPath + pFileName, 1200, 600, pFileName);
                //pFileName.Substring(0, pFileName.Length - 3) + "_New.jpg"
                string sql = string.Format(@"UPDATE  SHOP_JOBCAMCHECK
                            SET 
                                [PATH_IMAGE] = '{0}'
                                ,[PATH_WHOINS] = '{1}'
                                ,[PATH_DATEINS] = GETDATE() 
                                WHERE [CAM_ID] = '{2}' AND [CAM_DATECHECK] = '{3}' AND [CHECK_STATION] = '{4}' AND [COUNT] = '{5}'"
                        , pPath + pFileName
                        , SystemClass.SystemUserID
                        , _pdATACAM_IP.CamID
                        , DateTime.Now.ToString("yyyy-MM-dd")
                        , _pdATACAM_IP.CamStation
                        , _pdATACAM_IP.CamCount);

                string returnExc = Controllers.ConnectionClass.ExecuteSQL_Main(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถใช้งาน ฟังก์ชั่นนี้ ได้ใหม่อีกครั้ง{Environment.NewLine}[{ex.Message}]");
                return;
            }
        }
        //Close
        private void Camera_IE_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                singleForm.Close();
            }
            catch (Exception)
            {
                return;
            }
        }
        //Live
        private void RadButtonElement_live_Click(object sender, EventArgs e)
        {
            ShowLive();
        }

        private void RadButtonElement_CapJOB_Click(object sender, EventArgs e)
        {
            if (_pUserpermis == "1" && _pTypeOpen == "0") //centercapกล้องประจำจุด
            {
                CapStation();
            }
            else
            {
                try
                {
                    //MJOB190402000014
                    string pM = "20" + _pdATACAM_IP.JobNumber.Substring(4, 2) + "-" + _pdATACAM_IP.JobNumber.Substring(6, 2);
                    string pD = "20" + _pdATACAM_IP.JobNumber.Substring(4, 2) + "-" + _pdATACAM_IP.JobNumber.Substring(6, 2) + "-" + _pdATACAM_IP.JobNumber.Substring(8, 2);
                    string pPath = PathImageClass.pPathJOB + pM + @"\" + pD + @"\" + _pdATACAM_IP.JobNumber + @"\";
                    string pFileName = _pdATACAM_IP.JobNumber + "-" + _pdATACAM_IP.BranchID + "-" +
                           Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "")) + "-" +
                           Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) + "-" + SystemClass.SystemUserID + ".jpg";

                    if (Directory.Exists(pPath) == false) { Directory.CreateDirectory(pPath); }

                    Rectangle form = this.Bounds;
                    Bitmap bitmap = new Bitmap(form.Width, form.Height);
                    Graphics graphic = Graphics.FromImage(bitmap);
                    graphic.CopyFromScreen(form.Location, Point.Empty, form.Size);
                    bitmap.Save(pPath + pFileName);
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกหน้าจอเข้า JOB เรียบร้อย");
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถใช้งาน ฟังก์ชั่นนี้ ได้ใหม่อีกครั้ง{Environment.NewLine}[{ex.Message}]");
                    return;
                }
            }
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            Camera_Detail Camera_Detail = new Camera_Detail(this.Text + " :: แก้ไข", _pdATACAM_IP.CamID, _pUserpermis);
            if (Camera_Detail.ShowDialog(this) == DialogResult.OK) return;
        }
        public void FitImage(string pPath, int pNewWidth, int pMaxHeight, string pNewName)
        {
            Image FullsizeImage = Image.FromFile(pPath);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

            int NewHeight = FullsizeImage.Height * pNewWidth / FullsizeImage.Width;
            if (NewHeight > pMaxHeight)
            {
                // Resize with height instead
                pNewWidth = FullsizeImage.Width * pMaxHeight / FullsizeImage.Height;
                NewHeight = pMaxHeight;
            }

            Bitmap bmp = new Bitmap(pNewWidth, NewHeight,
                    System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (Graphics gr = Graphics.FromImage(bmp))
                gr.DrawImage(FullsizeImage, new Rectangle(0, 0, bmp.Width, bmp.Height));

            // Exclude the filename from the the path
            string path = pPath.Substring(0, pPath.LastIndexOf(@"\"));

            // Save resized picture
            bmp.Save(path + pNewName);
            bmp.Dispose();
        }
        void CheckCamChackday()
        {
            dATACAMERADETAIL = new DATACAMERADETAIL(_pUserpermis, _pdATACAM_IP.CamDate, _pdATACAM_IP.CamID, _pdATACAM_IP.CamStation, _pdATACAM_IP.CamCount);
        }

        private void RadButtonElement_Reboot_Click(object sender, EventArgs e)
        {
            if (singleForm.Reboot() == true) pSwitch = "0";
        }
        //LogOut
        private void RadButtonElement_LOGOUT_Click(object sender, EventArgs e)
        {
            if (radButtonElement_LOGOUT.Text == "LOGIN")
            {
                ShowLive();
                radButtonElement_LOGOUT.Text = "LOGOUT";
            }
            else
            {
                singleForm.SetLogout();
                pSwitch = "0";
                try
                {
                    singleForm.Close();
                }
                catch (Exception) { }
                radButtonElement_LOGOUT.Text = "LOGIN";
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeOpen);
        }

    }
}
