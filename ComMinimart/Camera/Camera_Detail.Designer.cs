﻿namespace PC_Shop24Hrs.ComMinimart.Camera
{
    partial class Camera_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Camera_Detail));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Close = new Telerik.WinControls.UI.RadButton();
            this.radGroupBox_Cam = new Telerik.WinControls.UI.RadGroupBox();
            this.RadTextBox_Emp1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Weekdays = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Dvr = new Telerik.WinControls.UI.RadDropDownList();
            this.DVR = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_Amor = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Check = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox_Poin = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Addrow = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Station = new Telerik.WinControls.UI.RadDropDownList();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDown_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.radRadio_Byday = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadio_CheckEvery = new Telerik.WinControls.UI.RadRadioButton();
            this.radTextBox_Location = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox_Cam = new System.Windows.Forms.PictureBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDown_Ch = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_EmpName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Link = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDown_Dpt = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDown_Area = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_Emp = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Cam)).BeginInit();
            this.radGroupBox_Cam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Emp1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Weekdays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dvr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Amor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Check)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Poin)).BeginInit();
            this.radGroupBox_Poin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Addrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Station)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio_Byday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio_CheckEvery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Cam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Ch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Link)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Area)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radButton_Save);
            this.radPanel1.Controls.Add(this.radButton_Close);
            this.radPanel1.Controls.Add(this.radGroupBox_Cam);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(868, 586);
            this.radPanel1.TabIndex = 0;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Location = new System.Drawing.Point(360, 539);
            this.radButton_Save.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(90, 35);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            this.radButton_Save.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Close
            // 
            this.radButton_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Close.Location = new System.Drawing.Point(456, 539);
            this.radButton_Close.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_Close.Name = "radButton_Close";
            this.radButton_Close.Size = new System.Drawing.Size(90, 35);
            this.radButton_Close.TabIndex = 4;
            this.radButton_Close.Text = "ยกเลิก";
            this.radButton_Close.ThemeName = "Fluent";
            this.radButton_Close.Click += new System.EventHandler(this.RadButton_Close_Click);
            this.radButton_Close.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Close.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Close.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radGroupBox_Cam
            // 
            this.radGroupBox_Cam.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Cam.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_Cam.Controls.Add(this.RadTextBox_Emp1);
            this.radGroupBox_Cam.Controls.Add(this.radLabel10);
            this.radGroupBox_Cam.Controls.Add(this.radDropDownList_Weekdays);
            this.radGroupBox_Cam.Controls.Add(this.radDropDownList_Dvr);
            this.radGroupBox_Cam.Controls.Add(this.DVR);
            this.radGroupBox_Cam.Controls.Add(this.radCheckBox_Amor);
            this.radGroupBox_Cam.Controls.Add(this.radCheckBox_Check);
            this.radGroupBox_Cam.Controls.Add(this.radGroupBox_Poin);
            this.radGroupBox_Cam.Controls.Add(this.radTextBox_Remark);
            this.radGroupBox_Cam.Controls.Add(this.radLabel9);
            this.radGroupBox_Cam.Controls.Add(this.radDropDown_Branch);
            this.radGroupBox_Cam.Controls.Add(this.radRadio_Byday);
            this.radGroupBox_Cam.Controls.Add(this.radRadio_CheckEvery);
            this.radGroupBox_Cam.Controls.Add(this.radTextBox_Location);
            this.radGroupBox_Cam.Controls.Add(this.radLabel8);
            this.radGroupBox_Cam.Controls.Add(this.groupBox1);
            this.radGroupBox_Cam.Controls.Add(this.radLabel11);
            this.radGroupBox_Cam.Controls.Add(this.radDropDown_Ch);
            this.radGroupBox_Cam.Controls.Add(this.radLabel_EmpName);
            this.radGroupBox_Cam.Controls.Add(this.radLabel2);
            this.radGroupBox_Cam.Controls.Add(this.radTextBox_Desc);
            this.radGroupBox_Cam.Controls.Add(this.radTextBox_Link);
            this.radGroupBox_Cam.Controls.Add(this.radLabel4);
            this.radGroupBox_Cam.Controls.Add(this.radLabel5);
            this.radGroupBox_Cam.Controls.Add(this.radDropDown_Dpt);
            this.radGroupBox_Cam.Controls.Add(this.radDropDown_Area);
            this.radGroupBox_Cam.Controls.Add(this.radTextBox_Emp);
            this.radGroupBox_Cam.Controls.Add(this.radLabel6);
            this.radGroupBox_Cam.Controls.Add(this.radLabel7);
            this.radGroupBox_Cam.Controls.Add(this.radLabel3);
            this.radGroupBox_Cam.Controls.Add(this.radLabel1);
            this.radGroupBox_Cam.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_Cam.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.radGroupBox_Cam.HeaderText = "";
            this.radGroupBox_Cam.Location = new System.Drawing.Point(14, 13);
            this.radGroupBox_Cam.Name = "radGroupBox_Cam";
            this.radGroupBox_Cam.Padding = new System.Windows.Forms.Padding(2, 19, 2, 2);
            this.radGroupBox_Cam.Size = new System.Drawing.Size(842, 517);
            this.radGroupBox_Cam.TabIndex = 1;
            this.radGroupBox_Cam.ThemeName = "Fluent";
            // 
            // RadTextBox_Emp1
            // 
            this.RadTextBox_Emp1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_Emp1.Location = new System.Drawing.Point(131, 326);
            this.RadTextBox_Emp1.Name = "RadTextBox_Emp1";
            this.RadTextBox_Emp1.Size = new System.Drawing.Size(243, 21);
            this.RadTextBox_Emp1.TabIndex = 8;
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(25, 327);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(58, 19);
            this.radLabel10.TabIndex = 82;
            this.radLabel10.Text = "ช่างพม่า";
            // 
            // radDropDownList_Weekdays
            // 
            this.radDropDownList_Weekdays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radDropDownList_Weekdays.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Weekdays.DropDownHeight = 124;
            this.radDropDownList_Weekdays.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Weekdays.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Weekdays.Location = new System.Drawing.Point(300, 461);
            this.radDropDownList_Weekdays.Name = "radDropDownList_Weekdays";
            this.radDropDownList_Weekdays.Size = new System.Drawing.Size(120, 21);
            this.radDropDownList_Weekdays.TabIndex = 81;
            // 
            // radDropDownList_Dvr
            // 
            this.radDropDownList_Dvr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dvr.DropDownHeight = 124;
            this.radDropDownList_Dvr.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Dvr.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Dvr.Location = new System.Drawing.Point(277, 262);
            this.radDropDownList_Dvr.Name = "radDropDownList_Dvr";
            this.radDropDownList_Dvr.Size = new System.Drawing.Size(143, 21);
            this.radDropDownList_Dvr.TabIndex = 80;
            // 
            // DVR
            // 
            this.DVR.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DVR.Location = new System.Drawing.Point(234, 262);
            this.DVR.Name = "DVR";
            this.DVR.Size = new System.Drawing.Size(36, 19);
            this.DVR.TabIndex = 79;
            this.DVR.Text = "DVR";
            // 
            // radCheckBox_Amor
            // 
            this.radCheckBox_Amor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radCheckBox_Amor.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radCheckBox_Amor.Location = new System.Drawing.Point(234, 488);
            this.radCheckBox_Amor.Name = "radCheckBox_Amor";
            this.radCheckBox_Amor.Size = new System.Drawing.Size(45, 19);
            this.radCheckBox_Amor.TabIndex = 13;
            this.radCheckBox_Amor.Text = "อมร";
            this.radCheckBox_Amor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radCheckBox_Check
            // 
            this.radCheckBox_Check.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radCheckBox_Check.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radCheckBox_Check.Location = new System.Drawing.Point(131, 488);
            this.radCheckBox_Check.Name = "radCheckBox_Check";
            this.radCheckBox_Check.Size = new System.Drawing.Size(91, 19);
            this.radCheckBox_Check.TabIndex = 12;
            this.radCheckBox_Check.Text = "กำหนดสิทธิ์";
            this.radCheckBox_Check.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radGroupBox_Poin
            // 
            this.radGroupBox_Poin.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Poin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_Poin.Controls.Add(this.radLabel14);
            this.radGroupBox_Poin.Controls.Add(this.radButton_Addrow);
            this.radGroupBox_Poin.Controls.Add(this.radDropDownList_Station);
            this.radGroupBox_Poin.Controls.Add(this.radGridView_Show);
            this.radGroupBox_Poin.HeaderText = "";
            this.radGroupBox_Poin.Location = new System.Drawing.Point(435, 227);
            this.radGroupBox_Poin.Name = "radGroupBox_Poin";
            this.radGroupBox_Poin.Size = new System.Drawing.Size(396, 280);
            this.radGroupBox_Poin.TabIndex = 17;
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel14.Location = new System.Drawing.Point(5, 23);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(96, 19);
            this.radLabel14.TabIndex = 93;
            this.radLabel14.Text = "กล้องประจำจุด";
            // 
            // radButton_Addrow
            // 
            this.radButton_Addrow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Addrow.Location = new System.Drawing.Point(258, 21);
            this.radButton_Addrow.Name = "radButton_Addrow";
            this.radButton_Addrow.Size = new System.Drawing.Size(74, 24);
            this.radButton_Addrow.TabIndex = 91;
            this.radButton_Addrow.Text = "เพิ่ม";
            this.radButton_Addrow.Click += new System.EventHandler(this.RadButton_Addrow_Click);
            // 
            // radDropDownList_Station
            // 
            this.radDropDownList_Station.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Station.DropDownHeight = 180;
            this.radDropDownList_Station.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Station.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Station.Location = new System.Drawing.Point(107, 23);
            this.radDropDownList_Station.Name = "radDropDownList_Station";
            this.radDropDownList_Station.Size = new System.Drawing.Size(131, 21);
            this.radDropDownList_Station.TabIndex = 90;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(2, 60);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(392, 218);
            this.radGridView_Show.TabIndex = 89;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_Show_CellBeginEdit);
            this.radGridView_Show.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellEndEdit);
            this.radGridView_Show.CellValidated += new Telerik.WinControls.UI.CellValidatedEventHandler(this.RadGridView_Show_CellValidated);
            this.radGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.Location = new System.Drawing.Point(131, 358);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Remark.Size = new System.Drawing.Size(243, 90);
            this.radTextBox_Remark.TabIndex = 9;
            this.radTextBox_Remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel9
            // 
            this.radLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(25, 358);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(65, 19);
            this.radLabel9.TabIndex = 78;
            this.radLabel9.Text = "หมายเหตุ";
            // 
            // radDropDown_Branch
            // 
            this.radDropDown_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Branch.DropDownHeight = 124;
            this.radDropDown_Branch.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDown_Branch.Location = new System.Drawing.Point(131, 22);
            this.radDropDown_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDown_Branch.Name = "radDropDown_Branch";
            this.radDropDown_Branch.Size = new System.Drawing.Size(243, 21);
            this.radDropDown_Branch.TabIndex = 0;
            this.radDropDown_Branch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radDropDown_Branch.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDown_Branch_SelectedIndexChanged);
            // 
            // radRadio_Byday
            // 
            this.radRadio_Byday.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radRadio_Byday.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadio_Byday.Location = new System.Drawing.Point(234, 461);
            this.radRadio_Byday.Name = "radRadio_Byday";
            this.radRadio_Byday.Size = new System.Drawing.Size(60, 19);
            this.radRadio_Byday.TabIndex = 11;
            this.radRadio_Byday.TabStop = false;
            this.radRadio_Byday.Text = "ระบุวัน";
            this.radRadio_Byday.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadio_Byday_ToggleStateChanged);
            this.radRadio_Byday.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radRadio_CheckEvery
            // 
            this.radRadio_CheckEvery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radRadio_CheckEvery.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadio_CheckEvery.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadio_CheckEvery.Location = new System.Drawing.Point(131, 461);
            this.radRadio_CheckEvery.Name = "radRadio_CheckEvery";
            this.radRadio_CheckEvery.Size = new System.Drawing.Size(87, 19);
            this.radRadio_CheckEvery.TabIndex = 10;
            this.radRadio_CheckEvery.Text = "ตรวจทุกวัน";
            this.radRadio_CheckEvery.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadio_CheckEvery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Location
            // 
            this.radTextBox_Location.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Location.Location = new System.Drawing.Point(131, 223);
            this.radTextBox_Location.Name = "radTextBox_Location";
            this.radTextBox_Location.Size = new System.Drawing.Size(243, 21);
            this.radTextBox_Location.TabIndex = 5;
            this.radTextBox_Location.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(25, 225);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(33, 19);
            this.radLabel8.TabIndex = 72;
            this.radLabel8.Text = "ที่ตั้ง";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pictureBox_Cam);
            this.groupBox1.Controls.Add(this.radLabel13);
            this.groupBox1.Controls.Add(this.radLabel12);
            this.groupBox1.Location = new System.Drawing.Point(435, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 187);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รูปภาพ";
            // 
            // pictureBox_Cam
            // 
            this.pictureBox_Cam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_Cam.Location = new System.Drawing.Point(3, 19);
            this.pictureBox_Cam.Name = "pictureBox_Cam";
            this.pictureBox_Cam.Size = new System.Drawing.Size(390, 165);
            this.pictureBox_Cam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox_Cam.TabIndex = 66;
            this.pictureBox_Cam.TabStop = false;
            this.pictureBox_Cam.DoubleClick += new System.EventHandler(this.PictureBox_Cam_DoubleClick);
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel13.Location = new System.Drawing.Point(17, 142);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(33, 19);
            this.radLabel13.TabIndex = 94;
            this.radLabel13.Text = "ช่อง";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel12.Location = new System.Drawing.Point(16, 112);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(65, 19);
            this.radLabel12.TabIndex = 93;
            this.radLabel12.Text = "คำอธิบาย";
            // 
            // radLabel11
            // 
            this.radLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(25, 461);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(40, 19);
            this.radLabel11.TabIndex = 69;
            this.radLabel11.Text = "ตั้งค่า";
            // 
            // radDropDown_Ch
            // 
            this.radDropDown_Ch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Ch.DropDownHeight = 124;
            this.radDropDown_Ch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Ch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDown_Ch.Location = new System.Drawing.Point(131, 262);
            this.radDropDown_Ch.Name = "radDropDown_Ch";
            this.radDropDown_Ch.Size = new System.Drawing.Size(67, 21);
            this.radDropDown_Ch.TabIndex = 6;
            this.radDropDown_Ch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel_EmpName
            // 
            this.radLabel_EmpName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmpName.Location = new System.Drawing.Point(234, 297);
            this.radLabel_EmpName.Name = "radLabel_EmpName";
            this.radLabel_EmpName.Size = new System.Drawing.Size(30, 19);
            this.radLabel_EmpName.TabIndex = 8;
            this.radLabel_EmpName.Text = "คุณ";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(25, 297);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(31, 19);
            this.radLabel2.TabIndex = 54;
            this.radLabel2.Text = "ช่าง";
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Desc.Location = new System.Drawing.Point(131, 158);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Desc.Size = new System.Drawing.Size(243, 55);
            this.radTextBox_Desc.TabIndex = 4;
            this.radTextBox_Desc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Link
            // 
            this.radTextBox_Link.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Link.Location = new System.Drawing.Point(131, 119);
            this.radTextBox_Link.Name = "radTextBox_Link";
            this.radTextBox_Link.Size = new System.Drawing.Size(243, 21);
            this.radTextBox_Link.TabIndex = 3;
            this.radTextBox_Link.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(25, 160);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 19);
            this.radLabel4.TabIndex = 53;
            this.radLabel4.Text = "คำอธิบาย";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(25, 121);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(32, 19);
            this.radLabel5.TabIndex = 52;
            this.radLabel5.Text = "ลิ้งค์";
            // 
            // radDropDown_Dpt
            // 
            this.radDropDown_Dpt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Dpt.DropDownHeight = 124;
            this.radDropDown_Dpt.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Dpt.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Dpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDown_Dpt.Location = new System.Drawing.Point(131, 88);
            this.radDropDown_Dpt.Name = "radDropDown_Dpt";
            this.radDropDown_Dpt.Size = new System.Drawing.Size(243, 21);
            this.radDropDown_Dpt.TabIndex = 2;
            this.radDropDown_Dpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radDropDown_Dpt.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDown_Dpt_SelectedIndexChanged);
            // 
            // radDropDown_Area
            // 
            this.radDropDown_Area.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Area.DropDownHeight = 124;
            this.radDropDown_Area.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Area.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Area.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDown_Area.Location = new System.Drawing.Point(131, 56);
            this.radDropDown_Area.Name = "radDropDown_Area";
            this.radDropDown_Area.Size = new System.Drawing.Size(243, 21);
            this.radDropDown_Area.TabIndex = 1;
            this.radDropDown_Area.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radDropDown_Area.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDown_Area_SelectedIndexChanged);
            // 
            // radTextBox_Emp
            // 
            this.radTextBox_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Emp.Location = new System.Drawing.Point(131, 295);
            this.radTextBox_Emp.Name = "radTextBox_Emp";
            this.radTextBox_Emp.Size = new System.Drawing.Size(97, 21);
            this.radTextBox_Emp.TabIndex = 7;
            this.radTextBox_Emp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Emp_KeyDown);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(25, 262);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(81, 19);
            this.radLabel6.TabIndex = 44;
            this.radLabel6.Text = "จำนวนกล้อง";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(25, 88);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(43, 19);
            this.radLabel7.TabIndex = 42;
            this.radLabel7.Text = "แผนก";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(25, 56);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(36, 19);
            this.radLabel3.TabIndex = 36;
            this.radLabel3.Text = "พื้นที่";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(25, 22);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(39, 19);
            this.radLabel1.TabIndex = 34;
            this.radLabel1.Text = "สาขา";
            // 
            // Camera_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 586);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Camera_Detail";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Camera_Detail";
            this.Load += new System.EventHandler(this.Camera_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Cam)).EndInit();
            this.radGroupBox_Cam.ResumeLayout(false);
            this.radGroupBox_Cam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Emp1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Weekdays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dvr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DVR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Amor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Check)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Poin)).EndInit();
            this.radGroupBox_Poin.ResumeLayout(false);
            this.radGroupBox_Poin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Addrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Station)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio_Byday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadio_CheckEvery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Cam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Ch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Link)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Area)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Cam;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox_Cam;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Location;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadRadioButton radRadio_Byday;
        private Telerik.WinControls.UI.RadRadioButton radRadio_CheckEvery;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Branch;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Poin;
        public Telerik.WinControls.UI.RadTextBox radTextBox_Link;
        public Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        public Telerik.WinControls.UI.RadTextBox radTextBox_Emp;
        public Telerik.WinControls.UI.RadLabel radLabel_EmpName;
        public Telerik.WinControls.UI.RadDropDownList radDropDown_Ch;
        public Telerik.WinControls.UI.RadDropDownList radDropDown_Dpt;
        public Telerik.WinControls.UI.RadDropDownList radDropDown_Area;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Check;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Amor;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Close;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Dvr;
        private Telerik.WinControls.UI.RadLabel DVR;
        public Telerik.WinControls.UI.RadLabel radLabel13;
        public Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadButton radButton_Addrow;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Station;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Weekdays;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        public Telerik.WinControls.UI.RadTextBox RadTextBox_Emp1;
    }
}