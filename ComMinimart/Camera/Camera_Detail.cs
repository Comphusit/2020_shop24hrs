﻿//CheckOK
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.ComMinimart.Camera
{
    public partial class Camera_Detail : Form
    {
        public string ReturnExecut;
        public DATACAMERA DATAROWCAM;
        string[] items;
        string CamIP;
        string PathCAMIP = PathImageClass.pImageEmply;
        string PathImage = PathImageClass.pPathCAMERA;
        readonly string _pDesc;
        readonly string _pPermisUser;
        readonly string _pCamID;
        DataTable dtcb = new DataTable();
        DataTable dt = new DataTable();
        enum WeekDays
        {
            Sunday = 1,
            Monday = 2,
            Tuesday = 3,
            Wednesday = 4,
            Thursday = 5,
            Friday = 6,
            Saturday = 7
        }
        #region Fields 
        private readonly string[] columnTypes = new string[] {
            "ComboBox",
            "Text",
            "Command"
        };
        #endregion

        public Camera_Detail(string pDesc, string pCamID, string pPermisUser)
        {
            InitializeComponent();
            _pDesc = pDesc;
            _pCamID = pCamID;
            _pPermisUser = pPermisUser;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void CreateBindingList(int count)
        {
            items = new string[count];
            for (int i = 0; i < count; i++)
            {
                items[i] = (i + 1).ToString();
            }
        }
        private void CreateBindingList_dvr()
        {
            DataTable dtDvr = ConnectionClass.SelectSQL_Main($@"
                SELECT	CAM_DVR,CAM_DESC
                FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK)
                GROUP BY CAM_DVR,CAM_DESC
                ORDER BY CAM_DVR,CAM_DESC
            ");
            radDropDownList_Dvr.DataSource = dtDvr;
            radDropDownList_Dvr.ValueMember = "CAM_DVR";
            radDropDownList_Dvr.DisplayMember = "CAM_DESC";
            radDropDownList_Dvr.SelectedIndex = 0;
        }
        private void CreateBindingWeeldays()
        {
            // Create a table to store data for the DropDownList control.
            DataTable dt = new DataTable();
            // Define the columns of the table.
            dt.Columns.Add(new DataColumn("TextField", typeof(String)));
            dt.Columns.Add(new DataColumn("ValueField", typeof(String)));
            int i = 1;
            foreach (WeekDays w in (WeekDays[])Enum.GetValues(typeof(WeekDays)))
            {
                dt.Rows.Add(i, w); i++;
            }
            radDropDownList_Weekdays.DataSource = dt;
            radDropDownList_Weekdays.ValueMember = "TextField";
            radDropDownList_Weekdays.DisplayMember = "ValueField";
            radDropDownList_Weekdays.SelectedIndex = -1;
        }
        private void CreateBindingList_station()
        {
            string sqlcb = string.Format(@"
                    SELECT [SHOW_ID],[SHOW_NAME] 
                    FROM   [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK) WHERE [TYPE_CONFIG] = '21' AND STA = '1' ");
            dtcb = ConnectionClass.SelectSQL_Main(sqlcb);

            radDropDownList_Station.DataSource = dtcb;
            radDropDownList_Station.ValueMember = "SHOW_ID";
            radDropDownList_Station.DisplayMember = "SHOW_NAME";
            radDropDownList_Station.SelectedIndex = -1;

            radGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("ComboBox", "SHOW_ID", "คำอธิบาย", 120));
            radGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Text", "CHANNEL", "ช่อง", 150));
            radGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Command", "DELETE", "", 50));

            string sql = string.Format(@"
                    SELECT  [SHOP_JOBCAMSTATION].[SHOW_ID] AS [SHOW_ID]
                            ,[SHOW_NAME]
                            ,[CHANNEL]
                            ,'ลบ' AS [DELETE]
                    FROM    [dbo].[SHOP_JOBCAMSTATION]  WITH (NOLOCK) INNER JOIN 
                            [SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK) on  [SHOP_JOBCAMSTATION].SHOW_ID = [SHOP_CONFIGBRANCH_GenaralDetail].SHOW_ID
                    WHERE   [CAM_ID] = '{0}'", _pCamID);
            dt = ConnectionClass.SelectSQL_Main(sql);
            radGridView_Show.DataSource = dt;
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion
        //Begin Edit
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.Column == radGridView_Show.Columns[0]) { e.Cancel = true; }
                try
                {
                    if (radGridView_Show.CurrentCell.Value.ToString() == "X") { e.Cancel = true; }
                }
                catch (Exception) { return; }
            }
        }
        //EndEdit
        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            try
            {
                radGridView_Show.CurrentCell.Value = radGridView_Show.CurrentCell.Value.ToString();
            }
            catch (Exception) { return; }
        }
        //Number Only 
        private void RadGridView_Show_CellValidated(object sender, CellValidatedEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                switch (e.Column.FieldName)
                {
                    case "CHANNEL":
                        string Chanel = radGridView_Show.CurrentRow.Cells["CHANNEL"].Value.ToString();
                        string Show_ID = radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString();

                        string sqlUp = string.Format(@"UPDATE dbo.[SHOP_JOBCAMSTATION] SET 
                                          [CHANNEL] = '{0}'
                                          ,[WHOUPD] = '{1}'
                                          ,[WHONAMEUPD] = '{2}'
                                          ,[DATEUPD] = GETDATE()
                                          WHERE [CAM_ID] = '{3}' AND [SHOW_ID] = '{4}'"
                                      , Chanel, SystemClass.SystemUserID, SystemClass.SystemUserName, _pCamID, Show_ID);
                        string returnExc = ConnectionClass.ExecuteSQL_Main(sqlUp);
                        if (returnExc != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                            return;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //do work
                if (e.Column.FieldName == "DELETE")
                {
                    radDropDownList_Station.SelectedValue = radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString();
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการลบข้อมูล ? " + radDropDownList_Station.Text) == DialogResult.Yes)
                    {
                        DataRow[] rows = dt.Select("SHOW_ID = '" + radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString() + "' ");
                        int iRow = dt.Rows.IndexOf(rows[0]);

                        string sqlIn = string.Format(@"DELETE FROM dbo.SHOP_JOBCAMSTATION WHERE CAM_ID = '{0}' AND SHOW_ID = '{1}'"
                                    , _pCamID, radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString());

                        string returnstr = ConnectionClass.ExecuteSQL_Main(sqlIn);
                        MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);

                        if (returnstr == "")
                        {
                            dt.Rows[iRow].Delete();   //Delete
                            dt.AcceptChanges();
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
        }
        private GridViewDataColumn AppendNewColumn(string columnType, string fieldName, string labelName, int _width)
        {
            GridViewDataColumn newColumn = null;
            switch (columnType)
            {
                case "ComboBox":
                    newColumn = new GridViewComboBoxColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        DataSource = dtcb,
                        ValueMember = "SHOW_ID",
                        DisplayMember = "SHOW_NAME",
                        HeaderTextAlignment = ContentAlignment.MiddleLeft,
                        TextAlignment = ContentAlignment.MiddleLeft,
                        Width = _width,
                        WrapText = true,
                    };
                    break;
                case "Text":
                    newColumn = new GridViewTextBoxColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        HeaderTextAlignment = ContentAlignment.MiddleLeft,
                        TextAlignment = ContentAlignment.MiddleLeft,
                        Width = _width,
                        WrapText = true
                    };
                    break;
                case "Command":
                    newColumn = new GridViewCommandColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        HeaderTextAlignment = ContentAlignment.MiddleCenter,
                        TextAlignment = ContentAlignment.MiddleCenter,
                        Width = _width,
                        WrapText = true
                    };
                    break;
            }
            return newColumn;
        }
        private void Camera_Detail_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;

            this.Text = _pDesc;
            //GroupBox
            radGroupBox_Cam.Text = "รายละเอียดกล้อง";
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Poin);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Cam);
            DatagridClass.SetTextControls(radGroupBox_Poin.Controls, (int)(CollectionControl.RadTextBoxBase));
            DatagridClass.SetTextControls(radGroupBox_Poin.Controls, (int)(CollectionControl.RadDropDownList));
            DatagridClass.SetTextControls(radGroupBox_Cam.Controls, (int)(CollectionControl.RadTextBoxBase));
            DatagridClass.SetTextControls(radGroupBox_Cam.Controls, (int)(CollectionControl.RadCheckBox));
            DatagridClass.SetTextControls(radGroupBox_Cam.Controls, (int)(CollectionControl.RadRadioButton));
            DatagridClass.SetTextControls(radGroupBox_Cam.Controls, (int)(CollectionControl.RadDropDownList));

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dvr);
            radLabel_EmpName.ForeColor = Class.ConfigClass.SetColor_Blue();
            radDropDownList_Dvr.DropDownListElement.SelectionMode = SelectionMode.MultiExtended;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Close.ButtonElement.ShowBorder = true;
            radButton_Addrow.ButtonElement.ShowBorder = true;
            radButton_Addrow.ButtonElement.Font = SystemClass.SetFontGernaral;
            radGridView_Show.TableElement.RowHeight = 35;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.ReadOnly = false;
            radGridView_Show.MasterTemplate.EnableFiltering = false;    //การกรองที่หัวกริด
            this.CreateBindingList(24);
            radDropDown_Ch.DataSource = items;
            radDropDown_Ch.SelectedIndex = -1;
            radDropDown_Ch.DropDownListElement.SelectionMode = SelectionMode.MultiExtended;

            this.CreateBindingList_station();
            this.CreateBindingWeeldays();

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Station);
            GetBrach();
            GetArea();
            GetDpt();
            CreateBindingList_dvr();
            radTextBox_Link.Focus();

            if (string.IsNullOrEmpty(_pCamID))
            {
                SetDefaultGroupBox();
            }
            else
            {
                GetDataCamDetail();
                radDropDown_Branch.Enabled = false;
                radDropDown_Dpt.Enabled = false;
                radDropDown_Area.Enabled = false;
            }

            if (_pPermisUser == "2" || _pPermisUser == "1")
            {
                radButton_Save.Enabled = false;
                foreach (var control in radGroupBox_Cam.Controls.Cast<Control>())
                {
                    if (control is RadTextBox)
                    {
                        RadTextBox tb = control as RadTextBox;
                        tb.ReadOnly = true; // controls like TextBox and RichTextBox
                    }
                    else if (control is RadDropDownList)
                    {
                        RadDropDownList cb = control as RadDropDownList;
                        cb.ReadOnly = true;
                    }
                    else if (control is RadCheckBox)
                    {
                        RadCheckBox ck = control as RadCheckBox;
                        ck.ReadOnly = true;
                    }
                    else if (control is RadRadioButton)
                    {
                        RadRadioButton rd = control as RadRadioButton;
                        rd.ReadOnly = true;
                    }
                    else
                    {
                        control.Enabled = true; // all other controls 
                    }
                }
            }
        }
        #region"Event"
        private void RadDropDown_Branch_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.radDropDown_Branch.SelectedIndex > -1)
            {
                if (this.radDropDown_Branch.SelectedIndex != 0)
                {
                    radDropDown_Dpt.SelectedValue = "D999";
                    radDropDown_Area.SelectedValue = "MN";
                    radDropDown_Dpt.Enabled = false;
                    radDropDown_Area.Enabled = false;
                }
                else
                {
                    radDropDown_Dpt.Enabled = true;
                    radDropDown_Area.Enabled = true;
                }
            }
        }
        private void RadDropDown_Area_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.radDropDown_Branch.SelectedIndex > -1)
            {
                if (radDropDown_Area.SelectedIndex != 1)
                {
                    radDropDown_Dpt.SelectedValue = "D999";
                    radDropDown_Dpt.Enabled = false;
                }
                else
                {
                    radDropDown_Dpt.Enabled = true;
                }
            }
        }
        private void RadDropDown_Dpt_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (radDropDown_Dpt.SelectedIndex > -1)
            {
                if (radDropDown_Area.SelectedIndex != 1) radDropDown_Dpt.Enabled = false; else radDropDown_Dpt.Enabled = true;
            }
        }
        private void RadTextBox_Emp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(radTextBox_Emp.Text))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสพนักงาน...");
                    radTextBox_Emp.Focus();
                    radLabel_EmpName.Text = string.Empty;
                }
                else
                {
                    if (radTextBox_Emp.Text.Length == 8) radTextBox_Emp.Text = Class.ConfigClass.ReplaceMDPForEmlpID(radTextBox_Emp.Text);

                    radLabel_EmpName.Text = GetEmployee(radTextBox_Emp.Text);
                    if (string.IsNullOrEmpty(radLabel_EmpName.Text))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบพนักงาน เช็คใหม่อีกครั้ง");
                        radTextBox_Emp.Focus();
                    }
                    else
                    {
                        radLabel_EmpName.Text = radLabel_EmpName.Text.Substring(4, radLabel_EmpName.Text.Length - 4);
                    }
                }
            }
        }
        private void RadButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Link.Text) || string.IsNullOrEmpty(radTextBox_Desc.Text) || string.IsNullOrEmpty(radTextBox_Location.Text)
                || string.IsNullOrEmpty(radTextBox_Emp.Text) || string.IsNullOrEmpty(radLabel_EmpName.Text) || string.IsNullOrEmpty(radDropDown_Branch.SelectedValue.ToString())
                || string.IsNullOrEmpty(radDropDown_Area.SelectedValue.ToString()) || string.IsNullOrEmpty(radDropDown_Dpt.SelectedValue.ToString()))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รายละเอียดกล้อง");
                radTextBox_Link.Focus();
            }
            else
            {
                string StatusCheckEver = "0";
                if (radRadio_Byday.IsChecked == true) StatusCheckEver = radDropDownList_Weekdays.SelectedValue.ToString();

                ReturnExecut = GetExecut(StatusCheckEver);
                if (ReturnExecut == "")
                {
                    string str;
                    if (_pCamID == "")
                    {
                        str = string.Format(@"
                                SELECT [CAM_ID],[CAM_IP],[CAM_DESC],[CAM_CH],[CAM_AMORN],[CAM_TECHNICID],[CAM_TECHNICNAME]
		                                ,[CAM_TYPECAM],[CAM_DEPT],[CAM_RECORD],[CAM_FREEZ],[CAM_UP],[CAM_DOWN],CAM_REMARK ,CAM_MYANMAR 
                                FROM    [SHOP_JOBCAM]  WITH(NOLOCK)
                                WHERE   [CAM_ID] = (SELECT MAX([CAM_ID]) FROM [SHOP_JOBCAM] )");

                    }
                    else
                    {
                        str = string.Format(@"
                                SELECT [CAM_ID],[CAM_IP],[CAM_DESC],[CAM_CH],[CAM_AMORN],[CAM_TECHNICID],[CAM_TECHNICNAME]
                                        ,[CAM_TYPECAM],[CAM_DEPT],[CAM_RECORD],[CAM_FREEZ],[CAM_UP],[CAM_DOWN],CAM_REMARK ,CAM_MYANMAR 
                                FROM [SHOP_JOBCAM]  WITH(NOLOCK)
                                WHERE CAM_ID = '{0}'", _pCamID);
                    }

                    DataTable dt = ConnectionClass.SelectSQL_Main(str);
                    this.DATAROWCAM = new DATACAMERA();
                    DATAROWCAM.CAMID = dt.Rows[0]["CAM_ID"].ToString();
                    DATAROWCAM.CAMDESC = dt.Rows[0]["CAM_DESC"].ToString();
                    DATAROWCAM.CAMIP = dt.Rows[0]["CAM_IP"].ToString();
                    DATAROWCAM.CAMCH = int.Parse(dt.Rows[0]["CAM_CH"].ToString());
                    DATAROWCAM.CAMRECODR = int.Parse(dt.Rows[0]["CAM_RECORD"].ToString());
                    DATAROWCAM.CAMAMORN = dt.Rows[0]["CAM_AMORN"].ToString();
                    DATAROWCAM.CAMTECHNICID = dt.Rows[0]["CAM_TECHNICID"].ToString();
                    DATAROWCAM.CAMTECHNICINAME = dt.Rows[0]["CAM_TECHNICNAME"].ToString();
                    DATAROWCAM.CAMDEPT = dt.Rows[0]["CAM_DEPT"].ToString();
                    DATAROWCAM.CAMTYPECAM = dt.Rows[0]["CAM_TYPECAM"].ToString();
                    DATAROWCAM.CAMFREEZ = dt.Rows[0]["CAM_FREEZ"].ToString();
                    DATAROWCAM.CAMUP = dt.Rows[0]["CAM_UP"].ToString();
                    DATAROWCAM.CAMDOWN = dt.Rows[0]["CAM_DOWN"].ToString();
                    DATAROWCAM.CAMREMARK = dt.Rows[0]["CAM_REMARK"].ToString();
                    DATAROWCAM.CAM_MYANMAR = dt.Rows[0]["CAM_MYANMAR"].ToString();
                }

                MsgBoxClass.MsgBoxShow_SaveStatus(ReturnExecut);
                this.DialogResult = DialogResult.OK;
                this.Close();
                return;
            }
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        private void PictureBox_Cam_DoubleClick(object sender, EventArgs e)
        {
            Process.Start(PathCAMIP);
        }
        private void RadButton_Addrow_Click(object sender, EventArgs e)
        {
            if (radDropDownList_Station.SelectedIndex > -1)
            {
                for (int i = 0; i < radGridView_Show.Rows.Count; i++)
                {
                    if (radDropDownList_Station.SelectedValue.ToString() == radGridView_Show.Rows[i].Cells["SHOW_ID"].Value.ToString())
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("มีข้อมูลกล้องอยู่แล้ว สามารถแก้ไขได้");
                        return;
                    }
                }
                string sql = string.Format(@"INSERT INTO [dbo].[SHOP_JOBCAMSTATION]   
                                ( [CAM_ID],[SHOW_ID],[CHANNEL],[WHOUPD],[WHONAMEUPD] )
                            VALUES ('{0}','{1}','','{2}','{3}')"
                            , _pCamID
                            , radDropDownList_Station.SelectedValue.ToString()
                            , SystemClass.SystemUserID
                            , SystemClass.SystemUserName);
                string returnExc = ConnectionClass.ExecuteSQL_Main(sql);

                if (returnExc != "")
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                    return;
                }
                else
                {
                    radGridView_Show.Rows.Add(radDropDownList_Station.SelectedValue, "", "ลบ");
                }
            }
        }
        #endregion
        #region Method
        private void SetDefaultGroupBox()
        {
            radTextBox_Link.Text = string.Empty;
            radTextBox_Desc.Text = string.Empty;
            radTextBox_Location.Text = string.Empty;
            radTextBox_Emp.Text = string.Empty;
            RadTextBox_Emp1.Text = string.Empty;
            radLabel_EmpName.Text = string.Empty;
        }
        private void GetDataCamDetail()
        {
            string sql;
            sql = string.Format(@"
                    SELECT  CAM_ID,CAM_IP,CAM_DESC,SHOP_JOBCAM.BRANCH_ID AS BRANCH_ID,CAM_CH,CAM_LOCATION,CAM_AMORN,CAM_CHECKDAY,CAM_CHECKUSER
                            ,CAM_TECHNICID,CAM_TECHNICNAME,CAM_TYPECAM,CAM_DEPT,CAM_RECORD,USERUPD,DATEUPD
                            ,ISNULL(CAM_REMARK, '') AS CAM_REMARK,CAM_DVR ,CAM_MYANMAR
                    FROM    SHOP_JOBCAM WITH (NOLOCK) 
                            LEFT OUTER JOIN  SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                    WHERE   CAM_ID = '{0}' ", _pCamID);

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radTextBox_Link.Text = dt.Rows[0]["CAM_IP"].ToString();
                radTextBox_Desc.Text = dt.Rows[0]["CAM_DESC"].ToString();
                radTextBox_Location.Text = dt.Rows[0]["CAM_LOCATION"].ToString();
                radTextBox_Emp.Text = dt.Rows[0]["CAM_TECHNICID"].ToString();
                RadTextBox_Emp1.Text = dt.Rows[0]["CAM_MYANMAR"].ToString();
                radLabel_EmpName.Text = dt.Rows[0]["CAM_TECHNICNAME"].ToString();
                radTextBox_Remark.Text = dt.Rows[0]["CAM_REMARK"].ToString();
                CamIP = radTextBox_Link.Text.ToString().Replace("http://", "").Replace(".", "_").Replace("/", "").Replace(":81", "").Replace(":82", "");

                if (dt.Rows[0]["CAM_CHECKUSER"].ToString() == "1") { radCheckBox_Check.Checked = true; }
                if (dt.Rows[0]["CAM_AMORN"].ToString() == "1") { radCheckBox_Amor.Checked = true; }

                if (dt.Rows[0]["CAM_CHECKDAY"].ToString() == "0")
                { radRadio_CheckEvery.IsChecked = true; radDropDownList_Weekdays.Enabled = false; }
                else
                {
                    radRadio_Byday.IsChecked = true;
                    radDropDownList_Weekdays.SelectedValue = dt.Rows[0]["CAM_CHECKDAY"].ToString();
                }

                radDropDown_Branch.SelectedValue = dt.Rows[0]["BRANCH_ID"].ToString();
                radDropDown_Area.SelectedValue = dt.Rows[0]["CAM_TYPECAM"].ToString();
                radDropDown_Dpt.SelectedValue = dt.Rows[0]["CAM_DEPT"].ToString();
                radDropDown_Ch.SelectedValue = dt.Rows[0]["CAM_CH"].ToString();
                radDropDownList_Dvr.SelectedValue = dt.Rows[0]["CAM_DVR"].ToString();
                GetImg();
            }
        }
        private void GetBrach()//string pPrice)
        {
            string sql = string.Format(@"SELECT  BRANCH_ID,BRANCH_ID + ' - ' + BRANCH_NAME AS BRANCH_NAME FROM SHOP_BRANCH  WITH(NOLOCK) ORDER BY BRANCH_ID ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radDropDown_Branch.DataSource = dt;
                radDropDown_Branch.DisplayMember = "BRANCH_NAME";
                radDropDown_Branch.ValueMember = "BRANCH_ID";
                radDropDown_Branch.SelectedIndex = -1;
            }
        }
        private void GetArea()//string AreaType)
        {
            string sql = string.Format(@"SELECT CAM_TYPECAM,CASE CAM_TYPECAM WHEN 'MN' THEN 'มินิมาร์ท'
                  WHEN 'RET' THEN 'สาขาใหญ่ RET' WHEN 'SPC' THEN 'สาขาใหญ่ SPC' WHEN 'OTH' THEN 'พื้นที่ส่วนนอก OTH'END AS CAMAREA
                  FROM SHOP24HRS.dbo.SHOP_JOBCAM WITH(NOLOCK) GROUP BY CAM_TYPECAM ");

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radDropDown_Area.DataSource = dt;
                radDropDown_Area.DisplayMember = "CAMAREA";
                radDropDown_Area.ValueMember = "CAM_TYPECAM";
                radDropDown_Area.SelectedIndex = -1;
            }
        }
        private void GetDpt()//string DeptID)
        {
            //string sql = string.Format(@"SELECT NUM + ' - ' + [DESCRIPTION] AS [DESCRIPTION],NUM FROM SHOP2013TMP.dbo.DIMENSIONS
            //      WHERE DIMENSIONCODE = '0' AND DATAAREAID = N'SPC' AND COMPANYGROUP != 'Cancel' AND NUM LIKE 'D%' 
            //      ORDER BY NUM ");

            DataTable dt = Models.DptClass.GetDptForShow(); //ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radDropDown_Dpt.DataSource = dt;
                radDropDown_Dpt.DisplayMember = "DESCRIPTION";
                radDropDown_Dpt.ValueMember = "NUM";
                radDropDown_Dpt.SelectedIndex = -1;
            }
        }
        private void GetImg()
        {
            PathImage += radDropDown_Branch.SelectedValue;
            //Determine whether the directory exists.
            if (!Directory.Exists(PathImage))
            {
                Directory.CreateDirectory(PathImage);
            }
            // Try to create the directory.
            DirectoryInfo DirInfo = new DirectoryInfo(PathImage);
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(string.Format(@"{0}-{1}-{2}.JPG", _pCamID, radDropDown_Branch.SelectedValue, CamIP), SearchOption.AllDirectories);
                if (Files.Length > 0)
                {
                    PathCAMIP = Files[0].FullName;
                    Image sizeImage = Image.FromFile(PathCAMIP);
                    if (sizeImage.Width > 1080)
                    {
                        //Resize
                        FitImage(PathCAMIP, 1080, 960, string.Format(@"{0}-{1}-{2}.JPG", _pCamID, radDropDown_Branch.SelectedValue, CamIP), sizeImage);
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                pictureBox_Cam.Image = Image.FromFile(PathCAMIP);
                pictureBox_Cam.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
        private string GetExecut(string pStatusCheckEver)
        {
            string sqlIn;
            if (string.IsNullOrEmpty(_pCamID))
            {
                sqlIn = string.Format(@"INSERT INTO dbo.SHOP_JOBCAM
                            ( CAM_ID,CAM_IP,CAM_DESC,BRANCH_ID,CAM_CH,CAM_LOCATION
                              ,CAM_AMORN,CAM_CHECKDAY,CAM_CHECKUSER,CAM_TECHNICID,CAM_TECHNICNAME
                              ,CAM_TYPECAM,CAM_DEPT,CAM_REMARK,USERINS,CAM_DVR,CAM_MYANMAR ) 
                            VALUES  ( '{0}','{1}','{2}','{3}','{4}','{5}'
                                        ,'{6}','{7}','{8}','{9}','{10}'
                                        ,'{11}','{12}','{13}','{14}','{15}','{16}')"
                            , GetMaxNum()
                            , radTextBox_Link.Text.Trim()
                            , radTextBox_Desc.Text
                            , radDropDown_Branch.SelectedValue.ToString()
                            , int.Parse(radDropDown_Ch.SelectedValue.ToString())
                            , radTextBox_Location.Text
                            , CheckBoxState(radCheckBox_Amor)
                            , pStatusCheckEver
                            , CheckBoxState(radCheckBox_Check)
                            , radTextBox_Emp.Text.Trim()
                            , radLabel_EmpName.Text
                            , radDropDown_Area.SelectedValue.ToString()
                            , radDropDown_Dpt.SelectedValue.ToString()
                            , radTextBox_Remark.Text.Replace(',', ' ')
                            , SystemClass.SystemUserID
                            , radDropDownList_Dvr.SelectedValue.ToString(), RadTextBox_Emp1.Text);
            }
            else
            {
                sqlIn = string.Format(@"UPDATE dbo.SHOP_JOBCAM 
                            SET CAM_IP = '{0}',CAM_DESC = '{1}',BRANCH_ID = '{2}',CAM_CH = '{3}',CAM_LOCATION = '{4}'
                              ,CAM_AMORN = '{5}',CAM_CHECKDAY = '{6}',CAM_CHECKUSER = '{7}',CAM_TECHNICID = '{8}',CAM_TECHNICNAME = '{9}'
                              ,CAM_TYPECAM = '{10}',CAM_DEPT = '{11}',CAM_REMARK = '{12}',CAM_DVR = '{13}',USERUPD = '{14}',DATEUPD = GETDATE(),CAM_MYANMAR = '{16}' 
                               WHERE CAM_ID = '{15}'"
                              , radTextBox_Link.Text.Trim()
                              , radTextBox_Desc.Text.Replace(',', ' ')
                              , radDropDown_Branch.SelectedValue.ToString()
                              , radDropDown_Ch.SelectedValue.ToString()
                              , radTextBox_Location.Text
                              , CheckBoxState(radCheckBox_Amor)
                              , pStatusCheckEver
                              , CheckBoxState(radCheckBox_Check)
                              , radTextBox_Emp.Text.Trim()
                              , radLabel_EmpName.Text
                              , radDropDown_Area.SelectedValue.ToString()
                              , radDropDown_Dpt.SelectedValue.ToString()
                              , radTextBox_Remark.Text.Replace(',', ' ')
                              , radDropDownList_Dvr.SelectedValue.ToString()
                              , SystemClass.SystemUserID
                              , _pCamID, RadTextBox_Emp1.Text);
            }
            return ConnectionClass.ExecuteSQL_Main(sqlIn);
        }
        private string GetEmployee(string pEmp)
        {
            DataTable dt = Class.Models.EmplClass.GetEmployee(pEmp);
            if (dt.Rows.Count > 0) return dt.Rows[0]["SPC_NAME"].ToString(); else return string.Empty;
        }
        private string CheckBoxState(RadCheckBox checkBox)
        {
            if (checkBox.IsChecked == true) return "1"; else return "0";
        }
        private string GetMaxNum()
        {
            string sql = string.Format(@" SELECT ISNULL(MAX(ISNULL(CAM_ID,0)),0)+1 AS CAM_ID 
                    FROM SHOP_JOBCAM WITH (NOLOCK) ");
            DataTable daMaxID = ConnectionClass.SelectSQL_Main(sql);
            return daMaxID.Rows[0][0].ToString();
        }
        public void FitImage(string pPath, int pNewWidth, int pMaxHeight, string pNewName, Image FullsizeImage)
        {
            //Image FullsizeImage = Image.FromFile(pPath);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

            int NewHeight = FullsizeImage.Height * pNewWidth / FullsizeImage.Width;
            if (NewHeight > pMaxHeight)
            {
                // Resize with height instead
                pNewWidth = FullsizeImage.Width * pMaxHeight / FullsizeImage.Height;
                NewHeight = pMaxHeight;
            }

            Bitmap bmp = new Bitmap(pNewWidth, NewHeight,
                    System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (Graphics gr = Graphics.FromImage(bmp))
                gr.DrawImage(FullsizeImage, new Rectangle(0, 0, bmp.Width, bmp.Height));

            // Exclude the filename from the the path
            string path = pPath.Substring(0, pPath.LastIndexOf(@"\")) + "\\";

            // Save resized picture
            bmp.Save(path + pNewName, System.Drawing.Imaging.ImageFormat.Jpeg);
            bmp.Dispose();
        }
        private void RadRadio_Byday_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadio_Byday.IsChecked == true)
            {
                radDropDownList_Weekdays.Enabled = true;
                radDropDownList_Weekdays.SelectedIndex = 0;
            }
            else
            {
                radDropDownList_Weekdays.Enabled = false;
                radDropDownList_Weekdays.SelectedIndex = -1;
            }
        }

        #endregion


    }
}
