﻿namespace PC_Shop24Hrs.ComMinimart
{
    partial class DetailBill_POSRetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailBill_POSRetail));
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_BillDT = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_BillRC = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_BillHD = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radTextBox_Price = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_PosNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_FindBarcode = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Cst = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Branch = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker_End = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_Begin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radTextBox_BillID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_FindCst = new Telerik.WinControls.UI.RadButton();
            this.RadButton_FindBranch = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillDT.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillRC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillRC.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillHD.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PosNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Cst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_End)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindCst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(656, 522);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(859, 623);
            this.radPanel1.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(859, 623);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_BillDT, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_BillHD, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_BillRC, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(653, 617);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 593);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(647, 19);
            this.radLabel_Detail.TabIndex = 54;
            // 
            // RadGridView_BillDT
            // 
            this.RadGridView_BillDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_BillDT.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_BillDT.Location = new System.Drawing.Point(3, 239);
            // 
            // 
            // 
            this.RadGridView_BillDT.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_BillDT.Name = "RadGridView_BillDT";
            // 
            // 
            // 
            this.RadGridView_BillDT.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_BillDT.Size = new System.Drawing.Size(647, 230);
            this.RadGridView_BillDT.TabIndex = 3;
            this.RadGridView_BillDT.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_BillDT_ViewCellFormatting);
            // 
            // RadGridView_BillRC
            // 
            this.RadGridView_BillRC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_BillRC.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_BillRC.Location = new System.Drawing.Point(3, 475);
            // 
            // 
            // 
            this.RadGridView_BillRC.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.RadGridView_BillRC.Name = "RadGridView_BillRC";
            // 
            // 
            // 
            this.RadGridView_BillRC.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_BillRC.Size = new System.Drawing.Size(647, 112);
            this.RadGridView_BillRC.TabIndex = 2;
            this.RadGridView_BillRC.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_BillRC_ViewCellFormatting);
            // 
            // RadGridView_BillHD
            // 
            this.RadGridView_BillHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_BillHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_BillHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_BillHD.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_BillHD.Name = "RadGridView_BillHD";
            // 
            // 
            // 
            this.RadGridView_BillHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_BillHD.Size = new System.Drawing.Size(647, 230);
            this.RadGridView_BillHD.TabIndex = 1;
            this.RadGridView_BillHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_BillHD_ViewCellFormatting);
            this.RadGridView_BillHD.SelectionChanged += new System.EventHandler(this.RadGridView_BillHD_SelectionChanged);
            this.RadGridView_BillHD.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_BillHD_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radTextBox_Price);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.radTextBox_PosNumber);
            this.panel1.Controls.Add(this.radTextBox_Barcode);
            this.panel1.Controls.Add(this.RadButton_FindBarcode);
            this.panel1.Controls.Add(this.radTextBox_Cst);
            this.panel1.Controls.Add(this.radTextBox_Branch);
            this.panel1.Controls.Add(this.radDateTimePicker_End);
            this.panel1.Controls.Add(this.radDateTimePicker_Begin);
            this.panel1.Controls.Add(this.radTextBox_BillID);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadButton_FindCst);
            this.panel1.Controls.Add(this.RadButton_FindBranch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(662, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 617);
            this.panel1.TabIndex = 6;
            // 
            // radTextBox_Price
            // 
            this.radTextBox_Price.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Price.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Price.Location = new System.Drawing.Point(147, 230);
            this.radTextBox_Price.Name = "radTextBox_Price";
            this.radTextBox_Price.Size = new System.Drawing.Size(36, 21);
            this.radTextBox_Price.TabIndex = 65;
            this.radTextBox_Price.Text = "100";
            this.radTextBox_Price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Price_KeyPress);
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(8, 389);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 5;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_PosNumber
            // 
            this.radTextBox_PosNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_PosNumber.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_PosNumber.Location = new System.Drawing.Point(8, 344);
            this.radTextBox_PosNumber.Name = "radTextBox_PosNumber";
            this.radTextBox_PosNumber.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_PosNumber.TabIndex = 4;
            this.radTextBox_PosNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_PosNumber_KeyDown);
            this.radTextBox_PosNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_PosNumber_KeyPress);
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(8, 230);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(138, 21);
            this.radTextBox_Barcode.TabIndex = 2;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            // 
            // RadButton_FindBarcode
            // 
            this.RadButton_FindBarcode.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_FindBarcode.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_FindBarcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_FindBarcode.Image = ((System.Drawing.Image)(resources.GetObject("RadButton_FindBarcode.Image")));
            this.RadButton_FindBarcode.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_FindBarcode.Location = new System.Drawing.Point(8, 203);
            this.RadButton_FindBarcode.Name = "RadButton_FindBarcode";
            this.RadButton_FindBarcode.Size = new System.Drawing.Size(26, 26);
            this.RadButton_FindBarcode.TabIndex = 64;
            this.RadButton_FindBarcode.Text = "radButton3";
            this.RadButton_FindBarcode.Click += new System.EventHandler(this.RadButton_FindBarcode_Click);
            // 
            // radTextBox_Cst
            // 
            this.radTextBox_Cst.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Cst.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Cst.Location = new System.Drawing.Point(8, 172);
            this.radTextBox_Cst.Name = "radTextBox_Cst";
            this.radTextBox_Cst.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Cst.TabIndex = 1;
            this.radTextBox_Cst.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Cst_KeyDown);
            // 
            // radTextBox_Branch
            // 
            this.radTextBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Branch.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Branch.Location = new System.Drawing.Point(6, 116);
            this.radTextBox_Branch.Name = "radTextBox_Branch";
            this.radTextBox_Branch.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Branch.TabIndex = 0;
            this.radTextBox_Branch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Branch_KeyDown);
            // 
            // radDateTimePicker_End
            // 
            this.radDateTimePicker_End.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_End.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_End.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_End.Location = new System.Drawing.Point(6, 55);
            this.radDateTimePicker_End.Name = "radDateTimePicker_End";
            this.radDateTimePicker_End.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_End.TabIndex = 24;
            this.radDateTimePicker_End.TabStop = false;
            this.radDateTimePicker_End.Text = "26/04/2020";
            this.radDateTimePicker_End.Value = new System.DateTime(2020, 4, 26, 9, 17, 9, 0);
            this.radDateTimePicker_End.ValueChanged += new System.EventHandler(this.RadDateTimePicker_End_ValueChanged);
            // 
            // radDateTimePicker_Begin
            // 
            this.radDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Begin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_Begin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_Begin.Location = new System.Drawing.Point(6, 30);
            this.radDateTimePicker_Begin.Name = "radDateTimePicker_Begin";
            this.radDateTimePicker_Begin.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_Begin.TabIndex = 23;
            this.radDateTimePicker_Begin.TabStop = false;
            this.radDateTimePicker_Begin.Text = "26/04/2020";
            this.radDateTimePicker_Begin.Value = new System.DateTime(2020, 4, 26, 9, 17, 9, 0);
            this.radDateTimePicker_Begin.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Begin_ValueChanged);
            // 
            // radTextBox_BillID
            // 
            this.radTextBox_BillID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_BillID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_BillID.Location = new System.Drawing.Point(8, 287);
            this.radTextBox_BillID.Name = "radTextBox_BillID";
            this.radTextBox_BillID.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_BillID.TabIndex = 3;
            this.radTextBox_BillID.Text = "S192004-0000000";
            this.radTextBox_BillID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BillID_KeyDown);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(10, 324);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(119, 19);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "ตำแหน่งเครื่องขาย";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(8, 267);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(57, 19);
            this.radLabel5.TabIndex = 21;
            this.radLabel5.Text = "เลขที่บิล";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(39, 208);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(55, 19);
            this.radLabel4.TabIndex = 20;
            this.radLabel4.Text = "บาร์โค้ด";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(35, 99);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(64, 19);
            this.radLabel3.TabIndex = 19;
            this.radLabel3.Text = "รหัสสาขา";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(35, 154);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(66, 19);
            this.radLabel2.TabIndex = 18;
            this.radLabel2.Text = "รหัสลูกค้า";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(6, 11);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(58, 19);
            this.radLabel1.TabIndex = 17;
            this.radLabel1.Text = "วันที่ขาย";
            // 
            // RadButton_FindCst
            // 
            this.RadButton_FindCst.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_FindCst.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_FindCst.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_FindCst.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_FindCst.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_FindCst.Location = new System.Drawing.Point(8, 145);
            this.RadButton_FindCst.Name = "RadButton_FindCst";
            this.RadButton_FindCst.Size = new System.Drawing.Size(26, 26);
            this.RadButton_FindCst.TabIndex = 62;
            this.RadButton_FindCst.Text = "radButton3";
            this.RadButton_FindCst.Click += new System.EventHandler(this.RadButton_FindCst_Click);
            // 
            // RadButton_FindBranch
            // 
            this.RadButton_FindBranch.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_FindBranch.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_FindBranch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_FindBranch.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_FindBranch.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_FindBranch.Location = new System.Drawing.Point(6, 89);
            this.RadButton_FindBranch.Name = "RadButton_FindBranch";
            this.RadButton_FindBranch.Size = new System.Drawing.Size(26, 26);
            this.RadButton_FindBranch.TabIndex = 63;
            this.RadButton_FindBranch.Text = "radButton3";
            this.RadButton_FindBranch.Click += new System.EventHandler(this.RadButton_FindBranch_Click);
            // 
            // DetailBill_POSRetail
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(859, 623);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DetailBill_POSRetail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ตรวจสอบข้อมูลการขาย";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DetailBill_POSRetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillDT.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillRC.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillRC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BillHD)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PosNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Cst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_End)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindCst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_BillID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_BillHD;
        private Telerik.WinControls.UI.RadGridView RadGridView_BillRC;
        private Telerik.WinControls.UI.RadGridView RadGridView_BillDT;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Begin;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_End;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Branch;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Cst;
        private Telerik.WinControls.UI.RadButton RadButton_FindCst;
        private Telerik.WinControls.UI.RadButton RadButton_FindBranch;
        private Telerik.WinControls.UI.RadButton RadButton_FindBarcode;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadTextBox radTextBox_PosNumber;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Price;
    }
}
