﻿namespace PC_Shop24Hrs.ComMinimart.Manage
{
    partial class DepPermission_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepPermission_Main));
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Refresh = new Telerik.WinControls.UI.RadButtonElement();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radPanel2
            // 
            this.radPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel2.BackColor = System.Drawing.SystemColors.Control;
            this.radPanel2.Controls.Add(this.radTextBox_Remark);
            this.radPanel2.Controls.Add(this.radStatusStrip);
            this.radPanel2.Controls.Add(this.radLabel2);
            this.radPanel2.Controls.Add(this.radDropDownList_Dept);
            this.radPanel2.Controls.Add(this.radLabel1);
            this.radPanel2.Controls.Add(this.radButton_Save);
            this.radPanel2.Controls.Add(this.radButton_Cancel);
            this.radPanel2.Location = new System.Drawing.Point(587, 3);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(194, 555);
            this.radPanel2.TabIndex = 11;
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(5, 140);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(180, 49);
            this.radTextBox_Remark.TabIndex = 24;
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.radButton_Add,
            this.commandBarSeparator1,
            this.radButton_Refresh});
            this.radStatusStrip.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(194, 37);
            this.radStatusStrip.TabIndex = 7;
            ((Telerik.WinControls.UI.RadStatusBarElement)(this.radStatusStrip.GetChildAt(0))).StretchVertically = false;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).AutoSize = true;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Alignment = System.Drawing.ContentAlignment.BottomRight;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Enabled = false;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButton_Add
            // 
            this.radButton_Add.AutoSize = true;
            this.radButton_Add.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Add, false);
            this.radButton_Add.Text = "เพิ่มแผนก";
            this.radButton_Add.ToolTipText = "เพิ่มแผนก";
            this.radButton_Add.UseCompatibleTextRendering = false;
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButton_Refresh
            // 
            this.radButton_Refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_Refresh.Name = "radButton_Refresh";
            this.radStatusStrip.SetSpring(this.radButton_Refresh, false);
            this.radButton_Refresh.Text = "Refresh";
            this.radButton_Refresh.ToolTipText = "Refresh";
            this.radButton_Refresh.Click += new System.EventHandler(this.RadButton_Refresh_Click);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(5, 115);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(65, 19);
            this.radLabel2.TabIndex = 23;
            this.radLabel2.Text = "คำอธิบาย";
            // 
            // radDropDownList_Dept
            // 
            this.radDropDownList_Dept.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dept.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Dept.DropDownAnimationEnabled = false;
            this.radDropDownList_Dept.DropDownHeight = 124;
            this.radDropDownList_Dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDropDownList_Dept.Location = new System.Drawing.Point(5, 83);
            this.radDropDownList_Dept.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Dept.Name = "radDropDownList_Dept";
            // 
            // 
            // 
            this.radDropDownList_Dept.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Dept.Size = new System.Drawing.Size(180, 21);
            this.radDropDownList_Dept.TabIndex = 21;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dept.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dept.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dept.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(5, 57);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 19);
            this.radLabel1.TabIndex = 22;
            this.radLabel1.Text = "คำอธิบาย";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(2, 211);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 20;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(100, 211);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 19;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(578, 555);
            this.radGridView_Show.TabIndex = 2;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // DepPermission_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DepPermission_Main";
            this.Text = "DepPermission_Main";
            this.Load += new System.EventHandler(this.DepPermission_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.RadButtonElement radButton_Add;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButton_Refresh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}