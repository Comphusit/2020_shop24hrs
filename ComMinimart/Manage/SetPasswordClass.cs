﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    class SetPasswordClass
    {
        
        // เข้ารหัส password ของพนักงาน
        public static string EncodePassword(string pass)
        {
            string res = "";
            for (int i = 0; i <= pass.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) - 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) + 4);
            }
            return res;
        }
        // ถอดรหัส password ของพนักงาน
        public static string DecodePassword(string pass)
        {
            string res = "";
            for (int i = 0; i <= pass.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) + 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) - 4);
            }
            return res;
        }
        
    }
}
