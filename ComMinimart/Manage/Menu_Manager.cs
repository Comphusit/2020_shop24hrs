﻿using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class Menu_Manager : Form
    {
        public Menu_Manager()
        {
            InitializeComponent();
        }

        private void Menu_Manager_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Clear.ButtonElement.ShowBorder = true;

            SetGrid(Class.MenuPermissionClass.GetAllMenu("", ""));

        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            string Status = "", Type = "";
            SetGrid(Class.MenuPermissionClass.GetAllMenu(Status, Type));

        }

        private void SetGrid(DataTable DtMenu)
        {
            if (dataGridView_Show.Rows.Count > 0) dataGridView_Show.Rows.Clear();

            if (DtMenu.Rows.Count > 0)
            {
                foreach (DataRow row in DtMenu.Rows)
                {
                    dataGridView_Show.Rows.Add(row["MENU_SEQ"], row["MENU_ID"], row["MENU_NAME"], row["MENU_DESC"], row["MENU_STATUS"], row["MENU_TYPE"]);
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่มีข้อมูล รายละเอียดเมนู");
                return;
            }
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (textBox_Id.Text == string.Empty)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ID MENU");
                textBox_Id.Focus();
                return;
            }
            if (textBox_name.Text == string.Empty)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อเมนู");
                textBox_name.Focus();
                return;
            }
            string type = string.Empty, status = "0";
            if (radioButton_SUPC.Checked == true)
            {
                type = "SUPC";
            }
            else if (radioButton_SHOP.Checked == true)
            {
                type = "SHOP";
            }
            if (checkBox_AddStatus.Checked == true)
            {
                status = "1";
            }

            string Ins = $@"insert into SHOP_MENU
                                (MENU_SEQ, MENU_ID, MENU_NAME, MENU_DESC, MENU_TYPE, MENU_STATUS,MENU_WHOINS,MENU_WHOUP)  
                            values('{Class.MenuPermissionClass.GetSeq(type)}', '{textBox_Id.Text.Trim()}', 
                            '{textBox_name.Text}','','{type}', '{status}', '{SystemClass.SystemUserID}','{SystemClass.SystemUserID}')";
            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(Ins));
        }


    }
}
