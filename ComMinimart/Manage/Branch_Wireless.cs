﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Data;
using System.Linq;
using System.Diagnostics;
using PC_Shop24Hrs.JOB;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class Branch_Wireless : Telerik.WinControls.UI.RadForm
    {
        DataTable dt = new DataTable();
        private string statusUpdate; //เพิ่ม 0,แก้ไข 1 
        private int iRow;
        public Branch_Wireless()
        {
            InitializeComponent();
            this.KeyPreview = true;

        }
        //OnKeyEvent
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            ConditionalFormattingForm form = (ConditionalFormattingForm)sender;
            DatagridClass.SetFormFont(form.Controls);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
            {
                e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
            }
            else
            {
                e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
            }
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            if (e.FilterPopup is RadListFilterPopup popup)
            {
                popup.MenuTreeElement.TreeView.NodeFormatting -= DatagridClass.TreeView_NodeFormatting;
                popup.MenuTreeElement.TreeView.NodeFormatting += DatagridClass.TreeView_NodeFormatting;
            }
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            BaseFilterPopup popup = (BaseFilterPopup)e.FilterPopup;
            DatagridClass.SetMenuFont(popup.Items);
            popup.Font = SystemClass.SetFontGernaral;
        }

        //private void RadGridView_Show_CellFormatting(object sender, CellFormattingEventArgs e)
        //{
        //    if (e.CellElement.RowElement is GridDataRowElement)
        //    {
        //        GridViewColumn validationInfo = GetColumnValidationInfo(e.CellElement.ColumnInfo);
        //        if (validationInfo != null)
        //        {
        //            if (e.CellElement.ColumnInfo.Name == validationInfo.Name)
        //            {
        //                if (e.CellElement.Text == "online")
        //                {
        //                    e.CellElement.BackColor = Color.FromArgb(253, 141, 142);
        //                }
        //            }
        //        }
        //    }
        //}
        //GridViewColumn GetColumnValidationInfo(GridViewColumn column)
        //{
        //    foreach (var item in RadGridView_Show.MasterTemplate.Columns)
        //    {
        //        if (item.Name == "PING")
        //        {
        //            return item;
        //        }
        //    }
        //    return null;
        //}
        #endregion
        //Load Main
        #region Event
        private void Branch_Wireless_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Branch);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_IP", "IP", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_REMARK", "คำอธิบาย", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA_110", "STA_110"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PING", "สถานะ", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("JOBID", "JOBID"));

            radCheckBox_110.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_110.ForeColor = ConfigClass.SetColor_Blue();

            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButtonElement_Delete.ShowBorder = true; radButtonElement_Delete.ToolTipText = "ลบข้อมูล";
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มข้อมูล";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขข้อมูล";
            radButtonElement_Refresh.ShowBorder = true; radButtonElement_Refresh.ToolTipText = "ดึงข้อมูลใหม่";
            radButtonElement_AddJob.ShowBorder = true; radButtonElement_AddJob.ToolTipText = "เปิด JOB"; //radButtonElement_AddJob.IsElementVisible = true;
            radStatusStrip1.SizingGrip = false;

            GetGridViewSummary();

            DatagridClass.SetCellBackClolorByExpression("PING", "PING = 'offline' ", ConfigClass.SetColor_Red(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("BRANCH_IP", "JOBID LIKE 'MJOB%' ", ConfigClass.SetColor_Red(), RadGridView_Show);

            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            ClearDataShow();
            SetEnabled(panel_Show.Controls, false);
            GetBranch();
            GetDataIP();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            statusUpdate = "0";
            ClearDataShow();
            radTextBox_IP.Focus();
            radDropDownList_Branch.SelectedIndex = 0;
            SetEnabled(panel_Show.Controls, true);
        }
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            statusUpdate = "1";
            DataRow[] rows = dt.Select(string.Format("BRANCH_ID = '{0}' AND BRANCH_IP = '{1}'"
                , this.RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), this.RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString()));
            iRow = dt.Rows.IndexOf(rows[0]);

            SetEnabled(panel_Show.Controls, true);
            radDropDownList_Branch.Enabled = false;

            if (RadGridView_Show.CurrentRow.Cells["STA_110"].Value.ToString() == "1") radCheckBox_110.Checked = true;

            radDropDownList_Branch.SelectedValue = this.GetSafeString(this.RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value);
            radTextBox_Rmk.Text = this.GetSafeString(this.RadGridView_Show.CurrentRow.Cells["BRANCH_REMARK"].Value);
            radTextBox_IP.Text = this.GetSafeString(this.RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value).Substring(RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString().Length - 3, 3);
            radLabel_Ip.Text = this.GetSafeString(this.RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value).Substring(0, RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString().Length - 3);
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearDataShow();
        }
        private void RadDropDownList_Branch_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (e.Position == -1) return;

            try
            {
                radLabel_Ip.Text = string.Format("10.0.{0}."
               , int.Parse(radDropDownList_Branch.SelectedValue.ToString().Substring(2, radDropDownList_Branch.SelectedValue.ToString().Length - 2)).ToString("0.#"));
            }
            catch
            {
                return;
            }
            finally
            {
                radTextBox_IP.Focus();
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radDropDownList_Branch.SelectedValue.ToString()) || string.IsNullOrEmpty(radTextBox_IP.Text)
                || string.IsNullOrEmpty(radTextBox_IP.Text) || string.IsNullOrEmpty(radTextBox_Rmk.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("เช็คข้อมูลทั้งหมดให้เรียบร้อยก่อนการบันทึก.");
                radTextBox_IP.Focus();
                return;
            }

            string sql;
            if (statusUpdate == "0")
            {
                sql = string.Format(@"INSERT INTO SHOP_BRANCH_IP (BRANCH_ID,BRANCH_NAME,BRANCH_IP,BRANCH_REMARK,STA_110,WHOIDINS,WHONAMEINS)
                          VALUES  ('" + radDropDownList_Branch.SelectedValue.ToString() + @"',
                                '" + BranchClass.GetBranchNameByID(radDropDownList_Branch.SelectedValue.ToString()) + @"',
                                '" + GetIpWireless() + @"',
                                '" + radTextBox_Rmk.Text + @"',
                                '{0}',
                                '{1}',
                                '{2}')"
                                , Check101status(), SystemClass.SystemUserID, SystemClass.SystemUserName);
            }
            else
            {
                sql = string.Format(@"UPDATE dbo.SHOP_BRANCH_IP 
                            SET 
                              BRANCH_IP =  '" + GetIpWireless() + @"',
                              BRANCH_REMARK =  '" + radTextBox_Rmk.Text + @"',
                              STA_110 = '{0}' WHERE BRANCH_ID = '{1}' AND BRANCH_IP = '{2}' "
                              , Check101status(), radDropDownList_Branch.SelectedValue.ToString(), GetIpWireless());

            }
            string ReturnExecut = ConnectionClass.ExecuteSQL_Main(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(ReturnExecut);
            //Updatdatagrid
            if (ReturnExecut == "")
            {
                if (statusUpdate == "0")
                {
                    dt.Rows.Add(radDropDownList_Branch.SelectedValue.ToString(),
                             BranchClass.GetBranchNameByID(radDropDownList_Branch.SelectedValue.ToString()),
                             GetIpWireless(),
                             radTextBox_Rmk.Text,
                             Check101status(),
                             Checkonline(GetIpWireless())
                           );
                }
                else
                {
                    dt.Rows[iRow]["BRANCH_IP"] = GetIpWireless();
                    dt.Rows[iRow]["BRANCH_REMARK"] = radTextBox_Rmk.Text;
                    dt.Rows[iRow]["STA_110"] = Check101status();
                    dt.Rows[iRow]["PING"] = Checkonline(GetIpWireless());
                }
            }
            dt.AcceptChanges();

            ClearDataShow();
        }
        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                RadGridView_Show.Rows[i].Cells["PING"].Value = Checkonline(RadGridView_Show.Rows[i].Cells["BRANCH_IP"].Value.ToString().Replace("http://", "").Replace("/", "").Replace(":81", "").Replace(":82", "").Replace("ie.htm", ""));
            }
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        private void RadButtonElement_Delete_Click(object sender, EventArgs e)
        {
            if (this.RadGridView_Show.CurrentRow != null)
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการลบข้อมูล ? " + RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString()) == DialogResult.Yes)
                {
                    DataRow[] rows = dt.Select("BRANCH_IP = '" + this.RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString() + "' ");
                    int iRow = dt.Rows.IndexOf(rows[0]);

                    string sqlIn = string.Format(@"DELETE FROM SHOP_BRANCH_IP WHERE BRANCH_ID = '{0}' AND BRANCH_IP = '{1}'"
                                    , RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString());

                    string returnstr = ConnectionClass.ExecuteSQL_Main(sqlIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);

                    if (returnstr == "")
                    {
                        dt.Rows[iRow].Delete();   //Delete
                        dt.AcceptChanges();
                    }
                }
            }
        }
        private void RadButtonElement_AddJob_Click(object sender, EventArgs e)
        {
            //ms conferm
            if (this.RadGridView_Show.CurrentRow != null)
            {
                if (RadGridView_Show.CurrentRow.Cells["JOBID"].Value.ToString() != "")
                {

                    JOB.Com.JOBCOM_EDIT frm = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComMinimart", "", "", "", "", RadGridView_Show.CurrentRow.Cells["JOBID"].Value.ToString());
                    if (frm.ShowDialog(this) == DialogResult.OK) return;
                }
                else
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ต้องการเปิด JOB {RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value} หรือไม่ ? ") == DialogResult.Yes)
                    {

                        DataTable dtJobGroup = JOB_Class.GetJOB_Group("00001");
                        string descJOB;
                        string typJOB = "SHOP";
                        descJOB = "- " + "ping ไม่เจอ";
                        if (SystemClass.SystemComMinimart == "1")
                        {
                            descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComMinimart] ";
                        }
                        else if (SystemClass.SystemDptID == "D179")
                        {
                            descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserNameShort + " [ComService] ";
                        }
                        else
                        {
                            descJOB += Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine + @" [" + SystemClass.SystemDptName + @"/" + SystemClass.SystemUserNameShort + "] ";
                        }
                        descJOB += @" [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ff") + "] ";

                        DataTable dtJobGroupSub = JOB_Class.GetJOB_GroupSub("00001");
                        DataRow[] dr = dtJobGroupSub.Select("JOBGROUPSUB_ID = '00008'");
                        //savejod
                        string billMaxNO = ConfigClass.GetMaxINVOICEID(dtJobGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString(), "-", dtJobGroup.Rows[0]["DOCUMENTTYPE_ID"].ToString(), "1");

                        string sql = string.Format(@"INSERT INTO {0} (JOB_Number,JOB_BRANCHID
                            ,JOB_BRANCHNAME,JOB_GROUPSUB,JOB_WHOINS,JOB_NAMEINS,JOB_SHORTNAMEINS
                            ,JOB_DESCRIPTION,JOB_IP,JOB_DATEINS,JOB_TYPE)  
                        VALUES (
                            '{1}'
                            ,'" + this.RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + @"'
                            ,'" + this.RadGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString() + @"'
                            ,'{2}'
                            ,'" + SystemClass.SystemUserID + @"'
                            ,'" + SystemClass.SystemUserName + @"'
                            ,'" + SystemClass.SystemUserNameShort + @"'
                            ,'{3}'
                            ,'" + this.RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString() + @"'
                            , GETDATE()
                            ,'{4}' )", dtJobGroup.Rows[0]["TABLENAME"].ToString()
                                    , billMaxNO
                                    , dr[0].ItemArray[0].ToString()
                                    , descJOB, typJOB);

                        string returnExecut = ConnectionClass.ExecuteSQL_Main(sql);
                        MsgBoxClass.MsgBoxShow_SaveStatus(returnExecut);
                        if (returnExecut == "")
                        {
                            RadGridView_Show.CurrentRow.Cells["JOBID"].Value = billMaxNO;
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            string ip;
            if (RadGridView_Show.CurrentRow.Cells["STA_110"].Value.ToString() == "1")
            {
                ip = RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString().Substring(0, RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString().Length - 3) + "110";
            }
            else
            {
                ip = RadGridView_Show.CurrentRow.Cells["BRANCH_IP"].Value.ToString();
            }

            try
            {
                Process.Start("iexplore.exe", ip);
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถเปิดลิ้งค์ได้ ลองใหม่อีกครั้ง" + ex.Message);
                return;
            }
        }
        #endregion

        #region Method
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("BRANCH_IP", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void ClearDataShow()
        {
            radTextBox_Rmk.Text = string.Empty;
            radTextBox_IP.Text = string.Empty;
            radLabel_Ip.Text = string.Empty;
            radCheckBox_110.Checked = false;
            radDropDownList_Branch.SelectedIndex = -1;
        }
        private void SetEnabled(Control.ControlCollection controlCollection, bool status)
        {
            if (controlCollection == null)
            {
                return;
            }
            foreach (RadTextBoxBase c in controlCollection.OfType<RadTextBoxBase>())
            {
                c.Enabled = status;
            }
            foreach (RadButton d in controlCollection.OfType<RadButton>())
            {
                d.Enabled = status;
            }
            foreach (RadCheckBox f in controlCollection.OfType<RadCheckBox>())
            {
                f.Enabled = status;
            }
            foreach (RadDropDownList g in controlCollection.OfType<RadDropDownList>())
            {
                g.Enabled = status;
            }
        }
        private void GetBranch()
        {
            //string sql = string.Format(@"select BRANCH_ID AS DATA_ID,BRANCH_ID +'-'+BRANCH_NAME AS DATA_DESC    
            //            from SHOP_BRANCH WITH (NOLOCK) 
            //            WHERE BRANCH_STA = '1' ");
            dt = BranchClass.GetBranchAll("'1'", "'0','1'");//  Controllers.ConnectionClass.SelectSQL_Main(sql);
            var ListPrice = radDropDownList_Branch;
            ListPrice.DataSource = dt;
            ListPrice.DisplayMember = "NAME_BRANCH";
            ListPrice.ValueMember = "BRANCH_ID";
            radDropDownList_Branch.SelectedIndex = -1;
        }
        private void GetDataIP()
        {
            string sql = $@"
                SELECT  SHOP_BRANCH_IP.BRANCH_ID AS BRANCH_ID,SHOP_BRANCH_IP.BRANCH_NAME AS BRANCH_NAME,
                        BRANCH_IP ,SHOP_BRANCH_IP.BRANCH_REMARK AS BRANCH_REMARK,ISNULL(STA_110,'0') AS STA_110,'' AS PING ,ISNULL(JOB_Number,'') AS JOBID
                FROM    SHOP_BRANCH_IP WITH(NOLOCK)
                        LEFT OUTER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_BRANCH.BRANCH_ID = SHOP_BRANCH_IP.BRANCH_ID
						LEFT OUTER JOIN  SHOP_JOBComMinimart WITH (NOLOCK)  ON SHOP_JOBComMinimart.JOB_IP = SHOP_BRANCH_IP.BRANCH_IP  AND JOB_STACLOSE = '0'
                order by SHOP_BRANCH_IP.BRANCH_ID ";
            dt = ConnectionClass.SelectSQL_Main(sql);

            if (dt.Rows.Count > 0)
            {
                RadGridView_Show.DataSource = dt;
                for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                {
                    RadGridView_Show.Rows[i].Cells["PING"].Value = Checkonline(RadGridView_Show.Rows[i].Cells["BRANCH_IP"].Value.ToString().Replace("http://", "").Replace("/", "").Replace(":81", "").Replace(":82", "").Replace("ie.htm", ""));
                }
            }
        }
        private string Checkonline(string pIpwireless)
        {
            string pingStatus = "offline";
            try
            {
                System.Net.NetworkInformation.Ping myPing = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply reply = myPing.Send(pIpwireless, 1000);
                if (reply != null)
                {
                    if (reply.Status.ToString() == "Success")
                    {
                        pingStatus = "online";
                    }
                }
            }
            catch (Exception ex)
            {
                pingStatus = "ไม่พบข้อมูล" + ex.Message;
            }
            return pingStatus;
        }
        private string GetSafeString(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            return value.ToString();
        }
        private string Check101status()
        {
            if (radCheckBox_110.Checked == true)  return "1";  else  return "0"; 
        }
        private string GetIpWireless()
        {
            return radLabel_Ip.Text.Trim() + radTextBox_IP.Text.Trim();
        }
        #endregion
    }
}
