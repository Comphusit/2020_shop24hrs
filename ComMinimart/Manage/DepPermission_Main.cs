﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class DepPermission_Main : Form
    {
        readonly string _pType;

        public DepPermission_Main(string pType)
        {
            InitializeComponent();

            radStatusStrip.SizingGrip = false;
            _pType = pType;
        }

        private DataTable GetMenu()
        {
            string sql = $@"
                SELECT  MENU_ID,MENU_NAME,MENU_TYPE 
                FROM    SHOP_MENU with(nolock) 
                WHERE   MENU_ID !='D999' AND MENU_STATUS ='1' AND MENU_TYPE = '{_pType}'
                ORDER BY MENU_TYPE,MENU_ID";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetMenuPermission()
        {
            string sql = $@"
                SELECT  DEPT_ID,SHOP_MENU_PERRMISSION.MENU_ID 
                FROM    SHOP_MENU_PERRMISSION WITH (NOLOCK) 
                        INNER JOIN SHOP_MENU  WITH (NOLOCK)  on SHOP_MENU.MENU_ID = SHOP_MENU_PERRMISSION.MENU_ID 
                WHERE   MENU_TYPE = '{_pType}'
                ORDER BY DEPT_ID,SHOP_MENU_PERRMISSION.MENU_ID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //private DataTable GetDeptforsetMenu()
        //{
        //    string sql = $@"
        //        SELECT  DEPT_ID,DESCRIPTION  
        //        FROM    SHOP_DEPT_PERRMISSION with(nolock) 
        //                inner join SHOP2013TMP.dbo.DIMENSIONS  with(nolock)  on SHOP_DEPT_PERRMISSION.DEPT_ID=DIMENSIONS.NUM 
        //        WHERE   DATAAREAID ='SPC' AND DIMENSIONCODE = '0'
        //        ORDER BY DEPT_ID";
        //    return ConnectionClass.SelectSQL_Main(sql); ;
        //}
        //private DataTable GetDeptNotInDatagrid()
        //{
        //    string sql = string.Format(@"
        //        SELECT	num,num+ ' '+DESCRIPTION  as Dimensionname 
        //        FROM	SHOP2013TMP.dbo.DIMENSIONS with(nolock)
        //        WHERE	num not in (select dept_id from SHOP_DEPT_PERRMISSION with(nolock) ) 
        //          AND DIMENSIONCODE = 0 
        //          AND COMPANYGROUP !='Cancel' 
        //          AND DESCRIPTION !=''
        //    ");
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //void SetList(DataTable DtList)
        //{
        //    radDropDownList_Dept.DataSource = DtList;
        //    radDropDownList_Dept.ValueMember = "num";
        //    radDropDownList_Dept.DisplayMember = "Dimensionname";
        //}


        void ClearTools(Boolean status)
        {
            if (status)
            {
                radDropDownList_Dept.Enabled = true;
                radButton_Save.Enabled = true;
                radTextBox_Remark.Enabled = true;
            }
            else
            {
                radDropDownList_Dept.Enabled = false;
                radButton_Save.Enabled = false;
                radTextBox_Remark.Enabled = false;
            }
        }

        private void ShowDatagrid()
        {
            this.Cursor = Cursors.WaitCursor;
            ClearTools(false);

            //radDropDownList_Dept.DataSource = ManageAndCameraClass.GetDataDptPermissionDptMenu("1",_pType);
            radDropDownList_Dept.DataSource = Models.ManageCameraClass.GetDataDptPermissionDptMenu("1", _pType);
            radDropDownList_Dept.ValueMember = "num";
            radDropDownList_Dept.DisplayMember = "Dimensionname";

            DataTable dtMenu = GetMenu();
            //DataTable dtDept = ManageAndCameraClass.GetDataDptPermissionDptMenu("0", _pType);//GetDeptforsetMenu();
            DataTable dtDept = Models.ManageCameraClass.GetDataDptPermissionDptMenu("0", _pType);//GetDeptforsetMenu();
            DataTable dtMenuPermission = GetMenuPermission();


            if (dtDept.Rows.Count < 0 || dtMenu.Rows.Count < 0 || dtMenuPermission.Rows.Count < 0) return;
            if (radGridView_Show.RowCount > 0) radGridView_Show.Rows.Clear(); ;
            if (radGridView_Show.ColumnCount > 0) radGridView_Show.Columns.Clear();

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dept);

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Add.ShowBorder = true;
            radButton_Refresh.ShowBorder = true;


            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPT_ID", "รหัส", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 250));

            this.radGridView_Show.MasterTemplate.Columns["DEPT_ID"].IsPinned = true;
            this.radGridView_Show.MasterTemplate.Columns["DESCRIPTION"].IsPinned = true;


            foreach (DataRow rowsMenu in dtMenu.Rows)
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual(
                                                            rowsMenu["MENU_ID"].ToString(), rowsMenu["MENU_ID"] +
                                                            Environment.NewLine + rowsMenu["MENU_NAME"], 160));

            }


            foreach (DataRow rowsDept in dtDept.Rows)
            {
                radGridView_Show.Rows.Add(rowsDept["DEPT_ID"].ToString(), rowsDept["DESCRIPTION"].ToString());
                DataRow[] results = dtMenuPermission.Select("DEPT_ID = '" + rowsDept["DEPT_ID"].ToString() + @"'");

                for (int i = 0; i < results.Length; i++)
                {
                    radGridView_Show.Rows[radGridView_Show.Rows.Count - 1].Cells[results[i].ItemArray[1].ToString()].Value = true;
                }
            }
            this.Cursor = Cursors.Default;
        }

        private void DepPermission_Main_Load(object sender, EventArgs e)
        {
            ShowDatagrid();
        }

        //Event Keyboard
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        #region "SetGridRow"
        //ROWS NUMBERS
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion


        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            ClearTools(true);
        }


        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string sqlUp = $@"insert into SHOP_DEPT_PERRMISSION (DEPT_ID, REMARK, DATETIMEIN, WHOIN, DATETIMEUP, WHOUP)
                              values    ('{radDropDownList_Dept.SelectedValue}', '{radTextBox_Remark.Text}', 
                                    convert(varchar,getdate(),20), '{SystemClass.SystemUserID}', convert(varchar,getdate(),20) , '{2}') ";
            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sqlUp));
            ShowDatagrid();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTools(false);
        }

        private void RadButton_Refresh_Click(object sender, EventArgs e)
        {
            ShowDatagrid();
        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.ColumnIndex > 1)
            {
                string sqlUp;
                if (radGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value is null)
                {
                    radGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = true;
                    sqlUp = $@"insert into SHOP_MENU_PERRMISSION(DEPT_ID, MENU_ID, REMARK, DATETIMEIN, WHOIN, DATETIMEUP, WHOUP) 
                               values('{radGridView_Show.CurrentRow.Cells[0].Value}', '{radGridView_Show.CurrentCell.Data.FieldName}', 
                                    '',convert(varchar,getdate(),20) , '{2}', convert(varchar,getdate(),20), '{SystemClass.SystemUserID}') ";
                }
                else
                {
                    radGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = false;
                    sqlUp = $@"delete SHOP_MENU_PERRMISSION where DEPT_ID='{radGridView_Show.CurrentRow.Cells[0].Value}' and MENU_ID='{radGridView_Show.CurrentCell.Data.FieldName}' ";
                }
                MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sqlUp));
            }
        }

       
    }
}