﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class ChangePassword : Telerik.WinControls.UI.RadForm
    {
        public string _EmplID = string.Empty;
        public string _EmplName = string.Empty;
        public string _OldPassword = string.Empty;


        string PASSWORD = string.Empty;

        Data_EMPLTABLE emp;

        public ChangePassword()
        {
            InitializeComponent();
        }
        void ClearText()
        {
            label_EmplID.Text = string.Empty;
            label_EmplName.Text = string.Empty;
            TextBox_OldPassWord.Text = string.Empty;
            TextBox_NewPassWord1.Text = string.Empty;
            TextBox_NewPassWord2.Text = string.Empty;
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            ClearText();

            label_EmplID.Text = _EmplID;
            label_EmplName.Text = _EmplName;
            emp = new Data_EMPLTABLE(_EmplID);

            if (emp.EMPLTABLE_EMPLID == "")
            {
                TextBox_OldPassWord.SelectAll();
                TextBox_OldPassWord.Focus();
                TextBox_OldPassWord.Enabled = false;
                PASSWORD = string.Empty;
            }
            else
            {
                emp.EMPLTABLE_ALTNUM = _EmplID;
                if (emp.EMPLTABLE_EMP_PASSWORD is null)
                {
                    TextBox_OldPassWord.Enabled = false;
                    PASSWORD = string.Empty;
                }
                else
                {
                    PASSWORD = Class.EncryptDataClass.DecodePassword(emp.EMPLTABLE_EMP_PASSWORD);
                }

                TextBox_NewPassWord1.Focus();
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (TextBox_OldPassWord.Text != PASSWORD && PASSWORD != string.Empty)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กรุณาใส่รหัสผ่านเก่าให้ถูกต้อง ");
                TextBox_OldPassWord.Focus();
                TextBox_OldPassWord.SelectAll();
                return;
            }

            if (TextBox_NewPassWord1.Text != TextBox_NewPassWord2.Text)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสผ่านใหม่ไม่ถูกต้อง กรุณาแก้ไข");
                TextBox_NewPassWord2.Focus();
                TextBox_NewPassWord2.SelectAll();
                return;
            }

            if (TextBox_NewPassWord1.Text == _EmplID)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กรุณาเปลี่ยนรหัสผ่าน ไม่สามารถใช้รหัสพนักงานได้");
                TextBox_NewPassWord1.Focus();
                TextBox_NewPassWord1.SelectAll();
                return;
            }


            string Ins;
            if (TextBox_OldPassWord.Enabled == false)
            {
                Ins = Models.EmplClass.Save_EMPLOYEE("0", emp.EMPLTABLE_ALTNUM,
                               Manage.SetPasswordClass.EncodePassword(this.TextBox_NewPassWord1.Text), "");
            }
            else
            {
                Ins = Models.EmplClass.Save_EMPLOYEE("1", emp.EMPLTABLE_ALTNUM, Manage.SetPasswordClass.EncodePassword(this.TextBox_NewPassWord1.Text), "");
            }
            try
            {
                string T = Controllers.ConnectionClass.ExecuteSQL_Main(Ins);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                this.DialogResult = DialogResult.Yes;
                this.Close();

            }
            catch
            {
                this.Close();
            }
        }

        private void TextBox_OldPassWord_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) TextBox_NewPassWord1.Focus();
        }

        private void TextBox_NewPassWord1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) TextBox_NewPassWord2.Focus();
        }

        private void TextBox_NewPassWord2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Save.Focus();
        }
    }
}
