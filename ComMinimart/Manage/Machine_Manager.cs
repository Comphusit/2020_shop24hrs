﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Collections;
using Telerik.WinControls.Data;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class Machine_Manager : Form
    {
        string Updatestatus;
        readonly string _pPermisstion;
        DataTable dt = new DataTable();
        //DataTable dtAsset = new DataTable();
        private ArrayList ItemsProgram = new ArrayList();
        public Machine_Manager(string pPermisstion)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pPermisstion = pPermisstion;

        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }

        #region "ROWSNUMBER"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        void SetPrograme()
        {
            this.radCheckedDropDownList_Program.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("15", "", "", "1");
            this.radCheckedDropDownList_Program.DisplayMember = "SHOW_DESC";
            this.radCheckedDropDownList_Program.ValueMember = "SHOW_NAME";
            DatagridClass.SetDefaultFontDropDownShot(radCheckedDropDownList_Program);
        }
        private void Machine_Manager_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COM_NAME", "ชื่อเครื่อง", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ID", "ID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "แผนก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DEPT_ID", "DEPT_ID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ใช้งานที่", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SHOW_ID", "PROGRAMCODE"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "โปรแกรม", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VERSIONUPD", "เวอร์ชัน", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PRICEGROUP", "ระดับราคา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHANNEL", "คลัง", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAMEUPD", "ใช้งานล่าสุด", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEUPD", "เวลา", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("COM_ALLBRANCH", "COM_ALLBRANCH"));
            //Freeze Column
            this.radGridView_Show.MasterTemplate.Columns["COM_NAME"].IsPinned = true;

            DatagridClass.SetTextControls(radPanel_Information.Controls, (int)(Camera.CollectionControl.RadTextBoxBase));
            DatagridClass.SetTextControls(radPanel_Information.Controls, (int)(Camera.CollectionControl.RadCheckBox));
            DatagridClass.SetTextControls(radPanel_Information.Controls, (int)(Camera.CollectionControl.RadDropDownList));

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Channel);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_PriceGroup);

            this.radDropDown_Branch.DropDownListElement.DropDownWidth = 200;
            this.radDropDown_Branch.DropDownListElement.DropDownHeight = 200;
            this.radDropDown_Dpt.DropDownListElement.DropDownWidth = 200;
            this.radDropDown_Dpt.DropDownListElement.DropDownHeight = 200;

            radButton_Add.ShowBorder = true;
            radButton_Edit.ShowBorder = true;
            radButton_Delet.ShowBorder = true;
            radButtonElement_Refresh.ShowBorder = true;

            //diabel
            radStatusStrip.SizingGrip = false;
            this.radCheckedDropDownList_Program.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("15", "", "", "1");
            this.radCheckedDropDownList_Program.DisplayMember = "SHOW_DESC";
            this.radCheckedDropDownList_Program.ValueMember = "SHOW_NAME";
            DatagridClass.SetDefaultFontDropDownShot(radCheckedDropDownList_Program);
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            GetGridViewSummary();

            LoadData();
        }
        //LoadData
        void LoadData()
        {
            BindDataDropDownList();
            SetDefaultPanal(false);
            //BindDropDownListCannel();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            //dt = ManageAndCameraClass.GetDataPermissionComName();
            dt = Models.ManageCameraClass.GetDataPermissionComName();
            radGridView_Show.DataSource = dt; //GetdataComName();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });

            if (SystemClass.SystemComProgrammer == "1") radRadioButton_AllBranch.Visible = true;

            if (_pPermisstion == "1")
            {
                radCheckedDropDownList_Program.SelectedValue = "A02";
                radCheckedDropDownList_Program.Enabled = false;
                GridViewfilter(radGridView_Show.Columns["SHOW_ID"], "A02");
                GridViewfilter(radGridView_Show.Columns["ID"], "MN000");
            }
        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("BRANCH_ID", "{0:F2}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void BindDataDropDownList()
        {
            if (_pPermisstion == "1") radDropDown_Branch.DataSource = Class.BranchClass.GetBranchAll("'2','4'", "'1'");
            else radDropDown_Branch.DataSource = Class.BranchClass.GetBranchAll("'1','2','3','4'", "'0','1'");

            radDropDown_Branch.DisplayMember = "NAME_BRANCH";
            radDropDown_Branch.ValueMember = "BRANCH_ID";
            radDropDown_Branch.SelectedIndex = -1;

            radDropDown_Dpt.DataSource = Models.DptClass.GetDpt_AllD();
            radDropDown_Dpt.DisplayMember = "DESCRIPTION_SHOW";
            radDropDown_Dpt.ValueMember = "NUM";
            radDropDown_Dpt.SelectedIndex = -1;
        }
        private void BindDropDownListCannel()
        {
            radDropDownList_Channel.DataSource = Models.InventClass.Find_InventSupc("Machine");
            radDropDownList_Channel.DisplayMember = "INVENTLOCATIONNAME";
            radDropDownList_Channel.ValueMember = "INVENTLOCATIONID";
            radDropDownList_Channel.SelectedIndex = -1;

            //string strprice = $@"
            //      SELECT COLUMNNAME
            //            ,LABEL
            //      FROM  SHOP_PRICE WITH (NOLOCK)
            //      ORDER BY COLUMNNAME  ASC ";
            DataTable dt_price = BranchClass.GetPriceLevel();// Controllers.ConnectionClass.SelectSQL_Main(strprice);

            radDropDownList_PriceGroup.DataSource = dt_price;
            radDropDownList_PriceGroup.DisplayMember = "LABEL";
            radDropDownList_PriceGroup.ValueMember = "COLUMNNAME";
            radDropDownList_PriceGroup.SelectedIndex = -1;
        }
        private void SetDefaultPanal(bool value)
        {
            if (value == false)
            {
                radTextBox_Spc.Text = string.Empty;
                radDropDown_Branch.SelectedIndex = -1;
                radDropDown_Dpt.SelectedIndex = -1;
                radCheckedDropDownList_Program.Text = string.Empty;
            }

            radLabel_Label.Visible = value;
            radTextBox_Spc.Enabled = value;
            radRadioButton_Branch.Enabled = value;
            radRadioButton_AllBranch.Enabled = value;
            radDropDown_Branch.Enabled = value;
            radDropDown_Dpt.Visible = value;
            radLabel_Dept.Visible = value;
            radButton_Save.Enabled = value;
            radButton_Cancel.Enabled = value;

            SetDefaultPanal_Location(false);
        }
        private void SetDefaultPanal_Location(bool value)
        {
            radLabel_Channel.Visible = value;
            radDropDownList_Channel.Visible = value;
            radLabel_sup.Visible = value;
            radLabel_PriceGroup.Visible = value;
            radDropDownList_PriceGroup.Visible = value;
            radLabel_sups.Visible = value;

            BindDropDownListCannel();
        }
        //private DataTable GetdataComName()
        //{
        //    //string sql = $@"
        //    //SELECT	COM_NAME,
        //    //  CASE WHEN SHOP_BRANCH_PERMISSION.BRANCH_ID = 'MN000' THEN SHOP_BRANCH_PERMISSION.DEPT_ID ELSE SHOP_BRANCH_PERMISSION.BRANCH_ID END AS BRANCH_ID,COM_ALLBRANCH,
        //    //  SHOP_BRANCH_PERMISSION.BRANCH_ID AS ID,SHOP_BRANCH_PERMISSION.DEPT_ID,
        //    //  CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN DIMENSIONS.[DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
        //    //  SHOP_BRANCH_PERMISSION.SHOW_ID AS SHOW_ID,SHOW_DESC, VERSIONUPD, PRICEGROUP, CHANNEL, BRANCHUPD,
        //    //  SHOP_BRANCH_PERMISSION.DATEUPD AS DATEUPD,SHOP_BRANCH_PERMISSION.WHOIDUPD AS WHOIDUPD,
        //    //  SHOP_BRANCH_PERMISSION.WHONAMEUPD AS WHONAMEUPD,SHOP_BRANCH_PERMISSION.REMARK 
        //    //FROM	SHOP24HRS.dbo.SHOP_BRANCH_PERMISSION WITH (NOLOCK)
        //    //  INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = SHOP_BRANCH_PERMISSION.BRANCH_ID
        //    //  LEFT OUTER JOIN  [SHOP_CONFIGBRANCH_GenaralDetail] ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = SHOP_BRANCH_PERMISSION.SHOW_ID  AND   TYPE_CONFIG = '15'       
        //    //        LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON SHOP_BRANCH_PERMISSION.DEPT_ID = DIMENSIONS.NUM
        //    //ORDER BY SHOW_ID ASC, BRANCH_ID,DEPT_ID ";

        //    dt = CameraClass.GetDataPermissionComName(); //ConnectionClass.SelectSQL_Main(sql);
        //    return dt;
        //}
        private void UpdatePanelInfo(GridViewRowInfo currentRow)
        {
            if (dt.Rows.Count > 0 && currentRow != null && !(currentRow is GridViewNewRowInfo))
            {
                this.radTextBox_Spc.Text = this.GetSafeString(currentRow.Cells["COM_NAME"].Value);
                radDropDown_Branch.SelectedValue = this.GetSafeString(currentRow.Cells["BRANCH_ID"].Value);

                if (this.GetSafeString(currentRow.Cells["BRANCH_ID"].Value).Contains("D"))
                {
                    radDropDown_Dpt.SelectedValue = this.GetSafeString(currentRow.Cells["DEPT_ID"].Value);
                    radLabel_Dept.Visible = true;
                    radDropDown_Dpt.Visible = true;
                }
                if (this.GetSafeString(currentRow.Cells["SHOW_DESC"].Value).Equals("SUPC_Support"))
                {
                    SetDefaultPanal_Location(true);
                    radDropDownList_Channel.SelectedValue = this.GetSafeString(currentRow.Cells["CHANNEL"].Value);
                    radDropDownList_PriceGroup.SelectedValue = this.GetSafeString(currentRow.Cells["PRICEGROUP"].Value);
                }//สำหรับการ Fix WH ของโปรแกรม ProductRecive_DROID
                if (this.GetSafeString(currentRow.Cells["SHOW_DESC"].Value).Equals("ProductRecive_DROID"))
                {
                    SetDefaultPanal_Location(true);
                    radDropDownList_Channel.SelectedValue = this.GetSafeString(currentRow.Cells["CHANNEL"].Value);
                }

                radCheckedDropDownList_Program.SelectedValue = this.GetSafeString(currentRow.Cells["SHOW_ID"].Value);
                radCheckedDropDownList_Program.Enabled = false;

                if (this.GetSafeString(currentRow.Cells["COM_ALLBRANCH"].Value) == "1") radRadioButton_AllBranch.IsChecked = true;
                else radRadioButton_AllBranch.IsChecked = false;

                radRadioButton_AllBranch.Enabled = true;
                radRadioButton_Branch.Enabled = true;
            }
            else
            {
                SetDefaultPanal(false);
            }
        }
        private string GetSafeString(object value)
        {
            if (value == null) return string.Empty; else return value.ToString();
        }
        private ArrayList GetDataProgramInsert(RadCheckedDropDownList CheckedDropDown)
        {
            ArrayList Items = new ArrayList();
            foreach (RadListDataItem item in CheckedDropDown.CheckedItems)
            {
                Items.Add(item.Value.ToString());
            }
            return Items;
        }
        private string CheckProgramCode(RadDropDownList CheckedDropDown)
        {
            return CheckedDropDown.SelectedValue.ToString();
        }
        private string GetAllbranch(RadRadioButton radRadio)
        {
            if (radRadio.IsChecked == true) return "1"; else return "0";
        }
        private string GetSelectedValueDropDown(RadDropDownList radDropDown)
        {
            if (radDropDown.Text != "") return radDropDown.SelectedValue.ToString();
            else
            {
                if (radDropDown.Name == "radDropDown_Branch") radDropDown.SelectedValue = "MN000"; else radDropDown.SelectedValue = "D999";
                return radDropDown.SelectedValue.ToString();
            }
        }
        private string RetValue(RadDropDownList radDropDown)
        {
            if (string.IsNullOrEmpty((string)radDropDown.SelectedValue)) return ""; else return radDropDown.SelectedValue.ToString();
        }
        //private string GetLocationLabel()
        //{
        //    if (radDropDown_Branch.SelectedValue.ToString() == "MN000")
        //    {
        //        return radDropDown_Dpt.Text.ToString().Substring(7, radDropDown_Dpt.Text.ToString().Length - 7);
        //    }
        //    else
        //    {
        //        return radDropDown_Branch.Text.ToString().Substring(6, radDropDown_Branch.Text.ToString().Length - 6);
        //    }
        //}
        //private DataTable GetCode_BySpc(string pSpcName, string pCodeID)
        //{
        //    string con = "";
        //    if (pCodeID != "") con = $@"AND SHOP_BRANCH_PERMISSION.SHOW_ID = '{pCodeID}'";
        //    string sql = $@"
        //        SELECT  SHOP_BRANCH_PERMISSION.SHOW_ID,SHOW_DESC FROM SHOP_BRANCH_PERMISSION LEFT OUTER JOIN  
        //                [SHOP_CONFIGBRANCH_GenaralDetail] ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = SHOP_BRANCH_PERMISSION.SHOW_ID AND STAUSE = '1' AND SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '15'         
        //        WHERE   COM_NAME = '{pSpcName}'  {con}";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        #region"Event"
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Spc.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("");
                radTextBox_Spc.Focus();
            }
            else
            {
                DataRow[] rows;
                //int iRow = 0;
                string returnExecut;
                ArrayList sqlIn = new ArrayList();

                if (Updatestatus == "0")
                {
                    ItemsProgram = GetDataProgramInsert(radCheckedDropDownList_Program);
                    if (ItemsProgram.Count == 0)
                    {
                        //Insert
                        sqlIn.Add(string.Format(@"INSERT INTO dbo.SHOP_BRANCH_PERMISSION
                            ( COM_NAME
                              ,COM_ALLBRANCH
                              ,BRANCH_ID
                              ,DEPT_ID 
                              ,SHOW_ID
                              ,WHOINS
                               )
                            VALUES  
                            ('" + radTextBox_Spc.Text.Trim().ToUpper() + @"',
                            '" + GetAllbranch(radRadioButton_AllBranch) + @"',
                            '" + GetSelectedValueDropDown(radDropDown_Branch) + @"',
                            '" + GetSelectedValueDropDown(radDropDown_Dpt) + @"',
                            'A01',
                            '" + SystemClass.SystemUserID + @"')"));
                    }
                    else
                    {
                        for (int i = 0; i < ItemsProgram.Count; i++)
                        {
                            string invent = RetValue(radDropDownList_Channel);
                            string price = RetValue(radDropDownList_PriceGroup);

                            if (CheckInvent(ItemsProgram[i].ToString()) == false) return;

                            sqlIn.Add(string.Format(@"INSERT INTO dbo.SHOP_BRANCH_PERMISSION
                            ( COM_NAME
                              ,COM_ALLBRANCH
                              ,BRANCH_ID
                              ,DEPT_ID
                              ,SHOW_ID
                              ,WHOINS
                              ,PRICEGROUP
                              ,CHANNEL
                                   )
                            VALUES  
                            ('" + radTextBox_Spc.Text.Trim().ToUpper() + @"',
                            '" + GetAllbranch(radRadioButton_AllBranch) + @"',
                            '" + GetSelectedValueDropDown(radDropDown_Branch) + @"',
                            '" + GetSelectedValueDropDown(radDropDown_Dpt) + @"',
                            '" + ItemsProgram[i] + @"',
                            '" + SystemClass.SystemUserID + @"',
                            '" + price + @"',
                            '" + invent + @"')"));
                        }
                    }
                }
                else
                {
                    ItemsProgram = GetDataProgramInsert(radCheckedDropDownList_Program);

                    for (int i = 0; i < ItemsProgram.Count; i++)
                    {
                        rows = dt.Select(string.Format(@"COM_NAME = '{0}' AND SHOW_ID = '{1}' ", radTextBox_Spc.Text, ItemsProgram[i]));

                        if (rows.Length > 0)
                        {
                            int iRow = dt.Rows.IndexOf(rows[0]);
                            if (CheckInvent(dt.Rows[iRow]["SHOW_ID"].ToString()) == false) return;
                            //update
                            sqlIn.Add(string.Format(@"UPDATE dbo.SHOP_BRANCH_PERMISSION
                                SET 
                                  COM_ALLBRANCH =   '" + GetAllbranch(radRadioButton_AllBranch) + @"'
                                  ,BRANCH_ID =  '" + GetSelectedValueDropDown(radDropDown_Branch) + @"'
                                  ,DEPT_ID =  '" + GetSelectedValueDropDown(radDropDown_Dpt) + @"'
                                  ,SHOW_ID = '" + CheckProgramCode(radCheckedDropDownList_Program) + @"'
                                  ,DATEUPD = GETDATE()
                                  ,WHOIDUPD = '{0}'
                                  ,WHONAMEUPD = '{1}'
                                  ,PRICEGROUP = '{4}'
                                  ,CHANNEL = '{5}'
                                   WHERE COM_NAME = '{2}' AND SHOW_ID = '{3}'"
                                , SystemClass.SystemUserID
                                , SystemClass.SystemUserName
                                , radTextBox_Spc.Text
                                , dt.Rows[iRow]["SHOW_ID"].ToString()
                                , RetValue(radDropDownList_PriceGroup)
                                , RetValue(radDropDownList_Channel)));
                        }
                        else
                        {
                            if (CheckInvent(ItemsProgram[i].ToString()) == false) return;
                            //Insert
                            sqlIn.Add(string.Format(@"INSERT INTO dbo.SHOP_BRANCH_PERMISSION
                            ( COM_NAME
                              ,COM_ALLBRANCH
                              ,BRANCH_ID
                              ,DEPT_ID
                              ,SHOW_ID
                              ,WHOINS
                              ,PRICEGROUP
                              ,CHANNEL
                               )
                            VALUES  
                            ('" + radTextBox_Spc.Text.Trim().ToUpper() + @"',
                            '" + GetAllbranch(radRadioButton_AllBranch) + @"',
                            '" + GetSelectedValueDropDown(radDropDown_Branch) + @"',
                            '" + GetSelectedValueDropDown(radDropDown_Dpt) + @"',
                            '" + ItemsProgram[i] + @"',
                            '" + SystemClass.SystemUserID + @"',
                            '" + RetValue(radDropDownList_PriceGroup) + @"',
                            '" + RetValue(radDropDownList_Channel) + @"')"));
                        }
                    }
                }

                returnExecut = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExecut);

                if (returnExecut == "")
                {
                    this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
                    //dt = ManageAndCameraClass.GetDataPermissionComName();
                    dt = Models.ManageCameraClass.GetDataPermissionComName();
                    radGridView_Show.DataSource = dt; //GetdataComName();
                    this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                }
                else
                {
                    radTextBox_Spc.Focus();
                }

                SetDefaultPanal(false);
            }
        }

        Boolean CheckInvent(string A)
        {
            string invent = RetValue(radDropDownList_Channel);
            string price = RetValue(radDropDownList_PriceGroup);
            if (A == "A04")
            {
                if (invent == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"การตั้งค่าเครื่องโปรแกรม{Environment.NewLine}SUPC_Support{Environment.NewLine}จะต้องเลือกคลังด้วยเท่านั้น");
                    return false;
                }

                if (price == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"การตั้งค่าเครื่องโปรแกรม{Environment.NewLine}SUPC_Support{Environment.NewLine}จะต้องเลือกระดับราคาด้วย");
                    return false;
                }
            }

            if (A == "A08")
            {
                if (invent == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"การตั้งค่าเครื่องโปรแกรม{Environment.NewLine}ProductRecive_DROID{Environment.NewLine}จะต้องเลือกคลังด้วยเท่านั้น");
                    return false;
                }
                if (GetSelectedValueDropDown(radDropDown_Branch) != "MN000")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"การตั้งค่าเครื่องโปรแกรมProductRecive_DROID{Environment.NewLine}จะต้องเลือกสาขาเป็น MN000 เท่านั้น{Environment.NewLine}แต่ต้องระบุแผนกให้ถูกต้อง");
                    return false;
                }
            }

            return true;
        }
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            Updatestatus = "0";
            radTextBox_Spc.Enabled = true;
            radTextBox_Spc.Text = "SPC";
            radTextBox_Spc.TextBoxElement.Select();
            radLabel_Label.Text = "เพิ่ม";
            radDropDown_Branch.SelectedIndex = 1;
            SetDefaultPanal(true);
            SetDefaultPanal_Location(false);
            SetPrograme();
            if (_pPermisstion == "1")
            {
                radCheckedDropDownList_Program.SelectedValue = "A02";
                radDropDown_Branch.SelectedValue = "MN000";
            }

            radCheckedDropDownList_Program.Enabled = true;
        }
        private void RadButton_Edit_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.CurrentRow.Index > -1)
            {
                if (Updatestatus == "1") SetDefaultPanal(false);

                radLabel_Label.Visible = true;
                radLabel_Label.Text = "แก้ไข";
                Updatestatus = "1";
                radTextBox_Spc.Enabled = false;
                radRadioButton_Branch.Enabled = true;
                radDropDown_Branch.Enabled = true;
                radButton_Save.Enabled = true;
                radButton_Cancel.Enabled = true;

                if (_pPermisstion == "1")
                {
                    radCheckedDropDownList_Program.SelectedValue = "A02";
                    radDropDown_Branch.SelectedValue = "MN000";
                    radDropDown_Branch.Enabled = false;
                }

                UpdatePanelInfo(this.radGridView_Show.CurrentRow);
            }
        }
        private void RadButton_Delet_Click(object sender, EventArgs e)
        {
            if (this.radGridView_Show.CurrentRow as GridViewDataRowInfo != null)
            {
                DataRow[] rows = dt.Select($@"COM_NAME = '{this.radGridView_Show.CurrentRow.Cells["COM_NAME"].Value}' AND SHOW_ID = '{this.radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value }' ");
                int iRow = dt.Rows.IndexOf(rows[0]);

                if (MsgBoxClass.MsgBoxShow_ConfirmDelete(this.radGridView_Show.CurrentRow.Cells["COM_NAME"].Value.ToString() + "ที่ใช้งานกับ" + this.radGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value.ToString()) == DialogResult.Yes)
                {
                    string sqlIn = string.Format(@"DELETE FROM SHOP24HRS.dbo.SHOP_BRANCH_PERMISSION WHERE COM_NAME = '{0}' AND SHOW_ID = '{1}'"
                                , this.radGridView_Show.CurrentRow.Cells["COM_NAME"].Value.ToString(), this.radGridView_Show.CurrentRow.Cells["SHOW_ID"].Value.ToString());
                    string returnstr = ConnectionClass.ExecuteSQL_Main(sqlIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                    if (returnstr == "")
                    {
                        dt.Rows[iRow].Delete();   //Delete
                        dt.AcceptChanges();
                    }
                }
                else
                {
                    radTextBox_Spc.Focus();
                }
            }
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            SetDefaultPanal(false);
        }
        private void RadDropDown_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDown_Branch.SelectedIndex == 0)
            {
                radLabel_Dept.Visible = true;
                radDropDown_Dpt.Visible = true;
                radDropDown_Dpt.Enabled = true;
            }
            else
            {
                radDropDown_Dpt.SelectedValue = "D999";
                radDropDown_Dpt.Visible = false;
                radLabel_Dept.Visible = false;
            }
            if (Updatestatus == "1") radDropDown_Dpt.SelectedValue = radGridView_Show.CurrentRow.Cells["DEPT_ID"].Value;
        }
        private void RadDropDown_Dpt_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDown_Dpt.DataSource != null && radDropDown_Dpt.SelectedIndex > -1)
            {
                if (radDropDown_Dpt.SelectedValue.ToString() != "D999") radDropDown_Branch.SelectedValue = "MN000";
            }
        }
        private void RadRadioButton_Branch_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Branch.IsChecked == true) radDropDown_Branch.Enabled = true; else radDropDown_Branch.Enabled = false;
        }
        private void RadRadioButton_Dpt_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_AllBranch.IsChecked == true) radDropDown_Dpt.Enabled = true; else radDropDown_Dpt.Enabled = false;
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        #endregion


        private void GridViewfilter(GridViewDataColumn ColName, string Value)
        {
            FilterDescriptor filter = new FilterDescriptor(ColName.Name, FilterOperator.Contains, Value);
            ColName.FilterDescriptor = filter;
        }

        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void RadCheckedDropDownList_Program_ItemCheckedChanged(object sender, RadCheckedListDataItemEventArgs e)
        {
            if (radDropDown_Dpt.DataSource == null && radDropDown_Dpt.SelectedIndex == -1)
            {
                SetDefaultPanal_Location(false);
                return;
            }
            try
            {
                if (radCheckedDropDownList_Program.SelectedValue.Equals("A04"))
                {
                    SetDefaultPanal_Location(true);
                    radDropDownList_Channel.Focus();
                    radDropDownList_Channel.Enabled = true;
                    radDropDownList_PriceGroup.Enabled = true;
                    radLabel_sup.Text = "SUPC_Support";

                    if (this.GetSafeString(radGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value).Equals("SUPC_Support"))
                    {
                        radDropDownList_Channel.SelectedValue = this.GetSafeString(radGridView_Show.CurrentRow.Cells["CHANNEL"].Value);
                        radDropDownList_PriceGroup.SelectedValue = this.GetSafeString(radGridView_Show.CurrentRow.Cells["PRICEGROUP"].Value);
                    }
                }
                else if (radCheckedDropDownList_Program.SelectedValue.Equals("A08"))
                {
                    SetDefaultPanal_Location(true);
                    radDropDownList_Channel.Focus();
                    radDropDownList_Channel.Enabled = true;
                    radLabel_sup.Text = "DROID";

                    radDropDownList_PriceGroup.Visible = false;
                    radLabel_PriceGroup.Visible = false;
                    radLabel_sups.Visible = false;

                    if (this.GetSafeString(radGridView_Show.CurrentRow.Cells["SHOW_DESC"].Value).Equals("ProductRecive_DROID"))
                    {
                        radDropDownList_Channel.SelectedValue = this.GetSafeString(radGridView_Show.CurrentRow.Cells["CHANNEL"].Value);
                    }
                }
                else
                {
                    SetDefaultPanal_Location(false);
                }
            }
            catch (Exception)
            {
                SetDefaultPanal_Location(false);
            }
        }

    }
}
