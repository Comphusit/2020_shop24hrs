﻿//CheckOK
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;
using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class Empl_Manager : RadForm
    {
        Data_EMPLTABLE Emp;

        //Main Load
        public Empl_Manager()
        {
            InitializeComponent();
        }

        #region "FunctionAndDatagrid"

        void ClearText(string Button_type)
        {
            if (Button_type == "NEW")
            {
                RadButton_FindEmpl.Enabled = true;
                radTextBox_Empl.Enabled = true; radTextBox_Empl.Text = "";
                radTextBox_Remark.Enabled = true; radTextBox_Remark.Text = "";
                radButton_Save.Enabled = false;
                radButton_Cancel.Enabled = false;
                radButton_ResetPass.Enabled = false;

                radLabel_EmplName.Text = "";
                radLabel_Possition.Text = "";
                radLabel_Dept.Text = "";
                radLabel_Pass.Text = "";

                radTextBox_Empl.Focus();
            }
            else if (Button_type == "UPDATE")
            {
                RadButton_FindEmpl.Enabled = false;
                radTextBox_Empl.Enabled = false;
                radTextBox_Remark.Enabled = true;
                radButton_Save.Enabled = false;
                radButton_Cancel.Enabled = true;
                radButton_ResetPass.Enabled = true;
                radTextBox_Remark.Focus();
            }

        }
        void ClearTextDefaul()
        {
            RadButton_FindEmpl.Enabled = false;

            radTextBox_Empl.Enabled = false;
            radTextBox_Empl.Text = string.Empty;

            radLabel_EmplName.Text = string.Empty;
            radLabel_Dept.Text = string.Empty;
            radLabel_Possition.Text = string.Empty;

            radTextBox_Remark.Text = string.Empty;
            radTextBox_Remark.Enabled = false;

            radButton_Save.Enabled = false;
            radButton_Cancel.Enabled = false;
            radButton_ResetPass.Enabled = false;
            radLabel_Pass.Text = string.Empty;
            radTextBox_Empl.Focus();
        }

        void SetDatagrid()
        {
            RadGridView_EmplID.DataSource = Class.Models.EmplClass.GetEmpAll_SHOP24("");
        }

        #endregion

        private void Empl_Manager_Load(object sender, EventArgs e)
        {
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มพนักงาน";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขพนักงาน";
            RadButtonElement_refresh.ShowBorder = true; RadButtonElement_refresh.ToolTipText = "refresh";

            RadButton_FindEmpl.ButtonElement.ShowBorder = true; RadButton_FindEmpl.ButtonElement.ToolTipText = "ค้นหาพนักงาน";
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_ResetPass.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_EmplID);

            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("EMP_PASSWORD", "รหัสผ่าน"));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_ID", "รหัส", 80));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 300));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NUM", "รหัส", 80));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 300));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("IVZ_HROMPositionTitle", "รหัส", 80));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 220));
            RadGridView_EmplID.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 220));

            SetDatagrid();

            ClearTextDefaul();
        }

        #region "ROWS"
        private void RadGridView_EmplID_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }


        private void RadGridView_EmplID_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        private void RadGridView_EmplID_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_EmplID_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        #endregion



        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string SqlExecute = ConnectionClass.ExecuteSQL_Main(Models.EmplClass.Save_EMPLOYEE("0", this.Emp.EMPLTABLE_ALTNUM,
                        Manage.SetPasswordClass.EncodePassword(this.Emp.EMPLTABLE_ALTNUM), radTextBox_Remark.Text));
            MsgBoxClass.MsgBoxShow_SaveStatus(SqlExecute);
            if (SqlExecute == string.Empty)
            {
                SetDatagrid();
                ClearTextDefaul();
            }
        }

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearText("NEW");
            radTextBox_Empl.Focus();
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            ClearText("UPDATE");
        }

        private void RadTextBox_Cst_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Emp = new Data_EMPLTABLE(radTextBox_Empl.Text);

                if ((Emp.EMPLTABLE_EMPLID != "") && (Emp.EMPLTABLE_EMPLID != null))
                {
                    radTextBox_Empl.Text = Emp.EMPLTABLE_ALTNUM;
                    radLabel_EmplName.Text = Emp.EMPLTABLE_SPC_NAME;
                    radLabel_Possition.Text = Emp.EMPLTABLE_POSSITION;
                    radLabel_Dept.Text = Emp.EMPLTABLE_DESCRIPTION;

                    if (Emp.EMPLTABLE_STA_SHOP24 == "")
                    {
                        radButton_Save.Enabled = true;
                        radButton_ResetPass.Enabled = false; radTextBox_Remark.Focus();
                    }
                    else
                    {
                        radButton_ResetPass.Enabled = true; radButton_ResetPass.Focus();
                        radButton_Save.Enabled = false;
                        radTextBox_Empl.Enabled = false;
                    }
                    radButton_Cancel.Enabled = true;
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลพนักงาน");
                    radTextBox_Empl.SelectAll();
                }


            }
        }

        private void RadButton_ResetPass_Click(object sender, EventArgs e)
        {
            if (radTextBox_Empl.Text == string.Empty || radLabel_EmplName.Text == string.Empty)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุรหัสพนักงานก่อนทุกครั้ง .");
                radTextBox_Empl.SelectAll();

                return;
            }
            string SqlExecute = ConnectionClass.ExecuteSQL_Main(Models.EmplClass.Save_EMPLOYEE("1", radTextBox_Empl.Text.Trim(),
                   SetPasswordClass.EncodePassword(radTextBox_Empl.Text), radTextBox_Remark.Text));
            MsgBoxClass.MsgBoxShow_SaveStatus(SqlExecute);
            if (SqlExecute == string.Empty) SetDatagrid();

        }


        private void RadGridView_EmplID_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_EmplID.RowCount > 0) && (RadGridView_EmplID.CurrentRow.Index > -1))
            {
                radTextBox_Empl.Text = RadGridView_EmplID.CurrentRow.Cells["EMP_ID"].Value.ToString();

                radLabel_EmplName.Text = RadGridView_EmplID.CurrentRow.Cells["SPC_NAME"].Value.ToString();
                radLabel_Possition.Text = RadGridView_EmplID.CurrentRow.Cells["POSSITION"].Value.ToString();
                radLabel_Dept.Text = RadGridView_EmplID.CurrentRow.Cells["NUM"].Value.ToString() + System.Environment.NewLine +
                                        RadGridView_EmplID.CurrentRow.Cells["DESCRIPTION"].Value.ToString();
                radTextBox_Remark.Text = RadGridView_EmplID.CurrentRow.Cells["REMARK"].Value.ToString();

                radLabel_Pass.Text = SetPasswordClass.DecodePassword(RadGridView_EmplID.CurrentRow.Cells["EMP_PASSWORD"].Value.ToString());

                radButton_Save.Enabled = false; radButton_Cancel.Enabled = false; radTextBox_Remark.Enabled = false;
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTextDefaul();
        }

        private void RadButtonElement_refresh_Click(object sender, EventArgs e)
        {
            ClearTextDefaul();
            SetDatagrid();
        }


    }
}



