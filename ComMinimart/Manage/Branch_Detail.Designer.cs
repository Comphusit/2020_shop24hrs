﻿namespace PC_Shop24Hrs.ComMinimart.Manage
{
    partial class Branch_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Branch_Detail));
            this.radPanel_Branch = new Telerik.WinControls.UI.RadPanel();
            this.radButton_save = new Telerik.WinControls.UI.RadButton();
            this.radButton_cancle = new Telerik.WinControls.UI.RadButton();
            this.RadGroupBox_Programmer = new Telerik.WinControls.UI.RadGroupBox();
            this.radTextBox_PathPos = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_WH = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_SmartShop = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_FarmHouse = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_SendMoney = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_TaxLabel = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_TaxNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_DatabaseName = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Password = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_UserName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_ServerIp = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Serverspc = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadGroupBox_Center = new Telerik.WinControls.UI.RadGroupBox();
            this.radDropDownList_ec = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_ec = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_ComLocation = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_ComLocation = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_ComOT = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_ComOT = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Tel = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_FreeItem = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_StatusShelf = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel_ManagerName = new Telerik.WinControls.UI.RadLabel();
            this.radMultiColumnComboBox_Size = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radTextBox_Min = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Max = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Head = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_addr = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Address = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_size = new Telerik.WinControls.UI.RadLabel();
            this.RadGroupBox_Internet = new Telerik.WinControls.UI.RadGroupBox();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radTextBox_remarks = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_ServiceCall = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Serviceid = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Androidversion = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Upload = new Telerik.WinControls.UI.RadButton();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Service = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_PDAversion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.RadGroupBox_Gernaral = new Telerik.WinControls.UI.RadGroupBox();
            this.radDropDownList_VenderLeo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Beer = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_City = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Out = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Staopen = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Status = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Longtitude = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList_Price = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_BranchName = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Latitude = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Branch = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_level = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_SizeName = new Telerik.WinControls.UI.RadLabel();
            this.radOpenFileDialog1 = new Telerik.WinControls.UI.RadOpenFileDialog();
            this.radOpenFolderDialog1 = new Telerik.WinControls.UI.RadOpenFolderDialog();
            this.radLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_AddOnPer = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Branch)).BeginInit();
            this.radPanel_Branch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_cancle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Programmer)).BeginInit();
            this.RadGroupBox_Programmer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PathPos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_WH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SmartShop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_FarmHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SendMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_TaxLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_TaxNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DatabaseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Password)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_UserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ServerIp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Serverspc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Center)).BeginInit();
            this.RadGroupBox_Center.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_ec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ComLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ComLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ComOT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ComOT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_FreeItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_StatusShelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ManagerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox_Size)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox_Size.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Head)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_addr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Address)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_size)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Internet)).BeginInit();
            this.RadGroupBox_Internet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_remarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ServiceCall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Serviceid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Androidversion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Upload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Service)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PDAversion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Gernaral)).BeginInit();
            this.RadGroupBox_Gernaral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_VenderLeo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Beer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_City)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Out)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Staopen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Longtitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Latitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SizeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_AddOnPer)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel_Branch
            // 
            this.radPanel_Branch.Controls.Add(this.radButton_save);
            this.radPanel_Branch.Controls.Add(this.radButton_cancle);
            this.radPanel_Branch.Controls.Add(this.RadGroupBox_Programmer);
            this.radPanel_Branch.Controls.Add(this.RadGroupBox_Center);
            this.radPanel_Branch.Controls.Add(this.RadGroupBox_Internet);
            this.radPanel_Branch.Controls.Add(this.RadGroupBox_Gernaral);
            this.radPanel_Branch.Controls.Add(this.radLabel_SizeName);
            this.radPanel_Branch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel_Branch.Location = new System.Drawing.Point(0, 0);
            this.radPanel_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radPanel_Branch.Name = "radPanel_Branch";
            this.radPanel_Branch.Size = new System.Drawing.Size(849, 652);
            this.radPanel_Branch.TabIndex = 0;
            // 
            // radButton_save
            // 
            this.radButton_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_save.Location = new System.Drawing.Point(326, 605);
            this.radButton_save.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_save.Name = "radButton_save";
            this.radButton_save.Size = new System.Drawing.Size(90, 35);
            this.radButton_save.TabIndex = 1;
            this.radButton_save.Text = "บันทึก";
            this.radButton_save.ThemeName = "Fluent";
            this.radButton_save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_cancle
            // 
            this.radButton_cancle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_cancle.Location = new System.Drawing.Point(426, 605);
            this.radButton_cancle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_cancle.Name = "radButton_cancle";
            this.radButton_cancle.Size = new System.Drawing.Size(90, 35);
            this.radButton_cancle.TabIndex = 0;
            this.radButton_cancle.Text = "ยกเลิก";
            this.radButton_cancle.ThemeName = "Fluent";
            this.radButton_cancle.Click += new System.EventHandler(this.RadButton_cancle_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_cancle.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_cancle.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancle.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancle.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancle.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancle.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancle.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadGroupBox_Programmer
            // 
            this.RadGroupBox_Programmer.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_PathPos);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel28);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel21);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_WH);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_SmartShop);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_FarmHouse);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel16);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_SendMoney);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel17);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_TaxLabel);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_TaxNumber);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel18);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel19);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel20);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_DatabaseName);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_Password);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel12);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_UserName);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel6);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_ServerIp);
            this.RadGroupBox_Programmer.Controls.Add(this.radTextBox_Serverspc);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel8);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel5);
            this.RadGroupBox_Programmer.Controls.Add(this.radLabel3);
            this.RadGroupBox_Programmer.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGroupBox_Programmer.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.RadGroupBox_Programmer.HeaderText = "Config Programmer";
            this.RadGroupBox_Programmer.Location = new System.Drawing.Point(12, 13);
            this.RadGroupBox_Programmer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadGroupBox_Programmer.Name = "RadGroupBox_Programmer";
            this.RadGroupBox_Programmer.Padding = new System.Windows.Forms.Padding(2, 22, 2, 2);
            this.RadGroupBox_Programmer.Size = new System.Drawing.Size(200, 583);
            this.RadGroupBox_Programmer.TabIndex = 5;
            this.RadGroupBox_Programmer.Text = "Config Programmer";
            // 
            // radTextBox_PathPos
            // 
            this.radTextBox_PathPos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_PathPos.Location = new System.Drawing.Point(10, 190);
            this.radTextBox_PathPos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_PathPos.Name = "radTextBox_PathPos";
            this.radTextBox_PathPos.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_PathPos.TabIndex = 46;
            // 
            // radLabel28
            // 
            this.radLabel28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel28.Location = new System.Drawing.Point(10, 164);
            this.radLabel28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(109, 19);
            this.radLabel28.TabIndex = 47;
            this.radLabel28.Text = "DatabasePathPos";
            // 
            // radLabel21
            // 
            this.radLabel21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel21.Location = new System.Drawing.Point(98, 217);
            this.radLabel21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(63, 19);
            this.radLabel21.TabIndex = 45;
            this.radLabel21.Text = "Password";
            // 
            // radTextBox_WH
            // 
            this.radTextBox_WH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_WH.Location = new System.Drawing.Point(10, 286);
            this.radTextBox_WH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_WH.Name = "radTextBox_WH";
            this.radTextBox_WH.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_WH.TabIndex = 5;
            this.radTextBox_WH.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_SmartShop
            // 
            this.radTextBox_SmartShop.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_SmartShop.Location = new System.Drawing.Point(10, 422);
            this.radTextBox_SmartShop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_SmartShop.Name = "radTextBox_SmartShop";
            this.radTextBox_SmartShop.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_SmartShop.TabIndex = 8;
            this.radTextBox_SmartShop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_SmartShop.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_SmartShop_KeyPress);
            // 
            // radTextBox_FarmHouse
            // 
            this.radTextBox_FarmHouse.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_FarmHouse.Location = new System.Drawing.Point(10, 516);
            this.radTextBox_FarmHouse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_FarmHouse.Name = "radTextBox_FarmHouse";
            this.radTextBox_FarmHouse.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_FarmHouse.TabIndex = 10;
            this.radTextBox_FarmHouse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel16.Location = new System.Drawing.Point(13, 493);
            this.radLabel16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(118, 19);
            this.radLabel16.TabIndex = 42;
            this.radLabel16.Text = "Import FarmHouse";
            // 
            // radTextBox_SendMoney
            // 
            this.radTextBox_SendMoney.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_SendMoney.Location = new System.Drawing.Point(10, 466);
            this.radTextBox_SendMoney.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_SendMoney.Name = "radTextBox_SendMoney";
            this.radTextBox_SendMoney.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_SendMoney.TabIndex = 9;
            this.radTextBox_SendMoney.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_SendMoney.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_SendMoney_KeyPress);
            // 
            // radLabel17
            // 
            this.radLabel17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel17.Location = new System.Drawing.Point(10, 446);
            this.radLabel17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(145, 19);
            this.radLabel17.TabIndex = 43;
            this.radLabel17.Text = "ยอดส่งเงินกลับสาขาใหญ่";
            // 
            // radTextBox_TaxLabel
            // 
            this.radTextBox_TaxLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_TaxLabel.Location = new System.Drawing.Point(10, 376);
            this.radTextBox_TaxLabel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_TaxLabel.Name = "radTextBox_TaxLabel";
            this.radTextBox_TaxLabel.Size = new System.Drawing.Size(175, 19);
            this.radTextBox_TaxLabel.TabIndex = 7;
            this.radTextBox_TaxLabel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_TaxNumber
            // 
            this.radTextBox_TaxNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_TaxNumber.Location = new System.Drawing.Point(10, 331);
            this.radTextBox_TaxNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_TaxNumber.Name = "radTextBox_TaxNumber";
            this.radTextBox_TaxNumber.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_TaxNumber.TabIndex = 6;
            this.radTextBox_TaxNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel18
            // 
            this.radLabel18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel18.Location = new System.Drawing.Point(10, 311);
            this.radLabel18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(104, 19);
            this.radLabel18.TabIndex = 40;
            this.radLabel18.Text = "หมายเลขเสียภาษี";
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel19.Location = new System.Drawing.Point(10, 355);
            this.radLabel19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(83, 19);
            this.radLabel19.TabIndex = 41;
            this.radLabel19.Text = "ลำดับเสียภาษี";
            // 
            // radLabel20
            // 
            this.radLabel20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel20.Location = new System.Drawing.Point(10, 400);
            this.radLabel20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(149, 19);
            this.radLabel20.TabIndex = 44;
            this.radLabel20.Text = "ยอดวงเงินของระบบเครดิต";
            // 
            // radTextBox_DatabaseName
            // 
            this.radTextBox_DatabaseName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_DatabaseName.Location = new System.Drawing.Point(10, 138);
            this.radTextBox_DatabaseName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_DatabaseName.Name = "radTextBox_DatabaseName";
            this.radTextBox_DatabaseName.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_DatabaseName.TabIndex = 2;
            this.radTextBox_DatabaseName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Password
            // 
            this.radTextBox_Password.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Password.Location = new System.Drawing.Point(99, 237);
            this.radTextBox_Password.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Password.Name = "radTextBox_Password";
            this.radTextBox_Password.Size = new System.Drawing.Size(85, 21);
            this.radTextBox_Password.TabIndex = 4;
            this.radTextBox_Password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(13, 264);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(84, 19);
            this.radLabel12.TabIndex = 17;
            this.radLabel12.Text = "รหัสคลังสินค้า";
            // 
            // radTextBox_UserName
            // 
            this.radTextBox_UserName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_UserName.Location = new System.Drawing.Point(10, 237);
            this.radTextBox_UserName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_UserName.Name = "radTextBox_UserName";
            this.radTextBox_UserName.Size = new System.Drawing.Size(85, 21);
            this.radTextBox_UserName.TabIndex = 3;
            this.radTextBox_UserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(10, 217);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(68, 19);
            this.radLabel6.TabIndex = 33;
            this.radLabel6.Text = "UserName";
            // 
            // radTextBox_ServerIp
            // 
            this.radTextBox_ServerIp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_ServerIp.Location = new System.Drawing.Point(10, 87);
            this.radTextBox_ServerIp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_ServerIp.Name = "radTextBox_ServerIp";
            this.radTextBox_ServerIp.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_ServerIp.TabIndex = 1;
            this.radTextBox_ServerIp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Serverspc
            // 
            this.radTextBox_Serverspc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Serverspc.Location = new System.Drawing.Point(10, 42);
            this.radTextBox_Serverspc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Serverspc.Name = "radTextBox_Serverspc";
            this.radTextBox_Serverspc.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Serverspc.TabIndex = 0;
            this.radTextBox_Serverspc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(10, 22);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(79, 19);
            this.radLabel8.TabIndex = 5;
            this.radLabel8.Text = "ServerName";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(10, 66);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(57, 19);
            this.radLabel5.TabIndex = 6;
            this.radLabel5.Text = "ServerIP";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(10, 113);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(96, 19);
            this.radLabel3.TabIndex = 34;
            this.radLabel3.Text = "DatabaseName";
            // 
            // RadGroupBox_Center
            // 
            this.RadGroupBox_Center.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox_Center.Controls.Add(this.radLabel33);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_AddOnPer);
            this.RadGroupBox_Center.Controls.Add(this.radDropDownList_ec);
            this.RadGroupBox_Center.Controls.Add(this.radLabel_ec);
            this.RadGroupBox_Center.Controls.Add(this.radLabel_ComLocation);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_ComLocation);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_ComOT);
            this.RadGroupBox_Center.Controls.Add(this.radLabel_ComOT);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_Tel);
            this.RadGroupBox_Center.Controls.Add(this.radLabel30);
            this.RadGroupBox_Center.Controls.Add(this.radCheckBox_FreeItem);
            this.RadGroupBox_Center.Controls.Add(this.radCheckBox_StatusShelf);
            this.RadGroupBox_Center.Controls.Add(this.radLabel_ManagerName);
            this.RadGroupBox_Center.Controls.Add(this.radMultiColumnComboBox_Size);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_Min);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_Max);
            this.RadGroupBox_Center.Controls.Add(this.radLabel4);
            this.RadGroupBox_Center.Controls.Add(this.radLabel7);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_Head);
            this.RadGroupBox_Center.Controls.Add(this.radLabel_addr);
            this.RadGroupBox_Center.Controls.Add(this.radTextBox_Address);
            this.RadGroupBox_Center.Controls.Add(this.radLabel_size);
            this.RadGroupBox_Center.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGroupBox_Center.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.RadGroupBox_Center.HeaderText = "ตั้งค่าสาขา";
            this.RadGroupBox_Center.Location = new System.Drawing.Point(426, 12);
            this.RadGroupBox_Center.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadGroupBox_Center.Name = "RadGroupBox_Center";
            this.RadGroupBox_Center.Padding = new System.Windows.Forms.Padding(2, 22, 2, 2);
            this.RadGroupBox_Center.Size = new System.Drawing.Size(200, 583);
            this.RadGroupBox_Center.TabIndex = 4;
            this.RadGroupBox_Center.Text = "ตั้งค่าสาขา";
            // 
            // radDropDownList_ec
            // 
            this.radDropDownList_ec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_ec.DropDownAnimationEnabled = false;
            this.radDropDownList_ec.DropDownHeight = 124;
            this.radDropDownList_ec.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_ec.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_ec.Location = new System.Drawing.Point(10, 478);
            this.radDropDownList_ec.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_ec.Name = "radDropDownList_ec";
            this.radDropDownList_ec.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_ec.TabIndex = 51;
            // 
            // radLabel_ec
            // 
            this.radLabel_ec.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_ec.Location = new System.Drawing.Point(12, 459);
            this.radLabel_ec.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_ec.Name = "radLabel_ec";
            this.radLabel_ec.Size = new System.Drawing.Size(82, 19);
            this.radLabel_ec.TabIndex = 50;
            this.radLabel_ec.Text = "สายส่งเหรียญ";
            // 
            // radLabel_ComLocation
            // 
            this.radLabel_ComLocation.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_ComLocation.Location = new System.Drawing.Point(10, 360);
            this.radLabel_ComLocation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_ComLocation.Name = "radLabel_ComLocation";
            this.radLabel_ComLocation.Size = new System.Drawing.Size(106, 19);
            this.radLabel_ComLocation.TabIndex = 49;
            this.radLabel_ComLocation.Text = "ค่าที่พัก+เบี้ยเลี้ยง";
            // 
            // radTextBox_ComLocation
            // 
            this.radTextBox_ComLocation.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_ComLocation.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ComLocation.Location = new System.Drawing.Point(10, 380);
            this.radTextBox_ComLocation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_ComLocation.Name = "radTextBox_ComLocation";
            this.radTextBox_ComLocation.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_ComLocation.TabIndex = 48;
            this.radTextBox_ComLocation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_ComLocation_KeyPress);
            // 
            // radTextBox_ComOT
            // 
            this.radTextBox_ComOT.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_ComOT.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ComOT.Location = new System.Drawing.Point(10, 334);
            this.radTextBox_ComOT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_ComOT.Name = "radTextBox_ComOT";
            this.radTextBox_ComOT.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_ComOT.TabIndex = 47;
            this.radTextBox_ComOT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_ComOT_KeyPress);
            // 
            // radLabel_ComOT
            // 
            this.radLabel_ComOT.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_ComOT.Location = new System.Drawing.Point(10, 312);
            this.radLabel_ComOT.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_ComOT.Name = "radLabel_ComOT";
            this.radLabel_ComOT.Size = new System.Drawing.Size(144, 19);
            this.radLabel_ComOT.TabIndex = 46;
            this.radLabel_ComOT.Text = "ค่าคอม พนง. OT วันหยุด";
            // 
            // radTextBox_Tel
            // 
            this.radTextBox_Tel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_Tel.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Tel.Location = new System.Drawing.Point(10, 286);
            this.radTextBox_Tel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Tel.MaxLength = 10;
            this.radTextBox_Tel.Name = "radTextBox_Tel";
            this.radTextBox_Tel.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Tel.TabIndex = 45;
            // 
            // radLabel30
            // 
            this.radLabel30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel30.Location = new System.Drawing.Point(8, 264);
            this.radLabel30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(119, 19);
            this.radLabel30.TabIndex = 44;
            this.radLabel30.Text = "เบอร์โทรประจำสาขา";
            // 
            // radCheckBox_FreeItem
            // 
            this.radCheckBox_FreeItem.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radCheckBox_FreeItem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_FreeItem.Location = new System.Drawing.Point(9, 555);
            this.radCheckBox_FreeItem.Name = "radCheckBox_FreeItem";
            this.radCheckBox_FreeItem.Size = new System.Drawing.Size(128, 18);
            this.radCheckBox_FreeItem.TabIndex = 43;
            this.radCheckBox_FreeItem.Text = "แจกของแถมลูกค้าได้";
            this.radCheckBox_FreeItem.CheckStateChanged += new System.EventHandler(this.RadCheckBox_FreeItem_CheckStateChanged);
            // 
            // radCheckBox_StatusShelf
            // 
            this.radCheckBox_StatusShelf.CheckAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radCheckBox_StatusShelf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_StatusShelf.Location = new System.Drawing.Point(9, 530);
            this.radCheckBox_StatusShelf.Name = "radCheckBox_StatusShelf";
            this.radCheckBox_StatusShelf.Size = new System.Drawing.Size(61, 18);
            this.radCheckBox_StatusShelf.TabIndex = 42;
            this.radCheckBox_StatusShelf.Text = "มีชั้นวาง";
            this.radCheckBox_StatusShelf.CheckStateChanged += new System.EventHandler(this.RadCheckBox_StatusShelf_CheckStateChanged);
            // 
            // radLabel_ManagerName
            // 
            this.radLabel_ManagerName.AutoSize = false;
            this.radLabel_ManagerName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_ManagerName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_ManagerName.Location = new System.Drawing.Point(9, 164);
            this.radLabel_ManagerName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_ManagerName.Name = "radLabel_ManagerName";
            this.radLabel_ManagerName.Size = new System.Drawing.Size(175, 19);
            this.radLabel_ManagerName.TabIndex = 41;
            this.radLabel_ManagerName.Text = "ชื่อ ผจก";
            // 
            // radMultiColumnComboBox_Size
            // 
            this.radMultiColumnComboBox_Size.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radMultiColumnComboBox_Size.NestedRadGridView
            // 
            this.radMultiColumnComboBox_Size.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.radMultiColumnComboBox_Size.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radMultiColumnComboBox_Size.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radMultiColumnComboBox_Size.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate.EnableGrouping = false;
            this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radMultiColumnComboBox_Size.EditorControl.Name = "NestedRadGridView";
            this.radMultiColumnComboBox_Size.EditorControl.ReadOnly = true;
            this.radMultiColumnComboBox_Size.EditorControl.ShowGroupPanel = false;
            this.radMultiColumnComboBox_Size.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.radMultiColumnComboBox_Size.EditorControl.TabIndex = 0;
            this.radMultiColumnComboBox_Size.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radMultiColumnComboBox_Size.Location = new System.Drawing.Point(56, 188);
            this.radMultiColumnComboBox_Size.Name = "radMultiColumnComboBox_Size";
            this.radMultiColumnComboBox_Size.Size = new System.Drawing.Size(128, 21);
            this.radMultiColumnComboBox_Size.TabIndex = 2;
            this.radMultiColumnComboBox_Size.TabStop = false;
            this.radMultiColumnComboBox_Size.SelectedIndexChanged += new System.EventHandler(this.RadMultiColumnComboBox_Size_SelectedIndexChanged);
            this.radMultiColumnComboBox_Size.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Min
            // 
            this.radTextBox_Min.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Min.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Min.Location = new System.Drawing.Point(105, 235);
            this.radTextBox_Min.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Min.Name = "radTextBox_Min";
            this.radTextBox_Min.Size = new System.Drawing.Size(80, 21);
            this.radTextBox_Min.TabIndex = 4;
            this.radTextBox_Min.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_Min.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Min_KeyPress);
            // 
            // radTextBox_Max
            // 
            this.radTextBox_Max.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Max.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Max.Location = new System.Drawing.Point(9, 235);
            this.radTextBox_Max.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Max.Name = "radTextBox_Max";
            this.radTextBox_Max.Size = new System.Drawing.Size(80, 21);
            this.radTextBox_Max.TabIndex = 3;
            this.radTextBox_Max.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_Max.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Max_KeyPress);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(9, 213);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(90, 19);
            this.radLabel4.TabIndex = 38;
            this.radLabel4.Text = "มาตรฐาน พนง.";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(12, 113);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(124, 19);
            this.radLabel7.TabIndex = 36;
            this.radLabel7.Text = "ผู้จัดการ [รหัส Enter]";
            // 
            // radTextBox_Head
            // 
            this.radTextBox_Head.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Head.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Head.Location = new System.Drawing.Point(9, 134);
            this.radTextBox_Head.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Head.MaxLength = 7;
            this.radTextBox_Head.Name = "radTextBox_Head";
            this.radTextBox_Head.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Head.TabIndex = 1;
            this.radTextBox_Head.Text = "0604049";
            this.radTextBox_Head.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Head_KeyDown);
            this.radTextBox_Head.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Head_KeyPress);
            this.radTextBox_Head.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Head_KeyUp);
            // 
            // radLabel_addr
            // 
            this.radLabel_addr.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_addr.Location = new System.Drawing.Point(11, 25);
            this.radLabel_addr.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_addr.Name = "radLabel_addr";
            this.radLabel_addr.Size = new System.Drawing.Size(32, 19);
            this.radLabel_addr.TabIndex = 0;
            this.radLabel_addr.Text = "ที่อยู่";
            // 
            // radTextBox_Address
            // 
            this.radTextBox_Address.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Address.Location = new System.Drawing.Point(9, 45);
            this.radTextBox_Address.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Address.Multiline = true;
            this.radTextBox_Address.Name = "radTextBox_Address";
            // 
            // 
            // 
            this.radTextBox_Address.RootElement.StretchVertically = true;
            this.radTextBox_Address.Size = new System.Drawing.Size(175, 65);
            this.radTextBox_Address.TabIndex = 0;
            this.radTextBox_Address.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel_size
            // 
            this.radLabel_size.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_size.Location = new System.Drawing.Point(9, 190);
            this.radLabel_size.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_size.Name = "radLabel_size";
            this.radLabel_size.Size = new System.Drawing.Size(38, 19);
            this.radLabel_size.TabIndex = 6;
            this.radLabel_size.Text = "ขนาด";
            // 
            // RadGroupBox_Internet
            // 
            this.RadGroupBox_Internet.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox_Internet.Controls.Add(this.RadGridView_Show);
            this.RadGroupBox_Internet.Controls.Add(this.radTextBox_remarks);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel25);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel14);
            this.RadGroupBox_Internet.Controls.Add(this.radTextBox_ServiceCall);
            this.RadGroupBox_Internet.Controls.Add(this.radTextBox_Serviceid);
            this.RadGroupBox_Internet.Controls.Add(this.radTextBox_Androidversion);
            this.RadGroupBox_Internet.Controls.Add(this.radButton_Upload);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel9);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel26);
            this.RadGroupBox_Internet.Controls.Add(this.radTextBox_Service);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel10);
            this.RadGroupBox_Internet.Controls.Add(this.radTextBox_PDAversion);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel11);
            this.RadGroupBox_Internet.Controls.Add(this.radLabel27);
            this.RadGroupBox_Internet.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadGroupBox_Internet.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.RadGroupBox_Internet.HeaderText = "Internet";
            this.RadGroupBox_Internet.Location = new System.Drawing.Point(632, 13);
            this.RadGroupBox_Internet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadGroupBox_Internet.Name = "RadGroupBox_Internet";
            this.RadGroupBox_Internet.Padding = new System.Windows.Forms.Padding(2, 22, 2, 2);
            this.RadGroupBox_Internet.Size = new System.Drawing.Size(200, 583);
            this.RadGroupBox_Internet.TabIndex = 3;
            this.RadGroupBox_Internet.Text = "Internet";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(9, 281);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.RadGridView_Show.MasterTemplate.AllowDragToGroup = false;
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(175, 199);
            this.RadGridView_Show.TabIndex = 22;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radTextBox_remarks
            // 
            this.radTextBox_remarks.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_remarks.Location = new System.Drawing.Point(9, 177);
            this.radTextBox_remarks.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_remarks.Multiline = true;
            this.radTextBox_remarks.Name = "radTextBox_remarks";
            // 
            // 
            // 
            this.radTextBox_remarks.RootElement.StretchVertically = true;
            this.radTextBox_remarks.Size = new System.Drawing.Size(175, 68);
            this.radTextBox_remarks.TabIndex = 3;
            this.radTextBox_remarks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel25
            // 
            this.radLabel25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel25.Location = new System.Drawing.Point(9, 253);
            this.radLabel25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(70, 19);
            this.radLabel25.TabIndex = 19;
            this.radLabel25.Text = "แปลนกล้อง";
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel14.Location = new System.Drawing.Point(12, 157);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(60, 19);
            this.radLabel14.TabIndex = 19;
            this.radLabel14.Text = "หมายเหตุ";
            // 
            // radTextBox_ServiceCall
            // 
            this.radTextBox_ServiceCall.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_ServiceCall.Location = new System.Drawing.Point(9, 131);
            this.radTextBox_ServiceCall.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_ServiceCall.Name = "radTextBox_ServiceCall";
            this.radTextBox_ServiceCall.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_ServiceCall.TabIndex = 2;
            this.radTextBox_ServiceCall.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Serviceid
            // 
            this.radTextBox_Serviceid.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Serviceid.Location = new System.Drawing.Point(9, 85);
            this.radTextBox_Serviceid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Serviceid.Name = "radTextBox_Serviceid";
            this.radTextBox_Serviceid.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Serviceid.TabIndex = 1;
            this.radTextBox_Serviceid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Androidversion
            // 
            this.radTextBox_Androidversion.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_Androidversion.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Androidversion.Location = new System.Drawing.Point(9, 556);
            this.radTextBox_Androidversion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Androidversion.Name = "radTextBox_Androidversion";
            this.radTextBox_Androidversion.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Androidversion.TabIndex = 6;
            this.radTextBox_Androidversion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radButton_Upload
            // 
            this.radButton_Upload.Location = new System.Drawing.Point(109, 253);
            this.radButton_Upload.Name = "radButton_Upload";
            this.radButton_Upload.Size = new System.Drawing.Size(75, 22);
            this.radButton_Upload.TabIndex = 21;
            this.radButton_Upload.Text = "Upload file";
            this.radButton_Upload.Click += new System.EventHandler(this.RadButton_Upload_Click);
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel9.Location = new System.Drawing.Point(12, 108);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(49, 19);
            this.radLabel9.TabIndex = 4;
            this.radLabel9.Text = "แจ้งเสีย";
            // 
            // radLabel26
            // 
            this.radLabel26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel26.Location = new System.Drawing.Point(10, 534);
            this.radLabel26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(146, 19);
            this.radLabel26.TabIndex = 21;
            this.radLabel26.Text = "Version Update Android";
            // 
            // radTextBox_Service
            // 
            this.radTextBox_Service.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Service.Location = new System.Drawing.Point(9, 40);
            this.radTextBox_Service.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Service.Name = "radTextBox_Service";
            this.radTextBox_Service.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Service.TabIndex = 0;
            this.radTextBox_Service.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(9, 64);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(62, 19);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "เลขบริการ";
            // 
            // radTextBox_PDAversion
            // 
            this.radTextBox_PDAversion.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_PDAversion.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_PDAversion.Location = new System.Drawing.Point(9, 509);
            this.radTextBox_PDAversion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_PDAversion.Name = "radTextBox_PDAversion";
            this.radTextBox_PDAversion.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_PDAversion.TabIndex = 5;
            this.radTextBox_PDAversion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel11.Location = new System.Drawing.Point(9, 20);
            this.radLabel11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 19);
            this.radLabel11.TabIndex = 0;
            this.radLabel11.Text = "ผู้ให้บริการ";
            // 
            // radLabel27
            // 
            this.radLabel27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel27.Location = new System.Drawing.Point(9, 484);
            this.radLabel27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(130, 19);
            this.radLabel27.TabIndex = 20;
            this.radLabel27.Text = "Version Update PDA ";
            // 
            // RadGroupBox_Gernaral
            // 
            this.RadGroupBox_Gernaral.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_VenderLeo);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel32);
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_Beer);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel31);
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_City);
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_Out);
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_Staopen);
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_Status);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel2);
            this.RadGroupBox_Gernaral.Controls.Add(this.radTextBox_Longtitude);
            this.RadGroupBox_Gernaral.Controls.Add(this.radDropDownList_Price);
            this.RadGroupBox_Gernaral.Controls.Add(this.radTextBox_BranchName);
            this.RadGroupBox_Gernaral.Controls.Add(this.radTextBox_Latitude);
            this.RadGroupBox_Gernaral.Controls.Add(this.radTextBox_Branch);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel1);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel_level);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel13);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel15);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel22);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel23);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel24);
            this.RadGroupBox_Gernaral.Controls.Add(this.radLabel29);
            this.RadGroupBox_Gernaral.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadGroupBox_Gernaral.HeaderMargin = new System.Windows.Forms.Padding(1);
            this.RadGroupBox_Gernaral.HeaderText = "ทั่วไป";
            this.RadGroupBox_Gernaral.Location = new System.Drawing.Point(218, 14);
            this.RadGroupBox_Gernaral.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadGroupBox_Gernaral.Name = "RadGroupBox_Gernaral";
            this.RadGroupBox_Gernaral.Padding = new System.Windows.Forms.Padding(2, 22, 2, 2);
            this.RadGroupBox_Gernaral.Size = new System.Drawing.Size(200, 583);
            this.RadGroupBox_Gernaral.TabIndex = 2;
            this.RadGroupBox_Gernaral.Text = "ทั่วไป";
            // 
            // radDropDownList_VenderLeo
            // 
            this.radDropDownList_VenderLeo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_VenderLeo.DropDownAnimationEnabled = false;
            this.radDropDownList_VenderLeo.DropDownHeight = 124;
            this.radDropDownList_VenderLeo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_VenderLeo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_VenderLeo.Location = new System.Drawing.Point(12, 526);
            this.radDropDownList_VenderLeo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_VenderLeo.Name = "radDropDownList_VenderLeo";
            this.radDropDownList_VenderLeo.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_VenderLeo.TabIndex = 23;
            this.radDropDownList_VenderLeo.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_VenderLeo_SelectedValueChanged);
            // 
            // radLabel32
            // 
            this.radLabel32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel32.Location = new System.Drawing.Point(13, 505);
            this.radLabel32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(116, 19);
            this.radLabel32.TabIndex = 22;
            this.radLabel32.Text = "ผู้จำหน่าย สิงห์-ลีโอ";
            // 
            // radDropDownList_Beer
            // 
            this.radDropDownList_Beer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Beer.DropDownAnimationEnabled = false;
            this.radDropDownList_Beer.DropDownHeight = 124;
            this.radDropDownList_Beer.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Beer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Beer.Location = new System.Drawing.Point(12, 475);
            this.radDropDownList_Beer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Beer.Name = "radDropDownList_Beer";
            this.radDropDownList_Beer.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Beer.TabIndex = 21;
            this.radDropDownList_Beer.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Beer_SelectedValueChanged);
            // 
            // radLabel31
            // 
            this.radLabel31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel31.Location = new System.Drawing.Point(13, 454);
            this.radLabel31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(125, 19);
            this.radLabel31.TabIndex = 20;
            this.radLabel31.Text = "สายส่งเบียร์ช้าง-เหล้า";
            // 
            // radDropDownList_City
            // 
            this.radDropDownList_City.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_City.DropDownAnimationEnabled = false;
            this.radDropDownList_City.DropDownHeight = 124;
            this.radDropDownList_City.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_City.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_City.Location = new System.Drawing.Point(12, 426);
            this.radDropDownList_City.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_City.Name = "radDropDownList_City";
            this.radDropDownList_City.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_City.TabIndex = 18;
            // 
            // radDropDownList_Out
            // 
            this.radDropDownList_Out.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Out.DropDownAnimationEnabled = false;
            this.radDropDownList_Out.DropDownHeight = 124;
            this.radDropDownList_Out.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Out.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Out.Location = new System.Drawing.Point(13, 380);
            this.radDropDownList_Out.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Out.Name = "radDropDownList_Out";
            this.radDropDownList_Out.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Out.TabIndex = 16;
            this.radDropDownList_Out.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radDropDownList_Staopen
            // 
            this.radDropDownList_Staopen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Staopen.DropDownAnimationEnabled = false;
            this.radDropDownList_Staopen.DropDownHeight = 124;
            this.radDropDownList_Staopen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Staopen.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Staopen.Location = new System.Drawing.Point(12, 331);
            this.radDropDownList_Staopen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Staopen.Name = "radDropDownList_Staopen";
            this.radDropDownList_Staopen.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Staopen.TabIndex = 14;
            this.radDropDownList_Staopen.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radDropDownList_Status
            // 
            this.radDropDownList_Status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Status.DropDownAnimationEnabled = false;
            this.radDropDownList_Status.DropDownHeight = 124;
            this.radDropDownList_Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Status.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Status.Location = new System.Drawing.Point(12, 283);
            this.radDropDownList_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Status.Name = "radDropDownList_Status";
            this.radDropDownList_Status.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Status.TabIndex = 12;
            this.radDropDownList_Status.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(13, 22);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(59, 19);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "รหัสสาขา";
            // 
            // radTextBox_Longtitude
            // 
            this.radTextBox_Longtitude.Location = new System.Drawing.Point(13, 234);
            this.radTextBox_Longtitude.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Longtitude.Name = "radTextBox_Longtitude";
            this.radTextBox_Longtitude.Size = new System.Drawing.Size(175, 20);
            this.radTextBox_Longtitude.TabIndex = 4;
            this.radTextBox_Longtitude.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radDropDownList_Price
            // 
            this.radDropDownList_Price.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Price.DropDownAnimationEnabled = false;
            this.radDropDownList_Price.DropDownHeight = 124;
            this.radDropDownList_Price.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Price.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Price.Location = new System.Drawing.Point(13, 138);
            this.radDropDownList_Price.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Price.Name = "radDropDownList_Price";
            this.radDropDownList_Price.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Price.TabIndex = 2;
            this.radDropDownList_Price.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_BranchName
            // 
            this.radTextBox_BranchName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_BranchName.Location = new System.Drawing.Point(13, 89);
            this.radTextBox_BranchName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_BranchName.Name = "radTextBox_BranchName";
            this.radTextBox_BranchName.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_BranchName.TabIndex = 1;
            this.radTextBox_BranchName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Latitude
            // 
            this.radTextBox_Latitude.Location = new System.Drawing.Point(13, 188);
            this.radTextBox_Latitude.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Latitude.Name = "radTextBox_Latitude";
            this.radTextBox_Latitude.Size = new System.Drawing.Size(175, 20);
            this.radTextBox_Latitude.TabIndex = 3;
            this.radTextBox_Latitude.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Branch
            // 
            this.radTextBox_Branch.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTextBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Branch.Location = new System.Drawing.Point(13, 42);
            this.radTextBox_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Branch.MaxLength = 5;
            this.radTextBox_Branch.Name = "radTextBox_Branch";
            this.radTextBox_Branch.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Branch.TabIndex = 0;
            this.radTextBox_Branch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(13, 68);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(53, 19);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "ชื่อสาขา";
            // 
            // radLabel_level
            // 
            this.radLabel_level.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_level.Location = new System.Drawing.Point(13, 116);
            this.radLabel_level.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_level.Name = "radLabel_level";
            this.radLabel_level.Size = new System.Drawing.Size(62, 19);
            this.radLabel_level.TabIndex = 4;
            this.radLabel_level.Text = "ระดับราคา";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.Location = new System.Drawing.Point(13, 167);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(45, 19);
            this.radLabel13.TabIndex = 8;
            this.radLabel13.Text = "ละติจูด";
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.Location = new System.Drawing.Point(12, 213);
            this.radLabel15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(54, 19);
            this.radLabel15.TabIndex = 10;
            this.radLabel15.Text = "ลองติจูด";
            // 
            // radLabel22
            // 
            this.radLabel22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel22.Location = new System.Drawing.Point(13, 261);
            this.radLabel22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(49, 19);
            this.radLabel22.TabIndex = 13;
            this.radLabel22.Text = "ประเภท";
            // 
            // radLabel23
            // 
            this.radLabel23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel23.Location = new System.Drawing.Point(12, 311);
            this.radLabel23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(43, 19);
            this.radLabel23.TabIndex = 15;
            this.radLabel23.Text = "สถานะ";
            // 
            // radLabel24
            // 
            this.radLabel24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel24.Location = new System.Drawing.Point(12, 359);
            this.radLabel24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(123, 19);
            this.radLabel24.TabIndex = 17;
            this.radLabel24.Text = "ในจังหวัด/ต่างจังหวัด";
            // 
            // radLabel29
            // 
            this.radLabel29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel29.Location = new System.Drawing.Point(13, 406);
            this.radLabel29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(45, 19);
            this.radLabel29.TabIndex = 19;
            this.radLabel29.Text = "จังหวัด";
            // 
            // radLabel_SizeName
            // 
            this.radLabel_SizeName.AutoSize = false;
            this.radLabel_SizeName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_SizeName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_SizeName.Location = new System.Drawing.Point(548, 617);
            this.radLabel_SizeName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_SizeName.Name = "radLabel_SizeName";
            this.radLabel_SizeName.Size = new System.Drawing.Size(172, 19);
            this.radLabel_SizeName.TabIndex = 39;
            this.radLabel_SizeName.Text = "ขนาด";
            this.radLabel_SizeName.Visible = false;
            // 
            // radLabel33
            // 
            this.radLabel33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel33.Location = new System.Drawing.Point(13, 407);
            this.radLabel33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel33.Name = "radLabel33";
            this.radLabel33.Size = new System.Drawing.Size(114, 19);
            this.radLabel33.TabIndex = 53;
            this.radLabel33.Text = "ค่ากันดาร/คน/สาขา";
            // 
            // radTextBox_AddOnPer
            // 
            this.radTextBox_AddOnPer.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_AddOnPer.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_AddOnPer.Location = new System.Drawing.Point(13, 427);
            this.radTextBox_AddOnPer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_AddOnPer.Name = "radTextBox_AddOnPer";
            this.radTextBox_AddOnPer.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_AddOnPer.TabIndex = 52;
            this.radTextBox_AddOnPer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_AddOnPer_KeyPress);
            // 
            // Branch_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 652);
            this.Controls.Add(this.radPanel_Branch);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Branch_Detail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายละเอียดสาขา";
            this.Load += new System.EventHandler(this.Branch_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Branch)).EndInit();
            this.radPanel_Branch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_cancle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Programmer)).EndInit();
            this.RadGroupBox_Programmer.ResumeLayout(false);
            this.RadGroupBox_Programmer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PathPos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_WH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SmartShop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_FarmHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SendMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_TaxLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_TaxNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_DatabaseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Password)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_UserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ServerIp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Serverspc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Center)).EndInit();
            this.RadGroupBox_Center.ResumeLayout(false);
            this.RadGroupBox_Center.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_ec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ComLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ComLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ComOT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ComOT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_FreeItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_StatusShelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ManagerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox_Size.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox_Size.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox_Size)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Head)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_addr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Address)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_size)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Internet)).EndInit();
            this.RadGroupBox_Internet.ResumeLayout(false);
            this.RadGroupBox_Internet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_remarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ServiceCall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Serviceid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Androidversion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Upload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Service)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PDAversion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox_Gernaral)).EndInit();
            this.RadGroupBox_Gernaral.ResumeLayout(false);
            this.RadGroupBox_Gernaral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_VenderLeo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Beer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_City)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Out)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Staopen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Longtitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Latitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_level)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SizeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_AddOnPer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel_Branch;
        private Telerik.WinControls.UI.RadGroupBox RadGroupBox_Gernaral;
        private Telerik.WinControls.UI.RadLabel radLabel_size;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Address;
        private Telerik.WinControls.UI.RadLabel radLabel_level;
        private Telerik.WinControls.UI.RadLabel radLabel_addr;
        private Telerik.WinControls.UI.RadGroupBox RadGroupBox_Programmer;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ServerIp;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadGroupBox RadGroupBox_Internet;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Password;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_UserName;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Head;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox_DatabaseName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_remarks;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Price;
        private Telerik.WinControls.UI.RadGroupBox RadGroupBox_Center;
        public Telerik.WinControls.UI.RadTextBox radTextBox_Serverspc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Longtitude;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Latitude;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel_SizeName;
        private Telerik.WinControls.UI.RadMultiColumnComboBox radMultiColumnComboBox_Size;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SmartShop;
        private Telerik.WinControls.UI.RadTextBox radTextBox_FarmHouse;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SendMoney;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadTextBox radTextBox_TaxLabel;
        public Telerik.WinControls.UI.RadTextBox radTextBox_TaxNumber;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel_ManagerName;
        protected Telerik.WinControls.UI.RadButton radButton_save;
        protected Telerik.WinControls.UI.RadButton radButton_cancle;
        private Telerik.WinControls.UI.RadTextBox radTextBox_WH;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Status;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Staopen;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ServiceCall;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Serviceid;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Service;
        private Telerik.WinControls.UI.RadTextBox radTextBox_BranchName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Branch;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Out;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadButton radButton_Upload;
        private Telerik.WinControls.UI.RadOpenFileDialog radOpenFileDialog1;
        private Telerik.WinControls.UI.RadOpenFolderDialog radOpenFolderDialog1;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Androidversion;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadTextBox radTextBox_PDAversion;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_StatusShelf;
        private Telerik.WinControls.UI.RadTextBox radTextBox_PathPos;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_City;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_FreeItem;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Beer;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_VenderLeo;
        private Telerik.WinControls.UI.RadLabel radLabel_ComLocation;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ComLocation;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ComOT;
        private Telerik.WinControls.UI.RadLabel radLabel_ComOT;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Tel;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Min;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Max;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_ec;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_ec;
        private Telerik.WinControls.UI.RadLabel radLabel33;
        private Telerik.WinControls.UI.RadTextBox radTextBox_AddOnPer;
    }
}