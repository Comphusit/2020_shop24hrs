﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using System.Diagnostics;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class Branch_Main : Form
    {
        DataTable dt = new DataTable();
        //private Data_BRANCH DataBranch;
        readonly string _pAdminstatus;
        public Branch_Main(string pAdminstatus) // 0 admin,1 center,2 ทั่วไป
        {
            InitializeComponent();
            _pAdminstatus = pAdminstatus;
            this.KeyPreview = true;
            this.SetButton();
            this.SetGridView();
            this.SetExpression();
        }
        //Event Keyboard
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        #region "SetGridRow"
        //ROWS NUMBERS
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        private void Branch_Main_Load(object sender, EventArgs e)
        {
            SetGDV();
            GetGridViewSummary();
            SetPermission();
        }
        void SetButton()
        {
            radButton_Add.ShowBorder = true;
            radButton_Delet.ShowBorder = true;
            radButton_Edit.ShowBorder = true;
            radButton_Excel.ShowBorder = true;
            radButton_Refresh.ShowBorder = true;
            radButton_Vnc.ShowBorder = true;
            radButton_Pda.ShowBorder = true;
        }
        void SetGridView()
        {
            radStatusStrip.SizingGrip = false;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_STATUSDESC", "ประเภทมินิมาร์ท", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_STAOPENDESC", "สถานะ", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LABEL", "ระดับราคา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_PROVINCENAME", "จังหวัด", 120));
            if (_pAdminstatus == "0")
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_SERVERNAME", "SERVER", 150));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_SERVICE", "ผู้ให้บริการ", 150));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_SERVICEID", "เลขที่บริการ", 220));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_SERVICETEL", "ติดต่อผู้ให้บริการ", 150));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_VERSIONUPDATEPDA", "SHOP24HRS.PDA", 120));
            }

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ManagerName", "ผู้จัดการ", 220));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ADDR", "ที่อยู่", 400));

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("SERVICEPDA", "SERVICEPDA"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("BRANCH_STAOPEN", "BRANCH_STAOPEN"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("BRANCH_STA", "BRANCH_STA"));

            radLabel_Detail.Text = "เลือกสาขา >> กด / เพื่อดดูรายละเอียดข้อมูลสาขาทั้งหมด";
            //Config เหล้า-เบียร์
            if ((SystemClass.SystemDptID == "D034") || (SystemClass.SystemComMinimart == "1"))
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddDropDown_AddManual("BRANCH_ROUTEBEER", "สายส่งเหล้า-เบียร์", 200,
             ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("49", "", "", "1"), "SHOW_ID", "SHOW_NAME"));

                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddDropDown_AddManual("BRANCH_VENDERBEER", "ผู้จำหน่าย สิงห์-ลีโอ", 200,
                    ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("2", " AND REMARK = 'D034' ", "", "1"), "SHOW_ID", "SHOW_NAME"));

                ////กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("BRANCH_ROUTEBEER", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_ROUTEBEER = '' ", false)
                //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                //this.radGridView_Show.Columns["BRANCH_ROUTEBEER"].ConditionalFormattingObjectList.Add(obj3);
                DatagridClass.SetCellBackClolorByExpression("BRANCH_ROUTEBEER", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_ROUTEBEER = '' ", ConfigClass.SetColor_DarkPurplePastel(), radGridView_Show);
                ////กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                //ExpressionFormattingObject obj4 = new ExpressionFormattingObject("BRANCH_VENDERBEER", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_VENDERBEER = '' ", false)
                //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                //this.radGridView_Show.Columns["BRANCH_VENDERBEER"].ConditionalFormattingObjectList.Add(obj4);
                DatagridClass.SetCellBackClolorByExpression("BRANCH_VENDERBEER", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_VENDERBEER = '' ", ConfigClass.SetColor_DarkPurplePastel(), radGridView_Show);

                radLabel_Detail.Text = "เลือกสาขา >> กด / เพื่อดดูรายละเอียดข้อมูลสาขาทั้งหมด | DoubleClick ช่องสายส่ง/ผู้จำหน่าย >> เพื่อตั้งค่าหรือแก้ไขข้อมูล";
            }
            //config FarmHouse
            if ((SystemClass.SystemDptID == "D027") || (SystemClass.SystemComMinimart == "1"))
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_V005450", "รหัสลูกค้า ฟาร์มเฮ้าส์", 200));
                ////กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                //ExpressionFormattingObject obj5 = new ExpressionFormattingObject("BRANCH_V005450", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_V005450 = '' ", false)
                //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                //this.radGridView_Show.Columns["BRANCH_V005450"].ConditionalFormattingObjectList.Add(obj5);
                DatagridClass.SetCellBackClolorByExpression("BRANCH_V005450", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_V005450 = '' ", ConfigClass.SetColor_DarkPurplePastel(), radGridView_Show);

                radLabel_Detail.Text = "เลือกสาขา >> กด / เพื่อดดูรายละเอียดข้อมูลสาขาทั้งหมด | DoubleClick ฟาร์มเฮ้าส์ >> เพื่อตั้งค่าหรือแก้ไขข้อมูล";
            }
            //config ไอติม
            if ((SystemClass.SystemDptID == "D014") || (SystemClass.SystemComMinimart == "1"))
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_V014483", "รหัสลูกค้า นวสร ดิสทรีบิวชั่น", 200));
                //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                //ExpressionFormattingObject obj5 = new ExpressionFormattingObject("BRANCH_V014483", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_V014483 = '' ", false)
                //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                //this.radGridView_Show.Columns["BRANCH_V014483"].ConditionalFormattingObjectList.Add(obj5);
                DatagridClass.SetCellBackClolorByExpression("BRANCH_V014483", "BRANCH_STA = '1' AND BRANCH_STAOPEN = '1' AND BRANCH_V014483 = '' ", ConfigClass.SetColor_DarkPurplePastel(), radGridView_Show);

                radLabel_Detail.Text = "เลือกสาขา >> กด / เพื่อดดูรายละเอียดข้อมูลสาขาทั้งหมด | DoubleClick นวสร >> เพื่อตั้งค่าหรือแก้ไขข้อมูล";
            }

            if (SystemClass.SystemComMinimart == "1")
                radLabel_Detail.Text = "เลือกสาขา >> กด / เพื่อดดูรายละเอียดข้อมูลสาขาทั้งหมด | DoubleClick ช่องสายส่ง/ผู้จำหน่าย/ฟาร์มเฮ้าส์/นวสร >> เพื่อตั้งค่าหรือแก้ไขข้อมูล";

            if ((_pAdminstatus == "0") || (_pAdminstatus == "1"))
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_CONFIGCOMMISSION_OT", "ค่าคอม OT/วัน", 100));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_CONFIGCOMMISSION_LOCATION", "ค่าเบี้ยเลี้ยง/เดือน", 120));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_RouteSendCoinName", "สายส่งเหรียญ", 90));
            }
            //Freeze Column
            this.radGridView_Show.MasterTemplate.Columns["BRANCH_ID"].IsPinned = true;
            this.radGridView_Show.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
        }
        void SetExpression()
        {
            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("BRANCH_STAOPENDESC", "BRANCH_STAOPENDESC = 'ปิดบริการ' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj1);
            this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj1);
            this.radGridView_Show.Columns["BRANCH_STAOPENDESC"].ConditionalFormattingObjectList.Add(obj1);

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("BRANCH_STAOPENDESC", "BRANCH_STAOPENDESC = 'กำลังก่อสร้าง' ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            this.radGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);
            this.radGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj2);
            this.radGridView_Show.Columns["BRANCH_STAOPENDESC"].ConditionalFormattingObjectList.Add(obj2);

        }
        void SetPermission()
        {
            switch (_pAdminstatus)
            {
                case "0"://admin
                    radButton_Add.Enabled = true;
                    radButton_Delet.Enabled = true;
                    radButton_Edit.Enabled = true;
                    radButton_Excel.Enabled = true;
                    radButton_Refresh.Enabled = true;
                    radButton_Vnc.Enabled = true;
                    radButton_Pda.Enabled = true;
                    break;
                case "1"://Center
                    radButton_Add.Enabled = false;
                    radButton_Delet.Enabled = false;
                    radButton_Edit.Enabled = true;
                    radButton_Excel.Enabled = true;
                    radButton_Refresh.Enabled = true;
                    radButton_Vnc.Enabled = false;
                    radButton_Pda.Enabled = false;
                    break;
                case "2"://ทัวไป
                    radButton_Add.Enabled = false;
                    radButton_Delet.Enabled = false;
                    radButton_Edit.Enabled = true;
                    radButton_Excel.Enabled = true;
                    radButton_Refresh.Enabled = false;
                    radButton_Vnc.Enabled = false;
                    radButton_Pda.Enabled = false;
                    break;
                default:
                    radButton_Add.Enabled = false;
                    radButton_Delet.Enabled = false;
                    radButton_Edit.Enabled = true;
                    radButton_Excel.Enabled = false;
                    radButton_Refresh.Enabled = false;
                    radButton_Vnc.Enabled = false;
                    radButton_Pda.Enabled = false;
                    break;
            }


        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("BRANCH_NAME", "ทั้งหมด : {0:F2}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void SetGDV()
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            dt = BranchClass.GetBranchAll_ByConditions(" '1','2','3','4','5' ", " '0','1','2' ", "");
            radGridView_Show.DataSource = dt;
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            Branch_Detail frmBranch_New = new Branch_Detail("", _pAdminstatus);
            if (frmBranch_New.ShowDialog(this) == DialogResult.OK)
            {
                if (frmBranch_New.ReturnExecut == "")
                {
                    dt = BranchClass.GetBranchAll_ByConditions(" '1','2','3','4','5' ", " '0','1','2' ", "");
                    radGridView_Show.DataSource = dt;
                }
            }
        }
        private void RadButton_Edit_Click(object sender, EventArgs e)
        {
            if (this.radGridView_Show.CurrentRow.Index > -1)
            {
                switch (radGridView_Show.CurrentColumn.Name.ToString())
                {
                    case "BRANCH_ROUTEBEER":
                        using (FormShare.ShowData.ShowDataDGV showData = new FormShare.ShowData.ShowDataDGV())
                        {
                            showData.dtData = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("49", "", "", "1");
                            DialogResult DrshowData = showData.ShowDialog();
                            if (DrshowData == DialogResult.Yes)
                            {

                                string res = BranchClass.Update_BRANCH_CONFIGDB("0", radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), showData.pID);
                                MsgBoxClass.MsgBoxShow_SaveStatus(res);
                                if (res == "") radGridView_Show.CurrentRow.Cells["BRANCH_ROUTEBEER"].Value = showData.pID;
                            }
                        }
                        return;
                    case "BRANCH_VENDERBEER":
                        using (FormShare.ShowData.ShowDataDGV showData = new FormShare.ShowData.ShowDataDGV())
                        {
                            showData.dtData = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("2", " AND REMARK = 'D034' ", "", "1");
                            DialogResult DrshowData = showData.ShowDialog();
                            if (DrshowData == DialogResult.Yes)
                            {

                                string res = BranchClass.Update_BRANCH_CONFIGDB("1", radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), showData.pID);
                                MsgBoxClass.MsgBoxShow_SaveStatus(res);
                                if (res == "") radGridView_Show.CurrentRow.Cells["BRANCH_VENDERBEER"].Value = showData.pID;
                            }
                        }
                        return;
                    case "BRANCH_V005450":
                        using (FormShare.InputData inputData = new FormShare.InputData("1", $@"ตั้งค่ารหัสร้านค้า V005450-เพรซิเดนท์ เบเกอรี่ บจก.", $@"สาขา {radGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value}", ""))
                        {
                            inputData.pInputData = radGridView_Show.CurrentRow.Cells["BRANCH_V005450"].Value.ToString();
                            DialogResult dr = inputData.ShowDialog();
                            if (dr == DialogResult.Yes)
                            {
                                DataTable dtIn = BranchClass.Update_BRANCH_CONFIGDB_CheckPK("BRANCH_V005450",
                                   radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), inputData.pInputData);
                                MsgBoxClass.MsgBoxShow_SaveStatus(dtIn.Rows[0]["DESC_STATUS"].ToString());
                                if (dtIn.Rows[0]["DESC_STATUS"].ToString() == "") radGridView_Show.CurrentRow.Cells["BRANCH_V005450"].Value = inputData.pInputData;
                            }
                        }

                        return;
                    case "BRANCH_V014483":
                        using (FormShare.InputData inputData = new FormShare.InputData("1", $@"ตั้งค่ารหัสร้านค้า V014483-บริษัท นวสร ดิสทรีบิวชั่น จำกัด", $@"สาขา {radGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value}", ""))
                        {
                            inputData.pInputData = radGridView_Show.CurrentRow.Cells["BRANCH_V014483"].Value.ToString();
                            DialogResult dr = inputData.ShowDialog();
                            if (dr == DialogResult.Yes)
                            {
                                DataTable dtIn = BranchClass.Update_BRANCH_CONFIGDB_CheckPK("BRANCH_V014483",
                                    radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), inputData.pInputData);
                                MsgBoxClass.MsgBoxShow_SaveStatus(dtIn.Rows[0]["DESC_STATUS"].ToString());
                                if (dtIn.Rows[0]["DESC_STATUS"].ToString() == "") radGridView_Show.CurrentRow.Cells["BRANCH_V014483"].Value = inputData.pInputData;
                            }
                        }
                        return;
                    default:

                        Branch_Detail frmBranch_Detail = new Branch_Detail(radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), _pAdminstatus);
                        if (frmBranch_Detail.ShowDialog(this) == DialogResult.OK)
                        {
                            if (frmBranch_Detail.ReturnExecut == "")
                            {
                                dt = BranchClass.GetBranchAll_ByConditions(" '1','2','3','4','5' ", " '0','1','2' ", "");
                                radGridView_Show.DataSource = dt;
                            }
                        }
                        break;
                }



            }
        }
        private void RadButton_Delet_Click(object sender, EventArgs e)
        {
            if (this.radGridView_Show.CurrentRow != null)
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmDelete(this.radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + " " + this.radGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString()) == DialogResult.Yes)
                {
                    DataRow[] rows = dt.Select("BRANCH_ID = '" + this.radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + "' ");
                    int iRow = dt.Rows.IndexOf(rows[0]);

                    string sqlIn = string.Format(@"DELETE FROM SHOP24HRS.dbo.SHOP_BRANCH WHERE BRANCH_ID = '" + this.radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + "'");

                    string returnstr = ConnectionClass.ExecuteSQL_Main(sqlIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);

                    if (returnstr == "")
                    {
                        dt.Rows[iRow].Delete();   //Delete
                        dt.AcceptChanges();
                    }
                }
                else
                {
                    return;// Do something  
                }
            }
        }
        private void RadButton_Vnc_Click(object sender, EventArgs e)
        {
            try
            {
                using (Process myProcess = new Process())
                {
                    myProcess.StartInfo.UseShellExecute = false;
                    myProcess.StartInfo.FileName = @"C:\\Program Files\RealVNC\VNC Viewer\vncviewer.EXE";
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ไม่มีโปรแกรม VNC ติดต่อ ComMN Tel 8570 เพื่อลงโปรแกรมก่อนใช้งาน. [" + ex.Message + "]",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void RadButton_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0)
            {
                return;
            }
            else
            {
                string returnExportStatus = DatagridClass.ExportExcelGridView("ข้อมูลสาขา ", radGridView_Show, "1");
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExportStatus);
            }

        }
        private void RadButton_Refresh_Click(object sender, EventArgs e)
        {
            SetGDV();
        }
        private void RadButton_Pda_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["SERVICEPDA"].Value.ToString() != "")
                {
                    Process.Start("chrome.exe", radGridView_Show.Rows[i].Cells["SERVICEPDA"].Value.ToString());
                }
            }
        }

    }
}