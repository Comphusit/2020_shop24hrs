﻿namespace PC_Shop24Hrs.ComMinimart.Manage
{
    partial class Empl_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Empl_Manager));
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_EmplID = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_refresh = new Telerik.WinControls.UI.RadButtonElement();
            this.radLabel_Pass = new Telerik.WinControls.UI.RadLabel();
            this.radButton_ResetPass = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Possition = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Empl = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_FindEmpl = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_EmplID.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Pass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_ResetPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindEmpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(656, 522);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(859, 623);
            this.radPanel1.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(859, 623);
            this.tableLayoutPanel1.TabIndex = 2;
            
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_EmplID, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(653, 617);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // RadGridView_EmplID
            // 
            this.RadGridView_EmplID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_EmplID.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_EmplID.Location = new System.Drawing.Point(4, 4);
            // 
            // 
            // 
            this.RadGridView_EmplID.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_EmplID.Name = "RadGridView_EmplID";
            // 
            // 
            // 
            this.RadGridView_EmplID.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_EmplID.Size = new System.Drawing.Size(645, 609);
            this.RadGridView_EmplID.TabIndex = 2;
            this.RadGridView_EmplID.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_EmplID_ViewCellFormatting);
            this.RadGridView_EmplID.SelectionChanged += new System.EventHandler(this.RadGridView_EmplID_SelectionChanged);
            this.RadGridView_EmplID.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_EmplID_ConditionalFormattingFormShown);
            this.RadGridView_EmplID.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_EmplID_FilterPopupRequired);
            this.RadGridView_EmplID.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_EmplID_FilterPopupInitialized);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.radLabel_Pass);
            this.panel1.Controls.Add(this.radButton_ResetPass);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBox_Remark);
            this.panel1.Controls.Add(this.radLabel_Dept);
            this.panel1.Controls.Add(this.radLabel_Possition);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radTextBox_Empl);
            this.panel1.Controls.Add(this.radLabel_EmplName);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.RadButton_FindEmpl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(662, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 617);
            this.panel1.TabIndex = 6;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.radButtonElement_Edit,
            this.commandBarSeparator2,
            this.RadButtonElement_refresh});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(192, 42);
            this.radStatusStrip1.TabIndex = 8;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่ม";
            this.radButtonElement_Add.UseCompatibleTextRendering = false;
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.AutoSize = true;
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.UseCompatibleTextRendering = false;
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_refresh
            // 
            this.RadButtonElement_refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.RadButtonElement_refresh.Name = "RadButtonElement_refresh";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_refresh, false);
            this.RadButtonElement_refresh.Text = "";
            this.RadButtonElement_refresh.Click += new System.EventHandler(this.RadButtonElement_refresh_Click);
            // 
            // radLabel_Pass
            // 
            this.radLabel_Pass.AutoSize = false;
            this.radLabel_Pass.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Pass.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Pass.Location = new System.Drawing.Point(8, 243);
            this.radLabel_Pass.Name = "radLabel_Pass";
            this.radLabel_Pass.Size = new System.Drawing.Size(175, 25);
            this.radLabel_Pass.TabIndex = 54;
            this.radLabel_Pass.Text = "พาส";
            // 
            // radButton_ResetPass
            // 
            this.radButton_ResetPass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_ResetPass.Location = new System.Drawing.Point(2, 394);
            this.radButton_ResetPass.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_ResetPass.Name = "radButton_ResetPass";
            this.radButton_ResetPass.Size = new System.Drawing.Size(189, 36);
            this.radButton_ResetPass.TabIndex = 63;
            this.radButton_ResetPass.Text = "รีเซ็ต รหัสผ่าน";
            this.radButton_ResetPass.ThemeName = "Fluent";
            this.radButton_ResetPass.Click += new System.EventHandler(this.RadButton_ResetPass_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_ResetPass.GetChildAt(0))).Text = "รีเซ็ต รหัสผ่าน";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_ResetPass.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_ResetPass.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ResetPass.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ResetPass.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ResetPass.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ResetPass.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ResetPass.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Blue;
            this.radLabel1.Location = new System.Drawing.Point(8, 274);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(175, 25);
            this.radLabel1.TabIndex = 53;
            this.radLabel1.Text = "หมายเหตุ";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(9, 306);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(175, 36);
            this.radTextBox_Remark.TabIndex = 1;
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.AutoSize = false;
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dept.Location = new System.Drawing.Point(9, 176);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(175, 60);
            this.radLabel_Dept.TabIndex = 53;
            this.radLabel_Dept.Text = "แผนก";
            this.radLabel_Dept.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel_Possition
            // 
            this.radLabel_Possition.AutoSize = false;
            this.radLabel_Possition.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Possition.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Possition.Location = new System.Drawing.Point(9, 144);
            this.radLabel_Possition.Name = "radLabel_Possition";
            this.radLabel_Possition.Size = new System.Drawing.Size(175, 25);
            this.radLabel_Possition.TabIndex = 52;
            this.radLabel_Possition.Text = "ตำแหน่ง";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.Location = new System.Drawing.Point(99, 350);
            this.radButton_Cancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 3;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Location = new System.Drawing.Point(2, 350);
            this.radButton_Save.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 2;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_Empl
            // 
            this.radTextBox_Empl.Enabled = false;
            this.radTextBox_Empl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Empl.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Empl.Location = new System.Drawing.Point(9, 83);
            this.radTextBox_Empl.Name = "radTextBox_Empl";
            this.radTextBox_Empl.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Empl.TabIndex = 0;
            this.radTextBox_Empl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Cst_KeyDown);
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.AutoSize = false;
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplName.Location = new System.Drawing.Point(9, 112);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(175, 25);
            this.radLabel_EmplName.TabIndex = 51;
            this.radLabel_EmplName.Text = "พนักงาน";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(9, 58);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(77, 19);
            this.radLabel2.TabIndex = 18;
            this.radLabel2.Text = "รหัสพนักงาน";
            // 
            // RadButton_FindEmpl
            // 
            this.RadButton_FindEmpl.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_FindEmpl.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_FindEmpl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_FindEmpl.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_FindEmpl.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_FindEmpl.Location = new System.Drawing.Point(157, 51);
            this.RadButton_FindEmpl.Name = "RadButton_FindEmpl";
            this.RadButton_FindEmpl.Size = new System.Drawing.Size(26, 26);
            this.RadButton_FindEmpl.TabIndex = 62;
            this.RadButton_FindEmpl.Text = "radButton3";
            this.RadButton_FindEmpl.Visible = false;
            // 
            // Empl_Manager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(859, 623);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Empl_Manager";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "พนักงาน";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Empl_Manager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_EmplID.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_EmplID)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Pass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_ResetPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindEmpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Empl;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
        private Telerik.WinControls.UI.RadButton RadButton_FindEmpl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_EmplID;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel_Possition;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        protected Telerik.WinControls.UI.RadButton radButton_ResetPass;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_refresh;
        private Telerik.WinControls.UI.RadLabel radLabel_Pass;
    }
}
