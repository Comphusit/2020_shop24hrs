﻿namespace PC_Shop24Hrs.ComMinimart.Manage
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_EmplName = new System.Windows.Forms.Label();
            this.label_EmplID = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_OldPassWord = new System.Windows.Forms.TextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.TextBox_NewPassWord1 = new System.Windows.Forms.TextBox();
            this.TextBox_NewPassWord2 = new System.Windows.Forms.TextBox();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label_EmplName);
            this.panel1.Controls.Add(this.label_EmplID);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TextBox_OldPassWord);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.TextBox_NewPassWord1);
            this.panel1.Controls.Add(this.TextBox_NewPassWord2);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(289, 216);
            this.panel1.TabIndex = 7;
            // 
            // label_EmplName
            // 
            this.label_EmplName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.label_EmplName.Location = new System.Drawing.Point(14, 33);
            this.label_EmplName.Name = "label_EmplName";
            this.label_EmplName.Size = new System.Drawing.Size(255, 29);
            this.label_EmplName.TabIndex = 24;
            this.label_EmplName.Text = "ขื่อ";
            this.label_EmplName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_EmplID
            // 
            this.label_EmplID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label_EmplID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.label_EmplID.Location = new System.Drawing.Point(14, 5);
            this.label_EmplID.Name = "label_EmplID";
            this.label_EmplID.Size = new System.Drawing.Size(255, 29);
            this.label_EmplID.TabIndex = 23;
            this.label_EmplID.Text = "รหัส";
            this.label_EmplID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(11, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 22;
            this.label3.Text = "รหัสผ่านเก่า";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(11, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "รหัสผ่านใหม่*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(11, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "รหัสผ่านใหม่*";
            // 
            // TextBox_OldPassWord
            // 
            this.TextBox_OldPassWord.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TextBox_OldPassWord.ForeColor = System.Drawing.Color.Blue;
            this.TextBox_OldPassWord.Location = new System.Drawing.Point(103, 73);
            this.TextBox_OldPassWord.Name = "TextBox_OldPassWord";
            this.TextBox_OldPassWord.PasswordChar = '*';
            this.TextBox_OldPassWord.Size = new System.Drawing.Size(166, 23);
            this.TextBox_OldPassWord.TabIndex = 1;
            this.TextBox_OldPassWord.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_OldPassWord_KeyDown);
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Location = new System.Drawing.Point(48, 167);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // TextBox_NewPassWord1
            // 
            this.TextBox_NewPassWord1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TextBox_NewPassWord1.ForeColor = System.Drawing.Color.Blue;
            this.TextBox_NewPassWord1.Location = new System.Drawing.Point(103, 104);
            this.TextBox_NewPassWord1.Name = "TextBox_NewPassWord1";
            this.TextBox_NewPassWord1.PasswordChar = '*';
            this.TextBox_NewPassWord1.Size = new System.Drawing.Size(166, 23);
            this.TextBox_NewPassWord1.TabIndex = 2;
            this.TextBox_NewPassWord1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_NewPassWord1_KeyDown);
            // 
            // TextBox_NewPassWord2
            // 
            this.TextBox_NewPassWord2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.TextBox_NewPassWord2.ForeColor = System.Drawing.Color.Blue;
            this.TextBox_NewPassWord2.Location = new System.Drawing.Point(102, 134);
            this.TextBox_NewPassWord2.Name = "TextBox_NewPassWord2";
            this.TextBox_NewPassWord2.PasswordChar = '*';
            this.TextBox_NewPassWord2.Size = new System.Drawing.Size(166, 23);
            this.TextBox_NewPassWord2.TabIndex = 3;
            this.TextBox_NewPassWord2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_NewPassWord2_KeyDown);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.Location = new System.Drawing.Point(153, 167);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 216);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePassword";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "เปลี่ยนรหัสผ่าน";
            this.Load += new System.EventHandler(this.ChangePassword_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.Panel panel1;
        protected System.Windows.Forms.Label label3;
        protected System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.TextBox TextBox_OldPassWord;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected System.Windows.Forms.TextBox TextBox_NewPassWord1;
        protected System.Windows.Forms.TextBox TextBox_NewPassWord2;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected System.Windows.Forms.Label label_EmplID;
        protected System.Windows.Forms.Label label_EmplName;
    }
}