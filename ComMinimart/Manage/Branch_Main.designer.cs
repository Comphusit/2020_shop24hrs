﻿namespace PC_Shop24Hrs.ComMinimart.Manage
{
    partial class Branch_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Branch_Main));
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Delet = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Vnc = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Refresh = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Pda = new Telerik.WinControls.UI.RadButtonElement();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(778, 482);
            this.radGridView_Show.TabIndex = 2;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            this.radGridView_Show.DoubleClick += new System.EventHandler(this.RadButton_Edit_Click);
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.radButton_Add,
            this.commandBarSeparator1,
            this.radButton_Edit,
            this.commandBarSeparator3,
            this.radButton_Delet,
            this.commandBarSeparator4,
            this.radButton_Vnc,
            this.commandBarSeparator5,
            this.radButton_Excel,
            this.commandBarSeparator6,
            this.radButton_Refresh,
            this.commandBarSeparator7,
            this.radButton_Pda});
            this.radStatusStrip.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(778, 42);
            this.radStatusStrip.TabIndex = 7;
            ((Telerik.WinControls.UI.RadStatusBarElement)(this.radStatusStrip.GetChildAt(0))).StretchVertically = false;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).AutoSize = true;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Alignment = System.Drawing.ContentAlignment.BottomRight;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Enabled = false;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButton_Add
            // 
            this.radButton_Add.AutoSize = true;
            this.radButton_Add.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Add, false);
            this.radButton_Add.Text = "เพิ่ม";
            this.radButton_Add.ToolTipText = "เพิ่ม";
            this.radButton_Add.UseCompatibleTextRendering = false;
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButton_Edit
            // 
            this.radButton_Edit.AutoSize = true;
            this.radButton_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButton_Edit.Name = "radButton_Edit";
            this.radButton_Edit.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Edit, false);
            this.radButton_Edit.Text = "แก้ไข";
            this.radButton_Edit.ToolTipText = "แก้ไข";
            this.radButton_Edit.UseCompatibleTextRendering = false;
            this.radButton_Edit.Click += new System.EventHandler(this.RadButton_Edit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButton_Delet
            // 
            this.radButton_Delet.AutoSize = true;
            this.radButton_Delet.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Delet.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Delet.Name = "radButton_Delet";
            this.radButton_Delet.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Delet, false);
            this.radButton_Delet.Text = "ลบ";
            this.radButton_Delet.ToolTipText = "ลบ";
            this.radButton_Delet.UseCompatibleTextRendering = false;
            this.radButton_Delet.Click += new System.EventHandler(this.RadButton_Delet_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButton_Vnc
            // 
            this.radButton_Vnc.AutoSize = true;
            this.radButton_Vnc.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Vnc.Image = global::PC_Shop24Hrs.Properties.Resources.remote;
            this.radButton_Vnc.Name = "radButton_Vnc";
            this.radButton_Vnc.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Vnc, false);
            this.radButton_Vnc.Text = "vnc";
            this.radButton_Vnc.ToolTipText = "vnc";
            this.radButton_Vnc.UseCompatibleTextRendering = false;
            this.radButton_Vnc.Click += new System.EventHandler(this.RadButton_Vnc_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButton_Excel
            // 
            this.radButton_Excel.AutoSize = true;
            this.radButton_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButton_Excel.Name = "radButton_Excel";
            this.radButton_Excel.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Excel, false);
            this.radButton_Excel.Text = "excel";
            this.radButton_Excel.ToolTipText = "export excel";
            this.radButton_Excel.UseCompatibleTextRendering = false;
            this.radButton_Excel.Click += new System.EventHandler(this.RadButton_Excel_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // radButton_Refresh
            // 
            this.radButton_Refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_Refresh.Name = "radButton_Refresh";
            this.radStatusStrip.SetSpring(this.radButton_Refresh, false);
            this.radButton_Refresh.Text = "Refresh";
            this.radButton_Refresh.ToolTipText = "Refresh";
            this.radButton_Refresh.Click += new System.EventHandler(this.RadButton_Refresh_Click);
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.radStatusStrip.SetSpring(this.commandBarSeparator7, false);
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // radButton_Pda
            // 
            this.radButton_Pda.AutoSize = true;
            this.radButton_Pda.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Pda.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.radButton_Pda.Name = "radButton_Pda";
            this.radStatusStrip.SetSpring(this.radButton_Pda, false);
            this.radButton_Pda.Text = "radButtonElement1";
            this.radButton_Pda.ToolTipText = "ServicePDA";
            this.radButton_Pda.Click += new System.EventHandler(this.RadButton_Pda_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radStatusStrip, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 539);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(778, 19);
            this.radLabel_Detail.TabIndex = 54;
            // 
            // Branch_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Branch_Main";
            this.Text = "Branch_Main";
            this.Load += new System.EventHandler(this.Branch_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.RadButtonElement radButton_Add;
        private Telerik.WinControls.UI.RadButtonElement radButton_Edit;
        private Telerik.WinControls.UI.RadButtonElement radButton_Delet;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadButtonElement radButton_Vnc;
        private Telerik.WinControls.UI.RadButtonElement radButton_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButton_Refresh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private Telerik.WinControls.UI.RadButtonElement radButton_Pda;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}