﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.IO;
using System.Collections.Generic;

namespace PC_Shop24Hrs.ComMinimart.Manage
{
    public partial class Branch_Detail : Form
    {
        public string ReturnExecut;
        readonly string _pBranchID;
        readonly string _pAdmstatus;
        readonly private Control.ControlCollection controlCollectionGroupBox;
        readonly DataTable datagrid = new DataTable();
        string statusShelf, statusFreeItem;
        string routeBeer, venderLeo;

        public Branch_Detail(string pBranchID, string pAdmstatus) //pAdmstatus >> 0 = Admin,1 = Center
        {
            //** SHOP_BRANCH_CONFIGDB ทุกฟิล ยกเว้น BranchName ในส่วนของ Group_Programmer
            // สิด Group_Programmer เฉพาะ Programmer,RadGroupBox_Center=Center,Group_Internet+Group_Gernaral = ComMN
            InitializeComponent();
            this.KeyPreview = true;
            _pBranchID = pBranchID;
            _pAdmstatus = pAdmstatus;
            this.SetButton();
            this.SetGroupBox();
            this.SetCheckBox();
            controlCollectionGroupBox = radPanel_Branch.Controls;
            if (controlCollectionGroupBox != null)
            {
                foreach (RadGroupBox c in controlCollectionGroupBox.OfType<RadGroupBox>())
                {
                    DatagridClass.SetTextControls(c.Controls, (int)(Camera.CollectionControl.RadTextBoxBase));
                    DatagridClass.SetTextControls(c.Controls, (int)(Camera.CollectionControl.RadDropDownList));
                }
            }
            this.SetMultiColumnComboBox();
            this.SetGridView();
        }
        //OnKeyDown
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }

        //MainLoad
        private void Branch_Detail_Load(object sender, EventArgs e)
        {
            GetBranchPrice();
            GetBranchRouteBeer();
            GetBranchVenderBeer(); DatagridClass.SetDefaultFontDropDown(radDropDownList_VenderLeo);
            GetBranchSize();
            GetBranchSTA();
            GetBranchStaopen();
            GetBranchOut();
            SetPermission();
            GetCity();
            BranchClass.GetDataRouteSendCoin(radDropDownList_ec);
            if (string.IsNullOrEmpty(_pBranchID))
            {
                this.Text += " :: เพิ่มสาขา";
                SetDefaultData();
                radTextBox_Branch.TextBoxElement.TextBoxItem.HostedControl.Select();
            }
            else
            {
                this.Text += " :: แก้ไข";
                radTextBox_Branch.Enabled = false;
                radTextBox_BranchName.TextBoxElement.TextBoxItem.HostedControl.Select();
                GetBranchInfor();
                GetImg();
            }

            if ((SystemClass.SystemDptID != "D156") && (SystemClass.SystemDptID != "D179"))
            {
                RadGroupBox_Programmer.Visible = false;
                RadGroupBox_Internet.Visible = false;
            }
        }
        void SetCheckBox()
        {
            radCheckBox_StatusShelf.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_FreeItem.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral;
        }
        void SetButton()
        {
            radButton_save.ButtonElement.ShowBorder = true;
            radButton_cancle.ButtonElement.ShowBorder = true;
            radButton_Upload.ButtonElement.ShowBorder = true;
        }
        void SetGroupBox()
        {
            DatagridClass.SetDefaultFontGroupBox(RadGroupBox_Gernaral);
            DatagridClass.SetDefaultFontGroupBox(RadGroupBox_Internet);
            DatagridClass.SetDefaultFontGroupBox(RadGroupBox_Programmer);
            DatagridClass.SetDefaultFontGroupBox(RadGroupBox_Center);
        }
        void SetMultiColumnComboBox()
        {
            RadMultiColumnComboBoxElement multiColumnComboElement = this.radMultiColumnComboBox_Size.MultiColumnComboBoxElement;
            multiColumnComboElement.DropDownMinSize = new Size(250, 200);
            radMultiColumnComboBox_Size.MultiColumnComboBoxElement.Font = SystemClass.SetFontGernaral;
            radMultiColumnComboBox_Size.MultiColumnComboBoxElement.ForeColor = ConfigClass.SetColor_Blue();
            radMultiColumnComboBox_Size.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ขนาด", 50));
            radMultiColumnComboBox_Size.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "คำอธิบาย", 150));
        }
        void SetGridView()
        {
            RadGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Text", "Filename", "ชื่อไฟล์", 110));
            RadGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Command", "Watch", "", 30));
            RadGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Command", "Delete", "", 30));
            RadGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Text", "Filefull", "", 30));
            RadGridView_Show.MasterTemplate.Columns["Filefull"].IsVisible = false;
        }
        void SetPermission()
        {
            //Group_Programmer
            radTextBox_Serverspc.Enabled = false;
            radTextBox_ServerIp.Enabled = false;
            radTextBox_DatabaseName.Enabled = false;
            radTextBox_PathPos.Enabled = false;
            radTextBox_Password.Enabled = false;
            radTextBox_UserName.Enabled = false;
            radTextBox_WH.Enabled = false;
            radTextBox_TaxLabel.Enabled = false;
            radTextBox_TaxNumber.Enabled = false;
            radTextBox_SmartShop.Enabled = false;
            radTextBox_SendMoney.Enabled = false;
            radTextBox_FarmHouse.Enabled = false;
            radTextBox_PDAversion.Enabled = false;
            radTextBox_Androidversion.Enabled = false;
            radTextBox_Tel.Enabled = false;
            radTextBox_ComOT.Enabled = false;
            radTextBox_ComLocation.Enabled = false;
            radTextBox_AddOnPer.Enabled = false;
            radDropDownList_ec.Enabled = false;
            radDropDownList_Beer.Enabled = false;
            radDropDownList_VenderLeo.Enabled = false;

            switch (_pAdmstatus)
            {
                case "0":
                    //Group_Gernaral
                    radTextBox_Branch.Enabled = true;
                    radTextBox_BranchName.Enabled = true;
                    radDropDownList_Price.Enabled = true;
                    radTextBox_Latitude.Enabled = true;
                    radTextBox_Longtitude.Enabled = true;
                    radDropDownList_Status.Enabled = true;
                    radDropDownList_Staopen.Enabled = true;
                    radDropDownList_Out.Enabled = true;
                    radDropDownList_City.Enabled = true;
                    radDropDownList_Beer.Enabled = true;
                    radDropDownList_VenderLeo.Enabled = true;
                    //Group_Internet
                    radTextBox_Service.Enabled = true;
                    radTextBox_ServiceCall.Enabled = true;
                    radTextBox_Serviceid.Enabled = true;
                    radTextBox_remarks.Enabled = true;
                    //RadGroupBox_Center
                    radTextBox_Address.Enabled = true;
                    radTextBox_Head.Enabled = true;
                    radMultiColumnComboBox_Size.Enabled = true;
                    radTextBox_Max.Enabled = true;
                    radTextBox_Min.Enabled = true;
                    radTextBox_Tel.Enabled = true;
                    radTextBox_ComOT.Enabled = true;
                    radTextBox_ComLocation.Enabled = true;
                    radTextBox_AddOnPer.Enabled = true;
                    radDropDownList_ec.Enabled = true;
                    radCheckBox_StatusShelf.Enabled = true;
                    radCheckBox_FreeItem.Enabled = true;

                    if (SystemClass.SystemComProgrammer == "1" || SystemClass.SystemDptID.Equals("D156"))
                    {
                        //Group_Programmer
                        radTextBox_Serverspc.Enabled = true;
                        radTextBox_ServerIp.Enabled = true;
                        radTextBox_DatabaseName.Enabled = true;
                        radTextBox_PathPos.Enabled = true;
                        radTextBox_Password.Enabled = true;
                        radTextBox_UserName.Enabled = true;
                        radTextBox_WH.Enabled = true;
                        radTextBox_TaxLabel.Enabled = true;
                        radTextBox_TaxNumber.Enabled = true;
                        radTextBox_SmartShop.Enabled = true;
                        radTextBox_SendMoney.Enabled = true;
                        radTextBox_FarmHouse.Enabled = true;
                        radTextBox_PDAversion.Enabled = true;
                        radTextBox_Androidversion.Enabled = true;
                    }
                    break;
                case "1":
                    //Group_Gernaral
                    radTextBox_Branch.Enabled = false;
                    radTextBox_BranchName.Enabled = false;
                    radDropDownList_Price.Enabled = false;
                    radTextBox_Latitude.Enabled = false;
                    radTextBox_Longtitude.Enabled = false;
                    radDropDownList_Staopen.Enabled = false;
                    radDropDownList_Status.Enabled = false;
                    radDropDownList_Out.Enabled = false;
                    radDropDownList_City.Enabled = false;
                    radDropDownList_Beer.Enabled = false;
                    radDropDownList_VenderLeo.Enabled = false;
                    //Group_Internet
                    radTextBox_Service.Enabled = false;
                    radTextBox_ServiceCall.Enabled = false;
                    radTextBox_Serviceid.Enabled = false;
                    radTextBox_remarks.Enabled = false;
                    //RadGroupBox_Center
                    radTextBox_Address.Enabled = true;
                    radTextBox_Head.Enabled = true;
                    radMultiColumnComboBox_Size.Enabled = true;
                    radTextBox_Max.Enabled = true;
                    radTextBox_Min.Enabled = true;
                    radTextBox_Tel.Enabled = true;
                    radTextBox_ComOT.Enabled = true;
                    radTextBox_ComLocation.Enabled = true;
                    radTextBox_AddOnPer.Enabled = true;
                    radDropDownList_ec.Enabled = true;
                    radCheckBox_StatusShelf.Enabled = true;
                    radCheckBox_FreeItem.Enabled = true;

                    break;
                case "2":
                    //Group_Gernaral
                    radTextBox_Branch.Enabled = false;
                    radTextBox_BranchName.Enabled = false;
                    radDropDownList_Price.Enabled = false;
                    radTextBox_Latitude.Enabled = false;
                    radTextBox_Longtitude.Enabled = false;
                    radDropDownList_Staopen.Enabled = false;
                    radDropDownList_Status.Enabled = false;
                    radDropDownList_Out.Enabled = false;
                    radDropDownList_City.Enabled = false;
                    if ((SystemClass.SystemDptID == "D034") || (SystemClass.SystemComMinimart == "1")) { radDropDownList_Beer.Enabled = true; radDropDownList_VenderLeo.Enabled = true; }
                    else { radDropDownList_Beer.Enabled = false; radDropDownList_VenderLeo.Enabled = false; }
                    //Group_Internet
                    radTextBox_Service.Enabled = false;
                    radTextBox_ServiceCall.Enabled = false;
                    radTextBox_Serviceid.Enabled = false;
                    radTextBox_remarks.Enabled = false;
                    //RadGroupBox_Center
                    radTextBox_Address.Enabled = false;
                    radTextBox_Head.Enabled = false;
                    radMultiColumnComboBox_Size.Enabled = false;
                    radTextBox_Max.Enabled = false;
                    radTextBox_Min.Enabled = false;
                    radCheckBox_StatusShelf.Enabled = false;
                    radCheckBox_FreeItem.Enabled = false;
                    radTextBox_Tel.Enabled = false;
                    radTextBox_ComOT.Enabled = false;
                    radTextBox_ComLocation.Enabled = false;
                    radTextBox_AddOnPer.Enabled = false;
                    radDropDownList_ec.Enabled = false;
                    break;
            }
        }
        //Set Data
        void SetGroupProgrammer()
        {
            DataTable dt = ConnectionClass.SelectSQL_Main($@"
                SELECT  [BRANCH_CHANNEL],[BRANCH_TAXNUMBER],[BRANCH_SERVERNAME],[BRANCH_SERVER_IP],[BRANCH_DATABASENAME],[BRANCH_USERNAME]
                        ,[BRANCH_PASSWORDNAME],[BRANCH_SEQTAX],[BRANCH_SMARTSHOP_LIMIT],[BRANCH_SendMoney],[BRANCH_FarmHouse]
                        ,[BRANCH_VERSIONUPDATEPDA],[BRANCH_VERSIONUPDATEANDROID],[BRANCH_STATUSSHELF],[BRANCH_PATHPOS],[BRANCH_FreeItem],
                        ISNULL([BRANCH_ROUTEBEER],'') AS BRANCH_ROUTEBEER,ISNULL([BRANCH_VENDERBEER],'') AS BRANCH_VENDERBEER
                FROM    SHOP_BRANCH_CONFIGDB WITH (NOLOCK)
                WHERE	[BRANCH_ID] = '{_pBranchID}' ");
            if (dt.Rows.Count > 0)
            {
                radTextBox_Serverspc.Text = dt.Rows[0]["BRANCH_SERVERNAME"].ToString();
                radTextBox_ServerIp.Text = dt.Rows[0]["BRANCH_SERVER_IP"].ToString();
                radTextBox_DatabaseName.Text = dt.Rows[0]["BRANCH_DATABASENAME"].ToString();
                radTextBox_PathPos.Text = dt.Rows[0]["BRANCH_PATHPOS"].ToString();
                radTextBox_Password.Text = dt.Rows[0]["BRANCH_PASSWORDNAME"].ToString();
                radTextBox_UserName.Text = dt.Rows[0]["BRANCH_USERNAME"].ToString();
                radTextBox_WH.Text = dt.Rows[0]["BRANCH_CHANNEL"].ToString();
                radTextBox_TaxLabel.Text = dt.Rows[0]["BRANCH_SEQTAX"].ToString();
                radTextBox_TaxNumber.Text = dt.Rows[0]["BRANCH_TAXNUMBER"].ToString();
                radTextBox_SmartShop.Text = dt.Rows[0]["BRANCH_SMARTSHOP_LIMIT"].ToString();
                radTextBox_SendMoney.Text = dt.Rows[0]["BRANCH_SendMoney"].ToString();
                radTextBox_FarmHouse.Text = dt.Rows[0]["BRANCH_FarmHouse"].ToString();
                radTextBox_PDAversion.Text = dt.Rows[0]["BRANCH_VERSIONUPDATEPDA"].ToString();
                radTextBox_Androidversion.Text = dt.Rows[0]["BRANCH_VERSIONUPDATEANDROID"].ToString();
                statusShelf = dt.Rows[0]["BRANCH_STATUSSHELF"].ToString();
                if (statusShelf.Equals("1")) radCheckBox_StatusShelf.CheckState = CheckState.Checked;
                else radCheckBox_StatusShelf.CheckState = CheckState.Unchecked;
                statusFreeItem = dt.Rows[0]["BRANCH_FreeItem"].ToString();
                if (statusFreeItem == "1") radCheckBox_FreeItem.CheckState = CheckState.Checked;
                else radCheckBox_FreeItem.CheckState = CheckState.Unchecked;

                routeBeer = dt.Rows[0]["BRANCH_ROUTEBEER"].ToString();
                if (routeBeer != "") radDropDownList_Beer.SelectedValue = routeBeer;
                venderLeo = dt.Rows[0]["BRANCH_VENDERBEER"].ToString();
                if (venderLeo != "") radDropDownList_VenderLeo.SelectedValue = venderLeo;

            }
        }
        #region"Function"
        private void SetDefaultData()
        {
            radTextBox_Branch.Text = string.Empty;
            radTextBox_BranchName.Text = string.Empty;
            radTextBox_Address.Text = string.Empty;
            radTextBox_remarks.Text = string.Empty;
            radTextBox_Serverspc.Text = string.Empty;
            radTextBox_ServerIp.Text = string.Empty;
            radTextBox_Password.Text = string.Empty;
            radTextBox_Service.Text = string.Empty;
            radTextBox_Serviceid.Text = string.Empty;
            radTextBox_ServiceCall.Text = string.Empty;
            radTextBox_DatabaseName.Text = string.Empty;
            radTextBox_UserName.Text = string.Empty;
            radTextBox_Head.Text = string.Empty;
            radTextBox_Max.Text = string.Empty;
            radTextBox_Min.Text = string.Empty;
            radLabel_SizeName.Text = string.Empty;
            radLabel_ManagerName.Text = string.Empty;

            radTextBox_Serverspc.Text = "";
            radTextBox_ServerIp.Text = "";
            radTextBox_DatabaseName.Text = "";
            radTextBox_PathPos.Text = "";
            radTextBox_Password.Text = "";
            radTextBox_UserName.Text = "";
            radTextBox_WH.Text = "";
            radTextBox_TaxLabel.Text = "";
            radTextBox_TaxNumber.Text = "";
            radTextBox_SmartShop.Text = "";
            radTextBox_SendMoney.Text = "";
            radTextBox_FarmHouse.Text = "";
            radTextBox_PDAversion.Text = "";
            radTextBox_Androidversion.Text = "";
        }
        private void GetBranchPrice()
        {
            //string sql = string.Format(@"SELECT COLUMNNAME,LABEL FROM SHOP_PRICE WITH (NOLOCK) ORDER BY COLUMNNAME ");
            DataTable dt = BranchClass.GetPriceLevel(); //Controllers.ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                var ListPrice = radDropDownList_Price;
                ListPrice.DataSource = dt;
                ListPrice.DisplayMember = "LABEL";
                ListPrice.ValueMember = "COLUMNNAME";
                radDropDownList_Price.SelectedIndex = -1;
            }
        }
        private void GetBranchSize()
        {
            DataTable dt = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("11", "", "", "1");
            if (dt.Rows.Count > 0)
            {
                this.radMultiColumnComboBox_Size.DisplayMember = "SHOW_NAME";
                this.radMultiColumnComboBox_Size.ValueMember = "SHOW_NAME";
                radMultiColumnComboBox_Size.DataSource = dt;
                radMultiColumnComboBox_Size.SelectedIndex = -1;
            }
        }
        private void GetBranchRouteBeer()
        {
            DataTable dt = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("49", "", "", "1");
            if (dt.Rows.Count > 0)
            {
                this.radDropDownList_Beer.DisplayMember = "SHOW_NAME";
                this.radDropDownList_Beer.ValueMember = "SHOW_ID";
                radDropDownList_Beer.DataSource = dt;
                radDropDownList_Beer.SelectedIndex = -1;
            }
        }

        private void GetBranchVenderBeer()
        {
            DataTable dt = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("2", " AND REMARK = 'D034' ", "", "1");
            if (dt.Rows.Count > 0)
            {
                this.radDropDownList_VenderLeo.DisplayMember = "DESCSHOW";
                this.radDropDownList_VenderLeo.ValueMember = "SHOW_ID";
                radDropDownList_VenderLeo.DataSource = dt;
                radDropDownList_VenderLeo.SelectedIndex = -1;
            }
        }
        private void GetBranchSTA()
        {
            string sql = $@"SELECT BRANCH_STA, CASE WHEN BRANCH_STA = '1' THEN 'มินิมาร์ท' WHEN BRANCH_STA = '2' THEN 'ซุปเปอร์ชีป' WHEN BRANCH_STA = '3' THEN 'ร้านยาน้องคอป'
                                    WHEN BRANCH_STA = '4' THEN 'ซุปเปอร์ชีป[ย่อย]' END AS BRANCH_STATUS
                                    FROM SHOP_BRANCH WITH(NOLOCK) GROUP BY BRANCH_STA ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radDropDownList_Status.DisplayMember = "BRANCH_STATUS";
                radDropDownList_Status.ValueMember = "BRANCH_STA";
                radDropDownList_Status.DataSource = dt;
                radDropDownList_Status.SelectedIndex = -1;
            }
        }
        private void GetBranchStaopen()
        {
            string sql = $@"SELECT BRANCH_STAOPEN AS STAOPEN,CASE WHEN BRANCH_STAOPEN = '0' THEN 'กำลังก่อสร้าง' WHEN BRANCH_STAOPEN = '1' THEN 'เปิดบริการ'  
                                        WHEN BRANCH_STAOPEN = '2' THEN 'ปิดบริการ' END AS BRANCH_STAOPEN   
                                        FROM SHOP_BRANCH WITH(NOLOCK) GROUP BY BRANCH_STAOPEN ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radDropDownList_Staopen.DisplayMember = "BRANCH_STAOPEN";
                radDropDownList_Staopen.ValueMember = "STAOPEN";
                radDropDownList_Staopen.DataSource = dt;
                radDropDownList_Staopen.SelectedIndex = -1;
            }
        }
        private void GetBranchOut()
        {
            string sql = $@"SELECT BRANCH_OUTPHUKET AS STAOUT,CASE BRANCH_OUTPHUKET WHEN  '0' THEN 'ต่างจังหวัด' WHEN  '1' THEN 'ในจังหวัด' END AS OUTLABEL
                                FROM SHOP_BRANCH WITH(NOLOCK) GROUP BY BRANCH_OUTPHUKET ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radDropDownList_Out.DisplayMember = "OUTLABEL";
                radDropDownList_Out.ValueMember = "STAOUT";
                radDropDownList_Out.DataSource = dt;
                radDropDownList_Out.SelectedIndex = -1;
            }
        }
        void GetCity()
        {
            //string sql = string.Format(@"
            //    SELECT [BRANCH_PROVINCE]
            //         ,CASE [BRANCH_PROVINCE]  
            //             WHEN '0' THEN 'ภูเก็ต'  
            //             WHEN '1' THEN 'พังงา'  
            //             WHEN '2' THEN 'กระบี่'  
            //             WHEN '3' THEN 'สุราษฎร์ธานี'  
            //       WHEN '4' THEN 'เกาะสมุย'  
            //       WHEN '5' THEN 'นครศรีธรรมราช'  
            //          END AS CITY 
            //    FROM    [SHOP_BRANCH] WITH (NOLOCK) 
            //    group by [BRANCH_PROVINCE]
            //    ");
            //DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            DataTable dt = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("50", "", "", "1");
            if (dt.Rows.Count > 0)
            {
                radDropDownList_City.DisplayMember = "SHOW_DESC";
                radDropDownList_City.ValueMember = "SHOW_ID";
                radDropDownList_City.DataSource = dt;
                radDropDownList_City.SelectedIndex = -1;
            }
        }
        private void GetBranchInfor()
        {
            DataTable dt = BranchClass.GetDetailBranchByID(_pBranchID, "");
            if (dt.Rows.Count > 0)
            {
                radTextBox_Branch.Text = dt.Rows[0]["BRANCH_ID"].ToString();
                radTextBox_BranchName.Text = dt.Rows[0]["BRANCH_NAME"].ToString();
                radTextBox_Address.Text = dt.Rows[0]["BRANCH_ADDR"].ToString();
                radTextBox_remarks.Text = dt.Rows[0]["BRANCH_REMARK"].ToString();
                radTextBox_Service.Text = dt.Rows[0]["BRANCH_SERVICE"].ToString();
                radTextBox_Serviceid.Text = dt.Rows[0]["BRANCH_SERVICEID"].ToString();
                radTextBox_ServiceCall.Text = dt.Rows[0]["BRANCH_SERVICETEL"].ToString();
                radTextBox_Head.Text = dt.Rows[0]["BRANCH_ManagerID"].ToString();
                radLabel_ManagerName.Text = dt.Rows[0]["BRANCH_ManagerName"].ToString();
                radTextBox_Max.Text = dt.Rows[0]["BRANCH_MAXPEOPLE1"].ToString();
                radTextBox_Min.Text = dt.Rows[0]["BRANCH_MAXPEOPLE2"].ToString();
                radTextBox_Latitude.Text = dt.Rows[0]["BRANCH_LATITUDE"].ToString();
                radTextBox_Longtitude.Text = dt.Rows[0]["BRANCH_LONGTITUDE"].ToString();
                radDropDownList_Price.SelectedValue = dt.Rows[0]["BRANCH_PRICE"].ToString();
                radDropDownList_Status.SelectedValue = dt.Rows[0]["BRANCH_STA"].ToString();
                radDropDownList_Staopen.SelectedValue = dt.Rows[0]["BRANCH_STAOPEN"].ToString();
                radMultiColumnComboBox_Size.SelectedValue = dt.Rows[0]["BRANCH_SIZE"].ToString();
                radDropDownList_Out.SelectedValue = dt.Rows[0]["BRANCH_OUTPHUKET"].ToString();
                radDropDownList_City.SelectedValue = dt.Rows[0]["BRANCH_PROVINCE"].ToString();
                if (dt.Rows[0]["BRANCH_SIZE"].ToString() == "") { radLabel_SizeName.Text = ""; }
                radTextBox_Tel.Text = dt.Rows[0]["BRANCH_TEL"].ToString();
                radTextBox_ComOT.Text = dt.Rows[0]["BRANCH_CONFIGCOMMISSION_OT"].ToString();
                radTextBox_ComLocation.Text = dt.Rows[0]["BRANCH_CONFIGCOMMISSION_LOCATION"].ToString();
                radTextBox_AddOnPer.Text = dt.Rows[0]["BRANCH_CONFIGCOMMISSION_PerAddON"].ToString();
                radDropDownList_ec.SelectedValue = dt.Rows[0]["BRANCH_RouteSendCoin"].ToString();
                SetGroupProgrammer();
            }
        }
        private string GetExecut()
        {
            string headID = radTextBox_Head.Text;
            if (radTextBox_Head.Text.Length != 7) headID = "";

            ArrayList sqlIn = new ArrayList();
            #region"admin"
            if (_pAdmstatus == "0")
            {
                if (_pBranchID == "")
                {
                    sqlIn.Add(string.Format(@"INSERT INTO dbo.SHOP_BRANCH
                                (BRANCH_ID,
                                  BRANCH_NAME,
                                  BRANCH_ADDR,
                                  BRANCH_PRICE,
                                  BRANCH_ManagerID,
                                  BRANCH_ManagerName,
                                  BRANCH_REMARK,
                                  BRANCH_SERVICE,
                                  BRANCH_SERVICEID,
                                  BRANCH_SERVICETEL,
                                  BRANCH_SIZE,
                                  BRANCH_MAXPEOPLE1,
                                  BRANCH_MAXPEOPLE2,
                                  BRANCH_STA,
                                  BRANCH_OUTPHUKET,
                                  BRANCH_STAOPEN,
                                  BRANCH_PROVINCE,
                                  BRANCH_LATITUDE,
                                  BRANCH_LONGTITUDE,BRANCH_RouteSendCoin)
                                VALUES('" + radTextBox_Branch.Text.Trim().ToUpper() + @"',
                                '" + radTextBox_BranchName.Text + @"',
                                '" + radTextBox_Address.Text + @"',
                                '" + GetDropDownListValue(radDropDownList_Price) + @"',
                                '" + headID + @"',
                                '" + radLabel_ManagerName.Text + @"',
                                '" + radTextBox_remarks.Text + @"',
                                '" + radTextBox_Service.Text + @"',
                                '" + radTextBox_Serviceid.Text + @"',
                                '" + radTextBox_ServiceCall.Text + @"',
                                '" + GetMultiDropDownListValue(radMultiColumnComboBox_Size) + @"',
                                '" + radTextBox_Max.Text + @"',
                                '" + radTextBox_Min.Text + @"',
                                '" + GetStatusValue(radDropDownList_Status) + @"',
                                '" + GetStatusOut() + @"',
                                '" + GetStatusValue(radDropDownList_Staopen) + @"',
                                '" + GetStatusValue(radDropDownList_City) + @"',
                                CAST('{0}' AS DECIMAL(28, 12)),
                                CAST('{1}' AS DECIMAL(28, 12)))
                                ", GetLocation(radTextBox_Latitude), GetLocation(radTextBox_Longtitude), radDropDownList_ec.SelectedValue.ToString()));
                }
                else
                {
                    sqlIn.Add(string.Format(@"UPDATE dbo.SHOP_BRANCH 
                            SET 
                            BRANCH_NAME = '" + radTextBox_BranchName.Text + @"'
                            ,BRANCH_ADDR = '" + radTextBox_Address.Text + @"'
                            ,BRANCH_PRICE = '" + GetDropDownListValue(radDropDownList_Price) + @"'
                            ,BRANCH_ManagerID = '" + headID + @"'
                            ,BRANCH_ManagerName = '" + radLabel_ManagerName.Text + @"'
                            ,BRANCH_REMARK = '" + radTextBox_remarks.Text + @"'
                            ,BRANCH_SERVICE = '" + radTextBox_Service.Text + @"'
                            ,BRANCH_SERVICEID = '" + radTextBox_Serviceid.Text + @"'
                            ,BRANCH_SERVICETEL = '" + radTextBox_ServiceCall.Text + @"'
                            ,BRANCH_SIZE = '" + GetMultiDropDownListValue(radMultiColumnComboBox_Size) + @"'
                            ,BRANCH_MAXPEOPLE1 = '" + radTextBox_Max.Text.Trim() + @"'
                            ,BRANCH_MAXPEOPLE2 = '" + radTextBox_Min.Text.Trim() + @"'
                            ,BRANCH_OUTPHUKET = '" + GetStatusOut() + @"'
                            ,BRANCH_STAOPEN = '" + GetStatusValue(radDropDownList_Staopen) + @"'
                            ,BRANCH_STA = '" + GetStatusValue(radDropDownList_Status) + @"'
                            ,BRANCH_PROVINCE = '" + GetStatusValue(radDropDownList_City) + @"'
                            ,BRANCH_LATITUDE =  CAST('{0}' AS DECIMAL(28,12))
                            ,BRANCH_LONGTITUDE =  CAST('{1}' AS DECIMAL(28,12))
                            ,BRANCH_TEL = '" + radTextBox_Tel.Text + @"'
                            ,BRANCH_CONFIGCOMMISSION_OT = '" + radTextBox_ComOT.Text + @"'
                            ,BRANCH_CONFIGCOMMISSION_LOCATION = '" + radTextBox_ComLocation.Text + @"'
                            ,BRANCH_CONFIGCOMMISSION_PerAddON = '" + radTextBox_AddOnPer.Text + @"'
                            ,BRANCH_RouteSendCoin = '" + radDropDownList_ec.SelectedValue.ToString() + @"'
                           WHERE BRANCH_ID = '{2}'
                           ", GetLocation(radTextBox_Latitude), GetLocation(radTextBox_Longtitude), _pBranchID));
                }
                //SHOP_BRANCH_CONFIGD
                if (SystemClass.SystemComProgrammer == "1" || SystemClass.SystemDptID.Equals("D156"))
                {
                    sqlIn.Add(GetStringUpdateBranchConfig(radTextBox_Branch.Text.ToUpper()));
                }
                #endregion
            }
            else
            {
                sqlIn.Add(string.Format(@"UPDATE dbo.SHOP_BRANCH 
                            SET 
                            BRANCH_ADDR = '" + radTextBox_Address.Text + @"' 
                            ,BRANCH_ManagerID = '" + headID + @"'
                            ,BRANCH_ManagerName = '" + radLabel_ManagerName.Text + @"'
                            ,BRANCH_SIZE = '" + GetMultiDropDownListValue(radMultiColumnComboBox_Size) + @"'
                            ,BRANCH_MAXPEOPLE1 = '" + radTextBox_Max.Text.Trim() + @"'
                            ,BRANCH_MAXPEOPLE2 = '" + radTextBox_Min.Text.Trim() + @"'
                            ,BRANCH_TEL = '" + radTextBox_Tel.Text + @"'
                            ,BRANCH_CONFIGCOMMISSION_OT = '" + radTextBox_ComOT.Text + @"'
                            ,BRANCH_CONFIGCOMMISSION_LOCATION = '" + radTextBox_ComLocation.Text + @"'
                            ,BRANCH_CONFIGCOMMISSION_PerAddON = '" + radTextBox_AddOnPer.Text + @"'
                            ,BRANCH_RouteSendCoin = '" + radDropDownList_ec.SelectedValue.ToString() + @"'
                           WHERE BRANCH_ID = '{0}' ", _pBranchID));
            }
            return ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
        }
        private DataRowView GetDataRowView(GridViewRowInfo rowInfo)
        {
            if (rowInfo != null)
            {
                return rowInfo.DataBoundItem as DataRowView;
            }

            return null;
        }
        private string GetStringUpdateBranchConfig(string pBranchID)
        {
            string strUpdateConfig;
            string sql = $@"SELECT ISNULL(BRANCH_ID,'') AS BRANCH_ID FROM SHOP_BRANCH_CONFIGDB WITH(NOLOCK)
            WHERE BRANCH_ID = '{pBranchID}'  GROUP BY BRANCH_ID ";
            DataTable dtConfig = ConnectionClass.SelectSQL_Main(sql);
            if (dtConfig.Rows.Count > 0)
            {

                strUpdateConfig = string.Format(@"UPDATE dbo.SHOP_BRANCH_CONFIGDB 
                                SET 
                                  [BRANCH_CHANNEL] = '" + radTextBox_WH.Text + @"'
                                  ,[BRANCH_TAXNUMBER] = '" + radTextBox_TaxNumber.Text + @"'
                                  ,[BRANCH_SERVERNAME] = '" + radTextBox_Serverspc.Text + @"'
                                  ,[BRANCH_SERVER_IP] = '" + radTextBox_ServerIp.Text + @"'
                                  ,[BRANCH_DATABASENAME] = '" + radTextBox_DatabaseName.Text + @"'
                                  ,[BRANCH_PATHPOS] = '" + radTextBox_PathPos.Text + @"'
                                  ,[BRANCH_USERNAME] = '" + radTextBox_UserName.Text + @"'
                                  ,[BRANCH_PASSWORDNAME] = '" + radTextBox_Password.Text + @"'
                                  ,[BRANCH_SEQTAX] = '" + radTextBox_TaxLabel.Text + @"'
                                  ,[BRANCH_SMARTSHOP_LIMIT] = '" + radTextBox_SmartShop.Text + @"'
                                  ,[BRANCH_SendMoney] = '" + radTextBox_SendMoney.Text + @"'
                                  ,[BRANCH_FarmHouse] = '" + radTextBox_FarmHouse.Text + @"'
                                  ,[BRANCH_VERSIONUPDATEPDA] = '" + radTextBox_PDAversion.Text.Trim() + @"'
                                  ,[BRANCH_VERSIONUPDATEANDROID] = '" + radTextBox_Androidversion.Text.Trim() + @"'
                                  ,[BRANCH_STATUSSHELF] = '" + statusShelf + @"'
                                   WHERE BRANCH_ID = '{0}'", _pBranchID);
            }
            else
            {
                strUpdateConfig = string.Format(@"INSERT INTO dbo.SHOP_BRANCH_CONFIGDB     
                                 ([BRANCH_ID],
                                  [BRANCH_CHANNEL],
                                  [BRANCH_TAXNUMBER],
                                  [BRANCH_SERVERNAME],
                                  [BRANCH_SERVER_IP],
                                  [BRANCH_DATABASENAME],
                                  [BRANCH_USERNAME],
                                  [BRANCH_PATHPOS],  
                                  [BRANCH_PASSWORDNAME],
                                  [BRANCH_SEQTAX],
                                  [BRANCH_SMARTSHOP_LIMIT],
                                  [BRANCH_SendMoney],
                                  [BRANCH_FarmHouse],
                                  [BRANCH_VERSIONUPDATEPDA],
                                  [BRANCH_VERSIONUPDATEANDROID])
                                 VALUES('" + radTextBox_Branch.Text.Trim().ToUpper() + @"',
                                    '" + radTextBox_WH.Text + @"',
                                    '" + radTextBox_TaxNumber.Text + @"',
                                    '" + radTextBox_Serverspc.Text + @"',
                                    '" + radTextBox_ServerIp.Text + @"',
                                    '" + radTextBox_DatabaseName.Text + @"',
                                    '" + radTextBox_PathPos.Text + @"',
                                    '" + radTextBox_UserName.Text + @"',
                                    '" + radTextBox_Password.Text + @"',
                                    '" + radTextBox_TaxLabel.Text + @"',
                                    '" + radTextBox_SmartShop.Text + @"',
                                    '" + radTextBox_SendMoney.Text + @"',
                                    '" + radTextBox_FarmHouse.Text + @"',
                                    '" + radTextBox_PDAversion.Text.Trim() + @"',
                                    '" + radTextBox_Androidversion.Text.Trim() + @"'  )");

            }
            return strUpdateConfig;
        }
        private string GetStatusValue(RadDropDownList radDropDown)
        {
            if (radDropDown.SelectedIndex == -1) radDropDown.SelectedIndex = 0;

            return radDropDown.SelectedValue.ToString();
        }
        private string GetStatusOut()
        {
            if (radDropDownList_Out.SelectedIndex > -1) return radDropDownList_Out.SelectedValue.ToString();
            else return "1"; //ก่อสร้าง
        }

        private double GetLocation(RadTextBox radTextBox)
        {
            if (radTextBox.Text != "") return double.Parse(radTextBox.Text);
            else return 0;
        }

        private string GetDropDownListValue(RadDropDownList List)
        {
            if (List.SelectedValue == null) return "";
            else return List.SelectedValue.ToString();

        }
        private string GetMultiDropDownListValue(RadMultiColumnComboBox MultiComboBox)
        {
            if (MultiComboBox.SelectedValue == null) return "";
            else return MultiComboBox.SelectedValue.ToString();

        }
        #endregion
        #region"Event"
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Branch.Text) || string.IsNullOrEmpty(radTextBox_BranchName.Text))
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("รหัส หรือ ชื่อสาขา");
                radTextBox_Branch.Focus();
            }
            else if ((radTextBox_Tel.Text.Length > 0) && (radTextBox_Tel.Text.Length != 10))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เบอร์โทรประจำร้านจะต้องระบุข้อมูล 10 หลักเท่านั้น !");
                radTextBox_Tel.Focus();
                return;
            }
            else if (radTextBox_ComOT.Text.Length > 0)
            {
                if (Double.TryParse(radTextBox_ComOT.Text.ToString(), out Double _) == false)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ค่าคอม พนง. OT วันหยุด จะต้องเป็นตัวเลขเท่านั้น !");
                    radTextBox_Tel.Focus();
                    return;
                }
            }
            else if (radTextBox_ComLocation.Text.Length > 0)
            {
                if (Double.TryParse(radTextBox_ComLocation.Text.ToString(), out Double _) == false)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ค่าคอม เบี้ยเลี้ยง+ที่พัก จะต้องเป็นตัวเลขเท่านั้น !");
                    radTextBox_ComLocation.Focus();
                    return;
                }
            }
            else if (radTextBox_AddOnPer.Text.Length > 0)
            {
                if (Double.TryParse(radTextBox_AddOnPer.Text.ToString(), out Double _) == false)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ค่าคอม ค่ากันดาร จะต้องเป็นตัวเลขเท่านั้น !");
                    radTextBox_AddOnPer.Focus();
                    return;
                }
            }

            ReturnExecut = GetExecut();
            MsgBoxClass.MsgBoxShow_SaveStatus(ReturnExecut);
            this.DialogResult = DialogResult.OK;
            this.Close();


        }
        private void RadMultiColumnComboBox_Size_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radMultiColumnComboBox_Size.SelectedIndex == -1) return;

            DataRowView dataRow = this.GetDataRowView(this.radMultiColumnComboBox_Size.SelectedItem as GridViewDataRowInfo);
            if (dataRow != null) radLabel_SizeName.Text = dataRow.Row.ItemArray[2].ToString();
            else radLabel_SizeName.Text = "";
        }
        private void RadTextBox_Head_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(radTextBox_Head.Text))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสพนักงาน...");
                    radTextBox_Head.Focus(); radTextBox_Head.Text = "";
                    radLabel_ManagerName.Text = "";
                }
                else
                {
                    if (radTextBox_Head.Text.Length == 7) CheckName();
                    Key_Enter(sender, e);
                }
            }
        }
        void CheckName()
        {
            string strName = "";
            DataTable dt = Models.EmplClass.GetEmployee_Altnum(radTextBox_Head.Text.Trim());
            if (dt.Rows.Count > 0) strName = dt.Rows[0]["SPC_NAME"].ToString() + "(" + dt.Rows[0]["IVZ_HRPANickName"].ToString() + ")";
            radLabel_ManagerName.Text = strName;

            if (strName == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบพนักงาน เช็คใหม่อีกครั้ง");
                radTextBox_Head.Focus(); radTextBox_Head.Text = "";
                radLabel_ManagerName.Text = "";
            }
            //else
            //{
            //    //radLabel_ManagerName.Text = radLabel_ManagerName.Text.Substring(4, radLabel_ManagerName.Text.Length - 4);
            //    Key_Enter(sender, e);
            //}
        }
        private void RadButton_cancle_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        private void RadTextBox_SmartShop_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void RadTextBox_SendMoney_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void RadTextBox_Max_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void RadTextBox_Min_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        #endregion
        #region PLAN
        private void RadButton_Upload_Click(object sender, EventArgs e)
        {
            this.radOpenFileDialog1.InitialDirectory = this.radOpenFolderDialog1.FileName;
            this.radOpenFileDialog1.FileName = string.Empty;
            if (this.radOpenFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = Cursors.WaitCursor;
                string fileName = this.radOpenFileDialog1.FileName;
                this.UploadFile(fileName);
                this.Cursor = Cursors.Default;
            }
        }
        private void UploadFile(string fileName)
        {
            string sourceFile = fileName;
            string lastFile = fileName.Substring(fileName.LastIndexOf(@"."), 4);
            string destinationFile = PathImageClass.pPathPLANMN + _pBranchID + "\\" + _pBranchID + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + DateTime.Now.ToString("HHmmss") + "-" + SystemClass.SystemUserID + lastFile;
            try
            {
                File.Copy(sourceFile, destinationFile, true);
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("Update เรียบร้อย");
                GetImg();
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Update ได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
            }
        }
        private void GetImg()
        {
            string PathImage = PathImageClass.pPathPLANMN + _pBranchID;
            if (!Directory.Exists(PathImage)) Directory.CreateDirectory(PathImage);

            // Try to create the directory.
            DirectoryInfo DirInfo = new DirectoryInfo(PathImage);
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(string.Format(@"{0}*.JPG", _pBranchID), SearchOption.AllDirectories);
                List<string> listImg = new List<string>();
                if (Files.Length > 0)
                {
                    foreach (var l in Files)
                    {
                        listImg.Add(l.FullName);
                    }
                    ListBoxshow(listImg);
                }
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error(ex.Message);
            }
        }
        private void ListBoxshow(List<string> list)
        {
            if (datagrid.Columns.Count == 0)
            {
                datagrid.Columns.Add("Filename", typeof(string));
                datagrid.Columns.Add("Watch", typeof(string));
                datagrid.Columns.Add("Delete", typeof(string));
                datagrid.Columns.Add("Filefull", typeof(string));
            }
            if (datagrid.Rows.Count > 0)
            {
                datagrid.Rows.Clear();
            }

            foreach (var l in list)
            {
                datagrid.Rows.Add(l.Substring(l.LastIndexOf(@"-") - 2, 14), "ดู", "ลบ", l);
            }

            RadGridView_Show.DataSource = datagrid;
        }
        private GridViewDataColumn AppendNewColumn(string columnType, string fieldName, string labelName, int _width)
        {
            GridViewDataColumn newColumn = null;
            switch (columnType)
            {
                case "Text":
                    newColumn = new GridViewTextBoxColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        HeaderTextAlignment = ContentAlignment.MiddleLeft,
                        TextAlignment = ContentAlignment.MiddleLeft,
                        Width = _width,
                        WrapText = true
                    };
                    break;
                case "Command":
                    newColumn = new GridViewCommandColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        HeaderTextAlignment = ContentAlignment.MiddleCenter,
                        TextAlignment = ContentAlignment.MiddleCenter,
                        Width = _width,
                        WrapText = true
                    };
                    break;
            }
            return newColumn;
        }
        #endregion
        //#region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == RadGridView_Show.Columns[2] && e.RowIndex > -1)
            {
                try
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการลบข้อมูล ? " + RadGridView_Show.CurrentRow.Cells["Filename"].Value.ToString()) == DialogResult.Yes)
                    {
                        File.Delete(RadGridView_Show.CurrentRow.Cells["Filefull"].Value.ToString());
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ลบรูปเรียบร้อย");
                        GetImg();
                    }
                }
                catch (Exception)
                {
                    return;
                }
            }

            if (e.Column == RadGridView_Show.Columns[1] && e.RowIndex > -1)
            {
                if (RadGridView_Show.CurrentRow.Cells["Filefull"].Value.ToString() == null) return;
                else System.Diagnostics.Process.Start(RadGridView_Show.CurrentRow.Cells["Filefull"].Value.ToString());

            }
        }
        //สาขาที่บังคับชั้นวาง
        private void RadCheckBox_StatusShelf_CheckStateChanged(object sender, EventArgs e)
        {
            var myValue = Convert.ToBoolean(Convert.ToInt32(statusShelf));
            if (!myValue == radCheckBox_StatusShelf.Checked)
            {
                if (radCheckBox_StatusShelf.Checked == true)
                {
                    statusShelf = "1";
                    string returnExecut = BranchClass.Update_BRANCH_CONFIGDB("3", radTextBox_Branch.Text.Trim(), statusShelf);
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExecut);
                    if (!returnExecut.Equals("")) radCheckBox_StatusShelf.CheckState = CheckState.Unchecked;
                }
                else
                {
                    statusShelf = "0";
                    string returnExecut = BranchClass.Update_BRANCH_CONFIGDB("3", radTextBox_Branch.Text.Trim(), statusShelf);
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExecut);
                    if (!returnExecut.Equals("")) radCheckBox_StatusShelf.CheckState = CheckState.Checked;
                }


            }
        }
        //Update Vender beer
        private void RadDropDownList_VenderLeo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radTextBox_Branch.Text == "") return;
            if (venderLeo == radDropDownList_VenderLeo.SelectedValue.ToString()) return;

            string res = BranchClass.Update_BRANCH_CONFIGDB("1", radTextBox_Branch.Text, radDropDownList_VenderLeo.SelectedValue.ToString());
            MsgBoxClass.MsgBoxShow_SaveStatus(res);
            if (res == "") venderLeo = radDropDownList_VenderLeo.SelectedValue.ToString();
        }

        private void RadTextBox_ComOT_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadTextBox_ComLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadTextBox_Head_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadTextBox_Head_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_Head.Text.ToString().Length != 7) radLabel_ManagerName.Text = ""; else CheckName();
        }

        private void RadTextBox_AddOnPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        //Update Route Beer
        private void RadDropDownList_Beer_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radTextBox_Branch.Text == "") return;
            if (routeBeer == radDropDownList_Beer.SelectedValue.ToString()) return;
            string res = BranchClass.Update_BRANCH_CONFIGDB("0", radTextBox_Branch.Text, radDropDownList_Beer.SelectedValue.ToString());
            MsgBoxClass.MsgBoxShow_SaveStatus(res);
            if (res == "") routeBeer = radDropDownList_Beer.SelectedValue.ToString();
        }
        //FreeItem CashDesktop
        private void RadCheckBox_FreeItem_CheckStateChanged(object sender, EventArgs e)
        {
            var myValue = Convert.ToBoolean(Convert.ToInt32(statusFreeItem));
            if (!myValue == radCheckBox_FreeItem.Checked)
            {
                statusFreeItem = "0";
                if (radCheckBox_FreeItem.Checked == true) statusFreeItem = "1";
                string returnExecut = BranchClass.Update_BRANCH_CONFIGDB("2", radTextBox_Branch.Text.Trim(), statusFreeItem); //ConnectionClass.ExecuteSQL_Main(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExecut);
                if (!returnExecut.Equals(""))
                {
                    if (statusFreeItem == "0") radCheckBox_FreeItem.CheckState = CheckState.Checked;
                    else radCheckBox_FreeItem.CheckState = CheckState.Unchecked;
                }
            }
        }
    }
}
