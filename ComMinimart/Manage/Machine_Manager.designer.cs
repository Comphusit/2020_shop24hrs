﻿namespace PC_Shop24Hrs.ComMinimart.Manage
{
    partial class Machine_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Machine_Manager));
            this.radPanel_Information = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_sups = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_sup = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_PriceGroup = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Channel = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_PriceGroup = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Channel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Label = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radCheckedDropDownList_Program = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radRadioButton_AllBranch = new Telerik.WinControls.UI.RadRadioButton();
            this.radDropDown_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDown_Dpt = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Spc = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Branch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radRadioButton_Branch = new Telerik.WinControls.UI.RadRadioButton();
            this.radTextBox_Spc = new Telerik.WinControls.UI.RadTextBox();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBar = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Refresh = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Delet = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Information)).BeginInit();
            this.radPanel_Information.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_sups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_sup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_PriceGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Channel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PriceGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Channel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownList_Program)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_AllBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Spc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Spc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // radPanel_Information
            // 
            this.radPanel_Information.Controls.Add(this.radLabel_sups);
            this.radPanel_Information.Controls.Add(this.radStatusStrip);
            this.radPanel_Information.Controls.Add(this.radLabel_sup);
            this.radPanel_Information.Controls.Add(this.radDropDownList_PriceGroup);
            this.radPanel_Information.Controls.Add(this.radDropDownList_Channel);
            this.radPanel_Information.Controls.Add(this.radLabel_PriceGroup);
            this.radPanel_Information.Controls.Add(this.radLabel_Channel);
            this.radPanel_Information.Controls.Add(this.radLabel_Label);
            this.radPanel_Information.Controls.Add(this.radLabel_Dept);
            this.radPanel_Information.Controls.Add(this.radCheckedDropDownList_Program);
            this.radPanel_Information.Controls.Add(this.radButton_Cancel);
            this.radPanel_Information.Controls.Add(this.radButton_Save);
            this.radPanel_Information.Controls.Add(this.radRadioButton_AllBranch);
            this.radPanel_Information.Controls.Add(this.radDropDown_Branch);
            this.radPanel_Information.Controls.Add(this.radDropDown_Dpt);
            this.radPanel_Information.Controls.Add(this.radLabel_Spc);
            this.radPanel_Information.Controls.Add(this.radLabel_Branch);
            this.radPanel_Information.Controls.Add(this.radLabel1);
            this.radPanel_Information.Controls.Add(this.radRadioButton_Branch);
            this.radPanel_Information.Controls.Add(this.radTextBox_Spc);
            this.radPanel_Information.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel_Information.Location = new System.Drawing.Point(587, 3);
            this.radPanel_Information.Name = "radPanel_Information";
            this.radPanel_Information.Size = new System.Drawing.Size(194, 555);
            this.radPanel_Information.TabIndex = 50;
            // 
            // radLabel_sups
            // 
            this.radLabel_sups.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_sups.ForeColor = System.Drawing.Color.Green;
            this.radLabel_sups.Location = new System.Drawing.Point(79, 354);
            this.radLabel_sups.Name = "radLabel_sups";
            this.radLabel_sups.Size = new System.Drawing.Size(98, 19);
            this.radLabel_sups.TabIndex = 47;
            this.radLabel_sups.Text = "[Supc_support]";
            // 
            // radLabel_sup
            // 
            this.radLabel_sup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_sup.ForeColor = System.Drawing.Color.Green;
            this.radLabel_sup.Location = new System.Drawing.Point(47, 303);
            this.radLabel_sup.Name = "radLabel_sup";
            this.radLabel_sup.Size = new System.Drawing.Size(98, 19);
            this.radLabel_sup.TabIndex = 46;
            this.radLabel_sup.Text = "[Supc_support]";
            // 
            // radDropDownList_PriceGroup
            // 
            this.radDropDownList_PriceGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_PriceGroup.DropDownAnimationEnabled = false;
            this.radDropDownList_PriceGroup.DropDownHeight = 124;
            this.radDropDownList_PriceGroup.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_PriceGroup.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_PriceGroup.Enabled = false;
            this.radDropDownList_PriceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_PriceGroup.Location = new System.Drawing.Point(12, 379);
            this.radDropDownList_PriceGroup.Name = "radDropDownList_PriceGroup";
            this.radDropDownList_PriceGroup.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_PriceGroup.TabIndex = 45;
            // 
            // radDropDownList_Channel
            // 
            this.radDropDownList_Channel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Channel.DropDownAnimationEnabled = false;
            this.radDropDownList_Channel.DropDownHeight = 124;
            this.radDropDownList_Channel.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_Channel.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Channel.Enabled = false;
            this.radDropDownList_Channel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Channel.Location = new System.Drawing.Point(12, 327);
            this.radDropDownList_Channel.Name = "radDropDownList_Channel";
            this.radDropDownList_Channel.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Channel.TabIndex = 44;
            // 
            // radLabel_PriceGroup
            // 
            this.radLabel_PriceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_PriceGroup.Location = new System.Drawing.Point(11, 354);
            this.radLabel_PriceGroup.Name = "radLabel_PriceGroup";
            this.radLabel_PriceGroup.Size = new System.Drawing.Size(62, 19);
            this.radLabel_PriceGroup.TabIndex = 43;
            this.radLabel_PriceGroup.Text = "ระดับราคา";
            // 
            // radLabel_Channel
            // 
            this.radLabel_Channel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Channel.Location = new System.Drawing.Point(11, 303);
            this.radLabel_Channel.Name = "radLabel_Channel";
            this.radLabel_Channel.Size = new System.Drawing.Size(30, 19);
            this.radLabel_Channel.TabIndex = 41;
            this.radLabel_Channel.Text = "คลัง";
            // 
            // radLabel_Label
            // 
            this.radLabel_Label.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Label.ForeColor = System.Drawing.Color.Green;
            this.radLabel_Label.Location = new System.Drawing.Point(130, 49);
            this.radLabel_Label.Name = "radLabel_Label";
            this.radLabel_Label.Size = new System.Drawing.Size(38, 19);
            this.radLabel_Label.TabIndex = 39;
            this.radLabel_Label.Text = "แก้ไข";
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Dept.Location = new System.Drawing.Point(12, 189);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(39, 19);
            this.radLabel_Dept.TabIndex = 38;
            this.radLabel_Dept.Text = "แผนก";
            // 
            // radCheckedDropDownList_Program
            // 
            this.radCheckedDropDownList_Program.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckedDropDownList_Program.Location = new System.Drawing.Point(11, 277);
            this.radCheckedDropDownList_Program.Name = "radCheckedDropDownList_Program";
            this.radCheckedDropDownList_Program.Size = new System.Drawing.Size(175, 20);
            this.radCheckedDropDownList_Program.TabIndex = 4;
            this.radCheckedDropDownList_Program.ItemCheckedChanged += new Telerik.WinControls.UI.RadCheckedListDataItemEventHandler(this.RadCheckedDropDownList_Program_ItemCheckedChanged);
            this.radCheckedDropDownList_Program.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.Location = new System.Drawing.Point(100, 430);
            this.radButton_Cancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            this.radButton_Cancel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Location = new System.Drawing.Point(3, 430);
            this.radButton_Save.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            this.radButton_Save.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radRadioButton_AllBranch
            // 
            this.radRadioButton_AllBranch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_AllBranch.Location = new System.Drawing.Point(116, 135);
            this.radRadioButton_AllBranch.Name = "radRadioButton_AllBranch";
            this.radRadioButton_AllBranch.Size = new System.Drawing.Size(71, 19);
            this.radRadioButton_AllBranch.TabIndex = 5;
            this.radRadioButton_AllBranch.TabStop = false;
            this.radRadioButton_AllBranch.Text = "ทุกแผนก";
            this.radRadioButton_AllBranch.Visible = false;
            this.radRadioButton_AllBranch.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Dpt_ToggleStateChanged);
            // 
            // radDropDown_Branch
            // 
            this.radDropDown_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Branch.DropDownAnimationEnabled = false;
            this.radDropDown_Branch.DropDownHeight = 124;
            this.radDropDown_Branch.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDown_Branch.Location = new System.Drawing.Point(12, 161);
            this.radDropDown_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDown_Branch.Name = "radDropDown_Branch";
            this.radDropDown_Branch.Size = new System.Drawing.Size(175, 21);
            this.radDropDown_Branch.TabIndex = 2;
            this.radDropDown_Branch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radDropDown_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDown_Branch_SelectedValueChanged);
            // 
            // radDropDown_Dpt
            // 
            this.radDropDown_Dpt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDown_Dpt.DropDownAnimationEnabled = false;
            this.radDropDown_Dpt.DropDownHeight = 124;
            this.radDropDown_Dpt.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDown_Dpt.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDown_Dpt.Enabled = false;
            this.radDropDown_Dpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDown_Dpt.Location = new System.Drawing.Point(12, 214);
            this.radDropDown_Dpt.Name = "radDropDown_Dpt";
            this.radDropDown_Dpt.Size = new System.Drawing.Size(175, 21);
            this.radDropDown_Dpt.TabIndex = 3;
            this.radDropDown_Dpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radDropDown_Dpt.SelectedValueChanged += new System.EventHandler(this.RadDropDown_Dpt_SelectedValueChanged);
            // 
            // radLabel_Spc
            // 
            this.radLabel_Spc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Spc.Location = new System.Drawing.Point(12, 49);
            this.radLabel_Spc.Name = "radLabel_Spc";
            this.radLabel_Spc.Size = new System.Drawing.Size(56, 19);
            this.radLabel_Spc.TabIndex = 17;
            this.radLabel_Spc.Text = "ชื่อเครื่อง";
            // 
            // radLabel_Branch
            // 
            this.radLabel_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Branch.Location = new System.Drawing.Point(8, 108);
            this.radLabel_Branch.Name = "radLabel_Branch";
            this.radLabel_Branch.Size = new System.Drawing.Size(81, 19);
            this.radLabel_Branch.TabIndex = 1;
            this.radLabel_Branch.Text = "สาขาที่ใช้งาน";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(11, 252);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(93, 19);
            this.radLabel1.TabIndex = 35;
            this.radLabel1.Text = "โปรแกรมใช้งาน";
            // 
            // radRadioButton_Branch
            // 
            this.radRadioButton_Branch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Branch.Location = new System.Drawing.Point(11, 135);
            this.radRadioButton_Branch.Name = "radRadioButton_Branch";
            this.radRadioButton_Branch.Size = new System.Drawing.Size(79, 19);
            this.radRadioButton_Branch.TabIndex = 1;
            this.radRadioButton_Branch.Text = "เลือกสาขา";
            this.radRadioButton_Branch.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Branch.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Branch_ToggleStateChanged);
            this.radRadioButton_Branch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radTextBox_Spc
            // 
            this.radTextBox_Spc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Spc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Spc.Location = new System.Drawing.Point(12, 72);
            this.radTextBox_Spc.MaxLength = 12;
            this.radTextBox_Spc.Name = "radTextBox_Spc";
            this.radTextBox_Spc.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Spc.TabIndex = 0;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.BackColor = System.Drawing.SystemColors.Control;
            this.radGridView_Show.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.ForeColor = System.Drawing.Color.Black;
            this.radGridView_Show.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.AllowColumnReorder = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            this.radGridView_Show.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.EnableHighlight = false;
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(216)))), ((int)(((byte)(221)))));
            this.radGridView_Show.Size = new System.Drawing.Size(572, 549);
            this.radGridView_Show.TabIndex = 1;
            this.radGridView_Show.TitlePosition = Telerik.WinControls.Layouts.Dock.Left;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.radButton_Add,
            this.commandBar,
            this.radButton_Edit,
            this.commandBarSeparator3,
            this.radButtonElement_Refresh,
            this.commandBarSeparator1,
            this.radButton_Delet,
            this.commandBarSeparator4});
            this.radStatusStrip.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(194, 43);
            this.radStatusStrip.TabIndex = 0;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).AutoSize = true;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Alignment = System.Drawing.ContentAlignment.BottomRight;
            ((Telerik.WinControls.UI.RadGripElement)(this.radStatusStrip.GetChildAt(0).GetChildAt(4))).Enabled = false;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButton_Add
            // 
            this.radButton_Add.AutoSize = true;
            this.radButton_Add.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Add, false);
            this.radButton_Add.Text = "เพิ่ม";
            this.radButton_Add.ToolTipText = "เพิ่ม";
            this.radButton_Add.UseCompatibleTextRendering = false;
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // commandBar
            // 
            this.commandBar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBar.Name = "commandBar";
            this.radStatusStrip.SetSpring(this.commandBar, false);
            this.commandBar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBar.UseCompatibleTextRendering = false;
            this.commandBar.VisibleInOverflowMenu = false;
            // 
            // radButton_Edit
            // 
            this.radButton_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButton_Edit.Name = "radButton_Edit";
            this.radStatusStrip.SetSpring(this.radButton_Edit, false);
            this.radButton_Edit.Text = "radButtonElement1";
            this.radButton_Edit.ToolTipText = "แก้ไข";
            this.radButton_Edit.UseCompatibleTextRendering = false;
            this.radButton_Edit.Click += new System.EventHandler(this.RadButton_Edit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Refresh
            // 
            this.radButtonElement_Refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButtonElement_Refresh.Name = "radButtonElement_Refresh";
            this.radStatusStrip.SetSpring(this.radButtonElement_Refresh, false);
            this.radButtonElement_Refresh.Text = "radButtonElement1";
            this.radButtonElement_Refresh.ToolTipText = "refresh";
            this.radButtonElement_Refresh.Click += new System.EventHandler(this.RadButtonElement_Refresh_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButton_Delet
            // 
            this.radButton_Delet.AutoSize = true;
            this.radButton_Delet.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Delet.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Delet.Name = "radButton_Delet";
            this.radButton_Delet.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Delet, false);
            this.radButton_Delet.Text = "ลบ";
            this.radButton_Delet.ToolTipText = "ลบ";
            this.radButton_Delet.UseCompatibleTextRendering = false;
            this.radButton_Delet.Click += new System.EventHandler(this.RadButton_Delet_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radPanel_Information, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel3.TabIndex = 19;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(578, 555);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // Machine_Manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Machine_Manager";
            this.Text = "Manchine_Manager";
            this.Load += new System.EventHandler(this.Machine_Manager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Information)).EndInit();
            this.radPanel_Information.ResumeLayout(false);
            this.radPanel_Information.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_sups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_sup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_PriceGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Channel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PriceGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Channel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownList_Program)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_AllBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDown_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Spc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Spc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadPanel radPanel_Information;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Spc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Spc;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Branch;
        private Telerik.WinControls.UI.RadLabel radLabel_Branch;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButton_Add;
        private Telerik.WinControls.UI.RadButtonElement radButton_Delet;
        private Telerik.WinControls.UI.CommandBarSeparator commandBar;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Dpt;
        private Telerik.WinControls.UI.RadDropDownList radDropDown_Branch;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButton_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_AllBranch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadCheckedDropDownList radCheckedDropDownList_Program;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel_Label;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Refresh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadLabel radLabel_PriceGroup;
        private Telerik.WinControls.UI.RadLabel radLabel_Channel;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_PriceGroup;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Channel;
        private Telerik.WinControls.UI.RadLabel radLabel_sups;
        private Telerik.WinControls.UI.RadLabel radLabel_sup;
    }
}