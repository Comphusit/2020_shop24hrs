﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.ComMinimart
{
    public partial class Config_Server : Telerik.WinControls.UI.RadForm
    {

        string _pType;//0 Insert 1 Edit
        readonly string _pIP;// 

        string typeServer;
        string typeDesc;

        string vmTypeSave;
        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        public Config_Server(string pIP)//0 Insert 1 Edit
        {
            InitializeComponent();
            _pIP = pIP;
            if (pIP == "") { _pType = "0"; typeDesc = "บันทึก"; } else { _pType = "1"; typeDesc = "แก้ไข"; }
        }

        //Load
        private void Config_Server_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            DatagridClass.SetDefaultFontDropDownShot(RadDropDownList_Remote);

            radButton_addVm.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_cancleVM.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_saveVM.ButtonElement.ShowBorder = true;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CLIENT_IP", "IP", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CLIENT_NAME", "Server", 160));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CLIENT_PROGRAM", "โปรแกรม", 330));
            radGridView_Show.EnableFiltering = false;

            radLabel_Detail.Text = "กด + เพื่อแก้ไขข้อมูล | DoubleClik รายการ >> แก้ไข | กด F2 ที่รายการ >> ลบ";

            ClearTxt();
            SetRemote();

            radTextBox_ip.Focus();

        }
        //SetComBo Remote
        void SetRemote()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("DESC");

            dt.Rows.Add("VMWARE/Browser", "VMWARE/Browser");
            dt.Rows.Add("VMWARE/Hyper-V", "VMWARE/Hyper-V");
            dt.Rows.Add("VMWARE/vSphere5", "VMWARE/vSphere5");
            dt.Rows.Add("VMWARE/vSphere6", "VMWARE/vSphere6");
            dt.Rows.Add("WINDOWS", "WINDOWS");

            RadDropDownList_Remote.DataSource = dt;
            RadDropDownList_Remote.ValueMember = "ID";
            RadDropDownList_Remote.DisplayMember = "DESC";

            if (_pIP != "") RadDropDownList_Remote.SelectedValue = typeServer;
        }

        //Clear Txt
        void ClearTxt()
        {
            radGridView_Show.DataSource = null;
            radTextBox_ip.Text = ""; radTextBox_ip.Focus();
            radTextBox_Name.Text = ""; radTextBox_Name.Enabled = false;
            radTextBox_ilo.Text = ""; radTextBox_ilo.Enabled = false;
            radTextBox_User.Text = ""; radTextBox_User.Enabled = false;
            radTextBox_Pass.Text = ""; radTextBox_Pass.Enabled = false;
            radTextBox_Location.Text = ""; radTextBox_Location.Enabled = false;
            radTextBox_remark.Text = ""; radTextBox_remark.Enabled = false;
            radTextBox_vmip.Text = ""; radTextBox_vmip.Enabled = false;
            radTextBox_vmname.Text = ""; radTextBox_vmname.Enabled = false;
            radTextBox_vmprogram.Text = ""; radTextBox_vmprogram.Enabled = false;
            radButton_Save.Enabled = false;
            radButton_saveVM.Enabled = false; radButton_cancleVM.Enabled = false;

            radButton_Save.Text = typeDesc;
            if (_pType == "1")
            {
                LoadServer();
                LoadClient();
            }
        }
        void LoadServer()
        {
            string str = $@"
                SELECT  [SERVER_IP],[SERVER_NAME],[SERVER_ILO],[SERVER_TYPE],
		                [SERVER_USER],[SERVER_PASSWORD],[SERVER_LOCATION],[SERVER_REMARK]
                FROM	[SHOP_SERVER] WITH (NOLOCK)
                WHERE	[SERVER_IP] = '{_pIP}'
                ORDER BY SERVER_IP ";
            DataTable dt = ConnectionClass.SelectSQL_Main(str);
            radTextBox_ip.Text = dt.Rows[0]["SERVER_IP"].ToString(); radTextBox_ip.Enabled = false;
            radTextBox_Name.Text = dt.Rows[0]["SERVER_NAME"].ToString(); radTextBox_Name.Enabled = true; radTextBox_Name.Focus();
            radTextBox_ilo.Text = dt.Rows[0]["SERVER_ILO"].ToString(); radTextBox_ilo.Enabled = true;
            radTextBox_User.Text = dt.Rows[0]["SERVER_USER"].ToString(); radTextBox_User.Enabled = true;
            radTextBox_Pass.Text = dt.Rows[0]["SERVER_PASSWORD"].ToString(); radTextBox_Pass.Enabled = true;
            radTextBox_Location.Text = dt.Rows[0]["SERVER_LOCATION"].ToString(); radTextBox_Location.Enabled = true;
            radTextBox_remark.Text = dt.Rows[0]["SERVER_REMARK"].ToString(); radTextBox_remark.Enabled = true;
            typeServer = dt.Rows[0]["SERVER_TYPE"].ToString();
            radButton_Save.Enabled = true;
        }
        //Loadclient
        void LoadClient()
        {
            string sql = $@"
                SELECT	[CLIENT_IP],[CLIENT_NAME],[CLIENT_PROGRAM]
                FROM	SHOP_SERVER_CLIENT	WITH (NOLOCK)
                WHERE	[SERVER_IP] = '{radTextBox_ip.Text.Trim()}' ";
            radGridView_Show.DataSource = ConnectionClass.SelectSQL_Main(sql);
        }

        #region "SetFocutEnter"
        void SetFocus(KeyEventArgs e, RadTextBox radTextBoxCheck, RadTextBox radTextBoxDesc, string Desc)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBoxCheck.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"บังคับระบุข้อมูล {Desc} เท่านั้น");
                    radTextBoxCheck.Focus();
                }
                else
                {
                    radTextBoxDesc.Enabled = true;
                    radTextBoxDesc.Focus();
                }
            }
        }

        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            SetFocus(e, radTextBox_Name, radTextBox_ilo, "Server Name");
        }

        private void RadTextBox_ilo_KeyDown(object sender, KeyEventArgs e)
        {
            SetFocus(e, radTextBox_ilo, radTextBox_User, "Server iL");
        }

        private void RadTextBox_User_KeyDown(object sender, KeyEventArgs e)
        {
            SetFocus(e, radTextBox_User, radTextBox_Pass, "User เข้า Server");
        }

        private void RadTextBox_Pass_KeyDown(object sender, KeyEventArgs e)
        {
            SetFocus(e, radTextBox_Pass, radTextBox_Location, "PassWord เข้า Server");
        }

        private void RadTextBox_Location_KeyDown(object sender, KeyEventArgs e)
        {
            SetFocus(e, radTextBox_Location, radTextBox_remark, "ที่ตั้ง");
        }

        private void RadTextBox_remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Save.Focus();
        }

        //IP Enter
        private void RadTextBox_ip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_ip.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("บังคับระบุข้อมูล SERVER IP เท่านั้น");
                    radTextBox_ip.Focus();
                }
                else
                {
                    radTextBox_ip.Enabled = false;
                    radTextBox_Name.Enabled = true;
                    radTextBox_Name.Focus();

                    radTextBox_ilo.Enabled = true;
                    radTextBox_User.Enabled = true;
                    radTextBox_Pass.Enabled = true;
                    radTextBox_Location.Enabled = true;
                    radTextBox_remark.Enabled = true;
                    radButton_Save.Enabled = true;
                }
            }
        }

        private void RadTextBox_vmip_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_vmip.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("บังคับระบุข้อมูล CLIENT IP เท่านั้น");
                    radTextBox_vmip.Focus();
                }
                else
                {
                    radTextBox_vmip.Enabled = false;
                    radTextBox_vmname.Enabled = true; radTextBox_vmname.Focus();
                    radTextBox_vmprogram.Enabled = true;
                    radButton_saveVM.Enabled = true;
                }
            }
        }
        private void RadTextBox_vmname_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_vmname.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("บังคับระบุข้อมูล Client Name เท่านั้น");
                    radTextBox_vmname.Focus();
                }
                else radTextBox_vmprogram.Focus();
            }
        }
        private void RadTextBox_vmprogram_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_vmprogram.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("บังคับระบุข้อมูล Program ที่ใช้งาน เท่านั้น");
                    radTextBox_vmprogram.Focus();
                }
                else radButton_saveVM.Focus();
            }
        }

        #endregion

        //Save ServerMain
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_Name.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล Server Name ให้เรียบร้อยก่อน{typeDesc}"); radTextBox_Name.Focus(); return; }
            if (radTextBox_ilo.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล Server iLO ให้เรียบร้อยก่อน{typeDesc}"); radTextBox_ilo.Focus(); return; }
            if (radTextBox_User.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล User เข้า Server ให้เรียบร้อยก่อน{typeDesc}"); radTextBox_User.Focus(); return; }
            if (radTextBox_Pass.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล PassWord เข้า Server ให้เรียบร้อยก่อน{typeDesc}"); radTextBox_Pass.Focus(); return; }
            if (radTextBox_Location.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล ที่ตั้ง ให้เรียบร้อยก่อน{typeDesc}"); radTextBox_Location.Focus(); return; }

            if (_pType == "0")
            {
                string sqlInsert = $@"
                INSERT INTO [dbo].[SHOP_SERVER]
                           ([SERVER_IP],[SERVER_NAME],[SERVER_ILO]
                           ,[SERVER_TYPE],[SERVER_USER],[SERVER_PASSWORD]
                           ,[SERVER_LOCATION],[SERVER_REMARK],[WHOIDINS],[WHONAMEINS])
                VALUES ('{radTextBox_ip.Text.Trim()}','{radTextBox_Name.Text.Trim()}','{radTextBox_ilo.Text.Trim()}',
                        '{RadDropDownList_Remote.SelectedValue}','{radTextBox_User.Text.Trim()}','{radTextBox_Pass.Text.Trim()}',
                        '{ConfigClass.ChecKStringForImport(radTextBox_Location.Text.Trim())}','{ConfigClass.ChecKStringForImport(radTextBox_remark.Text.Trim())}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')
                ";
                string resualt = ConnectionClass.ExecuteSQL_Main(sqlInsert);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                if (resualt == "")
                {
                    _pType = "1"; typeDesc = "แก้ไข";
                    radButton_Save.Text = typeDesc;
                    vmTypeSave = "0"; radTextBox_vmip.Enabled = true; radTextBox_vmip.Focus();
                }
            }
            else
            {
                string upStr = $@"
                UPDATE  SHOP_SERVER
                SET     [SERVER_NAME] = '{radTextBox_Name.Text.Trim()}',[SERVER_ILO] = '{radTextBox_ilo.Text.Trim()}',
                        [SERVER_TYPE] = '{RadDropDownList_Remote.SelectedValue}',[SERVER_USER] = '{radTextBox_User.Text.Trim()}',[SERVER_PASSWORD] = '{radTextBox_Pass.Text.Trim()}',
                        [SERVER_LOCATION] = '{ConfigClass.ChecKStringForImport(radTextBox_Location.Text.Trim())}',[SERVER_REMARK] = '{ConfigClass.ChecKStringForImport(radTextBox_remark.Text.Trim())}',
                        [WHOIDUPD] = '{SystemClass.SystemUserID}',[WHONAMEUPD] = '{SystemClass.SystemUserName}',[DATETIMEUPD] = GETDATE()
                WHERE   [SERVER_IP] = '{radTextBox_ip.Text.Trim()}' ";
                string resualt = ConnectionClass.ExecuteSQL_Main(upStr);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            }

        }
        //Client
        private void RadButton_addVm_Click(object sender, EventArgs e)
        {
            if (radTextBox_ip.Text.Trim() == "") return;
            vmTypeSave = "0";
            radTextBox_vmip.Enabled = true; radTextBox_vmip.Focus();
        }

        #region "Client"


        //Save Client
        private void RadButton_saveVM_Click(object sender, EventArgs e)
        {
            string desc = "บันทึก";
            if (vmTypeSave == "1") desc = "แก้ไข";
            if (radTextBox_vmip.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล CLIENT IP ให้เรียบร้อยก่อน{desc}"); radTextBox_vmip.Focus(); return; }
            if (radTextBox_vmname.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล CLIENT Name ให้เรียบร้อยก่อน{desc}"); radTextBox_vmname.Focus(); return; }
            if (radTextBox_vmprogram.Text.Trim() == "") { MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุข้อมูล Program ที่ใช้งาน ให้เรียบร้อยก่อน{desc}"); radTextBox_vmprogram.Focus(); return; }

            string sql = "";
            if (vmTypeSave == "0")//add
            {
                sql = $@"
                 INSERT INTO [dbo].[SHOP_SERVER_CLIENT]
                           ([SERVER_IP],[CLIENT_IP]
                           ,[CLIENT_NAME],[CLIENT_PROGRAM]
                           ,[WHOIDINS],[WHONAMEINS])
                VALUES  ('{radTextBox_ip.Text.Trim()}','{radTextBox_vmip.Text.Trim()}',
                         '{radTextBox_vmname.Text.Trim()}','{radTextBox_vmprogram.Text.Trim()}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}') ";
            }

            if (vmTypeSave == "1")//edit
            {
                sql = $@"
                UPDATE  SHOP_SERVER_CLIENT
                SET     CLIENT_NAME = '{radTextBox_vmname.Text.Trim()}',CLIENT_PROGRAM = '{radTextBox_vmprogram.Text.Trim()}',WHOIDUPD = '{SystemClass.SystemUserID}',WHONAMEUPD = '{SystemClass.SystemUserName}',DATETIMEUPD = GETDATE()
                WHERE   SERVER_IP = '{radTextBox_ip.Text.Trim()}' AND CLIENT_IP = '{radTextBox_vmip.Text.Trim()}' ";
            }

            string resualt = ConnectionClass.ExecuteSQL_Main(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                radButton_saveVM.Enabled = false;
                radTextBox_vmip.Text = ""; radTextBox_vmip.Enabled = false;
                radTextBox_vmname.Text = ""; radTextBox_vmname.Enabled = false;
                radTextBox_vmprogram.Text = ""; radTextBox_vmprogram.Enabled = false;
                LoadClient();
            }
        }
        //Edit Client
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            EditClient();
        }
        //Edit Client
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (radGridView_Show.Rows.Count == 0) return;
                if (radGridView_Show.CurrentRow.Cells["CLIENT_IP"].Value.ToString() == "") return;

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบข้อมูล Client {radGridView_Show.CurrentRow.Cells["CLIENT_IP"].Value }{Environment.NewLine}ออกจาก Server {radTextBox_ip.Text} ?") == DialogResult.Yes)
                {
                    string sql = $@"
                        DELETE    FROM    SHOP_SERVER_CLIENT 
                        WHERE   SERVER_IP = '{radTextBox_ip.Text.Trim()}' AND CLIENT_IP = '{radGridView_Show.CurrentRow.Cells["CLIENT_IP"].Value}' ";
                    MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sql));
                    radButton_saveVM.Enabled = false;
                    radTextBox_vmip.Text = ""; radTextBox_vmip.Enabled = false;
                    radTextBox_vmname.Text = ""; radTextBox_vmname.Enabled = false;
                    radTextBox_vmprogram.Text = ""; radTextBox_vmprogram.Enabled = false;
                    LoadClient();
                }
            }
        }
        //Edit Client
        void EditClient()
        {
            if (radTextBox_ip.Text.Trim() == "") return;
            if (radGridView_Show.Rows.Count == 0) return;

            vmTypeSave = "1";

            radTextBox_vmip.Enabled = false; radTextBox_vmip.Text = radGridView_Show.CurrentRow.Cells["CLIENT_IP"].Value.ToString();
            radTextBox_vmname.Enabled = true; radTextBox_vmname.Text = radGridView_Show.CurrentRow.Cells["CLIENT_NAME"].Value.ToString(); radTextBox_vmname.Focus();
            radTextBox_vmprogram.Enabled = true; radTextBox_vmprogram.Text = radGridView_Show.CurrentRow.Cells["CLIENT_PROGRAM"].Value.ToString();
            radButton_saveVM.Enabled = true;
            radButton_cancleVM.Enabled = true;
        }

        #endregion


    }
}