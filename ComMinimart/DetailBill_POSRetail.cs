﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;
using System.IO;

namespace PC_Shop24Hrs.ComMinimart
{
    public partial class DetailBill_POSRetail : RadForm
    {
        DataTable dtHD = new DataTable();
        DataTable dtDT = new DataTable();
        DataTable dtRC = new DataTable();

        Data_ITEMBARCODE items;
        Data_CUSTOMER cust;

        private void DetailBill_POSRetail_Load(object sender, EventArgs e)
        {
            ClearData();
            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            RadButton_FindBranch.ButtonElement.ShowBorder = true;
            RadButton_FindBarcode.ButtonElement.ShowBorder = true;
            RadButton_FindCst.ButtonElement.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            radDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy"; radDateTimePicker_End.CustomFormat = "dd/MM/yyyy";

            DatagridClass.SetDefaultRadGridView(RadGridView_BillHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_BillDT);
            DatagridClass.SetDefaultRadGridView(RadGridView_BillRC);

            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 70));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSGROUPNAME", "ชื่อสาขา", 100));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 130));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่ขาย", 150));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CSTNAME", "ลูกค้า", 220));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("INVOICEAMOUNT", "ยอดเงินรวม", 120));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPNAME", "แคชเชียร์", 220));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSNUMBER", "เครื่องขาย", 100));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATIONID", "ตำแหน่ง", 100));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SIGN_BILL", "ประเภทบิล", 120));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่บิล", 220));
            RadGridView_BillHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARKS", "หมายเหตุ", 220));
            RadGridView_BillHD.Columns["POSGROUP"].IsPinned = true;
            RadGridView_BillHD.Columns["INVOICEID"].IsPinned = true;
            RadGridView_BillHD.Columns["POSGROUPNAME"].IsPinned = true;
            RadGridView_BillHD.DataSource = dtHD;

            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 300));
            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALESPRICE", "ราคา/หน่วย", 135));
            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALESUNIT", "หน่วย", 100));
            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ราคารวม", 150));
            RadGridView_BillDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("T4UTOPUPSONUMBER", "ประเภทการป้อน", 130));
            RadGridView_BillDT.DataSource = dtDT;


            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSPAYMMODE", "ประเภทการจ่าย", 200));
            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAYMAMOUNT", "ยอดเงินรวม", 150));
            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNTCURCHANGE", "ยอดเงินทอน", 150));
            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TRANSTXT", "เลขที่อ้างอิง", 200));
            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BANKACCOUNT", "ธนาคาร", 100));
            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BANKBRANCHACCOUNT", "ชื่อธนาคาร", 180));
            RadGridView_BillRC.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COUPREASONID", "หมายเหตุ", 100));
            RadGridView_BillRC.DataSource = dtRC;
            RadGridView_BillRC.MasterTemplate.EnableFiltering = false;

            radLabel_Detail.Text = "DoubleClick >> เลขที่บิล เพื่อดู File VDO แคชเชียร์";
            ClearData();
        }
        //Main Load
        public DetailBill_POSRetail()
        {
            InitializeComponent();
        }
        //clear
        void ClearData()
        {
            ClearGrid();
            radDateTimePicker_Begin.Value = DateTime.Today;
            radDateTimePicker_End.Value = DateTime.Today;
            radTextBox_Branch.Text = "";
            radTextBox_Barcode.Text = "";
            radTextBox_Price.Text = "";
            radTextBox_BillID.Text = "";
            radTextBox_Cst.Text = "";
            radTextBox_PosNumber.Text = "";

            radTextBox_Branch.Focus();
        }
        //Clear Grig
        void ClearGrid()
        {
            if (dtDT.Rows.Count > 0)
            {
                dtDT.Rows.Clear(); dtDT.AcceptChanges();
            }
            if (dtRC.Rows.Count > 0)
            {
                dtRC.Rows.Clear(); dtRC.AcceptChanges();
            }
            if (dtHD.Rows.Count > 0)
            {
                dtHD.Rows.Clear(); dtHD.AcceptChanges();
            }
        }

        #region "ROWS"


        private void RadGridView_BillHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_BillDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_BillRC_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion

        //Find Bracnh
        private void RadButton_FindBranch_Click(object sender, EventArgs e)
        {
            ShowDataDGV frm = new ShowDataDGV
            {
                dtData = BranchClass.GetBranchAll_ByConditions("'1'", " '1'", "")
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radTextBox_Branch.Text = frm.pID; radTextBox_Cst.Focus();
            }
            else
            {
                radTextBox_Branch.Text = ""; radTextBox_Branch.Focus();
            }
        }
        //Find data
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            FindData();
        }
        //findData
        void FindData()
        {
            string find_Branch;
            if (radTextBox_Branch.Text == "") find_Branch = "";
            else find_Branch = " AND XXX_POSTABLE.POSGROUP LIKE '%" + radTextBox_Branch.Text.Replace(" ", "%").ToUpper() + "%' ";

            string find_Barcode;
            if (radTextBox_Barcode.Text == "") find_Barcode = "";
            else find_Barcode = "AND XXX_POSLINE.ITEMBARCODE LIKE '" + radTextBox_Barcode.Text.Replace(" ", "%").Replace("*", "%") + "'";

            string findPrice;
            if (radTextBox_Price.Text == "") findPrice = ""; else findPrice = $@" AND SALESPRICE >= '{radTextBox_Price.Text}' ";

            string find_Invoice;
            if (radTextBox_BillID.Text == "") find_Invoice = "";
            else find_Invoice = "AND XXX_POSTABLE.INVOICEID LIKE '%" + radTextBox_BillID.Text.Replace(" ", "%").ToUpper() + "%'";

            string find_CstID;
            if (radTextBox_Cst.Text == "") find_CstID = "";
            else find_CstID = " AND XXX_POSTABLE.INVOICEACCOUNT LIKE '%" + radTextBox_Cst.Text.Replace(" ", "%").ToUpper() + "%' ";

            string find_Location;
            if (radTextBox_PosNumber.Text == "") find_Location = "";
            else find_Location = "AND LOCATIONID  LIKE '%" + radTextBox_PosNumber.Text.Replace(" ", "%") + "%' ";

            if ((find_Branch == "") && (find_Barcode == "") && (find_Invoice == "") && (find_CstID == "") && (find_Location) == "")
            {
                if (dtHD.Rows.Count > 0) ClearGrid();
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("เงื่อนไขเพิ่มเติม");
                radTextBox_Branch.Focus();
                return;
            }

            //ถ้าเปิดตอนใช้ ลบ GroupBY ออกด้วย
            //string strFind = String.Format(@"SELECT	XXX_POSTABLE.POSGROUP,POSGROUP.NAME AS POSGROUPNAME, XXX_POSTABLE.INVOICEID,
            //    CONVERT (varchar ,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE ,
            //    XXX_POSTABLE.INVOICEACCOUNT + '-' + XXX_POSTABLE.NAME AS CSTNAME,XXX_POSTABLE.INVOICEAMOUNT , 
            //    XXX_POSTABLE.CASHIERID + '-' + EMPLTABLE.SPC_NAME AS EMPNAME,XXX_POSTABLE.POSNUMBER,LOCATIONID, 
            //    CASE XXX_POSTABLE.SIGN WHEN '-1' THEN 'บิลเปลี่ยน/บิลยกเลิก' ELSE 'บิลขาย' END AS SIGN_BILL,REMARKS, 
            //    CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) AS CREATEDDATETIME 
            //    FROM	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK)  
            //    INNER JOIN SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID   
            //    INNER JOIN SHOP2013TMP.dbo.POSTABLE  WITH (NOLOCK) ON XXX_POSTABLE.POSNUMBER = POSTABLE.POSNUMBER  
            //    INNER JOIN SHOP2013TMP.dbo.POSGROUP   WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP   
            //    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'  
            //    WHERE	XXX_POSTABLE.DOCUTYPE = '1' 
            //    AND XXX_POSTABLE.INVOICEDATE BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
            //    AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + @"' " + find_Branch + " " + find_Barcode + " " + find_CstID + " " + find_Invoice + " " + find_Location + " " + findPrice + " " );

            //strFind += @"GROUP BY XXX_POSTABLE.POSGROUP+'-'+POSGROUP.NAME  , XXX_POSTABLE.INVOICEID,CONVERT (varchar ,XXX_POSTABLE.INVOICEDATE,23)   , 
            //    XXX_POSTABLE.INVOICEACCOUNT + '-' + XXX_POSTABLE.NAME, XXX_POSTABLE.INVOICEAMOUNT, 
            //    XXX_POSTABLE.CASHIERID + '-' + EMPLTABLE.SPC_NAME, XXX_POSTABLE.POSNUMBER, LOCATIONID, 
            //    CASE XXX_POSTABLE.SIGN WHEN '-1' THEN 'บิลเปลี่ยน/บิลยกเลิก' ELSE 'บิลขาย' END, REMARKS,CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) 
            //    ORDER BY CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) ";
            Cursor.Current = Cursors.WaitCursor;
            //dtHD = ConnectionClass.SelectSQL_Main(strFind);

            //string strFind = $@"
            //    SELECT	XXX_POSTABLE.POSGROUP,POSGROUP.NAME AS POSGROUPNAME, XXX_POSTABLE.INVOICEID,
            //            CONVERT (varchar ,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE ,
            //            XXX_POSTABLE.INVOICEACCOUNT + '-' + XXX_POSTABLE.NAME AS CSTNAME,XXX_POSTABLE.INVOICEAMOUNT , 
            //            XXX_POSTABLE.CASHIERID + '-' + EMPLTABLE.SPC_NAME AS EMPNAME,XXX_POSTABLE.POSNUMBER,LOCATIONID, 
            //            CASE XXX_POSTABLE.SIGN WHEN '-1' THEN 'บิลเปลี่ยน/บิลยกเลิก' ELSE 'บิลขาย' END AS SIGN_BILL,REMARKS, 
            //            CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) AS CREATEDDATETIME  
            //    FROM	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK)  
            //            INNER JOIN SHOP2013TMP.dbo.POSTABLE  WITH (NOLOCK) ON XXX_POSTABLE.POSNUMBER = POSTABLE.POSNUMBER  
            //            INNER JOIN SHOP2013TMP.dbo.POSGROUP   WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP   
            //            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'  
            //            INNER JOIN SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID
            //    WHERE	XXX_POSTABLE.DOCUTYPE = '1' 
            //            AND XXX_POSTABLE.INVOICEDATE BETWEEN '{radDateTimePicker_Begin.Value:yyyy-MM-dd}' 
            //            AND '{radDateTimePicker_End.Value:yyyy-MM-dd}' {find_Branch}  {find_Barcode } {find_CstID} {find_Invoice} {find_Location} {findPrice} 
            //    GROUP BY XXX_POSTABLE.POSGROUP,POSGROUP.NAME  , XXX_POSTABLE.INVOICEID,
            //            CONVERT(varchar, XXX_POSTABLE.INVOICEDATE, 23) ,
            //            XXX_POSTABLE.INVOICEACCOUNT + '-' + XXX_POSTABLE.NAME,XXX_POSTABLE.INVOICEAMOUNT , 
            //            XXX_POSTABLE.CASHIERID + '-' + EMPLTABLE.SPC_NAME ,XXX_POSTABLE.POSNUMBER,LOCATIONID, 
            //            CASE XXX_POSTABLE.SIGN WHEN '-1' THEN 'บิลเปลี่ยน/บิลยกเลิก' ELSE 'บิลขาย' END , REMARKS,
            //            CONVERT(VARCHAR, XXX_POSTABLE.CREATEDDATETIME, 25)  
            //    ORDER BY CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) ";


            //dtHD = ConnectionClass.SelectSQL_POSRetail707(strFind);
            dtHD = PosSaleClass.GetDetail_XXXPOSTABLE(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                find_Branch, find_Barcode, find_CstID, find_Invoice, find_Location, findPrice); //ConnectionClass.SelectSQL_Main(strFind);
            RadGridView_BillHD.DataSource = dtHD;
            if (dtHD.Rows.Count > 0) RadGridView_BillHD.Rows[0].IsSelected = true;
            else
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลบิลขาย");
                ClearGrid();
            }
            Cursor.Current = Cursors.WaitCursor;
        }
        //SelectChange
        private void RadGridView_BillHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_BillHD.Rows.Count == 0)
            {
                ClearGrid();
                return;
            }
            try
            {
                //string strFindDT = $@"
                //SELECT	LINENUM ,ITEMBARCODE ,NAME ,QTY,SALESUNIT ,SALESPRICE,LINEAMOUNT ,
                //        CASE ISNULL(T4UTOPUPSONUMBER,'') WHEN '' THEN 'SCAN' ELSE ISNULL(T4UTOPUPSONUMBER,'') END  AS T4UTOPUPSONUMBER
                //FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)   
                //WHERE	INVOICEID = '{RadGridView_BillHD.CurrentRow.Cells["INVOICEID"].Value}' ORDER BY LINENUM" ;
                dtDT = PosSaleClass.GetDetail_XXXPOSLINE(RadGridView_BillHD.CurrentRow.Cells["INVOICEID"].Value.ToString());// ConnectionClass.SelectSQL_Main(strFindDT);
                RadGridView_BillDT.DataSource = dtDT;

                //string strFindRC = string.Format(@"SELECT	COUPREASONID,BANKACCOUNT,BANKBRANCHACCOUNT,TRANSTXT,PAYMAMOUNT,AMOUNTCURCHANGE,
                //CASE POSPAYMMODE WHEN '0' THEN 'เงินสด' WHEN '1' THEN 'ธนาคาร/บัตรเครดิต' WHEN '2' THEN 'เช็ค' WHEN '3' THEN 'คูปอง' ELSE '' END AS POSPAYMMODE 
                //FROM	SHOP2013TMP.dbo.XXX_POSPAYM WITH (NOLOCK)    
                //WHERE	INVOICEID = '" + RadGridView_BillHD.CurrentRow.Cells["INVOICEID"].Value.ToString() + "'   ORDER BY LINENUM ");
                dtRC = PosSaleClass.GetDetail_XXXPOSPAYM(RadGridView_BillHD.CurrentRow.Cells["INVOICEID"].Value.ToString());// ConnectionClass.SelectSQL_Main(strFindRC);
                RadGridView_BillRC.DataSource = dtRC;
                return;
            }
            catch (Exception)
            {
                return;
            }

        }

        private void RadButton_FindBarcode_Click(object sender, EventArgs e)
        {
            using (ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(""))
            {
                DialogResult dr = ShowDataDGV_Itembarcode.ShowDialog();

                if (dr == DialogResult.Yes)
                {
                    this.items = ShowDataDGV_Itembarcode.items;
                    radTextBox_Barcode.Text = this.items.Itembarcode_ITEMBARCODE;

                }
            }

        }

        #region "KeysEnter"
        private void RadTextBox_Branch_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_Branch.Text != "")) FindData();
        }

        private void RadTextBox_Cst_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_Cst.Text != "")) FindData();
        }

        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_Barcode.Text != "")) FindData();
        }

        private void RadTextBox_BillID_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_BillID.Text != "")) FindData(); 
        }

        private void RadTextBox_PosNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_PosNumber.Text != "")) FindData(); 
        }
        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        { 
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //NumberOnly
        private void RadTextBox_PosNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadButton_FindCst_Click(object sender, EventArgs e)
        {

            using (ShowDataDGV_Customer ShowDataDGV_Customer = new ShowDataDGV_Customer(""))
            {
                DialogResult dr = ShowDataDGV_Customer.ShowDialog();

                if (dr == DialogResult.Yes)
                {
                    this.cust = ShowDataDGV_Customer.cust;
                    radTextBox_Cst.Text = this.cust.Customer_ACCOUNTNUM;
                }
            }
        }

        private void RadGridView_BillHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (e.Column.Name == "INVOICEID")
            {
                string server;
                string bchID = RadGridView_BillHD.CurrentRow.Cells["POSGROUP"].Value.ToString();
                DataTable dtServer = BranchClass.GetDBBranch(bchID);
                if (dtServer.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูล Server ที่เก็บ File");
                    return;
                }
                else server = dtServer.Rows[0]["SERVER_PATHVDO"].ToString();

                string dateBill = RadGridView_BillHD.CurrentRow.Cells["INVOICEDATE"].Value.ToString();
                string invoiceID = RadGridView_BillHD.CurrentRow.Cells["INVOICEID"].Value.ToString();

                this.Cursor = Cursors.WaitCursor;

                DirectoryInfo DirInfoVDO = new DirectoryInfo($@"\\{server}\MNCamera\{bchID}\{dateBill.Substring(0, 7)}\{dateBill}");
                if (DirInfoVDO.Exists == false)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูลที่เก็บ File" + Environment.NewLine + DirInfoVDO.FullName);
                    this.Cursor = Cursors.Default;
                    return;
                }

                //S2109B64-0002872_20210905105424
                FileInfo[] FilesInvoice = DirInfoVDO.GetFiles(invoiceID + @"_*", SearchOption.AllDirectories);
                if (FilesInvoice.Length == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูล VDO บิลขาย ที่ระบุ");
                    this.Cursor = Cursors.Default;
                    return;
                }
                try
                {
                    System.Diagnostics.Process.Start(FilesInvoice[FilesInvoice.Length - 1].FullName);
                    this.Cursor = Cursors.Default;
                    return;
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถเปิด File VDO ได้" + Environment.NewLine + ex.Message);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
        }

        private void RadTextBox_Price_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}



