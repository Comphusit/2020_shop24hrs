﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using Telerik.WinControls.Data;

namespace PC_Shop24Hrs.ComMinimart
{
    public partial class Config_Server_Main : Telerik.WinControls.UI.RadForm
    {

        //Load
        public Config_Server_Main()
        {
            InitializeComponent();
        }
        //Load Main
        private void Config_Server_Main_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_CheckWeb.ShowBorder = true; radButtonElement_CheckWeb.ToolTipText = "เพิ่ม Server หลัก";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข Server/Client";
            radStatusStrip1.SizingGrip = false;
            if (SystemClass.SystemComMinimart != "1")
            {
                radButtonElement_CheckWeb.Enabled = false; radButtonElement_Edit.Enabled = false;
            }
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_IP", "SERVER IP", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_NAME", "SERVER NAME", 160));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_ILO", "SERVER ILO", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_TYPE", "Remote", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CLIENT_IP", "Client IP", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CLIENT_NAME", "Client Name", 160));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CLIENT_PROGRAM", "โปรแกรมที่ขึ้นใช้", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_LOCATION", "ที่ตั้ง", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_REMARK", "หมายเหตุ", 300));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_USER", "USER", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVER_PASSWORD", "PASSWORD", 120));

            radGridView_Show.Columns["SERVER_IP"].IsPinned = true;
            GroupDescriptor descriptor = new GroupDescriptor();
            descriptor.GroupNames.Add("SERVER_IP", System.ComponentModel.ListSortDirection.Ascending);
            radGridView_Show.GroupDescriptors.Add(descriptor);

            radLabel_Detail.Text = "ข้อมูล Server ทั้งหมดที่ขึ้นใช้งาน | แจ้งเพิ่ม Server/Client >> ติดต่อ ComMinimart [Tel.8570 หรือ Mail]";
            LoadData();
        }
        //ดึงข้อมูลทั้งหมด
        void LoadData()
        {
            string str = $@"
                SELECT  [SHOP_SERVER].[SERVER_IP],[SERVER_NAME],[SERVER_ILO],[SERVER_TYPE],
		                [SERVER_USER],[SERVER_PASSWORD],[SERVER_LOCATION],[SERVER_REMARK],
		                ISNULL(CLIENT_IP,'') AS CLIENT_IP,ISNULL(CLIENT_NAME,'') AS CLIENT_NAME,ISNULL(CLIENT_PROGRAM,'') AS CLIENT_PROGRAM
                FROM	[SHOP_SERVER] WITH (NOLOCK)
		                LEFT OUTER JOIN [SHOP_SERVER_CLIENT] WITH (NOLOCK) ON [SHOP_SERVER].[SERVER_IP] =  [SHOP_SERVER_CLIENT].[SERVER_IP]
                 ORDER BY [SHOP_SERVER].SERVER_IP,[SHOP_SERVER_CLIENT].[CLIENT_IP]  ";
            radGridView_Show.DataSource = ConnectionClass.SelectSQL_Main(str);
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //add Server
        private void RadButtonElement_CheckWeb_Click(object sender, EventArgs e)
        {
            Config_Server config_Server = new Config_Server("");
            if (config_Server.ShowDialog() == DialogResult.OK) { }
            LoadData();
        }
        void EditServer()
        {
            if (radGridView_Show.CurrentRow.Cells["SERVER_IP"].Value.ToString() == "") return;
            Config_Server config_Server = new Config_Server(radGridView_Show.CurrentRow.Cells["SERVER_IP"].Value.ToString());
            if (config_Server.ShowDialog() == DialogResult.OK) { }
            LoadData();
        }
        //Edit Server
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            EditServer();
        }
        //Edit
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            if (SystemClass.SystemComMinimart != "1") return;
            EditServer();
        }
    }
}
