﻿namespace PC_Shop24Hrs.ComMinimart
{
    partial class Config_Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Config_Server));
            this.radTextBox_ip = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.PrintDocument_LO = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Name = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_ilo = new Telerik.WinControls.UI.RadTextBox();
            this.RadDropDownList_Remote = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_User = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Pass = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Location = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_vmprogram = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_vmname = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_vmip = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton_addVm = new Telerik.WinControls.UI.RadButton();
            this.radButton_saveVM = new Telerik.WinControls.UI.RadButton();
            this.radButton_cancleVM = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ilo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Remote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_User)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Pass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_vmprogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_vmname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_vmip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_addVm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_saveVM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_cancleVM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radTextBox_ip
            // 
            this.radTextBox_ip.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_ip.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ip.Location = new System.Drawing.Point(121, 22);
            this.radTextBox_ip.MaxLength = 500;
            this.radTextBox_ip.Name = "radTextBox_ip";
            this.radTextBox_ip.Size = new System.Drawing.Size(232, 25);
            this.radTextBox_ip.TabIndex = 1;
            this.radTextBox_ip.Text = "192.168.100.47";
            this.radTextBox_ip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ip_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(19, 28);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(70, 19);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "Server IP";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 131);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(679, 187);
            this.radGridView_Show.TabIndex = 40;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(19, 65);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(94, 19);
            this.radLabel1.TabIndex = 45;
            this.radLabel1.Text = "Server Name";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(19, 107);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(79, 19);
            this.radLabel2.TabIndex = 46;
            this.radLabel2.Text = "Server ILO";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(362, 26);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(59, 19);
            this.radLabel3.TabIndex = 47;
            this.radLabel3.Text = "Remote";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(19, 142);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(37, 19);
            this.radLabel5.TabIndex = 48;
            this.radLabel5.Text = "User";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(19, 177);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(72, 19);
            this.radLabel6.TabIndex = 47;
            this.radLabel6.Text = "Password";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(362, 60);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(67, 19);
            this.radLabel7.TabIndex = 47;
            this.radLabel7.Text = "สถานที่ตั้ง";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(344, 11);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 19);
            this.radLabel8.TabIndex = 49;
            this.radLabel8.Text = "Program";
            // 
            // radTextBox_Name
            // 
            this.radTextBox_Name.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Name.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Name.Location = new System.Drawing.Point(120, 60);
            this.radTextBox_Name.MaxLength = 500;
            this.radTextBox_Name.Name = "radTextBox_Name";
            this.radTextBox_Name.Size = new System.Drawing.Size(232, 25);
            this.radTextBox_Name.TabIndex = 50;
            this.radTextBox_Name.Text = "192.168.100.47";
            this.radTextBox_Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Name_KeyDown);
            // 
            // radTextBox_ilo
            // 
            this.radTextBox_ilo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_ilo.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ilo.Location = new System.Drawing.Point(121, 101);
            this.radTextBox_ilo.MaxLength = 500;
            this.radTextBox_ilo.Name = "radTextBox_ilo";
            this.radTextBox_ilo.Size = new System.Drawing.Size(232, 25);
            this.radTextBox_ilo.TabIndex = 51;
            this.radTextBox_ilo.Text = "192.168.100.47";
            this.radTextBox_ilo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ilo_KeyDown);
            // 
            // RadDropDownList_Remote
            // 
            this.RadDropDownList_Remote.DropDownAnimationEnabled = false;
            this.RadDropDownList_Remote.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Remote.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Remote.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Remote.Location = new System.Drawing.Point(460, 24);
            this.RadDropDownList_Remote.Name = "RadDropDownList_Remote";
            this.RadDropDownList_Remote.Size = new System.Drawing.Size(233, 25);
            this.RadDropDownList_Remote.TabIndex = 52;
            this.RadDropDownList_Remote.Text = "radDropDownList1";
            // 
            // radTextBox_User
            // 
            this.radTextBox_User.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_User.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_User.Location = new System.Drawing.Point(120, 136);
            this.radTextBox_User.MaxLength = 500;
            this.radTextBox_User.Name = "radTextBox_User";
            this.radTextBox_User.Size = new System.Drawing.Size(233, 25);
            this.radTextBox_User.TabIndex = 53;
            this.radTextBox_User.Text = "192.168.100.47";
            this.radTextBox_User.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_User_KeyDown);
            // 
            // radTextBox_Pass
            // 
            this.radTextBox_Pass.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Pass.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Pass.Location = new System.Drawing.Point(121, 172);
            this.radTextBox_Pass.MaxLength = 500;
            this.radTextBox_Pass.Name = "radTextBox_Pass";
            this.radTextBox_Pass.Size = new System.Drawing.Size(233, 25);
            this.radTextBox_Pass.TabIndex = 54;
            this.radTextBox_Pass.Text = "192.168.100.47";
            this.radTextBox_Pass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Pass_KeyDown);
            // 
            // radTextBox_Location
            // 
            this.radTextBox_Location.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Location.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Location.Location = new System.Drawing.Point(460, 59);
            this.radTextBox_Location.MaxLength = 500;
            this.radTextBox_Location.Multiline = true;
            this.radTextBox_Location.Name = "radTextBox_Location";
            // 
            // 
            // 
            this.radTextBox_Location.RootElement.StretchVertically = true;
            this.radTextBox_Location.Size = new System.Drawing.Size(233, 67);
            this.radTextBox_Location.TabIndex = 55;
            this.radTextBox_Location.Text = "192.168.100.47";
            this.radTextBox_Location.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Location_KeyDown);
            // 
            // radTextBox_vmprogram
            // 
            this.radTextBox_vmprogram.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_vmprogram.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_vmprogram.Location = new System.Drawing.Point(442, 11);
            this.radTextBox_vmprogram.MaxLength = 500;
            this.radTextBox_vmprogram.Multiline = true;
            this.radTextBox_vmprogram.Name = "radTextBox_vmprogram";
            // 
            // 
            // 
            this.radTextBox_vmprogram.RootElement.StretchVertically = true;
            this.radTextBox_vmprogram.Size = new System.Drawing.Size(233, 65);
            this.radTextBox_vmprogram.TabIndex = 56;
            this.radTextBox_vmprogram.Text = "192.168.100.47";
            this.radTextBox_vmprogram.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_vmprogram_KeyDown);
            // 
            // radTextBox_vmname
            // 
            this.radTextBox_vmname.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_vmname.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_vmname.Location = new System.Drawing.Point(106, 48);
            this.radTextBox_vmname.MaxLength = 500;
            this.radTextBox_vmname.Name = "radTextBox_vmname";
            this.radTextBox_vmname.Size = new System.Drawing.Size(228, 25);
            this.radTextBox_vmname.TabIndex = 60;
            this.radTextBox_vmname.Text = "192.168.100.47";
            this.radTextBox_vmname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_vmname_KeyDown);
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(5, 53);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(89, 19);
            this.radLabel9.TabIndex = 59;
            this.radLabel9.Text = "Client Name";
            // 
            // radTextBox_vmip
            // 
            this.radTextBox_vmip.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_vmip.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_vmip.Location = new System.Drawing.Point(107, 10);
            this.radTextBox_vmip.MaxLength = 500;
            this.radTextBox_vmip.Name = "radTextBox_vmip";
            this.radTextBox_vmip.Size = new System.Drawing.Size(228, 25);
            this.radTextBox_vmip.TabIndex = 57;
            this.radTextBox_vmip.Text = "192.168.100.47";
            this.radTextBox_vmip.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_vmip_KeyDown);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(41, 13);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(65, 19);
            this.radLabel10.TabIndex = 58;
            this.radLabel10.Text = "Client IP";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 253);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(691, 369);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SERVER CLIENT";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(685, 347);
            this.tableLayoutPanel1.TabIndex = 68;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radButton_addVm);
            this.panel1.Controls.Add(this.radButton_saveVM);
            this.panel1.Controls.Add(this.radLabel10);
            this.panel1.Controls.Add(this.radTextBox_vmname);
            this.panel1.Controls.Add(this.radButton_cancleVM);
            this.panel1.Controls.Add(this.radLabel9);
            this.panel1.Controls.Add(this.radTextBox_vmprogram);
            this.panel1.Controls.Add(this.radTextBox_vmip);
            this.panel1.Controls.Add(this.radLabel8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(679, 122);
            this.panel1.TabIndex = 41;
            // 
            // radButton_addVm
            // 
            this.radButton_addVm.BackColor = System.Drawing.Color.Transparent;
            this.radButton_addVm.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_addVm.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_addVm.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButton_addVm.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_addVm.Location = new System.Drawing.Point(9, 12);
            this.radButton_addVm.Name = "radButton_addVm";
            this.radButton_addVm.Size = new System.Drawing.Size(26, 26);
            this.radButton_addVm.TabIndex = 74;
            this.radButton_addVm.Text = "radButton3";
            this.radButton_addVm.Click += new System.EventHandler(this.RadButton_addVm_Click);
            // 
            // radButton_saveVM
            // 
            this.radButton_saveVM.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_saveVM.Location = new System.Drawing.Point(244, 88);
            this.radButton_saveVM.Name = "radButton_saveVM";
            this.radButton_saveVM.Size = new System.Drawing.Size(92, 32);
            this.radButton_saveVM.TabIndex = 66;
            this.radButton_saveVM.Text = "บันทึก";
            this.radButton_saveVM.ThemeName = "Fluent";
            this.radButton_saveVM.Click += new System.EventHandler(this.RadButton_saveVM_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_saveVM.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_saveVM.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_saveVM.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_saveVM.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_saveVM.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_saveVM.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_saveVM.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_saveVM.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_saveVM.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radButton_cancleVM
            // 
            this.radButton_cancleVM.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_cancleVM.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_cancleVM.Location = new System.Drawing.Point(344, 88);
            this.radButton_cancleVM.Name = "radButton_cancleVM";
            this.radButton_cancleVM.Size = new System.Drawing.Size(92, 32);
            this.radButton_cancleVM.TabIndex = 67;
            this.radButton_cancleVM.Text = "ยกเลิก";
            this.radButton_cancleVM.ThemeName = "Fluent";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_cancleVM.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_cancleVM.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancleVM.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancleVM.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancleVM.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancleVM.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_cancleVM.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_remark
            // 
            this.radTextBox_remark.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_remark.Location = new System.Drawing.Point(460, 130);
            this.radTextBox_remark.MaxLength = 500;
            this.radTextBox_remark.Multiline = true;
            this.radTextBox_remark.Name = "radTextBox_remark";
            // 
            // 
            // 
            this.radTextBox_remark.RootElement.StretchVertically = true;
            this.radTextBox_remark.Size = new System.Drawing.Size(233, 67);
            this.radTextBox_remark.TabIndex = 62;
            this.radTextBox_remark.Text = "192.168.100.47";
            this.radTextBox_remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_remark_KeyDown);
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(362, 135);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(65, 19);
            this.radLabel11.TabIndex = 63;
            this.radLabel11.Text = "หมายเหตุ";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(262, 208);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 64;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(362, 208);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 65;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 324);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(679, 19);
            this.radLabel_Detail.TabIndex = 56;
            // 
            // Config_Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 634);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.radTextBox_remark);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radTextBox_Location);
            this.Controls.Add(this.radTextBox_Pass);
            this.Controls.Add(this.radTextBox_User);
            this.Controls.Add(this.RadDropDownList_Remote);
            this.Controls.Add(this.radTextBox_ilo);
            this.Controls.Add(this.radTextBox_Name);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_ip);
            this.Controls.Add(this.radLabel4);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Config_Server";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Config Server";
            this.Load += new System.EventHandler(this.Config_Server_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ilo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Remote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_User)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Pass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_vmprogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_vmname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_vmip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_addVm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_saveVM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_cancleVM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ip;
        private System.Drawing.Printing.PrintDocument PrintDocument_LO;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Name;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ilo;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Remote;
        private Telerik.WinControls.UI.RadTextBox radTextBox_User;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Pass;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Location;
        private Telerik.WinControls.UI.RadTextBox radTextBox_vmprogram;
        private Telerik.WinControls.UI.RadTextBox radTextBox_vmname;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox radTextBox_vmip;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_remark;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_saveVM;
        protected Telerik.WinControls.UI.RadButton radButton_cancleVM;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton_addVm;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}
