﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.ComMinimart
{
    public partial class DetailFingerprint : Telerik.WinControls.UI.RadForm
    {
        string pServerBch;
        DataTable dt707 = new DataTable();
        readonly DataTable dt701 = new DataTable();
        DataTable dt803 = new DataTable();
        DataTable dt000 = new DataTable();
        //Load
        public DetailFingerprint()
        {
            InitializeComponent();

            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Branch);

            DatagridClass.SetDefaultRadGridView(radGridView_701);
            DatagridClass.SetDefaultRadGridView(radGridView_803);
            DatagridClass.SetDefaultRadGridView(radGridView_707);
            DatagridClass.SetDefaultRadGridView(radGridView_000);

            DatagridClass.SetDefaultFontGroupBox(radGroupBox_701);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_803);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_707);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_000);

            radGridView_701.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("tfEmpId", "รหัส", 80));
            radGridView_701.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("tfEmpName", "ชื่อพนักงาน", 200));
            radGridView_701.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("tfFingerIndex", "FingerIndex", 100));
            radGridView_701.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("tfIndexValue", "FingerValue", 300));
            radGridView_701.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("tfInsDate", "วันที่", 150));
            radGridView_701.MasterTemplate.EnableFiltering = false;

            radGridView_803.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 80));
            radGridView_803.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FINGERINDEX", "FingerIndex", 100));
            radGridView_803.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FINGERTEMPLATESIZE", "FingerValue", 300));
            radGridView_803.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("", "วันที่", 150));
            radGridView_803.MasterTemplate.EnableFiltering = false;

            radGridView_707.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 80));
            radGridView_707.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FINGERINDEX", "FingerIndex", 100));
            radGridView_707.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INDEXINGVALUE", "FingerValue", 300));
            radGridView_707.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDBY", "วันที่", 150));
            radGridView_707.MasterTemplate.EnableFiltering = false;

            radGridView_000.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 80));
            radGridView_000.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FINGERINDEX", "FingerIndex", 100));
            radGridView_000.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INDEXINGVALUE", "FingerValue", 300));
            radGridView_000.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDBY", "วันที่", 150));
            radGridView_000.MasterTemplate.EnableFiltering = false;

        }
        //Load Main
        private void DetailFingerprint_Load(object sender, EventArgs e)
        {
            DataTable dtBch = BranchClass.GetBranchAll("'1','4'", "'1'");
            radDropDownList_Branch.DataSource = dtBch;
            radDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            radDropDownList_Branch.ValueMember = "BRANCH_ID";

            ClearData();
        }

        void ClearData()
        {
            pServerBch = "";
            if (dt000.Rows.Count > 0) { dt000.Rows.Clear(); }
            dt000.AcceptChanges();
            if (dt701.Rows.Count > 0) { dt701.Rows.Clear(); }
            dt701.AcceptChanges();
            if (dt707.Rows.Count > 0) { dt707.Rows.Clear(); }
            dt707.AcceptChanges();
            if (dt803.Rows.Count > 0) { dt803.Rows.Clear(); }
            dt803.AcceptChanges();
            radTextBox_Emp.Text = ""; radTextBox_Emp.Focus();
            radLabel_EmpName.Text = "";
        }
        //Set Server
        void SetServerBch()
        {
            if (radDropDownList_Branch.SelectedValue.ToString() is null)
            {
                ClearData();
                return;
            }

            DataTable dtBch = BranchClass.GetBranchSettingBranch(radDropDownList_Branch.SelectedValue.ToString());
            if (dtBch.Rows.Count == 0)
            {
                pServerBch = "";
                return;
            }

            pServerBch = @"Data Source=" + dtBch.Rows[0]["BRANCH_SERVERNAME"].ToString() + ";" +
                "Initial Catalog=" + dtBch.Rows[0]["BRANCH_DATABASENAME"].ToString() + "; " +
                "User Id=" + dtBch.Rows[0]["BRANCH_USERNAME"].ToString() + ";Password=" + dtBch.Rows[0]["BRANCH_PASSWORDNAME"].ToString();

        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            FindEmpl();
        }

        #region "ROWS"


        private void RadGridView_701_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_803_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_707_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_000_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion
 
        //Enter
        private void RadTextBox_Emp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) FindEmpl();
        }
        //Find Emp
        void FindEmpl()
        {
            string pEmpID = radTextBox_Emp.Text;
            if (pEmpID == "") { MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("รหัสพนักงาน"); radTextBox_Emp.Focus(); return; }

            SetServerBch();
            if (pServerBch == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบการตั้งค่า Server ของสาขา ติดต่อ ComMinimart [Programmer] เพื่อเช็คระบบอีกครั้ง.");
                ClearData();
                return;
            }

            DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(pEmpID);
            if (dtEmp.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน");
                ClearData();
                return;
            }

            radLabel_EmpName.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();

            Cursor.Current = Cursors.WaitCursor;
            dt707 = FingerClass.GetFinger707(pEmpID); radGridView_707.DataSource = dt707;
            dt803 = FingerClass.GetFinger803(pEmpID); radGridView_803.DataSource = dt803;
            dt000 = FingerClass.GetFinger000(pEmpID, pServerBch); radGridView_000.DataSource = dt000;
            Cursor.Current = Cursors.Default;

            radTextBox_Emp.SelectAll(); radTextBox_Emp.Focus();
        }
        //set Focus
        private void RadDropDownList_Branch_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Emp.Focus();
        }
        //Length
        private void RadTextBox_Emp_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_Emp.Text.Length != 7) radLabel_EmpName.Text = "";
        }
        //Number Only
        private void RadTextBox_Emp_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
