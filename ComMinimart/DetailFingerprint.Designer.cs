﻿namespace PC_Shop24Hrs.ComMinimart
{
    partial class DetailFingerprint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailFingerprint));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_EmpName = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Emp = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox_000 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView_000 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox_707 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView_707 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox_701 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView_701 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox_803 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView_803 = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_000)).BeginInit();
            this.radGroupBox_000.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_000.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_707)).BeginInit();
            this.radGroupBox_707.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_707)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_707.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_701)).BeginInit();
            this.radGroupBox_701.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_701)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_701.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_803)).BeginInit();
            this.radGroupBox_803.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_803)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_803.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radLabel_EmpName);
            this.panel1.Controls.Add(this.radDropDownList_Branch);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.radTextBox_Emp);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 7;
            // 
            // radLabel_EmpName
            // 
            this.radLabel_EmpName.AutoSize = false;
            this.radLabel_EmpName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radLabel_EmpName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmpName.Location = new System.Drawing.Point(8, 115);
            this.radLabel_EmpName.Name = "radLabel_EmpName";
            this.radLabel_EmpName.Size = new System.Drawing.Size(175, 23);
            this.radLabel_EmpName.TabIndex = 64;
            this.radLabel_EmpName.Text = "ชื่อพนักงาน";
            // 
            // radDropDownList_Branch
            // 
            this.radDropDownList_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Branch.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Branch.DropDownHeight = 160;
            this.radDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Branch.Location = new System.Drawing.Point(8, 30);
            this.radDropDownList_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Branch.Name = "radDropDownList_Branch";
            // 
            // 
            // 
            this.radDropDownList_Branch.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Branch.Size = new System.Drawing.Size(175, 25);
            this.radDropDownList_Branch.TabIndex = 63;
            this.radDropDownList_Branch.Text = "radDropDownList1";
            this.radDropDownList_Branch.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDownList_Branch_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Branch.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Branch.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(8, 159);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 1;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_Emp
            // 
            this.radTextBox_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Emp.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Emp.Location = new System.Drawing.Point(8, 92);
            this.radTextBox_Emp.MaxLength = 7;
            this.radTextBox_Emp.Name = "radTextBox_Emp";
            this.radTextBox_Emp.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Emp.TabIndex = 0;
            this.radTextBox_Emp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Emp_KeyDown);
            this.radTextBox_Emp.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Emp_KeyUp);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(8, 12);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(39, 19);
            this.radLabel3.TabIndex = 19;
            this.radLabel3.Text = "สาขา";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(7, 74);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(119, 19);
            this.radLabel2.TabIndex = 18;
            this.radLabel2.Text = "รหัส พนง. [Enter]";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radPanel4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.radPanel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radPanel2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radPanel4
            // 
            this.radPanel4.Controls.Add(this.radGroupBox_000);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(3, 474);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(617, 151);
            this.radPanel4.TabIndex = 12;
            // 
            // radGroupBox_000
            // 
            this.radGroupBox_000.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_000.Controls.Add(this.radGridView_000);
            this.radGroupBox_000.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_000.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGroupBox_000.HeaderText = "SERVER สาขา [กรณีที่ไม่มีข้อมูล ตรวจRep ที่ 707 และ 110,113,114,115 เส้น Finger]";
            this.radGroupBox_000.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_000.Name = "radGroupBox_000";
            this.radGroupBox_000.Size = new System.Drawing.Size(617, 151);
            this.radGroupBox_000.TabIndex = 0;
            this.radGroupBox_000.Text = "SERVER สาขา [กรณีที่ไม่มีข้อมูล ตรวจRep ที่ 707 และ 110,113,114,115 เส้น Finger]";
            // 
            // radGridView_000
            // 
            this.radGridView_000.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_000.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_000.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.radGridView_000.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_000.Name = "radGridView_000";
            // 
            // 
            // 
            this.radGridView_000.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_000.Size = new System.Drawing.Size(613, 131);
            this.radGridView_000.TabIndex = 2;
            this.radGridView_000.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_000_ViewCellFormatting);
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.radGroupBox_707);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(3, 317);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(617, 151);
            this.radPanel3.TabIndex = 11;
            // 
            // radGroupBox_707
            // 
            this.radGroupBox_707.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_707.Controls.Add(this.radGridView_707);
            this.radGroupBox_707.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_707.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGroupBox_707.HeaderText = "SPC707SRV [กรณีที่ไม่มีข้อมูล ให้เพิ่มข้อมูลพนักงานขาย Cpanel]";
            this.radGroupBox_707.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_707.Name = "radGroupBox_707";
            this.radGroupBox_707.Size = new System.Drawing.Size(617, 151);
            this.radGroupBox_707.TabIndex = 0;
            this.radGroupBox_707.Text = "SPC707SRV [กรณีที่ไม่มีข้อมูล ให้เพิ่มข้อมูลพนักงานขาย Cpanel]";
            // 
            // radGridView_707
            // 
            this.radGridView_707.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_707.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_707.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.radGridView_707.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_707.Name = "radGridView_707";
            // 
            // 
            // 
            this.radGridView_707.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_707.Size = new System.Drawing.Size(613, 131);
            this.radGridView_707.TabIndex = 2;
            this.radGridView_707.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_707_ViewCellFormatting);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radGroupBox_701);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(617, 151);
            this.radPanel1.TabIndex = 10;
            // 
            // radGroupBox_701
            // 
            this.radGroupBox_701.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_701.Controls.Add(this.radGridView_701);
            this.radGroupBox_701.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_701.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_701.HeaderText = "SPC701SRV ItWorks [กรณีที่ไม่มีข้อมูล ให้ติดต่อมาเก็บลายนิ้วมือรับเงินเดือน]";
            this.radGroupBox_701.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_701.Name = "radGroupBox_701";
            this.radGroupBox_701.Size = new System.Drawing.Size(617, 151);
            this.radGroupBox_701.TabIndex = 0;
            this.radGroupBox_701.Text = "SPC701SRV ItWorks [กรณีที่ไม่มีข้อมูล ให้ติดต่อมาเก็บลายนิ้วมือรับเงินเดือน]";
            // 
            // radGridView_701
            // 
            this.radGridView_701.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_701.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_701.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.radGridView_701.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radGridView_701.Name = "radGridView_701";
            // 
            // 
            // 
            this.radGridView_701.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_701.Size = new System.Drawing.Size(613, 131);
            this.radGridView_701.TabIndex = 1;
            this.radGridView_701.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_701_ViewCellFormatting);
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radGroupBox_803);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(3, 160);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(617, 151);
            this.radPanel2.TabIndex = 9;
            // 
            // radGroupBox_803
            // 
            this.radGroupBox_803.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_803.Controls.Add(this.radGridView_803);
            this.radGroupBox_803.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_803.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGroupBox_803.HeaderText = "SPC803SRV Digital Personal [กรณีที่ไม่มีข้อมูล ให้ติดต่อมาเก็บลายนิ้วมือรับเงินเด" +
    "ือน]";
            this.radGroupBox_803.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_803.Name = "radGroupBox_803";
            this.radGroupBox_803.Size = new System.Drawing.Size(617, 151);
            this.radGroupBox_803.TabIndex = 0;
            this.radGroupBox_803.Text = "SPC803SRV Digital Personal [กรณีที่ไม่มีข้อมูล ให้ติดต่อมาเก็บลายนิ้วมือรับเงินเด" +
    "ือน]";
            // 
            // radGridView_803
            // 
            this.radGridView_803.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_803.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_803.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.radGridView_803.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.radGridView_803.Name = "radGridView_803";
            // 
            // 
            // 
            this.radGridView_803.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_803.Size = new System.Drawing.Size(613, 131);
            this.radGridView_803.TabIndex = 2;
            this.radGridView_803.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_803_ViewCellFormatting);
            // 
            // DetailFingerprint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DetailFingerprint";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ตรวจสอบข้อมูลลายนิ้วมือ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DetailFingerprint_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_000)).EndInit();
            this.radGroupBox_000.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_000.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_707)).EndInit();
            this.radGroupBox_707.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_707.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_707)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_701)).EndInit();
            this.radGroupBox_701.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_701.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_701)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_803)).EndInit();
            this.radGroupBox_803.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_803.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_803)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Emp;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_000;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_707;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_701;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_803;
        private Telerik.WinControls.UI.RadGridView radGridView_701;
        private Telerik.WinControls.UI.RadGridView radGridView_000;
        private Telerik.WinControls.UI.RadGridView radGridView_707;
        private Telerik.WinControls.UI.RadGridView radGridView_803;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Branch;
        private Telerik.WinControls.UI.RadLabel radLabel_EmpName;
    }
}
