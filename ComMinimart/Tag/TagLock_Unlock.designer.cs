﻿namespace PC_Shop24Hrs.ComMinimart.Tag
{
    partial class TagLock_Unlock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagLock_Unlock));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel_BranchLock = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel_BranchUnlock = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Key = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Unlock = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Lock = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_BranckUnLock = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_BranckLock = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_LO = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_TAGFull = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_TAG = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_BranchLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_BranchUnlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Key)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Lock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranckUnLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranckLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_LO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_TAGFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_TAG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Controls.Add(this.radLabel8);
            this.radGroupBox_DB.Controls.Add(this.radLabel7);
            this.radGroupBox_DB.Controls.Add(this.RadLabel_BranchLock);
            this.radGroupBox_DB.Controls.Add(this.RadLabel_BranchUnlock);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Key);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Unlock);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Lock);
            this.radGroupBox_DB.Controls.Add(this.RadTextBox_BranckUnLock);
            this.radGroupBox_DB.Controls.Add(this.RadTextBox_BranckLock);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.RadTextBox_LO);
            this.radGroupBox_DB.Controls.Add(this.RadTextBox_Remark);
            this.radGroupBox_DB.Controls.Add(this.radLabel6);
            this.radGroupBox_DB.Controls.Add(this.RadTextBox_TAGFull);
            this.radGroupBox_DB.Controls.Add(this.radLabel5);
            this.radGroupBox_DB.Controls.Add(this.RadTextBox_TAG);
            this.radGroupBox_DB.Controls.Add(this.RadButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.RadButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Controls.Add(this.radLabel4);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(429, 418);
            this.radGroupBox_DB.TabIndex = 0;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.ForeColor = System.Drawing.Color.Black;
            this.radLabel8.Location = new System.Drawing.Point(100, 236);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(29, 19);
            this.radLabel8.TabIndex = 60;
            this.radLabel8.Text = "MN";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.ForeColor = System.Drawing.Color.Black;
            this.radLabel7.Location = new System.Drawing.Point(101, 171);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(29, 19);
            this.radLabel7.TabIndex = 59;
            this.radLabel7.Text = "MN";
            // 
            // RadLabel_BranchLock
            // 
            this.RadLabel_BranchLock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel_BranchLock.ForeColor = System.Drawing.Color.Blue;
            this.RadLabel_BranchLock.Location = new System.Drawing.Point(214, 171);
            this.RadLabel_BranchLock.Name = "RadLabel_BranchLock";
            this.RadLabel_BranchLock.Size = new System.Drawing.Size(85, 19);
            this.RadLabel_BranchLock.TabIndex = 58;
            this.RadLabel_BranchLock.Text = "BranchLock";
            // 
            // RadLabel_BranchUnlock
            // 
            this.RadLabel_BranchUnlock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel_BranchUnlock.ForeColor = System.Drawing.Color.Blue;
            this.RadLabel_BranchUnlock.Location = new System.Drawing.Point(214, 236);
            this.RadLabel_BranchUnlock.Name = "RadLabel_BranchUnlock";
            this.RadLabel_BranchUnlock.Size = new System.Drawing.Size(100, 19);
            this.RadLabel_BranchUnlock.TabIndex = 57;
            this.RadLabel_BranchUnlock.Text = "BranchUnlock";
            // 
            // radLabel_Key
            // 
            this.radLabel_Key.AutoSize = false;
            this.radLabel_Key.BorderVisible = true;
            this.radLabel_Key.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Key.Image = global::PC_Shop24Hrs.Properties.Resources.TagLock_unlock;
            this.radLabel_Key.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel_Key.Location = new System.Drawing.Point(35, 227);
            this.radLabel_Key.Name = "radLabel_Key";
            this.radLabel_Key.Size = new System.Drawing.Size(29, 34);
            this.radLabel_Key.TabIndex = 7;
            this.radLabel_Key.Click += new System.EventHandler(this.RadLabel_Key_Click);
            // 
            // radLabel_Unlock
            // 
            this.radLabel_Unlock.AutoSize = false;
            this.radLabel_Unlock.BorderVisible = true;
            this.radLabel_Unlock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Unlock.Image = global::PC_Shop24Hrs.Properties.Resources.TagLO;
            this.radLabel_Unlock.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel_Unlock.Location = new System.Drawing.Point(68, 227);
            this.radLabel_Unlock.Name = "radLabel_Unlock";
            this.radLabel_Unlock.Size = new System.Drawing.Size(29, 34);
            this.radLabel_Unlock.TabIndex = 6;
            this.radLabel_Unlock.Click += new System.EventHandler(this.RadLabel_Unlock_Click);
            // 
            // radLabel_Lock
            // 
            this.radLabel_Lock.AutoSize = false;
            this.radLabel_Lock.BorderVisible = true;
            this.radLabel_Lock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Lock.Image = global::PC_Shop24Hrs.Properties.Resources.TagLO;
            this.radLabel_Lock.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel_Lock.Location = new System.Drawing.Point(69, 162);
            this.radLabel_Lock.Name = "radLabel_Lock";
            this.radLabel_Lock.Size = new System.Drawing.Size(29, 34);
            this.radLabel_Lock.TabIndex = 4;
            this.radLabel_Lock.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel_Lock.Click += new System.EventHandler(this.RadLabel_Lock_Click);
            // 
            // RadTextBox_BranckUnLock
            // 
            this.RadTextBox_BranckUnLock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_BranckUnLock.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BranckUnLock.Location = new System.Drawing.Point(132, 234);
            this.RadTextBox_BranckUnLock.MaxLength = 3;
            this.RadTextBox_BranckUnLock.Name = "RadTextBox_BranckUnLock";
            this.RadTextBox_BranckUnLock.Size = new System.Drawing.Size(72, 21);
            this.RadTextBox_BranckUnLock.TabIndex = 5;
            this.RadTextBox_BranckUnLock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BranckUnLock_KeyDown);
            // 
            // RadTextBox_BranckLock
            // 
            this.RadTextBox_BranckLock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_BranckLock.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BranckLock.Location = new System.Drawing.Point(134, 169);
            this.RadTextBox_BranckLock.MaxLength = 3;
            this.RadTextBox_BranckLock.Name = "RadTextBox_BranckLock";
            this.RadTextBox_BranckLock.Size = new System.Drawing.Size(74, 21);
            this.RadTextBox_BranckLock.TabIndex = 3;
            this.RadTextBox_BranckLock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BranckLock_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(70, 202);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(152, 19);
            this.radLabel2.TabIndex = 56;
            this.radLabel2.Text = "สาขา unlock .  [Enter]";
            // 
            // RadTextBox_LO
            // 
            this.RadTextBox_LO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_LO.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_LO.Location = new System.Drawing.Point(24, 108);
            this.RadTextBox_LO.MaxLength = 10;
            this.RadTextBox_LO.Name = "RadTextBox_LO";
            this.RadTextBox_LO.Size = new System.Drawing.Size(139, 21);
            this.RadTextBox_LO.TabIndex = 2;
            this.RadTextBox_LO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_LO_KeyDown);
            // 
            // RadTextBox_Remark
            // 
            this.RadTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Remark.Location = new System.Drawing.Point(24, 290);
            this.RadTextBox_Remark.MaxLength = 200000;
            this.RadTextBox_Remark.Multiline = true;
            this.RadTextBox_Remark.Name = "RadTextBox_Remark";
            // 
            // 
            // 
            this.RadTextBox_Remark.RootElement.StretchVertically = true;
            this.RadTextBox_Remark.Size = new System.Drawing.Size(378, 73);
            this.RadTextBox_Remark.TabIndex = 8;
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(181, 31);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(67, 19);
            this.radLabel6.TabIndex = 55;
            this.radLabel6.Text = "TAGFULL";
            // 
            // RadTextBox_TAGFull
            // 
            this.RadTextBox_TAGFull.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_TAGFull.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_TAGFull.Location = new System.Drawing.Point(181, 56);
            this.RadTextBox_TAGFull.MaxLength = 8;
            this.RadTextBox_TAGFull.Name = "RadTextBox_TAGFull";
            this.RadTextBox_TAGFull.Size = new System.Drawing.Size(221, 21);
            this.RadTextBox_TAGFull.TabIndex = 1;
            this.RadTextBox_TAGFull.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_TAGFull_KeyDown);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(26, 31);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(34, 19);
            this.radLabel5.TabIndex = 53;
            this.radLabel5.Text = "TAG";
            // 
            // RadTextBox_TAG
            // 
            this.RadTextBox_TAG.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_TAG.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_TAG.Location = new System.Drawing.Point(24, 56);
            this.RadTextBox_TAG.MaxLength = 3;
            this.RadTextBox_TAG.Name = "RadTextBox_TAG";
            this.RadTextBox_TAG.Size = new System.Drawing.Size(139, 21);
            this.RadTextBox_TAG.TabIndex = 2;
            this.RadTextBox_TAG.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Dpt_TextChanging);
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(214, 372);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.RadButton_Cancel.TabIndex = 10;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(116, 372);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(92, 32);
            this.RadButton_Save.TabIndex = 9;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(34, 264);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(110, 27);
            this.radLabel1.TabIndex = 39;
            this.radLabel1.Text = "หมายเหตุ";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(26, 83);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(25, 19);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "LO";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(69, 135);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(135, 19);
            this.radLabel3.TabIndex = 43;
            this.radLabel3.Text = "สาขา lock .  [Enter]";
            // 
            // TagLock_Unlock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.RadButton_Cancel;
            this.ClientSize = new System.Drawing.Size(429, 418);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagLock_Unlock";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TagLock_Unlock";
            this.Load += new System.EventHandler(this.TagLock_Unlock_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_BranchLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_BranchUnlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Key)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Lock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranckUnLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranckLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_LO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_TAGFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_TAG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_TAG;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_TAGFull;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Remark;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BranckLock;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_LO;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BranckUnLock;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Key;
        private Telerik.WinControls.UI.RadLabel radLabel_Unlock;
        private Telerik.WinControls.UI.RadLabel radLabel_Lock;
        private Telerik.WinControls.UI.RadLabel RadLabel_BranchLock;
        private Telerik.WinControls.UI.RadLabel RadLabel_BranchUnlock;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
    }
}
