﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class TagSumAll : Telerik.WinControls.UI.RadForm
    { 
        public TagSumAll()
        {
            InitializeComponent();
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Branch", "สาขา", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อ", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TagDpt", "แผนก", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CountTagAll", "ทั้งหมด", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CountTagUse", "ใช้แล้ว", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CountTagAva", "เหลือ", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CountTagBeUse", "ระหว่างใช้", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CountTagToDay", "บันทึกล่าสุด", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("MaxDate", "วันที่ล่าสุด", 150));

            DatagridClass.SetCellBackClolorByExpression("CountTagAva", "CountTagAva <=  150 ", ConfigClass.SetColor_Red(), RadGridView_Show);

        }


        private void ShowDGV()
        {
            Cursor.Current = Cursors.WaitCursor;
            DataTable DtDgv = LogisticClass.GetDataTagAll();// GetTag_All();
            RadGridView_Show.DataSource = DtDgv;
            Cursor.Current = Cursors.Default;
        }

        private void TagSumAll_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            ShowDGV();
        }


        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        private void RadGridView_Show_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) ShowDGV();
        }
    }
}
