﻿namespace PC_Shop24Hrs.ComMinimart.Tag
{
    partial class TagDetailData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagDetailData));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_ShowBillDetail = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_ShowBill = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Driver = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Car = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_LO = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBillDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBillDetail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBill.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Driver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Car)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_LO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowBillDetail, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowBill, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(844, 612);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // RadGridView_ShowBillDetail
            // 
            this.RadGridView_ShowBillDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowBillDetail.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowBillDetail.Location = new System.Drawing.Point(3, 253);
            // 
            // 
            // 
            this.RadGridView_ShowBillDetail.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowBillDetail.Name = "RadGridView_ShowBillDetail";
            // 
            // 
            // 
            this.RadGridView_ShowBillDetail.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowBillDetail.Size = new System.Drawing.Size(838, 356);
            this.RadGridView_ShowBillDetail.TabIndex = 1;
            this.RadGridView_ShowBillDetail.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowBillDetail_ViewCellFormatting);
            this.RadGridView_ShowBillDetail.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowBillDetail_CellDoubleClick);
            // 
            // RadGridView_ShowBill
            // 
            this.RadGridView_ShowBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowBill.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowBill.Location = new System.Drawing.Point(3, 103);
            // 
            // 
            // 
            this.RadGridView_ShowBill.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_ShowBill.Name = "RadGridView_ShowBill";
            // 
            // 
            // 
            this.RadGridView_ShowBill.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowBill.Size = new System.Drawing.Size(838, 144);
            this.RadGridView_ShowBill.TabIndex = 0;
            this.RadGridView_ShowBill.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowBill_ViewCellFormatting);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radLabel_dept);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Controls.Add(this.radLabel5);
            this.radPanel1.Controls.Add(this.radLabel6);
            this.radPanel1.Controls.Add(this.radLabel_Remark);
            this.radPanel1.Controls.Add(this.radLabel_Driver);
            this.radPanel1.Controls.Add(this.radLabel_Car);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.radLabel_LO);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(838, 94);
            this.radPanel1.TabIndex = 2;
            // 
            // radLabel_dept
            // 
            this.radLabel_dept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_dept.AutoSize = false;
            this.radLabel_dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_dept.Location = new System.Drawing.Point(605, 9);
            this.radLabel_dept.Name = "radLabel_dept";
            this.radLabel_dept.Size = new System.Drawing.Size(224, 23);
            this.radLabel_dept.TabIndex = 60;
            this.radLabel_dept.Text = "แผนก";
            this.radLabel_dept.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(379, 37);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(14, 23);
            this.radLabel4.TabIndex = 63;
            this.radLabel4.Text = ":";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.ForeColor = System.Drawing.Color.Black;
            this.radLabel5.Location = new System.Drawing.Point(379, 65);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(14, 23);
            this.radLabel5.TabIndex = 62;
            this.radLabel5.Text = ":";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.ForeColor = System.Drawing.Color.Black;
            this.radLabel6.Location = new System.Drawing.Point(379, 9);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(14, 23);
            this.radLabel6.TabIndex = 61;
            this.radLabel6.Text = ":";
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.AutoSize = false;
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Remark.Location = new System.Drawing.Point(409, 37);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(420, 23);
            this.radLabel_Remark.TabIndex = 60;
            this.radLabel_Remark.Text = "หมายเหตุ";
            // 
            // radLabel_Driver
            // 
            this.radLabel_Driver.AutoSize = false;
            this.radLabel_Driver.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Driver.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Driver.Location = new System.Drawing.Point(409, 65);
            this.radLabel_Driver.Name = "radLabel_Driver";
            this.radLabel_Driver.Size = new System.Drawing.Size(420, 23);
            this.radLabel_Driver.TabIndex = 59;
            this.radLabel_Driver.Text = "พขร.";
            // 
            // radLabel_Car
            // 
            this.radLabel_Car.AutoSize = false;
            this.radLabel_Car.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Car.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Car.Location = new System.Drawing.Point(409, 9);
            this.radLabel_Car.Name = "radLabel_Car";
            this.radLabel_Car.Size = new System.Drawing.Size(190, 23);
            this.radLabel_Car.TabIndex = 58;
            this.radLabel_Car.Text = "ทะเบียนรถ";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.Black;
            this.radLabel2.Location = new System.Drawing.Point(293, 37);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(86, 23);
            this.radLabel2.TabIndex = 57;
            this.radLabel2.Text = "หมายเหตุ";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.Black;
            this.radLabel3.Location = new System.Drawing.Point(293, 65);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(86, 23);
            this.radLabel3.TabIndex = 57;
            this.radLabel3.Text = "พขร.";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(293, 9);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(86, 23);
            this.radLabel1.TabIndex = 56;
            this.radLabel1.Text = "ทะเบียนรถ";
            // 
            // radLabel_LO
            // 
            this.radLabel_LO.AutoSize = false;
            this.radLabel_LO.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_LO.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_LO.Location = new System.Drawing.Point(9, 8);
            this.radLabel_LO.Name = "radLabel_LO";
            this.radLabel_LO.Size = new System.Drawing.Size(266, 52);
            this.radLabel_LO.TabIndex = 55;
            this.radLabel_LO.Text = "LO";
            this.radLabel_LO.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TagDetailData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 612);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TagDetailData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "รายละเอียดการส่งของ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.TagDetailData_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBillDetail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBillDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBill.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Driver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Car)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_LO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowBillDetail;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowBill;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel_LO;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel_Driver;
        private Telerik.WinControls.UI.RadLabel radLabel_Car;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel_dept;
    }
}
