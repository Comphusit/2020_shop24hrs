﻿namespace PC_Shop24Hrs.ComMinimart.Tag 
{
    partial class TagDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagDetail));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadRadioButton_Not = new Telerik.WinControls.UI.RadRadioButton();
            this.RadRadioButton_OK = new Telerik.WinControls.UI.RadRadioButton();
            this.RadRadioButton_ALL = new Telerik.WinControls.UI.RadRadioButton();
            this.RadRadioButton_Use = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_SH = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_Lo = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_Tag = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_Dpt = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_search = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_CheckTag = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_Tagrefresh = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_ClearLO = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_TagClear = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_Not)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_ALL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_Use)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_SH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Lo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Tag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadRadioButton_Not);
            this.panel1.Controls.Add(this.RadRadioButton_OK);
            this.panel1.Controls.Add(this.RadRadioButton_ALL);
            this.panel1.Controls.Add(this.RadRadioButton_Use);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.RadTextBox_SH);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.RadTextBox_Lo);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadTextBox_Tag);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.RadTextBox_Dpt);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Controls.Add(this.RadCheckBox_Branch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 2;
            // 
            // RadRadioButton_Not
            // 
            this.RadRadioButton_Not.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.RadRadioButton_Not.Location = new System.Drawing.Point(11, 382);
            this.RadRadioButton_Not.Name = "RadRadioButton_Not";
            this.RadRadioButton_Not.Size = new System.Drawing.Size(61, 19);
            this.RadRadioButton_Not.TabIndex = 65;
            this.RadRadioButton_Not.TabStop = false;
            this.RadRadioButton_Not.Text = "ใช้แล้ว";
            // 
            // RadRadioButton_OK
            // 
            this.RadRadioButton_OK.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.RadRadioButton_OK.Location = new System.Drawing.Point(10, 331);
            this.RadRadioButton_OK.Name = "RadRadioButton_OK";
            this.RadRadioButton_OK.Size = new System.Drawing.Size(94, 19);
            this.RadRadioButton_OK.TabIndex = 63;
            this.RadRadioButton_OK.TabStop = false;
            this.RadRadioButton_OK.Text = "ว่างพร้อมใช้";
            // 
            // RadRadioButton_ALL
            // 
            this.RadRadioButton_ALL.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.RadRadioButton_ALL.Location = new System.Drawing.Point(10, 305);
            this.RadRadioButton_ALL.Name = "RadRadioButton_ALL";
            this.RadRadioButton_ALL.Size = new System.Drawing.Size(66, 19);
            this.RadRadioButton_ALL.TabIndex = 62;
            this.RadRadioButton_ALL.TabStop = false;
            this.RadRadioButton_ALL.Text = "ทั้งหมด";
            // 
            // RadRadioButton_Use
            // 
            this.RadRadioButton_Use.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RadRadioButton_Use.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.RadRadioButton_Use.Location = new System.Drawing.Point(11, 356);
            this.RadRadioButton_Use.Name = "RadRadioButton_Use";
            this.RadRadioButton_Use.Size = new System.Drawing.Size(82, 19);
            this.RadRadioButton_Use.TabIndex = 64;
            this.RadRadioButton_Use.Text = "ระหว่างใช้";
            this.RadRadioButton_Use.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(10, 232);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(62, 19);
            this.radLabel3.TabIndex = 61;
            this.radLabel3.Text = "เลขที่ SH";
            // 
            // RadTextBox_SH
            // 
            this.RadTextBox_SH.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadTextBox_SH.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_SH.Location = new System.Drawing.Point(10, 257);
            this.RadTextBox_SH.MaxLength = 15;
            this.RadTextBox_SH.Name = "RadTextBox_SH";
            this.RadTextBox_SH.Size = new System.Drawing.Size(175, 25);
            this.RadTextBox_SH.TabIndex = 60;
            this.RadTextBox_SH.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_SH_TextChanging);
            this.RadTextBox_SH.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_SH_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(10, 176);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(61, 19);
            this.radLabel2.TabIndex = 59;
            this.radLabel2.Text = "เลขที่ LO";
            // 
            // RadTextBox_Lo
            // 
            this.RadTextBox_Lo.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadTextBox_Lo.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Lo.Location = new System.Drawing.Point(10, 201);
            this.RadTextBox_Lo.MaxLength = 15;
            this.RadTextBox_Lo.Name = "RadTextBox_Lo";
            this.RadTextBox_Lo.Size = new System.Drawing.Size(175, 25);
            this.RadTextBox_Lo.TabIndex = 58;
            this.RadTextBox_Lo.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Lo_TextChanging);
            this.RadTextBox_Lo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Lo_KeyDown);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(10, 120);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(70, 19);
            this.radLabel1.TabIndex = 57;
            this.radLabel1.Text = "เลขที่ TAG";
            // 
            // RadTextBox_Tag
            // 
            this.RadTextBox_Tag.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadTextBox_Tag.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Tag.Location = new System.Drawing.Point(10, 145);
            this.RadTextBox_Tag.MaxLength = 8;
            this.RadTextBox_Tag.Name = "RadTextBox_Tag";
            this.RadTextBox_Tag.Size = new System.Drawing.Size(175, 25);
            this.RadTextBox_Tag.TabIndex = 56;
            this.RadTextBox_Tag.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Tag_TextChanging);
            this.RadTextBox_Tag.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Tag_KeyDown);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(10, 64);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(75, 19);
            this.radLabel5.TabIndex = 55;
            this.radLabel5.Text = "แผนก TAG";
            // 
            // RadTextBox_Dpt
            // 
            this.RadTextBox_Dpt.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadTextBox_Dpt.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Dpt.Location = new System.Drawing.Point(10, 89);
            this.RadTextBox_Dpt.MaxLength = 3;
            this.RadTextBox_Dpt.Name = "RadTextBox_Dpt";
            this.RadTextBox_Dpt.Size = new System.Drawing.Size(175, 25);
            this.RadTextBox_Dpt.TabIndex = 54;
            this.RadTextBox_Dpt.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Dpt_TextChanging);
            this.RadTextBox_Dpt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Dpt_KeyDown);
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 444);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 30;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.DropDownAnimationEnabled = false;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Branch.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(10, 37);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Branch.TabIndex = 29;
            this.RadDropDownList_Branch.Text = "radDropDownList1";
            // 
            // RadCheckBox_Branch
            // 
            this.RadCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Branch.Location = new System.Drawing.Point(10, 14);
            this.RadCheckBox_Branch.Name = "RadCheckBox_Branch";
            this.RadCheckBox_Branch.Size = new System.Drawing.Size(76, 19);
            this.RadCheckBox_Branch.TabIndex = 28;
            this.RadCheckBox_Branch.Text = "ระบุสาขา";
            this.RadCheckBox_Branch.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Branch_CheckStateChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator1,
            this.RadButtonElement_search,
            this.commandBarSeparator2,
            this.RadButtonElement_CheckTag,
            this.commandBarSeparator3,
            this.RadButtonElement_Tagrefresh,
            this.commandBarSeparator4,
            this.RadButtonElement_ClearLO,
            this.commandBarSeparator5,
            this.RadButtonElement_TagClear,
            this.commandBarSeparator6});
            this.radStatusStrip1.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(617, 44);
            this.radStatusStrip1.TabIndex = 8;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_search
            // 
            this.RadButtonElement_search.AutoSize = true;
            this.RadButtonElement_search.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_search.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButtonElement_search.Name = "RadButtonElement_search";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_search, false);
            this.RadButtonElement_search.Text = "radButtonElement2";
            this.RadButtonElement_search.ToolTipText = "แก้ไข";
            this.RadButtonElement_search.UseCompatibleTextRendering = false;
            this.RadButtonElement_search.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_CheckTag
            // 
            this.RadButtonElement_CheckTag.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_CheckTag.Image = global::PC_Shop24Hrs.Properties.Resources.TagLock_unlock;
            this.RadButtonElement_CheckTag.Name = "RadButtonElement_CheckTag";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_CheckTag, false);
            this.RadButtonElement_CheckTag.Text = "radButtonElement1";
            this.RadButtonElement_CheckTag.UseCompatibleTextRendering = false;
            this.RadButtonElement_CheckTag.Click += new System.EventHandler(this.RadButtonElement_CheckTag_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_Tagrefresh
            // 
            this.RadButtonElement_Tagrefresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_Tagrefresh.Image = global::PC_Shop24Hrs.Properties.Resources.TagRefresh;
            this.RadButtonElement_Tagrefresh.Name = "RadButtonElement_Tagrefresh";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_Tagrefresh, false);
            this.RadButtonElement_Tagrefresh.Text = "radButtonElement2";
            this.RadButtonElement_Tagrefresh.UseCompatibleTextRendering = false;
            this.RadButtonElement_Tagrefresh.Click += new System.EventHandler(this.RadButtonElement_Tagrefresh_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_ClearLO
            // 
            this.RadButtonElement_ClearLO.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_ClearLO.Image = global::PC_Shop24Hrs.Properties.Resources.TagLO;
            this.RadButtonElement_ClearLO.Name = "RadButtonElement_ClearLO";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_ClearLO, false);
            this.RadButtonElement_ClearLO.Text = "radButtonElement3";
            this.RadButtonElement_ClearLO.UseCompatibleTextRendering = false;
            this.RadButtonElement_ClearLO.Click += new System.EventHandler(this.RadButtonElement_ClearLO_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_TagClear
            // 
            this.RadButtonElement_TagClear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_TagClear.Image = global::PC_Shop24Hrs.Properties.Resources.TagRemove;
            this.RadButtonElement_TagClear.Name = "RadButtonElement_TagClear";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_TagClear, false);
            this.RadButtonElement_TagClear.Text = "radButtonElement4";
            this.RadButtonElement_TagClear.UseCompatibleTextRendering = false;
            this.RadButtonElement_TagClear.Click += new System.EventHandler(this.RadButtonElement_TagClear_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 53);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 572);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // TagDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TagDetail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "รายละเอียด Tag";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.TagDetail_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_Not)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_ALL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_Use)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_SH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Lo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Tag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Branch;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Lo;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Tag;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Dpt;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_SH;
        private Telerik.WinControls.UI.RadRadioButton RadRadioButton_Not;
        private Telerik.WinControls.UI.RadRadioButton RadRadioButton_OK;
        private Telerik.WinControls.UI.RadRadioButton RadRadioButton_ALL;
        private Telerik.WinControls.UI.RadRadioButton RadRadioButton_Use;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_search;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_Tagrefresh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_ClearLO;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_TagClear;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_CheckTag;
    }
}
