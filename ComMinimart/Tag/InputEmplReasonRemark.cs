﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Data;

namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class InputEmplReasonRemark : Telerik.WinControls.UI.RadForm
    {
        public string pSta; //ส่งตัวแปรจากของพี่จี๊ด โดยที่ไม่ต้องระบุรหัสพนักงานที่โดนหัก 

        public string pInputData, ReasonId,ReasonName,ReasonDeduct,EmplId,EmplName;
        public string[] reason;
        //readonly string sTANumberOtText; //0 ต้องการเปนตัวเลข 1 ต้องการเป็นข้อความ
        readonly String _ValueMember, _DataMember;
        readonly String _ReasonKey, _ReasonTag, _Reason;
        readonly DataTable _DtDtDropdown;

        //RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value + System.Environment.NewLine +
        //                    BranchId + " : " + BranchName, 
        //                    EmplId, EmplName,
        //                    "เหตุผล", TagImage.GetReason(), "SHOW_ID", "SHOW_NAMEDESC", Reason,
        //                    "หมายเหตุเพิ่มเติม",  ReasonKey, ReasonTag))


        public InputEmplReasonRemark(  string HeaderLable, 
            string EmplID, string EmplName,
            string HeaderDropdown,
            DataTable DtDropdown, String ValueMember, String DataMember, string Reason,
            string SubLabelReason,  string ReasonKey, string ReasonTag)
        {
            InitializeComponent();
            radLabel_Show.Text = HeaderLable;
            radLabel_Input.Text = SubLabelReason;                                      
            if (EmplID != "")
            {
               
                radTextBox_EmplID.Text = EmplID;
                radLabel_EmplName.Text = EmplName;
            }
            else 
            {
                radTextBox_EmplID.Enabled = false;
                radTextBox_EmplID.Text = string.Empty;
                radLabel_EmplName.Text = string.Empty;
            }

            _ReasonKey = ReasonKey;
            _ReasonTag = ReasonTag; 
            _Reason = Reason;

            radLabel_HeaderCombobox.Text = HeaderDropdown;
            _DtDtDropdown = DtDropdown;
            _ValueMember = ValueMember;
            _DataMember = DataMember;

        }
 

        private void RadTextBox_EmplID_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_EmplID.Text.Length < 8) radLabel_EmplName.Text = string.Empty;
            if (radTextBox_EmplID.Text.Length == 7)
            {
                DataTable DtEmpl = Models.EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_EmplID.Text); 
                if(DtEmpl.Rows.Count>0) radLabel_EmplName.Text = DtEmpl.Rows[0]["SPC_NAME"].ToString();
            }
           
        }

        void SetCombobox(DataTable DtCombobox, String ValueMember, String DataMember)
        {
            radDropDownList_Detail.DataSource = DtCombobox;
            radDropDownList_Detail.ValueMember = ValueMember;
            radDropDownList_Detail.DisplayMember = DataMember;
        }
        //load
        private void InputEmplReasonRemark_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Detail); 

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radTextBox_Input.Text = pInputData; 
            SetCombobox(_DtDtDropdown, _ValueMember, _DataMember); 

            if (_Reason != "")
            {
                pInputData = _ReasonTag + "" + _ReasonKey;
                radTextBox_Input.Text = pInputData;
                radDropDownList_Detail.SelectedValue = _Reason;
            }

            radTextBox_Input.SelectAll();
            radTextBox_Input.Focus();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            EnterData();
        }

        void EnterData()
        {
            if (pSta == "2")//กรณีของพี่จี๊ด เท่านั้น จะไม่เช็ค รหัส พนง
            {
                EmplId = radTextBox_EmplID.Text;
                EmplName = radLabel_EmplName.Text;

                ReasonId = radDropDownList_Detail.SelectedValue.ToString();
                reason = radDropDownList_Detail.SelectedItem[1].ToString().Split(',');
                ReasonName = reason[0];
                ReasonDeduct = reason[1];
                pInputData = radTextBox_Input.Text;
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else
            {
                if (radLabel_EmplName.Text.Length == 0)
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันไม่ใส่รหัสพนักงานหักเงิน และ ไม่ทำรายการหัก หรือไม่") == DialogResult.Yes)
                    {
                        //radTextBox_EmplID
                        radTextBox_EmplID.Enabled = true;
                        radTextBox_Input.Focus();
                        this.DialogResult = DialogResult.No;
                        this.Close();
                    }
                    else
                    {
                        radTextBox_EmplID.SelectAll();
                        radTextBox_EmplID.Focus();
                    }
                }
                else
                {
                    EmplId = radTextBox_EmplID.Text;
                    EmplName = radLabel_EmplName.Text;

                    ReasonId = radDropDownList_Detail.SelectedValue.ToString();
                    reason = radDropDownList_Detail.SelectedItem[1].ToString().Split(',');
                    ReasonName = reason[0];
                    ReasonDeduct = reason[1];
                    pInputData = radTextBox_Input.Text;
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
            }

           

        }

        //Enter
        private void RadTextBox_Input_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    EnterData();
                    break;
                default:
                    break;
            }
        }
    }
}
