﻿namespace PC_Shop24Hrs.ComMinimart.Tag
{
    partial class InputEmplReasonRemark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputEmplReasonRemark));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Show = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Input = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList_Detail = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_HeaderCombobox = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_EmplID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_HeaderCombobox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(68, 302);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 1;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(164, 302);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Input.Location = new System.Drawing.Point(23, 200);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(76, 19);
            this.radLabel_Input.TabIndex = 24;
            this.radLabel_Input.Text = "LB_INPUT";
            // 
            // radLabel_Show
            // 
            this.radLabel_Show.AutoSize = false;
            this.radLabel_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Show.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Show.Location = new System.Drawing.Point(24, 7);
            this.radLabel_Show.Name = "radLabel_Show";
            this.radLabel_Show.Size = new System.Drawing.Size(286, 53);
            this.radLabel_Show.TabIndex = 25;
            this.radLabel_Show.Text = "LB_SHOW";
            // 
            // radTextBox_Input
            // 
            this.radTextBox_Input.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Input.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Input.Location = new System.Drawing.Point(24, 225);
            this.radTextBox_Input.Multiline = true;
            this.radTextBox_Input.Name = "radTextBox_Input";
            // 
            // 
            // 
            this.radTextBox_Input.RootElement.StretchVertically = true;
            this.radTextBox_Input.Size = new System.Drawing.Size(286, 71);
            this.radTextBox_Input.TabIndex = 0;
            this.radTextBox_Input.Tag = "";
            this.radTextBox_Input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Input_KeyDown);
            // 
            // radDropDownList_Detail
            // 
            this.radDropDownList_Detail.DropDownAnimationEnabled = false;
            this.radDropDownList_Detail.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Detail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Detail.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Detail.Location = new System.Drawing.Point(24, 161);
            this.radDropDownList_Detail.Name = "radDropDownList_Detail";
            this.radDropDownList_Detail.Size = new System.Drawing.Size(286, 21);
            this.radDropDownList_Detail.TabIndex = 70;
            this.radDropDownList_Detail.Text = "radDropDownList1";
            // 
            // radLabel_HeaderCombobox
            // 
            this.radLabel_HeaderCombobox.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_HeaderCombobox.Location = new System.Drawing.Point(24, 133);
            this.radLabel_HeaderCombobox.Name = "radLabel_HeaderCombobox";
            this.radLabel_HeaderCombobox.Size = new System.Drawing.Size(127, 19);
            this.radLabel_HeaderCombobox.TabIndex = 71;
            this.radLabel_HeaderCombobox.Text = "HeaderCombobox";
            // 
            // radTextBox_EmplID
            // 
            this.radTextBox_EmplID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmplID.Location = new System.Drawing.Point(175, 66);
            this.radTextBox_EmplID.MaxLength = 7;
            this.radTextBox_EmplID.Name = "radTextBox_EmplID";
            this.radTextBox_EmplID.Size = new System.Drawing.Size(135, 25);
            this.radTextBox_EmplID.TabIndex = 74;
            this.radTextBox_EmplID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmplID_KeyUp);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(24, 72);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(84, 19);
            this.radLabel1.TabIndex = 72;
            this.radLabel1.Text = "รหัสพนักงาน";
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplName.Location = new System.Drawing.Point(24, 102);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(78, 19);
            this.radLabel_EmplName.TabIndex = 73;
            this.radLabel_EmplName.Text = "ชื่อพนักงาน";
            // 
            // InputEmplReasonRemark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(343, 347);
            this.Controls.Add(this.radLabel_EmplName);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_EmplID);
            this.Controls.Add(this.radLabel_HeaderCombobox);
            this.Controls.Add(this.radDropDownList_Detail);
            this.Controls.Add(this.radTextBox_Input);
            this.Controls.Add(this.radLabel_Show);
            this.Controls.Add(this.radLabel_Input);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputEmplReasonRemark";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุข้อมูล";
            this.Load += new System.EventHandler(this.InputEmplReasonRemark_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_HeaderCombobox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadLabel radLabel_Show;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Input;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Detail;
        private Telerik.WinControls.UI.RadLabel radLabel_HeaderCombobox;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
    }
}
