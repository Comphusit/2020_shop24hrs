﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowImage;

namespace PC_Shop24Hrs.ComMinimart.Tag
{

    public partial class TagLock_Unlock : Telerik.WinControls.UI.RadForm
    {
        public string TagFull, Lo, BranchTag, BranchNameTag, BranchLock, BranchNameLock, BranchUnLock, BranchNameUnLock, locked, Unlocked, PathLo, TagStatus;
        public string TagNoFull;

        public TagLock_Unlock()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        //Load
        private void TagLock_Unlock_Load(object sender, EventArgs e)
        {
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            ClearData();
            RadTextBox_TAG.Text = TagNoFull;
            RadTextBox_TAGFull.Text = TagFull;

            if (locked == "0" && Unlocked == "0")
            {
                CheckTools("Lock");
                TagStatus = "Lock";
            }
            else if (locked == "1" && Unlocked == "0")
            {
                RadTextBox_LO.Text = Lo.ToString().Substring(2, 10);
                CheckTools("UnLock");
                TagStatus = "UnLock";
            }
            PathLo = PathImageClass.pPathSH_CarCloseNewLO + @"\" + Lo + @"\";
            RadButton_Save.Enabled = false;
            RadButton_Cancel.Enabled = false;
        }


        #region FUNCTION
        //clear
        void ClearData()
        {
            RadTextBox_TAG.Text = string.Empty; RadTextBox_TAG.Enabled = false;
            RadTextBox_TAGFull.Text = string.Empty; RadTextBox_TAGFull.Enabled = false;

            RadTextBox_BranckLock.Text = string.Empty; RadLabel_BranchLock.Text = string.Empty;
            RadTextBox_BranckUnLock.Text = string.Empty; RadLabel_BranchUnlock.Text = string.Empty;

            RadTextBox_Remark.Text = string.Empty; RadTextBox_Remark.Focus();
        }

        //set properties tools 
        void CheckTools(string Status)
        {
            if (Status == "Lock")
            {
                RadTextBox_TAGFull.Enabled = true;
                RadTextBox_TAGFull.SelectAll();

                RadTextBox_LO.Enabled = false;
                RadTextBox_BranckLock.Enabled = false;
                RadTextBox_Remark.Enabled = false;

                radLabel_Key.Enabled = false;
                radLabel_Unlock.Enabled = false;
                radLabel_Key.BorderVisible = false;
                radLabel_Unlock.BorderVisible = false;

                radLabel_Lock.Enabled = false;
                radLabel_Lock.BorderVisible = false;

                RadTextBox_BranckUnLock.Enabled = false;
                RadLabel_BranchUnlock.Enabled = false;
                RadLabel_BranchUnlock.Text = string.Empty;

                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = true;

            }
            else if (Status == "UnLock")
            {
                RadTextBox_LO.Enabled = false;
                radLabel_Lock.Enabled = false;
                radLabel_Lock.BorderVisible = false;
                RadTextBox_BranckLock.Text = BranchLock.Substring(2, 3);
                RadTextBox_BranckLock.Enabled = false;
                RadLabel_BranchLock.Text = BranchNameLock;


                radLabel_Key.Enabled = false;
                radLabel_Unlock.Enabled = false;
                radLabel_Key.BorderVisible = false;
                radLabel_Unlock.BorderVisible = false;


                RadTextBox_BranckUnLock.Focus();
                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = true;


            }
            else if (Status == "Save")
            {
                RadButton_Save.Enabled = true;
                RadButton_Cancel.Enabled = true;
            }
        }

        #endregion

        #region EVEN
        private void RadTextBox_TAGFull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RadTextBox_TAGFull.Text = RadTextBox_TAGFull.Text.Replace(" ", "");
                if (!(RadTextBox_TAGFull.Text.Replace(" ", "").Length == 8 || RadTextBox_TAGFull.ToString().Trim().Length == 6))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุเลขที่แท็กให้ถูกต้องก่อน ENTER.");
                    return;
                }
                if (RadTextBox_TAG.Text.Replace(" ", "").Length == 7)
                {
                    if (!(RadTextBox_TAGFull.Text.Replace(" ", "").Length > 7))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุเลขที่แท็กให้ถูกต้องก่อน ENTER.");
                        RadTextBox_TAGFull.SelectAll();
                        return;
                    }
                }

                RadTextBox_TAGFull.Enabled = false;
                RadTextBox_LO.Enabled = true;
                RadTextBox_BranckLock.Enabled = true;
                RadTextBox_Remark.Enabled = true;

                RadTextBox_LO.Focus();

            }
        }


        private void RadTextBox_LO_KeyDown(object sender, KeyEventArgs e)
        {
            if (RadTextBox_LO.Text.Length == 0) return;

            if (e.KeyCode == Keys.Enter)
            {
                Lo = "LO" + RadTextBox_LO.Text;
                DataTable DtLo = LogisticClass.GetLogisticDetail_GroupByLO("LO" + RadTextBox_LO.Text);
                if (DtLo.Rows.Count > 0)
                {
                    PathLo = PathImageClass.pPathSH_CarCloseNewLO + Lo + @"\";

                    RadTextBox_LO.Enabled = false;
                    RadTextBox_BranckLock.Text = BranchTag.Substring(2, 3);
                    RadLabel_BranchLock.Text = BranchNameTag;

                    RadTextBox_Remark.Focus();
                    radLabel_Lock.Enabled = true;
                    radLabel_Lock.BorderVisible = true;
                    RadTextBox_BranckLock.Enabled = false;
                    RadTextBox_Remark.Focus();
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบ LO ที่ต้องการค้นหา เช็คใหม่อีกครั้ง.");
                    RadTextBox_LO.SelectAll();
                }
            }
        }

        private void RadTextBox_BranckUnLock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_BranckUnLock.Text.Length != 3) return;
                string BranchName = BranchClass.GetBranchNameByID("MN" + RadTextBox_BranckUnLock.Text);
                RadLabel_BranchUnlock.Text = BranchName;

                if (RadTextBox_BranckUnLock.Text != string.Empty)
                {
                    radLabel_Unlock.Enabled = true;
                    radLabel_Unlock.BorderVisible = true;


                    if (!(RadTextBox_BranckUnLock.Text == "000"))
                    {
                        radLabel_Key.Enabled = true;
                        radLabel_Key.BorderVisible = true;
                    }

                }


                if (RadLabel_BranchUnlock.Text != string.Empty && !(radLabel_Key.Enabled) && !(radLabel_Unlock.Enabled)) CheckTools("Save");

            }
        }

        private void RadLabel_Key_Click(object sender, EventArgs e)
        {
            if (RadLabel_BranchUnlock.Text == string.Empty) return;

            string pPath = PathLo;
            string pFileName = "CarO-MN" + RadTextBox_BranckUnLock.Text + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) +
                "-" + SystemClass.SystemUserID_M +
                "_" + Lo + ",C@" +
                RadTextBox_TAGFull.Text + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, "", "", "", "", "0");
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radLabel_Key.Enabled = false;
                radLabel_Key.BorderVisible = false;
            }

            if (RadLabel_BranchUnlock.Text != string.Empty && !(radLabel_Key.Enabled) && !(radLabel_Unlock.Enabled)) CheckTools("Save");


        }

        private void RadLabel_Unlock_Click(object sender, EventArgs e)
        {
            if (RadLabel_BranchUnlock.Text == string.Empty) return;
            string pPath = PathLo;

            string type = "B";
            if (RadLabel_BranchLock.Text == "000")
            {
                type = "R";
            }
            string pFileName = "CarO-MN" + RadTextBox_BranckUnLock.Text + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) +
                "-" + SystemClass.SystemUserID_M +
                "_" + Lo + "," + type + "@" +
                RadTextBox_TAGFull.Text + ".jpg";

            UploadImage frm = new UploadImage(pPath, pFileName, "", "", "", "", "0");
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radLabel_Unlock.Enabled = false;
                radLabel_Unlock.BorderVisible = false;
            }

            if (RadLabel_BranchUnlock.Text != string.Empty && !(radLabel_Key.Enabled) && !(radLabel_Unlock.Enabled)) CheckTools("Save");

        }

        private void RadTextBox_BranckLock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_BranckUnLock.Text.Length != 3) return;
                string BranchName = BranchClass.GetBranchNameByID("MN" + RadTextBox_BranckLock.Text);
                RadLabel_BranchLock.Text = BranchName;
                if (RadLabel_BranchLock.Text != string.Empty)
                {
                    radLabel_Lock.Enabled = true;
                    radLabel_Lock.BorderVisible = true;
                }


                if (RadLabel_BranchLock.Text != string.Empty && !(radLabel_Lock.Enabled)) CheckTools("Save");

            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string SqlInsUp = string.Empty;

            if (TagStatus == "Lock")
            {
                SqlInsUp = @"Update    Shop_TagCarTruck SET LOGISTICID = '" + "LO" + RadTextBox_LO.Text + @"',StaLock = '1',
                            BranchLock = '" + "MN" + RadTextBox_BranckLock.Text + @"',BranchNameLock = '" + RadLabel_BranchLock.Text + @"',
                            WhoLock = '" + SystemClass.SystemUserID + @"',WhoNameLock = '" + SystemClass.SystemUserName + @"',
                            DateLock = convert(varchar, getdate(), 23),TimeLock = convert(varchar, getdate(), 24),
                            TagFullNumber = '" + RadTextBox_TAGFull.Text + @"',TagRemark = '" + RadTextBox_Remark.Text + @"'
                            WHERE TagNumber = '" + RadTextBox_TAG.Text + @"'";
            }
            else if (TagStatus == "UnLock")
            {
                SqlInsUp = @"Update  Shop_TagCarTruck SET StaUnlock = '1', 
                            BranchUnLock = 'MN" + RadTextBox_BranckUnLock.Text + @"',BranchNameUnLock = '" + RadLabel_BranchUnlock.Text + @"',  
                            WhoUnLock = '" + SystemClass.SystemUserID + @"',WhoNameUnLock = '" + SystemClass.SystemUserName + @"',
                            DateUnLock = convert(varchar,getdate(),23),TimeUnLock = convert(varchar,getdate(),24),TagRemark ='" + RadTextBox_Remark.Text + @"' 
                            WHERE	TagNumber = '" + RadTextBox_TAG.Text + @"' ";
            }
                        
            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(SqlInsUp));

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void RadLabel_Lock_Click(object sender, EventArgs e)
        {
            if (RadLabel_BranchLock.Text == string.Empty) return;
           
            string pPath = PathLo;
            string type = "A";
            if (RadLabel_BranchLock.Text == "000")
            {
                type = "S";
            }
            string pFileName = "CarO-MN" + RadTextBox_BranckLock.Text + "-" +
                Convert.ToString(DateTime.Now.ToString("yyyy-MM-dd")) + "-" +
                Convert.ToString(DateTime.Now.ToString("HH:mm:ss").Replace(":", "")) +
                "-" + SystemClass.SystemUserID_M +
                "_" + Lo +
                "," + type + "@" +
                RadTextBox_TAGFull.Text + ".jpg";


            UploadImage frm = new UploadImage(pPath, pFileName, "", "", "", "", "0");
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radLabel_Lock.Enabled = false;
                radLabel_Lock.BorderVisible = false;
            }
            if (RadLabel_BranchLock.Text != string.Empty && !(radLabel_Lock.Enabled)) CheckTools("Save");
        }

        #endregion

        #region CheckNumOnly
        private void RadTextBox_Dpt_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        
        #endregion

    }
}

