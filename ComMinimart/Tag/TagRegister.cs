﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;


namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class TagRegister : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dtTag = new DataTable();
        string pBchID;
        string pDptID;
        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //clear
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count > 0)
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmDelete("ข้อมูล Tag ที่ค้าง") == DialogResult.No) return;
                ClearData();
            }
            else ClearData();
        }
        public TagRegister()
        {
            InitializeComponent();
        }
        //Claer
        void ClearData()
        {
            if (dtTag.Rows.Count > 0) dtTag.Rows.Clear();

            pBchID = "";
            pDptID = "";

            RadTextBox_BranchID.Text = ""; RadTextBox_BranchID.Enabled = true;
            RadTextBox_Dpt.Text = ""; RadTextBox_Dpt.Enabled = false;
            RadTextBox_Tag.Text = ""; RadTextBox_Tag.Enabled = false;

            radLabel_BranchName.Text = ""; radLabe_DptName.Text = "";

            radButton_Save.Enabled = false;
            radCheckBox_Supc.Enabled = true; radCheckBox_Supc.Checked = true;
            RadTextBox_BranchID.Focus();
        }
        //Load
        private void TagRegister_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Supc.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TagNumber", "เลขที่ Tag", 180));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STATag", "STATag"));

            radGridView_Show.EnableFiltering = false;

            DatagridClass.SetCellBackClolorByExpression("TagNumber", "STATag = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("TagNumber", "STATag = '2' ", ConfigClass.SetColor_SkyPastel(), radGridView_Show);

            dtTag.Columns.Add("TagNumber");
            dtTag.Columns.Add("STATag");

            radGridView_Show.DataSource = dtTag;

            ClearData();
        }

        #region "CheckNumOnly"
        private void RadTextBox_Dpt_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_BranchID_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_Tag_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        #endregion

        //Bch Enter
        private void RadTextBox_BranchID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) FindDataBch();
        }
        //Dpt Enter
        private void RadTextBox_Dpt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) FindDataDpt();
        }
        //Bch
        void FindDataBch()
        {
            if (RadTextBox_BranchID.Text == "") return;
            DataTable dtBch = BranchClass.GetDetailBranchByID("MN" + RadTextBox_BranchID.Text);
            if (dtBch.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสสาขา");
                RadTextBox_BranchID.Text = "";
                RadTextBox_BranchID.Focus();
                return;
            }

            RadTextBox_BranchID.Enabled = false;
            pBchID = dtBch.Rows[0]["BRANCH_ID"].ToString();
            radLabel_BranchName.Text = dtBch.Rows[0]["BRANCH_NAME"].ToString();

            if (pBchID == "MN000")
            {
                RadTextBox_Dpt.Enabled = true;
                RadTextBox_Dpt.Focus();
                return;
            }
            else
            {
                radCheckBox_Supc.Enabled = false;
                RadTextBox_Tag.Enabled = true;
                RadTextBox_Tag.Focus();
                return;
            }
        }
        //Dpt
        void FindDataDpt()
        {
            if (RadTextBox_Dpt.Text == "") return;
            DataTable dtDpt = Models.DptClass.GetDptDetail_ByDptID("D" + RadTextBox_Dpt.Text);
            if (dtDpt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสแผนก");
                RadTextBox_Dpt.Text = "";
                RadTextBox_Dpt.Focus();
                return;
            }

            RadTextBox_Dpt.Enabled = false;
            pDptID = dtDpt.Rows[0]["NUM"].ToString();
            radLabe_DptName.Text = dtDpt.Rows[0]["DESCRIPTION"].ToString();

            radCheckBox_Supc.Enabled = false;
            RadTextBox_Tag.Enabled = true;
            RadTextBox_Tag.Focus();
            return;
        }
        //Enter Tag
        private void RadTextBox_Tag_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radCheckBox_Supc.CheckState == CheckState.Checked) FindTagSUPC900(901); else FindTag();
            }
        }
        ////Check barcode IN DB
        //int CheckBarcodeTag(String pBarcode)
        //{
        //    string sql = $@"SELECT	*	 FROM   [Shop_TagCarTruck] WITH (NOLOCK) WHERE	TagNumber = '{pBarcode}' ";
        //    return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        //}
        //Check barcode IN DB
        DataTable CheckBarcodeTagUse(String pBarcode)
        {
            string sql = $@"SELECT	*	 FROM   [Shop_TagCarTruck] WITH (NOLOCK) WHERE	TagNumber = '{pBarcode}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        void InsertDGV6(String pTag)
        {
            Double T = Convert.ToDouble(pTag);
            dtTag.Rows.Add(pTag);
            for (int i = 1; i < 10; i++)
            {
                Double TY = T + i;
                switch (Convert.ToString(TY).Length)
                {
                    case 1: dtTag.Rows.Add("00000" + Convert.ToString(TY)); break;
                    case 2: dtTag.Rows.Add("0000" + Convert.ToString(TY)); break;
                    case 3: dtTag.Rows.Add("000" + Convert.ToString(TY)); break;
                    case 4: dtTag.Rows.Add("00" + Convert.ToString(TY)); break;
                    case 5: dtTag.Rows.Add("0" + Convert.ToString(TY)); break;
                    case 6: dtTag.Rows.Add(Convert.ToString(TY)); break;
                    default:
                        break;
                }
            }
            dtTag.AcceptChanges();
        }
        void InsertDGV8(String pTag)
        {

            String CC = pTag.Substring(0, 6);
            //for (int i = 1; i < 10; i++)
            //{
            //    dtTag.Rows.Add(CC + Convert.ToString(i));
            //}
            //String y = Convert.ToString(Convert.ToInt32(CC) + 1) + "0";
            //switch (y.Length)
            //{
            //    case 1: dtTag.Rows.Add("000000" + Convert.ToString(y)); break;
            //    case 2: dtTag.Rows.Add("00000" + Convert.ToString(y)); break;
            //    case 3: dtTag.Rows.Add("0000" + Convert.ToString(y)); break;
            //    case 4: dtTag.Rows.Add("000" + Convert.ToString(y)); break;
            //    case 5: dtTag.Rows.Add("00" + Convert.ToString(y)); break;
            //    case 6: dtTag.Rows.Add("0" + Convert.ToString(y)); break;
            //    case 7: dtTag.Rows.Add(Convert.ToString(y)); break;
            //    default:
            //        break;
            //}

            //dtTag.AcceptChanges();



            this.Cursor = Cursors.WaitCursor;
            //String CC = pTag.Substring(0, 7);
            double tagC = Convert.ToDouble(RadTextBox_Tag.Text);
            string StaSave = "1";

            for (int i = 1; i < 10; i++)
            {
                //string tagInput = String.Format("{0:0000000}", tagC);
                string tagInput = CC + Convert.ToString(i);
                //dtTag.Rows.Add(CC + Convert.ToString(i));
                string StaTag = CheckTag(tagInput);
                if (StaTag == "1") StaSave = "0";

                dtTag.Rows.Add(tagInput, StaTag);
                tagC += 1;
            }

            String y = Convert.ToString(Convert.ToInt32(CC) + 1) + "0";
            string tagInputA = "";// = String.Format("{0:0000000}", y);
            switch (y.Length)
            {
                case 1: tagInputA = "000000" + Convert.ToString(y); break;
                case 2: tagInputA = "00000" + Convert.ToString(y); break;
                case 3: tagInputA = "0000" + Convert.ToString(y); break;
                case 4: tagInputA = "000" + Convert.ToString(y); break;
                case 5: tagInputA = "00" + Convert.ToString(y); break;
                case 6: tagInputA = "0" + Convert.ToString(y); break;
                case 7: tagInputA = Convert.ToString(y); break;
                default:
                    break;
            }

            string StaTagA = CheckTag(tagInputA);
            if (StaTagA == "1") StaSave = "0";
            dtTag.Rows.Add(tagInputA, StaTagA);


            dtTag.AcceptChanges();
            RadTextBox_Tag.Enabled = false;
            if (StaSave == "0") radButton_Save.Enabled = false; else radButton_Save.Enabled = true;
            radButton_Save.Focus();
            this.Cursor = Cursors.Default;

        }
        //CheckTag
        string CheckTag(string tagInput)
        {
            DataTable dtCheckTag = CheckBarcodeTagUse(tagInput);
            string StaTag;
            if (dtCheckTag.Rows.Count == 0) StaTag = "0";
            else
            {
                if ((dtCheckTag.Rows[0]["StaLock"].ToString() == "1") && (dtCheckTag.Rows[0]["StaUnlock"].ToString()) == "1") StaTag = "2"; else StaTag = "1";
            }
            return StaTag;
        }
        void FindTag()
        {
            String pTag; String pTypeTag;

            switch (RadTextBox_Tag.Text.Length)
            {
                case 6:
                    pTag = RadTextBox_Tag.Text; pTypeTag = "6";
                    break;
                case 8:
                    pTag = RadTextBox_Tag.Text.Substring(0, 7); pTypeTag = "8";
                    break;
                default:
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุบาร์โค้ดให้ครบ 6 หรือ 8 หลัก ก่อนการ Enter.");
                    return;
            }
            ////Check ว่ามีหรือยัง
            //if (CheckBarcodeTag(pTag) > 0)
            //{
            //    MsgBoxClass.MsgBoxShowButtonOk_Warning("Tag ลำดับนี้มีข้อมูลอยู่แล้ว เช็คใหม่อีกครั้ง.");
            //    RadTextBox_Tag.SelectAll();
            //    RadTextBox_Tag.Focus();
            //    return;
            //}

            switch (pTypeTag)
            {
                case "6":
                    InsertDGV6(pTag);
                    break;
                case "8":
                    InsertDGV8(pTag);
                    break;
                default:
                    break;
            }

            RadTextBox_Tag.Enabled = false;
            radButton_Save.Enabled = true;
            radButton_Save.Focus();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (pBchID == "MN000")
            {
                if ((pDptID == "") || (radLabe_DptName.Text == ""))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุแผนกเจ้าของ Tag ให้เรียบร้อยก่อนการบันทึก.");
                    RadTextBox_Dpt.SelectAll();
                    RadTextBox_Dpt.Focus();
                    return;
                }
            }

            string rmk = "";
            if (radCheckBox_Supc.CheckState == CheckState.Checked)
            {
                if ((radGridView_Show.Rows.Count != 900))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ เนื่องจากข้อมูลไม่ครบ ลองใหม่ อีกครั้ง.");
                    RadTextBox_Dpt.SelectAll();
                    RadTextBox_Dpt.Focus();
                    return;
                }
                rmk = "TagSupc";
            }
            else
            {
                if ((radGridView_Show.Rows.Count != 10))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ เนื่องจากข้อมูลไม่ครบ ลองใหม่ อีกครั้ง.");
                    RadTextBox_Dpt.SelectAll();
                    RadTextBox_Dpt.Focus();
                    return;
                }
            }

            ArrayList sqlIn = new ArrayList();
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["STATag"].Value.ToString() == "2")
                {
                    sqlIn.Add(string.Format(@"
                    UPDATE  Shop_TagCarTruck 
                    SET     TagNumber = 'A'+TagNumber
                    WHERE   TagNumber = '" + radGridView_Show.Rows[i].Cells["TagNumber"].Value.ToString() + @"'
                    "));
                }

                sqlIn.Add(String.Format(@"INSERT INTO Shop_TagCarTruck (TagNumber,Branch,BranchName,WhoIns,WhoNameIns,TagDpt,TagRemark) VALUES (" +
                    "'" + radGridView_Show.Rows[i].Cells["TagNumber"].Value.ToString() + "','" + pBchID + "','" + radLabel_BranchName.Text + "','" + SystemClass.SystemUserID + "'," +
                    "'" + SystemClass.SystemUserName + "','" + pDptID + "','" + rmk + @"' " +
                    ") "));

            }
            string T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                ClearDataForSave();
            }
        }
        //CheckBch
        private void RadTextBox_BranchID_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_BranchID.Text.Length == 3) FindDataBch();
        }
        //Dpt
        private void RadTextBox_Dpt_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_Dpt.Text.Length == 3) FindDataDpt();
        }
        //Claer
        void ClearDataForSave()
        {
            if (dtTag.Rows.Count > 0) dtTag.Rows.Clear();
            radButton_Save.Enabled = false;
            RadTextBox_Tag.Text = ""; RadTextBox_Tag.Enabled = true;
            RadTextBox_Tag.Focus();
        }
        ////Tag ที่ทำเอง
        //void FindTagSUPC()
        //{
        //    String pTag; //String pTypeTag;
        //    if (RadTextBox_Tag.Text.Length != 8)
        //    {
        //        MsgBoxClass.MsgBoxShowButtonOk_Error("ข้อมูลที่ยิงผิดพลาด เนื่องจาก Tag ต้องมี 8 หลักเท่านั้น" + Environment.NewLine + "ลองใหม่อีกครั้ง");
        //        return;
        //    }

        //    pTag = RadTextBox_Tag.Text.Substring(0, 7); ///pTypeTag = "8";

        //    ////Check ว่ามีหรือยัง
        //    //if (CheckBarcodeTag(pTag) > 0)
        //    //{
        //    //    MessageBox.Show("Tag ลำดับนี้มีข้อมูลอยู่แล้ว เช็คใหม่อีกครั้ง.",
        //    //        SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    //    RadTextBox_Tag.SelectAll();
        //    //    RadTextBox_Tag.Focus();
        //    //    return;
        //    //}

        //    String CC = pTag.Substring(0, 7);
        //    double tagC = Convert.ToDouble(CC);
        //    string StaSave = "1";
        //    for (int i = 1; i < 7; i++)
        //    {
        //        string tagA = Convert.ToString(tagC);
        //        string tagInput = tagA;
        //        switch (tagA.Length)
        //        {
        //            case 1: tagInput = "000000" + tagA; break;
        //            case 2: tagInput = "00000" + tagA; break;
        //            case 3: tagInput = "0000" + tagA; break;
        //            case 4: tagInput = "000" + tagA; break;
        //            case 5: tagInput = "00" + tagA; break;
        //            case 6: tagInput = "0" + tagA; break;
        //            case 7: tagInput = tagA; break;
        //            default:
        //                break;
        //        }
        //        DataTable dtCheckTag = CheckBarcodeTagUse(tagInput);
        //        string StaTag;
        //        if (dtCheckTag.Rows.Count == 0)
        //        {
        //            StaTag = "0";
        //        }
        //        else
        //        {
        //            if ((dtCheckTag.Rows[0]["StaLock"].ToString() == "1") && (dtCheckTag.Rows[0]["StaUnlock"].ToString()) == "1")
        //            {
        //                StaTag = "2";
        //            }
        //            else
        //            {
        //                StaSave = "0";
        //                StaTag = "1";
        //            }
        //        }
        //        //dtTag.Rows.Add(CC + Convert.ToString(i));
        //        dtTag.Rows.Add(tagInput, StaTag);
        //        tagC -= 1;
        //    }

        //    //String y = Convert.ToString(Convert.ToInt32(CC) + 1) + "0";
        //    //switch (y.Length)
        //    //{
        //    //    case 1: dtTag.Rows.Add("000000" + Convert.ToString(y)); break;
        //    //    case 2: dtTag.Rows.Add("00000" + Convert.ToString(y)); break;
        //    //    case 3: dtTag.Rows.Add("0000" + Convert.ToString(y)); break;
        //    //    case 4: dtTag.Rows.Add("000" + Convert.ToString(y)); break;
        //    //    case 5: dtTag.Rows.Add("00" + Convert.ToString(y)); break;
        //    //    case 6: dtTag.Rows.Add("0" + Convert.ToString(y)); break;
        //    //    case 7: dtTag.Rows.Add(Convert.ToString(y)); break;
        //    //    default:
        //    //        break;
        //    //}

        //    dtTag.AcceptChanges();
        //    RadTextBox_Tag.Enabled = false;
        //    if (StaSave == "0")
        //    {
        //        radButton_Save.Enabled = false;
        //    }
        //    else
        //    {
        //        radButton_Save.Enabled = true;
        //    }
        //    radButton_Save.Focus();
        //}
        //Tag ที่ทำเอง
        void FindTagSUPC900(int iR)
        {
            //String pTag; //String pTypeTag;
            //if (RadTextBox_Tag.Text.Length != 8)
            //{
            //    MsgBoxClass.MsgBoxShowButtonOk_Error("ข้อมูลที่ยิงผิดพลาด เนื่องจาก Tag ต้องมี 8 หลักเท่านั้น" + Environment.NewLine + "ลองใหม่อีกครั้ง");
            //    return;
            //}

            //pTag = RadTextBox_Tag.Text.Substring(0, 7); ///pTypeTag = "8";


            this.Cursor = Cursors.WaitCursor;
            //String CC = pTag.Substring(0, 7);
            double tagC = Convert.ToDouble(RadTextBox_Tag.Text);
            string StaSave = "1";

            for (int i = 1; i < iR; i++)
            {
                string tagInput = String.Format("{0:0000000}", tagC);
                DataTable dtCheckTag = CheckBarcodeTagUse(tagInput);
                string StaTag;
                if (dtCheckTag.Rows.Count == 0) StaTag = "0";
                else
                {
                    if ((dtCheckTag.Rows[0]["StaLock"].ToString() == "1") && (dtCheckTag.Rows[0]["StaUnlock"].ToString()) == "1")
                    {
                        StaTag = "2";
                    }
                    else
                    {
                        StaSave = "0";
                        StaTag = "1";
                    }
                }
                dtTag.Rows.Add(tagInput, StaTag);
                tagC += 1;

            }


            dtTag.AcceptChanges();
            RadTextBox_Tag.Enabled = false;
            if (StaSave == "0") radButton_Save.Enabled = false; else radButton_Save.Enabled = true;
            radButton_Save.Focus();
            this.Cursor = Cursors.Default;
        }
    }
}

