﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class TagRemark : Telerik.WinControls.UI.RadForm
    {
        public string TagNoFull, TagFull, Lo, Branch, Remarks, TypeEdit, BranchName;
       
        //Load
        private void TagRemark_Load(object sender, EventArgs e)
        {
            
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            ClearData();

            RadTextBox_TAG.Text = this.TagNoFull;
            radTextBox_TAGFull.Text = this.TagFull;

            radLabel_LO.Text = this.Lo;
            radLabel_BranchName.Text = this.Branch + "-" + this.BranchName;

            radTextBox_Remark.Text = this.Remarks;
            radTextBox_Remark.SelectAll();

        }

        #region FUNCTION
        void ClearData()
        {
            RadTextBox_TAG.Text = string.Empty; RadTextBox_TAG.Enabled = false;
            radTextBox_TAGFull.Text = string.Empty; radTextBox_TAGFull.Enabled = false;

            radLabel_LO.Text = string.Empty;
            radLabel_BranchName.Text = string.Empty;

            radTextBox_Remark.Text = string.Empty; radTextBox_Remark.Focus();

        }

        #endregion

        #region EVEN
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            using (FormShare.ChooseData chooseData = new FormShare.ChooseData("หักเงิน", "ไม่หักเงิน"))
            {
                DialogResult dr = chooseData.ShowDialog();

                if (dr == DialogResult.Yes && chooseData.sSendData == "1")
                {
                    using (FormShare.EmpListInCorrect EmpListInCorrect = new FormShare.EmpListInCorrect(RadTextBox_TAG.Text, "TAG", "D179", radTextBox_Remark.Text))
                    {
                        DialogResult drMoney = EmpListInCorrect.ShowDialog();

                        if (drMoney == DialogResult.Yes)
                        {
                        }

                    }

                }

            }


            string sql = string.Empty;
            switch (TypeEdit)
            {
                case "RefreshTag":
                    sql = @"Update	Shop_TagCarTruck SET LOGISTICID = '',
                                StaLock = '0', 
                                BranchLock = '',BranchNameLock = '',
                                WhoLock = '',WhoNameLock = '', 
                                DateLock = convert(varchar,getdate(),23),TimeLock = convert(varchar,getdate(),24),
                                StaUnlock='0',
                                BranchUnLock = '',BranchNameUnLock = '',  
                                WhoUnLock = '',WhoNameUnLock = '',
                                DateUnLock = convert(varchar,getdate(),23),TimeUnLock = convert(varchar,getdate(),24),
                                TagRemark = '" + radTextBox_Remark.Text + @"'  
                                WHERE	TagNumber = '" + RadTextBox_TAG.Text + @"' ";
                    break;
                case "ClearLO":
                    sql = @"Update	Shop_TagCarTruck SET LOGISTICID = '" + Lo + @"_1', TagRemark = '" + radTextBox_Remark.Text + @"' 
                                WHERE	LOGISTICID = '" + Lo + @"' ";
                    break;
                case "ClearTag":
                    sql = @"Update	Shop_TagCarTruck SET StaLock = '1',StaUnlock = '1', TagRemark = '" + radTextBox_Remark.Text + @"' 
                                WHERE	TagNumber = '" + RadTextBox_TAG.Text + @"' ";
                    break;
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sql));

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //clear
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        public TagRemark(String TagNoFull, String TagFull, String Lo, String Branch, String BranchName, String Remarks, String TypeEdit)
        {
            InitializeComponent();
            this.TagNoFull = TagNoFull;
            this.TagFull = TagFull;
            this.Lo = Lo;
            this.Branch = Branch;
            this.BranchName = BranchName;
            this.Remarks = Remarks;
            this.TypeEdit = TypeEdit;


        }
        //Claer

        #endregion 
    }
}

