﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using PC_Shop24Hrs.FormShare.ShowImage;

namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class TagDetailData : Telerik.WinControls.UI.RadForm
    {
        public string _Lo;
        string PathLo;
        public TagDetailData()
        {
            InitializeComponent();
            //set column RadGridView_ShowBill
            RadGridView_ShowBill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CUSTACCOUNT", "สาขา", 100));
            RadGridView_ShowBill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อ", 150));
            RadGridView_ShowBill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 120));
            RadGridView_ShowBill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BoxQty", "จำนวนลัง", 100));

            //set column radGridView_ShowBillDetail
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TagNumber", "TagNumber", 100));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Branch", "สาขา", 100));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "ใบจัดส่ง", 100));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WhoLock", "WhoLock", 100));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WhoUnLock", "WhoUnLock", 100));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeBranch", "รูปก่อนเข้าสาขา", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeBranch", "รูปก่อนเข้าสาขา", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathAfterBranch", "รูปก่อนออกสาขา", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("AfterBranch", "รูปก่อนออกสาขา", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeIn", "รูปสาขาใหญ่ [ก่อนออก]", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeIn", "รูปสาขาใหญ่ [ก่อนออก]", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeOut", "รูปสาขาใหญ่ [ก่อนเข้า]", 150));
            RadGridView_ShowBillDetail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeOut", "รูปสาขาใหญ่ [ก่อนเข้า]", 150));

            this.RadGridView_ShowBillDetail.MasterTemplate.Columns["PathBeforeBranch"].IsVisible = false;
            this.RadGridView_ShowBillDetail.MasterTemplate.Columns["PathAfterBranch"].IsVisible = false;
            this.RadGridView_ShowBillDetail.MasterTemplate.Columns["PathBeforeIn"].IsVisible = false;
            this.RadGridView_ShowBillDetail.MasterTemplate.Columns["PathBeforeOut"].IsVisible = false;
        }

        private void TagDetailData_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowBill);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowBillDetail);
            ClearFrom();
            //รายละเอียด LO
            ShowDetailLO(_Lo);
            //รายละเอียดมินิมาร์ทตาม LO
            ShowBill(_Lo);
            //รายละเอียดรูป ตามมินิมาร์ทใน LO
            ShowImgLo(_Lo);

        }

        #region FUNCTION
        //Get ShipmentID
        private string GetSH_ByLoBranch(string _LOGISTICTID, string _Branch)
        {
            DataTable dt = LogisticClass.FindSH_ByLO(_LOGISTICTID, _Branch);
            string SH = string.Empty;
            if (dt.Rows.Count > 0) SH = dt.Rows[0]["SHIPMENTID"].ToString();
            return SH;
        }
        private void ClearFrom()
        {
            radLabel_LO.Text = string.Empty;
            radLabel_Car.Text = string.Empty;
            radLabel_Remark.Text = string.Empty;
            radLabel_Driver.Text = string.Empty;

            RadGridView_ShowBill.Rows.Clear();
            RadGridView_ShowBillDetail.Rows.Clear();
        }

        private void ShowDetailLO(string Lo)
        {
            DataTable DtLo = Class.LogisticClass.GetLogisticDetail_ByLO(Lo);
            if (DtLo.Rows.Count > 0)
            {
                radLabel_LO.Text = DtLo.Rows[0]["LOGISTICID"].ToString();

                radLabel_dept.Text = DtLo.Rows[0]["DEPTLO"].ToString();
                radLabel_Car.Text = DtLo.Rows[0]["VEHICLEID"].ToString();
                radLabel_Remark.Text = DtLo.Rows[0]["TRASHREMARK"].ToString();
                radLabel_Driver.Text = DtLo.Rows[0]["EMPLDRIVER"].ToString() + " - " + DtLo.Rows[0]["SPC_NAME"].ToString();
            }
        }

        private void ShowBill(string Lo)
        {
            DataTable DtLo = Class.LogisticClass.GetDetailLO_ByLo(Lo);
            if (DtLo.Rows.Count > 0) RadGridView_ShowBill.DataSource = DtLo;
        }

        private void ShowImgLo(string Lo)
        {
            string str = $@"
            SELECT	TagNumber,TagFullNumber,LOGISTICID,Branch,BranchName,WhoNameIns,CONVERT(VARCHAR,DateIns,23) AS DateIns , 
                    StaLock, BranchLock, BranchNameLock,WhoLock, WhoNameLock, CONVERT(VARCHAR,DateLock,23) AS DateLock,TimeLock,  
                    StaUnlock, BranchUnlock, BranchNameUnlock, WhoUnlock,WhoNameUnLock, CONVERT(VARCHAR,DateUnlock,23) as DateUnlock,TimeUnlock,TagRemark,TagDpt   
            FROM	Shop_TagCarTruck WITH (NOLOCK)   
            WHERE   LOGISTICID = '{Lo}'  
            ORDER BY DateLock,TimeLock,TagNumber ";
            DataTable DtLo = ConnectionClass.SelectSQL_Main(str); //Tag_Class.GetDetailLOImg_ByLo(Lo);

            if (DtLo.Rows.Count > 0)
            {
                Bitmap img;
                DirectoryInfo di;
                string Imagename;
                FileInfo[] Images;
                PathLo = PathImageClass.pPathSH_CarCloseNewLO + @"\" + radLabel_LO.Text;
                foreach (DataRow row in DtLo.Rows)
                {
                    RadGridView_ShowBillDetail.Rows.Add(row["TagFullNumber"],
                        row["TagDpt"] + System.Environment.NewLine + row["Branch"] + System.Environment.NewLine + row["BranchName"],
                        GetSH_ByLoBranch(Lo, row["Branch"].ToString()),
                        row["BranchLock"] + System.Environment.NewLine + row["BranchNameLock"] + System.Environment.NewLine + row["DateLock"] + System.Environment.NewLine + row["TimeLock"] + System.Environment.NewLine + row["WhoLock"],
                        row["BranchUnlock"] + System.Environment.NewLine + row["BranchNameUnlock"] + System.Environment.NewLine + row["DateUnlock"] + System.Environment.NewLine + row["TimeUnlock"] + System.Environment.NewLine + row["WhoUnlock"],
                        "", "", "", "", "", "", "", "");
                    img = new Bitmap(Controllers.PathImageClass.pImageEmply);

                    RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeBranch"].Value = img;
                    RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["AfterBranch"].Value = img;

                    RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeIn"].Value = img;
                    RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeOut"].Value = img;


                    di = new DirectoryInfo(PathLo);

                    Imagename = @"CarO-*" + radLabel_LO.Text + @"*" + row["TagFullNumber"] + @"*";

                    Images = di.GetFiles(Imagename + @"*.jpg");
                    for (int i = 0; i < Images.Length; i++)
                    {
                        string[] names = Images[i].ToString().Split(',');
                        String TypeImg = names[1].Substring(0, 1);
                        img = new Bitmap(Images[i].FullName.ToString());

                        //เช็คสถานะรูป  
                        if (row["BranchLock"].ToString() == "MN000")
                        {
                            if (TypeImg == "S")
                            {

                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["PathBeforeIn"].Value = Images[i].Name.ToString();
                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeIn"].Value = img;
                            }
                            else if (TypeImg == "R")
                            {

                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["PathBeforeOut"].Value = Images[i].Name.ToString();
                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeOut"].Value = img;

                            }
                        }
                        else
                        {
                            if (TypeImg == "A" || TypeImg == "E")
                            {
                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["PathAfterBranch"].Value = Images[i].Name.ToString();
                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["AfterBranch"].Value = img;
                            }
                            else if (TypeImg == "B" || TypeImg == "F")
                            {
                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["PathBeforeBranch"].Value = Images[i].Name.ToString();
                                RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeBranch"].Value = img;
                            }

                        }
                    }
                    RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Height = 110;
                }

                //Find รูปสุดท้าย ไม่ตรงกับ MN ไม่สามารถเช็คจากเลขแท็กได้จากเงื่อนไขข้างต้น
                di = new DirectoryInfo(PathLo);
                Imagename = @"CarO-*" + radLabel_LO.Text + @"*R*" + DtLo.Rows[DtLo.Rows.Count - 1]["TagFullNumber"].ToString() + @"*.jpg";
                Images = di.GetFiles(Imagename);
                for (int i = 0; i < Images.Length; i++)
                {
                    string[] names = Images[i].ToString().Split(',');
                    string TypeImg = names[1].Substring(0, 1);
                    img = new Bitmap(Images[i].FullName.ToString());

                    if (TypeImg == "R")
                    {
                        RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["PathBeforeOut"].Value = Images[i].Name.ToString();
                        RadGridView_ShowBillDetail.Rows[RadGridView_ShowBillDetail.Rows.Count - 1].Cells["BeforeOut"].Value = img;
                    }
                }

            }
        }
        #endregion


        #region SetFontInRadGridview
        private void RadGridView_ShowBill_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowBillDetail_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        //SHOW IMAGE รูปใหญ่ใน FORM
        private void RadGridView_ShowBillDetail_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (RadGridView_ShowBillDetail.Rows.Count > 0 && !(RadGridView_ShowBillDetail.CurrentRow.Cells[e.ColumnIndex - 1].Value is null))
            {
                ShowImage frmSPC = new ShowImage()
                { pathImg = PathLo + @"|" + RadGridView_ShowBillDetail.CurrentRow.Cells[e.ColumnIndex - 1].Value.ToString() };
                if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
            }
        }

        #endregion

    }
}
