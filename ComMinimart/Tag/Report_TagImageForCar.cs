﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using PC_Shop24Hrs.FormShare.ShowImage;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class Report_TagImageForCar : Telerik.WinControls.UI.RadForm
    {
        string Dept, Route, Empl, Car, Lo, SqlDeduct, SqlBranch;

        DataTable DtLogisDept;
        readonly string _pPermissionImg; // 1 ดูรูปได้
        readonly string _pDptOpen; // 1 ดูรูปได้

        public Report_TagImageForCar(string pPermissionImg, string pDptOpen)
        {
            InitializeComponent();
            _pPermissionImg = pPermissionImg;
            _pDptOpen = pDptOpen;
        }

        private void ClearFrom(string status)
        {
            if (status == "NEW")
            {
                Dept = string.Empty;
                Route = string.Empty;
                Empl = string.Empty;
                Car = string.Empty;
                Lo = string.Empty;
                SqlDeduct = string.Empty;
                SqlBranch = string.Empty;

                if (_pDptOpen == "027") RadCheckBox_Dept.Checked = false; else RadCheckBox_Dept.Checked = true;
                
                RadDropDownList_Dept.Enabled = true;
                if (SystemClass.SystemBranchID == "MN000")
                {
                    radCheckBox_Route.Checked = false;
                    radDropDownList_Route.Enabled = false;
                    radCheckBox_Empl.Checked = false;
                    radTextBox_Empl.Enabled = false;
                }
                else
                {
                    radCheckBox_Route.Visible = false;
                    radDropDownList_Route.Visible = false;
                    radCheckBox_Empl.Visible = false;
                    radTextBox_Empl.Visible = false;
                }




                radCheckBox_Lo.Checked = false;
                radTextBox_Lo.Enabled = false;

                radCheckBox_CarNumber.Checked = false;
                radTextBox_CarNumber.Enabled = false;

                radDateTimePicker_DateLo.Value = DateTime.Now.AddDays(-1);
            }
            else if (status == "REFRESH")
            {
                Dept = string.Empty;
                Route = string.Empty;
                Empl = string.Empty;
                Car = string.Empty;
                Lo = string.Empty;
                SqlDeduct = string.Empty;
                SqlBranch = string.Empty;
            }
        }

        private void Report_TagImageForCar_Load(object sender, EventArgs e)
        {
            //แยกสาขาใน SQL TagImage ถ้าไม่ใช่ MN000 จะแสดงแค่สาขานั้นๆ
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            radButtonElement_excel.ToolTipText = "Export To Excel";
            radButtonElement_excel.ShowBorder = true;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Route);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_DateLo, DateTime.Now.AddDays(0), DateTime.Now.AddDays(0));

            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Route.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Empl.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Lo.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_CarNumber.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Remark.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_NotRemark.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadButton_Search.ButtonElement.ShowBorder = true;

            //set column radGridView_ShowBillDetail
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "รอบส่งสินค้า", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTLO", "แผนกส่ง", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ROUTEIDDESC", "สายรถ", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร.", 100));
            //RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ พขร.", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLCLERK", "เด็กท้าย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TRASHREMARK", "หมายเหตุ", 100));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "สาขา", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WhoNameLock", "ล็อคโดย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WhoNameUnLock", "ปล๊ดล็อคโดย", 100));

            if (_pPermissionImg == "1")
            {
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathKey", "PathKey", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("Key", "รูปกุญแจ", 120));
                this.RadGridView_Show.MasterTemplate.Columns["PathKey"].IsVisible = false;
            }

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("KeyReason", "ตรวจสอบกุญแจ", 150));

            if (_pPermissionImg == "1")
            {
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeBranch", "PathBeforeBranch", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeBranch", "รูปก่อนเข้าสาขา", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathAfterBranch", "PathAfterBranch", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("AfterBranch", "รูปก่อนออกสาขา", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeIn", "PathBeforeIn", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeIn", "รูปสาขาใหญ่ " + System.Environment.NewLine + @"[ก่อนออก]", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeOut", "PathBeforeOut", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeOut", "รูปสาขาใหญ่ " + System.Environment.NewLine + @"[ก่อนเข้า]", 120));
                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathAfterBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeIn"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeOut"].IsVisible = false;
                RadGridView_Show.TableElement.RowHeight = 100;
            }

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ReasonImg", "หมายเหตุ", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TagNumber", "TagNumber"));


            //this.RadGridView_Show.MasterTemplate.Columns["TagNumber"].IsVisible = false;

            RadGridView_Show.Columns["LOGISTICID"].IsPinned = true;
            RadGridView_Show.Columns["DEPTLO"].IsPinned = true;

            RadGridView_Show.MasterTemplate.EnableSorting = false;
          
            if (_pDptOpen == "027")
            {
                this.RadGridView_Show.MasterTemplate.Columns["Key"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathKey"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["KeyReason"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["BeforeBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathAfterBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["AfterBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["ReasonImg"].IsVisible = false;

                RadCheckBox_Remark.Visible = false;
                RadCheckBox_Branch.Visible = false;
                RadCheckBox_NotRemark.Visible = false;
            }

            //ต้องการให้สีเปลี่ยน แต่ยังไม่เปลี่ยน ยังทำไม่ได้
            ConditionalFormattingObject c1 = new ConditionalFormattingObject("WhoNameUnLock", ConditionTypes.Equal, "", "", true)
            {
                RowBackColor = ConfigClass.SetColor_Red(),
                CellBackColor = ConfigClass.SetColor_Red()
            };
            RadGridView_Show.Columns["WhoNameUnLock"].ConditionalFormattingObjectList.Add(c1);

            DatagridClass.SetCellBackClolorByExpression("WhoNameUnLock", "WhoNameUnLock ='' ", ConfigClass.SetColor_SkyPastel(), RadGridView_Show);

            SetDropdownlistDeptLogis(Class.LogisticClass.GetLogisticDept());
            SetDropdownlistRoute(Class.LogisticClass.GetRoute("9"));

            ClearFrom("NEW");
        }

        //set dept
        private void SetDropdownlistDeptLogis(DataTable DeptLogis)
        {
            RadDropDownList_Dept.DataSource = DeptLogis;
            RadDropDownList_Dept.DisplayMember = "DEPTNAME";
            RadDropDownList_Dept.ValueMember = "DEPTID";
        }
        //set Route
        private void SetDropdownlistRoute(DataTable DeptLogis)
        {
            radDropDownList_Route.DataSource = DeptLogis;
            radDropDownList_Route.DisplayMember = "ROUTENAME";
            radDropDownList_Route.ValueMember = "ROUTEID";
        }


        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows


        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        //SHOW IMAGE รูปใหญ่ใน FORM



        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {

            if (RadGridView_Show.Rows.Count > 0 && !(RadGridView_Show.CurrentRow.Cells[e.ColumnIndex - 1].Value is null))
            {
                if (e.ColumnIndex == 11 || e.ColumnIndex == 14 || e.ColumnIndex == 16 || e.ColumnIndex == 18 || e.ColumnIndex == 20)
                {
                    string PathLo = Controllers.PathImageClass.pPathSH_CarCloseNewLO + @"\" +
                        RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString();
                    ShowImage frmSPC = new ShowImage()
                    {
                        pathImg = PathLo + @"|" + RadGridView_Show.CurrentRow.Cells[e.ColumnIndex - 1].Value.ToString()
                    };
                    if (frmSPC.ShowDialog(this) == DialogResult.OK)
                    { }
                }
                else if (e.ColumnIndex == 21 || e.ColumnIndex == 12)
                {

                    string TypeSql,
                        TypeImg = string.Empty,
                        EmplId = string.Empty,
                        EmplName = string.Empty,
                        BranchId,
                        BranchName,
                        ReasonKey = string.Empty,
                        ReasonTag = string.Empty,
                        Reason = string.Empty;

                    string[] EmplDesc, BranchDesc;

                    BranchDesc = RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString().Split(',');
                    BranchId = BranchDesc[0];
                    BranchName = BranchDesc[1].Replace("\r\n", "");

                    if (BranchId == "MN000") return;
                    if (e.ColumnIndex == 12 && BranchId != "MN000") TypeImg = "Keys";
                    if (e.ColumnIndex == 21 && BranchId != "MN000") TypeImg = "LockUnLock";

                    if (e.Value is null)
                    {
                        TypeSql = "Ins";
                        //KEYS
                        if (e.ColumnIndex == 12 && BranchId != "MN000")
                        {
                            EmplDesc = RadGridView_Show.CurrentRow.Cells["WhoNameUnLock"].Value.ToString().Split(',');
                            EmplId = EmplDesc[0];
                            EmplName = EmplDesc[1].Replace("\r\n", "");
                        }
                        //LOCK UNLOCK
                        else if (e.ColumnIndex == 21 && BranchId != "MN000")
                        {
                            EmplDesc = RadGridView_Show.CurrentRow.Cells["WhoNameLock"].Value.ToString().Split(',');
                            EmplId = EmplDesc[0];
                            EmplName = EmplDesc[1].Replace("\r\n", "");
                        }
                    }
                    else
                    {

                        TypeSql = "Up";
                        String[] ValuesColumns = e.Value.ToString().Replace("\r\n", "").Split(',');
                        EmplId = ValuesColumns[0];
                        EmplName = ValuesColumns[1];
                        ReasonKey = ValuesColumns[2];
                        ReasonTag = ValuesColumns[3];
                        Reason = ValuesColumns[4];
                    }

                    using (InputEmplReasonRemark InputEmplReasonRemark = new InputEmplReasonRemark(
                            RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value + System.Environment.NewLine +
                            BranchId + " : " + BranchName,
                            EmplId, EmplName,
                            "เหตุผล", ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("29", "", "order by SHOW_ID", "1"), "SHOW_ID", "SHOW_NAMEDESC", Reason,
                            "หมายเหตุเพิ่มเติม", ReasonKey, ReasonTag))
                    {
                        DialogResult dr = InputEmplReasonRemark.ShowDialog();
                        if (dr == DialogResult.Yes)
                        {

                            if (e.ColumnIndex == 12) ReasonKey = InputEmplReasonRemark.pInputData;
                            if (e.ColumnIndex == 21) ReasonTag = InputEmplReasonRemark.pInputData;
                            string sql;

                            if (TypeSql == "Ins")
                            {
                                sql = string.Format(@"insert into SHOP_RecheckReciveItem
                                (
                                    INVOICEID, DATEBILL, BRANCH, VAN, ITEMBARCODE, SPC_ITEMNAME, UNITID, 
                                    QtySend, QtyRecive, QtyCenter, QtyPurchase, SHIPMENTID, 
                                    WHOINS, WHOINSNAME, DATEINS, TIMEINS, DESCRIPTION, WhoDeduct,	
                                    WhoNameDeduct, 
                                    KeyReason, ReasonImg, ReasonID, ReasonName, ReasonDeduct,Path
                                )
                                values
                                (
                                    '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '', '', '', '', '{7}', '{8}', '{9}',
                                    convert(varchar, getdate(), 23),convert(varchar, getdate(), 24),
                                    '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}'
                                )",
                                RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString(), //0
                                radDateTimePicker_DateLo.Value.ToString("yyyy-MM-dd"),//1
                                BranchId + ' ' + BranchName, //2
                                "TAG",//3
                                TypeImg,//4
                                RadGridView_Show.CurrentRow.Cells["DEPTLO"].Value.ToString(),//5
                                RadGridView_Show.CurrentRow.Cells["VEHICLEID"].Value.ToString(),//6
                                RadGridView_Show.CurrentRow.Cells["ROUTEIDDESC"].Value.ToString(),//7
                                SystemClass.SystemUserID,//8
                                SystemClass.SystemUserName, //9
                                RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString(),//
                                InputEmplReasonRemark.EmplId,//
                                InputEmplReasonRemark.EmplName,// 
                                ReasonKey,
                                ReasonTag,
                                InputEmplReasonRemark.ReasonId,
                                InputEmplReasonRemark.ReasonName,
                                InputEmplReasonRemark.ReasonDeduct, "");
                            }
                            else
                            {
                                sql = string.Format(@"Update  SHOP_RecheckReciveItem set 
                                whoDeduct='{0}', 
                                whoNameDeduct='{1}', 
                                KeyReason='{2}', 
                                ReasonImg='{3}', 
                                ReasonID='{4}', 
                                ReasonName='{5}', 
                                ReasonDeduct='{6}'
                                where VAN='TAG' 
                                And ITEMBARCODE='{7}' 
                                And INVOICEID='{8}'",
                                InputEmplReasonRemark.EmplId,
                                InputEmplReasonRemark.EmplName,
                                ReasonKey,
                                ReasonTag,
                                InputEmplReasonRemark.ReasonId,
                                InputEmplReasonRemark.ReasonName,
                                InputEmplReasonRemark.ReasonDeduct,
                                TypeImg,
                                RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString());

                            }
                            String Transection = ConnectionClass.ExecuteSQL_Main(sql);
                            MsgBoxClass.MsgBoxShow_SaveStatus(Transection);

                            if (Transection == "")
                            {
                                String ReasonDesc = InputEmplReasonRemark.EmplId + ", " + InputEmplReasonRemark.EmplName + "," + System.Environment.NewLine +
                                    ReasonKey + "," + ReasonTag + "," + System.Environment.NewLine +
                                    InputEmplReasonRemark.ReasonId + "," + InputEmplReasonRemark.ReasonName + "," + "[" + InputEmplReasonRemark.ReasonDeduct + "]";

                                if (e.ColumnIndex == 12) RadGridView_Show.CurrentRow.Cells[12].Value = ReasonDesc;
                                if (e.ColumnIndex == 21) RadGridView_Show.CurrentRow.Cells[21].Value = ReasonDesc;
                            }
                        }
                    }
                }
            }
        }

        private void RadCheckBox_Dept_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            RadDropDownList_Dept.Enabled = true;
        }

        private void RadCheckBox_Route_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Route.Checked == true) radDropDownList_Route.Enabled = true; else radDropDownList_Route.Enabled = false;
        }

        private void RadCheckBox_Remark_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_NotRemark.Checked == true) RadCheckBox_Remark.Checked = false;
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadCheckBox_NotRemark_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_Remark.Checked == true) RadCheckBox_NotRemark.Checked = false;
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดแท็ก", RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }


        private void RadCheckBox_Empl_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Empl.Checked == true) radTextBox_Empl.Enabled = true; else radTextBox_Empl.Enabled = false;
        }

        private void RadCheckBox_Lo_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Lo.Checked == true) radTextBox_Lo.Enabled = true; else radTextBox_Lo.Enabled = false;
        }

        private void RadCheckBox_CarNumber_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_CarNumber.Checked == true) radTextBox_CarNumber.Enabled = true; else radTextBox_CarNumber.Enabled = false;
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ClearFrom("REFRESH");
            
            if (RadCheckBox_Dept.Checked == true) Dept = RadDropDownList_Dept.SelectedValue.ToString();
            if (radCheckBox_Route.Checked == true) Route = radDropDownList_Route.SelectedValue.ToString();
            if (radCheckBox_Empl.Checked == true) Empl = radTextBox_Empl.Text;
            if (radCheckBox_Lo.Checked == true) Lo = radTextBox_Lo.Text;
            if (radCheckBox_CarNumber.Checked == true) Car = radTextBox_CarNumber.Text;

            if (RadCheckBox_Remark.Checked == true)
            {
                SqlDeduct = $@"AND TagNumber in (select distinct INVOICEID from SHOP_RecheckReciveItem WITH (NOLOCK) 
                        where VAN = 'TAG' AND DATEBILL  between '{radDateTimePicker_DateLo.Value.AddDays(-3):yyyy-MM-dd}' 
                            and '{radDateTimePicker_DateLo.Value.AddDays(2):yyyy-MM-dd}')";
            }
            if (RadCheckBox_NotRemark.Checked == true)
            {
                SqlDeduct = $@"AND TagNumber not in (select distinct INVOICEID from SHOP_RecheckReciveItem WITH (NOLOCK) 
                        where VAN = 'TAG' AND DATEBILL  between '{radDateTimePicker_DateLo.Value.AddDays(-3):yyyy-MM-dd}' and '{radDateTimePicker_DateLo.Value.AddDays(2):yyyy-MM-dd}')";

            }
            if (RadCheckBox_Branch.Checked == true) SqlBranch = string.Format(@" BranchLock !='MN000'");

            string MaxCar = ">1";
            if (_pDptOpen == "027") MaxCar = ">= 1";

            DtLogisDept = LogisticClass.GetDataTagByLogistic(radDateTimePicker_DateLo.Value.ToString("yyyy-MM-dd"),
                Dept,
                Route, Empl, Lo, Car, SystemClass.SystemBranchID,
                SqlDeduct, SqlBranch, MaxCar, _pDptOpen);

            DtLogisDept.AcceptChanges();

            string PathLo;
            RadGridView_Show.DataSource = DtLogisDept;

            if (_pPermissionImg == "1")
            {
                for (int iRow = 0; iRow < DtLogisDept.Rows.Count; iRow++)
                {

                    string LOGISTICID = DtLogisDept.Rows[iRow]["LOGISTICID"].ToString();
                    string TagNumber = DtLogisDept.Rows[iRow]["TagFullNumber"].ToString();
                    string Branch = DtLogisDept.Rows[iRow]["Branch"].ToString();
                    string BranchLock = DtLogisDept.Rows[iRow]["BranchLock"].ToString();

                    PathLo = Controllers.PathImageClass.pPathSH_CarCloseNewLO + @"\" + LOGISTICID;

                    if (ImageClass.CheckFileExit(PathLo)) return;

                    var newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Controllers.PathImageClass.pImageEmply);
                    RadGridView_Show.Rows[iRow].Cells["Key"].Value = newImage;

                    RadGridView_Show.Rows[iRow].Cells["BeforeBranch"].Value = newImage;
                    RadGridView_Show.Rows[iRow].Cells["AfterBranch"].Value = newImage;

                    RadGridView_Show.Rows[iRow].Cells["BeforeIn"].Value = newImage;
                    RadGridView_Show.Rows[iRow].Cells["BeforeOut"].Value = newImage;

                    DirectoryInfo di = new DirectoryInfo(PathLo);
                    string Imagename = @"CarO-*" + LOGISTICID + @"*" + TagNumber + @"*";

                    FileInfo[] Images = di.GetFiles(Imagename + @"*.jpg");
                    for (int i = 0; i < Images.Length; i++)
                    {
                        string[] names = Images[i].ToString().Split(',');
                        String TypeImg = names[1].Substring(0, 1);

                        newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Images[i].FullName.ToString());


                        if (Branch == "MN000" && TypeImg == "S")
                        {
                            RadGridView_Show.Rows[iRow].Cells["PathBeforeIn"].Value = Images[i].Name.ToString();
                            RadGridView_Show.Rows[iRow].Cells["BeforeIn"].Value = newImage;

                        }

                        else
                        {
                            if (TypeImg == "A" || TypeImg == "E")
                            {
                                RadGridView_Show.Rows[iRow].Cells["PathAfterBranch"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[iRow].Cells["AfterBranch"].Value = newImage;
                            }
                            else if (TypeImg == "B" || TypeImg == "F")
                            {
                                RadGridView_Show.Rows[iRow].Cells["PathBeforeBranch"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[iRow].Cells["BeforeBranch"].Value = newImage;
                            }
                            else if (TypeImg == "R")
                            {
                                RadGridView_Show.Rows[iRow].Cells["PathBeforeOut"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[iRow].Cells["BeforeOut"].Value = newImage;
                            }

                        }
                    }
                    if (!(Branch.Equals("MN000")))
                    {
                        ////Find รูปสุดท้าย ไม่ตรงกับ MN ไม่สามารถเช็คจากเลขแท็กได้จากเงื่อนไขข้างต้น
                        di = new DirectoryInfo(PathLo);
                        Imagename = @"CarO-*" + BranchLock + @"*" + LOGISTICID + @"*C*.jpg";
                        Images = di.GetFiles(Imagename);
                        newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Images[0].FullName.ToString());
                        if (RadGridView_Show.Rows[iRow].Cells["WhoNameUnLock"].Value.ToString() == "")
                        {
                            RadGridView_Show.Rows[iRow].Cells["WhoNameUnLock"].Style.BackColor = Color.Red;
                        }

                        RadGridView_Show.Rows[iRow].Cells["PathKey"].Value = Images[0].Name.ToString();
                        RadGridView_Show.Rows[iRow].Cells["Key"].Value = newImage;

                    }

                    RadGridView_Show.Rows[iRow].Height = 110;

                }
            }


            this.Cursor = Cursors.Default;
        }


    }
}
