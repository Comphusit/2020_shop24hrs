﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Data;

namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class TagDetail : Telerik.WinControls.UI.RadForm
    {
        private DataTable dtBch = new DataTable();
        private DataTable DtDgv = new DataTable();

        readonly string _pTypeReport;//0 รายงาน Tag ท้ายรถ 1 รายงาน Tag ถังน้ำมัน
        readonly string _pTypePerMission;//สิทธิ์ดูเลขที่ 0 ไม่แสดงเลขที่ Tag 1 แสกงเลขที่ Tag
        //Load
        public TagDetail(string pTypeReport, string pTypePerMission)
        {
            InitializeComponent();

            _pTypeReport = pTypeReport;
            _pTypePerMission = pTypePerMission;

            if (_pTypePerMission == "0")
            {
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TagNumber", "เลขที่แท็ก")));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TagFullNumber", "เลขที่แท็ก")));
            }
            else
            {
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TagNumber", "เลขที่แท็ก", 80)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TagFullNumber", "เลขที่แท็ก", 80)));
            }

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Branch", "สาขา", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 150)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StaLock", "ล็อค", 60)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchLock", "สาขาล็อค", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchNameLock", "ชื่อสาขาล็อค", 150)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameLock", "ล็อคโดย", 170)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateLock", "วันที่ล็อค", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TimeLock", "เวลาล็อค", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StaUnlock", "ปล๊ดล็อค", 60)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchUnlock", "สาขาปลดล็อค", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchNameUnlock", "ชื่อสาขาปลดล็อค", 150)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameUnLock", "ปลดล็อคโดย", 170)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateUnlock", "วันที่ปลดล็อค", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TimeUnlock", "เวลาปลดล็อค", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TagRemark", "หมายเหตุ", 150)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TagDpt", "แท็กแผนก", 100)));

            RadGridView_Show.Columns["TagNumber"].IsPinned = true;
            RadGridView_Show.Columns["TagFullNumber"].IsPinned = true;
            RadGridView_Show.Columns["LOGISTICID"].IsPinned = true;


        }

        //Load Main
        private void TagDetail_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_ClearLO.ShowBorder = true; RadButtonElement_ClearLO.ToolTipText = "Clear LO";
            RadButtonElement_search.ShowBorder = true; RadButtonElement_search.ToolTipText = "เช็คข้อมูลทั้งหมด";
            RadButtonElement_Tagrefresh.ShowBorder = true; RadButtonElement_Tagrefresh.ToolTipText = "Clear Tag";
            RadButtonElement_TagClear.ShowBorder = true; RadButtonElement_TagClear.ToolTipText = "Clear Tag ทิ้ง";
            RadButtonElement_CheckTag.ShowBorder = true; RadButtonElement_CheckTag.ToolTipText = "Lock-UnLock Tag";

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_Branch.Checked = false;
            RadRadioButton_Use.IsChecked = true;

            dtBch = BranchClass.GetBranchAll("'1','2','4'", "'1'");
            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.Enabled = false;

            if (_pTypeReport == "1")
            {
                RadCheckBox_Branch.Checked = true;
                RadCheckBox_Branch.Enabled = false;
                RadDropDownList_Branch.Enabled = false;
                RadTextBox_Dpt.Text = "D079";
                RadTextBox_Dpt.Enabled = false;
                RadTextBox_Lo.Enabled = false;
                RadTextBox_SH.Enabled = false;

                RadButtonElement_search.Enabled = false;
                RadButtonElement_CheckTag.Enabled = false;
                RadButtonElement_ClearLO.Enabled = false;
            }

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            ShowDGV();
        }

        #region FUNCTION

        //ค้นหาแท็กตามเงื่อนไข rb=radioButton ที่เป็นสถานะการใช้งาน TAG
        private DataTable GetTag_ByCondition(Boolean rb_ok, Boolean rb_not, Boolean rb_O, Boolean rb_All, string Branch, string Tag, string Dept, string LO, string SH)
        {
            string Sql = $@"
            SELECT  TagNumber, TagFullNumber, LOGISTICID, Branch, BranchName, WhoNameIns, CONVERT(VARCHAR, DateIns, 23) AS DateIns, 
                    StaLock, BranchLock, BranchNameLock, WhoNameLock, CONVERT(VARCHAR,DateLock,23) AS DateLock,TimeLock, 
                    StaUnlock, BranchUnlock, BranchNameUnlock, WhoNameUnLock, CONVERT(VARCHAR,DateUnlock,23) as DateUnlock,TimeUnlock,TagRemark,TagDpt  
            FROM	Shop_TagCarTruck WITH (NOLOCK)  ";

            if (rb_ok) Sql += @"WHERE  StaLock = '0' AND StaUnlock = '0' ";

            if (rb_not) Sql += @"WHERE  StaLock = '1' AND StaUnlock = '1' ";

            if (rb_O) Sql += @"WHERE  StaLock = '1' AND StaUnlock = '0' ";

            if (rb_All) Sql += @"WHERE  StaLock IN ( '1','0') AND StaUnlock IN ('0','1')  ";

            if (Branch != "000") Sql += @"AND Branch = '" + Branch + @"'";

            if (Tag.Trim() != "")
            {
                if (Tag.Trim().Length == 8) Sql += @" AND TagNumber = '" + Tag.ToString().Substring(0, 7).Trim() + @"'";
                else if (Tag.Trim().Length < 8) Sql += @" AND TagNumber = '" + Tag.Trim() + @"'";
            }

            if (Dept.Trim() != "") Sql += @" AND TagDpt like '%" + Dept.Trim() + @"'";
            else Sql += @" AND TagDpt != 'D079' ";

            if (LO.Trim() != "") Sql += @" AND LOGISTICID LIKE '%" + LO.Replace(" ", "%") + @"%' ";

            if (SH.Trim() != "") Sql += $@"  AND LOGISTICID IN ( {LogisticClass.FindLO_BySH(SH.Trim())} )  ";

            Sql += @"  ORDER BY DateLock,TimeLock,TagNumber";

            return ConnectionClass.SelectSQL_Main(Sql);
        }

        //แสดงรายละเอียดใน datagrid ใช้คู่กับ Function GetTag_ByCondition
        private void ShowDGV()
        {
            switch (RadTextBox_Tag.Text.Length)
            {
                case 8:
                    break;
                case 6:
                    break;
                case 0:
                    break;
                default:
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุ Tag Number ให้ครบหลักเท่านั้น จึงจะสามารถค้นหาตาม tag Number ได้.");
                    break;
            }


            Cursor.Current = Cursors.WaitCursor;
            string Branch = "000";
            if (RadCheckBox_Branch.Checked) Branch = RadDropDownList_Branch.SelectedValue.ToString();

            DtDgv = new DataTable();

            DtDgv = GetTag_ByCondition(RadRadioButton_OK.IsChecked, RadRadioButton_Not.IsChecked, RadRadioButton_Use.IsChecked, RadRadioButton_ALL.IsChecked,
                                        Branch, RadTextBox_Tag.Text, RadTextBox_Dpt.Text, RadTextBox_Lo.Text, RadTextBox_SH.Text);
            RadGridView_Show.DataSource = DtDgv;

            Cursor.Current = Cursors.Default;
        }

        #endregion

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        #region EVEN

        //เลือกสาขา
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            ShowDGV();
        }

        private void RadTextBox_Dpt_TextChanging(object sender, TextChangingEventArgs e)
        {
            //e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        private void RadTextBox_Tag_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_Lo_TextChanging(object sender, TextChangingEventArgs e)
        {
            //e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_SH_TextChanging(object sender, TextChangingEventArgs e)
        {
            //e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        //แสดงรายละเอียดของแท็ก ตาม LO ที่มีการใช้งาน TAG
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            using (TagDetailData TagDetailData = new TagDetailData())
            {
                if (RadGridView_Show.RowCount == 0 && RadTextBox_Lo.Text != string.Empty)
                {
                    if (RadTextBox_Lo.Text.Length < 10)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กรุณาระบุ LO ให้เรียบร้อย { Environment.NewLine}ตัวอย่าง  20-0069545.");
                        return;
                    }
                    TagDetailData._Lo = "LO" + RadTextBox_Lo.Text;
                }
                else
                {
                    if (RadGridView_Show.CurrentRow.Cells["StaLock"].Value.ToString() == "0" && RadGridView_Show.CurrentRow.Cells["StaUnlock"].Value.ToString() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("แท็กยังไม่มีการใช้งาน ไม่มีรายละเอียดแท็ก.");
                        return;
                    }
                    TagDetailData._Lo = RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString();
                }

                DialogResult dr = TagDetailData.ShowDialog();
                if (dr == DialogResult.Yes) { }
            }
        }

        //lock unlock tag ที่ต้องการ ที่สาขาใหญ่
        private void RadButtonElement_CheckTag_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.CurrentRow.Cells["StaLock"].Value.ToString() == "1" && RadGridView_Show.CurrentRow.Cells["StaUnlock"].Value.ToString() == "1") return;

            using (TagLock_Unlock TagLock_Unlock = new TagLock_Unlock())
            {
                TagLock_Unlock.TagNoFull = RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString();
                if (RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString() != ""
                    && (RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString().Trim().Length == 8
                    || RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString().Trim().Length == 6))
                {
                    TagLock_Unlock.TagFull = RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString();
                }
                else
                {
                    TagLock_Unlock.TagFull = RadTextBox_Tag.Text;
                }
                TagLock_Unlock.Lo = RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString();
                TagLock_Unlock.BranchTag = RadGridView_Show.CurrentRow.Cells["Branch"].Value.ToString();
                TagLock_Unlock.BranchNameTag = RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString();

                TagLock_Unlock.BranchLock = RadGridView_Show.CurrentRow.Cells["BranchLock"].Value.ToString();
                TagLock_Unlock.BranchNameLock = RadGridView_Show.CurrentRow.Cells["BranchNameLock"].Value.ToString();
                TagLock_Unlock.locked = RadGridView_Show.CurrentRow.Cells["StaLock"].Value.ToString();
                TagLock_Unlock.Unlocked = RadGridView_Show.CurrentRow.Cells["StaUnlock"].Value.ToString();

                DialogResult dr = TagLock_Unlock.ShowDialog();
                if (dr == DialogResult.Yes) ShowDGV();
            }
        }

        //เคลียร์แท็กเป็นค่าเริ่มต้น
        private void RadButtonElement_Tagrefresh_Click(object sender, EventArgs e)
        {
            if (SystemClass.SystemComMinimart != "1") return;

            if (RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString() == "" ||
                (RadGridView_Show.CurrentRow.Cells["StaLock"].Value.ToString() == "0" && RadGridView_Show.CurrentRow.Cells["StaUnlock"].Value.ToString() == "0"))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("แท็กยังไม่มีการใช้งาน ไม่สามารถเคลีย์แท็กได้.");
                return;
            }

            using (TagRemark TagRemark = new TagRemark(RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["Branch"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString(),
                                                       "เคลียร์แท็กเป็นค่าเริ่มต้น",
                                                       "RefreshTag"))
            {
                DialogResult dr = TagRemark.ShowDialog();
                if (dr == DialogResult.Yes) ShowDGV();
            }
        }
        //เคลียร์ LO ไม่ให้อยู่ในระบบ
        private void RadButtonElement_ClearLO_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีเอกสาร LO ที่ต้องการเคลีย์.");
                return;
            }

            using (TagRemark TagRemark = new TagRemark(RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["Branch"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString(),
                                                       "เคลียร์ เอกสาร LO",
                                                       "ClearLO"))
            {

                DialogResult dr = TagRemark.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    ShowDGV();
                }
            }
        }

        //เคลียร์แท็กทิ้ง ไม่สามารถใช้งาน TAG ได้อีก
        private void RadButtonElement_TagClear_Click(object sender, EventArgs e)
        {
            if (SystemClass.SystemComMinimart != "1") return;

            using (TagRemark TagRemark = new TagRemark(RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["TagFullNumber"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["Branch"].Value.ToString(),
                                                       RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString(),
                                                       "เคลียร์แท็กทิ้ง ไม่สามารถใช้งานได้อีก",
                                                       "ClearTag"))
            {
                DialogResult dr = TagRemark.ShowDialog();
                if (dr == DialogResult.Yes) ShowDGV();
            }
        }

        private void RadTextBox_Dpt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) ShowDGV();
        }

        private void RadTextBox_Tag_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) ShowDGV();
        }

        private void RadTextBox_Lo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) ShowDGV();
        }

        private void RadTextBox_SH_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) ShowDGV();
        }

        #endregion
    }
}
