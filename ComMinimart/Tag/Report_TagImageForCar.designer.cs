﻿namespace PC_Shop24Hrs.ComMinimart.Tag
{
    partial class Report_TagImageForCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report_TagImageForCar));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.RadCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.RadCheckBox_NotRemark = new Telerik.WinControls.UI.RadCheckBox();
            this.RadCheckBox_Remark = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_CarNumber = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Lo = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Empl = new Telerik.WinControls.UI.RadCheckBox();
            this.radTextBox_CarNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Lo = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Empl = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_Route = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_Route = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_Dept = new Telerik.WinControls.UI.RadCheckBox();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_DateLo = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radDateTimePicker_DateLo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_NotRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CarNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Lo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CarNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Lo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Route)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Route)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DateLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 606F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 606F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(638, 606);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(632, 600);
            this.RadGridView_Show.TabIndex = 1;
            this.RadGridView_Show.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_CellFormatting);
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(844, 612);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.RadCheckBox_Branch);
            this.radPanel2.Controls.Add(this.RadCheckBox_NotRemark);
            this.radPanel2.Controls.Add(this.RadCheckBox_Remark);
            this.radPanel2.Controls.Add(this.radCheckBox_CarNumber);
            this.radPanel2.Controls.Add(this.radCheckBox_Lo);
            this.radPanel2.Controls.Add(this.radCheckBox_Empl);
            this.radPanel2.Controls.Add(this.radTextBox_CarNumber);
            this.radPanel2.Controls.Add(this.radTextBox_Lo);
            this.radPanel2.Controls.Add(this.radTextBox_Empl);
            this.radPanel2.Controls.Add(this.radCheckBox_Route);
            this.radPanel2.Controls.Add(this.radDropDownList_Route);
            this.radPanel2.Controls.Add(this.RadCheckBox_Dept);
            this.radPanel2.Controls.Add(this.radStatusStrip1);
            this.radPanel2.Controls.Add(this.RadDropDownList_Dept);
            this.radPanel2.Controls.Add(this.radLabel_DateLo);
            this.radPanel2.Controls.Add(this.RadButton_Search);
            this.radPanel2.Controls.Add(this.radDateTimePicker_DateLo);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(647, 3);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(194, 606);
            this.radPanel2.TabIndex = 2;
            // 
            // RadCheckBox_Branch
            // 
            this.RadCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Branch.Location = new System.Drawing.Point(13, 422);
            this.RadCheckBox_Branch.Name = "RadCheckBox_Branch";
            this.RadCheckBox_Branch.Size = new System.Drawing.Size(91, 19);
            this.RadCheckBox_Branch.TabIndex = 73;
            this.RadCheckBox_Branch.Text = "เฉพาะสาขา";
            // 
            // RadCheckBox_NotRemark
            // 
            this.RadCheckBox_NotRemark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_NotRemark.Location = new System.Drawing.Point(13, 397);
            this.RadCheckBox_NotRemark.Name = "RadCheckBox_NotRemark";
            this.RadCheckBox_NotRemark.Size = new System.Drawing.Size(111, 19);
            this.RadCheckBox_NotRemark.TabIndex = 73;
            this.RadCheckBox_NotRemark.Text = "ไม่มีรายการหัก";
            this.RadCheckBox_NotRemark.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_NotRemark_ToggleStateChanged);
            // 
            // RadCheckBox_Remark
            // 
            this.RadCheckBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Remark.Location = new System.Drawing.Point(13, 372);
            this.RadCheckBox_Remark.Name = "RadCheckBox_Remark";
            this.RadCheckBox_Remark.Size = new System.Drawing.Size(95, 19);
            this.RadCheckBox_Remark.TabIndex = 72;
            this.RadCheckBox_Remark.Text = "มีรายการหัก";
            this.RadCheckBox_Remark.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Remark_ToggleStateChanged);
            // 
            // radCheckBox_CarNumber
            // 
            this.radCheckBox_CarNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_CarNumber.Location = new System.Drawing.Point(13, 264);
            this.radCheckBox_CarNumber.Name = "radCheckBox_CarNumber";
            this.radCheckBox_CarNumber.Size = new System.Drawing.Size(107, 19);
            this.radCheckBox_CarNumber.TabIndex = 71;
            this.radCheckBox_CarNumber.Text = "ระบุทะเบียนรถ";
            this.radCheckBox_CarNumber.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_CarNumber_ToggleStateChanged);
            // 
            // radCheckBox_Lo
            // 
            this.radCheckBox_Lo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Lo.Location = new System.Drawing.Point(12, 205);
            this.radCheckBox_Lo.Name = "radCheckBox_Lo";
            this.radCheckBox_Lo.Size = new System.Drawing.Size(39, 19);
            this.radCheckBox_Lo.TabIndex = 71;
            this.radCheckBox_Lo.Text = "LO";
            this.radCheckBox_Lo.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Lo_ToggleStateChanged);
            // 
            // radCheckBox_Empl
            // 
            this.radCheckBox_Empl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Empl.Location = new System.Drawing.Point(11, 149);
            this.radCheckBox_Empl.Name = "radCheckBox_Empl";
            this.radCheckBox_Empl.Size = new System.Drawing.Size(73, 19);
            this.radCheckBox_Empl.TabIndex = 71;
            this.radCheckBox_Empl.Text = "พนักงาน";
            this.radCheckBox_Empl.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Empl_ToggleStateChanged);
            // 
            // radTextBox_CarNumber
            // 
            this.radTextBox_CarNumber.Enabled = false;
            this.radTextBox_CarNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CarNumber.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CarNumber.Location = new System.Drawing.Point(12, 286);
            this.radTextBox_CarNumber.Name = "radTextBox_CarNumber";
            this.radTextBox_CarNumber.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_CarNumber.TabIndex = 73;
            // 
            // radTextBox_Lo
            // 
            this.radTextBox_Lo.Enabled = false;
            this.radTextBox_Lo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Lo.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Lo.Location = new System.Drawing.Point(12, 230);
            this.radTextBox_Lo.Name = "radTextBox_Lo";
            this.radTextBox_Lo.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Lo.TabIndex = 19;
            // 
            // radTextBox_Empl
            // 
            this.radTextBox_Empl.Enabled = false;
            this.radTextBox_Empl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Empl.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Empl.Location = new System.Drawing.Point(11, 174);
            this.radTextBox_Empl.Name = "radTextBox_Empl";
            this.radTextBox_Empl.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Empl.TabIndex = 71;
            // 
            // radCheckBox_Route
            // 
            this.radCheckBox_Route.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Route.Location = new System.Drawing.Point(12, 97);
            this.radCheckBox_Route.Name = "radCheckBox_Route";
            this.radCheckBox_Route.Size = new System.Drawing.Size(84, 19);
            this.radCheckBox_Route.TabIndex = 70;
            this.radCheckBox_Route.Text = "ระบุสายรถ";
            this.radCheckBox_Route.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Route_ToggleStateChanged);
            // 
            // radDropDownList_Route
            // 
            this.radDropDownList_Route.DropDownAnimationEnabled = false;
            this.radDropDownList_Route.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Route.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Route.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Route.Location = new System.Drawing.Point(11, 122);
            this.radDropDownList_Route.Name = "radDropDownList_Route";
            this.radDropDownList_Route.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Route.TabIndex = 69;
            this.radDropDownList_Route.Text = "radDropDownList1";
            // 
            // RadCheckBox_Dept
            // 
            this.RadCheckBox_Dept.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RadCheckBox_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Dept.Location = new System.Drawing.Point(13, 42);
            this.RadCheckBox_Dept.Name = "RadCheckBox_Dept";
            this.RadCheckBox_Dept.Size = new System.Drawing.Size(80, 19);
            this.RadCheckBox_Dept.TabIndex = 68;
            this.RadCheckBox_Dept.Text = "ระบุแผนก";
            this.RadCheckBox_Dept.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.RadCheckBox_Dept.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Dept_ToggleStateChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.radButtonElement_excel,
            this.commandBarSeparator1,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 63;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.UseCompatibleTextRendering = false;
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadDropDownList_Dept
            // 
            this.RadDropDownList_Dept.DropDownAnimationEnabled = false;
            this.RadDropDownList_Dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Dept.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Dept.Location = new System.Drawing.Point(12, 67);
            this.RadDropDownList_Dept.Name = "RadDropDownList_Dept";
            this.RadDropDownList_Dept.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Dept.TabIndex = 67;
            this.RadDropDownList_Dept.Text = "radDropDownList1";
            // 
            // radLabel_DateLo
            // 
            this.radLabel_DateLo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DateLo.Location = new System.Drawing.Point(13, 317);
            this.radLabel_DateLo.Name = "radLabel_DateLo";
            this.radLabel_DateLo.Size = new System.Drawing.Size(55, 19);
            this.radLabel_DateLo.TabIndex = 66;
            this.radLabel_DateLo.Text = "ระบุวันที่";
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 446);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 63;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDateTimePicker_DateLo
            // 
            this.radDateTimePicker_DateLo.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_DateLo.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_DateLo.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_DateLo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_DateLo.Location = new System.Drawing.Point(13, 341);
            this.radDateTimePicker_DateLo.Name = "radDateTimePicker_DateLo";
            this.radDateTimePicker_DateLo.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_DateLo.TabIndex = 64;
            this.radDateTimePicker_DateLo.TabStop = false;
            this.radDateTimePicker_DateLo.Text = "22/05/2020";
            this.radDateTimePicker_DateLo.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // Report_TagImageForCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 612);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Report_TagImageForCar";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "รายละเอียดการส่งของ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Report_TagImageForCar_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_NotRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CarNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Lo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CarNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Lo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Route)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Route)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DateLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Route;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Route;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Dept;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel_DateLo;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_DateLo;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_CarNumber;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Lo;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Empl;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CarNumber;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Lo;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Empl;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Remark;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Branch;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_NotRemark;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
    }
}
