﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using PC_Shop24Hrs.FormShare.ShowImage;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.ComMinimart.Tag
{
    public partial class Report_TagImage : Telerik.WinControls.UI.RadForm
    {
        string Dept, Route, Empl, Car, Lo, SqlDeduct, SqlBranch;

        DataTable DtLogisDept;
        readonly string _pPermissionImg;//1 ดูรูปได้

        public Report_TagImage(string pPermissionImg)
        {
            InitializeComponent();
            _pPermissionImg = pPermissionImg;//1 ดูรูป
        }

        private void ClearFrom(string status)
        {
            if (status == "NEW")
            {
                Dept = string.Empty;
                Route = string.Empty;
                Empl = string.Empty;
                Car = string.Empty;
                Lo = string.Empty;
                SqlDeduct = string.Empty;
                SqlBranch = string.Empty;

                RadCheckBox_Dept.Checked = true;
                RadDropDownList_Dept.Enabled = true;
                if (SystemClass.SystemBranchID == "MN000")
                {
                    radCheckBox_Route.Checked = false;
                    radDropDownList_Route.Enabled = false;
                    radCheckBox_Empl.Checked = false;
                    radTextBox_Empl.Enabled = false;
                }
                else
                {
                    radCheckBox_Route.Visible = false;
                    radDropDownList_Route.Visible = false;
                    radCheckBox_Empl.Visible = false;
                    radTextBox_Empl.Visible = false;
                }

                radCheckBox_Lo.Checked = false;
                radTextBox_Lo.Enabled = false;

                radCheckBox_CarNumber.Checked = false;
                radTextBox_CarNumber.Enabled = false;

                radDateTimePicker_DateLo.Value = DateTime.Now.AddDays(-1);
            }
            else if (status == "REFRESH")
            {
                Dept = string.Empty;
                Route = string.Empty;
                Empl = string.Empty;
                Car = string.Empty;
                Lo = string.Empty;
                SqlDeduct = string.Empty;
                SqlBranch = string.Empty;
            }
        }

        private void Report_TagImage_Load(object sender, EventArgs e)
        {
            //แยกสาขาใน SQL TagImage ถ้าไม่ใช่ MN000 จะแสดงแค่สาขานั้นๆ
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            radButtonElement_excel.ToolTipText = "Export To Excel";
            radButtonElement_excel.ShowBorder = true;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Route);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_DateLo, DateTime.Now.AddDays(0), DateTime.Now.AddDays(0));

            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Route.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Empl.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Lo.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_CarNumber.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Remark.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_NotRemark.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadButton_Search.ButtonElement.ShowBorder = true;

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "รอบส่งสินค้า", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTLO", "แผนกส่ง", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ROUTEIDDESC", "สายรถ", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร.", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLCLERK", "เด็กท้าย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TRASHREMARK", "หมายเหตุ", 100));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "สาขา", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WhoNameLock", "ล็อคโดย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WhoNameUnLock", "ปล๊ดล็อคโดย", 100));

            if (_pPermissionImg == "1")
            {
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathKey", "PathKey", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("Key", "รูปกุญแจ", 120));
                this.RadGridView_Show.MasterTemplate.Columns["PathKey"].IsVisible = false;
            }
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("KeyReason", "ตรวจสอบกุญแจ", 150));

            if (_pPermissionImg == "1")
            {
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeBranch", "PathBeforeBranch", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeBranch", "รูปก่อนเข้าสาขา", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathAfterBranch", "PathAfterBranch", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("AfterBranch", "รูปก่อนออกสาขา", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeIn", "PathBeforeIn", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeIn", "รูปสาขาใหญ่ " + System.Environment.NewLine + @"[ก่อนออก]", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PathBeforeOut", "PathBeforeOut", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("BeforeOut", "รูปสาขาใหญ่ " + System.Environment.NewLine + @"[ก่อนเข้า]", 120));

                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathAfterBranch"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeIn"].IsVisible = false;
                this.RadGridView_Show.MasterTemplate.Columns["PathBeforeOut"].IsVisible = false;
            }

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ReasonImg", "หมายเหตุ", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TagNumber", "TagNumber", 80));


            this.RadGridView_Show.MasterTemplate.Columns["TagNumber"].IsVisible = false;

            RadGridView_Show.Columns["LOGISTICID"].IsPinned = true;
            RadGridView_Show.Columns["DEPTLO"].IsPinned = true;

            RadGridView_Show.MasterTemplate.EnableSorting = false;

            RadGridView_Show.TableElement.RowHeight = 100;

            //ต้องการให้สีเปลี่ยน แต่ยังไม่เปลี่ยน ยังทำไม่ได้
            ConditionalFormattingObject c1 = new ConditionalFormattingObject("WhoNameUnLock", ConditionTypes.Equal, "", "", true)
            {
                RowBackColor = ConfigClass.SetColor_Red(),
                CellBackColor = ConfigClass.SetColor_Red()
            };
            RadGridView_Show.Columns["WhoNameUnLock"].ConditionalFormattingObjectList.Add(c1);

            DatagridClass.SetCellBackClolorByExpression("WhoNameUnLock", "WhoNameUnLock ='' ", ConfigClass.SetColor_SkyPastel(), RadGridView_Show);

            SetDropdownlistDeptLogis(Class.LogisticClass.GetLogisticDept());
            SetDropdownlistRoute(Class.LogisticClass.GetRoute("9"));

            ClearFrom("NEW");
        }

        //set dept
        private void SetDropdownlistDeptLogis(DataTable DeptLogis)
        {
            RadDropDownList_Dept.DataSource = DeptLogis;
            RadDropDownList_Dept.DisplayMember = "DEPTNAME";
            RadDropDownList_Dept.ValueMember = "DEPTID";
        }
        //set Route
        private void SetDropdownlistRoute(DataTable DeptLogis)
        {
            radDropDownList_Route.DataSource = DeptLogis;
            radDropDownList_Route.DisplayMember = "ROUTENAME";
            radDropDownList_Route.ValueMember = "ROUTEID";
        }


        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows


        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_Show_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion

        //SHOW IMAGE รูปใหญ่ใน FORM


        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {

            if (RadGridView_Show.Rows.Count > 0 && !(RadGridView_Show.CurrentRow.Cells[e.ColumnIndex - 1].Value is null))
            {
                if (e.ColumnIndex == 11 || e.ColumnIndex == 14 || e.ColumnIndex == 16 || e.ColumnIndex == 18 || e.ColumnIndex == 20)
                {
                    string PathLo = PathImageClass.pPathSH_CarCloseNewLO + @"\" +
                        RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString();
                    ShowImage frmSPC = new ShowImage()
                    {
                        pathImg = PathLo + @"|" + RadGridView_Show.CurrentRow.Cells[e.ColumnIndex - 1].Value.ToString()
                    };
                    if (frmSPC.ShowDialog(this) == DialogResult.OK) { }
                }
                else if (e.ColumnIndex == 21 || e.ColumnIndex == 12)
                {

                    string TypeSql,
                        TypeImg = string.Empty,
                        EmplId = string.Empty,
                        EmplName = string.Empty,
                        BranchId,
                        BranchName,
                        ReasonKey = string.Empty,
                        ReasonTag = string.Empty,
                        Reason = string.Empty;

                    string[] EmplDesc, BranchDesc;

                    BranchDesc = RadGridView_Show.CurrentRow.Cells["BranchName"].Value.ToString().Split(',');
                    BranchId = BranchDesc[0];
                    BranchName = BranchDesc[1].Replace("\r\n", "");

                    if (BranchId == "MN000") return;
                    if (e.ColumnIndex == 12 && BranchId != "MN000") TypeImg = "Keys";
                    if (e.ColumnIndex == 21 && BranchId != "MN000") TypeImg = "LockUnLock";

                    if (e.Value is null)
                    {
                        TypeSql = "Ins";
                        //KEYS
                        if (e.ColumnIndex == 12 && BranchId != "MN000")
                        {
                            EmplDesc = RadGridView_Show.CurrentRow.Cells["WhoNameUnLock"].Value.ToString().Split(',');
                            EmplId = EmplDesc[0];
                            EmplName = EmplDesc[1].Replace("\r\n", "");
                        }
                        //LOCK UNLOCK
                        else if (e.ColumnIndex == 21 && BranchId != "MN000")
                        {
                            EmplDesc = RadGridView_Show.CurrentRow.Cells["WhoNameLock"].Value.ToString().Split(',');
                            EmplId = EmplDesc[0];
                            EmplName = EmplDesc[1].Replace("\r\n", "");
                        }
                    }
                    else
                    {

                        TypeSql = "Up";
                        String[] ValuesColumns = e.Value.ToString().Replace("\r\n", "").Split(',');
                        EmplId = ValuesColumns[0];
                        EmplName = ValuesColumns[1];
                        ReasonKey = ValuesColumns[2];
                        ReasonTag = ValuesColumns[3];
                        Reason = ValuesColumns[4];
                    }

                    using (InputEmplReasonRemark InputEmplReasonRemark = new InputEmplReasonRemark(
                            RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value + System.Environment.NewLine +
                            BranchId + " : " + BranchName,
                            EmplId, EmplName,
                            "เหตุผล", ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("29", "", "order by SHOW_ID", "1"), "SHOW_ID", "SHOW_NAMEDESC", Reason,
                            "หมายเหตุเพิ่มเติม", ReasonKey, ReasonTag))
                    {
                        DialogResult dr = InputEmplReasonRemark.ShowDialog();
                        if (dr == DialogResult.Yes)
                        {

                            if (e.ColumnIndex == 12) ReasonKey = InputEmplReasonRemark.pInputData;
                            if (e.ColumnIndex == 21) ReasonTag = InputEmplReasonRemark.pInputData;

                            string sql;

                            if (TypeSql == "Ins")
                            {
                                sql = string.Format(@"insert into SHOP_RecheckReciveItem
                                (
                                    INVOICEID, DATEBILL, BRANCH, VAN, ITEMBARCODE, SPC_ITEMNAME, UNITID, 
                                    QtySend, QtyRecive, QtyCenter, QtyPurchase, SHIPMENTID, 
                                    WHOINS, WHOINSNAME, DATEINS, TIMEINS, DESCRIPTION, WhoDeduct,	
                                    WhoNameDeduct, 
                                    KeyReason, ReasonImg, ReasonID, ReasonName, ReasonDeduct,Path
                                )
                                values
                                (
                                    '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '', '', '', '', '{7}', '{8}', '{9}',
                                    convert(varchar, getdate(), 23),convert(varchar, getdate(), 24),
                                    '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}'
                                )",
                                RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString(), //0
                                radDateTimePicker_DateLo.Value.ToString("yyyy-MM-dd"),//1
                                BranchId + ' ' + BranchName, //2
                                "TAG",//3
                                TypeImg,//4
                                RadGridView_Show.CurrentRow.Cells["DEPTLO"].Value.ToString(),//5
                                RadGridView_Show.CurrentRow.Cells["VEHICLEID"].Value.ToString(),//6
                                RadGridView_Show.CurrentRow.Cells["ROUTEIDDESC"].Value.ToString(),//7
                                SystemClass.SystemUserID,//8
                                SystemClass.SystemUserName, //9
                                RadGridView_Show.CurrentRow.Cells["LOGISTICID"].Value.ToString(),//
                                InputEmplReasonRemark.EmplId,//
                                InputEmplReasonRemark.EmplName,// 
                                ReasonKey,
                                ReasonTag,
                                InputEmplReasonRemark.ReasonId,
                                InputEmplReasonRemark.ReasonName,
                                InputEmplReasonRemark.ReasonDeduct, "");
                            }
                            else
                            {
                                sql = string.Format(@"Update  SHOP_RecheckReciveItem set 
                                whoDeduct='{0}', 
                                whoNameDeduct='{1}', 
                                KeyReason='{2}', 
                                ReasonImg='{3}', 
                                ReasonID='{4}', 
                                ReasonName='{5}', 
                                ReasonDeduct='{6}'
                                where VAN='TAG' 
                                And ITEMBARCODE='{7}' 
                                And INVOICEID='{8}'",
                                InputEmplReasonRemark.EmplId,
                                InputEmplReasonRemark.EmplName,
                                ReasonKey,
                                ReasonTag,
                                InputEmplReasonRemark.ReasonId,
                                InputEmplReasonRemark.ReasonName,
                                InputEmplReasonRemark.ReasonDeduct,
                                TypeImg,
                                RadGridView_Show.CurrentRow.Cells["TagNumber"].Value.ToString());

                            }
                            string Transection = ConnectionClass.ExecuteSQL_Main(sql);
                            MsgBoxClass.MsgBoxShow_SaveStatus(Transection);

                            if (Transection == "")
                            {
                                string ReasonDesc = InputEmplReasonRemark.EmplId + ", " + InputEmplReasonRemark.EmplName + "," + System.Environment.NewLine +
                                    ReasonKey + "," + ReasonTag + "," + System.Environment.NewLine +
                                    InputEmplReasonRemark.ReasonId + "," + InputEmplReasonRemark.ReasonName + "," + "[" + InputEmplReasonRemark.ReasonDeduct + "]";

                                if (e.ColumnIndex == 12) RadGridView_Show.CurrentRow.Cells[12].Value = ReasonDesc;
                                if (e.ColumnIndex == 21) RadGridView_Show.CurrentRow.Cells[21].Value = ReasonDesc;
                            }
                        }
                    }
                }
            }
        }

        private void RadCheckBox_Dept_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            RadDropDownList_Dept.Enabled = true;
        }

        private void RadCheckBox_Route_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Route.Checked == true) radDropDownList_Route.Enabled = true; else radDropDownList_Route.Enabled = false;
        }

        private void RadCheckBox_Remark_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_NotRemark.Checked == true) RadCheckBox_Remark.Checked = false;
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadGridView_Show_Click(object sender, EventArgs e)
        {

        }

        private void RadCheckBox_NotRemark_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_Remark.Checked == true) RadCheckBox_NotRemark.Checked = false;
        }


        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดแท็ก", RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }



        private void RadCheckBox_Empl_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Empl.Checked == true) radTextBox_Empl.Enabled = true; else radTextBox_Empl.Enabled = false;
        }

        private void RadCheckBox_Lo_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Lo.Checked == true) radTextBox_Lo.Enabled = true; else radTextBox_Lo.Enabled = false;
        }

        private void RadCheckBox_CarNumber_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_CarNumber.Checked == true) radTextBox_CarNumber.Enabled = true; else radTextBox_CarNumber.Enabled = false;
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ClearFrom("REFRESH");
            Dept = RadDropDownList_Dept.SelectedValue.ToString();
            if (radCheckBox_Route.Checked == true) Route = radDropDownList_Route.SelectedValue.ToString();
            if (radCheckBox_Empl.Checked == true) Empl = radTextBox_Empl.Text;
            if (radCheckBox_Lo.Checked == true) Lo = radTextBox_Lo.Text;
            if (radCheckBox_CarNumber.Checked == true) Car = radTextBox_CarNumber.Text;

            if (RadCheckBox_Remark.Checked == true)
            {
                SqlDeduct = $@"AND TagNumber in (select distinct INVOICEID from SHOP_RecheckReciveItem WITH (NOLOCK) 
                        where VAN = 'TAG' AND DATEBILL  between '{radDateTimePicker_DateLo.Value.AddDays(-3):yyyy-MM-dd}' and '{radDateTimePicker_DateLo.Value.AddDays(2):yyyy-MM-dd}')";
            }
            if (RadCheckBox_NotRemark.Checked == true)
            {
                SqlDeduct = $@"AND TagNumber not in (select distinct INVOICEID from SHOP_RecheckReciveItem WITH (NOLOCK) 
                        where VAN = 'TAG' AND DATEBILL  between '{radDateTimePicker_DateLo.Value.AddDays(-3):yyyy-MM-dd}' and '{radDateTimePicker_DateLo.Value.AddDays(2):yyyy-MM-dd}')";
            }
            if (RadCheckBox_Branch.Checked == true)
            {
                SqlBranch = @" BranchLock !='MN000' ";
            }

            DtLogisDept = LogisticClass.GetDataTagByLogistic(radDateTimePicker_DateLo.Value.ToString("yyyy-MM-dd"),
                RadDropDownList_Dept.SelectedValue.ToString(),
                Route, Empl, Lo, Car, SystemClass.SystemBranchID,
                SqlDeduct, SqlBranch, "=1");
            DtLogisDept.AcceptChanges();

            string PathLo;
            RadGridView_Show.DataSource = DtLogisDept;

            if (_pPermissionImg == "1")
            {
                int rows = 0;
                foreach (DataRow row in DtLogisDept.Rows)
                {
                    PathLo = Controllers.PathImageClass.pPathSH_CarCloseNewLO + @"\" + row["LOGISTICID"].ToString();

                    if (ImageClass.CheckFileExit(PathLo)) return;

                    var newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Controllers.PathImageClass.pImageEmply);
                    RadGridView_Show.Rows[rows].Cells["Key"].Value = newImage;

                    RadGridView_Show.Rows[rows].Cells["BeforeBranch"].Value = newImage;
                    RadGridView_Show.Rows[rows].Cells["AfterBranch"].Value = newImage;

                    RadGridView_Show.Rows[rows].Cells["BeforeIn"].Value = newImage;
                    RadGridView_Show.Rows[rows].Cells["BeforeOut"].Value = newImage;


                    DirectoryInfo di = new DirectoryInfo(PathLo);
                    string Imagename = @"CarO-*" + row["LOGISTICID"].ToString() + @"*" + row["TagFullNumber"] + @"*";
                    if (row["Branch"].ToString() != "MN000")
                    {
                        Imagename = @"CarO-*" + row["BranchLock"] + @"*" + row["LOGISTICID"].ToString() + @"*";
                    }
                    FileInfo[] Images = di.GetFiles(Imagename + @"*.jpg");
                    for (int i = 0; i < Images.Length; i++)
                    {
                        string[] names = Images[i].ToString().Split(',');
                        string TypeImg = names[1].Substring(0, 1);

                        newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Images[i].FullName.ToString());


                        if (row["Branch"].ToString() == "MN000")
                        {
                            if (TypeImg == "S")
                            {
                                RadGridView_Show.Rows[rows].Cells["PathBeforeIn"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[rows].Cells["BeforeIn"].Value = newImage;
                            }
                        }
                        else
                        {
                            if (TypeImg == "A" || TypeImg == "E")
                            {
                                RadGridView_Show.Rows[rows].Cells["PathAfterBranch"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[rows].Cells["AfterBranch"].Value = newImage;
                            }
                            else if (TypeImg == "B" || TypeImg == "F")
                            {
                                RadGridView_Show.Rows[rows].Cells["PathBeforeBranch"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[rows].Cells["BeforeBranch"].Value = newImage;
                            }
                            else if (TypeImg == "C")
                            {
                                RadGridView_Show.Rows[rows].Cells["PathKey"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[rows].Cells["Key"].Value = newImage;
                            }

                        }
                    }

                    //Find รูปสุดท้าย ไม่ตรงกับ MN ไม่สามารถเช็คจากเลขแท็กได้จากเงื่อนไขข้างต้น
                    di = new DirectoryInfo(PathLo);
                    Imagename = @"CarO-*" + row["LOGISTICID"].ToString() + @"*R*" + row["TagFullNumber"] + @"*.jpg";
                    Images = di.GetFiles(Imagename);
                    if (RadGridView_Show.Rows[rows].Cells["WhoNameUnLock"].Value.ToString() == "")
                    {
                        RadGridView_Show.Rows[rows].Cells["WhoNameUnLock"].Style.BackColor = Color.Red;
                    }
                    if (Images.Length == 0)
                    {
                        RadGridView_Show.Rows[rows].Cells["BeforeOut"].Style.BackColor = Color.Red;
                        //ถ้าtag ยังไม่ได้ ปล็ดล็อค จะเปลี่ยนรุป
                        if (row[30].ToString() == "0")
                        {
                            newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Controllers.PathImageClass.pImageCamera);
                            RadGridView_Show.Rows[rows].Cells["BeforeOut"].Value = newImage;
                        }
                    }

                    else
                    {
                        for (int i = 0; i < Images.Length; i++)
                        {
                            string[] names = Images[i].ToString().Split(',');
                            string TypeImg = names[1].Substring(0, 1);
                            try
                            {
                                newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Images[i].FullName.ToString());
                            }
                            catch
                            {
                                newImage = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Controllers.PathImageClass.pImageCamera);
                            }
                            if (TypeImg == "R")
                            {
                                RadGridView_Show.Rows[rows].Cells["PathBeforeOut"].Value = Images[i].Name.ToString();
                                RadGridView_Show.Rows[rows].Cells["BeforeOut"].Value = newImage;

                            }
                        }

                    }

                    RadGridView_Show.Rows[rows].Height = 110;
                    rows++;
                }
            }

            //ตรวจสอบแท็กที่มีรายการหัก 
            string sql = $@" 
            select INVOICEID AS TAGNUMBER, ITEMBARCODE AS TYPEDEDUCT,
                    Whodeduct +',' + CHAR(13) + CHAR(10) + 
                    WhoNamededuct +',' + CHAR(13) + CHAR(10) +
                    KeyReason +','+ ReasonImg +',' + CHAR(13) + CHAR(10) +
                    ReasonID  +','  + ReasonName  +','+ Convert(varchar, ReasonDeduct) as ReasonDeduct
            from SHOP_RecheckReciveItem WITH (NOLOCK) 
            where VAN='TAG' AND DATEBILL = '{radDateTimePicker_DateLo.Value:yyyy-MM-dd}' ";

            DataTable DtReasonDetail = ConnectionClass.SelectSQL_Main(sql);//Tag_Class.GetReasonDeatil(radDateTimePicker_DateLo.Value.ToString("yyyy-MM-dd"));
            if (DtReasonDetail.Rows.Count > 0)
            {
                foreach (DataRow row in DtReasonDetail.Rows)
                {
                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                    {
                        if (row["TAGNUMBER"].ToString() == RadGridView_Show.Rows[i].Cells["TagNumber"].Value.ToString())
                        {
                            switch (row["TYPEDEDUCT"].ToString())
                            {
                                case "Keys":
                                    RadGridView_Show.Rows[i].Cells["KeyReason"].Value = row["ReasonDeduct"].ToString();
                                    break;

                                case "LockUnLock":
                                    RadGridView_Show.Rows[i].Cells["ReasonImg"].Value = row["ReasonDeduct"].ToString();
                                    break;
                            }

                        }
                    }
                }

            }



            this.Cursor = Cursors.Default;
        }


    }
}
