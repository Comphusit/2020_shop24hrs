﻿namespace HikvisionPreview
{
    partial class ShowSingleLarge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowSingleLarge));
            this.pictureBoxPreview1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxPreview1
            // 
            this.pictureBoxPreview1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBoxPreview1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxPreview1.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxPreview1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBoxPreview1.Name = "pictureBoxPreview1";
            this.pictureBoxPreview1.Size = new System.Drawing.Size(925, 647);
            this.pictureBoxPreview1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPreview1.TabIndex = 0;
            this.pictureBoxPreview1.TabStop = false;
            // 
            // ShowSingleLarge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 647);
            this.Controls.Add(this.pictureBoxPreview1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ShowSingleLarge";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ShowSingleLarge_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ShowSingleLarge_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxPreview1;
    }
}

