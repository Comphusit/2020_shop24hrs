﻿namespace PC_Shop24Hrs
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.textBox_EmplID = new System.Windows.Forms.TextBox();
            this.textBox_Password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.label4 = new System.Windows.Forms.Label();
            this.button_singout = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button_singin = new System.Windows.Forms.Button();
            this.RadButton_ChangePassword = new Telerik.WinControls.UI.RadButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label_Version = new System.Windows.Forms.Label();
            this.checkBox_ax2012 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_ChangePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_EmplID
            // 
            this.textBox_EmplID.BackColor = System.Drawing.Color.White;
            this.textBox_EmplID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.textBox_EmplID.Location = new System.Drawing.Point(311, 89);
            this.textBox_EmplID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_EmplID.MaxLength = 7;
            this.textBox_EmplID.Name = "textBox_EmplID";
            this.textBox_EmplID.Size = new System.Drawing.Size(147, 27);
            this.textBox_EmplID.TabIndex = 0;
            this.textBox_EmplID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_EmplID_KeyDown);
            this.textBox_EmplID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_EmplID_KeyPress);
            // 
            // textBox_Password
            // 
            this.textBox_Password.BackColor = System.Drawing.Color.White;
            this.textBox_Password.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textBox_Password.ForeColor = System.Drawing.Color.Blue;
            this.textBox_Password.Location = new System.Drawing.Point(311, 126);
            this.textBox_Password.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_Password.Name = "textBox_Password";
            this.textBox_Password.PasswordChar = '*';
            this.textBox_Password.Size = new System.Drawing.Size(180, 27);
            this.textBox_Password.TabIndex = 1;
            this.textBox_Password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_Password_KeyDown);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(202, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "รหัสพนักงาน";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(217, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "รหัสผ่าน";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radDropDownList_Branch
            // 
            this.radDropDownList_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Branch.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Branch.DropDownAnimationEnabled = false;
            this.radDropDownList_Branch.DropDownHeight = 124;
            this.radDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 10F);
            this.radDropDownList_Branch.Location = new System.Drawing.Point(221, 51);
            this.radDropDownList_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Branch.Name = "radDropDownList_Branch";
            // 
            // 
            // 
            this.radDropDownList_Branch.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Branch.Size = new System.Drawing.Size(270, 25);
            this.radDropDownList_Branch.TabIndex = 5;
            this.radDropDownList_Branch.Text = "radDropDownList1";
            this.radDropDownList_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Branch_SelectedValueChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Branch.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(209, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(303, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "Contact TEL 8570 /  Outlook : Com-Minimark-Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_singout
            // 
            this.button_singout.BackColor = System.Drawing.Color.Transparent;
            this.button_singout.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button_singout.Location = new System.Drawing.Point(406, 161);
            this.button_singout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_singout.Name = "button_singout";
            this.button_singout.Size = new System.Drawing.Size(85, 35);
            this.button_singout.TabIndex = 3;
            this.button_singout.Text = "ยกเลิก";
            this.button_singout.UseVisualStyleBackColor = false;
            this.button_singout.Click += new System.EventHandler(this.Button_singout_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(220, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(271, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "SUPC Shop24Hrs.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PC_Shop24Hrs.Properties.Resources._24hrs;
            this.pictureBox1.Location = new System.Drawing.Point(13, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(196, 202);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // button_singin
            // 
            this.button_singin.BackColor = System.Drawing.Color.Transparent;
            this.button_singin.Font = new System.Drawing.Font("Tahoma", 12F);
            this.button_singin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_singin.Location = new System.Drawing.Point(311, 161);
            this.button_singin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_singin.Name = "button_singin";
            this.button_singin.Size = new System.Drawing.Size(85, 35);
            this.button_singin.TabIndex = 2;
            this.button_singin.Text = "ตกลง";
            this.button_singin.UseVisualStyleBackColor = false;
            this.button_singin.Click += new System.EventHandler(this.Button_singin_Click);
            // 
            // RadButton_ChangePassword
            // 
            this.RadButton_ChangePassword.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_ChangePassword.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_ChangePassword.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_ChangePassword.Image = global::PC_Shop24Hrs.Properties.Resources.add_user;
            this.RadButton_ChangePassword.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_ChangePassword.Location = new System.Drawing.Point(463, 90);
            this.RadButton_ChangePassword.Name = "RadButton_ChangePassword";
            this.RadButton_ChangePassword.Size = new System.Drawing.Size(26, 26);
            this.RadButton_ChangePassword.TabIndex = 63;
            this.RadButton_ChangePassword.Text = "radButton3";
            this.RadButton_ChangePassword.Click += new System.EventHandler(this.RadButton_ChangePassword_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(10, 239);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(488, 19);
            this.label5.TabIndex = 64;
            this.label5.Text = "สำหรับรหัสผ่านเริ่มต้น คือ รหัสพนักงาน ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Version
            // 
            this.label_Version.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Version.ForeColor = System.Drawing.Color.Black;
            this.label_Version.Location = new System.Drawing.Point(10, 9);
            this.label_Version.Name = "label_Version";
            this.label_Version.Size = new System.Drawing.Size(199, 19);
            this.label_Version.TabIndex = 65;
            this.label_Version.Text = "V";
            this.label_Version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBox_ax2012
            // 
            this.checkBox_ax2012.AutoSize = true;
            this.checkBox_ax2012.Location = new System.Drawing.Point(219, 176);
            this.checkBox_ax2012.Name = "checkBox_ax2012";
            this.checkBox_ax2012.Size = new System.Drawing.Size(71, 20);
            this.checkBox_ax2012.TabIndex = 66;
            this.checkBox_ax2012.Text = "AX2012";
            this.checkBox_ax2012.UseVisualStyleBackColor = true;
            this.checkBox_ax2012.Visible = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(120)))), ((int)(((byte)(181)))));
            this.ClientSize = new System.Drawing.Size(508, 286);
            this.Controls.Add(this.checkBox_ax2012);
            this.Controls.Add(this.label_Version);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.RadButton_ChangePassword);
            this.Controls.Add(this.radDropDownList_Branch);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_singout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_singin);
            this.Controls.Add(this.textBox_EmplID);
            this.Controls.Add(this.textBox_Password);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ":: เข้าสู่ระบบ ::";
            this.Load += new System.EventHandler(this.Login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_ChangePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox_EmplID;
        private System.Windows.Forms.TextBox textBox_Password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_singout;
        private System.Windows.Forms.Button button_singin;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Branch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadButton RadButton_ChangePassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_Version;
        private System.Windows.Forms.CheckBox checkBox_ax2012;
    }
}
