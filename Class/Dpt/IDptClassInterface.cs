﻿using System.Data;

namespace PC_Shop24Hrs.Class.Dpt
{
    interface IDptClassInterface
    {
        //MyJeeds CheckDptAll D%
        DataTable GetDpt_AllD();

        //MyJeeds CheckDptAll D + MN %
        DataTable GetDpt_All();

        //DptShow
        DataTable GetDptForShow();

        //MyJeeds CheckNameDpt 
        DataTable GetDptDetail_ByDptID(string _pDptID);

        //MyJeeds CheckNameDpt 
        string GetDptName_ByDptID(string _pDptID);

        //ค้นหา หน. แผนห
        string FindDptHead(string pDpt);

        //ข้อมูลแผนกที่เปนอาหารสด
        DataTable GetDpt_Purchase();

        //ข้อมูลการเปิด mnog สำหรับแผนกที่กำหนดเท่านั้น
        DataTable GetDpt_PurchaseForMNOG();
        //ข้อมูลรายละเอียดการคืนสินค้า
        DataTable MNPC_GetPc_DT(string docno);

        //รายงานการคืนสินค้าที่แก้ไข
        DataTable MNPC_GetReportMNPCEditDt(string DateBegin, string DateEnd, string Branch);

        //รายละเอียดการคืนตามบาร์โค้ดลดราคา
        DataTable MNPC_GetReportDt(string DateBegin, string DateEnd, string Dept, string Branch);

        DataTable MNPC_GetReportSumAllDim (string DateBegin, string DateEnd, string Dept, string Branch);
    }
}
