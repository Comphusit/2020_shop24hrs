﻿
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class.Dpt
{
    class DptClass : IDptClassInterface
    {
        //MyJeeds CheckDptAll D%
        DataTable IDptClassInterface.GetDpt_AllD()
        {
            return ConnectionClass.SelectSQL_Main(" DptClass_GetDpt_All 'D%' ");
        }
        //MyJeeds CheckDptAll D + MN %
        DataTable IDptClassInterface.GetDpt_All()
        {
            return ConnectionClass.SelectSQL_Main(" DptClass_GetDpt_All '%' ");

        }
        //DptShow
        DataTable IDptClassInterface.GetDptForShow()
        {
            string sql = $@"
                        SELECT  NUM AS DATA_ID,DESCRIPTION AS DATA_DESC,NUM,NUM + ' - ' + [DESCRIPTION] AS [DESCRIPTION]    
                        FROM    SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 
                        WHERE   DATAAREAID = 'SPC' and DIMENSIONCODE = 0  AND NUM LIKE 'D%' AND COMPANYGROUP != 'Cancel' 
                        ORDER BY NUM ";
            return Controllers.ConnectionClass.SelectSQL_Main(sql);
        }
        //MyJeeds CheckNameDpt 
        DataTable IDptClassInterface.GetDptDetail_ByDptID(string _pDptID)
        {
            return ConnectionClass.SelectSQL_Main(" DptClass_GetDpt_All '" + _pDptID + @"' ");
        }
        //MyJeeds CheckNameDpt 
        string IDptClassInterface.GetDptName_ByDptID(string _pDptID)
        {
            string sql = $@"
                SELECT	NUM,DESCRIPTION    
                FROM	SHOP2013TMP.dbo.DIMENSIONS with (nolock) 
                WHERE	DATAAREAID = 'SPC' and DIMENSIONCODE = 0
                        AND NUM = '{_pDptID}'
                ORDER BY NUM ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            if (dt.Rows.Count == 0) return ""; else return dt.Rows[0]["DESCRIPTION"].ToString();
        }
        //ค้นหา หน. แผนห
        string IDptClassInterface.FindDptHead(string pDpt)
        {
            string str = $@"
            SELECT	CASE SUBSTRING(ORGANIZATIONUNITID,0,2) WHEN 'M' THEN 'M0904073' ELSE EMPLTABLE.EMPLID END EMPLID, 
                    CASE SUBSTRING(ORGANIZATIONUNITID,0,2) WHEN 'M' THEN 'คุณ ภารดี ศิริชน' ELSE EMPLTABLE.SPC_NAME END SPC_NAME 
            FROM	SHOP2013TMP.dbo.DIRPARTYINTERNALORGANIZATI2216 WITH (NOLOCK) 
                    LEFT OUTER JOIN  SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON DIRPARTYINTERNALORGANIZATI2216.IVZ_VP = EMPLTABLE.EMPLID   AND EMPLTABLE.DATAAREAID = N'SPC' 
            WHERE	ORGANIZATIONUNITID = '{pDpt}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(str);

            if (dt.Rows.Count == 0) return "ยังไม่ได้ตั้งค่าหัวหน้าแผนก."; else return dt.Rows[0]["SPC_NAME"].ToString();
        }
        //ข้อมูลแผนกที่เปนอาหารสด
        DataTable IDptClassInterface.GetDpt_Purchase()
        {
            string str = $@"
                SELECT  NUM,NUM + ' - ' +DESCRIPTION as DESCRIPTION  ,NUM AS SHOW_ID,DESCRIPTION AS SHOW_NAME
                FROM    SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 
                WHERE	DIMENSIONCODE = 0 	AND  COMPANYGROUP IN ('Purchase','FreshFood')  AND  DATAAREAID = 'SPC'
                ORDER BY NUM  ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ข้อมูลการเปิด mnog สำหรับแผนกที่กำหนดเท่านั้น
        DataTable IDptClassInterface.GetDpt_PurchaseForMNOG()
        {
            string str = string.Format(@"
            SELECT  NUM,NUM + ' - ' + DESCRIPTION as DESCRIPTION  
            FROM    SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 
            WHERE	DIMENSIONCODE = 0 	AND  NUM IN (" + SystemClass.SystemPurchaseFresh + @")  AND  DATAAREAID = 'SPC' 
            ORDER BY NUM 
            ");
            return ConnectionClass.SelectSQL_Main(str);
        }
        //JEEDS OK Get Data DT
        DataTable IDptClassInterface.MNPC_GetPc_DT(string docno)
        {
            string Sql = $@"
            SELECT	SEQNO,SHOP_MNPC_DT.itemid as ITEMID,dimid as INVENTDIMID ,
		            BARCODE AS ITEMBARCODE,ItemName AS SPC_ITEMNAME,
		            CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_MNPC_DT.Qty ELSE  SHOP_BARCODEDISCOUNT.QTY END ELSE ISNULL(DISDIS.QTY,'0') END AS QtyOrder,
		            SHOP_MNPC_DT.CheckCN AS QtyOrderEdit,
		            CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_MNPC_DT.UnitID ELSE  SHOP_BARCODEDISCOUNT.UNITID END ELSE ISNULL(DISDIS.UNITID,'0') END AS UNITID,
                    CASE (CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_MNPC_DT.Qty ELSE  SHOP_BARCODEDISCOUNT.QTY END ELSE ISNULL(DISDIS.QTY,'0') END) WHEN 0 THEN 0 ELSE 
		            FLOOR((CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_MNPC_DT.PriceNet ELSE SHOP_MNBM_DT.PRICENET END ELSE ISNULL(DISDIS.PRICE,'0') END)/
		            (CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_MNPC_DT.Qty ELSE  SHOP_BARCODEDISCOUNT.QTY END ELSE ISNULL(DISDIS.QTY,'0') END)) END AS PRICEUNIT,
		            CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_MNPC_DT.PriceNet ELSE SHOP_MNBM_DT.PRICENET END ELSE ISNULL(DISDIS.PRICE,'0') END AS NET,
		            FACTOR, 
		            SHOW_ID as REMARK_ID,SHOW_DESC as REMARK_NAME,SHOP_MNPC_DT.DeptCode as DEPT ,
		            DIMENSIONS.DESCRIPTION ,
		            SHOP_MNPC_DT.SPC_SALESPRICETYPE as SPC_SALESPRICETYPE,StatusPurchase as RowsEdit,
		            CASE ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS,'') WHEN '' THEN '0' ELSE SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS END AS STA_BARCODEDISCOUNT
		            ,ISNULL(DISDIS.ITEMBARCODE_DIS,'0') AS ITEMBARCODE_DISDIS

            FROM	SHOP_MNPC_DT WITH (NOLOCK)	 
		            INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)	 ON SHOP_MNPC_DT.CNReason=SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
		            INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)	 ON  SHOP_MNPC_DT.DeptCode=DIMENSIONS.NUM 
                            AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONCODE = '0'
		            LEFT OUTER JOIN SHOP_BARCODEDISCOUNT WITH (NOLOCK) ON SHOP_MNPC_DT.Barcode = SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS
		            LEFT OUTER JOIN SHOP_MNBM_DT WITH (NOLOCK) ON SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS = SHOP_MNBM_DT.ITEMBARCODE_DIS
		            LEFT OUTER JOIN SHOP_BARCODEDISCOUNT DISDIS WITH (NOLOCK) ON SHOP_BARCODEDISCOUNT.ITEMBARCODE = DISDIS.ITEMBARCODE_DIS AND SHOP_BARCODEDISCOUNT.DPTID = 'MN999'

            WHERE	SHOP_MNPC_DT.DocNo = '{docno}' AND StaItem = '1'
            ORDER BY SEQNO
            ";
            return ConnectionClass.SelectSQL_Main(Sql);
        }

        //รายงานการแก้ไขรายการสินค้าคืน JEEDS OK
        DataTable IDptClassInterface.MNPC_GetReportMNPCEditDt(string DateBegin, string DateEnd, string Branch)
        {
            string bchCondition = "";
            if (Branch != "") bchCondition = $@" AND Branch = '{Branch}' ";

            string sql = $@"
            SELECT	Branch AS BRANCH_ID,BranchName AS BRANCH_NAME,
		            CONVERT(VARCHAR,SHOP_MNPC_HD.DocDate,23) AS DocDate,SHOP_MNPC_HD.DocNO, 
		            Case ISNULL(STADOC,0)  when '3' then 'ยกเลิก' else  
			            Case ISNULL(STAPRCDOC,0)  when '0' then 'สาขาไม่ยืนยัน' else 
				            Case ISNULL(STACN,0)  when '0' then 'CN ไม่อนุมัติ' else 'อนุมัติ' 
				            end 
			            end 
		            end AS STADOC ,  
		            Barcode,ItemName,Qty,UnitID,CheckPurchase,SHOP_MNPC_DT.REMARK,
		            DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION AS DESCRIPTION, 
		            SHOP_MNPC_DT.PriceNet,REASONNAME

            FROM	SHOP_MNPC_DT WITH (NOLOCK) 
		            INNER JOIN SHOP_MNPC_HD WITH (NOLOCK) ON SHOP_MNPC_DT.DOCNO = SHOP_MNPC_HD.DOCNO 
		            INNER JOIN  (
				            SELECT	SHOW_ID as REASONID , SHOW_DESC as REASONNAME 
				            FROM	SHOP_CONFIGBRANCH_GenaralDetail  WITH (NOLOCK)
				            WHERE	TYPE_CONFIG='16'   AND STA='1' AND STAUSE='1'
			            )SHOP_CONFIGBRANCH_GenaralDetail16 on SHOP_MNPC_DT.CNReason = SHOP_CONFIGBRANCH_GenaralDetail16.REASONID
		            INNER JOIN SHOP2013TMP.dbo.DIMENSIONS   WITH (NOLOCK) ON SHOP_MNPC_DT.DeptCode =DIMENSIONS .NUM  AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC' 

            WHERE 	SHOP_MNPC_HD.DocDate between '{DateBegin}' and '{DateEnd}'  
		            AND StatusPurchase = '1' 
		            AND SHOP_MNPC_HD.stadoc = '1'  
		            AND SHOP_MNPC_HD.StaPrcDoc = '1' {bchCondition}

            ORDER BY BRANCH_ID,SHOP_MNPC_HD.DocNO,SHOP_MNPC_HD.DocDate
            ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายละเอียดการคืนตามบาร์โค้ดลดราคา
        DataTable IDptClassInterface.MNPC_GetReportDt(string DateBegin, string DateEnd, string Dept, string Branch)
        {

            string dptCondition = "", dptConditionDiscount = "";
            if (Dept != "") { dptCondition = $@" AND DeptCode = '{Dept}' "; dptConditionDiscount = $" AND SHOP_BARCODEDISCOUNT.DPTID = '{Dept}' "; }

            string bchCondition = "";
            if (Branch != "") bchCondition = $@" AND Branch = '{Branch}' ";


            string sql = $@"
                SELECT  SHOP_MNPC_HD.Docno,convert(varchar,SHOP_MNPC_HD .DocDate,23) as  DocDate,  
		                SHOW_NAME as  TYPEPRODUCT,      
		                Case ISNULL(STADOC,0)  when '3' then 'ยกเลิก' else  
		                Case ISNULL(STAPRCDOC,0)  when '0' then 'สาขาไม่ยืนยัน' else 
			                Case ISNULL(STACN,0)  when '0' then 'CN ไม่อนุมัติ' else 'อนุมัติ' 
			                end 
		                end  end AS  StatusDoc,   
		                BRANCH AS BRANCH_ID,SHOP_MNPC_HD.BranchName AS BRANCH_NAME,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_BARCODEDISCOUNT.ITEMBARCODE ELSE ISNULL(DISDIS.ITEMBARCODE,'') END AS BARCODE,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_BARCODEDISCOUNT.SPC_ITEMNAME ELSE ISNULL(DISDIS.SPC_ITEMNAME,'') END AS ItemName,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SUM(ISNULL(SHOP_BARCODEDISCOUNT.QTY,0)) ELSE SUM(ISNULL(DISDIS.QTY,0)) END AS QTY,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_BARCODEDISCOUNT.UNITID ELSE ISNULL(DISDIS.UNITID,'') END AS UNITID,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SUM(ISNULL(SHOP_BARCODEDISCOUNT.PRICE,0)) ELSE SUM(ISNULL(DISDIS.PRICE,0)) END AS PRICENET,
		                REASONNAME, 
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_BARCODEDISCOUNT.DPTID ELSE ISNULL(DISDIS.DPTID,'') END AS DPTID,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_BARCODEDISCOUNT.DPTNAME ELSE ISNULL(DISDIS.DPTNAME,'') END AS DPTNAME,
		                CASE ISNULL(DISDIS.ITEMBARCODE_DIS,'') WHEN '' THEN SHOP_BARCODEDISCOUNT.DPTID + ' - ' + SHOP_BARCODEDISCOUNT.DPTNAME ELSE DISDIS.DPTID + ' - ' + DISDIS.DPTNAME END AS NUMDESC,

		                '1' AS BARCODEDISCOUNT,
                        ISNULL(VEN_DIS.ACCOUNTNUM,'') AS VENDID,ISNULL(VEN_DIS.ACCOUNTNUM,'') AS VENDNAME
                        --CASE ISNULL(VEN_DIS.ACCOUNTNUM,'') WHEN '' THEN VEN_DISDIS.ACCOUNTNUM ELSE ISNULL(VEN_DIS.ACCOUNTNUM,'') END AS VENDID,
                        --CASE ISNULL(VEN_DIS.ACCOUNTNUM,'') WHEN '' THEN VEN_DISDIS.NAME ELSE ISNULL(VEN_DIS.NAME,'') END AS VENDNAME
                        ,SHOP_MNPC_DT.ItemID AS ITEMID,SHOP_MNPC_DT.Dimid AS INVENTDIMID

                FROM	SHOP_MNPC_HD  WITH (NOLOCK)
		                INNER JOIN SHOP_MNPC_DT   WITH (NOLOCK)  ON SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo    
		                INNER JOIN (
			                SELECT	SHOW_ID , SHOW_DESC ,SHOW_NAME  
			                FROM	SHOP_CONFIGBRANCH_GenaralDetail   WITH (NOLOCK) 
			                where	TYPE_CONFIG = '34'   AND STA = '1' AND STAUSE = '1'
		                )SHOP_CONFIGBRANCH_GenaralDetail34 on SHOP_MNPC_HD.TYPEPRODUCT=SHOP_CONFIGBRANCH_GenaralDetail34.SHOW_ID
		                INNER JOIN (
			                SELECT	SHOW_ID as REASONID , SHOW_DESC as REASONNAME 
			                FROM	SHOP_CONFIGBRANCH_GenaralDetail  WITH (NOLOCK)
			                where	TYPE_CONFIG = '16'   AND STA = '1' AND STAUSE = '1'
		                )SHOP_CONFIGBRANCH_GenaralDetail16 on SHOP_MNPC_DT.CNReason=SHOP_CONFIGBRANCH_GenaralDetail16.REASONID
		                INNER JOIN SHOP_BARCODEDISCOUNT WITH (NOLOCK) ON SHOP_MNPC_DT.Barcode = SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS {dptConditionDiscount}
                        LEFT OUTER JOIN SHOP_BARCODEDISCOUNT DISDIS WITH (NOLOCK) ON SHOP_BARCODEDISCOUNT.ITEMBARCODE = DISDIS.ITEMBARCODE_DIS AND SHOP_BARCODEDISCOUNT.DPTID = 'MN999' 
                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTTABLE INVENT_DIS WITH (NOLOCK) ON SHOP_BARCODEDISCOUNT.ITEMID = INVENT_DIS.ITEMID AND INVENT_DIS.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE VEN_DIS WITH (NOLOCK) ON INVENT_DIS.PRIMARYVENDORID = VEN_DIS.ACCOUNTNUM AND VEN_DIS.DATAAREAID = N'SPC'
		                --LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTTABLE INVENT_DISDIS WITH (NOLOCK) ON DISDIS.ITEMID = INVENT_DIS.ITEMID AND INVENT_DISDIS.DATAAREAID = N'SPC'
		                --LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE VEN_DISDIS WITH (NOLOCK) ON INVENT_DISDIS.PRIMARYVENDORID = VEN_DISDIS.ACCOUNTNUM AND VEN_DISDIS.DATAAREAID = N'SPC'

                WHERE	SHOP_MNPC_HD.DocDate BETWEEN '{DateBegin}' and '{DateEnd}' AND SHOP_MNPC_DT.StaItem = '1'
                        {bchCondition}
                            
                GROUP BY	SHOP_MNPC_HD.Docno,SHOP_MNPC_HD.DocDate,SHOW_NAME, 
			                SHOP_MNPC_HD.StaDoc,SHOP_MNPC_HD.STACN,SHOP_MNPC_HD.StaPrcDoc,   
                            BRANCH,SHOP_MNPC_HD.BranchName,
			                SHOP_BARCODEDISCOUNT.ITEMBARCODE,SHOP_BARCODEDISCOUNT.SPC_ITEMNAME,
		  	                SHOP_BARCODEDISCOUNT.UNITID,
			                REASONNAME,SHOP_BARCODEDISCOUNT.DPTID,SHOP_BARCODEDISCOUNT.DPTNAME,
                            ISNULL(DISDIS.ITEMBARCODE_DIS,''), ISNULL(DISDIS.ITEMBARCODE,''),
			                ISNULL(DISDIS.SPC_ITEMNAME,''),ISNULL(DISDIS.UNITID,''),
			                ISNULL(DISDIS.DPTID,''),ISNULL(DISDIS.DPTNAME,''),DISDIS.DPTID + ' - ' + DISDIS.DPTNAME,
                            ISNULL(VEN_DIS.ACCOUNTNUM,''),ISNULL(VEN_DIS.ACCOUNTNUM,'')
                            --CASE ISNULL(VEN_DIS.ACCOUNTNUM,'') WHEN '' THEN VEN_DISDIS.ACCOUNTNUM ELSE ISNULL(VEN_DIS.ACCOUNTNUM,'') END,CASE ISNULL(VEN_DIS.ACCOUNTNUM,'') WHEN '' THEN VEN_DISDIS.NAME ELSE ISNULL(VEN_DIS.NAME,'') END
                            ,SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid

                UNION

                SELECT  SHOP_MNPC_HD.Docno,convert(varchar,SHOP_MNPC_HD .DocDate,23) as  DocDate,  
		                SHOW_NAME as  TYPEPRODUCT,      
		                Case ISNULL(STADOC,0)  when '3' then 'ยกเลิก' else  
		                Case ISNULL(STAPRCDOC,0)  when '0' then 'สาขาไม่ยืนยัน' else 
			                Case ISNULL(STACN,0)  when '0' then 'CN ไม่อนุมัติ' else 'อนุมัติ' 
			                end 
		                end  end AS  StatusDoc,   
		                BRANCH AS BRANCH_ID,SHOP_MNPC_HD.BranchName AS BRANCH_NAME,SHOP_MNPC_DT.Barcode ,  
		                SHOP_MNPC_DT.ItemName , sum(isnull(SHOP_MNPC_DT.Qty ,0)) as Qty,SHOP_MNPC_DT.UnitID , 
		                sum(isnull(SHOP_MNPC_DT.PriceNet ,0)) as PriceNet,REASONNAME, 
		                DIMENSIONS.NUM ,DIMENSIONS.DESCRIPTION,DIMENSIONS.NUM + ' - ' + DIMENSIONS.DESCRIPTION AS NUMDESC,'0' AS BARCODEDISCOUNT,PRIMARYVENDORID AS VENDID,VENDTABLE.NAME AS VENDNAME
                        ,SHOP_MNPC_DT.ItemID  AS ITEMID ,SHOP_MNPC_DT.Dimid AS INVENTDIMID

                FROM	SHOP_MNPC_HD  WITH (NOLOCK)
		                INNER JOIN SHOP_MNPC_DT   WITH (NOLOCK)  ON SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo   
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS   WITH (NOLOCK)   ON SHOP_MNPC_DT.DeptCode = DIMENSIONS .NUM  
			                AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC'  
		                INNER JOIN (
			                SELECT	SHOW_ID , SHOW_DESC ,SHOW_NAME  
			                FROM	SHOP_CONFIGBRANCH_GenaralDetail   WITH (NOLOCK) 
			                where	TYPE_CONFIG = '34'   AND STA = '1' AND STAUSE = '1'
		                )SHOP_CONFIGBRANCH_GenaralDetail34 on SHOP_MNPC_HD.TYPEPRODUCT=SHOP_CONFIGBRANCH_GenaralDetail34.SHOW_ID
		                INNER JOIN (
			                SELECT	SHOW_ID as REASONID , SHOW_DESC as REASONNAME 
			                FROM	SHOP_CONFIGBRANCH_GenaralDetail  WITH (NOLOCK)
			                where	TYPE_CONFIG = '16'   AND STA = '1' AND STAUSE = '1'
		                )SHOP_CONFIGBRANCH_GenaralDetail16 on SHOP_MNPC_DT.CNReason=SHOP_CONFIGBRANCH_GenaralDetail16.REASONID
                        LEFT OUTER JOIN SHOP_BARCODEDISCOUNT WITH (NOLOCK) ON SHOP_MNPC_DT.Barcode = SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS  
                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON SHOP_MNPC_DT.ItemID = INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
						LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON INVENTTABLE.PRIMARYVENDORID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = N'SPC'

                WHERE	SHOP_MNPC_HD.DocDate BETWEEN '{DateBegin}' and '{DateEnd}' AND SHOP_MNPC_DT.StaItem = '1'
                        AND ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS  ,'') = '' 
                        {dptCondition}  {bchCondition}  

                GROUP BY	SHOP_MNPC_HD.Docno,SHOP_MNPC_HD.DocDate,SHOW_NAME, 
			                SHOP_MNPC_HD.StaDoc,SHOP_MNPC_HD.STACN,SHOP_MNPC_HD.StaPrcDoc,   
                            BRANCH,SHOP_MNPC_HD.BranchName,SHOP_MNPC_DT.Barcode,SHOP_MNPC_DT.ItemName,SHOP_MNPC_DT.UnitID,
			                REASONNAME, DIMENSIONS.NUM ,DIMENSIONS.DESCRIPTION,PRIMARYVENDORID,VENDTABLE.NAME
                            ,SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid 

                ORDER BY	Branch, SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid --Docno,DocDate,Branch,Barcode
            ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายละเอียดการคืนตามบาร์โค้ดลดราคา
        DataTable IDptClassInterface.MNPC_GetReportSumAllDim(string DateBegin, string DateEnd, string Dept, string Branch)
        {
            string dptCondition = "";
            if (Dept != "") { dptCondition = $@" AND DeptCode = '{Dept}' "; }

            string bchCondition = "";
            if (Branch != "") bchCondition = $@" AND Branch = '{Branch}' ";

            string sql = $@"
                    SELECT	BRANCH_ID,BRANCH_NAME,ITEMBARCODE,SPC_ITEMNAME,QtySum,TMP_BARCODE.UNITID,PRICENET,NUM,DESCRIPTION,NUMDESC,VENDID,VENDNAME,TMP_BARCODE.ITEMID,TMP_BARCODE.INVENTDIMID
                    FROM	(		
			                    SELECT  BRANCH AS BRANCH_ID,SHOP_MNPC_HD.BranchName AS BRANCH_NAME, SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid, 
						                    SUM(isnull(SHOP_MNPC_DT.QtySum,0)) as QtySum, 
					                    SUM(ISNULL(SHOP_MNPC_DT.PriceNet ,0)) as PriceNet,
					                    DIMENSIONS.NUM ,DIMENSIONS.DESCRIPTION,DIMENSIONS.NUM + ' - ' + DIMENSIONS.DESCRIPTION AS NUMDESC, PRIMARYVENDORID AS VENDID,VENDTABLE.NAME AS VENDNAME

			                    FROM	SHOP_MNPC_HD  WITH (NOLOCK)
					                    INNER JOIN SHOP_MNPC_DT   WITH (NOLOCK)  ON SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo   
					                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS   WITH (NOLOCK)   ON SHOP_MNPC_DT.DeptCode = DIMENSIONS .NUM  
						                    AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC'  
					                    LEFT OUTER JOIN SHOP_BARCODEDISCOUNT WITH (NOLOCK) ON SHOP_MNPC_DT.Barcode = SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS  
					                    LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON SHOP_MNPC_DT.ItemID = INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
					                    LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON INVENTTABLE.PRIMARYVENDORID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = N'SPC'

			                    WHERE	SHOP_MNPC_HD.DocDate BETWEEN '{DateBegin}' and '{DateEnd}' AND SHOP_MNPC_DT.StaItem = '1'
					                    AND ISNULL(SHOP_BARCODEDISCOUNT.ITEMBARCODE_DIS  ,'') = '' 
					                    AND STACN = '1'
                                        {bchCondition} {dptCondition}
                            
			                    GROUP BY	BRANCH,SHOP_MNPC_HD.BranchName,SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid,
						                    DIMENSIONS.NUM ,DIMENSIONS.DESCRIPTION,PRIMARYVENDORID,VENDTABLE.NAME
		                    )TMP INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE_MINMAX  TMP_BARCODE 
				                    ON TMP.ItemID = TMP_BARCODE.ITEMID 
				                    AND TMP.Dimid = TMP_BARCODE.INVENTDIMID 
				                    AND	TYPE_ = 'MIN' AND DATAAREAID = 'SPC'
                    WHERE   QtySum > 0
                    ORDER BY PRICENET DESC
            ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
