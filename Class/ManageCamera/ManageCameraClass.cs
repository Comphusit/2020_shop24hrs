﻿using PC_Shop24Hrs.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_Shop24Hrs.Class.ManageCamera
{
    class ManageCameraClass : IManageCameraInterface
    {
        DataTable IManageCameraInterface.GetCamStation(string date, string typeStation, string _pUserpermis, int round)
        {
            string sql = $@"
            SELECT	SHOP_JOBCAM.CAM_ID,SHOP_JOBCAM.CAM_IP,SHOP_JOBCAM.CAM_DESC,
                    CAM_CH,SHOP_JOBCAM.BRANCH_ID,BRANCH_NAME,CAM_AMORN,CAM_TYPECAM,
                    CASE WHEN SHOP_JOBCAM.BRANCH_ID = 'MN000' THEN CAM_DEPT ELSE SHOP_JOBCAM.BRANCH_ID END AS CAM_DEPT,
                    CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN [DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
		            CHECK_EMPNICKNAME,CHECK_EMPID,CHECK_EMPNAME,CHECK_DATETIME,CAM_RECORD,
		            CONVERT(VARCHAR,CAM_DATERECORD,23) AS CAM_DATERECORD,ISNULL(JOB_Number,'') AS CAM_CHECKJOBID,
		            CONVERT(VARCHAR,JOB_DATEINS,23) AS JOB_DATEINS,
		            CASE WHEN CHECK_EMPID != '' THEN '1' ELSE '0' END AS CAM_CHECK,'1' AS CHECK_USERTYPE,
		            CHECK_REMARK,CAM_TECHNICID,CAM_TECHNICNAME,
		            SHOP_JOBCAM.CAM_DVR,SHOW_ID,ISNULL([CHANNEL] ,'') AS CAM_REMARKCHANNEL,[COUNT],[PATH_IMAGE],
		            ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
            FROM    SHOP_JOBCAM WITH (NOLOCK) 
                    LEFT OUTER JOIN  SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
		            INNER JOIN  [SHOP_JOBCAMSTATION] WITH (NOLOCK) ON SHOP_JOBCAM.CAM_ID = [SHOP_JOBCAMSTATION].CAM_ID
                    LEFT OUTER JOIN  
			            ( 
			            SELECT  ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) as Seq ,*
			            FROM    SHOP_JOBComMinimart where JOB_STACLOSE='0' and  JOB_GROUPSUB='00003' 
			            )JOB    ON JOB.JOB_SN = SHOP_JOBCAM.CAM_IP  AND JOB_STACLOSE = '0'  AND Seq='1'
                    LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM  
			            AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = 'SPC'
                    LEFT OUTER JOIN  
                        ( 
                        SELECT  *,CONVERT(VARCHAR,CHECK_DATEINS,25) AS CHECK_DATETIME 
                        FROM    SHOP_JOBCAMCHECK  WITH (NOLOCK) 
                        WHERE   CAM_DATECHECK = '{date}' AND [CHECK_STATION] = '{typeStation}' AND [COUNT] = '{round}'
			            )TMP ON SHOP_JOBCAM.CAM_ID = TMP.CAM_ID 
			                    AND CHECK_USERTYPE = '1' AND SHOW_ID = '{typeStation}' AND [COUNT] = '{round}'
                    LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '0' )CAM_PASS 
			            ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
            WHERE   CAM_TYPECAM = 'MN'  
            ORDER BY CAM_DESC    ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IManageCameraInterface.Getdatacheck(string Condition, string date, string _pUserpermis)
        {
            string sql = $@" SELECT SHOP_JOBCAM.CAM_ID,
                            SHOP_JOBCAM.CAM_IP,
                            Shop_JOBCAM.CAM_DESC,
                            CAM_CH,
                            SHOP_JOBCAM.BRANCH_ID,
                            BRANCH_NAME,
                            CAM_AMORN,
                            CAM_TYPECAM,
                            CASE WHEN SHOP_JOBCAM.BRANCH_ID = 'MN000' THEN CAM_DEPT ELSE SHOP_JOBCAM.BRANCH_ID END AS CAM_DEPT,
                            CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN [DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
                            CHECK_EMPNICKNAME,
                            CHECK_EMPID,
                            CHECK_EMPNAME,
                            CHECK_DATETIME,
                            CAM_RECORD,
                            CONVERT(VARCHAR,CAM_DATERECORD,23) AS CAM_DATERECORD,
                            ISNULL(JOB_Number,'') AS CAM_CHECKJOBID,
                            CONVERT(VARCHAR,JOB_DATEINS,23) AS JOB_DATEINS,
                            CASE WHEN CHECK_EMPID != '' THEN '1' ELSE '0' END AS CAM_CHECK,'{0}' AS CHECK_USERTYPE,
                            CHECK_REMARK,
                            CAM_TECHNICID,
                            CAM_TECHNICNAME,
                            SHOP_JOBCAM.CAM_DVR,
                    [CHECK_STATION] as SHOW_ID,[COUNT],PATH_IMAGE,'' AS CAM_REMARKCHANNEL
                    ,ISNULL(SET_USER,'') AS  SET_USER,ISNULL(SET_PASSWORD,'') AS  SET_PASSWORD
            FROM    SHOP_JOBCAM WITH (NOLOCK) 
                    LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_JOBCAM.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                    LEFT OUTER JOIN ( 
			                        SELECT  ROW_NUMBER() OVER(PARTITION BY JOB_SN ORDER BY JOB_DATEINS DESC) as Seq ,*
			                        FROM    SHOP_JOBComMinimart where JOB_STACLOSE='0' and  JOB_GROUPSUB='00003' 
			                        )JOB   
				                    ON JOB.JOB_SN = SHOP_JOBCAM.CAM_IP AND JOB_STACLOSE = '0'  AND Seq='1'
                    LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_JOBCAM.CAM_DEPT = DIMENSIONS.NUM  AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = 'SPC'
                    LEFT OUTER JOIN ( 
                                    SELECT  *,CONVERT(VARCHAR,CHECK_DATEINS,25) AS CHECK_DATETIME 
                                    FROM    SHOP_JOBCAMCHECK  WITH (NOLOCK) 
                                    WHERE   CAM_DATECHECK = '{date}' 
                                    )TMP    ON SHOP_JOBCAM.CAM_ID = TMP.CAM_ID AND CHECK_USERTYPE = '{_pUserpermis}' 
                                            AND [CHECK_STATION] = '' 
                    LEFT OUTER JOIN (SELECT	SET_USER,SET_PASSWORD,CAM_DVR,REMARK FROM	SHOP_JOBCAM_SETDVR WITH (NOLOCK) WHERE	TYPE_PERMISSION = '{_pUserpermis}' )CAM_PASS 
			                ON SHOP_JOBCAM.CAM_DVR = CAM_PASS.CAM_DVR
            {Condition} ORDER BY CAM_DESC";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IManageCameraInterface.GetDataPermissioByEmpID(string _pTopID, string _pEmpID)
        {
            string sql = $@"
            SELECT  *   
            FROM    (    
                SELECT {_pTopID} SHOP_JOBCAM_PERMISSION.CAM_ID
                        ,EMP_ID,EMPLTABLE.SPC_NAME AS USERNAME,SHOP_JOBCAM.CAM_DESC
                        ,USERDVR,PASSDVR,DEPTCODE,DIMENSIONS.[DESCRIPTION] AS DEPTNAME,WHONAMEINS,REMARK,DIMENSIONS.DATAAREAID
                FROM    SHOP_JOBCAM_PERMISSION WITH (NOLOCK)
                        INNER JOIN SHOP_JOBCAM WITH (NOLOCK) ON SHOP_JOBCAM_PERMISSION.CAM_ID =  SHOP_JOBCAM.CAM_ID
                        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_JOBCAM_PERMISSION.EMP_ID = EMPLTABLE.ALTNUM   
                            AND EMPLTABLE.DATAAREAID = 'SPC' 
                        LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM 
                            AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                    )TMP  
            WHERE DATAAREAID = 'SPC' {_pEmpID} ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IManageCameraInterface.GetDataPermissionComName()
        {
            string sql = $@"
            SELECT	COM_NAME,
		            CASE WHEN SHOP_BRANCH_PERMISSION.BRANCH_ID = 'MN000' THEN SHOP_BRANCH_PERMISSION.DEPT_ID ELSE SHOP_BRANCH_PERMISSION.BRANCH_ID END AS BRANCH_ID,COM_ALLBRANCH,
		            SHOP_BRANCH_PERMISSION.BRANCH_ID AS ID,SHOP_BRANCH_PERMISSION.DEPT_ID,
		            CASE WHEN SHOP_BRANCH.BRANCH_NAME = 'ซุปเปอร์ชีป' THEN DIMENSIONS.[DESCRIPTION] ELSE SHOP_BRANCH.BRANCH_NAME END AS [DESCRIPTION],
		            SHOP_BRANCH_PERMISSION.SHOW_ID AS SHOW_ID,SHOW_DESC, VERSIONUPD, PRICEGROUP, CHANNEL, BRANCHUPD,
		            SHOP_BRANCH_PERMISSION.DATEUPD AS DATEUPD,SHOP_BRANCH_PERMISSION.WHOIDUPD AS WHOIDUPD,
		            SHOP_BRANCH_PERMISSION.WHONAMEUPD AS WHONAMEUPD,SHOP_BRANCH_PERMISSION.REMARK 
            FROM	SHOP24HRS.dbo.SHOP_BRANCH_PERMISSION WITH (NOLOCK)
		            INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = SHOP_BRANCH_PERMISSION.BRANCH_ID
		            LEFT OUTER JOIN  [SHOP_CONFIGBRANCH_GenaralDetail] ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = SHOP_BRANCH_PERMISSION.SHOW_ID  AND   TYPE_CONFIG = '15'       
                    LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON SHOP_BRANCH_PERMISSION.DEPT_ID = DIMENSIONS.NUM
                        AND DATAAREAID = 'SPC' AND DIMENSIONCODE = 0
            ORDER BY SHOW_ID ASC, BRANCH_ID,DEPT_ID ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IManageCameraInterface.GetDataDptPermissionDptMenu(string pCase, string _pType)//0 แผนกที่มีสิิด 1 แผนกที่ไม่มีสิด
        {
            string sql;
            if (pCase == "0")
            {
                string dpt = "";
                if (_pType == "SUPC") dpt = " AND DEPT_ID LIKE 'D%' ";
                sql = $@"
                    SELECT  DEPT_ID,CASE ISNULL(BRANCH_ID,'') WHEN '' THEN DESCRIPTION ELSE BRANCH_NAME END AS DESCRIPTION
                    FROM    SHOP_DEPT_PERRMISSION WITH (NOLOCK)
                            INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) on SHOP_DEPT_PERRMISSION.DEPT_ID=DIMENSIONS.NUM 
		                    LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_DEPT_PERRMISSION.DEPT_ID = SHOP_BRANCH.BRANCH_ID
                    WHERE   DATAAREAID ='SPC' AND DIMENSIONCODE = '0'  {dpt}
                    ORDER BY DEPT_ID ";
            }
            else
            {
                sql = $@"
                    SELECT	num,num+ ' '+DESCRIPTION  AS Dimensionname 
                    FROM	SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)
                    WHERE	num NOT IN (SELECT	dept_id 
					                    FROM SHOP_DEPT_PERRMISSION WITH (NOLOCK) )  
		                    AND DIMENSIONCODE = 0 
		                    AND COMPANYGROUP !='Cancel' 
		                    AND DESCRIPTION !='' 
                            AND DATAAREAID ='SPC' 
                    ORDER BY num ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
