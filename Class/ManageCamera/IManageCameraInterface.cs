﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_Shop24Hrs.Class.ManageCamera
{
    interface IManageCameraInterface
    {
        //ค้นหาข้อมูลการตรวจกล้องทั้งหมด  ตาม Station
        DataTable GetCamStation(string date, string typeStation, string _pUserpermis, int round);

        //ดึงข้อมูลตรวจกล้อง
        DataTable Getdatacheck(string Condition, string date, string _pUserpermis);

        //เช็คสิทธิ์ดูกล้อง
        DataTable GetDataPermissioByEmpID(string _pTopID, string _pEmpID);

        DataTable GetDataPermissionComName();

        //ค้นหาแผนกเพือกำหนดสิทธิ์ในการใช้งานเมนู
        DataTable GetDataDptPermissionDptMenu(string pCase, string _pType);//0 แผนกที่มีสิิด 1 แผนกที่ไม่มีสิด
    }
}
