﻿using System;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class
{
    class LogisticClass
    {
        //ค้นหา Tag ทั้งหมด
        public static DataTable GetDataTagAll()
        {
            string sql = $@"
                SELECT	TMPPP.*,
		                CASE Branch WHEN 'MN000' THEN DIMENSIONS.DESCRIPTION ELSE BRANCH_NAME END AS BranchName
                FROM	(
                SELECT	TagAll.Branch,TagAll.TagDpt,  
		                ISNULL(TagAll.CountTagAll, 0) AS CountTagAll, ISNULL(Taguse.CountTagUse, 0) AS CountTagUse,
		                ISNULL(TagAll.CountTagAll, 0)-ISNULL(TagUse.CountTagUse, 0) AS CountTagAva,
		                ISNULL(CountTagBeUse, 0) AS CountTagBeUse, ISNULL(CountTagToDay, 0) AS CountTagToDay,  convert(varchar, MaxDate,23) as MaxDate
                FROM	(
		                SELECT  Branch,  ISNULL(TagDpt, '') AS TagDpt, COUNT(Branch) AS CountTagAll
		                FROM    Shop_TagCarTruck WITH(NOLOCK)
		                GROUP BY Branch, TagDpt
		                )TagAll LEFT JOIN
		                (
			                SELECT  Branch, ISNULL(TagDpt, '') AS TagDpt, COUNT(Branch) AS CountTagUse
			                FROM    Shop_TagCarTruck WITH(NOLOCK)
			                WHERE   StaLock = '1'
			                GROUP BY Branch, TagDpt
		                )TagUse ON TagAll.Branch = Taguse.Branch AND TagAll.TagDpt = Taguse.TagDpt
		                LEFT JOIN
		                (
			                SELECT Branch,  ISNULL(TagDpt,'') AS TagDpt, COUNT(Branch) AS CountTagBeUse
			                FROM   Shop_TagCarTruck WITH(NOLOCK)
			                WHERE StaLock = '1' AND StaUnlock = '0'
			                GROUP BY Branch,TagDpt
		                )TagBeUse ON TagAll.Branch = TagBeUse.Branch AND TagAll.TagDpt = TagBeUse.TagDpt
			                LEFT JOIN
		                (
			                SELECT Branch,  ISNULL(TagDpt,'') AS TagDpt, COUNT(Branch) AS CountTagToDay
			                FROM   Shop_TagCarTruck WITH(NOLOCK)
			                WHERE StaLock = '0' AND StaUnlock = '0'
			                AND DateIns = (SELECT max(DateIns) FROM Shop_TagCarTruck WITH(NOLOCK) WHERE StaLock = '0' AND StaUnlock = '0'  )
			                GROUP BY Branch,TagDpt
		                )TagToDay ON TagAll.Branch = TagToDay.Branch AND TagAll.TagDpt = TagToDay.TagDpt
		                LEFT JOIN
		                (
			                SELECT Branch,  ISNULL(TagDpt,'') AS TagDpt, max(DateIns)  AS MaxDate
			                FROM   Shop_TagCarTruck WITH(NOLOCK)
			                GROUP BY Branch,TagDpt
		                )MaxDate ON TagAll.Branch = MaxDate.Branch AND TagAll.TagDpt = MaxDate.TagDpt
		
		                )TMPPP
		                LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMPPP.Branch = SHOP_BRANCH.BRANCH_ID
		                LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON TMPPP.TagDpt = DIMENSIONS.NUM AND DIMENSIONCODE = '0' AND DATAAREAID = 'SPC'

                ORDER BY Branch,TagDpt
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหา Tag ตามรอบรถ
        public static DataTable GetDataTagByLogistic(string TRANSFERDATE, string DEPTLOGISTIC,
         string ROUTEID, string EMPLCLERK, string LOGISTICID, string VEHICLEID, string Branch, string Deduct,
         string SqlBranch, string MaxCar, string TypeLO = "")
        {
            string WhereBranch = "";
            if (Branch != "MN000")
            {
                WhereBranch = string.Format("AND BranchLock='{0}' OR BranchUnlock='{0}'", Branch);
                if (SqlBranch != "") SqlBranch = "AND " + SqlBranch;
            }
            else
            {
                if (SqlBranch != "") SqlBranch = "Where " + SqlBranch;
            }

            string conTypeLO = "";
            if (TypeLO != "") conTypeLO = $@" AND TYPELOGISTIC = '{TypeLO}' ";

            string str = $@"SELECT isnull(SHOP_CONFIGBRANCH_GenaralDetail.MaxImage,1) as MaxImage,LoDetail.*,TagLockUnlock.* FROM  
                    ( 
                    SELECT	LOGISTICID,VEHICLEID,EMPLDRIVER  +','+ CHAR(13) + CHAR(10)+ SPC_NAME as EMPLDRIVER,EMPLCLERK,TRASHREMARK ,
					        DEPTLOGISTIC+' : '+ DEPTNAME as DEPTLO,DEPTLOGISTIC,DEPTNAME ,
                            DATEADD(HOUR,7,SHIPPINGDATETIME) AS SHIPPINGDATETIME,
					        DATEADD(HOUR,7,ARRIVALDATETIME) AS ARRIVALDATETIME,
					        SPC_LOGISTICTABLE.ROUTEID,NAME AS ROUTENAME,SPC_LOGISTICTABLE.ROUTEID+' '+NAME AS ROUTEIDDESC,
                            CONVERT(VARCHAR,DATEADD(HOUR,7,SHIPPINGDATETIME),23) AS SHIPPINGDATE 
                    FROM    SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK)  
                                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID 
                                        AND EMPLTABLE.DATAAREAID = 'SPC' 
                                INNER JOIN SHOP2013TMP .dbo.SPC_LOGISTICDEPARTMENT WITH (NOLOCK)  ON SPC_LOGISTICTABLE.DEPTLOGISTIC=SPC_LOGISTICDEPARTMENT .DEPTID  
                                        AND SPC_LOGISTICDEPARTMENT .DATAAREAID ='SPC'
                                INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.ROUTEID = SPC_ROUTETABLE.ROUTEID
                    where   SPC_LOGISTICTABLE.DATAAREAID ='SPC'
                            AND EMPLTABLE.DATAAREAID ='SPC' 
					        AND SPC_LOGISTICTABLE.TRANSFERDATE='{TRANSFERDATE}'
					        {conTypeLO} ";
            if (DEPTLOGISTIC !="") str += $@" AND DEPTLOGISTIC = '{DEPTLOGISTIC}' ";
            if (ROUTEID != "") str += @"AND SPC_LOGISTICTABLE.ROUTEID = '" + ROUTEID + @"'";
            if (EMPLCLERK != "") str += @"AND EMPLCLERK = '" + EMPLCLERK + @"'";
            if (LOGISTICID != "") str += @"AND LOGISTICID = '" + LOGISTICID + @"'";
            if (VEHICLEID != "") str += @"AND VEHICLEID like '%" + VEHICLEID + @"%'";

            str += $@")LoDetail 
                    INNER join 
                    (							 
                        SELECT ROW_NUMBER() OVER(ORDER BY LOGISTICID,DateLock,TimeLock ASC) AS TagSeq,
	                        TagNumber,TagFullNumber,LOGISTICID,Branch,
	                        Branch  +','+  CHAR(13) + CHAR(10)+ BranchName as BranchName,
						    WhoNameIns,CONVERT(VARCHAR,DateIns,23) AS DateIns , StaLock, BranchLock,
						    BranchLock + CHAR(13) + CHAR(10)+ BranchNameLock as BranchNameLock ,WhoLock,
	                        WhoLock  +','+ CHAR(13) + CHAR(10)+ WhoNameLock as WhoNameLock,
						    CONVERT(VARCHAR,DateLock,23) AS DateLock,TimeLock, StaUnlock, BranchUnlock,
						    BranchUnlock + CHAR(13) + CHAR(10)+ BranchNameUnlock as BranchNameUnlock,ISNULL(WhoUnlock,'') AS WhoUnlock,
	                        ISNULL(WhoUnlock +','+ CHAR(13) + CHAR(10)+ WhoNameUnLock,'') as WhoNameUnLock, 
						    CONVERT(VARCHAR,DateUnlock,23) as DateUnlock,TimeUnlock,TagRemark,TagDpt      
                        FROM	Shop_TagCarTruck WITH (NOLOCK) 
                        WHERE DateLock between '{DateTime.Parse(TRANSFERDATE).AddDays(-3):yyyy-MM-dd}' and '{DateTime.Parse(TRANSFERDATE).AddDays(+1):yyyy-MM-dd}' {Deduct}
                    )TagLockUnlock
                    on LoDetail.LOGISTICID=TagLockUnlock.LOGISTICID
                        left outer join SHOP_CONFIGBRANCH_GenaralDetail on LoDetail.VEHICLEID=SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                   WHERE isnull(SHOP_CONFIGBRANCH_GenaralDetail.MaxImage,1) {MaxCar}  {WhereBranch} {SqlBranch} 
                    order by LoDetail.LOGISTICID,TagSeq";
            return ConnectionClass.SelectSQL_Main(str);
        }

        //CountBox LO
        public static DataTable GetBoxSum_ByLO(string pLO)
        {
            string sql = $@"
                SELECT	LOGISTICID,COUNT(LOGISTICID) AS SUM_BOX 
                FROM	SHOP2013TMP.dbo.SPC_SHIPMENTLINE  WITH (NOLOCK)  INNER jOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE  WITH (NOLOCK) ON SPC_SHIPMENTLINE.SHIPMENTID = SPC_SHIPMENTTABLE.SHIPMENTID  
		                    INNER JOIN SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK) ON SPC_SHIPMENTLINE.DOCUMENTNUM = SPC_BOXFACESHEET.DOCUMENTNUM   
		                    INNER JOIN SHOP2013TMP.dbo.SPC_VEHICLETABLE  WITH (NOLOCK) ON SPC_SHIPMENTTABLE.VEHICLEID = SPC_VEHICLETABLE.VEHICLEID   
                WHERE	LOGISTICID  = '{pLO}' 
                GROUP BY LOGISTICID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //
        public static DataTable GetCommissionForReciveBox(string pDate, string pCarBig, string pCarSmall)
        {
            string str = $@"
                SELECT	CONVERT(VARCHAR,DATEIN,23) AS DATEIN,BOXBRANCH,SPC_SHIPMENTTABLE.VEHICLEID,SPC_VEHICLETABLE.NAME,
		                SPC_SHIPMENTTABLE.LOGISTICID,COUNT(SPC_SHIPMENTTABLE.LOGISTICID) AS COUNT_BOX, 
		                CASE WHEN SPC_VEHICLETABLE.NAME LIKE '%10 ล้อ%' THEN '{pCarBig}' ELSE '{pCarSmall}' END AS RATE, 
		                MNPRWhoEmpDown,(COUNTBOX) AS COUNTBOX 
                FROM	SHOP2013TMP.dbo.SPC_SHIPMENTLINE  WITH (NOLOCK)  
			                INNER jOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE  WITH (NOLOCK)	ON SPC_SHIPMENTLINE.SHIPMENTID = SPC_SHIPMENTTABLE.SHIPMENTID 
			                INNER JOIN SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK) ON SPC_SHIPMENTLINE.DOCUMENTNUM = SPC_BOXFACESHEET.DOCUMENTNUM 
			                INNER JOIN SHOP2013TMP.dbo.SPC_VEHICLETABLE  WITH (NOLOCK) ON SPC_SHIPMENTTABLE.VEHICLEID = SPC_VEHICLETABLE.VEHICLEID	
			                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_SHIPMENTLINE.CUSTACCOUNT = SHOP_BRANCH.BRANCH_ID AND BRANCH_OUTPHUKET = '0' 
			                INNER JOIN ( 
			                SELECT	DATEIN,BOXBRANCH,SHIPMENTID,MNPRWhoEmpDown,COUNT(COUNTBOX) AS COUNTBOX 
			                FROM (SELECT	CONVERT(VARCHAR,DATEIN,23) AS DATEIN,BOXBRANCH,BOXNUMBER,SHIPMENTID,MNPRWhoEmpDown,COUNT(MNPRWhoEmpDown) AS COUNTBOX  
			                FROM	Shop_recivebox WITH (NOLOCK)	
			                WHERE	MNPRWhoEmpDown IS NOT NULL AND CONVERT(VARCHAR,DATEIN,23) = '{pDate}'  AND MNPRWhoEmpDown !=''
			                GROUP BY BOXBRANCH,BOXNUMBER,SHIPMENTID,MNPRWhoEmpDown,DATEIN)TT  WHERE	SHIPMENTID != ''  
			                GROUP BY BOXBRANCH,SHIPMENTID,MNPRWhoEmpDown,CONVERT(VARCHAR,DATEIN,23))TMPA ON SPC_SHIPMENTLINE.SHIPMENTID = TMPA.SHIPMENTID 
                GROUP BY BOXBRANCH,SPC_SHIPMENTTABLE.VEHICLEID,SPC_VEHICLETABLE.NAME,SPC_SHIPMENTTABLE.LOGISTICID,MNPRWhoEmpDown ,COUNTBOX,DATEIN  ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาข้อมูล LO จาก SHIH
        public static string FindLO_BySH(string shID)
        {
            string sql = $@" 
            SELECT	LOGISTICID  
            FROM	SHOP2013TMP.dbo.SPC_SHIPMENTTABLE WITH (NOLOCK) 
            WHERE	SHIPMENTID LIKE '%{shID}%' GROUP BY LOGISTICID ";
            return sql;
        }
        //ค้นหา SH จาก LO
        public static DataTable FindSH_ByLO(string loID, string bchID)
        {
            string str = $@"
                SELECT	LOGISTICID,SPC_SHIPMENTLINE.SHIPMENTID,CUSTACCOUNT  
                FROM	SHOP2013TMP.dbo. SPC_SHIPMENTTABLE WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTLINE WITH (NOLOCK)   ON SPC_SHIPMENTTABLE.SHIPMENTID = SPC_SHIPMENTLINE.SHIPMENTID  
                WHERE	LOGISTICID =  '{loID}' AND CUSTACCOUNT = '{bchID}' 
                        AND SPC_SHIPMENTTABLE.DATAAREAID ='SPC'AND SPC_SHIPMENTLINE.DATAAREAID ='SPC'  
                GROUP BY LOGISTICID,SPC_SHIPMENTLINE.SHIPMENTID,CUSTACCOUNT ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาข้อมูลรายละเอียดของ LO ตามเลขที่ LO
        public static DataTable GetLogisticDetail_ByLO(string loID)
        {
            string str = $@"
            SELECT	LOGISTICID,VEHICLEID,EMPLDRIVER,SPC_NAME,EMPLCLERK,TRASHREMARK ,
                    DEPTLOGISTIC+' : '+ DEPTNAME as DEPTLO,DEPTLOGISTIC,DEPTNAME,
                    DATEADD(HOUR,7,SHIPPINGDATETIME) AS SHIPPINGDATETIME,
                    DATEADD(HOUR,7,ARRIVALDATETIME) AS ARRIVALDATETIME,SPC_LOGISTICTABLE.ROUTEID, 
                    CONVERT(VARCHAR,DATEADD(HOUR,7,SHIPPINGDATETIME),23) AS SHIPPINGDATE ,SPC_ROUTETABLE.NAME AS ROUTENAME  
            FROM    SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK)  
            		LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK)  ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID		AND EMPLTABLE.DATAAREAID = 'SPC' 
		            INNER JOIN SHOP2013TMP.dbo.SPC_LOGISTICDEPARTMENT  ON SPC_LOGISTICTABLE.DEPTLOGISTIC=SPC_LOGISTICDEPARTMENT .DEPTID   AND SPC_LOGISTICDEPARTMENT .DATAAREAID ='SPC' 
		            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.ROUTEID = SPC_ROUTETABLE.ROUTEID AND SPC_ROUTETABLE.DATAAREAID = N'SPC'
            WHERE   LOGISTICID ='{loID}'  
                    AND SPC_LOGISTICTABLE.DATAAREAID ='SPC' AND EMPLTABLE.DATAAREAID ='SPC' ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาข้อมูล LO โดยที่ไม่ Join กับตารางอื่นๆ
        public static DataTable GetLogistic_ByLO(string loID)
        {
            string sqlMNRD = $@"
                SELECT	CONVERT(VARCHAR,TRANSFERDATE,23) AS TRANSFERDATE,SPC_LOGISTICTABLE.LOGISTICID,SPC_LOGISTICTABLE.ROUTEID,NAME,   
                        VEHICLEID,EMPLDRIVER,SPC_NAME,TRASHREMARK  
                FROM	SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK)        
                        INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK)   ON SPC_ROUTETABLE.ROUTEID = SPC_LOGISTICTABLE.ROUTEID    
                        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON   SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'     
                WHERE	SPC_LOGISTICTABLE.LOGISTICID = '{loID}'   ";
            return ConnectionClass.SelectSQL_Main(sqlMNRD);
        }
        //ค้นหาข้อมูล LO ที่เกี่ยวกับ TagCarTruck ตามเลขที่ LO
        public static DataTable GetDetailLO_ByLo(string loID)
        {
            string str = $@"
            SELECT	CUSTACCOUNT ,BRANCH_NAME,SPC_SHIPMENTLINE.SHIPMENTID,SUM(BOXQTYSINGLE+BOXQTYMEDICINE+BOXQTYALCOHOL+BOXQTYCIGAR+BOXQTYCARD+BOXQTYOTHER) AS BoxQty  
            FROM	SHOP2013TMP.dbo.SPC_SHIPMENTTABLE WITH (NOLOCK)   
		            INNER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTLINE WITH (NOLOCK)  ON SPC_SHIPMENTTABLE.SHIPMENTID = SPC_SHIPMENTLINE.SHIPMENTID  
		            INNER JOIN SHOP_BRANCH WITH (NOLOCK)  ON SPC_SHIPMENTLINE.CUSTACCOUNT = SHOP_BRANCH.BRANCH_ID  
            WHERE	LOGISTICID IN ( '{loID}'  ) 
		            AND SPC_SHIPMENTTABLE.DATAAREAID = N'SPC' 
		            AND SPC_SHIPMENTLINE.DATAAREAID = N'SPC'  
		            AND CUSTACCOUNT NOT IN
		                (
		                SELECT  BranchLock AS CUSTACCOUNT 
                        FROM    Shop_TagCarTruck WITH(NOLOCK)  
		                WHERE   LOGISTICID = '{loID}'  AND BranchLock <> 'MN000' 
		                GROUP BY BranchLock
		                )
            GROUP BY  CUSTACCOUNT, BRANCH_NAME, SPC_SHIPMENTLINE.SHIPMENTID
            ORDER BY  CUSTACCOUNT, BRANCH_NAME, SPC_SHIPMENTLINE.SHIPMENTID
            ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //แผนกทั้งหมดที่ทำการจัดส่งสินค้าได้ ปล่อย LO ได้
        public static DataTable GetLogisticDept()
        {
            string str = $@"
                SELECT	DEPTID,DEPTID+'-'+DEPTNAME AS DEPTNAME  
                FROM	SHOP2013TMP.dbo.SPC_LOGISTICDEPARTMENT WITH (NOLOCK) 
                WHERE	DATAAREAID = N'SPC' ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาเส้นทางการส่งสินค้า
        public static DataTable GetRoute(string RouteID)
        {
            string str = $@"  
            SELECT  ROUTEID, ROUTEID+'-'+NAME AS ROUTENAME 
            FROM    SHOP2013TMP.dbo.SPC_ROUTETABLE WITH(NOLOCK)
            WHERE   ROUTEID LIKE '{RouteID}%'   AND DATAAREAID = 'SPC'  
            ORDER BY ROUTEID ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาสาขาตามสายรถ ตามแผนก
        public static DataTable GetRouteAll_GroupBranch(string conditionBch = "")//GetRouteExcel_ByDept
        {
            string sql = $@"
            SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME,BRANCH_ID + '-' + BRANCH_NAME AS BRANCH
            FROM	(
		            SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
		            FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH(NOLOCK)
		            WHERE	ACCOUNTNUM LIKE 'MN%'
				            AND ISPRIMARY = '1'		AND DATAAREAID = N'SPC'
		            )TMP 
		            INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID AND DATAAREAID = N'SPC'
		            INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMP.ACCOUNTNUM = SHOP_BRANCH.BRANCH_ID 
            WHERE	SEQ = 1 {conditionBch}
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาสาขาตามสายรถ ตามแผนก
        public static string GetRouteAllByBranch()//GetRoute All By Branch
        {
            string sql = $@"
            SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID AS ROUTEID,SPC_ROUTETABLE.NAME AS ROUTENAME,BRANCH_ID + '-' + BRANCH_NAME AS BRANCH
            FROM	(
		            SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
		            FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH(NOLOCK)
		            WHERE	ACCOUNTNUM LIKE 'MN%'
				            AND ISPRIMARY = '1'		AND DATAAREAID = N'SPC'
		            )TMP 
		            INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID AND DATAAREAID = N'SPC'
		            INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMP.ACCOUNTNUM = SHOP_BRANCH.BRANCH_ID 
            WHERE	SEQ = 1  
            ";
            return sql;
        }
        //ค้นหาสายรถหลัก ทั้งหมด
        public static DataTable GetRouteAll_MainRoute()
        {
            string sql = $@"
                SELECT	DISTINCT ROUTEID,NAME,ROUTEID+' - '+NAME AS ROUTENAME
                FROM	(
		                SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME	
		                FROM	(
				                SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
				                FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH(NOLOCK)
				                WHERE	ACCOUNTNUM LIKE 'MN%'
						                AND ISPRIMARY = '1'		AND DATAAREAID = N'SPC'
				                )TMP INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID
		                WHERE	SEQ = 1
                )TMP2
                ORDER BY ROUTEID
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาข้อมูลรายละเอียดของ LO ตามเลขที่ LO แต่จะต้อง join SH ด้วย
        public static DataTable GetLogisticDetail_GroupByLO(string loID)
        {
            string sql = $@"
            SELECT CONVERT(VARCHAR,SPC_LOGISTICTABLE.TRANSFERDATE,23) AS TRANSFERDATE,SPC_LOGISTICTABLE.LOGISTICID,SPC_LOGISTICTABLE.ROUTEID,SPC_ROUTETABLE.NAME,  
                     SPC_LOGISTICTABLE.VEHICLEID,SPC_LOGISTICTABLE.EMPLDRIVER,SPC_NAME,  
                     TRASHREMARK,SPC_SHIPMENTLINE.CUSTACCOUNT as BRANCH_ID ,ISNULL(BRANCH_NAME,'') AS BRANCH_NAME    
            FROM	SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK)       
                     INNER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.LOGISTICID = SPC_SHIPMENTTABLE.LOGISTICID  
                     INNER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTLINE WITH (NOLOCK) ON SPC_SHIPMENTTABLE.SHIPMENTID = SPC_SHIPMENTLINE.SHIPMENTID  
                     INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK)   ON SPC_ROUTETABLE.ROUTEID = SPC_LOGISTICTABLE.ROUTEID   
                     LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON   SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'   
                     LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_SHIPMENTLINE.CUSTACCOUNT = SHOP_BRANCH.BRANCH_ID   
            WHERE	SPC_LOGISTICTABLE.LOGISTICID = '{loID}'  
            GROUP BY CONVERT(VARCHAR,SPC_LOGISTICTABLE.TRANSFERDATE,23) ,SPC_LOGISTICTABLE.LOGISTICID,SPC_LOGISTICTABLE.ROUTEID,SPC_ROUTETABLE.NAME, 
                     SPC_LOGISTICTABLE.VEHICLEID,SPC_LOGISTICTABLE.EMPLDRIVER,SPC_NAME,  
                     TRASHREMARK,SPC_SHIPMENTLINE.CUSTACCOUNT,ISNULL(BRANCH_NAME,'') ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาข้อมูลการจองล้างรถ
        public static DataTable CARCARE_FindData()
        {
            string sqlCar = $@"
                SELECT	VEHICLEID,NAME,DIMENSION AS DPTID,DIMENSIONS.DESCRIPTION AS DPTNAME,
		                ISNULL(CONVERT(NVARCHAR(50),DATE_HISTORY),'') AS DATE_HISTORY,ISNULL(CONVERT(NVARCHAR(50),DATE_BOOK),'') AS DATE_BOOK
                FROM	SHOP2013TMP.[dbo].[SPC_VEHICLETABLE] WITH(NOLOCK) 
		                LEFT OUTER JOIN SHOP2013TMP.[dbo].DIMENSIONS WITH (NOLOCK) ON [SPC_VEHICLETABLE].DIMENSION = DIMENSIONS.NUM 
		                LEFT OUTER JOIN 
		                (
			                SELECT	MAX([CARCARE_DATE]) AS DATE_HISTORY,[CARCARE_CARID]
			                FROM	[SHOP_CARCARE] WITH (NOLOCK)
			                WHERE	CONVERT(VARCHAR,[CARCARE_DATE],23) < CONVERT(VARCHAR,GETDATE(),23)
			                GROUP BY [CARCARE_CARID]
		                )CAR_HISTORY ON [SPC_VEHICLETABLE].VEHICLEID = CAR_HISTORY.CARCARE_CARID
		                LEFT OUTER JOIN 
		                (
			                SELECT	MAX([CARCARE_DATE]) AS DATE_BOOK,[CARCARE_CARID]
			                FROM	[SHOP_CARCARE] WITH (NOLOCK)
			                WHERE	CONVERT(VARCHAR,[CARCARE_DATE],23) >= CONVERT(VARCHAR,GETDATE(),23)
			                GROUP BY [CARCARE_CARID]
		                )CAR_BOOK ON [SPC_VEHICLETABLE].VEHICLEID = CAR_BOOK.CARCARE_CARID
		 
                WHERE	[SPC_VEHICLETABLE].DATAAREAID = N'SPC' 
		                AND DIMENSIONS.DATAAREAID = N'SPC' 
		                AND DIMENSIONS.DIMENSIONCODE = '0'
		                AND VEHICLEGROUP NOT IN ('MOTORCYCLE')
            ";
            return ConnectionClass.SelectSQL_Main(sqlCar);
        }
        //รายงานการล้างรถ
        public static DataTable CARCARE_WashReport(string date1, string date2)
        {
            string sql = $@"
            SELECT	Shop_EmpVehicleWash.VEHICLEID as VehicleID,YEARMONTHDAY,USERCREATE as UserCreate  ,
		            CONVERT(VARCHAR,Shop_EmpVehicleWash.CREATEDATE,23) as Createdate ,
		            [SPC_VEHICLETABLE].NAME as VehicleName,[SPC_VEHICLETABLE].DIMENSION as DimensionCar,
		            [DIMENSIONS].[DESCRIPTION] AS Description ,'' AS EMPLID_1,'' AS EMPLID_2,'' AS EMPLID_3

            FROM	Shop_EmpVehicleWash  WITH (NOLOCK)
		            LEFT OUTER JOIN Shop_BillLogTakeImage WITH (NOLOCK) on Shop_BillLogTakeImage.BILLID = Shop_EmpVehicleWash.VEHICLEID  
		            INNER JOIN [SHOP2013TMP].[dbo].[SPC_VEHICLETABLE] WITH (NOLOCK) on [SPC_VEHICLETABLE].VEHICLEID = Shop_EmpVehicleWash.VEHICLEID  
		            INNER JOIN [SHOP2013TMP].[dbo].[DIMENSIONS] WITH (NOLOCK) on [SPC_VEHICLETABLE].DIMENSION = [SHOP2013TMP].[dbo].[DIMENSIONS].NUM  
			            AND [DIMENSIONS].DATAAREAID = N'SPC'

            WHERE  TAKETYPE Like  'W%' and YEARMONTHDAY between '{date1}'  AND '{date2}'

            GROUP BY Shop_EmpVehicleWash.VEHICLEID,YEARMONTHDAY,USERCREATE,[SPC_VEHICLETABLE].NAME ,
                    CONVERT(VARCHAR,Shop_EmpVehicleWash.CREATEDATE,23),[SPC_VEHICLETABLE].DIMENSION,[DIMENSIONS].[DESCRIPTION]

            ORDER BY Shop_EmpVehicleWash.USERCREATE ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ฝาถังน้ำมัน
        public static DataTable Report_CapTankOil(string date1, string date2, string logisticID)
        {
            string conditionLogisticID = "";
            if (logisticID != "") conditionLogisticID = $@"  AND DEPTLOGISTIC = '{logisticID}' ";

            string sqlSelect7 = $@"
            SELECT	SPC_LOGISTICTABLE.LOGISTICID,VEHICLEID,DEPTLOGISTIC+CHAR(10)+DEPTNAME AS DEPTNAME,
                    DATEADD(HOUR,7,SHIPPINGDATETIME) AS SHIPPINGDATETIME,DATEADD(HOUR,7,ARRIVALDATETIME) AS ARRIVALDATETIME,
                    EMPLDRIVER+CHAR(10)+SPC_NAME AS SPC_NAME 
            FROM	SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK) 
		            INNER JOIN SHOP2013TMP.dbo.SPC_LOGISTICDEPARTMENT WITH (NOLOCK) ON SPC_LOGISTICTABLE.DEPTLOGISTIC = SPC_LOGISTICDEPARTMENT.DEPTID AND SPC_LOGISTICDEPARTMENT.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
            WHERE	SPC_LOGISTICTABLE.DATAAREAID = N'SPC' 
                    AND CONVERT(VARCHAR,(DATEADD(HOUR,7,SHIPPINGDATETIME)),23)  BETWEEN '{date1}'  AND '{date2}'
                    {conditionLogisticID}
            ORDER BY DATEADD(HOUR,7,SHIPPINGDATETIME)
                    ";
            return ConnectionClass.SelectSQL_Main(sqlSelect7);
        }
        //LO ส่งสินค้า ตามวันที่ รถออก
        public static DataTable Report_FindLODetail(string date1, string date2)
        {
            string sql = $@"
            SELECT	LOGISTICID,VEHICLEID,SPC_LOGISTICTABLE.ROUTEID,NAME,DEPTLOGISTIC,SPC_LOGISTICDEPARTMENT.DEPTNAME,			
		            DATEADD(HOUR,7,SHIPPINGDATETIME) AS SHIPPINGDATETIME,DATEADD(HOUR,7,ARRIVALDATETIME) AS ARRIVALDATETIME,
                    EMPLDRIVER,SPC_NAME,TRASHREMARK 
            FROM	SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK) 			
                    INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK)	ON SPC_LOGISTICTABLE.ROUTEID = SPC_ROUTETABLE.ROUTEID
			        INNER JOIN SHOP2013TMP.dbo.SPC_LOGISTICDEPARTMENT WITH (NOLOCK) ON SPC_LOGISTICTABLE.DEPTLOGISTIC = SPC_LOGISTICDEPARTMENT.DEPTID	
                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
            WHERE	CONVERT(VARCHAR,DATEADD(HOUR,7,SHIPPINGDATETIME),23) BETWEEN '{date1}'  AND '{date2}'  	
            ORDER BY 		 CONVERT(VARCHAR,DATEADD(HOUR,7,SHIPPINGDATETIME),25)	 ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        // 3 - รายงานการไม่เก็บ-ไม่มี อุปกรณ์ส่งคืน
        public static DataTable Report_MNRDNotReturn(string date1, string date2)
        {
            string sql = $@"
            SELECT	MNRDBRANCH,BRANCH_NAME,CONVERT(VARCHAR,MNRDDATE,23) AS MNRDDATE,SPC_ROUTETABLE.ROUTEID,NAME as RIUTEIDNAME,VEHICLEID,EMPLDRIVER,  
		                        EMPLTABLE.SPC_NAME AS EMPLNAME,TRASHLOGISTIC,  
		                        CASE TRASHLOGISTIC WHEN '001' THEN 'ไม่มี' ELSE 'ไม่เก็บ' END  AS   TRASHDESC,TRASHREMARK,MNRDLOID  
            FROM	SHOP_MNRD_RC With (NOLOCK)   
		                         INNER JOIN SHOP_Branch WITH (NOLOCK) ON SHOP_MNRD_RC.MNRDBRANCH = SHOP_Branch.BRANCH_ID  
		                         LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_LogisticTable WITH (NOLOCK)  ON SHOP_MNRD_RC.MNRDLOID = SPC_LogisticTable.LOGISTICID  
		                         LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK)    ON SPC_LogisticTable.ROUTEID   = SPC_ROUTETABLE.ROUTEID  
		                         LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) 	 ON SPC_LogisticTable.EMPLDRIVER = EMPLTABLE.EMPLID   
            WHERE	MNRDDATE Between '{date1}'  AND '{date2}'  
                    AND MNRDSTADOC = '1'
            GROUP BY MNRDBRANCH, BRANCH_NAME, MNRDDATE, SPC_ROUTETABLE.ROUTEID, NAME, VEHICLEID, EMPLDRIVER,
                    EMPLTABLE.SPC_NAME, TRASHLOGISTIC, TRASHREMARK, MNRDLOID, CASE TRASHLOGISTIC WHEN '001' THEN 'ไม่มี' ELSE 'ไม่เก็บ' END ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        // 2 - รายงานการส่งคืนอุปกรณ์ตาม LO
        public static DataTable Report_MNRDReturn(string date1, string date2)
        {
            string sql = $@"
            SELECT	CONVERT(VARCHAR,TMPSEND.MNRDDATE,23) AS MNRDDATE,TMPSEND.ROUTEID,TMPSEND.RIUTEIDNAME,TMPSEND.VEHICLEID,TMPSEND.EMPLDRIVER,  
                    TMPSEND.EMPLNAME,TMPSEND.TRASHLOGISTIC,
                    CASE TRASHLOGISTIC WHEN '001' THEN '001-ไม่มี' WHEN '002' THEN '002-น้อย' WHEN '003' THEN '003-ครึ่งคัน' WHEN '004' THEN '004-เต็มคัน' WHEN '005' THEN '005-ไม่เก็บ' END AS TRASHDESC,   
                    TMPSEND.TRASHREMARK,  
                    TMPSEND.MNRDSHID,TMPSEND.MNRDBarcode,TMPSEND.MNRDName,  
                    TMPSEND.ReturnQty,ISNULL(ReturnRecive,0) AS ReturnRecive,(ISNULL(ReturnRecive,0))-(TMPSEND.ReturnQty) AS SUMALL,
		            CASE  WHEN (ISNULL(ReturnRecive,0))-(TMPSEND.ReturnQty)  > 0 THEN '1' WHEN (ISNULL(ReturnRecive,0))-(TMPSEND.ReturnQty)  < 0 THEN '0' ELSE '2' END AS DiffSing
            FROM	(  
				            SELECT	MNRDDATE,SPC_ROUTETABLE.ROUTEID,NAME as RIUTEIDNAME,VEHICLEID,EMPLDRIVER,EMPLTABLE.SPC_NAME AS EMPLNAME,TRASHLOGISTIC,TRASHREMARK,  
						            MNRDSHID,MNRDBarcode,MNRDName,SUM(MNRDReturnQty) AS ReturnQty  
				            FROM	SHOP_MNRD_HD With (NOLOCK) INNER JOIN SHOP_MNRD_DT WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDDocNo = SHOP_MNRD_DT.MNRDDocNo  
						            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_LogisticTable WITH (NOLOCK)  ON SHOP_MNRD_HD.MNRDSHID = SPC_LogisticTable.LOGISTICID  
						            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK)    ON SPC_LogisticTable.ROUTEID   = SPC_ROUTETABLE.ROUTEID  
						            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) 	 ON SPC_LogisticTable.EMPLDRIVER = EMPLTABLE.EMPLID   
				            WHERE	MNRDDATE BETWEEN '{date1}'  AND '{date2}'  
						            AND MNRDSTADOC = '1' AND MNRDSTSTUS = '1' AND MNRDSTAPRCDOC = '1'  
						            AND MNRDBRANCH != 'MN000' AND SPC_ROUTETABLE.DATAAREAID = 'SPC'  
				            GROUP BY MNRDDATE,SPC_ROUTETABLE.ROUTEID,NAME,VEHICLEID,EMPLDRIVER,EMPLTABLE.SPC_NAME,TRASHLOGISTIC,TRASHREMARK, MNRDSHID, MNRDBarcode, MNRDName  
                        )TMPSEND LEFT OUTER JOIN  
	                    (  
				            SELECT	MNRDSHID,MNRDBarcode,MNRDName,SUM(MNRDReturnQty) AS ReturnRecive  
				            FROM	SHOP_MNRD_HD With (NOLOCK) INNER JOIN SHOP_MNRD_DT WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDDocNo = SHOP_MNRD_DT.MNRDDocNo  
				            WHERE	MNRDSTADOC = '1' AND MNRDSTSTUS = '1' AND MNRDSTAPRCDOC = '1'  
						            AND MNRDBRANCH = 'MN000'  AND MNRDSHID LIKE 'LO%'
				            GROUP BY MNRDSHID,MNRDBarcode,MNRDName  
				            HAVING SUM(MNRDReturnQty) > 0  
                        )TMPRECIVE  ON TMPSEND.MNRDSHID = TMPRECIVE.MNRDSHID AND  TMPSEND.MNRDBarcode = TMPRECIVE.MNRDBarcode  
            ORDER BY TMPSEND.MNRDSHID  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        // 1  - รายงานการตรวจรถ 4 ด้าน ค้นหา Lo ตามวันที่ส่งสินค้าและแผนก
        public static DataTable Report_CarCheck(string date1, string date2, string deptID)
        {
            string conditionDept = "";
            if (deptID != "") conditionDept = $@"  AND DEPTLOGISTIC = '{deptID}' ";
            string sql = $@"
                    SELECT	SPC_LOGISTICTABLE.LOGISTICID,VEHICLEID,DEPTLOGISTIC+CHAR(10)+DEPTNAME AS DEPTNAME,
                            DATEADD(HOUR,7,SHIPPINGDATETIME) AS SHIPPINGDATETIME,DATEADD(HOUR,7,ARRIVALDATETIME) AS ARRIVALDATETIME,
                            EMPLDRIVER+CHAR(10)+SPC_NAME AS SPC_NAME 
                    FROM	SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK) 
		                    INNER JOIN SHOP2013TMP.dbo.SPC_LOGISTICDEPARTMENT WITH (NOLOCK) ON SPC_LOGISTICTABLE.DEPTLOGISTIC = SPC_LOGISTICDEPARTMENT.DEPTID AND SPC_LOGISTICDEPARTMENT.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
                    WHERE	SPC_LOGISTICTABLE.DATAAREAID = N'SPC'
		                    AND CONVERT(VARCHAR,(DATEADD(HOUR,7,SHIPPINGDATETIME)),23)  BETWEEN '{date1}' AND '{date2}'
                            {conditionDept}
                    ORDER BY DATEADD(HOUR,7,SHIPPINGDATETIME) ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //  0 รายงานน้ำหนักรถสาขาต่างจังหวัด
        public static DataTable Report_CarWeight(string date1, string date2)
        {
            string sql = $@"
            SELECT	TMP.LOGISTICID,TMP.CUSTACCOUNT,TMP.CUSTNAME,
		            VEHICLEID,DEPTLOGISTIC+'-'+DEPTNAME AS DEPTNAME,DATEADD(HOUR,7,SHIPPINGDATETIME) AS SHIPPINGDATETIME,EMPLDRIVER+'-'+SPC_NAME AS SPC_NAME,ISNULL(VEHICLEWEGHT,0) AS VEHICLEWEGHT
            FROM	(
		            SELECT	LOGISTICID,CUSTACCOUNT,BRANCH_NAME AS CUSTNAME,CONVERT(VARCHAR,[SPC_SHIPMENTTABLE].SHIPMENTDATE,23) AS SHIPMENTDATE

		            FROM	[SHOP2013TMP] .[dbo].[SPC_SHIPMENTTABLE] WITH (NOLOCK)  
				            INNER JOIN [SHOP2013TMP] .[dbo].[SPC_SHIPMENTLINE] WITH (NOLOCK) ON [SPC_SHIPMENTTABLE].SHIPMENTID = [SPC_SHIPMENTLINE].SHIPMENTID  
				            INNER JOIN [Shop_Branch] WITH (NOLOCK) ON [SPC_SHIPMENTLINE].CUSTACCOUNT = [Shop_Branch].BRANCH_ID  
		
		            WHERE	CUSTACCOUNT in ('MN059','MN055') AND [SPC_SHIPMENTTABLE].LOGISTICID != '' 
				            AND [SPC_SHIPMENTLINE].DATAAREAID = N'SPC' AND  [SPC_SHIPMENTTABLE].DATAAREAID = N'SPC'
				            AND [SPC_SHIPMENTTABLE].SHIPMENTDATE between '{date1}'  AND '{date2}'

		            GROUP BY LOGISTICID,CUSTACCOUNT,BRANCH_NAME,[SPC_SHIPMENTTABLE].SHIPMENTDATE--,EMPLDRIVER 
		            )TMP 
		            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK) ON TMP.LOGISTICID = SPC_LOGISTICTABLE.LOGISTICID AND SPC_LOGISTICTABLE.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_LOGISTICDEPARTMENT WITH (NOLOCK) ON SPC_LOGISTICTABLE.DEPTLOGISTIC = SPC_LOGISTICDEPARTMENT.DEPTID 
				            AND SPC_LOGISTICDEPARTMENT.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN [Shop_BillLogTakeImage] WITH (NOLOCK)  ON TMP.LOGISTICID = [Shop_BillLogTakeImage].BILLID  AND VEHICLEWEGHT > 0.000  

            ORDER BY TMP.LOGISTICID,DATEADD(HOUR,7,SHIPPINGDATETIME) ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //public static DataTable AGetDataLogisticDetail(string logisticID)
        //{
        //    string sql = $@"
        //        SELECT	CONVERT(VARCHAR,TRANSFERDATE,23) AS TRANSFERDATE,SPC_LOGISTICTABLE.LOGISTICID,SPC_LOGISTICTABLE.ROUTEID,NAME,   
        //                VEHICLEID,EMPLDRIVER,SPC_NAME,TRASHREMARK  
        //        FROM	SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK)        
        //                INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK)   ON SPC_ROUTETABLE.ROUTEID = SPC_LOGISTICTABLE.ROUTEID    
        //                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON   SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'     
        //        WHERE	SPC_LOGISTICTABLE.LOGISTICID = '{logisticID}'    ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
    }
}
