﻿using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Scale;
using System.Data;

namespace PC_Shop24Hrs.Class.Scale
{
    class ScaleClass2012 : IScaleInterface
    {
        DataTable IScaleInterface.GetPurch()
        {
            string sql = $@"
                    SELECT	NUM,NUM+' - '+DIMENSIONS.DESCRIPTION AS DESCRIPTION_SHOW	 
                    FROM    SHOP_SCALEMASTER WITH(NOLOCK) 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
		                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH(NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM
                    WHERE	INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                    --AND DIMENSIONS.DATAAREAID = N'SPC'
		                    AND DIMENSIONS.DIMENSIONCODE = '0'
                    GROUP BY NUM, DIMENSIONS.DESCRIPTION 
                    ORDER BY NUM, DIMENSIONS.DESCRIPTION ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IScaleInterface.GetDataScaleShopMain(string price)
        {
            string sql = $@"
                    SELECT	DIMENSIONS.DESCRIPTION,EXPDAY,
		                    EXPDAYPRINT,TYPE_SCALE,
		                    INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,   
                            INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,
		                    SPC_PriceGroup3,{price} AS PRICE,  
                            CONVERT(VARCHAR,ISNULL(TRANSDATEEDIT,''),23) AS DATEEDIT, 
		                    CONVERT(VARCHAR,ISNULL(TRANSDATEEDIT,''),24) AS TIMEEDIT,  
                            ISNULL(TRANSDATEEDIT,'') AS TRANSDATE,SPC_ITEMBUYERGROUPID  
                    FROM	SHOP_SCALESEND WITH (NOLOCK) 
		                    INNER JOIN SHOP_SCALEMASTER WITH (NOLOCK)  ON SHOP_SCALESEND.ITEMBARCODE = SHOP_SCALEMASTER.ITEMBARCODE 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)  ON SHOP_SCALEMASTER.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
			                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN (SELECT	ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES,MAX(dateAdd(mi,420,MODIFIEDDATETIME)) AS TRANSDATEEDIT  
						                    FROM	SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH (NOLOCK) 
						                    WHERE	DATAAREAID = 'SPC' 
								                    AND PRICEFIELDID IN (SELECT	FieldID 
													                    FROM	dbo.Shop_Price WITH (NOLOCK) 
													                    WHERE   ColumnName = '{price}' ) 
								                    AND DATAAREAID = N'SPC'
						                    GROUP BY ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES)SPC_INVENTITEMPRICEHISTORY  ON INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID 
							                    AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID 
							                    AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID 
		                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
			                    AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC'

                    WHERE 	SPC_SALESPRICETYPE IN ('3','4','5')   
                    ORDER BY INVENTITEMBARCODE.ITEMBARCODE ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IScaleInterface.GetDataScale(string price, string dept)
        {
            string sql = $@"
                   SELECT	EXPDAY, EXPDAYPRINT, TYPE_SCALE,
		                    CASE WHEN TypeSend = 'SHOP' THEN '1' ELSE 0  END AS TYPESEND,
		                    INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,
		                    INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,
		                    {price} AS PRICE,SPC_PriceGroup13,SPC_PriceGroup14,
		                    SPC_PriceGroup15,SPC_PriceGroup17,SPC_PriceGroup18,SPC_PriceGroup19,
		                    CONVERT(VARCHAR, ISNULL(TRANSDATEEDIT,''),23) AS DATEEDIT,
		                    CONVERT(VARCHAR, ISNULL(TRANSDATEEDIT, ''), 24) AS TIMEEDIT,
		                    ISNULL(TRANSDATEEDIT, '') AS TRANSDATE,SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION
                    FROM	SHOP_SCALEMASTER WITH (NOLOCK) 
		                    LEFT OUTER JOIN SHOP_SCALESEND WITH (NOLOCK) ON SHOP_SCALEMASTER.ITEMBARCODE = SHOP_SCALESEND.ITEMBARCODE 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_SCALEMASTER.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
		                    LEFT OUTER JOIN (SELECT  ITEMID, INVENTDIMID, UNITID, NEWPRICESALES, ORIGPRICESALES, MAX(dateAdd(mi,420, MODIFIEDDATETIME)) as TRANSDATEEDIT
						                    FROM    SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH(NOLOCK) 
						                    WHERE   PRICEFIELDID IN ( SELECT FieldID FROM dbo.Shop_Price WITH (NOLOCK) WHERE ColumnName = '{price}')
								                    AND DATAAREAID = 'SPC'
						                    GROUP BY ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES
						                    ) SPC_INVENTITEMPRICEHISTORY ON INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID
							                    AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID
							                    AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID 
		                    INNER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 	ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
                    WHERE   SPC_SALESPRICETYPE IN ('3','4','5')   
		                    AND INVENTITEMBARCODE.DATAAREAID = 'SPC'  
		                    AND SPC_ITEMBUYERGROUPID = '{dept}' ";

            return ConnectionClass.SelectSQL_Main(sql); ;
        }

        //AX2012 OK
        ScaleMain.ScaleItem IScaleInterface.Itembarcode(string itembacode)
        {
            ScaleMain.ScaleItem item = new ScaleMain.ScaleItem();
            string sql = $@"
                    SELECT	SPC_SALESPRICETYPE,INVENTITEMBARCODE.ITEMBARCODE, SPC_ITEMNAME
		                    ,isnull(SHOP_SCALEMASTER.ITEMBARCODE, '0') AS ScaleMaster
		                    ,isnull(SHOP_SCALESEND.ITEMBARCODE, '0') AS ScaleSendShop
                    FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) 
		                    LEFT OUTER JOIN SHOP_SCALEMASTER WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE = SHOP_SCALEMASTER.ITEMBARCODE 
		                    LEFT OUTER JOIN SHOP_SCALESEND WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE = SHOP_SCALESEND.ITEMBARCODE 
                    WHERE	SPC_SALESPRICETYPE IN ('3', '4', '5') 
		                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                    AND INVENTITEMBARCODE.ITEMBARCODE = '{itembacode}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                item = new ScaleMain.ScaleItem()
                {
                    ITEMBARCODE = dt.Rows[0]["ITEMBARCODE"].ToString(),
                    SPC_ITEMNAME = dt.Rows[0]["SPC_ITEMNAME"].ToString(),
                    SCALEMASTER = dt.Rows[0]["ScaleMaster"].ToString(),
                    SCALESENDSHOP = dt.Rows[0]["ScaleSendShop"].ToString()
                };
            }
            return item;
        }

        DataTable IScaleInterface.GetDataDelete(string dept)
        {
            string Dpt = "";
            if (!dept.Equals("")) { Dpt = $@"AND NUM = '{dept}' "; }
            string sql = $@" 
                    SELECT  EXPDAY,EXPDAYPRINT,TYPE_SCALE,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,  
                            INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID  
                    FROM	Shop_scalemaster WITH (NOLOCK) LEFT OUTER JOIN 
                            Shop_ScaleSend WITH (NOLOCK)   ON Shop_ScaleSend.ITEMBARCODE = Shop_scalemaster.ITEMBARCODE INNER JOIN 
                            SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE   LEFT OUTER JOIN 
                            SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 	ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
                    WHERE 	SPC_SALESPRICETYPE IN ('3','4','5')  
                            AND ISNULL(Shop_ScaleSend.ITEMBARCODE,'') = ''  {Dpt}
                    ORDER BY INVENTITEMBARCODE.ITEMBARCODE ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
