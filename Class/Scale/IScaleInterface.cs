﻿using PC_Shop24Hrs.GeneralForm.Scale;
using System.Data;

namespace PC_Shop24Hrs.Class.Scale
{
    interface IScaleInterface
    {
        //ดึงข้อมูลจัดซื้อ ที่มีสินค้าชั่งน้ำหนัก
        DataTable GetPurch();

        //ดึงข้อมูลสินค้า ราคาจะอ้างอิงจากระกับราคาของสาขา
        DataTable GetDataScaleShopMain(string price);

        //ดึงข้อมูล สำหรับกำหนดสินค้าชั่ง
        DataTable GetDataScale(string price, string dept);

        //ดึงข้อมูลสินค้า ด้วยบาร์โค้ด
        ScaleMain.ScaleItem Itembarcode(string itembacode);

        //ไม่ได้ใช้
        DataTable GetDataDelete(string dept = "");
    }
}
