﻿using System.Data;

namespace PC_Shop24Hrs.Class.Asset
{
    interface IAssetClassInterface
    {
        //ค้นหาสินทรัพย์ทั้งหมดยกเว้นเครื่องมือ
        DataTable FindAsset_ByAllType(string pAssetID, string pCheckTool, string pTopRow);

        //ค้นหาเงื่อนไขในการปรับทะเบียนสินทรัพย์
        DataTable FindConditionAsset();

        //ค้นหาสต็อกทำมือ ยานยนต์
        DataTable FindStockItem_ByStockID(string _pStockID,string Condition = "");
        //ค้นหาข้อมูลของกล้องแมคโครหรือกล้องก่อสร้าง
        DataTable FindDetailAsset_ByTypeConfig(string _pTypeConfig);

        //ค้นหาข้อมูลขิงสินทรัพย์ที่ติดยืมอยู่หรือไม่
        DataTable FindDataAssetLending(string pAssetID);
        //MyJeeds ค้นหารถ
        DataTable FindDataVEHICLE(string pVehicleID, string pCon);
        //MyJeeds ค้นหารถ
        DataTable FindDataVEHICLELogTagOil(string pVehicleID);
        //ค้นหาอู่ซ่อมรถ
        DataTable FindDataVehicleRepairShop();
        //ค้นหาทะเบียนสินทรัพย์ จากเลขที่ AssetID
        DataTable FindAsset_ByAssetID(string assetID);
        //ค้นหาข่อมูลการ login เครือ่ง PDA
        DataTable FindAssetLoginPDA(string date1, string date2);
        //Find Recod AX JOBCar
        DataTable FindRecodJOBCar(string pCarID);
    }
}
