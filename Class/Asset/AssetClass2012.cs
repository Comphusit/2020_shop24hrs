﻿using System.Data;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.Class.Asset
{
    class AssetClass2012 : IAssetClassInterface
    {
        //ค้นหาสินทรัพย์ทั้งหมดยกเว้นเครื่องมือ
        //AX2012_NODATA
        DataTable IAssetClassInterface.FindAsset_ByAllType(string pAssetID, string pCheckTool, string pTopRow)
        {
            string str = $@"
                SELECT  {pTopRow} ASSETID,NAME,NAMEALIAS,SERIALNUM,LOCATION,LOCATIONMEMO,UNITOFMEASURE,UNITCOST,CONDITION,ASSETGROUP,
		                SUBSTRING(MAKE,9,LEN(MAKE)) AS VenderNAME ,SUBSTRING(MAKE,0,8) AS VenderID,CONVERT(VARCHAR,PolicyExpiration,23) as PolicyExpiration,
		                CONVERT(VARCHAR,GuaranteeDate,23) as GuaranteeDate,Reference, CONVERT(VARCHAR,LastMaintenance,23) AS LastMaintenance,MaintenanceInfo3,Lease,Notes,
                        TECHINFO1,TECHINFO2,TECHINFO3,MAKE 
                FROM	AX60CU13.dbo.ASSETTABLE WITH (NOLOCK) 
                WHERE   DATAAREAID = N'SPC'  AND ASSETID = '{pAssetID}' OR  NameAlias = '{pAssetID}' OR SerialNum = '{pAssetID}'  {pCheckTool}";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาเงื่อนไขในการปรับทะเบียนสินทรัพย์
        //AX2012_OK
        DataTable IAssetClassInterface.FindConditionAsset()
        {
            string str = $@"
                SELECT  CONDITION CODE_ID,DESCRIPTION AS CODE_NAME
                FROM	AX60CU13.dbo.AssetCondition WITH (NOLOCK) WHERE DATAAREAID =N'SPC'  ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาสต็อกทำมือ ยานยนต์
        //SHOP24HRS_ONLY
        DataTable IAssetClassInterface.FindStockItem_ByStockID(string _pStockID, string Condition )
        {
            string str = $@"
             SELECT	    StockID AS ASSETID,StockName AS NAME,'' AS NAMEALIAS,'' AS SERIALNUM,
                        '' AS LOCATION,'' AS LOCATIONMEMO,StockUnit AS UNITOFMEASURE,StockUnit AS UNITCOST,'' AS CONDITION,StockType AS ASSETGROUP 
             FROM	    SHOP_STOCKITEM WITH (NOLOCK) 
             WHERE      StockID = '{_pStockID}' AND STA = '1' {Condition}";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาข้อมูลของกล้องแมคโครหรือกล้องก่อสร้าง
        //AX2012_NODATA
        DataTable IAssetClassInterface.FindDetailAsset_ByTypeConfig(string _pTypeConfig)
        {
            string sql = $@"
            SELECT	[STA],[SHOW_ID],[SHOW_NAME],[SHOW_DESC],[REMARK],[LOCK],
                    SPC_NAME,SERIALNUM,MaintenanceInfo3,Condition
            FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)
                    LEFT OUTER JOIN AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH (NOLOCK) ON [SHOP_CONFIGBRANCH_GenaralDetail].SHOW_NAME = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN AX60CU13.dbo.ASSETTABLE WITH (NOLOCk) ON [SHOP_CONFIGBRANCH_GenaralDetail].SHOW_DESC = ASSETTABLE.ASSETID AND ASSETTABLE.DATAAREAID = N'SPC'
            WHERE	[TYPE_CONFIG] = '{_pTypeConfig}' 
            ORDER BY [SHOW_ID],[SHOW_NAME]";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาข้อมูลขิงสินทรัพย์ที่ติดยืมอยู่หรือไม่
        //AX2012_WAITTABLE
        DataTable IAssetClassInterface.FindDataAssetLending(string pAssetID)
        {
            string strRec = $@"
            SELECT	RECID	
            FROM	SHOP2013TMP.dbo.AssetLending  WITH (NOLOCK)  
            WHERE	ASSETID = '{pAssetID}' 
                    AND ActualReturn = '1900-01-01' AND DATAAREAID = 'SPC'";
            return ConnectionClass.SelectSQL_Main(strRec);
        }
        //MyJeeds ค้นหารถ
        //AX2012_WAITTABLE รอ SERIALNUMBER
        DataTable IAssetClassInterface.FindDataVEHICLE(string pVehicleID, string pCon)
        {
            string vehiceID = "";
            if (pVehicleID != "") vehiceID = $@" AND VEHICLEID = '{pVehicleID}' ";

            string str = $@"
                SELECT  VEHICLEID,NAME,BRAND,SERIALNUMBER,VEHICLEID AS DATA_ID, NAME AS DATA_DESC     
                FROM    AX60CU13.dbo.TMSVEHICLETABLE_SPC WITH(NOLOCK) 
                WHERE   DATAAREAID = N'SPC'  {vehiceID} {pCon}  ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหารถที่ log tag น้ำมัน
        DataTable IAssetClassInterface.FindDataVEHICLELogTagOil(string pVehicleID)
        {
            string sqlCar = $@"
            //SELECT	VEHICLEID AS DATA_ID,NAME,
            //        CASE ISNULL(DIMENSIONS.DESCRIPTION,'') WHEN '' THEN NAME ELSE NAME+'/'+DIMENSIONS.DESCRIPTION END AS DATA_DESC,
            //        ISNULL(SHOP_TAGCARTRUCK.LOGISTICID,'A') AS STANOLOCK 
            //FROM	SHOP2013TMP.[dbo].[SPC_VEHICLETABLE] WITH(NOLOCK) 
		          //  LEFT OUTER JOIN SHOP2013TMP.[dbo].DIMENSIONS WITH (NOLOCK) ON [SPC_VEHICLETABLE].DIMENSION = DIMENSIONS.NUM
			         //   AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
            //        LEFT OUTER JOIN SHOP_TAGCARTRUCK WITH (NOLOCK) ON [SPC_VEHICLETABLE].VEHICLEID = SHOP_TAGCARTRUCK.LOGISTICID AND StaLock = '1' AND StaUnlock = '0' 
            //WHERE	[SPC_VEHICLETABLE].DATAAREAID = N'SPC'  AND VEHICLEID LIKE '%{pVehicleID}'                            
                            ";
            return ConnectionClass.SelectSQL_Main(sqlCar);
        }
        //MyJeeds ค้นหาอู่ซ่อมรถ
        //AX2012_WAITTABLE
        DataTable IAssetClassInterface.FindDataVehicleRepairShop()
        {
            string str = $@"
                SELECT	CODE,TXT	
                FROM	[SHOP2013TMP].[dbo].[SPC_VehicleRepairShop] WITH (NOLOCK) 
                WHERE	DATAAREAID = N'SPC'
                ORDER BY CODE ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาทะเบียนสินทรัพย์ จากเลขที่ AssetID
        DataTable IAssetClassInterface.FindAsset_ByAssetID(string assetID)
        {
            string str = $@"
                        SELECT	ASSETID,NAME,NAMEALIAS,LOCATION,SERIALNUM,MAKE,
		                        CONVERT(VARCHAR,GUARANTEEDATE,23) AS GUARANTEEDATE,TECHINFO1,TECHINFO2,TECHINFO3,
		                        MAINTENANCEINFO3,REFERENCE,NOTES,LOCATIONMEMO,LEASE,CONDITION   
                        FROM    AX60CU13.dbo.ASSETTABLE WITH (NOLOCK) 
                        WHERE   DATAAREAID = N'SPC' AND ASSETID LIKE '{assetID}%'
                        ORDER   BY ASSETID ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาข่อมูลการ login เครือ่ง PDA
        DataTable IAssetClassInterface.FindAssetLoginPDA(string date1, string date2)
        {
            string sql = $@"
                SELECT	LoginPDAName,LoginBranch,LoginBranchName,ISNULL(ASSETTABLE.NAME,'CN50/CN51') + '   ['+Condition+']' AS NAME
                FROM	SHOP_COMPDANAMESHOP24HRS WITH (NOLOCK)
                        LEFT OUTER JOIN AX60CU13.dbo.ASSETTABLE WITH (NOLOCK) ON SHOP_COMPDANAMESHOP24HRS.LoginPDAName = ASSETTABLE.ASSETID AND DATAAREAID = N'SPC'
                WHERE	LoginDate BETWEEN '{date1}' AND '{date2}'
		                AND LoginBranch != 'MN000'
                GROUP BY LoginPDAName,LoginBranch,LoginBranchName,ASSETTABLE.NAME,Condition
                ORDER BY LoginBranch,LoginPDAName
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IAssetClassInterface.FindRecodJOBCar(string pCarID)
        {
            string sql = $@"select	RECID
                            from	SPC_VEHICLETRANS WITH (NOLOCK) 
                            WHERE	VEHICLEID = '{pCarID}' 
                                    AND     TRANSTYPE = '1' 
                                    AND COMPLETEDATETIME = '1900-01-01'";

            return ConnectionClass.SelectSQL_MainAX(sql);
        }
    }
}

