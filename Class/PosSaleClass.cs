﻿using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class
{
    class PosSaleClass
    {
        //QR CODE
        public static DataTable GetQRCODE()
        {
            return ConnectionClass.SelectSQL_POSRetail707(@"
                SELECT	StoreId,GatewayId,Block,
		            CASE Block
			            WHEN '0' THEN 'เปิด'
		            ELSE 'ปิด' END AS Block_DESC,
		            CASE GatewayId 
			            WHEN 'AIRPAY' THEN '[F6]  จ่ายผ่านระบบ Airpay'
			            WHEN 'BAY' THEN '[F7]  QR	code จากธนาคารกรุงศรี(BAY)'
			            WHEN 'BBL' THEN '[F7]  QR code จากธนาคารกรุงเทพ(BBL)'
                        WHEN 'SCB' THEN '[F7]  QR code จากธนาคารไทยพาณิชย์(SCB)' 
                        WHEN 'KTB_DSCF' THEN '[F5]  QR code จากธนาคารกรุงไทย(KTB-DSCF)'
		            ELSE 'อื่นๆ' END AS GatewayId_DESC,
		            CASE StoreId 
			            WHEN '' THEN 'ทุกสาขารวมถึงสาขาใหญ่'
			            WHEN 'RT' THEN 'เฉพาะสาขาใหญ่'
		            ELSE 'อื่นๆ' END AS StoreId_DESC
                FROM	RetailQRPaymentSetup WITH (NOLOCK)
                ORDER BY StoreId,GatewayId
            ");
        }
        //open - close QRCODE
        public static string UpdateQRCode(string storeID, string gateWay, string sta)
        {
            string sqlUp = $@"
                UPDATE  RetailQRPaymentSetup
                Set     Block = '{sta}'
                WHERE   StoreId = '{storeID}'  AND GatewayId = '{gateWay}'";
            return ConnectionClass.ExecuteSQL_POSRetail707(sqlUp);
        }
        //Update ยอดเงินบิลเปลี่ยน
        public static string UpdateAmountBillU(string storeID, double grand)
        {
            string sqlUp = $@" 
                Update  RetailReturnItemSetup 
                SET     Amount = '{grand}' 
                WHERE   Store = '{storeID}'";
            return ConnectionClass.ExecuteSQL_POSRetail707(sqlUp);
        }
        //การค้นหายอดเงิน ของบิลเปลี่ยนทั้งหมด
        public static DataTable GetAmount_BillU()
        {
            string sql = $@"
                     SELECT	POSGROUP.POSGROUP,NAME,RetailReturnItemSetup.Amount  
                     FROM	POSGROUP WITH (NOLOCK) INNER JOIN RetailReturnItemSetup  WITH (NOLOCK)   
		                    ON  POSGROUP.POSGROUP = RetailReturnItemSetup.Store   
                     ORDER BY POSGROUP.POSGROUP ";
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }
        //ค้นหาอัตราการขาย (จำนวนหน่วยย่อย)
        public static DataTable GetSaleSumInventQty_ByItemInvent(string _pBarcode, string pBchID, int pDateDiff) // ถ้าดึงทุกสาขาให้ส่ง MN%
        {
            DataTable dtBarcode = ItembarcodeClass.FindDIM_ByBarcode(_pBarcode);
            if (dtBarcode.Rows.Count == 0) return dtBarcode;
            string sql = $@"
                        SELECT	BRANCH,SUM(INVENTQTY) AS QTY 
                        FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)
                        WHERE	TimePeriod Between GETDATE()-{pDateDiff}  and GETDATE()  
	                             AND INVENTCOLORID = '{dtBarcode.Rows[0]["INVENTCOLORID"]}'  
	                             AND INVENTSIZEID = '{dtBarcode.Rows[0]["INVENTSIZEID"]}'  
	                             AND CONFIGID = '{dtBarcode.Rows[0]["CONFIGID"]}'  
	                             AND ITEMID = '{dtBarcode.Rows[0]["ITEMID"]}'  
	                             AND BRANCH LIKE '{pBchID}'  
	                             AND INVENTSITEID = 'SPC'  
                        GROUP BY BRANCH  
                        ORDER BY BRANCH ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //ประวัติการคืน
        public static DataTable GetQtyPC_ByItembarcodeByBch(string pBarcode, string pBch, double pDateDiff = 365)
        {
            DataTable dtPC = new DataTable();
            DataTable dt = ItembarcodeClass.FindDIM_ByBarcode(pBarcode);
            if (dt.Rows.Count == 0) return dtPC;
            string sql = $@"
                SELECT	 CONVERT(VARCHAR,TimePeriod,23) AS SALEDATE,SUM(INVENTQTY) AS SALEQTY	 
                 FROM	 dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)   
                 WHERE	 TRANSTYPE = '7'
                         AND TimePeriod BETWEEN GETDATE() - {pDateDiff} AND GETDATE()
                         AND INVENTSITEID = 'SPC'
                         AND CONFIGID = '{dt.Rows[0]["CONFIGID"]}'
                         AND INVENTSIZEID = '{dt.Rows[0]["INVENTSIZEID"]}'
                         AND INVENTCOLORID = '{dt.Rows[0]["INVENTCOLORID"]}'
                         AND ITEMID = '{dt.Rows[0]["ITEMID"]}'
                         AND BRANCH = '{pBch}'
                 GROUP BY TimePeriod
                 ORDER BY TimePeriod DESC ";
            dtPC = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
            return dtPC;
        }
        //ประวัติการขาย
        public static DataTable GetQtySale_ByItembarcodeByBch(string pBarcode, string pBch, double pDateDiff = 365)
        {
            DataTable dtSale = new DataTable();
            DataTable dt = ItembarcodeClass.FindDIM_ByBarcode(pBarcode);
            if (dt.Rows.Count == 0) return dtSale;
            string sql = $@"
                SELECT  CONVERT(VARCHAR, TimePeriod,23) AS SALEDATE, SUM(INVENTQTY) AS SALEQTY	 
                 FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)   
                 WHERE	TimePeriod BETWEEN GETDATE() - {pDateDiff} AND GETDATE()
                         AND INVENTSITEID = 'SPC'
                         AND CONFIGID = '{dt.Rows[0]["CONFIGID"]}'
                         AND INVENTSIZEID = '{dt.Rows[0]["INVENTSIZEID"]}'
                         AND INVENTCOLORID = '{dt.Rows[0]["INVENTCOLORID"]}'
                         AND ITEMID = '{dt.Rows[0]["ITEMID"]}'
                         AND BRANCH = '{pBch}'  
                 GROUP BY CONVERT(VARCHAR, TimePeriod, 23)
                 ORDER BY CONVERT(VARCHAR,TimePeriod,23) DESC ";
            dtSale = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
            return dtSale;
        }
        //ประวัติการขาย
        public static DataTable GetQtySale_ByBchByConfigGroupMMYY(string configID, string inventSize, string inventColorId, string itemID, string bchID, string date1, string date2)
        {
            string sql = $@"
                SELECT  MONTH(CONVERT(VARCHAR, TimePeriod,23)) AS SALE_MM,
                        YEAR(CONVERT(VARCHAR, TimePeriod,23)) AS SALE_YY,
                        SUM(INVENTQTY) AS SALEQTY	 
                 FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)   
                 WHERE	TimePeriod BETWEEN '{date1}' 
                            AND '{date2}'
                         AND INVENTSITEID = 'SPC'
                         AND CONFIGID = '{configID}'
                         AND INVENTSIZEID = '{inventSize}'
                         AND INVENTCOLORID = '{inventColorId}'
                         AND ITEMID = '{itemID}'
                         AND BRANCH = '{bchID}'  
                 GROUP BY   MONTH(CONVERT(VARCHAR, TimePeriod,23)),
                            YEAR(CONVERT(VARCHAR, TimePeriod,23))
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //ประวัติการรับ
        public static DataTable GetQtyRecive_ByItembarcodeByDate(string pBarcode, string pBch, double pDateDiff = 360)
        {
            DataTable dtRecive = new DataTable();
            DataTable dt = ItembarcodeClass.FindDIM_ByBarcode(pBarcode);
            if (dt.Rows.Count == 0) { return dtRecive; }
            string sql = string.Format(@"
                SELECT	 CONVERT(VARCHAR,TimePeriod,23) AS SALEDATE,SUM(INVENTQTY) AS SALEQTY	 
                 FROM	 dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)   
                 WHERE	 TRANSTYPE = '6'
                         AND TimePeriod BETWEEN GETDATE()-" + pDateDiff + @"  and GETDATE()  
                         AND INVENTSITEID = 'SPC'
                         AND CONFIGID = '" + dt.Rows[0]["CONFIGID"].ToString() + @"'
                         AND INVENTSIZEID = '" + dt.Rows[0]["INVENTSIZEID"].ToString() + @"'
                         AND INVENTCOLORID = '" + dt.Rows[0]["INVENTCOLORID"].ToString() + @"'
                         AND ITEMID = '" + dt.Rows[0]["ITEMID"].ToString() + @"'
                         AND BRANCH LIKE '" + pBch + @"'  
                 GROUP BY TimePeriod
                 ORDER BY TimePeriod DESC");
            dtRecive = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
            return dtRecive;
        }
        //สต็อกสินค้า
        public static DataTable GetQtyStock_ByItembarcodeByBch(string pBarcode, string pBch)
        {
            double QtyStk = ItembarcodeClass.FindStock_ByBarcode(pBarcode, pBch);

            DataTable dtStock = new DataTable();
            dtStock.Columns.Add("SALEDATE");
            dtStock.Columns.Add("SALEQTY");
            dtStock.Rows.Add("ปริมาณคงเหลือ", QtyStk.ToString("#,#0.00"));
            return dtStock;
        }
        //ประวัติการนับสต็อกสินค้า
        public static DataTable GetQtyCountStock_ByItembarcodeBYBch(string pBarcode, string pBch)
        {
            DataTable dtCountStock = new DataTable();
            DataTable dt = ItembarcodeClass.FindDIM_ByBarcode(pBarcode);
            if (dt.Rows.Count == 0) return dtCountStock;

            string sql = $@"
                DECLARE @pBch AS NVARCHAR(50) = '{pBch}'
                DECLARE @pConfigID AS NVARCHAR(50) = '{ dt.Rows[0]["CONFIGID"]}'
                DECLARE @pSizeID AS NVARCHAR(50) = '{dt.Rows[0]["INVENTSIZEID"]}'
                DECLARE @pColorID AS NVARCHAR(50) = '{dt.Rows[0]["INVENTCOLORID"]}'
                DECLARE @pItemID AS  NVARCHAR(50) = '{dt.Rows[0]["ITEMID"]}'

                SELECT 	INVENTJOURNALTRANS.JOURNALID AS DOCNO,CONVERT(VARCHAR,INVENTJOURNALTRANS.TRANSDATE,23) AS SALEDATE,
		                INVENTJOURNALTRANS.COUNTED AS SALEQTY,INVENTJOURNALTRANS.INVENTONHAND
		
                FROM	SHOP2013TMP.dbo.INVENTJOURNALTRANS WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK)  ON INVENTJOURNALTRANS.JOURNALID = INVENTJOURNALTABLE.JOURNALID
		                INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK) ON INVENTJOURNALTRANS.INVENTDIMID = INVENTDIM.INVENTDIMID	AND INVENTJOURNALTRANS.DATAAREAID = INVENTDIM.DATAAREAID 

                WHERE	INVENTJOURNALTRANS.DATAAREAID = N'SPC'
                        AND INVENTJOURNALTABLE.DATAAREAID = N'SPC'
		                AND INVENTJOURNALTRANS.JOURNALTYPE = '4'
		                AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom = @pBch
		                AND CONFIGID = @pConfigID
                        AND INVENTSIZEID = @pSizeID
                        AND INVENTCOLORID = @pColorID
                        AND INVENTJOURNALTRANS.ITEMID = @pItemID

                ORDER BY CONVERT(VARCHAR,INVENTJOURNALTRANS.TRANSDATE,23) DESC
            ";
            dtCountStock = ConnectionClass.SelectSQL_Main(sql);
            return dtCountStock;
        }
        //การโอนเข้าสาขา สรุปรวมตามบาร์โค้ด pType 6 การรับเข้า 7 การคืน
        public static DataTable GetQtyTOSumBarcodeMin_ByBch(string pGrp, string pDate1, string pDate2, string pBch, string pType)
        {
            string sql = $@"
                SELECT  ENTITY,PD_ENTITY.DESCRIPTION,INVENTITEMBARCODE_MINMAX.ITEMBARCODE AS PRODUCT_1,
		                PT_PURCHASE_TRANSFERSHIP.[SPC_ITEMNAME] ,SUM(INVENTQTY) AS INVENTQTY ,SUM(LINEAMOUNT) AS LINEAMOUNT,INVENTITEMBARCODE_MINMAX.UNITID
		
                FROM	dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)  LEFT OUTER JOIN dbo.PD_ENTITY WITH (NOLOCK)   ON PT_PURCHASE_TRANSFERSHIP.ENTITY = PD_ENTITY.CODE 
		                LEFT OUTER JOIN dbo.RPTINVENTTABLE WITH (NOLOCK) ON PT_PURCHASE_TRANSFERSHIP.ITEMID = RPTINVENTTABLE.ITEMID 
		                LEFT OUTER JOIN dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) ON PT_PURCHASE_TRANSFERSHIP.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID 
		                AND PT_PURCHASE_TRANSFERSHIP.CONFIGID = INVENTITEMBARCODE_MINMAX.CONFIGID AND PT_PURCHASE_TRANSFERSHIP.INVENTSIZEID = INVENTITEMBARCODE_MINMAX.INVENTSIZEID 
		                AND PT_PURCHASE_TRANSFERSHIP.INVENTCOLORID = INVENTITEMBARCODE_MINMAX.INVENTCOLORID 

                WHERE	BRANCH = '{pBch}'  AND PT_PURCHASE_TRANSFERSHIP.DATAAREAID = 'SPC'  AND PD_ENTITY.DATAAREAID = 'SPC' 
		                 AND TRANSTYPE = '{pType}'   AND TimePeriod BETWEEN '{pDate1}' AND '{pDate2}' AND RPTINVENTTABLE.DATAAREAID = 'SPC' 
		                 AND INVENTITEMBARCODE_MINMAX.TYPE_ LIKE 'MIN'  {pGrp}

                GROUP BY  ENTITY,PD_ENTITY.[DESCRIPTION],PT_PURCHASE_TRANSFERSHIP.SPC_ITEMNAME ,INVENTITEMBARCODE_MINMAX.ITEMBARCODE ,INVENTITEMBARCODE_MINMAX.UNITID
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //การขายสาขา สรุปรวมตามบาร์โค้ด  
        public static DataTable GetQtySaleSumBarcodeMin_ByBch(string pGrp, string pDate1, string pDate2, string pBch)
        {
            string sql = $@"
                SELECT	ENTITY,DESCRIPTION,INVENTITEMBARCODE_MINMAX.ITEMBARCODE AS PRODUCT_1,NAME AS SPC_ITEMNAME 
                        ,SUM(INVENTQTY) AS INVENTQTY 
                        ,SUM(LINEAMOUNT) AS LINEAMOUNT, INVENTITEMBARCODE_MINMAX.UNITID
                FROM	dbo.VIEW_PT_RETAIL WITH (NOLOCK) 
                        LEFT OUTER JOIN dbo.PD_ENTITY WITH (NOLOCK)   ON VIEW_PT_RETAIL.ENTITY = PD_ENTITY.CODE 
                        LEFT OUTER JOIN dbo.RPTINVENTTABLE WITH (NOLOCK) ON VIEW_PT_RETAIL.ITEMID = RPTINVENTTABLE.ITEMID 
                        LEFT OUTER JOIN dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) ON VIEW_PT_RETAIL.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID 
                        AND VIEW_PT_RETAIL.CONFIGID = INVENTITEMBARCODE_MINMAX.CONFIGID 
                        AND VIEW_PT_RETAIL.INVENTSIZEID = INVENTITEMBARCODE_MINMAX.INVENTSIZEID 
                        AND VIEW_PT_RETAIL.INVENTCOLORID = INVENTITEMBARCODE_MINMAX.INVENTCOLORID 
                WHERE	BRANCH = '{pBch}' 
                        AND VIEW_PT_RETAIL.INVENTSITEID = 'SPC'
                        AND PD_ENTITY.DATAAREAID = 'SPC' 
                        AND TimePeriod BETWEEN '{pDate1}' AND '{pDate2}' 
                        AND RPTINVENTTABLE.DATAAREAID = 'SPC'   AND INVENTITEMBARCODE_MINMAX.TYPE_ = 'MIN'    {pGrp}
                GROUP BY ENTITY,DESCRIPTION,NAME,INVENTITEMBARCODE_MINMAX.ITEMBARCODE , INVENTITEMBARCODE_MINMAX.UNITID
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //การขายสาขา  สรุปยอดรวมมูลค่า ตามฟิลด์ที่ระบุ (ถ้าต้องการตามสาขาส่งสาขามาเพิ่ม)
        public static DataTable GetQtySaleSumGrandAll_ByBch(string pDate1, string pDate2, string pGrpFiled, string pBch)
        {
            string sql = $@"
                SELECT	{pGrpFiled},SUM(LINEAMOUNT) AS LINEAMOUNT 
                FROM	dbo.VIEW_PT_RETAIL WITH (NOLOCK) 
                        LEFT OUTER JOIN dbo.PD_ENTITY WITH (NOLOCK)   ON VIEW_PT_RETAIL.ENTITY = PD_ENTITY.CODE 
                        LEFT OUTER JOIN dbo.RPTINVENTTABLE WITH (NOLOCK) ON VIEW_PT_RETAIL.ITEMID = RPTINVENTTABLE.ITEMID 
                        LEFT OUTER JOIN dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) ON VIEW_PT_RETAIL.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID 
                        AND VIEW_PT_RETAIL.CONFIGID = INVENTITEMBARCODE_MINMAX.CONFIGID 
                        AND VIEW_PT_RETAIL.INVENTSIZEID = INVENTITEMBARCODE_MINMAX.INVENTSIZEID 
                        AND VIEW_PT_RETAIL.INVENTCOLORID = INVENTITEMBARCODE_MINMAX.INVENTCOLORID 
                WHERE	BRANCH LIKE 'MN%' 
                        AND VIEW_PT_RETAIL.INVENTSITEID = 'SPC'
                        AND PD_ENTITY.DATAAREAID = 'SPC' 
                        AND TimePeriod BETWEEN '{pDate1}' AND '{pDate2}' 
                        AND RPTINVENTTABLE.DATAAREAID = 'SPC'   AND INVENTITEMBARCODE_MINMAX.TYPE_ = 'MIN'    {pBch}
                GROUP BY {pGrpFiled}  ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //การโอนเข้าสาขา สรุปยอดรวมมูลค่า ตามฟิลด์ที่ระบุ  (ถ้าต้องการตามสาขาส่งสาขามาเพิ่ม)
        public static DataTable GetQtyTOSumGrandAll_ByBch(string pType, string pDate1, string pDate2, string pGrpFiled, string pBch)
        {
            string sql = $@"
                SELECT {pGrpFiled},SUM(LINEAMOUNT) AS LINEAMOUNT
		
                FROM	dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)  LEFT OUTER JOIN dbo.PD_ENTITY WITH (NOLOCK)   ON PT_PURCHASE_TRANSFERSHIP.ENTITY = PD_ENTITY.CODE 
		                LEFT OUTER JOIN dbo.RPTINVENTTABLE WITH (NOLOCK) ON PT_PURCHASE_TRANSFERSHIP.ITEMID = RPTINVENTTABLE.ITEMID 
		                LEFT OUTER JOIN dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) ON PT_PURCHASE_TRANSFERSHIP.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID 
		                AND PT_PURCHASE_TRANSFERSHIP.CONFIGID = INVENTITEMBARCODE_MINMAX.CONFIGID AND PT_PURCHASE_TRANSFERSHIP.INVENTSIZEID = INVENTITEMBARCODE_MINMAX.INVENTSIZEID 
		                AND PT_PURCHASE_TRANSFERSHIP.INVENTCOLORID = INVENTITEMBARCODE_MINMAX.INVENTCOLORID 

                WHERE	BRANCH LIKE 'MN%'  AND PT_PURCHASE_TRANSFERSHIP.DATAAREAID = 'SPC'  AND PD_ENTITY.DATAAREAID = 'SPC' 
		                 AND TRANSTYPE = '{pType}'   AND TimePeriod BETWEEN '{pDate1}' AND '{pDate2}' AND RPTINVENTTABLE.DATAAREAID = 'SPC' 
		                 AND INVENTITEMBARCODE_MINMAX.TYPE_ LIKE 'MIN'  {pBch}
                GROUP BY  {pGrpFiled} ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //บิลเปลี่ยน
        public static DataTable GetBillU(string pDate1, string pDate2, string pBch)
        {
            string sql = $@"
            SELECT	INVENTLOCATIONID,REPLACE(POSGROUP.NAME,'-คืนสินค้า','') AS BRANCH_NAME,  
		             XXX_POSTABLE.INVOICEID, XXX_POSTABLE.INVOICEACCOUNT, XXX_POSTABLE.NAME, XXX_POSTABLE.PHONE,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE, 
		             XXX_POSLINE.LINENUM, XXX_POSLINE.ITEMBARCODE, XXX_POSLINE.NAME AS ItemName, XXX_POSLINE.QTY, XXX_POSLINE.SALESUNIT, XXX_POSLINE.SALESPRICE, XXX_POSLINE.LINEAMOUNT 
					 ,ISNULL(IMGPATH,'') AS IMGPATH,CASE XXX_POSTABLE.REMARKS WHEN '(Cashback)' THEN '1' ELSE '0' END AS Cash
					  ,POSLINEVOID.EMPLKBFUNCTION + CHAR(10) + TMP.SPC_NAME AS CASHIERIDREF
					  ,POSLINEVOID.CASHIERID + CHAR(10) + EMPLTABLE.SPC_NAME AS CASHIERID
             FROM	dbo.XXX_POSTABLE WITH (NOLOCK)    
		             INNER JOIN dbo.POSGROUP WITH (NOLOCK) on XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP   
		             INNER JOIN dbo.XXX_POSLINE WITH (NOLOCK) on XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID 
					 LEFT OUTER JOIN POSLINEVOIDIMG WITH (NOLOCK) ON   XXX_POSLINE.INVOICEID = POSLINEVOIDIMG.INVOICEID
					 LEFT OUTER JOIN POSLINEVOID WITH (NOLOCK) ON  XXX_POSLINE.INVOICEID  = POSLINEVOID.INVOICEID
					 INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
					 INNER JOIN EMPLTABLE  WITH (NOLOCK) ON POSLINEVOID.CASHIERID  = EMPLTABLE.EMPLID

             WHERE	XXX_POSTABLE.DOCUTYPE  = '2' AND XXX_POSTABLE.SIGN = '-1'  
		            AND CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23)  BETWEEN '{pDate1}' AND '{pDate2}'
					AND INVENTLOCATIONID LIKE 'MN%'  AND INVENTLOCATIONID NOT IN ('MN098','MN998')  {pBch}

             ORDER BY XXX_POSTABLE.POSGROUP,XXX_POSTABLE.INVOICEID,XXX_POSLINE.LINENUM
            ";
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }

        //ค้นหาจำนวนบิลและยอดซื้อ ตามลูกค้า/ตามวันที่เริ่ม จนถึง ปจบ
        public static DataTable GetXXX_DetailByCstID(string pType, string AccountNum, string PW_DATETIME_IN)//0 CountBill 1 Hitory 2 DetailBillByInvoiceID
        {
            string sql = "";
            switch (pType)
            {
                case "0":
                    sql = $@"  
                            SELECT  ISNULL(COUNT(INVOICEACCOUNT),0) AS COUNT_BILL, ISNULL(SUM(INVOICEAMOUNT), 0) AS SUM_BILL 
                            FROM	XXX_POSTABLE WITH (NOLOCK) 
                            WHERE	INVOICEACCOUNT = '{AccountNum}' 
                                    AND CREATEDDATETIME BETWEEN '{PW_DATETIME_IN}' and GETDATE() ";
                    break;
                case "1":
                    sql = $@" 
                        SELECT	TOP 50 'ประวัติการขาย' AS C,XXX_POSTABLE.INVOICEID AS DOCNO,
		                        XXX_POSTABLE.POSGROUP AS BRANCH,POSGROUP.NAME as BRANCH_NAME,
                                CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) + CHAR(10) +CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,24) AS DATEIN,ITEMBARCODE AS Barcode, 
                                XXX_POSLINE.NAME AS ItemName,QTY AS Qty,SALESUNIT AS UnitID  ,'2' AS STA,XXX_POSTABLE.POINT
                        FROM	dbo.XXX_POSTABLE WITH (NOLOCK) 
                                INNER JOIN dbo.XXX_POSLINE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID =XXX_POSLINE.INVOICEID 
                                LEFT OUTER JOIN POSGROUP  WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP 
                        WHERE	XXX_POSTABLE.INVOICEACCOUNT = '{AccountNum}'  AND XXX_POSTABLE.SIGN = '1' 
                        ORDER BY XXX_POSTABLE.INVOICEID DESC,LINENUM ";
                    break;
                case "2":
                    sql = $@"
                        SELECT	INVOICEID,INVOICEACCOUNT,XXX_POSTABLE.NAME AS ACCOUNTNAME,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE,
		                        XXX_POSTABLE.POSGROUP,POSGROUP.NAME AS POSGROUPNAME,CASHIERID,SYSUSERINFO.NAME AS CASHIERNAME,
                                FORMAT(INVOICEAMOUNT, 'N2') AS INVOICEAMOUNT,FORMAT(POINT, 'N2') AS POINT,
		                        CASE WHEN CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) = CONVERT(VARCHAR,GETDATE(),23) THEN '0' ELSE '1' END AS STA
		
                        FROM	XXX_POSTABLE WITH (NOLOCK)
		                        INNER JOIN SYSUSERINFO WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = SYSUSERINFO.EMPLID
		                        INNER JOIN POSGROUP WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP

                        WHERE	XXX_POSTABLE.INVOICEID = '{AccountNum}'
		                        AND XXX_POSTABLE.DOCUTYPE = '1' 
                        ";
                    break;
                default:
                    break;
            }
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }
        //ค้นหาพนักงานที่ login ขายอยู่
        public static DataTable Get_POSLOGINOPEN(string altNum)
        {
            string sql = $@"
                            SELECT	EMPLID,NAME,POSGROUP,ZONEID,POSNUMBER,LOCATIONID  
                            FROM	dbo.POSLOGINOPEN WITH (NOLOCK) 
                            WHERE  EMPLID LIKE '%{altNum}' ";
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }
        //ค้นหายอดเงินที่อยู่ในลิ้นชัก ที่ยังไม่ส่งเงิน
        public static DataTable Get_POSPAYMSUM(string altNum)
        {
            string sql = $@"
                            SELECT  LINEAMOUNT  
                            FROM    dbo.POSPAYMSUM WITH(NOLOCK)
                            WHERE   EMPLID  LIKE '%{altNum}' AND PAYMMODE = 0  ";
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }

        //RC By Bill
        public static DataTable GetDetail_XXXPOSTABLE(string date1, string date2, string find_Branch, string find_Barcode, string find_CstID,
            string find_Invoice, string find_Location, string findPrice)
        {
            string strHD = $@"
                SELECT	XXX_POSTABLE.POSGROUP,POSGROUP.NAME AS POSGROUPNAME, XXX_POSTABLE.INVOICEID,
                        CONVERT (varchar ,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE ,
                        XXX_POSTABLE.INVOICEACCOUNT + '-' + XXX_POSTABLE.NAME AS CSTNAME,XXX_POSTABLE.INVOICEAMOUNT , 
                        XXX_POSTABLE.CASHIERID + '-' + EMPLTABLE.SPC_NAME AS EMPNAME,XXX_POSTABLE.POSNUMBER,LOCATIONID, 
                        CASE XXX_POSTABLE.SIGN WHEN '-1' THEN 'บิลเปลี่ยน/บิลยกเลิก' ELSE 'บิลขาย' END AS SIGN_BILL,REMARKS, 
                        CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) AS CREATEDDATETIME  
                FROM	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK)  
                        INNER JOIN SHOP2013TMP.dbo.POSTABLE  WITH (NOLOCK) ON XXX_POSTABLE.POSNUMBER = POSTABLE.POSNUMBER  
                        INNER JOIN SHOP2013TMP.dbo.POSGROUP   WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP   
                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'  
                        INNER JOIN SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID
                WHERE	--XXX_POSTABLE.DOCUTYPE = '1' AND 
                        XXX_POSTABLE.INVOICEDATE BETWEEN '{date1}'  AND '{date2}' 
                        {find_Branch}  {find_Barcode } {find_CstID} {find_Invoice} {find_Location} {findPrice} 
                GROUP BY XXX_POSTABLE.POSGROUP,POSGROUP.NAME  , XXX_POSTABLE.INVOICEID,
                        CONVERT(varchar, XXX_POSTABLE.INVOICEDATE, 23) ,
                        XXX_POSTABLE.INVOICEACCOUNT + '-' + XXX_POSTABLE.NAME,XXX_POSTABLE.INVOICEAMOUNT , 
                        XXX_POSTABLE.CASHIERID + '-' + EMPLTABLE.SPC_NAME ,XXX_POSTABLE.POSNUMBER,LOCATIONID, 
                        CASE XXX_POSTABLE.SIGN WHEN '-1' THEN 'บิลเปลี่ยน/บิลยกเลิก' ELSE 'บิลขาย' END , REMARKS,
                        CONVERT(VARCHAR, XXX_POSTABLE.CREATEDDATETIME, 25)  
                ORDER BY CONVERT(VARCHAR,XXX_POSTABLE.CREATEDDATETIME,25) ";
            return ConnectionClass.SelectSQL_Main(strHD);
        }
        //Dt By Bill
        public static DataTable GetDetail_XXXPOSLINE(string pDocno)
        {
            string strDT = $@"
                SELECT	LINENUM ,ITEMBARCODE ,NAME ,QTY,SALESUNIT ,SALESPRICE,LINEAMOUNT ,
                        CASE ISNULL(T4UTOPUPSONUMBER,'') WHEN '' THEN 'SCAN' ELSE ISNULL(T4UTOPUPSONUMBER,'') END  AS T4UTOPUPSONUMBER
                FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)   
                WHERE	INVOICEID = '{pDocno}' 
                ORDER BY LINENUM";
            return ConnectionClass.SelectSQL_Main(strDT);
        }

        //RC By Bill
        public static DataTable GetDetail_XXXPOSPAYM(string pDocno)
        {
            string strRC = $@"SELECT	COUPREASONID,BANKACCOUNT,BANKBRANCHACCOUNT,TRANSTXT,PAYMAMOUNT,AMOUNTCURCHANGE,
                CASE POSPAYMMODE WHEN '0' THEN 'เงินสด' WHEN '1' THEN 'ธนาคาร/บัตรเครดิต' WHEN '2' THEN 'เช็ค' WHEN '3' THEN 'คูปอง' ELSE '' END AS POSPAYMMODE 
                FROM	SHOP2013TMP.dbo.XXX_POSPAYM WITH (NOLOCK)    
                WHERE	INVOICEID = '{pDocno}'   
                ORDER BY LINENUM ";
            return ConnectionClass.SelectSQL_Main(strRC);
        }

        //
        public static DataTable GetDetailSale_ByBarcode(string NotGroupBy2, string pBarcode, string pDate1, string pDate2, string pBch, string conditionSalePriceType)
        {
            string conditionBch = "";
            if (pBch != "") conditionBch = $@" AND XXX_POSTABLE.POSGROUP = '{pBch}' ";
            string conditionBarcode = "";
            if (pBarcode != "") conditionBarcode = $@" AND XXX_POSLINE.ITEMBARCODE  IN ('{pBarcode}') ";

            string sql;
            if (NotGroupBy2 == "2")
            {
                sql = $@"
                SELECT	XXX_POSLINE.POSGROUP,SHOP_BRANCH_CONFIGDB.BRANCH_NAME AS POSNAME,
		                XXX_POSTABLE.CASHIERID,EMPLTABLE.SPC_NAME,XXX_POSTABLE.INVOICEDATE,
		                XXX_POSLINE.ITEMBARCODE AS BARCODE, --บาร์โค้ดขาย
                        XXX_POSLINE.LINEAMOUNT,XXX_POSLINE.SALESUNIT,XXX_POSLINE.QTY,XXX_POSLINE.NAME AS BARCODENAME,
		                XXX_POSLINE.INVOICEID,CONVERT(VARCHAR,XXX_POSLINE.CREATEDDATETIME,25) AS CREATEDDATETIME,
		                '\\'+SHOP_BRANCH_CONFIGDB.BRANCH_PATHPOS+'\MNCamera\'+BRANCH_ID+'\'+SUBSTRING(CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23),0,8)+'\'+CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23)+'\'  AS PATH_WEBCAM,
                        TMPSALE.SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION,XXX_POSTABLE.INVOICEACCOUNT,XXX_POSTABLE.NAME

                FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) 
		                INNER JOIN	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK) ON XXX_POSLINE.INVOICEID = XXX_POSTABLE.INVOICEID 
		                INNER JOIN	SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
		                LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SUBSTRING(XXX_POSLINE.NAME,CHARINDEX('# ', XXX_POSLINE.NAME) + 2,15) = INVENTITEMBARCODE.ITEMBARCODE AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                INNER JOIN SHOP_BRANCH_CONFIGDB WITH (NOLOCK) ON XXX_POSLINE.POSGROUP = SHOP_BRANCH_CONFIGDB.BRANCH_ID
                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE TMPSALE WITH (NOLOCK) ON XXX_POSLINE.ITEMBARCODE = TMPSALE.ITEMBARCODE AND TMPSALE.DATAAREAID = N'SPC'
                        LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON TMPSALE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM AND DIMENSIONS.DIMENSIONCODE = '0'

                WHERE	XXX_POSLINE.INVOICEDATE BETWEEN '{pDate1}' AND '{pDate2}'
		                AND XXX_POSTABLE.POSGROUP NOT IN ('RT')  {conditionBch}
                        {conditionBarcode}
	                	AND SUPPITEMGROUPID != '/FREEITEM/' AND XXX_POSLINE.DOCUTYPE !='3'  
                        {conditionSalePriceType}

                ORDER BY  XXX_POSLINE.POSGROUP 
                OPTION (FAST 47) ";
            }
            else
            {
                //1 คือ แยกบาร์โค้ด 2 คือไม่แยกบาร์โค้ด
                string sqlGroup = $@"";
                if (NotGroupBy2 == "1") { sqlGroup = ",XXX_POSLINE.ITEMBARCODE "; }
                sql = $@"
                SELECT	XXX_POSLINE.POSGROUP,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE,  
		                SUM(XXX_POSLINE.QTY * XXX_POSLINE.SIGN) AS QTY,
		                SUM(XXX_POSLINE.LINEAMOUNT * XXX_POSLINE.SIGN) AS LINEAMOUNT {sqlGroup}
	 
                FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) 
		                INNER JOIN	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK) ON XXX_POSLINE.INVOICEID = XXX_POSTABLE.INVOICEID 

                WHERE	XXX_POSLINE.INVOICEDATE BETWEEN '{pDate1}' AND '{pDate2}'
		                AND XXX_POSTABLE.POSGROUP NOT IN ('RT')    {conditionBch}
		                {conditionBarcode} {conditionSalePriceType}
	    
                GROUP BY XXX_POSLINE.POSGROUP,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) {sqlGroup}
                ORDER BY  XXX_POSLINE.POSGROUP 
                OPTION (FAST 47) ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายงานการ - void
        public static DataTable GetDetailSale_BillVoid(string pCase, string date1, string date2, string bch)//0 รายละเอียด void Image
        {
            string sql = "";
            switch (pCase)
            {
                case "0"://0 รายละเอียด void Image
                    sql = $@"
                        SELECT	XXX_POSTABLE.POSGROUP + CHAR(10) + POSGROUP.NAME AS NAME,	
                                CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME ,23) AS INVOICEDATE, 
		                        POSLINEVOID.INVOICEID ,POSLINEVOID.CASHIERID + CHAR(10) + EMPLTABLE.SPC_NAME AS SPC_NAME,
                                POSLINEVOID.EMPLKBFUNCTION + CHAR(10) + TMP.SPC_NAME AS SPC_NAME1,
                                POSLINEVOID.REMARKS ,
		                        XXX_POSLINE.ITEMBARCODE,XXX_POSLINE.NAME AS ITEMNAME ,XXX_POSLINE.QTY ,XXX_POSLINE.SALESUNIT ,XXX_POSLINE.LINEAMOUNT ,XXX_POSLINE.CREATEDDATETIME 
	                            ,ISNULL(IMGPATH,'') AS IMGPATH,ISNULL(MACHINENAME,'') AS MACHINENAME

                        FROM	POSLINEVOID WITH (NOLOCK) 
		                        INNER JOIN XXX_POSLINE WITH (NOLOCK) 		ON POSLINEVOID.INVOICEID = XXX_POSLINE.INVOICEID 
			                        AND POSLINEVOID.LINENUM = XXX_POSLINE.LINENUM 
		                        INNER JOIN XXX_POSTABLE  WITH (NOLOCK) 		ON XXX_POSLINE.INVOICEID = XXX_POSTABLE.INVOICEID 
		                        INNER JOIN EMPLTABLE  WITH (NOLOCK) ON POSLINEVOID.CASHIERID  = EMPLTABLE.EMPLID
		                        INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
		                        INNER jOIN POSGROUP WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP 
                                LEFT OUTER JOIN POSLINEVOIDIMG WITH (NOLOCK) ON POSLINEVOID.LINENUM = POSLINEVOIDIMG.LINENUM 
										AND POSLINEVOID.INVOICEID = POSLINEVOIDIMG.INVOICEID

                        WHERE	CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME ,23) BETWEEN '{date1}' AND '{date2}'
                                AND XXX_POSTABLE.POSGROUP IN ( {bch} )
                         ORDER BY XXX_POSTABLE.POSGROUP,XXX_POSLINE.CREATEDDATETIME ";
                    break;
                case "1"://รายละเอียด void ละเอียด
                    string conBch = "";
                    if (bch != "") conBch = $@" AND XXX_POSTABLE.POSGROUP = '{bch}' ";
                    sql = $@"
                        SELECT	XXX_POSTABLE.POSGROUP,POSGROUP.NAME ,	CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME ,23) AS INVOICEDATE, 
		                        POSLINEVOID.INVOICEID ,POSLINEVOID.CASHIERID,EMPLTABLE.SPC_NAME,POSLINEVOID.EMPLKBFUNCTION,TMP.SPC_NAME AS SPC_NAME1,POSLINEVOID.REMARKS ,
		                        XXX_POSLINE.ITEMBARCODE,XXX_POSLINE.NAME AS ITEMNAME ,XXX_POSLINE.QTY ,XXX_POSLINE.SALESUNIT ,XXX_POSLINE.LINEAMOUNT ,XXX_POSLINE.CREATEDDATETIME 
	
                        FROM	POSLINEVOID WITH (NOLOCK) 
		                        INNER JOIN XXX_POSLINE WITH (NOLOCK) 		ON POSLINEVOID.INVOICEID = XXX_POSLINE.INVOICEID 
			                        AND POSLINEVOID.LINENUM = XXX_POSLINE.LINENUM 
		                        INNER JOIN XXX_POSTABLE  WITH (NOLOCK) 		ON XXX_POSLINE.INVOICEID = XXX_POSTABLE.INVOICEID 
		                        INNER JOIN EMPLTABLE  WITH (NOLOCK) ON POSLINEVOID.CASHIERID  = EMPLTABLE.EMPLID
		                        INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
		                        INNER jOIN POSGROUP WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP 

                        WHERE	CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME ,23) BETWEEN  '{date1}' AND '{date2}'  {conBch}
                        ORDER BY XXX_POSTABLE.POSGROUP,XXX_POSLINE.CREATEDDATETIME  ";
                    break;
                case "2"://รายละเอียด void สรุป
                    sql = $@"
                        SELECT	POSGROUP,POSName ,YEAR (DateSale) AS YearVoice,MONTH (DateSale) AS MonthVoice,
		                        CASHIERID,SPC_NAME,EMPLKBFUNCTION,NAMEEMP,COUNT(CASHIERID) AS CountVoice,SUM(LINEAMOUNT) AS CountSum

                        FROM	(

                            SELECT
		                            XXX_POSTABLE.POSGROUP,POSGROUP.NAME AS POSName  ,	CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME ,23) AS DateSale, 
		                            POSLINEVOID.INVOICEID ,POSLINEVOID.CASHIERID,EMPLTABLE.SPC_NAME,POSLINEVOID.EMPLKBFUNCTION,TMP.SPC_NAME AS NAMEEMP,POSLINEVOID.REMARKS ,
		                            XXX_POSLINE.ITEMBARCODE,XXX_POSLINE.NAME ,XXX_POSLINE.QTY ,XXX_POSLINE.SALESUNIT ,XXX_POSLINE.LINEAMOUNT ,XXX_POSLINE.CREATEDDATETIME 
	
                            FROM	POSLINEVOID WITH (NOLOCK) 
		                            INNER JOIN XXX_POSLINE WITH (NOLOCK) 		ON POSLINEVOID.INVOICEID = XXX_POSLINE.INVOICEID 
			                            AND POSLINEVOID.LINENUM = XXX_POSLINE.LINENUM 
		                            INNER JOIN XXX_POSTABLE  WITH (NOLOCK) 		ON XXX_POSLINE.INVOICEID = XXX_POSTABLE.INVOICEID 
		                            INNER JOIN EMPLTABLE  WITH (NOLOCK) ON POSLINEVOID.CASHIERID  = EMPLTABLE.EMPLID
		                            INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
		                            INNER jOIN POSGROUP WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP 

                            WHERE	CONVERT(VARCHAR(10),POSLINEVOID.CREATEDDATETIME ,23) BETWEEN'{date1}' AND '{date2}'

                        )TMP

                        GROUP BY POSGROUP,POSName ,YEAR (DateSale),MONTH(DateSale),CASHIERID,SPC_NAME,EMPLKBFUNCTION,NAMEEMP  ";
                    break;
                case "3":
                    //Data
                    sql = $@" 
                SELECT  CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME,23) AS DateSale, 
		                EMPLKBFUNCTION,SPC_NAME,COUNT(EMPLKBFUNCTION) AS COUNTVOID
                FROM	POSLINEVOID WITH (NOLOCK) 
		                INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
                WHERE	CONVERT(VARCHAR(10),POSLINEVOID.CREATEDDATETIME ,23) BETWEEN '{date1}' 
		                AND '{date2}' AND DIMENSION = 'D054'
                GROUP BY  CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME,23),EMPLKBFUNCTION,SPC_NAME ";
                    break;
                case "4":
                    sql = $@"
                SELECT	EMPLKBFUNCTION,SPC_NAME
                FROM	POSLINEVOID WITH (NOLOCK) 
		                INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
                WHERE	CONVERT(VARCHAR(10),POSLINEVOID.CREATEDDATETIME ,23) BETWEEN '{date1}' 
		                AND '{date2}' AND DIMENSION = 'D054' 
                GROUP BY EMPLKBFUNCTION,SPC_NAME
                ORDER BY EMPLKBFUNCTION,SPC_NAME ";
                    break;
                default:
                    break;
            }

            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }
        //รายงานพิมพ์บิลซ้ำ
        public static DataTable GetDetail_BillRePrint(string date1, string date2, string bchID)
        {
            string conBch = "";
            if (bchID != "") conBch = $@" AND POSGROUP.POSGROUP = '{bchID}' ";
            string sql = $@"
                     select	CONVERT(VARCHAR,INVOICEDATE,23) AS INVOICEDATE,POSGROUP.POSGROUP,POSGROUP.NAME,  
		                    INVOICEID, INVOICEAMOUNT, CASHIERID, NAMECH.SPC_NAME AS NAMECH, PRINTCOUNT, LASTPRINTBY, NAMELAST.SPC_NAME AS NAMELAST  
                     from	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK)   
		                     INNER JOIN  SHOP2013TMP.dbo.POSGROUP WITH (NOLOCK) 	ON XXX_POSTABLE.POSGROUP = POSGROUP.POSGROUP  
		                     LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE NAMECH  WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = NAMECH.EMPLID  
		                     LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE NAMELAST  WITH (NOLOCK) ON XXX_POSTABLE.LASTPRINTBY = NAMELAST.EMPLID  
                     WHERE	XXX_POSTABLE.posGroup LIKE 'MN%'  
		                     AND PRINTCOUNT > 1  AND DOCUTYPE = '1'  
		                     AND INVOICEDATE  between '{date1}' AND '{date2}'  {conBch}
                    ORDER BY POSGROUP.POSGROUP,PRINTCOUNT		                     
                    ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //พิมพ์ซ้ำ
        public static DataTable GetDetail_BillSaleDecimal(string date1, string date2)
        {
            string sqlSelect3 = $@"
                     SELECT	XXX_POSTABLE.POSGROUP,BRANCH_NAME,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE,CASHIERID,SPC_NAME,  
		                    XXX_POSTABLE.INVOICEID,ITEMBARCODE,XXX_POSLINE.NAME,QTY,SALESUNIT,SALESPRICE,LINEAMOUNT,QTY*10*SALESPRICE AS SUMALL,(QTY*10*SALESPRICE)-LINEAMOUNT AS DIF  

                     FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)  
		                     INNER JOIN SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK) ON XXX_POSLINE.INVOICEID = XXX_POSTABLE.INVOICEID  
		                     INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON XXX_POSLINE.POSGROUP = SHOP_BRANCH.BRANCH_ID  
		                     INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID  

                     WHERE	XXX_POSTABLE.INVOICEDATE BETWEEN '{date1}'  AND '{date2}'
		                    AND ITEMBARCODE NOT IN ( '5553','55533' ) 	  
		                    AND XXX_POSTABLE.DOCUTYPE = '1' AND XXX_POSLINE.DOCUTYPE = '1' 
		                    AND XXX_POSTABLE.POSGROUP LIKE 'MN%' 

                    ORDER BY XXX_POSTABLE.POSGROUP,XXX_POSTABLE.INVOICEDATE                     ";
            return ConnectionClass.SelectSQL_Main(sqlSelect3);
        }
        //  อัตราการขายย้อนหลัง 3 เดือน
        public static DataTable GetDataSaleTOPC_ByNum(string bchID, string num)
        {
            string sql = $@"
                    SELECT	ITEMBARCODE,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID,SPC_ITEMNAME, 
                    INVENTITEMBARCODE_MINMAX.SPC_ITEMBUYERGROUPID,INVENTITEMBARCODE_MINMAX.CONFIGID,INVENTITEMBARCODE_MINMAX.INVENTSIZEID,INVENTITEMBARCODE_MINMAX.INVENTCOLORID, 
                    ISNULL(TMPSALE.SALEQTY,0) AS SALEQTY,ISNULL(TMPPR.QTYPR,0) AS QTYPR,ISNULL(TMPPC.PCQTY,0) AS PCQTY 
                    ,CASE ISNULL(SALEQTY,0) WHEN 0 THEN '0' ELSE ISNULL(SALEQTY,0)/3 END AS PER_MONTH,  
                    (ISNULL(QTYPR,0)-ISNULL(SALEQTY,0)-ISNULL(PCQTY,0)) AS QTYHAVE, 
                    CASE ISNULL(SALEQTY,0) WHEN 0 THEN '100' ELSE (ISNULL(QTYPR,0)-ISNULL(SALEQTY,0)-ISNULL(PCQTY,0))/(ISNULL(SALEQTY,0)/3) END AS PER_SALE 
                    FROM	dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) 
                    LEFT OUTER JOIN 
                    ( 
                        SELECT	SUM(INVENTQTY) AS SALEQTY,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
                        FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)  
                        WHERE	BRANCH = '{bchID}'   
                        AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()  
                        AND ENTITY = '{num}' 
                        GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
                    )TMPSALE	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPSALE.CONFIGID 
                        AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPSALE.INVENTSIZEID 
                        AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPSALE.INVENTCOLORID 
                        AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPSALE.ITEMID 
                    LEFT OUTER JOIN  
                    ( SELECT	 SUM(INVENTQTY) AS QTYPR	,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
                        FROM	dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)  
                        WHERE	BRANCH = '{bchID}'   
                        AND TRANSTYPE = '6'  
                        AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()  
                        AND ENTITY = '{num}' 
                        GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
                    )TMPPR	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPPR.CONFIGID 
                        AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPPR.INVENTSIZEID 
                        AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPPR.INVENTCOLORID 
                        AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPPR.ITEMID 
                    LEFT OUTER JOIN  
                    ( SELECT	 SUM(INVENTQTY) AS PCQTY	,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
                        FROM	dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)   
                        WHERE	BRANCH = '{bchID}'   
                        AND TRANSTYPE = '7'   
                        AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()   
                        AND ENTITY = '{num}'
                        GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
                    )TMPPC	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPPC.CONFIGID 
                        AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPPC.INVENTSIZEID 
                        AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPPC.INVENTCOLORID 
                        AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPPC.ITEMID 
                    WHERE	SPC_ITEMBUYERGROUPID = '{num}' 
                        AND TYPE_ = 'MIN' 
                        AND SPC_ITEMACTIVE = '1'  
                        AND ISNULL(TMPSALE.SALEQTY,0) + ISNULL(TMPPR.QTYPR,0) + (ISNULL(TMPPC.PCQTY,0)*-1)   > 0 ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }
        //การขายสินค้าทั้งหมด ตามกลุ่ม
        public static DataTable GetDataSale_ByGroupID(string bchID, string grp)
        {
            string sql = $@"
                    SELECT	ITEMBARCODE,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID,SPC_ITEMNAME, 
		                    INVENTITEMBARCODE_MINMAX.SPC_ITEMBUYERGROUPID,DIMENSIONNAME,INVENTITEMBARCODE_MINMAX.CONFIGID,
		                    INVENTITEMBARCODE_MINMAX.INVENTSIZEID,INVENTITEMBARCODE_MINMAX.INVENTCOLORID, 
		                    ISNULL(TMPSALE.SALEQTY,0) AS SALEQTY    ,CASE ISNULL(SALEQTY,0) WHEN 0 THEN '0' ELSE ISNULL(SALEQTY,0)/3 END AS PER_MONTH             
                    FROM	dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) 
		                    LEFT OUTER JOIN 
		                    ( 
			                    SELECT	SUM(INVENTQTY) AS SALEQTY,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
			                    FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)  
			                    WHERE	BRANCH = '{bchID}'   
			                    AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()  
			                    GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
		                    )TMPSALE	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPSALE.CONFIGID 
			                    AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPSALE.INVENTSIZEID 
			                    AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPSALE.INVENTCOLORID 
			                    AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPSALE.ITEMID        
		                    LEFT OUTER JOIN dbo.RPTINVENTTABLE on dbo.INVENTITEMBARCODE_MINMAX.ITEMID = dbo.RPTINVENTTABLE.ITEMID 
                    WHERE	TYPE_ = 'MIN' 
		                    AND SPC_ITEMACTIVE = '1'  
		                    AND INVENTGROUPID = '{grp}'
                    ORDER BY SPC_ITEMNAME			 ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
        }

        public static DataTable GetDetailSale_ByLineAdd(string pCase, string date1, string date2)
        {
            string sql = "";
            switch (pCase)
            {
                case "0":
                    sql = $@" 
                       SELECT	SHOP_COUPONONLINETRANS.COUPONNUMBER,ISNULL(XXX_POSTABLE.POSGROUP,'') AS POSGROUP,
		                        CASE ISNULL(XXX_POSTABLE.POSGROUP,'') WHEN '' THEN 'บิลยังไม่โอน' WHEN 'RT' THEN 'สาขาใหญ่' ELSE Shop_Branch.BRANCH_NAME END AS POSNAME,
		                        CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) AS INVOICEDATE,CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25) AS CREATEDDATETIME,
		                        SHOP_COUPONONLINETRANS.INVOICEID AS INVOICEID,
		                        ISNULL(XXX_POSTABLE.INVOICEACCOUNT,'') AS INVOICEACCOUNT,ISNULL(XXX_POSTABLE.NAME,'') + CASE ISNULL(CUSTGROUP,'') WHEN 'CEMP' THEN ' [พนง.]' ELSE '' END AS NAME,
		                        ISNULL(CAST(XXX_POSTABLE.INVOICEAMOUNT AS DECIMAL(7,2)),0) AS INVOICEAMOUNT,COUPONVALUE,
		                        SHOP_COUPONONLINE.REMARK,
		                        ISNULL(COUNT(XXX_POSLINE.LINENUM),0) AS LineNum
                        FROM	SHOP_COUPONONLINETRANS WITH (NOLOCK)
		                        LEFT OUTER JOIN XXX_POSTABLE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.INVOICEID = XXX_POSTABLE.INVOICEID
		                        LEFT OUTER JOIN XXX_POSLINE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID AND XXX_POSLINE.DOCUTYPE = '1'
		                        LEFT OUTER JOIN SHOP_COUPONONLINE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
		                        LEFT OUTER JOIN Shop_Branch WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = Shop_Branch.BRANCH_ID
                                LEFT OUTER JOIN CUSTTABLE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEACCOUNT = CUSTTABLE.ACCOUNTNUM 
                                LEFT OUTER JOIN SHOP_COUPONONLINE_DT WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.ITEMBARCODE = SHOP_COUPONONLINE_DT.ITEMBARCODE AND 
			                        SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE_DT.COUPONNUMBER

                        WHERE	CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) BETWEEN  '{date1}' AND '{date2}' 

                        GROUP BY SHOP_COUPONONLINETRANS.COUPONNUMBER,ISNULL(XXX_POSTABLE.POSGROUP,''),CASE ISNULL(XXX_POSTABLE.POSGROUP,'') WHEN '' THEN 'บิลยังไม่โอน' WHEN 'RT' THEN 'สาขาใหญ่' ELSE Shop_Branch.BRANCH_NAME END ,
		                        CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23),CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25),
		                        SHOP_COUPONONLINETRANS.INVOICEID,ISNULL(XXX_POSTABLE.INVOICEACCOUNT,''),ISNULL(XXX_POSTABLE.NAME,''),ISNULL(CAST(XXX_POSTABLE.INVOICEAMOUNT AS DECIMAL(7,2)),0),
		                        CASE WHEN ISNULL(SHOP_COUPONONLINE_DT.SPC_ITEMNAME,'') = '' THEN 'ส่วนลดท้ายบิล' ELSE SHOP_COUPONONLINE_DT.SPC_ITEMNAME  END,SHOP_COUPONONLINE.REMARK,ISNULL(CUSTGROUP,''),COUPONVALUE

                        ORDER BY CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25)  ";
                    break;
                case "1":
                    sql = $@" 
                       SELECT	
		                        COUPONVALUE,COUNT(SHOP_COUPONONLINETRANS.ITEMBARCODE) AS QTY,
		                        COUPONVALUE*COUNT(SHOP_COUPONONLINETRANS.ITEMBARCODE) AS AMOUNT,SHOP_COUPONONLINE.REMARK
                                
                        FROM	SHOP_COUPONONLINETRANS WITH (NOLOCK)
		                        LEFT OUTER JOIN SHOP_COUPONONLINE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
		                        LEFT OUTER JOIN SHOP_COUPONONLINE_DT WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.ITEMBARCODE = SHOP_COUPONONLINE_DT.ITEMBARCODE AND 
			                        SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE_DT.COUPONNUMBER
                             
                        WHERE	CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) BETWEEN  '{date1}' AND '{date2}' 

                        GROUP BY SHOP_COUPONONLINE.REMARK,COUPONVALUE

                        ORDER BY SHOP_COUPONONLINE.REMARK
                    ";
                    break;
                case "2":
                    sql = $@"    SELECT	MONTH,YEAR,POSITION,
	 	                        SHOP_COUPONONLINE_CUST.COUPONNUMBER,ISNULL(XXX_POSTABLE.POSGROUP,'') AS POSGROUP,
		                        CASE ISNULL(XXX_POSTABLE.POSGROUP,'') WHEN '' THEN 'บิลยังไม่โอน' WHEN 'RT' THEN 'สาขาใหญ่' ELSE Shop_Branch.BRANCH_NAME END AS POSNAME,
		                        CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) AS INVOICEDATE,CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25) AS CREATEDDATETIME,
		                        ISNULL(SHOP_COUPONONLINETRANS.INVOICEID,'') AS INVOICEID,
		                        ISNULL(XXX_POSTABLE.INVOICEACCOUNT,'') AS INVOICEACCOUNT,XXX_POSTABLE.NAME,
		                        ISNULL(CAST(XXX_POSTABLE.INVOICEAMOUNT AS DECIMAL(7,2)),0) AS INVOICEAMOUNT,COUPONVALUE,
		                        SHOP_COUPONONLINE.REMARK

                        FROM	SHOP_COUPONONLINE_CUST WITH (NOLOCK)
		                        INNER JOIN SHOP_COUPONONLINE WITH (NOLOCK) ON SHOP_COUPONONLINE_CUST.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
		                        LEFT OUTER JOIN SHOP_COUPONONLINETRANS WITH (NOLOCK) 
		                        ON SHOP_COUPONONLINE_CUST.COUPONNUMBER = SHOP_COUPONONLINETRANS.COUPONNUMBER
			                        AND SHOP_COUPONONLINE_CUST.CUSTACCOUNT = SHOP_COUPONONLINETRANS.CUSTACCOUNT
		                        LEFT OUTER JOIN XXX_POSTABLE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.INVOICEID = XXX_POSTABLE.INVOICEID
		                        LEFT OUTER JOIN Shop_Branch WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = Shop_Branch.BRANCH_ID  

                        WHERE   MONTH = MONTH('{date1}') AND  YEAR = YEAR('{date2}') 
                                AND SHOP_COUPONONLINE_CUST.CUSTACCOUNT != 'CONTROL'

                        ORDER BY ISNULL(SHOP_COUPONONLINETRANS.INVOICEID,'') ";
                    break;
                default:
                    break;
            }

            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }
        //monitor pos webcam
        public static DataTable GetDetail_MonitorPOS(string pPos)
        {
            string sql = $@"
                        DECLARE @MAXVERSION AS VARCHAR(20)
                        SELECT @MAXVERSION=MAX(POSVERSION) FROM SYSVERSION;
                        SELECT
                               [STOREID]=T3.POSGROUP
                               ,[ZONEID]=T3.ZONEID
                               ,[TERMINAL]=T1.TERMINALID
                               ,[MACHINE]=T3.MACHINENAME
                               ,[LOCATION]=T3.LOCATIONID
                               ,[VERSION]=T4.POSVERSION+CASE T4.POSVERSION WHEN @MAXVERSION THEN '(Latest)' ELSE '' END
                               ,[CASHIER]=ISNULL(T6.EMPLID,'')
                               ,[LASTOFFLINEDB]=CASE WHEN T6.EMPLID IS NOT NULL AND DATEDIFF(DD,T5.LASTUPDATE,GETDATE())>=1 THEN CAST(DATEDIFF(DD,T5.LASTUPDATE,GETDATE()) AS VARCHAR)+' day' ELSE '' END
                               ,T1.LASTINVOICEID
                               ,T1.LASTINVOICEDATETIME
                               ,[LASTRECEIPT]=CASE WHEN ISNULL(T6.EMPLID,'') != '' AND T6.SIGNINDATETIME<T1.LASTINVOICEDATETIME THEN CAST(DATEDIFF(MI,T1.LASTINVOICEDATETIME,GETDATE()) AS VARCHAR)+' minute' ELSE '' END
                               ,'0' AS STAJOB 
                        FROM
                               (
                                      SELECT
                                             [TERMINALID]=S1.POSNUMBER
                                             ,[LASTINVOICEID]=MAX(S1.INVOICEID)
                                             ,[LASTINVOICEDATETIME]=MAX(S1.CREATEDDATETIME)
                                      FROM
                                             XXX_POSTABLE S1 WITH(NOLOCK)
                                             INNER JOIN (SELECT POSNUMBER FROM SYSVERSION WITH(NOLOCK) WHERE POSVERSION LIKE N'6%') S2 ON S2.POSNUMBER=S1.POSNUMBER
                                      GROUP BY 
                                             S1.POSNUMBER
                               ) T1
                               INNER JOIN POSTABLE T3 WITH(NOLOCK) ON T3.POSNUMBER=T1.TERMINALID AND T3.POSSTATUS=1 {pPos}
                               INNER JOIN SYSVERSION T4 WITH(NOLOCK) ON T4.POSNUMBER=T3.POSNUMBER AND T4.MACHINENAME=T3.MACHINENAME
                               LEFT JOIN SYSTABLELASTUPDATE T5 WITH(NOLOCK) ON T5.MACHINENAME=T3.MACHINENAME
                               LEFT JOIN POSLOGINOPEN T6 WITH(NOLOCK) ON T6.POSNUMBER=T1.TERMINALID AND T6.TRANSSTATUS<>4
                        ORDER BY
	                           T3.POSGROUP,
                               T3.LOCATIONID ASC

                    ";
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }
        //ค่าตคอมของแคชเชียร์ parttime
        public static DataTable GetCommissionCashPartTime(string pDate, string percen, string hr)
        {
            string sqlParttimeDetail = $@"
                        SELECT	PW_BRANCH_IN AS BRANCH_ID,SHOP_BRANCH.BRANCH_NAME,PW_IDCARD,PW_PWCARD,PW_NAME,PW_DATETIME_IN,PW_DATETIME_OUT,CONVERT(VARCHAR,PW_DATE_IN,23) AS PW_DATE_IN,
		                        SUM(DATEDIFF(hh,POSLOGINTRANS.SIGNINDATETIME,POSLOGINTRANS.SIGNOUTDATETIME)) AS Hour_LoginSale,
		                        DATEDIFF(hh,MIN(POSLOGINTRANS.SIGNINDATETIME),MAX(POSLOGINTRANS.SIGNOUTDATETIME)) AS HourMinMax_LoginSale,
                                DATEDIFF(hh,MIN(POSLOGINTRANS.SIGNINDATETIME),MAX(POSLOGINTRANS.SIGNOUTDATETIME))*{hr} AS SARALY_Hour,
		                        COUNT(XXX_POSTABLE.INVOICEID)  AS COUNT_BILL,SUM(XXX_POSTABLE.INVOICEAMOUNT) AS SUM_AMOUNT,
		                        FLOOR((SUM(XXX_POSTABLE.INVOICEAMOUNT)*{percen})/100)  AS SARALY_AMOUNT,
		                        (DATEDIFF(hh,MIN(POSLOGINTRANS.SIGNINDATETIME),MAX(POSLOGINTRANS.SIGNOUTDATETIME))*{hr})+(FLOOR((SUM(XXX_POSTABLE.INVOICEAMOUNT)*{percen})/100)) AS SARALY 

                        FROM	SHOP_TIMEKEEPER WITH (NOLOCK)
		                        INNER JOIN SHOP2013TMP.dbo.POSLOGINTRANS WITH (NOLOCK) 
				                        ON SHOP_TIMEKEEPER.PW_IDCARD = POSLOGINTRANS.EMPLID AND SHOP_TIMEKEEPER.PW_BRANCH_IN = POSLOGINTRANS.POSGROUP 
		                        INNER JOIN SHOP_BRANCH WITH (NOLOCK) 
				                        ON SHOP_TIMEKEEPER.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID
                                LEFT OUTER JOIN SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK) 
				                                ON SHOP_TIMEKEEPER.PW_IDCARD = XXX_POSTABLE.CASHIERID 

                        WHERE	PW_DATE_IN = '{pDate}'  
		                        AND PW_TYPE = '2' 
		                        AND POSLOGINTRANS.SIGNINDATETIME BETWEEN PW_DATETIME_IN AND PW_DATETIME_OUT 
                                AND XXX_POSTABLE.CREATEDDATETIME BETWEEN PW_DATETIME_IN AND PW_DATETIME_OUT 

                        GROUP BY PW_BRANCH_IN,SHOP_BRANCH.BRANCH_NAME,PW_IDCARD,PW_PWCARD,PW_NAME,PW_DATETIME_IN,PW_DATETIME_OUT,CONVERT(VARCHAR,PW_DATE_IN,23)
                        ORDER BY PW_BRANCH_IN , PW_IDCARD
                    ";

            return ConnectionClass.SelectSQL_Main(sqlParttimeDetail);
        }
        //ค่าคอมของออเดอร์ลูกค้า
        public static DataTable GetCommissionGetCountBillCash_ByBranch(string DateBegin, string DateEnd, string Branch)
        {
            string SqlCondition = $@" AND DIMENSION   ='{Branch}'";
            if (Branch == "") SqlCondition = $@" AND DIMENSION   like 'MN%' ";

            string sql = $@" 
            select  INVOICEDATE as DocDate,Sale.POSGROUP as Branch,CASHIERID as Cash,sum(Docno) as Docno,
                        sum(INVOICEID) as INVOICEID,sum(LINEAMOUNT) as LINEAMOUNT  from 
                        (   
                	        select INVOICEDATE, POSGROUP, CASHIERID,'0' as Docno, count(INVOICEID) as INVOICEID, LINEAMOUNT from 
                	        ( 
                		        select  SHOP_MNPD_HD.INVOICEDATE,SHOP_MNPD_HD.POSGROUP, 
                		                CASHIERID,'0' as Docno, SHOP_MNPD_HD.INVOICEID, 
                		                SUM(XXX_POSLINE.LINEAMOUNT) as LINEAMOUNT 
                		        FROM    SHOP_MNPD_HD WITH (NOLOCK) INNER JOIN  SHOP_MNPD_DT WITH (NOLOCK) ON SHOP_MNPD_HD.DocNo=SHOP_MNPD_DT.DocNo 
                		                INNER JOIN SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) ON SHOP_MNPD_HD.INVOICEID=XXX_POSLINE.INVOICEID 
                		                    AND SHOP_MNPD_DT.ItemID = XXX_POSLINE.ITEMID 
                		                    AND SHOP_MNPD_DT.INVENTDIMID = XXX_POSLINE.INVENTDIMID 
                		        where   SHOP_MNPD_HD.INVOICEDATE between '{DateBegin}' and '{DateEnd}' 
                		                and StaDoc !='3' AND StaPrcDoc ='1' AND SHOP_MNPD_HD.INVOICEID is not null    AND DOCUTYPE='1' 
                                        AND isnull(COMMISSIONPD,'0') !='1'  
                		        group by SHOP_MNPD_HD.INVOICEDATE,SHOP_MNPD_HD.POSGROUP,CASHIERID,SHOP_MNPD_HD.INVOICEID 
                	        )PDSALE 
                	        GROUP BY INVOICEDATE, POSGROUP, CASHIERID, LINEAMOUNT 
                    union all     
                	    select INVOICEDATE, POSGROUP, CASHIERID,'0' as Docno, count(INVOICEID) as INVOICEID, LINEAMOUNT from 
                	    ( 
                		    select  SHOP_MNOG_HD.INVOICEDATE,SHOP_MNOG_HD.POSGROUP, 
                		            CASHIERID,'0' as Docno, SHOP_MNOG_HD.INVOICEID, 
                		            SUM(XXX_POSLINE.LINEAMOUNT) as LINEAMOUNT 
                		    from    SHOP_MNOG_HD   WITH (NOLOCK)  	INNER JOIN  SHOP_MNOG_DT  WITH (NOLOCK) ON SHOP_MNOG_HD.DocNo =SHOP_MNOG_DT.DocNo 
                		            INNER JOIN SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK) ON SHOP_MNOG_HD.INVOICEID=XXX_POSLINE.INVOICEID 
                		                AND SHOP_MNOG_DT.ITEMID=XXX_POSLINE.ITEMID 
                		                AND SHOP_MNOG_DT.INVENTDIMID=XXX_POSLINE.INVENTDIMID 
                		    where   SHOP_MNOG_HD.INVOICEDATE between '{DateBegin}' and '{DateEnd}' 
                		            and STAPRCDOC ='1'  AND SHOP_MNOG_HD.INVOICEID is not null 
                                    AND SHOP_MNOG_HD.invoicedate !=''  
                                    AND DOCUTYPE='1' 
                                    AND isnull(COMMISSIONOG,'0') !='1'   
                		            group by SHOP_MNOG_HD.INVOICEDATE,SHOP_MNOG_HD.POSGROUP,CASHIERID,SHOP_MNOG_HD.INVOICEID 
                	    )OGSALE 
                	    GROUP BY INVOICEDATE, POSGROUP, CASHIERID, LINEAMOUNT 
                    union all    
                		    select   DocDate as INVOICEDATE,Branch_ID as POSGROUP,WHOINS as CASHIERID,count(DocNo) as Docno,'0' as INVOICEID   ,'0' as LINEAMOUNT 
                		    from     SHOP_MNPD_HD    WITH (NOLOCK) 
                		    where    DocDate  between '{DateBegin}' and '{DateEnd}'   and StaDoc !='3' AND StaPrcDoc ='1' AND isnull(COMMISSIONPOSID,'0') !='1' 
                		    group by DocDate,Branch_ID,WHOINS 
                    union all     
                		    select   DOCDATE as INVOICEDATE,BRANCH_ID as POSGROUP, WHOINS as CASHIERID,count(DocNo) as Docno  ,'0' as INVOICEID  ,'0' as LINEAMOUNT 
                		    from      SHOP_MNOG_HD WITH (NOLOCK)
                		    where    CONVERT(VARCHAR,DOCDATE,23) between '{DateBegin}' and '{DateEnd}'   and StaPrcDoc ='1'  AND isnull(COMMISSIONPOSID,'0') !='1' 
                		    group by DOCDATE,BRANCH_ID,WHOINS 
                    )Sale 
                    inner join SHOP2013TMP .dbo.EMPLTABLE 	WITH (NOLOCK)   on Sale.CASHIERID=EMPLTABLE.EMPLID    
                    inner join Shop_Branch WITH (NOLOCK) on Sale.POSGROUP=Shop_Branch.BRANCH_ID     
                where Sale.CASHIERID !='' and Sale.CASHIERID is not null and DATAAREAID ='spc'  {SqlCondition}  
		        group by  INVOICEDATE,Sale.POSGROUP,Sale.CASHIERID 
                order by  INVOICEDATE,Sale.POSGROUP,Sale.CASHIERID  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค่าคอมของออเดอร์ลูกค้า เปรียบออเดอร์กับบิลขาย
        public static DataTable GetCommissionProcessSaveInvoiceid()
        {
            string sql = $@" 
                WITH ProcessOrdersAndSales AS   
                (  
  	                select ROW_NUMBER() OVER(PARTITION BY OrderCust.OrderDocno,SALE.ITEMBARCODE ORDER BY Sale.CREATEDDATETIME) as SEQ,* from   
  	                (  
  	                    SELECT  SHOP_MNOG_HD.DocNo as OrderDocno , SHOP_MNOG_HD.DOCDATE as OrderDocDate ,SHOP_MNOG_HD.BRANCH_ID  as OrderBranch, LINENUM  as OrderSeqno ,  
  	                            ITEMID  as OrderItemID , ITEMBARCODE  as OrderBarcode ,INVENTDIMID as OrderDimid ,UNITID  as OrderUnitID ,QTYORDER as OrderQty  
  	                    FROM    SHOP_MNOG_HD  WITH (NOLOCK) 
                                inner join SHOP_MNOG_DT  WITH (NOLOCK)  on SHOP_MNOG_HD.DocNo=SHOP_MNOG_DT.DocNo   
  	                    WHERE   CONVERT(VARCHAR,DOCDATE,23) between getdate()-45 and getdate()-1  AND INVOICEID='' AND ISNULL(COMMISSIONOG,0)!='1'
  	                    union all  
  	                    SELECT  SHOP_MNPD_HD.DocNo as OrderDocno ,SHOP_MNPD_HD.DocDate as OrderDocDate ,SHOP_MNPD_HD.BRANCH_ID as OrderBranch , 
                                SHOP_MNPD_DT.LINENUM as OrderSeqno ,   
  	                            SHOP_MNPD_DT.ItemID as OrderItemID ,SHOP_MNPD_DT.ITEMBARCODE as OrderBarcode,SHOP_MNPD_DT.INVENTDIMID as OrderDimid ,
                                SHOP_MNPD_DT.UnitID as OrderUnitID , QTYORDER as OrderQty  
  	                    FROM    SHOP_MNPD_HD  WITH (NOLOCK) 
                                inner join  SHOP_MNPD_DT  WITH (NOLOCK)  on SHOP_MNPD_HD.DocNo=SHOP_MNPD_DT.DocNo   
  	                    WHERE  SHOP_MNPD_HD.DocDate  between getdate()-45 and getdate()-1  AND INVOICEID='' AND ISNULL(COMMISSIONPD,0)!='1'
  	                )OrderCust  
  	                inner join   
  	                (  
  	                    SELECT	XXX_POSTABLE.CASHIERID , XXX_POSTABLE.CREATEDDATETIME AS CREATEDDATETIME,REMARKS,  
  	                            XXX_POSLINE.INVOICEID,XXX_POSLINE.ITEMBARCODE ,XXX_POSLINE.NAME ,  
  	                            sum(XXX_POSLINE.QTY) as QTY ,sum(XXX_POSLINE .LINEAMOUNT) as LINEAMOUNT ,XXX_POSLINE.SALESUNIT ,ITEMID ,INVENTDIMID,XXX_POSTABLE.POSGROUP   
  	                    FROM	shop2013tmp.dbo.XXX_POSTABLE WITH (NOLOCK)  
                                INNER JOIN shop2013tmp.dbo.XXX_POSLINE WITH (NOLOCK)   ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID   
  	                    WHERE	XXX_POSTABLE.DOCUTYPE = '1' AND XXX_POSLINE.DOCUTYPE = '1'   
  	                            AND (REMARKS LIKE '%OG%' or REMARKS LIKE '%PD%')  
  	                            AND XXX_POSTABLE.INVOICEDATE between getdate()-45 and getdate()-1   
  	                    group by  XXX_POSTABLE.CASHIERID ,XXX_POSTABLE.CREATEDDATETIME ,REMARKS,  
  	                            XXX_POSLINE.INVOICEID,XXX_POSLINE.ITEMBARCODE ,XXX_POSLINE.NAME , XXX_POSLINE.SALESUNIT ,ITEMID ,INVENTDIMID,XXX_POSTABLE.POSGROUP   
  	                )Sale  
  	                on OrderCust.OrderDocno=Sale.Remarks  
  	                and OrderCust .OrderItemID =sale.ITEMID   
  	                and OrderCust .OrderDimid =sale.INVENTDIMID    
                )  

                SELECT  Distinct  substring(OrderDocno,3,2) as DocType,OrderDocno,INVOICEID ,convert(varchar,CREATEDDATETIME ,23) as INVOICEDATE ,CASHIERID,POSGROUP  
                FROM    ProcessOrdersAndSales  
                WHERE   SEQ =1     
                order by convert(varchar,CREATEDDATETIME ,23) ,INVOICEID,OrderDocno ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //OK รายงาน ค้นหาการนับสต็อกล่าสุดรวมถึง สต็อกย้อนหลัง 1 วัน ใช้ Server Report
        public static DataTable MNCS_FindLastCount(string whID)
        {
            string sql = $@"
            SELECT	ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID,COUNTEDQTY,CONVERT(VARCHAR,COUNTDATE,23) AS COUNTDATE,AVAILPHYSICAL
            FROM	[PT_PURCHASE_LASTBYINVENTDIM_LASTSTOCK] WITH (NOLOCK)
            WHERE	DATAAREAID = N'SPC'
		            AND INVENTLOCATIONID = '{whID}' ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess);
        }

        //OK รายงาน ค้นหายอดขายย้อนหลัง ใช้ Server Report
        public static DataTable MNCS_FindQtySale(int countMonth, string whID)
        {
            int d = countMonth * 30;
            string sql = $@"
            SELECT	BRANCH,ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID,SUM(INVENTQTY) AS INVENTQTY 
            FROM	VIEW_PT_RETAIL	WITH (NOLOCK)   
            WHERE	BRANCH = '{whID}' 
		            AND TimePeriod BETWEEN GETDATE()-{d} AND GETDATE() 
		            AND INVENTSITEID = 'SPC'
            GROUP BY BRANCH,ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess);
        }

        //FindSale
        public static DataTable MNOR_FindSale(string pJobGroup, int dd)//string pBchID, string pItemID, string pDimID
        {
            string sqlX = $@"
                SELECT	POSGROUP,ITEMID,INVENTDIMID,
                        CAST(SUM(INVENTQTY) as decimal(10,2)) AS INVENQTY
                FROM	SHOP2013TMP.dbo.XXX_POSLINE wITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,INVOICEDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{dd},23) AND CONVERT(VARCHAR,GETDATE(),23)
		                AND DOCUTYPE = '1'
                        AND POSGROUP LIKE 'MN%'
		                AND ITEMID  IN (SELECT	ITEMID 
						                FROM	SHOP_ITEMGROUPBARCODE WITH (NOLOCK) 
								                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON  SHOP_ITEMGROUPBARCODE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
						                WHERE	ITEMGROUPID LIKE '{pJobGroup}%' AND ITEMSTATUS1 = '1'  
						                GROUP BY ITEMID)
                GROUP BY POSGROUP,ITEMID,INVENTDIMID ";
            return ConnectionClass.SelectSQL_Main(sqlX);

        }
        //find Sale By TranferDelivery
        public static DataTable TranferDelivery_ItemLastRecive(string pCase, string _pTypeID, string date)//0 = ค้นหาการขายทั้งหมด ตามบาร์โค้ดที่ Comfig ไว้ 1 = ค้นหาการขายตามบาร์โค้ดที่ต้องการ
        {
            string strSale;
            if (pCase == "0")
            {
                strSale = $@"
                    SELECT	ITEMBARCODE AS SHOW_ID,POSGROUP AS BRANCH_ID,SUM(QTY) AS 	QTYALLSALE
                    FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)
                    WHERE	ITEMBARCODE IN (
                    SELECT SHOW_ID
                    FROM  SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
                    WHERE TYPE_CONFIG = '{_pTypeID}' AND STA = '1' 
                    )
		                    AND INVOICEDATE = '{date}' AND POSGROUP LIKE 'MN%' AND SIGN = '1'         
                    GROUP BY    ITEMBARCODE, POSGROUP
            ";
            }
            else
            {
                strSale = $@"
                    SELECT	POSGROUP AS BRANCH_ID,SUM(QTY) AS 	QTYALLSALE
                    FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)
                    WHERE	ITEMBARCODE IN ( {_pTypeID})
		                    AND INVOICEDATE = '{date}' AND POSGROUP LIKE 'MN%' AND SIGN = '1'         
                    GROUP BY     POSGROUP
            ";
            }
            return ConnectionClass.SelectSQL_Main(strSale);
        }
        //การส่งซองเงิน
        public static DataTable XXX_POSDRAWER()
        {
            string bchID = @"'" + SystemClass.SystemBranchID + @"'";
            string bchYa = "";
            switch (SystemClass.SystemBranchID)
            {
                case "MN004":
                    bchID = "'MN098','" + SystemClass.SystemBranchID + @"'";
                    break;
                case "MN030":
                    bchYa = " 'MYA30' ";
                    break;
                case "MN038":
                    bchYa = " 'MYA38' ";
                    break;
                case "MN039":
                    bchID = "'MN998','" + SystemClass.SystemBranchID + @"'";
                    break;
                case "MN046":
                    bchYa = " 'MYA' ";
                    break;
                default:
                    break;
            }

            string sqlMYA = "";
            if (bchYa != "")
            {
                sqlMYA = string.Format(@" UNION  
                SELECT	XXX_POSDRAWERCYCLETABLE.DRAWERID,CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.TRANSDATE,105) AS TRANSDATE ,CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25) as CREATEDDATETIME, 
		                XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK,
                        FORMAT(CONVERT(float,ISNULL(SUM(CASHCOUNT),'0')), 'N') as  LINEAMOUNT ,
		                POSGROUP,CASE POSGROUP WHEN 'MYA' THEN 'ห้องยา ถนนวิเศษ' WHEN 'MYA30' THEN 'ห้องยา นาใน' WHEN 'MYA38' THEN 'ห้องยา ท่าเรือ' END AS NAMETYPE,
		                DATEDIFF(DAY,CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.TRANSDATE,23),GETDATE()) AS DATEDIFF_TODAY    ,'0' AS SlipID ,'0' AS STACANCLE
                        ,'0' AS TYPE_STA
                FROM	XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
		                LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
                WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPM'    
                        AND POSGROUP IN ( " + bchYa + @" )
		                AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-10,23) and Convert(varchar,getdate(),23)   
		                AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'   
                Group by XXX_POSDRAWERCYCLETABLE.DRAWERID,XXX_POSDRAWERCYCLETABLE.TRANSDATE,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,  
                        XXX_POSDRAWERCYCLETABLE.EMPLID,XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK,
                        POSGROUP,CASE POSGROUP WHEN 'MYA' THEN 'ห้องยา ถนนวิเศษ' WHEN 'MYA30' THEN 'ห้องยา นาใน' WHEN 'MYA38' THEN 'ห้องยา ท่าเรือ' END
            ");
            }

            string sqlAll = string.Format(@"
                SELECT	XXX_POSDRAWERCYCLETABLE.DRAWERID,CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.TRANSDATE,105) AS TRANSDATE ,
		                CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25) as CREATEDDATETIME, 
		                XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK, 
                        FORMAT(CONVERT(float,ISNULL(SUM(CASHCOUNT),'0')), 'N') as  LINEAMOUNT ,
		                POSGROUP,BRANCH_NAME AS NAMETYPE,
		                DATEDIFF(DAY,CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.TRANSDATE,23),GETDATE()) AS DATEDIFF_TODAY  ,'0' AS SlipID,'0' AS STACANCLE
                        ,'0' AS TYPE_STA
                FROM	SHOP2013TMP.dbo.XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
			                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.POSGROUP = SHOP_BRANCH.BRANCH_ID
			                LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
                WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPC'    
			                AND XXX_POSDRAWERCYCLETABLE.POSGROUP IN (" + bchID + @")
			                AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)   
			                AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'   
                Group by XXX_POSDRAWERCYCLETABLE.DRAWERID,XXX_POSDRAWERCYCLETABLE.TRANSDATE,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,  
			                XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK,POSGROUP,BRANCH_NAME

                UNION

                SELECT	DRAWERID,CONVERT(VARCHAR,TRANSDATE,105) AS TRANSDATE,CONVERT(VARCHAR,TRANSDATETIME,25) AS CREATEDDATETIME,
		                EMPLID,EmplName AS NAME,REMARK,FORMAT(CONVERT(float,ISNULL(LINEAMOUNT,'0')), 'N') AS LINEAMOUNT, BranchID AS POSGROUP,
		                --CASE WHEN EmplNUM LIKE 'MN%' THEN 'สลิปโอนยอดบัตรสวัสดิการ EMV' ELSE  'ซองเงินบัญชีฝากส่ง'   END  AS NAMETYPE ,
                        TYPE_DESC AS NAMETYPE ,
		                DATEDIFF(DAY,CONVERT(VARCHAR,TRANSDATE,23),GETDATE()) AS DATEDIFF_TODAY ,
                        CASE WHEN EmplNUM LIKE 'MN%' THEN '1' ELSE  '0'   END  AS SlipID,'1' AS STACANCLE
                        ,IIF(TYPE_STA IN ('4','5','6'),'1','0') AS TYPE_STA
                FROM	SHOP_MNSM WITH (NOLOCk)  
                WHERE	SendSta = 0 AND BranchID IN (" + bchID + @")

                UNION

                SELECT	MNECDocno AS DRAWERID,CONVERT(VARCHAR,MNECDateDoc,105) AS TRANSDATE,CONVERT(VARCHAR,MNECDateDoc,25) AS CREATEDDATETIME,
		                MNECWhoIns AS EMPLID,MNECWhoNameIns AS NAME,MNECRemark AS REMARK,FORMAT(CONVERT(float,ISNULL(MNECSum,'0')), 'N') AS LINEAMOUNT, MNECBranch AS POSGROUP,
                        'ซองเงินแลกเหรียญ' AS NAMETYPE ,
		                DATEDIFF(DAY,CONVERT(VARCHAR,MNECDateDoc,23),GETDATE()) AS DATEDIFF_TODAY ,
                        '1'  AS SlipID,'0' AS STACANCLE
                        ,'0' AS TYPE_STA
                FROM	SHOP_MNEC_HD WITH (NOLOCk)  
                WHERE	MNECSandSta = 0 AND MNECStaApv IN ('1','2','3') AND MNECBranch IN (" + bchID + @")
                
            ");

            sqlAll += sqlMYA + " ORDER BY CREATEDDATETIME ";

            return ConnectionClass.SelectSQL_Main(sqlAll);
        }

        public static DataTable Promotion_DataAll(string pBchID, string date1, string date2)
        {
            string pCon1 = "", pCon2 = "";
            if (pBchID != "")
            {
                pCon1 = $@" AND INVENTLOCATIONID = '{pBchID}' ";
                pCon2 = $@" AND XXX_POSLINE.POSGROUP  = '{pBchID}'   ";
            }

            string sql = $@"
                    SELECT	TMP.INVENTLOCATIONID,BRANCH_NAME,CONVERT(VARCHAR,INVOICEDATE,23) AS INVOICEDATE1 ,INVOICEID,
		                    BARCODE.ITEMBARCODE, BARCODE.SPC_ITEMNAME, '1' AS QTY,'' AS SALESUNIT,'' AS NAMEALIAS, 
                            TMP.CASHIERID + ' ' + TMP.SPC_NAME AS SPC_NAME,TMP.REAMARK ,'0' AS STAFREE,CONVERT(VARCHAR,DateSale,25) AS DateSale 
                    FROM	(SELECT POSGROUP AS INVENTLOCATIONID,BRANCH_NAME ,  
		                    SUBSTRING(DESCRIPTION, 1, CHARINDEX(';', DESCRIPTION, 0) - 1) AS ITEMBARCODE, dbo.split(DESCRIPTION,';',3) AS REAMARK, 
		                    CASHIERID, EMPLTABLE.SPC_NAME, INVOICEID,CONVERT(varchar,CONVERT(DATE,SYSUSERLOG.CREATEDDATETIME),23)   AS INVOICEDATE,CONVERT(VARCHAR,SYSUSERLOG.CREATEDDATETIME,25) AS DateSale 
		                    FROM SYSUSERLOG WITH (NOLOCK) INNER JOIN EMPLTABLE WITH (NOLOCK) ON SYSUSERLOG.CASHIERID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'   
		                    INNER JOIN Shop_Branch WITH (NOLOCK) ON SYSUSERLOG.POSGROUP = Shop_Branch .BRANCH_ID  
                    WHERE        (KBMODIFIERS = N'None')  AND (KEYNAME = N'PageUp') AND (CONVERT(DATE,SYSUSERLOG.CREATEDDATETIME) 
		                    BETWEEN '{date1}' AND '{date2}') 
		                    GROUP BY POSGROUP, SUBSTRING(DESCRIPTION, 1, CHARINDEX(';', DESCRIPTION, 0) - 1), dbo.split(DESCRIPTION,';',3), CASHIERID, 
		                    EMPLTABLE.SPC_NAME, INVOICEID, BRANCH_NAME,SYSUSERLOG.CREATEDDATETIME,(CONVERT(DATE,SYSUSERLOG.CREATEDDATETIME)) ) AS TMP  
		                    INNER JOIN INVENTITEMBARCODE AS BARCODE ON BARCODE.ITEMBARCODE = TMP.ITEMBARCODE WHERE BARCODE.DATAAREAID = 'SPC'   
                            {pCon1}
                            AND INVENTLOCATIONID != 'RT'  

                    UNION 

                    SELECT	XXX_POSLINE.POSGROUP AS INVENTLOCATIONID,BRANCH_NAME,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE ,XXX_POSTABLE.INVOICEID, 
		                    XXX_POSLINE.ITEMBARCODE,XXX_POSLINE.NAME AS SPC_ITEMNAME,XXX_POSLINE.QTY,XXX_POSLINE.SALESUNIT,XXX_POSLINE.NAMEALIAS, 
		                    CASHIERID + ' ' + SPC_NAME AS SPC_NAME,'' AS REAMARK,'1' AS STAFREE,CONVERT(VARCHAR,XXX_POSLINE.CREATEDDATETIME,25) as DateSale  
                    FROM	XXX_POSTABLE WITH (NOLOCK)  
		                    INNER JOIN XXX_POSLINE  WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID  
		                    INNER JOIN EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID 
		                    INNER JOIN Shop_Branch WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = Shop_Branch.BRANCH_ID  
                    WHERE	SUPPITEMGROUPID = '/FREEITEM/' 
		                    AND XXX_POSTABLE.INVOICEDATE BETWEEN '{date1}' AND '{date2}'
		                    AND XXX_POSLINE.INVOICEDATE BETWEEN '{date1}' AND '{date2}'
		                    AND XXX_POSLINE.POSGROUP LIKE 'MN%' 
		                    AND XXX_POSLINE.DOCUTYPE = '1' AND XXX_POSLINE.SIGN = '1' 
		                    AND EMPLTABLE.DATAAREAID = N'SPC'  
                            {pCon2}
                            AND XXX_POSLINE.POSGROUP  != 'RT' 

                    ORDER BY   INVENTLOCATIONID,ITEMBARCODE,DateSale

                    ";
            return ConnectionClass.SelectSQL_POSRetail707(sql);
        }

        public static DataTable Promotion_DataHD()
        {
            string pCon = "";
            if (SystemClass.SystemBranchID != "MN000")
            {
                pCon = " AND	SPC_RetailPromotionLocation.INVENTLOCATIONID  = '" + SystemClass.SystemBranchID + @"' ";
            }
            string strSql = string.Format(@"
            SELECT	SPC_RetailPromotionTable.PromId,PROMNAME ,PROMNAMEONRECEIPT ,BLOCKED ,ItemBuyerGroupId,
		            ItemBuyerGroupId + '-' + CASE WHEN ItemBuyerGroupId = '' THEN '' ELSE Description END AS Description ,AMOUNT ,QTY,
		            CONVERT(VARCHAR,LIFEFROM,23) AS LIFEFROM ,CONVERT(VARCHAR,LIFETO,23) AS LIFETO ,BARCODEFREE ,NAMEONRECEIPT ,QTYFREE ,QTYHIGHEST,BLOCKED  
            FROM	SPC_RetailPromotionTable with (Nolock) 
		            INNER JOIN SPC_RetailPromotionLine with (Nolock) ON SPC_RetailPromotionTable.PromId = SPC_RetailPromotionLine.PromId 
		            INNER JOIN SPC_RetailPromotionLocation with (Nolock) ON SPC_RetailPromotionTable.PromId = SPC_RetailPromotionLocation.PromId 
		            INNER JOIN InventLocation WITH (NOLOCK) ON SPC_RetailPromotionLocation.INVENTLOCATIONID =  InventLocation.INVENTLOCATIONID 
		            LEFT OUTER JOIN InventBuyerGroup WITH (NOLOCK) ON SPC_RetailPromotionTable.ItemBuyerGroupId = InventBuyerGroup.GROUP_  
                        AND InventBuyerGroup.DATAAREAID = 'SPC' 
            WHERE	 LIFETO >= GETDATE() AND SPC_RetailPromotionLocation.DATAAREAID = 'SPC' AND SPC_RetailPromotionLine.DATAAREAID = 'SPC'  
		            AND SPC_RetailPromotionTable.DATAAREAID = 'SPC'  AND InventLocation.DATAAREAID = 'SPC'    
		            AND  BLOCKED = '0'  AND	SPC_RetailPromotionLocation.INVENTLOCATIONID  != 'RETAILAREA' " + pCon + @"
            GROUP BY SPC_RetailPromotionTable.PromId,PROMNAME ,PROMNAMEONRECEIPT ,BLOCKED ,ItemBuyerGroupId,
		            CASE WHEN ItemBuyerGroupId = '' THEN '' ELSE Description END   ,
		            AMOUNT ,QTY,LIFEFROM ,LIFETO ,BARCODEFREE ,NAMEONRECEIPT ,QTYFREE ,QTYHIGHEST   
            ORDER BY LIFEFROM  
            ");
            return ConnectionClass.SelectSQL_MainAX(strSql);
        }

        public static DataTable Promotion_DataDT(string pCase, string pProID)
        {
            string sql;
            if (pCase == "0")
            {
                sql = string.Format(@"
                SELECT	SPC_RetailPromotionLocation.INVENTLOCATIONID,NAME ,USECONFIRM 
                FROM	SPC_RetailPromotionLocation with (Nolock) 
		                INNER JOIN InventLocation WITH (NOLOCK) ON SPC_RetailPromotionLocation.INVENTLOCATIONID =  InventLocation.INVENTLOCATIONID  
                WHERE	PromId = '" + pProID + @"' 
                ORDER BY INVENTLOCATIONID ");
            }
            else
            {
                sql = string.Format(@"
                SELECT	LINENUM ,SPC_RetailPromotionLine.ItemId,SPC_RetailPromotionLine.INVENTDIMID ,INVENTITEMBARCODE.SPC_ITEMNAME ,SPC_RetailPromotionLine.UNITID 
                FROM	SPC_RetailPromotionLine with (Nolock)	 
		                INNER JOIN INVENTITEMBARCODE WITH (NOLOCK) ON  SPC_RetailPromotionLine.ItemId = INVENTITEMBARCODE.ITEMID 
		                AND SPC_RetailPromotionLine.INVENTDIMID = INVENTITEMBARCODE.INVENTDIMID 
		                AND SPC_RetailPromotionLine.UNITID = INVENTITEMBARCODE.UNITID  
                WHERE	PromId = '" + pProID + @"' AND SPC_RetailPromotionLine.DATAAREAID = 'SPC' AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                UNION 
                SELECT	LINENUM ,SPC_RetailPromotionLine.ItemId,INVENTDIMID AS INVENTDIMID ,ITEMNAME ,SPC_RetailPromotionLine.UNITID  
                FROM	SPC_RetailPromotionLine with (Nolock)	 
		                INNER JOIN INVENTTABLE  WITH (NOLOCK) ON  SPC_RetailPromotionLine.ItemId = INVENTTABLE.ITEMID 
                WHERE	PromId = '" + pProID + @"' AND SPC_RetailPromotionLine.DATAAREAID = 'SPC' AND INVENTTABLE.DATAAREAID = 'SPC' 
                ORDER BY LINENUM ");
            }
            return ConnectionClass.SelectSQL_MainAX(sql);
        }

        //ยอดขายตามบาร์โค้ด/สาขา/วัน
        public static DataTable MRT_GetSaleXXXDate(string pItemID, int Day)
        {
            string sql = $@"
                SELECT	POSGROUP,ITEMID,INVENTDIMID,
                        CONVERT(VARCHAR,INVOICEDATE,23) AS DATE,
                        CAST(SUM(INVENTQTY) AS decimal(10,2)) AS QTY
                FROM	SHOP2013TMP.dbo.XXX_POSLINE wITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,INVOICEDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE(),23)
		                AND DOCUTYPE = '1'
                        AND POSGROUP LIKE 'MN%'
		                AND ITEMID IN ({pItemID})
                GROUP BY POSGROUP,ITEMID,INVENTDIMID,CONVERT(VARCHAR,INVOICEDATE,23)";
            //return ConnectionClass.SelectSQL_POSRetail707(sql);
            //return ConnectionClass.SelectSQL_Main(sql);
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSelectMain, 300);

        }
        //ยอดขายวันปัจจุบัน
        public static DataTable MRT_GetSaleXXX(string pCase_0Barcode_1ItemID, string pAvg_0Avg_1Not, string pBarcode_ItemID, int Day, int DateToday = 0)//Day ย้อนหลังกี่วัน  pCase 0 barcode 1 Dim -- DateToday คือ รวมวันนี้มั้ย ถ้ารวมใส่ 0 ไม่รวมใส่1
        {
            string sqlX;//= "   SELECT	'' AS POSGROUP,'' AS ITEMID,'' AS INVENTDIMID,'' AS ITEMBARCODE,0 AS QTY,  0 AS INVENQTY,0 AS INVENQTYAVG  "; ;
            int dayDiff = Day; if (Day == 0) dayDiff = 1;

            if (pCase_0Barcode_1ItemID == "0")
            {
                string fName = $@" (SUM(QTY)/{dayDiff})  ";
                if (pAvg_0Avg_1Not == "1") fName = " SUM(QTY) ";

                sqlX = $@"
                SELECT	POSGROUP,XXX_POSLINE.ITEMBARCODE,{fName} AS QTY,
                        CAST(SUM(QTY) as decimal(10,2)) AS QTY1,
                        CAST((SUM(QTY)/{dayDiff}) as decimal(10,2)) AS QTYAVG1
                FROM	SHOP2013TMP.dbo.XXX_POSLINE wITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,INVOICEDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE()-{DateToday},23)
                  AND DOCUTYPE = '1'
                        AND POSGROUP LIKE 'MN%'
                  AND ITEMBARCODE IN ( {pBarcode_ItemID} )
                GROUP BY POSGROUP,XXX_POSLINE.ITEMBARCODE
            ";

            }
            else
            {
                string fName = $@" (SUM(INVENTQTY)/{dayDiff})  ";
                if (pAvg_0Avg_1Not == "1") fName = " SUM(INVENTQTY)  ";

                sqlX = $@"
                SELECT	POSGROUP,ITEMID,INVENTDIMID,{fName} AS QTY,
                        CAST(SUM(INVENTQTY) as decimal(10,2)) AS INVENQTY,
                        CAST((SUM(INVENTQTY))/{dayDiff} as decimal(10,2)) AS INVENQTYAVG
                FROM	SHOP2013TMP.dbo.XXX_POSLINE wITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,INVOICEDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE()-{DateToday},23)
                  AND DOCUTYPE = '1'
                        AND POSGROUP LIKE 'MN%'
                  AND ITEMID IN ( {pBarcode_ItemID} )
                GROUP BY POSGROUP,ITEMID,INVENTDIMID
                ";
            }

            //return ConnectionClass.SelectSQL_POSRetail707(sqlX);
            return ConnectionClass.SelectSQL_SentServer(sqlX, IpServerConnectClass.ConSelectMain, 120);
        }
    }
}
