﻿using PC_Shop24Hrs.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_Shop24Hrs.Class.AR
{
    class ARClass2012 : IARInterface
    {
        DataTable IARInterface.GetDRAWERNotSend_ByCondition(string pCase0Bch1Route)
        {
            string typeSql_1 = "", typeSql_2 = "";

            if (pCase0Bch1Route == "0")
            {
                typeSql_1 = " POSGROUP AS BCH,POSGROUP + CHAR(10) + BRANCH_NAME AS CASESELECT , ";
                typeSql_2 = " GROUP BY POSGROUP,BRANCH_NAME  ORDER BY POSGROUP,BRANCH_NAME ";
            }
            if (pCase0Bch1Route == "1")
            {
                typeSql_1 = " ROUTEID AS BCH,ROUTEID + CHAR(10) + ROUTENAME AS CASESELECT, ";
                typeSql_2 = string.Format(@"LEFT OUTER JOIN  
                     (
                     SELECT	ACCOUNTNUM,SPC_ROUTINGPOLICY.ROUTEID,NAME  AS ROUTENAME  
                     FROM	SHOP2013TMP.dbo.SPC_ROUTINGPOLICY  WITH (NOLOCK)  
		                    INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK)   ON SPC_ROUTINGPOLICY.ROUTEID = SPC_ROUTETABLE.ROUTEID   
                     WHERE	SPC_ROUTINGPOLICY.DATAAREAID = 'SPC' AND SPC_ROUTETABLE.DATAAREAID = 'SPC' AND SPC_ROUTINGPOLICY.ROUTEID LIKE '9%' 
                     )TMPROUTE ON TMP_YA.POSGROUP = TMPROUTE.ACCOUNTNUM  
                     GROUP BY ROUTEID,ROUTENAME  ORDER BY ROUTEID");
            }

            string sqlSelect = $@"
             SELECT	{typeSql_1} COUNT(POSGROUP) AS Count_Pack,SUM(LINEAMOUNT) AS LINEAMOUNT,SUM(THEN25) AS THEN25,SUM(THEN40) AS THEN40 FROM	( 

             SELECT CASE POSGROUP WHEN 'MN098' THEN 'MN004' WHEN 'MN998' THEN 'MN039' ELSE POSGROUP END AS POSGROUP,
                    CASE POSGROUP WHEN 'MN098' THEN 'นาคา' WHEN 'MN998' THEN 'เชิงทะเล' ELSE BRANCH_NAME END AS BRANCH_NAME,
                    XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT,   
		            CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) > 40 THEN 1 ELSE 0 END AS THEN40, 
		            CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) BETWEEN 25 AND 40 THEN 1 ELSE 0 END AS THEN25  
             FROM	SHOP2013TMP.dbo.XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
		            INNER JOIN  SHOP_BRANCH WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.POSGROUP = SHOP_BRANCH.BRANCH_ID  
		            LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
             WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPC'     
                    AND POSGROUP NOT IN ('RT','MN997')  
                    AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)   
                    AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'  
 
             UNION  
 
             SELECT	 BranchID AS POSGROUP, BRANCHNAME,DRAWERID  ,LINEAMOUNT   
		            ,CASE WHEN DATEDIFF(hh,TRANSDATETIME,GETDATE()) > 40 THEN 1 ELSE 0 END AS THEN40, 
		            CASE WHEN DATEDIFF(hh,TRANSDATETIME,GETDATE()) BETWEEN 25 AND 40 THEN 1 ELSE 0 END AS THEN25  
             FROM	SHOP_MNSM WITH (NOLOCk)     WHERE	SendSta = 0   

             UNION 
 
             SELECT	CASE POSGROUP WHEN 'MYA' THEN 'MN046' WHEN 'MYA30' THEN 'MN030' WHEN 'MYA38' THEN 'MN038' END AS POSGROUP,
		            CASE POSGROUP WHEN 'MYA' THEN 'ร้านยา ถนนวิเศษ'  WHEN 'MYA30' THEN 'ร้านยา นาใน' WHEN 'MYA38' THEN  'ร้านยา ท่าเรือ' END AS BRANCH_NAME,
		            XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT   
		            ,CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) > 40 THEN 1 ELSE 0 END AS THEN40, 
		            CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) BETWEEN 25 AND 40 THEN 1 ELSE 0 END AS THEN25  
             from	XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
		            LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID   
             WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPM'    
		            AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)    
		            AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'   

             )TMP_YA    {typeSql_2} ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }

        DataTable IARInterface.GetDetailDRAWERNotSend_ByCondition(string conditionWhere)
        {
            string sql = $@"
                    SELECT	TMP_YA.*,TMPROUTE.*,ROUTEID+char(10)+NAME  AS ROUTENAME_Desc,EMPLID + char(10) + NAME AS CH_NAME  
                    FROM	( 
                         SELECT CASE POSGROUP WHEN 'MN098' THEN 'MN004' WHEN 'MN998' THEN 'MN039' ELSE POSGROUP END AS POSGROUP,
		                        CASE POSGROUP WHEN 'MN098' THEN 'นาคา' WHEN 'MN998' THEN 'เชิงทะเล' ELSE BRANCH_NAME END AS BRANCH_NAME, 
                                CASE POSGROUP WHEN 'MN098' THEN 'MN004' WHEN 'MN998' THEN 'MN039' ELSE POSGROUP END + CHAR(10) + 
                                BRANCH_NAME AS BCH_NAME,
		                        XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT,
		                        CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25) as CREATEDDATETIME,
		                        XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK   
		                        ,DATEDIFF(hour,GETDATE(),CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25))*-1  AS DIFF_A

                         FROM	SHOP2013TMP.dbo.XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
		                        INNER JOIN  SHOP_BRANCH WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.POSGROUP = SHOP_BRANCH.BRANCH_ID  
		                        LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
                         WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPC'     
		                         AND POSGROUP NOT IN ('RT')  
		                         AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)   
		                         AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'  
                         UNION  
 
                         SELECT	BranchID AS POSGROUP, BRANCHNAME,
                                BranchID + CHAR(10) + BRANCHNAME AS BCH_NAME,
                                DRAWERID  ,LINEAMOUNT   ,
		                        CONVERT(VARCHAR,TRANSDATETIME,25) as CREATEDDATETIME,
		                        EMPLID,EmplName AS NAME, REMARK,DATEDIFF(hour,GETDATE(),CONVERT(VARCHAR,TRANSDATETIME,25))*-1  AS DIFF_A
                         FROM	SHOP_MNSM WITH (NOLOCk)     WHERE	SendSta = 0   
 
                         UNION 
 
                         SELECT	CASE POSGROUP WHEN 'MYA' THEN 'MN046' WHEN 'MYA30' THEN 'MN030' WHEN 'MYA38' THEN 'MN038' END AS POSGROUP,
		                        CASE POSGROUP WHEN 'MYA' THEN 'ร้านยา ถนนวิเศษ'  WHEN 'MYA30' THEN 'ร้านยา นาใน' WHEN 'MYA38' THEN  'ร้านยา ท่าเรือ' END AS BRANCH_NAME,
                                CASE POSGROUP WHEN 'MYA' THEN 'MN046' WHEN 'MYA30' THEN 'MN030' WHEN 'MYA38' THEN 'MN038' END + CHAR(10) + CASE POSGROUP WHEN 'MYA' THEN 'ร้านยา ถนนวิเศษ'  WHEN 'MYA30' THEN 'ร้านยา นาใน' WHEN 'MYA38' THEN  'ร้านยา ท่าเรือ' END  AS BCH_NAME,
		                        XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT  , 
		                        CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25) as CREATEDDATETIME,
		                        XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK 
		                        ,DATEDIFF(hour,GETDATE(),CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25))*-1  AS DIFF_A
		 
                         FROM	XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
		                        LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID   
                         WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPM'    
		                        AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)    
		                        AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'   
                         )TMP_YA  
 
                    LEFT OUTER JOIN  
                    (
 
                    SELECT	ACCOUNTNUM,SPC_ROUTINGPOLICY.ROUTEID,NAME  AS ROUTENAME  
                    FROM	SHOP2013TMP.dbo.SPC_ROUTINGPOLICY  WITH (NOLOCK)  
		                    INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK)   ON SPC_ROUTINGPOLICY.ROUTEID = SPC_ROUTETABLE.ROUTEID   
                    WHERE	SPC_ROUTINGPOLICY.DATAAREAID = 'SPC' AND SPC_ROUTETABLE.DATAAREAID = 'SPC' AND SPC_ROUTINGPOLICY.ROUTEID LIKE '9%' AND ISPRIMARY= '1'
                    )TMPROUTE ON TMP_YA.POSGROUP = TMPROUTE.ACCOUNTNUM  
 
                   {conditionWhere}

                    ORDER BY POSGROUP ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //AX2012OK
        DataTable IARInterface.ReportSendMoneyByLO(string date1, string date2)
        {
            string sql = $@"
                SELECT		MNPMLO,  CONVERT(VARCHAR,MNPMDate,23) AS MNPMDate, MNPMTime,  
			                MNPMUserCar,SPC_NAME,MNPMCar, MNPMBranch, BRANCH_NAME, 
			                MNPMSum, MNPMCountPack  
                FROM	    SHOP_MNPM_HD WITH (NOLOCK)   
			                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMBranch = SHOP_BRANCH.BRANCH_ID  
			                LEFT OUTER JOIN AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE  WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCar = EMPLTABLE.ALTNUM  
                WHERE		CONVERT(VARCHAR,MNPMDate,23) BETWEEN '{date1}'  AND '{date2}'
			                AND EMPLTABLE.DATAAREAID  = N'SPC'
                ORDER BY 	MNPMLO,MNPMTIME
 ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IARInterface.ReportReciveMoney(string date1, string date2)
        {
            string sql = $@"
                SELECT	RoundTrans,MNPMBranch,BRANCH_NAME,ISNULL(CONVERT(VARCHAR,TRANSFERDATE,23),'') AS TRANSFERDATE,SPC_LOGISTICTABLE.LOGISTICID,VEHICLEID,  
		                SPC_LOGISTICTABLE.EMPLDRIVER,EMPLTABLE.SPC_NAME,TRASHREMARK,MNPMDRAWERID, MNPMGranchDRAWERID  ,  
		                StaTrans,CASE StaTrans WHEN '0' THEN 'ไม่ได้รับ' ELSE 'รับแล้ว' END AS StaTransDESC,
		                ISNULL(CONVERT(VARCHAR,DateTrans,23),'')+' '+TimeTrans  AS DateTrans,
		                ISNULL(WhoTrans,'')+'-'+ISNULL(TMPTRANS.SPC_NAME,'') AS WhoTrans,ISNULL(RemarkTrans,'') AS RemarkTrans,
		                StaAcc,CASE StaAcc WHEN '0' THEN 'ไม่ได้รับ' ELSE 'รับแล้ว' END AS StaAccDESC,
		                ISNULL(CONVERT(VARCHAR,DateAcc,23),'') + ' ' + TimeAcc AS DateAcc ,ISNULL(WhoAcc,'')+ '-' +ISNULL(TMPACC.SPC_NAME,'') AS WhoAcc,ISNULL(RemarkAcc,'') AS RemarkAcc 
                FROM	SHOP_MNPM_HD WITH (NOLOCK)  
		                INNER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMDocno = SHOP_MNPM_DT.MNPMDocno  
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_LOGISTICTABLE WITH (NOLOCK)   ON SHOP_MNPM_HD.MNPMLO = SPC_LOGISTICTABLE.LOGISTICID AND SPC_LOGISTICTABLE.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_LOGISTICTABLE.EMPLDRIVER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMBranch = SHOP_BRANCH.BRANCH_ID  
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE TMPACC WITH (NOLOCK)	 ON SHOP_MNPM_DT.WhoAcc = TMPACC.EMPLID AND TMPACC.DATAAREAID = N'SPC'   
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE TMPTRANS WITH (NOLOCK)  ON SHOP_MNPM_DT.WhoTrans = TMPTRANS.EMPLID AND TMPTRANS.DATAAREAID = N'SPC'   
                WHERE	MNPMDate  BETWEEN '{date1}'  AND '{date2}'
                ORDER BY MNPMBranch,TRANSFERDATE,MNPMDRAWERID  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //AX2012OK
        DataTable IARInterface.ReportSendMoneyByAR(string date1, string date2)
        {
            string sql = $@"
                SELECT	BranchID,BranchName,SHOP_MNSM.DRAWERID,CONVERT(VARCHAR,TRANSDATE,23) + ' ' + TRANSTIME AS TRANSDATE,   
                        SHOP_MNSM.EmplID+'-'+EmplName AS EmplName,EmplDepart,LINEAMOUNT,Remark,WHOINS+'-'+SPC_NAME AS SPC_NAME,
		                ISNULL(StaAcc,0) AS SendAcc,CASE ISNULL(StaAcc,0) WHEN '0' THEN 'ไม่ได้รับ' ELSE 'รับแล้ว' END AS SendAccDESC,
		                ISNULL(MNPMLO,'') AS MNPMLO,ISNULL(MNPMUserCar,'') AS MNPMUserCar,ISNULL(MNPMCar,'') AS MNPMCar,
		                ISNULL(SendSta,0) AS SendSta,CASE ISNULL(SendSta,0) WHEN '0' THEN 'ไม่ได้ส่ง' ELSE 'ส่งแล้ว' END AS SendStaDESC
                FROM	SHOP_MNSM WITH (NOLOCK)  
		                INNER JOIN AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH (NOLOCK) ON SHOP_MNSM.WHOINS = EMPLTABLE.ALTNUM  
		                LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON SHOP_MNSM.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
		                LEFT OUTER JOIN SHOP_MNPM_HD WITH (NOLOCK) ON SHOP_MNPM_DT.MNPMDocno = SHOP_MNPM_HD.MNPMDocno  
                WHERE	TRANSDATE   BETWEEN '{date1}'  AND '{date2}'
                        AND EmplNUM NOT LIKE 'MN%' 
		                AND EMPLTABLE.DATAAREAID = N'SPC'
                Order by SHOP_MNPM_HD.MNPMDocno  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IARInterface.ReportDetailMoney(string date1, string date2)
        {
            string bch = "";
            if (SystemClass.SystemBranchID != "MN000") bch = $@" AND MNPMBranch = '{SystemClass.SystemBranchID}' ";

            string sql = $@"
               SELECT	SHOP_MNPM_HD.MNPMDocno,MNPMBranch,Branch_NAME,CONVERT(VARCHAR,MNPMDate,23)+ ' ' + MNPMTime as MNPMDate,MNPMStaAcc, 
                        MNPMSendCount,MNPMCar,MNPMSum,MNPMCountPack,SHOP_MNPM_HD.MNPMRemark,  
                        SHOP_MNPM_HD.MNPMWhoIn + '-' + A.SPC_NAME As NAMESEND,CONVERT(VARCHAR,MNPMDateIn,23)+' ' +MNPMTimeIn as MNPMDateIn,  
                        MNPMUserCar+'-'+EMPLTABLE.SPC_NAME as UserCar ,MNPMDRAWERID,MNPMUserDRAWERID+'-'+MNPMNameDRAWERID AS MNPMNameDRAWERID,MNPMGranchDRAWERID,MNPMLO  
                FROM	SHOP_MNPM_HD WITH (NOLOCK)  
		                INNER JOIN Shop_Branch WITH (NOLOCK)  ON SHOP_MNPM_HD.MNPMBranch = Shop_Branch.Branch_ID  
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK)  ON SHOP_MNPM_HD.MNPMUserCar = EMPLTABLE.EMPLID  
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE A WITH (NOLOCK)	ON SHOP_MNPM_HD.MNPMWhoIn = A.EMPLID  
                        INNER JOIN SHOP_MNPM_DT WITH (NOLOCK)   ON SHOP_MNPM_HD.MNPMDocno = SHOP_MNPM_DT.MNPMDocno  
                WHERE	MNPMStaDoc = '1' 
                        AND CONVERT(VARCHAR,MNPMDateIn,23)   BETWEEN  '{date1}'  AND '{date2}'
                        {bch}
                Order by MNPMDocno,MNPMSeqNo  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IARInterface.GetMoneyAx707(string StartDate, string EndDate, string StratPosGroup, string EndPosGroup, string CashierId)
        {
            string Sql = $@"
                USE [AX50SP1_SPC]  
                DECLARE @StartDate datetime='{StartDate}'  
                DECLARE @EndDate datetime = '{EndDate}'
                DECLARE @StratPosGroup nvarchar(10) ='{StratPosGroup}'
                DECLARE @EndPosGroup nvarchar(10) = '{EndPosGroup}'
                DECLARE @CashierId nvarchar(20) = '{CashierId}'
                DECLARE @Company nvarchar(4) = 'SPC'
                EXECUTE	[dbo].[SPCN_CheckBalance_AmountAndRecieveByCashierByMN]  
		                @StartDate  ,
		                @EndDate  ,
		                @StratPosGroup  ,
		                @EndPosGroup  ,
		                @CashierId  ,
		                @Company
            ";
            DataTable dt = ConnectionClass.SelectSQL_MainAX(Sql);

            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                if (dt.Rows[i]["POSGROUP"].ToString().Equals(""))
                {
                    DataTable DtDRAWERID = Models.ARClass.Check_DRAWERID(dt.Rows[i]["SLROUND"].ToString());
                    if (DtDRAWERID.Rows.Count > 0)
                    {
                        dt.Rows[i]["TRANSDATE"] = DtDRAWERID.Rows[i]["TRANSDATE"].ToString();
                        dt.Rows[i]["POSGROUP"] = DtDRAWERID.Rows[i]["POSGROUP"].ToString();
                        dt.Rows[i]["CASHIERID"] = DtDRAWERID.Rows[i]["EMPLID"].ToString();
                        dt.Rows[i]["CASHIERNAME"] = DtDRAWERID.Rows[i]["SPC_NAME"].ToString();
                    }
                }
            }

            return dt;
        }

        DataTable IARInterface.Check_DRAWERID(string DRAWERID)
        {
            string sql = $@"
                SELECT	EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME,POSGROUP,TRANSDATE 
                FROM	SHOP2013TMP.dbo.XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)
                        INNER JOIN  SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.EMPLID=EMPLTABLE.EMPLID 
                WHERE	DRAWERID = '{DRAWERID}'
                AND XXX_POSDRAWERCYCLETABLE.DATAAREAID = N'SPC'
		        AND EMPLTABLE.DATAAREAID = N'SPC'";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IARInterface.GetDrawer(string DateBegin, string DateEnd, string TimeBegin, string TimeEnd, string CashReceive)
        {
            string sql = $@"
            SELECT  Convert(varchar,SPC_SLROUND.Transdate,23) as Transdate,  
                    SPC_SLROUND.POSGROUP,BRANCH_NAME,MNPMDRAWERID ,MNPMUserDRAWERID +' ' + MNPMNameDRAWERID as MNPMUserDRAWERID , 
			        REPLACE(REPLACE(MNPMRemark, CHAR(13), ''), CHAR(10), '') as MNPMRemark ,
                    CASHSENT, CASHRECEIVE, RemarkAcc 
            FROM    SHOP_MNPM_DT WITH (NOLOCK) 
                    INNER JOIN SHOP2013TMP.dbo.SPC_SLROUND WITH (NOLOCK) ON SHOP_MNPM_DT.MNPMDRAWERID  = SPC_SLROUND.SLROUND AND SPC_SLROUND.DATAAREAID = 'SPC' 
                    LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_SLLINE WITH(NOLOCK) ON  SPC_SLROUND.SLROUND = SPC_SLLINE.SLROUND AND SPC_SLLINE.DATAAREAID = 'SPC' 
                    LEFT OUTER JOIN SHOP2013TMP.dbo.USERINFO WITH(NOLOCK) ON SPC_SLLINE.MODIFIEDBY = USERINFO.ID   
                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_SLROUND.POSGROUP = SHOP_BRANCH.BRANCH_ID 
            WHERE 	convert (varchar, DateAcc,23)+' '+convert (varchar, TimeAcc,24)  BETWEEN '{DateBegin + " " + TimeBegin}' AND  '{DateEnd + " " + TimeEnd}' 
                    AND MNPMDRAWERID LIKE 'D%' 
                    AND SPC_SLROUND.EDITED = '0' 
                    AND SPC_SLROUND.SLTYPE = 1  AND MNPM_SlipSaraly !='1'
                    AND SPC_SLLINE.PAYMMODE = 0  AND CASHRECEIVE {CashReceive} 0
            ORDER BY Transdate,USERINFO.NAME,SPC_SLROUND.POSGROUP,MNPMUserDRAWERID ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable IARInterface.GetSendMoneyToFinance(string DateTimeBegin, string DateTimeEnd)
        {
            string sql = $@"
                SELECT  Convert(varchar,SPC_SLROUND.Transdate,23) as Transdate,  
                        USERINFO.NAME,count(MNPMDRAWERID) as MNPMDRAWERID,
		                sum(SPC_SLROUND.BANKNOTE1000)  as BANKNOTE1000, 
		                sum(SPC_SLROUND.BANKNOTE500)  as BANKNOTE500, 
		                sum(SPC_SLROUND.BANKNOTE100)  as BANKNOTE100, 
		                sum(SPC_SLROUND.BANKNOTE50)  as BANKNOTE50, 
		                sum(SPC_SLROUND.BANKNOTE20)  as BANKNOTE20, 
		                sum(SPC_SLROUND.BANKNOTE10)  as BANKNOTE10, 
                        sum(SPC_SLROUND.COIN10)  as COIN10, 
		                sum(SPC_SLROUND.COIN5)  as COIN5, 
		                sum(SPC_SLROUND.COIN2)  as COIN2, 
		                sum(SPC_SLROUND.COIN1)  as  COIN1
                FROM   SHOP_MNPM_DT WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.SPC_SLROUND WITH (NOLOCK) ON SHOP_MNPM_DT.MNPMDRAWERID  = SPC_SLROUND.SLROUND AND SPC_SLROUND.DATAAREAID = 'SPC' 
                        LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_SLLINE WITH(NOLOCK) ON  SPC_SLROUND.SLROUND = SPC_SLLINE.SLROUND AND SPC_SLLINE.DATAAREAID = 'SPC' 
                        LEFT OUTER JOIN SHOP2013TMP.dbo.USERINFO WITH(NOLOCK) ON SPC_SLLINE.MODIFIEDBY = USERINFO.ID    
                WHERE 	convert (varchar, DateAcc,23)+' '+convert (varchar, TimeAcc,24)  BETWEEN '{DateTimeBegin}' AND  '{DateTimeEnd}' 
		                AND MNPMDRAWERID LIKE 'D%' 
		                AND SPC_SLROUND.EDITED = '0' 
		                AND SPC_SLROUND.SLTYPE = 1 
		                AND SPC_SLLINE.PAYMMODE = 0  
                GROUP BY Transdate,NAME
                ORDER BY Transdate,USERINFO.NAME
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //เช็คยอดการรับ SL ของบิลที่ทำ MNEC
        DataTable IARInterface.GetSLByMNEC(string docno)
        {
            //string sql = $@"
            //   SELECT	ShipmentId,SPC_SLTable.SLId,IIF(STATUSFINANCE='0','เปิดค้างไว้','การเงินรับแล้ว') AS STA,SLAMOUNT,CASHAMOUNT,CONVERT(varchar,SLDATE,23) AS SLDATE,STATUSFINANCE
            //    FROM	SPC_SLTable WITH (NOLOCK)
            //      INNER JOIN SPC_SLTableLinks WITH (NOLOCK) ON SPC_SLTable.SLID = SPC_SLTableLinks.SLID
            //    WHERE	ShipmentId = '{docno}'
            //";
            string sql = $@"
               SELECT	SPC_SLRound.SLROUND,IIF(POSTED=1,1,0) AS POSTED,ShipmentId,SPC_SLTable.SLId,IIF(STATUSFINANCE='0','เปิดค้างไว้','การเงินรับแล้ว') AS STA,SLAMOUNT,SPC_SLTable.CASHAMOUNT,CONVERT(varchar,SLDATE,23) AS SLDATE,STATUSFINANCE
               FROM	SPC_SLTableLinks WITH (NOLOCK) 
		                INNER JOIN SPC_SLTable WITH (NOLOCK) ON SPC_SLTableLinks.SLID = SPC_SLTable.SLID
		                INNER JOIN SPC_SLRound WITH (NOLOCK) ON SPC_SLTable.SLROUND = SPC_SLRound.SLROUND
               WHERE	ShipmentId = '{docno}'
            ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
    }
}
