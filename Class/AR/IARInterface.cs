﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PC_Shop24Hrs.Class.AR
{
    interface IARInterface
    {
        //ค้นหาข้อมูลซองเงินที่ค้างส่งทั้งหมด ตามสาขาหรือเส้นทาง
        DataTable GetDRAWERNotSend_ByCondition(string pCase0Bch1Route);

        //ค้นหาข้อมูลซองเงินที่ค้างส่งทั้งหมด ตามสาขาหรือเส้นทาง โดยแสดงรายละเอียดของซองเงิน
        DataTable GetDetailDRAWERNotSend_ByCondition(string conditionWhere);

        //รายงานจำนวนซองส่งตามรอบ LO
        DataTable ReportSendMoneyByLO(string date1, string date2);

        //รายงานการรับซองเงิน
        DataTable ReportReciveMoney(string date1, string date2);

        //รายงานซองส่งเงินของ พนักงานบัญชีลูกหนี้
        DataTable ReportSendMoneyByAR(string date1, string date2);

        // รายงานรายละเอียดส่งซองเงิน
        DataTable ReportDetailMoney(string date1, string date2);

        //ค้นหาข้อมูลยอด ยอดขาดเกิน จาก procedure 707 โดยตรง
        DataTable GetMoneyAx707(string StartDate, string EndDate, string StratPosGroup, string EndPosGroup, string CashierId);

        //Check รอบส่งเงิน
        DataTable Check_DRAWERID(string DRAWERID);

        //ค้นหารอบที่ส่งเงินมา ทั้งหมดตามช่วงเวลาที่กำหนด
        DataTable GetDrawer(string DateBegin, string DateEnd, string TimeBegin, string TimeEnd, string CashReceive);

        //เปรียบยอดส่งเงินสด
        DataTable GetSendMoneyToFinance(string DateTimeBegin, string DateTimeEnd);

        //เช็คยอดการรับ SL ของบิลที่ทำ MNEC
        DataTable GetSLByMNEC(string docno);
    }
}
