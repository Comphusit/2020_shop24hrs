﻿using System;
using System.Data;

namespace PC_Shop24Hrs.Class
{
    class FingerClass
    {
        //ค้นหาลายนิ้วมือจาก 803
        public static DataTable GetFinger803(string _emplID)
        {
            string sql = $@"SELECT EMPLID,FINGERINDEX,FINGERTEMPLATE,FINGERTEMPLATESIZE     	
                            FROM	EMPLFINGERPRINT WITH (NOLOCK)
                            WHERE	EMPLID LIKE '%{_emplID}' " ;

            return Controllers.ConnectionClass.SelectSQL_SentServer(sql,Controllers.IpServerConnectClass.ConFinger_Digital);
        }
        //ค้นหาลายนิ้วมือจาก 707
        public static DataTable GetFinger707(string _emplID)
        {
            string sql = $@"SELECT EMPLID , FINGERINDEX  , INDEXINGVALUE  , FEATURES , MODIFIEDDATETIME , MODIFIEDBY , CREATEDDATETIME  , CONVERT(VARCHAR,CREATEDBY,23) AS CREATEDBY        	
                            FROM	SYSUSERFINGERPRINT WITH (NOLOCK)
                            WHERE	EMPLID LIKE '%{_emplID}' ";

            return Controllers.ConnectionClass.SelectSQL_SentServer(sql, Controllers.IpServerConnectClass.ConMainRatail707);
        }
        ////ค้นหาลายนิ้วมือจาก 701
        //public static DataTable GetFinger701(string _emplID)
        //{
        //    string sql = string.Format(@"SELECT tfEmpId , tfEmpName , tfFingerIndex, tfIndexValue, tfFeatures, tfEmpIndex, tfUserIns, CONVERT(nvarchar,tfInsDate,23) AS tfInsDate         	
        //                    FROM	tblEmpFinger WITH (NOLOCK)
        //                    WHERE	tfEmpId LIKE '%" + _emplID + "'");

        //    DataTable dt = Controllers.ConnectionClass.SelectSQL_SentServer(sql, Controllers.IpServerConnectClass.ConFinger_ItWorks);
        //    return dt;
        //}
        //ค้นหาลายนิ้วมือจาก server สาขา
        public static DataTable GetFinger000(string _emplID,string _serverDB)
        {
            string sql = $@"SELECT EMPLID , FINGERINDEX  , INDEXINGVALUE  , FEATURES , MODIFIEDDATETIME , MODIFIEDBY , CREATEDDATETIME  , CREATEDBY        	
                            FROM	SYSUSERFINGERPRINT WITH (NOLOCK)
                            WHERE	EMPLID LIKE '%{_emplID}' ";

            return Controllers.ConnectionClass.SelectSQL_SentServer(sql, _serverDB);
        }
    }
}
