﻿using System.Data;
using PC_Shop24Hrs.Controllers;

 namespace PC_Shop24Hrs.Class.Vend
{
    class VendTableClass : IVendClassInterface
    {
        //ค้นหาผู้จำหน่าย
        DataTable IVendClassInterface.GetVENDTABLE_All(string accountNum,string dimensionID)
        {
            string conditionAccountNum = "", conditionDimension = "";
            if (accountNum != "") conditionAccountNum = $@" AND ACCOUNTNUM = '{accountNum}' ";
            if (dimensionID != "") conditionDimension = $@" AND DIMENSION = '{dimensionID}' ";
            string sql = $@"
                SELECT  ACCOUNTNUM,NAME,ADDRESS,PHONE,ITEMBUYERGROUPID,DIMENSION, ACCOUNTNUM AS DATA_ID ,NAME  AS DATA_DESC ,ACCOUNTNUM+' - '+NAME AS SHOW_NAME 
                FROM    SHOP2013TMP.dbo.VENDTABLE  WITH (NOLOCK)
                WHERE   DATAAREAID = 'SPC' AND BLOCKED = 0 
                        {conditionAccountNum} {conditionDimension}
                ORDER BY ACCOUNTNUM,NAME
                ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }

}
