﻿using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.Class.Vend
{
    class VendTableClass2012 : IVendClassInterface
    {
        //ค้นหาผู้จำหน่าย
        //AX2012_OK
        DataTable IVendClassInterface.GetVENDTABLE_All(string accountNum, string dimensionID)
        {
            string conditionAccountNum = "", conditionDimension="";
            if (accountNum != "") conditionAccountNum = $@" AND ACCOUNTNUM = '{accountNum}' ";
            if (dimensionID != "") conditionDimension = $@" AND ITEMBUYERGROUPID = '{dimensionID}' ";
            //ใน 2009 จะใช้ DIMENSION แต่ 2012 ใช้ ITEMBUYERGROUPID แทน
            string sql = $@"
               SELECT	ACCOUNTNUM,NAME_SPC AS NAME,INVOICEADDRESS_SPC AS ADDRESS,PHONE_SPC AS PHONE,ITEMBUYERGROUPID,ITEMBUYERGROUPID AS DIMENSION,
                        ACCOUNTNUM AS DATA_ID,NAME_SPC  AS DATA_DESC ,ACCOUNTNUM+' - '+NAME_SPC AS SHOW_NAME
                FROM	AX60CU13.dbo.VENDTABLE WITH (NOLOCK)
                WHERE	DATAAREAID = 'SPC' AND BLOCKED = 0 
                        {conditionAccountNum} {conditionDimension}
                ORDER BY ACCOUNTNUM,NAME_SPC
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
