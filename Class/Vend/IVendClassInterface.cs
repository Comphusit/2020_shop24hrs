﻿ using System.Data;
 
namespace PC_Shop24Hrs.Class.Vend
{
    interface IVendClassInterface
    {
        //ค้นหาผู้จำหน่าย
        DataTable GetVENDTABLE_All(string accountNum, string dimensionID);
    }
}
