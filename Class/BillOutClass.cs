﻿using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class
{
    class BillOutClass
    {
        //ค้นหา IS จากกลุ่มใน AX ทั้งหมด
        public static DataTable FindIS_AxForGroupMain()
        {
            string sql = $@"
                SELECT	SPC_INVENTCOSTCENTERID AS DATA_ID,SPC_INVENTCOSTCENTERNAME AS DATA_DESC
                FROM	SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) 
                WHERE	SPC_INVENTCOSTCENTERID NOT IN (SELECT GROUPRO_ID FROM SHOP_RO_GROUPMAIN WITH (NOLOCK) )
		                AND DATAAREAID = N'SPC' AND SPC_INVENTCOSTCENTERID LIKE 'IS%'
                ORDER BY SPC_INVENTCOSTCENTERID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหา Item ทั้งหมด
        public static DataTable Find_ItemRO()
        {
            string sql = $@"
            SELECT	STA,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME AS SPC_NAME,GROUPMAIN,
                    (SELECT	GROUPRO_NAME 	FROM	SHOP_RO_GROUPMAIN WITH (NOLOCK) WHERE	GROUPRO_ID = GROUPMAIN)  AS GROUPMAIN_NAME   ,
                    GROUPSUB, 
                    (SELECT	GROUPRO_SUBNAME 	FROM	SHOP_RO_GROUPSUB WITH (NOLOCK) 
                        WHERE	GROUPRO_SUBID = GROUPSUB AND GROUPRO_ID = GROUPMAIN )  AS GROUPSUB_NAME , ZONE,REMARK , 
                    WHOIDINS,WHONAMEINS,INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID AS DptID,DIMENSIONS.DESCRIPTION AS DptName
            FROM	SHOP_RO_ITEM WITH (NOLOCK) 
                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                        AND INVENTITEMBARCODE.DATAAREAID = 'SPC'
                    LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM
                        AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = 'SPC'
            ORDER BY GROUPMAIN,GROUPSUB ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหา Item ระหว่าง AX กับ RO
        public static DataTable Find_ItemRoByItembarcode(string _pBarcode)
        {
            return ConnectionClass.SelectSQL_Main($@"SmartRO_Find_ItemRoByItembarcode '{_pBarcode}'");
        }
        //ค้นหา Item RO ในกลุ่ม Main - Sub
        public static DataTable Find_ItemRoByGrp(string grpMain, string grpSub)
        {
            string sql = $@"
                SELECT	SHOP_RO_ITEM.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME,'0' AS STA,'0.00' AS QTY,UNITID,REMARK
                FROM	SHOP_RO_ITEM WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                WHERE	GROUPMAIN = '{grpMain}' AND GROUPSUB = '{grpSub}'
                ORDER BY GROUPMAIN,GROUPSUB,INVENTITEMBARCODE.SPC_ITEMNAME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหารายละเอียดบิล
        public static string SqlFindBill_ByBilID(string pCase, string pBillID, string serverDB)
        {
            string sql = "";
            switch (pCase)
            {
                case "I":
                    sql = $@"
                    SELECT  Bill_ID,CONVERT(VARCHAR,SPC_TransDate,23) AS Bill_Date,
                                IVZ_REMARKS AS  RMK,JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP_ID AS DptIDCreate,
		                        JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID + '-' + BRANCH_NAME AS BRANCH_NAME,WHOOPENBILL_ID AS WHOINS,WHOOPENBILL_NAME AS WHONAMEINS,
		                        Bill_StaDoc AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                SPC_ITEMBARCODE AS ITEM,SPC_ITEMNAME AS ITEMNAME,SPC_QTY AS QTY,SPC_ITEMBARCODEUNIT AS UNIT,COSTAMOUNT AS COSTAMOUNT,
                                TYPEBILL AS TypeBill,JOBGROUP_DESCRIPTION AS BillDptNameCreate,JOBGROUP_ID AS BillDptIDCreate,
                                STA_RECIVE,STA_INSTALL,STA_RETURN,STA_RETURNRECIVE,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate,
                                '  ['+Bill_INVENTCOSTCENTERID+']' AS INVENTCOSTCENTERID,WH AS WH
                                ,'0' AS STA_D003
                    FROM	dbo.Shop_BillOut WITH (NOLOCK)    
                                INNER JOIN {serverDB}INVENTJOURNALTABLE WITH (NOLOCK) ON  Shop_BillOut.Bill_ID = INVENTJOURNALTABLE.JOURNALID   
                                INNER JOIN {serverDB}INVENTJOURNALTRANS WITH (NOLOCK) ON INVENTJOURNALTABLE.JournalId = INVENTJOURNALTRANS.JournalId   
                    WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC' AND INVENTJOURNALTRANS.DATAAREAID = 'SPC'     
                                AND INVENTJOURNALTABLE.JournalId  = '{pBillID}'  ORDER BY    LineNum ";
                    break;
                case "F": //ใช SQL เดวกับ
                    sql = $@"
                    SELECT  Bill_ID,CONVERT(VARCHAR,SPC_TransDate,23) AS Bill_Date,
                                IVZ_REMARKS AS  RMK,JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP_ID AS DptIDCreate,
		                        JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID + '-' + BRANCH_NAME AS BRANCH_NAME,WHOOPENBILL_ID AS WHOINS,WHOOPENBILL_NAME AS WHONAMEINS,
		                        Bill_StaDoc AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                SPC_ITEMBARCODE AS ITEM,SPC_ITEMNAME AS ITEMNAME,SPC_QTY AS QTY,SPC_ITEMBARCODEUNIT AS UNIT,COSTAMOUNT AS COSTAMOUNT,
                                TYPEBILL AS TypeBill,JOBGROUP_DESCRIPTION AS BillDptNameCreate,JOBGROUP_ID AS BillDptIDCreate,
                                STA_RECIVE,STA_INSTALL,STA_RETURN,STA_RETURNRECIVE,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate,
                                '  ['+Bill_INVENTCOSTCENTERID+']' AS INVENTCOSTCENTERID,WH AS WH  
                                ,'0' AS STA_D003
                    FROM	dbo.Shop_BillOut WITH (NOLOCK)    
                                INNER JOIN {serverDB}INVENTJOURNALTABLE WITH (NOLOCK) ON  Shop_BillOut.Bill_ID = INVENTJOURNALTABLE.JOURNALID   
                                INNER JOIN {serverDB}INVENTJOURNALTRANS WITH (NOLOCK) ON INVENTJOURNALTABLE.JournalId = INVENTJOURNALTRANS.JournalId   
                    WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC' AND INVENTJOURNALTRANS.DATAAREAID = 'SPC'     
                                AND INVENTJOURNALTABLE.JournalId  = '{pBillID}'  ORDER BY    LineNum ";
                    break;
                case "FAL":
                    sql = $@"
                    SELECT	Bill_ID,CONVERT(VARCHAR,SPC_AssetLendingTable.LendingDate,23) AS Bill_Date,
                                SPC_Remarks AS  RMK,JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP_ID AS DptIDCreate,
		                        JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,WHOOPENBILL_ID AS WHOINS,WHOOPENBILL_NAME AS WHONAMEINS,
		                        Bill_StaDoc AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                AssetLending.AssetId AS ITEM,AssetLending.SPC_Name AS ITEMNAME,'1' AS QTY,UnitOfMeasure AS UNIT,UnitCost AS COSTAMOUNT,
                                TYPEBILL AS TypeBill,JOBGROUP_DESCRIPTION AS BillDptNameCreate,JOBGROUP_ID AS BillDptIDCreate,
                                STA_RECIVE,STA_INSTALL,STA_RETURN,STA_RETURNRECIVE,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate,'' AS INVENTCOSTCENTERID,WH AS WH 
                                ,'0' AS STA_D003
                    FROM	dbo.Shop_BillOut WITH (NOLOCK)    
                                INNER JOIN SHOP2013TMP.dbo.SPC_AssetLendingTable WITH (NOLOCK) ON  Shop_BillOut.Bill_ID = SPC_AssetLendingTable.SPC_ASSETLENDINGID    
                                INNER JOIN SHOP2013TMP.dbo.AssetLending WITH (NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = AssetLending.SPC_ASSETLENDINGID    
                                INNER JOIN SHOP2013TMP.dbo.AssetTable WITH (NOLOCK) ON AssetLending.AssetId = AssetTable.AssetId  
                    WHERE	SPC_AssetLendingTable.DATAAREAID = 'SPC' AND AssetLending.DATAAREAID = 'SPC'     
                                AND AssetTable.DATAAREAID = 'SPC' AND SPC_AssetLendingTable.SPC_ASSETLENDINGID  = '{pBillID}' ";
                    break;
                case "MNRB"://บิลส่งของมีทะเบียน
                    sql = $@"
                    SELECT	    SHOP_MNRB.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRB.DATE,23) AS Bill_Date,
                                REMARK AS  RMK,SentToName AS DptNameCreate,'' AS DptIDCreate,
		                        Bill_JOB AS JOBID,BranchID AS BRANCH_ID ,BranchID + '-' + BranchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
		                        CASE StaDoc WHEN '3' THEN '0' ELSE '1' END  AS STADOC,
		                         '1' AS STABCH,Bill_ReciveSta AS BCHRecive,'' AS BCHRmk,
			                    Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_ReciveTime,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                IDProduct AS ITEM,NameProduct AS ITEMNAME,'1' AS QTY,'ชุด' AS UNIT,'0' AS COSTAMOUNT,
                                'MNRB' AS TypeBill, 
                               '1' AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                ShipSta,ShipID,ShipCarID,ShipDriverID+'-'+EMPLTABLE.SPC_NAME AS ShipDriverID,ShipWho,
                                CONVERT(VARCHAR,ShipDate,23)+' ' + ShipTime AS ShipDate ,'' AS INVENTCOSTCENTERID,BranchID AS WH ,'2' AS STA_D003
                                ,ISNULL(Bill_typeCNRecive,'0') AS Bill_typeCNRecive,Bill_TypeCNRemark,Bill_TypeCNWhoID,Bill_TypeCNWhoName,CONVERT(VARCHAR,Bill_TypeCNDate,23) AS Bill_TypeCNDate
								,CONVERT(VARCHAR,Bill_TypeCNTime,25) AS Bill_TypeCNTime
                    FROM	    SHOP_MNRB WITH (NOLOCK) 
                                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRB.ShipDriverID= EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    WHERE	SHOP_MNRB.DOCNO = '{pBillID}'    ";
                    break;
                case "MNRG"://บิลส่งของไม่มีทะเบียน
                    sql = $@"
                    SELECT	SHOP_MNRG.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRG.DATE,23) AS Bill_Date,
                                REMARK AS  RMK,SentToName AS DptNameCreate,'' AS DptIDCreate,
		                        Bill_JOB AS JOBID,BranchID AS BRANCH_ID ,BranchID +'-'+BranchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
		                        CASE StaDoc WHEN '3' THEN '0' ELSE '1' END AS  STADOC,
		                         Bill_ReciveSta AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
			                    Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_ReciveTime,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                '' AS ITEM,NameProduct AS ITEMNAME,Qty AS QTY,'ชุด' AS UNIT,'0' AS COSTAMOUNT,
                                'MNRG' AS TypeBill, 
                                '1' AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                ShipSta,ShipID,ShipCarID,ShipDriverID+'-'+EMPLTABLE.SPC_NAME AS ShipDriverID,ShipWho,CONVERT(VARCHAR,ShipDate,23)+' ' + ShipTime AS ShipDate ,
                                '' AS INVENTCOSTCENTERID,BranchID AS WH ,'2' AS STA_D003  
                                ,ISNULL(Bill_typeCNRecive,'0') AS Bill_typeCNRecive,Bill_TypeCNRemark,Bill_TypeCNWhoID,Bill_TypeCNWhoName,CONVERT(VARCHAR,Bill_TypeCNDate,23) AS Bill_TypeCNDate
								,CONVERT(VARCHAR,Bill_TypeCNTime,25) AS Bill_TypeCNTime
                    FROM	SHOP_MNRG WITH (NOLOCK) 
                                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRG.ShipDriverID= EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    WHERE	SHOP_MNRG.DOCNO = '{pBillID}'    ";
                    break;
                case "MNRH"://บิลส่งเอกสาร
                    sql = $@"
                    SELECT	SHOP_MNRH_HD.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRH_HD.DATE,23) AS Bill_Date,
                                REMARK AS  RMK,'แผนก D004 - แผนกข้อมูลคอมพิวเตอร์ และสถิติ' AS DptNameCreate,'' AS DptIDCreate,
		                        '' AS JOBID,BranchID AS BRANCH_ID ,BranchID+'-'+BranchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
		                        CASE StaDoc WHEN '3' THEN '0' ELSE '1' END AS  STADOC,
		                         '1' AS STABCH,Bill_ReciveSta AS BCHRecive,'' AS BCHRmk,
			                    Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_ReciveTime,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                '' AS ITEM,ItemName AS ITEMNAME,Qty AS QTY,'ชุด' AS UNIT,'0' AS COSTAMOUNT,
                                'MNRH' AS TypeBill, 
                                '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                ShipSta,ShipID,ShipCarID,ShipDriverID+'-'+EMPLTABLE.SPC_NAME AS ShipDriverID,ShipWho,CONVERT(VARCHAR,ShipDate,23)+' ' + ShipTime AS ShipDate ,'' AS INVENTCOSTCENTERID,BranchID AS WH ,'2' AS STA_D003  
                                ,ISNULL(Bill_typeCNRecive,'0') AS Bill_typeCNRecive,Bill_TypeCNRemark,Bill_TypeCNWhoID,Bill_TypeCNWhoName,CONVERT(VARCHAR,Bill_TypeCNDate,23) AS Bill_TypeCNDate
								,CONVERT(VARCHAR,Bill_TypeCNTime,25) AS Bill_TypeCNTime
                    FROM	SHOP_MNRH_HD WITH (NOLOCK) INNER JOIN SHOP_MNRH_DT WITH (NOLOCK)
							ON SHOP_MNRH_HD.DocNo = SHOP_MNRH_DT.DocNo
                                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRH_HD.ShipDriverID= EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'

                    WHERE	SHOP_MNRH_HD.DOCNO = '{pBillID}'  ORDER BY SeqNo ";
                    break;
                case "MNRR"://บิลเบิกของที่สาขาใช้ที่สาขา
                    sql = $@"
                    SELECT	Shop_MNRR_HD.MNRRDocNo AS Bill_ID,CONVERT(VARCHAR,Shop_MNRR_HD.MNRRDate,23) AS Bill_Date,
                                Shop_MNRR_HD.MNRRRemark AS  RMK,WH.BRANCH_NAME AS DptNameCreate,WH.BRANCH_ID AS DptIDCreate,
		                        '' AS JOBID,MNRRBranch AS BRANCH_ID ,
                                MNRRBranchTO+'-'+ CASE WHEN ISNULL(SHOP_BRANCH.BRANCH_NAME,'')='' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END  AS BRANCH_NAME
                                ,MNRRUserCode AS WHOINS,MNRRNAMECH AS WHONAMEINS,
		                        CASE MNRRStaDoc WHEN '3' THEN '0' ELSE '1' END AS STADOC,
		                        '1' AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
			                    MNRRUserCode AS BCHWhoID,MNRRNAMECH AS BCHWhoName,
                                CONVERT(VARCHAR,Shop_MNRR_HD.MNRRDate,23) AS BCHDATE,CONVERT(VARCHAR,MNRRTime,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                MNRRBarcode AS ITEM,MNRRName AS ITEMNAME,MNRRQtyOrder AS QTY,MNRRQtyUnitID AS UNIT,MNRRPriceSum AS COSTAMOUNT,
                                'MNRR' AS TypeBill, 
                                '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                               '2' AS  ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,Shop_MNRR_HD.MNRRBranch AS WH
                                ,'1' AS STA_D003
                    FROM	Shop_MNRR_HD WITH (NOLOCK) 
                            INNER JOIN  Shop_MNRR_DT WITH (NOLOCK) ON Shop_MNRR_HD.MNRRDocNo = Shop_MNRR_DT.MNRRDocNo
                            LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  SHOP_BRANCH.BRANCH_ID
							INNER JOIN SHOP_BRANCH WH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranch =  WH.BRANCH_ID
                            INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' ANd DIMENSIONS.DIMENSIONCODE = '0'
                    WHERE	Shop_MNRR_HD.MNRRDocNo = '{pBillID}'  ORDER BY MNRRSeqNo ";
                    break;
                case "MNRD":
                    sql = $@"
                    SELECT	SHOP_MNRD_HD.MNRDDocNo AS Bill_ID,CONVERT(VARCHAR,MNRDDATE,23) AS Bill_Date,
		                    CASE [TYPEPRODUCT] WHEN '0' THEN 'รถจัดส่ง' ELSE 'รถห้องเย็น' END  + CHAR(10) +ShipDoc + CHAR(10) + MNRDSHID  + CHAR(10) + SHOP_MNRD_HD.MNRDREMARK AS  RMK,
                            '' AS DptNameCreate,MNRDBRANCH AS DptIDCreate,
		                    '' AS JOBID,MNRDBRANCH AS BRANCH_ID ,
		                     MNRDBRANCH+'-'+BRANCH_NAME  AS BRANCH_NAME,MNRDUSERCODE AS WHOINS,SPC_NAME AS WHONAMEINS,
		                    CASE MNRDSTADOC WHEN '3' THEN '0' ELSE '1' END AS STADOC,
		                    '1' AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
		                    '' AS BCHWhoID,'' AS BCHWhoName,
		                    '' AS BCHDATE,'' AS BCHDATETIME,
		                    '/' AS Bill_SpcType,'' AS Bill_SpcWhoIn,'' AS Bill_SpcWhoName,
		                    '' AS Bill_SpcDate,'' AS Bill_SpcDateTime,
		                    '' AS Bill_SpcMoneyStatus,'' AS Bill_SpcMoneySum,'' AS Bill_SpcMoneyID,'' AS Bill_SpcMoneyName,'' AS Bill_SpcRemark,
		                    MNRDBarcode AS ITEM,MNRDName AS ITEMNAME,MNRDReturnQty AS QTY,'Ea.' AS UNIT,'0' AS COSTAMOUNT,
		                    'MNRD' AS TypeBill, 
		                    '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
		                    '2' AS  ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH,'1' AS STA_D003
                    FROM	SHOP_MNRD_HD WITH(NOLOCK) 
			                    INNER JOIN SHOP_MNRD_DT ON SHOP_MNRD_HD.MNRDDOCNO = SHOP_MNRD_DT.MNRDDocNo
			                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDBRANCH = SHOP_BRANCH.BRANCH_ID
			                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDUSERCODE = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    WHERE	 SHOP_MNRD_HD.MNRDDOCNO = '{pBillID}'  
                    ORDER BY MNRDSeqNo ";
                    break;
                default:
                    break;
            }
            return sql;
        }
        //ค้นหารายรายบิล
        public static string FindBillForReport_ByTypeBill(string pTypeBill, string pDate1, string pDate2, string pCondition, string pIS)
        {
            string sql = "";
            switch (pTypeBill)
            {
                case "IMN":
                    sql = $@" SELECT	 SPC_DIMENSION AS BRANCH_ID,SPC_DIMENSION+'-'+ISNULL(Shop_Branch.BRANCH_NAME,'') AS BRANCH_NAME,
		                JournalId AS DOCNO,CONVERT(VARCHAR,SPC_TransDate,23) AS DATE,
		                                InventJournalTable.EMPLID+'-'+ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS InsName,
		                                ISNULL(Bill_TypeBchStatus,0) AS Bill_TypeBchStatus,
		                                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                                IVZ_Remarks AS REMARK,ISNULL(Bill_SpcType,0) AS Bill_SpcType,Bill_StaDoc AS STADOC,
						                ISNULL(STA_RECIVE, CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END) AS STA_RECIVE,
						                ISNULL(STA_INSTALL,CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END) AS STA_INSTALL,
						                ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN, ISNULL(Shop_BillOut.STA_RETURNRECIVE,0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN    ,SPC_Department AS DPT_ID     ,Bill_INVENTCOSTCENTERID AS ISA  

                        FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                                             LEFT OUTER JOIN  Shop_Branch WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = Shop_Branch.BRANCH_ID 
                                             INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DEPARTMENT = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                                             LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                                             LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                                             LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum  = TMPCOM.EMP_ID 
                                             LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                                             ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]

                        WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                            AND JournalId NOT LIKE 'MNRS%' AND JournalId NOT LIKE 'MNRR%' 
                            AND [JOURNALTYPE] ! = '4'  AND INVENTJOURNALTABLE.JournalNameId != 'IM-MM-PT'   
                            AND SPC_DIMENSION LIKE '{pCondition}' 
                            AND  CONVERT(VARCHAR, SPC_TransDate, 23) BETWEEN '{pDate1}' AND '{pDate2}'    

                        ORDER BY SPC_DIMENSION,JournalId ";
                    break;

                case "ISPC":
                    sql = $@" SELECT	 SPC_DIMENSION AS BRANCH_ID,SPC_DIMENSION+'-'+ISNULL(B_NAME.DESCRIPTION,'') AS BRANCH_NAME,
		                        JournalId AS DOCNO,CONVERT(VARCHAR,SPC_TransDate,23) AS DATE,
		                                InventJournalTable.EMPLID+'-'+ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS InsName,
		                                ISNULL(Bill_TypeBchStatus,0) AS Bill_TypeBchStatus,
		                                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                                IVZ_Remarks AS REMARK,Bill_SpcType,Bill_StaDoc AS STADOC,
						                ISNULL(STA_RECIVE, CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END) AS STA_RECIVE,
						                ISNULL(STA_INSTALL,CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END) AS STA_INSTALL,
						                ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN, ISNULL(Shop_BillOut.STA_RETURNRECIVE,0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,SPC_Department AS DPT_ID   ,Bill_INVENTCOSTCENTERID AS ISA


                        FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                                             
                                             INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                                             LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                                             LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                                             LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum  = TMPCOM.EMP_ID 
                                             LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                                             ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]

                        WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                                             AND JournalId NOT LIKE 'MNRS%' AND JournalId NOT LIKE 'MNRR%' 
                                             AND [JOURNALTYPE] ! = '4'  AND INVENTJOURNALTABLE.JournalNameId != 'IM-MM-PT'   
                                    AND SPC_DIMENSION LIKE '{pCondition}' 
                                AND  CONVERT(VARCHAR, SPC_TransDate, 23) BETWEEN '{pDate1}'
                                    AND '{pDate2}'  ";
                    if (pIS != "")
                    {
                        sql += $@" AND SPC_INVENTCOSTCENTERID = '{pIS}' ";
                    }
                    sql += " ORDER BY SPC_DIMENSION,JournalId   ";
                    break;

                case "FAL":
                    sql = $@"
                            SELECT	SPC_AssetLendingTable.Dimension AS BRANCH_ID,
		                                    SPC_AssetLendingTable.Dimension+'-'+CASE WHEN ISNULL(Shop_Branch.BRANCH_NAME, '') = '' THEN B_NAME.DESCRIPTION ELSE Shop_Branch.BRANCH_NAME END AS BRANCH_NAME,
                                            SPC_AssetLendingTable.SPC_ASSETLENDINGID AS DOCNO,CONVERT(VARCHAR,LendingDate,23) AS DATE,
		                                    SPC_AssetLendingTable.EmplId+'-'+ISNULL(SPC_NAME, 'ดูแลระบบมินิมาร์ท') AS InsName,
		                                    ISNULL(Bill_TypeBchStatus,'0') AS Bill_TypeBchStatus,
		                                    CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                                    CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                                    CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                                   SPC_Remarks AS  REMARK,ISNULL(Bill_SpcType,0) AS Bill_SpcType,ISNULL(Bill_StaDoc,0) AS STADOC,
						                    CASE WHEN SUBSTRING(SPC_AssetLendingTable.Dimension ,1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,
						                    '1' AS STA_INSTALL,ISNULL(STA_RETURN, 0) AS STA_RETURN,ISNULL(STA_RETURNRECIVE, 0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                                             ISNULL(TMPCOM.EMP_ID, 0) AS ComMN    ,DIMENSIONS.NUM AS DPT_ID  ,Bill_INVENTCOSTCENTERID AS ISA
 
                            from SHOP2013TMP.dbo.SPC_AssetLendingTable with (NOLOCK)
                                         LEFT OUTER JOIN  Shop_Branch WITH(NOLOCK) ON SPC_AssetLendingTable.Dimension = Shop_Branch.BRANCH_ID
                                         LEFT OUTER JOIN    SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON SPC_AssetLendingTable.EMPLID = EMPLTABLE.EMPLID   AND EMPLTABLE.DATAAREAID = 'SPC'
                                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH(NOLOCK) ON SPC_AssetLendingTable.Dimension = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                                         LEFT OUTER JOIN    Shop_BillOut WITH(NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = Shop_BillOut.Bill_ID
                                         LEFT OUTER JOIN(SELECT EMP_ID  FROM Shop_Employee WITH(NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum = TMPCOM.EMP_ID
                                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 

                            WHERE  SPC_AssetLendingTable.DATAAREAID = 'SPC'
                                         AND CONVERT(VARCHAR, LendingDate, 23) BETWEEN '{pDate1}' AND '{pDate2}'
					                     AND SPC_AssetLendingTable.Dimension LIKE '{pCondition}'  

                            ORDER BY SPC_AssetLendingTable.Dimension, SPC_AssetLendingTable.SPC_ASSETLENDINGID ";
                    break;
                case "MNRR":
                    sql = $@"  SELECT	MNRRBranch AS BRANCH_ID,
                        MNRRBranchTO+'-'+ CASE WHEN ISNULL(SHOP_BRANCH.BRANCH_NAME,'')='' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END  AS BRANCH_NAME,
                        MNRRDocNo AS DocNo,CONVERT(VARCHAR,MNRRDate,23) AS DATE,
		                MNRRUserCode+'-'+MNRRNAMECH AS InsName,
		               '1' AS Bill_TypeBchStatus,
		                ''  AS BchRemark,
		                CONVERT(VARCHAR,MNRRDate,23) AS BchDate,
		                MNRRWhoIn+char(10)+MNRRNAMECH  AS BchReciveNAME,
		                MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END  AS REMARK,Bill_SpcType,
						CASE MNRRStaDoc WHEN '3' THEN '0'  WHEN '0' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,MNRRBranch AS DPT_ID  ,'MNRR' AS ISA
                FROM	Shop_MNRR_HD WITH (NOLOCK)  
                         LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON Shop_MNRR_HD.MNRRUserCode  = TMPCOM.EMP_ID 
						 LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  SHOP_BRANCH.BRANCH_ID
								INNER JOIN SHOP_BRANCH WH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranch =  WH.BRANCH_ID
                        INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' ANd DIMENSIONS.DIMENSIONCODE = '0'
                WHERE   CONVERT(VARCHAR, MNRRDate, 23) BETWEEN   '{pDate1}'
                            AND '{pDate2}' AND MNRRBranch LIKE '{pCondition}'  ORDER BY MNRRBranch,MNRRDocNo ";
                    break;
                case "IDM":
                    sql = $@" SELECT	 SPC_DIMENSION AS BRANCH_ID,SPC_DIMENSION+'-'+ISNULL(B_NAME.DESCRIPTION,'') AS BRANCH_NAME,
		                        JournalId AS DOCNO,CONVERT(VARCHAR,SPC_TransDate,23) AS DATE,
		                                InventJournalTable.EMPLID+'-'+ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS InsName,
		                                ISNULL(Bill_TypeBchStatus,0) AS Bill_TypeBchStatus,
		                                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                                IVZ_Remarks AS REMARK,Bill_SpcType,Bill_StaDoc AS STADOC,
						                ISNULL(STA_RECIVE, CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END) AS STA_RECIVE,
						                '0' AS STA_INSTALL,
						                ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN, ISNULL(Shop_BillOut.STA_RETURNRECIVE,0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                                        '0' AS ComMN   ,SPC_DIMENSION AS DPT_ID  ,'IDM' AS ISA


                        FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                                             
                                             INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                                             LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                                             LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                                           

                        WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                                AND     JOURNALNAMEID = 'IM-MM-IDM'    
                                AND  CONVERT(VARCHAR, SPC_TransDate, 23) BETWEEN '{pDate1}' AND '{pDate2}'  
                        
                        ORDER BY SPC_DIMENSION,JournalId  ";
                    break;
                case "MNRD":
                    sql = $@"SELECT MNRDBRANCH AS BRANCH_ID, MNRDBRANCH + '-' + BRANCH_NAME AS BRANCH_NAME, MNRDDOCNO AS DOCNO, CONVERT(VARCHAR, MNRDDATE, 23) AS DATE,
                            MNRDUSERCODE + CHAR(10) + SPC_NAME AS InsName,
                            '1' AS Bill_TypeBchStatus,
                            ShipDoc+CHAR(10)+ MNRDSHID AS BchRemark,
                            '' AS BchDate,
                            '' AS BchReciveNAME,
                            CASE [TYPEPRODUCT] WHEN '0' THEN 'รถจัดส่ง' ELSE 'รถห้องเย็น' END  + CHAR(10) + MNRDREMARK AS REMARK,'1'  AS Bill_SpcType,
                            MNRDSTADOC AS STADOC,
                            '0'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                            '0' AS ComMN  ,MNRDBRANCH AS DPT_ID   ,'MNRD' AS ISA
                    FROM    SHOP_MNRD_HD WITH(NOLOCK)
		                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDBRANCH = SHOP_BRANCH.BRANCH_ID
		                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDUSERCODE = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    WHERE   CONVERT(VARCHAR, MNRDDATE, 23) BETWEEN '{pDate1}' AND '{pDate2}'  AND BRANCH_ID = '{pCondition}'  
                    ORDER BY MNRDBRANCH,MNRDDOCNO ";
                    break;
                default:
                    break;
            }
            return sql;
        }
        //ค้นหาการตรวจบิลของแผนก รปภ.
        public static DataTable FindBill_ByD003(string pBillID)
        {
            string sql = $@"
            SELECT	Shop_BillOutStatusCountItem.*,SPC_NAME,CONVERT(VARCHAR,CREATEDATE,25) AS DATEOUT 
            FROM	Shop_BillOutStatusCountItem  WITH (NOLOCK)
                    LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE with (nolock)  ON Shop_BillOutStatusCountItem.USERCREATE =  
                        EMPLTABLE.EMPLID  AND DATAAREAID = N'SPC' 
            WHERE	BILLID = '{pBillID}'";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาเลขที่บิลใน AX
        public static DataTable FindBillAX_ByBillID(string pBillId)
        {

            string str = $@"
             SELECT	INVENTJOURNALTABLE.JournalId AS Bill_ID,SPC_DIMENSION AS BRANCH_ID,
                 case WHEN Shop_Branch.BRANCH_NAME IS NULL THEN T.DESCRIPTION ELSE Shop_Branch.BRANCH_NAME END AS BRANCH_NAME,
                 CONVERT(VARCHAR,SPC_TransDate,23)  AS DATEBill, 
                 InventJournalTable.EMPLID AS EmplId,ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS SPC_NAME , 
                 SPC_DEPARTMENT AS DPT_ID,DIMENSIONS.DESCRIPTION AS DPT_NAME,
                 IVZ_REMARKS AS RMK,
                 case When SUBSTRING(INVENTJOURNALTABLE.JournalId,1,1) = 'F' Then AssetId ELSE SPC_ITEMBARCODE END AS BARCODE, 
                 SPC_ITEMNAME AS ITEMNAME, SPC_QTY AS QTY, SPC_ITEMBARCODEUNIT AS UNIT, COSTAMOUNT AS COST,
                 SPC_INVENTCOSTCENTERID AS INVENTCOSTCENTERID,
                 SPC_INVENTLOCATIONIDFROM AS INVENTLOCATIONIDFROM,ISNULL (Bill_ID,'0') AS Bill24,SPC_Approved AS Apps ,
                 '' AS SerialNum,ISNULL(STA_RETURN,0) AS STA_RETURN,ISNULL(PRINTCOUNT,1) AS PRINTCOUNT,ISNULL(JOB_NUMBER,'') AS JOB_NUMBER     
                 ,CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END AS STA_RECIVE,
                     CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END AS STA_INSTALL,SUBSTRING(INVENTJOURNALTABLE.JournalId,1,1) AS TYPE_BILL

             FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK)   
                 INNER JOIN  SHOP2013TMP.dbo.INVENTJOURNALTRANS WITH (NOLOCK) ON INVENTJOURNALTABLE.JournalId = INVENTJOURNALTRANS.JournalId  
                 INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DEPARTMENT = DIMENSIONS.NUM   AND DIMENSIONS.DIMENSIONCODE = '0'
                 INNER JOIN SHOP2013TMP.dbo.DIMENSIONS T WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = T.NUM   
                 LEFT OUTER JOIN  Shop_Branch WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = Shop_Branch.BRANCH_ID  
                 LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID   
                 LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID
                 LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                     ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]

             WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC' AND INVENTJOURNALTRANS.DATAAREAID = 'SPC'    AND T.DATAAREAID = 'SPC'  
                 AND T.DIMENSIONCODE = '0'  AND EMPLTABLE.DATAAREAID = 'SPC'
                 AND INVENTJOURNALTABLE.JournalId = '{pBillId}' ORDER BY 	LINENUM  ";
            DataTable dt = ConnectionClass.SelectSQL_Main(str);
            if (dt.Rows.Count == 0)
            {
                str = $@"
                 SELECT	SPC_AssetLendingTable.SPC_ASSETLENDINGID AS Bill_ID,SPC_AssetLendingTable.Dimension AS BRANCH_ID,
                     case WHEN Shop_Branch.BRANCH_NAME IS NULL THEN T.DESCRIPTION ELSE Shop_Branch.BRANCH_NAME END AS BRANCH_NAME , 
                     CONVERT(VARCHAR,SPC_AssetLendingTable.LendingDate,23) AS DATEBill,   
                     SPC_AssetLendingTable.EmplId AS EmplId,ISNULL(EMPLTABLE.SPC_NAME,'ดูแลระบบมินิมาร์ท') AS SPC_NAME ,   
                     EMPLTABLE.DIMENSION AS DPT_ID,DIMENSIONS.DESCRIPTION AS DPT_NAME,        
                     SPC_Remarks AS RMK,
                     AssetLending.AssetId AS BARCODE,AssetLending.SPC_Name AS  ITEMNAME,'1' AS QTY,UnitOfMeasure AS UNIT ,UnitCost AS COST,
                     '' AS INVENTCOSTCENTERID,'' AS INVENTLOCATIONIDFROM,ISNULL (Bill_ID,'0') AS Bill24,SPC_Approved AS Apps, 
                     SerialNum,ISNULL(STA_RETURN,0) AS STA_RETURN ,ISNULL(PRINTCOUNT,1) AS PRINTCOUNT,ISNULL(JOB_NUMBER,'') AS JOB_NUMBER  ,
                    CASE WHEN SUBSTRING(SPC_AssetLendingTable.Dimension,1,1)='M' THEN '1' ELSE '0' END AS STA_RECIVE,'1' AS STA_INSTALL,'FAL'  AS TYPE_BILL 
                 FROM	SHOP2013TMP.dbo.SPC_AssetLendingTable WITH (NOLOCK)    
                     INNER JOIN  SHOP2013TMP.dbo.AssetLending WITH (NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = AssetLending.SPC_ASSETLENDINGID    
                     INNER JOIN SHOP2013TMP.dbo.AssetTable WITH (NOLOCK) ON AssetLending.AssetId = AssetTable.AssetId   
                     LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_AssetLendingTable.EMPLID =  EMPLTABLE.EMPLID     
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM   
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS T WITH (NOLOCK) ON SPC_AssetLendingTable.Dimension = T.NUM   
                     LEFT OUTER JOIN Shop_Branch WITH (NOLOCK) ON SPC_AssetLendingTable.Dimension = Shop_Branch.BRANCH_ID  
                     LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = Shop_BillOut.Bill_ID   
                 WHERE	SPC_AssetLendingTable.DATAAREAID = 'SPC' AND AssetLending.DATAAREAID = 'SPC'     
                 AND T.DATAAREAID = 'SPC'   AND T.DIMENSIONCODE = '0'  AND EMPLTABLE.DATAAREAID = 'SPC'   
                 AND AssetLending.SPC_ASSETLENDINGID = '{pBillId}'    ORDER BY	SPC_AssetLendingTable.SPC_ASSETLENDINGID  ";
                dt = ConnectionClass.SelectSQL_Main(str);
            }

            if (dt.Rows.Count == 0)
            {
                str = $@"
                    SELECT	SHOP_RO_MNRSHD.DOCNO AS Bill_ID,SHOP_RO_MNRSHD.BRANCH_ID AS BRANCH_ID,
		                    SHOP_RO_MNRSHD.BRANCH_NAME,
		                    CONVERT(VARCHAR,SHOP_RO_MNRSHD.DATE,23)  AS DATEBill, 
		                    SHOP_RO_MNRSHD.WHOIDINS AS EmplId,SHOP_RO_MNRSHD.WHONAMEINS AS SPC_NAME , 
		                    DPTIDINS AS DPT_ID,DPTNAMEINS AS DPT_NAME,
		                    SHOP_RO_MNRSHD. REMARK AS RMK,
		                    SHOP_RO_MNRSDT.ITEMBARCODE AS BARCODE, 
		                    SPC_ITEMNAME AS ITEMNAME, QTY AS QTY, UNITID AS UNIT, LINEAMOUNT AS COST,
		                    GROUPMAIN AS INVENTCOSTCENTERID,
		                    INVENT AS INVENTLOCATIONIDFROM,CASE WHEN ISNULL(JOB_NUMBER,'0') = '' THEN '0' ELSE ISNULL(JOB_NUMBER,'0') END AS Bill24,'1' AS Apps ,
		                    '' AS SerialNum,'0' AS STA_RETURN,ISNULL(SHOP_RO_MNRSHD.PRINTCOUNT,1) AS PRINTCOUNT,ISNULL(JOB_NUMBER,'') AS JOB_NUMBER,Bill_TypeBchStatus AS STA_RECIVE,
		                    CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END AS STA_INSTALL,'MNRS' AS TYPE_BILL
                    FROM	SHOP_RO_MNRSHD WITH (NOLOCK)   
		                    INNER JOIN  SHOP_RO_MNRSDT WITH (NOLOCK) ON SHOP_RO_MNRSHD.DOCNO = SHOP_RO_MNRSDT.DOCNO  
		                    LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
			                    ON SHOP_RO_MNRSDT.GROUPMAIN = TMP_COST.[SHOW_ID]

                    WHERE	SHOP_RO_MNRSHD.DOCNO =  '{pBillId}'
		                    AND SHOP_RO_MNRSHD.STADOC = '1' AND SHOP_RO_MNRSDT.STADOC = '1'

                    ORDER BY 	LINENUM 
                ";
                dt = ConnectionClass.SelectSQL_Main(str);
            }
            return dt;
        }
        //ค้นหาบิลเพื่อ Import
        public static DataTable FindAllBillAX_ForImport(string pDate1, string pDate2)
        {
            // WHEN '18' THEN SUBSTRING(JournalId,1,2)  WHEN '16' THEN SUBSTRING(JournalId,1,4)
            string strSql = $@"
                SELECT	JournalId,SPC_DIMENSION AS BRANCH_ID,B_NAME.DESCRIPTION AS BRANCH_NAME , 
                     CONVERT(VARCHAR,SPC_TransDate,23) AS SPC_TransDate, SPC_DEPARTMENT AS SPC_DEPARTMENT_ID,DIMENSIONS.DESCRIPTION AS SPC_DEPARTMENT_NAME, 
                     SPC_INVENTCOSTCENTERID,InventJournalTable.EMPLID,ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS SPC_NAME , 
                     SPC_INVENTLOCATIONIDFROM,IVZ_REMARKS, 
                     CASE WHEN ISNULL (Bill_ID,'0')='0' THEN '0' ELSE '/' END AS Bill24,ISNULL (TMPCOM.EMP_ID ,0) AS ComMN,SPC_Picked ,
                     CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END AS STA_RECIVE,
                     CASE WHEN ISNULL([SHOW_ID],'0') = '0' THEN '0' ELSE '1' END AS STA_INSTALL,
                     ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN,
                     CASE LEN(JournalId) WHEN '11' THEN SUBSTRING(JournalId,1,1)  ELSE 'I' END AS BillTYPE,
                     CASE	WHEN ISNULL(Bill_ID,'0') != '0' THEN '0' ELSE 
			                    CASE WHEN ISNULL (SPC_Approved,'0')= '0' THEN '0' ELSE		
				                    CASE WHEN ISNULL(TMPCOM.EMP_ID, '0') = '0' THEN  CASE WHEN ISNULL(SPC_Department, '0') != 'D179' THEN '1' ELSE '0' END ELSE '0' END END
                     END AS Import ,SPC_Department AS DPT_ID   
 
                     FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DEPARTMENT = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                     LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                     LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                     LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum  = TMPCOM.EMP_ID 
 
                     INNER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                     ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]
 
                     WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                     AND JournalId NOT LIKE 'MNRS%' AND JournalId NOT LIKE 'MNRR%' 
                     AND [JOURNALTYPE] ! = '4'  AND INVENTJOURNALTABLE.JournalNameId != 'IM-MM-PT'   
                     AND SPC_TransDate BETWEEN '{pDate1}'  AND '{pDate2}'   
                     AND SPC_DIMENSION LIKE'D%'

                     Union

                     SELECT	 
                     JournalId,SPC_DIMENSION AS BRANCH_ID,--B_BANE.DESCRIPTION AS BRANCH_NAME , 
                        ISNULL(Shop_Branch.BRANCH_NAME,'') AS BRANCH_NAME,
                     CONVERT(VARCHAR,SPC_TransDate,23) AS SPC_TransDate, 
                     SPC_DEPARTMENT AS SPC_DEPARTMENT_ID,DIMENSIONS.DESCRIPTION AS SPC_DEPARTMENT_NAME, 
                     SPC_INVENTCOSTCENTERID,InventJournalTable.EMPLID,ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS SPC_NAME , 
                     SPC_INVENTLOCATIONIDFROM,IVZ_REMARKS,  
                     CASE WHEN ISNULL (Bill_ID,'0')='0' THEN '0' ELSE '/' END AS Bill24,
                     ISNULL (TMPCOM.EMP_ID ,0) AS ComMN,SPC_Picked ,
                     CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END AS STA_RECIVE,
                     CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END AS STA_INSTALL,
                     ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN , 
                     CASE LEN(JournalId) WHEN '11'  THEN SUBSTRING(JournalId,1,1)   ELSE 'I' END AS BillTYPE,
                     CASE	WHEN ISNULL(Bill_ID,'0') != '0' THEN '0' ELSE 
			                    CASE WHEN ISNULL (SPC_Approved,'0')= '0' THEN '0' ELSE		
				                    CASE WHEN ISNULL(TMPCOM.EMP_ID, '0') = '0' THEN  CASE WHEN ISNULL(SPC_Department, '0') != 'D179' THEN '1' ELSE '0' END ELSE '0' END END
                     END AS Import ,SPC_Department AS DPT_ID  
 
                     FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                     LEFT OUTER JOIN  Shop_Branch WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = Shop_Branch.BRANCH_ID 
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DEPARTMENT = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                     LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                     LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                     LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum  = TMPCOM.EMP_ID 
 
                     LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                     ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]

                     WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                     AND JournalId NOT LIKE 'MNRS%' AND JournalId NOT LIKE 'MNRR%' 
                     AND [JOURNALTYPE] ! = '4'  AND INVENTJOURNALTABLE.JournalNameId != 'IM-MM-PT'   
                     AND SPC_TransDate BETWEEN '{pDate1}'  AND '{pDate2}'   
                     AND SPC_DIMENSION LIKE'MN%'

                     union


                     select     SPC_AssetLendingTable.SPC_ASSETLENDINGID AS JournalId, SPC_AssetLendingTable.Dimension AS BRACNH_ID,
                    CASE WHEN ISNULL(Shop_Branch.BRANCH_NAME, '') = '' THEN B_NAME.DESCRIPTION ELSE Shop_Branch.BRANCH_NAME END AS BRANCH_NAME,
                     CONVERT(VARCHAR, LendingDate, 23) AS SPC_TransDate,
                      EMPLTABLE.DIMENSION AS SPC_DEPARTMENT_ID,DIMENSIONS.DESCRIPTION AS SPC_DEPARTMENT_NAME, '' AS SPC_INVENTCOSTCENTERID,
                     SPC_AssetLendingTable.EmplId, ISNULL(SPC_NAME, 'ดูแลระบบมินิมาร์ท') AS SPC_NAME,
                     '' AS SPC_INVENTLOCATIONIDFROM,
                     SPC_Remarks AS IVZ_REMARKS,CASE WHEN ISNULL (Bill_ID,'0')='0' THEN '0' ELSE '/' END AS Bill24, ISNULL(TMPCOM.EMP_ID, 0) AS ComMN, SPC_Approved AS SPC_Picked,
                      CASE WHEN SUBSTRING(SPC_AssetLendingTable.Dimension, 1, 1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE, '1' AS STA_INSTALL,
                     ISNULL(STA_RETURN, 0) AS STA_RETURN, 'FAL' AS BillTYPE,
                        CASE	WHEN ISNULL(Bill_ID,'0') != '0' THEN '0' ELSE 
			                        CASE WHEN ISNULL (SPC_Approved,'0')= '0' THEN '0' ELSE		
				                        CASE WHEN ISNULL(TMPCOM.EMP_ID, '0') = '0' THEN  CASE WHEN ISNULL(DIMENSIONS.NUM, '0') != 'D179' THEN '1' ELSE '0' END ELSE '0' END END
                         END AS Import , DIMENSIONS.NUM AS DPT_ID

                     from SHOP2013TMP.dbo.SPC_AssetLendingTable with (NOLOCK)
                     LEFT OUTER JOIN  Shop_Branch WITH(NOLOCK) ON SPC_AssetLendingTable.Dimension = Shop_Branch.BRANCH_ID
                     LEFT OUTER JOIN    SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON SPC_AssetLendingTable.EMPLID = EMPLTABLE.EMPLID   AND EMPLTABLE.DATAAREAID = 'SPC'
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH(NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = 'SPC'AND DIMENSIONS.DIMENSIONCODE = '0'
                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH(NOLOCK) ON SPC_AssetLendingTable.Dimension = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                     LEFT OUTER JOIN    Shop_BillOut WITH(NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = Shop_BillOut.Bill_ID
                     LEFT OUTER JOIN(SELECT EMP_ID  FROM Shop_Employee WITH(NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum = TMPCOM.EMP_ID

                     WHERE  SPC_AssetLendingTable.DATAAREAID = 'SPC'
                     AND CONVERT(VARCHAR, LendingDate, 23) BETWEEN '{pDate1}'  AND '{pDate2}'   

                    UNION

                     SELECT	JournalId,SPC_DIMENSION AS BRANCH_ID,B_NAME.DESCRIPTION AS BRANCH_NAME , 
                                         CONVERT(VARCHAR,SPC_TransDate,23) AS SPC_TransDate, '00010' AS SPC_DEPARTMENT_ID,'รับคืนสินค้า' AS SPC_DEPARTMENT_NAME, 
                                         SPC_INVENTCOSTCENTERID,SUBSTRING(InventJournalTable.EMPLID,2,7) AS EMPLID,ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS SPC_NAME , 
                                         SPC_INVENTLOCATIONIDFROM,IVZ_REMARKS, CASE WHEN ISNULL (Bill_ID,'0')='0' THEN '0' ELSE '/' END AS Bill24,'0' AS ComMN,SPC_Picked ,
                                         CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END AS STA_RECIVE,
                                         '0' AS STA_INSTALL,
                                         ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN,
                                         'IDM' AS BillTYPE, 
                                         CASE	WHEN ISNULL(Bill_ID,'0') != '0' THEN '0' ELSE 
			                                        CASE WHEN ISNULL (SPC_Picked,'0')= '0' THEN '0' ELSE '1' END
                                         END AS Import ,'D035' AS DPT_ID
 
                     FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DEPARTMENT = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                                         LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                                         LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                
                     WHERE	 JOURNALNAMEID = 'IM-MM-IDM' 
						                    AND SPC_APPROVED = '1' 
						                    AND SPC_InventLocationIdFrom = 'RETURN' 
						                    AND InventJournalTable.DATAAREAID = N'SPC' 
						                    AND SPC_TransDate BETWEEN '{pDate1}'  AND '{pDate2}'    

                    ORDER BY BRANCH_ID
            ";

            return ConnectionClass.SelectSQL_Main(strSql);
        }
        //ค้นหารายการหัก
        public static DataTable Find_EMP_ListInCorrect(string pDptID, string pDate1, string pDate2, string pType, string pBch)
        {
            string conType = "";
            if (pType != "") conType = $@" AND TYPE = '{pType}' ";

            string sql = $@"SELECT DOCNO,TYPE,EMPLID AS WHOID,WHONAME,DPTID,DPTNAME,GRAND,REMARK,WHOIDINS,WHONAMEINS,CONVERT(VARCHAR,DATEINS,23)  AS DATEINS
            FROM	[SHOP_EMPLOYEE_ListInCorrect] WITH (NOLOCK)
                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON [SHOP_EMPLOYEE_ListInCorrect].WHOID = EMPLTABLE.ALTNUM  
            WHERE	DPTIDINS = '{pDptID}' {pBch}
		            AND CONVERT(VARCHAR,DATEINS,23) BETWEEN '{pDate1}' AND '{pDate2}'  {conType}
            ORDER BY DOCNO ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Find Tab
        public static string FindTab_ByIME(string pIME)
        {
            string str = $@"SELECT TABLOCATION 	FROM  [TABTABLESETUPAPP] WITH (NOLOCK)  WHERE	IMEI_1 = '{pIME}' ";
            DataTable dt = ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.Con803SRV);
            if (dt.Rows.Count > 0) return dt.Rows[0]["TABLOCATION"].ToString(); else return "";
        }
        //Find IS ALL
        public static DataTable Find_IS()
        {
            string str = string.Format(@"
            Select 	SPC_INVENTCOSTCENTERID,SPC_INVENTCOSTCENTERID + ' - ' +  SPC_INVENTCOSTCENTERNAME AS SPC_INVENTCOSTCENTERNAME  
            From 	SHOP2013TMP.dbo.SPC_InventCostCenter WITH (NOLOCK) 
            WHERE	Dataareaid = 'SPC' 
            ORDER BY  SPC_INVENTCOSTCENTERID ");
            return ConnectionClass.SelectSQL_Main(str);
        }
        //Find Bill RA
        public static DataTable FindBill_RA(string pDate1, string pDate2)
        {
            string str = $@" 
                SELECT  [TRAN_SHIPMENTINVOICE].BADOCNO AS BillTrans,INVOICE,CREATEDUSER,CONVERT(VARCHAR, CREATEDDATE,23) CREATEDDATE,AMOUNT,[TRAN_BILLACCEPTHD].DOCSTATUS AS DOCSTATUS 
                FROM    TRAN_SHIPMENTINVOICE INNER JOIN TRAN_BILLACCEPTHD ON [TRAN_SHIPMENTINVOICE].BADOCNO = [TRAN_BILLACCEPTHD].BADOCNO 
                WHERE   [TRAN_SHIPMENTINVOICE].BADOCNO LIKE 'RA%' 
                        and [TRAN_BILLACCEPTHD].DOCSTATUS LIKE '2' and CONVERT(VARCHAR, CREATEDDATE,23)  between '{pDate1}' and '{pDate2}'
            ";
            return ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConRA);
        }
        //ค้นหา PXE จากบิล RA
        public static DataTable FindPXE_ByRA(string pRA)
        {
            string str = string.Format(@"
                SELECT	PURCHID,IVZ_REMARKS
                FROM	[AX50SP1_SPC].[dbo].[PURCHTABLE] WIIH (NOLOCK)
                WHERE	[NUMBERSEQUENCEGROUP] = 'PXE' 
		                AND [IVZ_REMARK] = '" + pRA + @"' 
            ");

            DataTable dt = ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConMainReportAX);
            if (dt.Rows.Count == 0) return ConnectionClass.SelectSQL_MainAX(str); else return dt;
        }
        //ค้นหาสินค้าทั้งหมดก่อนสั่ง ตามกลุ่ม
        public static DataTable SHOP_RO_ItemForOrder(string grpMain, string grpSub, string pCase = "", string dDate = "")
        {
            string sql;
            if (pCase != "D062")
            {
                string sub = "";
                if (grpSub != "") sub = $@"  AND GROUPSUB  = '{grpSub}' ";
                sql = $@"
                SELECT	INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,ZONE ,UNITID
                FROM	SHOP_RO_ITEM WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                WHERE   GROUPMAIN = '{grpMain}'  {sub}
                ORDER BY SPC_ITEMNAME,GROUPMAIN,GROUPSUB ";
            }
            else
            {
                //sql = $@"
                //WITH GROUP1 AS(
                //SELECT	INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,ZONE ,UNITID
                //FROM	SHOP_RO_ITEM WITH (NOLOCK) 
                //        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                //WHERE   GROUPMAIN IN ('IS0602','IS0079','IS0044')
                //),
                //GROUP2 AS(
                //SELECT	INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,ZONE ,UNITID
                //FROM	SHOP_RO_ITEM WITH (NOLOCK) 
                //  INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                //WHERE	GROUPMAIN = 'IS0098'
                //  AND GROUPSUB NOT LIKE  'GR01'
                //) 
                //SELECT	*
                //FROM	GROUP1
                //UNION
                //SELECT	*
                //FROM	GROUP2
                //ORDER BY SPC_ITEMNAME,GROUPMAIN,GROUPSUB";
                sql = $@"
                            ;WITH TMP AS(
                            SELECT	INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,ZONE ,UNITID
                            FROM	SHOP_RO_ITEM WITH (NOLOCK) 
                                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                            WHERE   GROUPMAIN IN ('IS0602','IS0079','IS0044')
                            UNION
                            SELECT	INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,ZONE ,UNITID
                            FROM	SHOP_RO_ITEM WITH (NOLOCK) 
		                            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                            WHERE	GROUPMAIN = 'IS0098'
		                            AND GROUPSUB NOT LIKE  'GR01'
                            )
                            SELECT	*
                            FROM	TMP
                            WHERE	TMP.ITEMBARCODE IN (
						                            SELECT	ITEMBARCODE
						                            FROM	SHOP_RO_MNROHD WITH (NOLOCK)
								                            INNER JOIN SHOP_RO_MNRODT WITH (NOLOCK)    ON SHOP_RO_MNRODT.DOCNo = SHOP_RO_MNROHD.DOCNo
						                            WHERE	STADOC = '1' AND STAAPV = '1' AND STA = '1'
								                            AND CONVERT(VARCHAR,DATE,23) = '{dDate}')
                            ORDER BY TMP.GROUPMAIN, TMP.GROUPSUB DESC ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาบิล MNRR
        public static string GetData_MNRR_HD(string date1, string date2, string bchID, string staApv)
        {
            string conBchID = "";
            if (bchID != "") conBchID = $@" AND MNRRBranch = '{bchID}' ";
            string conApv = " AND MNRRUpAX IN ('0','1') ";
            if (staApv == "0") conApv = "  AND MNRRUpAX = '0'  ";
            string sqlSelect = $@"			
                    SELECT	MNRRUpAX AS STAUPAX ,
					        MNRRBranchTO+'-'+CASE WHEN ISNULL(SHOP_BRANCH.BRANCH_NAME,'')='' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END AS BRANCH_ID,
					        MNRRDocNo AS DOCNO,
                            CONVERT(varchar,MNRRDate,23) AS DATE,MNRRBranch AS INVENT,
		                    MNRRUserCode+'-'+MNRRNAMECH AS WHOINS,
		                    Grand AS BILLAMOUNT,'1' AS BOXQTY,MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END AS REMARK,'1' AS PRINTCOUNT,		
		                    CASE MNRRUpAX WHEN '1' THEN MNRRWhoApv+'-'+EMPLTABLE.SPC_NAME  ELSE '' END AS WHOAX,
		                    CASE MNRRUpAX WHEN '1' THEN CONVERT(VARCHAR,MNRRDateApv,23)  ELSE '' END AS DATEAX,
                            CASE MNRRStaDoc WHEN '3' THEN '1' ELSE '0' END AS STADOC ,MNRRStaPrcDoc AS StaPrcDoc  
                    FROM	Shop_MNRR_HD WITH (NOLOCK)
						          LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  SHOP_BRANCH.BRANCH_ID
                                  INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' ANd DIMENSIONS.DIMENSIONCODE = '0'
                                  LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON Shop_MNRR_HD.MNRRWhoApv = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'   
                    WHERE	MNRRStaDoc = '1' AND CONVERT(varchar,MNRRDate,23)  BETWEEN '{date1}' AND '{date2}'  {conApv} {conBchID}
                    ORDER BY MNRRDocNo DESC  ";
            return sqlSelect;
        }
        public static string GetData_MNRR_DT(string docno)
        {
            string sqlSelect = $@"
                    SELECT	Shop_MNRR_DT.MNRRDocNo AS DOCNO,MNRRBarcode AS ITEMBARCODE,MNRRName AS SPC_ITEMNAME,MNRRGroup AS GROUPMAIN,
		                    MNRRGroupSub AS GROUPSUB,MNRRQtyUnitID AS UNITID,MNRRPrice AS SALEPRICE,MNRRQtyOrder  AS QTY,MNRRPriceSum AS LINEAMOUNT,
                            MNRRBranchTO AS BRANCH_ID,CONVERT(varchar,MNRRDate,23) AS DATE,MNRRBranch AS INVENT,
                            Shop_MNRR_HD.MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END AS REMARK,
		                    MNRRItemID AS ITEMID,MNRRItemDim AS INVENTDIMID    , '' AS WHOIDPACKING
                    FROM	Shop_MNRR_DT WITH (NOLOCK)
		                    INNER JOIN Shop_MNRR_HD WITH (NOLOCK) ON Shop_MNRR_DT.MNRRDOCNO = Shop_MNRR_HD.MNRRDOCNO  
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER  WITH (NOLOCK)   ON Shop_MNRR_DT.MNRRGroup = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID   

                    WHERE	MNRRSta = '1'    AND Shop_MNRR_DT.MNRRDocNo = '{docno}' AND MNRRQtyOrder <> 0  
                    ORDER BY MNRRGroup  ";
            return sqlSelect;
        }

        public static DataTable GetCostCenter()
        {
            string sql = $@"
                SELECT 	SPC_INVENTCOSTCENTERID AS DATA_ID,
                        SPC_INVENTCOSTCENTERID + ' - ' + SPC_INVENTCOSTCENTERNAME AS DATA_DESC 
                FROM 	SHOP2013TMP.dbo.SPC_InventCostCenter WITH (NOLOCK) 
                WHERE   DATAAREAID = 'SPC' 
                ORDER BY  SPC_INVENTCOSTCENTERID  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static string GetData_BillAXAddJOB(string Pcase = "")
        {
            string sql = $@"
                        SELECT  SPC_ITEMBARCODE AS BARCODE,SPC_ITEMNAME AS ITEMNAME, (SPC_QTY)*(-1) AS QTY ,SPC_ITEMBARCODEUNIT AS Unit
                        FROM    SHOP2013TMP.dbo.INVENTJOURNALTRANS WITH (NOLOCK) 
                        WHERE   INVENTJOURNALTRANS.DATAAREAID = 'SPC' AND JournalId = @Docno ";

            if (Pcase == "PXE") //Bill PXE
            {
                sql = $@"            
                        SELECT  PurchLINE.BARCODE AS BARCODE, PurchLINE.NAME AS ITEMNAME, PurchLINE.QTYORDERED AS QTY, PurchLINE.PURCHUNIT AS Unit
                        FROM    SPC704SRV.AX50SP1_SPC.dbo.PurchLINE WITH(NOLOCK)
                        WHERE   PurchLINE.PURCHID = @Docno ";
            }

            return sql;
        }


        //ค้นหาข้อมูล การสั่งของโรงไม้
        public static DataTable OrderWood_QueryDataOrder(string pStatus, string DateStart, string DateEnd)
        {
            #region "OLDCODE"
            //     string sql = string.Format(@"  SELECT SHOP_MNOM_HD.DOCNO,CAST(DATECREATE AS date) AS DATECREATE
            //                ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS)
            //  ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD
            //  ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS)
            //  ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT
            //                   ,USERCREATE,NAMECREATE,SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID
            //                ,JOBGROUPSUB_DESCRIPTION,BRANCH_ID,DIMENSIONS.DESCRIPTION AS DIMENSIONSNAME
            //                   ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME,VENDID,ISNULL(VENDTABLE.[NAME],'') AS ACCOUNTNAME
            //                   ,STADOC,SHOP_MNOM_HD.[STATUS] AS [STATUS],PRINTCOUNT,CASE WHEN PRINTCOUNT > 0 THEN 1 ELSE 0 END AS PRINTCOUNT2
            //                   ,WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION] AS [DESCRIPTION],CASE WHEN STADOC = '3' THEN '0' ELSE APPROVESTATUS END AS APPROVESTATUS  
            //                   ,STATUSSENDTOAX,NAMESENDTOAX,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
            //               FROM SHOP_MNOM_HD WITH (NOLOCK) 
            //               INNER JOIN [SHOP_MNOM_DT] WITH (NOLOCK) ON SHOP_MNOM_HD.DOCNO = [SHOP_MNOM_DT].DOCNO
            //               INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
            //               LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
            //               LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID
            //               LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON SHOP_MNOM_HD.VENDID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = 'SPC' 
            //               WHERE [STATUS] = '{2}' AND SHOP_MNOM_DT.QTY > 0
            //               AND CONVERT(nvarchar,[DATECREATE],23) BETWEEN '{0:yyyy-MM-dd}' AND '{1:yyyy-MM-dd}'  
            //               GROUP BY 
            //               SHOP_MNOM_HD.DOCNO,
            //DATECREATE,
            //DATECREATE,
            //SHOP_MNOM_HD.DATEUPD,
            //USERCREATE,NAMECREATE,
            //SHOP_MNOM_HD.JOBGROUPSUB_ID
            //,JOBGROUPSUB_DESCRIPTION
            //,BRANCH_ID,DIMENSIONS.DESCRIPTION 
            //               ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME 
            //               ,VENDID,VENDTABLE.[NAME]
            //               ,STADOC,SHOP_MNOM_HD.[STATUS]
            //               ,PRINTCOUNT
            //               ,WAIT_STATUS
            //               ,SHOP_MNOM_HD.[DESCRIPTION]
            //               ,STADOC,APPROVESTATUS
            //               ,STATUSSENDTOAX,NAMESENDTOAX
            //               ,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
            //               ORDER BY SHOP_MNOM_HD.DATECREATE DESC", DateStart, DateEnd, pStatus);
            #endregion

            string pConDate = "  AND STADOC = '1'  AND  SHOP_MNOM_HD.[RECEIVEWHOINS] = ''  ";
            if (DateStart != "") pConDate = $@" AND CONVERT(nvarchar,[DATECREATE],23) BETWEEN '{DateStart}' AND '{DateEnd}'   ";


            string sql = $@"
                 SELECT SHOP_MNOM_HD.DOCNO,CAST(DATECREATE AS date) AS DATECREATE,
		                CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS) ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD,
		                CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS) ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT,
		                USERCREATE,NAMECREATE,SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID,JOBGROUPSUB_DESCRIPTION,SHOP_MNOM_HD.BRANCH_ID,
		                IIF((ISNULL(SHOP_BRANCH.BRANCH_NAME,'')=''),DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME) AS DIMENSIONSNAME,
		                COSTCENTERID,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME,VENDID,ISNULL(VENDTABLE.[NAME],'') AS ACCOUNTNAME,STADOC,SHOP_MNOM_HD.[STATUS] AS [STATUS],PRINTCOUNT,
		                CASE WHEN PRINTCOUNT > 0 THEN 1 ELSE 0 END AS PRINTCOUNT2,WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION] AS [DESCRIPTION],CASE WHEN STADOC = '3' THEN '0' ELSE APPROVESTATUS END AS APPROVESTATUS,
		                STATUSSENDTOAX,NAMESENDTOAX,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
                FROM	SHOP_MNOM_HD WITH (NOLOCK) 
		                INNER JOIN [SHOP_MNOM_DT] WITH (NOLOCK) ON SHOP_MNOM_HD.DOCNO = [SHOP_MNOM_DT].DOCNO
		                INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
		                LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)  ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID
		                LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON SHOP_MNOM_HD.VENDID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = 'SPC' 
		                LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNOM_HD.BRANCH_ID =SHOP_BRANCH.BRANCH_ID
                WHERE	[STATUS] = '{pStatus}'  AND SHOP_MNOM_DT.QTY > 0
                        {pConDate}
                GROUP BY	SHOP_MNOM_HD.DOCNO,DATECREATE,SHOP_MNOM_HD.DATEUPD,USERCREATE,NAMECREATE,
		                SHOP_MNOM_HD.JOBGROUPSUB_ID,JOBGROUPSUB_DESCRIPTION,
		                SHOP_MNOM_HD.BRANCH_ID,IIF((ISNULL(SHOP_BRANCH.BRANCH_NAME,'')=''),DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME),
		                COSTCENTERID,SPC_INVENTCOSTCENTERNAME,VENDID,VENDTABLE.[NAME],STADOC,SHOP_MNOM_HD.[STATUS],PRINTCOUNT,WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION],
		                APPROVESTATUS,STATUSSENDTOAX,NAMESENDTOAX,RECEIVER_DESCRIPTION,PXEDOCNO,RECEIVEWHOINS,RECIEVEDATEINS
                ORDER BY SHOP_MNOM_HD.DATECREATE DESC 
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหาข้อมูล การสั่ง ออก Report
        public static DataTable OrderWood_QueryDataReport(string pStatus, string DateStart, string DateEnd)
        {
            #region "OLDCODE"
            //    string sql = string.Format(@"SELECT SHOP_MNOM_HD.DOCNO, CAST(DATECREATE AS date) AS DATECREATE
            //                   ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS)
            // ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD
            // ,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS)
            // ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT
            //                     , USERCREATE, NAMECREATE,SEQNO, ITEMBARCODE, ITEMNAME, QTY
            //, CASE WHEN RECEIVEWHOINS = '' THEN 0
            //ELSE QTYEDIT END AS QTYEDIT, UNIT, AMOUNT
            //                     , SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID
            //               ,JOBGROUPSUB_DESCRIPTION,BRANCH_ID,DIMENSIONS.DESCRIPTION AS DIMENSIONSNAME
            //                  ,COSTCENTERID ,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME,VENDID,ISNULL(VENDTABLE.[NAME],'') AS ACCOUNTNAME
            //                  , STADOC, SHOP_MNOM_HD.[STATUS] AS[STATUS],PRINTCOUNT
            //,CASE WHEN PRINTCOUNT > 0 THEN 1 ELSE 0 END AS PRINTCOUNT2
            //                  ,WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION] AS[DESCRIPTION], CASE WHEN STADOC = '3' THEN '0' ELSE APPROVESTATUS END AS APPROVESTATUS  
            //                  ,[APPROVEWHOINS],[APPROVENAMEINS],[APPROVEDATEINS]  
            //                  ,STATUSSENDTOAX,WHOSENDTOAX,NAMESENDTOAX
            //,RECEIVEWHOINS,RECIEVEDATEINS,RECEIVER_DESCRIPTION,PXEDOCNO,PAYMENT
            //              FROM SHOP_MNOM_HD WITH(NOLOCK)
            //              INNER JOIN[SHOP_MNOM_DT] WITH(NOLOCK) ON SHOP_MNOM_HD.DOCNO = [SHOP_MNOM_DT].DOCNO
            //              INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
            //            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
            //            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID
            //            LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON SHOP_MNOM_HD.VENDID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = 'SPC'
            //            WHERE[STATUS] = '{0}' AND SHOP_MNOM_DT.QTY > 0
            //            AND CONVERT(nvarchar,[DATECREATE],23) BETWEEN '{1:yyyy-MM-dd}' AND '{2:yyyy-MM-dd}'   
            //            ORDER BY SHOP_MNOM_HD.DATECREATE DESC", pStatus, DateStart, DateEnd);

            #endregion

            string sql = $@"
                SELECT	SHOP_MNOM_HD.DOCNO, CAST(DATECREATE AS date) AS DATECREATE,CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, DATECREATE, RECIEVEDATEINS) ELSE DATEDIFF(DAY, DATECREATE, GETDATE()) END AS DATERECORD,
		                CASE WHEN RECIEVEDATEINS ! = '' THEN DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, RECIEVEDATEINS) ELSE DATEDIFF(DAY, SHOP_MNOM_HD.DATEUPD, GETDATE()) END AS DATERECORDEDIT,
		                USERCREATE, NAMECREATE,SEQNO, ITEMBARCODE, ITEMNAME, QTY, CASE WHEN RECEIVEWHOINS = '' THEN 0 ELSE QTYEDIT END AS QTYEDIT, UNIT, AMOUNT,
		                SHOP_MNOM_HD.JOBGROUPSUB_ID AS JOBGROUPSUB_ID,JOBGROUPSUB_DESCRIPTION,SHOP_MNOM_HD.BRANCH_ID,IIF((ISNULL(SHOP_BRANCH.BRANCH_NAME,'')=''),DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME) AS DIMENSIONSNAME,
		                COSTCENTERID ,SPC_INVENTCOSTCENTERNAME AS COSTCENTERNAME,VENDID,ISNULL(VENDTABLE.[NAME],'') AS ACCOUNTNAME,
		                STADOC, SHOP_MNOM_HD.[STATUS] AS[STATUS],PRINTCOUNT,CASE WHEN PRINTCOUNT > 0 THEN 1 ELSE 0 END AS PRINTCOUNT2,
		                WAIT_STATUS,SHOP_MNOM_HD.[DESCRIPTION] AS[DESCRIPTION], CASE WHEN STADOC = '3' THEN '0' ELSE APPROVESTATUS END AS APPROVESTATUS,
		                [APPROVEWHOINS],[APPROVENAMEINS],[APPROVEDATEINS],STATUSSENDTOAX,WHOSENDTOAX,NAMESENDTOAX,RECEIVEWHOINS,RECIEVEDATEINS,RECEIVER_DESCRIPTION,PXEDOCNO,PAYMENT
                FROM SHOP_MNOM_HD WITH(NOLOCK)
		                INNER JOIN[SHOP_MNOM_DT] WITH(NOLOCK) ON SHOP_MNOM_HD.DOCNO = [SHOP_MNOM_DT].DOCNO
		                INNER JOIN SHOP_JOBGROUPSUB WITH (NOLOCK) ON SHOP_MNOM_HD.JOBGROUPSUB_ID = SHOP_JOBGROUPSUB.JOBGROUPSUB_ID
		                LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON SHOP_MNOM_HD.BRANCH_ID = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER WITH (NOLOCK) ON SHOP_MNOM_HD.COSTCENTERID = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID
		                LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON SHOP_MNOM_HD.VENDID = VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = 'SPC'
		                LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNOM_HD.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                WHERE	[STATUS] = '{pStatus}' AND SHOP_MNOM_DT.QTY > 0
		                AND CONVERT(nvarchar,[DATECREATE],23) BETWEEN '{DateStart}' AND '{DateEnd}'   
                ORDER BY SHOP_MNOM_HD.DATECREATE DESC ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
