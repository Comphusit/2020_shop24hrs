﻿using PC_Shop24Hrs.Controllers;
using System.Data;


namespace PC_Shop24Hrs.Class
{
    class PurchClass
    {
        //GetPXE
        public static DataTable FindData_PurchTable(string purchID)
        {
            string sql = $@"
                SELECT		PurchTable.PurchStatus,PurchTable.PurchId,OrderAccount,PurchName,InventLocationId,CONVERT(VARCHAR,PurchTable.SPC_PurchDate,23) AS SPC_PurchDate,
			                PurchTable.DIMENSION,PurchTable.SPC_DOCUMENTNUM AS DOCUMENTNUM 
                FROM		SHOP2013TMP.dbo.PurchTable WITH (NOLOCK) 
                WHERE		PurchTable.PurchId = '{purchID}' ";

            //DataTable dt = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportAX); if (dt.Rows.Count == 0) return ConnectionClass.SelectSQL_MainAX(sql); else return dt;
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาเลขที่ใบสั่งซื้อ
        public static DataTable GetDetailPD_ByDocno(string docno)
        {
            string sql = $@"
            SELECT	PurchTable.PurchStatus,PurchTable.PurchId,OrderAccount,PurchName,InventLocationId,CONVERT(VARCHAR,PurchTable.SPC_PurchDate,23) AS SPC_PurchDate,PurchTable.DIMENSION,RETURNACTIONID
		            LINENUM, BarCode, Name, PurchQty, PurchUnit, PurchPrice, LineAmount, SPC_Remarks,DIMENSIONS.DESCRIPTION
            FROM    dbo.PurchTable WITH(NOLOCK) 
		            INNER JOIN dbo.PurchLINE WITH(NOLOCK) ON PurchTable.PurchId = PurchLINE.PurchId
		            INNER JOIN dbo.DIMENSIONS WITH(NOLOCK) ON PurchTable.DIMENSION = DIMENSIONS.NUM AND DIMENSIONCODE = '0'  AND DIMENSIONS.DATAAREAID = 'SPC'
            WHERE	PurchTable.PurchId = '{docno}'
            ORDER BY LINENUM";
            DataTable dt = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportAX);
            if (dt.Rows.Count == 0) dt = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainAX); return dt;
        }
        //ค้นหา BA จาก Invoice
        public static DataTable FarmHouse_GetBA(string condition, string BA_MA)//string pInvoice
        {
            string sql = $@"
                    SELECT	INVOICEID,NUMRECEIVE,INVOICEAMOUNT  
                    FROM	Spc_shipcarrierinvoice WITH (NOLOCK) 
                    WHERE	NUMRECEIVE LIKE '{BA_MA}%' {condition} ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportAX);
        }
        //ค้นหา BA จาก Invoice
        public static DataTable FarmHouse_GetBA708(string invoice)//string pInvoice
        {
            string sql = $@"
                    SELECT	INVOICEID,NUMRECEIVE,INVOICEAMOUNT  
                    FROM	Spc_shipcarrierinvoice WITH (NOLOCK) 
                    WHERE	INVOICEID = '{invoice}' ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainAX);
        }
        //ค้นหาเอกสาร MA ใน AX
        public static DataTable CheckStatusAX_SPC_SHIPCARRIERINVOICE(string MABill)
        {
            string sql = $@"
                DECLARE @MA NVARCHAR(50) = '{MABill}';

                SELECT      INVOICEID ,NUMRECEIVE ,LOCKED ,cast(INVOICEAMOUNT as decimal(10,2)) as INVOICEAMOUNT,RECID,ISNULL(PURCHID,'') AS PURCHID
                FROM        SPC_SHIPCARRIERINVOICE  WITH (NOLOCK)
                WHERE       NUMRECEIVE = @MA AND DATAAREAID = N'SPC' ";

            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        //ค้นหาเลขที่ PB ตามที่ระบุ
        public static DataTable FarmHouse_GetPB(string pPB, string pVenderID)
        {
            string sql = $@"
                    SELECT  * 
                    FROM    SPC_PurchBillTable WITH (NOLOCK) 
                    where   PURCHBILLID = '{pPB}'  and VENDCODE = '{pVenderID}' ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        //ค้นหา ใบ MNPR ทั้งหมด ของ MN
        public static DataTable FarmHouse_GetPR_MN(string pDate1, string pDate2, string pVenderID)
        {
            string inventLocationId = " AND InventLocationId != '' ", orderby = " ORDER BY	VendInvoiceJour.INVOICEDATE,InventLocationId ";
            if (pVenderID == "V005450")
            {
                inventLocationId = $@" AND InventLocationId LIKE 'MN%' ";
                orderby = " ORDER BY	InventLocationId,VendInvoiceJour.INVOICEDATE ";
            }

            string sql = $@"
                    SELECT	LedgerVoucher,InventLocationId,VendInvoiceJour.INVOICEID,CONVERT (varchar,VendInvoiceJour.INVOICEDATE,23) AS INVOICEDATE ,
                            CONVERT(varchar, VendInvoiceJour.DUEDATE, 23) AS DUEDATE,FORMAT(VendInvoiceJour.INVOICEAMOUNT ,'N2') AS INVOICEAMOUNT, DOCUREF.NAME, IVZ_Remark 
                            ,FORMAT(SumTax,'N2') AS SumTax ,VendInvoiceJour.PurchId,FORMAT(VendInvoiceJour.SALESBALANCE,'N2') AS   SALESBALANCE
                    FROM	VendInvoiceJour WITH (NOLOCK)	  
                            INNER JOIN  DOCUREF WITH (NOLOCK)	ON VendInvoiceJour.RECID = DOCUREF.REFRECID  AND REFTABLEID = '491' AND NAME != '' 
                                AND DOCUREF.NAME NOT LIKE 'CN_*' AND DOCUREF.NAME NOT LIKE 'CANCEL_*'
                            INNER JOIN PurchTable WITH (NOLOCK)  ON VendInvoiceJour.PURCHID = PurchTable.PURCHID  
                    WHERE VendInvoiceJour.INVOICEACCOUNT = '{pVenderID}' AND VendInvoiceJour.INVOICEDATE BETWEEN '{pDate1}' AND '{pDate2}' 
                            {inventLocationId} AND VendInvoiceJour.DATAAREAID = N'SPC'  AND PurchTable.DATAAREAID = N'SPC' 
                    {orderby} 
            ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        //ค้นหา ใบ MNPR ทั้งหมด ของ Retail
        public static DataTable FarmHouse_GetPR_Retail(string pDate1, string pDate2, string pVenderID)
        {
            string sql = $@" 
            SELECT	LedgerVoucher,InventLocationId,VendInvoiceJour.INVOICEID,CONVERT (varchar,VendInvoiceJour.INVOICEDATE,23) AS INVOICEDATE ,
                            CONVERT(varchar, VendInvoiceJour.DUEDATE, 23) AS DUEDATE,FORMAT(VendInvoiceJour.INVOICEAMOUNT ,'N2') AS INVOICEAMOUNT, DOCUREF.NAME, IVZ_Remark 
                            ,FORMAT(SumTax,'N2') AS SumTax ,VendInvoiceJour.PurchId,FORMAT(VendInvoiceJour.SALESBALANCE,'N2') AS   SALESBALANCE
            FROM	VendInvoiceJour WITH (NOLOCK)	  
                    INNER JOIN  DOCUREF WITH (NOLOCK)	ON VendInvoiceJour.RECID = DOCUREF.REFRECID AND REFTABLEID = '491'  AND NAME != '' 
                        AND DOCUREF.NAME NOT LIKE 'CN_*' AND DOCUREF.NAME NOT LIKE 'CANCEL_*'
                    INNER JOIN PurchTable WITH (NOLOCK)  ON VendInvoiceJour.PURCHID = PurchTable.PURCHID  
            WHERE VendInvoiceJour.INVOICEACCOUNT = '{pVenderID}' AND VendInvoiceJour.INVOICEDATE BETWEEN '{pDate1}' AND '{pDate2}' 
                    AND InventLocationId = 'RETAILAREA' AND LedgerVoucher LIKE 'PR%'  AND VendInvoiceJour.DATAAREAID = N'SPC'  AND PurchTable.DATAAREAID = N'SPC' 
            ORDER BY	InventLocationId,VendInvoiceJour.INVOICEDATE 
            ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        //ค้นหา PXS ทั้งหมด
        public static DataTable FarmHouse_Get_PXS(string pDate1, string pDate2, string pVenderID)
        {
            string sql = $@"
            SELECT  LedgerVoucher, InventLocationId, VendInvoiceJour.INVOICEID, CONVERT(varchar, VendInvoiceJour.INVOICEDATE, 23) AS INVOICEDATE,
                    CONVERT(varchar, VendInvoiceJour.DUEDATE, 23) AS DUEDATE,FORMAT(VendInvoiceJour.INVOICEAMOUNT,'N2') AS INVOICEAMOUNT, DOCUREF.NAME, IVZ_Remark
                    ,FORMAT(SumTax,'N2') AS SumTax, VendInvoiceJour.PurchId,FORMAT(VendInvoiceJour.SALESBALANCE,'N2') AS SALESBALANCE
            FROM    VendInvoiceJour WITH(NOLOCK)
                    INNER JOIN  DOCUREF WITH(NOLOCK)   ON VendInvoiceJour.RECID = DOCUREF.REFRECID AND REFTABLEID = '491'
                    INNER JOIN PurchTable WITH(NOLOCK)  ON VendInvoiceJour.PURCHID = PurchTable.PURCHID
            WHERE   VendInvoiceJour.INVOICEACCOUNT = '{pVenderID}' AND VendInvoiceJour.INVOICEDATE BETWEEN '{pDate1}' AND '{pDate2}'
                    AND LedgerVoucher LIKE 'PXS%'  AND VendInvoiceJour.DATAAREAID = N'SPC'  AND PurchTable.DATAAREAID = N'SPC'
            ORDER BY    LedgerVoucher
            ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        //ค้นหารายละเอียด PD ที่ระบุ 
        public static DataTable FarmHouse_GetPDDetail(string pPdDocno, string pVenderID)
        {
            string str = $@" 
            SELECT	    PurchTable.PURCHID,BarCode,NAME,FORMAT(SUM(PurchQty*-1),'N2') AS QTY,PurchUnit,FORMAT(SPC_PriceGroup3,'N2')  AS SPC_PriceGroup3
            FROM	    PurchTable	WITH (NOLOCK) 
                        INNER JOIN PurchLine	WITH (NOLOCK)  ON PurchTable.PURCHID = PurchLine.PURCHID 
                        INNER JOIN INVENTITEMBARCODE WITH (NOLOCK) ON PurchLine.BarCode = INVENTITEMBARCODE.ITEMBARCODE 
                            AND  INVENTITEMBARCODE.DATAAREAID = N'SPC'  
            WHERE	    PurchTable.PURCHID IN ( '{pPdDocno}') 
                        AND PurchTable.DATAAREAID = N'SPC' AND  PurchLine.DATAAREAID = N'SPC' AND INVOICEACCOUNT = '{pVenderID}' 
            GROUP BY	PurchTable.PURCHID,BarCode,NAME,PurchUnit,SPC_PriceGroup3 ";
            return ConnectionClass.SelectSQL_MainAX(str);
        }
        //ค้นหาข้อมูลบิลที่ป้อนไว้ทั้งหมด
        public static DataTable VenderAX_GetDataPR(string ServerConnect, string _branch = "", string _date = "", string _vendorid = "", string _ledgerid = "")
        {
            string str = $@"
                        SELECT	LINENUM,PURCHTABLE.INVENTLOCATIONID,LEDGERVOUCHER,VENDINVOICEJOUR.INVOICEACCOUNT,VENDINVOICEJOUR.PURCHID, 
                                VENDINVOICEJOUR.INVOICEID, CONVERT(VARCHAR, DUEDATE, 23) AS DUEDATE, InvoiceAmount, SALESBALANCE,
                                ITEMID, INVENTDIMID, SPC_ITEMBARCODE, NAME, VENDINVOICETRANS.QTY, INVENTQTY, PURCHUNIT,'' AS WhoIns,'' AS WhoInsName 
                        FROM    dbo.VENDINVOICEJOUR WITH(NOLOCK) INNER JOIN 
                                dbo.VENDINVOICETRANS WITH(NOLOCK) ON VENDINVOICEJOUR.PurchId = VENDINVOICETRANS.PurchId
                                AND VENDINVOICEJOUR.InvoiceId = VENDINVOICETRANS.InvoiceId
                                AND VENDINVOICEJOUR.InvoiceDate = VENDINVOICETRANS.InvoiceDate
                                AND VENDINVOICEJOUR.NUMBERSEQUENCEGROUP = VENDINVOICETRANS.NUMBERSEQUENCEGROUP
                                AND VENDINVOICEJOUR.INTERNALINVOICEID = VENDINVOICETRANS.INTERNALINVOICEID INNER JOIN 
                                dbo.PURCHTABLE WITH(NOLOCK) ON VENDINVOICEJOUR.PurchId = PURCHTABLE.PurchId
                        WHERE   VENDINVOICEJOUR.NUMBERSEQUENCEGROUP != 'CANCEL_AP'
                                AND VENDINVOICEJOUR.DATAAREAID = 'SPC' 
                                AND VENDINVOICETRANS.DATAAREAID = 'SPC'  
                                AND PURCHTABLE.DATAAREAID = 'SPC' ";

            if (!_ledgerid.Equals("")) { str += @"AND LEDGERVOUCHER = '" + _ledgerid + @"'"; }
            if (!_branch.Equals("")) { str += @"AND PURCHTABLE.INVENTLOCATIONID = '" + _branch + @"'"; }
            if (!_date.Equals("")) { str += @"AND VENDINVOICEJOUR.InvoiceDate = '" + _date + @"'"; }
            if (!_vendorid.Equals("")) { str += @"AND VENDINVOICEJOUR.INVOICEACCOUNT = '" + _vendorid + @"'"; }

            str += @" ORDER BY LINENUM";
            return ConnectionClass.SelectSQL_SentServer(str, ServerConnect);  //704or708
        }
        //ค้นหาบิลเช็คสถานะ
        public static bool VenderAX_GetDataPRCheckCancle(string _ledgerid, string ServerConnect)
        {
            string str = $@"
                        SELECT	PURCHTABLE.INVENTLOCATIONID,LEDGERVOUCHER,VENDINVOICEJOUR.INVOICEACCOUNT,VENDINVOICEJOUR.PURCHID, 
                                VENDINVOICEJOUR.INVOICEID, CONVERT(VARCHAR, DUEDATE, 23) AS DUEDATE, INVOICEAMOUNT, SALESBALANCE
                        FROM    dbo.VENDINVOICEJOUR WITH(NOLOCK)  INNER JOIN 
                                dbo.PURCHTABLE WITH(NOLOCK) ON VENDINVOICEJOUR.PURCHID = PURCHTABLE.PURCHID
                        WHERE   VENDINVOICEJOUR.NUMBERSEQUENCEGROUP = 'CANCEL_AP'
                                AND VENDINVOICEJOUR.DATAAREAID = 'SPC' 
                                AND PURCHTABLE.DATAAREAID = 'SPC'
                                AND LEDGERVOUCHER = '{_ledgerid}' ";
            DataTable dt = ConnectionClass.SelectSQL_SentServer(str, ServerConnect);  //704or708
            if (dt.Rows.Count > 0) return true; else return false;
        }
        //ค้นหารายละเอียดทั้งหมดก่อนพิม
        public static DataTable VenderAX_GetdataPrint(string _branchid, string _datereceive, string _invoiceid, string _ledgerid)
        {
            string str = $@"
                        SELECT	SHOP_BRANCH.BRANCH_ID,BRANCH_NAME, LEDGERVOUCHER,PURCHID,InvoiceAmount,ReciveInvoice, INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID
		                        ,SPC_ITEMBARCODE,ISNULL(TMP.SPC_ITEMNAME,'') AS NAME,  SUM(Shop_VenderPN.QTY) AS QTY,SUM(Shop_VenderPN.INVENTQTY) AS INVENTQTY
		                        ,ISNULL(QTY_BRANCH/INVENTITEMBARCODE.QTY,0) AS QTY_BRANCH
		                        ,ISNULL(QTY_SUPC/INVENTITEMBARCODE.QTY,0) AS QTY_SUPC,PURCHUNIT
		                        ,ISNULL(ReciveWhoIns,'') AS ReciveWhoIns
		                        ,ReciveWhoNameIns AS SPC_NAME 
		                        ,INVOICEACCOUNT,[SHOW_NAME] AS INVOICEACCOUNTNAME
                        FROM	SHOP_VENDERPN WITH (NOLOCK) 
                                INNER JOIN  SHOP_BRANCH WITH (NOLOCK) ON Shop_VenderPN.INVENTLOCATIONID = SHOP_BRANCH.BRANCH_ID 
                                INNER JOIN  SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON Shop_VenderPN.INVOICEACCOUNT = [SHOP_CONFIGBRANCH_GenaralDetail].SHOW_ID AND TYPE_CONFIG = '2' 
                                INNER JOIN  SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_VenderPN.SPC_ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE LEFT OUTER JOIN 

			                        (	SELECT	ITEMID,INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME, ReciveQtyBch
						                        ,ReciveQtyBch*INVENTITEMBARCODE.QTY AS QTY_BRANCH
						                        ,ReciveQtySupc
						                        ,ReciveQtySupc*INVENTITEMBARCODE.QTY  AS QTY_SUPC
						                        ,UnitID, ReciveWhoIns, ReciveWhoNameIns 
				                        FROM 	Shop_VenderCheck WITH (NOLOCK) INNER JOIN 
						                        SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_VenderCheck.ReciveItemBarcode = INVENTITEMBARCODE.ITEMBARCODE 
						                        
				                        WHERE	ReciveVender = '{_invoiceid}' 
						                        AND ReciveBranchID = '{_branchid}' 
						                        AND ReciveDate = '{_datereceive}' 
						                        AND INVENTITEMBARCODE.DATAAREAID = 'SPC' )TMP ON INVENTITEMBARCODE.ITEMID = TMP.ITEMID 
				                        AND INVENTITEMBARCODE.INVENTDIMID = TMP.INVENTDIMID 
				                        AND INVENTITEMBARCODE.UnitID = TMP.UnitID  
			 
                        WHERE	Shop_VenderPN.INVOICEACCOUNT  = '{_invoiceid}'
			                        AND Shop_VenderPN.INVENTLOCATIONID = '{_branchid}'
			                        AND Shop_VenderPN.ReciveDate = '{_datereceive}' 
			                        AND LEDGERVOUCHER = '{_ledgerid}'  
			                        AND Shop_VenderPN.QTY > 0 
                        GROUP BY SHOP_BRANCH.BRANCH_ID,BRANCH_NAME, LEDGERVOUCHER,PURCHID,InvoiceAmount,ReciveInvoice, INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID
		                            ,SPC_ITEMBARCODE,ISNULL(TMP.SPC_ITEMNAME,'') ,  INVOICEACCOUNT,[SHOW_NAME] 
			                        ,ISNULL(QTY_BRANCH/INVENTITEMBARCODE.QTY,0) 
		                            ,ISNULL(QTY_SUPC/INVENTITEMBARCODE.QTY,0) ,PURCHUNIT
		                            ,ISNULL(ReciveWhoIns,'') 
		                            ,ReciveWhoNameIns   ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //Order โรงไม้เชิงทะเล
        public static DataTable OrderWood_GetPXE(string pDocNo, string status)
        {
            string sql;
            if (status == "0")
            {
                sql = $@"
                SELECT	PURCHTABLE.PURCHSTATUS,PURCHTABLE.PURCHID,ORDERACCOUNT,PURCHNAME,INVENTLOCATIONID, 
                        ITEMID AS ITEMID,INVENTDIMID AS ITEMDIM,CONVERT(VARCHAR,PURCHTABLE.SPC_PURCHDATE,23) AS SPC_PURCHDATE,PURCHTABLE.DIMENSION, 
                        PURCHTABLE.SPC_DOCUMENTNUM AS DOCUMENTNUM,RETURNACTIONID,LINENUM,BARCODE AS ITEMBARCODE,NAME AS ITEMNAME,PURCHQTY AS QTY,PURCHUNIT AS UNIT,
                        PURCHPRICE AS PRICE,LINEAMOUNT AS AMOUNT,SPC_REMARKS 
                FROM	SHOP2013TMP.dbo.PURCHTABLE WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.PURCHLINE WITH (NOLOCK) ON PURCHTABLE.PURCHID = PURCHLINE.PURCHID 
                WHERE	PURCHTABLE.PURCHID = '{pDocNo}' ";

                //return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportAX);
                return ConnectionClass.SelectSQL_Main(sql);
            }
            else
            {
                sql = $@"
                SELECT	PURCHTABLE.PURCHSTATUS,PURCHTABLE.PURCHID,ORDERACCOUNT,PURCHNAME,INVENTLOCATIONID, 
                        ITEMID AS ITEMID,INVENTDIMID AS ITEMDIM,CONVERT(VARCHAR,PURCHTABLE.SPC_PURCHDATE,23) AS SPC_PURCHDATE,PURCHTABLE.DIMENSION, 
                        PURCHTABLE.SPC_DOCUMENTNUM AS DOCUMENTNUM,RETURNACTIONID,LINENUM,BARCODE AS ITEMBARCODE,NAME AS ITEMNAME,PURCHQTY AS QTY,PURCHUNIT AS UNIT,
                        PURCHPRICE AS PRICE,LINEAMOUNT AS AMOUNT,SPC_REMARKS 
                FROM	SHOP2013TMP.dbo.PURCHTABLE WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.PURCHLINE WITH (NOLOCK) ON PURCHTABLE.PURCHID = PURCHLINE.PURCHID 
                WHERE	PURCHTABLE.PURCHID = '{pDocNo}' ";

                //return ConnectionClass.SelectSQL_MainAX(sql);
                return ConnectionClass.SelectSQL_Main(sql);
            }
        }
        //ค้นหาใบสั่งซื้อที่เปิดค้างไว้ ตามบาร์โค้ด ref BY n'Aum
        public static DataTable ProductRecive_GetPurchPO(string pCase, string strBarcode, string pOpenType)//string barcode, double qty
        {
            string sql = "";
            switch (pCase)
            {
                case "0":
                    string openType = $@" AND PURCHLINE.PURCHSTATUS = '1' ";
                    if ((pOpenType == "1") && (SystemClass.SystemComMinimart == "1")) openType = "";

                    sql = $@"
                    SELECT  PURCHTABLE.ITEMBUYERGROUPID,PURCHLINE.VENDACCOUNT,PURCHNAME,
                            CONVERT(VARCHAR,PURCHLINE.SPC_PURCHDATE,23) AS SPC_PURCHDATE,PURCHTABLE.PURCHID,
                            PURCHLINE.ITEMID,PURCHLINE.INVENTDIMID,PURCHLINE.BARCODE,PURCHLINE.NAME,
                            PURCHLINE.PURCHQTY,INVENTITEMBARCODE.QTY AS FACTOR,PURCHLINE.PURCHUNIT,PURCHLINE.QTYORDERED,PURCHLINE.INVENTTRANSID,PURCHLINE.PURCHSTATUS,
                            ISNULL(QTYRECIVE,0) AS QTYRECIVE,PURCHLINE.PURCHQTY -   ISNULL(QTYRECIVE,0) AS QTYRECIVE_AMOUNT,
		                    ISNULL(QTYRECIVEALL,0) AS QTYRECIVEALL,PURCHLINE.QTYORDERED - ISNULL(QTYRECIVEALL,0) AS QTYRECIVEALL_AMOUNT,
                            REPLACE(INVENTDIM.CONFIGID,'''','') AS CONFIGID,REPLACE(INVENTDIM.INVENTSIZEID,'''','') AS INVENTSIZEID,REPLACE(INVENTDIM.INVENTCOLORID,'''','') AS INVENTCOLORID,
                            CASE PURCHLINE.PURCHSTATUS 
			                    WHEN '1' THEN 'เปิดค้างไว้'	
			                    WHEN '2' THEN 'ได้รับแล้ว'
			                    WHEN '3' THEN 'ออกใบแจ้งหนี้' END AS STADESC
                    FROM	SHOP2013TMP.dbo.PURCHTABLE WITH (NOLOCK) 
                            INNER JOIN SHOP2013TMP.dbo.PURCHLINE WITH (NOLOCK) ON PURCHTABLE.PURCHID = PURCHLINE.PURCHID  AND PURCHTABLE.DATAAREAID = PURCHLINE.DATAAREAID 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK) ON PURCHLINE.INVENTDIMID = INVENTDIM.INVENTDIMID	AND PURCHLINE.DATAAREAID = INVENTDIM.DATAAREAID 
		                    INNER JOIN
                                (
                                SELECT	INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.ITEMBARCODE,INVENTDIM.CONFIGID,INVENTDIM.INVENTSIZEID,
                                        INVENTDIM.INVENTCOLORID,INVENTITEMBARCODE.DATAAREAID 
                                FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
                                        INNER JOIN  SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK) ON INVENTITEMBARCODE.INVENTDIMID = INVENTDIM.INVENTDIMID AND INVENTITEMBARCODE.DATAAREAID = INVENTDIM.DATAAREAID
                                WHERE	INVENTITEMBARCODE.DATAAREAID = N'SPC'
                                        AND INVENTITEMBARCODE.ITEMBARCODE IN ({strBarcode})
                                ) AS BARCODE ON PURCHLINE.ITEMID = BARCODE.ITEMID
                                AND INVENTDIM.CONFIGID = BARCODE.CONFIGID
                                AND INVENTDIM.INVENTSIZEID = BARCODE.INVENTSIZEID
                                AND INVENTDIM.INVENTCOLORID = BARCODE.INVENTCOLORID
                                AND PURCHLINE.DATAAREAID = BARCODE.DATAAREAID
                            LEFT OUTER JOIN 
		                    (
			                    SELECT	INVENTTRANSID,SUM(QTYRECIVE) AS QTYRECIVE,SUM(QTYRECIVEALL) AS QTYRECIVEALL
			                    FROM	SupcAndroid.dbo.ProductRecive_LOGPO WITH (NOLOCK)
			                    GROUP BY INVENTTRANSID
		                    )ProductRecive_LOGPO ON PURCHLINE.INVENTTRANSID = ProductRecive_LOGPO.INVENTTRANSID
                            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON PURCHLINE.BARCODE = INVENTITEMBARCODE.ITEMBARCODE AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                    WHERE   PURCHTABLE.DATAAREAID = N'SPC'
                            AND PURCHLINE.PURCHASETYPE IN ('3','22')
                            {openType}
                            --AND PURCHLINE.PURCHSTATUS = '3'  1 เปิดค้าง  2 ได้รับแล้ว 3 ออกใบแจ้งหนี้
                            AND  PURCHTABLE.NUMBERSEQUENCEGROUP IN ( 'PXE','','PXF')
                            AND PURCHTABLE.INVENTLOCATIONID NOT LIKE 'MN%' 
                    ORDER BY PURCHTABLE.PURCHID DESC 
                ";
                    break;
                case "1":
                    sql = $@"
                    SELECT	PURCHLINE.VENDACCOUNT, 
                            CONVERT(VARCHAR,PURCHLINE.SPC_PURCHDATE,23) AS SPC_PURCHDATE,PURCHID,
                            PURCHLINE.ITEMID,PURCHLINE.INVENTDIMID,PURCHLINE.BARCODE,PURCHLINE.NAME,
                            PURCHLINE.PURCHQTY,PURCHLINE.PURCHUNIT,PURCHLINE.QTYORDERED,PURCHLINE.INVENTTRANSID,PURCHLINE.PURCHSTATUS,INVENTITEMBARCODE.QTY,
                            CASE PURCHLINE.PURCHSTATUS 
			                    WHEN '1' THEN 'เปิดค้างไว้'	
			                    WHEN '2' THEN 'ได้รับแล้ว'
			                    WHEN '3' THEN 'ออกใบแจ้งหนี้' END AS STADESC
                    FROM	SHOP2013TMP.dbo.PURCHLINE WITH (NOLOCK)
                            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON PURCHLINE.BARCODE = INVENTITEMBARCODE.ITEMBARCODE  
                    WHERE	InventTransId  = '{strBarcode}'
                            AND PURCHLINE.DATAAREAID = N'SPC' AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                ";
                    break;
                case "2":
                    sql = $@"
                    SELECT	PURCHLINE.BARCODE,PURCHLINE.PURCHID,PURCHLINE.PURCHQTY
                    FROM	SHOP2013TMP.dbo.PURCHLINE WITH (NOLOCK)
                    WHERE	VENDACCOUNT = '{strBarcode}'
                            AND PURCHLINE.DATAAREAID = N'SPC'  
		                    AND PURCHID LIKE 'PO%'
		                    AND PURCHSTATUS = '1' ";
                    break;
                case "3":
                    sql = $@"
                    SELECT	ISNULL(VendInvoiceTrans.InvoiceId,'') as BA,
                            IIF(ISNULL(VendInvoiceTrans.InvoiceId,'')='','0',DATEDIFF(DAY,CONVERT(VARCHAR,InvoiceDate,23),CONVERT(VARCHAR,DATETIMERECIVE,23))) AS DATEDIFFBA,
                            CONVERT(VARCHAR,DATETIMERECIVE,23) AS DATERECIVE,CONVERT(VARCHAR,InvoiceDate,23) AS RECIVEDATEBA,
                            ProductRecive_DROID.TRANSID,INVENTLOCATIONTO,ITEMBARCODE,SPCITEMNAME,ProductRecive_DROID.QTYRECIVE,UNITID,EMPLNAMERECIVE,DIMENSIONNAME,ProductRecive_DROID.DIMENSION,
                            VENDID,VENDNAME,VendInvoiceTrans.INTERNALINVOICEID,
                            ProductRecive_DROID.PURCHID,ProductRecive_DROID.INVENTTRANSID,BUYERID,BUYERNAME,DEVICENAME,EMPLIDRECIVE,EMPLNAMERECIVE,
                            DATEDIFF(DAY,CONVERT(VARCHAR,DATETIMERECIVE,23),CONVERT(VARCHAR,DATETIMESENDAX,23)) AS DATEDIFFAX,CONVERT(VARCHAR,DATETIMESENDAX,23) AS DATEAX
                    FROM	SupcAndroid.dbo.ProductRecive_DROID WITH (NOLOCK)
		                    INNER JOIN SupcAndroid.dbo.ProductRecive_LOGPO WITH (NOLOCK) ON ProductRecive_DROID.TRANSID = ProductRecive_LOGPO.TRANSID
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.VendInvoiceTrans	WITH (NOLOCK) 
			                    ON ProductRecive_DROID.INVENTTRANSID = VendInvoiceTrans.INVENTTRANSID 
			                    AND ProductRecive_DROID.PURCHID = VendInvoiceTrans.PURCHID
			                    AND VendInvoiceTrans.DATAAREAID = N'SPC'

                    WHERE	ITEMSTA = '1' AND SENDAXSTA != '0' 
		                    AND CONVERT(varchar,DATETIMERECIVE,23) BETWEEN '{strBarcode}'  AND '{pOpenType}'
                    ORDER BY  IIF(ISNULL(VendInvoiceTrans.InvoiceId,'')='','0',DATEDIFF(DAY,CONVERT(VARCHAR,InvoiceDate,23),CONVERT(VARCHAR,DATETIMERECIVE,23)))  DESC
                    ";
                    break;
                default:
                    break;
            }


            return ConnectionClass.SelectSQL_Main(sql);
        }

    }
}
