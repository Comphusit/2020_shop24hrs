﻿using System;
using System.Collections;
using System.Data;

namespace PC_Shop24Hrs.Class.Cust
{
    interface ICustInterface
    {
        #region "Country"
        //จังหวัด
        DataTable Get_State();
        //อำเภอ
        DataTable Get_Country(string STATE);
        //ตำบล
        DataTable Get_City(string STATE, string COUNTYID);
        //ค้นหาตำบลทั้งหมด
        DataTable Get_CityAll();
        //รหัสไปรษณีย์
        DataTable Get_Zipcode(string STATE, string COUNTYID, string CITYALIAS);
        //Get ThaiID
        ArrayList Get_IdCardReader();
        #endregion

        //ค้นหารายละเอียดลูกค้า
        DataTable FindCust_ByCustID(string _pTopCustID, string _pCustID);
        #region "Customer AX"

        //ค้นหารายละเอียดลูกค้า SPC_EXTERNALLIST
        Boolean GetEXTERNALLIST_ByIDENTIFICATIONNUMBER(string pType, string IDENTIFICATIONNUMBER);

        //Image Cust
        DataTable GetImage_ByCustID(string custID);
        #endregion

        #region "Report Customers"
        //1 รายงานรายละเอียกลูกค้าแต่ละคนที่สมัครแต่ละสาขา
        DataTable Get_CutomerRegister(string pType0Detail1Count, string Branch, string DateBegin, string DateEnd);
        // รายงานการโทรหาลูกค้า
        DataTable Get_TelOrder(string pType, string DateBegin, string DateEnd);//pType 0 GroupCst 1 CountCst 2 GroupEmp 3 CountEmp 4 History

        // บันทึกการสั่งของลูกค้า
        DataTable Get_OrderCust(string pType, string DateBegin, string DateEnd, string pBch );//pType 0 GroupCst 1 CountCst 2 GroupEmp 3 SumItem 4 HisoryPD 5 HistoryOG

        #endregion

        //Insert Custtable
        string Save_CUSTTABLE(string custId, string custName, string idCard, string phone, string address, string recID, string rmk);
        //Insert cust Tel Order
        string Save_CUSTOMER_TELORDER(string pType, string custId, string custName, string rmk);

        //ระบบแลกแต้มลูกค้า
        DataTable Get_CustRedim(string pType, string DateBegin, string DateEnd); //0 Detail 1 Sum 2 History 3 Check การแลกแต้ม องก์กร 4 check การเปลี่ยนแต้มลูกค้าในบิล
        
        // ค้นหาข้อมูลระบบลูกค้าเครดิต
        DataTable GetReport_MNHSDetail(string pDate1, string pDate2, string posGroup);

    }
}
