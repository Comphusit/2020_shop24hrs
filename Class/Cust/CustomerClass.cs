﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using Microsoft.VisualBasic;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class.Cust
{
    class CustomerClass : ICustInterface
    {
        #region "Country"
        //จังหวัด
        DataTable ICustInterface.Get_State()
        {
            return ConnectionClass.SelectSQL_Main(@"
                SELECT	distinct STATEID,NAME AS STATENAME 
                FROM	SHOP2013TMP.dbo.ADDRESSSTATE  WITH (NOLOCK)
                WHERE	DATAAREAID ='SPC' and STATEID != '' 
                ORDER BY NAME
            ");
        }
        //อำเภอ
        DataTable ICustInterface.Get_Country(string STATE)
        {
            return ConnectionClass.SelectSQL_Main($@"
                DECLARE	@state AS NVARCHAR(100) = '{STATE}';

                SELECT	COUNTYID,NAME  
                FROM	SHOP2013TMP.dbo.ADDRESSCOUNTY WITH (NOLOCK)
                WHERE	DATAAREAID ='SPC'
		                AND stateid = @state
                ORDER BY NAME
          ");
        }
        //ตำบล
        //SELECT CITYALIAS AS CITYALIAS, ZIPCODE+' : '+CITY AS CITYDESC
        //    FROM     SHOP2013TMP.dbo.ADDRESSZIPCODE WITH(NOLOCK)

        //                    INNER JOIN  SHOP2013TMP.dbo.ADDRESSSTATE WITH(NOLOCK) ON ADDRESSZIPCODE.STATE= ADDRESSSTATE.STATEID
        //     WHERE    ADDRESSZIPCODE.DATAAREAID = 'SPC' and ADDRESSSTATE.DATAAREAID = 'SPC'

        //              AND STATEID = @state

        //              AND COUNTY = @county

        //     ORDER BY CITY, ZIPCODE
        DataTable ICustInterface.Get_City(string STATE, string COUNTYID)
        {
            return ConnectionClass.SelectSQL_Main($@"
               DECLARE	@state AS NVARCHAR(100) = '{STATE}';
               DECLARE	@county AS NVARCHAR(100) = '{COUNTYID}';

               SELECT   ADDRESSZIPCODE.RECID,ZIPCODE+' : '+CITY AS CITYDESC   
               FROM	    SHOP2013TMP.dbo.ADDRESSZIPCODE  WITH (NOLOCK)
			                INNER JOIN  SHOP2013TMP.dbo.ADDRESSSTATE  WITH (NOLOCK) ON ADDRESSZIPCODE.STATE= ADDRESSSTATE.STATEID 
               WHERE	ADDRESSZIPCODE.DATAAREAID ='SPC' and ADDRESSSTATE.DATAAREAID ='SPC'   
		                AND STATEID = @state
		                AND COUNTY = @county 
               ORDER BY CITY,ZIPCODE  
            ");
        }
        //ค้นหาตำบลทั้งหมด
        DataTable ICustInterface.Get_CityAll()
        {
            return ConnectionClass.SelectSQL_Main($@"
				SELECT  ADDRESSZIPCODE.RECID,ZIPCODE,CITYALIAS,STATEID,COUNTY
                FROM	SHOP2013TMP.dbo.ADDRESSZIPCODE  WITH (NOLOCK)
			            INNER JOIN  SHOP2013TMP.dbo.ADDRESSSTATE  WITH (NOLOCK) ON ADDRESSZIPCODE.STATE= ADDRESSSTATE.STATEID 
				WHERE	ADDRESSZIPCODE.DATAAREAID ='SPC' and ADDRESSSTATE.DATAAREAID ='SPC'        
				ORDER BY CITY,ZIPCODE  
            ");
        }
        //รหัสไปรษณีย์
        //SELECT ZIPCODE
        //        FROM SHOP2013TMP.dbo.ADDRESSZIPCODE   WITH (NOLOCK)
        //        WHERE   ADDRESSZIPCODE.DATAAREAID = 'SPC'

        //                AND STATE = @state
        //                AND COUNTY = @county
        //                AND CITYALIAS = @city
        DataTable ICustInterface.Get_Zipcode(string STATE, string COUNTYID, string CITYALIAS)
        {
            return ConnectionClass.SelectSQL_Main($@"
                DECLARE	@state AS NVARCHAR(100) = '{STATE}';
                DECLARE	@county AS NVARCHAR(100) = '{COUNTYID}';
                DECLARE	@city AS NVARCHAR(100) = '{CITYALIAS}'

                SELECT	ZIPCODE   
                FROM	SHOP2013TMP.dbo.ADDRESSZIPCODE   WITH (NOLOCK)
                WHERE	ADDRESSZIPCODE.DATAAREAID ='SPC'    
		                AND STATE = @state
                        AND COUNTY = @county
                        AND RECID = @city
            ");
        }
        //Get ThaiID
        ArrayList ICustInterface.Get_IdCardReader()
        {
            ArrayList ArrayIdCard = new ArrayList();
            string ProgramPath;
            if (!(Environment.Is64BitOperatingSystem))
            { ProgramPath = @"C:\" + "Program Files"; }
            else
            { ProgramPath = @"C:\" + "Program Files (x86)"; }

            if (!(File.Exists(ProgramPath + @"\ThaiID\ThaiID.EXE")))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ยังไม่สามารถใช้งานสแกนบัตรประชาชนได้{System.Environment.NewLine}ลงโปรแกรม ThaiID ติดต่อ ComMinimark Tel 8570 ");
                return ArrayIdCard;
            }

            String Path = ProgramPath + @"\ThaiID\ThaiID.exe";
            System.Diagnostics.Process proc = new System.Diagnostics.Process
            {
                EnableRaisingEvents = false
            };

            proc.StartInfo.FileName = Path;
            proc.StartInfo.Arguments = @" /Read /Exit /PhotoOn /JPGOn /DataTXTOn/Filename='NIDCard' /Folder='C:\MyFolder' /RTNOn";
            proc.Start();
            proc.WaitForExit();

            int FileNum;
            String RTNData = string.Empty;
            FileNum = FileSystem.FreeFile();
            FileSystem.FileOpen(FileNum, @"C:\MyFolder\Data\ThaiID.RTN", OpenMode.Input);
            FileSystem.Input(FileNum, ref RTNData);
            FileSystem.FileClose(FileNum);

            if (RTNData != "0")
            {
                String TextData = string.Empty;
                String[] CardData;
                FileNum = FileSystem.FreeFile();
                FileSystem.FileOpen(FileNum, @"C:\MyFolder\Data\NIDCard_Data.TXT", OpenMode.Input);
                FileSystem.Input(FileNum, ref TextData);
                FileSystem.FileClose(FileNum);

                CardData = TextData.ToString().Split('#');
                if (CardData.Length > 0 && RTNData == "2")
                { ArrayIdCard.AddRange(CardData); }
            }

            return ArrayIdCard;
        }

        #endregion

        //ค้นหารายละเอียดลูกค้า
        DataTable ICustInterface.FindCust_ByCustID(string _pTopCustID, string _pCustID)
        {
            string str = $@" 
                SELECT  {_pTopCustID} ACCOUNTNUM, NAME, PHONE,NAMEALIAS, IDENTIFICATIONNUMBER, SPC_REMAINPOINT, 
                        ADDRESS, CREDITMAX,SPC_IMAGEPATH,PriceGroup ,PartyType,CASE PartyType WHEN '1' THEN 'บุคคล' ELSE 'องค์กร' END AS PartyTypeName,
                        ZIPCODE, COUNTY, STATE, CITY, STREET,PHONE 
                FROM    SHOP2013TMP.dbo.CUSTTABLE  WITH (NOLOCK)
                WHERE   DATAAREAID ='SPC' {_pCustID}
                ORDER BY ACCOUNTNUM ";

            return ConnectionClass.SelectSQL_Main(str);
        }

        #region "Customer AX"

        //ค้นหารายละเอียดลูกค้า SPC_EXTERNALLIST
        Boolean ICustInterface.GetEXTERNALLIST_ByIDENTIFICATIONNUMBER(string pType, string IDENTIFICATIONNUMBER)
        {
            string str = "";
            switch (pType)
            {
                case "0":
                    str = $@"
                        SELECT  *
                        FROM    SPC_EXTERNALLIST WITH (NOLOCK)
                        WHERE   TABLENAME = 'CUSTTABLE' and FIELDVALUE like N'%{IDENTIFICATIONNUMBER}%'";
                    break;
                case "1":
                    str = $@"
                        SELECT	*	 
                        FROM	SPC_REDEMPTABLE00 WITH (NOLOCK) 
                        WHERE	ACCOUNTNUM = '{IDENTIFICATIONNUMBER}'  ";
                    break;
                default:
                    break;
            }
            if (ConnectionClass.SelectSQL_MainAX(str).Rows.Count > 0) return true; else return false;
        }


        //Image Cust
        DataTable ICustInterface.GetImage_ByCustID(string custID)
        {
            string sql = $@"
            SELECT	ISNULL(IMAGEPATH,'') AS SPC_IMAGEPATH 
            FROM    SPC_CustTableImagePath WITH(NOLOCK)
            WHERE   ACCOUNTNUM = '{custID}'  AND SPC_CustTableImagePath.DATAAREAID = N'SPC'
            ORDER BY RECID DESC ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }


        #endregion

        #region "Report Customers"
        //1 รายงานรายละเอียกลูกค้าแต่ละคนที่สมัครแต่ละสาขา
        DataTable ICustInterface.Get_CutomerRegister(string pType0Detail1Count, string Branch, string DateBegin, string DateEnd)//string TypeRegis,
        {
            string conditionBranch = "";
            if (Branch != "") conditionBranch = $@" AND BRANCH ='{Branch}' ";
            string sql;
            if (pType0Detail1Count == "0")
            {
                sql = $@"
                SELECT  BRANCH_ID,BRANCH_NAME,
		                CUSTOMERNAME ,convert(varchar, DATEIN, 23) as DATEIN,REMARK,ACCOUNTNUM,
		                WHOIN,WHONAMEIN
                FROM    SHOP_CUSTTABLE WITH (NOLOCK)
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_CUSTTABLE.BRANCH = SHOP_BRANCH.BRANCH_ID
                WHERE   convert(varchar, DATEIN,23) between '{DateBegin}' and '{DateEnd}'
                        {conditionBranch}  
                ORDER BY   convert(varchar,DATEIN,23) DESC ";
            }
            else
            {
                sql = $@" 
                select  BRANCH AS BRANCH_ID, BRANCH_NAME  as BRANCH_NAME ,count(ACCOUNTNUM) as ACCOUNTNUM 
                from    Shop_CUSTTABLE with(nolock) 
                        INNER JOIN  SHOP_BRANCH  with(nolock)    on Shop_CUSTTABLE.BRANCH= Shop_Branch.BRANCH_ID
                where   convert(varchar, DATEIN,23) between '{DateBegin}' and '{DateEnd}'  
                        {conditionBranch}
                group by  BRANCH,BRANCH_NAME 
                order by  BRANCH ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        // รายงานการโทรหาลูกค้า
        DataTable ICustInterface.Get_TelOrder(string pType, string DateBegin, string DateEnd)//pType 0 GroupCst 1 CountCst 2 GroupEmp 3 CountEmp 4 History
        {
            string sql = "";
            switch (pType)
            {
                case "0":
                    sql = $@"
                        SELECT	CustID,CustName
                        FROM	SHOP_CUSTOMER_TELORDER WITH (NOLOCK) 
                        WHERE	DATEIN BETWEEN '{DateBegin}' AND '{DateEnd}'
                        GROUP BY CustID,CustName
                        ORDER BY CustID,CustName";
                    break;
                case "1":
                    sql = $@"
                        SELECT	DATEIN,CustID,COUNT(CustID) AS SumCount
                        FROM	SHOP_CUSTOMER_TELORDER WITH (NOLOCK)
                        WHERE	DATEIN BETWEEN '{DateBegin}' AND '{DateEnd}'
                        GROUP BY DATEIN,CustID ";
                    break;
                case "2":
                    sql = $@"
                        SELECT  WHOIN, WHOINNAME AS SPC_NAME
                        FROM    SHOP_CUSTOMER_TELORDER WITH(NOLOCK)
                        WHERE   DATEIN  BETWEEN '{DateBegin}' AND '{DateEnd}'
                        GROUP BY WHOIN,WHOINNAME ";
                    break;
                case "3":
                    sql = $@"
                        SELECT	DATEIN,WHOIN,COUNT(WHOIN) AS SumCount
                        FROM	SHOP_CUSTOMER_TELORDER WITH (NOLOCK)
                        WHERE	DATEIN BETWEEN '{DateBegin}'   AND '{DateEnd}' 
                        GROUP BY DATEIN,WHOIN ";
                    break;
                case "4":
                    sql = $@"
                        SELECT	'ประวัติการโทร' AS C,Remark,CONVERT(VARCHAR,DATEIN,23) + CHAR(10)+ TIMEIN AS DATEIN,WHOIN,WHOINNAME ,'2' AS STA
                        FROM	SHOP_CUSTOMER_TELORDER WITH (NOLOCK) 
                        WHERE	CustID = '{DateBegin}' 
                        ORDER BY DATEIN DESC,TIMEIN DESC ";
                    break;
                default:
                    break;
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        // บันทึกการสั่งของลูกค้า
        DataTable ICustInterface.Get_OrderCust(string pType, string DateBegin, string DateEnd, string pBch)//pType 0 GroupCst 1 CountCst 2 GroupEmp 3 SumItem 4 HisoryPD 5 HistoryOG
        {
            string sql = "";
            switch (pType)
            {
                case "0":
                    sql = $@"
                        SELECT	CUST_ID AS CustID ,CUST_NAME AS CustName 
                        FROM	SHOP_MNOG_HD WITH (NOLOCK)     
                        WHERE	DOCDATE BETWEEN '{DateBegin}' AND '{DateEnd}' 
		                        AND	STADOC = '1' AND CUST_ID IS NOT NULL AND CUST_ID != ''
                        GROUP BY CUST_ID,CUST_NAME
                        UNION
                        SELECT	CUST_ID AS CustID,CUST_NAME AS CustName 
                        FROM	SHOP_MNPD_HD WITH (NOLOCK) 
                        WHERE	DOCDATE   BETWEEN '{DateBegin}' AND '{DateEnd}' 
		                        AND StaDoc = '1' AND CUST_ID IS NOT NULL AND CUST_ID != ''
                        GROUP BY CUST_ID,CUST_NAME

                        ORDER BY CustID,CUST_NAME ";
                    break;
                case "1":
                    sql = $@"
                        SELECT	DATEIN,CustID,SUM(SumCount) AS SumCount
				        FROM	(
                        SELECT	DOCDATE AS DATEIN,CUST_ID AS CustID,COUNT(CUST_ID) AS SumCount
                        FROM	SHOP_MNOG_HD WITH (NOLOCK)     
                        WHERE	DOCDATE BETWEEN '{DateBegin}' AND '{DateEnd}' 
		                        AND	STADOC = '1' AND CUST_ID IS NOT NULL AND CUST_ID != ''
                        GROUP BY DOCDATE,CUST_ID
                        UNION
                        SELECT	DOCDATE AS DATEIN,CUST_ID AS CustID,COUNT(CUST_ID) AS SumCount
                        FROM	SHOP_MNPD_HD WITH (NOLOCK) 
                        WHERE	DOCDATE   BETWEEN '{DateBegin}'  AND '{DateEnd}' 
		                        AND StaDoc = '1' AND CUST_ID IS NOT NULL AND CUST_ID != ''
                        GROUP BY DOCDATE,CUST_ID
                        )TMP
				        GROUP BY DATEIN,CustID
                        ORDER BY DATEIN,CustID ";
                    break;
                case "2":
                    sql = $@"
                        SELECT	UserCode,DATEIN,SUM(SumCount) AS SumCount
				        FROM	(
                        SELECT	WHOINS AS UserCode,CONVERT(VARCHAR,DOCDATE,23) AS DATEIN,COUNT(CUST_ID) AS SumCount
                        FROM	Shop_MNOG_HD WITH (NOLOCK)     
                        WHERE	CONVERT(VARCHAR,DOCDATE,23) BETWEEN '{DateBegin}'  AND '{DateEnd}' 
		                        AND	StaDoc = '1' 
                        GROUP BY WHOINS,CONVERT(VARCHAR,DOCDATE,23)
                        UNION
                        SELECT  WHOINS AS UserCode,DOCDATE AS DATEIN,COUNT(CUST_ID) AS SumCount
                        FROM	SHOP_MNPD_HD WITH (NOLOCK) 
                        WHERE	DOCDATE   BETWEEN '{DateBegin}' AND '{DateEnd}' 
		                        AND StaDoc = '1'  
                        GROUP BY WHOINS,DOCDATE
                        )TMP
				        GROUP BY UserCode,DATEIN
                        ORDER BY UserCode,DATEIN ";
                    break;
                case "3":
                    string BranchSql = "";
                    if (pBch != "") BranchSql = $@" where s.Branch = '{pBch}' ";
                    sql = $@" 
                        SELECT	branch,DocDate,Cash,convert(varchar,DocDate,23) as dayDocdate,sum(Items) as Items 
                        FROM	  
                        ( 
                            SELECT	BRANCH_ID AS  Branch,DocDate,count(ITEMBARCODE) as Items, SHOP_MNPD_HD.WHOINS as Cash  
                            FROM	SHOP_MNPD_HD 
			                        INNER JOIN SHOP_MNPD_DT on SHOP_MNPD_HD.DocNo=SHOP_MNPD_DT.DocNo  
                            WHERE	SHOP_MNPD_HD.OPENBY='OrderCustByCash'  and  DocDate between '{DateBegin}' and '{DateEnd}'  
                            GROUP BY BRANCH_ID, DocDate, SHOP_MNPD_HD.WHOINS
                        UNION ALL
                            SELECT  BRANCH_ID as Branch, DOCDATE as DocDate, count(ITEMBARCODE) as Items, SHOP_MNOG_HD.WHOINS as Cash  
                            FROM	SHOP_MNOG_HD
			                        INNER JOIN SHOP_MNOG_DT  on SHOP_MNOG_HD.DOCNO = SHOP_MNOG_DT.DOCNO
                            WHERE	SHOP_MNOG_HD.OPENBY = 'OrderCustByCash' and   DOCDATE between  '{DateBegin}' and '{DateEnd}' 
                            GROUP BY BRANCH_ID, DOCDATE, SHOP_MNOG_HD.WHOINS
                        ) s
                        {BranchSql}
                        GROUP BY branch,DocDate,Cash   ";
                    break;
                case "4":
                    sql = $@"
                       SELECT	TOP 50 'ออเดอร์ทั่วไป' AS C,BRANCH_ID AS Branch,BRANCH_NAME,SHOP_MNPD_DT.DOCNO,CONVERT(VARCHAR,DocDate,23) + CHAR(10) +DocTime AS DocDate,
		                        ITEMBARCODE AS Barcode,SPC_NAME AS ItemName,QTYORDER AS Qty,UnitID, 
		                        CASE DPTID WHEN 'MN000' THEN 'Center' ELSE 'Minimart' END AS OpenBY,StaPrcDoc,LINENUM AS SeqNo, 
		                        CASE WHEN STA_SHOP IS NULL THEN '0' ELSE STA_SHOP END AS StatusShop ,SHOP_MNPD_DT.REMARK,AX_STA AS MNPCAX,TYPEPRODUCT,ITEMID,INVENTDIMID AS Dimid ,
		                        CONVERT(VARCHAR,SHOP_MNPD_DT.DATEINS,23) AS DATEIN,CONVERT(VARCHAR,SHOP_MNPD_DT.DATEINS,24) AS TIMEIN,
		                        CONVERT(VARCHAR,SHOP_MNPD_DT.DATEUPD,23) AS DATEUP,CONVERT(VARCHAR,SHOP_MNPD_DT.DATEUPD,24) AS TIMEUP ,
                                CASE WHEN STA_SHOP IS NULL THEN '0' ELSE STA_SHOP END AS STA
                        FROM	SHOP_MNPD_HD WITH (NOLOCK) 
		                        INNER JOIN SHOP_MNPD_DT WITH (NOLOCK) ON SHOP_MNPD_HD.DocNo =SHOP_MNPD_DT.DOCNO 
                        WHERE	CUST_ID = '{DateBegin}' AND QTYORDER > 0 AND StaDoc = '1' 
                        ORDER BY SHOP_MNPD_HD.DocNo DESC,LINENUM ";
                    break;
                case "5":
                    sql = $@"
                        SELECT	TOP 50	'ออเดอร์ของสด' AS C,BRANCH_ID + CHAR(10) +  BRANCH_NAME AS BRANCH,SHOP_MNOG_HD.DOCNO AS DOCNO,
		                        CONVERT(VARCHAR,DOCDATE,23) + CHAR(10) + DOCTIME AS DocDate,
		                        ITEMBARCODE AS Barcode,SPC_ITEMNAME AS ItemName,QTYORDER AS Qty,UNITID AS UnitID, 
		                        CASE DPTID WHEN 'MN000' THEN 'Center' ELSE 'Minimart' END AS OpenBY,STADOC, 
		                        CASE WHEN CUSTRECIVE_STA IS NULL THEN '0' WHEN CUSTRECIVE_STA = '0' THEN '0' ELSE '1' END MNPOINVENT,
		                        SHOP_MNOG_DT.REMARK AS REMARK,LINENUM AS MNPOSeqNo,CONVERT(VARCHAR,SHOP_MNOG_DT.DATEINS,24) AS MNPOTimeIN ,
		                        SHOP_MNOG_DT.STAAX,ITEMID AS MNPOITEMID,INVENTDIMID AS MNPODIMID ,
		                        CONVERT(VARCHAR,SHOP_MNOG_DT.DATEINS,23) AS DATEIN,CONVERT(VARCHAR,SHOP_MNOG_DT.DATEINS,24) AS TIMEIN ,
		                        CONVERT(VARCHAR,SHOP_MNOG_DT.DATEUPD,23) AS DATEUP,CONVERT(VARCHAR,SHOP_MNOG_DT.DATEUPD,24) AS  TIMEUP ,
		                        CASE CASE WHEN CUSTRECIVE_STA IS NULL THEN '0' WHEN CUSTRECIVE_STA = '0' THEN '0' ELSE '1' END WHEN '0' THEN
		                        DATEDIFF(hour,CONVERT(VARCHAR,SHOP_MNOG_DT.DATEINS,23)+' '+CONVERT(VARCHAR,SHOP_MNOG_DT.DATEINS,24),GETDATE()) ELSE
		                        DATEDIFF(hour,CONVERT(VARCHAR,SHOP_MNOG_DT.DATEUPD,23)+' '+CONVERT(VARCHAR,SHOP_MNOG_DT.DATEUPD,24),GETDATE()) END AS SUMDAY,
                                CASE WHEN CUSTRECIVE_STA IS NULL THEN '0' WHEN CUSTRECIVE_STA = '0' THEN '0' ELSE '1' END STA,
                                CASE RECIVE_STA WHEN '2' THEN 'จัดซื้อไม่รับออเดอร์/' + RECIVE_RMK ELSE '' END AS mnpoax,RECIVE_STA
                        FROM	SHOP_MNOG_HD WITH (NOLOCK)  
		                        INNER JOIN SHOP_MNOG_DT WITH (NOLOCK) ON SHOP_MNOG_HD.DOCNO =SHOP_MNOG_DT.DOCNO 
                        WHERE	CUST_ID = '{DateBegin}' AND QTYORDER > 0 AND STADOC = 1 
                        ORDER BY SHOP_MNOG_HD.DOCNO DESC,LINENUM ";

                    break;
                default:
                    break;
            }

            return ConnectionClass.SelectSQL_Main(sql);

        }

        #endregion

        //Insert Custtable
        string ICustInterface.Save_CUSTTABLE(string custId, string custName, string idCard, string phone, string address, string recID, string rmk)
        {
            string sql = $@" 
                    INSERT INTO Shop_CUSTTABLE (
                                ACCOUNTNUM, IDENTIFICATIONNUMBER,CUSTOMERNAME, ADDRESS, TEL,
                                BRANCH,RECID,REMARK, WHOIN,WHONAMEIN)   
                    VALUES      ('{custId}', '{idCard}', '{custName}','{address.Replace(",", "")}', '{phone}',
                                '{SystemClass.SystemBranchID}', '{recID}', '{rmk}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";
            return sql;
        }
        //Insert cust Tel Order
        string ICustInterface.Save_CUSTOMER_TELORDER(string pType, string custId, string custName, string rmk)
        {
            string sql = "";
            switch (pType)
            {
                case "0":
                    sql = $@"
                        INSERT INTO SHOP_CUSTOMER_TELORDER 
                        (CustID,CustName,Remark,WHOIN,WHOINNAME) values (
                        '{custId}','{custName}','{rmk}','{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}' ) ";
                    break;
                case "1":
                    sql = $@"
                        INSERT INTO SHOP_CUSTOMER_CURRENT
                                ( ACCOUNTNUM,CUSTOMERNAME,IDENTIFICATIONNUMBER,DATEBEGIN,DATEEND,WHOINS,DATEINS)
                        VALUES  ('{custId}','{custName}','{rmk}',GETDATE(),FORMAT(GETDATE(), 'yyyy-MM-dd 23:59:59'),'{3}',GETDATE() )";
                    break;
                default:
                    break;
            }
            return sql;
        }

        //ระบบแลกแต้มลูกค้า
        DataTable ICustInterface.Get_CustRedim(string pType, string DateBegin, string DateEnd) //0 Detail 1 Sum 2 History 3 Check การแลกแต้ม องก์กร 4 check การเปลี่ยนแต้มลูกค้าในบิล
        {
            string conditionBch = "";
            if (SystemClass.SystemBranchID != "MN000") conditionBch = $@" AND INVENTLOCATIONID = '{SystemClass.SystemBranchID}' ";
            string sql = "";

            switch (pType)
            {
                case "0":
                    sql = $@"
                         SELECT	CONVERT(VARCHAR,SHOP_MNFR_REDEMPTABLE.TRANSDATE,23) AS TRANSDATE,INVENTLOCATIONID,INVENTLOCATIONNAME,
		                        IDENTIFICATIONNUMBER,IDENTIFICATIONNAME,ACCOUNTNUM,SHOP_MNFR_REDEMPTABLE.REDEMPID,
		                        SHOP_MNFR_REDEMPTABLE.POINTTOTAL,POINTCUR,POINTRemain,SHOP_MNFR_REDEMPTABLE.CREATEDBY,CREATEDBYNAME,
		                        ITEMBARCODE,NAME,QTY,UNITID,SHOP_MNFR_REDEMPLINE.POINTTOTAL AS ITEMBARCODETOTALPOINT,REDEMPPOINT AS POINT
                         FROM	SHOP_MNFR_REDEMPTABLE  WITH (NOLOCK) 
		                            INNER JOIN SHOP_MNFR_REDEMPLINE  WITH (NOLOCK)  ON SHOP_MNFR_REDEMPTABLE.REDEMPID = SHOP_MNFR_REDEMPLINE.REDEMPID
                         WHERE	CONVERT(VARCHAR,SHOP_MNFR_REDEMPTABLE.TRANSDATE,23) BETWEEN '{DateBegin}'  AND '{DateEnd}'  {conditionBch}
                         ORDER BY SHOP_MNFR_REDEMPTABLE.REDEMPID ";
                    break;
                case "1":
                    sql = $@"
                        SELECT	INVENTLOCATIONID,SUM(pointTotal) AS SUMPOINT 
                        FROM	SPC_RedempTable WITH (NOLOCK) 
                        WHERE	INVENTLOCATIONID LIKE 'MN%'  {conditionBch}
                                AND TRANSDATE  BETWEEN '{DateBegin}'  AND '{DateEnd}'
                        GROUP BY INVENTLOCATIONID  
                        ORDER BY INVENTLOCATIONID";
                    break;
                case "2":
                    sql = $@"
                        SELECT	TOP 50 'ประวัติแลกแต้ม' AS C,SPC_RedempTable.INVENTLOCATIONID, SPC_RedempTable.REDEMPID,
		                        CONVERT(VARCHAR,SPC_RedempTable.TRANSDATE,23) + CHAR(10) + CONVERT(VARCHAR,SPC_RedempTable.CREATEDDATETIME,24) AS DOCDATE,
		                        ITEMBARCODE,NAME,QTY,UNITID,'2' AS STA,IDENTIFICATIONNUMBER,IDENTIFICATIONNAME
                        FROM	SPC_RedempTable WITH (NOLOCK)
		                        INNER JOIN SPC_REDEMPLINE WITH (NOLOCK) ON SPC_RedempTable.REDEMPID = SPC_REDEMPLINE.REDEMPID
                        WHERE	ACCOUNTNUM = '{DateBegin}'
		                        AND INVENTSITEID = N'SPC' AND SPC_RedempTable.DATAAREAID = N'SPC' AND SPC_REDEMPLINE.DATAAREAID = N'SPC'
                        ORDER BY CONVERT(VARCHAR,SPC_RedempTable.CREATEDDATETIME,25) DESC ";
                    break;
                case "3":
                    sql = $@"
                      SELECT    ACCOUNTNUM, DATEBEGIN, DATEEND
                      FROM      SHOP_CUSTOMER_CURRENT WITH(NOLOCK)
                      WHERE     ACCOUNTNUM = '{DateBegin}'
                                AND GETDATE() < DATEEND ";
                    break;
                case "4":
                    sql = $@" 
                        SELECT	*
                        FROM	[SHOP_SPC_CUSTPOINTHISTORY] WITH (NOLOCK)
                        WHERE	INVOICEID = '{DateBegin}'";
                    break;
                default:
                    break;
            }
            if (pType == "1") return ConnectionClass.SelectSQL_MainAX(sql); else if (pType == "2") return ConnectionClass.SelectSQL_MainAX(sql); else return ConnectionClass.SelectSQL_Main(sql);

        }

        // ค้นหาข้อมูลระบบลูกค้าเครดิต
        DataTable ICustInterface.GetReport_MNHSDetail(string pDate1, string pDate2, string posGroup)
        {
            string pConBch = "";
            if (posGroup != "") pConBch = $@" AND POSGROUP = '{posGroup} ";

            string sqlSelect0 = $@"
                    SELECT	SHOP_MNHS_HD.INVOICEID,SHOP_MNHS_HD.INVOICEACCOUNT,SHOP_MNHS_HD.NAME AS CSTNAME,  
		                    CONVERT(VARCHAR,SHOP_MNHS_HD.INVOICEDATE,23) AS INVOICEDATE,SHOP_MNHS_HD.POSGROUP AS BRANCH_ID,BRANCH_NAME,   
		                    SHOP_MNHS_HD.PERSONNELID, SHOP_MNHS_HD.PERSONNELNAME, SHOP_MNHS_HD.SALESID,WHONAME  AS SPC_NAME,
		                    ISNULL(SALEAX.BARCODE,'0000') AS BARCODEAX,
		                    ITEMBARCODE ,SHOP_MNHS_DT.NAME AS SPC_ITEMNAME,QTY,SALESUNIT

                    FROM	SHOP_MNHS_HD WITH (NOLOCK)   
		                    INNER JOIN SHOP_MNHS_DT WITH (NOLOCK) ON SHOP_MNHS_HD.INVOICEID = SHOP_MNHS_DT.INVOICEID AND SHOP_MNHS_DT.DOCUTYPE = '1' 
		                    LEFT OUTER JOIN   
     		                    (  
           		                    SELECT	SUBSTRING(SALESTABLE.SalesID,CHARINDEX('M',SALESTABLE.SalesId),16) AS DOCNO,INVENTLOCATIONID,BARCODE,
                                            SUBSTRING(INVENTTRANSID,18,LEN(INVENTTRANSID)-17) AS LINENUM
           		                    FROM	SHOP2013TMP.dbo.SALESTABLE WITH (NOLOCK) INNER JOIN SHOP2013TMP.dbo.SALESLINE WITH (NOLOCK)  
           				                    ON SALESTABLE.SalesId = SALESLINE.SalesId  
           		                    WHERE	SALESTABLE.SalesId LIKE '%MNHS%' AND SALESTABLE.SPC_SalesDate  BETWEEN '{pDate1}' AND '{pDate2}'     
           			                    AND SALESTABLE.DATAAREAID = N'SPC' AND SALESLINE.DATAAREAID = N'SPC'  
			                       )SALEAX ON SHOP_MNHS_HD.INVOICEID = SALEAX.DOCNO   AND SHOP_MNHS_DT.ITEMBARCODE = SALEAX.BARCODE
                                            AND SHOP_MNHS_DT.LINENUM =  SALEAX.LINENUM

                    WHERE	SHOP_MNHS_HD.DOCUTYPE = '1'   
		                    AND CONVERT(VARCHAR,SHOP_MNHS_HD.INVOICEDATE,23) BETWEEN '{pDate1}' AND '{pDate2}' 
		                    {pConBch}

                    ORDER BY  SHOP_MNHS_HD.INVOICEID    ";

           return ConnectionClass.SelectSQL_Main(sqlSelect0);
        }
    }
}
