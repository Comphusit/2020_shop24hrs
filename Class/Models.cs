﻿using PC_Shop24Hrs.Class.Asset;
using PC_Shop24Hrs.Class.Empl;
using PC_Shop24Hrs.Class.Vend;
using PC_Shop24Hrs.Class.Dpt;
using PC_Shop24Hrs.Class.Invent;
using PC_Shop24Hrs.Class.Cust;
using PC_Shop24Hrs.Class.Scale;
using PC_Shop24Hrs.Class.AR;
using PC_Shop24Hrs.Class.ManageCamera;

namespace PC_Shop24Hrs.Class
{
    class Models
    {
        public static IAssetClassInterface AssetClass { get; set; } = new AssetClass();
        public static IEmplClassInterface EmplClass { get; set; } = new EmplClass();
        public static IVendClassInterface VendTableClass { get; set; } = new VendTableClass();
        public static IDptClassInterface DptClass { get; set; } = new DptClass();
        public static IInventClassInterface InventClass { get; set; } = new InventClass();
        public static ICustInterface CustomerClass { get; set; } = new CustomerClass();
        public static IScaleInterface ScaleClass { get; set; } = new ScaleClass();
        public static IARInterface ARClass { get; set; } = new ARClass();
        public static IManageCameraInterface ManageCameraClass { get; set; } = new ManageCameraClass();
    }
}
