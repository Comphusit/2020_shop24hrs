﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.Class
{
    class EmployeeClass
    {

        #region EmployeeImage
        public static DataTable EmployeeCheckQuantity(string DateBegin, string DateEnd, string ConditionBranch = "", string pwType = "1")//DateBegin.AddDays(1)
        {
            string sql = $@"
                    SELECT	BRANCH_ID,BRANCH_NAME,PW_IDCARD,PW_NAME
                            ,ISNULL(convert(varchar,PW_DATETIME_IN,120),'1900-01-01') as PW_DATETIME_IN
                            ,ISNULL(convert(varchar,PW_DATETIME_OUT,120),'1900-01-01') as PW_DATETIME_OUT 
                            ,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS DESCRIPTION 
				            ,ISNULL(COUNTSCN,0) AS COUNTSCN,ISNULL(convert(varchar,DATETIMESCAN,25),'') as DATETIMESCAN
                            --,ISNULL(convert(varchar,DATEINSCAN,23),'') as DATEINSCAN
                            --,ISNULL(convert(varchar,TIMEINSCAN,24),'') as TIMEINSCAN 
                    FROM	SHOP_BRANCH with (nolock) 
                            LEFT OUTER JOIN  SHOP_TIMEKEEPER with (nolock) ON Shop_Branch.Branch_ID = Shop_TimeKeeper.PW_BRANCH_IN  AND PW_TYPE = '{pwType}'
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK)  ON Shop_TimeKeeper.PW_IDCARD = EMPLTABLE.EMPLID  
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226  with (nolock)  ON EMPLTABLE.EMPLID  = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE 
		                            AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC' AND REFERENCE <> ''
		                            AND DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE)
		                    LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID 
                            LEFT OUTER JOIN (
			                    SELECT  EMPLID,COUNT(EMPLID) AS COUNTSCN,MAX(DATETIMESCAN) AS DATETIMESCAN
                                        --,MAX(DATEIN) AS DATEINSCAN,MAX(TIMEIN) AS TIMEINSCAN
			                    FROM	SHOP_TIMEKEEPERCHECK    WITH (NOLOCK) 
			                    WHERE	convert(varchar,DATEINS,23) between '{DateBegin}' AND '{DateEnd}'
			                    GROUP BY EMPLID,SHOP_TIMEKEEPERCHECK.BRANCH_ID
			                    ) Shop_SCANFINGER  ON Shop_TimeKeeper.PW_IDCARD = Shop_SCANFINGER.EMPLID 

                    WHERE   HRPPARTYPOSITIONTABLERELAT2226.ValidToDateTime >= getdate()  
		                    AND PW_DATE_IN = '{DateBegin}' AND PW_STATUS = '1'   {ConditionBranch} 
                    ORDER BY PW_BRANCH_IN,PW_DATETIME_IN,PW_IDCARD ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable EmployeeTimeStampDetail(string DateBegin, string DateEnd, string ConditionBranch = "", string pwType = "1")
        {
            string sql = $@"
                SELECT	    SPC_PhoneMobile,BRANCH_ID,BRANCH_NAME,PW_IDCARD
                            ,CASE  WHEN EMPLTABLE.IVZ_HRPANICKNAME != '' THEN PW_NAME + ' [' + EMPLTABLE.IVZ_HRPANICKNAME + ']' ELSE PW_NAME END PW_NAME   
                            ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_IN,120),'1900-01-01') AS PW_DATETIME_IN,
                            ISNULL(CONVERT(VARCHAR,PW_DATETIME_OUT,120),'1900-01-01') AS PW_DATETIME_OUT, 
                            ISNULL(CONVERT(VARCHAR,PW_SUMDATE,120),'') AS PW_SUMDATE,ISNULL(PW_SUMTIME,'') AS PW_SUMTIME,PW_STATUS
                            ,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS [DESCRIPTION]  
                            ,ISNULL(PW_MNEP,'') AS PW_MNEP
					        ,ISNULL(PW_MNEP_Check,'0') AS PW_MNEP_CHECK
					        ,ISNULL(PW_MNEP_CheckWho,'') AS PW_MNEP_CHECKWHO
					        ,ISNULL(PW_MNEP_CheckName,'') AS PW_MNEP_CHECKNAME
		                    ,ISNULL(CONVERT(VARCHAR,PW_MNEP_CheckDate,23),'') AS PW_MNEP_CHECKDATE
		                    ,ISNULL(PW_MNEP_CheckTime,'') AS PW_MNEP_CHECKTIME
		                    ,ISNULL((CONVERT(VARCHAR,PW_MNEP_CheckDate,23) + ' ' + PW_MNEP_CheckTime),'') AS PW_MNEP_CheckDateTime
                            ,PW_OTHER,ISNULL(PW_QTYBill,0) AS PW_QTYBill,ISNULL(PW_CAR,0) AS PW_CAR 
                            ,ISNULL(PW_MONEY,0) AS PW_MONEY ,ISNULL(PW_MONEY,0) - (ISNULL(PW_MONEYUSEIN,0)+ISNULL(PW_MONEYUSEOUT,0)) AS PW_MONEYHAVE  
                            ,ISNULL(PW_MONEYUSEIN,0) AS PW_MONEYUSEIN ,ISNULL(PW_MONEYUSEOUT,0) AS PW_MONEYUSEOUT
                FROM	    SHOP_BRANCH WITH (NOLOCK)  
                            LEFT OUTER JOIN Shop_TimeKeeper WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = Shop_TimeKeeper.PW_BRANCH_IN AND PW_TYPE = '{pwType}' 
                            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON Shop_TimeKeeper.PW_IDCARD = EMPLTABLE.EMPLID 
                            LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226  WITH (NOLOCK)  ON EMPLTABLE.EMPLID  = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE 
                                    AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC' AND REFERENCE <> ''
		                            AND DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE)
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID  
                WHERE       PW_DATE_IN BETWEEN '{DateBegin}' AND '{DateEnd}'   {ConditionBranch}
		        ORDER BY    PW_BRANCH_IN,PW_DATETIME_IN ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายงานการพัก
        public static DataTable EmployeeTimeBreak(DateTime DateBegin, string ConditionBranch, string pwType = "1")
        {
            string sql = $@"
            DECLARE @DateBegin date
            SET @DateBegin = '{DateBegin:yyyy-MM-dd}'   
            

            SELECT	SHOP_BRANCH.BRANCH_ID,SHOP_BRANCH.BRANCH_NAME,Shop_TimeKeeper.PW_IDCARD
		                ,CASE  WHEN EMPLTABLE.IVZ_HRPANICKNAME != '' THEN Shop_TimeKeeper.PW_NAME + ' [' + EMPLTABLE.IVZ_HRPANICKNAME + ']' 
		                ELSE Shop_TimeKeeper.PW_NAME END PW_NAME   
		                ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_IN,120),'1900-01-01') AS PW_DATETIME_IN
		                ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_OUT,120),'1900-01-01') AS PW_DATETIME_OUT
		                ,CAST(PW_DATETIME_IN AS date) AS  PW_DATE
		                ,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS [DESCRIPTION]
		                ,E.PW_TIME_IN,E.PW_TIME_OUT,E.PW_DATE_IN,ISNULL(E.PW_COUNT,0) AS PW_COUNT,E.PW_SUMDATE,E.PW_SUMDATETIME,E.PW_SUMTIME,E.PW_REMARK,E.SPC_NAME 
		
				
                FROM	SHOP_BRANCH WITH (NOLOCK)  
		                LEFT OUTER JOIN SHOP_TIMEKEEPER WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = Shop_TimeKeeper.PW_BRANCH_IN AND PW_TYPE = '{pwType}'
		                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON Shop_TimeKeeper.PW_IDCARD = EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC'
		                INNER JOIN SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226  WITH (NOLOCK)  ON EMPLTABLE.EMPLID  = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  
                                AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC' AND REFERENCE <> '' AND DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE)	
		                INNER JOIN	SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID  
                                AND HRPPartyJobTableRelationship.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN 
	                            (
				                SELECT  Shop_TimeBreak.PW_IDCARD,Shop_TimeBreak.PW_PWCARD,Shop_TimeBreak.PW_NAME, 
		                                CONVERT(VARCHAR,Shop_TimeKeeper.PW_DATE_IN,23) AS PW_DATE_IN,
		                                CONVERT(VARCHAR,Shop_TimeBreak.PW_DATETIME_IN,25) AS PW_TIME_IN,
		                                ISNULL(CONVERT(VARCHAR,Shop_TimeBreak.PW_DATETIME_OUT,25),'') AS PW_TIME_OUT,  
		                                CASE WHEN ISNULL(Shop_TimeBreak.PW_SUMDATE,'') = '' THEN 
											CASE WHEN ISNULL( CONVERT(VARCHAR,Shop_TimeKeeper.PW_DATE_IN,23),'') = '' THEN Shop_TimeBreak.PW_SUMDATE ELSE '0' END 
										ELSE Shop_TimeBreak.PW_SUMDATE END AS PW_SUMDATE,
                                        CASE WHEN ISNULL(Shop_TimeBreak.PW_SUMTIME,'') = '' THEN 
											CASE WHEN ISNULL( CONVERT(VARCHAR,Shop_TimeKeeper.PW_DATE_IN,23),'') = '' THEN Shop_TimeBreak.PW_SUMTIME ELSE '00:00' END 
										ELSE Shop_TimeBreak.PW_SUMTIME END AS PW_SUMTIME,
		                                (CASE WHEN ISNULL(Shop_TimeBreak.PW_SUMDATE,'') = '' THEN 
											CASE WHEN ISNULL( CONVERT(VARCHAR,Shop_TimeKeeper.PW_DATE_IN,23),'') = '' THEN Shop_TimeBreak.PW_SUMDATE ELSE '0' END 
										ELSE Shop_TimeBreak.PW_SUMDATE END + ':' + CASE WHEN ISNULL(Shop_TimeBreak.PW_SUMTIME,'') = '' THEN 
											CASE WHEN ISNULL( CONVERT(VARCHAR,Shop_TimeKeeper.PW_DATE_IN,23),'') = '' THEN Shop_TimeBreak.PW_SUMTIME ELSE '00:00' END 
										ELSE Shop_TimeBreak.PW_SUMTIME END) AS PW_SUMDATETIME,Shop_TimeBreak.PW_REMARK,ISNULL(SPC_NAME,'')  AS SPC_NAME 
		                                ,ROW_NUMBER() OVER(PARTITION BY Shop_TimeBreak.PW_IDCARD,Shop_TimeBreak.PW_DATE_IN ORDER BY Shop_TimeBreak.PW_TIME_IN,Shop_TimeBreak.PW_IDCARD DESC) AS PW_COUNT

                                FROM	Shop_TimeBreak WITH (NOLOCK) 
		                                LEFT OUTER JOIN	SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON Shop_TimeBreak.PW_WHOOUT = EMPLTABLE.ALTNUM  
		                                INNER JOIN  SHOP_TIMEKEEPER WITH (NOLOCK) ON Shop_TimeBreak.PW_IDTimeKeeper = Shop_TimeKeeper.PW_ID

				                WHERE	Shop_TimeKeeper.PW_DATE_IN = @DateBegin AND SHOP_TIMEKEEPER.PW_TYPE = '{pwType}' --AND DATEADD(day,1,@DateBegin)  
				                )E  ON Shop_TimeKeeper.PW_IDCARD=E.PW_IDCARD  AND E.PW_DATE_IN = Shop_TimeKeeper.PW_DATE_IN

                WHERE       Shop_TimeKeeper.PW_DATE_IN = @DateBegin  {ConditionBranch}
                ORDER BY    BRANCH_ID,PW_IDCARD,PW_COUNT ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        #endregion

        #region EmployeeVDO
        public static DataTable EmployeeVDO(string DateBegin, string Condition = "", string pwType = "1")
        {
            string sql = $@"
                        SELECT	BRANCH_ID,BRANCH_ID +'-'+ BRANCH_NAME  as BRANCH_NAME,EMPLID , SPC_NAME as EMPLNAME,HRPPartyJobTableRelationship.DESCRIPTION AS DESCRIPTION,  
                            HRPPARTYPOSITIONTABLERELAT2226.DIMENSION  ,ISNULL(PW, 0) AS PW
                            ,ISNULL(PW_QTYBill,0) AS PW_QTYBill
                            ,CONVERT(VARCHAR, PW_DATETIME_IN, 25) AS PW_DATETIME_IN
                            ,CONVERT(VARCHAR, PW_DATETIME_OUT, 25) AS PW_DATETIME_OUT
                            ,PW_BRANCH_IN,PW_BRANCH_OUT
                        FROM SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH(NOLOCK)
                            INNER JOIN SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH(NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID
                            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.REFERENCE = EMPLTABLE.EMPLID 
                            INNER JOIN SHOP_BRANCH WITH(NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID
                            LEFT OUTER JOIN(SELECT PW_PWCARD AS PW,ISNULL(PW_QTYBill,0) AS PW_QTYBill, PW_DATETIME_IN, PW_DATETIME_OUT,PW_BRANCH_IN,PW_BRANCH_OUT
                                FROM SHOP_TIMEKEEPER WITH(NOLOCK)
                                WHERE   PW_DATE_OUT = '{DateBegin}' AND PW_TYPE = '{pwType}'
                                ) Shop_TimeKeeper     ON EMPLTABLE.ALTNUM = Shop_TimeKeeper.PW
                        WHERE REFERENCE <> ''
		                    AND DATEADD(HOUR, 7, HRPPARTYPOSITIONTABLERELAT2226.VALIDTODATETIME) >= CAST(GETDATE() AS DATE)
                            AND HRPPARTYPOSITIONTABLERELAT2226.DIMENSION {Condition}
		                    AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC'
                            AND HRPPartyJobTableRelationship.DATAAREAID = 'SPC'
                            AND EMPLTABLE.DATAAREAID = 'SPC'
                            AND HRPPARTYPOSITIONTABLERELAT2226.ORGANIZATIONUNITID = HRPPARTYPOSITIONTABLERELAT2226.DIMENSION
                        ORDER BY HRPPARTYPOSITIONTABLERELAT2226.DIMENSION,EMPLID  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable EmployeeTimeKeeper(string DateBegin, string DateEnd, string Condition, int tmp = 0, string pwType = "1")
        {
            string sql = $@"
                    SELECT		EMPLTABLE.EMPLID,spc_name AS EMPLNAME,EMPLTABLE.DIMENSION AS BRANCH_ID,BRANCH_NAME
				                ,PW_PWCARD AS PW,ISNULL(PW_QTYBill,0) AS PW_QTYBill, Convert(varchar,PW_DATETIME_IN,23) AS PW_DATETIME_IN, Convert(varchar,PW_DATETIME_OUT,23) AS PW_DATETIME_OUT,PW_BRANCH_IN,PW_BRANCH_OUT
				                ,HRPPartyJobTableRelationship.DESCRIPTION AS [DESCRIPTION]
	                FROM		SHOP_TIMEKEEPER WITH(NOLOCK)  
                                INNER JOIN  SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON EMPLTABLE.ALTNUM = Shop_TimeKeeper.PW_PWCARD  
                                INNER JOIN  SHOP_BRANCH WITH(NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID 
                                INNER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH(NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.REFERENCE = EMPLTABLE.EMPLID  
						                AND  HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC' AND REFERENCE <> ''
						                AND DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE)    
                                INNER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH(NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID
	
	                WHERE       EMPLTABLE.DATAAREAID = 'SPC' AND PW_TYPE = '{pwType}' 
                                AND HRPPARTYPOSITIONTABLERELAT2226.ORGANIZATIONUNITID = HRPPARTYPOSITIONTABLERELAT2226.DIMENSION
                                AND HRPPARTYPOSITIONTABLERELAT2226.DIMENSION  {Condition}
							    AND PW_DATE_OUT BETWEEN '{DateBegin}' AND '{DateEnd}' ";
            if (tmp == 0) sql = $@"SELECT EMPLID,EMPLNAME,BRANCH_ID,BRANCH_NAME,[DESCRIPTION] FROM({sql})TMP group by EMPLID,EMPLNAME,BRANCH_ID,BRANCH_NAME,[DESCRIPTION]";
            sql += @"ORDER BY BRANCH_ID,EMPLID";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable EmployeeBranch(string Branch)
        {
            string sql = $@"
                    SELECT BRANCH_ID, BRANCH_ID +' - ' + BRANCH_NAME  AS BRANCHNAME, EMPLID, SPC_NAME AS EMPLNAME, HRPPartyJobTableRelationship.DESCRIPTION AS DESCRIPTION,EMPLTABLE.DIMENSION AS DIMENSION
                    FROM    SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) INNER JOIN
                            SHOP2013TMP.dbo.DIMENSIONS WITH(NOLOCK) ON DIMENSIONS.NUM = EMPLTABLE.DIMENSION  INNER JOIN
                            SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH(NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.REFERENCE = EMPLTABLE.EMPLID
                                AND DATEADD(HOUR, 7, HRPPARTYPOSITIONTABLERELAT2226.VALIDTODATETIME) >= CAST(GETDATE() AS DATE) INNER JOIN
                            SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH(NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID INNER JOIN
                            SHOP_BRANCH WITH(NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID AND BRANCH_STAOPEN = '1'
                    WHERE HRPPARTYPOSITIONTABLERELAT2226.DIMENSION {Branch}
                            AND REFERENCE != ''
                            AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC'
                            AND HRPPartyJobTableRelationship.DATAAREAID = 'SPC'
                            AND EMPLTABLE.DATAAREAID = 'SPC'
                            AND DIMENSIONCODE = 0
                            AND IVZ_HRPARESIGNATIONDATE = '1900-01-01 00:00:00.000'
                            AND HRPPARTYPOSITIONTABLERELAT2226.ORGANIZATIONUNITID = HRPPARTYPOSITIONTABLERELAT2226.DIMENSION
                    ORDER BY DIMENSION,EMPLID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable EmployeeDept(string Dept, string branch, string DateBegin, string DateEnd, string condition = "", string pwType = "1")
        {
            if (pwType == "1")
            {
                if (Dept == "D054" || Dept == "D088") condition += $@" AND EMPLTABLE.DIMENSION = '{Dept}' ";
                else if (Dept == "") condition += $@" AND EMPLTABLE.DIMENSION NOT LIKE 'MN%' AND  EMPLTABLE.DIMENSION != 'D054' AND  EMPLTABLE.DIMENSION != 'D088' ";
            }

            //else condition = Dept;

            string sql = string.Format($@"
                SELECT	    PW_IDCARD,PW_NAME,DIMENSIONS.NUM AS DIMENSIONS,DIMENSIONS.DESCRIPTION AS DIMENSIONSNAME,SHOP_BRANCH.BRANCH_ID AS BRANCH_ID,SHOP_BRANCH.BRANCH_NAME AS BRANCH_NAME
		                    ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_IN,120),'1900-01-01') as PW_DATETIME_IN, 
                            TMP.BRANCH_ID AS IDOUT,TMP.BRANCH_NAME AS NAMEOUT,ISNULL(CONVERT(VARCHAR,PW_DATETIME_OUT,120),'1900-01-01') as PW_DATETIME_OUT, 
                            ISNULL(CONVERT(VARCHAR,PW_SUMDATE,120),'') as PW_SUMDATE,ISNULL(PW_SUMTIME,'') as PW_SUMTIME, 
                            PW_STATUS ,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS DESCRIPTION ,SPC_PhoneMobile 
                            ,PW_MNEP,PW_MNEP_CHECK,PW_MNEP_CHECKWHO,PW_MNEP_CHECKNAME,CONVERT(VARCHAR,PW_MNEP_CheckDate,23) as PW_MNEP_CHECKDATE 
		                    ,PW_MNEP_CHECKTIME ,PW_OTHER,ISNULL(PW_QTYBill,0) AS PW_QTYBill,ISNULL(PW_CAR,0) AS PW_CAR  
                            ,ISNULL(PW_MONEY,0) AS PW_MONEY ,ISNULL(PW_MONEY,0) - (ISNULL(PW_MONEYUSEIN,0)+ISNULL(PW_MONEYUSEOUT,0)) AS PW_MONEYHAVE   
                            ,ISNULL(PW_MONEYUSEIN,0) AS PW_MONEYUSEIN ,ISNULL(PW_MONEYUSEOUT,0) AS PW_MONEYUSEOUT 
                            ,ISNULL(convert(VARCHAR,PW_DATETIME_OUT,23),'1900-01-01') AS DATEOUTT , PW_PWCARD 
                FROM	    SHOP_TIMEKEEPER WITH (NOLOCK) 
                            LEFT OUTER JOIN SHOP_BRANCH  WITH (NOLOCK) ON  Shop_TimeKeeper.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID  
                            LEFT OUTER JOIN 
                            (   SELECT	BRANCH_ID,BRANCH_NAME 
                                FROM SHOP_BRANCH WITH (NOLOCK))TMP ON Shop_TimeKeeper.PW_BRANCH_OUT  = TMP.BRANCH_ID 
                            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK)  ON Shop_TimeKeeper.PW_IDCARD = EMPLTABLE.EMPLID 
                            LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226  with (NOLOCK)  ON EMPLTABLE.EMPLID  = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  
                            LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID 
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONCODE = 0 AND DIMENSIONS.DATAAREAID = 'SPC' 
                WHERE       PW_DATE_IN BETWEEN '{DateBegin}' AND '{DateEnd}' 
		                    {condition} --AND EMPLTABLE.DIMENSION  
                            AND SHOP_BRANCH.BRANCH_ID {branch}
                            AND EMPLTABLE.DATAAREAID = 'SPC' 
                            AND PW_TYPE = '{pwType}'
		                    AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC' 
		                    AND HRPPartyJobTableRelationship.DATAAREAID = 'SPC' 
		        ORDER BY    DIMENSIONS.NUM,SHOP_BRANCH.BRANCH_ID,PW_IDCARD,PW_DATETIME_IN");

            return ConnectionClass.SelectSQL_Main(sql);
        }
        #endregion
        //ค่าคอม
        #region Commission
        //ค่าคอมแลกแต้ม  ต้องขึ้นอยู่กับตำแหน่งด้วย
        public static DataTable CommissionRedemp(string Y_NEW, string M_NEW, string condition_branch)
        {
            string sqlEmp = $@"
            DECLARE	@Y_NEW	AS NVARCHAR(10) = '{Y_NEW}'
            DECLARE	@M_NEW	AS NVARCHAR(10) = '{M_NEW}'
            DECLARE	@Bch	AS NVARCHAR(500) = '{condition_branch}'

            SELECT	TMP1.INVENTLOCATIONID AS BRANCH_ID,TMP1.INVENTLOCATIONNAME AS BRANCH_NAME,TMP1.CREATEDBY,TMP1.CREATEDBYNAME,
		            ISNULL(TMP2.COUNT_CST,0) AS NEW_CST,ISNULL(TMP1.COUNT_REDEMPID,0) AS NEW_REDEM,ISNULL(TMP1.SUM_POINT,0) AS NEW_POINT,
                    ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS POSSITION
            FROM	(
		            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,CREATEDBYNAME,COUNT(REDEMPID) AS COUNT_REDEMPID,SUM(POINTCUR) AS SUM_POINT
		            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
		            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
		            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,CREATEDBYNAME
		            )TMP1 LEFT OUTER JOIN 
		            (
		            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,COUNT(ACCOUNTNUM) AS COUNT_CST
		            FROM	(
				            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,ACCOUNTNUM
				            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
				            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
				            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,ACCOUNTNUM
				            )TMP
		            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY
		            )TMP2 ON TMP1.CREATEDBY = TMP2.CREATEDBY AND TMP1.INVENTLOCATIONID = TMP2.INVENTLOCATIONID
                    LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)   
					        ON TMP1.CREATEDBY = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  and HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID  = 'SPC' 
					        AND  DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE) 
                    LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK) 
					        ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID and HRPPartyJobTableRelationship.DATAAREAID  = 'SPC'
            WHERE	TMP1.INVENTLOCATIONID LIKE @Bch
            ORDER BY TMP1.INVENTLOCATIONID,TMP1.CREATEDBY
            ";
            return ConnectionClass.SelectSQL_Main(sqlEmp);
        }
        //ค่าคอมพนักงานลงสินค้า
        public static DataTable CommissionReciveBox(string pDate1, string pDate2, string pConBch, string typeReport)
        {

            string sqlSum = $@"
                        SELECT	ROW_NUMBER() OVER ( 	ORDER BY DIMENSIONS.NUM,Shop_EmpDownLO.EmplID   )  AS ROWSNUMBER,
                                Shop_EmpDownLO.EmplID,EmplName,DIMENSIONS.NUM + '-' +DESCRIPTION AS EmplDept,ROUND(SUM(EmplSum),0) as EmplSum 
                        FROM	SHOP_EMPDOWNLO WITH (NOLOCK) 
                                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK)  ON 	[Shop_EmpDownLO].EmplID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
							    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                        WHERE	RoundDate BETWEEN  '{pDate1}' AND  '{pDate2}' {pConBch}
                                AND ISNULL(TypeReport,'0') = '{typeReport}' 
                        GROUP BY 	Shop_EmpDownLO.EmplID,EmplName,DIMENSIONS.NUM,DESCRIPTION 
                        HAVING  ROUND(SUM(EmplSum),0) > 0
                        ORDER BY DIMENSIONS.NUM,Shop_EmpDownLO.EmplID ";

            return ConnectionClass.SelectSQL_Main(sqlSum);
        }
        //ค่าคอมของ ออเดอร์ลูกค้า
        public static DataTable CommissionGetCash_ByBranch(string DateBegin, string DateEnd, string Branch)
        {
            string SqlCondition = $@" AND DIMENSION = '{Branch}' ";
            if (Branch == "") SqlCondition = $@" AND DIMENSION  like 'MN%' ";

            string sql = $@" select distinct  DIMENSION as Branch,DIMENSIONS.DESCRIPTION as BRANCH_NAME ,
                Cash as EMPLID , SPC_NAME  as EMPLNAME ,ALTNUM from     
                   (       
               	        select  distinct BRANCH_ID,SHOP_MNPD_HD.WHOINS as Cash  
                        FROM    SHOP_MNPD_HD WITH (NOLOCK) right join  SHOP_MNPD_DT WITH (NOLOCK) on SHOP_MNPD_HD.DocNo = SHOP_MNPD_DT.DocNo         
               	        WHERE   DocDate between '{DateBegin}' and '{DateEnd}'  
                                AND isnull(COMMISSIONPD,'0') !='1'  

               	 union all    

               	        SELECT  distinct POSGROUP as Branch,SHOP_MNPD_HD.cashierid as Cash  
                        FROM    SHOP_MNPD_HD WITH (NOLOCK)  right join  SHOP_MNPD_DT WITH (NOLOCK) on SHOP_MNPD_HD.DocNo=SHOP_MNPD_DT.DocNo         
               	        WHERE   INVOICEDATE between '{DateBegin}' and '{DateEnd}'    
                                AND isnull(COMMISSIONPOSID,'0') !='1'  

               	 union all        
               	        SELECT  distinct BRANCH_ID as Branch,SHOP_MNOG_HD.CASHIERID  as Cash  
                        FROM    SHOP_MNOG_HD WITH (NOLOCK) left join SHOP_MNOG_DT WITH (NOLOCK) on SHOP_MNOG_HD.DocNo = SHOP_MNOG_DT.DocNo    	    
               	        WHERE   CONVERT(VARCHAR,DOCDATE,23) between '{DateBegin}' and '{DateEnd}'   
                                AND isnull(COMMISSIONOG,'0') !='1'   

               	 union all    
               	        SELECT  distinct POSGROUP as Branch,SHOP_MNOG_HD.CASHIERID  as Cash  
                        FROM    SHOP_MNOG_HD  WITH (NOLOCK) right join SHOP_MNOG_DT WITH (NOLOCK) on SHOP_MNOG_HD.DocNo =SHOP_MNOG_DT.DocNo           
               	        WHERE   INVOICEDATE between '{DateBegin}' and '{DateEnd}'    
                                AND isnull(COMMISSIONPOSID,'0') !='1'  
                   ) s       
                        inner join SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) on s.Cash=EMPLTABLE.EMPLID    
	                    inner join SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) on EMPLTABLE.DIMENSION=DIMENSIONS.NUM      
                        inner join Shop_Branch WITH (NOLOCK) on s.BRANCH_ID=Shop_Branch.BRANCH_ID       
                WHERE   cash !='' and cash is not null  
                        and EMPLTABLE.DATAAREAID ='spc' 
				        and DIMENSIONS.DATAAREAID ='spc'  
				        AND DIMENSIONCODE='0' {SqlCondition} 
                order by   DIMENSION,DIMENSIONS.DESCRIPTION ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค่าคอมของ พนักงาน OT
        public static DataTable CommissionOT(string DateBegin, string DateEnd)
        {
            string bch = "";
            if (SystemClass.SystemBranchID != "MN000") bch = $@"AND SHOP_BRANCH.BRANCH_ID  = '{SystemClass.SystemBranchID}' ";
            string sqlSum = $@"                        
                    SELECT	ROW_NUMBER() OVER (ORDER BY DIMENSIONS.NUM,PW_IDCARD ) AS ROWSNUMBER,
		                    PW_IDCARD AS EmplID,SPC_NAME AS EmplName,DIMENSIONS.NUM + '-' +DESCRIPTION AS EmplDept,
		                    SUM(
		                    IIF(PW_SUMDATE <> 0,CAST(BRANCH_CONFIGCOMMISSION_OT AS DECIMAL(9,2)),
			                    CASE	
					                    WHEN CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2)) BETWEEN '6' AND '11' THEN BRANCH_CONFIGCOMMISSION_OT/2
					                    WHEN CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2)) >= 12 THEN BRANCH_CONFIGCOMMISSION_OT
					                    ELSE '0'
			                    END))
		                    AS EmplSum

                    FROM	SHOP_TIMEKEEPER WITH (NOLOCK)
		                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_TIMEKEEPER.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID
		                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_TIMEKEEPER.PW_IDCARD = EMPLTABLE.EMPLID  
				                    AND EMPLTABLE.DATAAREAID = 'SPC'
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM 
				                    AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONCODE = '0'

                    WHERE	PW_DATE_IN BETWEEN '{DateBegin}' AND '{DateEnd}'
		                    AND PW_STATUS = '0' AND PW_TYPE = '3'
		                    AND PW_MNEP_Check = '1'
                            {bch}
                    GROUP BY PW_IDCARD,SPC_NAME,DIMENSIONS.NUM,DESCRIPTION
                    ORDER BY DIMENSIONS.NUM,PW_IDCARD ";

            return ConnectionClass.SelectSQL_Main(sqlSum);
        }
        //ค่าเบี้ยเลี้ยง+ค่าที่พัก
        public static DataTable CommissionLocation(string pTypeSum, string DateBegin, string DateEnd)//pTypeSum 0=Detail 1=Sum
        {
            string bch = "";
            if (SystemClass.SystemBranchID != "MN000") bch = $@"AND BRANCH_ID  = '{SystemClass.SystemBranchID}' ";
            //--PW_BRANCH_IN,SHOP_BRANCH.BRANCH_NAME, --SHOP_BRANCH.BRANCH_CONFIGCOMMISSION_LOCATION,
            //PW_BRANCH_IN,BRANCH_NAME,BRANCH_CONFIGCOMMISSION_LOCATION,
            string sql = $@"
                SELECT	
		                CONVERT(VARCHAR,PW_DATE_IN,23) AS PW_DATE_IN,PW_IDCARD,PW_NAME,
		                PW_SUMDATE,PW_SUMDATE + ':' + PW_SUMTIME AS PW_SUMTIME,PW_STATUS,PW_MNEP_Check,
		                PW_DATETIME_IN,PW_DATETIME_OUT,--EMPLTABLE.DIMENSION,DPT.BRANCH_NAME AS DPTNAME,
                        SHOP_TIMEKEEPER.PW_BRANCH_IN,SHOP_BRANCH.BRANCH_NAME,
		                IIF(PW_SUMDATE <> 0,'10.00',CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2))) AS DAYCASE,
                        IIF(IIF(PW_SUMDATE <> 0,'10.00',CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2))) >= 10,'จ่าย','ไม่จ่าย') AS STA,DIMENSION,DPT.BRANCH_NAME AS DPTNAME

                FROM	SHOP_TIMEKEEPER WITH (NOLOCK)
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_TIMEKEEPER.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID AND SHOP_BRANCH.[BRANCH_PROVINCE] = 'A05'
		                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_TIMEKEEPER.PW_IDCARD = EMPLTABLE.EMPLID 
				                AND DATAAREAID = N'SPC' AND EMPLTABLE.DIMENSION LIKE 'MN%'
		                INNER JOIN SHOP_BRANCH DPT WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DPT.BRANCH_ID AND DPT.[BRANCH_PROVINCE] = 'A05'
                WHERE	PW_DATE_IN  BETWEEN '{DateBegin}' AND '{DateEnd}'
		                AND PW_STATUS = '0'
		                AND PW_TYPE = '1' 
                        AND PW_MNEP_Check = '1'
                        {bch}
            ";
            if (pTypeSum == "1")
            {
                sql = $@"
                SELECT	ROW_NUMBER() OVER (ORDER BY DIMENSION,PW_IDCARD ) AS ROWSNUMBER,
		                PW_IDCARD AS EmplID,PW_NAME AS EmplName, DIMENSION + '-' +DPTNAME AS EmplDept,DIMENSION,
		                IIF(SUMDAY > 9,BRANCH_CONFIGCOMMISSION_LOCATION,BRANCH_CONFIGCOMMISSION_LOCATION/2) AS EmplSum
                FROM	(
		                SELECT	DIMENSION,DPTNAME,PW_IDCARD,PW_NAME,COUNT(*) AS SUMDAY
		                FROM	(
                                {sql}
                                    AND IIF(PW_SUMDATE <> 0,'10.00',CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2))) >= 10
                                )TMP
		                GROUP BY DIMENSION,DPTNAME,PW_IDCARD,PW_NAME
                )TMPCOUNT
                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMPCOUNT.DIMENSION = SHOP_BRANCH.BRANCH_ID AND SHOP_BRANCH.[BRANCH_PROVINCE] = 'A05'    
                ";
            }
            else
            {
                sql = $@"{sql} ORDER BY PW_BRANCH_IN,PW_IDCARD ";
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //4 ค่าคอมเพิ่มของตำแหน่งแคชเชียร์
        public static DataTable CommissionCashierAddOn(string pCase, string DateBegin, string DateEnd)//pCase = 0 Detail,1 = SUM
        {
            //string sql = $@"
            //    DECLARE	@Data table (CASHIERID VARCHAR(50),SPC_NAME VARCHAR(300),SalesAmount FLOAT(53),ReturnItemAmount FLOAT(53),POSGROUP VARCHAR(10),DIMENSION VARCHAR(10))

            //    INSERT INTO @Data EXEC	[SPC707SRV].[MNPOS_SPC].dbo.[SPCN_CashierSaleMNByCashierByMonth] '{DateBegin}','{DateEnd}','' 

            //    SELECT	ROW_NUMBER() OVER (ORDER BY DIMENSION,CASHIERID ) AS ROWSNUMBER,CASHIERID AS EmplID ,SPC_NAME AS EmplName,
            //            DIMENSION,BRANCH_NAME,DIMENSION+'-'+BRANCH_NAME AS EmplDept,SUM(SalesAmount) AS SalesAmount ,
            //            CASE 
            //       WHEN SUM(SalesAmount) > 2700000 THEN '5,000'
            //       WHEN SUM(SalesAmount) > 2500000 THEN '4,500'
            //       WHEN SUM(SalesAmount) > 2000000 THEN '4,000'
            //       WHEN SUM(SalesAmount) > 1500000 THEN '3,500'
            //       WHEN SUM(SalesAmount) > 1000000 THEN '3,000'
            //       WHEN SUM(SalesAmount) >  500000 THEN '2,500'
            //       WHEN SUM(SalesAmount) >  300000 THEN '2,000'
            //       WHEN SUM(SalesAmount) >  100000 THEN '1,000'
            //      END AS EmplSum
            //    FROM	@Data TMP INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMP.DIMENSION = SHOP_BRANCH.BRANCH_ID
            //    {bch}
            //    GROUP BY CASHIERID,SPC_NAME,DIMENSION,BRANCH_NAME
            //";

            string bch = " AND EMPLTABLE.DIMENSION LIKE 'MN%'	 ";
            if (SystemClass.SystemBranchID != "MN000") bch = $@"  AND EMPLTABLE.DIMENSION = '{SystemClass.SystemBranchID}' ";
            string conSum = "";
            if (pCase == "1") conSum = " AND SalesAmount >  100000 ";

            string sql = $@"
                SELECT	ROW_NUMBER() OVER (ORDER BY EMPLTABLE.DIMENSION,EMPLID ) AS ROWSNUMBER,
		                EMPLTABLE.DIMENSION,BRANCH_NAME,EMPLTABLE.EMPLID AS EmplID,SPC_NAME AS EmplName,HRPPARTYJOBTABLERELATIONSHIP.DESCRIPTION,SalesAmount,
		                EMPLTABLE.DIMENSION+'-'+BRANCH_NAME AS EmplDept,
		                CASE 
			                WHEN (SalesAmount) > 2700000 THEN '4,000'
			                WHEN (SalesAmount) > 2500000 THEN '3,500'
			                WHEN (SalesAmount) > 2000000 THEN '3,000'
			                WHEN (SalesAmount) > 1500000 THEN '2,500'
			                WHEN (SalesAmount) > 1000000 THEN '2,000'
			                WHEN (SalesAmount) >  500000 THEN '1,500'
			                WHEN (SalesAmount) >  300000 THEN '1,000'
			                WHEN (SalesAmount) >  100000 THEN '500'
			                ELSE '0.00'
		                END AS EmplSum
                FROM	SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK)
		                INNER JOIN SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK) ON 
			                EMPLTABLE.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  AND 
			                HRPPARTYPOSITIONTABLERELAT2226.REFERENCE <> '' AND DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE) 
			                AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = 'SPC' 
		                INNER JOIN SHOP2013TMP.dbo.HRPPARTYJOBTABLERELATIONSHIP WITH (NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPARTYJOBTABLERELATIONSHIP.JOBID
			                AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = HRPPARTYJOBTABLERELATIONSHIP.DATAAREAID 
			                AND HRPPARTYJOBTABLERELATIONSHIP.DATAAREAID = 'SPC'
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID 
		                LEFT OUTER JOIN 
		                (
			                SELECT	XXX_POSTABLE.CASHIERID,SUM(XXX_POSTABLE.INVOICEAMOUNT) AS SalesAmount
			                FROM	SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK)  
			                WHERE	XXX_POSTABLE.INVOICEDATE BETWEEN  '{DateBegin}' AND  '{DateEnd}'
					                AND XXX_POSTABLE.POSGROUP LIKE 'MN%'
					                AND XXX_POSTABLE.SIGN = '1'
			                GROUP BY XXX_POSTABLE.CASHIERID
		                )TMPXXX ON EMPLTABLE.EMPLID = TMPXXX.CASHIERID

                WHERE	EMPLTABLE.ALTNUM != ''
		                AND (EMPLTABLE.IVZ_HRPASTARTDATE != '1900-01-01')
		                AND IVZ_HRPARESIGNATIONDATE = '1900-01-01'
		                {bch}
		                AND HRPPARTYPOSITIONTABLERELAT2226.IVZ_HROMPOSITIONTITLE = 03
		                AND (HRPPARTYJOBTABLERELATIONSHIP.DESCRIPTION LIKE '%ผู้ช่วยผู้จัดการ%'  OR HRPPARTYJOBTABLERELATIONSHIP.DESCRIPTION LIKE '%แคชเชียร์%' )
		                AND EMPLTABLE.DATAAREAID = 'SPC'
                        {conSum}

                ORDER BY EMPLTABLE.DIMENSION,EMPLID
 
            ";


            return ConnectionClass.SelectSQL_Main(sql);
        }
        #endregion

    }
}
