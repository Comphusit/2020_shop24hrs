﻿
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class
{

    class MRTClass
    {
        //ค้นหาข้อมูลแผนกที่ต้องใช้งาน เฉพาะที่เปิดให้ใช้งานระบบ
        public static DataTable GetDept(string Dept)
        {
            string sql = string.Format($@"
            SELECT  DISTINCT DIMENSIONS.NUM, DIMENSIONS.NUM + ' ' + DESCRIPTION as DeptName,DESCRIPTION
            FROM    ANDROID_DEPT with (nolock)
                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS with (nolock) ON ANDROID_DEPT.DEPTID= DIMENSIONS.NUM
                        AND DATAAREAID = N'SPC' AND DIMENSIONCODE = '0'
            WHERE   STATUS = '1'
                    AND ANDROID_DEPT.DEPT_USE = '{Dept}'
            ORDER BY DIMENSIONS.NUM");
            return ConnectionClass.SelectSQL_Main(sql);
        }
      

        public static DataTable GetSumItemMRTO_ByDate(string Dept, string GroupItem, string DateDoc)//, string Round
        {
            string sqlCondition = string.Empty;
            if (!(GroupItem.Equals("")))
            {
                sqlCondition = string.Format(@" AND SHOP_ITEMGROUP.ITEMGROUPID='{0}'
                    AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID='{0}'", GroupItem);
            }
          
            string sql = $@"
                    SELECT  *   FROM    
                    (
                        SELECT	SHOP_ITEMGROUP.ITEMGROUPID +' '+ SHOP_ITEMGROUP.ITEMGROUPNAME as ITEMGROUP , 
		                        SHOP_ITEMGROUPBARCODE.ITEMBARCODE, SPC_ITEMNAME, INVENTITEMBARCODE.UNITID,
		                        SUM(ISNULL(TMP_SEND.QtySend,0)) AS QtyBranch,
		                        SUM(ISNULL(TMP_CST.QtyCst,0)) AS QtyCust,
		                        SUM(ISNULL(TMP_SEND.QtySend,0)) + SUM(ISNULL(TMP_CST.QtyCst,0)) AS Qty
                        FROM	SHOP_ITEMGROUP WITH (NOLOCK)
		                        INNER JOIN SHOP_ITEMGROUPBARCODE WITH (NOLOCK) on SHOP_ITEMGROUP.ITEMGROUPID=SHOP_ITEMGROUPBARCODE.ITEMGROUPID 
				                        AND SHOP_ITEMGROUP.ITEMGROUPDEPT=SHOP_ITEMGROUPBARCODE.ITEMDEPT
		                        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) on SHOP_ITEMGROUPBARCODE.ITEMBARCODE=INVENTITEMBARCODE.ITEMBARCODE
		                        LEFT OUTER JOIN (
			                        SELECT	Barcode,SUM(Qty) AS QtySend
			                        FROM	ANDROID_ORDERTODT WITH (NOLOCK)
					                        INNER JOIN	ANDROID_ORDERTOHD  WITH (NOLOCK) ON ANDROID_ORDERTOHD.DocNo=ANDROID_ORDERTODT.DocNo
			                        WHERE	ANDROID_ORDERTOHD.Date='{DateDoc}'
					                        AND StaPrcDoc !=3 AND StaDoc=1  
					                        AND Qty > 0
					                        AND Remark NOT LIKE '%ออเดอร์ลูกค้า%'
			                        GROUP BY Barcode)TMP_SEND ON INVENTITEMBARCODE.ITEMBARCODE = TMP_SEND.Barcode
		                        LEFT OUTER JOIN (
			                        SELECT	Barcode,SUM(Qty) AS QtyCst
			                        FROM	ANDROID_ORDERTODT WITH (NOLOCK)
					                        INNER JOIN	ANDROID_ORDERTOHD  WITH (NOLOCK) ON ANDROID_ORDERTOHD.DocNo=ANDROID_ORDERTODT.DocNo
			                        WHERE	ANDROID_ORDERTOHD.Date='{DateDoc}'
					                        AND ANDROID_ORDERTOHD.ROUND='2'
					                        AND StaPrcDoc !=3 AND StaDoc=1  
					                        AND Qty > 0
					                        AND Remark LIKE '%ออเดอร์ลูกค้า%'
			                        GROUP BY Barcode)TMP_CST ON INVENTITEMBARCODE.ITEMBARCODE = TMP_CST.Barcode
		
		 
                        WHERE	ITEMDEPT = '{Dept}'  AND ITEMGROUPDEPT = '{Dept}'  
		                        {sqlCondition}
		                        AND ITEMGROUPSTATUS3 = '1'  
		                        and ITEMSTATUS1 = '1'
		                        AND DATAAREAID = N'SPC'	AND SPC_ITEMACTIVE='1'
		 
                        GROUP BY SHOP_ITEMGROUP.ITEMGROUPID , SHOP_ITEMGROUP.ITEMGROUPNAME, 
		                        SHOP_ITEMGROUPBARCODE.ITEMBARCODE, SPC_ITEMNAME, INVENTITEMBARCODE.UNITID 
                    )TMP
                    WHERE   Qty > 0
                    ORDER BY SPC_ITEMNAME, ITEMBARCODE, UNITID 
            ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ออเดอร์ลูกค้า
        public static DataTable GetOrderCust(string Dept, string Date)
        {
            string sql = $@"
                SELECT	BRANCH_ID, BRANCH_NAME AS MNOGBranch,
		                CASE ISNULL(SHOP_MNOG_DT.PROCESS_ORDER,'') WHEN '' THEN ROW_NUMBER( )   OVER(PARTITION BY BRANCH_ID,ISNULL(SHOP_MNOG_DT.PROCESS_ORDER,'') ORDER BY BRANCH_ID ASC) ELSE '0' END AS OrderSeq,
		                SHOP_MNOG_DT.ITEMBARCODE AS BARCODE , 
		                SHOP_MNOG_DT.ITEMID AS MNPOItemID, SHOP_MNOG_DT.INVENTDIMID AS MNPODimid,SHOP_MNOG_DT.SPC_ITEMNAME AS  MNPOName, 
		                SUM(QTYORDER) AS total ,SHOP_MNOG_DT.UNITID AS MNPOUnitID,ISNULL(SHOP_MNOG_DT.PROCESS_ORDER,'') AS POAX, SPC_PRICEGROUP3
 
                FROM	SHOP_MNOG_HD WITH(NOLOCK)
		                INNER JOIN SHOP_MNOG_DT WITH(NOLOCK)	ON SHOP_MNOG_HD.DOCNO = SHOP_MNOG_DT.DOCNO
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE= SHOP_MNOG_DT.ITEMBARCODE AND DATAAREAID = N'SPC'
		                
                WHERE	STADOC !=3 AND STAPRCDOC = 1  AND RECIVE_STA = '1'
		                AND RECIVE_DATE = '{Date}'
		                AND NUMID = '{Dept}'

                GROUP BY  BRANCH_ID, BRANCH_NAME,SHOP_MNOG_DT.ITEMID, 
		                SHOP_MNOG_DT.INVENTDIMID,SHOP_MNOG_DT.SPC_ITEMNAME,SHOP_MNOG_DT.ITEMBARCODE,SHOP_MNOG_DT.UNITID,SHOP_MNOG_DT.PROCESS_ORDER , SPC_PRICEGROUP3

                ORDER BY BRANCH_ID,BRANCH_NAME, 
		                ROW_NUMBER( )   OVER(PARTITION BY BRANCH_ID ORDER BY BRANCH_ID ASC)  
            ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        
        //ค้นหาสินค้าทั้งหมด ในเงื่อนไข จัดซื้อหรือ กลุ่ม
        public static DataTable GetItemBarcode_ByDeptItemGroup(string Dept, string ItemGroup)
        {
            string ConditionItemGroup = $@" AND SHOP_ITEMGROUP.ITEMGROUPID LIKE '{Dept}%' AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID LIKE '{Dept}%' ";
            if (ItemGroup != "")
                ConditionItemGroup = $@" AND SHOP_ITEMGROUP.ITEMGROUPID='{ItemGroup}' AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID='{ItemGroup}' ";

            string sql = string.Format($@"   
            SELECT  INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID, 
                    INVENTITEMBARCODE.ITEMBARCODE, INVENTITEMBARCODE.SPC_ITEMNAME ,
                    INVENTITEMBARCODE.UNITID, INVENTITEMBARCODE.QTY,
                    INVENTITEMBARCODE.SPC_PRICEGROUP3
            FROM    SHOP_ITEMGROUP WITH(NOLOCK)
                    INNER JOIN SHOP_ITEMGROUPBARCODE WITH(NOLOCK)  ON SHOP_ITEMGROUP.ITEMGROUPID = SHOP_ITEMGROUPBARCODE.ITEMGROUPID
                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE=SHOP_ITEMGROUPBARCODE.ITEMBARCODE
            WHERE   ITEMGROUPDEPT='{Dept}' AND ITEMDEPT='{Dept}'
                    AND SHOP_ITEMGROUP.ITEMGROUPSTATUS3 = '1' AND ITEMSTATUS1 = '1'
                    AND DATAAREAID='SPC' AND SPC_ITEMACTIVE='1' {ConditionItemGroup}
                    --AND SPC_ITEMBUYERGROUPID='{Dept}' 
            GROUP BY INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID, 
                    INVENTITEMBARCODE.ITEMBARCODE, INVENTITEMBARCODE.SPC_ITEMNAME ,
                    INVENTITEMBARCODE.UNITID, INVENTITEMBARCODE.QTY,
                    INVENTITEMBARCODE.SPC_PRICEGROUP3
            ORDER BY INVENTITEMBARCODE.SPC_ITEMNAME
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาจำนวนที่ทำบิล MRT แล้ว แต่ยังไม่ได้จัดสินค้า
        public static DataTable GetItemBarcodeMRTO_ByDept(string DateReceive, string Dept)
        {
            string sql = string.Format($@"
                    select  ANDROID_ORDERTODT.Barcode, BRANCH_ID , 
                            isnull(sum(ANDROID_ORDERTODT.Qty ),0) as totalQty, 
                            isnull(sum(ANDROID_ORDERTODT.QtyOrderOR),0) as totalOR,
                            isnull(sum(ANDROID_ORDERTODT.QtyOrderSaleAGV),0) as totalSale,
                            isnull(sum(ANDROID_ORDERTODT.QtyOrderTF),0) as totalTF,
                            isnull(sum(ANDROID_ORDERTODT.QtyOrderCN),0) as totalCN,
                            isnull(sum(ANDROID_ORDERTODT.QtyOrderCust),0) as Sale1 
                    from    ANDROID_ORDERTOHD WITH(NOLOCK) 
                            INNER JOIN ANDROID_ORDERTODT WITH(NOLOCK) ON ANDROID_ORDERTOHD.DocNo=ANDROID_ORDERTODT.DocNo 
                            INNER JOIN Shop_Branch WITH(NOLOCK) ON ANDROID_ORDERTOHD.Branch=Shop_Branch.BRANCH_ID 
                            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) on ANDROID_ORDERTODT.Barcode= INVENTITEMBARCODE.ITEMBARCODE
                    where   StaPrcDoc !=3 
                            AND StaApvDoc = 0
                            AND StaAx != 1 
                            AND convert(varchar,ANDROID_ORDERTOHD.Date,23) = '{DateReceive}' 
                            AND Depart='{Dept}'
                            AND DATAAREAID = 'SPC' 
                            AND SPC_ITEMACTIVE = '1' 
                            AND SPC_ITEMBUYERGROUPID = '{Dept}'
                            AND ANDROID_ORDERTOHD.Remark=''
                    group by Barcode , BRANCH_ID
                    order by Barcode, BRANCH_ID");
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //public static DataTable CheckDateItem(string Database2013,string Barcode, string Group, string GroupItem) {
        //กรณี ส่งตามยอดขาย โดยใช้วิธีการหาแบบรหัสสินค้า+มิติ เอาไปคำนวนแล้ว หาร Qty อีกทีด้วย
        public static DataTable GetQtySale_ByDptOrder(string Group, string GroupItem, string DateBegin, string DateEnd)
        {
            string sqlcondition = "";
            if (!GroupItem.Equals("")) sqlcondition = string.Format(@" AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID = '{0}'", GroupItem);

            string sql = $@"
            SELECT  BRANCH as BRANCH_ID,PT_RETAIL.ITEMID,INVENTDIMID,
		           CAST(SUM(INVENTQTY) as decimal(10,2)) AS totalOR
            FROM	spc705srv.Staging.dbo.PT_RETAIL WITH (NOLOCK)
		            INNER JOIN  (
		            SELECT  INVENTITEMBARCODE.ITEMID COLLATE SQL_Latin1_General_CP1_CI_AS AS ITEMID,
                            INVENTITEMBARCODE.INVENTDIMID  COLLATE SQL_Latin1_General_CP1_CI_AS AS INVENTDIMID, 
				            CONFIGID  COLLATE SQL_Latin1_General_CP1_CI_AS AS CONFIGID,
				            INVENTSIZEID  COLLATE SQL_Latin1_General_CP1_CI_AS AS INVENTSIZEID,
				            INVENTCOLORID  COLLATE SQL_Latin1_General_CP1_CI_AS AS INVENTCOLORID
		            FROM    SHOP_ITEMGROUP WITH(NOLOCK)
				            INNER JOIN SHOP_ITEMGROUPBARCODE WITH(NOLOCK)  ON SHOP_ITEMGROUP.ITEMGROUPID = SHOP_ITEMGROUPBARCODE.ITEMGROUPID
				            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE=SHOP_ITEMGROUPBARCODE.ITEMBARCODE AND INVENTITEMBARCODE.DATAAREAID='SPC'
				            INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK)	ON INVENTITEMBARCODE.INVENTDIMID = INVENTDIM.INVENTDIMID  AND INVENTITEMBARCODE.DATAAREAID='SPC'
		            WHERE   ITEMGROUPDEPT='{Group}' AND ITEMDEPT='{Group}'
				            AND SHOP_ITEMGROUP.ITEMGROUPSTATUS3 = '1' AND ITEMSTATUS1 = '1'
				            AND INVENTITEMBARCODE.DATAAREAID='SPC' AND SPC_ITEMACTIVE='1' 
				            AND SPC_ITEMBUYERGROUPID='{Group}'
				            {sqlcondition}
                    GROUP BY INVENTITEMBARCODE.ITEMID COLLATE SQL_Latin1_General_CP1_CI_AS,
				            CONFIGID  COLLATE SQL_Latin1_General_CP1_CI_AS,
				            INVENTITEMBARCODE.INVENTDIMID  COLLATE SQL_Latin1_General_CP1_CI_AS  ,
				            INVENTSIZEID  COLLATE SQL_Latin1_General_CP1_CI_AS ,
				            INVENTCOLORID  COLLATE SQL_Latin1_General_CP1_CI_AS
		        
		            )TMP ON PT_RETAIL.CONFIGID = TMP.CONFIGID AND PT_RETAIL.INVENTSIZEID = TMP.INVENTSIZEID AND PT_RETAIL.INVENTCOLORID = TMP.INVENTCOLORID
				            AND PT_RETAIL.ITEMID = TMP.ITEMID

            WHERE   TimePeriod between  '{DateBegin}' and '{DateEnd}' 
		            AND BRANCH LIKE 'MN%'
                    AND PT_RETAIL.INVENTSITEID = 'SPC'
            GROUP BY BRANCH,PT_RETAIL.ITEMID,INVENTDIMID
            ORDER BY BRANCH,PT_RETAIL.ITEMID,INVENTDIMID
            ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
       
        //ค้นหาจำนวนบิล MRT
        public static DataTable GetQtyMRT(string pCase_0Barcode_1ItemID, string pAvg_0Avg_1Not, string pBarcode_ItemID, int Day)
        {
            string sqlMRT;
            int dayDiff = Day; if (Day == 0) dayDiff = 1;
            if (pCase_0Barcode_1ItemID == "0")
            {
                string fName = $@" (SUM(ANDROID_POSTOLINE.QTY)/{dayDiff}) ";
                if (pAvg_0Avg_1Not == "1") fName = " SUM(ANDROID_POSTOLINE.QTY)  ";

                sqlMRT = $@"
                SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,ANDROID_POSTOLINE.ITEMBARCODE,
                        {fName} AS QTY,
		                CAST(SUM(ANDROID_POSTOLINE.QTY) as decimal(10,2)) AS QTY1,
		                CAST(SUM(ANDROID_POSTOLINE.QTY)/{dayDiff} as decimal(10,2)) AS QTYAVG
                FROM	ANDROID_POSTOLINE WITH (NOLOCK)
		                INNER JOIN ANDROID_POSTOTABLE WITH (NOLOCK)  ON ANDROID_POSTOLINE.VOUCHERID = ANDROID_POSTOTABLE.VOUCHERID
                WHERE	CONVERT(VARCHAR,ANDROID_POSTOTABLE.SHIPDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE(),23)
		                AND STADOC = '1' AND STAAX = '1'
		                AND ANDROID_POSTOLINE.ITEMBARCODE IN ({pBarcode_ItemID})
                GROUP BY INVENTLOCATIONIDTO,ANDROID_POSTOLINE.ITEMBARCODE
                ";
            }
            else if (pCase_0Barcode_1ItemID == "1")
            {
                string fName = $@" (SUM(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY)/{dayDiff}) ";
                if (pAvg_0Avg_1Not == "1") fName = " SUM(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY) ";

                sqlMRT = $@"
                SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,ANDROID_POSTOLINE.ITEMID,ANDROID_POSTOLINE.INVENTDIMID,
                        {fName} AS QTY,
		                CAST(SUM(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY) as decimal(10,2)) AS QTY1,
		                CAST(SUM(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY)/{dayDiff} as decimal(10,2)) AS QTYAVG
                FROM	ANDROID_POSTOLINE WITH (NOLOCK)
		                INNER JOIN ANDROID_POSTOTABLE WITH (NOLOCK)  ON ANDROID_POSTOLINE.VOUCHERID = ANDROID_POSTOTABLE.VOUCHERID
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ANDROID_POSTOLINE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
                                AND INVENTITEMBARCODE.DATAAREAID = 'SPC'
                WHERE	CONVERT(VARCHAR,ANDROID_POSTOTABLE.SHIPDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE(),23)
		                AND STADOC = '1' AND STAAX = '1'
		                AND ANDROID_POSTOLINE.ITEMID IN ({pBarcode_ItemID})
                GROUP BY INVENTLOCATIONIDTO,ANDROID_POSTOLINE.ITEMID,ANDROID_POSTOLINE.INVENTDIMID
                ";
            }
            else if (pCase_0Barcode_1ItemID == "3")
            {
                sqlMRT = $@"
                SELECT	TMP.*,'0' AS QTY1,CAST(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY AS decimal(10,2)) AS QTY,'0' AS QTYAVG
                FROM	(
		                SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,ANDROID_POSTOLINE.ITEMID,ANDROID_POSTOLINE.INVENTDIMID,
				                MAX(CONVERT(VARCHAR,ANDROID_POSTOTABLE.VOUCHERID,23)) AS VOUCHERID
		                FROM	ANDROID_POSTOLINE WITH (NOLOCK)
				                INNER JOIN ANDROID_POSTOTABLE WITH (NOLOCK)  ON ANDROID_POSTOLINE.VOUCHERID = ANDROID_POSTOTABLE.VOUCHERID
		                WHERE	STADOC = '1' AND STAAX = '1'
				                AND ANDROID_POSTOLINE.ITEMID IN ({pBarcode_ItemID})
		                GROUP BY INVENTLOCATIONIDTO,ANDROID_POSTOLINE.ITEMID,ANDROID_POSTOLINE.INVENTDIMID
		                )TMP 
		                INNER JOIN ANDROID_POSTOLINE WITH (NOLOCK) ON TMP.VOUCHERID = ANDROID_POSTOLINE.VOUCHERID 
					                AND TMP.INVENTDIMID = ANDROID_POSTOLINE.INVENTDIMID AND TMP.ITEMID = ANDROID_POSTOLINE.ITEMID
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ANDROID_POSTOLINE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                                    AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                ORDER BY BRANCH_ID	
                ";
            }
            else
            {
                sqlMRT = $@"
                SELECT	TMP.*,'0' AS QTY1,CAST(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY AS decimal(10,2)) AS QTY,'0' AS QTYAVG
                FROM	(
		                SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,ANDROID_POSTOLINE.ITEMBARCODE,
				                MAX(CONVERT(VARCHAR,ANDROID_POSTOTABLE.VOUCHERID,23)) AS VOUCHERID
		                FROM	ANDROID_POSTOLINE WITH (NOLOCK)
				                INNER JOIN ANDROID_POSTOTABLE WITH (NOLOCK)  ON ANDROID_POSTOLINE.VOUCHERID = ANDROID_POSTOTABLE.VOUCHERID
		                WHERE	STADOC = '1' AND STAAX = '1'
				                AND ANDROID_POSTOLINE.ITEMBARCODE IN ({pBarcode_ItemID})
		                GROUP BY INVENTLOCATIONIDTO,ANDROID_POSTOLINE.ITEMBARCODE
		                )TMP 
		                INNER JOIN ANDROID_POSTOLINE WITH (NOLOCK) ON TMP.VOUCHERID = ANDROID_POSTOLINE.VOUCHERID 
					                AND TMP.ITEMBARCODE = ANDROID_POSTOLINE.ITEMBARCODE
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ANDROID_POSTOLINE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                                    AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                ORDER BY BRANCH_ID	
                ";
            }
            return ConnectionClass.SelectSQL_Main(sqlMRT);

        }
        //ค้นหาจำนวนบิล MRT ตามวัน / สาขา / บาร์โค้ด
        public static DataTable GetQtyMRTDate(string pItemID, int Day)
        {
            string sqlMRT = $@"
                SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,ANDROID_POSTOLINE.ITEMID,ANDROID_POSTOLINE.INVENTDIMID,
                        CONVERT(VARCHAR,ANDROID_POSTOTABLE.SHIPDATE,23) AS DATE,
		                CAST(SUM(ANDROID_POSTOLINE.QTY*INVENTITEMBARCODE.QTY) AS decimal(10,2)) AS QTY
                FROM	ANDROID_POSTOLINE WITH (NOLOCK)
		                INNER JOIN ANDROID_POSTOTABLE WITH (NOLOCK)  ON ANDROID_POSTOLINE.VOUCHERID = ANDROID_POSTOTABLE.VOUCHERID
                        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ANDROID_POSTOLINE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE  AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                WHERE	CONVERT(VARCHAR,ANDROID_POSTOTABLE.SHIPDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE(),23)
		                AND STADOC = '1' AND STAAX = '1'
		                AND ANDROID_POSTOLINE.ITEMID IN ({pItemID})
                GROUP BY INVENTLOCATIONIDTO,ANDROID_POSTOLINE.ITEMID,ANDROID_POSTOLINE.INVENTDIMID,CONVERT(VARCHAR,ANDROID_POSTOTABLE.SHIPDATE,23)
                ";
            return ConnectionClass.SelectSQL_Main(sqlMRT);
        }

        //public static DataTable GetQtyMRTOrderExcel(string pCase_0Barcode_1ItemID, string pAvg_0Avg_1Not, string pBarcode_ItemID, int Day)
        //{
        //    string sqlMRT;
        //    int dayDiff = Day; if (Day == 0) dayDiff = 1;
        //    if (pCase_0Barcode_1ItemID == "0")
        //    {
        //        string fName = $@" (SUM(QtyExcel)/{dayDiff}) ";
        //        if (pAvg_0Avg_1Not == "1") fName = " SUM(QtyExcel)  ";

        //        sqlMRT = $@"
        //        SELECT	Branch AS BRANCH_ID,Barcode AS ITEMBARCODE,
        //                {fName} AS QTY,
        //          CAST(SUM(QtyExcel) as decimal(10,2)) AS QTY1,
        //          CAST(SUM(QtyExcel)/{dayDiff} as decimal(10,2)) AS QTYAVG
        //        FROM	Android_OrderTODT WITH (NOLOCK)
        //          INNER JOIN Android_OrderTOHD WITH (NOLOCK)  ON Android_OrderTODT.DocNo = Android_OrderTOHD.DocNo
        //        WHERE	CONVERT(VARCHAR,Date,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE(),23)
        //          AND STADOC = '1' 
        //          AND Barcode IN ({pBarcode_ItemID})
        //        GROUP BY Branch,Barcode
        //        ";//AND STAAX = '1'
        //    }
        //    else
        //    {
        //        string fName = $@" (SUM(Android_OrderTODT.QtyExcel*INVENTITEMBARCODE.QTY)/{dayDiff}) ";
        //        if (pAvg_0Avg_1Not == "1") fName = " SUM(Android_OrderTODT.QtyExcel*INVENTITEMBARCODE.QTY) ";

        //        sqlMRT = $@"
        //        SELECT	Branch AS BRANCH_ID,Android_OrderTODT.ItemID AS ITEMID,Android_OrderTODT.ItemDim AS INVENTDIMID,
        //                {fName} AS QTY,
        //          CAST(SUM(Android_OrderTODT.QtyExcel*INVENTITEMBARCODE.QTY) as decimal(10,2)) AS QTY1,
        //          CAST(SUM(Android_OrderTODT.QtyExcel*INVENTITEMBARCODE.QTY)/{dayDiff} as decimal(10,2)) AS QTYAVG
        //        FROM	Android_OrderTODT WITH (NOLOCK)
        //          INNER JOIN Android_OrderTOHD WITH (NOLOCK)  ON Android_OrderTODT.DocNo = Android_OrderTOHD.DocNo
        //          INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Android_OrderTODT.Barcode = INVENTITEMBARCODE.ITEMBARCODE
        //        WHERE	CONVERT(VARCHAR,Date,23) BETWEEN CONVERT(VARCHAR,GETDATE()-{Day},23) AND CONVERT(VARCHAR,GETDATE(),23)
        //          AND STADOC = '1' 
        //          AND Android_OrderTODT.ITEMID IN ({pBarcode_ItemID})
        //        GROUP BY Branch,Android_OrderTODT.ItemID,Android_OrderTODT.ItemDim
        //        ";//AND STAAX = '1'
        //    }
        //    return ConnectionClass.SelectSQL_Main(sqlMRT);
        //    //
        //}
        ////ค้นหาจำนวน คืน CN วันนี้และย้อนหลัง 1 วัน
        //public static DataTable GetCN(string pCase_0Barcode_1ItemID, string pAvg_0Avg_1Not, int Day)
        //{
        //    string sql;
        //    int dayDiff = Day; if (Day == 0) dayDiff = 1;
        //    if (pCase_0Barcode_1ItemID == "0")
        //    {
        //        string fName = $@" (SUM(QTY)/{dayDiff}) ";
        //        if (pAvg_0Avg_1Not == "1") fName = " SUM(QTY) ";

        //        sql = $@"
        //        SELECT  SHOP_MNPC_HD.Branch ,SHOP_MNPC_DT.Barcode,
        //                {fName} AS QTY,
        //          sum(QTY) AS sumQty , sum(QTY)/{dayDiff} AS QtyAVG
        //        FROM    SHOP_MNPC_HD WITH (NOLOCK) 
        //                INNER JOIN SHOP_MNPC_DT WITH (NOLOCK)  on SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo 
        //        WHERE   DocDate  between convert(varchar,getdate()-{Day},23) and convert(varchar,getdate(),23)
        //                and StaDoc ='1' and StaPrcDoc ='1' 
        //        GROUP BY SHOP_MNPC_HD.Branch , SHOP_MNPC_DT.Barcode  ";
        //    }
        //    else
        //    {
        //        string fName = $@" (SUM(QtySum)/{dayDiff})  ";
        //        if (pAvg_0Avg_1Not == "1") fName = " SUM(QtySum)  ";

        //        sql = $@"
        //        SELECT  SHOP_MNPC_HD.Branch , SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid,
        //                {fName} AS QTY,
        //          sum(QtySum) AS sumQty , sum(QtySum)/{dayDiff} AS QtyAVG
        //        FROM    SHOP_MNPC_HD WITH (NOLOCK) 
        //                INNER JOIN SHOP_MNPC_DT WITH (NOLOCK)  on SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo 
        //        WHERE   DocDate  between convert(varchar,getdate()-{Day},23) and convert(varchar,getdate(),23)
        //                and StaDoc ='1' and StaPrcDoc ='1' 
        //        GROUP BY SHOP_MNPC_HD.Branch , SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid  ";
        //    }

        //    return ConnectionClass.SelectSQL_Main(sql);
        //}

        ////check เลขที่เอกสารจากเงื่อนไข
        //public static DataTable CheckDocumentMRTO(string Dept, string Date, string round, string Branch, string groupitem)
        //{
        //    string sqlcondition = "";
        //    if (!groupitem.Equals("")) sqlcondition = string.Format($@" AND GroupItem='{groupitem}' ");
        //    string sql = string.Format($@"
        //            select  DocNo ,StaApvDoc 
        //            from    ANDROID_ORDERTOHD   WITH (NOLOCK)
        //            where   StaPrcDoc != '3' AND Depart = '{Dept}'
        //                    and convert(varchar,Date,23)='{Date}' and Round='{round}' and Branch='{Branch}'
        //        {sqlcondition}"
        //            );
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //ค้นหาสินค้าในกลุ่ม ประกบกับ MNOR
        public static DataTable GetDocumentMNOR(string Dept, string Date, string groupitem, string Branch)
        {
            string sqlConditions = "", sqlConditionsItemGroup = "";
            if (!groupitem.Equals(""))
            {
                sqlConditions = string.Format(@"AND  GROUPID ='{0}'", groupitem);
                sqlConditionsItemGroup = string.Format(@"AND SHOP_ITEMGROUP.ITEMGROUPID='{0}'
                AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID='{0}'", groupitem);
            }

            string sql = $@"
                SELECT	INVENTITEMBARCODE.ITEMID, INVENTITEMBARCODE.INVENTDIMID, 
		                ISNULL(SHOP_ITEMGROUPBARCODE.ITEMBARCODE,'-') as ITEMBARCODE, 
		                INVENTITEMBARCODE.SPC_ITEMNAME,ISNULL(QTYORDER,0) as QTYORDER, 
		                INVENTITEMBARCODE.UNITID,SPC_PRICEGROUP13,SPC_PRICEGROUP14,SPC_PRICEGROUP15,SPC_PRICEGROUP17,SPC_PRICEGROUP18,SPC_PRICEGROUP19,
		                (SELECT	BRANCH_PRICE FROM SHOP_BRANCH WITH (NOLOCK) WHERE BRANCH_ID = '{Branch}') AS PRICELAVEL

                FROM	SHOP_ITEMGROUP WITH (NOLOCK)
		                INNER JOIN SHOP_ITEMGROUPBARCODE WITH (NOLOCK) ON SHOP_ITEMGROUP.ITEMGROUPID = SHOP_ITEMGROUPBARCODE.ITEMGROUPID
		                LEFT OUTER JOIN
                        (
			                SELECT	
					                Shop_MNOR_DT.ITEMBARCODE, Shop_MNOR_DT.SPC_ITEMNAME,SUM(QTYORDER) AS QTYORDER, 
					                Shop_MNOR_DT.UNITID 
			                FROM	Shop_MNOR_DT WITH(NOLOCK)	INNER JOIN Shop_MNOR_HD WITH(NOLOCK) on Shop_MNOR_HD.DOCNO = Shop_MNOR_DT.DOCNO 
			                WHERE	QTYORDER != 0
					                AND STA_DOC != 3
					                AND STA_PRCDOC = 1
					                AND STA_APVDOC = 1
					                AND DPTID = '{Dept}'
					                AND DATE_RECIVE = '{Date}'
					                AND BRANCH_ID = '{Branch}'  {sqlConditions}
                            GROUP BY Shop_MNOR_DT.ITEMBARCODE, Shop_MNOR_DT.SPC_ITEMNAME,Shop_MNOR_DT.UNITID 
		                )MNOR	ON SHOP_ITEMGROUPBARCODE.ITEMBARCODE = MNOR.ITEMBARCODE  
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE = SHOP_ITEMGROUPBARCODE.ITEMBARCODE
			                AND DATAAREAID = N'SPC' AND SPC_ITEMACTIVE = '1' 

                WHERE	SHOP_ITEMGROUP.ITEMGROUPDEPT = '{Dept}'
                                AND SHOP_ITEMGROUPBARCODE.ITEMDEPT = '{Dept}' {sqlConditionsItemGroup}
                                AND SHOP_ITEMGROUP.ITEMGROUPSTATUS3 = '1'
                                AND SHOP_ITEMGROUPBARCODE.ITEMSTATUS1 = '1' 
                
                ORDER BY QTYORDER DESC
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        ////ค้นหาบาร์โค้ดในบิล
        //public static int CheckMRTOBarcode(string DocNo, string Barcode)
        //{
        //    string sql = string.Format($@"
        //        SELECT  Barcode
        //        FROM    ANDROID_ORDERTODT  
        //        WHERE   DocNo = '{DocNo}'
        //                AND Barcode = '{Barcode}' ");
        //    DataTable dt = ConnectionClass.SelectSQL_Main(sql);
        //    //            Boolean MRTO = false;
        //    if (dt.Rows.Count > 0)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        DataTable dA = ConnectionClass.SelectSQL_Main($@"
        //            SELECT	COUNT(DocNo)+1 AS SEQ	
        //            FROM	Android_OrderTODT WITH (NOLOCK)
        //            WHERE	DocNo = '{DocNo}'
        //        ");
        //        return Convert.ToInt32(dA.Rows[0]["SEQ"].ToString());
        //    }
        //}
        ////ค้นหาสาขาตามสายรถ ตามแผนก
        //public static DataTable GetRouteExcel_ByDept(string conditionBch = "")
        //{
        //    string sql = $@"
        //    SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME,BRANCH_ID + '-' + BRANCH_NAME AS BRANCH
        //    FROM	(
        //      SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
        //      FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH(NOLOCK)
        //      WHERE	ACCOUNTNUM LIKE 'MN%'
        //        AND ISPRIMARY = '1'		
        //      )TMP 
        //      INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID
        //      INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMP.ACCOUNTNUM = SHOP_BRANCH.BRANCH_ID AND BRANCH_ORDERTOAX = '1'
        //    WHERE	SEQ = 1 {conditionBch}
        //    ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //เส้นทาง
        //public static DataTable GetRouteAll()
        //{
        //    string sql = $@"
        //        SELECT	DISTINCT ROUTEID,NAME,ROUTEID+' - '+NAME AS ROUTENAME
        //        FROM	(
        //          SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME	
        //          FROM	(
        //            SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
        //            FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH(NOLOCK)
        //            WHERE	ACCOUNTNUM LIKE 'MN%'
        //              AND ISPRIMARY = '1'		
        //            )TMP INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID
        //          WHERE	SEQ = 1
        //        )TMP2
        //        ORDER BY ROUTEID
        //    ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        #region BoxGroup
        public static DataTable PrintT_BoxGroup(string _Doc)
        {
            string sql;

            sql = $@"
                    SELECT  BOXGROUP
                    FROM    SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK)   
                    WHERE   DOCUMENTNUM = '{_Doc}' AND DATAAREAID = 'SPC'  
                ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable PrintT_GetAndroidOrderTo(string _DateDoc, string _Dept, string _Branch, string _Route)
        {

            if (!string.IsNullOrEmpty(_Branch)) _Branch = $"AND BRANCH_ID='{_Branch}'"; //_Branch = "",mn009
            if (!string.IsNullOrEmpty(_Route)) _Route = $"AND ANDROID_ROUTE.ROUTEID='{_Route}'"; // _Route = '901'

            string sql = $@"
                SELECT  '0' AS [CHECK],
                        INVENTTRANSFERJOUR.TRANSFERID AS VOUCHERID,INVENTTRANSFERJOUR.INVENTLOCATIONIDFROM,
		                CONVERT(VARCHAR,INVENTTRANSFERJOUR.CREATEDDATETIME,23) AS DATEDOCNO,  
		                CONVERT(VARCHAR,DATEADD(HOUR,+7, INVENTTRANSFERJOUR.CREATEDDATETIME),24) AS TIMEDOCNO,   
		                SHOP_BRANCH.BRANCH_ID,SHOP_BRANCH.BRANCH_NAME AS BRANCHNAME,   
		                Android_Route.ROUTEID,
		                ANDROID_ROUTE.NAME AS ROUTENAME,  
		                ISNULL(Shop_TypeStatus.Shop_statypedesc,'รอจัดส่งสินค้า')  AS STATUSDOCNO,
		                ISNULL(CountPrint,0) AS COUNTPRINT,
		                ROW_NUMBER( ) OVER(PARTITION BY INVENTTRANSFERJOUR.TRANSFERID ORDER BY LINENUM ASC) AS LINENUM,
		                INVENTTRANSFERJOURLINE.SPC_ITEMNAME AS SPC_ITEMNAME,
		                CAST(ROUND(INVENTTRANSFERJOURLINE.SPC_QTYSHIPPEDUNIT,2) AS decimal(18,2)) AS QTY,INVENTTRANSFERJOURLINE.SPC_ITEMBARCODEUNIT,
                        UPDATEDBY AS CASHIERID,
						SPC_NAME AS CASHIERNAME,
                        SPC_ITEMBARCODE AS ITEMBARCODE,
						COUNT(BOXGROUP) AS BOXGROUP

                FROM	SHOP2013TMP.dbo.INVENTTRANSFERJOUR WITH (NOLOCK)  
		                INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERJOURLINE WITH(NOLOCK) ON INVENTTRANSFERJOUR.VOUCHERID = INVENTTRANSFERJOURLINE.VOUCHERID
                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON INVENTTRANSFERJOUR.UPDATEDBY = EMPLTABLE.EMPLID
		                INNER JOIN  (
			                SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME	
			                FROM	(
					                SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
					                FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH (NOLOCK)
					                WHERE	ACCOUNTNUM LIKE 'MN%'
							                AND ISPRIMARY = '1' AND [SPC_ROUTINGPOLICY].DATAAREAID = N'SPC'		
					                )TMP INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID
			                WHERE	SEQ = 1 AND SPC_ROUTETABLE.DATAAREAID = N'SPC'
			                )ANDROID_ROUTE  ON INVENTTRANSFERJOUR.INVENTLOCATIONIDTO = ANDROID_ROUTE.ACCOUNTNUM   
		                INNER JOIN  SHOP_BRANCH   WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = ANDROID_ROUTE.ACCOUNTNUM     
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTLINE WITH (NOLOCK)  ON INVENTTRANSFERJOUR.TRANSFERID = SPC_SHIPMENTLINE.DOCUMENTNUM 
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE WITH (NOLOCK)  ON SPC_SHIPMENTTABLE.SHIPMENTID = SPC_SHIPMENTLINE.SHIPMENTID  
		                LEFT OUTER JOIN 
			                (SELECT Shop_Desc,Shop_Table,Shop_StaType,Shop_StatypeDesc 
			                FROM    Shop_TypeStatus WITH (NOLOCK) 
			                WHERE   SHOP_TYPE = 'SH')Shop_TypeStatus ON SPC_SHIPMENTTABLE.SHIPMENTSTATUS = Shop_TypeStatus.Shop_StaType
		                LEFT OUTER JOIN ANDROID_COUNTPRINT WITH (NOLOCK) ON INVENTTRANSFERJOUR.VOUCHERID = ANDROID_COUNTPRINT.DocNo AND  Type = 'Bill'
						INNER JOIN SHOP2013TMP.dbo.SPC_BOXFACESHEET WITH(NOLOCK) ON INVENTTRANSFERJOURLINE.VOUCHERID = SPC_BOXFACESHEET.DOCUMENTNUM 

                WHERE 	CONVERT(VARCHAR, DATEADD(HOUR,+7,INVENTTRANSFERJOUR.CREATEDDATETIME),23) = '{_DateDoc}' 
		                AND SPC_REMARKS ='{_Dept}'
                        AND INVENTTRANSFERJOUR.DATAAREAID = N'SPC'
						AND SPC_BOXFACESHEET.DATAAREAID = N'SPC'
						AND EMPLTABLE.DATAAREAID = N'SPC'
						AND INVENTTRANSFERJOURLINE.DATAAREAID = N'SPC'
                        {_Branch} {_Route}
				GROUP BY INVENTTRANSFERJOUR.TRANSFERID, INVENTTRANSFERJOUR.INVENTLOCATIONIDFROM, 
						 INVENTTRANSFERJOUR.CREATEDDATETIME,SHOP_BRANCH.BRANCH_ID,
						 SHOP_BRANCH.BRANCH_NAME,Android_Route.ROUTEID,
						 ANDROID_ROUTE.NAME,Shop_TypeStatus.Shop_statypedesc,
						 ANDROID_COUNTPRINT.CountPrint,INVENTTRANSFERJOURLINE.LINENUM,
						 INVENTTRANSFERJOURLINE.SPC_ITEMNAME,INVENTTRANSFERJOURLINE.SPC_QTYSHIPPEDUNIT,
						 INVENTTRANSFERJOURLINE.SPC_ITEMBARCODEUNIT,INVENTTRANSFERJOUR.UPDATEDBY,
						 EMPLTABLE.SPC_NAME,INVENTTRANSFERJOURLINE.SPC_ITEMBARCODE
                ORDER BY INVENTTRANSFERJOUR.CREATEDDATETIME,CONVERT(VARCHAR,DATEADD(HOUR,+7, INVENTTRANSFERJOUR.CREATEDDATETIME),24) DESC
            ";

            return ConnectionClass.SelectSQL_Main(sql);

        }
        #endregion
    }



}