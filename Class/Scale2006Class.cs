﻿
using System.Data;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.GeneralForm.Scale;

namespace PC_Shop24Hrs.Class
{
    class Scale2006Class
    {
        public static DataTable GetPurch()
        {
            string sql = $@"
                    SELECT	NUM,NUM+' - '+DIMENSIONS.DESCRIPTION AS DESCRIPTION_SHOW	 
                    FROM    Shop_scalemaster WITH(NOLOCK) INNER JOIN 
                            SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK)   ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE INNER JOIN 
                            SHOP2013TMP.dbo.DIMENSIONS WITH(NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM
                    GROUP BY NUM, DIMENSIONS.DESCRIPTION ORDER BY NUM, DIMENSIONS.DESCRIPTION ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //public static DataTable GetPriceGroup()
        //{
        //    string sql = @"SELECT	 COLUMNNAME,LABEL	FROM [SHOP_PRICE] WITH (NOLOCK)  ORDER BY COLUMNNAME";
        //    DataTable table = Controllers.ConnectionClass.SelectSQL_Main(sql);
        //    return table;
        //}

        public static DataTable GetDataScaleShopMain(string price)
        {
            string sql = $@"
                    SELECT	DIMENSIONS.DESCRIPTION,EXPDAY,EXPDAYPRINT,TYPE_SCALE,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,   
                            INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,SPC_PriceGroup3,{price} AS PRICE,  
                            CONVERT(VARCHAR,ISNULL(TRANSDATEEDIT,''),23) AS DATEEDIT, CONVERT(VARCHAR,ISNULL(TRANSDATEEDIT,''),24) AS TIMEEDIT,  
                            ISNULL(TRANSDATEEDIT,'') AS TRANSDATE,SPC_ITEMBUYERGROUPID  
                    FROM	Shop_ScaleSend	WITH (NOLOCK) INNER JOIN 
                            Shop_scalemaster WITH (NOLOCK)  ON Shop_ScaleSend.ITEMBARCODE = Shop_scalemaster.ITEMBARCODE INNER JOIN 
                            SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)  ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE LEFT OUTER JOIN   
                            (SELECT	ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES,MAX(dateAdd(mi,420,MODIFIEDDATETIME)) AS TRANSDATEEDIT  
                            FROM	SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH (NOLOCK) WHERE DATAAREAID = 'SPC' AND 	PRICEFIELDID  
                                    IN (SELECT FieldID FROM dbo.Shop_Price WITH (NOLOCK) WHERE ColumnName = '{price}' ) 
                            GROUP BY ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES)SPC_INVENTITEMPRICEHISTORY  ON INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID 
                                    AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID 
                                    AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID INNER JOIN 
                            SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 	ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
                    WHERE 	 SPC_SALESPRICETYPE IN ('3','4','5')   AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                    ORDER BY INVENTITEMBARCODE.ITEMBARCODE ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //public static DataTable GetDataScaleSupcMain(string price, string dept)
        //{
        //    string sql = $@"
        //            SELECT EXPDAY, EXPDAYPRINT, TYPE_SCALE, INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,  
        //                    INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,SPC_PriceGroup3, {price}  AS PRICE,  
        //                    CONVERT(VARCHAR, ISNULL(TRANSDATEEDIT,''),23) AS DATEEDIT,CONVERT(VARCHAR, ISNULL(TRANSDATEEDIT, ''), 24) AS TIMEEDIT,
        //                    ISNULL(TRANSDATEEDIT, '') AS TRANSDATE, SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION
        //            FROM    Shop_scalemaster WITH (NOLOCK)  
        //                    INNER JOIN  SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
        //                    LEFT OUTER JOIN
        //                    (SELECT     ITEMID, INVENTDIMID, UNITID, NEWPRICESALES, ORIGPRICESALES, MAX(dateAdd(mi,420, MODIFIEDDATETIME)) as TRANSDATEEDIT
        //                     FROM       SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH(NOLOCK)
        //                     WHERE      PRICEFIELDID IN (SELECT FieldID FROM dbo.Shop_Price WITH (NOLOCK) WHERE ColumnName = '{price}')  AND DATAAREAID = 'SPC'
        //                     GROUP BY   ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES
        //                    )SPC_INVENTITEMPRICEHISTORY ON INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID
        //                        AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID
        //                        AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID 
        //                    INNER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 	ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
        //             WHERE  SPC_SALESPRICETYPE IN('3','4','5')  AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
        //              AND SPC_ITEMBUYERGROUPID = '{dept}' ORDER BY INVENTITEMBARCODE.ITEMBARCODE ";

        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //public static DataTable GetDataScaleByPriceGroup(object purchid, string conditionpurchid = "")
        //{
        //    if (!(purchid is null))
        //    {
        //        conditionpurchid = string.Format(@"AND SPC_ITEMBUYERGROUPID = '{0}'", purchid.ToString());
        //    }
        //    string sql = string.Format(@"
        //            SELECT  TYPE_SCALE,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,  
        //                    INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,SPC_PriceGroup3,SPC_PriceGroup13,SPC_PriceGroup14,SPC_PriceGroup15,  SPC_PriceGroup17,SPC_PriceGroup18,
        //                    CONVERT(VARCHAR, ISNULL(TRANSDATEEdit,''),23) AS DATEEdit,CONVERT(VARCHAR, ISNULL(TRANSDATEEdit, ''), 24) AS TimeEdit,
        //                    ISNULL(TRANSDATEEdit, '') AS TRANSDATE, SPC_ITEMBUYERGROUPID, TYPE_SCALE, EXPDAY, EXPDAYPRINT
        //             FROM   Shop_scaleMaster WITH (NOLOCK) INNER JOIN 
        //                    Shop_scaleSend WITH (NOLOCK) ON Shop_scaleMaster.ITEMBARCODE = Shop_scaleSend.ITEMBARCODE INNER JOIN  
        //                    SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_scaleSend.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE LEFT OUTER JOIN 
        //                    (SELECT ITEMID, INVENTDIMID, UNITID, MAX(dateAdd(mi,420, MODIFIEDDATETIME)) AS TRANSDATEEdit
        //                    FROM SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH (NOLOCK)
        //                    WHERE  PRICEFIELDID IN (SELECT FieldID FROM dbo.Shop_Price WITH (NOLOCK)
        //                    WHERE ColumnName IN ('SPC_PriceGroup13','SPC_PriceGroup14','SPC_PriceGroup15','SPC_PriceGroup17',SPC_PriceGroup18  )  AND DATAAREAID = 'SPC' )    
        //                    GROUP BY ITEMID,INVENTDIMID,UNITID)SPC_INVENTITEMPRICEHISTORY
        //                    ON INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID
        //                    AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID
        //                    AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID
        //             WHERE  TypeSend = 'SHOP' {0}
        //             Order By INVENTITEMBARCODE.ITEMBARCODE", conditionpurchid);

        //    DataTable ret = Controllers.ConnectionClass.SelectSQL_Main(sql);
        //    return ret;
        //}
        public static DataTable GetDataScale(string price, string dept)
        {
            string sql = $@"
                   SELECT	EXPDAY, EXPDAYPRINT, TYPE_SCALE,CASE WHEN TypeSend = 'SHOP' THEN '1' ELSE 0  END AS TYPESEND,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,
		                    INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,{price} AS PRICE,
		                    SPC_PriceGroup13,SPC_PriceGroup14,SPC_PriceGroup15,SPC_PriceGroup17  ,SPC_PriceGroup18,SPC_PriceGroup19,
		                    CONVERT(VARCHAR, ISNULL(TRANSDATEEDIT,''),23) AS DATEEDIT,CONVERT(VARCHAR, ISNULL(TRANSDATEEDIT, ''), 24) AS TIMEEDIT,
		                    ISNULL(TRANSDATEEDIT, '') AS TRANSDATE,SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION
                    FROM	Shop_scalemaster WITH (NOLOCK) 
		                    LEFT OUTER JOIN  Shop_scaleSend WITH (NOLOCK) ON Shop_scaleMaster.ITEMBARCODE = Shop_scaleSend.ITEMBARCODE 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
		                    LEFT OUTER JOIN
		                    (	SELECT  ITEMID, INVENTDIMID, UNITID, NEWPRICESALES, ORIGPRICESALES, MAX(dateAdd(mi,420, MODIFIEDDATETIME)) as TRANSDATEEDIT
			                    FROM    SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH(NOLOCK) 
			                    WHERE   PRICEFIELDID IN ( SELECT FieldID FROM dbo.Shop_Price WITH (NOLOCK) WHERE ColumnName = '{price}')  
					                    AND DATAAREAID = 'SPC'
			                    GROUP BY ITEMID,INVENTDIMID,UNITID,NEWPRICESALES,ORIGPRICESALES
		                    ) SPC_INVENTITEMPRICEHISTORY ON INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID
			                    AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID
			                    AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID 
		                    INNER JOIN  SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 	ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
                    WHERE   SPC_SALESPRICETYPE IN ('3','4','5')   
                        AND INVENTITEMBARCODE.DATAAREAID = 'SPC'  
                        AND SPC_ITEMBUYERGROUPID = '{dept}' ";

            DataTable ret = Controllers.ConnectionClass.SelectSQL_Main(sql);
            return ret;
        }
        public static ScaleMain.ScaleItem Itembarcode(string itembacode)
        {
            ScaleMain.ScaleItem item = new ScaleMain.ScaleItem();
            string sql = $@"
                    SELECT	SPC_SALESPRICETYPE,INVENTITEMBARCODE.ITEMBARCODE, SPC_ITEMNAME
		                    ,isnull(Shop_scalemaster.ITEMBARCODE, '0') AS ScaleMaster
		                    ,isnull(Shop_ScaleSend.ITEMBARCODE, '0') AS ScaleSendShop
                    FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) LEFT OUTER JOIN 
		                    Shop_scalemaster WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE = Shop_scalemaster.ITEMBARCODE LEFT OUTER JOIN  
		                    Shop_ScaleSend WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE = Shop_ScaleSend.ITEMBARCODE 
                    WHERE	SPC_SALESPRICETYPE IN ('3', '4', '5') 
		                    AND INVENTITEMBARCODE.ITEMBARCODE = '{itembacode}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                item = new ScaleMain.ScaleItem()
                {
                    ITEMBARCODE = dt.Rows[0]["ITEMBARCODE"].ToString(),
                    SPC_ITEMNAME = dt.Rows[0]["SPC_ITEMNAME"].ToString(),
                    SCALEMASTER = dt.Rows[0]["ScaleMaster"].ToString(),
                    SCALESENDSHOP = dt.Rows[0]["ScaleSendShop"].ToString()
                };
            }
            return item;
        }
        public static DataTable GetDataDelete(string dept = "")
        {
            if (!dept.Equals("")) { dept = $@"AND NUM = '{dept}' "; }
            string sql = $@" 
                    SELECT  EXPDAY,EXPDAYPRINT,TYPE_SCALE,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,  
                            INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID  
                    FROM	Shop_scalemaster WITH (NOLOCK) LEFT OUTER JOIN 
                            Shop_ScaleSend WITH (NOLOCK)   ON Shop_ScaleSend.ITEMBARCODE = Shop_scalemaster.ITEMBARCODE INNER JOIN 
                            SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_scalemaster.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE   LEFT OUTER JOIN 
                            SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) 	ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
                    WHERE 	SPC_SALESPRICETYPE IN ('3','4','5')  
                            AND ISNULL(Shop_ScaleSend.ITEMBARCODE,'') = ''  {dept}
                    ORDER BY INVENTITEMBARCODE.ITEMBARCODE ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        //public static string[] GetPriceGroupMN()
        //{
        //    string[] Tmp;
        //    //string sql = @"SELECT [FieldID],[ColumnName],[Label],[ID],[TYPE_DATA] FROM [dbo].[Shop_Price] where [ID] like 'MN%' and LEN(ID) = 4 order by [ID] ";

        //    DataTable table = BranchClass.GetPriceLevel(" where [ID] like 'MN%' and LEN(ID) = 4 "); //Controllers.ConnectionClass.SelectSQL_Main(sql);
        //    int i = 0; Tmp = new string[table.Rows.Count];
        //    foreach (DataRow row in table.Rows)
        //    {
        //        Tmp[i] = row["ColumnName"].ToString();
        //        i++;
        //    }
        //    return Tmp;
        //}
        //public static string GetCatalogScal(ScaleCatl scale, string prc)
        //{
        //    var catalog = from l in scale.List where l.PriceID == prc select l;
        //    foreach (var c in catalog)
        //    {
        //        prc = c.ScaleCat;
        //    }
        //    return prc;
        //}
        //public static string UpdateBarcodeToScaleMaster(ScaleClass Item, string ConnectionScale, string ConnectionMain)
        //{
        //    ArrayList array = new ArrayList();
        //    ArrayList array707 = new ArrayList();

        //    string Str = string.Format(@"
        //            Update {5}Shop_ScaleMaster 
        //            Set TYPE_SCALE = '{0}',
        //                QTYSYMBOL = '{1}',
        //                EXPDAY = '{2}',
        //                EXPDAYPRINT = '{3}' 
        //            WHERE ITEMBARCODE = '{4}'"
        //            , Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT, Item.ITEMBARCODE, "");

        //    string Str707 = string.Format(@"
        //            Update Shop_ScaleMaster 
        //            Set TYPE_SCALE = '{0}',
        //                QTYSYMBOL = '{1}',
        //                EXPDAY = '{2}',
        //                EXPDAYPRINT = '{3}' 
        //            WHERE ITEMBARCODE = '{4}'"
        //            , Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT, Item.ITEMBARCODE);

        //    array.Add(Str);
        //    array707.Add(Str707);

        //    //insert scalesend
        //    if (!Item.TYPESENDSCALESHOP.Equals("") && !CheckItemsendShop(Item.ITEMBARCODE).Equals(Item.ITEMBARCODE))
        //    {
        //        array.Add(string.Format(@"
        //                    INSERT INTO {2}Shop_scaleSend 
        //                            (ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
        //                    VALUES 
        //                            ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
        //                    , Item.ITEMBARCODE
        //                    , Controllers.SystemClass.SystemUserID_M
        //                    , ""));

        //        array707.Add(string.Format(@"
        //                    INSERT INTO Shop_scaleSend
        //                            (ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
        //                    VALUES 
        //                            ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
        //                    , Item.ITEMBARCODE
        //                    , Controllers.SystemClass.SystemUserID_M));
        //    }
        //    else if (Item.TYPESENDSCALESHOP.Equals("") && CheckItemsendShop(Item.ITEMBARCODE).Equals(Item.ITEMBARCODE))
        //    {
        //        array.Add(string.Format(@"DELETE FROM {0}Shop_scaleSend WHERE ItemBarcode = {1}", "", Item.ITEMBARCODE));
        //        array707.Add(string.Format(@"DELETE FROM Shop_scaleSend WHERE ItemBarcode = {0}", Item.ITEMBARCODE));
        //    }

        //    return Controllers.ConnectionClass.Execute_SameTime_2Server(array707, ConnectionScale, array, ConnectionMain);
        //}
        //public static string InsertBarcodeToScaleMaster(ScaleClass Item, string ConnectionScale, string ConnectionMain)
        //{
        //    ArrayList array = new ArrayList();
        //    ArrayList array707 = new ArrayList();
        //    string Str = string.Format(@"
        //            INSERT INTO {6}Shop_ScaleMaster(ITEMBARCODE,SKU,BARCODE,TYPE_SCALE,QTYSYMBOL,EXPDAY,EXPDAYPRINT) 
        //            VALUES ('{0}','{1}','2 {1}','{2}','{3}','{4}','{5}')"
        //            , Item.ITEMBARCODE, Item.ITEMBARCODE.Substring(1, 6), Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT, "");

        //    string Str707 = string.Format(@"
        //            INSERT INTO Shop_ScaleMaster(ITEMBARCODE,SKU,BARCODE,TYPE_SCALE,QTYSYMBOL,EXPDAY,EXPDAYPRINT) 
        //            VALUES ('{0}','{1}','2 {1}','{2}','{3}','{4}','{5}')"
        //            , Item.ITEMBARCODE, Item.ITEMBARCODE.Substring(1, 6), Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT);
        //    array.Add(Str);
        //    array707.Add(Str707);

        //    if (!Item.TYPESENDSCALESHOP.Equals(""))
        //    {
        //        array.Add(string.Format(@"
        //                    INSERT INTO {2}Shop_scaleSend(ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
        //                    VALUES 
        //                            ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
        //                    , Item.ITEMBARCODE
        //                    , Controllers.SystemClass.SystemUserID_M
        //                    , ""));

        //        array707.Add(string.Format(@"
        //                    INSERT INTO Shop_scaleSend(ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
        //                    VALUES 
        //                            ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
        //                    , Item.ITEMBARCODE
        //                    , Controllers.SystemClass.SystemUserID_M));
        //    }
        //    return Controllers.ConnectionClass.Execute_SameTime_2Server(array707, ConnectionScale, array, ConnectionMain);
        //}
        //private static string CheckItemsendShop(string item)
        //{
        //    ScaleItem tmp = Itembarcode(item);
        //    return tmp.SCALESENDSHOP;
        //}
    }
}
