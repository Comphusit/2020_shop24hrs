﻿using System;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class
{
    class ItembarcodeClass
    {
        //check เวลาที่ Input
        public static string CheckInputScannerKB(DateTime TimeStartInput)
        {
            TimeSpan Span = DateTime.Now - TimeStartInput;
            string typeInput = "SCANNER";
            if (Span.TotalMilliseconds > 500) typeInput = "KEYBOARD";
            return typeInput;
        }
        //Check KB For Return barocde Real
        public static string CheckTextboxBarcode(DateTime TimeStartInput, string txt)
        {
            string txtReturn = txt;
            string typeInput = CheckInputScannerKB(TimeStartInput);
            if (typeInput == "KEYBOARD")
            {
                if (txt.Contains(" "))
                {
                    string[] txtbarcode = txt.Split(' ');
                    txtReturn = txtbarcode[0];
                }
            }
            return txtReturn;
        }
        //ค้นหาสินค้าทั้งหมด
        public static DataTable FindItembarcodeAll(string _pTop, string _pCondition)
        {
            string sql = string.Format(@"SELECT	" + _pTop + @" ITEMBARCODE,INVENTITEMBARCODE.ITEMID,INVENTDIMID,QTY,UNITID,SPC_ITEMNAME,INVENTITEMBARCODE.SPC_ITEMACTIVE,
		                SPC_PRICEGROUP3,SPC_PRICEGROUP13,SPC_PRICEGROUP14,SPC_PRICEGROUP15,SPC_PRICEGROUP17,INVENTITEMBARCODE.SPC_IMAGEPATH,
		                PRIMARYVENDORID,VENDTABLE.NAME as PRIMARYVENDORIDNAME,INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,SPC_SalesPriceType,
		                SPC_INVENTGROUPID AS GRPID0,SPC_INVENTGROUP.TXT AS TAX0,
		                SPC_INVENTSUBGROUPID1 AS GRPID1,SPC_INVENTSUBGROUP1.TXT AS TAX1,
		                SPC_INVENTSUBGROUPID2 AS GRPID2,SPC_INVENTSUBGROUP2.TXT AS TAX2,SPC_SalesPriceType,SPC_GiftPoint,
                        CASE SPC_SalesPriceType WHEN '1' THEN 'บังคับใช้'	WHEN '2' THEN 'แก้ไขได้'	WHEN '3' THEN 'เครื่องชัง'	WHEN '4' THEN 'เครื่องชั่งแพ็ค' WHEN '5' THEN 'น้ำหนัก' END AS SPC_SalesPriceTypeNAME  
                FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
		                INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTITEMBARCODE.ITEMID = INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON INVENTTABLE.PRIMARYVENDORID =VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = N'SPC' 
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTGROUP.INVENTGROUP AND SPC_INVENTGROUP.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTSUBGROUP1 WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTSUBGROUPID1 = SPC_INVENTSUBGROUP1.INVENTSUBGROUP1
			                AND INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTSUBGROUP1.INVENTGROUP AND SPC_INVENTSUBGROUP1.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTSUBGROUP2 WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTSUBGROUPID2 = SPC_INVENTSUBGROUP2.INVENTSUBGROUP2 
			                AND INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTSUBGROUP2.INVENTGROUP AND INVENTTABLE.SPC_INVENTSUBGROUPID1 = SPC_INVENTSUBGROUP2.INVENTSUBGROUP1
			                AND SPC_INVENTSUBGROUP2.DATAAREAID = N'SPC'
                WHERE	INVENTTABLE.DATAAREAID = N'SPC' " + _pCondition +
                @"  ORDER BY INVENTITEMBARCODE.ITEMID,INVENTDIMID,QTY");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            //MessageBox.Show(Convert.ToString(dt.Rows.Count));
            return dt;
        }
        //ค้นหาสินค้าทั้งหมด
        public static DataTable GetItembarcode_ALLDeatil(string Itembarcode)
        {
            //string sql = string.Format(@"
            //    SELECT	 ITEMBARCODE,INVENTITEMBARCODE.ITEMID,INVENTDIMID,QTY,UNITID,SPC_ITEMNAME,INVENTITEMBARCODE.SPC_ITEMACTIVE,
            //      SPC_PRICEGROUP3,SPC_PRICEGROUP13,SPC_PRICEGROUP14,SPC_PRICEGROUP15,INVENTITEMBARCODE.SPC_IMAGEPATH,
            //      PRIMARYVENDORID,VENDTABLE.NAME as PRIMARYVENDORIDNAME,INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,SPC_SalesPriceType,
            //      SPC_INVENTGROUPID AS GRPID0,SPC_INVENTGROUP.TXT AS TAX0,
            //      SPC_INVENTSUBGROUPID1 AS GRPID1,SPC_INVENTSUBGROUP1.TXT AS TAX1,
            //      SPC_INVENTSUBGROUPID2 AS GRPID2,SPC_INVENTSUBGROUP2.TXT AS TAX2,SPC_SalesPriceType,
            //            SPC_GiftPoint,
            //            CASE SPC_SalesPriceType WHEN '1' THEN 'บังคับใช้'	WHEN '2' THEN 'แก้ไขได้'	WHEN '3' THEN 'เครื่องชัง'	WHEN '4' THEN 'เครื่องชั่งแพ็ค' WHEN '5' THEN 'น้ำหนัก' END AS SPC_SalesPriceTypeNAME  
            //    FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
            //      INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTITEMBARCODE.ITEMID = INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
            //      LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC'
            //      LEFT OUTER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON INVENTTABLE.PRIMARYVENDORID =VENDTABLE.ACCOUNTNUM AND VENDTABLE.DATAAREAID = N'SPC' 
            //      LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTGROUP.INVENTGROUP AND SPC_INVENTGROUP.DATAAREAID = N'SPC'
            //      LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTSUBGROUP1 WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTSUBGROUPID1 = SPC_INVENTSUBGROUP1.INVENTSUBGROUP1
            //       AND INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTSUBGROUP1.INVENTGROUP AND SPC_INVENTSUBGROUP1.DATAAREAID = N'SPC'
            //      LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTSUBGROUP2 WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTSUBGROUPID2 = SPC_INVENTSUBGROUP2.INVENTSUBGROUP2 
            //       AND INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTSUBGROUP2.INVENTGROUP AND INVENTTABLE.SPC_INVENTSUBGROUPID1 = SPC_INVENTSUBGROUP2.INVENTSUBGROUP1
            //       AND SPC_INVENTSUBGROUP2.DATAAREAID = N'SPC'
            //    WHERE	INVENTTABLE.DATAAREAID = N'SPC' AND ITEMBARCODE='" + Itembarcode + @"'
            //    ORDER BY INVENTITEMBARCODE.ITEMID,INVENTDIMID,QTY");

            DataTable dt = ConnectionClass.SelectSQL_Main(" ItembarcodeClass_GetItembarcode_ALLDeatil '" + Itembarcode + @"' ");
            //MessageBox.Show(Convert.ToString(dt.Rows.Count));
            return dt;
        }
        //ค้นหา ราคา + น้ำหนัก จาก สินค้า
        public static DataTable FindPriceNetWeight_ByBarcode(string _pBarcode, string _pType, double _pPrice, double _pweight) //_pType ประเภทราคา _pPrice ราคาขายตอหน่วย //_pweight น้ำนหัก
        {
            DataTable T = new DataTable();
            T.Columns.Add("priceNet"); T.Columns.Add("weight");
            double weight = 0; double priceNet = 0;

            switch (_pType)
            {
                case "1":
                    priceNet = _pweight * _pPrice;
                    weight = _pweight;
                    break;
                case "2":
                    priceNet = _pweight * _pPrice;
                    weight = _pweight;
                    break;
                case "3":
                    priceNet = Convert.ToDouble(_pBarcode.Substring(7, 3) + '.' + _pBarcode.Substring(10, 3));
                    weight = priceNet / _pPrice;
                    break;
                case "4":
                    weight = Convert.ToDouble(_pBarcode.Substring(7, 3) + '.' + _pBarcode.Substring(10, 3));
                    priceNet = weight / _pPrice;
                    break;
                case "5":
                    priceNet = _pPrice;
                    weight = 1;
                    break;
            }
            T.Rows.Add(priceNet, weight);
            return T;
        }
        //ค้นหารายละเอียดบาร์โค้ดทั้งหมด ตาม ID + DIM
        public static DataTable FindItembarcodeAll_ByItemidDim(string _pID, string _pDim)
        {
            //string str = string.Format(@"
            //    SELECT	ITEMBARCODE,INVENTITEMBARCODE.ITEMID,INVENTDIMID,QTY,UNITID,SPC_ITEMNAME,INVENTITEMBARCODE.SPC_ITEMACTIVE,
            //            SPC_PRICEGROUP3,SPC_PRICEGROUP13,SPC_PRICEGROUP14,SPC_PRICEGROUP15,INVENTITEMBARCODE.SPC_IMAGEPATH, 'เลือก' AS OK 
            //    FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)
            //    WHERE DATAAREAID = N'SPC' AND ITEMID = '" + _pID + @"' AND INVENTDIMID = '" + _pDim + @"' 
            //    ORDER BY QTY,SPC_PRICEGROUP3 
            //    ");
            DataTable dt = ConnectionClass.SelectSQL_Main(" ItembarcodeClass_FindItembarcodeAll_ByItemidDim '" + _pID + @"','" + _pDim + @"' ");
            return dt;
        }
        //ค้นหา DIM
        public static DataTable FindDIM_ByBarcode(string _pBarcode)
        {
            string str = string.Format(@"
                ItembarcodeClass_FindDIM_ByBarcode '" + _pBarcode + @"'
                ");
            DataTable dt = ConnectionClass.SelectSQL_Main(str);
            return dt;
        }
        //ค้นหา STOCK
        public static double FindStock_ByBarcode(string _pBarcode, string _pBchID)
        {
            DataTable dtBarcode = FindDIM_ByBarcode(_pBarcode);
            if (dtBarcode.Rows.Count == 0) return 0;

            //string str = string.Format(@"
            //     select	INVENTSUM.ITEMID,INVENTSERIALID,SUM(AVAILPHYSICAL) AS AVAILPHYSICAL,CONFIGID,INVENTSIZEID,INVENTCOLORID 	 
            //     from	SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK)  
            //      INNER JOIN SHOP2013TMP.dbo.INVENTSUM WITH (NOLOCK)  ON  INVENTSUM.INVENTDIMID = INVENTDIM.INVENTDIMID   
            //     WHERE 	INVENTLOCATIONID = '" + _pBchID + @"'    
            //             AND INVENTSUM.DATAAREAID = 'SPC'
            //             AND INVENTSUM.ITEMID = '" + dtBarcode.Rows[0]["ITEMID"].ToString() + @"'
            //             AND CONFIGID = '" + dtBarcode.Rows[0]["CONFIGID"].ToString() + @"'
            //             AND INVENTSIZEID = '" + dtBarcode.Rows[0]["INVENTSIZEID"].ToString() + @"'
            //             AND INVENTCOLORID = '" + dtBarcode.Rows[0]["INVENTCOLORID"].ToString() + @"'
            //     GROUP BY INVENTSUM.ITEMID, INVENTSERIALID, CONFIGID, INVENTSIZEID, INVENTCOLORID
            //    ");

            string str = string.Format(@"
                ItembarcodeClass_FindStockByDIM '" + _pBchID + @"',
                    '" + dtBarcode.Rows[0]["ITEMID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["CONFIGID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["INVENTSIZEID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["INVENTCOLORID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["BatchNumGroupId"].ToString() + @"'    
            ");
            DataTable dt = ConnectionClass.SelectSQL_Main(str);
            if (dt.Rows.Count == 0) return 0;
            else return Convert.ToDouble(dt.Rows[0]["AVAILPHYSICAL"].ToString());
        }
        //ค้นหา STOCK ทุกสาขา
        public static DataTable FindStock_ByBarcode_AllBranch(string _pBarcode)
        {
            DataTable dt = new DataTable();
            DataTable dtBarcode = FindDIM_ByBarcode(_pBarcode);
            if (dtBarcode.Rows.Count == 0)
            {
                return dt;
            }

            //string str = string.Format(@"
            //     select	INVENTLOCATIONID,INVENTSUM.ITEMID,INVENTSERIALID,SUM(AVAILPHYSICAL) AS AVAILPHYSICAL,CONFIGID,INVENTSIZEID,INVENTCOLORID 	 
            //     from	SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK)  
            //      INNER JOIN SHOP2013TMP.dbo.INVENTSUM WITH (NOLOCK)  ON  INVENTSUM.INVENTDIMID = INVENTDIM.INVENTDIMID   
            //     WHERE 	INVENTSUM.DATAAREAID = 'SPC'
            //             AND INVENTSUM.ITEMID = '" + dtBarcode.Rows[0]["ITEMID"].ToString() + @"'
            //             AND CONFIGID = '" + dtBarcode.Rows[0]["CONFIGID"].ToString() + @"'
            //             AND INVENTSIZEID = '" + dtBarcode.Rows[0]["INVENTSIZEID"].ToString() + @"'
            //             AND INVENTCOLORID = '" + dtBarcode.Rows[0]["INVENTCOLORID"].ToString() + @"'
            //     GROUP BY INVENTSUM.ITEMID, INVENTSERIALID, CONFIGID, INVENTSIZEID, INVENTCOLORID,INVENTLOCATIONID
            //    ");

            string str = string.Format(@"
                ItembarcodeClass_FindStockByDIM '',
                    '" + dtBarcode.Rows[0]["ITEMID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["CONFIGID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["INVENTSIZEID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["INVENTCOLORID"].ToString() + @"',
                    '" + dtBarcode.Rows[0]["BatchNumGroupId"].ToString() + @"'    
            ");
            dt = ConnectionClass.SelectSQL_Main(str);
            return dt;
        }
        //ค้นหาผู้จำหน่าย
        public static DataTable GetVender_ByVenderID(string _pVender)
        {
            string sql = string.Format(@"SELECT	ACCOUNTNUM,NAME,ADDRESS,PHONE,TELEFAX,PAYMTERMID,BLOCKED,DIMENSION,NAMEALIAS,ITEMBUYERGROUPID,SMS,SPC_IMAGEPATH
            FROM	SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK)
            WHERE	ACCOUNTNUM = '" + _pVender + @"' ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return dt;
        }
        //ค้นหาสินค้าห้ามทำบิล ในส่วนของห้ามส่งของไปขายที่สาขา เกบเฉพาะ บาร์โค้ด เลยให้ return ว่ามีหรือไม่มีพอ
        public static DataTable GetTransferBlockedTable_ByBarcode(string pINVENT, string pBarcode)
        {
            DataTable dt = ConnectionClass.SelectSQL_Main(string.Format(@"
                ItembarcodeClass_GetTransferBlockedTable_ByBarcode '" + pINVENT + @"','" + pBarcode + @"'
                "));
            return dt;
        }
        //ค้นหาสินค้าห้ามสั่ง โดยล็อกอยู่ไม่ว่ากรณีใดๆ แสดงว่าจะโดนปลดเมื่อไหร่
        public static DataTable GetRETAILPLANORDERITEMBLOCKED_ByDim(string pINVENT, string pItemID, string pDim)
        {
            return ConnectionClass.SelectSQL_Main(string.Format(@"
             ItembarcodeClass_GetRETAILPLANORDERITEMBLOCKED_ByDim '" + pINVENT + @"','" + pItemID + @"','" + pDim + @"'
            "));
        }
        //ค้นหากลุ่มสินค้า
        public static DataTable GetInventGroup()
        {
            string sql = string.Format(@"SELECT	INVENTGROUP,TXT
            FROM	SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK)
            ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return dt;
        }
        //ค้นหาบาร์โค้ดที่เลกที่สุด
        public static DataTable GetBarcodeMin(string pGrp)
        {
            string sql = string.Format(@"
                SELECT *
                FROM INVENTITEMBARCODE_MINMAX WITH (NOLOCK) 
                    LEFT OUTER JOIN dbo.RPTINVENTTABLE on dbo.INVENTITEMBARCODE_MINMAX.ITEMID = dbo.RPTINVENTTABLE.ITEMID 
                WHERE INVENTITEMBARCODE_MINMAX.[TYPE_] = 'MIN'  " + pGrp + @"
            ");

            return Controllers.ConnectionClass.SelectSQL_SentServer(sql, Controllers.IpServerConnectClass.ConMainReportProcess, 120);
        }
        //ค้นหาสินค้าทั้งหมด ตามจัดซื้อ
        public static DataTable GetBarcode_ByPurchase(string pPurchase)
        {
            string sql = string.Format(@"
                     SELECT ITEMID,INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME,UNITID,QTY,SPC_PRICEGROUP3 AS PRICE                    
                     FROM	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
                     WHERE	DATAAREAID ='SPC' AND SPC_ITEMBUYERGROUPID ='" + pPurchase + @"'  ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาสินค้าห้ามสั่งทั้งหมดตามเงื่อนไข
        public static DataTable GetRETAILPLANORDERITEMBLOCKED_All(string bchID)
        {
            //string sql = string.Format(@"
            //        SELECT	INVENTLOCATIONIDTO,BRANCH_NAME,INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID, 
            //          convert(varchar,FROMDATE,23) as FROMDATE,convert(varchar,TODATE,23) as TODATE 
            //        FROM	SHOP2013TMP.dbo.SPC_RETAILPLANORDERITEMBLOCKED with (NOLOCK)  
            //          INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE  with (NOLOCK)  
            //            ON  SPC_RETAILPLANORDERITEMBLOCKED.ITEMID = INVENTITEMBARCODE.ITEMID	AND SPC_RETAILPLANORDERITEMBLOCKED.INVENTDIMID = INVENTITEMBARCODE.INVENTDIMID 
            //          INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_RETAILPLANORDERITEMBLOCKED.INVENTLOCATIONIDTO = SHOP_BRANCH.BRANCH_ID
            //        WHERE	OrderType = '6'
            //          AND SPC_RETAILPLANORDERITEMBLOCKED.DATAAREAID = 'SPC' AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
            //                AND CASE WHEN convert(varchar,TODATE,23) = '1900-01-01' THEN '2099-01-01' ELSE  convert(varchar,TODATE,23) END >= GETDATE()  
            //                AND convert(varchar,TODATE,23) <> '1900-01-01' " + pCon + @"

            //        ORDER BY INVENTLOCATIONIDTO,convert(varchar,TODATE,23),INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,INVENTITEMBARCODE.UNITID 
            //        ");

            string ConditionLocationID = " AND INVENTLOCATIONIDTO IN ('') ";
            if (bchID != "") ConditionLocationID = $@" AND INVENTLOCATIONIDTO IN ('{bchID}','') ";

            string sql = $@"
            SELECT	ISNULL(INVENTLOCATIONIDTO,'ทุกคลัง') AS INVENTLOCATIONIDTO,ISNULL(BRANCH_NAME,'ทุกคลัง') AS BRANCH_NAME,INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID, 
		            convert(varchar,FROMDATE,23) as FROMDATE
		            ,CASE CONVERT(varchar,TODATE,23) WHEN '1900-01-01' THEN 'ไม่มีกำหนดปลดล็อก' ELSE CONVERT(varchar,TODATE,23) END AS TODATE ,SPC_RETAILPLANORDERITEMBLOCKED.REMARKS
            FROM	SHOP2013TMP.dbo.SPC_RETAILPLANORDERITEMBLOCKED with (NOLOCK)  
		            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE  with (NOLOCK)  
				            ON  SPC_RETAILPLANORDERITEMBLOCKED.ITEMID = INVENTITEMBARCODE.ITEMID	AND SPC_RETAILPLANORDERITEMBLOCKED.INVENTDIMID = INVENTITEMBARCODE.INVENTDIMID 
		            LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_RETAILPLANORDERITEMBLOCKED.INVENTLOCATIONIDTO = SHOP_BRANCH.BRANCH_ID
            WHERE	OrderType = '6'
		            AND SPC_RETAILPLANORDERITEMBLOCKED.DATAAREAID = 'SPC' AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                    AND CASE WHEN convert(varchar,TODATE,23) = '1900-01-01' THEN '2099-01-01' ELSE  convert(varchar,TODATE,23) END >= GETDATE()  
		            AND INVENTITEMBARCODE.SPC_ITEMACTIVE = '1'
		            {ConditionLocationID}

            UNION 

            SELECT	ISNULL(INVENTLOCATIONIDTO,'ทุกคลัง') AS INVENTLOCATIONIDTO,ISNULL(BRANCH_NAME,'ทุกคลัง') AS BRANCH_NAME,INVENTITEMBARCODE.ITEMID,'ทุกบาร์โค้ด' AS INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME,INVENTITEMBARCODE.UNITID, 
		            convert(varchar,FROMDATE,23) as FROMDATE
		            ,CASE CONVERT(varchar,TODATE,23) WHEN '1900-01-01' THEN 'ไม่มีกำหนดปลดล็อก' ELSE CONVERT(varchar,TODATE,23) END AS STA_DATELOCK ,SPC_RETAILPLANORDERITEMBLOCKED.REMARKS
            FROM	SHOP2013TMP.dbo.SPC_RETAILPLANORDERITEMBLOCKED with (NOLOCK)  
		            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE  with (NOLOCK)  
				            ON  SPC_RETAILPLANORDERITEMBLOCKED.ITEMID = INVENTITEMBARCODE.ITEMID
		            LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_RETAILPLANORDERITEMBLOCKED.INVENTLOCATIONIDTO = SHOP_BRANCH.BRANCH_ID
            WHERE	OrderType = '6'
		            AND SPC_RETAILPLANORDERITEMBLOCKED.DATAAREAID = 'SPC' AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                    AND CASE WHEN convert(varchar,TODATE,23) = '1900-01-01' THEN '2099-01-01' ELSE  convert(varchar,TODATE,23) END >= GETDATE()  
		            AND INVENTITEMBARCODE.SPC_ITEMACTIVE = '1'
                    AND SPC_RETAILPLANORDERITEMBLOCKED.INVENTDIMID = 'AllBlank' 
                    {ConditionLocationID}


            ORDER BY INVENTLOCATIONIDTO,ITEMID,INVENTDIMID,SPC_ITEMNAME,UNITID 
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาสินค้าห้ามขายที่สาขา lock ทำบิล TF
        public static DataTable GetTransferBlockedItemTable_All(string pBch)
        {
            //string sql = $@"
            //           SELECT	ITEMID,SPC_TransferBlockedItemTable.ITEMBARCODE,ITEMNAME,UNITID ,GROUPID
            //            FROM	SHOP2013TMP.dbo.SPC_TransferBlockedItemTable WITH (NOLOCK)  
            //            WHERE	SPC_TransferBlockedItemTable.DATAAREAID = N'SPC'  
            //              AND GROUPID IN (
            //                        SELECT	GROUPID	
            //                        FROM	SHOP2013TMP.dbo.SPC_TRANSFERBLOCKEDTABLE WITH (NOLOCK)	
            //                        WHERE	INVENTLOCATIONID = '{pBch}' AND DATAAREAID = N'SPC')
            //            ORDER BY ITEMID ,ITEMNAME,UNITID
            //        ";
            string sql = $@"
            SELECT	INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,
		            SPC_TransferBlockedItemTable.ITEMID,SPC_TransferBlockedItemTable.ITEMBARCODE,SPC_TransferBlockedItemTable.ITEMNAME,UNITID ,
		            SPC_INVENTGROUP.TXT + '/' + SPC_INVENTSUBGROUP1.TXT + '/'  + SPC_INVENTSUBGROUP2.TXT AS GROUPDESC,
		            GROUPID,SPC_INVENTGROUPID AS GRPID0,SPC_INVENTGROUP.TXT AS TAX0,SPC_INVENTSUBGROUPID1 AS GRPID1,
		            SPC_INVENTSUBGROUP1.TXT AS TAX1,SPC_INVENTSUBGROUPID2 AS GRPID2,SPC_INVENTSUBGROUP2.TXT AS TAX2,
                    SPC_TransferBlockedItemTable.MODIFIEDBY,NAME,SPC_TransferBlockedItemTable.MODIFIEDDATETIME
            FROM	SHOP2013TMP.dbo.SPC_TransferBlockedItemTable WITH (NOLOCK)  
		            INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON SPC_TransferBlockedItemTable.ITEMID = INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONCODE = '0'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTGROUP.INVENTGROUP AND SPC_INVENTGROUP.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTSUBGROUP1 WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTSUBGROUPID1 = SPC_INVENTSUBGROUP1.INVENTSUBGROUP1
		                AND INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTSUBGROUP1.INVENTGROUP AND SPC_INVENTSUBGROUP1.DATAAREAID = N'SPC'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTSUBGROUP2 WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTSUBGROUPID2 = SPC_INVENTSUBGROUP2.INVENTSUBGROUP2 
		                AND INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTSUBGROUP2.INVENTGROUP AND INVENTTABLE.SPC_INVENTSUBGROUPID1 = SPC_INVENTSUBGROUP2.INVENTSUBGROUP1
		                AND SPC_INVENTSUBGROUP2.DATAAREAID = N'SPC'
                    LEFT OUTER JOIN SHOP2013TMP.dbo.UserInfo  WITH (NOLOCK)  ON SPC_TransferBlockedItemTable.MODIFIEDBY = UserInfo.ID  
            WHERE	SPC_TransferBlockedItemTable.DATAAREAID = N'SPC'  
		            AND GROUPID IN (
                        SELECT	GROUPID	
                        FROM	SHOP2013TMP.dbo.SPC_TRANSFERBLOCKEDTABLE WITH (NOLOCK)	
                        WHERE	INVENTLOCATIONID = '{pBch}' AND DATAAREAID = N'SPC')
            ORDER BY INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,SPC_TransferBlockedItemTable.ITEMID ,SPC_TransferBlockedItemTable.ITEMNAME,UNITID
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาการเปลี่ยนแปลงั้นวาง
        public static DataTable GetSHELFITEMS_CHANGE(string pBarcode, string pBch)
        {
            string sql = string.Format(@"
                    SELECT	SHOP_SHELFZONE.ZONE_ID,SHOP_SHELFZONE.ZONE_NAME,SHOP_SHELF.SHELF_ID,SHOP_SHELF.SHELF_NAME,
		                    OLD_Z.ZONE_ID AS ZONE_ID_OLD,OLD_Z.ZONE_NAME AS ZONE_NAME_OLD,
                            OLD_S.SHELF_ID AS SHELF_ID_OLD,OLD_S.SHELF_NAME AS SHELF_NAME_OLD,
                            SHOP_SHELFITEMS_CHANGE.BRANCH_ID,SHOP_SHELFITEMS_CHANGE.BRANCH_NAME,SHOP_SHELFITEMS_CHANGE.REAMRK

                    FROM	SHOP_SHELFITEMS_CHANGE WITH (NOLOCK)
		                    LEFT OUTER JOIN SHOP_SHELF WITH (NOLOCK)  ON SHOP_SHELFITEMS_CHANGE.SHELF_ID = SHOP_SHELF.SHELF_ID  
		                            AND SHOP_SHELFITEMS_CHANGE.BRANCH_ID = SHOP_SHELF.BRANCH_ID AND SHOP_SHELFITEMS_CHANGE.ZONE_ID = SHOP_SHELF.ZONE_ID
		                    LEFT OUTER JOIN SHOP_SHELFZONE WITH (NOLOCK) ON SHOP_SHELFITEMS_CHANGE.ZONE_ID = SHOP_SHELFZONE.ZONE_ID 
		                            AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID
		                    LEFT OUTER JOIN SHOP_SHELF OLD_S WITH (NOLOCK) ON SHOP_SHELFITEMS_CHANGE.SHELF_ID_OLD = OLD_S.SHELF_ID  
		                            AND SHOP_SHELFITEMS_CHANGE.BRANCH_ID = OLD_S.BRANCH_ID AND SHOP_SHELFITEMS_CHANGE.ZONE_ID = OLD_S.ZONE_ID
		                    LEFT OUTER JOIN SHOP_SHELFZONE OLD_Z WITH (NOLOCK) ON SHOP_SHELFITEMS_CHANGE.ZONE_ID_OLD = OLD_Z.ZONE_ID 
		                            AND SHOP_SHELF.BRANCH_ID = OLD_Z.BRANCH_ID
				
                    WHERE	SHOP_SHELFITEMS_CHANGE.BRANCH_ID = '" + pBch + @"' AND STA = '1' 
		                    AND ITEMBARCODE = '" + pBarcode + @"'
                ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาชั้นวางทั้งหมดของสาขา
        public static DataTable GetSHELF_Add(string pBch)
        {
            string str = string.Format(@"
                    SELECT	SHOP_SHELFZONE.ZONE_ID AS DATA_ID2,SHOP_SHELFZONE.ZONE_NAME AS DATA_DESC2,
                            SHOP_SHELF.SHELF_ID AS DATA_ID,
                            SHOP_SHELFZONE.ZONE_NAME + ' / ' + SHOP_SHELF.SHELF_ID + '-' +SHOP_SHELF.SHELF_NAME AS DATA_DESC
                    FROM	SHOP_SHELF WITH (NOLOCK)   
		                    INNER JOIN SHOP_SHELFZONE WITH (NOLOCK) ON SHOP_SHELF.ZONE_ID = SHOP_SHELFZONE.ZONE_ID 
			                    AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID 
                    WHERE	SHOP_SHELF.BRANCH_ID = '" + pBch + @"' 
                    ORDER BY SHOP_SHELFZONE.ZONE_ID,SHOP_SHELFZONE.ZONE_NAME    ");
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาสินค้าทั้งหมดตามชั้นวาง ตามสาขา
        public static DataTable GetSHELF_GetItemAll(string pShelfID, string pZoneID, string bch)
        {
            string sql = $@"
                SELECT	INVENTITEMBARCODE.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME,INVENTITEMBARCODE.UNITID
                FROM	SHOP_SHELFITEMS	WITH (NOLOCK)
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_SHELFITEMS.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                WHERE	SHELF_ID = '{pShelfID}' AND ZONE_ID = '{pZoneID}' AND BRANCH_ID = '{bch}'
                ORDER BY INVENTITEMBARCODE.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Set zone+shelf
        public static DataTable GetSHELFITEMS_Check(string barcode, string bch)
        {
            string str = string.Format(@"SELECT	SHOP_SHELFZONE.ZONE_ID,SHOP_SHELFZONE.ZONE_NAME,SHOP_SHELF.SHELF_ID,SHOP_SHELF.SHELF_NAME
                FROM	SHOP_SHELFITEMS WITH (NOLOCK)
		                INNER JOIN SHOP_SHELF WITH (NOLOCK)  ON SHOP_SHELFITEMS.SHELF_ID = SHOP_SHELF.SHELF_ID  
			                AND SHOP_SHELFITEMS.BRANCH_ID = SHOP_SHELF.BRANCH_ID AND SHOP_SHELFITEMS.ZONE_ID = SHOP_SHELF.ZONE_ID
		                INNER JOIN SHOP_SHELFZONE WITH (NOLOCK) ON SHOP_SHELFITEMS.ZONE_ID = SHOP_SHELFZONE.ZONE_ID 
			                AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID 
                WHERE	SHOP_SHELFITEMS.BRANCH_ID = '" + bch + @"'
		                AND ITEMBARCODE = '" + barcode + @"'
            ");
            return ConnectionClass.SelectSQL_Main(str);
        }
        //Insert ItemZone
        public static string Save_SHELFITEMS(string shelfID, string zoneID, string itembarcode, string unit, string oldZoneID, string oldShelfID, string rmk)
        {
            string strIn;
            if (oldZoneID == "")
            {
                strIn = $@"
                INSERT INTO [SHOP_SHELFITEMS]
                    ([SHELF_ID],[ZONE_ID],[BRANCH_ID],[BRANCH_NAME],
                    [ITEMBARCODE],[UNITID],
                    [WHOINS],[WHONAMEINS])
                VALUES (
                    '{shelfID}','{zoneID}','{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}',
                    '{itembarcode}','{unit}',
                    '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}'  )";
            }
            else
            {
                strIn = $@"
                INSERT INTO [SHOP_SHELFITEMS_CHANGE]
                    ([SHELF_ID_OLD],[ZONE_ID_OLD],[SHELF_ID],[ZONE_ID],
                    [BRANCH_ID],[BRANCH_NAME],[ITEMBARCODE],[WHOINS],[WHONAMEINS],[REAMRK])
                VALUES ('{oldShelfID}','{oldZoneID}','{shelfID}','{zoneID}',
                    '{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}',
                    '{itembarcode}',
                    '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}','{rmk}' )";
            }
            return strIn;
        }
        //Update ItemZone
        public static string Update_SHELFITEMS(string pCase, string shelfID, string zoneID, string itembarcode, string bchID)//0 และ 2 คือ Update Sta ด้วย
        {
            string strIn;
            if (pCase == "1")
            {
                strIn = $@"
                        UPDATE  SHOP_SHELFITEMS
                        SET     SHELF_ID = '{shelfID}',ZONE_ID = '{zoneID}',
                                WHOUP='{SystemClass.SystemUserID_M}',WHONAMEUP='{SystemClass.SystemUserName}',DATEUP = CONVERT(VARCHAR,GETDATE(),25)
                        WHERE   ITEMBARCODE = '{itembarcode}' AND BRANCH_ID = '{bchID}' ";
            }
            else
            {
                strIn = $@" 
                        UPDATE  SHOP_SHELFITEMS_CHANGE
                        SET     STA = '{pCase}',WHOUP='{SystemClass.SystemUserID_M}',WHONAMEUP='{SystemClass.SystemUserName}',DATEUP = CONVERT(VARCHAR,GETDATE(),25)
                        WHERE   ITEMBARCODE = '{itembarcode}' AND BRANCH_ID = '{bchID}' AND STA = '1' ";
            }
            return strIn;
        }
        //Copy Sheft+Zone
        public static string Copy_SHELFITEMS(string bchIDOld, string bchID, string bchName)
        {
            string sqlCopy = $@"
                DECLARE @BchID_OLD NVARCHAR(10) = '{bchIDOld}';
                DECLARE @BchID_NEW NVARCHAR(10) = '{bchID}';
                DECLARE @BchName_NEW NVARCHAR(10) = '{bchName}';

                INSERT INTO [dbo].[SHOP_SHELFITEMS]
                           ([SHELF_ID]
                           ,[ZONE_ID]
                           ,[BRANCH_ID]
                           ,[BRANCH_NAME]
                           ,[ITEMBARCODE]
                           ,[REAMRK]
                           ,[WHOINS]
                           ,[WHONAMEINS])

                SELECT	[SHELF_ID]
                           ,[ZONE_ID]
                           ,@BchID_NEW AS [BRANCH_ID]
                           ,@BchName_NEW AS [BRANCH_NAME]
                           ,SHOP_SHELFITEMS.[ITEMBARCODE]
                           ,[REAMRK]
                           ,'" + SystemClass.SystemUserID + @"' AS [WHOINS]
                           ,'" + SystemClass.SystemUserName + @"' AS [WHONAMEINS]
           
                FROM	SHOP_SHELFITEMS WITH (NOLOCK) 
		                INNER JOIN 
			                (SELECT	ISNULL(T1.BARCODE,T2.BARCODE) AS BARCODE 
			                FROM	(
			                SELECT	BARCODE 
			                FROM	SHOP2013TMP.dbo.SPC_BARCODEBYBRANCH WITH (NOLOCK) 
			                WHERE	YEAR BETWEEN YEAR(GETDATE()-180) AND YEAR(GETDATE()) AND BRANCH = @BchID_OLD
			                GROUP BY BARCODE
			                )T1 FULL OUTER JOIN (
			                SELECT	PRODUCT AS BARCODE 
			                FROM	SPC705SRV.[Staging].dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)   
			                WHERE	TRANSTYPE = '6'
					                AND TimePeriod BETWEEN GETDATE()-180 AND GETDATE()
					                AND INVENTSITEID = 'SPC'        AND BRANCH = @BchID_OLD
			                GROUP BY PRODUCT
			                )T2 ON T1.BARCODE = T2.BARCODE
			                COLLATE database_default)TMP ON SHOP_SHELFITEMS.ITEMBARCODE = TMP.BARCODE
		                LEFT OUTER JOIN
		                (SELECT	ITEMBARCODE
		                FROM	SHOP_SHELFITEMS WITH (NOLOCK) 
		                WHERE	BRANCH_ID = @BchID_NEW)BCH ON SHOP_SHELFITEMS.ITEMBARCODE = BCH.ITEMBARCODE

                WHERE	BRANCH_ID = @BchID_OLD
		                AND ISNULL(BCH.ITEMBARCODE,'0') = '0'
            ";
            return sqlCopy;
        }
        //get item all Sheft
        public static DataTable GetSHELFITEMS_All(string noShelf1, string changeShelf1, string bchShelf1)
        {
            string conditionShelf = "";
            if (noShelf1 != "") conditionShelf = " AND ISNULL(SHOP_SHELF.SHELF_ID,'') = '' ";

            string conditionChange = "";
            if (changeShelf1 != "") conditionChange = " AND ISNULL(SHOP_SHELFITEMS_CHANGE.ITEMBARCODE,'') != '' ";

            string conditionBch = "";
            if (bchShelf1 != "") conditionBch = $@" AND BRANCH = '{bchShelf1}' ";

            string sql = $@"
                    SELECT	SHOP_BRANCH.BRANCH_ID,SHOP_BRANCH.BRANCH_NAME,
		                    INVENTITEMBARCODE.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,
		                    INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,SPC_INVENTGROUPID AS GRPID0,SPC_INVENTGROUP.TXT AS TAX0,
		                    ISNULL(SHOP_SHELFZONE.ZONE_ID,'') AS ZONE_ID,ISNULL(SHOP_SHELFZONE.ZONE_NAME,'') AS ZONE_NAME,
		                    ISNULL(SHOP_SHELF.SHELF_ID,'') AS SHELF_ID,ISNULL(SHOP_SHELF.SHELF_NAME,'') AS SHELF_NAME,
                            ISNULL(SHOP_SHELFITEMS_CHANGE.ITEMBARCODE,'')  AS CC

                    FROM	(SELECT	ISNULL(T1.BARCODE,T2.BARCODE) AS BARCODE,ISNULL(T1.BRANCH,T2.BRANCH) AS BRANCH  
                                FROM	(
                                SELECT	BARCODE,BRANCH 
                                FROM	SHOP2013TMP.dbo.SPC_BARCODEBYBRANCH WITH (NOLOCK) 
                                WHERE	YEAR BETWEEN YEAR(GETDATE()-180) AND YEAR(GETDATE()) {conditionBch}
                                GROUP BY BARCODE,BRANCH
                                )T1 FULL OUTER JOIN (
                                SELECT	PRODUCT AS BARCODE,BRANCH 
                                FROM	SPC705SRV.[Staging].dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)   
                                WHERE	TRANSTYPE = '6'
		                                AND TimePeriod BETWEEN GETDATE()-180 AND GETDATE()
		                                AND INVENTSITEID = 'SPC'   {conditionBch}
                                GROUP BY PRODUCT,BRANCH
                                )T2 ON T1.BARCODE = T2.BARCODE
                            COLLATE database_default)SPC_BARCODEBYBRANCH

		                    INNER JOIN	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SPC_BARCODEBYBRANCH.BARCODE = INVENTITEMBARCODE.ITEMBARCODE 
				                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC' AND INVENTITEMBARCODE.SPC_ITEMACTIVE = '1' AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                    INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTITEMBARCODE.ITEMID = INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
                            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC'
	                        LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTGROUP.INVENTGROUP AND SPC_INVENTGROUP.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN	SHOP_SHELFITEMS WITH (NOLOCK) ON SPC_BARCODEBYBRANCH.BARCODE = SHOP_SHELFITEMS.ITEMBARCODE 
				                    AND SPC_BARCODEBYBRANCH.BRANCH = SHOP_SHELFITEMS.BRANCH_ID
		                    LEFT OUTER JOIN SHOP_SHELF WITH (NOLOCK)  ON SHOP_SHELFITEMS.SHELF_ID = SHOP_SHELF.SHELF_ID  
		                            AND SHOP_SHELFITEMS.BRANCH_ID = SHOP_SHELF.BRANCH_ID AND SHOP_SHELFITEMS.ZONE_ID = SHOP_SHELF.ZONE_ID
		                    LEFT OUTER JOIN SHOP_SHELFZONE WITH (NOLOCK) ON SHOP_SHELFITEMS.ZONE_ID = SHOP_SHELFZONE.ZONE_ID 
		                            AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID
		                    INNER JOIN	SHOP_BRANCH WITH (NOLOCK) ON SPC_BARCODEBYBRANCH.BRANCH = SHOP_BRANCH.BRANCH_ID
                            LEFT OUTER JOIN SHOP_SHELFITEMS_CHANGE WITH (NOLOCK) ON SPC_BARCODEBYBRANCH.BARCODE = SHOP_SHELFITEMS_CHANGE.ITEMBARCODE
				                    AND SPC_BARCODEBYBRANCH.BRANCH = SHOP_SHELFITEMS_CHANGE.BRANCH_ID AND STA = '1'

                    WHERE	INVENTITEMBARCODE.DATAAREAID = N'SPC'  {conditionBch}  {conditionShelf} {conditionChange}

                    GROUP BY	SHOP_BRANCH.BRANCH_ID,SHOP_BRANCH.BRANCH_NAME,INVENTITEMBARCODE.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME,INVENTITEMBARCODE.UNITID,
		                    INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,SPC_INVENTGROUPID,SPC_INVENTGROUP.TXT,
		                    ISNULL(SHOP_SHELFZONE.ZONE_ID,''),ISNULL(SHOP_SHELFZONE.ZONE_NAME,''),
		                    ISNULL(SHOP_SHELF.SHELF_ID,''),ISNULL(SHOP_SHELF.SHELF_NAME,''),ISNULL(SHOP_SHELFITEMS_CHANGE.ITEMBARCODE,'')

                    ORDER BY	SHOP_BRANCH.BRANCH_ID,INVENTTABLE.DIMENSION,DIMENSIONS.DESCRIPTION,SPC_INVENTGROUPID,SPC_INVENTGROUP.TXT,INVENTITEMBARCODE.SPC_ITEMNAME
		 
                    ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Item PriceChage
        public static DataTable GetPriceChange(string pCase, string buyerID, string date1, string date2, string conditionQty)//0 เปลี่ยนแปลงราคา 1 เปลี่ยนป้ายราคา
        {
            string sql;
            if (pCase == "0")
            {
                string conditionBuyerID = "";
                if (buyerID != "") conditionBuyerID = $@" AND SPC_ITEMBUYERGROUPID = '{buyerID}' ";
                sql = $@"
                    SELECT		SPC_INVENTITEMPRICEHISTORY.ITEMID,SPC_INVENTITEMPRICEHISTORY.INVENTDIMID,SPC_INVENTITEMPRICEHISTORY.UNITID, 
			                    INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMBUYERGROUPID+'-'+DIMENSIONS.DESCRIPTION AS DESCRIPTION,SPC_ITEMNAME,QTY, NEWPRICESALES,ORIGPRICESALES,
			                    MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)) as TRANSDATEEDIT ,
			                    CONVERT(VARCHAR,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)),25) AS TIMEEDIT  ,
			                    ISNULL(CONVERT(VARCHAR,TMP.DATEMAX,23),'1900-01-01') AS MAXPRT,
                                CASE ISNULL(CONVERT(VARCHAR,TMP.DATEMAX,23),'1900-01-01') WHEN CONVERT(VARCHAR,GETDATE(),23) THEN '0' ELSE '1' END AS C 

                    FROM		SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH (NOLOCK) 
			                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON  INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID  
					                    AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID  AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID  
			                    LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM 
			                    LEFT OUTER JOIN (SELECT	BRANCH,ITEMBARCODE,MAX(DATE) AS DATEMAX	
					                    FROM	SHOP_ITEMPRTBARCODE WITH (NOLOCK)   
					                    WHERE	BRANCH = 'RT' 
					                    GROUP BY	BRANCH,ITEMBARCODE )TMP ON INVENTITEMBARCODE.ITEMBARCODE = TMP.ITEMBARCODE 

                    WHERE		PRICEFIELDID IN (SELECT FieldID FROM dbo.Shop_Price WITH (NOLOCK) WHERE ColumnName = 'SPC_PriceGroup3' ) 
			                    AND CONVERT(VARCHAR,TRANSDATE,23)  BETWEEN '{date1}' AND '{date2}'
			                    AND SPC_ITEMBUYERGROUPID NOT IN ('D032','D110','D144','D164','D029','D081','D101','D120','D100','D005','D024','D074','D083','D106')  
                                {conditionBuyerID} {conditionQty}

                    GROUP BY	SPC_INVENTITEMPRICEHISTORY.ITEMID,SPC_INVENTITEMPRICEHISTORY.INVENTDIMID,QTY, 
			                    SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION,SPC_ITEMNAME, INVENTITEMBARCODE.ITEMBARCODE, 
			                    SPC_INVENTITEMPRICEHISTORY.UNITID, NEWPRICESALES, ORIGPRICESALES ,ISNULL(CONVERT(VARCHAR,TMP.DATEMAX,23),'1900-01-01') 

                    ORDER BY	SPC_ITEMBUYERGROUPID,SPC_ITEMNAME,QTY 
                    ";
            }
            else
            {
                string conditionBuyerID = "";
                if (buyerID != "") conditionBuyerID = $@" AND CheckDepart = '{buyerID}' ";
                sql = $@"
                    select	CheckWho + '-' + CheckWhoName AS CheckWhoName,SHOP_CHECKPRICEFOREXPORT.ITEMBARCODE,SPC_ITEMNAME,CheckPrice,UNITID,
		                    DptID + '-' + DptName AS DptName,CheckPDA,CheckDepart + '-' + CheckDepartName AS CheckDepartName,
		                    CONVERT(VARCHAR,CheckDate,25) AS CheckDate,ISNULL(CONVERT(VARCHAR,TMP.DATEMAX,23),'1900-01-01') AS MAXPRT,
                            CASE ISNULL(CONVERT(VARCHAR,TMP.DATEMAX,23),'1900-01-01') WHEN CONVERT(VARCHAR,GETDATE(),23) THEN '0' ELSE '1' END AS C 

                     FROM	SHOP_CHECKPRICEFOREXPORT WITH (NOLOCK) 
		                    LEFT OUTER JOIN  
		                    (	SELECT	BRANCH,ITEMBARCODE,MAX(DATE) AS DATEMAX	
			                    FROM	Shop_ItemPrtBarcode WITH (NOLOCK)    
			                    WHERE	BRANCH = 'RT' 
			                    GROUP BY	BRANCH,ITEMBARCODE )TMP ON SHOP_CHECKPRICEFOREXPORT.ITEMBARCODE = TMP.ITEMBARCODE  
                     WHERE	CONVERT(VARCHAR,CheckDate,23) BETWEEN  '{date1}'  and '{date2}' {conditionBuyerID}

                    ORDER BY CheckDate ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //บันทึกข้อมูลการเปลี่ยนแปลงราคา
        public static string Save_PriceChange(string itembarcode, string itemName, string unit, double PriceNew)
        {
            string sql = $@"
                INSERT INTO SHOP_ITEMPRTBARCODE (
                Branch,ItemBarcode,ItemName,ItemPrice, ItemUnit, WhoIns) VALUES ( 
                'RT','{itembarcode}','{ConfigClass.ChecKStringForImport(itemName)}','{PriceNew}','{unit}','{SystemClass.SystemUserID}')  ";
            return sql;
        }
        //Report MNBC
        public static DataTable GetMNBC(string bchID, string date1, string date2)
        {
            string conditionBchID = "";
            if (bchID != "") conditionBchID = $@" AND MNBCBranch = '{bchID}' ";

            string sql = $@"
                        SELECT	MNBCBranch,BRANCH_NAME,SHOP_MNBC_HD.MNBCDocNo, 
		                        convert(varchar,SHOP_MNBC_HD.MNBCDate,23) as MNBCDate ,MNBCUserCode+ CHAR(10) +MNBCUserName AS WHONAME, 
		                        MNBCBarcode, MNBCName, MNBCQty, MNBCPrice,SHOP_MNBC_DT.MNBCNet , MNBCPriceNet, MNBCUnitID, MNBCStaPurchase ,
		                        MNBCCstID + CHAR(10) + MNBCCstName + CHAR(10) + MNBCCstTel AS MNBCCstName,MNBCMoneyRound,
                                CASE MNBCStaPurchase WHEN '1' THEN '1' ELSE ShipSta END AS ShipSta,
                                CASE ShipSta WHEN '0' THEN CASE MNBCStaPurchase WHEN '1' THEN 'สาขาเอาของขึ้นรถโดยไม่ระบุในระบบ' ELSE '' END ELSE 	ShipCarID + CHAR(10) + ShipDriverID + CHAR(10) + WHOCAR.SPC_NAME END AS ShipDriverName
                        FROM	SHOP_MNBC_HD WITH (NOLOCK) 
			                        INNER JOIN SHOP_MNBC_DT  WITH (NOLOCK) on SHOP_MNBC_HD.MNBCDocNo=SHOP_MNBC_DT.MNBCDocNo 
			                        INNER JOIN Shop_Branch WITH (NOLOCK) on SHOP_MNBC_HD.MNBCBranch =Shop_Branch.BRANCH_ID  
			                        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WHOCAR WITH (NOLOCK) on SHOP_MNBC_HD.ShipDriverID = WHOCAR.EMPLID 
                         WHERE	MNBCStaDoc !='3' 
		                        AND SHOP_MNBC_HD.MNBCDate between '{date1}'  AND '{date2}'   {conditionBchID}
                        ORDER BY MNBCDocNo DESC ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Check Data in External List
        public static string Check_EXTERNALLIST_RETAILPLANORDERITEMBLOCKED(string itemID, string dimID)
        {
            string sql = $@"
                SELECT	*	
                FROM	 SPC_EXTERNALLIST
                WHERE   TABLENAME = 'SPC_RETAILPLANORDERITEMBLOCKED'            
                        AND  CONVERT(NVARCHAR, PKFIELDVALUE) = CONVERT(NVARCHAR, '{itemID}|{dimID}|{SystemClass.SystemBranchID}') ";
            return sql;
        }
        //Report MNBM
        public static DataTable GetDataMNBM(string setCase, string setGrpDate, string bchID, string date1, string date2, string pCon)
        {
            string sql = "";
            switch (setCase)
            {
                case "0":// All
                    string SetDateField = " CONVERT(VARCHAR,[DATEINS_DIS],23) "; if (setGrpDate == "1") SetDateField = " CONVERT(VARCHAR,[DATEINS],23) ";
                    sql = $@"
                    SELECT	[BRANCHID],[BRANCHNAME],[ITEMBARCODE_DIS],[SPC_ITEMNAME_DIS],[UNITID_DIS],[PRICE_DIS],
		                    [BOMID],[DATEINS_DIS],[WHOINS_DIS],[WHONAMEINS_DIS],
		                    [ITEMID],[INVENTDIMID],[ITEMBARCODE],[SPC_ITEMNAME],[UNITID],[QTY],[PRICE],
		                    [DPTID],[DPTNAME],[DATEINS],[WHOINS],[WHONAMEINS],[BOMID],
		                    [XXX_INVOICEID],[XXX_INVOICEDATE],[XXX_KEYBOARD],[XXX_INVOICEDATE],XXX_CASHIERID,XXX_CASHIERNAME,
                            ISNULL(CHECK_STA,'0') AS CHECK_STA,CASE ISNULL(CHECK_STA,'0') WHEN '1' THEN CASE [CHECK_REMARK] WHEN '' THEN 'เรียบร้อย' ELSE [CHECK_REMARK] END ELSE [CHECK_REMARK] END AS [CHECK_REMARK],
                            [CHECK_WHOID],[CHECK_WHONAME],CONVERT(VARCHAR,CHECK_DATE,25) AS CHECK_DATE,ISNULL(COUNT_PRINT,0) AS COUNT_PRINT,
                            '\\'+SHOP_BRANCH_CONFIGDB.BRANCH_PATHPOS+'\MNCamera\'+BRANCH_ID+'\'+SUBSTRING(CONVERT(VARCHAR,[XXX_INVOICEDATE],23),0,8)+'\'+CONVERT(VARCHAR,[XXX_INVOICEDATE],23)+'\'  AS PATH_WEBCAM,
                            XXX_INVOICEACCOUNT,XXX_INVOICEACCOUNTNAME,CASE PRICE WHEN '0' THEN 100 ELSE   100-((PRICE_DIS*100)/PRICE) END  AS PERCENT_DIS
                    FROM	[SHOP_BARCODEDISCOUNT] WITH (NOLOCK)
                            INNER JOIN SHOP_BRANCH_CONFIGDB WITH (NOLOCK) ON [SHOP_BARCODEDISCOUNT].BRANCHID = SHOP_BRANCH_CONFIGDB.BRANCH_ID
                    WHERE	{SetDateField} BETWEEN '{date1}' AND '{date2}'
                            {bchID} {pCon}
                    ORDER BY [BRANCHID],[ITEMBARCODE_DIS] DESC ";
                    break;
                case "1":// Report
                    string GroupDate = ""; if (setGrpDate == "1") GroupDate = " ,CONVERT(VARCHAR,SHOP_BARCODEDISCOUNT.DATEINS,23) AS DATEINS ";
                    sql = $@"
                        SELECT	SHOP_BARCODEDISCOUNT.BRANCHID,SHOP_BRANCH.BRANCH_NAME AS BRANCHNAME{GroupDate},
                                SUM(PRICE_DIS) AS SUM_DIS,COUNT(PRICE_DIS) AS COUNT_DIS,SUM(PRICE) AS SUM_PRICE
                        FROM	SHOP_BARCODEDISCOUNT WITH (NOLOCK) 
                                INNER JOIN SHOP_BRANCH  WITH (NOLOCK) ON SHOP_BARCODEDISCOUNT.BRANCHID  = SHOP_BRANCH.BRANCH_ID
                        WHERE	CONVERT(VARCHAR,SHOP_BARCODEDISCOUNT.DATEINS,23) BETWEEN '{date1}' AND '{date2}'   
                                {bchID} {pCon}
                        GROUP BY SHOP_BARCODEDISCOUNT.BRANCHID,SHOP_BRANCH.BRANCH_NAME {GroupDate}
                        ORDER BY SHOP_BARCODEDISCOUNT.BRANCHID,SHOP_BRANCH.BRANCH_NAME {GroupDate} ";
                    break;
                case "2":
                    string groupchHD = " '' AS  [BRANCHID],'' AS [BRANCHNAME], ", groupch = "";
                    if (setGrpDate == "1")
                    {
                        groupch = "[BRANCHID],[BRANCHNAME],";
                        groupchHD = "[BRANCHID],[BRANCHNAME],";
                    }
                    sql = $@"
                        DECLARE @date1 AS NVARCHAR(50) = '{date1}'
                        DECLARE @date2 AS NVARCHAR(50) = '{date2}'
                        DECLARE @i AS int 

                        set @i = DATEDIFF(dd,@date1,@date2)
                        IF (@i = 0) BEGIN set @i = 1 END  
 
                        SELECT	TMP1.*,ISNULL(COUNT_RECORD_SALE,0) AS COUNT_SALE,ISNULL(PRICESUM_DIS_SALE,0) AS PRICESUM_SALE,
                                (COUNT_RECORD)/@i AS AVG_RECORD,(PRICESUM_DIS)-(ISNULL(PRICESUM_DIS_SALE,0)) AS LOSS 
                        FROM	(
		                       SELECT	{groupchHD}[SHOP_BARCODEDISCOUNT].[ITEMBARCODE],
                                        CASE WHEN ISNULL(INVENTITEMBARCODE.[SPC_ITEMNAME],'') = '' THEN [SHOP_BARCODEDISCOUNT].[SPC_ITEMNAME] ELSE INVENTITEMBARCODE.[SPC_ITEMNAME] END AS SPC_ITEMNAME,
                                        NUM AS [DPTID], DIMENSIONS.DESCRIPTION AS [DPTNAME] ,INVENTITEMBARCODE.UNITID,
				                        SUM([SHOP_BARCODEDISCOUNT].[QTY]) AS QTYSUM,SUM([PRICE]) AS PRICESUM,SUM([PRICE_DIS]) AS PRICESUM_DIS,
				                        CASE SUM([PRICE]) WHEN '0' THEN '100' ELSE 100-((SUM([PRICE_DIS])*100)/SUM([PRICE])) END AS PERCENT_DIS,
				                        COUNT(*) AS COUNT_RECORD
		                        FROM	[SHOP_BARCODEDISCOUNT] WITH (NOLOCK)
				                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON [SHOP_BARCODEDISCOUNT].ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                                        LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM AND DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC' 
		                        WHERE	CONVERT(VARCHAR,[DATEINS],23) BETWEEN @date1 AND @date2
			                            AND BOMID LIKE 'MNBM%' 
                                        {bchID}
		                        GROUP BY {groupch}[SHOP_BARCODEDISCOUNT].[ITEMBARCODE],CASE WHEN ISNULL(INVENTITEMBARCODE.[SPC_ITEMNAME],'') = '' THEN [SHOP_BARCODEDISCOUNT].[SPC_ITEMNAME] ELSE INVENTITEMBARCODE.[SPC_ITEMNAME] END,[NUM],DIMENSIONS.DESCRIPTION,INVENTITEMBARCODE.UNITID
		                        )TMP1 LEFT OUTER JOIN
		                        (
		                        SELECT	{groupchHD}[ITEMBARCODE],SUM([PRICE_DIS]) AS PRICESUM_DIS_SALE,
				                        COUNT(*) AS COUNT_RECORD_SALE
		                        FROM	[SHOP_BARCODEDISCOUNT] WITH (NOLOCK)
		                        WHERE	CONVERT(VARCHAR,[DATEINS],23) BETWEEN @date1 AND @date2
			                            AND BOMID LIKE 'MNBM%' AND XXX_INVOICEID LIKE '%-%'
                                        {bchID}
		                        GROUP BY {groupch}[ITEMBARCODE]
		                        )TMP2 ON TMP1.BRANCHID = TMP2.BRANCHID AND TMP1.ITEMBARCODE = TMP2.ITEMBARCODE
                        ORDER BY COUNT_RECORD DESC
                    ";

                    break;
                default:
                    break;
            }


            return ConnectionClass.SelectSQL_Main(sql);
        }
        // การเปลี่ยนแปลงราคาสินค้าของมินิมาร์ท
        public static DataTable GetItemDetailPriceChange(string date1, string bchID, string priceLevel, string qty)
        {
            string sql = $@"
                        select	CONVERT(VARCHAR,ISNULL(DATEMAX,'1900-01-01'),23) AS DATEPRT  ,
		                        SPC_INVENTITEMPRICEHISTORY.ITEMID,SPC_INVENTITEMPRICEHISTORY.INVENTDIMID,SPC_INVENTITEMPRICEHISTORY.UNITID,  
		                        INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION,SPC_ITEMNAME,QTY,
                                CAST (ROUND(NEWPRICESALES,2) AS decimal(18,2)) AS   NEWPRICESALES,
		                        CAST (ROUND(ORIGPRICESALES,2) AS decimal(18,2)) AS ORIGPRICESALES,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)) as TRANSDATEEDIT  ,
		                        CONVERT(VARCHAR,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)),24) AS TIMEEDIT ,
                                CASE WHEN CONVERT(VARCHAR,ISNULL(DATEMAX,'1900-01-01'),23) >=	CONVERT(VARCHAR,'{date1}',23) THEN '0' ELSE '1' END AS STAPRT,
                                SPC_INVENTGROUPID AS GRPID0,SPC_INVENTGROUP.TXT AS TAX0

                        from	SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH (NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON  INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID  
				                        AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID   AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID   
                                INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON  INVENTITEMBARCODE.ITEMID =INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
		                        LEFT OUTER JOIN SHOP2013TMP.dbo. DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM  
		                        INNER JOIN 
				                        ( SELECT	BRANCH,BARCODE	FROM	SHOP2013TMP.dbo.SPC_BARCODEBYBRANCH WITH (NOLOCK) WHERE	BRANCH = '{bchID}' GROUP BY BRANCH,BARCODE )TMPSALE 
				                        ON INVENTITEMBARCODE.ITEMBARCODE = TMPSALE.BARCODE
		                        LEFT OUTER JOIN (  SELECT	BRANCH,ITEMBARCODE,MAX(DATE) AS DATEMAX 	FROM	SHOP_ITEMPRTBARCODE WITH (NOLOCK) 
				                        WHERE	BRANCH = '{bchID}' GROUP BY	BRANCH,ITEMBARCODE )TMPPRT ON  INVENTITEMBARCODE.ITEMBARCODE = TMPPRT.ITEMBARCODE 
                                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTGROUP.INVENTGROUP AND SPC_INVENTGROUP.DATAAREAID = N'SPC'

                        WHERE	PRICEFIELDID  IN (SELECT FieldID FROM Shop_Price WITH (NOLOCK) WHERE ColumnName = '{priceLevel}' ) 
		                        AND CONVERT(VARCHAR,TRANSDATE,23)  = '{date1}' 
                                AND SPC_ITEMBUYERGROUPID NOT IN ('D110','D164','D032','D144') AND INVENTITEMBARCODE.SPC_ItemActive = '1'  {qty}
		                        

                        GROUP BY	SPC_INVENTITEMPRICEHISTORY.ITEMID,SPC_INVENTITEMPRICEHISTORY.INVENTDIMID,QTY, 
			                        SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION,SPC_ITEMNAME, INVENTITEMBARCODE.ITEMBARCODE, SPC_INVENTITEMPRICEHISTORY.UNITID, 
			                        NEWPRICESALES, ORIGPRICESALES ,	ISNULL(DATEMAX,'1900-01-01')  ,
                                    CASE WHEN CONVERT(VARCHAR,ISNULL(DATEMAX,'1900-01-01'),23) <	CONVERT(VARCHAR,{date1},23) THEN '1' ELSE '0' END ,
                                    SPC_INVENTGROUPID,SPC_INVENTGROUP.TXT

                        ORDER BY	CONVERT(VARCHAR,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)),24),SPC_ITEMBUYERGROUPID,SPC_ITEMNAME,QTY   ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหา CN ทั้งหมด จาก Import File EXcel
        public static DataTable FarmHouse_FindCNExcel(string pDate, string bchID)
        {
            string coditionBranch = " AND INVENTLOCATIONID LIKE 'MN%' ";
            if (bchID != "") coditionBranch = "  AND INVENTLOCATIONID = 'RETAILAREA' ";
            string str = $@" 
                SELECT	SPC_ITEMBARCODE,SPC_ITEMNAME,UnitId,
                        FORMAT(CASE ISNULL(PRICECOST,0) WHEN '0' THEN SPC_PriceGroup3 ELSE PRICECOST END,'N2') AS COST,
                        SUM(CNQTY) AS CNQTY 
                FROM	Shop_VenderPR WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON Shop_VenderPR.SPC_ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
                        AND  DATAAREAID = N'SPC'  
                WHERE	recivedate = '{pDate}' 
                        {coditionBranch}
                        AND CNQTY > 0  AND INVOICEACCOUNT = 'V005450'
                GROUP BY SPC_ITEMBARCODE,FORMAT(CASE ISNULL(PRICECOST,0) WHEN '0' THEN SPC_PriceGroup3 ELSE PRICECOST END,'N2'),SPC_ITEMNAME,UnitId 
            ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาขอ้มูลเพื่อบันทึกรับสินค้า
        public static DataTable VenderAX_CheckBarcode_ForRecive(string _pDate, string _pVenderID, string _pBchID, string _pBarcode)
        {
            string sql = $@"
                SELECT	    ITEMBARCODE ,SPC_ITEMNAME ,UNITID ,ISNULL(ReciveQtyBch,0) AS QTYBCH,
                            ISNULL(ReciveQtySupc,0) AS ReciveQtySupc,ISNULL(ReciveQtySum,0) AS ReciveQtySum,ISNULL(ReciveItemBarcode,'0') AS STA 
                FROM	    SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)  LEFT OUTER JOIN 
			                (SELECT	ReciveItemBarcode,ReciveQtyBch,ReciveQtySupc,ReciveQtySum 
                            FROM	SHOP_VENDERCHECK WITH (NOLOCK) 
                            WHERE	ReciveDate = '{_pDate}' 
                                    AND ReciveVender = '{_pVenderID}' 
                                    AND ReciveBranchID = '{_pBchID}' )TMP ON INVENTITEMBARCODE.ITEMBARCODE = TMP.ReciveItemBarcode
                WHERE	    ITEMBARCODE IN  ({_pBarcode})
                ORDER BY    ITEMBARCODE ,SPC_ITEMNAME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหารายการสินค้าตามประเภท 01 สินค้าสั่ง 02 สินค้าแลกแต้ม
        public static DataTable ItemGroup_GetItemBarcode(string Dept, string ITEMGROUPTYPE, string ITEMGROUPITEM, string pCon)
        {
            string sql = $@"
             SELECT	'' as CheckBarcode, SHOP_ITEMGROUPBARCODE.ITEMGROUPID,	SHOP_ITEMGROUPBARCODE.ITEMGROUPID+ ' ' + ITEMGROUPNAME as ITEMGROUPNAME  ,
		            SHOP_ITEMGROUPBARCODE.ITEMBARCODE  , SPC_ITEMNAME , ITEMUNIT , ITEMSTATUS1 ,  ITEMSTATUS2 , 
		            ITEMREMARK  ,DIMENSIONS.Description,EMPLID,EMPLNAME,INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,
                    FORMAT(INVENTITEMBARCODE.SPC_REDEMPPOINT,'N2') as SPC_REDEMPPOINT,QTY,
		            CASE SPC_SalesPriceType 
                            WHEN '1' THEN 'บังคับใช้'	
                            WHEN '2' THEN 'แก้ไขได้'	
                            WHEN '3' THEN 'เครื่องชัง'	
                            WHEN '4' THEN 'เครื่องชั่งแพ็ค' 
                            WHEN '5' THEN 'น้ำหนัก' 
                    END AS SPC_SalesPriceTypeNAME,
                    ISNULL(SHOP_ITEMGROUPBARCODE.ITEMCOST,0) AS COSTITEM ,'0' AS STA,'0.00' AS QTYORDER 
            FROM	SHOP_ITEMGROUPBARCODE WITH (NOLOCK)
		            INNER JOIN SHOP_ITEMGROUP WITH (NOLOCK) ON SHOP_ITEMGROUP.ITEMGROUPID=SHOP_ITEMGROUPBARCODE.ITEMGROUPID
		            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE  WITH (NOLOCK) ON INVENTITEMBARCODE.ITEMBARCODE=SHOP_ITEMGROUPBARCODE.ITEMBARCODE
                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS  WITH (NOLOCK) ON DIMENSIONS.NUM = SHOP_ITEMGROUPBARCODE.ITEMDEPT
                   
            WHERE	ITEMDEPT = '{Dept}'
		            AND SHOP_ITEMGROUPBARCODE.ITEMGROUPTYPE='{ITEMGROUPTYPE}' and SHOP_ITEMGROUP.ITEMGROUPTYPE='{ITEMGROUPTYPE}'
                    AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID='{ITEMGROUPITEM}'  {pCon}
                    AND INVENTITEMBARCODE.DATAAREAID='SPC'  and DIMENSIONS.DATAAREAID='SPC'
            ORDER BY  SHOP_ITEMGROUPBARCODE.ITEMGROUPID, ITEMBARCODE		 
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable ItemGroup_GetItemBarcodeByGroup(string Dept, string Group, string ITEMGROUPTYPE)
        {
            string sql = $@"
                SELECT	'False' as CheckBarcode,SHOP_ITEMGROUPBARCODE.ITEMBARCODE  , SPC_ITEMNAME , ITEMUNIT    
                FROM	SHOP_ITEMGROUPBARCODE WITH(NOLOCK)
		                INNER JOIN SHOP_ITEMGROUP  WITH(NOLOCK) on SHOP_ITEMGROUP.ITEMGROUPID=SHOP_ITEMGROUPBARCODE.ITEMGROUPID
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) on INVENTITEMBARCODE.ITEMBARCODE=SHOP_ITEMGROUPBARCODE.ITEMBARCODE
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH(NOLOCK) on DIMENSIONS.NUM = SHOP_ITEMGROUPBARCODE.ITEMDEPT
                WHERE	ITEMDEPT = '{Dept}'
		                AND SHOP_ITEMGROUPBARCODE.ITEMGROUPID = '{Group}'
		                AND SHOP_ITEMGROUPBARCODE.ITEMGROUPTYPE = '{ITEMGROUPTYPE}'
		                AND INVENTITEMBARCODE.DATAAREAID='SPC'  and DIMENSIONS.DATAAREAID='SPC'
                        AND SHOP_ITEMGROUPBARCODE.ITEMSTATUS1 = '1' 
                ORDER BY  SHOP_ITEMGROUPBARCODE.ITEMGROUPID, ITEMBARCODE
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหาสินค้าทั้งหมด ตามกลุ่ม
        public static DataTable ItemGroup_GetItemByGroupForOrder(string grpSub)
        {
            string sql = string.Format(@"
            SELECT	INVENTITEMBARCODE.ITEMBARCODE AS ITEMBARCODE,SPC_ITEMNAME,UNITID,INVENTDIMID,ITEMID,QTY AS QTY_UNITID, 
                    '0' AS STA,'0.00' AS QTY,'0' AS BlockSend,0 AS STOCK,0 AS SALE ,CAST(ISNULL(ITEMCOST,'0') AS DECIMAL(12,2)) AS ITEMCOST 
            FROM	SHOP_ITEMGROUPBARCODE WITH (NOLOCK) 
                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON  SHOP_ITEMGROUPBARCODE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
            WHERE	ITEMGROUPID LIKE '" + grpSub + @"' AND ITEMSTATUS1 = '1'  
            Order BY SPC_ITEMNAME
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาสินค้าเพิื่อทำการรับบิล MNPI
        public static DataTable MNPI_FindItem(string _pTypeID, string bchID)
        {
            string str = $@"
            SELECT	ITEMID,INVENTDIMID,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,'0.00' AS QTY,UNITID,'0' AS STA,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC AS INVENT
            FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
		            INNER JOIN SHOP_CONFIGBRANCH_DETAIL  WITH (NOLOCK) ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = SHOP_CONFIGBRANCH_DETAIL.SHOW_ID
		            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = INVENTITEMBARCODE.ITEMBARCODE 
			            AND INVENTITEMBARCODE.DATAAREAID = N'SPC' AND SPC_ITEMACTIVE = '1'
            WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '{_pTypeID}'
		            AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
		            AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
		            AND BRANCH_ID = '{bchID}'
            ORDER BY SPC_ITEMNAME ";
            return ConnectionClass.SelectSQL_Main(str);
        }
        //ค้นหาการส่งสต็อก MNOS
        public static DataTable MNOS_GetItemSendStockByBuyer(string Date)
        {
            string sql = $@"
                    SELECT	[MNOSBranch],COUNT([MNOSBarcode]) AS ItemAll , [SPC_ITEMBUYERGROUPID]
                    FROM	SHOP_MNOS_DT WITH (NOLOCK) 
                            INNER JOIN SHOP_MNOS_HD WITH (NOLOCK) ON SHOP_MNOS_DT.MNOSDocNo  = SHOP_MNOS_HD.MNOSDocNo 
		                    INNER JOIN [SHOP2013TMP].[dbo].[INVENTITEMBARCODE] WITH (NOLOCK) ON SHOP_MNOS_DT.MNOSBARCODE = INVENTITEMBARCODE.ITEMBARCODE
		                    AND INVENTITEMBARCODE.DATAAREAID = 'SPC'
                    WHERE	[MNOSDate] = '{Date}'
		                    AND [MNOSStaDoc] = '1' 
                    GROUP BY [MNOSBranch],[SPC_ITEMBUYERGROUPID]
                    ORDER BY MNOSBranch   ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาการส่งสต็อก MNOS ตามแผนก
        public static DataTable MNOS_GetItems_ByDept(string Dept)
        {
            string sql = $@" 
                DECLARE @DeptD NVARCHAR(10) = '{Dept}';

                SELECT  distinct SHOP_ITEMGROUPSTOCK.itembarcode,SPC_ITEMNAME,UNITID  ,
                        SHOP_ITEMGROUPSTOCK.itembarcode + CHAR(13)  + CHAR(10) + SPC_ITEMNAME + CHAR(13)  + CHAR(10) + UNITID as ItemDetail
                FROM    SHOP_ITEMGROUPSTOCK  WITH (NOLOCK)
                        INNER JOIN shop2013tmp.dbo.INVENTITEMBARCODE with (nolock)  on SHOP_ITEMGROUPSTOCK.itembarcode=INVENTITEMBARCODE.itembarcode 
                        INNER JOIN SHOP_ITEMGROUPBARCODE WITH (NOLOCK) ON	 SHOP_ITEMGROUPSTOCK.ITEMBARCODE = SHOP_ITEMGROUPBARCODE.ITEMBARCODE 
									 AND SHOP_ITEMGROUPBARCODE.ITEMSTATUS1 = '1'
                WHERE   SHOP_ITEMGROUPSTOCK.ITEMDEPT = @DeptD
                        AND SPC_ITEMACTIVE='1' AND SPC_ITEMBUYERGROUPID = @DeptD 
                ORDER BY SHOP_ITEMGROUPSTOCK.itembarcode,SPC_ITEMNAME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable MNEC_FindSumForSendAR(string date1, string bchRoute)
        {
            string sqlSumQty = $@" SELECT	ITEMID,SPC_ITEMNAME,UNITID,ITEMBARCODE,INVENTDIMID,SPC_PRICEGROUP3,
		                                    SUM(MNECQty) AS MNECQty,SUM(MNECQty) * (SPC_PRICEGROUP3) AS MNECQTYALL, SUM(MNECSum) AS MNECSUM
                                    FROM	SHOP_MNEC_DT WITH (NOLOCK)	
		                                    INNER JOIN SHOP_MNEC_HD WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNEC_DT.MNECDocno
                                            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_MNEC_DT.MNECBarcode = INVENTITEMBARCODE.ITEMBARCODE AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                                    WHERE	MNECDateDoc = '{date1}'
		                                    AND MNECStaDoc = '1'
		                                    AND MNECStaApv = '1'
		                                    AND MNECBranchRoute = '{bchRoute}'
                                            AND MNECQty > 0 
                                    GROUP BY ITEMID,SPC_ITEMNAME,UNITID,ITEMBARCODE,INVENTDIMID,SPC_PRICEGROUP3
                                    ORDER BY SPC_PRICEGROUP3 ";

            return ConnectionClass.SelectSQL_Main(sqlSumQty);
        }
        //ค้นหาสินค้าโรงงานในส่วนของการผลิต
        public static DataTable GetDatailFactoryAll(string pCase, string pGroupDate, string pDate1, string pDate2)
        {
            string grpMakeAs = "", grpMake = "", grpSendAs = "", grpSend = "", grpReciveAS = "", grpRecive = "", grpMain = "";
            if (pGroupDate == "1")
            {
                //grpMakeAs = "CONVERT(VARCHAR,DATETIMEQTY,23) AS TRANSDATE, "; grpMake = "CONVERT(VARCHAR,DATETIMEQTY,23),";
                grpMakeAs = "CONVERT(VARCHAR,PRODDATE,23) AS TRANSDATE, "; grpMake = "CONVERT(VARCHAR,PRODDATE,23),";
                grpSendAs = "CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23) AS TRANSDATE,"; grpSend = " CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23),";
                grpReciveAS = "CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) AS TRANSDATE,"; grpRecive = "CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23),";
                grpMain = "ISNULL(IIF(ISNULL(T_MAKE.TRANSDATE,'') = '',T_SEND.TRANSDATE,T_MAKE.TRANSDATE),T_RECIVE.TRANSDATE) AS TRANSDATE,";
            }
            string sql = "";
            switch (pCase)
            {
                case "0":
                    sql = $@"
                        SELECT	SPC_ITEMBUYERGROUPID AS BUYERID,DIMENSIONS.DESCRIPTION AS BUYERNAME,T.*,ITEMBARCODE,INVENTITEMBARCODE_MINMAX.SPC_ITEMNAME,INVENTITEMBARCODE_MINMAX.UNITID
                                ,(ISNULL([OrderProductPlastic_INGREDIENTTABLE].COSTPRICE, 0)) COST ,INVENTITEMBARCODE_MINMAX.SPC_PRICEGROUP3 AS PRICE
                        FROM	(
                        SELECT	DISTINCT {grpMain}
		                        ISNULL(IIF(ISNULL(T_MAKE.ITEMID,'') = '',T_SEND.ITEMID,T_MAKE.ITEMID) ,T_RECIVE.ITEMID) AS ITEMID,
		                        ISNULL(IIF(ISNULL(T_MAKE.INVENTDIMID,'') = '',T_SEND.INVENTDIMID,T_MAKE.INVENTDIMID) ,T_RECIVE.INVENTDIMID) AS INVENTDIMID
                        FROM	(
			                        --SELECT	{grpMakeAs}INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID
			                        --FROM	[OrderProductPlastic_STOCKALLDAY] ST WITH(NOLOCK)
					                --        INNER JOIN [SHOP2013TMP].[dbo].[INVENTITEMBARCODE] WITH(NOLOCK) ON ST.[BARCODE] = INVENTITEMBARCODE.ITEMBARCODE
			                        --WHERE	[STATUS] = 1 AND CONVERT(VARCHAR,DATETIMEQTY,23) BETWEEN '{pDate1}' AND  '{pDate2}'
			                        --GROUP BY {grpMake}INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID
                                    SELECT	{grpMakeAs}INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID            
                                    FROM	[OrderProductPlastic_STOCKALLDAYTABLE] STABLE WITH(NOLOCK)
		                                    INNER JOIN [SHOP2013TMP].[dbo].[INVENTITEMBARCODE] WITH(NOLOCK) ON STABLE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                                    WHERE	STABLE.STATUS = 1 AND CONVERT(VARCHAR,PRODDATE,23) BETWEEN '{pDate1}' AND '{pDate2}'
                                    GROUP BY {grpMake}INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID 
		                        )T_MAKE
		                        FULL OUTER  JOIN 
		                        (
			                        SELECT	{grpSendAs}ITEMID,INVENTDIMID 
			                        FROM	ProductRecive_FACTORY WITH (NOLOCK)
			                        WHERE	CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23) BETWEEN '{pDate1}' AND  '{pDate2}' AND ITEMSTA = '1' 
			                        GROUP BY {grpSend}ITEMID,INVENTDIMID
		                        )T_SEND ON T_MAKE.ITEMID = T_SEND.ITEMID AND T_MAKE.INVENTDIMID = T_SEND.INVENTDIMID 
		                        FULL OUTER  JOIN 
		                        (
			                        SELECT	{grpReciveAS}ITEMID,INVENTDIMID
			                        FROM	ProductRecive_DROID WITH (NOLOCK)
					                        INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
						                        AND TYPE_CONFIG = '60' AND STA = '1'		
			                        WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) BETWEEN '{pDate1}' AND  '{pDate2}'  AND ITEMSTA = '1' 
			                        GROUP BY  {grpRecive}ITEMID,INVENTDIMID
		                        )T_RECIVE ON T_MAKE.ITEMID = T_RECIVE.ITEMID AND T_MAKE.INVENTDIMID = T_RECIVE.INVENTDIMID
		                        )T
		                        INNER JOIN	SHOP2013TMP.dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) ON T.INVENTDIMID = INVENTITEMBARCODE_MINMAX.INVENTDIMID	
			                        AND T.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID AND TYPE_ = 'MIN' AND DATAAREAID = 'SPC'
		                        INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE_MINMAX.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM
			                        AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                                LEFT OUTER JOIN [OrderProductPlastic_INGREDIENTTABLE]  WITH (NOLOCK) ON INVENTITEMBARCODE_MINMAX.ITEMBARCODE = [OrderProductPlastic_INGREDIENTTABLE].BARCODE 
                        ORDER BY SPC_ITEMBUYERGROUPID,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID
                    ";
                    break;
                case "1":
                    string docDate = "";
                    if (grpMakeAs != "") docDate = "TRANSDATE,";
                    sql = $@"
		                (
			                --SELECT	'MAKE' AS STA,{docDate}ITEMID,INVENTDIMID,SUM(SUMQTY) AS QTY 
			                --FROM	(
						    --            SELECT	{grpMakeAs}[DOCID],INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,ST.[QTY],(ST.[QTY] *INVENTITEMBARCODE.QTY) AS SUMQTY
						    --            FROM	[OrderProductPlastic_STOCKALLDAY] ST WITH(NOLOCK)
							--	                INNER JOIN [SHOP2013TMP].[dbo].[INVENTITEMBARCODE] WITH(NOLOCK) ON ST.[BARCODE] = INVENTITEMBARCODE.ITEMBARCODE
						    --            WHERE	[STATUS] = 1 AND CONVERT(VARCHAR,DATETIMEQTY,23) BETWEEN '{pDate1}' AND  '{pDate2}'
						    --            GROUP BY {grpMake}[DOCID],INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,ST.[QTY],(ST.[QTY] *INVENTITEMBARCODE.QTY)
					        --        )TMP                    
			                --GROUP BY {docDate}ITEMID,INVENTDIMID
                            SELECT	'MAKE' AS STA,{docDate}ITEMID,INVENTDIMID,SUM(SUMQTY) AS QTY 
                            FROM	(
                                    SELECT	{grpMakeAs}INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,STABLE.[DOCID] ,STABLE.[PRODQTY] ,(INVENTITEMBARCODE.QTY*STABLE.[PRODQTY]) AS SUMQTY
                                    FROM	[OrderProductPlastic_STOCKALLDAYTABLE] STABLE WITH(NOLOCK)
				                             INNER JOIN [SHOP2013TMP].[dbo].[INVENTITEMBARCODE] WITH(NOLOCK) ON STABLE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
                                    WHERE	STABLE.STATUS = 1
				                            AND CONVERT(VARCHAR,PRODDATE,23) BETWEEN '{pDate1}' AND '{pDate2}'
                                    GROUP BY {grpMake}INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,STABLE.[DOCID] ,STABLE.[PRODQTY] ,(INVENTITEMBARCODE.QTY*STABLE.[PRODQTY])
                                    )TMP
                            GROUP BY {docDate}ITEMID,INVENTDIMID	
		                ) 
		                UNION
		                (
			                SELECT	'SEND' AS STA,{grpSendAs}ITEMID,INVENTDIMID,SUM(QTYSEND*QTY) AS QTY 
			                FROM	ProductRecive_FACTORY WITH (NOLOCK)
			                WHERE	CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23) BETWEEN '{pDate1}' AND  '{pDate2}' AND ITEMSTA = '1' 
			                GROUP BY {grpSend}ITEMID,INVENTDIMID
		                ) 
		                UNION
		                (
		                SELECT	'RECIVE' AS STA,{grpReciveAS}ITEMID,INVENTDIMID,SUM(ProductRecive_DROID.QTYRECIVE*QTY) AS QTY
		                FROM	ProductRecive_DROID WITH (NOLOCK)
				                INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
					                AND TYPE_CONFIG = '60' AND STA = '1'		
		                WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) BETWEEN '{pDate1}' AND  '{pDate2}'  AND ITEMSTA = '1' 
		                GROUP BY  {grpRecive}ITEMID,INVENTDIMID
		                )  ";
                    break;
                default:
                    break;
            }
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid);
        }

        public static DataTable FindItemOrderMNOI(string pDate1, string groupmain, string sub)
        {
            string groupsub = "";
            if (sub != "") groupsub = $@"AND GROUPID = '{sub}'";
            string sql = $@"
                    SELECT	SHOP_ITEMORDERBARCODE.ITEMBARCODE, INVENTITEMBARCODE.SPC_ITEMNAME, INVENTITEMBARCODE.UNITID,
                            INVENTITEMBARCODE.SPC_PRICEGROUP13 AS PRICE,
		                    ITEMGROUPNAME,ISNULL(MNOI.QTYORDER,'') AS QTYORDER, ISNULL(MNOI.REMARK,0) AS REMARK
                    FROM	SHOP_ITEMORDERBARCODE
		                    INNER JOIN SHOP_ITEMORDERGROUP WITH (NOLOCK)
			                    ON SHOP_ITEMORDERBARCODE.GROUPSUB = SHOP_ITEMORDERGROUP.ITEMGROUPID
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
			                    ON INVENTITEMBARCODE.ITEMBARCODE = SHOP_ITEMORDERBARCODE.ITEMBARCODE
			                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                                AND INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = '{groupmain}'
		                    LEFT JOIN (
			                    SELECT	ITEMBARCODE,SPC_ITEMNAME,UNITID, SUM(QTYORDER) AS QTYORDER, 
					                    SHOP_MNOI_DT.REMARK AS REMARK
			                    FROM	SHOP_MNOI_HD WITH (NOLOCK)
					                    INNER JOIN SHOP_MNOI_DT WITH (NOLOCK)
						                    ON SHOP_MNOI_HD.DOCNO = SHOP_MNOI_DT.DOCNO
			                    WHERE	DATE_RECIVE = '{pDate1}'
					                    AND STA_DOC = '1'
					                    AND STADT = '1'
                                        {groupsub}
			                    GROUP BY ITEMBARCODE,SPC_ITEMNAME,UNITID,SHOP_MNOI_DT.REMARK) MNOI
			                    ON INVENTITEMBARCODE.ITEMBARCODE = MNOI.ITEMBARCODE
                    WHERE	SHOP_ITEMORDERBARCODE.ITEMDEPT = '{groupmain}'
                    ORDER BY ITEMBARCODE ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable FindItemBarcodeMNOI(string groupMain, string group)
        {
            string groupsub = "";
            if (group != "") groupsub = $@"AND GROUPSUB = '{group}'";
            string sql = $@"
                    SELECT	INVENTITEMBARCODE.ITEMBARCODE, INVENTITEMBARCODE.SPC_ITEMNAME, INVENTITEMBARCODE.UNITID, ITEMDEPT, VENDERID,GROUPSUB, STA,
		                    ISNULL(PRICE,0) AS COSRPERQTY,INVENTITEMBARCODE.QTY,
		                    CASE WHEN ISNULL(PRICE*INVENTITEMBARCODE.QTY,0) = 0 THEN 0
			                     ELSE PRICE*INVENTITEMBARCODE.QTY
		                    END AS COST, 
		                    INVENTITEMBARCODE.SPC_PRICEGROUP13 AS PRICE
                    FROM	SHOP_ITEMORDERBARCODE
		                    INNER JOIN SHOP_ITEMORDERGROUP WITH (NOLOCK)
			                    ON SHOP_ITEMORDERBARCODE.GROUPSUB = SHOP_ITEMORDERGROUP.ITEMGROUPID
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
			                    ON INVENTITEMBARCODE.ITEMBARCODE = SHOP_ITEMORDERBARCODE.ITEMBARCODE
			                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                                AND INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = 'D083'
		                    INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK)
			                    ON INVENTDIM.INVENTDIMID = INVENTITEMBARCODE.INVENTDIMID
			                    AND INVENTDIM.DATAAREAID = N'SPC'
		                    LEFT JOIN SPC708SRV.AX50SP1_SPC.DBO.RPTINVENTITEMPRICE_LAST WITH (NOLOCK) 
			                    ON INVENTITEMBARCODE.ITEMID = RPTINVENTITEMPRICE_LAST.ITEMID
			                    AND RPTINVENTITEMPRICE_LAST.DATAAREAID  = N'SPC'
			                    AND INVENTDIM.CONFIGID = RPTINVENTITEMPRICE_LAST.CONFIGID 
                                AND INVENTDIM.INVENTSIZEID = RPTINVENTITEMPRICE_LAST.INVENTSIZEID
                                AND INVENTDIM.INVENTCOLORID = RPTINVENTITEMPRICE_LAST.INVENTCOLORID
                    WHERE	STA = '1'
		                    AND ITEMDEPT = '{groupMain}'
		                    {groupsub}
                    ORDER BY ITEMBARCODE ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable FindBarcodeForOrderMNOI(string Dept, string pCon)
        {
            string sql = $@"
                    SELECT	INVENTITEMBARCODE.ITEMBARCODE AS ITEMBARCODE,SPC_ITEMNAME,UNITID,SHOP_ITEMORDERBARCODE.INVENTDIMID,SHOP_ITEMORDERBARCODE.ITEMID,QTY AS QTY_UNITID,
		                    VENDERID, VENDERNAME,'0' AS STA,'0.00' AS QTY,'0' AS BlockSend, '0' AS REMARK
                    FROM	SHOP_ITEMORDERBARCODE WITH (NOLOCK)
		                    INNER JOIN 	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
			                    ON SHOP_ITEMORDERBARCODE.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
			                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                    WHERE	SHOP_ITEMORDERBARCODE.ITEMDEPT = '{Dept}'
		                    {pCon}
                            AND SHOP_ITEMORDERBARCODE.STA = '1'
                    ORDER BY SPC_ITEMNAME
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายงาน สินค้าที่รับเข้าไม่มีน้ำหนัก/ปริมาตร
        public static DataTable ReportProductRecive_NoWeight(string date1, string date2, string staW, string staC)
        {
            string UpData = $@"
            UPDATE	SupcAndroid.dbo.ProductRecive_NoWeight	
            SET     STATUS = IIF(SPC_BarCodeWeight=0,'0','1'),STATUS_CAPACITY = IIF((SPC_BarCodeWidth*SPC_BarCodeLength*SPC_BarCodeHigh)=0,'0','1')
            FROM	SupcAndroid.dbo.ProductRecive_NoWeight WITH (NOLOCK)
		            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ProductRecive_NoWeight.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
            WHERE	STATUS = 0 OR STATUS_CAPACITY = 0 ";
            ConnectionClass.ExecuteSQL_Main(UpData);

            string condition_W = "", condition_C = "";
            if (staW == "1") condition_W = $@" AND ProductRecive_NoWeight.STATUS = '0' ";
            if (staC == "1") condition_C = $@" AND STATUS_CAPACITY = '0' ";
            if ((staC == "1") && (staW == "1")) { condition_W = $@" AND (ProductRecive_NoWeight.STATUS = '0'  OR STATUS_CAPACITY = '0') "; condition_C = ""; }


            string sql = $@"
                SELECT	ProductRecive_NoWeight.STATUS,STATUS_CAPACITY, 
		                CONVERT(VARCHAR,DATETIMERECIVE,23) AS DATERECIVE,CONVERT(VARCHAR,DATETIMERECIVE,24) AS TIMERECIVE,INVENTLOCATION,
		                ProductRecive_NoWeight.ITEMBARCODE,SPCITEMNAME,ProductRecive_NoWeight.UNITID,BUYERID,BUYERNAME,
		                EMPLIDRECIVE,EMPLNAMERECIVE,DIMENSION,DIMENSIONNAME,DEVICENAME,VENDID,VENDNAME,ProductRecive_NoWeight.STATUS,STATUS_CAPACITY
                FROM	SupcAndroid.dbo.ProductRecive_NoWeight WITH (NOLOCK)
		                LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) ON ProductRecive_NoWeight.BUYERID = SHOP_CONFIGBRANCH_DETAIL.SHOW_ID 
			                AND TYPE_CONFIG  = '67' AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
                WHERE	ISNULL(SHOW_ID,'') != ''
                        AND CONVERT(VARCHAR,DATETIMERECIVE,23) BETWEEN '{date1}' AND '{date2}' 
                        {condition_C} {condition_W}
                ORDER BY DATETIMERECIVE ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

    }
}
