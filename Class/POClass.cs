﻿using System;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class
{
    class POClass
    {
        //top 1 ค้นหาการสั่ง ล่าสุด ย้อนหลัง เปนการหาเฉพาะรายการที่ทำบิลแล้วเท่านั้น
        public static DataTable GetINVENTTRANSFERJOURLINE_ByDim(string pInvent, string pItemID, string pDim) //pDate คือ จำนวนวันที่ต้องการหาย้อนหลัง
        {
            //string sql = $@" POClass_GetINVENTTRANSFERJOURLINE_ByDim '{pInvent}','{pItemID}','{pDim}','{pDate}' ";
            //DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            //if (dt.Rows.Count == 0)
            //{
            //    dt = GetINVENTTRANSFERLINE_ByDim(pInvent, pItemID, pDim, pDate);
            //}
            //return dt;

            return ConnectionClass.SelectSQL_Main($@" POClass_GetDetailItembarcode '1','{pInvent}','','{pItemID}','{pDim}'"); ;

        }
        ////top 1 ค้นหาการสั่ง ที่ทั้งหมด ทั้งที่ทำบิลหรือยังไม่ทำให้หา ด้านบนก่อน ถ้าไม่มีค่อยมาหาที่นี่
        //public static DataTable GetINVENTTRANSFERLINE_ByDim(string pInvent, string pItemID, string pDim, int pDate) //pDate คือ จำนวนวันที่ต้องการหาย้อนหลัง
        //{
        //    return ConnectionClass.SelectSQL_Main($@" POClass_GetINVENTTRANSFERLINE_ByDim  '{pInvent}','{pItemID}','{pDim}','{pDate}' ");
        //}
        //ค้นหา PO ที่พร้อมส่งเข้า AX
        public static DataTable GetDataDetailMNPO_ForSendAX(string pCase, string pDocno, string pCondition)
        {
            //return ConnectionClass.SelectSQL_Main($@" POClass_GetDataDetailMNPO_ForSendAX '{pDocno}' ");
            return ConnectionClass.SelectSQL_Main($@" POClass_GetDataDetailMNPO_ForSendAX_TRANSWEB '{pCase}','{pDocno}','{pCondition}' ");
        }
        //ค้นหา PD ที่พร้อมส่งเข้า AX
        public static DataTable GetDataDetailMNPD_ForSendAX(string pDocno)
        {
            return ConnectionClass.SelectSQL_Main($@" CashDesktop_OrderCust '2','{pDocno}' ");
        }
        //Find Lock
        public static DataTable GetLock_SUPC()
        {
            string sql = $@"
            SELECT   CODE,CODE + ' ' + TXT As CODE_TXT 
            FROM     SHOP2013TMP.dbo.DLVTERM  WITH (NOLOCK) 
            ORDER BY CODE DESC ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //    INVENTTRANSFERTABLE   
        public static DataTable FindStaOrderINAX_ByTranID(string tranID)
        {
            string str = $@"
                select	INVENTTRANSFERTABLE.Transferid,INVENTLOCATIONIDFROM,INVENTLOCATIONIDTO,SPC_LOCATIONNAMETO,  
		                INVENTTRANSFERTABLE.CREATEDDATETIME,INVENTTRANSFERLINE.ITEMID,INVENTDIMID,SPC_ITEMBARCODEUNIT,QTYRECEIVED,  
		                SPC_ITEMBARCODE,SPC_ITEMNAME,SPC_QTY,UNITID,INVENTTRANSFERLINE.SPC_REMARKS,INVENTTRANSFERLINE.RemainStatus, 
		                CONVERT(VARCHAR,INVENTTRANSFERTABLE.SHIPDATE,23) as SHIPDATE,INVENTTRANSFERLINE.InventTransId  ,
		                CASE RemainStatus	WHEN '0' THEN 
								                CASE QTYRECEIVED WHEN '0' THEN 'ออเดอร์ถูกยกเลิก' + CHAR(10) + 'สั่งใหม่ได้เลย' ELSE 'รับครบแล้ว' END 
							                WHEN '1' THEN InventTransId + CHAR(10) + 'กำลังส่งสินค้า' 
							                WHEN '2' THEN InventTransId + CHAR(10) + 'กำลังจัดสินค้า' 
							                ELSE 'ไม่พบข้อมูล'
		                END AS STA
                from	SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK)  
		                INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK) ON INVENTTRANSFERTABLE.Transferid = INVENTTRANSFERLINE.Transferid  
		                INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTTRANSFERLINE.ITEMID = INVENTTABLE.ITEMID  
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON  INVENTTABLE.Dimension = DIMENSIONS.NUM  
                where	INVENTTRANSFERLINE.InventTransId  = '{tranID}' 
	                    AND INVENTTRANSFERTABLE.DATAAREAID = N'SPC' AND INVENTTRANSFERLINE.DATAAREAID = N'SPC' AND INVENTTABLE.DATAAREAID = N'SPC' AND DIMENSIONS.DATAAREAID = N'SPC' 
                Order By INVENTTRANSFERTABLE.SHIPDATE 
            ";
            return ConnectionClass.SelectSQL_Main(str);
        }

        //report ข้อมูลการสั่งสินค้า
        public static DataTable Report_DetailMNPO(string date1, string date2, string bchID, string billID, string typeID, string wh, string dptID)
        {
            string conBch = "";
            if (bchID != "") conBch = $@" AND INVENTLOCATIONIDTO = '{bchID}' ";
            string conBill = "", conWH = "", conDpt = "";
            if (billID != "")
            {
                conBill = $@" AND INVENTTRANSFERTABLE.Transferid LIKE '{billID}%' 
                            AND SPC_ITEMBARCODE IN ( SELECT	SHOW_ID FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) WHERE	TYPE_CONFIG = '{typeID}' ) ";
            }
            if (wh != "") conWH = $@" AND INVENTLOCATIONIDFROM = '{wh}'";
            if (dptID != "") conDpt = $@" AND NUM  = '{dptID}' ";


            string sqlSelect = $@" 
                       SELECT	
		                        INVENTTRANSFERTABLE.Transferid,INVENTLOCATIONIDFROM,INVENTLOCATIONIDTO,SPC_LOCATIONNAMETO,  
                                INVENTLOCATIONIDTO+' '+SPC_LOCATIONNAMETO AS BRANCH_NAME,
		                        CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),23) AS DateCreate ,
		                        CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),24) AS TimeCreate ,
		                        CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),25) AS DateTimeCreate ,
		                        INVENTTRANSFERLINE.ITEMID,INVENTDIMID,
		                        SPC_ITEMBARCODE,SPC_ITEMNAME,SPC_QTY,SPC_ITEMBARCODEUNIT,QtyTransfer,QTYRECEIVED, UNITID,
		                        INVENTTRANSFERLINE.SPC_REMARKS,
		                        INVENTTRANSFERLINE.RemainStatus,
		                        CASE INVENTTRANSFERLINE.RemainStatus WHEN '0' THEN 'ได้รับแล้ว' WHEN '1' THEN 'จัดส่งสินค้าแล้ว' ELSE 'สร้างแล้ว' END AS RemainStatus_DESC,
		                        CONVERT(VARCHAR,INVENTTRANSFERTABLE.SHIPDATE,23) as SHIPDATE,INVENTTRANSFERLINE.InventTransId ,NUM,DESCRIPTION AS NUM_DESC

                        FROM	SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK) 
			                        INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK) ON INVENTTRANSFERTABLE.Transferid = INVENTTRANSFERLINE.Transferid 
			                        INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTTRANSFERLINE.ITEMID = INVENTTABLE.ITEMID 
                                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONCODE = '0' 

                        WHERE	INVENTTRANSFERTABLE.SHIPDATE BETWEEN '{date1}' AND '{date2}' 
		                        AND INVENTTRANSFERTABLE.DATAAREAID = N'SPC' 
                                AND INVENTTABLE.DATAAREAID = N'SPC' 
                                AND INVENTTRANSFERLINE.DATAAREAID = N'SPC' 
                                AND DIMENSIONS.DATAAREAID = N'SPC'
		                        AND INVENTLOCATIONIDTO LIKE 'MN%'            
                                {conBch} {conBill } {conWH} {conDpt}
                         ORDER BY INVENTLOCATIONIDTO,INVENTTRANSFERTABLE.Transferid,INVENTTRANSFERTABLE.SHIPDATE   ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //report ข้อมูลการสั่งสินค้า สำหรับการใช้ รหัสการติดตามมาช่วย
        public static DataTable Report_DetailMNPOBySPC_TRACKINGID(string pCaseOrder, string date1, string date2, string bchID, string dptID, string itemID, string inventDim)////pCaseOrder 0 = ทั้งหมด 1 เฉพาะสาขาสั่ง
        {
            string conBch = "";
            if (bchID != "") conBch = $@" AND INVENTLOCATIONIDTO = '{bchID}' ";

            string conDpt = "";
            if (dptID != "") conDpt = $@" AND INVENTTRANSFERLINE.SPC_ITEMBUYERGROUPID  = '{dptID}' ";

            string conOpen = "";
            if (pCaseOrder == "1") conOpen = " AND (SPC_TRACKINGID LIKE 'MNPO%' OR  SPC_TRACKINGID LIKE 'MNPD%') ";

            string conItemId = "";
            if (itemID != "") conItemId = $@" AND INVENTITEMBARCODE.ITEMID = '{itemID}' AND INVENTITEMBARCODE.INVENTDIMID = '{inventDim}' ";


            string sqlSelect = $@" 
            SELECT	IIF(QtyShipped = 0 AND QtyRemainShip = 0,'ยกเลิก',IIF(INVENTTRANSFERLINE.QTYRECEIVED>0,'ได้รับแล้ว',IIF(INVENTTRANSFERLINE.QTYSHIPPED>0,'กำลังจัดส่ง','กำลังจัดสินค้า'))) AS STATUS,
		            IIF(QtyShipped = 0 AND QtyRemainShip = 0,'0',IIF(INVENTTRANSFERLINE.QTYRECEIVED>0,'3',IIF(INVENTTRANSFERLINE.QTYSHIPPED>0,'1','2'))) AS STATUSID,
		            --IIF(INVENTTRANSFERTABLE.TRANSFERID LIKE 'MRT%','ออเดอร์ MNOR',IIF(SPC_TRACKINGID LIKE 'MN%','สาขาสั่ง','ไม่ได้สั่ง')) AS STAORDER,
                    IIF(INVENTTRANSFERTABLE.TRANSFERID LIKE 'MRT%','ออเดอร์ผ่านMNOR',
			            IIF(INVENTTRANSFERTABLE.TRANSFERID LIKE 'MNPT%','ระบบจัดซื้อจัดสินค้าเอง',
			            IIF(INVENTTRANSFERTABLE.TRANSFERID LIKE 'MNPF%','สาขามาจัดสินค้าเอง',
				            IIF(SPC_TRACKINGID LIKE 'MNPO%','สาขาสั่ง',
				            IIF(SPC_TRACKINGID LIKE 'MNPD%','ออเดอร์ลูกค้าสั่ง',
				            IIF(SPC_TRACKINGID LIKE 'MNPI%','น้ำแข็ง/อาหารกล่อง',
				            IIF(SPC_TRACKINGID LIKE 'MNT%','พื้นเมืองส่ง',
				            IIF(SPC_TRACKINGID LIKE 'MO%','เซลล์เปิดบิลสั่ง','ไม่มีประวัติสั่ง')))))
		            )))  AS STAORDER,
		            CONVERT(VARCHAR,INVENTTRANSFERTABLE.CREATEDDATETIME,23) AS TRANSDATE,
		            INVENTTRANSFERTABLE.TRANSFERID,INVENTTRANSFERTABLE.INVENTLOCATIONIDFROM,INVENTTRANSFERTABLE.INVENTLOCATIONIDTO,INVENTTRANSFERLINE.SPC_LOCATIONNAMETO,
		            INVENTTRANSFERLINE.SPC_ItemBarCode,INVENTTRANSFERLINE.SPC_ItemName,INVENTTRANSFERLINE.SPC_Qty,SPC_ItemBarCodeUnit,
		            INVENTTRANSFERLINE.INVENTTRANSID,INVENTTRANSFERLINE.SPC_TRACKINGID,
		            IIF(INVENTTRANSFERLINE.QTYSHIPPED = 0,0,INVENTTRANSFERLINE.QTYSHIPPED/INVENTITEMBARCODE.QTY) AS QTY_SHIP,
		            IIF(INVENTTRANSFERLINE.QTYRECEIVED = 0,0,INVENTTRANSFERLINE.QTYRECEIVED/INVENTITEMBARCODE.QTY) AS QTY_RECIVE,
		            InventTransferLine.SPC_Remarks,INVENTTRANSFERLINE.SPC_ITEMBUYERGROUPID AS NUM,DIMENSIONS.DESCRIPTION AS NUMNAME,
		            INVENTITEMBARCODE.QTY,INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID 
            FROM	SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK)
		            INNER JOIN  SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK) ON INVENTTRANSFERLINE.TRANSFERID = INVENTTRANSFERTABLE.TRANSFERID
		            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTTRANSFERLINE.SPC_ITEMBUYERGROUPID  = DIMENSIONS.NUM 
			            AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
		            LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON INVENTTRANSFERLINE.SPC_ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
			            AND INVENTITEMBARCODE.DATAAREAID = 'SPC'
            WHERE	INVENTTRANSFERTABLE.INVENTLOCATIONIDTO LIKE 'MN%'
		            AND CONVERT(VARCHAR,INVENTTRANSFERTABLE.CREATEDDATETIME,23) BETWEEN '{date1}' AND '{date2}'
		            --AND SPC_TRACKINGID NOT LIKE 'MN%'
		            AND SPC_ITEMBARCODE != 'MN0002'
                    --AND IIF((QtyShipped = '0' AND QtyRemainShip = '0'),IIF(INVENTTRANSFERLINE.INVENTTRANSID=INVENTTRANSFERLINE.SPC_TRACKINGID,'0','1'),'1') = '1'         
                    {conDpt} {conBch} {conOpen} {conItemId}
            ORDER BY INVENTTRANSFERTABLE.INVENTLOCATIONIDTO,INVENTTRANSFERLINE.SPC_TRACKINGID,INVENTTRANSFERTABLE.TRANSFERID,CONVERT(VARCHAR,INVENTTRANSFERTABLE.CREATEDDATETIME,23)
            ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //report web Detail
        public static DataTable Report_WebDetail(string pDateA, string pDateB, string bchID)
        {
            return ConnectionClass.SelectSQL_Main($@"
                    SELECT	SHOP_BRANCH.BRANCH_ID AS NUM,SHOP_BRANCH.BRANCH_NAME AS DESCRIPTION,SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLNAME AS SPC_NAME,
		                    HRPPartyJobTableRelationship.DESCRIPTION AS POSSITION,CONVERT(VARCHAR,TRANSDATE,23) AS TRANSDATE,
		                    IIF(ISBYBARCODE=0,'',ITEMBARCODE) AS ITEMBARCODE,
		                    IIF(ISBYBARCODE=0,SPC_ITEMNAME2,SPC_ITEMNAME) AS SPC_ITEMNAME,
		                    IIF(ISBYBARCODE=0,QTY2,QTY) AS QTY,
		                    IIF(ISBYBARCODE=0,UNITID2,UNITID) AS UNITID
                    FROM	SHOP_BRANCH WITH (NOLOCK)
		                    LEFT OUTER JOIN SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.BRANCHID = SHOP_BRANCH.BRANCH_ID
				                    AND CONVERT(VARCHAR,TRANSDATE,23) BETWEEN '{pDateA}' AND '{pDateB}'
                                    AND REFNO2 = 'Shop24HrsWeb' AND ORDERSTATUS != '0'  
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)
				                    ON SHOP_WEB_PDTTRANSORDER.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE AND  DATEADD(HOUR, 7, HRPPARTYPOSITIONTABLERELAT2226.VALIDTODATETIME) >= CAST(GETDATE() AS DATE)
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPartyJobTableRelationship with (nolock)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID  
                    WHERE	SHOP_BRANCH.BRANCH_ID  IN ({bchID})
                    ORDER BY   SHOP_BRANCH.BRANCH_ID,CONVERT(VARCHAR,TRANSDATE,23),SHOP_WEB_PDTTRANSORDER.EMPLID,HRPPartyJobTableRelationship.DESCRIPTION
            ");
        }
        //การส่งอาหารกล่อง
        public static string TranferDelivery_ItemQty(string date, string _pWH, string _pTypeID)
        {
            string strQty = $@"
                    SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,SPC_ITEMBARCODE AS SHOW_ID,SUM(SPC_QTY) AS QTY,'R' AS TYPE

                    FROM	SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK) 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK) ON INVENTTRANSFERTABLE.Transferid = INVENTTRANSFERLINE.Transferid 

                    WHERE	INVENTTRANSFERTABLE.SHIPDATE = '{date}' 
		                    AND INVENTTRANSFERTABLE.DATAAREAID = N'SPC' AND INVENTTRANSFERLINE.DATAAREAID = N'SPC'	 
		                    AND INVENTLOCATIONIDTO LIKE '%'  
		                    AND INVENTTRANSFERTABLE.Transferid LIKE 'MNPI%'
		                    AND INVENTLOCATIONIDFROM = '{_pWH}'

                    GROUP BY INVENTLOCATIONIDTO, CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),23),SPC_ITEMBARCODE

                    UNION

                    SELECT	BRANCH_ID,SHOW_ID AS SPC_ITEMBARCODE,QTY AS QTY,'S' AS TYPE
                    FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK)
                    WHERE	TYPE_CONFIG = '{_pTypeID}'  AND QTY > 0
                            AND CONVERT(VARCHAR,DATE_SEND,23) =  '{date}' ";
            return strQty;
        }
        //การส่งอาหารกล่อง
        public static string TranferDelivery_ItemQtyByBch(string date, string _pWH, string _pTypeID, string bchID)
        {
            string strBch = string.Format(@" SELECT	TMP.BRANCH_ID,BRANCH_NAME FROM ( ");
            strBch += TranferDelivery_ItemQty(date, _pWH, _pTypeID) + " )TMP INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMP.BRANCH_ID = SHOP_BRANCH.BRANCH_ID  ";
            if (bchID != "") strBch += $@" WHERE	TMP.BRANCH_ID = '{bchID}' ";
            strBch += " GROUP BY TMP.BRANCH_ID,BRANCH_NAME ORDER BY BRANCH_ID ,BRANCH_NAME ";
            return strBch;
        }
        //ค้นหาเวลารับล่าสุด
        public static DataTable TranferDelivery_ItemLastRecive(string date, string _pWH)
        {
            string strTime = $@"
                SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,SPC_ITEMBARCODE AS SHOW_ID,
                        MAX(CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),24)) AS MAXTIME_RECIVE

                FROM	SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK) 
		                INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK) ON INVENTTRANSFERTABLE.Transferid = INVENTTRANSFERLINE.Transferid 

                WHERE	INVENTTRANSFERTABLE.SHIPDATE = '{date}' 
		                AND INVENTTRANSFERTABLE.DATAAREAID = N'SPC' AND INVENTTRANSFERLINE.DATAAREAID = N'SPC'
		                AND INVENTLOCATIONIDTO LIKE 'MN%'  
		                AND INVENTTRANSFERTABLE.Transferid LIKE 'MNPI%'
		                AND INVENTLOCATIONIDFROM = '{_pWH}'

                GROUP BY INVENTLOCATIONIDTO,SPC_ITEMBARCODE ";
            return ConnectionClass.SelectSQL_Main(strTime);
        }
        //ค้นหาข้อมูลเพื่อ พิมพ์
        public static DataTable TranferDelivery_ForPrint(string ptypePrint, string _pTypeID, string date, string pBarcode, string pBch)
        {
            string sql;
            if (ptypePrint == "1")
            {
                sql = $@"
                SELECT  SHOW_ID,SPC_ITEMNAME,SHOP_CONFIGBRANCH_QTY.BRANCH_ID,BRANCH_NAME,SHOP_CONFIGBRANCH_QTY.QTY,BILLSEND,UNITID  
                FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK) 
                            INNER JOIN SHOP_BRANCH WITH (NOLOCK)    ON SHOP_CONFIGBRANCH_QTY.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                            INNER JOIN	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_QTY.SHOW_ID = INVENTITEMBARCODE.ITEMBARCODE
                WHERE	TYPE_CONFIG = '{_pTypeID}' 
		                AND CONVERT(VARCHAR,DATE_SEND,23) = '{date}'
                        AND SHOW_ID = '{pBarcode}' 
		                AND SHOP_CONFIGBRANCH_QTY.QTY > 0
                ORDER BY SHOW_ID,SHOP_CONFIGBRANCH_QTY.BRANCH_ID   ";
            }
            else
            {
                sql = $@"
                SELECT  SHOW_ID,SPC_ITEMNAME,SHOP_CONFIGBRANCH_QTY.BRANCH_ID,BRANCH_NAME,SHOP_CONFIGBRANCH_QTY.QTY,BILLSEND,UNITID  
                FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK) 
                            INNER JOIN SHOP_BRANCH WITH (NOLOCK)    ON SHOP_CONFIGBRANCH_QTY.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                            INNER JOIN	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_QTY.SHOW_ID = INVENTITEMBARCODE.ITEMBARCODE
                WHERE	TYPE_CONFIG = '{_pTypeID}' 
		                AND CONVERT(VARCHAR,DATE_SEND,23) = '{date}'
                        AND SHOW_ID = '{pBarcode}' AND SHOP_CONFIGBRANCH_QTY.BRANCH_ID = '{pBch}' 
		                AND SHOP_CONFIGBRANCH_QTY.QTY > 0
                ORDER BY SHOP_CONFIGBRANCH_QTY.BRANCH_ID   ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาการทำบิลจาก ยาไปหน้าร้าน HD
        public static DataTable MNTF_FindRetailHD(string pDate1, string pDate2)
        {
            string sql = string.Format(@"
                SELECT	EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME
                FROM	SHOP2013TMP.dbo.InventTransferJour   with (nolock) 
		                INNER JOIN SHOP2013TMP.dbo.InventTransferJourLine  with (nolock) ON InventTransferJour.TRANSFERID = InventTransferJourLine.TRANSFERID   
		                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON InventTransferJour.UpdatedBy = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
                WHERE	CONVERT (VARCHAR,InventTransferJour.createdDateTime,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"'
		                AND InventTransferJour.DATAAREAID = N'SPC' AND InventTransferJourLine.DATAAREAID = N'SPC' 
		                AND InventLocationIdFrom = 'WH-A' AND InventLocationIdTO = 'RETAILAREA' AND DlvTermId = 'DRUG' 
		                AND UpdateType = '0'  
                GROUP BY EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME  
                ORDER BY EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME  
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาการทำบิลจาก ยาไปหน้าร้าน DT
        public static DataTable MNTF_FindRetailDT(string pDate1, string pDate2)
        {
            string sql = string.Format(@"
                SELECT	CONVERT (VARCHAR,InventTransferJour.createdDateTime,23) AS DateI,EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME,COUNT(SPC_ITEMBARCODE) AS COUNT_LINE 
                FROM	SHOP2013TMP.dbo.InventTransferJour   with (nolock) 
		                INNER JOIN SHOP2013TMP.dbo.InventTransferJourLine  with (nolock) ON InventTransferJour.TRANSFERID = InventTransferJourLine.TRANSFERID   
		                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON InventTransferJour.UpdatedBy = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
                WHERE	CONVERT (VARCHAR,InventTransferJour.createdDateTime,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"'
		                AND InventTransferJour.DATAAREAID = N'SPC' AND InventTransferJourLine.DATAAREAID = N'SPC' 
		                AND InventLocationIdFrom = 'WH-A' AND InventLocationIdTO = 'RETAILAREA' AND DlvTermId = 'DRUG' 
		                AND UpdateType = '0'  
                GROUP BY CONVERT (VARCHAR,InventTransferJour.createdDateTime,23),EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME  
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาข้อมูลการเปิดบิล ไม่อ่างอิงรายการสั่ง
        public static DataTable OrderWeb_NoOrder(string dptID, string bchID)
        {
            string sql6 = $@"
                SELECT	CONVERT(varchar,CREATEDATE,23) AS TRANSDATE,ORDERNUM,INVENTLOCATIONIDTO,INVENTLOCATIONIDTONAME,INVENTLOCATIONIDFROM,SHOP_WEB_PDTTRANSORDER_PICKING.EMPLID,EMPLTABLE.SPC_NAME
                FROM	SHOP_WEB_PDTTRANSORDER_PICKING WITH (NOLOCK)
		                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER_PICKING.EMPLID = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                WHERE	CONVERT(varchar,CREATEDATE,23) BETWEEN GETDATE()-3 AND GETDATE() 
		                AND SHOP_WEB_PDTTRANSORDER_PICKING.DIMENSION = '{dptID}'
                        AND INVENTLOCATIONIDTO = '{bchID}'  
		                AND STASENDAX = '0'
		                AND STATYPEOPEN = '1'
                ORDER BY INVENTLOCATIONIDTO,ORDERNUM DESC  ";
            return ConnectionClass.SelectSQL_Main(sql6);
        }
        //แสดงออเดอร์เมนูจัดสินค้าผ่านรูป My Jeeds
        public static DataTable OrderByWeb_GetDataOrder(string dimCode, string BchID, string orderSta, string date1, string date2, string staImage_Barcode, string STAPACKING_MN) //staImage_Barcode 0 รูป 1 บาร์โค้ด 2 ทั้งหมด
        {
            string str_OrderSta = " AND ORDERSTATUS != '0'  ";
            if (orderSta == "1") str_OrderSta = " AND ORDERSTATUS = '1'  ";

            string str_Dim = "";
            if (dimCode != "") str_Dim = $@" AND TARGETDIMENSION = '{dimCode}' ";

            string str_Bch = "";
            if (BchID != "") str_Bch = $@" AND BRANCHID = '{BchID}' ";

            string strPackMN = "";
            if (STAPACKING_MN == "0") strPackMN = " AND BRANCHID NOT LIKE 'MN%' ";

            string sqlBarcode = $@"
                    SELECT  SHOP_WEB_PDTTRANSORDER.[TRANSID],[TRANSDATE],[EMPLID],BRANCHID,BRANCHNAME,
					            IIF (ISBYBARCODE = '0','',[ITEMBARCODE]) AS [ITEMBARCODE],
					            IIF (ISBYBARCODE = '0',[SPC_ITEMNAME2],[SPC_ITEMNAME]) AS [SPC_ITEMNAME],
					            IIF (ISBYBARCODE = '0',[QTY2],[QTY]) AS [QTY],
					            IIF (ISBYBARCODE = '0',[UNITID2],[UNITID]) AS [UNITID],
					            [SPC_PICKINGLOCATIONID],[TARGETDIMENSION],DESCRIPTION AS NAMEPACKINGALL,ORDERIMAGEPATH,ORDERSTATUS,
					            REFNO2,SHOP_WEB_PDTTRANSORDER.CREATEDDATE,REFMNPO,[EMPLID]+CHAR(10)+[EMPLNAME] AS [EMPLNAMEORDER],
                                [EMPIDPICKING] AS EMPLIDPACKING,[EMPNAMEPICKING] AS EMPLNAMEPACKING,
					            [EMPIDPICKING]+CHAR(10)+[EMPNAMEPICKING] AS [EMPNAMEPICKING]
                     
			            FROM	SHOP_WEB_PDTTRANSORDER WITH(NOLOCK)
					            INNER JOIN SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.TARGETDIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION AND ISACTIVE = '1' AND ISPACKINGALL = '1'							 
                                LEFT OUTER JOIN (SELECT	SHOW_ID,SHOW_DESC	FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) WHERE	TYPE_CONFIG = '61' AND STA = '1')TMP_VENDER ON SHOP_WEB_PDTTRANSORDER.VENDERID = TMP_VENDER.SHOW_ID
			            WHERE   ISBYBARCODE IN ('1') AND ISNULL(TMP_VENDER.SHOW_ID,'') = '' 
                                {str_OrderSta} 
					            AND TRANSDATE BETWEEN @Date1 AND @Date2
					            {str_Dim} {str_Bch} {strPackMN} ";
            string sqlImage = $@"
                        SELECT  [TRANSID],SHOP_WEB_PDTTRANSORDER.[TRANSDATE],[EMPLID],BRANCHID,BRANCHNAME,
					            IIF (ISBYBARCODE = '0','',[ITEMBARCODE]) AS [ITEMBARCODE],
					            IIF (ISBYBARCODE = '0',[SPC_ITEMNAME2],[SPC_ITEMNAME]) AS [SPC_ITEMNAME],
					            IIF (ISBYBARCODE = '0',[QTY2],[QTY]) AS [QTY],
					            IIF (ISBYBARCODE = '0',[UNITID2],[UNITID]) AS [UNITID],
					            [SPC_PICKINGLOCATIONID],[TARGETDIMENSION],DESCRIPTION AS NAMEPACKINGALL,ORDERIMAGEPATH,ORDERSTATUS,
					            REFNO2,SHOP_WEB_PDTTRANSORDER.CREATEDDATE,REFMNPO,[EMPLID]+CHAR(10)+[EMPLNAME] AS [EMPLNAMEORDER],
                                [EMPIDPICKING] AS EMPLIDPACKING,[EMPNAMEPICKING] AS EMPLNAMEPACKING,
					            [EMPIDPICKING]+CHAR(10)+[EMPNAMEPICKING] AS [EMPNAMEPICKING]
                     
			            FROM	SHOP_WEB_PDTTRANSORDER WITH(NOLOCK)
					            INNER JOIN SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.TARGETDIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION AND ISACTIVE = '1' --AND ISPACKINGALL = '1'							 
			            WHERE   ISBYBARCODE IN ('0')  AND ISBARCODEMATCHING = 0 
                                {str_OrderSta}
					            AND TRANSDATE BETWEEN @Date1 AND @Date2
					            {str_Dim} {str_Bch} {strPackMN} ";

            string sql1;
            if (staImage_Barcode == "0") sql1 = sqlImage;
            else if (staImage_Barcode == "1") sql1 = sqlBarcode;
            else sql1 = $@" {sqlBarcode} UNION {sqlImage} ";

            string sql = $@"
            DECLARE @Date1 NVARCHAR(10) = N'{date1}';
            DECLARE @Date2 NVARCHAR(10) = N'{date2}';

            SELECT	'0' AS C,TMP.TRANSID,TMP.TRANSDATE,TMP.EMPLID,BRANCHID,BRANCHNAME,TMP.ITEMBARCODE,TMP.SPC_ITEMNAME,TMP.QTY,TMP.UNITID,
		            TMP.SPC_PICKINGLOCATIONID,TMP.TARGETDIMENSION,NAMEPACKINGALL,ORDERIMAGEPATH,ORDERSTATUS,
		            REFNO2,CREATEDDATE,REFMNPO,TMP.EMPLNAMEORDER,TMP.EMPNAMEPICKING,EMPLIDPACKING,EMPLNAMEPACKING,
		            IIF (ISNULL([BRANCHNAME], '') = '',BRANCH_NAME,[BRANCHNAME]) AS [BRANCHNAME] ,
		            IIF (TMP.ORDERSTATUS = '13',Order_NAME,SHOW_NAME) AS SHOW_NAME,ISNULL(ORDERSTA_PACKING,'0') AS ORDERSTA_PACKING,
                    BCHROUTING.ROUTEID+CHAR(10)+BCHROUTING.ROUTENAME AS ROUTEID,ISNULL(SPC_WMSLocationId,'') AS SPC_WMSLocationId
            FROM	( {sql1} )TMP
		            LEFT OUTER	JOIN SHOP_BRANCH WITH(NOLOCK) ON TMP.BRANCHID = SHOP_BRANCH.BRANCH_ID
		            LEFT OUTER	JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH(NOLOCK) ON TMP.ORDERSTATUS = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND  TYPE_CONFIG = '54' AND STA = '1'
		            LEFT OUTER  JOIN ( 
			            SELECT	[TransId] AS Trans,[OrderStatus] AS ORDERSTA_PACKING,SHOW_NAME AS Order_NAME 
			            FROM	SupcAndroid.dbo.ProductContainer_OrderImageRef  WITH (NOLOCK)
					            INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON 
						            SupcAndroid.dbo.ProductContainer_OrderImageRef.[OrderStatus] = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND  TYPE_CONFIG = '53' AND STA = '1'
				            )STA_PACKING ON TMP.TRANSID =  STA_PACKING.Trans
                    LEFT OUTER JOIN (
				      {LogisticClass.GetRouteAllByBranch()}
		                )BCHROUTING ON SHOP_BRANCH.BRANCH_ID = BCHROUTING.ACCOUNTNUM
                    LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON TMP.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE AND INVENTITEMBARCODE.DATAAREAID = N'SPC'

            ORDER BY BRANCHID,TARGETDIMENSION,CREATEDDATE DESC ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาข้อมูลการเปิดบิล ไม่อ่างอิงรายการสั่ง
        public static DataTable OrderWeb_NoOrder(string dptID)
        {
            string sql6 = $@"
                        SELECT	'0' AS C,BRANCH_ID,BRANCH_NAME,ISNULL(COUNT_BILL,0) AS COUNT_BILL,ROUTEID,ROUTENAME
                        FROM	SHOP_BRANCH WITH (NOLOCK)
		                        LEFT OUTER JOIN 
		                        (
			                        SELECT	INVENTLOCATIONIDTO,COUNT(*) AS COUNT_BILL
			                        FROM	SHOP_WEB_PDTTRANSORDER_PICKING WITH (NOLOCK)
			                        WHERE	CONVERT(varchar,CREATEDATE,23) BETWEEN GETDATE()-3 AND GETDATE()  
					                        AND DIMENSION = '{dptID}' AND STASENDAX = '0' AND STATYPEOPEN = '1'
			                        GROUP BY INVENTLOCATIONIDTO
		                        )SHOP_WEB_PDTTRANSORDER_PICKING ON SHOP_BRANCH.BRANCH_ID = SHOP_WEB_PDTTRANSORDER_PICKING.INVENTLOCATIONIDTO
                                LEFT OUTER JOIN (
				                   {LogisticClass.GetRouteAllByBranch()}
		                            )BCHROUTING ON SHOP_BRANCH.BRANCH_ID = BCHROUTING.ACCOUNTNUM
                        WHERE	 BRANCH_STA IN ('4','1')  AND  BRANCH_STAOPEN = '1'
                        ORDER BY BRANCH_ID
                    ";
            return ConnectionClass.SelectSQL_Main(sql6);
        }
        //รายงานการจัดสินค้าตามพนักงานจัดบิล
        public static DataTable OrderWeb_ReportByWorker(string pType, string dptID, string startDate, string EndDate)//pType 0 คือ เฉพาะพนักงานจัด  1 คือ แยกสาขา 2 แยกพนักงาน
        {
            string sqlSelect;
            string sqlJoinEmp = "";
            string sqlGroup;
            string sqlOrderBy;
            switch (pType)
            {
                case "0":
                    sqlSelect = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId  AS EMPLID,EMPLTABLE.SPC_NAME ";
                    sqlJoinEmp = " LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON ProductContainer_OrderImagePicking.WorkerId = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' ";
                    sqlGroup = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,EMPLTABLE.SPC_NAME ";
                    sqlOrderBy = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,EMPLTABLE.SPC_NAME ";
                    break;
                case "1":
                    sqlSelect = " CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) AS DATEPICKING,ProductContainer_OrderImageRef.DIMENSION,COUNT(ProductContainer_OrderImageRef.BarcodeConfirm) AS COUNT_BARCODE,SUM(ProductContainer_OrderImageRef.QtyConfirm) AS SUM_QTY ";
                    sqlGroup = " ProductContainer_OrderImageRef.DIMENSION,CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) ";
                    sqlOrderBy = " ProductContainer_OrderImageRef.DIMENSION,CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) ";
                    break;
                default://= pType 2
                    sqlSelect = " CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) AS DATEPICKING,ProductContainer_OrderImageRef.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,COUNT(ProductContainer_OrderImageRef.BarcodeConfirm) AS COUNT_BARCODE,SUM(ProductContainer_OrderImageRef.QtyConfirm) AS SUM_QTY ";
                    sqlGroup = " CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23),ProductContainer_OrderImageRef.DIMENSION,ProductContainer_OrderImagePicking.WorkerId ";
                    sqlOrderBy = " ProductContainer_OrderImageRef.DIMENSION,CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) ";
                    break;
            }

            string sqlMain = $@"
                SELECT	{sqlSelect}
                FROM	ProductContainer_OrderImageRef WITH (NOLOCK)
		                INNER JOIN ProductContainer_OrderImagePicking WITH (NOLOCK) ON ProductContainer_OrderImageRef.ContainerId = ProductContainer_OrderImagePicking.ContainerId
			                AND ProductContainer_OrderImageRef.BarcodeConfirm = ProductContainer_OrderImagePicking.BarcodeConfirm
                        {sqlJoinEmp}
                WHERE	CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) BETWEEN '{startDate}' AND '{EndDate}'
		                AND ProductContainer_OrderImagePicking.DIMENSION = '{dptID}'
		                AND ProductContainer_OrderImageRef.STATUSSend = '1'
		                AND ProductContainer_OrderImageRef.OrderStatus IN ('1','3','4') 
                GROUP BY {sqlGroup}
                ORDER BY {sqlOrderBy}
            ";
            return ConnectionClass.SelectSQL_SentServer(sqlMain, IpServerConnectClass.ConSupcAndroid);
        }
        //9 = รายงานประเมินการจัดบิล ตามพนักงานจัดบิล
        public static DataTable OrderWeb_ReportPerformance(string pType, string startDate, string EndDate)
        {
            string sqlSelect;
            string sqlJoinEmp = "";
            string sqlWhere = "";
            string sqlGroup;
            string sqlOrderBy;
            switch (pType)
            {
                case "0"://จัดได้
                    sqlSelect = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,COUNT(ProductContainer_OrderImageRef.BarcodeConfirm) AS COUNT_BARCODE   ";
                    sqlWhere = $@" AND ProductContainer_OrderImageRef.OrderStatus IN ('1','3','4') ";
                    sqlGroup = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId  ";
                    sqlOrderBy = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId ";
                    break;
                case "1"://จัดไม่ได้
                    sqlSelect = "  ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,COUNT(ProductContainer_OrderImageRef.BarcodeConfirm) AS COUNT_BARCODE   ";
                    sqlWhere = $@" AND ProductContainer_OrderImageRef.OrderStatus NOT IN ('1','3','4') ";
                    sqlGroup = "   ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId  ";
                    sqlOrderBy = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId ";
                    break;
                default:// รวมทั้งหมด
                    sqlSelect = " ProductContainer_OrderImagePicking.DIMENSION,SHOP_WEB_CONFIGTARGETDIMS.DESCRIPTION,ProductContainer_OrderImagePicking.WorkerId,EMPLTABLE.SPC_NAME  ";
                    sqlJoinEmp = $@" LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON ProductContainer_OrderImagePicking.WorkerId = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
                                    INNER JOIN SHOP24HRS.dbo.SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON ProductContainer_OrderImagePicking.DIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION AND ISACTIVE = '1' ";
                    sqlGroup = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,EMPLTABLE.SPC_NAME,SHOP_WEB_CONFIGTARGETDIMS.DESCRIPTION ";
                    sqlOrderBy = "ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId  ";
                    break;
            }

            string sqlMain = $@"
                SELECT	{sqlSelect}
                FROM	ProductContainer_OrderImageRef WITH (NOLOCK)
		                INNER JOIN ProductContainer_OrderImagePicking WITH (NOLOCK) ON ProductContainer_OrderImageRef.ContainerId = ProductContainer_OrderImagePicking.ContainerId
			                AND ProductContainer_OrderImageRef.BarcodeConfirm = ProductContainer_OrderImagePicking.BarcodeConfirm
                        {sqlJoinEmp}
                WHERE	CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) BETWEEN '{startDate}' AND '{EndDate}'
		                AND ProductContainer_OrderImageRef.STATUSSend = '1'
		                {sqlWhere} 
                GROUP BY {sqlGroup}
                ORDER BY {sqlOrderBy}
            ";

            return ConnectionClass.SelectSQL_SentServer(sqlMain, IpServerConnectClass.ConSupcAndroid);
        }
        //10 = รายงานประเมินการจัดบิลไม่ได้ ตามพนักงานจัดบิล
        public static DataTable OrderWeb_ReportPerformanceMonth(string pType, string year)//pType 0 คือ รวมรายชื่อ
        {
            string sqlSelect;
            string sqlJoinEmp = "";
            string sqlWhere = "";
            string sqlGroup;
            string sqlOrderBy;
            switch (pType)
            {
                case "1"://จัดไม่ได้
                    sqlSelect = "  ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,MONTH(CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23)) AS MONTH,COUNT(ProductContainer_OrderImageRef.BarcodeConfirm) AS COUNT_BARCODE   ";
                    sqlWhere = $@" AND ProductContainer_OrderImageRef.OrderStatus NOT IN ('1','3','4') ";
                    sqlGroup = "   ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,MONTH(CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23))  ";
                    sqlOrderBy = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId ";
                    break;
                case "2"://ทั้งหมด
                    sqlSelect = "  ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,MONTH(CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23)) AS MONTH,COUNT(ProductContainer_OrderImageRef.BarcodeConfirm) AS COUNT_BARCODE   ";
                    sqlWhere = $@"  ";
                    sqlGroup = "   ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,MONTH(CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23))  ";
                    sqlOrderBy = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId ";
                    break;
                default:// รวมทั้งหมด
                    sqlSelect = " ProductContainer_OrderImagePicking.DIMENSION,SHOP_WEB_CONFIGTARGETDIMS.DESCRIPTION,ProductContainer_OrderImagePicking.WorkerId,EMPLTABLE.SPC_NAME   ";
                    sqlJoinEmp = $@" LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON ProductContainer_OrderImagePicking.WorkerId = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
                                    INNER JOIN SHOP24HRS.dbo.SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON ProductContainer_OrderImagePicking.DIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION AND ISACTIVE = '1' ";
                    sqlGroup = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId,EMPLTABLE.SPC_NAME,SHOP_WEB_CONFIGTARGETDIMS.DESCRIPTION ";
                    sqlOrderBy = " ProductContainer_OrderImagePicking.DIMENSION,ProductContainer_OrderImagePicking.WorkerId  ";
                    break;
            }

            string sqlMain = $@"
                SELECT	{sqlSelect}
                FROM	ProductContainer_OrderImageRef WITH (NOLOCK)
		                INNER JOIN ProductContainer_OrderImagePicking WITH (NOLOCK) ON ProductContainer_OrderImageRef.ContainerId = ProductContainer_OrderImagePicking.ContainerId
			                AND ProductContainer_OrderImageRef.BarcodeConfirm = ProductContainer_OrderImagePicking.BarcodeConfirm
                        {sqlJoinEmp}
                WHERE	YEAR(CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23)) = '{year}'
		                AND ProductContainer_OrderImageRef.STATUSSend = '1'
		                {sqlWhere} 
                GROUP BY {sqlGroup}
                ORDER BY {sqlOrderBy}
            ";

            return ConnectionClass.SelectSQL_SentServer(sqlMain, IpServerConnectClass.ConSupcAndroid);
        }
        //รายการจัดสินค้าที่ไม่สามารถจัดได้(หมด)
        public static DataTable OrderWeb_ReportItemPacking(string pCase, string dptID, string startDate, string EndDate, string bchID, string itemID, string inventDim)//pCase 0 outOfStock 1 Normal
        {
            string conDpt = "";
            if (dptID != "") conDpt = $@" AND ProductContainer_OrderImageRef.Dimension  = '{dptID}' ";

            string conItemId = "";
            if (itemID != "") conItemId = $@" AND INVENTITEMBARCODE.[ITEMID] = '{itemID}' AND INVENTITEMBARCODE.[INVENTDIMID] = '{inventDim}' ";

            string conBch = "";
            //if (SystemClass.SystemBranchID != "MN000") conBch = $@" AND TMP_BILL.INVENTLOCATIONIDTO = '{SystemClass.SystemBranchID}' ";
            if (bchID != "") conBch = $@" AND TMP_BILL.INVENTLOCATIONIDTO = '{bchID}' ";
            string conTo = "";
            if (SystemClass.SystemBranchID != "MN000") conTo = $@" AND TMP_BILL.INVENTLOCATIONIDTO LIKE 'MN%' ";

            string dateOld = (DateTime.Parse(startDate)).AddDays(-31).ToString("yyyy-MM-dd");

            string selectData = $@" IIF (ISBYBARCODE = '0','',SHOP_WEB_PDTTRANSORDER.[ITEMBARCODE]) AS [ITEMBARCODE],
		            IIF (ISBYBARCODE = '0',[SPC_ITEMNAME2],SHOP_WEB_PDTTRANSORDER.[SPC_ITEMNAME]) AS [SPC_ITEMNAME],
		            IIF (ISBYBARCODE = '0',[QTY2],SHOP_WEB_PDTTRANSORDER.[QTY]) AS [QTY],
		            IIF (ISBYBARCODE = '0',[UNITID2],SHOP_WEB_PDTTRANSORDER.[UNITID]) AS [UNITID],'0' AS STACOPY  ";
            string conSelect = " AND ProductContainer_OrderImageRef.OrderStatus NOT  IN ('1','3','4')  ";
            if (pCase == "1")
            {
                selectData = $@" ProductContainer_OrderImageRef.BarcodeConfirm AS [ITEMBARCODE],
                    ProductContainer_OrderImageRef.SpcItemName AS [SPC_ITEMNAME],
                    ProductContainer_OrderImageRef.QtyConfirm AS [QTY],
                    ProductContainer_OrderImageRef.UnitId AS [UNITID],'1' AS STACOPY  ";
                conSelect = " AND ProductContainer_OrderImageRef.OrderStatus IN ('1','3','4')  ";
            }

            string sql = $@"
            SELECT	CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) AS DATEPACKING,
		            ProductContainer_OrderImageRef.ORDERNUM,
                    --ProductContainer_OrderImageRef.DIMENSION AS TARGETDIMENSION,DESCRIPTION AS NAMEPACKINGALL,
                    ISNULL(SHOP_WEB_PDTTRANSORDER_PICKING.DIMENSION,ProductContainer_OrderImageRef.DIMENSION) AS TARGETDIMENSION,
                    ISNULL(SHOP_WEB_CONFIGTARGETDIMS.DESCRIPTION,TMPDPT.DESCRIPTION) AS NAMEPACKINGALL,
		            TMP_BILL.INVENTLOCATIONIDFROM,TMP_BILL.INVENTLOCATIONIDTO,TMP_BILL.INVENTLOCATIONIDTONAME,
		            WorkerId AS EMPIDPICKING,WorkerName AS EMPNAMEPICKING,{selectData},
                    IIF(ISNULL(REFNO2,'') != '',REFNO2,IIF(ProductContainer_OrderImageRef.OrderStatus='3','สินค้าทดแทน/เพิ่มจากสั่ง','สินค้าไม่ได้สั่ง')) AS REFNO2,
                    ISNULL(SHOP_WEB_PDTTRANSORDER.ORDERIMAGEPATH,'\\192.168.100.77\ImageMinimark\emply.png') AS ORDERIMAGEPATH,SHOW_NAME,
                    CONVERT(VARCHAR,ISNULL(SHOP_WEB_PDTTRANSORDER.TRANSDATE,''),23) AS ORDER_TRANSDATE,
		            ISNULL(SHOP_WEB_PDTTRANSORDER.EMPLID,'') AS ORDER_EMPLID,
		            ISNULL(SHOP_WEB_PDTTRANSORDER.EMPLNAME,'') AS ORDER_EMPLNAME,
                    ISNULL(IIF(SHOP_WEB_PDTTRANSORDER.ISBYBARCODE=1,SHOP_WEB_PDTTRANSORDER.ITEMBARCODE,''),'') AS ORDER_BARCODE,
		            ISNULL(IIF(SHOP_WEB_PDTTRANSORDER.ISBYBARCODE=1,SHOP_WEB_PDTTRANSORDER.SPC_ITEMNAME,SHOP_WEB_PDTTRANSORDER.SPC_ITEMNAME2),'') AS ORDER_SPC_ITEMNAME,
		            ISNULL(IIF(SHOP_WEB_PDTTRANSORDER.ISBYBARCODE=1,SHOP_WEB_PDTTRANSORDER.QTY,SHOP_WEB_PDTTRANSORDER.QTY2),0) AS ORDER_QTY,
		            ISNULL(IIF(SHOP_WEB_PDTTRANSORDER.ISBYBARCODE=1,SHOP_WEB_PDTTRANSORDER.UNITID,SHOP_WEB_PDTTRANSORDER.UNITID2),'') AS ORDER_UNITID,
		            --IIF(ProductContainer_OrderImageRef.OrderStatus='1',DATEDIFF(DD,SHOP_WEB_PDTTRANSORDER.TRANSDATE,ProductContainer_OrderImageRef.CreateDateTime),0) AS DIFFDATE
                    DATEDIFF(DD,SHOP_WEB_PDTTRANSORDER.TRANSDATE,ProductContainer_OrderImageRef.CreateDateTime) AS DIFFDATE,
                    INVENTTABLE.PrimaryVendorId AS VENDERID,ProductContainer_OrderImageRef.TRANSID ,INVENTITEMBARCODE.[ITEMID],INVENTITEMBARCODE.[INVENTDIMID],INVENTITEMBARCODE.QTY AS FACTOR
		
            FROM	ProductContainer_OrderImageRef WITH (NOLOCK)
		            LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_WEB_PDTTRANSORDER_PICKING WITH (NOLOCK) ON ProductContainer_OrderImageRef.TransId  = SHOP_WEB_PDTTRANSORDER_PICKING.TRANSID
		            LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER_PICKING.TransId  = SHOP_WEB_PDTTRANSORDER.TRANSID
		            LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER_PICKING.DIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION 
                    LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_WEB_CONFIGTARGETDIMS TMPDPT WITH (NOLOCK) ON ProductContainer_OrderImageRef.DIMENSION = TMPDPT.DIMENSION 
		            LEFT OUTER JOIN (SELECT * FROM	SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) WHERE TYPE_CONFIG = '53')TYPESTA ON ProductContainer_OrderImageRef.OrderStatus = TYPESTA.SHOW_ID
                    LEFT OUTER JOIN (
			            SELECT	ORDERNUM,INVENTLOCATIONIDFROM,INVENTLOCATIONIDTO,INVENTLOCATIONIDTONAME
			            FROM	SHOP24HRS.dbo.SHOP_WEB_PDTTRANSORDER_PICKING WITH (NOLOCK) 
			            WHERE	CONVERT(VARCHAR,CREATEDATE,23) BETWEEN '{dateOld}' AND '{EndDate}'
			            GROUP BY ORDERNUM,INVENTLOCATIONIDFROM,INVENTLOCATIONIDTO,INVENTLOCATIONIDTONAME
		            )TMP_BILL ON ProductContainer_OrderImageRef.OrderNum = TMP_BILL.OrderNum
                    LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)  ON ProductContainer_OrderImageRef.BarcodeConfirm = INVENTITEMBARCODE.ITEMBARCODE
					LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTITEMBARCODE.ITEMID = INVENTTABLE.ITEMID
		
            WHERE	CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) BETWEEN '{startDate}' AND '{EndDate}'
		            {conDpt}
		            {conSelect}
                    {conBch} {conTo} {conItemId}

            ORDER BY ProductContainer_OrderImageRef.DIMENSION,CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23),EMPIDPICKING ";

            //string sql = $@"
            //SELECT	CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) AS DATEPACKING,
            //  SHOP_WEB_PDTTRANSORDER_PICKING.ORDERNUM,SHOP_WEB_PDTTRANSORDER_PICKING.DIMENSION AS TARGETDIMENSION,DESCRIPTION AS NAMEPACKINGALL,
            //  INVENTLOCATIONIDFROM,INVENTLOCATIONIDTO,INVENTLOCATIONIDTONAME,
            //  EMPIDPICKING,EMPNAMEPICKING,{selectData},REFNO2,ORDERIMAGEPATH,SHOW_NAME

            //FROM	ProductContainer_OrderImageRef WITH (NOLOCK)
            //  INNER JOIN SHOP24HRS.dbo.SHOP_WEB_PDTTRANSORDER_PICKING WITH (NOLOCK) ON ProductContainer_OrderImageRef.TransId  = SHOP_WEB_PDTTRANSORDER_PICKING.TRANSID
            //  INNER JOIN SHOP24HRS.dbo.SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER_PICKING.TransId  = SHOP_WEB_PDTTRANSORDER.TRANSID
            //  INNER JOIN SHOP24HRS.dbo.SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER_PICKING.DIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION 
            //  INNER JOIN (SELECT * FROM	SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) WHERE TYPE_CONFIG = '53')TYPESTA ON ProductContainer_OrderImageRef.OrderStatus = TYPESTA.SHOW_ID

            //WHERE	CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23) BETWEEN '{startDate}' AND '{EndDate}'
            //  {conDpt}
            //  {conSelect}
            //        {conBch}

            //ORDER BY SHOP_WEB_PDTTRANSORDER_PICKING.DIMENSION,CONVERT(VARCHAR,ProductContainer_OrderImageRef.CreateDateTime,23),EMPIDPICKING ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid);
        }
        //13 = รายงานรายเดือนตามประเภทการสั่ง ตามบุคคล
        public static DataTable OrderWeb_ReportByTypePO(string pCase, string typePO, string year, string date1, string date2)
        {
            string sql = "";
            switch (pCase)
            {
                case "0":
                    sql = $@"
                        SELECT	REFNO2 AS NAME_BRANCH,REFNO2 AS BRANCH_ID	FROM	SHOP_WEB_PDTTRANSORDER WITH (NOLOCK)
                        WHERE   REFNO2 != ''  AND  CONVERT(VARCHAR,TRANSDATE,23) BETWEEN CONVERT(VARCHAR,GETDATE()-30,23) AND CONVERT(VARCHAR,GETDATE(),23)
                        GROUP BY REFNO2
                        ORDER BY REFNO2 ";
                    break;
                case "1":
                    sql = $@"
                        SELECT	SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME AS EMPLNAME,DIMENSIONS.NUM,IIF(ISNULL(SHOP_BRANCH.BRANCH_ID,'')='',DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME)  AS NUM_DESC
                        FROM	SHOP_WEB_PDTTRANSORDER WITH (NOLOCK)
                                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE  WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
                                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC' 
                                LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID 
                        WHERE	REFNO2 = '{typePO}'	AND  YEAR(TRANSDATE) = '{year}'
                        GROUP BY SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME,DIMENSIONS.NUM,IIF(ISNULL(SHOP_BRANCH.BRANCH_ID,'')='',DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME) 
                        ORDER BY SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME,DIMENSIONS.NUM,IIF(ISNULL(SHOP_BRANCH.BRANCH_ID,'')='',DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME)  ";
                    break;
                case "2":
                    sql = $@"
                        SELECT	MONTH(TRANSDATE) AS MONTH,EMPLID,
		                        COUNT(EMPLID) AS C_ALL,SUM(IIF(ISNULL(ProductContainer_OrderImageRef.QtyConfirm,0)=0,0,1)) AS C_OK,
		                        SUM(ISNULL(ProductContainer_OrderImageRef.QtyConfirm,0)*ISNULL(INVENTITEMBARCODE.SPC_PRICEGROUP3,0)) AS SUMALL
                        FROM	SHOP_WEB_PDTTRANSORDER WITH (NOLOCK)
		                        LEFT OUTER JOIN SupcAndroid.dbo.ProductContainer_OrderImageRef WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.TRANSID = ProductContainer_OrderImageRef.TransId
		                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ProductContainer_OrderImageRef.BarcodeConfirm = INVENTITEMBARCODE.ITEMBARCODE
                        WHERE	REFNO2 = '{typePO}'
		                        AND YEAR(TRANSDATE) = '{year}'
                        GROUP BY MONTH(TRANSDATE),EMPLID,EMPLNAME
                        ORDER BY MONTH(TRANSDATE), EMPLID,EMPLNAME ";
                    break;
                case "3":
                    sql = $@"
                        SELECT	SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME AS EMPLNAME,DIMENSIONS.NUM,IIF(ISNULL(SHOP_BRANCH.BRANCH_ID,'')='',DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME)  AS NUM_DESC
                        FROM	SHOP_WEB_PDTTRANSORDER WITH (NOLOCK)
                                INNER JOIN SHOP2013TMP.dbo.EMPLTABLE  WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
                                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC' 
                                LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID 
                        WHERE	REFNO2 = 'Vansale'	AND  CONVERT(VARCHAR,TRANSDATE,23) BETWEEN '{date1}' AND '{date2}'
                        GROUP BY SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME,DIMENSIONS.NUM,IIF(ISNULL(SHOP_BRANCH.BRANCH_ID,'')='',DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME) 
                        ORDER BY SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME,DIMENSIONS.NUM,IIF(ISNULL(SHOP_BRANCH.BRANCH_ID,'')='',DIMENSIONS.DESCRIPTION,SHOP_BRANCH.BRANCH_NAME) 
                    ";
                    break;
                case "4":
                    sql = $@"
                        SELECT	CONVERT(VARCHAR,TRANSDATE,23) AS MONTH,EMPLID,
		                        COUNT(EMPLID) AS C_ALL,SUM(IIF(ISNULL(ProductContainer_OrderImageRef.QtyConfirm,0)=0,0,1)) AS C_OK,
		                        SUM(ISNULL(ProductContainer_OrderImageRef.QtyConfirm,0)*ISNULL(INVENTITEMBARCODE.SPC_PRICEGROUP3,0)) AS SUMALL
                        FROM	SHOP_WEB_PDTTRANSORDER WITH (NOLOCK)
		                        LEFT OUTER JOIN SupcAndroid.dbo.ProductContainer_OrderImageRef WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.TRANSID = ProductContainer_OrderImageRef.TransId
		                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ProductContainer_OrderImageRef.BarcodeConfirm = INVENTITEMBARCODE.ITEMBARCODE
                        WHERE	REFNO2 = '{typePO}'
		                        AND  CONVERT(VARCHAR,TRANSDATE,23) BETWEEN '{date1}' AND '{date2}'
                        GROUP BY CONVERT(VARCHAR,TRANSDATE,23),EMPLID,EMPLNAME
                        ORDER BY CONVERT(VARCHAR,TRANSDATE,23), EMPLID,EMPLNAME ";
                    break;
                default:
                    break;
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //
        public static string MNEC_CheckMNPT()
        {
            string sql = $@"
            SELECT	MAX(TRANSFERID) AS 	TRANSFERID
            FROM	SHOP2013TMP.dbo.InventTransferTable WITH (NOLOCK)
            WHERE	SPC_Remarks = 'สำหรับแลกเหรียญ' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0) return ""; else return dt.Rows[0]["TRANSFERID"].ToString();

        }
        //รายงานรอทำบิลจัดสินค้าในระบบ Container
        public static DataTable ReportGetDataContainerNotApv()
        {
            string sql = $@"
                SELECT	WHSWorkTransClosed_SPC.OrderNum,InventTransferTable.INVENTLOCATIONIDFROM,InventLocationIdTo,InventLocation.NAME,
		                WHSWorkTransClosed_SPC.ContainerId,WHSWorkTransClosed_SPC.BarcodeConfirm,INVENTITEMBARCODE.SPC_ITEMNAME,WHSWorkTransClosed_SPC.QtyConfirm,INVENTITEMBARCODE.UNITID,
		                EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME,DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,
		                REPLACE(CONVERT(VARCHAR,DATEADD(HOUR,7, WHSWorkTransClosed_SPC.CreatedDateTime),25),'.000','')  AS CreatedDateTime,
		                InventTransferTable.SPC_Remarks

                FROM	WHSWorkTransClosed_SPC WITH (NOLOCK)
		                INNER JOIN InventTransferTable WITH (NOLOCK) ON WHSWorkTransClosed_SPC.OrderNum = InventTransferTable.TransferId
		                INNER JOIN InventLocation  WITH (NOLOCK) ON InventTransferTable.InventLocationIdTo = InventLocation.INVENTLOCATIONID
		                INNER JOIN EMPLTABLE WITH (NOLOCK) ON WHSWorkTransClosed_SPC.WorkerId = EMPLTABLE.EMPLID
		                INNER JOIN INVENTITEMBARCODE WITH (NOLOCK) ON WHSWorkTransClosed_SPC.BarcodeConfirm = INVENTITEMBARCODE.ITEMBARCODE
		                INNER JOIN DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DIMENSIONCODE = '0'

                WHERE	InventTransferTable.DATAAREAID = 'SPC' 
		                AND WHSWorkTransClosed_SPC.DATAAREAID = 'SPC' AND InventLocation.DATAAREAID = 'SPC'
		                AND EMPLTABLE.DATAAREAID = 'SPC' AND DIMENSIONS.DATAAREAID = 'SPC'

                ORDER BY WHSWorkTransClosed_SPC.CreatedDateTime ,WHSWorkTransClosed_SPC.ContainerId ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        //16 = รายงานการจัดสินค้าสาขาใหญ่
        public static DataTable ReportMN_OrderSortProductSupc(string date1)
        {
            string conBch = "";
            if (SystemClass.SystemBranchID != "MN000") conBch = $@" AND MNPOBranch = '{SystemClass.SystemBranchID}' ";
            
            string sql = $@"
                SELECT	InventTransferLine.TRANSFERID,INVENTTRANSFERJOUR.INVENTLOCATIONIDFROM AS INVENTLOCATIONIDFROM,INVENTTRANSFERJOUR.INVENTLOCATIONIDTO AS INVENTLOCATIONIDTO,BRANCH_NAME AS SPC_LOCATIONNAMETO,
		                CONVERT(VARCHAR,InventTransferLine.createdDateTime,23) AS DATEINS,CONVERT(VARCHAR,SHIPDATE,23) AS SHIPDATE,CONVERT(VARCHAR,RECEIVEDATE,23) AS RECEIVEDATE,INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,
		                SPC_ITEMBARCODE,InventTransferLine.SPC_ITEMNAME,SPC_QTY,SPC_ITEMBARCODEUNIT,
                        IIF(ISNULL(QTY_SCAN,0)>0,ISNULL(QTY_SCAN,0)/QTY,0) AS QTY_SCAN,QTYTRANSFER,QTYSHIPPED/QTY AS QTYSHIPPED,QTYRECEIVED/QTY AS QTYRECEIVED 
		
                FROM	SHOP2013TMP.dbo.InventTransferLine WITH (NOLOCK)
		                INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERJOUR WITH (NOLOCK) ON InventTransferLine.TRANSFERID = INVENTTRANSFERJOUR.TRANSFERID AND INVENTTRANSFERJOUR.DATAAREAID = N'SPC'
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON InventTransferLine.SPC_ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
			                AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                LEFT OUTER JOIN (
			                SELECT	MNPODocNo,MNPOitemid AS ITEMID,MNPODimid AS INVENTDIMID,SUM(MNPOQtyOrder*MNPOFactor) AS QTY_SCAN
			                FROM	SHOP_MNPO_DT WITH (NOLOCK)
			                WHERE	MNPODocNo IN (SELECT	MNPODocNo FROM	SHOP_MNPO_HD WITH (NOLOCK)   WHERE	ISNULL(MNPODlvTerm,'0') = '0' AND MNPODate = '{date1}' AND MNPODocNo LIKE 'MNPF%' {conBch})
			                GROUP BY MNPODocNo,MNPOitemid,MNPODimid
		                )TMP ON INVENTITEMBARCODE.ITEMID = TMP.ITEMID  AND INVENTITEMBARCODE.INVENTDIMID = TMP.INVENTDIMID
                                AND InventTransferLine.TRANSFERID = TMP.MNPODocNo
                        INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON INVENTTRANSFERJOUR.INVENTLOCATIONIDTO = SHOP_BRANCH.BRANCH_ID 
				
                WHERE	InventTransferLine.TRANSFERID  IN (SELECT	MNPODocNo FROM	SHOP_MNPO_HD WITH (NOLOCK)   WHERE	ISNULL(MNPODlvTerm,'0') = '0' AND MNPODate = '{date1}' AND MNPODocNo LIKE 'MNPF%'  {conBch} )
		                AND InventTransferLine.DATAAREAID = N'SPC'
		                AND SPC_ITEMBARCODE != 'MN0002'

                UNION

                SELECT	MNPODocNo AS TRANSFERID,MNPOINVENT AS INVENTLOCATIONIDFROM,MNPOBranch AS INVENTLOCATIONIDTO,MNPOBranchName AS SPC_LOCATIONNAMETO,
		                CONVERT(VARCHAR,DATEINS,23) AS DATEINS,'' AS SHIPDATE,'' AS RECEIVEDATE,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID,
		                INVENTITEMBARCODE_MINMAX.ITEMBARCODE AS SPC_ITEMBARCODE,INVENTITEMBARCODE_MINMAX.SPC_ITEMNAME AS SPC_ITEMNAME,
		                '0' AS SPC_QTY,INVENTITEMBARCODE_MINMAX.UNITID AS SPC_ITEMBARCODEUNIT,ISNULL(QTY_SCAN,0) AS QTY_SCAN,'0' AS QTYTRANSFER,'0' AS QTYSHIPPED,'0' AS QTYRECEIVED
                FROM	(
			                SELECT	SHOP_MNPO_HD.MNPODocNo,MNPOINVENT,MNPOBranch,MNPOBranchName,CONVERT(VARCHAR,SHOP_MNPO_HD.MNPODateIn,23) AS DATEINS,MNPOitemid AS ITEMID,MNPODimid AS INVENTDIMID,SUM(MNPOQtyOrder*MNPOFactor) AS QTY_SCAN
			                FROM	SHOP_MNPO_DT WITH (NOLOCK)
					                INNER JOIN SHOP_MNPO_HD WITH (NOLOCK) ON SHOP_MNPO_DT.MNPODocNo = SHOP_MNPO_HD.MNPODocNo
			                WHERE	SHOP_MNPO_HD.MNPODocNo  IN (SELECT	MNPODocNo FROM	SHOP_MNPO_HD WITH (NOLOCK)   WHERE	ISNULL(MNPODlvTerm,'0') = '0' AND MNPODate = '{date1}' AND MNPODocNo LIKE 'MNPF%'  {conBch})
			                GROUP BY SHOP_MNPO_HD.MNPODocNo,MNPOINVENT,MNPOBranch,MNPOBranchName,CONVERT(VARCHAR,SHOP_MNPO_HD.MNPODateIn,23),MNPOitemid,MNPODimid
		                )TMP
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) ON TMP.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID 
			                AND TMP.INVENTDIMID = INVENTITEMBARCODE_MINMAX.INVENTDIMID AND INVENTITEMBARCODE_MINMAX.DATAAREAID = N'SPC'
			                AND INVENTITEMBARCODE_MINMAX.TYPE_ = 'MIN'
		                LEFT OUTER JOIN
		                (	SELECT	InventTransferLine.TRANSFERID,INVENTITEMBARCODE.ITEMID,INVENTITEMBARCODE.INVENTDIMID,QTYTRANSFER
			                FROM	SHOP2013TMP.dbo.InventTransferLine WITH (NOLOCK)
					                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON InventTransferLine.SPC_ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE 
						                AND INVENTITEMBARCODE.DATAAREAID = N'SPC' 
			                WHERE	TRANSFERID  IN (SELECT	MNPODocNo FROM	SHOP_MNPO_HD WITH (NOLOCK)   WHERE	ISNULL(MNPODlvTerm,'0') = '0' AND MNPODate = '{date1}' AND MNPODocNo LIKE 'MNPF%'  {conBch} )
					                AND InventTransferLine.DATAAREAID = N'SPC'AND SPC_ITEMBARCODE != 'MN0002'
		                )TMP2 ON INVENTITEMBARCODE_MINMAX.ITEMID =  TMP2.ITEMID  AND INVENTITEMBARCODE_MINMAX.INVENTDIMID =  TMP2.INVENTDIMID
                            AND TMP.MNPODocNo = TMP2.TRANSFERID 
                WHERE	ISNULL(QTYTRANSFER,0) = 0
		
                ORDER BY INVENTLOCATIONIDTO,INVENTLOCATIONIDFROM

            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

    }
}
