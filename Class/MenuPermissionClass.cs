﻿using System.Data;

namespace PC_Shop24Hrs.Class
{
    class MenuPermissionClass
    {
        //GetMenu
        public static DataTable GetMenu(string _dept, string _Type)
        {
            if (_Type == "MN000") _Type = "SUPC"; else _Type = "SHOP";

            string pPermissAddOn = "";//สำหรับจั้ม
            if (Controllers.SystemClass.SystemUserID == "1406093")
            {
                pPermissAddOn = $@" 
                    UNION 
                    SELECT  MENU_ID ,MENU_NAME  
                    FROM    SHOP_MENU  WITH (NOLOCK) 
                    WHERE   MENU_TYPE = 'SUPC' AND MENU_ID in ('D9BS')
                    GROUP BY MENU_ID,MENU_NAME ";
            }
            if (Controllers.SystemClass.SystemUserID == "1012148")
            {
                pPermissAddOn = $@" 
                    UNION 
                    SELECT  MENU_ID ,MENU_NAME  
                    FROM    SHOP_MENU  WITH (NOLOCK) 
                    WHERE   MENU_TYPE = 'SUPC' AND MENU_ID in ('D000')
                    GROUP BY MENU_ID,MENU_NAME ";
            }

            string sql;
            if ((Controllers.SystemClass.SystemComMinimart != "1") && (_Type == "SUPC"))
            {
                sql = $@"SELECT SHOP_MENU_PERRMISSION.MENU_ID ,MENU_NAME  
                        FROM    SHOP_MENU_PERRMISSION   WITH (NOLOCK) 
                                INNER JOIN SHOP_MENU  WITH (NOLOCK)  ON SHOP_MENU.MENU_ID = SHOP_MENU_PERRMISSION.MENU_ID
                        WHERE   MENU_STATUS = '1' 
                                AND MENU_TYPE = '{_Type}'   AND DEPT_ID = '{_dept}'
                        UNION 
                        SELECT  MENU_ID ,MENU_NAME  
                        FROM    SHOP_MENU  WITH (NOLOCK) 
                        WHERE   MENU_TYPE = '{_Type}'  AND MENU_ID in ('D999') 
                        GROUP BY MENU_ID ,MENU_NAME
                        { pPermissAddOn }
                        ORDER BY MENU_ID ";
            }
            else
            {
                sql = $@"SELECT MENU_ID ,MENU_NAME  
                        FROM    SHOP_MENU  WITH (NOLOCK) 
                        WHERE   MENU_STATUS = '1'
                                AND MENU_TYPE = '{_Type}' AND MENU_STATUS = '1' 
                        ORDER BY MENU_ID";
            }

            return Controllers.ConnectionClass.SelectSQL_Main(sql);
        }
        //GetSeq
        public static string GetSeq(string _Type)
        {
            string sql = $@"SELECT  COUNT(MENU_SEQ)+1 AS MENU_SEQ 
                            FROM    SHOP_MENU  WITH (NOLOCK)
                            WHERE   MENU_TYPE = '{_Type}' ";
            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            string seq = string.Empty;
            if (dt.Rows.Count > 0) seq = dt.Rows[0]["MENU_SEQ"].ToString();
            return seq;
        }

        public static DataTable GetAllMenu(string _STATUS, string _TYPE)
        {
            string sql = @"SELECT MENU_SEQ, MENU_ID, MENU_NAME, MENU_DESC, MENU_TYPE, MENU_STATUS, MENU_REMARK 
                            FROM    SHOP_MENU WITH (NOLOCK) ";

            if (_TYPE != "SUPCSHOP" && _TYPE != "" && _STATUS != "")
            {
                sql += @" WHERE  MENU_STATUS='" + _STATUS + @"' and MENU_TYPE='" + _TYPE + @"' ";
            }
            else if (_TYPE != "SUPCSHOP" && _TYPE == "" && _STATUS != "")
            {
                sql += @" WHERE  MENU_STATUS='" + _STATUS + @"' ";
            }
            else if (_TYPE != "SUPCSHOP" && _TYPE != "" && _STATUS == "")
            {
                sql += @" WHERE  _TYPE='" + _TYPE + @"' ";
            }

            sql += @"ORDER BY MENU_TYPE,MENU_SEQ";


            return Controllers.ConnectionClass.SelectSQL_Main(sql);
        }
    }



}
