﻿
using System.Data;
using System;
using PC_Shop24Hrs.Controllers;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.Class
{
    class BranchClass
    {
        //MyJeeds ค้นหาสาขาทั้งหมดที่เปิด เฉพาะสาขา MN
        public static DataTable GetBranchAll(string _pSta, string _pStaOpen) //_pSta 1=MN,2=SUPC,3=YA Cop,4 = MN SPC // _pStaOpen 0 = ก่อสร้าง,1 = เปิด ,2 = ปิด
        {
            string sql = $@"
            SELECT  *,BRANCH_ID +'-'+BRANCH_NAME AS NAME_BRANCH ,'0' AS STA ,'0' AS C  ,
                    CASE BRANCH_STAOPEN  WHEN '1' THEN 'เปิดบริการ' ELSE 'ปิดบริการ' END AS BRANCH_STAOPENDESC,
                    CASE BRANCH_ORDERTOAX  WHEN '1' THEN 'เปิดใช้งานระบบ' ELSE 'ปิดใช้งานระบบ' END AS BRANCH_ORDERTOAXDESC        
            FROM    SHOP_BRANCH WITH (NOLOCK) 
            WHERE   BRANCH_STA IN ({_pSta}) 
                    AND  BRANCH_STAOPEN IN ( {_pStaOpen}) ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //MyJeeds ค้นหาสาขาทั้งหมดที่เปิด เฉพาะสาขา MN
        public static DataTable GetBranchAll_ByConditions(string _pSta, string _pStaOpen, string _pConditions) //_pSta 1=MN,2=SUPC,3=YA Cop,4 = MN SPC // _pStaOpen 0 = ก่อสร้าง,1 = เปิด ,2 = ปิด
        {
            string sql = $@"
            SELECT  SHOP_BRANCH.*,SHOP_BRANCH.BRANCH_ID +'-'+SHOP_BRANCH.BRANCH_NAME AS NAME_BRANCH ,                    
                    CASE BRANCH_ORDERTOAX  WHEN '1' THEN 'เปิดใช้งานระบบ' ELSE 'ปิดใช้งานระบบ' END AS BRANCH_ORDERTOAXDESC,
                    SHOP_BRANCH.BRANCH_ID AS DATA_ID,SHOP_BRANCH.BRANCH_NAME AS DATA_DESC  ,
                    SHOP_PRICE.LABEL,CASE WHEN BRANCH_STA = '1' THEN 'มินิมาร์ท' WHEN BRANCH_STA = '2' THEN 'ซุปเปอร์ชีป' WHEN BRANCH_STA = '3' THEN 'ร้านยาน้องคอป' WHEN BRANCH_STA = '4' THEN 'ซุปเปอร์ชีป[ย่อย]' END AS BRANCH_STATUSDESC,
                    CASE WHEN BRANCH_STAOPEN = '0' THEN 'กำลังก่อสร้าง' WHEN BRANCH_STAOPEN = '1' THEN 'เปิดบริการ'     WHEN BRANCH_STAOPEN = '2' THEN 'ปิดบริการ' END AS BRANCH_STAOPENDESC,
                    CASE BRANCH_STAOPEN WHEN '1' THEN  CASE BRANCH_STA WHEN '1'  THEN 'http:'+BRANCH_SERVER_IP+'/Service_Shop24Hrs2013/Service_PDA_Shop24Hrs.asmx/AAA_UpdateService' ELSE '' END ELSE '' END   AS SERVICEPDA ,
					BRANCH_VERSIONUPDATEPDA,BRANCH_ManagerID+'-'+BRANCH_ManagerName AS BRANCH_ManagerName,BRANCH_ADDR,
		            ISNULL([BRANCH_ROUTEBEER],'') AS BRANCH_ROUTEBEER,ISNULL([BRANCH_VENDERBEER],'') AS BRANCH_VENDERBEER,
                    BRANCH_FarmHouse,
                    ISNULL(BRANCH_V014483,'') AS BRANCH_V014483,ISNULL(BRANCH_V005450,'') AS BRANCH_V005450,BRANCH_CHANNEL,SHOW_DESC AS BRANCH_PROVINCENAME,
                    IIF(BRANCH_RouteSendCoin=1,'สาย 1',IIF(BRANCH_RouteSendCoin=2,'สาย 2',IIF(BRANCH_RouteSendCoin=3,'สาย 3','ยังไม่ระบุ'))) AS BRANCH_RouteSendCoinName

            FROM    SHOP_BRANCH WITH (NOLOCK) 
                    LEFT OUTER JOIN SHOP_BRANCH_CONFIGDB WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = SHOP_BRANCH_CONFIGDB.BRANCH_ID 
                    LEFT OUTER JOIN SHOP_PRICE WITH(NOLOCK) ON SHOP_BRANCH.BRANCH_PRICE = SHOP_PRICE.COLUMNNAME 
                    LEFT OUTER JOIN (SELECT	SHOW_ID,SHOW_DESC	FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) WHERE	TYPE_CONFIG = '50' )TMP
						ON SHOP_BRANCH.BRANCH_PROVINCE = TMP.SHOW_ID

            WHERE   BRANCH_STA IN ({_pSta}) and  BRANCH_STAOPEN IN ({_pStaOpen}) {_pConditions}";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        // MyJeeds ค้นหารายละเอียดสาขาตามสาขา
        public static DataTable GetDetailBranchByID(string _bch, string _pCon = " AND BRANCH_STAOPEN != '2' ")
        {
            string sql = $@"
                    SELECT  *,BRANCH_ID +'-'+BRANCH_NAME AS NAME_BRANCH  
                    FROM    SHOP_BRANCH WITH (NOLOCK) 
                    WHERE   BRANCH_ID = '{_bch}' {_pCon} ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        // MyJeeds ค้นหาชือสาขาตามสาขา
        public static string GetBranchNameByID(string _bch)
        {
            string sql = $@"
                    SELECT  BRANCH_NAME 
                    FROM    SHOP_BRANCH WITH (NOLOCK) 
                    WHERE   BRANCH_ID ='{_bch}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return dt.Rows[0]["BRANCH_NAME"].ToString();
        }
        //MyJeeds ค้นหาการตั้งค่าสาขาที่สำคัญ
        public static DataTable GetBranchSettingBranch(string _bchID)
        {
            return ConnectionClass.SelectSQL_Main(" BranchClass_GetBranchSettingBranch '" + _bchID + @"' ");
        }
        //MyJeeds ค้นหาเบอร์โทรของแต่แผนก
        public static String GetTel_ByBranchID(string _bchID)
        {
            string sql = String.Format(@"SELECT	*	FROM	SHOP_PHONEBOOK WITH (NOLOCK) WHERE	PHONEDPT = '" + _bchID + "'");
            DataTable Dt = ConnectionClass.SelectSQL_Main(sql);
            if (Dt.Rows.Count == 0) return "ไม่มีการตั้งค่าเบอร์โทร";
            else
            {
                string telNum = "";
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    telNum = telNum + Dt.Rows[i]["PhoneID"].ToString() + "|";
                }
                return "Tel. " + telNum.Substring(0, telNum.Length - 1);
            }
        }
        ////MyJeeds ค้นหาสาขาทั้งหมด เพื่อไปแสดงหน้าจอค้นหาสาขาทั้งหมด [เฉพาะสาขาทั้งหมดไม่รวมสาขาปิด] ฟังก์ชั่นนี้ไว้ใช้กับหน้า FormShare เท่านั้น
        //public static DataTable GetBranchForShow()
        //{
        //    string sql = $@"
        //                SELECT  BRANCH_ID AS DATA_ID,BRANCH_NAME AS DATA_DESC    
        //                FROM    SHOP_BRANCH WITH (NOLOCK) 
        //                WHERE   BRANCH_STA = '1' AND BRANCH_STAOPEN != '2' ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //Bee ค้นหาสาขาทั้งหมดหน้าหลักสาขา

        //public static DataTable GetBranch()
        //{
        //    string sql = $@"
        //    SELECT	SHOP_BRANCH.BRANCH_ID, SHOP_BRANCH.BRANCH_NAME,SHOP_PRICE.LABEL,BRANCH_SERVERNAME,BRANCH_SERVICE,BRANCH_SERVICEID,BRANCH_SERVICETEL,
        //      CASE WHEN BRANCH_STA = '1' THEN 'มินิมาร์ท' WHEN BRANCH_STA = '2' THEN 'ซุปเปอร์ชีป' WHEN BRANCH_STA = '3' THEN 'ร้านยาน้องคอป'
        //      WHEN BRANCH_STA = '4' THEN 'ซุปเปอร์ชีป[ย่อย]' END AS BRANCH_STATUS,
        //      CASE WHEN BRANCH_STAOPEN = '0' THEN 'กำลังก่อสร้าง' WHEN BRANCH_STAOPEN = '1' THEN 'เปิดบริการ'  
        //      WHEN BRANCH_STAOPEN = '2' THEN 'ปิดบริการ' END AS BRANCH_STAOPEN,
        //      CASE BRANCH_STAOPEN WHEN '1' THEN  CASE BRANCH_STA WHEN '1' 
        //      THEN 'http:'+BRANCH_SERVER_IP+'/Service_Shop24Hrs2013/Service_PDA_Shop24Hrs.asmx/AAA_UpdateService' ELSE '' END ELSE '' END   AS SERVICEPDA ,
        //      BRANCH_VERSIONUPDATEPDA,BRANCH_ManagerID+'-'+BRANCH_ManagerName AS BRANCH_ManagerName,BRANCH_ADDR,
        //      ISNULL([BRANCH_ROUTEBEER],'') AS BRANCH_ROUTEBEER,ISNULL([BRANCH_VENDERBEER],'') AS BRANCH_VENDERBEER,
        //            BRANCH_FarmHouse,ISNULL(BRANCH_V014483,'') AS BRANCH_V014483,
        //            ISNULL(BRANCH_V005450,'') AS BRANCH_V005450

        //    FROM	SHOP_BRANCH WITH(NOLOCK) 
        //      LEFT OUTER JOIN SHOP_BRANCH_CONFIGDB WITH(NOLOCK)  ON SHOP_BRANCH.BRANCH_ID = SHOP_BRANCH_CONFIGDB.BRANCH_ID
        //      LEFT OUTER JOIN SHOP_PRICE WITH(NOLOCK) ON SHOP_BRANCH.BRANCH_PRICE = SHOP_PRICE.COLUMNNAME 

        //    ORDER BY BRANCH_ID
        //    ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        ////MyJeeds Config สาขา
        //public static DataTable GetConfigBranch(string pFlied)
        //{
        //    string sql = $@"
        //        SELECT	*  
        //        FROM	SHOP_BRANCH_CONFIGDB WITH (NOLOCK) 
        //        " + pFlied + "";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        ////ค้นหาสาขาทั้งหมด
        //public static DataTable GetDetailBranchByIDAll(string _bch)
        //{
        //    string sql = $@"
        //            select  *,BRANCH_ID +'-'+BRANCH_NAME AS NAME_BRANCH  
        //            from    SHOP_BRANCH WITH (NOLOCK) 
        //            where   BRANCH_ID ='" + _bch + "'";
        //    DataTable dt = ConnectionClass.SelectSQL_Main(sql);
        //    return dt;
        //}
        //return server ที่เก็บรูป Path VDO ขาย พร้อมสั่ง Connecttion Server ขายสาขา 
        public static DataTable GetDBBranch(string _bch)
        {
            DataTable dtBch = new DataTable();
            dtBch.Columns.Add("SERVER_Connection");
            dtBch.Columns.Add("SERVER_PATHVDO");
            string sql = $@"
                SELECT	BRANCH_SERVER_IP,BRANCH_DATABASENAME,BRANCH_USERNAME,BRANCH_PASSWORDNAME,BRANCH_PATHPOS	
                FROM	SHOP_BRANCH_CONFIGDB WITH (NOLOCK)
                WHERE	BRANCH_ID = '{_bch}'
            ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                dtBch.Rows.Add($@"Data Source={dt.Rows[0]["BRANCH_SERVER_IP"]};Initial Catalog={dt.Rows[0]["BRANCH_DATABASENAME"]};User Id={dt.Rows[0]["BRANCH_USERNAME"]};Password={dt.Rows[0]["BRANCH_PASSWORDNAME"]}", $@"{dt.Rows[0]["BRANCH_PATHPOS"]}");
            }
            return dtBch;
        }
        //ประเภทสาขา
        public static DataTable GetBranchType()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BRANCH_OUTPHUKET");
            dt.Columns.Add("OUTPHUKET");
            dt.Rows.Add("1", "ในจังหวัด");
            dt.Rows.Add("0", "ต่างจังหวัด");
            return dt;
            //string sql = $@"
            //SELECT	BRANCH_OUTPHUKET,CASE BRANCH_OUTPHUKET  WHEN '1' THEN 'ในจังหวัด' ELSE 'ต่างจังหวัด' END AS OUTPHUKET
            //FROM	SHOP_BRANCH	WITH (NOLOCK)
            //GROUP BY BRANCH_OUTPHUKET,CASE BRANCH_OUTPHUKET  WHEN '1' THEN 'ในจังหวัด' ELSE 'ต่างจังหวัด' END
            //";
            //return ConnectionClass.SelectSQL_Main(sql);
        }
        //Update branch config db
        public static string Update_BRANCH_CONFIGDB(string _pCase, string bchID, string Values)
        {
            string sql = "";
            switch (_pCase)
            {
                case "0"://Update สายส่งเหล้าเบียร์
                    sql = $@"   UPDATE  SHOP_BRANCH_CONFIGDB 
                                SET     [BRANCH_ROUTEBEER]  = '{Values}'
                                WHERE   BRANCH_ID = '{bchID}'";
                    break;
                case "1"://Update Vender สิงห์-ลีโอ
                    sql = $@"   UPDATE  SHOP_BRANCH_CONFIGDB 
                                SET     [BRANCH_VENDERBEER]  = '{Values}'
                                WHERE   BRANCH_ID = '{bchID}'";
                    break;
                case "2"://การแจกของแถมลูกค้า //FreeItem CashDesktop
                    sql = $@"   UPDATE  SHOP_BRANCH_CONFIGDB
                                SET     BRANCH_FreeItem = '{Values}'
                                WHERE   BRANCH_ID = '{bchID}' ";
                    break;
                case "3"://สาขาที่บังคับชั้นวาง
                    sql = $@"   UPDATE  SHOP_BRANCH_CONFIGDB 
                                SET     BRANCH_STATUSSHELF = '{Values}'
                                WHERE   BRANCH_ID = '{bchID}'";
                    break;
            }
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Update branch config แต่เป็นการเช็คขอ้มูลก่อนว่ามีอยู่แล้วมั้ย เพื่อป้องกันการซ้ำกันของ File
        public static DataTable Update_BRANCH_CONFIGDB_CheckPK(string pField, string bchID, string Values)
        {
            string sql = $@"    DECLARE @DIMTABLE TABLE (DESC_STATUS NVARCHAR(500));
                                DECLARE @Bch AS NVARCHAR(50) = '{bchID}';
                                DECLARE @Values AS NVARCHAR(200) = '{Values}';

                                IF NOT EXISTS  (SELECT	*	FROM	SHOP_BRANCH_CONFIGDB WITH (NOLOCK) WHERE	{pField} = @Values AND  BRANCH_ID != @Bch)
                                BEGIN
	                                UPDATE  SHOP_BRANCH_CONFIGDB 
	                                SET     {pField} = @Values
                                    WHERE   BRANCH_ID = @Bch
		 
	                                INSERT INTO @DIMTABLE VALUES ('')
                                END
                                ELSE
                                  INSERT INTO @DIMTABLE VALUES ('ข้อมูลที่ระบุมีบันทึกใช้งานที่สาขาอื่นอยู่แล้ว เช็คใหม่อีกครั้ง')


                                SELECT * FROM @DIMTABLE";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ระดับราคา
        public static DataTable GetPriceLevel(string sqlWhere = "")
        {
            string strprice = $@"
                  SELECT COLUMNNAME,LABEL
                  FROM  SHOP_PRICE WITH (NOLOCK)  {sqlWhere}
                  ORDER BY COLUMNNAME  ASC ";
            return ConnectionClass.SelectSQL_Main(strprice);
        }
        //สายส่งเหรียญ
        public static void GetDataRouteSendCoin(RadDropDownList radDropDownList)
        {
            DataTable dtCoin = new DataTable();
            dtCoin.Columns.Add("Route");
            dtCoin.Columns.Add("RouteName");
            dtCoin.Rows.Add("0", "ยังไม่ระบุ");
            dtCoin.Rows.Add("1", "สาย 1");
            dtCoin.Rows.Add("2", "สาย 2");
            dtCoin.Rows.Add("3", "สาย 3");
            radDropDownList.DataSource = dtCoin;
            radDropDownList.DisplayMember = "RouteName";
            radDropDownList.ValueMember = "Route";
        }
    }
}
