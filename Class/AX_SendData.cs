﻿using PC_Shop24Hrs.Controllers;
using System;
using System.Collections;
using System.Data;

namespace PC_Shop24Hrs.Class
{
    public class Var_PURCHTABLE
    {
        public string PURCHASETYPE { get; set; } = "";
        public string PURCHSTATUS { get; set; } = "";
        public string TAXINVOICE { get; set; } = "";
        public string INCLTAX { get; set; } = "";
        public string PURCHID { get; set; } = "";
        public string PURCHDATE { get; set; } = "";
        public string ORDERACCOUNT { get; set; } = "";
        public string INVENTLOCATIONID { get; set; } = "";
        public string DIMENSION { get; set; } = "";
        public string REMARK { get; set; } = "";
        public string INVOICEID { get; set; } = "";
        public string INVOICEDATE { get; set; } = "";
        public string VENDORREF { get; set; } = "";
        public string NUMRECEIVE { get; set; } = "";
    }
    public class Var_PURCHLINE
    {
        public string PURCHID { get; set; } = "";
        public string PURCHDATE { get; set; } = "";
        public int LINENUM { get; set; } = 1;
        public string BARCODE { get; set; } = "";
        public string NAME { get; set; } = "";
        public string REMARK { get; set; } = "";
        public double PURCHQTY { get; set; } = 0;
        public double PURCHPRICE { get; set; } = 0;
        public string MULTIDISCTXT { get; set; } = "";

    }
    public class Var_POSTOTABLE
    {
        public string FREIGHTSLIPTYPE { get; set; } = "";
        public string CASHIERID { get; set; } = "";
        public string ALLPRINTSTICKER { get; set; } = "";
        public string INVENTLOCATIONIDTO { get; set; } = "";
        public string TRANSFERSTATUS { get; set; } = "";
        public string INVENTLOCATIONIDFROM { get; set; } = "";
        public string TRANSFERID { get; set; } = "";
        public string SHIPDATE { get; set; } = "";
        public string VOUCHERID { get; set; } = "";
        public string REMARKS { get; set; } = "";
        public string DlvTermId { get; set; } = "";
    }
    public class Var_POSTOLINE
    {
        public string VOUCHERID { get; set; } = "";
        public string SHIPDATE { get; set; } = "";
        public int LINENUM { get; set; } = 1;
        public double QTY { get; set; } = 0;
        public double SALESPRICE { get; set; } = 0;
        public string ITEMID { get; set; } = "";
        public string NAME { get; set; } = "";
        public string SALESUNIT { get; set; } = "";
        public string ITEMBARCODE { get; set; } = "";
        public string INVENTDIMID { get; set; } = "";
        public string INVENTTRANSID { get; set; } = "";
    }

    class AX_SendData
    {
        public static ArrayList JOBWaitForSendDataToAX(string pType, string tableName, string billID)
        {
            ArrayList sqlDel = new ArrayList();
            switch (pType)
            {
                case "SPC_EXTERNALLIST":
                    sqlDel.Add($@"
                       DELETE   SPC_EXTERNALLIST
                       WHERE    LOG != '' 
                                AND TABLENAME = '{tableName}'  AND CAST(PKFIELDVALUE AS NVARCHAR) = N'{billID}'
                    ");
                    break;
                case "SPC_POSTOTABLE20":
                    sqlDel.Add(@"
                       DELETE   SPC_POSTOLINE20
                       WHERE    VOUCHERID IN (SELECT VOUCHERID FROM SPC_POSTOTABLE20 WHERE    VOUCHERID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_POSTOTABLE20
                       WHERE    VOUCHERID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_POSTOTABLE18":
                    sqlDel.Add(@"
                       DELETE   SPC_POSTOLINE18
                       WHERE    VOUCHERID IN (SELECT VOUCHERID FROM SPC_POSTOTABLE18 WHERE    VOUCHERID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_POSTOTABLE18
                       WHERE    VOUCHERID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_POSTOTABLE10":
                    sqlDel.Add(@"
                       DELETE   SPC_POSTOLINE10
                       WHERE    VOUCHERID IN (SELECT VOUCHERID FROM SPC_POSTOTABLE10 WHERE    VOUCHERID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_POSTOTABLE10
                       WHERE    VOUCHERID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_PURCHTABLE00":
                    sqlDel.Add(@"
                       DELETE   SPC_PURCHLINE00
                       WHERE    PURCHID IN (SELECT PURCHID FROM SPC_PURCHTABLE00 WHERE  PURCHID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_PURCHTABLE00
                       WHERE    PURCHID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_POSWSTABLE20":
                    sqlDel.Add(@"
                       DELETE   SPC_POSWSLINE20
                       WHERE    INVOICEID IN (SELECT INVOICEID FROM SPC_POSWSTABLE20 WHERE  INVOICEID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_POSWSTABLE20
                       WHERE    INVOICEID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_TRANSFERUPDRECEIVE":
                    sqlDel.Add(@"
                       DELETE   SPC_TRANSFERUPDRECEIVE
                       WHERE    BOXID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_REDEMPTABLE00":
                    sqlDel.Add(@"
                       DELETE   SPC_REDEMPLINE00
                       WHERE    REDEMPID IN (SELECT REDEMPID FROM SPC_REDEMPTABLE00 WHERE  REDEMPID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_REDEMPTABLE00
                       WHERE    REDEMPID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                case "SPC_INVENTJOURNALTABLE":
                    sqlDel.Add(@"
                       DELETE   SPC_INVENTJOURNALTRANS
                       WHERE    JOURNALID IN (SELECT JOURNALID FROM SPC_INVENTJOURNALTABLE WHERE  JOURNALID = '" + billID + @"' AND LOG != '' )
                    ");
                    sqlDel.Add(@"
                       DELETE   SPC_INVENTJOURNALTABLE
                       WHERE    JOURNALID = '" + billID + @"' AND LOG != ''
                    ");
                    break;
                default:
                    break;
            }
            return sqlDel;
        }
        //ใบสั่งซื้อ HD
        //public static string SaveHD_PURCHTABLE(string pPURCHASETYPE, string pPURCHSTATUS,
        //    string pTAXINVOICE, string pINCLTAX,
        //    string pPURCHID, string pPURCHDATE,
        //    string pORDERACCOUNT, string pINVENTLOCATIONID, string pDIMENSION, string pREMARK,
        //    string pINVOICEID, string pINVOICEDATE, string pVENDORREF, string pNUMRECEIVE)
        public static string SaveHD_PURCHTABLE(Var_PURCHTABLE var_PURCHTABLE)
        {
            ///*** PURCHASETYPE = 3 [เป็นใบสั่งซื้อทั้งหมด] 
            ///*** PURCHSTATUS = 1 คือสถานะใบสั่งซื้อเปิดค้างไว้ 3 ลงรายการบัญชีแล้ว
            ///*** TAXINVOICE 0 บิล NoVat 1 บิล Vat
            ///*** INCLTAX 0 แยกน้อก 1 รวมใน
            /// INVOICEID ในกรณีที่ PURCHSTATUS = 3 ตรงนี้คือ เลขที่เอกสาร invoice ของ vender
            /// INVOICEDATE ในกรณีที่ PURCHSTATUS = 3 ตรงนี้คือ วันที่บิล ของ vender
            /// VENDORREF ในกรณีที่ PURCHSTATUS = 3 ตรงนี้คือ ผู้จำหน่าย invoice ของ vender
            /// NUMRECEIVE ในกรณีที่ PURCHSTATUS = 3 ตรงนี้คือ เลขที่ RA-MA ของ vender
            return $@"
                INSERT INTO SPC_PURCHTABLE00(DATAAREAID, RECVERSION, RECID, PURCHID,  
                        PURCHDATE, PURCHASETYPE, ORDERACCOUNT,
                        TAXINVOICE, INCLTAX, INVENTSITEID, INVENTLOCATIONID, PURCHPLACER,
                        REMARK, DIMENSION, PURCHSTATUS, NUM, TRANSDATE,
                        INVOICEID, INVOICEDATE, VENDORREF, NUMRECEIVE)
                VALUES ('SPC','1', '1','{var_PURCHTABLE.PURCHID}',
                        '{var_PURCHTABLE.PURCHDATE}', '{var_PURCHTABLE.PURCHASETYPE}', '{var_PURCHTABLE.ORDERACCOUNT}',
                        '{var_PURCHTABLE.TAXINVOICE}', '{var_PURCHTABLE.INCLTAX}', 'SPC','{var_PURCHTABLE.INVENTLOCATIONID}','{SystemClass.SystemUserID_M}',
                        '{var_PURCHTABLE.REMARK}', '{var_PURCHTABLE.DIMENSION}', '{var_PURCHTABLE.PURCHSTATUS}', '{var_PURCHTABLE.NUMRECEIVE}','{var_PURCHTABLE.PURCHDATE}',
                        '{var_PURCHTABLE.INVOICEID}','{var_PURCHTABLE.INVOICEDATE}','{var_PURCHTABLE.VENDORREF}','{var_PURCHTABLE.NUMRECEIVE}') ";
        }
        //ใบสั่งซื้อ DT
        //public static string SaveDT_PURCHTABLE(string pPURCHID, string pPURCHDATE,
        //    int pLINENUM, string pBARCODE, string pNAME, double pPURCHQTY, double pPURCHPRICE, string pREMARK)
        public static string SaveDT_PURCHLINE(Var_PURCHLINE var_PURCHLINE)
        {
            /// PURCHPRICE คือ ราคาต้นทุน
            /// MULTIDISCTXT คือ ส่วนลด
            return $@"
                INSERT INTO SPC_PURCHlINE00(DATAAREAID,RECVERSION,RECID,
                        PURCHID,PURCHDATE,LINENUM,PURCHQTY,
                        PURCHPRICE,MULTIDISCTXT,BARCODE, NAME, REMARK) 
                VALUES  ('SPC', '1', '1', 
                        '{var_PURCHLINE.PURCHID}','{var_PURCHLINE.PURCHDATE}','{var_PURCHLINE.LINENUM}','{var_PURCHLINE.PURCHQTY}',
                        '{var_PURCHLINE.PURCHPRICE}', '{var_PURCHLINE.MULTIDISCTXT}', '{var_PURCHLINE.BARCODE}','{var_PURCHLINE.NAME}', '{var_PURCHLINE.REMARK}')  ";
        }
        //ใบสั่งโอนย้าย Fix WH
        public static string SaveHD_POSTOTABLE18(Var_POSTOTABLE varHD)
        {
            return $@"
                    INSERT INTO SPC_POSTOTABLE18 (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,
                        DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS, 
                        INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, 
                        TRANSFERID, SHIPDATE, VOUCHERID,REMARKS,DLVMODE)  
                    VALUES  ('{varHD.FREIGHTSLIPTYPE}','{varHD.CASHIERID}','{varHD.ALLPRINTSTICKER}',
                        'SPC','1','1','{varHD.TRANSFERSTATUS}', 
                        '{varHD.INVENTLOCATIONIDTO}', '{varHD.INVENTLOCATIONIDFROM}', 
                        '{varHD.TRANSFERID}', '{varHD.SHIPDATE}', '{varHD.VOUCHERID}','{varHD.REMARKS}','{varHD.DlvTermId}' )";
        }
        //ใบสั่งโอนย้าย Fix WH
        public static string SaveDT_POSTOLINE18(Var_POSTOLINE varDT)
        {
            return $@"
                    INSERT INTO SPC_POSTOLINE18 (RECID,REFBOXTRANS,DATAAREAID, 
                        VOUCHERID,SHIPDATE,LINENUM,QTY,
                        SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                        INVENTDIMID,INVENTTRANSID,RECVERSION) 
                    VALUES ('1','0','SPC',
                        '{varDT.VOUCHERID}', '{varDT.SHIPDATE}', '{varDT.LINENUM}', '{varDT.QTY}',
                        '{varDT.SALESPRICE}','{varDT.ITEMID}','{ConfigClass.ChecKStringForImport(varDT.NAME)}', '{varDT.SALESUNIT}','{varDT.ITEMBARCODE}',
                        '{varDT.INVENTDIMID}','{varDT.INVENTTRANSID}','1')";
        }
        //Send Data AX SPC_POSTOTABLE10 NO Fix WH -- เปิดบิลจาก Minimart
        public static string Save_POSTOLINE10(ArrayList sqlUp24, DataTable dtDataForSendAX, string FREIGHTSLIPTYPE_0MNTF_98MNPO)
        {
            ArrayList sqlAX = new ArrayList();
            //ในกรณีที่ต้องหาคลังก่อน 1 บิลมีมากกว่า 1 คลัง
            int SeqNo = 0, II = 1;
            string Docno = "-";

            try
            {
                for (int i = 0; i < dtDataForSendAX.Rows.Count; i++)
                {
                    if (Docno != dtDataForSendAX.Rows[i]["WHSoure"].ToString())
                    {
                        Docno = dtDataForSendAX.Rows[i]["WHSoure"].ToString();
                        SeqNo += 1;
                        II = 1;

                        //HD
                        sqlAX.Add($@"
                        insert into SPC_POSTOTABLE10 
                            (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                            INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
                        values('{FREIGHTSLIPTYPE_0MNTF_98MNPO}','{dtDataForSendAX.Rows[i]["WHOINS"]}','0','SPC','1','1','0',
                            '{dtDataForSendAX.Rows[i]["WHDestination"]}', 
                            '{dtDataForSendAX.Rows[i]["WHSoure"]}',
                            '{dtDataForSendAX.Rows[i]["DOCNO"]}-{SeqNo}',
                            '{dtDataForSendAX.Rows[i]["DateIns"]}',
                            '{dtDataForSendAX.Rows[i]["DOCNO"]}-{SeqNo}', '{ dtDataForSendAX.Rows[i]["Lock"]}',
                            '{dtDataForSendAX.Rows[i]["RMK"]}' )  ");

                        //DT  
                        sqlAX.Add($@"
                            insert into SPC_POSTOLINE10 
                                        (RECID,REFBOXTRANS,DATAAREAID,
                                        VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                        INVENTDIMID,INVENTTRANSID,RECVERSION) 
                            values('1','0','SPC','{ dtDataForSendAX.Rows[i]["DOCNO"]}-{SeqNo}',
                                        '{dtDataForSendAX.Rows[i]["DateIns"]}',
                                        '{II}',
                                        '{Convert.ToDouble(dtDataForSendAX.Rows[i]["QtyOrder"].ToString()) }',
                                        '{Convert.ToDouble(dtDataForSendAX.Rows[i]["PRICE"].ToString()) }',
                                        '{dtDataForSendAX.Rows[i]["ITEMID"]}',
                                        '{ConfigClass.ChecKStringForImport(dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString())}',
                                        '{dtDataForSendAX.Rows[i]["UnitID"]}',
                                        '{dtDataForSendAX.Rows[i]["ITEMBARCODE"]}',
                                        '{dtDataForSendAX.Rows[i]["INVENTDIM"]}',
                                        '{dtDataForSendAX.Rows[i]["DOCNO"]}-{dtDataForSendAX.Rows[i]["LineNum"]}',
                                        '1')");
                        II += 1;
                    }
                    else
                    {
                        sqlAX.Add($@"
                            insert into SPC_POSTOLINE10 
                                        (RECID,REFBOXTRANS,DATAAREAID,
                                        VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                        INVENTDIMID,INVENTTRANSID,RECVERSION) 
                            values('1','0','SPC','{dtDataForSendAX.Rows[i]["DOCNO"]}-{ SeqNo}',
                                        '{dtDataForSendAX.Rows[i]["DateIns"]}',
                                        '{II} ',
                                        '{Convert.ToDouble(dtDataForSendAX.Rows[i]["QtyOrder"].ToString())}',
                                        '{Convert.ToDouble(dtDataForSendAX.Rows[i]["PRICE"].ToString())}',
                                        '{dtDataForSendAX.Rows[i]["ITEMID"]}',
                                        '{ConfigClass.ChecKStringForImport(dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString())}',
                                        '{dtDataForSendAX.Rows[i]["UnitID"]}',
                                        '{dtDataForSendAX.Rows[i]["ITEMBARCODE"]}',
                                        '{dtDataForSendAX.Rows[i]["INVENTDIM"]}',
                                        '{dtDataForSendAX.Rows[i]["DOCNO"]}-{dtDataForSendAX.Rows[i]["LineNum"]}',
                                        '1') ");
                        II += 1;
                    }
                }
                return ConnectionClass.ExecuteMain_AX_24_SameTime(sqlUp24, sqlAX);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        //Send Data AX ในกรณีที่ fix คลัง Save พร้อม 24
        public static string Save_POSTOTABLE20(ArrayList sqlUp24, DataTable dtDataForSendAX, string FREIGHTSLIPTYPE_0MNTF_98MNPO, string statusAX)
        {
            string a_Docno = dtDataForSendAX.Rows[0]["DOCNO"].ToString();
            string a_Date = dtDataForSendAX.Rows[0]["DateIns"].ToString();

            ArrayList sqlAX = new ArrayList
            {//HD
                $@"
                    insert into SPC_POSTOTABLE20 
                        (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                        INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
                    values('{FREIGHTSLIPTYPE_0MNTF_98MNPO}','{SystemClass.SystemUserID_M}','0','SPC','1','1','{statusAX}',
                        '{dtDataForSendAX.Rows[0]["WHDestination"]}', 
                        '{dtDataForSendAX.Rows[0]["WHSoure"]}',
                        '{a_Docno}',
                        '{a_Date}',
                        '{a_Docno}', '{dtDataForSendAX.Rows[0]["Lock"]}',
                        '{dtDataForSendAX.Rows[0]["RMK"]}') "
            };
            //DT

            for (int i = 0; i < dtDataForSendAX.Rows.Count; i++)
            {
                double a_qty = double.Parse(dtDataForSendAX.Rows[i]["QtyOrder"].ToString());
                string a_ItemID = dtDataForSendAX.Rows[i]["ITEMID"].ToString();
                string a_name = dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString().Replace("{", "").Replace("}", "").Replace("'", "");
                string a_Unit = dtDataForSendAX.Rows[i]["UnitID"].ToString();
                string a_Barcode = dtDataForSendAX.Rows[i]["ITEMBARCODE"].ToString();
                string a_Dim = dtDataForSendAX.Rows[i]["INVENTDIM"].ToString();
                string a_LineNum = dtDataForSendAX.Rows[i]["LineNum"].ToString();
                string a_tranID = a_Docno + "-" + a_LineNum;

                sqlAX.Add($@"
                    insert into SPC_POSTOLINE20 
                                (RECID,REFBOXTRANS,DATAAREAID,
                                VOUCHERID,SHIPDATE,LINENUM,QTY,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTDIMID,INVENTTRANSID,RECVERSION) 
                    values('1','0','SPC',
                                '{a_Docno}','{a_Date}','{i + 1}','{a_qty}','{a_ItemID}','{a_name}','{a_Unit}','{a_Barcode}',
                                '{a_Dim}','{a_tranID}','1')
                    ");
            }
            return ConnectionClass.ExecuteMain_AX_24_SameTime(sqlUp24, sqlAX);
        }
        //ส่งค่าเข้า AX แต่บันทึกที่หน้า Main
        public static string Save_POSTOTABLE20_HD(string sta_TRANSFERSTATUS, string bchID, string Invent, string docNo, string EMPLPICK, string EMPLCHECKER)
        {
            return $@"  INSERT INTO [SPC_POSTOTABLE20] ( 
                                FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                                INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,EMPLPICK,EMPLCHECKER)
                        VALUES ('98','{SystemClass.SystemUserID_M}','0','SPC','1','1','{sta_TRANSFERSTATUS}',
                                '{bchID}','{Invent}',
                                '{docNo}',CONVERT(VARCHAR,GETDATE(),23),'{docNo}','{EMPLPICK}','{EMPLCHECKER}' ) ";
        }
        //ส่งค่าเข้า AX แต่บันทึกที่หน้า Main
        public static string Save_POSTOTABLE20_DT(string docNo, int iRow, string itemID, string itemDim, string itembarcode, string itemName, double qty, string unitID)
        {
            return $@"  INSERT INTO [SPC_POSTOLINE20] (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,
                                ITEMID,NAME,SALESUNIT,ITEMBARCODE,INVENTDIMID,INVENTTRANSID,RECVERSION)  
                        VALUES ('1','0','SPC','{docNo}',CONVERT(VARCHAR,GETDATE(),23),'{iRow}','{qty}',
                            '{itemID}','{ConfigClass.ChecKStringForImport(itemName)}','{unitID}',
                            '{itembarcode}','{itemDim}','{docNo}_{iRow}','1') ";
        }
        //ใบวางบิล
        public static string Save_PurchBillLine01(string pbDocno, int lineNum, string invoiceID, string leggerVoucher, string date)
        {
            string recID = leggerVoucher.Replace("PR", "").Replace("-", "").Replace("PXS", "");
            return $@"
                INSERT INTO SPC_PurchBillLine01 
                        (PURCHBILLID,LINENUM,INVOICEID,VOUCHER,REMARKS,REMARKSTABLE,TRANSDATE,DATAAREAID,RECVERSION,RECID) VALUES (
                        '{pbDocno}','{lineNum}','{invoiceID}','{leggerVoucher}','','','{date}','SPC','" + recID + @"','1')
            ";
        }
        //Update BA/MA
        public static string Update_ShipCarrierInvoice(string invoiceID, string DocnoMA, string puchID)
        {
            return $@"
            UPDATE	SPC_ShipCarrierInvoice
            SET		PurchId = '{puchID}' , Locked = '1'
            WHERE	SPC_ShipCarrierInvoice.NumReceive = '{DocnoMA}'
		            AND SPC_ShipCarrierInvoice.InvoiceId = '{invoiceID}'
		            AND SPC_ShipCarrierInvoice.Dataareaid = N'SPC'
            ";
        }

        public static string SPC_TRANSFERUPDRECEIVE_Insert(string bchID, string boxID, string documentNum, string rmk)
        {
            string sqlAX = $@"
                        INSERT INTO SPC_TRANSFERUPDRECEIVE 
                                    (   RECVERSION,RECID,DATAAREAID,
                                        VOUCHERID, BOXID, 
                                        EMPLID, RECEIVEDATE, REMARKS, INVENTLOCATIONIDTO) 
                        VALUES      (   '1','{boxID}','SPC',
                                        '{documentNum}',
                                        '{boxID}',
                                        '{SystemClass.SystemUserID_M}',CONVERT(VARCHAR,GETDATE(),23),'{rmk}',
                                        '{bchID}')
                    ";
            return ConnectionClass.ExecuteSQL_MainAX(sqlAX);
        }
        //SPC_EXTERNALLIST -- SPC_SHIPMENTINVOICE
        public static string Save_EXTERNALLIST_SHIPMENTINVOICE(string billID, string date, string bchID, string bchName, string recID)
        {
            string sql = $@"
                        INSERT INTO SPC_EXTERNALLIST (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,
                            METHODNAME,DATAAREAID,RECVERSION,RECID) 
                        VALUES ('SPC_SHIPMENTINVOICE','LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID','1|10|{billID}|SPC',
                            'LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT',
                            '1|10|{date}|{billID}|{SystemClass.SystemUserID}|{bchName}|{date}|{date}|{bchID}',
                            CONVERT(NVARCHAR,'CREATE'),'SPC','1','{recID}') 
                        ";
            return sql;
        }
        //Insert SPC_SHIPCARRIERINVOICE
        public static string Save_SHIPCARRIERINVOICE(string pType0Insert1Update, string maBill, string docno, double money, string recID,
            string receiveDate, string doubleCheckDate, string department, string venderID)
        {
            string sql = "";
            switch (pType0Insert1Update)
            {
                case "0":
                    sql = $@"
                        INSERT INTO SPC_SHIPCARRIERINVOICE(DATAAREAID,RECVERSION,RECID,
                                    NUMRECEIVE, DOUCBLECHECK,PACKINGSLIPID,INVOICEID,PURCHID,InvoiceAmount,
                                    RECEIVEDATE,DOUCBLECHECKDATE,DEPARTMENT,ACCOUNTNUM)  
                        VALUES('SPC', '1', '{recID}',
                                    '{maBill}', '1', '{maBill}', '{docno}', '', '{money}',
                                    '{receiveDate}','{doubleCheckDate}','{department}','{venderID}')";
                    break;
                case "1"://Edit By RECID
                    sql = $@"
                        UPDATE  SPC_SHIPCARRIERINVOICE 
                        SET     InvoiceAmount ='{money}',INVOICEID = '{docno}',
                                MODIFIEDDATETIME = convert(varchar, getdate(), 120)
                        WHERE   RECID = '{recID}'";
                    break;
                case "2"://Edit By NUMRECEIVE
                    sql = $@"
                        UPDATE  SPC_SHIPCARRIERINVOICE 
                        SET     InvoiceAmount ='{money}',INVOICEID = '{docno}',
                                MODIFIEDDATETIME = convert(varchar, getdate(), 120)
                        WHERE   NUMRECEIVE = '{maBill}'";
                    break;
                default:
                    break;
            }
            return sql;
        }
        //สร้าง-ลบสินค้าออกจากการหยุดสั่งให้สั่งได้ปกติ
        public static ArrayList Save_EXTERNALLIST_RETAILPLANORDERITEMBLOCKED(string pID, string pDim, string pBch, string pCase)
        {
            string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "");
            string pDate1 = DateTime.Now.ToString("yyyy-MM-dd");
            string pDate2 = DateTime.Now.AddDays(30).ToString("yyyy-MM-dd");

            string str = $@"
                    SELECT	*   
                    FROM	SHOP2013TMP.dbo.SPC_RETAILPLANORDERITEMBLOCKED  WITH (NOLOCK)  
                    WHERE	INVENTLOCATIONIDTO = '{pBch}' AND DATAAREAID = 'SPC' 
                            AND ITEMID = '{pID}'  AND INVENTDIMID =  '{pDim}'
            ";

            DataTable dtCheck;
            if (pCase == "CREATE")
            {
                dtCheck = ConnectionClass.SelectSQL_Main(str);
                if (dtCheck.Rows.Count > 0) pCase = "UPDATE";
            }
            else if (pCase == "UPDATE")
            {
                dtCheck = ConnectionClass.SelectSQL_Main(str);
                if (dtCheck.Rows.Count == 0) pCase = "CREATE";
            }

            string rmk = "Shop24Hrs_PC [" + SystemClass.SystemUserName + "-" + SystemClass.SystemDptName + "]";
            ArrayList sql = new ArrayList();

            switch (pCase)
            {
                case "CREATE":
                    sql.Add(@"
                        INSERT INTO SPC_EXTERNALLIST
                       (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID)   VALUES ( 
                        'SPC_RETAILPLANORDERITEMBLOCKED','ITEMID|INVENTDIMID|INVENTLOCATIONIDTO','" + pID + @"|" + pDim + @"|" + pBch + @"',
                        'ITEMID|INVENTDIMID|INVENTLOCATIONIDTO|FROMDATE|TODATE|Remarks','" + pID + @"|" + pDim + @"|" + pBch + @"|" + pDate1 + @"|" + pDate2 + @"|" + rmk + @"',
                        CONVERT(NVARCHAR,'" + pCase + @"'),'SPC','1',CONVERT(BIGINT," + recID + @") )
                    ");
                    break;
                case "DELETE":
                    sql.Add(@"
                        INSERT INTO SPC_EXTERNALLIST
                       (TABLENAME,PKFIELDNAME,PKFIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID)   VALUES ( 
                        'SPC_RETAILPLANORDERITEMBLOCKED','ITEMID|INVENTDIMID|INVENTLOCATIONIDTO','" + pID + @"|" + pDim + @"|" + pBch + @"',
                        CONVERT(NVARCHAR,'" + pCase + @"'),'SPC','1',CONVERT(BIGINT," + recID + @") )
                    ");
                    break;
                case "UPDATE":

                    sql.Add(@"
                        INSERT INTO SPC_EXTERNALLIST
                       (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID)   VALUES ( 
                        'SPC_RETAILPLANORDERITEMBLOCKED','ITEMID|INVENTDIMID|INVENTLOCATIONIDTO','" + pID + @"|" + pDim + @"|" + pBch + @"',
                        'FROMDATE|TODATE|Remarks','" + pDate1 + @"|" + pDate2 + @"|" + rmk + @"',
                        CONVERT(NVARCHAR,'" + pCase + @"'),'SPC','1',CONVERT(BIGINT," + recID + @") )
                    ");
                    break;
            }

            //return ConnectionClass.ExecuteSQL_ArrayMainAX(sql);
            return sql;
        }
        //สร้่างลูกค้า
        public static string Save_EXTERNALLIST_CUSTTABLE(string custId, string sqlAxColumns, string sqlAxValues, string recID)
        {
            string sql = $@" 
                INSERT INTO SPC_EXTERNALLIST  
                        (TABLENAME, PKFIELDNAME, PKFIELDVALUE, FIELDNAME, FIELDVALUE, LOG, METHODNAME, DATAAREAID, RECVERSION, RECID) 
                VALUES  ('CUSTTABLE', 'ACCOUNTNUM', '{custId}',  '{sqlAxColumns}',  '{sqlAxValues}', 
                        '', 'CREATE',  'SPC', '1', '{recID}')";
            return sql;
        }
        //EXTERNALLIST  
        public static string Save_EXTERNALLIST(string methodName, string tblName, string pkField, string pkValues, string fieldName, string fieldValues, string recID)
        {
            string sql = $@" 
                INSERT INTO SPC_EXTERNALLIST  
                        (TABLENAME, PKFIELDNAME, PKFIELDVALUE, FIELDNAME, FIELDVALUE, LOG, METHODNAME, DATAAREAID, RECVERSION, RECID) 
                VALUES  ('{tblName}', '{pkField}', '{pkValues}',  '{fieldName}',  '{fieldValues}', 
                        '', '{methodName}',  'SPC', '1', '{recID}')";
            return sql;
        }
        //Insert ExternalList ASSET Codition
        public static string Save_EXTERNALLIST_AssetCodition(string pAssID, string pCondition)//pAssID  คือ ทะเบียน pCondition สภาพการใช้งาน
        {
            string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "");
            string sql708 = $@"
                INSERT INTO SPC_EXTERNALLIST  
                    (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID) 
                VALUES ('AssetTable','AssetId|DATAAREAID','{pAssID}|SPC','Condition','{pCondition}',CONVERT(NVARCHAR,'UPDATE'),'SPC','1',{recID}) ";
            return ConnectionClass.ExecuteSQL_MainAX(sql708);
        }
        //Insert ExternalList Car Send AX
        public static string Save_EXTERNALLIST_OpenJOBCarExternalListLock(string pCarID, string pType, string pDesc, string pCarSN)//pType  คือ ประเภทที่เข้าซ่อม pDesc อุปกรณ์มีปัญหา
        {
            string recId = (DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")).Replace("-", "").ToString().Replace(":", "").Replace(" ", "");

            ArrayList sql = new ArrayList() {
            $@"
            INSERT INTO SPC_EXTERNALLIST 
                    (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,
                    METHODNAME,DATAAREAID,RECVERSION,RECID)  
            VALUES ('SPC_VehicleTrans','VehicleId|DATAAREAID','{pCarID}|SPC',
                    'VehicleId|TransType|EmplBooking|REPAIRITEMS|DIMENSION|SERIALNUMBER','{pCarID}|1|{SystemClass.SystemUserID}|{pType}-{pDesc}|{SystemClass.SystemDptID}|{pCarSN}',
                    CONVERT(NVARCHAR,'CREATE'),'SPC','1',CONVERT(BIGINT,{recId}) ) ",
            $@"
            INSERT INTO SPC_EXTERNALLIST 
                    (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID)  
            VALUES  ('SPC_VEHICLETABLE','VEHICLEID','{pCarID}','VEHICLESTATUS','5',CONVERT(NVARCHAR,'UPDATE'),'SPC','1',CONVERT(BIGINT,{recId}1) )",

            $@"
            DELETE	SPC_EXTERNALLIST WHERE	log != '' AND TABLENAME = 'SPC_VehicleTrans' "
            };
            return ConnectionClass.ExecuteSQL_ArrayMainAX(sql);
        }
        //Insert ExternalList Car Send AX
        public static string Save_EXTERNALLIST_CloseJOBCarExternalList(string pRecID, string pCarID, string pRepairID, string pDesc)//pRepairID  คือ อู่ซ่อม pDesc เลขที่ JOB วันที่
        {
            string recId = (DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")).Replace("-", "").ToString().Replace(":", "").Replace(" ", "");

            ArrayList sql = new ArrayList {
            $@"
            INSERT INTO SPC_EXTERNALLIST 
                    (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID)  
            VALUES ('SPC_VehicleTrans','RECID|DATAAREAID','{pRecID}|SPC','EmplApprove|EmplComplete|RepairShop|REMARK',
                    '{SystemClass.SystemUserID}|{SystemClass.SystemUserID}|{pRepairID}|{pDesc}' ,CONVERT(NVARCHAR,'UPDATE'),'SPC','1',CONVERT(BIGINT,{recId}) )",
            $@"
            INSERT INTO SPC_EXTERNALLIST 
                    (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID)  
            VALUES  ('SPC_VEHICLETABLE','VEHICLEID','{pCarID}','VEHICLESTATUS','0',CONVERT(NVARCHAR,'UPDATE'),'SPC','1',CONVERT(BIGINT,{recId}1) ) "
            };
            return ConnectionClass.ExecuteSQL_ArrayMainAX(sql);
        }
        //ส่งข้อมูลเข้า SPC_EXTERNALLIST SPC_SHIPMENTINVOICE
        public static string Save_EXTERNALLIST_SPC_SHIPMENTINVOICE(string pDocno, string pDate, string pBchID, string pBchName)
        {
            //string sqlIn = string.Format(@"INSERT INTO SPC_EXTERNALLIST (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID) 
            //            VALUES ('SPC_SHIPMENTINVOICE','LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID','1|10|" + pDocno + @"|SPC',
            //            'LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT',
            //            '1|10|" + pDate + @"|" + pDocno + @"|" + SystemClass.SystemUserID + @"|" + pBchName + @"|" + pDate + @"|" + pDate + @"|" + pBchID + @"',
            //            CONVERT(NVARCHAR,'CREATE'),'SPC','1',CONVERT(BIGINT," + pDocno.Substring(4, 12) + @"1) )
            //            ");
            //inStrAX.Add(AX_SendData.Save_EXTERNALLIST("CONVERT(NVARCHAR,'CREATE')", "SPC_SHIPMENTINVOICE",
            //        "LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID",
            //        $@"1|10|{dtDataDT.Rows[i]["DOCNO"]}|SPC",
            //        "LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT",
            //        $@"'1|10|{dtDataDT.Rows[i]["DATE"]}|{dtDataDT.Rows[i]["DOCNO"]}|{SystemClass.SystemUserID}|{bchName}|{dtDataDT.Rows[i]["DATE"]}|{dtDataDT.Rows[i]["DATE"]}|{dtDataDT.Rows[i]["BRANCH_ID"]}", $@"CONVERT(BIGINT,{dtDataDT.Rows[i]["DOCNO"].ToString().Substring(4, 12)}1)"));

            string recID = (DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")).Replace("-", "").ToString().Replace(":", "").Replace(" ", ""); ;
            // if (pCase == "0") recID = $@"{pDocno.Substring(4, 12)}1";

            string sqlIn = AX_SendData.Save_EXTERNALLIST("CREATE", "SPC_SHIPMENTINVOICE",
                "LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID",
                $@"1|10|{pDocno}|SPC",
                "LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT",
                $@"1|10|{pDate}|{pDocno}|{SystemClass.SystemUserID}|{pBchName}|{pDate}|{pDate}|{pBchID}", recID);

            return sqlIn;
            //return ConnectionClass.ExecuteSQL_MainAX(sqlIn);
        }
        //เิปดปิดโปรโมชั้น
        public static string Save_EXTERNALLIST_SPC_RetailPromotionLocation(string methodName, string pProID, string pBch)
        {
            string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "");
            return AX_SendData.Save_EXTERNALLIST(methodName, "SPC_RetailPromotionLocation",
                       "PROMID|INVENTLOCATIONID",
                       $@"{pProID}|{pBch}",
                       "PROMID|INVENTLOCATIONID|USECONFIRM",
                       $@"{pProID}|{pBch}|1", recID);
        }
        //Check 
        public static int EXTERNALLIST_Check(string tblName, string pK)
        {
            string sql_CheckLog708 = $@"
            select  * 
            from    SPC_EXTERNALLIST WITH (NOLOCK)
            where	TABLENAME = '{tblName}'  AND CONVERT(NVARCHAR,PKFIELDVALUE) = N'{pK}' AND LOG = '' 
            ";
            return ConnectionClass.SelectSQL_MainAX(sql_CheckLog708).Rows.Count;
        }

        //ส่งข้อมูลบิลเบิกเข้า AX SPC_INVENTJOURNALTABLE ใบเบิกสินค้า
        public static ArrayList Save_SPC_INVENTJOURNALTABLE(DataTable dtDataDT)
        {
            ArrayList inStrAX = new ArrayList();
            string mnrs = string.Empty;
            int seqNo = 0;
            int mndocno = 0;

            for (int i = 0; i < dtDataDT.Rows.Count; i++)
            {
                if (mnrs != dtDataDT.Rows[i]["GROUPMAIN"].ToString())
                {
                    mnrs = dtDataDT.Rows[i]["GROUPMAIN"].ToString();
                    seqNo = 0;
                    mndocno += 1;
                    inStrAX.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTABLE 
                                (DATAAREAID,RECVERSION,RECID,JOURNALID,DESCRIPTION,TRANSDATE,JOURNALTYPE,
                                EMPLID,REMARKS,INVENTLOCATIONIDTO,INVENTLOCATIONIDFROM,INVENTCOSTCENTERID,
                                JOBID,REFJOURNALID,DIMENSION, DIMENSION2_,DIMENSION3_,PICKEDBYWORKER) 
                    VALUES ( 'SPC','1','1',
                            '{dtDataDT.Rows[i]["DOCNO"]}-{Convert.ToString(mndocno)}',
                            'สมุดเบิกสินค้า-{dtDataDT.Rows[i]["BRANCH_ID"]}',
                            '{dtDataDT.Rows[i]["DATE"]}',
                            '0','{SystemClass.SystemUserID_M}',
                            '{dtDataDT.Rows[i]["REMARK"]}','',
                            '{dtDataDT.Rows[i]["INVENT"]}',
                            '{dtDataDT.Rows[i]["GROUPMAIN"]}','','',
                            '{dtDataDT.Rows[i]["BRANCH_ID"]}','','','{dtDataDT.Rows[i]["WHOIDPACKING"]}'  )");

                    inStrAX.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTRANS (DATAAREAID,RECVERSION,RECID,
                            JOURNALID,LINENUM,TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY,INVENTTRANSID, 
                            INVENTSERIALID,INVENTBATCHID,COSTPRICE) 
                    VALUES ('SPC','1','1',
                        '{dtDataDT.Rows[i]["DOCNO"]}-{Convert.ToString(mndocno)}','{seqNo + 1}',
                        '{dtDataDT.Rows[i]["DATE"]}',
                        '{dtDataDT.Rows[i]["ITEMBARCODE"]}',
                        '{dtDataDT.Rows[i]["ITEMID"]}',
                        '{dtDataDT.Rows[i]["INVENTDIMID"]}',
                        '{Convert.ToDouble(dtDataDT.Rows[i]["QTY"].ToString()) * (-1)}',
                        '{dtDataDT.Rows[i]["DOCNO"]}{Convert.ToString(i + 1)}','','',0 )");
                    seqNo += 1;
                }
                else
                {
                    inStrAX.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTRANS (DATAAREAID,RECVERSION,RECID,
                            JOURNALID,LINENUM,TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY,INVENTTRANSID, 
                            INVENTSERIALID,INVENTBATCHID,COSTPRICE) VALUES ('SPC','1','1',
                    '{dtDataDT.Rows[i]["DOCNO"]}-{Convert.ToString(mndocno)}','{seqNo + 1}',
                    '{dtDataDT.Rows[i]["DATE"]}',
                    '{dtDataDT.Rows[i]["ITEMBARCODE"]}',
                    '{dtDataDT.Rows[i]["ITEMID"]}',
                    '{dtDataDT.Rows[i]["INVENTDIMID"]} ',
                    '{Convert.ToDouble(dtDataDT.Rows[i]["QTY"].ToString()) * (-1)}',
                    '{dtDataDT.Rows[i]["DOCNO"]}{Convert.ToString(i + 1)}','','','0' )");
                    seqNo += 1;
                }
            }

            return inStrAX;
        }
        //บิลเบิกสินค้า
        public static ArrayList Save_SPC_INVENTJOURNALTABLE_FixIS(DataTable dtData_Rows)
        {
            ArrayList InStrAX = new ArrayList
            {
                $@"INSERT INTO SPC_INVENTJOURNALTABLE
                       (DATAAREAID,RECVERSION,RECID,JOURNALID,DESCRIPTION
                       ,TRANSDATE,JOURNALTYPE,EMPLID,REMARKS,INVENTLOCATIONIDTO
                       ,INVENTLOCATIONIDFROM,LOG,INVENTCOSTCENTERID,JOBID
                       ,REFJOURNALID,DIMENSION ,DIMENSION2_,DIMENSION3_,MANUALPOSTED) 
                       VALUES 
                            ('SPC','1','1','{ dtData_Rows.Rows[0]["DOCNO"]}','สมุดเบิกสินค้า-{dtData_Rows.Rows[0]["DEPT"]}'
                            ,'{dtData_Rows.Rows[0]["DATECREATE"]}','0','{SystemClass.SystemUserID_M}','{dtData_Rows.Rows[0]["DESCRIPTION"]}','{ dtData_Rows.Rows[0]["DEPT"]}'
                            ,'MN998','','{dtData_Rows.Rows[0]["COSTCENTERID"]}',''
                            ,'','{dtData_Rows.Rows[0]["DEPT"]}','','','1')"
            };

            for (int i = 0; i < dtData_Rows.Rows.Count; i++)
            {
                if (double.Parse(dtData_Rows.Rows[i]["QTYEDIT"].ToString()) > 0)
                {
                    InStrAX.Add($@"INSERT INTO SPC_INVENTJOURNALTRANS
                        (DATAAREAID,RECVERSION,RECID,JOURNALID,LINENUM
                        ,TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY
                        ,INVENTTRANSID,INVENTSERIALID,INVENTBATCHID,COSTPRICE)
                    VALUES(
                        'SPC','1','1','{dtData_Rows.Rows[i]["DOCNO"]}','{i + 1}'
                        ,'{dtData_Rows.Rows[i]["DATECREATE"]}','{dtData_Rows.Rows[i]["ITEMBARCODE"]}','{dtData_Rows.Rows[i]["ITEMID"]}','{dtData_Rows.Rows[i]["ITEMDIM"]}',
                        '{double.Parse(dtData_Rows.Rows[i]["QTYEDIT"].ToString()) * (-1)}'
                        ,'{dtData_Rows.Rows[i]["DOCNO"]}_{i + 1}','','','0') ");
                }
            }
            return InStrAX;
        }
        //IM-TF-WH 
        public static string Save_SPC_INVENTJOURNALTABLE_TYPE2(DataTable dtHD, DataTable dtDT)
        {
            ArrayList sql24 = new ArrayList();
            ArrayList sqlAx = new ArrayList();

            for (int i = 0; i < dtHD.Rows.Count; i++)
            {
                string docNo = ConfigClass.GetMaxINVOICEID("MNAF", "-", "MNAF", "1");
                string poRmkHD = "";
                if (dtHD.Rows[i]["PO"].ToString() != "") poRmkHD = $@"{dtHD.Rows[i]["PO"]}-";

                string vender = $@"{poRmkHD}{dtHD.Rows[i]["VENDID"]}-{dtHD.Rows[i]["VENDNAME"]}";
                if (vender.Length > 60) vender = vender.Substring(0, 60);
                sqlAx.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTABLE  
                            (DATAAREAID,RECVERSION,RECID,JOURNALID,
                             DESCRIPTION,TRANSDATE,JOURNALTYPE,EMPLID, 
                             INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM,DLVMODE,REFJOURNALID,PickedByWorker) 
                    VALUES  ('SPC','1','1','{docNo}',
                            '{vender}',GETDATE(),'2','{SystemClass.SystemUserID_M}',
                            '{dtHD.Rows[i]["INVENTLOCATIONTO"]}','{dtHD.Rows[i]["INVENTLOCATIONFROM"]}','{dtHD.Rows[i]["VENDID"]}','{dtHD.Rows[i]["PO"]}','{dtHD.Rows[i]["EMPLIDRECIVE"]}')
                ");
                //check DT
                DataRow[] drItem = dtDT.Select($@" INVENTLOCATIONFROM = '{dtHD.Rows[i]["INVENTLOCATIONFROM"]}' 
                                                    AND INVENTLOCATIONTO = '{dtHD.Rows[i]["INVENTLOCATIONTO"]}' 
                                                    AND VENDID = '{dtHD.Rows[i]["VENDID"]}' 
                                                    AND BUYERID = '{dtHD.Rows[i]["BUYERID"]}'
                                                    AND PO = '{dtHD.Rows[i]["PO"]}'
                                                    AND EMPLIDRECIVE = '{dtHD.Rows[i]["EMPLIDRECIVE"]}' ");

                for (int iT = 0; iT < drItem.Length; iT++)
                {
                    string transID = docNo + "_" + (iT + 1).ToString();
                    string rmk = $@"ผู้รับ {drItem[iT]["EMPLIDRECIVE"]}-{drItem[iT]["EMPLNAMERECIVE"]} {drItem[iT]["REMARK"]}";
                    if (rmk.Length > 200) rmk = rmk.Substring(0, 200);
                    //REMARKS -- 'ผู้รับ {drItem[iT]["EMPLIDRECIVE"]}-{drItem[iT]["EMPLNAMERECIVE"]} {drItem[iT]["REMARK"]}'
                    sqlAx.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTRANS 
                            (RECID,RECVERSION,DATAAREAID,JOURNALID,
                            TRANSDATE,LINENUM,QTY,ITEMID,
                            ITEMBARCODE,INVENTDIMID,INVENTTRANSID,INVENTTRANSIDFATHER,INVENTONHAND,REMARKS
                            ) 
                    VALUES  ('1','1','SPC','{docNo}',
                            GETDATE(),'{iT + 1}','{drItem[iT]["QTY"]}','{drItem[iT]["ITEMID"]}',
                            '{drItem[iT]["ITEMBARCODE"]}','{drItem[iT]["INVENTDIMID"]}','{transID}','{drItem[iT]["INVENTTRANSID"]}','{drItem[iT]["INVENTONHAND"]}','{rmk}'
                            )
                    ");

                    sql24.Add($@"
                        UPDATE	SupcAndroid.dbo.[ProductRecive_DROID]
                        SET		SENDAXSTA = '{docNo}',EMPLIDSENDAX = '{SystemClass.SystemUserID_M}',EMPLNAMESENDAX = '{SystemClass.SystemUserName}',DATETIMESENDAX = GETDATE()
                        WHERE	[TRANSID] = '{drItem[iT]["TRANSID"]}' ");

                    sql24.Add($@"
                        UPDATE	SupcAndroid.dbo.[ProductRecive_LOGPO]
                        SET		STA = '1',AXDOCNO = '{docNo}',EMPLIDUPDATEAX = '{SystemClass.SystemUserID_M}',EMPLNAMEUPDATEAX = '{SystemClass.SystemUserName}',DATETIMEUPDATEAX = GETDATE()
                        WHERE	[TRANSID] = '{drItem[iT]["TRANSID"]}' ");

                    if (drItem[iT]["INVENTONHAND"].ToString() != "1")
                    {
                        //sql24.Add($@"
                        //UPDATE	SupcAndroid.dbo.[ProductRecive_DROID]
                        //SET		SENDAXSTA = '{docNo}',EMPLIDSENDAX = '{SystemClass.SystemUserID_M}',EMPLNAMESENDAX = '{SystemClass.SystemUserName}',DATETIMESENDAX = GETDATE()
                        //WHERE	[TRANSID] = '{drItem[iT]["TRANSID"]}' ");

                        //sql24.Add($@"
                        //UPDATE	SupcAndroid.dbo.[ProductRecive_LOGPO]
                        //SET		STA = '1',AXDOCNO = '{docNo}',EMPLIDUPDATEAX = '{SystemClass.SystemUserID_M}',EMPLNAMEUPDATEAX = '{SystemClass.SystemUserName}',DATETIMEUPDATEAX = GETDATE()
                        //WHERE	[TRANSID] = '{drItem[iT]["TRANSID"]}' ");
                    }
                    else
                    {

                        sql24.Add($@"
                            INSERT INTO SupcAndroid.[dbo].[ProductRecive_DROID]
                                       ([TRANSID],[DATETIMERECIVE],[PURCHID],[INVENTTRANSID]
                                       ,[INVENTLOCATIONFROM],[INVENTLOCATIONTO],[EMPLIDRECIVE],[EMPLNAMERECIVE],[DIMENSION],[DIMENSIONNAME]
                                       ,[DEVICENAME],[ITEMSTA]
                                       ,[VENDID],[VENDNAME],[BUYERID],[BUYERNAME]
		                               ,[ITEMID],[INVENTDIMID],[ITEMBARCODE],[SPCITEMNAME],[QTY],[UNITID]
                                       ,[QTYRECIVE]
                                       ,[REMARK]         
                                       ,[EMPLIDUPDATE],[EMPLNAMEUPDATE],[DATETIMEUPDATE]
                                       ,[SENDAXSTA],[EMPLIDSENDAX],[EMPLNAMESENDAX],[DATETIMESENDAX]
                                       )

                            SELECT		TRANSID+'_1_'+'{iT + 1}' AS [TRANSID],[DATETIMERECIVE],PURCHID,INVENTTRANSID 
                                       ,[INVENTLOCATIONFROM],[INVENTLOCATIONTO],[EMPLIDRECIVE],[EMPLNAMERECIVE],[DIMENSION],[DIMENSIONNAME]
                                       ,'{SystemClass.SystemPcName}' AS [DEVICENAME],'2' AS ITEMSTA
		                               ,[VENDID],[VENDNAME],BUYERID,BUYERNAME
                                       ,[ITEMID],[INVENTDIMID],[ITEMBARCODE],[SPCITEMNAME],[QTY],[UNITID]
                                       ,'{drItem[iT]["QTY"]}'*(-1) AS [QTYRECIVE]
                                       ,'เกิน PO' AS [REMARK]
		                               ,'{SystemClass.SystemUserID_M}' AS [EMPLIDUPDATE],'{SystemClass.SystemUserName}' AS [EMPLNAMEUPDATE],GETDATE() AS [DATETIMEUPDATE]
                                       ,'{docNo}' AS [SENDAXSTA],'{SystemClass.SystemUserID_M}' AS [EMPLIDSENDAX],'{SystemClass.SystemUserName}' AS [EMPLNAMESENDAX],GETDATE() AS [DATETIMESENDAX] 
                            FROM	SupcAndroid.dbo.ProductRecive_DROID WITH (NOLOCK)
                            WHERE	TRANSID = '{drItem[iT]["TRANSID"]}'
                            ");
                    };
                }
            }

            return ConnectionClass.Execute_SameTime_2Server(sql24, IpServerConnectClass.ConSelectMain, sqlAx, IpServerConnectClass.ConMainAX);
            //return ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlAx, IpServerConnectClass.ConDev);
        }
        //IM-TF-WH 
        public static string Save_SPC_INVENTJOURNALTABLE_TYPE2_Factory(string pDate, ArrayList sql24, string venderID)
        {
            //ArrayList sql24 = new ArrayList();
            ArrayList sqlAx = new ArrayList();

            DataTable dataHD = GeneralForm.ProductRecive.ProductRecive_Class.GetDatailFactory("4", pDate, venderID);
            DataTable dataDT = GeneralForm.ProductRecive.ProductRecive_Class.GetDatailFactory("5", pDate, venderID);

            for (int i = 0; i < dataHD.Rows.Count; i++)
            {
                string docNo = ConfigClass.GetMaxINVOICEID("MNAF", "-", "MNAF", "1");

                string vender = $@"{dataHD.Rows[i]["VENDID"]}-{dataHD.Rows[i]["VENDNAME"]}";
                if (vender.Length > 60) vender = vender.Substring(0, 60);

                sqlAx.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTABLE  
                            (DATAAREAID,RECVERSION,RECID,JOURNALID,
                             DESCRIPTION,TRANSDATE,JOURNALTYPE,EMPLID, 
                             INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM,DLVMODE,REFJOURNALID,PickedByWorker) 
                    VALUES  ('SPC','1','1','{docNo}',
                            '{vender}','{pDate}','2','{SystemClass.SystemUserID_M}',
                            '{dataHD.Rows[i]["INVENTLOCATIONTO"]}','{dataHD.Rows[i]["INVENTLOCATIONFROM"]}','{dataHD.Rows[i]["VENDID"]}','','{dataHD.Rows[i]["EMPLIDRECIVE"]}')
                ");
                //check DT
                DataRow[] drItem = dataDT.Select($@" INVENTLOCATIONFROM = '{dataHD.Rows[i]["INVENTLOCATIONFROM"]}' 
                                                    AND INVENTLOCATIONTO = '{dataHD.Rows[i]["INVENTLOCATIONTO"]}' 
                                                    AND VENDID = '{dataHD.Rows[i]["VENDID"]}' 
                                                    AND BUYERID = '{dataHD.Rows[i]["BUYERID"]}'
                                                    AND EMPLIDRECIVE = '{dataHD.Rows[i]["EMPLIDRECIVE"]}' ");

                for (int iT = 0; iT < drItem.Length; iT++)
                {
                    string transID = docNo + "_" + (iT + 1).ToString();
                    string rmk = $@"ผู้รับ {drItem[iT]["EMPLIDRECIVE"]}-{drItem[iT]["EMPLNAMERECIVE"]} {drItem[iT]["REMARK"]}";
                    if (rmk.Length > 200) rmk = rmk.Substring(0, 200);

                    sqlAx.Add($@"
                    INSERT INTO SPC_INVENTJOURNALTRANS 
                            (RECID,RECVERSION,DATAAREAID,JOURNALID,
                            TRANSDATE,LINENUM,QTY,ITEMID,
                            ITEMBARCODE,INVENTDIMID,INVENTTRANSID,INVENTTRANSIDFATHER,INVENTONHAND,REMARKS
                            ) 
                    VALUES  ('1','1','SPC','{docNo}',
                            {pDate},'{iT + 1}','{drItem[iT]["QTYRECIVE"]}','{drItem[iT]["ITEMID"]}',
                            '{drItem[iT]["ITEMBARCODE"]}','{drItem[iT]["INVENTDIMID"]}','{transID}','','1','{rmk}'
                            )
                    ");

                    sql24.Add($@"
                        UPDATE	SupcAndroid.dbo.[ProductRecive_DROID]
                        SET		SENDAXSTA = '{docNo}',EMPLIDSENDAX = '{SystemClass.SystemUserID_M}',EMPLNAMESENDAX = '{SystemClass.SystemUserName}',DATETIMESENDAX = GETDATE()
                        WHERE	[TRANSID] = '{drItem[iT]["TRANSID"]}' ");

                }
            }
            return ConnectionClass.Execute_SameTime_2Server(sql24, IpServerConnectClass.ConSelectMain, sqlAx, IpServerConnectClass.ConMainAX);
        }

        //Insert Data Container
        public static string Save_WHSContainer(string conID, string worker, string docnoMNPT)
        {
            string sqlAX = $@" INSERT INTO WHSContainer_SPC (ContainerId,ContainerTypeCode,WorkerId,OrderNum) VALUES ('{conID}','1','{worker}','{docnoMNPT}') ";
            return ConnectionClass.ExecuteSQL_MainAX(sqlAX);
        }

        ////save AX สำหรับจัดของสาขาใหญ่
        //public static string SentMNPOAX_FixInvent(string pCase, string docno, string invent = "", string bchID = "", string bchName = "", string emplID = "")
        //{
        //    string sql;
        //    if (pCase == "HD")
        //    {
        //        sql = $@"
        //            insert into SPC_POSTOTABLE10 
        //                (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
        //                INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
        //            values('98','{emplID}','0','SPC','1','1','0',
        //                '{bchID}', '{invent}','{docno}',GETDATE(),'{docno}', '','มินิมาร์ทจัดสินค้า-{bchName}' )  ";
        //    }
        //    else
        //    {
        //        sql = $@"
        //            insert into SPC_POSTOLINE10 
        //                        (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,
        //                        ITEMID,NAME,SALESUNIT,ITEMBARCODE, INVENTDIMID,INVENTTRANSID,RECVERSION) 
        //            values('1','0','SPC','{docno}',GETDATE(),'1','1','0',
        //                        'P0084197','มินิมาร์ทจัดสินค้า','ชิ้น','MN0002','DIM13-000566071','{docno}-1','1') ";
        //    }
        //    return sql;
        //}

        public static ArrayList SAVE_SPC_POSWSTABLE20_MNEC(string INVOICEDate, string docno, DataTable DtCoins)
        {
            ArrayList sqlAX = new ArrayList
            {
                $@" INSERT INTO SPC_POSWSTABLE20( 
                                    INVOICEACCOUNT,INVODATE,
                                    PRINTTAXINVOICE,SALESORIGINID,
                                    CUSTACCOUNT,SALESID,
                                    INVOICEDATE,INVOICEID,
                                    TAXINVOICE,DLVMODE,
                                    DLVTERM,DLVREASON,
                                    PAYMENT,PRICEGROUPID,LINEDISC,
                                    MULTILINEDISC,ENDDISC,DISCPERCENT,
                                    PURCHORDERFORMNUM,SALESTAKER,
                                    SALESSPN,DELIVERYNAME,
                                    DELIVERYADDRESS,EMPLCHECKER,
                                    EMPLPICK,SHIPPINGDATEREQUESTED,UPDATE2SALESSTATUS,SALESTYPE,
                                    CASHIERID,REMARKS,
                                    INVENTSITEID, INVENTLOCATIONID, 
                                    DATAAREAID, RECVERSION,
                                    RECID,NAME,
                                    SALESPOOLID,DEPARTMENT,
                                    LINEPRICECALC,EmplARResponsible)
                            VALUES (
                                    'C063553','0',
                                    '0','51',
                                    'C063553','{docno}',
                                    '{INVOICEDate}','{docno}',
                                    '0','Shipping',
                                    '','',
                                    'D15','CASH','0',
                                    '','','0',
                                    '','{SystemClass.SystemUserID_M}',
                                    '','',
                                    '','{SystemClass.SystemUserID_M}',
                                    '{SystemClass.SystemUserID_M}','{INVOICEDate}','1','3',
                                    '{SystemClass.SystemUserID_M}','',
                                    'SPC','',
                                    'SPC','1',
                                    '{docno.Replace("MNEC", "")}','ลูกค้าแลกเหรียญ',
                                    '{SystemClass.SystemUserID_M}','D051',
                                    '0',''
                            ) "
            };

            int iR = 1;
            for (int i = 0; i < DtCoins.Rows.Count; i++)
            {
                sqlAX.Add($@" INSERT INTO SPC_POSWSLINE20 (
                                INVOICEID,INVOICEDATE,LINENUM,
                                QTY,SALESPRICE,
                                LINEAMOUNT,ITEMID,
                                NAME,SALESUNIT,
                                ITEMBARCODE,INVENTSERIALID,
                                LINEDISC,LINEPERCENT,
                                SALESTYPE,SUPPITEMGROUPID,
                                ITEMSALESTAX,INVENTDIMID,
                                MULTILNDISC,MULTILNPERCENT,INVENTTRANSID,
                                SUPPITEMTABLERECID, DATAAREAID,
                             RECVERSION,RECID,COSTPRICE
                             ) 
                             VALUES (
                                '{docno}','{INVOICEDate}','{iR}',
                                '{DtCoins.Rows[i]["MNECQty"]}','{DtCoins.Rows[i]["SPC_PRICEGROUP3"]}',
                                '{DtCoins.Rows[i]["MNECQTYALL"]}','{DtCoins.Rows[i]["ITEMID"]}',
                                '{DtCoins.Rows[i]["SPC_ITEMNAME"]}','{DtCoins.Rows[i]["UNITID"]}',
                                '{DtCoins.Rows[i]["ITEMBARCODE"]}','',
                                '0.00','0.00',
                                '3','',
                                '0','{DtCoins.Rows[i]["INVENTDIMID"]}',
                                '0.00','0.00','{docno}_{iR}',
                                '0','SPC',
                                '1','{docno.Replace("MNEC", "")}','{DtCoins.Rows[i]["SPC_PRICEGROUP3"]}'
                             ) ");
                iR++;
            }

            return sqlAX;
        }

        public static ArrayList SAVE_SPC_TMSLOADINGTABLE_MNEC(string docno, string docnoRef)
        {
            //DOCUMENTTYPE (4) => 2,6,7 || DOCUMENTNUM ใบสั่งโอนย้าย
            DateTime now = DateTime.Now;


            ArrayList sqlAX = new ArrayList
            {
                $@"  INSERT	SPC_TMSLOADINGLINE
		                    (SHIPMENTID, SHIPMENTDATE, 
		                    DOCUMENTNUM, DOCUMENTTYPE, 
		                    DATAAREAID, RECVERSION, RECID)
                    VALUES	('{docno}','{now:yyyy-MM-dd}',
		                    '{docnoRef}','4',
		                    'SPC','1','{docno.Replace("MNEC", "")}')  ",
                $@" INSERT	SPC_TMSLOADINGPARMTABLE
		                    (SHIPMENTID, DATAAREAID, 
		                    RECVERSION, RECID)
                    VALUES	('{docno}','SPC',
		                    '1','{docno.Replace("MNEC", "")}'
                            ) ",
                $@"     INSERT	SPC_TMSLOADINGTABLE
		                        (SHIPMENTID, SHIPMENTDATE, 
		                        EMPLID, TRANSFERDATE, 
		                        ROUTEID, VEHICLEID, EMPLCHECKER,
		                        DIMENSION, DIMENSION2_, 
		                        DIMENSION3_, VEHICLECAPACITY, 
		                        EMPLLOADING, LOADINGDATETIME,
		                        LOADINGDATETIMETZID, EMPLLOADED, 
		                        LOADEDDATETIME,LOADEDDATETIMETZID, 
		                        LOG, LOADINGTYPE,
		                        PICKINGDATETIME, PICKINGDATETIMETZID, 
		                        PICKEDDATETIME, PICKEDDATETIMETZID, 
		                        DATAAREAID,RECVERSION, RECID)
                        VALUES	('{docno}','{now:yyyy-MM-dd HH:mm:ss}',
		                        'M0503137','{now:yyyy-MM-dd}',
		                        'DT','SPC','M0503137',
		                        'D051','',
		                        '','0',
		                        '{SystemClass.SystemUserID_M}','{now:yyyy-MM-dd}',
		                        '37001','{SystemClass.SystemUserID_M}',
		                        '{now:yyyy-MM-dd}','37001',
		                        '','0',
		                        '{now:yyyy-MM-dd}','37001',
		                        '1900-01-01 00:00:00.000','37001',
		                        'SPC','1','{docno.Replace("MNEC", "")}'
		                        ) "
            };


            return sqlAX;
        }

        public static string SAVE_SPC_PurchReceiveJourLine(DataTable dt)
        {
            ArrayList sqlAX = new ArrayList();
            ArrayList sql24 = new ArrayList();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sqlAX.Add($@"
                        IF NOT EXISTS  (SELECT	*	FROM	SPC_PURCHRECEIVEJOURLINE WITH (NOLOCK) WHERE INVENTTRANSID = '{dt.Rows[i]["InventTransId"]}' AND INVOICEID = '{dt.Rows[i]["InvoiceId"]}' AND DATAAREAID = N'SPC' )
                        BEGIN
	                        INSERT INTO SPC_PurchReceiveJourLine 
                                                        (InventTransId, LineNum, InvoiceId, PurchId,RECID,DATAAREAID,VENDACCOUNT ) 
                                                      VALUES ('{dt.Rows[i]["InventTransId"]}','{dt.Rows[i]["LineNum"]}','{dt.Rows[i]["InvoiceId"]}','{dt.Rows[i]["PurchId"]}',
                                                                '1','SPC','{dt.Rows[i]["VENDACCOUNT"]}')
                        END
                        ELSE
                        BEGIN
	                        UPDATE  SPC_PurchReceiveJourLine
                            SET     LineNum = '{dt.Rows[i]["LineNum"]}',PurchId = '{dt.Rows[i]["PurchId"]}',InvoiceId = '{dt.Rows[i]["InvoiceId"]}',VENDACCOUNT = '{dt.Rows[i]["VENDACCOUNT"]}'
                            WHERE   InventTransId = '{dt.Rows[i]["InventTransId"]}' AND DATAAREAID = N'SPC' AND INVOICEID = '{dt.Rows[i]["InvoiceId"]}' 
                        END
                ");

                //sqlAX.Add($@" INSERT INTO SPC_PurchReceiveJourLine 
                //                (InventTransId, LineNum, InvoiceId, PurchId,RECID,DATAAREAID,VENDACCOUNT ) 
                //              VALUES ('{dt.Rows[i]["InventTransId"]}','{dt.Rows[i]["LineNum"]}','{dt.Rows[i]["InvoiceId"]}','{dt.Rows[i]["PurchId"]}',
                //                        '1','SPC','{dt.Rows[i]["VENDACCOUNT"]}') ");

                sql24.Add($@"   UPDATE  SupcAndroid.[dbo].[ProductRecive_DROID]
                                SET     SENDLINENUM = '1',EMPLIDSENDLINENUM = '{SystemClass.SystemUserID}',EMPLNAMESENDLINENUM = '{SystemClass.SystemUserName}', DATETIMESENDLINENUM = GETDATE()
                                WHERE   TRANSID = '{dt.Rows[i]["TRANSID"]}' ");
            }
            return ConnectionClass.Execute_SameTime_2Server(sqlAX, IpServerConnectClass.ConMainAX, sql24, IpServerConnectClass.ConSelectMain);
        }
    }
}
