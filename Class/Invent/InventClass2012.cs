﻿using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.Class.Invent
{
    class InventClass2012 : IInventClassInterface
    {
        //ค้นหาคลังสินค้าที่เป็น SUPC
        //AX2012_OK
        DataTable IInventClassInterface.Find_InventSupc(string pCon) 
        {
            if (pCon == "Machine")
            {
                pCon = $@" UNION 
					SELECT	INVENTLOCATIONID,INVENTLOCATIONID+'-'+NAME AS INVENTLOCATIONNAME,NAME
                    FROM	SHOP2013TMP.dbo.INVENTLOCATION WITH (NOLOCK)   
					WHERE	INVENTLOCATIONID IN ('MN998','RONGMEE')	   ";
            }

            string sql = $@"
                    SELECT	INVENTLOCATIONID,INVENTLOCATIONID+'-'+NAME AS INVENTLOCATIONNAME,NAME
                    FROM	AX60CU13.dbo.INVENTLOCATIONV_SPC WITH (NOLOCK)   
                    WHERE	SPC_INVENTLOCATIONTYPE IN ('0','1')  
		                    AND DATAAREAID = N'SPC'	AND SPC_INVENTLOCATIONTYPE != '99' AND NAME NOT LIKE '%ยกเลิก%' 
                            AND INVENTLOCATIONID NOT IN ('COM-MN','Damage','RETURN-MN','WH-NAKA','WH-NAKA-RT','WH-C','MN997')
		                    {pCon}
                    ORDER BY INVENTLOCATIONID  DESC";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
