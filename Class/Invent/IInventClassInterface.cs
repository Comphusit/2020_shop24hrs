﻿using System.Data;

namespace PC_Shop24Hrs.Class.Invent
{
    interface IInventClassInterface
    {
        //ค้นหาคลังสินค้าที่เป็น SUPC
        DataTable Find_InventSupc(string pCon);
    }
}
