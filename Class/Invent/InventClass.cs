﻿using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.Class.Invent
{
    class InventClass : IInventClassInterface
    {
        //ค้นหาคลังสินค้าที่เป็น SUPC
        DataTable IInventClassInterface.Find_InventSupc(string pCon) 
        {
            if (pCon == "Machine")
            {
                pCon = $@" UNION 
					SELECT	INVENTLOCATIONID,INVENTLOCATIONID+'-'+NAME AS INVENTLOCATIONNAME,NAME
                    FROM	SHOP2013TMP.dbo.INVENTLOCATION WITH (NOLOCK)   
					WHERE	INVENTLOCATIONID IN ('MN998','RONGMEE','WH-KRABI')	   ";
            }
            string sql = $@"
                    SELECT	INVENTLOCATIONID,INVENTLOCATIONID+'-'+NAME AS INVENTLOCATIONNAME,NAME
                    FROM	SHOP2013TMP.dbo.INVENTLOCATION WITH (NOLOCK)   
                    WHERE	SPC_INVENTLOCATIONTYPE IN ('0','1')  
                            AND DATAAREAID = N'SPC'	AND SPC_INVENTLOCATIONTYPE != '99' AND NAME NOT LIKE '%ยกเลิก%' 
                            AND INVENTLOCATIONID NOT IN ('MN997','Damage','RETURN-MN','WH-NAKA','WH-NAKA-RT','WH-C','WH-KOKKLOY','WH-L','PREMIUM','WH-5','WH-FREE')
                            {pCon}
                    ORDER BY INVENTLOCATIONID ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
