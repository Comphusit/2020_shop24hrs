﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PC_Shop24Hrs.Controllers;
using System.Threading.Tasks;
using System.Collections;
using PC_Shop24Hrs.GeneralForm.MNCS;

namespace PC_Shop24Hrs.Class
{
    class CheckStockClass
    {
        //ค้นหา
        public static DataTable CheckStock_FindDataHD(string date1, string date2, string bch, string staApv, string _pPermiossion)
        {

            string conBch = "";
            if (bch != "") conBch = $@" AND INVENTLOCATIONID = '{bch}' ";
            else
            {
                switch (_pPermiossion)
                {
                    case "1": //สำหรับแผนก centershop
                        conBch = " AND INVENTLOCATIONID LIKE 'MN%' ";
                        break;//เบิกภายใน D
                    case "2":
                        conBch = " AND INVENTLOCATIONID NOT LIKE 'MN%' ";
                        break;
                    case "3"://เบิกภายใน MN
                        conBch = " AND INVENTLOCATIONID ( SELECT	BRANCH_ID FROM	SHOP_BRANCH WITH (NOLOCK) WHERE	BRANCH_STA IN ('4') ) ";
                        break;
                    default:
                        break;
                }
            }
            string conApv = "";
            if (staApv != "") conApv = @" AND CASE SHOP_COUNTING_HD.status WHEN '2' THEN '0' ELSE '1' END = '1' 
                                AND CASE SHOP_COUNTING_HD.status WHEN '1' THEN '1' ELSE '0' END = '0'  ";
            string sqlSelect = $@"
                SELECT  STOCKID, SUBSTRING(STOCKDescription,0,60) as STOCKDescription, INVENTLOCATIONID, EMPLChecker, JOURNALTYPE, NAMEChecker, 
		                CONVERT(VARCHAR,checkstockDate,23) AS checkstockDate, CONVERT(VARCHAR,createDate,25) AS createDate, SHOP_COUNTING_HD.status,  remark ,
		                CASE SHOP_COUNTING_HD.status WHEN '2' THEN '1' ELSE '0' END AS STADOC,
		                CASE SHOP_COUNTING_HD.status WHEN '1' THEN '1' ELSE '0' END AS STAPRCDOC,
                        DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,[ModifiedUser],[ModifiedName],CONVERT(VARCHAR,ModifiedDate,25) AS [ModifiedDate],REMARK ,
                        CASE ISNULL(STA_COUNT,'0') WHEN '0' THEN '1' ELSE '0' END AS STA_COUNT 
                FROM	SHOP_COUNTING_HD WITH(NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_COUNTING_HD.EMPLChecker = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
                WHERE	CONVERT(VARCHAR,checkstockDate,23) BETWEEN '{date1}' AND '{date2}' 
		                AND SHOP_COUNTING_HD.DATAAREAID = N'SPC' 
                        {conBch} {staApv} {conApv}
                ORDER BY createDate DESC
                ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //OK ลงรายการบัญชี
        public static string MNCS_ApvBill(string pCaseSave, string pDocno, string pEmpID, string pEmpName, DataRow rowHD)// 
        {
            int countLine = 0, LINENUM = 0, countTable = 0;
            string journalId = "";
            ArrayList sql708 = new ArrayList();
            ArrayList sqlMP = new ArrayList();

            foreach (DataRow row in CheckStock_Class.MNCS_FindDetailSendAXByDocno(pDocno).Rows)
            {
                /*หายอดคงเหลือล่าสุดในชุดมิติเดียวกัน*/
                double inventOnHand;
                if (pCaseSave == "0")
                {
                    inventOnHand = CheckStock_Class.MNCS_FindOnHandByDocno(pDocno,
                                            row["ITEMID"].ToString(),
                                            row["INVENTDIMID"].ToString(),
                                            row["INVENTSERIALID"].ToString(), row["INVENTBATCHID"].ToString());
                }
                else
                {
                    inventOnHand = ItembarcodeClass.FindStock_ByBarcode(row["ITEMBARCODE"].ToString(), rowHD["INVENTLOCATIONID"].ToString());
                }

                if (!(Convert.ToDouble(row["QTYUP"].ToString()) == 0 && inventOnHand == 0)) //สำหรับการนับที่เป็น 0 แล้วสต็อกก็เป็น 0 ข้ามไป
                {
                    countLine++;

                    if (countLine % 10 == 1)//10 line ขึ้นเอกสารใหม่
                    {
                        countTable++;
                        LINENUM = 0;
                        journalId = rowHD["stockID"].ToString() + "_" + countTable.ToString();

                        sql708.Add(@"INSERT INTO  SPC_INVENTJOURNALTABLE(DATAAREAID,RECVERSION,RECID,JOURNALID,DESCRIPTION,
                                        TRANSDATE,JOURNALTYPE,EMPLID,REMARKS,INVENTLOCATIONIDTO,
                                        INVENTLOCATIONIDFROM,REFJOURNALID) 
                                    VALUES('SPC',1,1,'" + journalId + @"','" + rowHD["stockDescription"].ToString() + @"' ,
                                        '" + rowHD["checkstockDate"].ToString() + @"','" + rowHD["JOURNALTYPE"].ToString() + @"',
                                        '" + rowHD["EMPLChecker"].ToString() + @"','" + rowHD["remark"].ToString() + @"','',
                                        '" + rowHD["INVENTLOCATIONID"].ToString() + @"','" + rowHD["stockID"].ToString() + @"')");
                    }

                    LINENUM++;
                    String INVENTTRANSID = journalId + "_" + LINENUM.ToString();
                    sql708.Add(@"INSERT INTO SPC_INVENTJOURNALTRANS(DATAAREAID,RECVERSION,RECID,JOURNALID,LINENUM,
                                    TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY,
                                    INVENTTRANSID,INVENTSERIALID,INVENTBATCHID, INVENTONHAND)  
                                VALUES('SPC',1,1,'" + journalId + @"','" + LINENUM + @"',
                                    '" + rowHD["checkstockDate"].ToString() + @"','" + row["ITEMBARCODE"].ToString() + @"',
                                    '" + row["ITEMID"].ToString() + @"','" + row["INVENTDIMID"].ToString() + @"','" + row["QTYUP"].ToString() + @"',
                                    '" + INVENTTRANSID + @"','" + row["INVENTSERIALID"].ToString() + @"','" + row["INVENTBATCHID"].ToString() + @"','" + inventOnHand + "')");
                }

            }

            sqlMP.Add($@"
                UPDATE  SHOP_COUNTING_HD 
                SET     status = '1',ModifiedUser = '{pEmpID}',ModifiedName = '{pEmpName}',ModifiedDate = GETDATE(),REMARK = REMARK + ' ลงรายการผ่าน SHOP24HRS'
                WHERE   STOCKID = '{pDocno}' ");

            return ConnectionClass.ExecuteMain_AX_24_SameTime(sqlMP, sql708);
        }
        //OK ใบนับสต็อกย้อนหลัง 180 ของแต่ละคลัง
        public static DataTable MNCS_JournalByWH(string caseID, string bchID)
        {
            string sql;
            if (caseID == "0")
            {
                sql = $@"
                    SELECT	CONVERT(VARCHAR,SPC_TRANSDATE,23) AS DisplayMember,CONVERT(VARCHAR,SPC_TRANSDATE,23) AS ValueMember
                    FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH(NOLOCK) 
                    WHERE	CONVERT(VARCHAR,SPC_TRANSDATE,23) BETWEEN GETDATE()-180 AND GETDATE() 
                            AND INVENTJOURNALTABLE.JOURNALTYPE = 4
                            {bchID}
                    GROUP BY CONVERT(VARCHAR,SPC_TRANSDATE,23)
                    ORDER BY CONVERT(VARCHAR,SPC_TRANSDATE,23)  DESC
                ";
            }
            else
            {
                //,
                sql = $@"
                    SELECT	SPC_REFJOURNALID AS ValueMember,SPC_REFJOURNALID + ' [' + SPC_InventLocationIdFrom + '] ' + CONVERT(VARCHAR,SPC_TRANSDATE,23) AS DisplayMember,
                            CONVERT(VARCHAR,SPC_TRANSDATE,23) AS TRANSDATE 
                    FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH(NOLOCK) 
                    WHERE	CONVERT(VARCHAR,SPC_TRANSDATE,23) BETWEEN GETDATE()-180 AND GETDATE() 
                            AND INVENTJOURNALTABLE.JOURNALTYPE = 4
                            AND SPC_REFJOURNALID != '' 
                            {bchID}
                    GROUP BY SPC_REFJOURNALID,CONVERT(VARCHAR,SPC_TRANSDATE,23),SPC_InventLocationIdFrom
                    ORDER BY SPC_REFJOURNALID DESC
                ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }

        //OK รายงานการนับสต็อก ตามใบนับสต็อก Report
        public static DataTable MNCS_ReportItemByJournalID(string bch, string date, string pPermission)
        {
            string transDate = $@" AND CONVERT(VARCHAR,SPC_TRANSDATE,23) = '{date}' ";
            string bchID = "";
            if (bch != "")
            {
                bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom = '{bch}' ";
            }
            else
            {
                switch (pPermission)
                {
                    case "0":
                        bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom = '{SystemClass.SystemBranchID}' ";
                        break;
                    case "1":
                        bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom LIKE 'MN%' ";
                        break;
                    case "2":
                        bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom NOT LIKE 'MN%' AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom NOT LIKE 'V%' ";
                        break;
                    default:
                        break;
                }
            }

            string sql = $@"
                SELECT	INVENTJOURNALTABLE.SPC_InventLocationIdFrom AS WH,ISNULL(INVENTLOCATION.NAME,'') AS WH_NAME,
		                INVENTJOURNALTABLE.JOURNALID,VOUCHER,INVENTJOURNALTRANS.ITEMID,SPC_ITEMBARCODE,INVENTJOURNALTRANS.SPC_ITEMNAME,
		                (INVENTONHAND/INVENTITEMBARCODE.QTY)AS INVENTONHAND,(COUNTED/INVENTITEMBARCODE.QTY)AS COUNTED,
		                ((INVENTONHAND/INVENTITEMBARCODE.QTY)-(COUNTED/INVENTITEMBARCODE.QTY))AS AMOUNT_OLD,
                        ((COUNTED/INVENTITEMBARCODE.QTY)-(INVENTONHAND/INVENTITEMBARCODE.QTY))AS AMOUNT,
		                ((INVENTONHAND/INVENTITEMBARCODE.QTY)-(COUNTED/INVENTITEMBARCODE.QTY))*SPC_PriceGroup3 AS pricetotal_OLD,
                        ((COUNTED/INVENTITEMBARCODE.QTY)-(INVENTONHAND/INVENTITEMBARCODE.QTY))*SPC_PriceGroup3 AS pricetotal,
		                COSTPRICE,INVENTJOURNALTRANS.INVENTDIMID,INVENTJOURNALTABLE.EMPLID,
		                INVENTITEMBARCODE.UNITID,INVENTITEMBARCODE.QTY,SPC_PriceGroup3,
		                INVENTTABLE.PRIMARYVENDORID,VENDTABLE.NAME AS VENNAME,
                        INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID AS BUYERID,DIMENSIONS.DESCRIPTION AS BUYERNAME,
                        DPT.NUM AS DPT, REPLACE(DPT.DESCRIPTION,'แผนกมินิมาร์ท 24 Hr. ','') AS DPTNAME

                FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH(NOLOCK) 
		                INNER JOIN SHOP2013TMP.dbo.INVENTJOURNALTRANS WITH(NOLOCK) ON INVENTJOURNALTABLE.DATAAREAID = INVENTJOURNALTRANS.DATAAREAID 
			                AND INVENTJOURNALTABLE.JOURNALID = INVENTJOURNALTRANS.JOURNALID 
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH(NOLOCK) ON  INVENTJOURNALTRANS.SPC_ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
			                AND INVENTJOURNALTRANS.DATAAREAID = INVENTITEMBARCODE.DATAAREAID
		                INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH(NOLOCK) ON INVENTITEMBARCODE.ITEMID = INVENTTABLE.ITEMID AND INVENTITEMBARCODE.DATAAREAID = INVENTTABLE.DATAAREAID 
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM AND DIMENSIONS.DIMENSIONCODE = '0'
		                INNER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON INVENTTABLE.PRIMARYVENDORID = VENDTABLE.ACCOUNTNUM 
                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTLOCATION WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_InventLocationIdFrom = INVENTLOCATION.INVENTLOCATIONID 
                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS DPT WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DPT.NUM AND DPT.DATAAREAID = N'SPC' AND DPT.DIMENSIONCODE = '0'

                WHERE INVENTJOURNALTABLE.DATAAREAID = 'SPC'
		                AND INVENTJOURNALTRANS.DATAAREAID = 'SPC'
		                AND INVENTITEMBARCODE.DATAAREAID = 'SPC'
		                AND DIMENSIONS.DATAAREAID = 'SPC'
		                AND VENDTABLE.DATAAREAID = 'SPC'
                        AND INVENTLOCATION.DATAAREAID = 'SPC' 
		                {bchID}
                        {transDate}
		                AND INVENTJOURNALTABLE.JOURNALTYPE = 4
                        AND INVENTJOURNALTABLE.JOURNALID LIKE 'MNCS%'

                ORDER BY INVENTJOURNALTABLE.SPC_InventLocationIdFrom,INVENTJOURNALTABLE.SPC_REFJOURNALID ,INVENTJOURNALTABLE.JOURNALID,
		                INVENTJOURNALTRANS.LINENUM,INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID,INVENTTABLE.PRIMARYVENDORID
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //OK รายงานเปรียบเทียบยอดขายกับจำนวนสต็อกคงเหลือ(มินิมาร์ท) Report
        public static DataTable MNCS_SPCN_SumInventCountingMNByVoucherByDate_FindJournalId(string docID)
        {
            string sql = $@"
                SELECT 	INVENTJOURNALTABLE.SPC_InventLocationIdFrom AS WH,ISNULL(INVENTLOCATION.NAME,'') AS WH_NAME,
                        INVENTJOURNALTRANS.JOURNALID,
		                CONVERT(VARCHAR,INVENTJOURNALTRANS.TRANSDATE,23) AS TRANSDATE,
		                INVENTJOURNALTRANS.DIMENSION,DIMENSIONS.DESCRIPTION AS DIMENSION_NAME,
		                VENDTABLE.ACCOUNTNUM  AS VENDID,VENDTABLE.NAME AS VENDNAME,
                        DPT.NUM AS DPT, REPLACE(DPT.DESCRIPTION,'แผนกมินิมาร์ท 24 Hr. ','') AS DPTNAME,
		                INVENTJOURNALTRANS.ITEMID,
		                INVENTDIM.CONFIGID,INVENTDIM.INVENTSIZEID,INVENTDIM.INVENTCOLORID,
		                INVENTJOURNALTRANS.SPC_ITEMBARCODE,
		                INVENTJOURNALTRANS.SPC_ITEMNAME,
		                INVENTJOURNALTRANS.SPC_ITEMBARCODEUNIT,UNITCONVERT.FACTOR,
		                INVENTJOURNALTRANS.COUNTED,
		                INVENTJOURNALTRANS.INVENTONHAND
		
                FROM	SHOP2013TMP.dbo.INVENTJOURNALTRANS WITH (NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK)  ON INVENTJOURNALTRANS.JOURNALID = INVENTJOURNALTABLE.JOURNALID
		                INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK) ON INVENTJOURNALTRANS.INVENTDIMID = INVENTDIM.INVENTDIMID	AND INVENTJOURNALTRANS.DATAAREAID = INVENTDIM.DATAAREAID 
		                INNER JOIN SHOP2013TMP.dbo.UNITCONVERT WITH (NOLOCK) ON INVENTJOURNALTRANS.SPC_ITEMBARCODEUNIT = UNITCONVERT.TOUNIT AND INVENTJOURNALTRANS.DATAAREAID = UNITCONVERT.DATAAREAID
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTRANS.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DIMENSIONCODE = '0'
		                INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON INVENTJOURNALTRANS.ITEMID = INVENTTABLE.ITEMID
		                INNER JOIN SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) ON INVENTTABLE.PRIMARYVENDORID = VENDTABLE.ACCOUNTNUM 
                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS DPT WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DPT.NUM AND DPT.DATAAREAID = N'SPC' AND DPT.DIMENSIONCODE = '0'
                        LEFT OUTER JOIN SHOP2013TMP.dbo.INVENTLOCATION WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_InventLocationIdFrom = INVENTLOCATION.INVENTLOCATIONID 

                WHERE	INVENTJOURNALTRANS.DATAAREAID = N'SPC'
                        AND INVENTJOURNALTABLE.DATAAREAID = N'SPC'
                        AND INVENTDIM.DATAAREAID = N'SPC' AND UNITCONVERT.DATAAREAID = N'SPC'
                        AND DIMENSIONS.DATAAREAID = N'SPC' AND INVENTTABLE.DATAAREAID = N'SPC' AND VENDTABLE.DATAAREAID = N'SPC'
                        AND INVENTLOCATION.DATAAREAID = N'SPC'
		                AND INVENTJOURNALTRANS.JOURNALTYPE = '4'
		                AND INVENTJOURNALTRANS.JOURNALID LIKE '{docID}%'
                
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //รายงานจำนวนรายการเช็คสต๊อค(ตามวันที่) 0 คือ เช็คคนที่มีการนับทั้งหมด 1 คือ รายละเอียดทั้งหมด
        public static DataTable MNCS_ReportCountItemByWhoID(string caseID, string dpt, string dateData, string stockID, string conAddOn)
        {
            string dptID = "";
            if (dpt != "") dptID = $@" AND EMPLTABLE.DIMENSION = '{dpt}' ";

            string sql = "";
            switch (caseID)
            {
                case "0":
                    sql = $@"
                            SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME
                            FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                            INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
                            WHERE	SHOP_COUNTING_HD.STATUS = 1
		                            {dptID}  {conAddOn}
                                    {dateData}
                                    {stockID}
                            GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME
                     ";
                    break;
                case "1":
                    sql = $@"
                        SELECT	EMPLID,SPC_NAME,DATECOUNTING,COUNT(*) AS COUNTCHECK
                        FROM	(
                        SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,
		                        CONVERT(VARCHAR,DATECOUNTING,23) AS DATECOUNTING,Barcode,INVENTBATCHID,INVENTSERIALID
                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
                        WHERE	SHOP_COUNTING_HD.STATUS = 1
		                        {dptID}  {conAddOn}
                                {dateData}
		                        {stockID}
                        GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,CONVERT(VARCHAR,DATECOUNTING,23),Barcode,INVENTBATCHID,INVENTSERIALID
		                        )TMP
                        GROUP BY EMPLID,SPC_NAME,DATECOUNTING
                        ORDER BY EMPLID,SPC_NAME,DATECOUNTING
                    ";
                    break;
                case "2":
                    sql = $@"
                        SELECT	EMPLID,SPC_NAME,TIMEHOUR,COUNT(*) AS COUNTCHECK
                        FROM	(
                        SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,
		                        DATEPART(HOUR, DATECOUNTING) AS TIMEHOUR,Barcode,INVENTBATCHID,INVENTSERIALID
                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
                        WHERE	SHOP_COUNTING_HD.STATUS = 1
		                        {dptID} {conAddOn}
                                {dateData}
		                        {stockID}
                        GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,DATEPART(HOUR, DATECOUNTING),Barcode,INVENTBATCHID,INVENTSERIALID
		                        )TMP
                        GROUP BY EMPLID,SPC_NAME,TIMEHOUR
                        ORDER BY EMPLID,SPC_NAME,TIMEHOUR
                    ";
                    break;
                case "3":
                    sql = $@"
                        SELECT	EMPLID,SPC_NAME,M_MONTH,COUNT(*) AS COUNTCHECK
                        FROM	(
                        SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,
		                        FORMAT(DATECOUNTING,'MM') AS M_MONTH,Barcode,ISNULL(INVENTBATCHID,'') AS INVENTBATCHID,ISNULL(INVENTSERIALID,'') AS INVENTSERIALID
                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
                        WHERE	SHOP_COUNTING_HD.STATUS = 1
		                        {dptID} {conAddOn}
		                        {dateData}
                        GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,FORMAT(DATECOUNTING,'MM'),Barcode,ISNULL(INVENTBATCHID,''),ISNULL(INVENTSERIALID,'')
		                        )TMP
                        GROUP BY EMPLID,SPC_NAME,M_MONTH
                        ORDER BY EMPLID,SPC_NAME,M_MONTH
                    ";
                    //AND DATEPART(YEAR, DATECOUNTING) = '2021'
                    break;
                case "4":
                    sql = $@"
                            SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS POSITION
                                    ,CASE ISNULL(SHOP_BRANCH.BRANCH_ID,'') WHEN '' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END AS NUM_DESC
                                    ,DIMENSIONS.NUM

                            FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                            INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
                                    LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)  
                                            ON EMPLTABLE.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID  = 'SPC'  
                                    LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK) 
                                            ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID and HRPPartyJobTableRelationship.DATAAREAID  = 'SPC'
                                    LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON EMPLTABLE.DIMENSION = SHOP_BRANCH.BRANCH_ID 
		                            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONS.DIMENSIONCODE = 0

                            WHERE	SHOP_COUNTING_HD.STATUS = 1
                                    AND ISNULL(DATEADD(HOUR, 7, HRPPARTYPOSITIONTABLERELAT2226.VALIDTODATETIME),'2020-01-01') >= CAST(GETDATE() AS DATE)  
		                            {dptID} {conAddOn}
                                    {dateData} 
                            GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'')
                                    ,CASE ISNULL(SHOP_BRANCH.BRANCH_ID,'') WHEN '' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END,DIMENSIONS.NUM
                            ORDER BY DIMENSIONS.NUM,SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME
                     ";
                    break;
                case "7":
                    sql = $@"
                        SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,EMPLTABLE.DIMENSION,BRANCH_NAME,
		                        ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS POSSITION,
		                        ISNULL(COUNTCHECK,'0') AS SUMLINE,ROUND((ISNULL(COUNTCHECK,'0')*0.4),0) AS SUMGRAND

                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
		                        LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)    ON EMPLTABLE.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE    AND  DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE) 
		                        LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID  
		                        INNER JOIN SHOP_BRANCH  WITH(NOLOCK)  ON  EMPLTABLE.DIMENSION =Shop_Branch .BRANCH_ID    
		                        LEFT OUTER JOIN 
		                        (
			                        SELECT	EMPLID,COUNT(*) AS COUNTCHECK
			                        FROM	(
			                        SELECT	SHOP_COUNTING_RC.EMPLID,
					                        Barcode,INVENTBATCHID,INVENTSERIALID
			                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
					                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
			                        WHERE	SHOP_COUNTING_HD.STATUS = 1  
					                         {dateData}
			                        GROUP BY SHOP_COUNTING_RC.EMPLID,Barcode,INVENTBATCHID,INVENTSERIALID
					                        )TMP
			                        GROUP BY EMPLID
		                        )COUNT_RC ON SHOP_COUNTING_RC.EMPLID = COUNT_RC.EMPLID

                        WHERE	SHOP_COUNTING_HD.STATUS = 1
		                        AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = N'SPC' AND HRPPartyJobTableRelationship.DATAAREAID = N'SPC'
		                        AND ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') LIKE '%เช็ค%'
		                         {dptID} {conAddOn}
                                 {dateData}
                        GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,
		                        ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,''),EMPLTABLE.DIMENSION,BRANCH_NAME,ISNULL(COUNTCHECK,'0')
                        ORDER BY EMPLTABLE.DIMENSION
                    ";
                    break;
                case "7_1"://เปลี่ยนเป็นไปใช้ 7
                    sql = $@"
                        SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,EMPLTABLE.DIMENSION,BRANCH_NAME,
		                        ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS POSSITION 
                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
		                        LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)    ON EMPLTABLE.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE    AND  DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE) 
		                        LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID  
		                        INNER JOIN SHOP_BRANCH  WITH(NOLOCK)  ON  EMPLTABLE.DIMENSION =Shop_Branch .BRANCH_ID    
                        WHERE	SHOP_COUNTING_HD.STATUS = 1
		                        AND HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID = N'SPC' AND HRPPartyJobTableRelationship.DATAAREAID = N'SPC'
		                        AND ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') LIKE '%เช็ค%'
		                        {dptID}  {conAddOn}
                                {dateData}
                        GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,''),EMPLTABLE.DIMENSION,BRANCH_NAME
                        ORDER BY EMPLTABLE.DIMENSION
                    ";
                    break;

                case "7_2"://เปลี่ยนเป็นไปใช้ 7
                    sql = $@"
                        SELECT	EMPLID,SPC_NAME,COUNT(*) AS COUNTCHECK
                        FROM	(
                        SELECT	SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,
		                        Barcode,INVENTBATCHID,INVENTSERIALID
                        FROM    SHOP_COUNTING_RC WITH(NOLOCK) 
		                        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK)	ON SHOP_COUNTING_RC.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                        INNER JOIN SHOP_COUNTING_HD WITH(NOLOCK) ON SHOP_COUNTING_RC.STOCKID = SHOP_COUNTING_HD.STOCKID
                        WHERE	SHOP_COUNTING_HD.STATUS = 1
                                {dateData} 
                        GROUP BY SHOP_COUNTING_RC.EMPLID,EMPLTABLE.SPC_NAME,Barcode,INVENTBATCHID,INVENTSERIALID
		                        )TMP
                        GROUP BY EMPLID,SPC_NAME
                        ORDER BY EMPLID,SPC_NAME
                    ";
                    break;
                default:
                    break;
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }

        //OK รายงาน ค้นหาสินค้าทั้งหมดในสาขา Report
        public static DataTable MNCS_FindItemInWH(int countMonth, string whID, string typeUnit, string buyer)
        {
            string pBuyerID = "";
            if (buyer != "") pBuyerID = $@" AND SPC_ITEMBUYERGROUPID = '{buyer}' ";

            string sql = $@"
                DECLARE @DateM int = { countMonth } --จำนวนเดือนยอดหลัง
                DECLARE @DateD int = @DateM * 30--จำนวนวันย้อนหลัง
                DECLARE @WH NVARCHAR(25) = '{whID}'--คลังสินค้า
                DECLARE @TypeUnit NVARCHAR(25) = '{typeUnit}'--MAX,MIN

                SELECT		INVENTITEMBARCODE_MINMAX.ITEMBARCODE,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID,
			                INVENTITEMBARCODE_MINMAX.UNITID,SPC_ITEMBUYERGROUPID AS DPT_ID,DIMENSIONS.DESCRIPTION AS DPT_NAME,
			                SPC_ITEMNAME,SPC_ITEMACTIVE,
			                INVENTITEMBARCODE_MINMAX.CONFIGID,INVENTITEMBARCODE_MINMAX.INVENTSIZEID,INVENTITEMBARCODE_MINMAX.INVENTCOLORID,
			                SPC_PRICEGROUP3,'' AS P_1,'' AS P_2,'' AS P_3,'' AS P_4,'' AS P_5
			                --,ISNULL(COUNTEDQTY,0) AS COUNTEDQTY,ISNULL(COUNTDATE,'-') AS COUNTDATE,ISNULL(AVAILPHYSICAL,0) AS AVAILPHYSICAL,
			                ---SUM(INVENTQTY) AS QTYSALE,SUM(INVENTQTY) / @DateM AS AVGM,
			                --(ISNULL(AVAILPHYSICAL,0) / (SUM(INVENTQTY) / @DateM)) AS SALEEND
                FROM		(
			                SELECT		ISNULL(T1.ITEMID,T2.ITEMID) AS ITEMID,ISNULL(T1.CONFIGID,T2.CONFIGID) AS CONFIGID,
						                ISNULL(T1.INVENTSIZEID,T2.INVENTSIZEID) AS INVENTSIZEID,ISNULL(T1.INVENTCOLORID,T2.INVENTCOLORID) AS INVENTCOLORID
			                FROM		(
							                SELECT	ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID
							                FROM	SHOP2013TMP.dbo.SPC_BARCODEBYBRANCH WITH (NOLOCK) 
							                WHERE	YEAR BETWEEN YEAR(GETDATE()-@DateD) AND YEAR(GETDATE()) AND BRANCH = @WH
							                GROUP BY ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID
						                )T1 FULL OUTER JOIN 
						                (
							                SELECT	ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID 
							                FROM	SPC705SRV.[Staging].dbo.PT_PURCHASE_TRANSFERSHIP 	WITH (NOLOCK)    
							                WHERE	TRANSTYPE = '6'
									                AND TimePeriod BETWEEN GETDATE()-@DateD AND GETDATE()
									                AND INVENTSITEID = 'SPC' AND BRANCH = @WH
							                GROUP BY ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID
						                )T2 ON T1.ITEMID = T2.ITEMID   COLLATE database_default
							                AND T1.CONFIGID = T2.CONFIGID   COLLATE database_default
							                AND T1.INVENTSIZEID = T2.INVENTSIZEID   COLLATE database_default
							                AND T1.INVENTCOLORID = T2.INVENTCOLORID COLLATE DATABASE_DEFAULT
			                )SPC_BARCODEBYBRANCH 
			
			                INNER JOIN SPC705SRV.[Staging].dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK)  
				                ON SPC_BARCODEBYBRANCH.ITEMID = INVENTITEMBARCODE_MINMAX.ITEMID COLLATE database_default
				                AND SPC_BARCODEBYBRANCH.CONFIGID = INVENTITEMBARCODE_MINMAX.CONFIGID COLLATE database_default
				                AND SPC_BARCODEBYBRANCH.INVENTSIZEID = INVENTITEMBARCODE_MINMAX.INVENTSIZEID COLLATE database_default
				                AND SPC_BARCODEBYBRANCH.INVENTCOLORID = INVENTITEMBARCODE_MINMAX.INVENTCOLORID COLLATE database_default
				                AND INVENTITEMBARCODE_MINMAX.TYPE_ = @TypeUnit AND INVENTITEMBARCODE_MINMAX.DATAAREAID = N'SPC'
                                {pBuyerID}
			                LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS	WITH (NOLOCK) ON INVENTITEMBARCODE_MINMAX.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM  COLLATE database_default
				                AND DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC' 
                ORDER BY 	SPC_ITEMBUYERGROUPID,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.ITEMBARCODE		               

            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ประวัติการนับสต็อกย้อนหลัง 5 ครั้ง
        public static DataTable MNCS_FindHistory(string whID, string buyer)
        {
            string sql = $@"
            SELECT	*
            FROM	(
		            SELECT	*,
				            CAST(ROUND(CASE WHEN QTY_BALANCE = 0 THEN '100' ELSE CASE  WHEN (C_DIFF) != 0 THEN ((C_DIFF)*100)/QTY_BALANCE ELSE '0.00' END END,2) AS numeric(36,2)) AS CPercent,
				            ROW_NUMBER() OVER (PARTITION BY ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID, INVENTDIMID
					            ORDER BY STOCKID DESC) AS ROWNUMBER 
		            FROM	(
				            SELECT  CONVERT(VARCHAR,checkstockDate,23) AS TRANSDATE,SHOP_COUNTING_DT.STOCKID,SHOP_COUNTING_DT.ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID, INVENTDIMID,QTY_BALANCE,
						            SUM((QTY_Count_front + QTY_Count_back) * QTY) AS QTY_COUNT,SUM((QTY_Count_front + QTY_Count_back) * QTY) - QTY_BALANCE AS C_DIFF

				            FROM	SHOP_COUNTING_DT with(nolock) 
						            INNER JOIN SHOP_COUNTING_HD WITH (NOLOCK) ON SHOP_COUNTING_DT.STOCKID = SHOP_COUNTING_HD.STOCKID
                                    INNER JOIN SHOP2013TMP.dbo.INVENTTABLE  WITH (NOLOCK) ON SHOP_COUNTING_DT.ITEMID = INVENTTABLE.ITEMID   AND INVENTTABLE.DATAAREAID = N'SPC' 

				            WHERE	INVENTLOCATIONID = '{whID}' {buyer}

				            GROUP BY CONVERT(VARCHAR,checkstockDate,23),SHOP_COUNTING_DT.STOCKID,SHOP_COUNTING_DT.ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID, INVENTDIMID,QTY_BALANCE
		            )DETAIL
            )SUMMARY
            WHERE ROWNUMBER < 6
            ORDER BY ROWNUMBER,ITEMID,CONFIGID,INVENTSIZEID,INVENTCOLORID, INVENTDIMID,STOCKID
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //รายงานรายละเอียดการนับสต๊อก ตามพนักงาน
        public static DataTable MNCS_RC(string date1, string date2)
        {
            string sql = $@"
                SELECT	 [SHOP_COUNTING_HD].[STOCKDescription] AS STOCKDescription
		                ,[SHOP_COUNTING_HD].[INVENTLOCATIONID] AS INVENTLOCATIONID
		                ,[SHOP_COUNTING_RC].[EmplID] AS EmplID
		                ,[EMPLTABLE].[SPC_NAME] AS SPC_NAME
		                ,[SHOP_COUNTING_RC].[StockID] AS StockID
		                ,[SHOP_COUNTING_RC].[Barcode] AS Barcode
		                ,[INVENTITEMBARCODE].[SPC_ITEMNAME] AS SPC_ITEMNAME 
		                ,[SHOP_COUNTING_RC].[QTYCounting] AS QTYCounting 
		                ,[SHOP_COUNTING_RC].[Unit] AS Unit
		                ,[SHOP_COUNTING_RC].[CountType] AS CountType
		                ,[SHOP_COUNTING_RC].[CountDesc] AS CountDesc
		                ,[SHOP_COUNTING_RC].[DATECounting] AS DATECounting
                FROM	[SHOP24HRS].[dbo].[SHOP_COUNTING_RC]
		                INNER JOIN [SHOP24HRS].[dbo].[SHOP_COUNTING_HD] WITH(NOLOCK)	ON [SHOP_COUNTING_RC].[StockID] = [SHOP_COUNTING_HD].[STOCKID]
		                INNER JOIN [SHOP2013TMP].[dbo].[EMPLTABLE] WITH(NOLOCK)	ON [SHOP_COUNTING_RC].[EmplID] = [EMPLTABLE].[EMPLID]
		                INNER JOIN [SHOP2013TMP].[dbo].[INVENTITEMBARCODE] WITH(NOLOCK)	ON [SHOP_COUNTING_RC].[Barcode] = [INVENTITEMBARCODE].[ITEMBARCODE]
                WHERE	[SHOP_COUNTING_HD].DATAAREAID ='spc'
		                AND [EMPLTABLE].DATAAREAID ='spc'
		                AND [INVENTITEMBARCODE].DATAAREAID ='spc'
		                AND [INVENTLOCATIONID] NOT LIKE 'MN%'
                        --AND STOCKDescription NOT LIKE 'ปรับสต็อกสินค้าจาก Excel%'
		                AND CONVERT(VARCHAR,DATECounting,23) BETWEEN '{date1}' AND '{date2}'
                ORDER BY EmplID,StockID,Barcode,DATECounting
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static string MNCS_InsertDel(string StockID, string ItemID, string InventdimID)
        {
            ArrayList sql = new ArrayList() {
                                $@"INSERT INTO [dbo].[SHOP_COUNTING_DEL]([STOCKID], [STOCKDescription] ,[INVENTLOCATIONID],[EMPLChecker],[NAMEChecker],
                                                                   [ITEMBARCODE], [SPC_ITEMNAME], [QTY_Count_front], [QTY_Count_back], [QTY_Balance],
                                                                   [QTY_Sum], [UNITID], [INVENTBATCHID], [INVENTSERIALID], [LastCheckDate],
                                                                   [LastQTY], [LastCountDate], [WHOINS], [WHONAMEINS],
                                                                   [QTYCounting],[CountType],[CountDesc])
    
                                SELECT SHOP_COUNTING_DT.[STOCKID], [STOCKDescription], [INVENTLOCATIONID], SHOP_COUNTING_RC.EmplID, [EMPLTABLE].SPC_NAME,
                                                         [ITEMBARCODE], [SPC_ITEMNAME], [QTY_Count_front], [QTY_Count_back], [QTY_Balance],
                                                         [QTY_Sum], [UNITID], SHOP_COUNTING_DT.[INVENTBATCHID], SHOP_COUNTING_DT.[INVENTSERIALID], [LastCheckDate],
                                                         [LastQTY], SHOP_COUNTING_RC.DATECounting AS LastCountDate,' {SystemClass.SystemUserID_M}' AS WHOINS ,'{SystemClass.SystemUserName}' AS WHONAMEINS,
                                                         SHOP_COUNTING_RC.QTYCounting, SHOP_COUNTING_RC.CountType, SHOP_COUNTING_RC.CountDesc
                                FROM SHOP_COUNTING_DT WITH (NOLOCK)
                                  INNER JOIN SHOP_COUNTING_HD WITH (NOLOCK) ON SHOP_COUNTING_DT.STOCKID = SHOP_COUNTING_HD.STOCKID
                                        INNER JOIN SHOP_COUNTING_RC WITH (NOLOCK) ON SHOP_COUNTING_DT.STOCKID = SHOP_COUNTING_RC.StockID 
                                  INNER JOIN [SHOP2013TMP].[dbo].[EMPLTABLE] WITH (NOLOCK) ON SHOP_COUNTING_RC.EmplID = [SHOP2013TMP].[dbo].[EMPLTABLE].EMPLID
                                     AND SHOP_COUNTING_DT.ITEMBARCODE = SHOP_COUNTING_RC.Barcode
                                WHERE SHOP_COUNTING_DT.STOCKID = '{StockID}'
                                  AND ITEMID = '{ItemID}' AND INVENTDIMID = '{InventdimID}' "};

            sql.Add($@" DELETE
                        FROM    SHOP_COUNTING_DT
                        WHERE   STOCKID = '{StockID}'
                                AND ITEMID = '{ItemID}' AND INVENTDIMID = '{InventdimID}' ");

            return ConnectionClass.ExecuteSQL_ArrayMain(sql);
        }
    }
}
