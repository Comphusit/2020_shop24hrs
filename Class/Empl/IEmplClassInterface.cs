﻿using System.Data;

namespace PC_Shop24Hrs.Class.Empl
{
    interface IEmplClassInterface
    {
        //สำหรับการอ้างอิงกับ ShopEmp
        DataTable GetEmployee(string _emplID);

        //สำหรับการไม่อ้างอิงกับ ShopEmp
        DataTable GetEmployeeCheckTime_Altnum(string _Altnum, string _pTypeTimeKeeper = "0");

        //ค้นหาพนักงานตามรหัส โดยที่ไม่ผูกกับ Shop24
        DataTable GetEmployee_Altnum(string _Altnum);

        //เบอร์โทร
        string GetTelEmployeeByEmplID(string _emplID);

        //ค้นหา ราบละเอียด(ไม่เยอะ) พนก. จากรหัส ไม่ผูกกับ Shop24
        DataTable GetEmployeeDetail_ByEmplID(string _emplALTNUM);

        //ค้นหา พนง ตามแผนกและเงือ่นไขอื่นๆ เพิ่มเติม
        DataTable GetEmployeeAll_ByDptID(string pCase, string _fixPosition, string _dptID, string _pPositionDesc, string _pEmpID);//_fixPosition 0 NoFix ,1 Fix  // pCase 0 = MN เท่านั้น 1 = D

        //ค้นหา พนง ตามแผนกและเงือ่นไขอื่นๆ เพิ่มเติม โดยที่ผู้กกับลงเวลาเช้างาน
        DataTable GetEmployeeAllCheckTimeKeeper_ByDptID(string bchID, string dateInBranch);

        //รูปถ่ายพนักงาน
        DataTable GetImage_byALTNUM(string ALTNUM);

        //MYJEEDS พนักงานทั้งหมด ที่อยู่ในระบบ Shop24
        DataTable GetEmpAll_SHOP24(string depID);//กรณีที่เป้น mn ทั้งหมด ให้ส่ง MN%  

        //MYJEEDS ค้นหาพนักงาน ตามวันที่ลงเวลาเข้าออก-งาน ประกบกับชั้นวาง ที่รับผิดชอบ ในวันที่กำหนด
        DataTable GetEmpAllShelf_ByDate(string pBchID, string pDate, string pEmpID, string pCon);//สำหรับ pEmpID จะต้องระบุฟิลด์มาด้วย AND EMPLTABLE.EMPLID = ''

        //ค้นหารายได้ พนักงานล่าสุด
        DataTable GetSalary_ByEmplID(string emplID);

        //Insert Empl
        string Save_EMPLOYEE(string pCase, string altNum, string password, string rmk);//0 insert 1 Update

        //รายงานการรับคืนสินค้า JEEDS OK
        DataTable MNPC_GetReport_MNPC(string DateBegin, string DateEnd, string Branch);

        //รายงานค่าคอมของพนักงานทีมจัดร้าน
        DataTable MNSV_CommissionTeamAB(string date1, string date2, string pBch);
        //การรีเช็คซองส่งเงินของสาขาโดยCentershop
        DataTable MNPM_SendMoneyForRecheck(string pCase, string date);
        //รายงานสแกนนิ้ว 1.30
        DataTable Scan_ReportGetScanHalfHour(string DateStart, string DateEnd, string Dpt);
        //รายงานสแกนนิ้ว ช่างก่อสร้าง
        DataTable Scan_ReportGetScanD041(string DateStart, string DateEnd, string Dpt);
        //ประเมินพนักงาน
        DataTable RateEmp_EVALUATE(string pCase, string bch, string month, string round);
        //ซองเงินแลกเหรียญ
        DataTable MNEC_ReportMain(string pDate1, string pDate2);
    }
}
