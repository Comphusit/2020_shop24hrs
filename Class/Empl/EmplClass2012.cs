﻿using System;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.Class.Empl
{
    class EmplClass2012 : IEmplClassInterface
    {
        //สำหรับการอ้างอิงกับ ShopEmp
        DataTable IEmplClassInterface.GetEmployee(string _emplID)
        {
            //ย้าย SQL ทั้งหมด เข้า Stored Procedures EmplClass_GetEmployee เพื่อใช้งานในโปรแกรม 24 PDA + Andriod
            return Controllers.ConnectionClass.SelectSQL_Main($@"[EmplClass_GetEmployee_AX2012] '5','{_emplID}','' ");
        }
        //สำหรับการไม่อ้างอิงกับ ShopEmp
        DataTable IEmplClassInterface.GetEmployeeCheckTime_Altnum(string _Altnum, string _pTypeTimeKeeper)
        {
            //ย้าย SQL ทั้งหมด เข้า Stored Procedures EmplClass_GetEmployee เพื่อใช้งานในโปรแกรม 24 PDA + Andriod
            return Controllers.ConnectionClass.SelectSQL_Main($@"[EmplClass_GetEmployee_AX2012] '{_pTypeTimeKeeper}','{_Altnum}','' ");
        }
        //ค้นหาพนักงานตามรหัส โดยที่ไม่ผูกกับ Shop24
        DataTable IEmplClassInterface.GetEmployee_Altnum(string _Altnum)
        {
            return ConnectionClass.SelectSQL_Main($@" [EmplClass_GetEmployee_AX2012] '4','{_Altnum}','' ");
        }
        //เบอร์โทร
        string IEmplClassInterface.GetTelEmployeeByEmplID(string _emplID)
        {
            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main($@" [EmplClass_GetEmployee_AX2012] '2','{_emplID}','' ");
            if (dt.Rows.Count == 0) return "";
            else return dt.Rows[0]["SPC_PhoneMobile"].ToString();
        }
        //ค้นหา ราบละเอียด(ไม่เยอะ) พนก. จากรหัส ไม่ผูกกับ Shop24
        DataTable IEmplClassInterface.GetEmployeeDetail_ByEmplID(string _emplALTNUM)
        {
            return Controllers.ConnectionClass.SelectSQL_Main($@" [EmplClass_GetEmployee_AX2012]   '3','{_emplALTNUM}','' ");
        }

        //ค้นหา พนง ตามแผนกและเงือ่นไขอื่นๆ เพิ่มเติม
        //AX2012_OK
        DataTable IEmplClassInterface.GetEmployeeAll_ByDptID(string pCase, string _fixPosition, string _dptID, string _pPositionDesc, string _pEmpID)//_fixPosition 0 NoFix ,1 Fix
        {
            string coditionDpt = "", conditionPosition = "", conditionEmpID = "", fixPosition = "";
            if (_dptID != "") coditionDpt = $@" AND DIMENSIONS.NAMEALIAS = '{_dptID}' ";
            if (_pPositionDesc != "") conditionPosition = $@" AND POSITIONDETAIL = '{_pPositionDesc}' ";
            if (_pEmpID != "") conditionEmpID = $@" AND EMPLTABLE.ALTNUM  = '{_pEmpID}' ";
            if (_fixPosition == "1") fixPosition = " AND CASE WHEN POSITIONTITLE = 'ผู้จัดการฝ่าย' THEN '02' WHEN POSITIONTITLE = 'พนักงาน' THEN '03' ELSE '01' END != '02' ";

            string stringsql = "";
            switch (pCase)
            {
                case "0":
                    stringsql = $@"
                        SELECT  EMPLTABLE.ALTNUM AS EMPLID,SPC_NAME,DIMENSIONS.NAMEALIAS AS NUM,DIMENSIONS.DESCRIPTION AS  DESCRIPTION,
                                DIMENSIONS.NAMEALIAS+'-'+DIMENSIONS.DESCRIPTION AS DeptDesc,
                                POSITIONDETAIL AS POSSITION,CASE WHEN POSITIONTITLE = 'ผู้จัดการฝ่าย' THEN '02' WHEN POSITIONTITLE = 'พนักงาน' THEN '03' ELSE '01' END AS IVZ_HROMPositionTitle,DIMENSIONS.NUM AS NUM2012
                        FROM	AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH (NOLOCK)  
                                INNER JOIN AX60CU13.dbo.SYSUSERINFOV_SPC WITH (NOLOCK) ON EMPLTABLE.EMPLID = SYSUSERINFOV_SPC.EMPLID
                                INNER JOIN AX60CU13.dbo.DEPARTMENTTABLEV_SPC DIMENSIONS WITH (NOLOCK)   on EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = 'SPC'  
                        WHERE	EMPLTABLE.DATAAREAID  = 'SPC'  
                                {fixPosition} {conditionEmpID} {conditionPosition} {coditionDpt}
                        ORDER BY DIMENSIONS.NAMEALIAS,EMPLTABLE.EMPLID ";
                    break;
                case "1":
                    stringsql = $@"
                        SELECT  EMPLTABLE.ALTNUM AS EMPLID,SPC_NAME,DIMENSIONS.NAMEALIAS AS NUM,DIMENSIONS.DESCRIPTION AS  DESCRIPTION,
                                DIMENSIONS.NAMEALIAS+'-'+DIMENSIONS.DESCRIPTION AS DeptDesc,
                                POSITIONDETAIL AS POSSITION,CASE WHEN POSITIONTITLE = 'ผู้จัดการฝ่าย' THEN '02' WHEN POSITIONTITLE = 'พนักงาน' THEN '03' ELSE '01' END AS IVZ_HROMPositionTitle,DIMENSIONS.NUM AS NUM2012,
                                EMPLTABLE.ALTNUM +' - '+ SPC_NAME AS DISPLAYNAME 
                        FROM	AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH (NOLOCK)  
                                INNER JOIN AX60CU13.dbo.SYSUSERINFOV_SPC WITH (NOLOCK) ON EMPLTABLE.EMPLID = SYSUSERINFOV_SPC.EMPLID
                                INNER JOIN AX60CU13.dbo.DEPARTMENTTABLEV_SPC DIMENSIONS WITH (NOLOCK)   on EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = 'SPC'  
                        WHERE	EMPLTABLE.DATAAREAID  = 'SPC'  
                                {fixPosition} {conditionEmpID} {conditionPosition} {coditionDpt}
                        ORDER BY DIMENSIONS.NAMEALIAS,EMPLTABLE.EMPLID ";
                    break;
                default:
                    break;
            }
            
            return Controllers.ConnectionClass.SelectSQL_Main(stringsql);
        }

        //ค้นหา พนง ตามแผนกและเงือ่นไขอื่นๆ เพิ่มเติม โดยที่ผู้กกับลงเวลาเช้างาน
        //AX2012_OK
        DataTable IEmplClassInterface.GetEmployeeAllCheckTimeKeeper_ByDptID(string bchID, string dateInBranch)
        {
            string conditionBch = "";
            if (bchID != "") conditionBch = $@" AND DIMENSIONS.NAMEALIAS  {bchID} ";

            string stringsql = ($@"
                    SELECT	DIMENSIONS.NAMEALIAS AS NUM,DIMENSIONS.DESCRIPTION AS DESCRIPTION,EMPLTABLE.ALTNUM AS EMPLID , SPC_NAME ,
		                    POSITIONDETAIL AS POSSITION,CASE WHEN POSITIONTITLE = 'ผู้จัดการฝ่าย' THEN '02' WHEN POSITIONTITLE = 'พนักงาน' THEN '03' ELSE '01' END AS IVZ_HROMPositionTitle,
		                    ISNULL(PW_TIME_IN,'-') AS TimeInsA,
		                    CASE 	CONVERT(VARCHAR,ISNULL(PW_DATETIME_IN,'00:00:00'),25)  WHEN '1900-01-01 00:00:00.000' THEN '-' ELSE CONVERT(VARCHAR,ISNULL(PW_DATETIME_IN,'00:00:00'),25) END AS TimeIns  

                    FROM	AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH(NOLOCK)  
		                    INNER JOIN AX60CU13.dbo.SYSUSERINFOV_SPC WITH (NOLOCK) ON EMPLTABLE.EMPLID = SYSUSERINFOV_SPC.EMPLID
		                    INNER JOIN AX60CU13.dbo.DEPARTMENTTABLEV_SPC DIMENSIONS WITH (NOLOCK)   on EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = 'SPC'  
		                    LEFT OUTER JOIN ( 
			                    SELECT  PW_IDCARD,PW_DATETIME_IN,PW_TIME_IN 
                                FROM    SHOP_TIMEKEEPER WITH(NOLOCK)  
			                    WHERE   PW_DATE_IN = '{dateInBranch}' )
                            Shop_TimeKeeper   ON EMPLTABLE.ALTNUM = Shop_TimeKeeper.PW_IDCARD  
 
                    WHERE	EMPLTABLE.DATAAREAID = 'SPC'
		                    AND ISNULL(PW_TIME_IN,'-') != '-' 
		                    {conditionBch}
                        
                    ORDER BY DIMENSIONS.NAMEALIAS,TimeIns,EMPLID
            ");

            return Controllers.ConnectionClass.SelectSQL_Main(stringsql);
        }
        //รูปถ่ายพนักงาน
        DataTable IEmplClassInterface.GetImage_byALTNUM(string ALTNUM)
        {
            return Controllers.ConnectionClass.SelectSQL_Main($@" [EmplClass_GetEmployee_AX2012] '1','{ALTNUM}','' ");
        }

        //MYJEEDS พนักงานทั้งหมด ที่อยู่ในระบบ Shop24
        DataTable IEmplClassInterface.GetEmpAll_SHOP24(string depID)//กรณีที่เป้น mn ทั้งหมด ให้ส่ง MN%  
        {
            string conditionDptID = "";
            if (depID != "") conditionDptID = $@" AND DIMENSIONS.NAMEALIAS LIKE ''{depID}'' ";
            return Controllers.ConnectionClass.SelectSQL_Main($@" [EmplClass_GetEmployee_AX2012] '6','','{conditionDptID}' ");
        }
        //MYJEEDS ค้นหาพนักงาน ตามวันที่ลงเวลาเข้าออก-งาน ประกบกับชั้นวาง ที่รับผิดชอบ ในวันที่กำหนด
        //AX2012_OK
        DataTable IEmplClassInterface.GetEmpAllShelf_ByDate(string pBchID, string pDate, string pEmpID, string pCon)//สำหรับ pEmpID จะต้องระบุฟิลด์มาด้วย AND EMPLTABLE.EMPLID = ''
        {
            string coditionEmpID = "";
            if (pEmpID != "") coditionEmpID = $@" AND EMPLTABLE.ALTNUM = '{pEmpID}' ";

            string sqlSelect1 = $@"
            SELECT	EMPLTABLE.ALTNUM AS EMPLID,SPC_NAME,DIMENSIONS.NAMEALIAS AS NUM,DIMENSIONS.DESCRIPTION AS  DESCRIPTION,
		            POSITIONDETAIL AS POSSITION,CASE WHEN POSITIONTITLE = 'ผู้จัดการฝ่าย' THEN '02' WHEN POSITIONTITLE = 'พนักงาน' THEN '03' ELSE '01' END AS IVZ_HROMPositionTitle,
		            CONVERT(VARCHAR,isnull(PW_DATETIME_IN,''),25) AS DATEINS,ZONE_ID,ZONE_NAME,SHELF_ID,SHELF_NAME,ISNULL(MAXIMAGE,0) AS MAXIMAGE

            FROM	AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH (NOLOCK)  
		            INNER JOIN AX60CU13.dbo.DEPARTMENTTABLEV_SPC DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM
		            INNER JOIN AX60CU13.dbo.SYSUSERINFOV_SPC WITH (NOLOCK) ON EMPLTABLE.EMPLID = SYSUSERINFOV_SPC.EMPLID
		            --INNER JOIN SHOP_BRANCH  WITH(NOLOCK)  ON  DIMENSIONS.NAMEALIAS  = Shop_Branch .BRANCH_ID    
		            LEFT OUTER JOIN
		            (
			            SELECT  PW_BRANCH_IN,PW_PWCARD,PW_DATETIME_IN  
                        FROM    SHOP_TIMEKEEPER WITH (NOLOCK) 
			            WHERE	CONVERT(VARCHAR,PW_DATE_IN,23) = '{pDate}' 
					            AND PW_BRANCH_IN IN ({pBchID} )
		            )Shop_TimeKeeper
		            ON EMPLTABLE.EMPLID = Shop_TimeKeeper.PW_PWCARD AND  DIMENSIONS.NAMEALIAS = Shop_TimeKeeper.PW_BRANCH_IN 
		            LEFT OUTER JOIN 
		            (
                        SELECT	DIMENSIONS.NAMEALIAS AS BRANCH,EMPLTABLE.ALTNUM AS EMPL_ID,'Z00' AS ZONE_ID,'อื่นๆ' AS ZONE_NAME ,'SH000' AS SHELF_ID, 'อื่นๆ-รูปจัดสินค้า' AS SHELF_NAME, '0' AS MAXIMAGE
                        FROM	AX60CU13.dbo.EMPLTABLEV_SPC EMPLTABLE WITH (NOLOCK) 
					            INNER JOIN AX60CU13.dbo.DEPARTMENTTABLEV_SPC DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM
                        WHERE	DIMENSIONS.NAMEALIAS  IN ({pBchID})
		                        AND EMPLTABLE.DATAAREAID  = 'SPC' 
                        
                        UNION
                        
                        SELECT	 SHOP_SHELFZONE.BRANCH_ID AS BRANCH,EMPL_ID,SHOP_SHELFZONE.ZONE_ID, SHOP_SHELFZONE.ZONE_NAME ,
						            SHOP_SHELFEMPLDATE.SHELF_ID,SHOP_SHELFEMPLDATE.SHELF_NAME,SHOP_SHELF.IMAGE AS MAXIMAGE
			            FROM	SHOP_SHELFEMPLDATE  WITH(NOLOCK) 
						            INNER JOIN SHOP_SHELFZONE  WITH(NOLOCK) ON SHOP_SHELFEMPLDATE.ZONE_ID=SHOP_SHELFZONE.ZONE_ID
						            INNER JOIN SHOP_SHELF WITH (NOLOCK) ON SHOP_SHELFEMPLDATE.SHELF_ID = SHOP_SHELF.SHELF_ID 
							            AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID 
							            AND SHOP_SHELF.ZONE_ID = SHOP_SHELFZONE.ZONE_ID
			            WHERE	SHOP_SHELFZONE.BRANCH_ID IN ( {pBchID}) 
					            AND SHOP_SHELFEMPLDATE.BRANCH_ID IN ( {pBchID} )
					            AND DATE_EMPL = '{pDate}' 
					            AND DATE_EVENT=''	
					            AND	SHIFTID	!=	'')TMP ON EMPLTABLE.ALTNUM = TMP.EMPL_ID  AND DIMENSIONS.NAMEALIAS = TMP.BRANCH


            WHERE	DIMENSIONS.NAMEALIAS IN ( {pBchID} )
                    AND EMPLTABLE.DATAAREAID  = 'SPC' 
        
	                AND CONVERT(VARCHAR,isnull(PW_DATETIME_IN,''),23)  != '1900-01-01'
                    {coditionEmpID}
                    {pCon}
           
            ORDER BY DIMENSIONS.NAMEALIAS,EMPLTABLE.ALTNUM,ZONE_ID,SHELF_ID    ";
            return ConnectionClass.SelectSQL_Main(sqlSelect1);
        }
        //ค้นหารายได้ พนักงานล่าสุด
        DataTable IEmplClassInterface.GetSalary_ByEmplID(string emplID)
        {
            string str = $@"
            SELECT  TOP 1 tblTermMs.tTermCode,tblTermMs.tTermDetail,tEmpCode,tEmpName,tEmpAmount 
            FROM    dbFingerAuthen.dbo.tblTermMs WITH (NOLOCK) 
                        INNER JOIN dbFingerAuthen.dbo.tblTermDt WITH (NOLOCK)   ON tblTermMs.tTermCode = tblTermDt.tTermCode
            WHERE  tEmpCode = '{emplID}'
            ORDER BY tStartTermDate DESC ";
            return ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.Con701SRV);
        }
        //Insert Empl
        string IEmplClassInterface.Save_EMPLOYEE(string pCase, string altNum, string password, string rmk)//0 insert 1 Update
        {
            string sql = "";
            switch (pCase)
            {
                case "0":
                    sql = $@"
                insert into SHOP_EMPLOYEE
                        (EMP_ID, EMP_PASSWORD, EMP_LEVEL, EMP_POSITION, EMP_REMARK,
                        EMP_DATETIMEIN, EMP_WHOIN, 
                        EMP_DATETIMEUP, EMP_WHOUP, 
                        EMP_TRANSTOCK, EMP_STACOMMN, EMP_LASTUSED, EMP_LASTUSEDSPC)
                values('{altNum}', '{password}', '0', '0', '{rmk}',
                        convert(varchar,getdate(),20), '{SystemClass.SystemUserID}', 
                        convert(varchar,getdate(),20), '{SystemClass.SystemUserID}', 
                        '0', '', '', '{Controllers.SystemClass.SystemPcName}') ";
                    break;
                case "1":
                    sql = $@"update SHOP_EMPLOYEE 
                        set     EMP_PASSWORD ='{password}',
                                EMP_REMARK = '{rmk}',
                                EMP_DATETIMEUP=convert(varchar,getdate(),20), EMP_WHOUP = '{SystemClass.SystemUserID}'
                        where   EMP_ID ='{altNum}' ";
                    break;
                case "2":
                    sql = $@"
                        UPDATE  SHOP_EMPLOYEE 
                        SET     EMP_TRANSTOCK = '{password}',
                                EMP_DATETIMEUP = GETDATE(),EMP_WHOUP = '{SystemClass.SystemUserID}'
                        WHERE   EMP_ID = '{altNum}' ";
                    break;
                case "3":
                    sql = $@"
                        UPDATE  SHOP_EMPLOYEE 
                        SET     EMP_STADISCOUNT = '{password}',
                                EMP_DATETIMEUP = GETDATE(),EMP_WHOUP = '{SystemClass.SystemUserID}'
                        WHERE   EMP_ID = '{altNum}' ";
                    break;
            }

            return sql;
        }

        //รายงานการรับคืนสินค้า JEEDS OK
        DataTable IEmplClassInterface.MNPC_GetReport_MNPC(string DateBegin, string DateEnd, string Branch)
        {
            string bchCondition = "";
            if (Branch != "") bchCondition = $@" AND Branch = '{Branch}' ";


            string sql = $@"
            SELECT	SHOW_NAME as  TYPEPRODUCT,
		            Branch AS BRANCH_ID,BranchName AS BRANCH_NAME,
		            DocNo,CONVERT(VARCHAR,DocDate,23) + ' '+DocTime AS DocDate,WhoIn + CHAR(10) + WhoNameIn AS ShopName,
		            Case ISNULL(STADOC,0)  when '3' then 'ยกเลิก' else  
		            Case ISNULL(STAPRCDOC,0)  when '0' then 'สาขาไม่ยืนยัน' else 
			            Case ISNULL(STACN,0)  when '0' then 'CN ไม่อนุมัติ' else 'อนุมัติ' 
			            end 
		            end 
		            end AS STADOC,
		            SHIPID,ISNULL(TagNumber,0) AS TagNumber,ISNULL(CountRecive,0) AS CountRecive,ISNULL(ShipSta,0) AS ShipSta,ShipCarID,ShipDriverID, 
		            CONVERT(VARCHAR,ShipDate,23)+ ' ' + ShipTime AS ShipDate,ShipTime,ShipWho +CHAR(10) +SHIP.SPC_NAME AS ShipName,  
		            CONVERT(VARCHAR,DateCN,23) + ' ' +TimeCN as DateCN,WhoCN + CHAR(10) +WhoNameCN AS CNName,STADOC  

            FROM    SHOP_MNPC_HD WITH(NOLOCK) 
		            LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE SHIP WITH(NOLOCK) ON SHOP_MNPC_HD.ShipWho = SHIP.EMPLID AND SHIP.DATAAREAID = 'SPC' 
		            LEFT OUTER JOIN
		            (
			            SELECT  TagMNPC, COUNT(TagMNPC) AS CountRecive
			            FROM    SHOP_MNPC_TAG WITH(NOLOCK)
			            WHERE   TagReciveSta = 1  
					            AND TagCreateDate Between '{DateTime.Parse(DateBegin).AddDays(-5):yyyy-MM-dd}' and '{DateTime.Parse(DateEnd).AddDays(1):yyyy-MM-dd}'
			            GROUP BY TagMNPC
		            )TagRecive ON SHOP_MNPC_HD.DocNo = TagRecive.TagMNPC
		            INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH(NOLOCK) on SHOP_MNPC_HD.TYPEPRODUCT=SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID and TYPE_CONFIG='34'

            WHERE	DocDate BETWEEN '{DateBegin}' and '{DateEnd}' 
		            AND TYPE_CONFIG = '34'   {bchCondition}

            ORDER BY DocNo,DocDate
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายงานค่าคอมของพนักงานทีมจัดร้าน
        DataTable IEmplClassInterface.MNSV_CommissionTeamAB(string date1, string date2, string pBch)
        {
            string sql = string.Format(@"
                    SELECT	Team_ID,Team_NAME,EMPLTABLE.EMPLID,SHOW_NAME AS SPC_NAME,DIMENSIONS.NUM,
		                    DIMENSIONS.DESCRIPTION, ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS POSSITION,
		                    ISNULL(C_WORK,0) AS C_WORK,ISNULL(MaxImage,0) AS PRICE,ISNULL(C_WORK,0)*ISNULL(MaxImage,0) AS SUMALL
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
		                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = EMPLTABLE.EMPLID
                            INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK)   ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM   
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)   ON EMPLTABLE.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  
                                and HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID  = 'SPC' AND  DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE)
                            LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK) ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID 
                                and HRPPartyJobTableRelationship.DATAAREAID  = 'SPC'
		                   LEFT OUTER JOIN (
			                    SELECT	SHOW_ID AS EmpConfigID,BRANCH_ID AS TEAMID                         
			                    FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) 
			                    WHERE	TYPE_CONFIG = '38'  AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
		                    )EmpTeam ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = EmpTeam.EmpConfigID
							LEFT OUTER JOIN (
			                    SELECT	SHOW_ID AS Team_ID,SHOW_NAME AS Team_NAME                         
			                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
			                    WHERE	TYPE_CONFIG = '37'
		                    )Team ON EmpTeam.TEAMID = Team.Team_ID
		                    LEFT OUTER JOIN (
			                    SELECT	Shop_MNSV_DT.MNSV_EmpID AS EMPLID,COUNT(Shop_MNSV_RC.MNSV_Docno) as C_WORK  
			                    FROM	Shop_MNSV_HD WITH (NOLOCK)
					                    INNER JOIN Shop_MNSV_DT WITH (NOLOCK) ON Shop_MNSV_HD.MNSV_Docno = Shop_MNSV_DT.MNSV_Docno  
					                    INNER JOIN Shop_MNSV_RC WITH (NOLOCK) ON Shop_MNSV_DT.MNSV_Docno =Shop_MNSV_RC.MNSV_Docno  
                                WHERE	Shop_MNSV_HD.MNSV_IN = '1' AND MNSV_STA = '1' 
					                    AND CONVERT(VARCHAR,MNSV_Date,23) BETWEEN '" + date1 + @"' 
                                        AND '" + date2 + @"'
					                    " + pBch + @"
			                    GROUP BY Shop_MNSV_DT.MNSV_EmpID
		                    )Work ON EMPLTABLE.EMPLID = Work.EMPLID

                    WHERE	TYPE_CONFIG = '38' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1'
		                     and DIMENSIONCODE = 0  and DIMENSIONS.DATAAREAID = 'SPC' and EMPLTABLE.DATAAREAID  = 'SPC'

                    ORDER BY SHOW_ID 
                    ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //การรีเช็คซองส่งเงินของสาขาโดยCentershop
        DataTable IEmplClassInterface.MNPM_SendMoneyForRecheck(string pCase, string date)
        {
            string sql = "";
            string pConBch = "";
            if (SystemClass.SystemBranchID != "MN000") pConBch = " AND MNPMBranch = '" + SystemClass.SystemBranchID + @"'";

            switch (pCase)
            {
                case "0":
                    sql = $@"
                     SELECT	MNPMBranch,BRANCH_NAME,MNPMDocno,CASE MNPMLO WHEN '1' THEN 'ฝากเงินเข้าธนาคาร' ELSE MNPMLO END AS MNPMLO,
                            CONVERT(VARCHAR,MNPMDate,23) AS MNPMDate,MNPMTime,MNPMUserCode + '-' + A.SPC_NAME AS MNPMUserCode,MNPMCountPack,
		                    MNPMUserCar + '-' + B.SPC_NAME AS MNPMUserCar,MNPMCar,MNPMRemark,ISNULL(RMK,'') AS RMK,ISNULL(TMP.INVOICEID,'0') AS INVOICEID 
                     FROM	SHOP_MNPM_HD WITH (NOLOCK)   
		                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMBranch = SHOP_BRANCH.BRANCH_ID  
                            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE A WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCode = A.EMPLID AND A.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE B WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCar = B.EMPLID AND B.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN 
		                    ( SELECT	INVOICEID,ITEMBARCODE,DESCRIPTION  AS RMK
		                     FROM	Shop_RecheckReciveItem WITH (NOLOCK)    	     
		                     WHERE	Datebill = '{date}'     AND VAN = 'MNPM' AND SHIPMENTID IS NULL  )TMP ON SHOP_MNPM_HD.MNPMDocno = TMP.INVOICEID
                     WHERE	MNPMDATE = '{date}'   {pConBch}
                            AND MNPMLO != '1'
                     ORDER BY MNPMDocno ";
                    break;
                case "2":
                    sql = $@"
                     SELECT	MNPMBranch,BRANCH_NAME,MNPMDocno,CASE MNPMLO WHEN '1' THEN 'ฝากเงินเข้าธนาคาร' ELSE MNPMLO END AS MNPMLO,
                            CONVERT(VARCHAR,MNPMDate,23) AS MNPMDate,MNPMTime,MNPMUserCode + CHAR(10) + A.SPC_NAME AS MNPMUserCode,MNPMCountPack,MNPMSum,
		                    MNPMUserCar + '-' + B.SPC_NAME AS MNPMUserCar,MNPMCar,MNPMRemark,ISNULL(RMK,'') AS RMK,ISNULL(TMP.INVOICEID,'0') AS INVOICEID 
                     FROM	SHOP_MNPM_HD WITH (NOLOCK)   
		                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMBranch = SHOP_BRANCH.BRANCH_ID  
                            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE A WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCode = A.EMPLID AND A.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE B WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCar = B.EMPLID AND B.DATAAREAID = N'SPC'
		                    LEFT OUTER JOIN 
		                    ( SELECT	INVOICEID,ITEMBARCODE,DESCRIPTION  AS RMK
		                     FROM	Shop_RecheckReciveItem WITH (NOLOCK)    	     
		                     WHERE	Datebill = '{date}'     AND VAN = 'MNPM' AND SHIPMENTID  = '1'  )TMP ON SHOP_MNPM_HD.MNPMDocno = TMP.INVOICEID
                     WHERE	MNPMDATE = '{date}'  {pConBch}
                            AND MNPMLO = '1'
                     ORDER BY MNPMDocno ";
                    break;
                default:
                    break;
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รายงานสแกนนิ้ว 1.30
        DataTable IEmplClassInterface.Scan_ReportGetScanHalfHour(string DateStart, string DateEnd, string Dpt)
        {
            string SelectDept = "AND SCANID = '" + SystemClass.SystemBranchID + @"' "; ;
            if (SystemClass.SystemBranchID == "MN000") SelectDept = "AND NUM IN ( '" + Dpt + @"') ";

            string str = $@"
                    DECLARE @DateBegin Date = '{DateStart}';
                    DECLARE @DateEnd Date = '{DateEnd}';  

                    select  SCANID AS BRANCH_ID,
		                    SHOP_BRANCH.BRANCH_NAME,
                            EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME ,DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,  
                            CONVERT(VARCHAR, CONVERTDATETIME, 25) AS CONVERTDATETIME   
                    from  	SCANFINGER_TAFFDT WITH (NOLOCK)
		                    INNER JOIN SHOP.SHOP24HRS.dbo.SHOP_BRANCH WITH (NOLOCK) ON SCANFINGER_TAFFDT.SCANID = SHOP_BRANCH.BRANCH_ID
		                    INNER JOIN SHOP.SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SCANFINGER_TAFFDT.PWCARD = EMPLTABLE.ALTNUM AND EMPLTABLE.DATAAREAID = N'SPC'
		                    INNER JOIN SHOP.SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON  EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = 0
                    WHERE	CONVERTDATE BETWEEN @DateBegin AND @DateEnd  
                            AND LOCATIONDATA = '2' 
                            AND SCANFINGER_TAFFDT.PWCARD <> ' ' 
                            {SelectDept}
                    ORDER BY DIMENSIONS.NUM,EMPLTABLE.EMPLID,CONVERTDATETIME";
            return ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConHROS);
        }
        //รายงานสแกนนิ้ว ช่างก่อสร้าง
        DataTable IEmplClassInterface.Scan_ReportGetScanD041(string DateStart, string DateEnd, string Dpt)
        {
            string SelectDept = "AND SCANID = '" + SystemClass.SystemBranchID + @"' "; ;
            if (SystemClass.SystemBranchID == "MN000") SelectDept = "AND NUM IN ( '" + Dpt + @"') ";

            string str = $@"
                    DECLARE @DateBegin Date = '{DateStart}';
                    DECLARE @DateEnd Date = '{DateEnd}';  

                    select  SCANID AS BRANCH_ID,
		                    SHOP_BRANCH.BRANCH_NAME,
                            EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME ,DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,  
                            CONVERT(VARCHAR, CONVERTDATETIME, 25) AS CONVERTDATETIME   
                    from  	SPC_WS_TAFFDTNEWSITE_SHOP24 WITH (NOLOCK)
		                    INNER JOIN SHOP.SHOP24HRS.dbo.SHOP_BRANCH WITH (NOLOCK) ON SPC_WS_TAFFDTNEWSITE_SHOP24.SCANID = SHOP_BRANCH.BRANCH_ID
		                    INNER JOIN SHOP.SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_WS_TAFFDTNEWSITE_SHOP24.PWCARD = EMPLTABLE.ALTNUM AND EMPLTABLE.DATAAREAID = N'SPC'
		                    INNER JOIN SHOP.SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON  EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = 0
                    WHERE	CONVERTDATE BETWEEN @DateBegin AND @DateEnd  
                            AND LOCATIONDATA = '2' 
                            AND PWCARD <> ' ' 
                            {SelectDept}
                    ORDER BY DIMENSIONS.NUM,EMPLTABLE.EMPLID,CONVERTDATETIME";
            return ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConHROS);
        }
        //ประเมินพนักงาน
        DataTable IEmplClassInterface.RateEmp_EVALUATE(string pCase, string bch, string month, string round)
        {
            string sql;
            if (pCase == "0")
            {
                sql = $@"
                SELECT	PWEMPLOYEE, PWCARD AS RateEmpId, 'คุณ ' + PWFNAME + ' ' + PWLNAME AS RateEmpName, 
		                BRANCH_ID, BRANCH_NAME, 
		                PWEMPLOYEE_V_SPC.PWSECTION, PWEMPLOYEE_V_SPC.PWSECTIONNAME,
		                PWDEPTNAME, PWDEPT, MAX(ISNULL(RateMonth,'')) AS RateMonth
                FROM	SPC160SRV.HROS2013.DBO.PWEMPLOYEE_V_SPC WITH (NOLOCK)
		                INNER JOIN SPC160SRV.Piswin7.dbo.PWSECTION WITH (NOLOCK)
			                ON PWSECTION.PWSECTION = PWEMPLOYEE_V_SPC.PWSECTION  COLLATE Thai_CS_AS 
		                INNER JOIN SHOP_BRANCH
			                ON PWSECTION.PWEDESC = SHOP_BRANCH.BRANCH_ID COLLATE Thai_CS_AS 
			                AND {bch}
		                LEFT JOIN SHOP_RATEEMP
			                ON PWEMPLOYEE_V_SPC.PWCARD = SHOP_RATEEMP.RateEmpId
			                AND {month}
			                AND RateRound = '{round}'
                WHERE	PWSTATWORK IN ('A','V')
		                AND PWDEPTNAME NOT LIKE 'ผู้จัดการร้าน%'
                GROUP BY PWEMPLOYEE, PWCARD, PWFNAME, PWLNAME, 
		                BRANCH_ID, BRANCH_NAME, PWEMPLOYEE_V_SPC.PWSECTION, 
		                PWEMPLOYEE_V_SPC.PWSECTIONNAME, PWDEPTNAME, PWDEPT
                ORDER BY BRANCH_ID, PWEMPLOYEE, PWDEPT DESC
                    ";
            }
            else
            {
                sql = $@"
                SELECT	PWEMPLOYEE, PWCARD AS RateEmpId, 'คุณ ' + PWFNAME + ' ' + PWLNAME AS RateEmpName, 
	                BRANCH_ID, BRANCH_NAME, 
	                PWEMPLOYEE_V_SPC.PWSECTION, PWEMPLOYEE_V_SPC.PWSECTIONNAME,
	                PWDEPTNAME, PWDEPT
                FROM	SPC160SRV.HROS2013.DBO.PWEMPLOYEE_V_SPC WITH (NOLOCK)
	                INNER JOIN SPC160SRV.Piswin7.dbo.PWSECTION WITH (NOLOCK)
		                ON PWSECTION.PWSECTION = PWEMPLOYEE_V_SPC.PWSECTION  COLLATE Thai_CS_AS 
	                INNER JOIN SHOP_BRANCH
		                ON PWSECTION.PWEDESC = SHOP_BRANCH.BRANCH_ID COLLATE Thai_CS_AS              
                ORDER BY BRANCH_ID, PWEMPLOYEE, PWDEPT DESC ";
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ซองเงินแลกเหรียญ
        DataTable IEmplClassInterface.MNEC_ReportMain(string pDate1, string pDate2)
        {
            string sql = $@"
                SELECT	CONVERT(NVARCHAR,MNECDateDoc,23) AS MNECDateDoc, MNECDocno, MNECBranch, MNECBranchName, MNECBranchRoute, MNECSum,
		                ISNULL(MNPMLO,'') AS LOGISTICID, ISNULL(MNPMCar,'') AS VEHICLEID,
		                ISNULL(CONVERT(NVARCHAR,MNPMDateIn,23) + ' ' + MNPMTimeIn,'') AS MNPMDateIn,
		                ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,'') AS MNPMDRAWERID, 
		                ISNULL(MNPMUserCode,'')+'-'+ISNULL(EMPID.SPC_NAME,'') AS MNWhoSend,
		                CASE WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') = '' THEN ' ' 
				                WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') < (CONVERT(VARCHAR,DateAcc,23) +' 08:30:59') THEN '1'
				                WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') < (CONVERT(VARCHAR,DateAcc,23) +' 15:00:59') THEN '2'
				                WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') < (CONVERT(VARCHAR,DateAcc,23) +' 19:30:59') THEN '3'
		                END AS ROUNDSEND,
		                ISNULL(StaTrans,'') AS StaTrans,CASE ISNULL(StaTrans,'') WHEN '1' THEN 'รับแล้ว' ELSE 'ไม่ได้รับ' END AS StaTransDESC,
		                ISNULL(CONVERT(VARCHAR,DateTrans,23) +' '+TimeTrans,'')  AS DateTrans,
		                ISNULL(WhoTrans,'')+'-'+ISNULL(TMPTRANS.SPC_NAME,'') AS WhoTrans,
		                ISNULL(StaAcc,'') AS StaAcc,CASE ISNULL(StaAcc,'') WHEN '1' THEN 'รับแล้ว' ELSE 'ไม่ได้รับ' END AS StaAccDESC ,
		                ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'')  AS DateAcc,
		                ISNULL(WhoAcc,'')+ '-' +ISNULL(TMPACC.SPC_NAME,'') AS WhoAcc,
                        RSL_DOCNO AS SLROUND,SL_DOCNO AS SLId,SL_NET AS CASHAMOUNT,'การเงินรับแล้ว' AS STA ,'1' AS POSTED
                        
                FROM	SHOP_MNEC_HD WITH (NOLOCK)
		                LEFT JOIN SHOP_MNPM_DT WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNPM_DT.MNPMDRAWERID AND SHOP_MNPM_DT.MNPMDRAWERID LIKE 'MNEC%'
		                LEFT JOIN SHOP_MNPM_HD WITH (NOLOCK) ON SHOP_MNPM_DT.MNPMDocno = SHOP_MNPM_HD.MNPMDocno
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE TMPACC WITH (NOLOCK)	 ON SHOP_MNPM_DT.WhoAcc = TMPACC.EMPLID AND TMPACC.DATAAREAID = N'SPC'   
		                LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE TMPTRANS WITH (NOLOCK)  ON SHOP_MNPM_DT.WhoTrans = TMPTRANS.EMPLID AND TMPTRANS.DATAAREAID = N'SPC'
                        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE EMPID WITH (NOLOCK)	 ON SHOP_MNPM_HD.MNPMUserCode = EMPID.EMPLID AND EMPID.DATAAREAID = N'SPC'
                WHERE	MNECDateDoc BETWEEN '{pDate1}' AND '{pDate2}'
		                AND MNECStaApv IN ('1','2','3')
                ORDER BY SHOP_MNEC_HD.MNECDocno ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
