﻿
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.Class
{
    class ConfigClass
    {
        ////ค้นหาว่าฟอร์มนี้จะต้องใช้ DB- SHOP2013 หรือ SHOP24HRS
        //public static string GetConfigDB_FORMNAME(string pFormName) // ในกรณีที่ return "" คือใช้ DB ใหม่ เลย
        //{
        //    DataTable dt = Controllers.ConnectionClass.SelectSQL_Main
        //        (string.Format(@"SELECT	FORMNAME,FORMSTATUS	
        //            FROM	[24HRS_FORMNAME]  WITH (NOLOCK) 
        //            WHERE	FORMNAME = '" + pFormName + @"'"));
        //    if (dt.Rows.Count == 0)
        //    {
        //        return "";
        //    }

        //    if (dt.Rows[0]["FORMSTATUS"].ToString() == "1")//ใช้ Database SHOP2013
        //    {
        //        return "[SHOP2013].[dbo].";
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}

        //ค้นหาว่าฟอร์มนี้ เปิดให้ใช้งานในระบบ User หรือยัง 0 ยัง 1 เปิดแล้ว
        public static string GetConfigUSE_FORMNAME(string pFormName) // ในกรณีที่ return "" คือใช้ DB ใหม่ เลย
        {
            DataTable dt = ConnectionClass.SelectSQL_Main
                (string.Format(@"SELECT	FORMUSE  	
                    FROM	[24HRS_FORMNAME]  WITH (NOLOCK) 
                    WHERE	FORMNAME = '" + pFormName + @"'"));
            if (dt.Rows.Count == 0) return "1"; else return dt.Rows[0]["FORMUSE"].ToString();
        }
        //My Jeeds ค้นหาเลขที่บิลล่าสุด 0 = ประเภทของบิล 1 = เลขที่บิลขึ้นหน้า
        public static string GetMaxINVOICEID(string _pType, string _pPOS, string _pFrist, string _pCase)
        {
            string strBillID = string.Format(@" [config_SHOP_GENARATE_BILLAUTO] '" + _pType + "','" + _pPOS + "','" + _pFrist + "','" + _pCase + "'");
            DataTable dt = ConnectionClass.SelectSQL_Main(strBillID);
            return dt.Rows[0]["ID"].ToString();
        }
        //My Jeeds ค้นหาในตาราง SHOP_CONFIGBRANCH_GenaralDetail ตาม typeID
        public static DataTable FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(string _pTypeID, string pCon, string orderBy, string sta01)
        {
            string OrderByDesc = " ORDER BY SHOW_NAME ";
            if (orderBy != "") OrderByDesc = orderBy;
            if (sta01 == "") sta01 = "1";

            string strBillID = $@"
                    SELECT	*,
                            SHOW_ID+'-'+SHOW_NAME AS DESCSHOW	,SHOW_ID AS DATA_ID,SHOW_NAME AS DATA_DESC ,SHOW_ID AS BRANCH_ID,SHOW_NAME AS BRANCH_NAME,
                            CASE TYPE_CONFIG WHEN '28' THEN CASE [MaxImage] WHEN '0' THEN 'ข้อความ' ELSE 'ตัวเลข' END 	ELSE CONVERT(varchar,MaxImage) END  AS [MaxImage] ,SHOW_NAME+','+SHOW_DESC as SHOW_NAMEDESC  
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                    WHERE	TYPE_CONFIG = '{_pTypeID}' AND STA IN ({sta01})  {pCon}
                    {OrderByDesc}
                    ";
            return ConnectionClass.SelectSQL_Main(strBillID);
        }
        //ค้นหาข้อมูลที่บันทึกใน CONFIGBRANCH_DETAIL ตามประเภท
        public static DataTable FindData_CONFIGBRANCH_DETAIL(string pTypeID, string pCon = "")
        {
            return ConnectionClass.SelectSQL_Main($@" 
                                    SELECT	*
                                    FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK)
                                    WHERE	TYPE_CONFIG = '{pTypeID}' AND STATUS = '1'
                                            {pCon}
                                    ORDER BY SHOW_ID ");
        }
        //ค้นหข้อมูลจำนวน ตามที่ระบุ ต่อวัน ใน SHOP_CONFIGBRANCH_QTY
        public static DataTable FindData_CONFIGBRANCH_QTY(string pTypeID, string date)
        {
            return ConnectionClass.SelectSQL_Main($@" 
                                    SELECT	SHOW_ID,BRANCH_ID,QTY	
                                    FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK)
                                    WHERE	TYPE_CONFIG = '{pTypeID}' 
                                            AND CONVERT(VARCHAR,DATE_SEND,23) = '{date}' ");

        }
        //Insert
        public static string Save_CONFIGBRANCH_QTY(string pTypeID, string billID, string date, string bchID, string showID, double qty)
        {
            string sql = $@" 
                INSERT INTO [dbo].[SHOP_CONFIGBRANCH_QTY]
	                ([SHOW_ID],[DATE_SEND],[TYPE_CONFIG],[BRANCH_ID]
	                ,[QTY],[WHOIDINS],[WHONAMEINS],[BILLSEND]) 
                VALUES 
	                ('{showID}','{date}',
	                '{pTypeID}','{bchID}','{qty}',
	                '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{billID}')                            
            ";
            return sql;
        }
        //Insert
        public static string Save_CONFIGBRANCH_DETAIL(string showID, string showName, string showDesc, string configID, string bchID, string remark = "")
        {
            string sql;
            if (showName == "")
            {
                sql = $@"DELETE  SHOP_CONFIGBRANCH_DETAIL  
                        WHERE   SHOW_ID='{showID}' and BRANCH_ID='{bchID}' AND TYPE_CONFIG = '{configID}' ";
            }
            else
            {
                sql = $@"INSERT INTO SHOP_CONFIGBRANCH_DETAIL 
                                        (SHOW_ID, SHOW_NAME, SHOW_DESC, REMARK,TYPE_CONFIG,  BRANCH_ID, 
                                         WHOIDINS, WHONAMEINS )
                        VALUES  ('{showID}','{showName}','{showDesc}','{remark}','{configID}','{bchID}', 
                                        '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";
            }
            return sql;
        }
        //Insert 
        public static string Save_CONFIGBRANCH_GenaralDetail(string showID, string showName, string desc, string configID, string configName, string rmk, string sta, double image, string stalock)
        {
            string sql;
            if (configName == "")
            {
                sql = $@"UPDATE [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] SET SHOW_NAME = '{showName}',
                            SHOW_DESC = '{desc}',REMARK = '{rmk}',LOCK= '{stalock}',
                            STA = '{sta}',MaxImage = '{image}',DATEUPD = GETDATE(),WHOIDUPD = '{SystemClass.SystemUserID}',
                            WHONAMEUPD = '{SystemClass.SystemUserName}' 
                        WHERE TYPE_CONFIG = '{configID}' AND SHOW_ID = '{showID}' ";
            }
            else
            {
                sql = $@"INSERT INTO [dbo].[SHOP_CONFIGBRANCH_GenaralDetail] 
                                ([SHOW_ID],[SHOW_NAME],[SHOW_DESC],[TYPE_CONFIG],[TYPE_CONFIG_DESC],[REMARK],
                                [STA],[MaxImage],[WHOIDINS],[WHONAMEINS],[LOCK] )
                            VALUES  ('{showID}','{ConfigClass.ChecKStringForImport(showName)}','{desc}',
                                '{configID}','{configName}','{rmk}',
                                '{sta}','{image}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{stalock}')";
            }
            return sql;
        }
        //ค้นหาสาขา SHOP_CONFIGBRANCH_DETAIL ที่ผูกไว้กับ TypeID
        public static DataTable FindData_BranchconfigByTypeID(string pTypeID, string pCon = "")
        {
            return ConnectionClass.SelectSQL_Main($@" 
                                    SELECT	SHOP_BRANCH.BRANCH_ID,BRANCH_NAME,SHOP_BRANCH.BRANCH_ID + '-' + BRANCH_NAME AS NAMEDESC
                                    FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) INNER JOIN SHOP_BRANCH WITH (NOLOCK)
			                                    ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                                    WHERE	TYPE_CONFIG = '{pTypeID}' {pCon}
                                    GROUP BY SHOP_BRANCH.BRANCH_ID,BRANCH_NAME
                                    ORDER BY SHOP_BRANCH.BRANCH_ID ");
        }

        public static string Update_CONFIGBRANCH_DETAIL(string Bch, string Id, string Remark)
        {
            string sql = $@"
            UPDATE	SHOP_CONFIGBRANCH_DETAIL
            SET		REMARK = '{Remark}'
            WHERE	BRANCH_ID = '{Bch}'
		            AND SHOW_ID = '{Id}' ";
            return sql;

        }
        #region SetColor
        //Setcolor ม่วง
        public static Color SetColor_PurplePastel()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(198, 146, 225);
            return myRgbColor;
        }
        //Setcolor ม่วง
        public static Color SetColor_SkyPastel()
        {
            Color myRgbColor = Color.FromArgb(154, 225, 255);
            return myRgbColor;
        }
        //Setcolor เขียว Pastel
        public static Color SetColor_GreenPastel()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(173, 244, 215);
            return myRgbColor;
        }
        //Setcolor เขียว
        public static Color SetColor_Green()
        {
            //  Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(0, 128, 0);
            return myRgbColor;
        }
        //Setcolor ชมพู
        public static Color SetColor_PinkPastel()

        {
            Color myRgbColor = Color.FromArgb(255, 203, 226);
            return myRgbColor;
        }
        //Setcolor แดง
        public static Color SetColor_Red()

        {
            //  Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(255, 0, 0);
            return myRgbColor;
        }
        //Setcolor เหลือง
        public static Color SetColor_YellowPastel()

        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(228, 219, 124);
            return myRgbColor;
        }
        //Setcolor เทา
        public static Color SetColor_GrayPastel()

        {
            // Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(190, 187, 188);
            return myRgbColor;
        }
        //Setcolor ม่วงเข้ม
        public static Color SetColor_DarkPurplePastel()

        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(137, 76, 169);
            return myRgbColor;
        }
        //Setcolor น้ำเงิน
        public static Color SetColor_Blue()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.Blue;
            return myRgbColor;
        }
        //Setcolor BlueViolet
        public static Color SetColor_Wheat()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.Wheat;
            return myRgbColor;
        }
        //Setcolor Black
        public static Color SetColor_Black()
        {
            Color myRgbColor = Color.Black;
            return myRgbColor;
        }
        #endregion

        //Input Number only
        public static bool IsNumber(string text)
        {
            bool res = true;
            try
            {
                if (!string.IsNullOrEmpty(text) && ((text.Length != 1) || (text != "-")))
                {
                    Decimal d = decimal.Parse(text, CultureInfo.CurrentCulture);
                }
            }
            catch
            {
                res = false;
            }
            return res;
        }
        //Send EMail
        public static void SendEmail(string subject, string Message, double iRows, string branchid, string branchname)
        {
            string sta = "";
            Message = Message.Replace("ไม่สามารถบันทึกข้อมูลเข้าระบบได้ ลองใหม่อีกครั้ง.", "");
            try
            {
                Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                Microsoft.Office.Interop.Outlook.MailItem mailItem = app.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);
                mailItem.Subject = subject;
                //mailItem.To = "Com-Minimark@supercheapphuket.com";
                mailItem.To = "secretary@supercheapphuket.com;sasithon@supercheapphuket.com;Jutima.p@supercheapphuket.com;patchareec@supercheapphuket.com";
                mailItem.Body = Message + Environment.NewLine + DateTime.Now.ToString() + Environment.NewLine +
                    branchid + " - " + branchname + Environment.NewLine +
                    Controllers.SystemClass.SystemUserID + " - " + Controllers.SystemClass.SystemUserName;
                mailItem.Importance = Microsoft.Office.Interop.Outlook.OlImportance.olImportanceNormal;
                mailItem.Send();
            }
            catch (Exception ex)
            {
                sta = ex.Message;
            }

            string strError = $@"
                INSERT INTO [SHOP_LOGERROR]
                           ([ErrorSubject],[ErrorDesc],[ErrorBranchID],[ErrorBranchName],
                            [ErrorWhoIDIns],[ErrorWhoName],[ErrorExMail],[ErrorRows])
                VALUES
                           ('{subject}','{Message.Replace("'", "")}','{branchid}','{branchname}',
                            '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{sta.Replace("'", "")}','{iRows}') 
                ";
            ConnectionClass.ExecuteSQL_Main(strError);
        }
        //Check { "
        public static string ChecKStringForImport(string str)
        {
            return str.Replace("{", "").Replace("}", "").Replace("'", "").Replace("]", "").Replace("[", "");
        }
        //Replace
        public static string ReplaceMDPForEmlpID(string str)
        {
            return str.Trim().Replace("M", "").Replace("D", "").Replace("P", "");
        }
    }
}


