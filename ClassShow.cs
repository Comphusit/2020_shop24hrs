﻿using System;
using NVRCsharpDemo;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace HikvisionPreview
{
    class ClassShow
    {
        public static Int16 DVRPortNumber = 8000;
        //public static string DVRUserName = "admin";
        //public static string DVRPassword = "admin8570";

        //Set Path Emp
        public static void PathEmp(PictureBox pictureBoxShow)
        {
            try
            { pictureBoxShow.Image = Image.FromFile(@"\\192.168.100.27\ImageMinimark\emply.png"); }
            catch
            {
                try
                { pictureBoxShow.Image = Image.FromFile(@"\\192.168.100.60\ImageMinimark\emply.png"); }
                catch
                { return; }
            }
        }

        
        //Preview
        public static Int32 Preview(int channel, PictureBox picturebox, Int32 m_lUserID)
        {
            Int32 m_lRealHandle_ = -1;
            try
            {
                CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO
                {
                    hPlayWnd = picturebox.Handle,//set picture box
                    lChannel = channel,//the device channel number
                    dwStreamType = 0,//码流类型：0-主码流，1-子码流，2-码流3，3-码流4，以此类推
                    dwLinkMode = 0,//连接方式：0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP 
                    bBlocked = true, //0- 非阻塞取流，1- 阻塞取流
                    dwDisplayBufNum = 1,//播放库显示缓冲区最大帧数
                    byProtoType = 0,
                    byPreviewMode = 0
                };
                IntPtr pUser = IntPtr.Zero;//用户数据 user data 
                m_lRealHandle_ = CHCNetSDK.NET_DVR_RealPlay_V40(m_lUserID, ref lpPreviewInfo, null, pUser);
                if (m_lRealHandle_ < 0)
                {
                    PathEmp(picturebox);
                }
            }
            catch
            {
                PathEmp(picturebox);
            }
            return m_lRealHandle_;
        }

    }
}
