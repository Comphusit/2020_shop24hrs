﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    public partial class D204_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        readonly string _pPermission;
        // 0 - รายงานน้ำหนักรถสาขาต่างจังหวัด
        // 1 - รายงานการตรวจรถ 4 ด้าน
        // 2 - รายงานการส่งคืนอุปกรณ์ตาม LO
        // 3 - รายงานการไม่เก็บ-ไม่มี อุปกรณ์ส่งคืน
        // 4 - LO ส่งสินค้า
        // 5 - รายงานล้างรถ
        // 6 - คิวพนักงานขับรถ [จัดส่ง]
        // 7 - รายงานฝาถังน้ำมัน
        // 8 - รายงาน LO ที่ไม่เก็บ CN/ขยะ
        // 9 - รายงาน รายการรับบิลเบิก I/F/FAL

        //Load
        public D204_Report(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void D204_Report_Load(object sender, EventArgs e)
        {
            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;

            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Text = "สีแดง >> ไม่ได้ช่างน้ำหนักรถก่อนออกไปส่งของ | แสดงเฉพาะ LO ที่ไปส่งของสาขา MN055 คลองแห้ง และ MN059 เมืองกระบี่เท่านั้น";
                    radLabel_Date.Text = "ระบุวันที่ SH";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CUSTACCOUNT", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CUSTNAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "แผนกรับผิดชอบ", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPPINGDATETIME", "เวลาออกจากสาขาใหญ่", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "พนักงานขับรถ", 220)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("VEHICLEWEGHT", "น้ำหนักช่าง", 120)));

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "VEHICLEWEGHT = 0 ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["LOGISTICID"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["CUSTACCOUNT"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["CUSTNAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["DEPTNAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["VEHICLEID"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["SHIPPINGDATETIME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["SPC_NAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["VEHICLEWEGHT"].ConditionalFormattingObjectList.Add(obj2);
                    break;
                case "1":
                    radLabel_Detail.Text = "สีแดง >> ไม่มีการถ่ายรูปในช่องที่กำหนด | สีม่วง >> ระบุรถมีปัญหาตอนถ่ายรูป | DoubleClick >> ช่องรูปเพื่อดูขนาดใหญ่";
                    radLabel_Date.Text = "ระบุวันที่ LO";
                    DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
                    RadDropDownList_1.DataSource = LogisticClass.GetLogisticDept();
                    RadDropDownList_1.DisplayMember = "DEPTNAME";
                    RadDropDownList_1.ValueMember = "DEPTID";
                    radDateTimePicker_D1.Value = DateTime.Now;
                    radDateTimePicker_D2.Value = DateTime.Now;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked;
                    RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "แผนกรับผิดชอบ", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPPINGDATETIME", "เวลาเข้าสาขาใหญ่", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ARRIVALDATETIME", "เวลาออกจากสาขาใหญ่", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "พนักงานขับรถ", 210)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME_Image", "พนักงานถ่ายรูป", 210)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image1", "รูปด้านหน้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage1", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage1", "PathImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("F1", "ppp")));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image2", "รูปข้างซ้าย", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage2", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage2", "PathImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("F2", "ppp")));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image3", "รูปข้างขวา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage3", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage3", "PathImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("F3", "ppp")));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image4", "รูปด้านหลัง", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage4", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage4", "PathImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("F4", "ppp")));

                    ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition1", "CountImage1 = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["Image1"].ConditionalFormattingObjectList.Add(obj3);

                    ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition1", "CountImage2 = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["Image2"].ConditionalFormattingObjectList.Add(obj4);

                    ExpressionFormattingObject obj5 = new ExpressionFormattingObject("MyCondition1", "CountImage3 = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["Image3"].ConditionalFormattingObjectList.Add(obj5);

                    ExpressionFormattingObject obj6 = new ExpressionFormattingObject("MyCondition1", "CountImage4 = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["Image4"].ConditionalFormattingObjectList.Add(obj6);

                    ExpressionFormattingObject obj7 = new ExpressionFormattingObject("MyCondition1", "F1 = 'True' ", false)
                    { CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                    RadGridView_ShowHD.Columns["Image1"].ConditionalFormattingObjectList.Add(obj7);

                    ExpressionFormattingObject obj8 = new ExpressionFormattingObject("MyCondition1", "F2 = 'True' ", false)
                    { CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                    RadGridView_ShowHD.Columns["Image2"].ConditionalFormattingObjectList.Add(obj8);

                    ExpressionFormattingObject obj9 = new ExpressionFormattingObject("MyCondition1", "F3 = 'True' ", false)
                    { CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                    RadGridView_ShowHD.Columns["Image3"].ConditionalFormattingObjectList.Add(obj9);

                    ExpressionFormattingObject obj10 = new ExpressionFormattingObject("MyCondition1", "F4 = 'True' ", false)
                    { CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                    RadGridView_ShowHD.Columns["Image4"].ConditionalFormattingObjectList.Add(obj10);
                    RadGridView_ShowHD.TableElement.RowHeight = 120;
                    break;
                case "2":
                    radLabel_Detail.Text = "สีแดง >> ได้รับของขาด | สีฟ้า >> ได้รับของเกิน";
                    radLabel_Date.Text = "ระบุวันที่ส่งคืน";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDDATE", "วันที่ส่งคืน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTEID", "เส้นทาง", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RIUTEIDNAME", "ชื่อเส้นทาง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อ พขร", 250)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRASHDESC", "ปริมาณ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRASHREMARK", "หมายเหตุ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDSHID", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDBarcode", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDName", "ชื่ออุปกรณ์", 150)));

                    if (_pPermission == "0")
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ReturnQty", "จำนวนส่ง")));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ReturnRecive", "จำนวนรับ")));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SUMALL", "ผลต่าง")));
                    }
                    else
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("ReturnQty", "จำนวนส่ง", 80)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("ReturnRecive", "จำนวนรับ", 80)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMALL", "ผลต่าง", 80)));
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DiffSing", "เครื่องหมาย")));

                    //ExpressionFormattingObject obj2_1 = new ExpressionFormattingObject("MyCondition1", "DiffSing = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //RadGridView_ShowHD.Columns["MNRDSHID"].ConditionalFormattingObjectList.Add(obj2_1);

                    //ExpressionFormattingObject obj2_2 = new ExpressionFormattingObject("MyCondition1", "DiffSing = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_SkyPastel() };
                    //RadGridView_ShowHD.Columns["MNRDSHID"].ConditionalFormattingObjectList.Add(obj2_2);

                    DatagridClass.SetCellBackClolorByExpression("MNRDSHID", "DiffSing = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNRDSHID", "DiffSing = '1' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                    break;
                case "3":
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Text = "ระบุวันที่บันทึก";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDBRANCH", "สาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDDATE", "วันที่บันทึก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRDSHID", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTEID", "เส้นทาง", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RIUTEIDNAME", "ชื่อเส้นทาง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อ พขร", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRASHDESC", "ปริมาณ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRASHREMARK", "หมายเหตุ", 100)));


                    break;
                case "4":
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Text = "ระบุวันที่ส่งสินค้า";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "เลขที่ LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTEID", "สาย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อเส้นทาง", 230)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEPTLOGISTIC", "แผนก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "ชื่อแผนกส่ง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPPINGDATETIME", "เวลาออกส่งสินค้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ARRIVALDATETIME", "เวลากลับส่งจากสินค้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงานขับรถ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRASHREMARK", "หมายเหตุ", 200)));

                    break;
                case "5":
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Text = "ระบุวันที่ล้างรถ";

                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-365));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now.AddDays(365));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CARCARE_DATE", "วันที่ล้าง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CARCARE_TIME", "เวลาล้าง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CARCARE_CARID", "ทะเบียน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CARCARE_CARNAME", "ชื่อรถ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CARCARE_DPTID", "แผนก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CARCARE_DPTNAME", "แผนกรถ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่จอง", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOINS", "ผู้จอง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ชื่อผู้จอง", 250)));

                    break;

                case "6":
                    radLabel_Detail.Text = "รายชื่อพนักงานขับรถที่รอในคิว | กด F5 เพื่อดึงข้อมูลใหม่ | DoubleClick เพื่อระบุปล่อย LO ให้ พขร แล้ว"; radLabel_Detail.Visible = true;
                    radLabel_Date.Visible = false;
                    radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    RadCheckBox_1.Visible = true; RadCheckBox_1.Text = "เฉพาะ พขร ที่ว่าง"; RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
                    RadCheckBox_1.CheckState = CheckState.Checked;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDATETIME", "วันที่-เวลา ลงคิว", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "รหัส พขร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMEDRIVER", "ชื่อ พขร", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDPTNAME", "แผนก", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "STA")));

                    //ExpressionFormattingObject obj6_0 = new ExpressionFormattingObject("MyCondition1", "STA = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //RadGridView_ShowHD.Columns["LOGISTICID"].ConditionalFormattingObjectList.Add(obj6_0);

                    DatagridClass.SetCellBackClolorByExpression("LOGISTICID", "STA = '1' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    break;
                case "7":
                    radLabel_Detail.Text = "สีแดง >> ไม่มีการถ่ายรูปในช่องที่กำหนด | สีม่วง >> ระบุรถมีปัญหาตอนถ่ายรูป | DoubleClick >> ช่องรูปเพื่อดูขนาดใหญ่";
                    radLabel_Date.Text = "ระบุวันที่ LO";
                    DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
                    RadDropDownList_1.DataSource = LogisticClass.GetLogisticDept();
                    RadDropDownList_1.DisplayMember = "DEPTNAME";
                    RadDropDownList_1.ValueMember = "DEPTID";
                    radDateTimePicker_D1.Value = DateTime.Now;
                    radDateTimePicker_D2.Value = DateTime.Now;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked;
                    RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "แผนกรับผิดชอบ", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPPINGDATETIME", "เวลาออกจากสาขาใหญ่", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ARRIVALDATETIME", "เวลเข้าสาขาใหญ่", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "พนักงานขับรถ", 210)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME_ImageOut", "พนักงานถ่ายรูปขาออก", 210)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image1", "รูปขาออก", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage1", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage1", "PathImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("F1", "ppp")));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME_ImageIn", "พนักงานถ่ายรูปขาเข้า", 210)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image2", "รูปขาเข้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage2", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage2", "PathImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("F2", "ppp")));

                    //ExpressionFormattingObject obj7_3 = new ExpressionFormattingObject("MyCondition1", "CountImage1 = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //RadGridView_ShowHD.Columns["Image1"].ConditionalFormattingObjectList.Add(obj7_3);

                    //ExpressionFormattingObject obj7_4 = new ExpressionFormattingObject("MyCondition1", "CountImage2 = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //RadGridView_ShowHD.Columns["Image2"].ConditionalFormattingObjectList.Add(obj7_4);

                    //ExpressionFormattingObject obj7_5 = new ExpressionFormattingObject("MyCondition1", "F1 = 'True' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                    //RadGridView_ShowHD.Columns["Image1"].ConditionalFormattingObjectList.Add(obj7_5);

                    //ExpressionFormattingObject obj7_6 = new ExpressionFormattingObject("MyCondition1", "F2 = 'True' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
                    //RadGridView_ShowHD.Columns["Image2"].ConditionalFormattingObjectList.Add(obj7_6);


                    DatagridClass.SetCellBackClolorByExpression("Image1", "CountImage1 = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("Image2", "CountImage2 = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("Image1", "F1 = 'True' ", ConfigClass.SetColor_DarkPurplePastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("Image2", "F2 = 'True' ", ConfigClass.SetColor_DarkPurplePastel(), RadGridView_ShowHD);

                    RadGridView_ShowHD.TableElement.RowHeight = 120;
                    break;
                case "8"://รายงานLO ที่ไม่เก็บ CN/ขยะ
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_TIME", "เวลาจัดส่งสั่งเก็บ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BCH_DATE", "สาขาบันทึก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BCH_TIME", "เวลาสาขาบันทึก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LO", "LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DriverID", "รหัส พขร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DriverName", "ชื่อ พขร", 210)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Remark", "หมายเหตุ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RemarkBill", "เลขที่บิลค้าง", 500)));

                    break;
                case "9": //รายงาน รายการรับบิลเบิก I/F/FAL

                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7);
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BILLID", "เลขที่บิล", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoIdBill", "ผู้ทำบิล", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameBill", "ชื่อผู้ทำบิล", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoIns", "ผู้รับบิล", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameIns", "ชื่อผู้รับบิล", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateIns", "วันที่รับบิล", 180)));

                    break;
                default:
                    break;
            }

            ClearTxt();
            if (_pTypeReport == "6") SetDGV_HD();

        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0"://น้ำหนักรถต่างจังหวัด

                    dt_Data = LogisticClass.Report_CarWeight(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect0);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "1"://ตรวจรถ 4 ข้าง

                    string depID = "";
                    if (RadCheckBox_1.CheckState == CheckState.Checked) depID = RadDropDownList_1.SelectedValue.ToString();

                    dt_Data = LogisticClass.Report_CarCheck(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                        depID);// ConnectionClass.SelectSQL_Main(sqlSelect1);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        string lo = dt_Data.Rows[i]["LOGISTICID"].ToString();
                        string pathFileName = PathImageClass.pPathSH_CarCloseNewLO + lo;
                        string FindEmpImage = "";

                        string pathFullName1 = PathImageClass.pImageEmply, pathFullName2 = PathImageClass.pImageEmply,
                            pathFullName3 = PathImageClass.pImageEmply, pathFullName4 = PathImageClass.pImageEmply;
                        string wantFile1 = "", wantFile2 = "", wantFile3 = "", wantFile4 = "";
                        string pCount1 = "0", pCount2 = "0", pCount3 = "0", pCount4 = "0";
                        Boolean pF1 = false, pF2 = false, pF3 = false, pF4 = false;

                        DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                        if (DirInfo.Exists)
                        {

                            wantFile1 = "*MN000*" + lo + @"*CF*.JPG";
                            FileInfo[] Files1 = DirInfo.GetFiles(wantFile1, SearchOption.AllDirectories);
                            if (Files1.Length > 0)
                            {
                                pathFullName1 = Files1[0].FullName;
                                pCount1 = "1";
                                pF1 = Files1[0].Name.Contains("_F_");
                                string[] A = Files1[0].Name.Split('_');
                                string[] B = A[0].Split('-');
                                FindEmpImage = B[6];
                            }

                            wantFile2 = "*MN000*" + lo + @"*CL*.JPG";
                            FileInfo[] Files2 = DirInfo.GetFiles(wantFile2, SearchOption.AllDirectories);
                            if (Files2.Length > 0)
                            {
                                pathFullName2 = Files2[0].FullName;
                                pCount2 = "1";

                                pF2 = Files2[0].Name.Contains("_F_");
                                if (FindEmpImage == "")
                                {
                                    string[] A = Files2[0].Name.Split('_');
                                    string[] B = A[0].Split('-');
                                    FindEmpImage = B[6];
                                }
                            }

                            wantFile3 = "*MN000*" + lo + @"*CR*.JPG";
                            FileInfo[] Files3 = DirInfo.GetFiles(wantFile3, SearchOption.AllDirectories);
                            if (Files3.Length > 0)
                            {
                                pathFullName3 = Files3[0].FullName;
                                pCount3 = "1";

                                pF3 = Files3[0].Name.Contains("_F_");
                                if (FindEmpImage == "")
                                {
                                    string[] A = Files3[0].Name.Split('_');
                                    string[] B = A[0].Split('-');
                                    FindEmpImage = B[6];
                                }
                            }

                            wantFile4 = "*MN000*" + lo + @"*CB*.JPG";
                            FileInfo[] Files4 = DirInfo.GetFiles(wantFile4, SearchOption.AllDirectories);
                            if (Files4.Length > 0)
                            {
                                pathFullName4 = Files4[0].FullName;
                                pCount4 = "1";

                                pF4 = Files4[0].Name.Contains("_F_");
                                if (FindEmpImage == "")
                                {
                                    string[] A = Files4[0].Name.Split('_');
                                    string[] B = A[0].Split('-');
                                    FindEmpImage = B[6];
                                }
                            }
                        }

                        string EmpName = "";
                        if (FindEmpImage != "")
                        {
                            DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(FindEmpImage.Replace("P", "").Replace("M", "").Replace("D", ""));
                            if (dtEmp.Rows.Count > 0)
                            {
                                EmpName = FindEmpImage + Environment.NewLine + dtEmp.Rows[0]["SPC_NAME"].ToString();
                            }
                            else
                            { EmpName = FindEmpImage; }
                        }

                        RadGridView_ShowHD.Rows[i].Cells["SPC_NAME_Image"].Value = EmpName;
                        RadGridView_ShowHD.Rows[i].Cells["F1"].Value = pF1;
                        RadGridView_ShowHD.Rows[i].Cells["F2"].Value = pF2;
                        RadGridView_ShowHD.Rows[i].Cells["F3"].Value = pF3;
                        RadGridView_ShowHD.Rows[i].Cells["F4"].Value = pF4;

                        RadGridView_ShowHD.Rows[i].Cells["Image1"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName1);
                        RadGridView_ShowHD.Rows[i].Cells["PathImage1"].Value = pathFileName + @"|" + wantFile1;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage1"].Value = pCount1;

                        RadGridView_ShowHD.Rows[i].Cells["Image2"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName2);
                        RadGridView_ShowHD.Rows[i].Cells["PathImage2"].Value = pathFileName + @"|" + wantFile2;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage2"].Value = pCount2;

                        RadGridView_ShowHD.Rows[i].Cells["Image3"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName3);
                        RadGridView_ShowHD.Rows[i].Cells["PathImage3"].Value = pathFileName + @"|" + wantFile3;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage3"].Value = pCount3;

                        RadGridView_ShowHD.Rows[i].Cells["Image4"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName4);
                        RadGridView_ShowHD.Rows[i].Cells["PathImage4"].Value = pathFileName + @"|" + wantFile4;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage4"].Value = pCount4;
                    }
                    this.Cursor = Cursors.Default;

                    break;
                case "2":  // 2 - รายงานการส่งคืนอุปกรณ์ตาม LO

                    dt_Data = LogisticClass.Report_MNRDReturn(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect2);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;

                    break;
                case "3":// 3 - รายงานการไม่เก็บ-ไม่มี อุปกรณ์ส่งคืน

                    dt_Data = LogisticClass.Report_MNRDNotReturn(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                        radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect3);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "4": // 4 - LO ส่งสินค้า

                    dt_Data = LogisticClass.Report_FindLODetail(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect4);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "5": // 5 - รายงานล้างรถ

                    dt_Data = Logistic_Class.CARCARE_FindData(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlSelect5);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                //case "6"://6 - คิวพนักงานขับรถ [จัดส่ง]
                //    string caseQ = "";
                //    if (RadCheckBox_1.Checked == true) caseQ = " WHERE	STA = '0' ";

                //    string sqlSelect6 = $@"
                //        SELECT	CONVERT(VARCHAR,CREATEDATETIME,25) AS CREATEDATETIME,EMPLDRIVER,EMPLNAMEDRIVER,EMPLDPTNAME,LOGISTICID,STA
                //        FROM	[SHOP_DRIVERQUEUE] WITH (NOLOCK)
                //        {caseQ}
                //        ORDER BY CREATEDATETIME
                //    ";

                //    dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect6);
                //    RadGridView_ShowHD.FilterDescriptors.Clear();
                //    RadGridView_ShowHD.DataSource = dt_Data;
                //    dt_Data.AcceptChanges();

                //    this.Cursor = Cursors.Default;
                //    break;
                case "7"://ฝาถังน้ำมัน

                    string logisticID = "";
                    if (RadCheckBox_1.CheckState == CheckState.Checked) logisticID = RadDropDownList_1.SelectedValue.ToString();

                    dt_Data = LogisticClass.Report_CapTankOil(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                            radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), logisticID);// ConnectionClass.SelectSQL_Main(sqlSelect7);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        string lo = dt_Data.Rows[i]["LOGISTICID"].ToString();
                        string pathFileName = PathImageClass.pPathSH_CarCloseNewLO + lo;
                        string FindEmpImage = "";
                        string FindEmpImage_In = "";

                        string pathFullName1 = PathImageClass.pImageEmply,
                               pathFullName2 = PathImageClass.pImageEmply;

                        string wantFile1 = "", wantFile2 = "";

                        string pCount1 = "0", pCount2 = "0";

                        Boolean pF1 = false, pF2 = false;

                        DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                        if (DirInfo.Exists)
                        {
                            wantFile1 = "*MN000*" + lo + @"*FC-BF*.JPG"; //รูปขาออก
                            FileInfo[] Files1 = DirInfo.GetFiles(wantFile1, SearchOption.AllDirectories);
                            if (Files1.Length > 0)
                            {
                                pathFullName1 = Files1[0].FullName;
                                pCount1 = "1";

                                pF1 = Files1[0].Name.Contains("_F_"); //เช็ค F ในชื่อไฟล์
                                if (FindEmpImage == "")
                                {
                                    string[] A = Files1[0].Name.Split('_');
                                    string[] B = A[0].Split('-');
                                    FindEmpImage = B[6];
                                }
                            }

                            wantFile2 = "*MN000*" + lo + @"*FC-AF*.JPG"; //รูปเขาเข้า
                            FileInfo[] Files2 = DirInfo.GetFiles(wantFile2, SearchOption.AllDirectories);
                            if (Files2.Length > 0)
                            {
                                pathFullName2 = Files2[0].FullName;
                                pCount2 = "1";

                                pF2 = Files2[0].Name.Contains("_F_"); //เช็ค F ในชื่อไฟล์
                                if (FindEmpImage_In == "")
                                {
                                    string[] A = Files2[0].Name.Split('_');
                                    string[] B = A[0].Split('-');
                                    FindEmpImage_In = B[6];
                                }
                            }
                        }

                        string EmpName = "";
                        string EmpName_IN = "";
                        if (FindEmpImage != "" || FindEmpImage_In != "")
                        {
                            if (FindEmpImage != "")
                            {
                                DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(FindEmpImage.Replace("P", "").Replace("M", "").Replace("D", ""));
                                if (dtEmp.Rows.Count > 0)
                                {
                                    if (dtEmp.Rows.Count > 0) EmpName = FindEmpImage + Environment.NewLine + dtEmp.Rows[0]["SPC_NAME"].ToString();
                                }
                                else EmpName = FindEmpImage;
                            }
                            if (FindEmpImage_In != "")
                            {
                                DataTable dtEmp_In = Models.EmplClass.GetEmployeeDetail_ByEmplID(FindEmpImage_In.Replace("P", "").Replace("M", "").Replace("D", ""));
                                if (dtEmp_In.Rows.Count > 0)
                                {
                                    if (dtEmp_In.Rows.Count > 0) EmpName_IN = FindEmpImage_In + Environment.NewLine + dtEmp_In.Rows[0]["SPC_NAME"].ToString();
                                }
                                else EmpName_IN = FindEmpImage_In;
                            }
                        }

                        RadGridView_ShowHD.Rows[i].Cells["SPC_NAME_ImageOut"].Value = EmpName;
                        RadGridView_ShowHD.Rows[i].Cells["F1"].Value = pF1;
                        RadGridView_ShowHD.Rows[i].Cells["SPC_NAME_ImageIn"].Value = EmpName_IN;
                        RadGridView_ShowHD.Rows[i].Cells["F2"].Value = pF2;

                        RadGridView_ShowHD.Rows[i].Cells["Image1"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName1);
                        RadGridView_ShowHD.Rows[i].Cells["PathImage1"].Value = pathFileName + @"|" + wantFile1;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage1"].Value = pCount1;

                        RadGridView_ShowHD.Rows[i].Cells["Image2"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName2);
                        RadGridView_ShowHD.Rows[i].Cells["PathImage2"].Value = pathFileName + @"|" + wantFile2;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage2"].Value = pCount2;
                    }
                    this.Cursor = Cursors.Default;
                    break;
                case "8"://รายงานLO ที่ไม่เก็บ CN/ขยะ
                    string sql8 = $@"
                        SELECT	
		                        CONVERT(VARCHAR,SHOP_SHIPDOCNOTSEND.DateIns,23) AS BCH_DATE,CONVERT(VARCHAR,SHOP_SHIPDOCNOTSEND.DateIns,24) AS BCH_TIME,
		                        SHOP_SHIPDOCNOTSEND.LO,SHOP_SHIPDOCNOTSEND.DriverID,SHOP_SHIPDOCNOTSEND.DriverName,
		                        SHOP_SHIPDOCNOTSEND.BranchID,SHOP_SHIPDOCNOTSEND.BranchName,Remark,REPLACE(RemarkBill,'มาเก็บทีหลัง','') AS RemarkBill,
		                        ISNULL(SHOP_MNRD_ORDER.DateINS,'') AS ORDER_DATE,ISNULL(SHOP_MNRD_ORDER.TimeINS,'NoOrder') AS ORDER_TIME
                        FROM	SHOP_SHIPDOCNOTSEND	WITH (NOLOCK) 
		                        LEFT OUTER JOIN SHOP_MNRD_ORDER WITH (NOLOCK) ON SHOP_SHIPDOCNOTSEND.LO = SHOP_MNRD_ORDER.LOGISTICID AND SHOP_SHIPDOCNOTSEND.BranchID = SHOP_MNRD_ORDER.BRANCH_ID
                        WHERE	CONVERT(VARCHAR,SHOP_SHIPDOCNOTSEND.DateIns,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                                AND TYPEDATA = 'LO'
                        GROUP BY SHOP_SHIPDOCNOTSEND.LO,SHOP_SHIPDOCNOTSEND.DriverID,SHOP_SHIPDOCNOTSEND.DriverName,
		                        SHOP_SHIPDOCNOTSEND.BranchID,SHOP_SHIPDOCNOTSEND.BranchName,Remark,REPLACE(RemarkBill,'มาเก็บทีหลัง','')  ,
		                        ISNULL(SHOP_MNRD_ORDER.DateINS,'') ,ISNULL(SHOP_MNRD_ORDER.TimeINS,'NoOrder') ,CONVERT(VARCHAR,SHOP_SHIPDOCNOTSEND.DateIns,24),CONVERT(VARCHAR,SHOP_SHIPDOCNOTSEND.DateIns,23)
                        ORDER BY BranchID,LO";
                    dt_Data = ConnectionClass.SelectSQL_Main(sql8);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "9":
                    string sql9 = $@"
                        SELECT	LO AS BILLID,BranchID,BranchName,DriverID AS WhoIdBill,DriverName AS WhoNameBill,WhoIns,WhoNameIns,CONVERT(VARCHAR,DateIns,25) AS DateIns	
                        FROM	SHOP_SHIPDOCNOTSEND	WITH (NOLOCK)
                        WHERE	TYPEDATA = 'Bill'
		                        AND CONVERT(VARCHAR,DateIns,23) BETWEEN  '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                        ORDER BY CONVERT(VARCHAR,DateIns,25) DESC ";
                    dt_Data = ConnectionClass.SelectSQL_Main(sql9);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            //radDateTimePicker_D1.Value = DateTime.Now;
            //radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
            if (_pTypeReport == "6") SetDGV_HD();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            string T;

            if (_pTypeReport == "1") T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "2");
            else T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            return;

        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            RadDropDownList_1.Enabled = false;

            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true;

        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            switch (_pTypeReport)
            {
                case "1":
                    switch (e.Column.Name)
                    {
                        case "Image1":
                            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage1"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["PathImage1"].Value.ToString());
                            break;
                        case "Image2":
                            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage2"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["PathImage2"].Value.ToString());
                            break;
                        case "Image3":
                            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage3"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["PathImage3"].Value.ToString());
                            break;
                        case "Image4":
                            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage4"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["PathImage4"].Value.ToString());
                            break;
                        default:
                            break;
                    }
                    break;
                case "6":
                    if (RadGridView_ShowHD.CurrentRow.Cells["STA"].Value.ToString() == "1") return;
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการปล่อย LO ให้พนักงานขับรถเรียบร้อยแล้ว ?") == DialogResult.Yes)
                    {
                        string T6 = Logistic_Class.Update_DriverQueueLO(RadGridView_ShowHD.CurrentRow.Cells["EMPLDRIVER"].Value.ToString()); //ConnectionClass.ExecuteSQL_Main(sqlUp);
                        if (T6 == "")
                        {
                            RadGridView_ShowHD.CurrentRow.Cells["STA"].Value = "1";
                            RadGridView_ShowHD.CurrentRow.Cells["LOGISTICID"].Value = "ไม่ได้ระบุ";
                        }
                    }

                    break;
                case "7":
                    switch (e.Column.Name)
                    {
                        case "Image1":
                            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage1"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["PathImage1"].Value.ToString());
                            break;
                        case "Image2":
                            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage2"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["PathImage2"].Value.ToString());
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        private void RadGridView_ShowHD_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) SetDGV_HD();
        }
    }
}
