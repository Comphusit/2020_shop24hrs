﻿namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    partial class CarCare_ShowData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarCare_ShowData));
            this.panel1 = new System.Windows.Forms.Panel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radButton_60 = new Telerik.WinControls.UI.RadButton();
            this.radButton_30 = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radButton_90 = new Telerik.WinControls.UI.RadButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radButton_Cancle = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Head = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_30 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_60 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_90 = new Telerik.WinControls.UI.RadLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_30)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_90)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.radGridView_Show);
            this.panel1.Location = new System.Drawing.Point(3, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(786, 424);
            this.panel1.TabIndex = 0;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(786, 424);
            this.radGridView_Show.TabIndex = 16;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radButton_60
            // 
            this.radButton_60.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_60.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_60.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_60.Location = new System.Drawing.Point(335, 3);
            this.radButton_60.Name = "radButton_60";
            this.radButton_60.Size = new System.Drawing.Size(115, 28);
            this.radButton_60.TabIndex = 8;
            this.radButton_60.Text = "60 นาที";
            this.radButton_60.ThemeName = "Fluent";
            this.radButton_60.Click += new System.EventHandler(this.RadButton_60_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_60.GetChildAt(0))).Text = "60 นาที";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_60.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_60.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_60.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_60.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_60.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_60.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_30
            // 
            this.radButton_30.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_30.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_30.Location = new System.Drawing.Point(73, 3);
            this.radButton_30.Name = "radButton_30";
            this.radButton_30.Size = new System.Drawing.Size(115, 28);
            this.radButton_30.TabIndex = 7;
            this.radButton_30.Text = "30 นาที";
            this.radButton_30.ThemeName = "Fluent";
            this.radButton_30.Click += new System.EventHandler(this.RadButton_30_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_30.GetChildAt(0))).Text = "30 นาที";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_30.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_30.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_30.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_30.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_30.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_30.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_30.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_30.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 570);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 548);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(786, 19);
            this.radLabel_Detail.TabIndex = 54;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.radButton_90, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.radButton_30, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radButton_60, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 508);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(786, 34);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // radButton_90
            // 
            this.radButton_90.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_90.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_90.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_90.Location = new System.Drawing.Point(597, 3);
            this.radButton_90.Name = "radButton_90";
            this.radButton_90.Size = new System.Drawing.Size(115, 28);
            this.radButton_90.TabIndex = 9;
            this.radButton_90.Text = "90 นาที";
            this.radButton_90.ThemeName = "Fluent";
            this.radButton_90.Click += new System.EventHandler(this.RadButton_90_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_90.GetChildAt(0))).Text = "90 นาที";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_90.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_90.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_90.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_90.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_90.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_90.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radButton_Cancle);
            this.panel2.Controls.Add(this.radLabel_Head);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(786, 34);
            this.panel2.TabIndex = 55;
            // 
            // radButton_Cancle
            // 
            this.radButton_Cancle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Cancle.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancle.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Cancle.Location = new System.Drawing.Point(668, 3);
            this.radButton_Cancle.Name = "radButton_Cancle";
            this.radButton_Cancle.Size = new System.Drawing.Size(115, 28);
            this.radButton_Cancle.TabIndex = 55;
            this.radButton_Cancle.Text = "Close";
            this.radButton_Cancle.ThemeName = "Fluent";
            this.radButton_Cancle.Visible = false;
            this.radButton_Cancle.Click += new System.EventHandler(this.RadButton_Cancle_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancle.GetChildAt(0))).Text = "Close";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancle.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancle.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancle.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancle.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Head
            // 
            this.radLabel_Head.AutoSize = false;
            this.radLabel_Head.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_Head.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Head.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Head.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Head.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Head.Name = "radLabel_Head";
            this.radLabel_Head.Size = new System.Drawing.Size(786, 34);
            this.radLabel_Head.TabIndex = 54;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_90, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_60, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_30, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 473);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(786, 29);
            this.tableLayoutPanel3.TabIndex = 56;
            // 
            // radLabel_30
            // 
            this.radLabel_30.AutoSize = false;
            this.radLabel_30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_30.ForeColor = System.Drawing.Color.Black;
            this.radLabel_30.Location = new System.Drawing.Point(3, 3);
            this.radLabel_30.Name = "radLabel_30";
            this.radLabel_30.Size = new System.Drawing.Size(255, 23);
            this.radLabel_30.TabIndex = 55;
            this.radLabel_30.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel_60
            // 
            this.radLabel_60.AutoSize = false;
            this.radLabel_60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            this.radLabel_60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_60.ForeColor = System.Drawing.Color.Black;
            this.radLabel_60.Location = new System.Drawing.Point(264, 3);
            this.radLabel_60.Name = "radLabel_60";
            this.radLabel_60.Size = new System.Drawing.Size(255, 23);
            this.radLabel_60.TabIndex = 56;
            this.radLabel_60.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel_90
            // 
            this.radLabel_90.AutoSize = false;
            this.radLabel_90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            this.radLabel_90.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_90.ForeColor = System.Drawing.Color.Black;
            this.radLabel_90.Location = new System.Drawing.Point(525, 3);
            this.radLabel_90.Name = "radLabel_90";
            this.radLabel_90.Size = new System.Drawing.Size(258, 23);
            this.radLabel_90.TabIndex = 57;
            this.radLabel_90.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CarCare_ShowData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancle;
            this.ClientSize = new System.Drawing.Size(792, 570);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "CarCare_ShowData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายละเอียดข้อมูลรถ";
            this.Load += new System.EventHandler(this.ShowDataDGV_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_30)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_90)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton radButton_60;
        protected Telerik.WinControls.UI.RadButton radButton_30;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        protected Telerik.WinControls.UI.RadButton radButton_90;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Head;
        protected Telerik.WinControls.UI.RadButton radButton_Cancle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel_90;
        private Telerik.WinControls.UI.RadLabel radLabel_60;
        private Telerik.WinControls.UI.RadLabel radLabel_30;
    }
}
