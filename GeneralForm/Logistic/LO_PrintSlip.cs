﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    public partial class LO_PrintSlip : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();

        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly string _pTypeOpen; // 0 คือ บิลขยะ   1 คือ บิลซองเงิน
        DataTable dt = new DataTable();

        string id_lo, date_lo, car_lo, emp_lo, name_lo, routed_lo;
        string bch_id, bch_Name, yy;
        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        public LO_PrintSlip(string pTypeOpen)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
        }

        //Load
        private void LO_PrintSlip_Load(object sender, EventArgs e)
        {

            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 250));

            radGridView_Show.EnableFiltering = false;

            yy = "LO" + DateTimeSettingClass.GetShortYear();
            ClearTxt();

            if (_pTypeOpen == "0") radLabel_F2.Visible = true; else radLabel_F2.Visible = false;

            radTextBox_LO2.Focus();

        }

        #region "CheckNumOnly"

        private void RadTextBox_Tag_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_LO2_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }



        #endregion
        //Clear Txt
        void ClearTxt()
        {
            radTextBox_LO1.Text = yy;

            if (dt.Rows.Count > 0) dt.Rows.Clear();
            dt.AcceptChanges();
            radGridView_Show.DataSource = null;

            bch_id = ""; bch_Name = "";
            id_lo = ""; date_lo = ""; car_lo = ""; emp_lo = ""; name_lo = ""; routed_lo = "";
            radLabel_Car.Text = "";
            radLabel_EmpDriver.Text = "";
            radLabel_Rount.Text = ""; radLabel_Branch.Text = "";
            radTextBox_LO1.Enabled = true; radTextBox_LO2.Enabled = true;
            RadTextBox_MN.Text = ""; RadTextBox_MN.Enabled = false;
            radButton_Save.Enabled = false;
            radTextBox_LO2.Text = ""; radTextBox_LO2.Focus();
            radTextBox_LO2.Focus();
        }

        void CheckLO_MNRD(string pLO)
        {
            dt = LogisticClass.GetLogistic_ByLO(pLO); //ConnectionClass.SelectSQL_Main(sqlMNRD);
            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูล LO ที่ระบุ");
                return;
            }

            radGridView_Show.DataSource = Logistic_Class.MNRD_FindData(pLO);//  ConnectionClass.SelectSQL_Main(sql);

            SetData(dt);
            RadTextBox_MN.Enabled = true; RadTextBox_MN.Focus();
        }
        //Check MNPM
        void CheckLO_MNPM(string pLO)
        {

            dt = LogisticClass.GetLogisticDetail_GroupByLO(pLO);//  ConnectionClass.SelectSQL_Main(sqlMNRD);
            radGridView_Show.DataSource = dt;

            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูล LO ที่ระบุ"); return;
            }

            SetData(dt);
            radButton_Save.Focus();
        }

        //Set data
        void SetData(DataTable dt)
        {

            id_lo = dt.Rows[0]["LOGISTICID"].ToString();
            date_lo = dt.Rows[0]["TRANSFERDATE"].ToString();
            car_lo = dt.Rows[0]["VEHICLEID"].ToString();
            emp_lo = dt.Rows[0]["EMPLDRIVER"].ToString();
            name_lo = dt.Rows[0]["SPC_NAME"].ToString();
            routed_lo = dt.Rows[0]["ROUTEID"].ToString() + " - " + dt.Rows[0]["NAME"].ToString();

            radLabel_Car.Text = car_lo + " [" + dt.Rows[0]["TRASHREMARK"].ToString() + @"]";
            radLabel_EmpDriver.Text = emp_lo + " - " + name_lo;
            radLabel_Rount.Text = routed_lo;
            radTextBox_LO1.Enabled = false; radTextBox_LO2.Enabled = false;
            radButton_Save.Enabled = true;
        }
        //Delete    
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (_pTypeOpen == "1") return;
                if (radGridView_Show.Rows.Count == 0) return;
                if (radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() == "") return;
                if (MsgBoxClass.MsgBoxShow_ConfirmDelete(radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() +
                    " - " + radGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString()) == DialogResult.No) return;

                //string sqlDel = string.Format(@"Delete SHOP_MNRD_ORDER WHERE LOGISTICID = '" + id_lo + @"' 
                //        AND BRANCH_ID = '" + radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + @"' ");

                string T = Logistic_Class.MNRD_DeleteRow(id_lo, radGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString());// ConnectionClass.ExecuteSQL_Main(sqlDel);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "") radGridView_Show.Rows.Remove(radGridView_Show.CurrentRow);

            }
        }

        private void RadTextBox_MN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (bch_id == "") { return; }

                string T = Logistic_Class.MNRD_InsertRow(bch_id, bch_Name, id_lo);// ConnectionClass.ExecuteSQL_Main(sqlIn);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    radGridView_Show.Rows.Add(bch_id, bch_Name);
                    bch_id = ""; bch_Name = ""; radLabel_Branch.Text = "";
                    RadTextBox_MN.Text = ""; RadTextBox_MN.Focus();
                }
            }
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pTypeOpen);
        }

        private void RadTextBox_MN_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_MN.Text.Length != 3) { radLabel_Branch.Text = ""; return; }
            if (RadTextBox_MN.Text == "000") { RadTextBox_MN.Text = ""; RadTextBox_MN.Focus(); return; }

            DataTable dtBch = BranchClass.GetDetailBranchByID("MN" + RadTextBox_MN.Text);
            if (dtBch.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("สาขาที่ระบุ"); RadTextBox_MN.Text = ""; RadTextBox_MN.Focus(); return;
            }
            int icount = 0;
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["BRANCH_ID"].Value.ToString() == dtBch.Rows[0]["BRANCH_ID"].ToString()) icount += 1;
            }
            if (icount > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สาขาที่ระบุมีอยู่แล้ว ไม่จำเป็นต้องเพิ่มอีก");
                bch_id = ""; bch_Name = ""; radLabel_Branch.Text = "";
                RadTextBox_MN.Text = ""; RadTextBox_MN.Focus();
                return;
            }

            bch_id = ""; bch_Name = "";
            bch_id = dtBch.Rows[0]["BRANCH_ID"].ToString(); bch_Name = dtBch.Rows[0]["BRANCH_NAME"].ToString();
            radLabel_Branch.Text = bch_Name;
        }

        //LO
        private void RadTextBox_LO2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if ((radTextBox_LO1.Text == "") || (radTextBox_LO1.Text.Length != 4)) return;
                if (radTextBox_LO2.Text == "") return;

                int lo2;
                try
                {
                    lo2 = Int32.Parse(radTextBox_LO2.Text);
                }
                catch (Exception) { return; }

                string lo = string.Format("{0:0000000}", lo2);
                radTextBox_LO2.Text = lo;
                lo = radTextBox_LO1.Text + "-" + lo;

                switch (_pTypeOpen)
                {
                    case "0":
                        CheckLO_MNRD(lo);
                        break;
                    case "1":
                        CheckLO_MNPM(lo);
                        break;
                    default:
                        break;
                }
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //print
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            PrintDocument_LO.PrintController = printController;
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument_LO.PrinterSettings = printDialog1.PrinterSettings;
                PrintDocument_LO.Print();
            }
        }
        //Set Focuss
        private void RadTextBox_LO1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radTextBox_LO2.Focus();
        }

        private void PrintDocument_LO_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string headTxt = "เอกสารรับซองส่งเงิน.";
            string line1 = "จำนวน[ซอง] ______________________________";
            string line2 = "ผู้ส่ง : เวลาส่ง ______________________________";
            string line3 = "พขร : เวลารับ ______________________________";

            if (_pTypeOpen == "0")
            {
                headTxt = "เอกสารเก็บขยะ - ตะกร้า.";
                line1 = "ขยะ-ตะกร้า ส่งคืน มี__________ไม่มี __________";
                line2 = "ปริมาณคงเหลือ ___________________________";
                line3 = "พนง.สาขา ________________________________";
            }

            barcode.Data = id_lo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            e.Graphics.DrawString(headTxt + "  " + id_lo, SystemClass.printFont, Brushes.Black, 0, Y);

            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่ LO " + date_lo + " / ทะเบียนรถ " + car_lo, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("พขร " + emp_lo, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("" + name_lo, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("สาย " + routed_lo, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                Y += 25;
                e.Graphics.DrawString((i + 1).ToString() + "." + radGridView_Show.Rows[i].Cells["BRANCH_ID"].Value.ToString() + " - " +
                                  radGridView_Show.Rows[i].Cells["BRANCH_NAME"].Value.ToString(), SystemClass.printFont, Brushes.Black, 1, Y);
                Y += 20;
                e.Graphics.DrawString(line1, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString(line2, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString(line3, SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 25;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
    }
}