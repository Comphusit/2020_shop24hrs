﻿namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    partial class CarCare_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarCare_Report));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radGridView_Image = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_Bill = new Telerik.WinControls.UI.RadDateTimePicker();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel_Area = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElement1 = new Telerik.WinControls.UI.RadLabelElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_OK = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Image = new Telerik.WinControls.UI.RadButtonElement();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            this.radStatusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(829, 634);
            this.radPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Image, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radGridView_Image
            // 
            this.radGridView_Image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Image.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Image.Location = new System.Drawing.Point(3, 256);
            // 
            // 
            // 
            this.radGridView_Image.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Image.Name = "radGridView_Image";
            // 
            // 
            // 
            this.radGridView_Image.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Image.Size = new System.Drawing.Size(823, 375);
            this.radGridView_Image.TabIndex = 6;
            this.radGridView_Image.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Image_ViewCellFormatting);
            this.radGridView_Image.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Image_CellDoubleClick);
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Controls.Add(this.radDateTimePicker1);
            this.radStatusStrip.Controls.Add(this.radDateTimePicker_Bill);
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButton_excel,
            this.commandBarSeparator5,
            this.radButton_Print,
            this.commandBarSeparator6,
            this.radLabel_Area,
            this.radLabelElement1,
            this.commandBarSeparator4,
            this.radButtonElement_OK,
            this.commandBarSeparator1,
            this.RadButtonElement_pdt,
            this.commandBarSeparator2,
            this.radButtonElement_Image});
            this.radStatusStrip.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(823, 42);
            this.radStatusStrip.TabIndex = 5;
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDateTimePicker1.Location = new System.Drawing.Point(304, 9);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(164, 21);
            this.radDateTimePicker1.TabIndex = 69;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "วันศุกร์ที่ 10 กรกฎาคม 2020";
            this.radDateTimePicker1.Value = new System.DateTime(2020, 7, 10, 11, 34, 58, 853);
            this.radDateTimePicker1.ValueChanged += new System.EventHandler(this.RadDateTimePicker1_ValueChanged);
            // 
            // radDateTimePicker_Bill
            // 
            this.radDateTimePicker_Bill.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Bill.EnableCodedUITests = true;
            this.radDateTimePicker_Bill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDateTimePicker_Bill.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_Bill.Location = new System.Drawing.Point(125, 9);
            this.radDateTimePicker_Bill.Name = "radDateTimePicker_Bill";
            this.radDateTimePicker_Bill.Size = new System.Drawing.Size(164, 21);
            this.radDateTimePicker_Bill.TabIndex = 68;
            this.radDateTimePicker_Bill.TabStop = false;
            this.radDateTimePicker_Bill.Text = "15/05/2020";
            this.radDateTimePicker_Bill.Value = new System.DateTime(2020, 5, 15, 0, 0, 0, 0);
            this.radDateTimePicker_Bill.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Bill_ValueChanged);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButton_excel
            // 
            this.radButton_excel.AutoSize = true;
            this.radButton_excel.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButton_excel.Name = "radButton_excel";
            this.radButton_excel.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_excel, false);
            this.radButton_excel.Text = "เพิ่ม";
            this.radButton_excel.ToolTipText = "เพิ่ม";
            this.radButton_excel.UseCompatibleTextRendering = false;
            this.radButton_excel.Click += new System.EventHandler(this.RadButton_excel_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator5.UseCompatibleTextRendering = false;
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButton_Print
            // 
            this.radButton_Print.AutoSize = true;
            this.radButton_Print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_Print.Name = "radButton_Print";
            this.radButton_Print.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_Print, false);
            this.radButton_Print.Text = "แก้ไข";
            this.radButton_Print.ToolTipText = "แก้ไข";
            this.radButton_Print.UseCompatibleTextRendering = false;
            this.radButton_Print.Click += new System.EventHandler(this.RadButton_Print_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator6.UseCompatibleTextRendering = false;
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // radLabel_Area
            // 
            this.radLabel_Area.AutoSize = false;
            this.radLabel_Area.Bounds = new System.Drawing.Rectangle(4, 0, 227, 32);
            this.radLabel_Area.Name = "radLabel_Area";
            this.radStatusStrip.SetSpring(this.radLabel_Area, false);
            this.radLabel_Area.Text = "วันที่ : ";
            this.radLabel_Area.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel_Area.TextWrap = true;
            this.radLabel_Area.UseCompatibleTextRendering = false;
            // 
            // radLabelElement1
            // 
            this.radLabelElement1.AutoSize = false;
            this.radLabelElement1.Bounds = new System.Drawing.Rectangle(0, 0, 170, 32);
            this.radLabelElement1.Name = "radLabelElement1";
            this.radStatusStrip.SetSpring(this.radLabelElement1, false);
            this.radLabelElement1.Text = "";
            this.radLabelElement1.TextWrap = true;
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_OK
            // 
            this.radButtonElement_OK.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButtonElement_OK.Name = "radButtonElement_OK";
            this.radStatusStrip.SetSpring(this.radButtonElement_OK, false);
            this.radButtonElement_OK.Text = "ดึงข้อมูล";
            this.radButtonElement_OK.Click += new System.EventHandler(this.RadButtonElement_OK_Click);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButtonElement_OK.GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButtonElement_OK.GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButtonElement_OK.GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButtonElement_OK.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Image
            // 
            this.radButtonElement_Image.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Image.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Image.Name = "radButtonElement_Image";
            this.radStatusStrip.SetSpring(this.radButtonElement_Image, false);
            this.radButtonElement_Image.Text = "radButtonElement1";
            this.radButtonElement_Image.Click += new System.EventHandler(this.RadButtonElement_Image_Click);
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.BackgroundImage = global::PC_Shop24Hrs.Properties.Resources._24hrs;
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(796, 188);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.SelectionChanged += new System.EventHandler(this.RadGridView_Show_SelectionChanged);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // CarCare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CarCare";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ล้างรถ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CarCare_Report_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            this.radStatusStrip.ResumeLayout(false);
            this.radStatusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Bill;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButton_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadButtonElement radButton_Print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.RadLabelElement radLabel_Area;
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_OK;
        private Telerik.WinControls.UI.RadGridView radGridView_Image;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Image;
    }
}
