﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    public partial class CarCare_Report : Telerik.WinControls.UI.RadForm
    {
        readonly string _pCase; //0 รายงานละเอียด 1 รายงานสรุป ออกแต่รูป
        private DataTable dt = new DataTable();
        //Load
        public CarCare_Report(string pCase)
        {
            InitializeComponent();

            _pCase = pCase;

            radButton_Print.ShowBorder = true; radButton_Print.ToolTipText = "พิมพ์รูปภาพ";
            radButton_excel.ShowBorder = true; radButton_excel.ToolTipText = "Export Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Image.ShowBorder = true; radButtonElement_Image.ToolTipText = "Export รูปภาพ";

            radStatusStrip.SizingGrip = false;
            radButtonElement_OK.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultRadGridView(radGridView_Image);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Bill, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker1, DateTime.Now, DateTime.Now);

            if (_pCase == "0")
            {
                radButton_Print.Enabled = true; radButton_excel.Enabled = true;
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Createdate", "วันที่ล้าง", 130));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VehicleName", "ประเภทรถ", 250));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DimensionCar", "แผนก", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 210));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID_1", "พนักงานล้าง 1", 170));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID_2", "พนักงานล้าง 2", 170));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID_3", "พนักงานล้าง 3", 170));

                tableLayoutPanel2.RowStyles[1].SizeType = SizeType.Percent; tableLayoutPanel2.RowStyles[1].Height = 35;

                tableLayoutPanel2.RowStyles[2].SizeType = SizeType.Percent; tableLayoutPanel2.RowStyles[2].Height = 65;
            }
            else
            {
                radButton_Print.Enabled = false; radButton_excel.Enabled = false;

                tableLayoutPanel2.RowStyles[1].SizeType = SizeType.Percent; tableLayoutPanel2.RowStyles[1].Height = 0;
                tableLayoutPanel2.RowStyles[2].SizeType = SizeType.Percent; tableLayoutPanel2.RowStyles[2].Height = 100;
            }

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESC", "คำอธิบาย", 180));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "รูปถ่ายภายใน", 200));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG1", "C_IMG1"));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "รูปถ่ายด้านหลัง", 200));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG2", "C_IMG2"));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG3", "รูปถ่ายด้านหน้า", 200));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG3", "P_IMG3"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG3", "C_IMG3"));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG4", "รูปถ่ายด้านขวา", 200));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG4", "P_IMG4"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG4", "C_IMG4"));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG5", "รูปถ่ายด้านซ้าย", 200));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG5", "P_IMG5"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG5", "C_IMG5"));
            radGridView_Image.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Image.TableElement.RowHeight = 250;
            radGridView_Image.TableElement.TableHeaderHeight = 70;
        }
        //Load Main
        private void CarCare_Report_Load(object sender, EventArgs e)
        {
            radDateTimePicker_Bill.Value = DateTime.Now.AddDays(-7);
            radDateTimePicker1.Value = DateTime.Now.AddDays(0);
        }
        //Set Valus
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt.Rows.Count > 0) { dt.Rows.Clear(); }
            if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();

            dt = LogisticClass.CARCARE_WashReport(radDateTimePicker_Bill.Value.ToString("yyyy-MM-dd"), radDateTimePicker1.Value.ToString("yyyy-MM-dd"));//  ConnectionClass.SelectSQL_Main(sql);

            if (_pCase == "0")
            {
                if (RadGridView_Show.Rows.Count > 0) { RadGridView_Show.Rows.Clear(); }
                RadGridView_Show.DataSource = dt;
            }

            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                DataTable dtemp = Logistic_Class.EmpVehicleWash_FindData(dt.Rows[i]["YEARMONTHDAY"].ToString(), dt.Rows[i]["VehicleID"].ToString()); //ConnectionClass.SelectSQL_Main(findEmp);

                String[] EmpID1 = new string[5];
                EmpID1[0] = ""; EmpID1[1] = ""; EmpID1[2] = ""; EmpID1[3] = ""; EmpID1[4] = "";
                for (int ik = 0; ik < dtemp.Rows.Count; ik++)
                {
                    EmpID1[ik] = dtemp.Rows[ik]["EMPLID"].ToString() + "-" + dtemp.Rows[ik]["SPC_NAME"].ToString();
                }

                if (_pCase == "0")
                {
                    RadGridView_Show.Rows[i].Cells["EMPLID_1"].Value = EmpID1[0];//pEmpID.Substring(0, pEmpID.Length - 2);
                    RadGridView_Show.Rows[i].Cells["EMPLID_2"].Value = EmpID1[1];//pEmpName.Substring(0, pEmpName.Length - 2);
                    RadGridView_Show.Rows[i].Cells["EMPLID_3"].Value = EmpID1[2];
                }
                else
                {
                    string emp = "ผู้ล้าง " + Environment.NewLine + EmpID1[0] + Environment.NewLine + EmpID1[1] + Environment.NewLine + EmpID1[2];

                    FindImage(dt.Rows[i]["Createdate"].ToString(), dt.Rows[i]["VEHICLEID"].ToString(), emp);
                }
            }
            this.Cursor = Cursors.Default;
        }


        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //OK
        private void RadButtonElement_OK_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Selection Change
        private void RadGridView_Show_SelectionChanged(object sender, EventArgs e)
        {
            if (_pCase == "0")
            {
                if (RadGridView_Show.Rows.Count == 0) return;

                try
                {
                    radGridView_Image.Columns["DESC"].HeaderText = "วันที่ล้าง " + RadGridView_Show.CurrentRow.Cells["Createdate"].Value.ToString() +
                        Environment.NewLine + "ทะเบียนรถ " + RadGridView_Show.CurrentRow.Cells["VEHICLEID"].Value.ToString();
                    string emp = "ผู้ล้าง " +
                        Environment.NewLine + RadGridView_Show.CurrentRow.Cells["EMPLID_1"].Value.ToString() +
                        Environment.NewLine + RadGridView_Show.CurrentRow.Cells["EMPLID_2"].Value.ToString() +
                        Environment.NewLine + RadGridView_Show.CurrentRow.Cells["EMPLID_3"].Value.ToString();
                    FindImage(RadGridView_Show.CurrentRow.Cells["Createdate"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["VEHICLEID"].Value.ToString(), emp);
                }
                catch (Exception)
                {
                    if (radGridView_Image.Rows.Count > 0)
                    { radGridView_Image.Rows.Clear(); }
                    return;
                }
            }
        }
        //Find Image
        void FindImage(string pDate, string pCarID, string pEmp)
        {
            if (_pCase == "0")
            { if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear(); }

            string pathImage = PathImageClass.pPathCarwash + pDate.Substring(0, 7) + @"\" + pDate + @"\" + pCarID;

            DirectoryInfo DirInfo = new DirectoryInfo(pathImage);
            if (DirInfo.Exists == false) return;

            string pWantBefore1 = "*WIN-BF.JPG", pWantBefore2 = "*WCB-BF.JPG", pWantBefore3 = "*WCF-BF.JPG", pWantBefore4 = "*WCR-BF.JPG", pWantBefore5 = "*WCL-BF.JPG";

            string pathFullNameBefore1 = PathImageClass.pImageEmply, pathFullNameBefore2 = PathImageClass.pImageEmply,
                           pathFullNameBefore3 = PathImageClass.pImageEmply, pathFullNameBefore4 = PathImageClass.pImageEmply, pathFullNameBefore5 = PathImageClass.pImageEmply;

            string pCountBefore1 = "0", pCountBefore2 = "0", pCountBefore3 = "0", pCountBefore4 = "0", pCountBefore5 = "0";
            //Before
            FileInfo[] FilesBefore1 = DirInfo.GetFiles(pWantBefore1, SearchOption.AllDirectories);
            if (FilesBefore1.Length > 0)
            {
                pathFullNameBefore1 = FilesBefore1[0].FullName; pCountBefore1 = "1";
            }

            FileInfo[] FilesBefore2 = DirInfo.GetFiles(pWantBefore2, SearchOption.AllDirectories);
            if (FilesBefore2.Length > 0)
            {
                pathFullNameBefore2 = FilesBefore2[0].FullName; pCountBefore2 = "1";
            }

            FileInfo[] FilesBefore3 = DirInfo.GetFiles(pWantBefore3, SearchOption.AllDirectories);
            if (FilesBefore3.Length > 0)
            {
                pathFullNameBefore3 = FilesBefore3[0].FullName; pCountBefore3 = "1";
            }

            FileInfo[] FilesBefore4 = DirInfo.GetFiles(pWantBefore4, SearchOption.AllDirectories);
            if (FilesBefore4.Length > 0)
            {
                pathFullNameBefore4 = FilesBefore4[0].FullName; pCountBefore4 = "1";
            }

            FileInfo[] FilesBefore5 = DirInfo.GetFiles(pWantBefore5, SearchOption.AllDirectories);
            if (FilesBefore5.Length > 0)
            {
                pathFullNameBefore5 = FilesBefore5[0].FullName; pCountBefore5 = "1";
            }

            radGridView_Image.Rows.Add("ก่อนล้างรถ " + Environment.NewLine + pDate + Environment.NewLine + pCarID + Environment.NewLine + pEmp,
                   ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBefore1), pathImage + @"|" + pWantBefore1, pCountBefore1,
                   ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBefore2), pathImage + @"|" + pWantBefore2, pCountBefore2,
                   ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBefore3), pathImage + @"|" + pWantBefore3, pCountBefore3,
                   ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBefore4), pathImage + @"|" + pWantBefore4, pCountBefore4,
                   ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBefore5), pathImage + @"|" + pWantBefore5, pCountBefore5);

            //Back
            string pWantBack1 = "*WIN-AF.JPG", pWantBack2 = "*WCB-AF.JPG", pWantBack3 = "*WCF-AF.JPG", pWantBack4 = "*WCR-AF.JPG", pWantBack5 = "*WCL-AF.JPG";
            string pathFullNameBack1 = PathImageClass.pImageEmply, pathFullNameBack2 = PathImageClass.pImageEmply,
                           pathFullNameBack3 = PathImageClass.pImageEmply, pathFullNameBack4 = PathImageClass.pImageEmply, pathFullNameBack5 = PathImageClass.pImageEmply;
            string pCountBack1 = "0", pCountBack2 = "0", pCountBack3 = "0", pCountBack4 = "0", pCountBack5 = "0";

            FileInfo[] FilesBack1 = DirInfo.GetFiles(pWantBack1, SearchOption.AllDirectories);
            if (FilesBack1.Length > 0)
            {
                pathFullNameBack1 = FilesBack1[0].FullName;
                pCountBack1 = "1";
            }

            FileInfo[] FilesBack2 = DirInfo.GetFiles(pWantBack2, SearchOption.AllDirectories);
            if (FilesBack2.Length > 0)
            {
                pathFullNameBack2 = FilesBack2[0].FullName;
                pCountBack2 = "1";
            }

            FileInfo[] FilesBack3 = DirInfo.GetFiles(pWantBack3, SearchOption.AllDirectories);
            if (FilesBack3.Length > 0)
            {
                pathFullNameBack3 = FilesBack3[0].FullName;
                pCountBack3 = "1";
            }

            FileInfo[] FilesBack4 = DirInfo.GetFiles(pWantBack4, SearchOption.AllDirectories);
            if (FilesBack4.Length > 0)
            {
                pathFullNameBack4 = FilesBack4[0].FullName;
                pCountBack4 = "1";
            }

            FileInfo[] FilesBack5 = DirInfo.GetFiles(pWantBack5, SearchOption.AllDirectories);
            if (FilesBack5.Length > 0)
            {
                pathFullNameBack5 = FilesBack5[0].FullName;
                pCountBack5 = "1";
            }

            radGridView_Image.Rows.Add("หลังล้างรถ" + Environment.NewLine + pDate + Environment.NewLine + pCarID + Environment.NewLine + pEmp,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBack1), pathImage + @"|" + pWantBack1, pCountBack1,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBack2), pathImage + @"|" + pWantBack2, pCountBack2,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBack3), pathImage + @"|" + pWantBack3, pCountBack3,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBack4), pathImage + @"|" + pWantBack4, pCountBack4,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameBack5), pathImage + @"|" + pWantBack5, pCountBack5);
        }
        //print
        private void RadButton_Print_Click(object sender, EventArgs e)
        {
            if (radGridView_Image.Rows.Count == 0) return;

            radGridView_Image.PrintPreview();
        }
        //Image Set Format
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        //DoubleClick
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "IMG1":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG1"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    break;
                case "IMG2":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG2"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG2"].Value.ToString());
                    break;
                case "IMG3":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG3"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG3"].Value.ToString());
                    break;
                case "IMG4":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG4"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG4"].Value.ToString());
                    break;
                case "IMG5":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG5"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG5"].Value.ToString());
                    break;
                default:
                    break;
            }
        }

        private void RadDateTimePicker_Bill_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Bill, radDateTimePicker1);
        }

        private void RadDateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Bill, radDateTimePicker1);
        }

        private void RadButton_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("ข้อมูลการล้างรถ", RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        //Export Image
        private void RadButtonElement_Image_Click(object sender, EventArgs e)
        {
            string nameFile = "รายงานล้างรถ";
            if (_pCase == "0")
            {
                nameFile = RadGridView_Show.CurrentRow.Cells["VEHICLEID"].Value.ToString() + " [" + RadGridView_Show.CurrentRow.Cells["Createdate"].Value.ToString() + "]";
            }

            if (radGridView_Image.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView(nameFile, radGridView_Image, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}
