﻿namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    partial class CarCare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarCare));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel101 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel102 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel103 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName19 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName18 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName17 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName16 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName15 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName14 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName13 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName12 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName7 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName6 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName3 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName10 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName9 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName8 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName5 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName4 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabelDptID1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelDptName1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarID1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCarName1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch18 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch17 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch16 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch15 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch14 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch13 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch12 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch10 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch9 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch8 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch7 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch6 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch5 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch4 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch3 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch2 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch1 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch19 = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_Car = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radButtonElement11_pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radDateTimePicker_D1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_D2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel103)).BeginInit();
            this.tableLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName19)).BeginInit();
            this.tableLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName18)).BeginInit();
            this.tableLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName17)).BeginInit();
            this.tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName16)).BeginInit();
            this.tableLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName15)).BeginInit();
            this.tableLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName14)).BeginInit();
            this.tableLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName13)).BeginInit();
            this.tableLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName12)).BeginInit();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName7)).BeginInit();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName6)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName3)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName2)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName10)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName9)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName8)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName5)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName4)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.radStatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Car)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 705);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(868, 699);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 677);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(862, 19);
            this.radLabel_Detail.TabIndex = 53;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel23, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel22, 2, 19);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel21, 2, 18);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel20, 2, 17);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel19, 2, 16);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel18, 2, 15);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel17, 2, 14);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel16, 2, 13);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel15, 2, 12);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel13, 2, 7);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel12, 2, 6);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel11, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel10, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel9, 2, 10);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel8, 2, 9);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel6, 2, 5);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel2, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.radLabel22, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch18, 1, 18);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch17, 1, 17);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch16, 1, 16);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch15, 1, 15);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch14, 1, 14);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch13, 1, 13);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch12, 1, 12);
            this.tableLayoutPanel4.Controls.Add(this.radLabel20, 1, 11);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch10, 1, 10);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch9, 1, 9);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch8, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch7, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch6, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch5, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch4, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch3, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch2, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.radToggleSwitch19, 1, 19);
            this.tableLayoutPanel4.Controls.Add(this.radLabel19, 0, 19);
            this.tableLayoutPanel4.Controls.Add(this.radLabel18, 0, 18);
            this.tableLayoutPanel4.Controls.Add(this.radLabel17, 0, 17);
            this.tableLayoutPanel4.Controls.Add(this.radLabel16, 0, 16);
            this.tableLayoutPanel4.Controls.Add(this.radLabel15, 0, 15);
            this.tableLayoutPanel4.Controls.Add(this.radLabel14, 0, 14);
            this.tableLayoutPanel4.Controls.Add(this.radLabel13, 0, 13);
            this.tableLayoutPanel4.Controls.Add(this.radLabel12, 0, 12);
            this.tableLayoutPanel4.Controls.Add(this.radLabel11, 0, 11);
            this.tableLayoutPanel4.Controls.Add(this.radLabel10, 0, 10);
            this.tableLayoutPanel4.Controls.Add(this.radLabel9, 0, 9);
            this.tableLayoutPanel4.Controls.Add(this.radLabel8, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.radLabel7, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.radLabel6, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.radLabel5, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.radLabel4, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.radLabel3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.radLabel1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.radLabel2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.radLabel24, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 53);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 21;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.988122F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.262239F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.986049F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.98678F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(862, 618);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 4;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Controls.Add(this.radLabel27, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.radLabel101, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.radLabel102, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.radLabel103, 0, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(203, 3);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel23.TabIndex = 120;
            // 
            // radLabel27
            // 
            this.radLabel27.AutoSize = false;
            this.radLabel27.BorderVisible = true;
            this.radLabel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel27.ForeColor = System.Drawing.Color.Black;
            this.radLabel27.Location = new System.Drawing.Point(283, 3);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(74, 18);
            this.radLabel27.TabIndex = 105;
            this.radLabel27.Text = "แผนก";
            // 
            // radLabel101
            // 
            this.radLabel101.AutoSize = false;
            this.radLabel101.BorderVisible = true;
            this.radLabel101.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel101.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel101.ForeColor = System.Drawing.Color.Black;
            this.radLabel101.Location = new System.Drawing.Point(363, 3);
            this.radLabel101.Name = "radLabel101";
            this.radLabel101.Size = new System.Drawing.Size(290, 18);
            this.radLabel101.TabIndex = 104;
            this.radLabel101.Text = "ชื่อแผนก";
            // 
            // radLabel102
            // 
            this.radLabel102.AutoSize = false;
            this.radLabel102.BorderVisible = true;
            this.radLabel102.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel102.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel102.ForeColor = System.Drawing.Color.Black;
            this.radLabel102.Location = new System.Drawing.Point(3, 3);
            this.radLabel102.Name = "radLabel102";
            this.radLabel102.Size = new System.Drawing.Size(74, 18);
            this.radLabel102.TabIndex = 103;
            this.radLabel102.Text = "ทะเบียน";
            // 
            // radLabel103
            // 
            this.radLabel103.AutoSize = false;
            this.radLabel103.BorderVisible = true;
            this.radLabel103.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel103.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel103.ForeColor = System.Drawing.Color.Black;
            this.radLabel103.Location = new System.Drawing.Point(83, 3);
            this.radLabel103.Name = "radLabel103";
            this.radLabel103.Size = new System.Drawing.Size(194, 18);
            this.radLabel103.TabIndex = 102;
            this.radLabel103.Text = "ชื่อรถ";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 4;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.radLabelDptID19, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.radLabelDptName19, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.radLabelCarID19, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.radLabelCarName19, 0, 0);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(203, 575);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel22.TabIndex = 119;
            // 
            // radLabelDptID19
            // 
            this.radLabelDptID19.AutoSize = false;
            this.radLabelDptID19.BorderVisible = true;
            this.radLabelDptID19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID19.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID19.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID19.Name = "radLabelDptID19";
            this.radLabelDptID19.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID19.TabIndex = 105;
            this.radLabelDptID19.Text = "D156";
            // 
            // radLabelDptName19
            // 
            this.radLabelDptName19.AutoSize = false;
            this.radLabelDptName19.BorderVisible = true;
            this.radLabelDptName19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName19.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName19.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName19.Name = "radLabelDptName19";
            this.radLabelDptName19.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName19.TabIndex = 104;
            this.radLabelDptName19.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID19
            // 
            this.radLabelCarID19.AutoSize = false;
            this.radLabelCarID19.BorderVisible = true;
            this.radLabelCarID19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID19.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID19.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID19.Name = "radLabelCarID19";
            this.radLabelCarID19.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID19.TabIndex = 103;
            this.radLabelCarID19.Text = "กอ-8991";
            // 
            // radLabelCarName19
            // 
            this.radLabelCarName19.AutoSize = false;
            this.radLabelCarName19.BorderVisible = true;
            this.radLabelCarName19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName19.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName19.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName19.Name = "radLabelCarName19";
            this.radLabelCarName19.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName19.TabIndex = 102;
            this.radLabelCarName19.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 4;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Controls.Add(this.radLabelDptID18, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.radLabelDptName18, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.radLabelCarID18, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.radLabelCarName18, 0, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(203, 545);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel21.TabIndex = 118;
            // 
            // radLabelDptID18
            // 
            this.radLabelDptID18.AutoSize = false;
            this.radLabelDptID18.BorderVisible = true;
            this.radLabelDptID18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID18.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID18.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID18.Name = "radLabelDptID18";
            this.radLabelDptID18.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID18.TabIndex = 105;
            this.radLabelDptID18.Text = "D156";
            // 
            // radLabelDptName18
            // 
            this.radLabelDptName18.AutoSize = false;
            this.radLabelDptName18.BorderVisible = true;
            this.radLabelDptName18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName18.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName18.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName18.Name = "radLabelDptName18";
            this.radLabelDptName18.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName18.TabIndex = 104;
            this.radLabelDptName18.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID18
            // 
            this.radLabelCarID18.AutoSize = false;
            this.radLabelCarID18.BorderVisible = true;
            this.radLabelCarID18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID18.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID18.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID18.Name = "radLabelCarID18";
            this.radLabelCarID18.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID18.TabIndex = 103;
            this.radLabelCarID18.Text = "กอ-8991";
            // 
            // radLabelCarName18
            // 
            this.radLabelCarName18.AutoSize = false;
            this.radLabelCarName18.BorderVisible = true;
            this.radLabelCarName18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName18.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName18.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName18.Name = "radLabelCarName18";
            this.radLabelCarName18.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName18.TabIndex = 102;
            this.radLabelCarName18.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 4;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Controls.Add(this.radLabelDptID17, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.radLabelDptName17, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.radLabelCarID17, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.radLabelCarName17, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(203, 515);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel20.TabIndex = 117;
            // 
            // radLabelDptID17
            // 
            this.radLabelDptID17.AutoSize = false;
            this.radLabelDptID17.BorderVisible = true;
            this.radLabelDptID17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID17.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID17.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID17.Name = "radLabelDptID17";
            this.radLabelDptID17.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID17.TabIndex = 105;
            this.radLabelDptID17.Text = "D156";
            // 
            // radLabelDptName17
            // 
            this.radLabelDptName17.AutoSize = false;
            this.radLabelDptName17.BorderVisible = true;
            this.radLabelDptName17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName17.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName17.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName17.Name = "radLabelDptName17";
            this.radLabelDptName17.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName17.TabIndex = 104;
            this.radLabelDptName17.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID17
            // 
            this.radLabelCarID17.AutoSize = false;
            this.radLabelCarID17.BorderVisible = true;
            this.radLabelCarID17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID17.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID17.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID17.Name = "radLabelCarID17";
            this.radLabelCarID17.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID17.TabIndex = 103;
            this.radLabelCarID17.Text = "กอ-8991";
            // 
            // radLabelCarName17
            // 
            this.radLabelCarName17.AutoSize = false;
            this.radLabelCarName17.BorderVisible = true;
            this.radLabelCarName17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName17.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName17.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName17.Name = "radLabelCarName17";
            this.radLabelCarName17.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName17.TabIndex = 102;
            this.radLabelCarName17.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 4;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.radLabelDptID16, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.radLabelDptName16, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.radLabelCarID16, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.radLabelCarName16, 0, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(203, 485);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel19.TabIndex = 116;
            // 
            // radLabelDptID16
            // 
            this.radLabelDptID16.AutoSize = false;
            this.radLabelDptID16.BorderVisible = true;
            this.radLabelDptID16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID16.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID16.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID16.Name = "radLabelDptID16";
            this.radLabelDptID16.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID16.TabIndex = 105;
            this.radLabelDptID16.Text = "D156";
            // 
            // radLabelDptName16
            // 
            this.radLabelDptName16.AutoSize = false;
            this.radLabelDptName16.BorderVisible = true;
            this.radLabelDptName16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName16.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName16.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName16.Name = "radLabelDptName16";
            this.radLabelDptName16.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName16.TabIndex = 104;
            this.radLabelDptName16.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID16
            // 
            this.radLabelCarID16.AutoSize = false;
            this.radLabelCarID16.BorderVisible = true;
            this.radLabelCarID16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID16.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID16.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID16.Name = "radLabelCarID16";
            this.radLabelCarID16.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID16.TabIndex = 103;
            this.radLabelCarID16.Text = "กอ-8991";
            // 
            // radLabelCarName16
            // 
            this.radLabelCarName16.AutoSize = false;
            this.radLabelCarName16.BorderVisible = true;
            this.radLabelCarName16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName16.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName16.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName16.Name = "radLabelCarName16";
            this.radLabelCarName16.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName16.TabIndex = 102;
            this.radLabelCarName16.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 4;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.radLabelDptID15, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.radLabelDptName15, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.radLabelCarID15, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.radLabelCarName15, 0, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(203, 455);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel18.TabIndex = 115;
            // 
            // radLabelDptID15
            // 
            this.radLabelDptID15.AutoSize = false;
            this.radLabelDptID15.BorderVisible = true;
            this.radLabelDptID15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID15.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID15.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID15.Name = "radLabelDptID15";
            this.radLabelDptID15.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID15.TabIndex = 105;
            this.radLabelDptID15.Text = "D156";
            // 
            // radLabelDptName15
            // 
            this.radLabelDptName15.AutoSize = false;
            this.radLabelDptName15.BorderVisible = true;
            this.radLabelDptName15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName15.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName15.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName15.Name = "radLabelDptName15";
            this.radLabelDptName15.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName15.TabIndex = 104;
            this.radLabelDptName15.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID15
            // 
            this.radLabelCarID15.AutoSize = false;
            this.radLabelCarID15.BorderVisible = true;
            this.radLabelCarID15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID15.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID15.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID15.Name = "radLabelCarID15";
            this.radLabelCarID15.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID15.TabIndex = 103;
            this.radLabelCarID15.Text = "กอ-8991";
            // 
            // radLabelCarName15
            // 
            this.radLabelCarName15.AutoSize = false;
            this.radLabelCarName15.BorderVisible = true;
            this.radLabelCarName15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName15.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName15.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName15.Name = "radLabelCarName15";
            this.radLabelCarName15.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName15.TabIndex = 102;
            this.radLabelCarName15.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 4;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Controls.Add(this.radLabelDptID14, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.radLabelDptName14, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.radLabelCarID14, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.radLabelCarName14, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(203, 425);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel17.TabIndex = 114;
            // 
            // radLabelDptID14
            // 
            this.radLabelDptID14.AutoSize = false;
            this.radLabelDptID14.BorderVisible = true;
            this.radLabelDptID14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID14.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID14.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID14.Name = "radLabelDptID14";
            this.radLabelDptID14.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID14.TabIndex = 105;
            this.radLabelDptID14.Text = "D156";
            // 
            // radLabelDptName14
            // 
            this.radLabelDptName14.AutoSize = false;
            this.radLabelDptName14.BorderVisible = true;
            this.radLabelDptName14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName14.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName14.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName14.Name = "radLabelDptName14";
            this.radLabelDptName14.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName14.TabIndex = 104;
            this.radLabelDptName14.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID14
            // 
            this.radLabelCarID14.AutoSize = false;
            this.radLabelCarID14.BorderVisible = true;
            this.radLabelCarID14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID14.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID14.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID14.Name = "radLabelCarID14";
            this.radLabelCarID14.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID14.TabIndex = 103;
            this.radLabelCarID14.Text = "กอ-8991";
            // 
            // radLabelCarName14
            // 
            this.radLabelCarName14.AutoSize = false;
            this.radLabelCarName14.BorderVisible = true;
            this.radLabelCarName14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName14.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName14.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName14.Name = "radLabelCarName14";
            this.radLabelCarName14.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName14.TabIndex = 102;
            this.radLabelCarName14.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 4;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.radLabelDptID13, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.radLabelDptName13, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.radLabelCarID13, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.radLabelCarName13, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(203, 395);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel16.TabIndex = 113;
            // 
            // radLabelDptID13
            // 
            this.radLabelDptID13.AutoSize = false;
            this.radLabelDptID13.BorderVisible = true;
            this.radLabelDptID13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID13.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID13.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID13.Name = "radLabelDptID13";
            this.radLabelDptID13.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID13.TabIndex = 105;
            this.radLabelDptID13.Text = "D156";
            // 
            // radLabelDptName13
            // 
            this.radLabelDptName13.AutoSize = false;
            this.radLabelDptName13.BorderVisible = true;
            this.radLabelDptName13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName13.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName13.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName13.Name = "radLabelDptName13";
            this.radLabelDptName13.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName13.TabIndex = 104;
            this.radLabelDptName13.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID13
            // 
            this.radLabelCarID13.AutoSize = false;
            this.radLabelCarID13.BorderVisible = true;
            this.radLabelCarID13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID13.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID13.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID13.Name = "radLabelCarID13";
            this.radLabelCarID13.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID13.TabIndex = 103;
            this.radLabelCarID13.Text = "กอ-8991";
            // 
            // radLabelCarName13
            // 
            this.radLabelCarName13.AutoSize = false;
            this.radLabelCarName13.BorderVisible = true;
            this.radLabelCarName13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName13.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName13.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName13.Name = "radLabelCarName13";
            this.radLabelCarName13.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName13.TabIndex = 102;
            this.radLabelCarName13.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 4;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.radLabelDptID12, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.radLabelDptName12, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.radLabelCarID12, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.radLabelCarName12, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(203, 365);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel15.TabIndex = 112;
            // 
            // radLabelDptID12
            // 
            this.radLabelDptID12.AutoSize = false;
            this.radLabelDptID12.BorderVisible = true;
            this.radLabelDptID12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID12.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID12.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID12.Name = "radLabelDptID12";
            this.radLabelDptID12.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID12.TabIndex = 105;
            this.radLabelDptID12.Text = "D156";
            // 
            // radLabelDptName12
            // 
            this.radLabelDptName12.AutoSize = false;
            this.radLabelDptName12.BorderVisible = true;
            this.radLabelDptName12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName12.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName12.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName12.Name = "radLabelDptName12";
            this.radLabelDptName12.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName12.TabIndex = 104;
            this.radLabelDptName12.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID12
            // 
            this.radLabelCarID12.AutoSize = false;
            this.radLabelCarID12.BorderVisible = true;
            this.radLabelCarID12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID12.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID12.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID12.Name = "radLabelCarID12";
            this.radLabelCarID12.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID12.TabIndex = 103;
            this.radLabelCarID12.Text = "กอ-8991";
            // 
            // radLabelCarName12
            // 
            this.radLabelCarName12.AutoSize = false;
            this.radLabelCarName12.BorderVisible = true;
            this.radLabelCarName12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName12.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName12.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName12.Name = "radLabelCarName12";
            this.radLabelCarName12.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName12.TabIndex = 102;
            this.radLabelCarName12.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.radLabelDptID7, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.radLabelDptName7, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.radLabelCarID7, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.radLabelCarName7, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(203, 213);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel13.TabIndex = 110;
            // 
            // radLabelDptID7
            // 
            this.radLabelDptID7.AutoSize = false;
            this.radLabelDptID7.BorderVisible = true;
            this.radLabelDptID7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID7.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID7.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID7.Name = "radLabelDptID7";
            this.radLabelDptID7.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID7.TabIndex = 105;
            this.radLabelDptID7.Text = "D156";
            // 
            // radLabelDptName7
            // 
            this.radLabelDptName7.AutoSize = false;
            this.radLabelDptName7.BorderVisible = true;
            this.radLabelDptName7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName7.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName7.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName7.Name = "radLabelDptName7";
            this.radLabelDptName7.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName7.TabIndex = 104;
            this.radLabelDptName7.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID7
            // 
            this.radLabelCarID7.AutoSize = false;
            this.radLabelCarID7.BorderVisible = true;
            this.radLabelCarID7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID7.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID7.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID7.Name = "radLabelCarID7";
            this.radLabelCarID7.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID7.TabIndex = 103;
            this.radLabelCarID7.Text = "กอ-8991";
            // 
            // radLabelCarName7
            // 
            this.radLabelCarName7.AutoSize = false;
            this.radLabelCarName7.BorderVisible = true;
            this.radLabelCarName7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName7.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName7.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName7.Name = "radLabelCarName7";
            this.radLabelCarName7.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName7.TabIndex = 102;
            this.radLabelCarName7.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 4;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.radLabelDptID6, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.radLabelDptName6, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.radLabelCarID6, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.radLabelCarName6, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(203, 183);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel12.TabIndex = 109;
            // 
            // radLabelDptID6
            // 
            this.radLabelDptID6.AutoSize = false;
            this.radLabelDptID6.BorderVisible = true;
            this.radLabelDptID6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID6.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID6.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID6.Name = "radLabelDptID6";
            this.radLabelDptID6.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID6.TabIndex = 105;
            // 
            // radLabelDptName6
            // 
            this.radLabelDptName6.AutoSize = false;
            this.radLabelDptName6.BorderVisible = true;
            this.radLabelDptName6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName6.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName6.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName6.Name = "radLabelDptName6";
            this.radLabelDptName6.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName6.TabIndex = 104;
            // 
            // radLabelCarID6
            // 
            this.radLabelCarID6.AutoSize = false;
            this.radLabelCarID6.BorderVisible = true;
            this.radLabelCarID6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID6.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID6.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID6.Name = "radLabelCarID6";
            this.radLabelCarID6.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID6.TabIndex = 103;
            // 
            // radLabelCarName6
            // 
            this.radLabelCarName6.AutoSize = false;
            this.radLabelCarName6.BorderVisible = true;
            this.radLabelCarName6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName6.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName6.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName6.Name = "radLabelCarName6";
            this.radLabelCarName6.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName6.TabIndex = 102;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 4;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.radLabelDptID3, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.radLabelDptName3, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.radLabelCarID3, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.radLabelCarName3, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(203, 93);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel11.TabIndex = 108;
            // 
            // radLabelDptID3
            // 
            this.radLabelDptID3.AutoSize = false;
            this.radLabelDptID3.BorderVisible = true;
            this.radLabelDptID3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID3.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID3.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID3.Name = "radLabelDptID3";
            this.radLabelDptID3.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID3.TabIndex = 105;
            this.radLabelDptID3.Text = "D156";
            // 
            // radLabelDptName3
            // 
            this.radLabelDptName3.AutoSize = false;
            this.radLabelDptName3.BorderVisible = true;
            this.radLabelDptName3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName3.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName3.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName3.Name = "radLabelDptName3";
            this.radLabelDptName3.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName3.TabIndex = 104;
            this.radLabelDptName3.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID3
            // 
            this.radLabelCarID3.AutoSize = false;
            this.radLabelCarID3.BorderVisible = true;
            this.radLabelCarID3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID3.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID3.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID3.Name = "radLabelCarID3";
            this.radLabelCarID3.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID3.TabIndex = 103;
            this.radLabelCarID3.Text = "กอ-8991";
            // 
            // radLabelCarName3
            // 
            this.radLabelCarName3.AutoSize = false;
            this.radLabelCarName3.BorderVisible = true;
            this.radLabelCarName3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName3.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName3.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName3.Name = "radLabelCarName3";
            this.radLabelCarName3.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName3.TabIndex = 102;
            this.radLabelCarName3.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 4;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.radLabelDptID2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.radLabelDptName2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.radLabelCarID2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.radLabelCarName2, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(203, 63);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel10.TabIndex = 107;
            // 
            // radLabelDptID2
            // 
            this.radLabelDptID2.AutoSize = false;
            this.radLabelDptID2.BorderVisible = true;
            this.radLabelDptID2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID2.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID2.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID2.Name = "radLabelDptID2";
            this.radLabelDptID2.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID2.TabIndex = 105;
            this.radLabelDptID2.Text = "D156";
            // 
            // radLabelDptName2
            // 
            this.radLabelDptName2.AutoSize = false;
            this.radLabelDptName2.BorderVisible = true;
            this.radLabelDptName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName2.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName2.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName2.Name = "radLabelDptName2";
            this.radLabelDptName2.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName2.TabIndex = 104;
            this.radLabelDptName2.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID2
            // 
            this.radLabelCarID2.AutoSize = false;
            this.radLabelCarID2.BorderVisible = true;
            this.radLabelCarID2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID2.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID2.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID2.Name = "radLabelCarID2";
            this.radLabelCarID2.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID2.TabIndex = 103;
            this.radLabelCarID2.Text = "กอ-8991";
            // 
            // radLabelCarName2
            // 
            this.radLabelCarName2.AutoSize = false;
            this.radLabelCarName2.BorderVisible = true;
            this.radLabelCarName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName2.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName2.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName2.Name = "radLabelCarName2";
            this.radLabelCarName2.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName2.TabIndex = 102;
            this.radLabelCarName2.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.radLabelDptID10, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.radLabelDptName10, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.radLabelCarID10, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.radLabelCarName10, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(203, 303);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel9.TabIndex = 106;
            // 
            // radLabelDptID10
            // 
            this.radLabelDptID10.AutoSize = false;
            this.radLabelDptID10.BorderVisible = true;
            this.radLabelDptID10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID10.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID10.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID10.Name = "radLabelDptID10";
            this.radLabelDptID10.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID10.TabIndex = 105;
            this.radLabelDptID10.Text = "D156";
            // 
            // radLabelDptName10
            // 
            this.radLabelDptName10.AutoSize = false;
            this.radLabelDptName10.BorderVisible = true;
            this.radLabelDptName10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName10.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName10.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName10.Name = "radLabelDptName10";
            this.radLabelDptName10.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName10.TabIndex = 104;
            this.radLabelDptName10.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID10
            // 
            this.radLabelCarID10.AutoSize = false;
            this.radLabelCarID10.BorderVisible = true;
            this.radLabelCarID10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID10.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID10.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID10.Name = "radLabelCarID10";
            this.radLabelCarID10.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID10.TabIndex = 103;
            this.radLabelCarID10.Text = "กอ-8991";
            // 
            // radLabelCarName10
            // 
            this.radLabelCarName10.AutoSize = false;
            this.radLabelCarName10.BorderVisible = true;
            this.radLabelCarName10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName10.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName10.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName10.Name = "radLabelCarName10";
            this.radLabelCarName10.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName10.TabIndex = 102;
            this.radLabelCarName10.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.radLabelDptID9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.radLabelDptName9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.radLabelCarID9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.radLabelCarName9, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(203, 273);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel8.TabIndex = 105;
            // 
            // radLabelDptID9
            // 
            this.radLabelDptID9.AutoSize = false;
            this.radLabelDptID9.BorderVisible = true;
            this.radLabelDptID9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID9.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID9.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID9.Name = "radLabelDptID9";
            this.radLabelDptID9.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID9.TabIndex = 105;
            this.radLabelDptID9.Text = "D156";
            // 
            // radLabelDptName9
            // 
            this.radLabelDptName9.AutoSize = false;
            this.radLabelDptName9.BorderVisible = true;
            this.radLabelDptName9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName9.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName9.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName9.Name = "radLabelDptName9";
            this.radLabelDptName9.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName9.TabIndex = 104;
            this.radLabelDptName9.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID9
            // 
            this.radLabelCarID9.AutoSize = false;
            this.radLabelCarID9.BorderVisible = true;
            this.radLabelCarID9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID9.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID9.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID9.Name = "radLabelCarID9";
            this.radLabelCarID9.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID9.TabIndex = 103;
            this.radLabelCarID9.Text = "กอ-8991";
            // 
            // radLabelCarName9
            // 
            this.radLabelCarName9.AutoSize = false;
            this.radLabelCarName9.BorderVisible = true;
            this.radLabelCarName9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName9.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName9.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName9.Name = "radLabelCarName9";
            this.radLabelCarName9.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName9.TabIndex = 102;
            this.radLabelCarName9.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.radLabelDptID8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.radLabelDptName8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.radLabelCarID8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.radLabelCarName8, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(203, 243);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel7.TabIndex = 104;
            // 
            // radLabelDptID8
            // 
            this.radLabelDptID8.AutoSize = false;
            this.radLabelDptID8.BorderVisible = true;
            this.radLabelDptID8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID8.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID8.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID8.Name = "radLabelDptID8";
            this.radLabelDptID8.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID8.TabIndex = 105;
            this.radLabelDptID8.Text = "D156";
            // 
            // radLabelDptName8
            // 
            this.radLabelDptName8.AutoSize = false;
            this.radLabelDptName8.BorderVisible = true;
            this.radLabelDptName8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName8.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName8.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName8.Name = "radLabelDptName8";
            this.radLabelDptName8.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName8.TabIndex = 104;
            this.radLabelDptName8.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID8
            // 
            this.radLabelCarID8.AutoSize = false;
            this.radLabelCarID8.BorderVisible = true;
            this.radLabelCarID8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID8.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID8.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID8.Name = "radLabelCarID8";
            this.radLabelCarID8.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID8.TabIndex = 103;
            this.radLabelCarID8.Text = "กอ-8991";
            // 
            // radLabelCarName8
            // 
            this.radLabelCarName8.AutoSize = false;
            this.radLabelCarName8.BorderVisible = true;
            this.radLabelCarName8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName8.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName8.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName8.Name = "radLabelCarName8";
            this.radLabelCarName8.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName8.TabIndex = 102;
            this.radLabelCarName8.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.radLabelDptID5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.radLabelDptName5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.radLabelCarID5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.radLabelCarName5, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(203, 153);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel6.TabIndex = 103;
            // 
            // radLabelDptID5
            // 
            this.radLabelDptID5.AutoSize = false;
            this.radLabelDptID5.BorderVisible = true;
            this.radLabelDptID5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID5.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID5.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID5.Name = "radLabelDptID5";
            this.radLabelDptID5.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID5.TabIndex = 105;
            // 
            // radLabelDptName5
            // 
            this.radLabelDptName5.AutoSize = false;
            this.radLabelDptName5.BorderVisible = true;
            this.radLabelDptName5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName5.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName5.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName5.Name = "radLabelDptName5";
            this.radLabelDptName5.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName5.TabIndex = 104;
            // 
            // radLabelCarID5
            // 
            this.radLabelCarID5.AutoSize = false;
            this.radLabelCarID5.BorderVisible = true;
            this.radLabelCarID5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID5.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID5.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID5.Name = "radLabelCarID5";
            this.radLabelCarID5.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID5.TabIndex = 103;
            // 
            // radLabelCarName5
            // 
            this.radLabelCarName5.AutoSize = false;
            this.radLabelCarName5.BorderVisible = true;
            this.radLabelCarName5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName5.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName5.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName5.Name = "radLabelCarName5";
            this.radLabelCarName5.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName5.TabIndex = 102;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.radLabelDptID4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.radLabelDptName4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.radLabelCarID4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.radLabelCarName4, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(203, 123);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel5.TabIndex = 102;
            // 
            // radLabelDptID4
            // 
            this.radLabelDptID4.AutoSize = false;
            this.radLabelDptID4.BorderVisible = true;
            this.radLabelDptID4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID4.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID4.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID4.Name = "radLabelDptID4";
            this.radLabelDptID4.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID4.TabIndex = 105;
            this.radLabelDptID4.Text = "D156";
            // 
            // radLabelDptName4
            // 
            this.radLabelDptName4.AutoSize = false;
            this.radLabelDptName4.BorderVisible = true;
            this.radLabelDptName4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName4.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName4.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName4.Name = "radLabelDptName4";
            this.radLabelDptName4.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName4.TabIndex = 104;
            this.radLabelDptName4.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID4
            // 
            this.radLabelCarID4.AutoSize = false;
            this.radLabelCarID4.BorderVisible = true;
            this.radLabelCarID4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID4.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID4.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID4.Name = "radLabelCarID4";
            this.radLabelCarID4.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID4.TabIndex = 103;
            this.radLabelCarID4.Text = "กอ-8991";
            // 
            // radLabelCarName4
            // 
            this.radLabelCarName4.AutoSize = false;
            this.radLabelCarName4.BorderVisible = true;
            this.radLabelCarName4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName4.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName4.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName4.Name = "radLabelCarName4";
            this.radLabelCarName4.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName4.TabIndex = 102;
            this.radLabelCarName4.Text = "รถเก๋ง";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabelDptID1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabelDptName1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabelCarID1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabelCarName1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(203, 33);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(656, 24);
            this.tableLayoutPanel2.TabIndex = 65;
            // 
            // radLabelDptID1
            // 
            this.radLabelDptID1.AutoSize = false;
            this.radLabelDptID1.BorderVisible = true;
            this.radLabelDptID1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptID1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptID1.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptID1.Location = new System.Drawing.Point(283, 3);
            this.radLabelDptID1.Name = "radLabelDptID1";
            this.radLabelDptID1.Size = new System.Drawing.Size(74, 18);
            this.radLabelDptID1.TabIndex = 105;
            this.radLabelDptID1.Text = "D156";
            // 
            // radLabelDptName1
            // 
            this.radLabelDptName1.AutoSize = false;
            this.radLabelDptName1.BorderVisible = true;
            this.radLabelDptName1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelDptName1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelDptName1.ForeColor = System.Drawing.Color.Blue;
            this.radLabelDptName1.Location = new System.Drawing.Point(363, 3);
            this.radLabelDptName1.Name = "radLabelDptName1";
            this.radLabelDptName1.Size = new System.Drawing.Size(290, 18);
            this.radLabelDptName1.TabIndex = 104;
            this.radLabelDptName1.Text = "แผนกยานยนต์";
            // 
            // radLabelCarID1
            // 
            this.radLabelCarID1.AutoSize = false;
            this.radLabelCarID1.BorderVisible = true;
            this.radLabelCarID1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarID1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarID1.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarID1.Location = new System.Drawing.Point(3, 3);
            this.radLabelCarID1.Name = "radLabelCarID1";
            this.radLabelCarID1.Size = new System.Drawing.Size(74, 18);
            this.radLabelCarID1.TabIndex = 103;
            this.radLabelCarID1.Text = "กอ-8991";
            // 
            // radLabelCarName1
            // 
            this.radLabelCarName1.AutoSize = false;
            this.radLabelCarName1.BorderVisible = true;
            this.radLabelCarName1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelCarName1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabelCarName1.ForeColor = System.Drawing.Color.Blue;
            this.radLabelCarName1.Location = new System.Drawing.Point(83, 3);
            this.radLabelCarName1.Name = "radLabelCarName1";
            this.radLabelCarName1.Size = new System.Drawing.Size(194, 18);
            this.radLabelCarName1.TabIndex = 102;
            this.radLabelCarName1.Text = "รถเก๋ง";
            // 
            // radLabel22
            // 
            this.radLabel22.AutoSize = false;
            this.radLabel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel22.ForeColor = System.Drawing.Color.Black;
            this.radLabel22.Location = new System.Drawing.Point(3, 3);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(94, 24);
            this.radLabel22.TabIndex = 65;
            this.radLabel22.Text = "ช่วงเวลา";
            this.radLabel22.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radToggleSwitch18
            // 
            this.radToggleSwitch18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch18.Location = new System.Drawing.Point(103, 545);
            this.radToggleSwitch18.Name = "radToggleSwitch18";
            this.radToggleSwitch18.OffText = "ไม่ว่าง";
            this.radToggleSwitch18.OnText = "ว่าง";
            this.radToggleSwitch18.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch18.TabIndex = 92;
            this.radToggleSwitch18.Tag = "";
            this.radToggleSwitch18.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch18.ValueChanged += new System.EventHandler(this.RadToggleSwitch18_ValueChanged);
            this.radToggleSwitch18.Click += new System.EventHandler(this.RadToggleSwitch18_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch18.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch18.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch17
            // 
            this.radToggleSwitch17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch17.Location = new System.Drawing.Point(103, 515);
            this.radToggleSwitch17.Name = "radToggleSwitch17";
            this.radToggleSwitch17.OffText = "ไม่ว่าง";
            this.radToggleSwitch17.OnText = "ว่าง";
            this.radToggleSwitch17.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch17.TabIndex = 91;
            this.radToggleSwitch17.Tag = "";
            this.radToggleSwitch17.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch17.ValueChanged += new System.EventHandler(this.RadToggleSwitch17_ValueChanged);
            this.radToggleSwitch17.Click += new System.EventHandler(this.RadToggleSwitch17_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch17.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch17.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch16
            // 
            this.radToggleSwitch16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch16.Location = new System.Drawing.Point(103, 485);
            this.radToggleSwitch16.Name = "radToggleSwitch16";
            this.radToggleSwitch16.OffText = "ไม่ว่าง";
            this.radToggleSwitch16.OnText = "ว่าง";
            this.radToggleSwitch16.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch16.TabIndex = 90;
            this.radToggleSwitch16.Tag = "";
            this.radToggleSwitch16.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch16.ValueChanged += new System.EventHandler(this.RadToggleSwitch16_ValueChanged);
            this.radToggleSwitch16.Click += new System.EventHandler(this.RadToggleSwitch16_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch16.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch16.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch15
            // 
            this.radToggleSwitch15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch15.Location = new System.Drawing.Point(103, 455);
            this.radToggleSwitch15.Name = "radToggleSwitch15";
            this.radToggleSwitch15.OffText = "ไม่ว่าง";
            this.radToggleSwitch15.OnText = "ว่าง";
            this.radToggleSwitch15.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch15.TabIndex = 89;
            this.radToggleSwitch15.Tag = "";
            this.radToggleSwitch15.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch15.ValueChanged += new System.EventHandler(this.RadToggleSwitch15_ValueChanged);
            this.radToggleSwitch15.Click += new System.EventHandler(this.RadToggleSwitch15_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch15.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch15.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch14
            // 
            this.radToggleSwitch14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch14.Location = new System.Drawing.Point(103, 425);
            this.radToggleSwitch14.Name = "radToggleSwitch14";
            this.radToggleSwitch14.OffText = "ไม่ว่าง";
            this.radToggleSwitch14.OnText = "ว่าง";
            this.radToggleSwitch14.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch14.TabIndex = 88;
            this.radToggleSwitch14.Tag = "";
            this.radToggleSwitch14.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch14.ValueChanged += new System.EventHandler(this.RadToggleSwitch14_ValueChanged);
            this.radToggleSwitch14.Click += new System.EventHandler(this.RadToggleSwitch14_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch14.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch14.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch13
            // 
            this.radToggleSwitch13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch13.Location = new System.Drawing.Point(103, 395);
            this.radToggleSwitch13.Name = "radToggleSwitch13";
            this.radToggleSwitch13.OffText = "ไม่ว่าง";
            this.radToggleSwitch13.OnText = "ว่าง";
            this.radToggleSwitch13.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch13.TabIndex = 87;
            this.radToggleSwitch13.Tag = "";
            this.radToggleSwitch13.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch13.ValueChanged += new System.EventHandler(this.RadToggleSwitch13_ValueChanged);
            this.radToggleSwitch13.Click += new System.EventHandler(this.RadToggleSwitch13_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch13.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch13.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch12
            // 
            this.radToggleSwitch12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch12.Location = new System.Drawing.Point(103, 365);
            this.radToggleSwitch12.Name = "radToggleSwitch12";
            this.radToggleSwitch12.OffText = "ไม่ว่าง";
            this.radToggleSwitch12.OnText = "ว่าง";
            this.radToggleSwitch12.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch12.TabIndex = 86;
            this.radToggleSwitch12.Tag = "";
            this.radToggleSwitch12.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch12.ValueChanged += new System.EventHandler(this.RadToggleSwitch12_ValueChanged);
            this.radToggleSwitch12.Click += new System.EventHandler(this.RadToggleSwitch12_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch12.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch12.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radLabel20
            // 
            this.radLabel20.AutoSize = false;
            this.radLabel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel20.ForeColor = System.Drawing.Color.Red;
            this.radLabel20.Location = new System.Drawing.Point(103, 333);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(94, 26);
            this.radLabel20.TabIndex = 74;
            this.radLabel20.Text = "พัก";
            this.radLabel20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radToggleSwitch10
            // 
            this.radToggleSwitch10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch10.Location = new System.Drawing.Point(103, 303);
            this.radToggleSwitch10.Name = "radToggleSwitch10";
            this.radToggleSwitch10.OffText = "ไม่ว่าง";
            this.radToggleSwitch10.OnText = "ว่าง";
            this.radToggleSwitch10.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch10.TabIndex = 85;
            this.radToggleSwitch10.Tag = "";
            this.radToggleSwitch10.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch10.ValueChanged += new System.EventHandler(this.RadToggleSwitch10_ValueChanged);
            this.radToggleSwitch10.Click += new System.EventHandler(this.RadToggleSwitch10_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch10.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch10.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch9
            // 
            this.radToggleSwitch9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch9.Location = new System.Drawing.Point(103, 273);
            this.radToggleSwitch9.Name = "radToggleSwitch9";
            this.radToggleSwitch9.OffText = "ไม่ว่าง";
            this.radToggleSwitch9.OnText = "ว่าง";
            this.radToggleSwitch9.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch9.TabIndex = 84;
            this.radToggleSwitch9.Tag = "";
            this.radToggleSwitch9.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch9.ValueChanged += new System.EventHandler(this.RadToggleSwitch9_ValueChanged);
            this.radToggleSwitch9.Click += new System.EventHandler(this.RadToggleSwitch9_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch9.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch9.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch8
            // 
            this.radToggleSwitch8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch8.Location = new System.Drawing.Point(103, 243);
            this.radToggleSwitch8.Name = "radToggleSwitch8";
            this.radToggleSwitch8.OffText = "ไม่ว่าง";
            this.radToggleSwitch8.OnText = "ว่าง";
            this.radToggleSwitch8.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch8.TabIndex = 83;
            this.radToggleSwitch8.Tag = "";
            this.radToggleSwitch8.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch8.ValueChanged += new System.EventHandler(this.RadToggleSwitch8_ValueChanged);
            this.radToggleSwitch8.Click += new System.EventHandler(this.RadToggleSwitch8_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch8.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch8.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch7
            // 
            this.radToggleSwitch7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch7.Location = new System.Drawing.Point(103, 213);
            this.radToggleSwitch7.Name = "radToggleSwitch7";
            this.radToggleSwitch7.OffText = "ไม่ว่าง";
            this.radToggleSwitch7.OnText = "ว่าง";
            this.radToggleSwitch7.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch7.TabIndex = 82;
            this.radToggleSwitch7.Tag = "";
            this.radToggleSwitch7.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch7.ValueChanged += new System.EventHandler(this.RadToggleSwitch7_ValueChanged);
            this.radToggleSwitch7.Click += new System.EventHandler(this.RadToggleSwitch7_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch7.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch7.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch6
            // 
            this.radToggleSwitch6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch6.Location = new System.Drawing.Point(103, 183);
            this.radToggleSwitch6.Name = "radToggleSwitch6";
            this.radToggleSwitch6.OffText = "ไม่ว่าง";
            this.radToggleSwitch6.OnText = "ว่าง";
            this.radToggleSwitch6.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch6.TabIndex = 81;
            this.radToggleSwitch6.Tag = "";
            this.radToggleSwitch6.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch6.ValueChanged += new System.EventHandler(this.RadToggleSwitch6_ValueChanged);
            this.radToggleSwitch6.Click += new System.EventHandler(this.RadToggleSwitch6_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch6.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch6.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch5
            // 
            this.radToggleSwitch5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch5.Location = new System.Drawing.Point(103, 153);
            this.radToggleSwitch5.Name = "radToggleSwitch5";
            this.radToggleSwitch5.OffText = "ไม่ว่าง";
            this.radToggleSwitch5.OnText = "ว่าง";
            this.radToggleSwitch5.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch5.TabIndex = 80;
            this.radToggleSwitch5.Tag = "";
            this.radToggleSwitch5.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch5.ValueChanged += new System.EventHandler(this.RadToggleSwitch5_ValueChanged);
            this.radToggleSwitch5.Click += new System.EventHandler(this.RadToggleSwitch5_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch5.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch5.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch4
            // 
            this.radToggleSwitch4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch4.Location = new System.Drawing.Point(103, 123);
            this.radToggleSwitch4.Name = "radToggleSwitch4";
            this.radToggleSwitch4.OffText = "ไม่ว่าง";
            this.radToggleSwitch4.OnText = "ว่าง";
            this.radToggleSwitch4.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch4.TabIndex = 79;
            this.radToggleSwitch4.Tag = "";
            this.radToggleSwitch4.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch4.ValueChanged += new System.EventHandler(this.RadToggleSwitch4_ValueChanged);
            this.radToggleSwitch4.Click += new System.EventHandler(this.RadToggleSwitch4_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch4.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch4.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch3
            // 
            this.radToggleSwitch3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch3.Location = new System.Drawing.Point(103, 93);
            this.radToggleSwitch3.Name = "radToggleSwitch3";
            this.radToggleSwitch3.OffText = "ไม่ว่าง";
            this.radToggleSwitch3.OnText = "ว่าง";
            this.radToggleSwitch3.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch3.TabIndex = 78;
            this.radToggleSwitch3.Tag = "";
            this.radToggleSwitch3.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch3.ValueChanged += new System.EventHandler(this.RadToggleSwitch3_ValueChanged);
            this.radToggleSwitch3.Click += new System.EventHandler(this.RadToggleSwitch3_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch3.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch3.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch2
            // 
            this.radToggleSwitch2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch2.Location = new System.Drawing.Point(103, 63);
            this.radToggleSwitch2.Name = "radToggleSwitch2";
            this.radToggleSwitch2.OffText = "ไม่ว่าง";
            this.radToggleSwitch2.OnText = "ว่าง";
            this.radToggleSwitch2.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch2.TabIndex = 77;
            this.radToggleSwitch2.Tag = "";
            this.radToggleSwitch2.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch2.ValueChanged += new System.EventHandler(this.RadToggleSwitch2_ValueChanged);
            this.radToggleSwitch2.Click += new System.EventHandler(this.RadToggleSwitch2_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch2.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch2.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch1
            // 
            this.radToggleSwitch1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch1.Location = new System.Drawing.Point(103, 33);
            this.radToggleSwitch1.Name = "radToggleSwitch1";
            this.radToggleSwitch1.OffText = "ไม่ว่าง";
            this.radToggleSwitch1.OnText = "ว่าง";
            this.radToggleSwitch1.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch1.TabIndex = 76;
            this.radToggleSwitch1.Tag = "";
            this.radToggleSwitch1.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch1.ValueChanged += new System.EventHandler(this.RadToggleSwitch1_ValueChanged);
            this.radToggleSwitch1.Click += new System.EventHandler(this.RadToggleSwitch1_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch1.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BorderColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BorderColor2 = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BorderColor3 = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BorderColor4 = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BorderInnerColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BorderInnerColor2 = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BorderInnerColor3 = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BorderInnerColor4 = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch1.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch19
            // 
            this.radToggleSwitch19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radToggleSwitch19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch19.Location = new System.Drawing.Point(103, 575);
            this.radToggleSwitch19.Name = "radToggleSwitch19";
            this.radToggleSwitch19.OffText = "ไม่ว่าง";
            this.radToggleSwitch19.OnText = "ว่าง";
            this.radToggleSwitch19.Size = new System.Drawing.Size(94, 24);
            this.radToggleSwitch19.TabIndex = 74;
            this.radToggleSwitch19.Tag = "";
            this.radToggleSwitch19.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.Click;
            this.radToggleSwitch19.ValueChanged += new System.EventHandler(this.RadToggleSwitch19_ValueChanged);
            this.radToggleSwitch19.Click += new System.EventHandler(this.RadToggleSwitch19_Click);
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch19.GetChildAt(0))).ThumbOffset = 74;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).Text = "ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.LimeGreen;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).Text = "ไม่ว่าง";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch19.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radLabel19
            // 
            this.radLabel19.AutoSize = false;
            this.radLabel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel19.ForeColor = System.Drawing.Color.Black;
            this.radLabel19.Location = new System.Drawing.Point(3, 575);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(94, 24);
            this.radLabel19.TabIndex = 56;
            this.radLabel19.Text = "16.30-17.00";
            this.radLabel19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel18
            // 
            this.radLabel18.AutoSize = false;
            this.radLabel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel18.ForeColor = System.Drawing.Color.Black;
            this.radLabel18.Location = new System.Drawing.Point(3, 545);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(94, 24);
            this.radLabel18.TabIndex = 56;
            this.radLabel18.Text = "16.00-16.30";
            this.radLabel18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel17
            // 
            this.radLabel17.AutoSize = false;
            this.radLabel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel17.ForeColor = System.Drawing.Color.Black;
            this.radLabel17.Location = new System.Drawing.Point(3, 515);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(94, 24);
            this.radLabel17.TabIndex = 56;
            this.radLabel17.Text = "15.30-16.00";
            this.radLabel17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel16.ForeColor = System.Drawing.Color.Black;
            this.radLabel16.Location = new System.Drawing.Point(3, 485);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(94, 24);
            this.radLabel16.TabIndex = 56;
            this.radLabel16.Text = "15.00-15.30";
            this.radLabel16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel15.ForeColor = System.Drawing.Color.Black;
            this.radLabel15.Location = new System.Drawing.Point(3, 455);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(94, 24);
            this.radLabel15.TabIndex = 56;
            this.radLabel15.Text = "14.30-15.00";
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel14.ForeColor = System.Drawing.Color.Black;
            this.radLabel14.Location = new System.Drawing.Point(3, 425);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(94, 24);
            this.radLabel14.TabIndex = 56;
            this.radLabel14.Text = "14.00-14.30";
            this.radLabel14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel13.ForeColor = System.Drawing.Color.Black;
            this.radLabel13.Location = new System.Drawing.Point(3, 395);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(94, 24);
            this.radLabel13.TabIndex = 56;
            this.radLabel13.Text = "13.30-14.00";
            this.radLabel13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel12.ForeColor = System.Drawing.Color.Black;
            this.radLabel12.Location = new System.Drawing.Point(3, 365);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(94, 24);
            this.radLabel12.TabIndex = 56;
            this.radLabel12.Text = "13.00-13.30";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9F);
            this.radLabel11.ForeColor = System.Drawing.Color.Red;
            this.radLabel11.Location = new System.Drawing.Point(3, 333);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(94, 26);
            this.radLabel11.TabIndex = 56;
            this.radLabel11.Text = "12.00-13.00";
            this.radLabel11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.ForeColor = System.Drawing.Color.Black;
            this.radLabel10.Location = new System.Drawing.Point(3, 303);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(94, 24);
            this.radLabel10.TabIndex = 56;
            this.radLabel10.Text = "11.30-12.00";
            this.radLabel10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.ForeColor = System.Drawing.Color.Black;
            this.radLabel9.Location = new System.Drawing.Point(3, 273);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(94, 24);
            this.radLabel9.TabIndex = 56;
            this.radLabel9.Text = "11.00-11.30";
            this.radLabel9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.ForeColor = System.Drawing.Color.Black;
            this.radLabel8.Location = new System.Drawing.Point(3, 243);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(94, 24);
            this.radLabel8.TabIndex = 56;
            this.radLabel8.Text = "10.30-11.00";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.ForeColor = System.Drawing.Color.Black;
            this.radLabel7.Location = new System.Drawing.Point(3, 213);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(94, 24);
            this.radLabel7.TabIndex = 56;
            this.radLabel7.Text = "10.00-10.30";
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.ForeColor = System.Drawing.Color.Black;
            this.radLabel6.Location = new System.Drawing.Point(3, 183);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(94, 24);
            this.radLabel6.TabIndex = 56;
            this.radLabel6.Text = "09.30-10.00";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.ForeColor = System.Drawing.Color.Black;
            this.radLabel5.Location = new System.Drawing.Point(3, 153);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(94, 24);
            this.radLabel5.TabIndex = 56;
            this.radLabel5.Text = "09.00-09.30";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(3, 123);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(94, 24);
            this.radLabel4.TabIndex = 56;
            this.radLabel4.Text = "08.30-09.00";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.ForeColor = System.Drawing.Color.Black;
            this.radLabel3.Location = new System.Drawing.Point(3, 93);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(94, 24);
            this.radLabel3.TabIndex = 56;
            this.radLabel3.Text = "08.00-08.30";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(3, 33);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(94, 24);
            this.radLabel1.TabIndex = 54;
            this.radLabel1.Text = "07.00-07.30";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.ForeColor = System.Drawing.Color.Black;
            this.radLabel2.Location = new System.Drawing.Point(3, 63);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(94, 24);
            this.radLabel2.TabIndex = 55;
            this.radLabel2.Text = "07.30-08.00";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel24
            // 
            this.radLabel24.AutoSize = false;
            this.radLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel24.ForeColor = System.Drawing.Color.Black;
            this.radLabel24.Location = new System.Drawing.Point(103, 3);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(94, 24);
            this.radLabel24.TabIndex = 100;
            this.radLabel24.Text = "สถานะ";
            this.radLabel24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radStatusStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(862, 44);
            this.panel2.TabIndex = 1;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Controls.Add(this.radLabel28);
            this.radStatusStrip1.Controls.Add(this.radDateTimePicker_Car);
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement11_pdf,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(862, 34);
            this.radStatusStrip1.TabIndex = 54;
            // 
            // radLabel28
            // 
            this.radLabel28.AutoSize = false;
            this.radLabel28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel28.ForeColor = System.Drawing.Color.Black;
            this.radLabel28.Location = new System.Drawing.Point(303, 40);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(94, 19);
            this.radLabel28.TabIndex = 97;
            this.radLabel28.Text = "07.00-07.30";
            this.radLabel28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radDateTimePicker_Car
            // 
            this.radDateTimePicker_Car.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Car.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_Car.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_Car.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_Car.Location = new System.Drawing.Point(50, 6);
            this.radDateTimePicker_Car.Name = "radDateTimePicker_Car";
            this.radDateTimePicker_Car.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_Car.TabIndex = 61;
            this.radDateTimePicker_Car.TabStop = false;
            this.radDateTimePicker_Car.Text = "22/05/2020";
            this.radDateTimePicker_Car.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_Car.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Car_ValueChanged);
            // 
            // radButtonElement11_pdf
            // 
            this.radButtonElement11_pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement11_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement11_pdf.Name = "radButtonElement11_pdf";
            this.radStatusStrip1.SetSpring(this.radButtonElement11_pdf, false);
            this.radButtonElement11_pdf.Text = "radButtonElement1";
            this.radButtonElement11_pdf.UseCompatibleTextRendering = false;
            this.radButtonElement11_pdf.Click += new System.EventHandler(this.RadButtonElement11_pdf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radDateTimePicker_D1
            // 
            this.radDateTimePicker_D1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D1.Location = new System.Drawing.Point(11, 126);
            this.radDateTimePicker_D1.Name = "radDateTimePicker_D1";
            this.radDateTimePicker_D1.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_D1.TabIndex = 58;
            this.radDateTimePicker_D1.TabStop = false;
            this.radDateTimePicker_D1.Text = "22/05/2020";
            this.radDateTimePicker_D1.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // radDateTimePicker_D2
            // 
            this.radDateTimePicker_D2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D2.Location = new System.Drawing.Point(11, 153);
            this.radDateTimePicker_D2.Name = "radDateTimePicker_D2";
            this.radDateTimePicker_D2.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_D2.TabIndex = 59;
            this.radDateTimePicker_D2.TabStop = false;
            this.radDateTimePicker_D2.Text = "22/05/2020";
            this.radDateTimePicker_D2.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // CarCare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 705);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "CarCare";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "จองคิวล้างรถ";
            this.Load += new System.EventHandler(this.CarCare_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel103)).EndInit();
            this.tableLayoutPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName19)).EndInit();
            this.tableLayoutPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName18)).EndInit();
            this.tableLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName17)).EndInit();
            this.tableLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName16)).EndInit();
            this.tableLayoutPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName15)).EndInit();
            this.tableLayoutPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName14)).EndInit();
            this.tableLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName13)).EndInit();
            this.tableLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName12)).EndInit();
            this.tableLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName7)).EndInit();
            this.tableLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName6)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName3)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName2)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName10)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName9)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName8)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName5)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName4)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDptName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCarName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.radStatusStrip1.ResumeLayout(false);
            this.radStatusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Car)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Car;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement11_pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch19;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch10;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch9;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch8;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch7;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch6;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch5;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch4;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch3;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch2;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch1;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch18;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch17;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch16;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch15;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch14;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch13;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch12;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabelCarID1;
        private Telerik.WinControls.UI.RadLabel radLabelCarName1;
        private Telerik.WinControls.UI.RadLabel radLabelDptID1;
        private Telerik.WinControls.UI.RadLabel radLabelDptName1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private Telerik.WinControls.UI.RadLabel radLabelDptID9;
        private Telerik.WinControls.UI.RadLabel radLabelDptName9;
        private Telerik.WinControls.UI.RadLabel radLabelCarID9;
        private Telerik.WinControls.UI.RadLabel radLabelCarName9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private Telerik.WinControls.UI.RadLabel radLabelDptID8;
        private Telerik.WinControls.UI.RadLabel radLabelDptName8;
        private Telerik.WinControls.UI.RadLabel radLabelCarID8;
        private Telerik.WinControls.UI.RadLabel radLabelCarName8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private Telerik.WinControls.UI.RadLabel radLabelDptID5;
        private Telerik.WinControls.UI.RadLabel radLabelDptName5;
        private Telerik.WinControls.UI.RadLabel radLabelCarID5;
        private Telerik.WinControls.UI.RadLabel radLabelCarName5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private Telerik.WinControls.UI.RadLabel radLabelDptID4;
        private Telerik.WinControls.UI.RadLabel radLabelDptName4;
        private Telerik.WinControls.UI.RadLabel radLabelCarID4;
        private Telerik.WinControls.UI.RadLabel radLabelCarName4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private Telerik.WinControls.UI.RadLabel radLabelDptID10;
        private Telerik.WinControls.UI.RadLabel radLabelDptName10;
        private Telerik.WinControls.UI.RadLabel radLabelCarID10;
        private Telerik.WinControls.UI.RadLabel radLabelCarName10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private Telerik.WinControls.UI.RadLabel radLabelDptID7;
        private Telerik.WinControls.UI.RadLabel radLabelDptName7;
        private Telerik.WinControls.UI.RadLabel radLabelCarID7;
        private Telerik.WinControls.UI.RadLabel radLabelCarName7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private Telerik.WinControls.UI.RadLabel radLabelDptID6;
        private Telerik.WinControls.UI.RadLabel radLabelDptName6;
        private Telerik.WinControls.UI.RadLabel radLabelCarID6;
        private Telerik.WinControls.UI.RadLabel radLabelCarName6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private Telerik.WinControls.UI.RadLabel radLabelDptID3;
        private Telerik.WinControls.UI.RadLabel radLabelDptName3;
        private Telerik.WinControls.UI.RadLabel radLabelCarID3;
        private Telerik.WinControls.UI.RadLabel radLabelCarName3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private Telerik.WinControls.UI.RadLabel radLabelDptID2;
        private Telerik.WinControls.UI.RadLabel radLabelDptName2;
        private Telerik.WinControls.UI.RadLabel radLabelCarID2;
        private Telerik.WinControls.UI.RadLabel radLabelCarName2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private Telerik.WinControls.UI.RadLabel radLabelDptID19;
        private Telerik.WinControls.UI.RadLabel radLabelDptName19;
        private Telerik.WinControls.UI.RadLabel radLabelCarID19;
        private Telerik.WinControls.UI.RadLabel radLabelCarName19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private Telerik.WinControls.UI.RadLabel radLabelDptID18;
        private Telerik.WinControls.UI.RadLabel radLabelDptName18;
        private Telerik.WinControls.UI.RadLabel radLabelCarID18;
        private Telerik.WinControls.UI.RadLabel radLabelCarName18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private Telerik.WinControls.UI.RadLabel radLabelDptID17;
        private Telerik.WinControls.UI.RadLabel radLabelDptName17;
        private Telerik.WinControls.UI.RadLabel radLabelCarID17;
        private Telerik.WinControls.UI.RadLabel radLabelCarName17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private Telerik.WinControls.UI.RadLabel radLabelDptID16;
        private Telerik.WinControls.UI.RadLabel radLabelDptName16;
        private Telerik.WinControls.UI.RadLabel radLabelCarID16;
        private Telerik.WinControls.UI.RadLabel radLabelCarName16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private Telerik.WinControls.UI.RadLabel radLabelDptID15;
        private Telerik.WinControls.UI.RadLabel radLabelDptName15;
        private Telerik.WinControls.UI.RadLabel radLabelCarID15;
        private Telerik.WinControls.UI.RadLabel radLabelCarName15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private Telerik.WinControls.UI.RadLabel radLabelDptID14;
        private Telerik.WinControls.UI.RadLabel radLabelDptName14;
        private Telerik.WinControls.UI.RadLabel radLabelCarID14;
        private Telerik.WinControls.UI.RadLabel radLabelCarName14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private Telerik.WinControls.UI.RadLabel radLabelDptID13;
        private Telerik.WinControls.UI.RadLabel radLabelDptName13;
        private Telerik.WinControls.UI.RadLabel radLabelCarID13;
        private Telerik.WinControls.UI.RadLabel radLabelCarName13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private Telerik.WinControls.UI.RadLabel radLabelDptID12;
        private Telerik.WinControls.UI.RadLabel radLabelDptName12;
        private Telerik.WinControls.UI.RadLabel radLabelCarID12;
        private Telerik.WinControls.UI.RadLabel radLabelCarName12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadLabel radLabel101;
        private Telerik.WinControls.UI.RadLabel radLabel102;
        private Telerik.WinControls.UI.RadLabel radLabel103;
    }
}
