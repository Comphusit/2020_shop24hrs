﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    public partial class Report_Queue : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_DataHD = new DataTable();
        DataTable dt_DataDT = new DataTable();

        private DateTime pTimeRefresh;

        //Load
        public Report_Queue()
        {
            InitializeComponent();
        }
        //Load
        private void Report_Queue_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radLabel_Detail.Visible = true;
            radLabel_Detail.Text = "โหลดข้อมูลใหม่ทุก 1 นาที | กด F5 เพื่อดึงข้อมูลใหม่ | DoubleClick >> รหัส พนง เพื่อระบุปล่อย LO แล้ว | DoubleClick >> ตำแหน่ง เพื่อย้ายตำแหน่ง";
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDATETIME", "วันที่-เวลา ลงคิว", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "รหัส พขร", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMEDRIVER", "ชื่อ พขร", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSITION", "ตำแหน่ง", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "STA")));

            ExpressionFormattingObject obj6_0 = new ExpressionFormattingObject("MyCondition1", "STA = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["LOGISTICID"].ConditionalFormattingObjectList.Add(obj6_0);

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDATETIME", "วันที่-เวลา ลงคิว", 180)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "รหัส เด็กท้าย", 80)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMEDRIVER", "ชื่อ เด็กท้าย", 250)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSITION", "ตำแหน่ง", 200)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "STA")));

            DatagridClass.SetCellBackClolorByExpression("LOGISTICID", "STA = '1' ", ConfigClass.SetColor_Red(), RadGridView_ShowDT);

            radRadioButtonElement_Q.ShowBorder = true;
            radRadioButtonElement_All.ShowBorder = true;

            SetDGV_HD();
        }

        //Set HD
        void SetDGV_HD()
        {
            if (dt_DataHD.Rows.Count > 0) { dt_DataHD.Rows.Clear(); }
            this.Cursor = Cursors.WaitCursor;

            string caseQ = "";
            if (radRadioButtonElement_Q.CheckState == CheckState.Checked) caseQ = " AND	STA = '0' ";
             
            dt_DataHD = Logistic_Class.FindData_DRIVERQUEUE("0", caseQ); //ConnectionClass.SelectSQL_Main(sqlSelect1);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_DataHD;
            dt_DataHD.AcceptChanges();
 
            dt_DataDT = Logistic_Class.FindData_DRIVERQUEUE("1", caseQ); //ConnectionClass.SelectSQL_Main(sqlSelect2);
            RadGridView_ShowDT.FilterDescriptors.Clear();
            RadGridView_ShowDT.DataSource = dt_DataDT;
            dt_DataDT.AcceptChanges();

            Timer_refresh.Start();
            pTimeRefresh = DateTime.Now.AddMinutes(1);

            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }


        #endregion


        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //refresh
        private void RadGridView_ShowHD_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) SetDGV_HD();
        }
        //refresh
        private void RadGridView_ShowDT_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) SetDGV_HD();
        }

        private void RadRadioButtonElement_All_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        private void RadRadioButtonElement_Q_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
              
        //พขร
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "EMPLDRIVER":
                    if (RadGridView_ShowHD.CurrentRow.Cells["STA"].Value.ToString() == "1") return;
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการปล่อย LO ให้พนักงาน" + Environment.NewLine +
                        RadGridView_ShowHD.CurrentRow.Cells["EMPLDRIVER"].Value.ToString() + " - " + RadGridView_ShowHD.CurrentRow.Cells["EMPLNAMEDRIVER"].Value.ToString() +
                        Environment.NewLine + "เรียบร้อยแล้ว ?") == DialogResult.Yes)
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(Logistic_Class.Update_DriverQueueLO(RadGridView_ShowHD.CurrentRow.Cells["EMPLDRIVER"].Value.ToString()));
                        SetDGV_HD();
                    }
                    break;
                case "POSITION":
                    if (RadGridView_ShowHD.CurrentRow.Cells["STA"].Value.ToString() == "1") return;
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการเปลี่ยนตำแหน่งพนักงาน" + Environment.NewLine +
                        RadGridView_ShowHD.CurrentRow.Cells["EMPLDRIVER"].Value.ToString() + " - " + RadGridView_ShowHD.CurrentRow.Cells["EMPLNAMEDRIVER"].Value.ToString() +
                        Environment.NewLine + "เป็นเด็กท้าย ?") == DialogResult.Yes)
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(Logistic_Class.Update_DriverQueueType(RadGridView_ShowHD.CurrentRow.Cells["EMPLDRIVER"].Value.ToString(), "1"));
                        SetDGV_HD();
                    }
                    break;
                default:
                    break;
            }
        }
        //เดกท้าย
        private void RadGridView_ShowDT_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "EMPLDRIVER":
                    if (RadGridView_ShowDT.CurrentRow.Cells["STA"].Value.ToString() == "1") return;
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการปล่อย LO ให้พนักงาน" + Environment.NewLine +
                        RadGridView_ShowDT.CurrentRow.Cells["EMPLDRIVER"].Value.ToString() + " - " + RadGridView_ShowDT.CurrentRow.Cells["EMPLNAMEDRIVER"].Value.ToString() +
                        Environment.NewLine + "เรียบร้อยแล้ว ?") == DialogResult.Yes)
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(Logistic_Class.Update_DriverQueueLO(RadGridView_ShowDT.CurrentRow.Cells["EMPLDRIVER"].Value.ToString()));
                        SetDGV_HD();
                    }
                    break;

                case "POSITION":
                    if (RadGridView_ShowDT.CurrentRow.Cells["STA"].Value.ToString() == "1") return;
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการเปลี่ยนตำแหน่งพนักงาน" + Environment.NewLine +
                        RadGridView_ShowDT.CurrentRow.Cells["EMPLDRIVER"].Value.ToString() + " - " + RadGridView_ShowDT.CurrentRow.Cells["EMPLNAMEDRIVER"].Value.ToString() +
                        Environment.NewLine + "เป็นพนักงานขับรถ ?") == DialogResult.Yes)
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(Logistic_Class.Update_DriverQueueType(RadGridView_ShowDT.CurrentRow.Cells["EMPLDRIVER"].Value.ToString(), "0"));
                        SetDGV_HD();
                    }
                    break;
                default:
                    break;
            }
        }
        //refresh
        private void Timer_refresh_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > pTimeRefresh)
            {
                Timer_refresh.Stop();
                SetDGV_HD();
            }
        }
    }
}

