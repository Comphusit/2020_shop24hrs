﻿using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{    class Logistic_Class
    {
        //Insert สาขาเกบขยะ
        public static string Insert_Garbage(string BchID, string BchName)
        {
            string sql = $@"
                DECLARE @bchID AS NVARCHAR(50) = '{BchID}';
                DECLARE @bchName AS NVARCHAR(200) = '{BchName}';

                DECLARE @userID AS NVARCHAR(200) = '{SystemClass.SystemUserID}'; 
                DECLARE @userName AS NVARCHAR(200) = '{SystemClass.SystemUserName}';
                DECLARE @DIMTABLE TABLE (DESC_STATUS NVARCHAR(500));
            
                IF NOT EXISTS ( SELECT * FROM    [SHOP_MNRD_ORDER] WITH(NOLOCK) WHERE    BRANCH_ID = @emplID AND LOGISTICID = '' )
                BEGIN
                        INSERT INTO  [SHOP_MNRD_ORDER]
                                    ([BRANCH_ID],[BRANCH_NAME],[WhoIns],[WhoNameIns]) 
                        VALUES      (@bchID,@bchName,@userID,@userName)                                 

                    INSERT INTO @DIMTABLE VALUES('')
                END
                ELSE
                    INSERT INTO @DIMTABLE VALUES('สาขาที่ระบุเก็บขยะ มีอยู่ในคิวเรียบร้อยแล้ว ไม่ต้องลงซ้ำ')

                SELECT * FROM @DIMTABLE";

            return ConnectionClass.SelectSQL_Main(sql).Rows[0]["DESC_STATUS"].ToString();
        }
        //Insert พนง + เดกท้าย ว่างในคิว
        public static string Insert_DRIVERQUEUE(string emplID, string emplName, string dptID, string dptName, string position, string type)
        {
            string sql = $@"    
            DECLARE @emplID AS NVARCHAR(50) = '{emplID}';
            DECLARE @emplName AS NVARCHAR(200) = '{emplName}';
            DECLARE @dptID AS NVARCHAR(50) = '{dptID}';
            DECLARE @dptName AS NVARCHAR(200) = '{dptName}';
            DECLARE @position AS NVARCHAR(200) = '{position}';
            DECLARE @type   AS NVARCHAR(2) = '{type}';

            DECLARE @userID AS NVARCHAR(200) = '{SystemClass.SystemUserID}';
            DECLARE @userName AS NVARCHAR(200) = '{SystemClass.SystemUserName}';
            DECLARE @DIMTABLE TABLE (DESC_STATUS NVARCHAR(500));
            
            IF NOT EXISTS ( SELECT * FROM    [SHOP_DRIVERQUEUE] WITH(NOLOCK) WHERE    EMPLDRIVER = @emplID AND STA = '0' )
            BEGIN
                  INSERT INTO   [SHOP_DRIVERQUEUE]
                            ([EMPLDRIVER],[EMPLNAMEDRIVER],[EMPLDPTID],[EMPLDPTNAME],[POSITION],[TYPEQ],
                            [WHOIDINS],[WHONAMEINS]) 
                   values       (@emplID,@emplName,@dptID,@dptName,@position,@type,@userID,@userName)                                 

                INSERT INTO @DIMTABLE VALUES('')
            END
            ELSE
                INSERT INTO @DIMTABLE VALUES('พนักงานขับรถที่ระบุมีอยู่ในคิวเรียบร้อยแล้ว ไม่ต้องลงซ้ำ')


            SELECT * FROM @DIMTABLE";

            return ConnectionClass.SelectSQL_Main(sql).Rows[0]["DESC_STATUS"].ToString();
        }
        //เปลี่ยนตำแหน่งให้พนักงาน
        public static string Update_DriverQueueType(string DriverID, string staQ)
        {
            string sqlUp = $@" 
                UPDATE  SHOP_DRIVERQUEUE 
                SET     TYPEQ = '{staQ}'
                WHERE   STA = '0' AND EMPLDRIVER = '{DriverID}'
            ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        //ปล่อย LO ให้พนักงาน
        public static string Update_DriverQueueLO(string DriverID)
        {
            string sqlUp = $@"
                UPDATE SHOP_DRIVERQUEUE
                SET STA = '1', LOGISTICID = 'ไม่ได้ระบุ'
                WHERE STA = '0' AND EMPLDRIVER = '{DriverID}'
            ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        //ค้นหาคิว เก็บขยะ / เด็กท้าย / พขร
        public static DataTable FindData_DRIVERQUEUE(string typeQ, string pCon)
        {
            string sql = $@"
            SELECT	CONVERT(VARCHAR,CREATEDATETIME,25) AS CREATEDATETIME,EMPLDRIVER,EMPLNAMEDRIVER,EMPLDPTNAME,LOGISTICID,STA,POSITION 
            FROM	[SHOP_DRIVERQUEUE] WITH (NOLOCK)
            WHERE   TYPEQ = '{typeQ}'  {pCon}
            ORDER BY CREATEDATETIME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //FindData MNRD
        public static DataTable MNRD_FindData(string logisticId)
        {
            string sql = $@"
                SELECT  BRANCH_ID,BRANCH_NAME   
                FROM    SHOP_MNRD_ORDER  WITH (NOLOCK) 
                WHERE   LOGISTICID  = '{logisticId}' 
                GROUP BY BRANCH_ID,BRANCH_NAME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Delete MNRD
        public static string MNRD_DeleteRow(string logisticID, string bchID)
        {
            string sqlDel = $@"
            Delete  SHOP_MNRD_ORDER 
            WHERE   LOGISTICID = '{logisticID}'  AND BRANCH_ID = '{bchID}' ";
            return ConnectionClass.ExecuteSQL_Main(sqlDel);
        }
        //Insert MNRD
        public static string MNRD_InsertRow(string bchID, string bchName, string logisticId)
        {
            string sqlIn = $@"
                        INSERT Into SHOP_MNRD_ORDER ( LOGISTICID,BRANCH_ID,BRANCH_NAME,WhoIns,WhoNameIns ) VALUES ( 
                        '{logisticId}','{bchID}','{bchName}','{ SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";

            return ConnectionClass.ExecuteSQL_Main(sqlIn);
        }
        //Carcare FindData
        public static DataTable CARCARE_FindData(string date1, string date2)
        {
            string sql = $@"
            SELECT	[CARCARE_DATE],[CARCARE_TIME],
		            [CARCARE_CARID],[CARCARE_CARNAME],[CARCARE_DPTID],[CARCARE_DPTNAME],
		            [DATEINS],[WHOINS],[WHONAMEINS]
            FROM	[SHOP_CARCARE] WITH (NOLOCK)
            WHERE	[CARCARE_DATE] BETWEEN  '{date1}' AND '{date2}'		 ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Carcare Delete
        public static string CARCARE_DeleteRow(string date, string time)
        {
            string sql = $@"
                    DELETE	SHOP_CARCARE
                    WHERE	CARCARE_DATE = '{date}' 
		                    AND CARCARE_CARID IN (
			                    SELECT	CARCARE_CARID	FROM	SHOP_CARCARE WITH (NOLOCK)
			                    WHERE	CARCARE_TIME = '{time}'
					                    AND CARCARE_DATE = '{date}') ";
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Carcare Delete Sql
        public static string CARCARE_DeleteRowSql(string date, string pVEHICLEID)
        {
            return $@"  DELETE	SHOP_CARCARE
                        WHERE	CARCARE_DATE = '{date}' AND CARCARE_CARID = '{pVEHICLEID}'";
        }
        //Carcare Insert
        public static string CARCARE_InsertRow(string pDate, string pVEHICLEID, string pNAME, string pDPTID, string pDPTNAME, string labeltime)
        {
            return $@"
                INSERT INTO [SHOP_CARCARE] ([CARCARE_DATE],[CARCARE_TIME]
                           ,[CARCARE_CARID],[CARCARE_CARNAME]
                           ,[CARCARE_DPTID],[CARCARE_DPTNAME]
                           ,[WHOINS],[WHONAMEINS])
                VALUES  ('{pDate}','{labeltime}',
                        '{pVEHICLEID}','{pNAME}','{pDPTID}','{pDPTNAME}',
                        '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')
                ";
        }
        //ค้นหาพนักงานที่ล้างรถ
        public static DataTable EmpVehicleWash_FindData(string date,string pVEHICLEID)
        {
            string sql =$@"
                      SELECT	Shop_EmpVehicleWash.EMPLID as EmplID,SPC_NAME,CREATEDATE,USERCREATE
                      FROM	    Shop_EmpVehicleWash WITH (NOLOCK)
                      WHERE	    YEARMONTHDAY = '{date}'  AND VEHICLEID = '{pVEHICLEID}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
