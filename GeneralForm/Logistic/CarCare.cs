﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    public partial class CarCare : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        DataTable dt_Car = new DataTable();

        readonly string _pTypeReport;
        readonly string _pPermission;

        //Load
        public CarCare(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }

        
        //Set Desige
        void SetDesign()
        {
            radStatusStrip1.SizingGrip = false;

            radButtonElement11_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement11_pdf.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Car, DateTime.Now.AddDays(-45), DateTime.Now.AddDays(30));

            radLabel_Detail.Text = "เลือกวันที่ที่ต้องการ | กดที่ ว่าง/สีเขียว >> เพื่อจองคิว | สีแดง >> มีคิว | สีเขียว >> ว่าง";

        }
        //Load
        private void CarCare_Load(object sender, EventArgs e)
        {
            SetDesign();
            //SetCar();
            dt_Car = LogisticClass.CARCARE_FindData();
            SetText();
            ClearTxt();
        }
        //set Text
        void SetText()
        {
            SetData();

            ClearGroup1(dt_Data);
            ClearGroup2(dt_Data);
            ClearGroup3(dt_Data);
            ClearGroup4(dt_Data);
            ClearGroup5(dt_Data);
            ClearGroup6(dt_Data);
            ClearGroup7(dt_Data);
            ClearGroup8(dt_Data);
            ClearGroup9(dt_Data);
            ClearGroup10(dt_Data);
            ClearGroup12(dt_Data);
            ClearGroup13(dt_Data);
            ClearGroup14(dt_Data);
            ClearGroup15(dt_Data);
            ClearGroup16(dt_Data);
            ClearGroup17(dt_Data);
            ClearGroup18(dt_Data);
            ClearGroup19(dt_Data);

            if (Convert.ToDateTime(radDateTimePicker_Car.Value).Date < DateTime.Now.Date)
            {
                radToggleSwitch1.ReadOnly = true;
                radToggleSwitch2.ReadOnly = true;
                radToggleSwitch3.ReadOnly = true;
                radToggleSwitch4.ReadOnly = true;
                radToggleSwitch5.ReadOnly = true;
                radToggleSwitch6.ReadOnly = true;
                radToggleSwitch7.ReadOnly = true;
                radToggleSwitch8.ReadOnly = true;
                radToggleSwitch9.ReadOnly = true;
                radToggleSwitch10.ReadOnly = true;
                radToggleSwitch12.ReadOnly = true;
                radToggleSwitch13.ReadOnly = true;
                radToggleSwitch14.ReadOnly = true;
                radToggleSwitch15.ReadOnly = true;
                radToggleSwitch16.ReadOnly = true;
                radToggleSwitch17.ReadOnly = true;
                radToggleSwitch18.ReadOnly = true;
                radToggleSwitch19.ReadOnly = true;
            }
        }
        //Set HD
        void SetData()
        {
            this.Cursor = Cursors.WaitCursor;
            dt_Data = Logistic_Class.CARCARE_FindData(radDateTimePicker_Car.Value.ToString("yyyy-MM-dd"), radDateTimePicker_Car.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect0);
            this.Cursor = Cursors.Default;
        }

        #region "AddValues"
        //Claer From 1
        void ClearGroup1(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel1.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch1.SetToggleState(false);// radToggleSwitch1.ReadOnly = true;
                radLabelCarID1.BorderVisible = true; radLabelCarName1.BorderVisible = true;
                radLabelDptID1.BorderVisible = true; radLabelDptName1.BorderVisible = true;
                radLabelCarID1.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName1.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID1.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName1.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch1.SetToggleState(true); radToggleSwitch1.ReadOnly = false;
                radLabelCarID1.Text = ""; radLabelCarID1.BorderVisible = false;
                radLabelCarName1.Text = ""; radLabelCarName1.BorderVisible = false;
                radLabelDptID1.Text = ""; radLabelDptID1.BorderVisible = false;
                radLabelDptName1.Text = ""; radLabelDptName1.BorderVisible = false;
            }
        }
        //Claer From 2
        void ClearGroup2(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel2.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch2.SetToggleState(false);// radToggleSwitch2.ReadOnly = true;
                radLabelCarID2.BorderVisible = true; radLabelCarName2.BorderVisible = true;
                radLabelDptID2.BorderVisible = true; radLabelDptName2.BorderVisible = true;
                radLabelCarID2.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName2.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID2.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName2.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch2.SetToggleState(true); ; radToggleSwitch2.ReadOnly = false;
                radLabelCarID2.Text = ""; radLabelCarID2.BorderVisible = false;
                radLabelCarName2.Text = ""; radLabelCarName2.BorderVisible = false;
                radLabelDptID2.Text = ""; radLabelDptID2.BorderVisible = false;
                radLabelDptName2.Text = ""; radLabelDptName2.BorderVisible = false;
            }
        }
        //Claer From 3
        void ClearGroup3(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel3.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch3.SetToggleState(false);// radToggleSwitch3.ReadOnly = true;
                radLabelCarID3.BorderVisible = true; radLabelCarName3.BorderVisible = true;
                radLabelDptID3.BorderVisible = true; radLabelDptName3.BorderVisible = true;
                radLabelCarID3.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName3.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID3.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName3.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch3.SetToggleState(true); radToggleSwitch3.ReadOnly = false;
                radLabelCarID3.Text = ""; radLabelCarID3.BorderVisible = false;
                radLabelCarName3.Text = ""; radLabelCarName3.BorderVisible = false;
                radLabelDptID3.Text = ""; radLabelDptID3.BorderVisible = false;
                radLabelDptName3.Text = ""; radLabelDptName3.BorderVisible = false;
            }
        }
        //Claer From 4
        void ClearGroup4(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel4.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch4.SetToggleState(false);// radToggleSwitch4.ReadOnly = true;
                radLabelCarID4.BorderVisible = true; radLabelCarName4.BorderVisible = true;
                radLabelDptID4.BorderVisible = true; radLabelDptName4.BorderVisible = true;
                radLabelCarID4.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName4.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID4.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName4.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch4.SetToggleState(true); radToggleSwitch4.ReadOnly = false;
                radLabelCarID4.Text = ""; radLabelCarID4.BorderVisible = false;
                radLabelCarName4.Text = ""; radLabelCarName4.BorderVisible = false;
                radLabelDptID4.Text = ""; radLabelDptID4.BorderVisible = false;
                radLabelDptName4.Text = ""; radLabelDptName4.BorderVisible = false;
            }
        }
        //Claer From 5
        void ClearGroup5(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel5.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch5.SetToggleState(false);// radToggleSwitch5.ReadOnly = true;
                radLabelCarID5.BorderVisible = true; radLabelCarName5.BorderVisible = true;
                radLabelDptID5.BorderVisible = true; radLabelDptName5.BorderVisible = true;
                radLabelCarID5.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName5.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID5.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName5.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch5.SetToggleState(true); radToggleSwitch5.ReadOnly = false;
                radLabelCarID5.Text = ""; radLabelCarID5.BorderVisible = false;
                radLabelCarName5.Text = ""; radLabelCarName5.BorderVisible = false;
                radLabelDptID5.Text = ""; radLabelDptID5.BorderVisible = false;
                radLabelDptName5.Text = ""; radLabelDptName5.BorderVisible = false;
            }
        }
        //Claer From 6
        void ClearGroup6(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel6.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch6.SetToggleState(false); //radToggleSwitch6.ReadOnly = true;
                radLabelCarID6.BorderVisible = true; radLabelCarName6.BorderVisible = true;
                radLabelDptID6.BorderVisible = true; radLabelDptName6.BorderVisible = true;
                radLabelCarID6.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName6.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID6.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName6.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch6.SetToggleState(true); radToggleSwitch6.ReadOnly = false;
                radLabelCarID6.Text = ""; radLabelCarID6.BorderVisible = false;
                radLabelCarName6.Text = ""; radLabelCarName6.BorderVisible = false;
                radLabelDptID6.Text = ""; radLabelDptID6.BorderVisible = false;
                radLabelDptName6.Text = ""; radLabelDptName6.BorderVisible = false;
            }
        }
        //Claer From 7
        void ClearGroup7(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel7.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch7.SetToggleState(false); //radToggleSwitch7.ReadOnly = true;
                radLabelCarID7.BorderVisible = true; radLabelCarName7.BorderVisible = true;
                radLabelDptID7.BorderVisible = true; radLabelDptName7.BorderVisible = true;
                radLabelCarID7.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName7.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID7.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName7.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch7.SetToggleState(true); radToggleSwitch7.ReadOnly = false;
                radLabelCarID7.Text = ""; radLabelCarID7.BorderVisible = false;
                radLabelCarName7.Text = ""; radLabelCarName7.BorderVisible = false;
                radLabelDptID7.Text = ""; radLabelDptID7.BorderVisible = false;
                radLabelDptName7.Text = ""; radLabelDptName7.BorderVisible = false;
            }
        }
        //Claer From 8
        void ClearGroup8(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel8.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch8.SetToggleState(false);// radToggleSwitch8.ReadOnly = true;
                radLabelCarID8.BorderVisible = true; radLabelCarName8.BorderVisible = true;
                radLabelDptID8.BorderVisible = true; radLabelDptName8.BorderVisible = true;
                radLabelCarID8.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName8.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID8.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName8.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch8.SetToggleState(true); radToggleSwitch8.ReadOnly = false;
                radLabelCarID8.Text = ""; radLabelCarID8.BorderVisible = false;
                radLabelCarName8.Text = ""; radLabelCarName8.BorderVisible = false;
                radLabelDptID8.Text = ""; radLabelDptID8.BorderVisible = false;
                radLabelDptName8.Text = ""; radLabelDptName8.BorderVisible = false;
            }
        }
        //Claer From 9
        void ClearGroup9(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel9.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch9.SetToggleState(false);// radToggleSwitch9.ReadOnly = true;
                radLabelCarID9.BorderVisible = true; radLabelCarName9.BorderVisible = true;
                radLabelDptID9.BorderVisible = true; radLabelDptName9.BorderVisible = true;
                radLabelCarID9.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName9.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID9.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName9.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch9.SetToggleState(true); radToggleSwitch9.ReadOnly = false;
                radLabelCarID9.Text = ""; radLabelCarID9.BorderVisible = false;
                radLabelCarName9.Text = ""; radLabelCarName9.BorderVisible = false;
                radLabelDptID9.Text = ""; radLabelDptID9.BorderVisible = false;
                radLabelDptName9.Text = ""; radLabelDptName9.BorderVisible = false;
            }
        }
        //Claer From 10
        void ClearGroup10(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel10.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch10.SetToggleState(false); //radToggleSwitch10.ReadOnly = true;
                radLabelCarID10.BorderVisible = true; radLabelCarName10.BorderVisible = true;
                radLabelDptID10.BorderVisible = true; radLabelDptName10.BorderVisible = true;
                radLabelCarID10.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName10.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID10.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName10.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch10.SetToggleState(true); radToggleSwitch10.ReadOnly = false;
                radLabelCarID10.Text = ""; radLabelCarID10.BorderVisible = false;
                radLabelCarName10.Text = ""; radLabelCarName10.BorderVisible = false;
                radLabelDptID10.Text = ""; radLabelDptID10.BorderVisible = false;
                radLabelDptName10.Text = ""; radLabelDptName10.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup12(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel12.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch12.SetToggleState(false);// radToggleSwitch12.ReadOnly = true;

                radLabelCarID12.BorderVisible = true; radLabelCarName12.BorderVisible = true;
                radLabelDptID12.BorderVisible = true; radLabelDptName12.BorderVisible = true;
                radLabelCarID12.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName12.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID12.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName12.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch12.SetToggleState(true); radToggleSwitch12.ReadOnly = false;
                radLabelCarID12.Text = ""; radLabelCarID12.BorderVisible = false;
                radLabelCarName12.Text = ""; radLabelCarName12.BorderVisible = false;
                radLabelDptID12.Text = ""; radLabelDptID12.BorderVisible = false;
                radLabelDptName12.Text = ""; radLabelDptName12.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup13(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel13.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch13.SetToggleState(false);// radToggleSwitch13.ReadOnly = true;

                radLabelCarID13.BorderVisible = true; radLabelCarName13.BorderVisible = true;
                radLabelDptID13.BorderVisible = true; radLabelDptName13.BorderVisible = true;
                radLabelCarID13.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName13.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID13.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName13.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch13.SetToggleState(true); radToggleSwitch13.ReadOnly = false;
                radLabelCarID13.Text = ""; radLabelCarID13.BorderVisible = false;
                radLabelCarName13.Text = ""; radLabelCarName13.BorderVisible = false;
                radLabelDptID13.Text = ""; radLabelDptID13.BorderVisible = false;
                radLabelDptName13.Text = ""; radLabelDptName13.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup14(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel14.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch14.SetToggleState(false);// radToggleSwitch14.ReadOnly = true;

                radLabelCarID14.BorderVisible = true; radLabelCarName14.BorderVisible = true;
                radLabelDptID14.BorderVisible = true; radLabelDptName14.BorderVisible = true;
                radLabelCarID14.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName14.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID14.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName14.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch14.SetToggleState(true); radToggleSwitch14.ReadOnly = false;
                radLabelCarID14.Text = ""; radLabelCarID14.BorderVisible = false;
                radLabelCarName14.Text = ""; radLabelCarName14.BorderVisible = false;
                radLabelDptID14.Text = ""; radLabelDptID14.BorderVisible = false;
                radLabelDptName14.Text = ""; radLabelDptName14.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup15(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel15.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch15.SetToggleState(false);// radToggleSwitch15.ReadOnly = true;

                radLabelCarID15.BorderVisible = true; radLabelCarName15.BorderVisible = true;
                radLabelDptID15.BorderVisible = true; radLabelDptName15.BorderVisible = true;
                radLabelCarID15.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName15.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID15.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName15.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch15.SetToggleState(true); radToggleSwitch15.ReadOnly = false;
                radLabelCarID15.Text = ""; radLabelCarID15.BorderVisible = false;
                radLabelCarName15.Text = ""; radLabelCarName15.BorderVisible = false;
                radLabelDptID15.Text = ""; radLabelDptID15.BorderVisible = false;
                radLabelDptName15.Text = ""; radLabelDptName15.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup16(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel16.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch16.SetToggleState(false); //radToggleSwitch16.ReadOnly = true;

                radLabelCarID16.BorderVisible = true; radLabelCarName16.BorderVisible = true;
                radLabelDptID16.BorderVisible = true; radLabelDptName16.BorderVisible = true;
                radLabelCarID16.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName16.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID16.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName16.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch16.SetToggleState(true); radToggleSwitch16.ReadOnly = false;
                radLabelCarID16.Text = ""; radLabelCarID16.BorderVisible = false;
                radLabelCarName16.Text = ""; radLabelCarName16.BorderVisible = false;
                radLabelDptID16.Text = ""; radLabelDptID16.BorderVisible = false;
                radLabelDptName16.Text = ""; radLabelDptName16.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup17(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel17.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch17.SetToggleState(false);// radToggleSwitch17.ReadOnly = true;
                radLabelCarID17.BorderVisible = true; radLabelCarName17.BorderVisible = true;
                radLabelDptID17.BorderVisible = true; radLabelDptName17.BorderVisible = true;
                radLabelCarID17.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName17.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID17.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName17.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch17.SetToggleState(true); radToggleSwitch17.ReadOnly = false;
                radLabelCarID17.Text = ""; radLabelCarID17.BorderVisible = false;
                radLabelCarName17.Text = ""; radLabelCarName17.BorderVisible = false;
                radLabelDptID17.Text = ""; radLabelDptID17.BorderVisible = false;
                radLabelDptName17.Text = ""; radLabelDptName17.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup18(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel18.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch18.SetToggleState(false); //radToggleSwitch18.ReadOnly = true;
                radLabelCarID18.BorderVisible = true; radLabelCarName18.BorderVisible = true;
                radLabelDptID18.BorderVisible = true; radLabelDptName18.BorderVisible = true;
                radLabelCarID18.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName18.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID18.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName18.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch18.SetToggleState(true); radToggleSwitch18.ReadOnly = false;
                radLabelCarID18.Text = ""; radLabelCarID18.BorderVisible = false;
                radLabelCarName18.Text = ""; radLabelCarName18.BorderVisible = false;
                radLabelDptID18.Text = ""; radLabelDptID18.BorderVisible = false;
                radLabelDptName18.Text = ""; radLabelDptName18.BorderVisible = false;
            }
        }
        //Claer From 
        void ClearGroup19(DataTable SetValue)
        {
            DataRow[] result = SetValue.Select($@"CARCARE_TIME = '{radLabel19.Text}'");
            if (result.Length > 0)
            {
                radToggleSwitch19.SetToggleState(false); radToggleSwitch19.ReadOnly = true;
                radLabelCarID19.BorderVisible = true; radLabelCarName19.BorderVisible = true;
                radLabelDptID19.BorderVisible = true; radLabelDptName19.BorderVisible = true;
                radLabelCarID19.Text = result[0]["CARCARE_CARID"].ToString();
                radLabelCarName19.Text = result[0]["CARCARE_CARNAME"].ToString();
                radLabelDptID19.Text = result[0]["CARCARE_DPTID"].ToString();
                radLabelDptName19.Text = result[0]["CARCARE_DPTNAME"].ToString();
            }
            else
            {
                radToggleSwitch19.SetToggleState(true); radToggleSwitch19.ReadOnly = false;
                radLabelCarID19.Text = ""; radLabelCarID19.BorderVisible = false;
                radLabelCarName19.Text = ""; radLabelCarName19.BorderVisible = false;
                radLabelDptID19.Text = ""; radLabelDptID19.BorderVisible = false;
                radLabelDptName19.Text = ""; radLabelDptName19.BorderVisible = false;
            }
        }

        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }
            radDateTimePicker_Car.Value = DateTime.Now;
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }


        //Change Date
        private void RadDateTimePicker_Car_ValueChanged(object sender, EventArgs e)
        {
            SetText();
        }
        //Insert Database
        void InsertData(RadLabel labelTime, RadLabel labelCarID, string pLabel2, string pLabel3)
        {

            if (Convert.ToDateTime(radDateTimePicker_Car.Value).Date < DateTime.Now.Date) return;

            if (labelCarID.Text == "")
            {
                //SetCar();
                dt_Car = LogisticClass.CARCARE_FindData();

                CarCare_ShowData inputData = new CarCare_ShowData(radDateTimePicker_Car.Value.ToString("yyyy-MM-dd"), labelTime.Text, pLabel2, pLabel3)
                { dtData = dt_Car };
                if (inputData.ShowDialog(this) == DialogResult.Yes) { }
            }
            else
            {
                if (_pPermission == "0") return;

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการยกเลิกรถ {labelCarID.Text } ที่จอง {Environment.NewLine}ในข่วงเวลาที่ระบุ {labelTime.Text}
                    {Environment.NewLine}จะยกเลิกรถทะเบียนนี้ทั้งหมดในวันนี้ทุกช่วงเวลา ?") == DialogResult.No) return;
 
                string T = Logistic_Class.CARCARE_DeleteRow(radDateTimePicker_Car.Value.ToString("yyyy-MM-dd"), labelTime.Text);// ConnectionClass.ExecuteSQL_Main(sqlDel);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "") SetText();
            }
        }

        #region "Set7.00_1"
        //Set 7.00-7.30
        private void RadToggleSwitch1_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID2.Text == "")
            {
                label2 = radLabel2.Text;
                if (radLabelCarID3.Text == "") label3 = radLabel3.Text;
            }

            InsertData(radLabel1, radLabelCarID1, label2, label3);
        }
        private void RadToggleSwitch1_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup1(dt_Data);
            ClearGroup2(dt_Data);
            ClearGroup3(dt_Data);
        }
        #endregion

        #region "Set7.30_2"
        private void RadToggleSwitch2_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID3.Text == "")
            {
                label2 = radLabel3.Text;
                if (radLabelCarID4.Text == "") label3 = radLabel4.Text;
            }
            InsertData(radLabel2, radLabelCarID2, label2, label3);
        }
        private void RadToggleSwitch2_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup2(dt_Data);
            ClearGroup3(dt_Data);
            ClearGroup4(dt_Data);
        }
        #endregion

        #region "Set8.00_3"
        private void RadToggleSwitch3_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup3(dt_Data);
            ClearGroup4(dt_Data);
            ClearGroup5(dt_Data);
        }

        private void RadToggleSwitch3_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID4.Text == "")
            {
                label2 = radLabel4.Text;
                if (radLabelCarID5.Text == "") label3 = radLabel5.Text;
            }

            InsertData(radLabel3, radLabelCarID3, label2, label3);
        }
        #endregion

        #region "Set8.30_4"
        private void RadToggleSwitch4_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup4(dt_Data);
            ClearGroup5(dt_Data);
            ClearGroup6(dt_Data);
        }

        private void RadToggleSwitch4_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID5.Text == "")
            {
                label2 = radLabel5.Text;
                if (radLabelCarID6.Text == "") label3 = radLabel6.Text;
            }

            InsertData(radLabel4, radLabelCarID4, label2, label3);
        }
        #endregion

        #region "Set9.00_5"

        private void RadToggleSwitch5_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup5(dt_Data);
            ClearGroup6(dt_Data);
            ClearGroup7(dt_Data);
        }

        private void RadToggleSwitch5_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID6.Text == "")
            {
                label2 = radLabel6.Text;
                if (radLabelCarID7.Text == "") label3 = radLabel7.Text;
            }
            InsertData(radLabel5, radLabelCarID5, label2, label3);
        }
        #endregion

        #region "Set9.30_6"
        private void RadToggleSwitch6_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup6(dt_Data);
            ClearGroup7(dt_Data);
            ClearGroup8(dt_Data);
        }

        private void RadToggleSwitch6_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID7.Text == "")
            {
                label2 = radLabel7.Text;
                if (radLabelCarID8.Text == "") label3 = radLabel8.Text;
            }
            InsertData(radLabel6, radLabelCarID6, label2, label3);
        }
        #endregion

        #region "Set10.00_7"
        private void RadToggleSwitch7_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup7(dt_Data);
            ClearGroup8(dt_Data);
            ClearGroup9(dt_Data);
        }

        private void RadToggleSwitch7_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID8.Text == "")
            {
                label2 = radLabel8.Text;
                if (radLabelCarID9.Text == "") label3 = radLabel9.Text;
            }

            InsertData(radLabel7, radLabelCarID7, label2, label3);
        }
        #endregion

        #region "Set10.30_8"
        private void RadToggleSwitch8_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID9.Text == "")
            {
                label2 = radLabel9.Text;
                if (radLabelCarID10.Text == "") label3 = radLabel10.Text;
            }

            InsertData(radLabel8, radLabelCarID8, label2, label3);
        }

        private void RadToggleSwitch8_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup8(dt_Data);
            ClearGroup9(dt_Data);
            ClearGroup10(dt_Data);
        }
        #endregion

        #region "Set11.00_9"
        private void RadToggleSwitch9_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup9(dt_Data);
            ClearGroup10(dt_Data);
            ClearGroup12(dt_Data);
        }

        private void RadToggleSwitch9_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID10.Text == "")
            {
                label2 = radLabel10.Text;
                if (radLabelCarID12.Text == "") label3 = radLabel12.Text;
            }

            InsertData(radLabel9, radLabelCarID9, label2, label3);
        }
        #endregion

        #region "Set11.30_10"
        private void RadToggleSwitch10_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup10(dt_Data);
            ClearGroup12(dt_Data);
            ClearGroup13(dt_Data);
        }

        private void RadToggleSwitch10_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID12.Text == "")
            {
                label2 = radLabel12.Text;
                if (radLabelCarID13.Text == "") label3 = radLabel13.Text;
            }

            InsertData(radLabel10, radLabelCarID10, label2, label3);
        }
        #endregion

        #region "Set13.00_12"
        private void RadToggleSwitch12_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup12(dt_Data);
            ClearGroup13(dt_Data);
            ClearGroup14(dt_Data);
        }

        private void RadToggleSwitch12_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID13.Text == "")
            {
                label2 = radLabel13.Text;
                if (radLabelCarID14.Text == "") label3 = radLabel14.Text;
            }

            InsertData(radLabel12, radLabelCarID12, label2, label3);
        }
        #endregion

        #region "Set13.30_13"
        private void RadToggleSwitch13_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID14.Text == "")
            {
                label2 = radLabel14.Text;
                if (radLabelCarID15.Text == "") label3 = radLabel15.Text;
            }

            InsertData(radLabel13, radLabelCarID13, label2, label3);
        }

        private void RadToggleSwitch13_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup13(dt_Data);
            ClearGroup14(dt_Data);
            ClearGroup15(dt_Data);
        }

        #endregion

        #region "Set14.00_14"
        private void RadToggleSwitch14_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup14(dt_Data);
            ClearGroup15(dt_Data);
            ClearGroup16(dt_Data);
        }

        private void RadToggleSwitch14_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID15.Text == "")
            {
                label2 = radLabel15.Text;
                if (radLabelCarID16.Text == "") label3 = radLabel16.Text;
            }

            InsertData(radLabel14, radLabelCarID14, label2, label3);
        }
        #endregion

        #region "Set14.30_15"
        private void RadToggleSwitch15_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup15(dt_Data);
            ClearGroup16(dt_Data);
            ClearGroup17(dt_Data);
        }

        private void RadToggleSwitch15_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID16.Text == "")
            {
                label2 = radLabel16.Text;
                if (radLabelCarID17.Text == "") label3 = radLabel17.Text;
            }

            InsertData(radLabel15, radLabelCarID15, label2, label3);
        }
        #endregion

        #region "Set15.00_16"
        private void RadToggleSwitch16_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID17.Text == "")
            {
                label2 = radLabel17.Text;
                if (radLabelCarID18.Text == "") label3 = radLabel18.Text;
            }

            InsertData(radLabel16, radLabelCarID16, label2, label3);
        }

        private void RadToggleSwitch16_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup16(dt_Data);
            ClearGroup17(dt_Data);
            ClearGroup18(dt_Data);
        }
        #endregion

        #region "Set15.30_17"
        private void RadToggleSwitch17_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup17(dt_Data);
            ClearGroup18(dt_Data);
            ClearGroup19(dt_Data);
        }

        private void RadToggleSwitch17_Click(object sender, EventArgs e)
        {
            string label2 = "", label3 = "";
            if (radLabelCarID18.Text == "")
            {
                label2 = radLabel18.Text;
                if (radLabelCarID19.Text == "") label3 = radLabel19.Text;
            }

            InsertData(radLabel17, radLabelCarID17, label2, label3);
        }

        #endregion

        #region "Set16.00_18"
        private void RadToggleSwitch18_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup18(dt_Data);
            ClearGroup19(dt_Data);
        }

        private void RadToggleSwitch18_Click(object sender, EventArgs e)
        {
            string label2 = "";
            if (radLabelCarID19.Text == "") label2 = radLabel19.Text;

            InsertData(radLabel18, radLabelCarID18, label2, "");
        }
        #endregion

        #region "Set16.30_19"
        private void RadToggleSwitch19_ValueChanged(object sender, EventArgs e)
        {
            SetData();
            ClearGroup19(dt_Data);
        }

        private void RadToggleSwitch19_Click(object sender, EventArgs e)
        {
            InsertData(radLabel19, radLabelCarID19, "", "");

        }

        #endregion

        private void RadButtonElement11_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
    }
}
