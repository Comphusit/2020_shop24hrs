﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Logistic
{
    public partial class CarCare_ShowData : Telerik.WinControls.UI.RadForm
    {
        public DataTable dtData;

        readonly string _pDate;
        readonly string _pLabel1;
        readonly string _pLabel2;
        readonly string _pLabel3;
             

        public CarCare_ShowData(string pDate, string pLabel1, string pLabel2, string pLabel3)
        {
            InitializeComponent();
            _pDate = pDate;
            _pLabel1 = pLabel1;
            _pLabel2 = pLabel2;
            _pLabel3 = pLabel3;
        }

        private void ShowDataDGV_Load(object sender, EventArgs e)
        {
            radButton_30.ButtonElement.ShowBorder = true; radButton_30.Enabled = true;
            radButton_60.ButtonElement.ShowBorder = true; radButton_60.Enabled = false;
            radButton_90.ButtonElement.ShowBorder = true; radButton_90.Enabled = false;

            radLabel_30.Text = "เวลาสำหรับ กะบะ/เก๋ง/รถ 4 ล้อ";
            radLabel_60.Text = "เวลาสำหรับ รถ 6 ล้อ/รถ 10 ล้อ";
            radLabel_90.Text = "เวลาสำหรับ รถพ่วง";


            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียน", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อรถ", 300)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATE_HISTORY", "ล้างล่าสุด", 110)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATE_BOOK", "คิวที่จอง", 110)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPTID", "แผนก", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPTNAME", "ชื่อแผนก", 300)));
            radGridView_Show.MasterTemplate.Columns["VEHICLEID"].IsPinned = true;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "DATE_HISTORY <> '' ", false)
            { CellBackColor = ConfigClass.SetColor_YellowPastel() };
            radGridView_Show.Columns["DATE_HISTORY"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "DATE_BOOK <> '' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["DATE_BOOK"].ConditionalFormattingObjectList.Add(obj2);

            radGridView_Show.DataSource = dtData;
                        
            if (_pLabel2 != "") radButton_60.Enabled = true;
            if (_pLabel3 != "") radButton_90.Enabled = true;

            radLabel_Head.Text = $@"วันที่ระบุจอง {_pDate} : เวลาเริ่มจอง {_pLabel1}";
            radLabel_Detail.Text = "ค้นหารถที่ต้องการได้จากแถวด้านบนสุด | เลือกรถที่ต้องการ >> กดเลือกเวลาที่ใช้ล้าง | สีเหลือง >> ประวัติล้างล่าสุด | สีแดง >> มีคิวล้างอยู่แล้ว";
        }

       
        void InsertData(string pTimeUse)
        {
            string pVEHICLEID = radGridView_Show.CurrentRow.Cells["VEHICLEID"].Value.ToString();

            if (pVEHICLEID == "")
            { MsgBoxClass.MsgBoxShow_ChooseDataWarning("ข้อมูล"); return; }

            ArrayList sql = new ArrayList();

            if (radGridView_Show.CurrentRow.Cells["DATE_BOOK"].Value.ToString() != "")
            {
                string msg = $@"ทะเบียน {pVEHICLEID}  {Environment.NewLine} ถูกจองในวันที่ {radGridView_Show.CurrentRow.Cells["DATE_BOOK"].Value} {Environment.NewLine} {Environment.NewLine} ต้องการเปลี่ยนแปลงวันที่ล้างรถ ?";
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult(msg) == DialogResult.No)
                { return; }
                else
                {
               
                    sql.Add(Logistic_Class.CARCARE_DeleteRowSql(radGridView_Show.CurrentRow.Cells["DATE_BOOK"].Value.ToString(), pVEHICLEID));
                }
            }

            string pNAME = radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString();
            string pDPTID = radGridView_Show.CurrentRow.Cells["DPTID"].Value.ToString();
            string pDPTNAME = radGridView_Show.CurrentRow.Cells["DPTNAME"].Value.ToString();

            //กรณีจอง 30 นาที
            sql.Add(Logistic_Class.CARCARE_InsertRow(_pDate, pVEHICLEID, pNAME, pDPTID, pDPTNAME, _pLabel1));
            //กรณีจอง 60 นาที
            if (pTimeUse == "60")
            {
                sql.Add(Logistic_Class.CARCARE_InsertRow(_pDate,pVEHICLEID, pNAME, pDPTID, pDPTNAME, _pLabel2));
            }
            //กรณีจอง 90 นาที
            if (pTimeUse == "90")
            {
                sql.Add(Logistic_Class.CARCARE_InsertRow(_pDate, pVEHICLEID, pNAME, pDPTID, pDPTNAME, _pLabel2));
                sql.Add(Logistic_Class.CARCARE_InsertRow(_pDate, pVEHICLEID, pNAME, pDPTID, pDPTNAME, _pLabel3));
            }
            string result = ConnectionClass.ExecuteSQL_ArrayMain(sql);

            if (result == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(result);
                return;
            }
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        private void RadButton_30_Click(object sender, EventArgs e)
        {
            InsertData("30");
        }

        private void RadButton_60_Click(object sender, EventArgs e)
        {
            InsertData("60");
        }

        private void RadButton_90_Click(object sender, EventArgs e)
        {
            InsertData("90");
        }
        //Close
        private void RadButton_Cancle_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
