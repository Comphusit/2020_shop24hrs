﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using Telerik.WinControls.Data;
using System.ComponentModel;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class I_Item : Telerik.WinControls.UI.RadForm
    {
        string staSave;

        DataTable dt = new DataTable();
        //Load
        public I_Item()
        {
            InitializeComponent();
        }
        //Load Main
        private void I_Item_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(radDropDownList_SubGroup);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radStatusStrip1.SizingGrip = false;

            this.radButtonElement_Add.ShowBorder = true;
            this.radButtonElement_Edit.ShowBorder = true;
            this.radButtonElement_Delete.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_main);

            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "ใช้งาน"));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อสินค้า", 300));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN", "กลุ่มหลัก", 100));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN_NAME", "ชื่อกลุ่มหลัก", 150));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPSUB", "กลุ่มย่อย", 100));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPSUB_NAME", "ชื่อกลุ่มย่อย", 150));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE", "โซนจัดสินค้า", 120));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 300));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้สร้าง", 100));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ชื่อผู้สร้าง", 300));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DptID", "จัดซื้อ", 80));
            radGridView_main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DptName", "ชื่อจัดซื้อ", 300));

            ClearData();
            this.Cursor = Cursors.WaitCursor;
            dt = BillOutClass.Find_ItemRO();
            radGridView_main.DataSource = dt;
            this.Cursor = Cursors.Default;
        }
        //sub grp
        void Set_GropMain()
        {
            radDropDownList_MainGroup.DataSource = I_Class.Find_GroupMain("'0','1'");
            radDropDownList_MainGroup.ValueMember = "GROUPRO_ID";
            radDropDownList_MainGroup.DisplayMember = "GROUPRO_NAME";

            Set_GropSub();
        }
        //main grp
        void Set_GropSub()
        {
            string grpMain;
            try
            {
                grpMain = radDropDownList_MainGroup.SelectedValue.ToString();
            }
            catch (Exception)
            {
                radDropDownList_MainGroup.SelectedIndex = 0;
                grpMain = radDropDownList_MainGroup.SelectedValue.ToString();
            }

            radDropDownList_SubGroup.DataSource = I_Class.Find_GroupSub(grpMain, "'0','1'");
            radDropDownList_SubGroup.ValueMember = "GROUPRO_SUBID";
            radDropDownList_SubGroup.DisplayMember = "GROUPRO_SUBNAME";
        }
        //claer
        void ClearData()
        {
            staSave = "0";
            radTextBox_Barcode.Text = ""; radTextBox_Barcode.Enabled = false;
            radTextBox_Name.Text = ""; //radTextBox_Name.ReadOnly= true;
            radTextBox_Zone.Text = ""; radTextBox_Zone.Enabled = false;
            radTextBox_Dpt.Text = ""; radTextBox_Dpt.Enabled = false;
            radTextBox_Rmk.Text = ""; radTextBox_Rmk.Enabled = false;
            radDropDownList_MainGroup.Enabled = false;
            radDropDownList_SubGroup.Enabled = false;
            radButton_Save.Enabled = false;
            radCheckBox_Sta.CheckState = CheckState.Checked; radCheckBox_Sta.Enabled = false;
            Set_GropMain();
        }

        #region RowsGrid
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_main_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        //เพิ่มสินค้า
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            radCheckBox_Sta.Enabled = true;
            radTextBox_Barcode.Text = ""; radTextBox_Barcode.Enabled = true;
            radTextBox_Barcode.Focus();
            staSave = "1";
        }

        //Enter
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Barcode.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                    return;
                }

                DataTable dtBarcode = BillOutClass.Find_ItemRoByItembarcode(radTextBox_Barcode.Text.Trim());
                if (dtBarcode.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }

                if (dtBarcode.Rows[0]["STA24"].ToString() != "0")
                {
                    MessageBox.Show("สินค้านี้มีอยู่ในระบบเรียบร้อยแล้ว ไม่ต้องนำเข้าซ้ำ.", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }

                if (dtBarcode.Rows[0]["SPC_ITEMACTIVE"].ToString() == "0")
                {
                    MessageBox.Show("สินค้านี้มีสถานะไม่เคลื่อนไหว ไม่สามารถใช้งานได้.", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }

                radTextBox_Barcode.Text = dtBarcode.Rows[0]["ITEMBARCODE"].ToString();
                radTextBox_Name.Text = dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString();
                radTextBox_Dpt.Text = dtBarcode.Rows[0]["DIMENSION"].ToString() + '-' + dtBarcode.Rows[0]["DESCRIPTION"].ToString();

                radCheckBox_Sta.Enabled = true;

                radTextBox_Zone.Enabled = true;
                radTextBox_Rmk.Enabled = true;
                radButton_Save.Enabled = true;
                radTextBox_Barcode.Enabled = false;
                radDropDownList_MainGroup.Enabled = true;
                radDropDownList_SubGroup.Enabled = true;

                radTextBox_Zone.Focus();

                Set_GropMain();

            }
        }
        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Select Change
        private void RadDropDownList_MainGroup_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            Set_GropSub();
            radDropDownList_SubGroup.Focus();
        }
        //Select Change
        private void RadDropDownList_SubGroup_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Zone.Focus();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string grpSub = "";
            string grpSubName = "";

            try
            {
                grpSub = radDropDownList_SubGroup.SelectedValue.ToString();
                grpSubName = radDropDownList_SubGroup.SelectedItem.Text;
            }
            catch (Exception) { }

            if (radTextBox_Barcode.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("บาร์โค้ดสินค้า");
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }

            string sta;
            if (radCheckBox_Sta.Checked == true) sta = "1"; else sta = "0";

            string sql;
            switch (staSave)
            {
                case "1": //Insert
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("บาร์โค้ดสินค้า") == DialogResult.No) return;
                    sql = I_Class.SHOP_RO_ITEM_InsertUpdate("0", radTextBox_Barcode.Text, radDropDownList_MainGroup.SelectedValue.ToString(),
                        grpSub, radTextBox_Zone.Text, radTextBox_Rmk.Text, sta);
                    break;

                case "2"://Update
                    if (MsgBoxClass.MsgBoxShow_ConfirmEdit("บาร์โค้ดสินค้า") == DialogResult.No) return;
                    sql = I_Class.SHOP_RO_ITEM_InsertUpdate("1", radTextBox_Barcode.Text, radDropDownList_MainGroup.SelectedValue.ToString(),
                       grpSub, radTextBox_Zone.Text, radTextBox_Rmk.Text, sta); 
                    break;

                case "3"://Delete   
                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete("บาร์โค้ดสินค้า") == DialogResult.No) return;
                    sql = I_Class.SHOP_RO_ITEM_InsertUpdate("2", radTextBox_Barcode.Text, "", "", "", "", "");
                   break;

                default:
                    // break;
                    return;
            }

            string T = ConnectionClass.ExecuteSQL_Main(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                switch (staSave)
                {
                    case "1":
                        DataRow row = dt.NewRow();
                        row[0] = sta;
                        row[1] = radTextBox_Barcode.Text;
                        row[2] = radTextBox_Name.Text;
                        row[3] = radDropDownList_MainGroup.SelectedValue.ToString();
                        row[4] = radDropDownList_MainGroup.SelectedItem.Text;
                        row[5] = grpSub;
                        row[6] = grpSubName;
                        row[7] = radTextBox_Zone.Text;
                        row[8] = radTextBox_Rmk.Text;
                        dt.Rows.Add(row);
                        dt.AcceptChanges();

                        SortDescriptor descriptor = new SortDescriptor()
                        {
                            PropertyName = "GROUPMAIN",
                            Direction = ListSortDirection.Ascending
                        };
                        this.radGridView_main.MasterTemplate.SortDescriptors.Add(descriptor);
                        break;

                    case "2":
                        radGridView_main.CurrentRow.Cells["GROUPMAIN"].Value = radDropDownList_MainGroup.SelectedValue.ToString();
                        radGridView_main.CurrentRow.Cells["GROUPMAIN_NAME"].Value = radDropDownList_MainGroup.SelectedItem.Text;

                        radGridView_main.CurrentRow.Cells["GROUPSUB"].Value = grpSub;
                        radGridView_main.CurrentRow.Cells["GROUPSUB_NAME"].Value = grpSubName;

                        radGridView_main.CurrentRow.Cells["REMARK"].Value = radTextBox_Rmk.Text;
                        radGridView_main.CurrentRow.Cells["ZONE"].Value = radTextBox_Zone.Text;
                        radGridView_main.CurrentRow.Cells["STA"].Value = sta;
                        dt.AcceptChanges();
                        break;

                    case "3":
                        radGridView_main.Rows.Remove(radGridView_main.CurrentRow);
                        dt.AcceptChanges();
                        break;

                    default:
                        return;
                }

                ClearData();
            }
        }
        //SetData For Edit
        void SetDataForEdit()
        {
            if (radGridView_main.CurrentRow.Cells[0].Value.ToString() == "") return;

            Set_GropMain();
            radTextBox_Barcode.Text = radGridView_main.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
            radTextBox_Name.Text = radGridView_main.CurrentRow.Cells["SPC_NAME"].Value.ToString();
            radTextBox_Zone.Text = radGridView_main.CurrentRow.Cells["ZONE"].Value.ToString();
            radTextBox_Rmk.Text = radGridView_main.CurrentRow.Cells["REMARK"].Value.ToString();
            radTextBox_Dpt.Text = radGridView_main.CurrentRow.Cells["DptName"].Value.ToString();
            radDropDownList_MainGroup.SelectedValue = radGridView_main.CurrentRow.Cells["GROUPMAIN"].Value.ToString();
            radDropDownList_SubGroup.SelectedValue = radGridView_main.CurrentRow.Cells["GROUPSUB"].Value.ToString();

            if (radGridView_main.CurrentRow.Cells["STA"].Value.ToString() == "1") radCheckBox_Sta.Checked = true;
            else radCheckBox_Sta.Checked = false;

            radButton_Save.Enabled = true;
            radTextBox_Zone.Focus();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            SetDataForEdit();
            radDropDownList_MainGroup.Enabled = true;
            radDropDownList_SubGroup.Enabled = true;
            radTextBox_Zone.Enabled = true;
            radTextBox_Rmk.Enabled = true;
            radCheckBox_Sta.Enabled = true;
            staSave = "2";
        }
        //Delete
        private void RadButtonElement_Delete_Click(object sender, EventArgs e)
        {
            SetDataForEdit();
            staSave = "3";
        }

        private void RadGridView_main_SelectionChanged(object sender, EventArgs e)
        {
            if (radButton_Save.Enabled == true) ClearData();
        }


    }
}
