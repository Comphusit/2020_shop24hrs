﻿namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    partial class MNRS_HOME
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Desc = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_QtyBox = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_MNRS = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Docno1 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Docno2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Show = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_EmpID = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_EmpName = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_QtyBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MNRS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmpID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(12, 69);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(401, 55);
            this.radTextBox_Remark.TabIndex = 1;
            this.radTextBox_Remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Remark_KeyDown);
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(111, 243);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(207, 243);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 4;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_Desc
            // 
            this.radLabel_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Desc.Location = new System.Drawing.Point(12, 49);
            this.radLabel_Desc.Name = "radLabel_Desc";
            this.radLabel_Desc.Size = new System.Drawing.Size(117, 19);
            this.radLabel_Desc.TabIndex = 24;
            this.radLabel_Desc.Text = "หมายเหตุ [Enter]";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(12, 128);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(116, 19);
            this.radLabel1.TabIndex = 25;
            this.radLabel1.Text = "จำนวนลัง [Enter]";
            // 
            // radTextBox_QtyBox
            // 
            this.radTextBox_QtyBox.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_QtyBox.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_QtyBox.Location = new System.Drawing.Point(14, 148);
            this.radTextBox_QtyBox.Name = "radTextBox_QtyBox";
            this.radTextBox_QtyBox.Size = new System.Drawing.Size(401, 25);
            this.radTextBox_QtyBox.TabIndex = 2;
            this.radTextBox_QtyBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_QtyBox_KeyDown);
            // 
            // radLabel_MNRS
            // 
            this.radLabel_MNRS.AutoSize = false;
            this.radLabel_MNRS.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_MNRS.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_MNRS.Location = new System.Drawing.Point(12, 24);
            this.radLabel_MNRS.Name = "radLabel_MNRS";
            this.radLabel_MNRS.Size = new System.Drawing.Size(63, 23);
            this.radLabel_MNRS.TabIndex = 63;
            this.radLabel_MNRS.Text = "MNRS";
            // 
            // radTextBox_Docno1
            // 
            this.radTextBox_Docno1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Docno1.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Docno1.Location = new System.Drawing.Point(68, 21);
            this.radTextBox_Docno1.MaxLength = 6;
            this.radTextBox_Docno1.Name = "radTextBox_Docno1";
            this.radTextBox_Docno1.Size = new System.Drawing.Size(82, 25);
            this.radTextBox_Docno1.TabIndex = 64;
            this.radTextBox_Docno1.Tag = "";
            this.radTextBox_Docno1.Text = "200422";
            // 
            // radTextBox_Docno2
            // 
            this.radTextBox_Docno2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Docno2.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Docno2.Location = new System.Drawing.Point(156, 21);
            this.radTextBox_Docno2.MaxLength = 6;
            this.radTextBox_Docno2.Name = "radTextBox_Docno2";
            this.radTextBox_Docno2.Size = new System.Drawing.Size(82, 25);
            this.radTextBox_Docno2.TabIndex = 0;
            this.radTextBox_Docno2.Tag = "";
            this.radTextBox_Docno2.Text = "000010";
            this.radTextBox_Docno2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Docno2_KeyDown);
            // 
            // radLabel_Show
            // 
            this.radLabel_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Show.Location = new System.Drawing.Point(68, 1);
            this.radLabel_Show.Name = "radLabel_Show";
            this.radLabel_Show.Size = new System.Drawing.Size(212, 19);
            this.radLabel_Show.TabIndex = 66;
            this.radLabel_Show.Text = "ระบุที่เลขที่บิลหลักสุดท้าย [Enter]";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(14, 179);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(104, 19);
            this.radLabel2.TabIndex = 26;
            this.radLabel2.Text = "ผู้จัดบิล [Enter]";
            // 
            // radTextBox_EmpID
            // 
            this.radTextBox_EmpID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_EmpID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmpID.Location = new System.Drawing.Point(14, 204);
            this.radTextBox_EmpID.Name = "radTextBox_EmpID";
            this.radTextBox_EmpID.Size = new System.Drawing.Size(136, 25);
            this.radTextBox_EmpID.TabIndex = 3;
            this.radTextBox_EmpID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmpID_KeyDown);
            // 
            // radTextBox_EmpName
            // 
            this.radTextBox_EmpName.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_EmpName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmpName.Location = new System.Drawing.Point(156, 204);
            this.radTextBox_EmpName.Name = "radTextBox_EmpName";
            this.radTextBox_EmpName.Size = new System.Drawing.Size(257, 25);
            this.radTextBox_EmpName.TabIndex = 4;
            // 
            // MNRS_HOME
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(427, 283);
            this.Controls.Add(this.radTextBox_EmpName);
            this.Controls.Add(this.radTextBox_EmpID);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radTextBox_Docno2);
            this.Controls.Add(this.radTextBox_Docno1);
            this.Controls.Add(this.radLabel_MNRS);
            this.Controls.Add(this.radTextBox_QtyBox);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.radTextBox_Remark);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radLabel_Desc);
            this.Controls.Add(this.radLabel_Show);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MNRS_HOME";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุหมายเหตุ - จำนวนลัง";
            this.Load += new System.EventHandler(this.MNRS_HOME_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_QtyBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MNRS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmpID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Desc;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_QtyBox;
        private Telerik.WinControls.UI.RadLabel radLabel_MNRS;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Docno1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Docno2;
        private Telerik.WinControls.UI.RadLabel radLabel_Show;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmpID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmpName;
    }
}
