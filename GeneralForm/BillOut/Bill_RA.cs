﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class Bill_RA : Telerik.WinControls.UI.RadForm
    {
        private DataTable dt = new DataTable();
        //Load
        public Bill_RA()
        {
            InitializeComponent();
        }
        //Load Main
        private void Bill_RA_Load(object sender, EventArgs e)
        {

            radButtonElement_pdf.ShowBorder = true; radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์รูปภาพ";
            radButtonElement_Refresh.ShowBorder = true; radButtonElement_Refresh.ToolTipText = "ดึงข้อมูลใหม่";

            radStatusStrip1.SizingGrip = false;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BillTrans", "เลขที่บิลรับ", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVOICE", "เลขที่บิลผู้จำหน่าย", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATE", "วันที่บิล", 110));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("AMOUNT", "ยอดเงิน", 110));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PXE", "เอกสาร PXE", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 700));
            RadGridView_Show.Columns["AMOUNT"].FormatString = "{0:#,##0.00}";

            DatagridClass.SetDefaultRadGridView(radGridView_Image);
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG1", "C_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG2", "C_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG3", "", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG3", "P_IMG3"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG3", "C_IMG3"));
            radGridView_Image.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Image.TableElement.RowHeight = 300;
            radGridView_Image.TableElement.TableHeaderHeight = 50;

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Begin, DateTime.Now.AddDays(-7), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_End, DateTime.Now, DateTime.Now);
        }
        //Set Valus
        void SetDGV()
        {
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();

            this.Cursor = Cursors.WaitCursor;

            dt = BillOutClass.FindBill_RA(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadDateTimePicker_End.Value.ToString("yyyy-MM-dd"));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string pxe = "";
                string rmk = "";
                DataTable dtPXE = BillOutClass.FindPXE_ByRA(dt.Rows[i]["BillTrans"].ToString());
                if (dtPXE.Rows.Count > 0)
                {
                    pxe = dtPXE.Rows[0]["PURCHID"].ToString();
                    rmk = dtPXE.Rows[0]["IVZ_REMARKS"].ToString();
                }

                RadGridView_Show.Rows.Add(dt.Rows[i]["BillTrans"].ToString(),
                    dt.Rows[i]["INVOICE"].ToString(),
                    dt.Rows[i]["CREATEDDATE"].ToString(),
                    dt.Rows[i]["AMOUNT"].ToString(), pxe, rmk);
            }

            //dt.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //OK
        private void RadButtonElement_OK_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Selection Change
        private void RadGridView_Show_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            try
            {
                radGridView_Image.Columns["IMG1"].HeaderText = "เลขที่บิลรับ " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString();
                radGridView_Image.Columns["IMG2"].HeaderText = "เลขที่บิลผู้จำหน่าย " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["INVOICE"].Value.ToString();
                radGridView_Image.Columns["IMG3"].HeaderText = "เลขที่บิลจ่าย  " + Environment.NewLine + RadGridView_Show.CurrentRow.Cells["PXE"].Value.ToString();

                FindImage(RadGridView_Show.CurrentRow.Cells["CREATEDDATE"].Value.ToString(),
                            RadGridView_Show.CurrentRow.Cells["BillTrans"].Value.ToString(),
                            RadGridView_Show.CurrentRow.Cells["PXE"].Value.ToString());
            }
            catch (Exception)
            {
                if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();
                return;
            }

        }
        //Find Image
        void FindImage(string pDate, string pBillRA, string pBillPXE)
        {
            radButtonElement_print.Enabled = false;
            if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();

            DirectoryInfo DirInfo = new DirectoryInfo(PathImageClass.pPathRepairRA + pDate);
            if (DirInfo.Exists == false) return;

            FileInfo[] Files = DirInfo.GetFiles("RA-RAINVOICE*_" + pBillRA + @"_R.JPG", SearchOption.AllDirectories);

            int FindIndex = 0;
            int Round = (int)Math.Ceiling((float)(Files.Length) / 3);
            for (int i = 0; i < Round; i++)
            {
                string img1 = Files[FindIndex].FullName;
                FindIndex += 1;
                string p_img1 = Files[FindIndex].Name;

                string img2 = PathImageClass.pImageEmply;
                string c_img2 = "0";
                string p_img2 = "";
                try
                {
                    img2 = Files[FindIndex].FullName; c_img2 = "1"; p_img2 = Files[FindIndex].Name;
                }
                catch (Exception) { }
                FindIndex += 1;

                string img3 = PathImageClass.pImageEmply;
                string c_img3 = "0"; string p_img3 = "";
                try
                {
                    img3 = Files[FindIndex].FullName; c_img3 = "1"; p_img3 = Files[FindIndex].Name;
                }
                catch (Exception) { }
                FindIndex += 1;

                radGridView_Image.Rows.Add(
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img1), PathImageClass.pPathRepairRA + pDate + @"|" + p_img1, "1",
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img2), PathImageClass.pPathRepairRA + pDate + @"|" + p_img2, c_img2,
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img3), PathImageClass.pPathRepairRA + pDate + @"|" + p_img3, c_img3);
            }

            if (pBillPXE != "") radButtonElement_print.Enabled = true;
        }
       
        //Image Set Format
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
       
        //DoubleClick
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "IMG1":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG1"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    break;
                case "IMG2":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG2"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG2"].Value.ToString());
                    break;
                case "IMG3":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG3"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG3"].Value.ToString());
                    break;
                default:
                    break;
            }
        } 
        private void RadButtonElement_print_Click(object sender, EventArgs e)
        {
            if (radGridView_Image.Rows.Count == 0) return;

            radGridView_Image.PrintPreview();
        }

        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(RadDateTimePicker_Begin, RadDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(RadDateTimePicker_Begin, RadDateTimePicker_End);
        }
    }
}
