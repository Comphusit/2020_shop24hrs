﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.JOB;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class BillOut_Detail : Telerik.WinControls.UI.RadForm
    {
        //check สิทธิ์ แผนก D062 + ComProgrammer
        readonly string _pBillID;

        string pType;
        string JobNumberID;
        string pGrpID;
        string pGrpDesc;
        string sSendDialog;
        DataTable Dt = new DataTable();
        #region "ROWS DGV"      
        private void RadGridView_Bill_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void RadGridView_Bill_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            ConditionalFormattingForm form = (ConditionalFormattingForm)sender;
            DatagridClass.SetFormFont(form.Controls);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            ConditionalFormattingForm form = (ConditionalFormattingForm)sender;
            DatagridClass.SetFormFont(form.Controls);
        }

        private void RadGridView_Bill_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            BaseFilterPopup popup = (BaseFilterPopup)e.FilterPopup;
            DatagridClass.SetMenuFont(popup.Items);
            popup.Font = SystemClass.SetFontGernaral;
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            BaseFilterPopup popup = (BaseFilterPopup)e.FilterPopup;
            DatagridClass.SetMenuFont(popup.Items);
            popup.Font = SystemClass.SetFontGernaral;
        }

        private void RadGridView_Bill_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            if (e.FilterPopup is RadListFilterPopup popup)
            {
                popup.MenuTreeElement.TreeView.NodeFormatting -= DatagridClass.TreeView_NodeFormatting;
                popup.MenuTreeElement.TreeView.NodeFormatting += DatagridClass.TreeView_NodeFormatting;
            }
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            if (e.FilterPopup is RadListFilterPopup popup)
            {
                popup.MenuTreeElement.TreeView.NodeFormatting -= DatagridClass.TreeView_NodeFormatting;
                popup.MenuTreeElement.TreeView.NodeFormatting += DatagridClass.TreeView_NodeFormatting;
            }
        }
        #endregion
        //setPermission
        void SetPermission()
        {
            radButton_Add.Enabled = false;
            radButton_CancleBill.Enabled = false;
            radButton_Money.Enabled = false;
            radButton_Save.Enabled = false;
        }
        void ClearData()
        {
            pType = "";
            radLabel_WH.Text = "";
            radTextBox_BillID.Text = "";
            radLabel_BillGroup.Text = "";
            radLabel_BranchName.Text = "";
            radLabel_Create.Text = "";
            radLabel_D003.Text = "";
            radLabel_D062.Text = "";
            radLabel_Date.Text = "";
            radLabel_MN.Text = "";
            radLabel_Money.Text = "";
            radLabel_Remark.Text = "";
            radLabel_BillDptCreate.Text = "";
            radLabel_IS.Text = "";
            JobNumberID = "";
            pGrpID = "";
            pGrpDesc = "";

            radToggleSwitch_StaBillCancle.Toggle(false);
            radToggleSwitch_StaOld.Toggle(false);

            radButton_CancleBill.Enabled = false;
            radButton_Job.Enabled = false;
            radButton_Money.Enabled = false;
            radButton_Save.Enabled = false;

            if (Dt.Rows.Count > 0) Dt.Rows.Clear(); Dt.AcceptChanges();
            if (radGridView_Show.Rows.Count > 0) radGridView_Show.Rows.Clear();

            radTextBox_BillID.Enabled = true;
            radTextBox_BillID.Focus();
        }
        //Close
        private void RadButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //กดเพิ่มบิล เพื่อใส่เลขที่บิลใหม่
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        public BillOut_Detail(string pBillID)
        {
            InitializeComponent();
            _pBillID = pBillID;
        }
        //load
        private void BillOut_Detail_Load(object sender, EventArgs e)
        {
            ClearData();

            radButton_Add.ButtonElement.ShowBorder = true;
            radButton_Job.ButtonElement.ShowBorder = true;
            radButton_Close.ButtonElement.ShowBorder = true;
            radButton_Money.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_CancleBill.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            DatagridClass.SetDefaultRadGridView(radGridView_Bill);

            //Bill Detail
            radGridView_Bill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEM", "บาร์โค้ด", 140));
            radGridView_Bill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 260));
            radGridView_Bill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY", "จำนวน", 90));
            radGridView_Bill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 90));
            radGridView_Bill.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COSTAMOUNT", "ราคารวม", 120));
            radGridView_Bill.EnableFiltering = false;
            radGridView_Bill.Columns["QTY"].FormatString = "{0:#,##0.00}";
            radGridView_Bill.Columns["COSTAMOUNT"].FormatString = "{0:#,##0.00}";

            JOB_Class.SetHeadGrid_ForShowImage(radGridView_Show, "1");

            if (_pBillID == "")
            {
                sSendDialog = "1";
                ClearData();
                radTextBox_BillID.Focus();
            }
            else
            {
                sSendDialog = "0";
                radTextBox_BillID.Text = _pBillID;
                SetDataInFrom(_pBillID);
                radTextBox_BillID.Enabled = false;
                radButton_Add.Enabled = false;
            }
        }

        //Set Data In Form and findBill
        void SetDataInFrom(string pBill)
        {
            pBill = pBill.ToUpper();
            //Dt = BillAllClass.FindAllBill_ByBillID(pBill);
            Dt = BillOut_Class.FindAllBill_ByBillID(pBill);
            if (Dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่บิล");
                if (_pBillID == "")
                {
                    radTextBox_BillID.SelectAll();
                    radTextBox_BillID.Focus();
                    return;
                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
            }
            if (Dt.Rows[0]["BRANCH_ID"].ToString().Substring(0, 1) == "D") pType = "SUPC"; else pType = "SHOP";

            radLabel_BranchName.Text = Dt.Rows[0]["BRANCH_NAME"].ToString();
            radLabel_Date.Text = Dt.Rows[0]["Bill_Date"].ToString();
            radLabel_Create.Text = Dt.Rows[0]["WHOINS"].ToString() + "-" + Dt.Rows[0]["WHONAMEINS"].ToString();
            radLabel_BillGroup.Text = Dt.Rows[0]["TypeBill"].ToString();
            radLabel_IS.Text = Dt.Rows[0]["INVENTCOSTCENTERID"].ToString();
            pGrpID = Dt.Rows[0]["DptIDCreate"].ToString();
            pGrpDesc = Dt.Rows[0]["DptNameCreate"].ToString();
            radLabel_BillDptCreate.Text = pGrpID + " " + pGrpDesc;
            radLabel_WH.Text = Dt.Rows[0]["WH"].ToString();

            switch (Dt.Rows[0]["ShipSta"].ToString())
            {
                case "0":
                    radLabel_Remark.Text = Dt.Rows[0]["RMK"].ToString() + Environment.NewLine +
                        ">> สาขายังไม่ส่งของ";
                    break;
                case "1":
                    radLabel_Remark.Text = Dt.Rows[0]["RMK"].ToString() + Environment.NewLine +
                        ">> ส่งของแล้ว-" + Dt.Rows[0]["ShipID"].ToString() + Environment.NewLine +
                        ">> ทะเบียนรถ-" + Dt.Rows[0]["ShipCarID"].ToString() + Environment.NewLine +
                        ">> พขร-" + Dt.Rows[0]["ShipDriverID"].ToString() + Environment.NewLine +
                        ">> เวลาส่ง-" + Dt.Rows[0]["ShipDate"].ToString();
                    break;
                case "2": radLabel_Remark.Text = Dt.Rows[0]["RMK"].ToString(); break;
                default:
                    break;
            }

            switch (Dt.Rows[0]["STA_D003"].ToString())
            {
                case "0":
                    FindD003(Dt.Rows[0]["Bill_ID"].ToString());
                    break;
                case "2":
                    if (Dt.Rows[0]["Bill_typeCNRecive"].ToString() == "1")
                    {
                        radLabel_D003.Text = "CN รับ " + Environment.NewLine +
                             Dt.Rows[0]["Bill_TypeCNWhoID"].ToString() + "-" + Dt.Rows[0]["Bill_TypeCNWhoName"].ToString() +
                             " [" + Dt.Rows[0]["Bill_TypeCNDate"].ToString() + " " + Dt.Rows[0]["Bill_TypeCNTime"].ToString() + "]";
                        radLabel_D003.ForeColor = Color.Blue;
                    }
                    else
                    {
                        radLabel_D003.Text = "CN ยังไม่ได้รับ ";
                        radLabel_D003.ForeColor = Color.Red;
                    }
                    break;
                default:
                    radLabel_D003.Text = "-";
                    radLabel_D003.ForeColor = Color.Blue;
                    break;
            }

            if (Dt.Rows[0]["STABCH"].ToString() == "1")
            {
                string strReciveDescMN = "";
                switch (Dt.Rows[0]["BCHRecive"].ToString())
                {
                    case "1":
                        strReciveDescMN = "รับครบ"; break;
                    case "2": strReciveDescMN = "ได้รับสินค้าไม่ครบ"; break;
                    case "3": strReciveDescMN = "นำสินค้ากลับ"; break;
                    default:
                        break;
                }
                radLabel_MN.Text = strReciveDescMN + " " + Dt.Rows[0]["BCHRmk"].ToString() + Environment.NewLine +
                    Dt.Rows[0]["BCHWhoID"].ToString() + "-" + Dt.Rows[0]["BCHWhoName"].ToString() +
                    " [" + Dt.Rows[0]["BCHDATETIME"].ToString() + "]";
                radLabel_MN.ForeColor = Color.Blue;

            }
            else
            {
                radLabel_MN.Text = "ยังไม่ได้บันทึกรับสินค้า";
                radLabel_MN.ForeColor = Color.Red;
            }

            if (Dt.Rows[0]["Bill_SpcType"].ToString() == "/")
            {
                radLabel_D062.Text = Dt.Rows[0]["Bill_SpcRemark"].ToString() + Environment.NewLine +
                    Dt.Rows[0]["Bill_SpcWhoIn"].ToString() + "-" + Dt.Rows[0]["Bill_SpcWhoName"].ToString() +
                    " [" + Dt.Rows[0]["Bill_SpcDateTime"].ToString() + "]";
                radLabel_D062.ForeColor = Color.Blue;
            }
            else
            {
                radLabel_D062.Text = "ยังไม่ได้บันทึกตรวจบิล";
                radLabel_D062.ForeColor = Color.Red;

                radButton_CancleBill.Enabled = true;
                radButton_Money.Enabled = true;
                radButton_Save.Enabled = true;

            }

            if (Dt.Rows[0]["Bill_SpcMoneyStatus"].ToString() == "1")
            {
                radLabel_Money.Text = Dt.Rows[0]["Bill_SpcMoneyID"].ToString() + "-" + Dt.Rows[0]["Bill_SpcMoneyName"].ToString() +
                    " [" + Dt.Rows[0]["Bill_SpcMoneySum"].ToString() + " B.]";
                radLabel_Money.ForeColor = Color.Red;
            }
            else
            {
                radLabel_Money.Text = "ไม่มีรายการหัก";
                radLabel_Money.ForeColor = Color.Blue;
            }

            if (Dt.Rows[0]["STADOC"].ToString() == "0")
            {
                radToggleSwitch_StaBillCancle.SetToggleState(true);

                radButton_CancleBill.Enabled = false;
                radButton_Money.Enabled = false;
                radButton_Save.Enabled = false;
            }
            else
            {
                radToggleSwitch_StaBillCancle.SetToggleState(false);
            }

            if (Dt.Rows[0]["STA_RETURN"].ToString() == "1") radToggleSwitch_StaOld.SetToggleState(true);
            else radToggleSwitch_StaOld.SetToggleState(false);


            if (Dt.Rows[0]["JOBID"].ToString() != "")
            {
                JobNumberID = Dt.Rows[0]["JOBID"].ToString();
                radButton_Job.Enabled = true;
            }
            else
            {
                radButton_Job.Enabled = false;
            }

            radGridView_Bill.DataSource = Dt;

            //FindImageByBill(Dt);
            JOB_Class.FindImageBillAll_ByJobID(Dt, radGridView_Show);

            if (SystemClass.SystemComProgrammer != "1")
            {
                if (SystemClass.SystemDptID != "D062") SetPermission();
            }
            radTextBox_BillID.Enabled = false;
            radTextBox_BillID.Text = radTextBox_BillID.Text.ToUpper();
        }

        void FindD003(string pID)
        {
            DataTable dt;// = new DataTable();
            dt = BillOutClass.FindBill_ByD003(pID);

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["STATUSCOUNTITEM"].ToString() == "F")
                {
                    radLabel_D003.Text = "ระบุ : ของไม่ครบ" + Environment.NewLine +
                        dt.Rows[0]["USERCREATE"].ToString() + "-" + dt.Rows[0]["SPC_NAME"].ToString() +
                        " [" + dt.Rows[0]["DATEOUT"].ToString() + "]";
                    radLabel_D003.ForeColor = Color.Red;
                }
                else
                {
                    radLabel_D003.Text = "ระบุ : ของครบ" + Environment.NewLine +
                        dt.Rows[0]["USERCREATE"].ToString() + "-" + dt.Rows[0]["SPC_NAME"].ToString() +
                        " [" + dt.Rows[0]["DATEOUT"].ToString() + "]";
                    radLabel_D003.ForeColor = Color.Blue;
                }
            }
            else
            {
                radLabel_D003.Text = "ยังไม่ได้ตรวจบิลออก";
                radLabel_D003.ForeColor = Color.Red;
            }

        }
        //Check Image         
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_SPC": //รูปส่งของให้สาขา
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                                radGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN": //รูปสาขารับของ
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END": //รูปสาขาติดตั้ง
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD": //รูปสาขาส่งซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLD_Count"].Value.ToString(),
                             radGridView_Show.CurrentRow.Cells["Bill_OLD_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC": //รูปสาขาใหญ่รับซาก
                    ImageClass.OpenShowImage(radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                            radGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //บิลยกเลิก
        private void RadButton_CancleBill_Click(object sender, EventArgs e)
        {
            ShowRemark frm = new ShowRemark("1")
            { pDesc = "เหตุผลในการยกเลิกบิล" };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                SaveData(radTextBox_BillID.Text, "0", frm.pRmk, "0", "", "", "0");
            }
            else
            {
                //MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("หมายเหตุ");
                return;
            }
        }
        //บันทึกเรียบร้อย
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            ShowRemark frm = new ShowRemark("0")
            { pDesc = "หมายเหตุในการบันทึกบิลเรียบร้อย" };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                SaveData(radTextBox_BillID.Text, "1", frm.pRmk, "0", "", "", "0");
            }
            else
            {
                //MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("หมายเหตุ");
                return;
            }
        }
        //Save 
        void SaveData(string pBillID, string pSta, string pRmk
            , string pStaMoney, string pEmpIDMoney, string pEmpNameMoney,
            string pMoney)//, string pEmpIDMoneyDptID, string pEmpIDMoneyDptName
        {
            //set เลขที่บิลและตาราง
            string tblName = ""; string fieldName = ""; string fieldName_StaDoc = "";
            switch (radLabel_BillGroup.Text.ToUpper())
            {
                case "MNIO":
                    tblName = "SHOP_MNIO_HD"; fieldName = "DOCNO"; fieldName_StaDoc = "STADOC"; break;
                case "MNOT":
                    tblName = "SHOP_MNOT_HD"; fieldName = "DOCNO"; fieldName_StaDoc = "STADOC"; break;
                case "MNRS":
                    tblName = "SHOP_RO_MNRSHD"; fieldName = "DOCNO"; fieldName_StaDoc = "STADOC"; break;
                case "I":
                    tblName = "SHOP_BillOut"; fieldName = "Bill_ID"; fieldName_StaDoc = "Bill_StaDoc"; break;
                case "F":
                    tblName = "SHOP_BillOut"; fieldName = "Bill_ID"; fieldName_StaDoc = "Bill_StaDoc"; break;
                case "FAL":
                    tblName = "SHOP_BillOut"; fieldName = "Bill_ID"; fieldName_StaDoc = "Bill_StaDoc"; break;
                case "MNRZ":
                    tblName = "SHOP_MNRZ_HD"; fieldName = "DOCNO"; fieldName_StaDoc = "STADOC"; break;
                case "MNRB":
                    tblName = "SHOP_MNRB"; fieldName = "DOCNO"; fieldName_StaDoc = "StaDoc";
                    if (pSta == "0") { pSta = "3"; }
                    break;
                case "MNRG":
                    tblName = "SHOP_MNRG"; fieldName = "DOCNO"; fieldName_StaDoc = "StaDoc";
                    if (pSta == "0") { pSta = "3"; }
                    break;
                case "MNRH":
                    tblName = "SHOP_MNRH_HD"; fieldName = "DOCNO"; fieldName_StaDoc = "StaDoc";
                    if (pSta == "0") { pSta = "3"; }
                    break;
                case "MNRR":
                    tblName = " Shop_MNRR_HD"; fieldName = "MNRRDocNo"; fieldName_StaDoc = "MNRRStaDoc";
                    if (pSta == "0") { pSta = "3"; }
                    break;
                case "MNPZ":
                    tblName = "SHOP_REWARDTABLE"; fieldName = "MNREWARDID"; fieldName_StaDoc = "StaDoc";
                    if (pSta == "0") { pSta = "3"; }
                    break;
                case "IDM":
                    tblName = "SHOP_BillOut"; fieldName = "Bill_ID"; fieldName_StaDoc = "Bill_StaDoc";
                    break;
                case "MNCM":
                    tblName = "SHOP_JOBCLAIM"; fieldName = "Claim_ID"; fieldName_StaDoc = "STADOC";
                    break;
                case "MNRM":
                    tblName = "SHOP_MNRM"; fieldName = "DOCNO"; fieldName_StaDoc = "STADOC"; break;
                default:
                    break;
            }

            string upStr; string T;
            upStr = string.Format(@"UPDATE " + tblName + " " +
                " SET Bill_SpcType = '1',Bill_SpcMoneyStatus = '" + pStaMoney + "'," +
                "Bill_SpcMoneySum = '" + float.Parse(pMoney) + "',Bill_SpcMoneyID = '" + pEmpIDMoney + "'," +
                "Bill_SpcMoneyName = '" + pEmpNameMoney + "'," +
                "Bill_SpcRemark = '" + pRmk + "'," +
                "Bill_SpcWhoIn = '" + SystemClass.SystemUserID + "',Bill_SpcWhoName = '" + SystemClass.SystemUserName + "'," +
                "Bill_SpcDate = CONVERT(VARCHAR,GETDATE(),25),  " +
                "" + fieldName_StaDoc + " = '" + pSta + "' " +
                "WHERE " + fieldName + " = '" + pBillID + "'  " +
                "");
 
            T = ConnectionClass.ExecuteSQL_Main(upStr);
 
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                if (sSendDialog == "1")
                {
                    ClearData();
                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }
        private void RadButton_Money_Click(object sender, EventArgs e)
        {
            FormShare.EmpListInCorrect frm = new EmpListInCorrect(radTextBox_BillID.Text, radLabel_BillGroup.Text, "D062", "");
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                SaveData(radTextBox_BillID.Text, "1", frm.pRmk, "1", frm.pEmplID, frm.pEmplName, frm.pMoney);
            }
            else
            {
                return;
            }
        }
        //Open JOB
        private void RadButton_Job_Click(object sender, EventArgs e)
        {
            if (JobNumberID != "")
            {
                switch (JobNumberID.Substring(0, 4))
                {
                    case "MJOB"://com minimart
                        JOB.Com.JOBCOM_EDIT frmComMN = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComMinimart", "00001", "ComMinimart", "0", pType,
                            JobNumberID);
                        if (frmComMN.ShowDialog(this) == DialogResult.OK) { }
                        break;
                    case "SJOB"://comservice
                        JOB.Com.JOBCOM_EDIT frmComService = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComService", "00002", "ComService", "0", pType,
                           JobNumberID);
                        if (frmComService.ShowDialog(this) == DialogResult.OK) { }
                        break;
                    case "DJOB"://ช่างทั่วไป
                        JOB.General.JOBGeneral_EDIT frmMaintenance = new JOB.General.JOBGeneral_EDIT("SHOP_JOBMaintenance", "00003", "งานซ่อมทั่วไป", "0", pType,
                             JobNumberID);
                        if (frmMaintenance.ShowDialog(this) == DialogResult.OK) { }
                        break;
                    case "HJOB":// Center
                        DataTable dt = JOB_Class.GetJOBGROUP_ByJOBID(JobNumberID);
                        JOB.Center.JOBCenter_EDIT frmCenter = new JOB.Center.JOBCenter_EDIT("SHOP_JOBCenter",
                            dt.Rows[0]["JOBGROUP_ID"].ToString(), dt.Rows[0]["JOBGROUP_DESCRIPTION"].ToString(), JobNumberID, "0");
                        if (frmCenter.ShowDialog(this) == DialogResult.OK) { }
                        break;
                    case "CJOB"://ซ่อมรถ
                        JOB.Car.JOBCar_EDIT frmCar = new JOB.Car.JOBCar_EDIT("Shop_JOBCarMachine", "00007", "งานซ่อมรถ-เครื่องจักร", JobNumberID, "0", pType);
                        if (frmCar.ShowDialog(this) == DialogResult.OK) { }
                        break;
                    default: break;
                }
            }
        }
        //SetFont
        private void RadGridView_Show_ViewCellFormatting_1(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
        }
        //Enter
        private void RadTextBox_BillID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetDataInFrom(radTextBox_BillID.Text);
        }
    }

}
