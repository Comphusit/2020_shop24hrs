﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using Telerik.WinControls.Data;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class MNRO_LoadOrder : Telerik.WinControls.UI.RadForm
    {
        //string printBchId, printBchName;
        //readonly PrintController printController = new StandardPrintController();
        //readonly DataTable dtBchPrint = new DataTable();
        private DataTable dtBch = new DataTable();
        readonly private DataTable dt = new DataTable();
        readonly string _pCase;
        double QtyCell;
        private int iRowDT = 0;
        //readonly string _Dept;
        //readonly string _DateReceive;
        string BranchId;
        string BranchName;
        string MNRSDocNo;
        private class Qty
        {
            public double QtyBeforeEdit { get; set; }
            public double QtyEndEdit { get; set; }
        }
        private class QtyUpdate
        {
            public string Branch { get; set; }
            public double Qty { get; set; }
        }
        //Load
        public MNRO_LoadOrder(string pCase = "")
        {
            InitializeComponent();
            _pCase = pCase;
        }
        //Load Main
        private void MNRO_LoadOrder_Load(object sender, EventArgs e)
        {
            radStatusStrip_Menu.SizingGrip = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-1), DateTime.Now);

            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            radButtonElement_pdf.ShowBorder = true; radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน";
            radButtonElementSaveMNRS.ShowBorder = true; radButtonElementSaveMNRS.ToolTipText = "บันทึกบิล MNRS.";

            RadButton_Search.ButtonElement.ShowBorder = true;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_Branch.Checked = false;
            RadCheckBox_GrpMain.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_GrpMain.Checked = false;
            RadCheckBox_GrpMain.Enabled = true;
            radDropDownList_MainGroup.Enabled = false;
            if (_pCase != "D062") { RadCheckBox_GrpMain.Checked = true; RadCheckBox_GrpMain.Enabled = false; }

            RadCheckBox_GrpSub.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_GrpSub.Checked = false;

            radDropDownList_SubGroup.Enabled = false;

            dtBch = BranchClass.GetBranchAll("'1'", "'1'");
            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.Enabled = false;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_SubGroup);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);


            //dtBchPrint.Columns.Add("BchID");
            //dtBchPrint.Columns.Add("BchName");

            if (_pCase == "D062") { RadGridView_Show.ReadOnly = false; RadGridView_Show.MasterTemplate.AllowAddNewRow = false; }
            SetGroupMain();
        }
        void SetGroupMain()
        {
            //string sql = string.Format(@"SELECT	[GROUPRO_ID],[GROUPRO_NAME],[GROUPRO_STA],[GROUPRO_REMARK]
            //    FROM	[SHOP_RO_GROUPMAIN] WITH (NOLOCK)
            //    ORDER BY [GROUPRO_ID]");
            radDropDownList_MainGroup.DataSource = I_Class.Find_GroupMain("1"); //ConnectionClass.SelectSQL_Main(sql);
            radDropDownList_MainGroup.DisplayMember = "GROUPRO_NAME";
            radDropDownList_MainGroup.ValueMember = "GROUPRO_ID";
        }
        //แสดงกลุ่มย่อย
        void SetGroupSub(string _pGrpmain)
        {
            //String sql = String.Format(@"SELECT GROUPRO_SUBID,GROUPRO_SUBNAME,GROUPRO_SUBSTA,GROUPRO_SUBREMARK 
            //     FROM SHOP_RO_GROUPSUB WITH (NOLOCK) 
            //     WHERE GROUPRO_ID = '" + _pGrpmain + @"'  ");

            radDropDownList_SubGroup.DataSource = I_Class.Find_GroupSub(_pGrpmain, "1");// ConnectionClass.SelectSQL_Main(sql);
            radDropDownList_SubGroup.DisplayMember = "GROUPRO_SUBNAME";
            radDropDownList_SubGroup.ValueMember = "GROUPRO_SUBID";
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //checkChange
        private void RadCheckBox_GrpSub_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpSub.Checked == true)
            {
                SetGroupSub(radDropDownList_MainGroup.SelectedValue.ToString());
                radDropDownList_SubGroup.Enabled = true;
            }
            else radDropDownList_SubGroup.Enabled = false;
        }
        //เลือกสาขา
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        //เลือกงบหลัก
        private void RadCheckBox_GrpMain_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpMain.Checked == true) radDropDownList_MainGroup.Enabled = true; else radDropDownList_MainGroup.Enabled = false;
        }

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //LoadData
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dtColume = dtBch;
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

            if (RadCheckBox_Branch.Checked == true) dtColume = BranchClass.GetDetailBranchByID(RadDropDownList_Branch.SelectedValue.ToString());

            if (RadGridView_Show.Columns.Count > 0)
            {
                RadGridView_Show.Columns.Clear(); RadGridView_Show.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_Show.SummaryRowsTop.Clear();
            }
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN", "งบเบิก", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SUM", "รวม", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns[$@"GROUPMAIN"].ReadOnly = true;
            RadGridView_Show.MasterTemplate.Columns[$@"GROUPMAIN"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns[$@"ITEMBARCODE"].ReadOnly = true;
            RadGridView_Show.MasterTemplate.Columns[$@"ITEMBARCODE"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns[$@"SPC_ITEMNAME"].ReadOnly = true;
            RadGridView_Show.MasterTemplate.Columns[$@"SPC_ITEMNAME"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns[$@"SUM"].ReadOnly = true;
            RadGridView_Show.MasterTemplate.Columns[$@"SUM"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns[$@"UNITID"].ReadOnly = true;

            //RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            //RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

            for (int ii = 0; ii < dtColume.Rows.Count; ii++)
            {
                RadGridView_Show.MasterTemplate.Columns.Add
                    (DatagridClass.AddTextBoxColumn_AddManualSetRight(dtColume.Rows[ii]["BRANCH_ID"].ToString(), dtColume.Rows[ii]["BRANCH_NAME"].ToString(), 100));

                RadGridView_Show.MasterTemplate.Columns[dtColume.Rows[ii]["BRANCH_ID"].ToString()].FormatString = "{0:#,##0.00}";

                DatagridClass.SetCellBackClolorByExpression(dtColume.Rows[ii]["BRANCH_ID"].ToString(), $@" {dtColume.Rows[ii]["BRANCH_ID"]} <> '0.00'", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
                GridViewSummaryItem summaryItem = new GridViewSummaryItem(dtColume.Rows[ii]["BRANCH_ID"].ToString(), "รวม = {0}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem);
            }



            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);

            this.RadGridView_Show.EnableCustomFiltering = true;
            this.RadGridView_Show.CustomFiltering += new GridViewCustomFilteringEventHandler(RadGridView_Show_CustomFiltering);
            FilterDescriptor descriptor = new FilterDescriptor("SUM", FilterOperator.IsGreaterThan, 0);
            this.RadGridView_Show.FilterDescriptors.Add(descriptor);

            string grpSub = "";
            if (RadCheckBox_GrpSub.Checked == true) grpSub = radDropDownList_SubGroup.SelectedValue.ToString();

            DataTable dtRow = new DataTable();
            if (RadCheckBox_GrpMain.Checked == true) dtRow = BillOutClass.SHOP_RO_ItemForOrder(radDropDownList_MainGroup.SelectedValue.ToString(), grpSub);
            else dtRow = BillOutClass.SHOP_RO_ItemForOrder(radDropDownList_MainGroup.SelectedValue.ToString(), grpSub, "D062", radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));

            DataTable dtData = I_Class.SHOP_RO_ItemOrder(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));

            for (int iR = 0; iR < dtRow.Rows.Count; iR++)
            {
                RadGridView_Show.Rows.Add(dtRow.Rows[iR]["GROUPMAIN"].ToString(), dtRow.Rows[iR]["ITEMBARCODE"].ToString(),
                    dtRow.Rows[iR]["SPC_ITEMNAME"].ToString(), "0.00", dtRow.Rows[iR]["UNITID"].ToString());
                double sumRow = 0;
                for (int i = 0; i < dtColume.Rows.Count; i++)
                {
                    DataRow[] dr = dtData.Select("BRANCH_ID = '" + dtColume.Rows[i]["BRANCH_ID"].ToString() + @"' AND ITEMBARCODE = '" + dtRow.Rows[iR]["ITEMBARCODE"].ToString() + @"' ");
                    if (dr.Length > 0)
                    {
                        RadGridView_Show.Rows[iR].Cells[dtColume.Rows[i]["BRANCH_ID"].ToString()].Value = Convert.ToDouble(dr[0]["QTY"].ToString()).ToString("N2");
                        sumRow += Convert.ToDouble(dr[0]["QTY"].ToString());
                    }
                    else
                    {
                        RadGridView_Show.Rows[iR].Cells[dtColume.Rows[i]["BRANCH_ID"].ToString()].Value = "0.00";
                    }

                    RadGridView_Show.Rows[iR].Cells["SUM"].Value = sumRow.ToString("N2");


                }
            }

            for (int kk = 5; kk < RadGridView_Show.Columns.Count; kk++)
            {
                double qty = 0;
                for (int qq = 0; qq < RadGridView_Show.Rows.Count; qq++)
                {
                    qty += Convert.ToDouble(RadGridView_Show.Rows[qq].Cells[kk].Value);
                }
                if (qty == 0) RadGridView_Show.Columns[kk].IsVisible = false;
            }
            this.Cursor = Cursors.Default;
        }
        //DataTable ALoadRow()
        //{
        //    string sql = string.Format(@"SELECT	INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,ZONE ,UNITID
        //        FROM	SHOP_RO_ITEM WITH (NOLOCK) INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)
        //                          ON SHOP_RO_ITEM.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
        //        WHERE GROUPMAIN = '" + radDropDownList_MainGroup.SelectedValue.ToString() + @"'");
        //    if (RadCheckBox_GrpSub.Checked == true)
        //    {
        //        sql += " AND GROUPSUB = '" + radDropDownList_SubGroup.SelectedValue.ToString() + "' ";
        //    }
        //    sql += " ORDER BY SPC_ITEMNAME,GROUPMAIN,GROUPSUB ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //DataTable LoadData()
        //{
        //    string sql = string.Format(@"SELECT	BRANCH_ID,ITEMBARCODE,SUM(QTY) AS QTY  
        //             FROM	SHOP_RO_MNROHD WITH (NOLOCK) INNER JOIN SHOP_RO_MNRODT WITH (NOLOCK)   
        //                    ON SHOP_RO_MNRODT.DOCNo = SHOP_RO_MNROHD.DOCNo   
        //             WHERE	 STADOC = '1' AND STAAPV = '1' AND STA = '1'  
        //                    AND CONVERT(VARCHAR,DATE,23) = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"'   
        //             GROUP BY BRANCH_ID,ITEMBARCODE   
        //             ORDER BY BRANCH_ID,ITEMBARCODE ");
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //Remove Row Sum = 0
        private void RadGridView_Show_CustomFiltering(object sender, GridViewCustomFilteringEventArgs e)
        {
            try
            {
                e.Visible = Convert.ToDouble(e.Row.Cells["SUM"].Value.ToString()) > 0;
            }
            catch (Exception)
            {
                return;
            }

        }
        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_Show.Rows.Count == 0) return;

            if (_pCase == "D062")
            {
                try
                {
                    Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                    app.Visible = true;
                    worksheet = workbook.Sheets["Sheet1"];
                    worksheet = workbook.ActiveSheet;
                    worksheet.Name = "ออร์เดอร์";

                    int iColumns = 1; //Set Column
                    for (int iH = 1; iH < RadGridView_Show.Columns.Count; iH++) //set column : เฉพาะสาขาที่มีการสั่งสินค้า
                    {
                        if (RadGridView_Show.Columns[iH].IsVisible == true)
                        {
                            worksheet.Cells[1, iColumns].value = RadGridView_Show.Columns[iH].HeaderText.ToString();
                            worksheet.Columns[2].ColumnWidth = 47;
                            worksheet.Columns[1].ColumnWidth = 19;
                            if (iColumns > 4) worksheet.Columns[iColumns].ColumnWidth = 16;
                            iColumns++;
                        }
                    }

                    int iRows = 1; //Set Row
                    for (int iItem = 0; iItem < RadGridView_Show.Rows.Count; iItem++)
                    {
                        if (RadGridView_Show.Rows[iItem].Cells[3].Value.ToString() != "0.00")
                        {
                            int iColumnsR = 1;
                            for (int i = 1; i < RadGridView_Show.Columns.Count; i++) //set row : เฉพาะสินค้าที่มีรายการสั่ง
                            {
                                if (RadGridView_Show.Columns[i].IsVisible == true)
                                {
                                    worksheet.Cells[iRows + 1, 1].NumberFormat = "@";
                                    string vv = RadGridView_Show.Rows[iItem].Cells[i].Value.ToString();
                                    worksheet.Cells[iRows + 1, iColumnsR].value = vv;
                                    if (vv == "0.00") worksheet.Cells[iRows + 1, iColumnsR].value = "";
                                    iColumnsR++;
                                }
                            }
                            iRows++;
                        }
                    }

                }
                catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }
            }
            else
            {
                string T = DatagridClass.ExportExcelGridView("รายการจัดสินค้าเบิก " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "1");
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }


        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (_pCase == "D062")
            {
                if (RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString() == "")
                {
                    RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = "0.00"; return;
                } //ค่าที่ใส่ไม่เท่ากับ ""

                bool isNumerical = double.TryParse(RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString(), out _); //out double number
                if (isNumerical == false)
                {
                    RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = "0.00"; return;
                } //ค่าที่ใส่ไม่เท่ากับตัวเลข 

                string QtyInput = RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString();
                int QTYIn = Convert.ToInt32(double.Parse(RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value.ToString()));
                string Branch = RadGridView_Show.Columns[RadGridView_Show.CurrentCell.ColumnIndex].Name.ToString();

                if (double.TryParse(QtyInput, out _))
                {
                    //แก้ไข+-จำนวนด้วย
                    List<Qty> ListQty = new List<Qty>();
                    Qty qty = new Qty()
                    {
                        QtyBeforeEdit = QtyCell,
                        QtyEndEdit = double.Parse(QtyInput)
                    };
                    ListQty.Add(qty);

                    List<QtyUpdate> listUp = new List<QtyUpdate>();
                    QtyUpdate Tmp = new QtyUpdate()
                    {
                        Branch = Branch,
                        //Barcode = Barcode,
                        Qty = Convert.ToDouble(QtyInput)
                    };
                    listUp.Add(Tmp);

                    //แก้ไขการแสดงผลของยอดรวม
                    RadGridView_Show.CurrentRow.Cells[RadGridView_Show.CurrentCell.ColumnIndex].Value = QTYIn.ToString("N2");
                    RadGridView_Show.CurrentRow.Cells["SUM"].Value =
                        SumQtyItembarcodeAllBranch(RadGridView_Show.CurrentRow.Cells["SUM"].Value.ToString(), ListQty);
                }
            }

        }

        private string SumQtyItembarcodeAllBranch(string SumQty, List<Qty> Qty)
        {
            if (SumQty.Equals("")) SumQty = "0";
            Double Ans = double.Parse(SumQty) + (Qty[0].QtyEndEdit - Qty[0].QtyBeforeEdit);
            return Ans.ToString("N2");
        }

        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            string Qty = RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
            if (Qty.Equals(""))
            {
                QtyCell = 0;
                return;
            }
            QtyCell = double.Parse(Qty);
        }

        private void RadButtonElementSaveMNRS_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;

            //if (dtBchPrint.Rows.Count > 0) dtBchPrint.Rows.Clear();

            double priceNet, weight;
            this.Cursor = Cursors.WaitCursor;
            for (int iC = 5; iC < RadGridView_Show.Columns.Count; iC++)
            {
                if (RadGridView_Show.Columns[iC].IsVisible == true)
                {
                    BranchId = RadGridView_Show.Columns[iC].Name.ToString();
                    BranchName = RadGridView_Show.Columns[iC].HeaderText.ToString();
                    //dtBchPrint.Rows.Add(BranchId, BranchName);
                    for (int iR = 0; iR < RadGridView_Show.Rows.Count; iR++)
                    {
                        if (RadGridView_Show.Rows[iR].Cells[iC].Value.ToString() != "0.00")
                        {
                            DataTable dtBarcode = BillOutClass.Find_ItemRoByItembarcode(RadGridView_Show.Rows[iR].Cells[1].Value.ToString());

                            weight = Convert.ToDouble(RadGridView_Show.Rows[iR].Cells[iC].Value.ToString());
                            priceNet = weight * (Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString()));

                            if (iRowDT == 0) SaveHD(dtBarcode, priceNet, weight); //SaveHD
                            else SaveDT(dtBarcode, priceNet, weight);//SaveDT
                        }
                    }
                }
                iRowDT = 0;
            }
            
            ArrayList sql = new ArrayList();
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                if (Convert.ToDouble(RadGridView_Show.Rows[i].Cells["SUM"].Value.ToString()) > 0)
                {
                    DateTime now = DateTime.Now;
                    string date = now.ToString("yyyy-MM-dd");
                    sql.Add(BoxRecive.BOX_Class.Shop_RecheckReciveItem_Insert(date.Replace("-", ""),
                                                                              date,
                                                                              "MNRS",
                                                                              "MN000",
                                                                              RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                                                                              RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                                                                              RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString(),
                                                                              RadGridView_Show.Rows[i].Cells["SUM"].Value.ToString(),
                                                                              RadGridView_Show.Rows[i].Cells["SUM"].Value.ToString(),
                                                                              ""));
                }

            }
            string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            //printDocument1.PrintController = printController;
            //printDocument1.PrinterSettings = printDialog1.PrinterSettings;

            //for (int i = 0; i < dtBchPrint.Rows.Count; i++)
            //{
            //    printBchId = dtBchPrint.Rows[i]["BchID"].ToString();
            //    printBchName = dtBchPrint.Rows[i]["BchName"].ToString();
            //}

            this.Cursor = Cursors.Default;
        }

        //SAVE MNRS
        void SaveHD(DataTable dtBarcode, double priceNet, double weight)
        {
            MNRSDocNo = ConfigClass.GetMaxINVOICEID("MNRS", "-", "MNRS", "1");
            string staInStall = "0";
            //if (SystemClass.SystemComMinimart == "1") staInStall = "1";

            string T = ConnectionClass.ExecuteSQL2Str_Main(
                I_Class.SHOP_RO_MNRS_SaveHD(MNRSDocNo, BranchId, BranchName, "RETAILAREA", staInStall, "", "0"),
                I_Class.SHOP_RO_MNRS_SaveDT(1, MNRSDocNo, dtBarcode, priceNet, weight));

            iRowDT = 1;
            if (T != "") MsgBoxClass.MsgBoxShow_SaveStatus(T);

        }
        //Save DT
        void SaveDT(DataTable dtBarcode, double priceNet, double weight)
        {
            iRowDT += 1;

            string T = ConnectionClass.ExecuteSQL_Main(I_Class.SHOP_RO_MNRS_SaveDT(iRowDT, MNRSDocNo, dtBarcode, priceNet, weight));// ConnectionClass.ExecuteSQL_Main(sqlDT);
            if (T != "")
            {
                iRowDT -= 1;
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }

        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //PrintClass.Print_Bch(e, printBchId, printBchName);

            //int Y = 0;
            //Y += 20;

            //string branchId;
            //string branchName;

            //e.Graphics.DrawString(branchId, new Font(new FontFamily("Tahoma"), 50), Brushes.Black, 0, Y);
            //Y += 70;

            //Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            //StringFormat stringFormat = new StringFormat()
            //{
            //    Alignment = StringAlignment.Near,
            //    LineAlignment = StringAlignment.Near
            //};
            //e.Graphics.DrawString(branchName, new Font(new FontFamily("Tahoma"), 40), Brushes.Black, rect1, stringFormat);
        }
    }
}
