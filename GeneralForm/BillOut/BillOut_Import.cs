﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class BillOut_Import : Telerik.WinControls.UI.RadForm
    {
        private DataTable dt = new DataTable();
        //Load
        public BillOut_Import()
        {
            InitializeComponent();
        }
        //Load Main
        private void BillOut_Import_Load(object sender, EventArgs e)
        {
            radButton_Edit.ShowBorder = true; radButton_Edit.ToolTipText = "ดูรายละเอียดบิล";
            radButton_Add.ShowBorder = true; radButton_Add.ToolTipText = "Import บิลทั้งหมดทุกรายการที่เลือกไว้";
            radButtonElement_Refresh.ShowBorder = true; radButtonElement_Refresh.ToolTipText = "ดึงข้อมูลใหม่";

            radStatusStrip.SizingGrip = false;
            radButtonElement_OK.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Bill, DateTime.Now.AddDays(-2), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Import", "เลือก"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA_RETURN", "มีซาก"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("JournalId", "เลขที่บิล", 145));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_TransDate", "วันที่บิล", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_INVENTCOSTCENTERID", "งบเบิก", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BillTYPE", "หมวดบิล", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_INVENTLOCATIONIDFROM", "คลังเบิก", 90));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("IVZ_REMARKS", "หมายเหตุ", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "ผู้สร้าง", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อผู้สร้าง", 200));//
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_DEPARTMENT_ID", "สร้าง", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_DEPARTMENT_NAME", "แผนกที่สร้าง", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "รูปรับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_INSTALL", "รูปติดตั้ง"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("Bill24", "Import", 90));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_Picked", "อนุมัติ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ComMN", "ComMN"));//
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DPT_ID", "DPT_ID"));

            RadGridView_Show.Columns["Import"].IsPinned = true;
            RadGridView_Show.Columns["STA_RETURN"].IsPinned = true;
            RadGridView_Show.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_Show.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_Show.Columns["JournalId"].IsPinned = true;

            DatagridClass.SetCellBackClolorByExpression("JournalId", "Bill24 = '0' ", ConfigClass.SetColor_Red(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("JournalId", "SPC_Picked = '0' ", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("JournalId", "ComMN <> '0' AND Bill24 = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("JournalId", "ComMN = '0' AND Bill24 = '0' AND DPT_ID = 'D179' ", ConfigClass.SetColor_PinkPastel(), RadGridView_Show);

        }
        //Set Valus
        void SetDGV()
        {
            if (dt.Rows.Count > 0) dt.Rows.Clear();

            this.Cursor = Cursors.WaitCursor;
            dt = BillOutClass.FindAllBillAX_ForImport(radDateTimePicker_Bill.Value.ToString("yyyy-MM-dd"), radDateTimePicker1.Value.ToString("yyyy-MM-dd"));
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //BillDetail
        void BillDetail()
        {
            if (RadGridView_Show.CurrentCell.ColumnInfo.FieldName == "STA_RETURN") return;
            if (RadGridView_Show.CurrentCell.ColumnInfo.FieldName == "Import") return;
            if ((RadGridView_Show.CurrentRow.Cells["JournalId"].Value.ToString() == "") || (RadGridView_Show.CurrentRow.Index == -1)) return;

            if (RadGridView_Show.CurrentRow.Cells["Bill24"].Value.ToString() != "0") // Import แล้ว
            {
                BillOut_Detail frm = new BillOut_Detail(RadGridView_Show.CurrentRow.Cells["JournalId"].Value.ToString());
                if (frm.ShowDialog(this) == DialogResult.OK)
                {
                }
            }
            else
            {
                JOB.Bill.BillAXAddJOB frm = new JOB.Bill.BillAXAddJOB
                (RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString(), "0", "", "", "", "", "",
                RadGridView_Show.CurrentRow.Cells["JournalId"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["ComMN"].Value.ToString());
                frm.ShowDialog(this);
                if (frm.sSendDialog == "1")
                {
                    RadGridView_Show.CurrentRow.Cells["Bill24"].Value = "1";
                    RadGridView_Show.CurrentRow.Cells["Import"].Value = "0";
                }
            }
        }
        //Edit
        private void RadButton_Edit_Click(object sender, EventArgs e)
        {
            BillDetail();
        }
        //refresh
        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Import BillALL
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("บิลทั้งหมดที่เลือกไว้") == DialogResult.No) return;

            ArrayList sql = new ArrayList();
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                if (RadGridView_Show.Rows[i].Cells["Import"].Value.ToString() == "1")
                {
                    string i_Bill_ID = RadGridView_Show.Rows[i].Cells["JournalId"].Value.ToString();
                    string i_Bill_INVENTCOSTCENTERID = RadGridView_Show.Rows[i].Cells["SPC_INVENTCOSTCENTERID"].Value.ToString();
                    string i_BRANCH_ID = RadGridView_Show.Rows[i].Cells["BRANCH_ID"].Value.ToString();
                    string i_BRANCH_NAME = RadGridView_Show.Rows[i].Cells["BRANCH_NAME"].Value.ToString();
                    string i_WH = RadGridView_Show.Rows[i].Cells["SPC_INVENTLOCATIONIDFROM"].Value.ToString();
                    string i_REMARK = RadGridView_Show.Rows[i].Cells["IVZ_REMARKS"].Value.ToString();
                    string i_SPC_DEPARTMENT_ID = RadGridView_Show.Rows[i].Cells["SPC_DEPARTMENT_ID"].Value.ToString();
                    string i_SPC_DEPARTMENT_NAME = RadGridView_Show.Rows[i].Cells["SPC_DEPARTMENT_NAME"].Value.ToString();
                    string i_EMPLID = RadGridView_Show.Rows[i].Cells["EMPLID"].Value.ToString();
                    string i_SPC_NAME = RadGridView_Show.Rows[i].Cells["SPC_NAME"].Value.ToString();
                    string i_BillTYPE = RadGridView_Show.Rows[i].Cells["BillTYPE"].Value.ToString();
                    string i_SPC_TransDate = RadGridView_Show.Rows[i].Cells["SPC_TransDate"].Value.ToString();

                    string i_STA_RECIVE = RadGridView_Show.Rows[i].Cells["STA_RECIVE"].Value.ToString();
                    string i_STA_INSTALL = RadGridView_Show.Rows[i].Cells["STA_INSTALL"].Value.ToString();
                    string i_STA_RETURN = RadGridView_Show.Rows[i].Cells["STA_RETURN"].Value.ToString();

                    string i_STA_RETURNRECIVE = i_STA_RETURN;
                    string i_Bill_TypeBchStatus = "0";
                    if (i_BRANCH_ID.StartsWith("D"))
                    {
                        i_Bill_TypeBchStatus = "1";
                        if (i_STA_RETURN == "1")
                        {
                            i_STA_RETURNRECIVE = "1"; i_STA_RETURN = "0";
                        }
                    }

                    sql.Add($@"
                    INSERT INTO [SHOP_BillOut] 
                      ( [Bill_ID],[Bill_INVENTCOSTCENTERID]
                      ,[WH],[BRANCH_ID],[BRANCH_NAME]
                      ,[WHOOPENBILL_ID],[WHOOPENBILL_NAME]
                      ,[REMARK]
                      ,[WHOINS],[WHONAMEINS],[TYPEBILL],[JOBGROUP_ID],[JOBGROUP_DESCRIPTION],DATE
                      ,[STA_RECIVE],[STA_INSTALL],[STA_RETURN],[STA_RETURNRECIVE],Bill_TypeBchStatus
                      ) VALUES 
                      ('{i_Bill_ID}','{i_Bill_INVENTCOSTCENTERID}',
                      '{i_WH}','{i_BRANCH_ID}','{i_BRANCH_NAME}',
                      '{i_EMPLID}','{i_SPC_NAME}','{i_REMARK}',
                      '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}',
                      '{i_BillTYPE}','{i_SPC_DEPARTMENT_ID}','{i_SPC_DEPARTMENT_NAME}','{i_SPC_TransDate}',
                      '{i_STA_RECIVE}','{i_STA_INSTALL}','{i_STA_RETURN}','{i_STA_RETURNRECIVE}','{i_Bill_TypeBchStatus}'  ) ");
                }
            }

            if (sql.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ไม่มีข้อมูลที่เลือกในการ Import");
                return;
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "") SetDGV();

        }
        //Double Click
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (e.Column.Name)
            {
                case "STA_RETURN":

                    if (CheckBill() == "0") return;

                    if (RadGridView_Show.CurrentRow.Cells["STA_RETURN"].Value.ToString() == "1") RadGridView_Show.CurrentRow.Cells["STA_RETURN"].Value = "0";
                    else RadGridView_Show.CurrentRow.Cells["STA_RETURN"].Value = "1";

                    break;
                case "Import":
                    if (CheckBill() == "0") return;

                    if (RadGridView_Show.CurrentRow.Cells["Import"].Value.ToString() == "1") RadGridView_Show.CurrentRow.Cells["Import"].Value = "0";
                    else RadGridView_Show.CurrentRow.Cells["Import"].Value = "1";

                    break;
                default:
                    break;
            }
        }
        //CheckBill
        string CheckBill()
        {
            if (RadGridView_Show.CurrentRow.Cells["Bill24"].Value.ToString() != "0") return "0";// Import แล้ว
            if (RadGridView_Show.CurrentRow.Cells["BillTYPE"].Value.ToString() == "IDM") return "0";//บิลทำทิ้ง
            if (RadGridView_Show.CurrentRow.Cells["ComMN"].Value.ToString() != "0") return "0";//Com
            if (RadGridView_Show.CurrentRow.Cells["SPC_Picked"].Value.ToString() == "0") return "0";//อนุมัติ
            if (RadGridView_Show.CurrentRow.Cells["DPT_ID"].Value.ToString() == "D179") return "0";//ComService

            return "1";
        }
        private void RadGridView_Show_DoubleClick(object sender, EventArgs e)
        {
            BillDetail();
        }

        private void RadButtonElement_OK_Click(object sender, EventArgs e)
        {
            SetDGV();
        }


    }
}
