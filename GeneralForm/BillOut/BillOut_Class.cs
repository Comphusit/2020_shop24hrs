﻿//CheckOK
using System;
using System.Data;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.IO;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    class BillOut_Class
    {
        //ค้นหาบิลทั้งหมด
        public static DataTable FindAllBill_ByBillID(string pBillID)
        {
            string pBillCase = ""; // เช็คเลขที่เอกสารก่อนว่าควรจะหาที่ตารางไหน
            if (pBillID.Length >= 4)
            {
                pBillCase = pBillID.Substring(0, 4);
                if (pBillCase.Substring(0, 1) == "I") { pBillCase = "I"; }
                else if (pBillCase.Substring(0, 3) == "FAL") { pBillCase = "FAL"; }
                else if (pBillCase.Substring(0, 1) == "F") { pBillCase = "F"; }
                else if (pBillCase.Substring(0, 2) == "GS") { pBillCase = "I"; }
                else if (pBillCase.Substring(0, 4) == "MNOM") { pBillCase = "I"; }
            }
            string sql = "";
            switch (pBillCase)
            {
                case "MNIO"://บิลนำของออก
                    sql = $@" 
                    SELECT	    [SHOP_MNIO_HD].DOCNO AS Bill_ID,CONVERT(VARCHAR,DATE,23) AS Bill_Date,
                                REMARK AS  RMK,
                                CASE WHEN JOBGROUP_ID = '00003' THEN ISNULL(JOB_HEADNAME,'') ELSE JOBGROUP_DESCRIPTION END DptNameCreate,
                                CASE WHEN JOBGROUP_ID = '00003' THEN JOBGROUP_DESCRIPTION ELSE JOBGROUP_ID END DptIDCreate,
		                        SHOP_MNIO_HD.JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID + '-'+BRANCH_NAME AS BRANCH_NAME,WHOINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        STADOC AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEM AS ITEM,ITEMNAME AS ITEMNAME,QTY AS QTY,UNIT AS UNIT,'0' AS COSTAMOUNT,
                                'MNIO' AS TypeBill, 
                                CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH ,'0' AS STA_D003
                    FROM	    [SHOP_MNIO_HD] WITH (NOLOCK) 
                                INNER JOIN [SHOP_MNIO_DT] WITH (NOLOCK) ON  [SHOP_MNIO_HD].DOCNO = [SHOP_MNIO_DT].DOCNO
                                LEFT OUTER JOIN 
                                ( SELECT	JOB_Number,JOB_HEADID,JOB_HEADNAME
                                  FROM	    SHOP_JOBMaintenance WITH (NOLOCK) )JOB ON [SHOP_MNIO_HD].JOB_Number = JOB.JOB_Number
                    WHERE	    [SHOP_MNIO_HD].DOCNO = '{pBillID}' ORDER BY LINENUM ";
                    break;
                case "MNII"://บิลนำของออก
                    sql = $@" 
                    SELECT	[SHOP_MNIO_HD].DOCNO AS Bill_ID,CONVERT(VARCHAR,DATE,23) AS Bill_Date,
                                REMARK AS  RMK,
                                JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP_ID AS DptIDCreate,
		                        JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID + '-'+BRANCH_NAME AS BRANCH_NAME,WHOINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        STADOC AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        '/' AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEM AS ITEM,ITEMNAME AS ITEMNAME,QTY AS QTY,UNIT AS UNIT,'0' AS COSTAMOUNT,
                                'MNII' AS TypeBill, 
                                CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH ,'0' AS STA_D003
                    FROM	[SHOP_MNIO_HD] WITH (NOLOCK) 
                            INNER JOIN [SHOP_MNIO_DT] WITH (NOLOCK) ON  [SHOP_MNIO_HD].DOCNO = [SHOP_MNIO_DT].DOCNO
                    WHERE	[SHOP_MNIO_HD].DOCNO = '{pBillID}' ORDER BY LINENUM ";
                    break;
                case "MNOT"://เครื่องมือ มี 3 รูป 
                    //JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP_ID AS DptIDCreate,
                    sql = $@" 
                    SELECT	SHOP_MNOT_HD.DOCNO AS Bill_ID,CONVERT(VARCHAR,DATE,23) AS Bill_Date,
                                REMARK AS  RMK,
                                CASE WHEN JOBGROUP_ID = '00003' THEN ISNULL(JOB_HEADNAME,'') ELSE JOBGROUP_DESCRIPTION END DptNameCreate,
                                CASE WHEN JOBGROUP_ID = '00003' THEN JOBGROUP_DESCRIPTION ELSE JOBGROUP_ID END DptIDCreate,
		                        SHOP_MNOT_HD.JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID + '-' + BRANCH_NAME AS BRANCH_NAME ,WHOINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        STADOC AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEM AS ITEM,ITEMNAME AS ITEMNAME,QTY AS QTY,UNIT AS UNIT,'0' AS COSTAMOUNT,
                                'MNOT' AS TypeBill,
                                CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID  ,'' AS WH,'0' AS STA_D003  
                    FROM	[SHOP_MNOT_HD] WITH (NOLOCK) INNER JOIN [SHOP_MNOT_DT] WITH (NOLOCK) ON 
                                [SHOP_MNOT_HD].DOCNO = [SHOP_MNOT_DT].DOCNO
                            LEFT OUTER JOIN (SELECT	JOB_Number,JOB_HEADID,JOB_HEADNAME
											FROM	SHOP_JOBMaintenance WITH (NOLOCK)
											)JOB ON [SHOP_MNOT_HD].JOB_Number = JOB.JOB_Number
                    WHERE	SHOP_MNOT_HD.DOCNO = '{pBillID}' ORDER BY LINENUM ";
                    break;
                case "I":
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    break;
                case "F": //ใช SQL เดวกับ
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    break;
                case "FAL":
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    //sql = $@"
                    //SELECT	Bill_ID,CONVERT(VARCHAR,SPC_AssetLendingTable.LendingDate,23) AS Bill_Date,
                    //            SPC_Remarks AS  RMK,JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP_ID AS DptIDCreate,
                    //      JOB_Number AS JOBID,BRANCH_ID ,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,WHOOPENBILL_ID AS WHOINS,WHOOPENBILL_NAME AS WHONAMEINS,
                    //      Bill_StaDoc AS STADOC,
                    //      Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
                    //   Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                    //            CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
                    //      CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                    //            CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
                    //      Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                    //            AssetLending.AssetId AS ITEM,AssetLending.SPC_Name AS ITEMNAME,'1' AS QTY,UnitOfMeasure AS UNIT,UnitCost AS COSTAMOUNT,
                    //            TYPEBILL AS TypeBill,JOBGROUP_DESCRIPTION AS BillDptNameCreate,JOBGROUP_ID AS BillDptIDCreate,
                    //            STA_RECIVE,STA_INSTALL,STA_RETURN,STA_RETURNRECIVE,
                    //            '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate,'' AS INVENTCOSTCENTERID,WH AS WH 
                    //            ,'0' AS STA_D003
                    //FROM	dbo.Shop_BillOut WITH (NOLOCK)    
                    //            INNER JOIN SHOP2013TMP.dbo.SPC_AssetLendingTable WITH (NOLOCK) ON  Shop_BillOut.Bill_ID = SPC_AssetLendingTable.SPC_ASSETLENDINGID    
                    //            INNER JOIN  SHOP2013TMP.dbo.AssetLending WITH (NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = AssetLending.SPC_ASSETLENDINGID    
                    //            INNER JOIN SHOP2013TMP.dbo.AssetTable WITH (NOLOCK) ON AssetLending.AssetId = AssetTable.AssetId  
                    //WHERE	SPC_AssetLendingTable.DATAAREAID = 'SPC' AND AssetLending.DATAAREAID = 'SPC'     
                    //            AND AssetTable.DATAAREAID = 'SPC' AND SPC_AssetLendingTable.SPC_ASSETLENDINGID  = '{pBillID}' ";
                    break;
                case "MNRS"://บิลเบิกส่งเฉพาะสาขา
                    sql = $@"
                    SELECT	SHOP_RO_MNRSHD.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_RO_MNRSHD.DATE,23) AS Bill_Date,
                                REMARK AS  RMK,DPTNAMEINS AS DptNameCreate,DPTIDINS AS DptIDCreate,
		                        JOB_NUMBER AS JOBID,BRANCH_ID ,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME ,WHOIDINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        SHOP_RO_MNRSHD.STADOC AS STADOC,
		                        Bill_TypeBchStatus AS STABCH,Bill_TypeBchRecive AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_TypeBchWhoID AS BCHWhoID,Bill_TypeBchWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_TypeBchDate,25) AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEMBARCODE AS ITEM,SPC_ITEMNAME AS ITEMNAME,QTY AS QTY,UNITID AS UNIT,LINEAMOUNT AS COSTAMOUNT,
                                'MNRS' AS TypeBill, 
                                '1' AS STA_RECIVE,ISNULL(STA_INSTALL,'0') AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,INVENT AS WH ,'0' AS STA_D003
                    FROM	SHOP_RO_MNRSHD WITH (NOLOCK) 
                            INNER JOIN SHOP_RO_MNRSDT WITH (NOLOCK) ON  SHOP_RO_MNRSHD.DOCNO = SHOP_RO_MNRSDT.DOCNO
                    WHERE	SHOP_RO_MNRSHD.DOCNO = '{pBillID}' AND SHOP_RO_MNRSDT.STADOC = '1' ORDER BY LINENUM   ";
                    break;
                case "MNRZ"://บิลส่งของซ่อมภายใน
                    sql = $@"
                    SELECT	SHOP_MNRZ_HD.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRZ_HD.DATE,23) AS Bill_Date,
                                REMARK AS  RMK,DPT_NAME AS DptNameCreate,DPT_ID AS DptIDCreate,
		                        JOB_Number AS JOBID,DPT_ID AS BRANCH_ID ,DPT_ID + '-' +DPT_NAME AS BRANCH_NAME ,WHOINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        SHOP_MNRZ_HD.STADOC AS STADOC,
		                        '0' AS STABCH,'0' AS BCHRecive,'' AS BCHRmk,
			                    '' AS BCHWhoID,'' AS BCHWhoName,
                                '' AS BCHDATE,'' AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEM AS ITEM,ITEMNAME AS ITEMNAME,QTY AS QTY,UNIT AS UNIT,'0' AS COSTAMOUNT,
                                'MNRZ' AS TypeBill, 
                                '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH,'0' AS STA_D003
                    FROM	SHOP_MNRZ_HD WITH (NOLOCK) 
                            INNER JOIN SHOP_MNRZ_DT WITH (NOLOCK) ON  SHOP_MNRZ_HD.DOCNO = SHOP_MNRZ_DT.DOCNO
                    WHERE	SHOP_MNRZ_HD.DOCNO = '{pBillID}' ORDER BY LINENUM   ";
                    break;
                case "MNRX"://บิลส่งของซ่อมภายใน
                    sql = $@"
                    SELECT	SHOP_MNRZ_HD.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRZ_HD.DATE,23) AS Bill_Date,
                                REMARK AS  RMK,DPT_NAME AS DptNameCreate,DPT_ID AS DptIDCreate,
		                        JOB_Number AS JOBID,DPT_ID AS BRANCH_ID ,DPT_ID + '-' +DPT_NAME AS BRANCH_NAME ,WHOINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        SHOP_MNRZ_HD.STADOC AS STADOC,
		                        '0' AS STABCH,'0' AS BCHRecive,'' AS BCHRmk,
			                    '' AS BCHWhoID,'' AS BCHWhoName,
                                '' AS BCHDATE,'' AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEM AS ITEM,ITEMNAME AS ITEMNAME,QTY AS QTY,UNIT AS UNIT,'0' AS COSTAMOUNT,
                                'MNRZ' AS TypeBill, 
                                '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH,'0' AS STA_D003
                    FROM	SHOP_MNRZ_HD WITH (NOLOCK) 
                            INNER JOIN SHOP_MNRZ_DT WITH (NOLOCK) ON  SHOP_MNRZ_HD.DOCNO = SHOP_MNRZ_DT.DOCNO
                    WHERE	SHOP_MNRZ_HD.DOCNO = '{pBillID}' ORDER BY LINENUM   ";
                    break;
                case "MNRB"://บิลส่งของมีทะเบียน
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    //            sql = $@"
                    //            SELECT	    SHOP_MNRB.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRB.DATE,23) AS Bill_Date,
                    //                        REMARK AS  RMK,SentToName AS DptNameCreate,'' AS DptIDCreate,
                    //                  Bill_JOB AS JOBID,BranchID AS BRANCH_ID ,BranchID + '-' + BranchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
                    //                  CASE StaDoc WHEN '3' THEN '0' ELSE '1' END  AS STADOC,
                    //                   '1' AS STABCH,Bill_ReciveSta AS BCHRecive,'' AS BCHRmk,
                    //               Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                    //                        CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_ReciveTime,25) AS BCHDATETIME,
                    //                  CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                    //                        CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
                    //                  Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                    //                        IDProduct AS ITEM,NameProduct AS ITEMNAME,'1' AS QTY,'ชุด' AS UNIT,'0' AS COSTAMOUNT,
                    //                        'MNRB' AS TypeBill, 
                    //                       '1' AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                    //                        ShipSta,ShipID,ShipCarID,ShipDriverID+'-'+EMPLTABLE.SPC_NAME AS ShipDriverID,ShipWho,
                    //                        CONVERT(VARCHAR,ShipDate,23)+' ' + ShipTime AS ShipDate ,'' AS INVENTCOSTCENTERID,BranchID AS WH ,'2' AS STA_D003
                    //                        ,ISNULL(Bill_typeCNRecive,'0') AS Bill_typeCNRecive,Bill_TypeCNRemark,Bill_TypeCNWhoID,Bill_TypeCNWhoName,CONVERT(VARCHAR,Bill_TypeCNDate,23) AS Bill_TypeCNDate
                    //,CONVERT(VARCHAR,Bill_TypeCNTime,25) AS Bill_TypeCNTime
                    //            FROM	    SHOP_MNRB WITH (NOLOCK) 
                    //                        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRB.ShipDriverID= EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'

                    //            WHERE	SHOP_MNRB.DOCNO = '{pBillID}'    ";
                    break;
                case "MNRG"://บิลส่งของไม่มีทะเบียน
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    //            sql = $@"
                    //            SELECT	SHOP_MNRG.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRG.DATE,23) AS Bill_Date,
                    //                        REMARK AS  RMK,SentToName AS DptNameCreate,'' AS DptIDCreate,
                    //                  Bill_JOB AS JOBID,BranchID AS BRANCH_ID ,BranchID +'-'+BranchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
                    //                  CASE StaDoc WHEN '3' THEN '0' ELSE '1' END AS  STADOC,
                    //                   Bill_ReciveSta AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
                    //               Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                    //                        CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_ReciveTime,25) AS BCHDATETIME,
                    //                  CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                    //                        CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
                    //                  Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                    //                        '' AS ITEM,NameProduct AS ITEMNAME,Qty AS QTY,'ชุด' AS UNIT,'0' AS COSTAMOUNT,
                    //                        'MNRG' AS TypeBill, 
                    //                        '1' AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                    //                        ShipSta,ShipID,ShipCarID,ShipDriverID+'-'+EMPLTABLE.SPC_NAME AS ShipDriverID,ShipWho,CONVERT(VARCHAR,ShipDate,23)+' ' + ShipTime AS ShipDate ,
                    //                        '' AS INVENTCOSTCENTERID,BranchID AS WH ,'2' AS STA_D003  
                    //                        ,ISNULL(Bill_typeCNRecive,'0') AS Bill_typeCNRecive,Bill_TypeCNRemark,Bill_TypeCNWhoID,Bill_TypeCNWhoName,CONVERT(VARCHAR,Bill_TypeCNDate,23) AS Bill_TypeCNDate
                    //,CONVERT(VARCHAR,Bill_TypeCNTime,25) AS Bill_TypeCNTime
                    //            FROM	SHOP_MNRG WITH (NOLOCK) 
                    //                        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRG.ShipDriverID= EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    //            WHERE	SHOP_MNRG.DOCNO = '{pBillID}'    ";
                    break;
                case "MNRH"://บิลส่งเอกสาร
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    //             sql = $@"
                    //             SELECT	SHOP_MNRH_HD.DOCNO AS Bill_ID,CONVERT(VARCHAR,SHOP_MNRH_HD.DATE,23) AS Bill_Date,
                    //                         REMARK AS  RMK,'แผนก D004 - แผนกข้อมูลคอมพิวเตอร์ และสถิติ' AS DptNameCreate,'' AS DptIDCreate,
                    //                   '' AS JOBID,BranchID AS BRANCH_ID ,BranchID+'-'+BranchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
                    //                   CASE StaDoc WHEN '3' THEN '0' ELSE '1' END AS  STADOC,
                    //                    '1' AS STABCH,Bill_ReciveSta AS BCHRecive,'' AS BCHRmk,
                    //                Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                    //                         CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,CONVERT(VARCHAR,Bill_ReciveTime,25) AS BCHDATETIME,
                    //                   CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                    //                         CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
                    //                   Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                    //                         '' AS ITEM,ItemName AS ITEMNAME,Qty AS QTY,'ชุด' AS UNIT,'0' AS COSTAMOUNT,
                    //                         'MNRH' AS TypeBill, 
                    //                         '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                    //                         ShipSta,ShipID,ShipCarID,ShipDriverID+'-'+EMPLTABLE.SPC_NAME AS ShipDriverID,ShipWho,CONVERT(VARCHAR,ShipDate,23)+' ' + ShipTime AS ShipDate ,'' AS INVENTCOSTCENTERID,BranchID AS WH ,'2' AS STA_D003  
                    //                         ,ISNULL(Bill_typeCNRecive,'0') AS Bill_typeCNRecive,Bill_TypeCNRemark,Bill_TypeCNWhoID,Bill_TypeCNWhoName,CONVERT(VARCHAR,Bill_TypeCNDate,23) AS Bill_TypeCNDate
                    //	,CONVERT(VARCHAR,Bill_TypeCNTime,25) AS Bill_TypeCNTime
                    //             FROM	SHOP_MNRH_HD WITH (NOLOCK) INNER JOIN SHOP_MNRH_DT WITH (NOLOCK)
                    //ON SHOP_MNRH_HD.DocNo = SHOP_MNRH_DT.DocNo
                    //                         LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRH_HD.ShipDriverID= EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'

                    //             WHERE	SHOP_MNRH_HD.DOCNO = '{pBillID}'  ORDER BY SeqNo ";
                    break;
                case "MNRR"://บิลเบิกของที่สาขาใช้ที่สาขา
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    //             sql = $@"
                    //             SELECT	Shop_MNRR_HD.MNRRDocNo AS Bill_ID,CONVERT(VARCHAR,Shop_MNRR_HD.MNRRDate,23) AS Bill_Date,
                    //                         Shop_MNRR_HD.MNRRRemark AS  RMK,WH.BRANCH_NAME AS DptNameCreate,WH.BRANCH_ID AS DptIDCreate,
                    //                   '' AS JOBID,MNRRBranch AS BRANCH_ID ,
                    //                         MNRRBranchTO+'-'+ CASE WHEN ISNULL(SHOP_BRANCH.BRANCH_NAME,'')='' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END  AS BRANCH_NAME
                    //                         ,MNRRUserCode AS WHOINS,MNRRNAMECH AS WHONAMEINS,
                    //                   CASE MNRRStaDoc WHEN '3' THEN '0' ELSE '1' END AS STADOC,
                    //                   '1' AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
                    //                MNRRUserCode AS BCHWhoID,MNRRNAMECH AS BCHWhoName,
                    //                         CONVERT(VARCHAR,Shop_MNRR_HD.MNRRDate,23) AS BCHDATE,CONVERT(VARCHAR,MNRRTime,25) AS BCHDATETIME,
                    //                   CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                    //                         CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
                    //                   Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                    //                         MNRRBarcode AS ITEM,MNRRName AS ITEMNAME,MNRRQtyOrder AS QTY,MNRRQtyUnitID AS UNIT,MNRRPriceSum AS COSTAMOUNT,
                    //                         'MNRR' AS TypeBill, 
                    //                         '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                    //                        '2' AS  ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,Shop_MNRR_HD.MNRRBranch AS WH
                    //                         ,'1' AS STA_D003
                    //             FROM	Shop_MNRR_HD WITH (NOLOCK) 
                    //                     INNER JOIN  Shop_MNRR_DT WITH (NOLOCK) ON Shop_MNRR_HD.MNRRDocNo = Shop_MNRR_DT.MNRRDocNo
                    //                     LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  SHOP_BRANCH.BRANCH_ID
                    //INNER JOIN SHOP_BRANCH WH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranch =  WH.BRANCH_ID
                    //                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' ANd DIMENSIONS.DIMENSIONCODE = '0'
                    //             WHERE	Shop_MNRR_HD.MNRRDocNo = '{pBillID}'  ORDER BY MNRRSeqNo ";
                    break;

                case "MNPZ"://นำของรางวัล ออก
                    sql = $@"
                     SELECT	SHOP_REWARDTABLE.MNREWARDID AS Bill_ID,CONVERT(VARCHAR,SHOP_REWARDTABLE.DATEIN,23) AS Bill_Date,
                                REMARK AS  RMK,'' AS DptNameCreate,'' AS DptIDCreate,
		                        '' AS JOBID,DIMENSION AS BRANCH_ID ,
                                DIMENSION+'-'+DIMENSIONNAME  AS BRANCH_NAME
                                ,SHOP_REWARDTABLE.WHOIN AS WHOINS,WHONAMEIN AS WHONAMEINS,
		                        CASE StaDoc WHEN '3' THEN '0' ELSE '1' END AS STADOC,
		                        '1' AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
			                    '' AS BCHWhoID,'' AS BCHWhoName,
                                '' AS BCHDATE,'' AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ITEMBARCODE AS ITEM,DESCRIPTION AS ITEMNAME,EDIT AS QTY,'Ea.' AS UNIT,AMOUNTCUR AS COSTAMOUNT,
                                'MNPZ' AS TypeBill, 
                                '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                               '2' AS  ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH
                                ,'0' AS STA_D003
                    FROM	SHOP_REWARDTABLE WITH (NOLOCK) 
                            INNER JOIN SHOP_REWARDLINE WITH (NOLOCK) ON SHOP_REWARDTABLE.MNREWARDID = SHOP_REWARDLINE.MNREWARDID
                 
                    WHERE	SHOP_REWARDTABLE.MNREWARDID = '{pBillID}'  ORDER BY LINENUM ";
                    break;

                case "MNCM":
                    sql = $@"  SELECT	Claim_ID AS Bill_ID,CONVERT(VARCHAR,DATE_INS,23) AS Bill_Date,
                               CASE WHEN STA_TYPE = '1' THEN 'นำทิ้ง ' ELSE 'ส่งซ่อม ' END +DESCRIPTION  AS  RMK,JOBGROUP_DESCRIPTION AS DptNameCreate,JOBGROUP AS DptIDCreate,
		                        '' AS JOBID,CASE WHEN STA_TYPE = '1' THEN 'D050 ' ELSE 'D035 ' END AS BRANCH_ID ,
                               CASE WHEN STA_TYPE = '1' THEN 'D050 แผนกแม่บ้าน' ELSE 'D035 แผนกรับคืนสินค้า C/N' END AS BRANCH_NAME,WHOIDINS AS WHOINS,WHONAMEINS AS WHONAMEINS,
		                        STADOC AS STADOC,
		                        CASE WHEN STA_TYPE = '1' THEN '1' ELSE RECIVE_STA END AS STABCH,
								CASE WHEN STA_TYPE = '1' THEN '1' ELSE RECIVE_STA END	AS BCHRecive,
								REPLACE(CASE WHEN STA_TYPE = '1' THEN '' ELSE RECIVE_DESC END,CHAR(10),' ')  AS BCHRmk,
			                    CASE WHEN STA_TYPE = '1' THEN '' ELSE RECIVE_WHOID END  AS BCHWhoID,
								CASE WHEN STA_TYPE = '1' THEN '' ELSE RECIVE_WHONAME END  AS BCHWhoName,
                                CASE WHEN STA_TYPE = '1' THEN '' ELSE CONVERT(VARCHAR,RECIVE_DATE,23) END  AS BCHDATE,
								CASE WHEN STA_TYPE = '1' THEN '' ELSE CONVERT(VARCHAR,RECIVE_DATE,25) END  AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                ASSETID AS ITEM,Name AS ITEMNAME,QTY AS QTY,UNIT AS UNIT,'0' AS COSTAMOUNT,
                                'MNCM' AS TypeBill, 
                                '1' AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,
                                CASE WHEN STA_TYPE = '1' THEN '0' ELSE '1' END AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH ,'0' AS STA_D003
                    FROM	SHOP_JOBCLAIM WITH (NOLOCK)  
                    WHERE	Claim_ID = '{pBillID}' ";
                    break;
                case "MNRD":
                    sql = BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, "SHOP2013TMP.dbo.");
                    //sql = $@"
                    //SELECT	SHOP_MNRD_HD.MNRDDocNo AS Bill_ID,CONVERT(VARCHAR,MNRDDATE,23) AS Bill_Date,
                    //  CASE [TYPEPRODUCT] WHEN '0' THEN 'รถจัดส่ง' ELSE 'รถห้องเย็น' END  + CHAR(10) +ShipDoc + CHAR(10) + MNRDSHID  + CHAR(10) + SHOP_MNRD_HD.MNRDREMARK AS  RMK,
                    //        '' AS DptNameCreate,MNRDBRANCH AS DptIDCreate,
                    //  '' AS JOBID,MNRDBRANCH AS BRANCH_ID ,
                    //   MNRDBRANCH+'-'+BRANCH_NAME  AS BRANCH_NAME,MNRDUSERCODE AS WHOINS,SPC_NAME AS WHONAMEINS,
                    //  CASE MNRDSTADOC WHEN '3' THEN '0' ELSE '1' END AS STADOC,
                    //  '1' AS STABCH,'1' AS BCHRecive,'' AS BCHRmk,
                    //  '' AS BCHWhoID,'' AS BCHWhoName,
                    //  '' AS BCHDATE,'' AS BCHDATETIME,
                    //  '/' AS Bill_SpcType,'' AS Bill_SpcWhoIn,'' AS Bill_SpcWhoName,
                    //  '' AS Bill_SpcDate,'' AS Bill_SpcDateTime,
                    //  '' AS Bill_SpcMoneyStatus,'' AS Bill_SpcMoneySum,'' AS Bill_SpcMoneyID,'' AS Bill_SpcMoneyName,'' AS Bill_SpcRemark,
                    //  MNRDBarcode AS ITEM,MNRDName AS ITEMNAME,MNRDReturnQty AS QTY,'Ea.' AS UNIT,'0' AS COSTAMOUNT,
                    //  'MNRD' AS TypeBill, 
                    //  '0' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                    //  '2' AS  ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,'' AS WH,'1' AS STA_D003
                    //FROM	SHOP_MNRD_HD WITH(NOLOCK) 
                    //   INNER JOIN SHOP_MNRD_DT ON SHOP_MNRD_HD.MNRDDOCNO = SHOP_MNRD_DT.MNRDDocNo
                    //   INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDBRANCH = SHOP_BRANCH.BRANCH_ID
                    //   INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDUSERCODE = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    //WHERE	 SHOP_MNRD_HD.MNRDDOCNO = '{pBillID}'  
                    //ORDER BY MNRDSeqNo ";
                    break;
                case "MNRM"://บิลเบิกส่งเฉพาะสาขา
                    sql = $@"
                    SELECT	DOCNO AS Bill_ID,CONVERT(VARCHAR,DATE,23) AS Bill_Date,
                                REMARK AS  RMK,BranchName AS DptNameCreate,BranchID AS DptIDCreate,
		                        '' AS JOBID,SentToBch AS BRANCH_ID ,SentToBch+'-'+SentToBchName AS BRANCH_NAME ,UserID AS WHOINS,UserName AS WHONAMEINS,
		                        STADOC AS STADOC,
		                        Bill_ReciveSta AS STABCH,Bill_ReciveSta AS BCHRecive,Bill_TypeBchRemark AS BCHRmk,
			                    Bill_ReciveWhoIn AS BCHWhoID,Bill_ReciveWhoName AS BCHWhoName,
                                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE,[Bill_ReciveTime] AS BCHDATETIME,
		                        CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,Bill_SpcWhoIn,Bill_SpcWhoName,
                                CONVERT(VARCHAR,Bill_SpcDate,23) AS Bill_SpcDate,CONVERT(VARCHAR,Bill_SpcDate,25) AS Bill_SpcDateTime,
		                        Bill_SpcMoneyStatus,Bill_SpcMoneySum,Bill_SpcMoneyID,Bill_SpcMoneyName,Bill_SpcRemark,
                                '' AS ITEM,NameProduct AS ITEMNAME,QTY AS QTY,'ชิ้น' AS UNIT,'0' AS COSTAMOUNT,
                                'MNRM' AS TypeBill, 
                                '1' AS STA_RECIVE,'0' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  ,
                                '2' AS ShipSta,'' AS ShipID,'' AS ShipCarID,'' AS ShipDriverID,'' AS ShipWho,'' AS ShipDate ,'' AS INVENTCOSTCENTERID,BranchID AS WH ,'0' AS STA_D003
                    FROM	SHOP_MNRM WITH (NOLOCK)  
                    WHERE	DOCNO = '{pBillID}' AND STADOC = '1'    ";
                    break;
                default:
                    break;
            }

            DataTable Dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            if (((Dt.Rows.Count == 0) && (pBillCase == "I")) || ((Dt.Rows.Count == 0) && (pBillCase == "F")))
                return ConnectionClass.SelectSQL_Main(BillOutClass.SqlFindBill_ByBilID(pBillCase, pBillID, " SPC704SRV.AX50SP1_SPC."));
            else return Dt;
        }
        //ค้นหาบิลทั้งหมดส่งของ ตามเลขที่ JOB
        public static DataTable FindAllBill_ByJobNumberID(string pJobNumberID)
        {
            string sql = $@"
                        SELECT	BRANCH_ID,DOCNO AS Bill_ID, CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,
                                CASE SUBSTRING(BRANCH_ID,1,1) WHEN 'D' THEN '/' ELSE CASE Bill_TypeBchStatus WHEN '1' THEN '/' ELSE 'X' END END  AS Bill_TypeBchStatus, 
                                CONVERT(VARCHAR,DATE,23) AS Bill_Date,'MNIO' AS TypeOpenBill ,
                                CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE    
                        FROM    [SHOP_MNIO_HD] WITH (NOLOCK) 
                        WHERE   STADOC = '1' AND JOB_Number = '{pJobNumberID}' 
                        union 
                        SELECT  BRANCH_ID, DOCNO AS Bill_ID,CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,
                                CASE SUBSTRING(BRANCH_ID,1,1) WHEN 'D' THEN '/' ELSE CASE Bill_TypeBchStatus WHEN '1' THEN '/' ELSE 'X' END END  AS Bill_TypeBchStatus,
                                CONVERT(VARCHAR, DATE, 23) AS Bill_Date,'MNOT' AS TypeOpenBill,
                                CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  
                        FROM     [SHOP_MNOT_HD] WITH(NOLOCK) 
                        WHERE   STADOC = '1' AND JOB_Number = '{pJobNumberID}' 
                        union 
                        SELECT	BRANCH_ID, Bill_ID, CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,
                                CASE SUBSTRING(BRANCH_ID,1,1) WHEN 'D' THEN '/' ELSE CASE Bill_TypeBchStatus WHEN '1' THEN '/' ELSE 'X' END END  AS Bill_TypeBchStatus, 
                                CONVERT(VARCHAR, DATE, 23) AS Bill_Date, TYPEBILL AS TypeOpenBill ,  STA_RECIVE,STA_INSTALL,STA_RETURN,STA_RETURNRECIVE
                        FROM    SHOP_BillOut WITH(NOLOCK) 
                        WHERE   Bill_StaDoc = '1' AND JOB_NUMBER = '{pJobNumberID}'
                        UNION
                        SELECT  BRANCH_ID, DOCNO AS Bill_ID,CASE Bill_SpcType WHEN '1' THEN '/' ELSE 'X' END AS Bill_SpcType,
		                        CASE SUBSTRING(BRANCH_ID,1,1) WHEN 'D' THEN '/' ELSE CASE Bill_TypeBchStatus WHEN '1' THEN '/' ELSE 'X' END END  AS Bill_TypeBchStatus,
		                        CONVERT(VARCHAR, DATE, 23) AS Bill_Date,'MNRS' AS TypeOpenBill,
		                        CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'1' AS  STA_INSTALL,'0' AS  STA_RETURN,'0' AS STA_RETURNRECIVE  
                        FROM    SHOP_RO_MNRSHD WITH(NOLOCK) 
                        WHERE	STADOC = '1' AND JOB_Number = '{pJobNumberID}' 
                        ORDER BY Bill_ID ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาบิลทั้งหมดส่งซ่อม ตามเลขที่ JOB
        public static DataTable FindAllBillMNRZRB_ByJobNumberID(string pJobNumberID)
        {
            string sql = $@"
                        SELECT	DPT_ID AS BRANCH_ID,DOCNO AS Bill_ID, 
                                CONVERT(VARCHAR,DATE,23) AS Bill_Date,'0' AS STA_RECIVE, '0' AS STA_INSTALL
                        FROM    [SHOP_MNRZ_HD] WITH (NOLOCK) 
                        WHERE   STADOC = '1' AND JOB_Number = '{pJobNumberID}' 
                        union 
                        SELECT	BranchID AS BRANCH_ID,DOCNO AS Bill_ID, 
                                CONVERT(VARCHAR,DATE,23) AS Bill_Date,'1' AS STA_RECIVE, '1' AS STA_INSTALL
                        FROM    SHOP_MNRB WITH (NOLOCK) 
                        WHERE   STADOC = '1' AND Bill_JOB = '{pJobNumberID}'
                        union
                        SELECT	BranchID AS BRANCH_ID,DOCNO AS Bill_ID, 
                                CONVERT(VARCHAR,DATE,23) AS Bill_Date,'1' AS STA_RECIVE, '1' AS STA_INSTALL
                        FROM    SHOP_MNRG WITH (NOLOCK) 
                        WHERE   STADOC = '1' AND Bill_JOB = '{pJobNumberID}' ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาบิลทั้งหมด
        public static DataTable FindAllBillNotApv_ByTypeID(string pTypeBill)
        {
            string sql = "";
            switch (pTypeBill)
            {
                case "MNIO"://บิลนำของออก
                    sql = $@"SELECT  Bill_SpcType,'0' AS TYPERETURN,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNIO' AS TYPEBILL, REMARK AS  RMK,WHOINS+'-'+WHONAMEINS AS WHONAMEINS,
		                    CASE WHEN BRANCH_ID LIKE 'D%' THEN '1' ELSE Bill_TypeBchStatus END AS STABCH,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN Bill_TypeBchWhoID+'-'+Bill_TypeBchWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE
                     FROM	SHOP_MNIO_HD WITH (NOLOCK)    
                     WHERE	STADOC = '1' AND Bill_SpcType = '0' AND DOCNO LIKE 'MNIO%'  
                     ORDER BY Bill_ID ";
                    break;
                case "MNOT"://เครื่องมือ มี 3 รูป 
                    sql = $@"  SELECT  Bill_SpcType,'0' AS TYPERETURN,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNOT' AS TYPEBILL, REMARK AS  RMK,ISNULL([JOB_HEADNAME],WHOINS+'-'+WHONAMEINS) AS WHONAMEINS,
		                    CASE WHEN BRANCH_ID LIKE 'D%' THEN '1' ELSE Bill_TypeBchStatus END AS STABCH,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN Bill_TypeBchWhoID+'-'+Bill_TypeBchWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE
                     FROM	SHOP_MNOT_HD WITH (NOLOCK)    
                            LEFT OUTER JOIN SHOP_JOBMaintenance WITH (NOLOCK) ON SHOP_MNOT_HD.JOB_Number = SHOP_JOBMaintenance.JOB_Number 
                     WHERE	STADOC = '1' AND Bill_SpcType = '0'
                     ORDER BY Bill_ID ";
                    break;
                case "I":
                    sql = $@"SELECT  Bill_SpcType,STA_RETURN AS TYPERETURN,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,Bill_INVENTCOSTCENTERID AS TYPEBILL, REMARK AS  RMK,WHOOPENBILL_ID+'-'+WHOOPENBILL_NAME AS WHONAMEINS,
		                    CASE WHEN BRANCH_ID LIKE 'D%' THEN '1' ELSE Bill_TypeBchStatus END AS STABCH,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN Bill_TypeBchWhoID+'-'+Bill_TypeBchWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE
                     FROM	Shop_BillOut WITH (NOLOCK)    
                     WHERE	Bill_StaDoc = '1' AND Bill_SpcType = '0'
		                    AND TYPEBILL NOT  IN ('FAL','IDM')
                     ORDER BY Bill_ID ";
                    break;
                case "FAL":
                    sql = $@"SELECT  Bill_SpcType,STA_RETURN AS TYPERETURN,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,Bill_ID
		                ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,TYPEBILL, REMARK AS  RMK,WHOOPENBILL_ID+'-'+WHOOPENBILL_NAME AS WHONAMEINS,
		                CASE WHEN BRANCH_ID LIKE 'D%' THEN '1' ELSE Bill_TypeBchStatus END AS STABCH,
		                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                CASE Bill_TypeBchRecive WHEN '1' THEN Bill_TypeBchWhoID+'-'+Bill_TypeBchWhoName ELSE '' END AS BCHWhoName,
		                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE
                 FROM	Shop_BillOut WITH (NOLOCK)    
                 WHERE	Bill_StaDoc = '1' AND Bill_SpcType = '0'
		                AND TYPEBILL IN ('FAL')
                 ORDER BY Bill_ID ";
                    break;
                case "MNRS":
                    sql = $@"
                    SELECT	Bill_SpcType,'0' AS TYPERETURN,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,SHOP_RO_MNRSHD.DATE,23) AS DATE_INS,'MNRS' AS TypeBill,
		                    REMARK AS RMK,WHOIDINS+'-'+WHONAMEINS AS WHONAMEINS,
		                    Bill_TypeBchStatus AS STABCH,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN Bill_TypeBchWhoID+'-'+Bill_TypeBchWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE
                    FROM	SHOP_RO_MNRSHD WITH (NOLOCK) 
                    WHERE	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY DOCNO   ";
                    break;

                case "MNRR":
                    sql = $@"
                    SELECT	Bill_SpcType,'0' AS TYPERETURN,MNRRBranch+'-'+BRANCH_NAME AS BRANCH_NAME,MNRRDocNo AS Bill_ID
		                    ,CONVERT(VARCHAR,MNRRDate,23) AS DATE_INS,'MNRR' AS TypeBill,
		                    MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END AS RMK,MNRRWhoIn+'-'+MNRRNAMECH AS WHONAMEINS,
		                    '1' AS STABCH,
		                    '' AS BchRemark,
		                    '' AS BCHWhoName,
		                    '' AS BCHDATE
                    FROM	Shop_MNRR_HD WITH (NOLOCK) 
							INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranch = SHOP_BRANCH.BRANCH_ID
                    WHERE	Bill_SpcType = '0' AND MNRRStaDoc = '1'
                    ORDER BY MNRRDocNo   ";
                    break;

                case "MNPZ":
                    sql = $@"
                    SELECT	Bill_SpcType,'0' AS TYPERETURN,DIMENSION+'-'+DIMENSIONNAME AS BRANCH_NAME,MNREWARDID AS Bill_ID
		                    ,CONVERT(VARCHAR,DATEIN,23) AS DATE_INS,'MNPZ' AS TypeBill,
		                    REMARK AS RMK,WHOIN+'-'+WHONAMEIN AS WHONAMEINS,
		                    '1' AS STABCH,
		                    '' AS BchRemark,
		                    '' AS BCHWhoName,
		                    '' AS BCHDATE
                    FROM	SHOP_REWARDTABLE WITH (NOLOCK) 
                    WHERE	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY MNREWARDID   ";
                    break;

                case "MNRG":
                    sql = $@"
                     SELECT	Bill_SpcType,'0' AS TYPERETURN,BranchID+'-'+BranchName AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNRG' AS TypeBill,
		                    REMARK AS RMK,UserID+'-'+UserName AS WHONAMEINS,
		                    Bill_ReciveSta AS STABCH,
		                    CASE Bill_ReciveSta WHEN '1' THEN 'รับสินค้าครบ ' WHEN '2' THEN 'รับขาด '  END AS BchRemark,
		                    CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+'-'+Bill_ReciveWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE
                    FROM	SHOP_MNRG WITH (NOLOCK) 
                    WHERE	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY DOCNO   ";
                    break;

                case "MNRB":
                    sql = $@"
                      SELECT	Bill_SpcType,'0' AS TYPERETURN,BranchID+'-'+BranchName AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNRB' AS TypeBill,
		                    REMARK AS RMK,UserID+'-'+UserName AS WHONAMEINS,
		                    Bill_ReciveSta AS STABCH,
		                    CASE Bill_ReciveSta WHEN '1' THEN 'รับสินค้าครบ ' WHEN '2' THEN 'รับขาด '  END AS BchRemark,
		                    CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+'-'+Bill_ReciveWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE
                    FROM	SHOP_MNRB WITH (NOLOCK) 
                    WHERE	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY DOCNO   ";
                    break;

                case "MNRH":
                    sql = $@"
                       SELECT	Bill_SpcType,'0' AS TYPERETURN,BranchID+'-'+BranchName AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNRH' AS TypeBill,
		                    REMARK AS RMK,UserID+'-'+UserName AS WHONAMEINS,
		                    Bill_ReciveSta AS STABCH,
		                    CASE Bill_ReciveSta WHEN '1' THEN 'รับสินค้าครบ ' WHEN '2' THEN 'รับขาด '  END AS BchRemark,
		                    CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+'-'+Bill_ReciveWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE
                    FROM	SHOP_MNRH_HD WITH (NOLOCK) 
                    WHERE	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY DOCNO   ";
                    break;

                case "MNRZ":
                    sql = $@"
                       SELECT	Bill_SpcType,'0' AS TYPERETURN,DPT_ID+'-'+DPT_NAME AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNRZ' AS TypeBill,
		                    REMARK AS RMK,WHOINS+'-'+WHONAMEINS AS WHONAMEINS,
		                    '1' AS STABCH,
		                    '' AS BchRemark,
		                    '' AS BCHWhoName,
		                    '' AS BCHDATE
                    FROM	SHOP_MNRZ_HD WITH (NOLOCK) 
                    WHERE 	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY DOCNO   ";
                    break;

                case "IDM":
                    sql = $@"SELECT  Bill_SpcType,STA_RETURN AS TYPERETURN,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,TYPEBILL, REMARK AS  RMK,WHOOPENBILL_ID+'-'+WHOOPENBILL_NAME AS WHONAMEINS,
		                    '1' AS STABCH,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                    CASE Bill_TypeBchRecive WHEN '1' THEN Bill_TypeBchWhoID+'-'+Bill_TypeBchWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BCHDATE
                     FROM	Shop_BillOut WITH (NOLOCK)    
                     WHERE	Bill_StaDoc = '1' AND Bill_SpcType = '0'
		                    AND TYPEBILL IN ('IDM')
                     ORDER BY Bill_ID ";
                    break;

                case "MNCM_0":
                    sql = $@"SELECT  Bill_SpcType,'1' AS TYPERETURN, 'D035-แผนกรับคืนสินค้า C/N' AS BRANCH_NAME,Claim_ID AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE_INS,23) AS DATE_INS,'MNCM' AS TYPEBILL, DESCRIPTION AS  RMK,WHOID_SEND+'-'+WHONAMEINS AS WHONAMEINS,
		                    RECIVE_STA AS STABCH,
		                   RECIVE_DESC AS BchRemark,
		                    CASE RECIVE_STA WHEN '1' THEN RECIVE_WHOID+'-'+RECIVE_WHONAME ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,RECIVE_DATE,23) AS BCHDATE
                     FROM	SHOP_JOBCLAIM WITH (NOLOCK)    
                     WHERE	StaDoc = '1' AND Bill_SpcType = '0'  AND RECIVE_STA = '1'
		                    AND STA_TYPE IN ('0')
                     ORDER BY Claim_ID ";
                    break;

                case "MNCM_1":
                    sql = $@"SELECT  Bill_SpcType,'1' AS TYPERETURN, 'D050-แผนกแม่บ้าน' AS BRANCH_NAME,Claim_ID AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE_INS,23) AS DATE_INS,'MNCM' AS TYPEBILL, DESCRIPTION AS  RMK,WHOID_SEND+'-'+WHONAMEINS AS WHONAMEINS,
		                    RECIVE_STA AS STABCH,
		                   RECIVE_DESC AS BchRemark,
		                    CASE RECIVE_STA WHEN '1' THEN RECIVE_WHOID+'-'+RECIVE_WHONAME ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,RECIVE_DATE,23) AS BCHDATE
                     FROM	SHOP_JOBCLAIM WITH (NOLOCK)    
                            LEFT OUTER JOIN [Shop_BillOutHistoryShearch] WITH (NOLOCK)   ON SHOP_JOBCLAIM.Claim_ID = [Shop_BillOutHistoryShearch].BILLID AND   TYPESHEARCH = 'VT' AND USERCREATE = 'S'  
                     WHERE	StaDoc = '1' AND Bill_SpcType = '0'
		                    AND STA_TYPE IN ('1')  AND ISNULL(TYPESHEARCH,'0') != '0'  
                     ORDER BY Claim_ID ";
                    break;
                // AND RECIVE_STA = '1' 
                case "MNCM_2":
                    sql = $@"SELECT  Bill_SpcType,'1' AS TYPERETURN, 'D035-แผนกรับคืนสินค้า C/N' AS BRANCH_NAME,Claim_ID AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE_INS,23) AS DATE_INS,'MNCM' AS TYPEBILL, DESCRIPTION AS  RMK,WHOID_SEND+'-'+WHONAMEINS AS WHONAMEINS,
		                    RECIVE_STA AS STABCH,
		                   RECIVE_DESC AS BchRemark,
		                    CASE RECIVE_STA WHEN '1' THEN RECIVE_WHOID+'-'+RECIVE_WHONAME ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,RECIVE_DATE,23) AS BCHDATE
                     FROM	SHOP_JOBCLAIM WITH (NOLOCK)    
                     WHERE	StaDoc = '1' AND Bill_SpcType = '0'  AND RECIVE_STA = '1'
		                    AND STA_TYPE IN ('2')
                     ORDER BY Claim_ID ";
                    break;

                case "MNCM_3":
                    sql = $@"SELECT  Bill_SpcType,'1' AS TYPERETURN, 'D035-แผนกรับคืนสินค้า C/N' AS BRANCH_NAME,Claim_ID AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE_INS,23) AS DATE_INS,'MNCM' AS TYPEBILL, DESCRIPTION AS  RMK,WHOID_SEND+'-'+WHONAMEINS AS WHONAMEINS,
		                    RECIVE_STA AS STABCH,
		                   RECIVE_DESC AS BchRemark,
		                    CASE RECIVE_STA WHEN '1' THEN RECIVE_WHOID+'-'+RECIVE_WHONAME ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,RECIVE_DATE,23) AS BCHDATE
                     FROM	SHOP_JOBCLAIM WITH (NOLOCK)    
                     WHERE	StaDoc = '1' AND Bill_SpcType = '0'  AND RECIVE_STA = '1'
		                    AND STA_TYPE IN ('3')
                     ORDER BY Claim_ID ";
                    break;

                case "MNRM":
                    sql = $@"
                    SELECT	Bill_SpcType,'0' AS TYPERETURN,SentToBch+'-'+SentToBchName AS BRANCH_NAME,DOCNO AS Bill_ID
		                    ,CONVERT(VARCHAR,DATE,23) AS DATE_INS,'MNRM' AS TypeBill,
		                    REMARK AS RMK,UserID+'-'+UserName AS WHONAMEINS,
		                    Bill_ReciveSta AS STABCH,
                            CASE Bill_ReciveSta WHEN '1' THEN Bill_TypeBchRemark ELSE '' END AS BchRemark,
		                    CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+'-'+Bill_ReciveWhoName ELSE '' END AS BCHWhoName,
		                    CONVERT(VARCHAR,Bill_ReciveDate,23) AS BCHDATE
                    FROM	SHOP_MNRM WITH (NOLOCK) 
                    WHERE	Bill_SpcType = '0' AND STADOC = '1'
                    ORDER BY DOCNO    ";
                    break;

                default:
                    break;
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาบิลเพื่อออกรายกงาน
        public static DataTable FindBillForReport_ByTypeBill(string pTypeBill, string pDate1, string pDate2, string pCondition, string pIS)
        {
            string sql = string.Empty;
            switch (pTypeBill)
            {
                case "MNRS":
                    sql = $@"SELECT BRANCH_ID, BRANCH_ID + '-' + BRANCH_NAME AS BRANCH_NAME, DOCNO, CONVERT(VARCHAR, DATE, 23) AS DATE,
                        WHOIDINS + '-' + WHONAMEINS AS InsName,
                        Bill_TypeBchStatus,
                        CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ ' + Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
                        CONVERT(VARCHAR, Bill_TypeBchDate, 23) AS BchDate,
                        CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID + char(10) + Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
                        REMARK, Bill_SpcType, STADOC,
                        CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END  AS STA_RECIVE,ISNULL(STA_INSTALL,'0') AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, '0') AS ComMN  ,DPTIDINS AS DPT_ID   ,'MNRS' AS ISA
                FROM    SHOP_RO_MNRSHD WITH(NOLOCK)
                        LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_RO_MNRSHD.WHOIDINS  = TMPCOM.EMP_ID  
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}'  AND BRANCH_ID LIKE '{pCondition}'  ORDER BY BRANCH_ID,DOCNO";
                    break;

                case "MNOT":
                    sql = $@"SELECT	BRANCH_ID,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,DOCNO,CONVERT(VARCHAR,DATE,23) AS DATE,
		                WHOINS+'-'+WHONAMEINS AS InsName,
		                Bill_TypeBchStatus,
		                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                REMARK,Bill_SpcType,STADOC,
						CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN,CASE JOBGROUP_ID WHEN '00002' THEN 'D179' ELSE '' END AS DPT_ID   ,'MNOT' AS ISA
                FROM	SHOP_MNOT_HD WITH (NOLOCK)  
                         LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_MNOT_HD.WHOINS  = TMPCOM.EMP_ID 
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}' AND BRANCH_ID LIKE '{pCondition}'  ORDER BY BRANCH_ID,DOCNO ";
                    break;
                case "Detail_MNOT":
                    sql = $@"SELECT	BRANCH_ID,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,SHOP_MNOT_HD.DOCNO,CONVERT(VARCHAR,DATE,23) AS DATE,
		                WHOINS+'-'+WHONAMEINS AS InsName,
                        ITEM AS Barcode,ITEMNAME AS NameProduct,QTY,UNIT,
		                Bill_TypeBchStatus,
		                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                REMARK,Bill_SpcType,STADOC,
						CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN,CASE JOBGROUP_ID WHEN '00002' THEN 'D179' ELSE '' END AS DPT_ID   ,'MNOT' AS ISA
                FROM	SHOP_MNOT_HD WITH (NOLOCK)  
                        LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_MNOT_HD.WHOINS  = TMPCOM.EMP_ID 
                        INNER JOIN SHOP_MNOT_DT WITH (NOLOCK) ON SHOP_MNOT_HD.DOCNO = SHOP_MNOT_DT.DOCNO
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}' AND BRANCH_ID LIKE '{pCondition}'  ORDER BY BRANCH_ID,SHOP_MNOT_HD.DOCNO ";
                    break;

                case "MNIO":
                    sql = $@"SELECT	BRANCH_ID,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,DOCNO,CONVERT(VARCHAR,DATE,23) AS DATE,
		                WHOINS+'-'+WHONAMEINS AS InsName,
		                Bill_TypeBchStatus,
		                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                REMARK,Bill_SpcType,STADOC,
						CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,CASE JOBGROUP_ID WHEN '00002' THEN 'D179' ELSE '' END AS DPT_ID   ,'MNIO' AS ISA
                FROM	SHOP_MNIO_HD WITH (NOLOCK)  
                         LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_MNIO_HD.WHOINS  = TMPCOM.EMP_ID 
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}' AND  BRANCH_ID LIKE  '{pCondition}'  AND DOCNO LIKE 'MNIO%'  ORDER BY BRANCH_ID,DOCNO ";
                    break;
                case "Detail_MNIO":
                    sql = $@"SELECT	BRANCH_ID,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,SHOP_MNIO_HD.DOCNO,CONVERT(VARCHAR,DATE,23) AS DATE,
		                WHOINS+'-'+WHONAMEINS AS InsName,
                        ITEM AS Barcode,ITEMNAME AS NameProduct,QTY,UNIT,
		                Bill_TypeBchStatus,
		                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                REMARK,Bill_SpcType,STADOC,
						CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,CASE JOBGROUP_ID WHEN '00002' THEN 'D179' ELSE '' END AS DPT_ID   ,'MNIO' AS ISA
                FROM	SHOP_MNIO_HD WITH (NOLOCK)  
                        LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_MNIO_HD.WHOINS  = TMPCOM.EMP_ID 
                        INNER JOIN SHOP_MNIO_DT WITH (NOLOCK) ON SHOP_MNIO_HD.DOCNO = SHOP_MNIO_DT.DOCNO 
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}' AND  BRANCH_ID LIKE  '{pCondition}'  AND SHOP_MNIO_HD.DOCNO LIKE 'MNIO%'  ORDER BY BRANCH_ID,SHOP_MNIO_HD.DOCNO ";
                    break;
                case "MNII":
                    sql = $@"SELECT	BRANCH_ID,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_NAME,DOCNO,CONVERT(VARCHAR,DATE,23) AS DATE,
		                WHOINS+'-'+WHONAMEINS AS InsName,
		                '1' AS Bill_TypeBchStatus,
		                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
		                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
		                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
		                REMARK,'1' AS Bill_SpcType,STADOC,
						CASE WHEN SUBSTRING([BRANCH_ID],1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,CASE JOBGROUP_ID WHEN '00002' THEN 'D179' ELSE '' END AS DPT_ID   ,'MNIO' AS ISA
                FROM	SHOP_MNIO_HD WITH (NOLOCK)  
                         LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_MNIO_HD.WHOINS  = TMPCOM.EMP_ID 
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}' AND  BRANCH_ID LIKE  '{pCondition}'  AND DOCNO LIKE 'MNII%'  ORDER BY BRANCH_ID,DOCNO ";
                    break;
                case "IMN":
                    sql = BillOutClass.FindBillForReport_ByTypeBill(pTypeBill, pDate1, pDate2, pCondition, pIS);
                    //  sql = $@" SELECT	 SPC_DIMENSION AS BRANCH_ID,SPC_DIMENSION+'-'+ISNULL(Shop_Branch.BRANCH_NAME,'') AS BRANCH_NAME,
                    //JournalId AS DOCNO,CONVERT(VARCHAR,SPC_TransDate,23) AS DATE,
                    //                InventJournalTable.EMPLID+'-'+ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS InsName,
                    //                ISNULL(Bill_TypeBchStatus,0) AS Bill_TypeBchStatus,
                    //                CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
                    //                CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
                    //                CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
                    //                IVZ_Remarks AS REMARK,ISNULL(Bill_SpcType,0) AS Bill_SpcType,Bill_StaDoc AS STADOC,
                    //    ISNULL(STA_RECIVE, CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END) AS STA_RECIVE,
                    //    ISNULL(STA_INSTALL,CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END) AS STA_INSTALL,
                    //    ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN, ISNULL(Shop_BillOut.STA_RETURNRECIVE,0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                    //                      ISNULL(TMPCOM.EMP_ID, 0) AS ComMN    ,SPC_Department AS DPT_ID     ,Bill_INVENTCOSTCENTERID AS ISA  

                    //      FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 
                    //                           LEFT OUTER JOIN  Shop_Branch WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = Shop_Branch.BRANCH_ID 
                    //                           INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DEPARTMENT = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
                    //                           LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                    //                           LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                    //                           LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum  = TMPCOM.EMP_ID 
                    //                           LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                    //                           ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]

                    //      WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                    //          AND JournalId NOT LIKE 'MNRS%' AND JournalId NOT LIKE 'MNRR%' 
                    //          AND [JOURNALTYPE] ! = '4'  AND INVENTJOURNALTABLE.JournalNameId != 'IM-MM-PT'   
                    //          AND SPC_DIMENSION LIKE '{pCondition}' 
                    //          AND  CONVERT(VARCHAR, SPC_TransDate, 23) BETWEEN '{pDate1}' AND '{pDate2}'    

                    //      ORDER BY SPC_DIMENSION,JournalId ";
                    break;

                case "ISPC":
                    sql = BillOutClass.FindBillForReport_ByTypeBill(pTypeBill, pDate1, pDate2, pCondition, pIS);
                    //sql = $@" SELECT	 SPC_DIMENSION AS BRANCH_ID,SPC_DIMENSION+'-'+ISNULL(B_NAME.DESCRIPTION,'') AS BRANCH_NAME,
                    //      JournalId AS DOCNO,CONVERT(VARCHAR,SPC_TransDate,23) AS DATE,
                    //              InventJournalTable.EMPLID+'-'+ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS InsName,
                    //              ISNULL(Bill_TypeBchStatus,0) AS Bill_TypeBchStatus,
                    //              CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
                    //              CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
                    //              CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
                    //              IVZ_Remarks AS REMARK,Bill_SpcType,Bill_StaDoc AS STADOC,
                    //  ISNULL(STA_RECIVE, CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END) AS STA_RECIVE,
                    //  ISNULL(STA_INSTALL,CASE WHEN ISNULL([SHOW_ID],'0')='0' THEN '0' ELSE '1' END) AS STA_INSTALL,
                    //  ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN, ISNULL(Shop_BillOut.STA_RETURNRECIVE,0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                    //                    ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,SPC_Department AS DPT_ID   ,Bill_INVENTCOSTCENTERID AS ISA


                    //    FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 

                    //                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                    //                         LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                    //                         LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 
                    //                         LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum  = TMPCOM.EMP_ID 
                    //                         LEFT OUTER JOIN  ( SELECT	[SHOW_ID]   FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)  WHERE	[TYPE_CONFIG] = '14' AND [STA] = '1' )TMP_COST
                    //                         ON INVENTJOURNALTABLE.SPC_INVENTCOSTCENTERID = TMP_COST.[SHOW_ID]

                    //    WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                    //                         AND JournalId NOT LIKE 'MNRS%' AND JournalId NOT LIKE 'MNRR%' 
                    //                         AND [JOURNALTYPE] ! = '4'  AND INVENTJOURNALTABLE.JournalNameId != 'IM-MM-PT'   
                    //                AND SPC_DIMENSION LIKE '{pCondition}' 
                    //            AND  CONVERT(VARCHAR, SPC_TransDate, 23) BETWEEN '{pDate1}'
                    //                AND '{pDate2}'  ";
                    //if (pIS != "")
                    //{
                    //    sql += $@" AND SPC_INVENTCOSTCENTERID = '{pIS}' ";
                    //}
                    //sql += " ORDER BY SPC_DIMENSION,JournalId   ";
                    break;

                case "FAL":
                    sql = BillOutClass.FindBillForReport_ByTypeBill(pTypeBill, pDate1, pDate2, pCondition, pIS);
                    //sql = $@"
                    //        SELECT	SPC_AssetLendingTable.Dimension AS BRANCH_ID,
                    //                  SPC_AssetLendingTable.Dimension+'-'+CASE WHEN ISNULL(Shop_Branch.BRANCH_NAME, '') = '' THEN B_NAME.DESCRIPTION ELSE Shop_Branch.BRANCH_NAME END AS BRANCH_NAME,
                    //                        SPC_AssetLendingTable.SPC_ASSETLENDINGID AS DOCNO,CONVERT(VARCHAR,LendingDate,23) AS DATE,
                    //                  SPC_AssetLendingTable.EmplId+'-'+ISNULL(SPC_NAME, 'ดูแลระบบมินิมาร์ท') AS InsName,
                    //                  ISNULL(Bill_TypeBchStatus,'0') AS Bill_TypeBchStatus,
                    //                  CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
                    //                  CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
                    //                  CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
                    //                 SPC_Remarks AS  REMARK,ISNULL(Bill_SpcType,0) AS Bill_SpcType,ISNULL(Bill_StaDoc,0) AS STADOC,
                    //      CASE WHEN SUBSTRING(SPC_AssetLendingTable.Dimension ,1,1) = 'M' THEN '1' ELSE '0' END AS STA_RECIVE,
                    //      '1' AS STA_INSTALL,ISNULL(STA_RETURN, 0) AS STA_RETURN,ISNULL(STA_RETURNRECIVE, 0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                    //                         ISNULL(TMPCOM.EMP_ID, 0) AS ComMN    ,DIMENSIONS.NUM AS DPT_ID  ,Bill_INVENTCOSTCENTERID AS ISA

                    //        from SHOP2013TMP.dbo.SPC_AssetLendingTable with (NOLOCK)
                    //                     LEFT OUTER JOIN  Shop_Branch WITH(NOLOCK) ON SPC_AssetLendingTable.Dimension = Shop_Branch.BRANCH_ID
                    //                     LEFT OUTER JOIN    SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON SPC_AssetLendingTable.EMPLID = EMPLTABLE.EMPLID   AND EMPLTABLE.DATAAREAID = 'SPC'
                    //                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH(NOLOCK) ON SPC_AssetLendingTable.Dimension = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                    //                     LEFT OUTER JOIN    Shop_BillOut WITH(NOLOCK) ON SPC_AssetLendingTable.SPC_ASSETLENDINGID = Shop_BillOut.Bill_ID
                    //                     LEFT OUTER JOIN(SELECT EMP_ID  FROM Shop_Employee WITH(NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON EMPLTABLE.AltNum = TMPCOM.EMP_ID
                    //                     INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 

                    //        WHERE  SPC_AssetLendingTable.DATAAREAID = 'SPC'
                    //                     AND CONVERT(VARCHAR, LendingDate, 23) BETWEEN '{pDate1}' AND '{pDate2}'
                    //      AND SPC_AssetLendingTable.Dimension LIKE '{pCondition}'  

                    //        ORDER BY SPC_AssetLendingTable.Dimension, SPC_AssetLendingTable.SPC_ASSETLENDINGID ";
                    break;

                case "MNRG":
                    sql = $@" SELECT	BranchID AS BRANCH_ID,BranchID+'-'+BranchName AS BRANCH_NAME,DocNo,CONVERT(VARCHAR,DATE,23) AS DATE,
		                UserID+'-'+UserName AS InsName,
                        '' AS Barcode,NameProduct,Qty,'ชุด' AS Unit,
		                ISNULL(Bill_ReciveSta,'0') AS Bill_TypeBchStatus,
		                CASE ISNULL(Bill_ReciveSta,'0') WHEN   '1' THEN 'ส่งของเรียบร้อยแล้ว' ELSE 	CASE ISNULL([Bill_TypeCNRecive],0) WHEN '1' THEN 'CN รับของแล้ว' ELSE 	CASE ISNULL(ShipSta,'0') WHEN '1'  THEN 'ของยังไม่ถึง CN' ELSE  'สาขายังไม่ส่งของ' END	END END   AS BchRemark,
		                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BchDate,
		                CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+char(10)+Bill_ReciveWhoName ELSE '' END AS BchReciveNAME,
		                CASE ShipSta WHEN '1' THEN ShipCarID+CHAR(10)+ SENTTONAME + ' / ' +REMARK ELSE  SENTTONAME + ' / ' +REMARK END  AS REMARK,Bill_SpcType,
						CASE STADOC WHEN '3' THEN '0'   WHEN '0' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  ,BranchID AS DPT_ID,'MNRG' AS ISA
                FROM	SHOP_MNRG WITH (NOLOCK)  
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN  '{pDate1}' AND '{pDate2}' AND BranchID LIKE '{pCondition}'  ORDER BY BranchID,DOCNO ";
                    break;

                case "MNRB":
                    sql = $@" SELECT	BranchID AS BRANCH_ID,BranchID+'-'+BranchName AS BRANCH_NAME,DocNo,CONVERT(VARCHAR,DATE,23) AS DATE,
		                UserID+'-'+UserName AS InsName,
		                ISNULL(Bill_ReciveSta,'0') AS Bill_TypeBchStatus,
		                CASE ISNULL(Bill_ReciveSta,'0') WHEN   '1' THEN 'ส่งของเรียบร้อยแล้ว' ELSE 	CASE ISNULL([Bill_TypeCNRecive],0) WHEN '1' THEN 'CN รับของแล้ว' ELSE 	CASE ISNULL(ShipSta,'0') WHEN '1'  THEN 'ของยังไม่ถึง CN' ELSE  'สาขายังไม่ส่งของ' END	END END  AS BchRemark,
		                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BchDate,
		                CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+char(10)+Bill_ReciveWhoName ELSE '' END AS BchReciveNAME,
		               CASE ShipSta WHEN '1' THEN ShipCarID+CHAR(10)+ SENTTONAME + ' / ' +REMARK ELSE  SENTTONAME + ' / ' +REMARK END  AS REMARK,Bill_SpcType,
						CASE STADOC WHEN '3' THEN '0'  WHEN '0' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  , BranchID AS DPT_ID  ,'MNRB' AS ISA 
                FROM	SHOP_MNRB WITH (NOLOCK)  
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN  '{pDate1}' AND '{pDate2}' AND BranchID LIKE '{pCondition}'  
                ORDER BY BranchID,DOCNO ";
                    break;

                case "MNRH":
                    sql = $@" SELECT	BranchID AS BRANCH_ID,BranchID+'-'+BranchName AS BRANCH_NAME,DocNo,CONVERT(VARCHAR,DATE,23) AS DATE,
		                UserID+'-'+UserName AS InsName,
		                ISNULL(Bill_ReciveSta,'0') AS Bill_TypeBchStatus,
		                CASE ISNULL(Bill_ReciveSta,'0') WHEN   '1' THEN 'ส่งของเรียบร้อยแล้ว' ELSE 	CASE ISNULL([Bill_TypeCNRecive],0) WHEN '1' THEN 'CN รับของแล้ว' ELSE 	CASE ISNULL(ShipSta,'0') WHEN '1'  THEN 'ของยังไม่ถึง CN' ELSE  'สาขายังไม่ส่งของ' END	END END  AS BchRemark,
		                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BchDate,
		                CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+char(10)+Bill_ReciveWhoName ELSE '' END AS BchReciveNAME,
		                CASE ShipSta WHEN '1' THEN ShipCarID+CHAR(10)+ 'แผนก D004 - แผนกข้อมูลคอมพิวเตอร์ และสถิติ' + ' / ส่งคืน' ELSE  'แผนก D004 - แผนกข้อมูลคอมพิวเตอร์ และสถิติ' + ' / ส่งคืน' END  AS REMARK,Bill_SpcType,
						CASE STADOC WHEN '3' THEN '0'  WHEN '0' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  ,BranchID AS DPT_ID  ,'MNRH' AS ISA
                FROM	SHOP_MNRH_HD WITH (NOLOCK)  
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN  '{pDate1}' AND '{pDate2}' AND BranchID LIKE '{pCondition}'  
                ORDER BY BranchID,DOCNO ";
                    break;

                case "MNRZ":
                    sql = $@"  SELECT	DPT_ID AS BRANCH_ID,DPT_ID+'-'+DPT_NAME AS BRANCH_NAME,DocNo,CONVERT(VARCHAR,DATE,23) AS DATE,
		                WHOINS+'-'+WHONAMEINS AS InsName,
		               '1' AS Bill_TypeBchStatus,
		               '' AS BchRemark,'' AS BchDate,
		                '' AS BchReciveNAME,''  AS REMARK,Bill_SpcType,
						CASE STADOC WHEN '3' THEN '0'  WHEN '0' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        ISNULL(TMPCOM.EMP_ID, 0) AS ComMN ,DPT_ID   AS DPT_ID  ,'MNRZ' AS ISA
                FROM	SHOP_MNRZ_HD WITH (NOLOCK)  
                         LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON SHOP_MNRZ_HD.WHOINS  = TMPCOM.EMP_ID 
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN   '{pDate1}' AND '{pDate2}' AND DPT_ID LIKE '{pCondition}'  ORDER BY DPT_ID,DOCNO ";
                    break;

                case "MNRR":
                    sql = BillOutClass.FindBillForReport_ByTypeBill(pTypeBill, pDate1, pDate2, pCondition, pIS);
                    //              sql = $@"  SELECT	MNRRBranch AS BRANCH_ID,
                    //                  MNRRBranchTO+'-'+ CASE WHEN ISNULL(SHOP_BRANCH.BRANCH_NAME,'')='' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END  AS BRANCH_NAME,
                    //                  MNRRDocNo AS DocNo,CONVERT(VARCHAR,MNRRDate,23) AS DATE,
                    //            MNRRUserCode+'-'+MNRRNAMECH AS InsName,
                    //           '1' AS Bill_TypeBchStatus,
                    //            ''  AS BchRemark,
                    //            CONVERT(VARCHAR,MNRRDate,23) AS BchDate,
                    //            MNRRWhoIn+char(10)+MNRRNAMECH  AS BchReciveNAME,
                    //            MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END  AS REMARK,Bill_SpcType,
                    //CASE MNRRStaDoc WHEN '3' THEN '0'  WHEN '0' THEN '0' ELSE '1' END AS STADOC,
                    //'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                    //                  ISNULL(TMPCOM.EMP_ID, 0) AS ComMN  ,MNRRBranch AS DPT_ID  ,'MNRR' AS ISA
                    //          FROM	Shop_MNRR_HD WITH (NOLOCK)  
                    //                   LEFT OUTER JOIN (SELECT	EMP_ID	FROM Shop_Employee WITH (NOLOCK) WHERE EMP_STACOMMN = '1')TMPCOM   ON Shop_MNRR_HD.MNRRUserCode  = TMPCOM.EMP_ID 
                    // LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  SHOP_BRANCH.BRANCH_ID
                    //		INNER JOIN SHOP_BRANCH WH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranch =  WH.BRANCH_ID
                    //                  INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' ANd DIMENSIONS.DIMENSIONCODE = '0'
                    //          WHERE   CONVERT(VARCHAR, MNRRDate, 23) BETWEEN   '{pDate1}'
                    //                      AND '{pDate2}' AND MNRRBranch LIKE '{pCondition}'  ORDER BY MNRRBranch,MNRRDocNo ";
                    break;

                case "MNPZ":
                    sql = $@"  SELECT	DIMENSION AS BRANCH_ID,
                        DIMENSION+'-'+ DIMENSIONNAME  AS BRANCH_NAME,
                        MNREWARDID AS DocNo,CONVERT(VARCHAR,DATEIN,23) AS DATE,
		                Bill_SpcWhoIn+'-'+Bill_SpcWhoName AS InsName,
		               '1' AS Bill_TypeBchStatus,
		                ''  AS BchRemark,
		                CONVERT(VARCHAR,DATEIN,23) AS BchDate,
		                WHOIN+char(10)+WHONAMEIN  AS BchReciveNAME,
		                REMARK  AS REMARK,Bill_SpcType,
						CASE StaDoc WHEN '3' THEN '0'  WHEN '0' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  , DIMENSION  AS DPT_ID,'MNPZ' AS ISA

                FROM	[SHOP_REWARDTABLE] WITH (NOLOCK)  
                         
                WHERE   CONVERT(VARCHAR, DATEIN, 23)  BETWEEN '{pDate1}' AND '{pDate2}' AND DIMENSION LIKE '{pCondition}'  ORDER BY DIMENSION,MNREWARDID ";
                    break;

                case "IDM":
                    sql = BillOutClass.FindBillForReport_ByTypeBill(pTypeBill, pDate1, pDate2, pCondition, pIS);
                    //sql = $@" SELECT	 SPC_DIMENSION AS BRANCH_ID,SPC_DIMENSION+'-'+ISNULL(B_NAME.DESCRIPTION,'') AS BRANCH_NAME,
                    //      JournalId AS DOCNO,CONVERT(VARCHAR,SPC_TransDate,23) AS DATE,
                    //              InventJournalTable.EMPLID+'-'+ISNULL(SPC_NAME,'ดูแลระบบมินิมาร์ท') AS InsName,
                    //              ISNULL(Bill_TypeBchStatus,0) AS Bill_TypeBchStatus,
                    //              CASE Bill_TypeBchRecive WHEN '1' THEN 'รับสินค้าครบ '+ Bill_TypeBchRemark WHEN '2' THEN 'รับขาด ' + Bill_TypeBchRemark ELSE Bill_TypeBchRemark END AS BchRemark,
                    //              CONVERT(VARCHAR,Bill_TypeBchDate,23) AS BchDate,
                    //              CASE Bill_TypeBchStatus WHEN '1' THEN Bill_TypeBchWhoID+char(10)+Bill_TypeBchWhoName ELSE '' END AS BchReciveNAME,
                    //              IVZ_Remarks AS REMARK,Bill_SpcType,Bill_StaDoc AS STADOC,
                    //  ISNULL(STA_RECIVE, CASE WHEN SUBSTRING(SPC_DIMENSION,1,1)='M' THEN '1' ELSE '0' END) AS STA_RECIVE,
                    //  '0' AS STA_INSTALL,
                    //  ISNULL(Shop_BillOut.STA_RETURN,0) AS STA_RETURN, ISNULL(Shop_BillOut.STA_RETURNRECIVE,0) AS STA_RETURNRECIVE,ISNULL(Shop_BillOut.Bill_ID,'0') AS STA_IMPORT,
                    //                    '0' AS ComMN   ,SPC_DIMENSION AS DPT_ID  ,'IDM' AS ISA


                    //    FROM	SHOP2013TMP.dbo.INVENTJOURNALTABLE WITH (NOLOCK) 

                    //                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS B_NAME WITH (NOLOCK) ON INVENTJOURNALTABLE.SPC_DIMENSION = B_NAME.NUM AND B_NAME.DATAAREAID = N'SPC' AND B_NAME.DIMENSIONCODE = '0'
                    //                         LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON INVENTJOURNALTABLE.EMPLID =  EMPLTABLE.EMPLID  AND EMPLTABLE.DATAAREAID = N'SPC' 
                    //                         LEFT OUTER JOIN Shop_BillOut WITH (NOLOCK) ON INVENTJOURNALTABLE.JOURNALID = Shop_BillOut.Bill_ID 


                    //    WHERE	INVENTJOURNALTABLE.DATAAREAID = 'SPC'   
                    //            AND     JOURNALNAMEID = 'IM-MM-IDM'    
                    //            AND  CONVERT(VARCHAR, SPC_TransDate, 23) BETWEEN '{pDate1}' AND '{pDate2}'  

                    //    ORDER BY SPC_DIMENSION,JournalId  ";
                    break;

                case "MNCM_1":
                    sql = $@"SELECT 'D050' AS BRANCH_ID, 'D050 แผนกแม่บ้าน' AS BRANCH_NAME,Claim_ID AS  DOCNO, CONVERT(VARCHAR, DATE_INS, 23) AS DATE,
                        WHOIDINS + '-' + WHONAMEINS AS InsName,
                        RECIVE_STA AS Bill_TypeBchStatus,
                        RECIVE_DESC AS BchRemark,
                        CASE WHEN RECIVE_STA = '1' THEN CONVERT(VARCHAR,RECIVE_DATE,23) ELSE '' END  AS BchDate,
                        CASE WHEN RECIVE_STA = '1' THEN RECIVE_WHOID+char(10)+RECIVE_WHONAME ELSE '' END AS BchReciveNAME,
                        DESCRIPTION AS REMARK, Bill_SpcType, STADOC,
                        '1'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  ,'D050' AS DPT_ID  ,'MNCM_1' AS ISA
                FROM    SHOP_JOBCLAIM WITH(NOLOCK)
                WHERE   CONVERT(VARCHAR, DATE_INS, 23) BETWEEN '{pDate1}' AND '{pDate2}'   AND STA_TYPE = '1'   ORDER BY Claim_ID ";
                    break;

                case "MNCM_0":
                    sql = $@"SELECT 'D035' AS BRANCH_ID, 'D035-แผนกรับคืนสินค้า C/N' AS BRANCH_NAME,Claim_ID AS  DOCNO, CONVERT(VARCHAR, DATE_INS, 23) AS DATE,
                        WHOIDINS + '-' + WHONAMEINS AS InsName,
                        RECIVE_STA AS Bill_TypeBchStatus,
                        RECIVE_DESC AS BchRemark,
                        CASE WHEN RECIVE_STA = '1' THEN CONVERT(VARCHAR,RECIVE_DATE,23) ELSE '' END  AS BchDate,
                        CASE WHEN RECIVE_STA = '1' THEN RECIVE_WHOID+char(10)+RECIVE_WHONAME ELSE '' END AS BchReciveNAME,
                        DESCRIPTION AS REMARK, Bill_SpcType, STADOC,
                        '1'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'1' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  , 'D035' AS DPT_ID  ,'MNCM_0' AS ISA 
                FROM    SHOP_JOBCLAIM WITH(NOLOCK)
                WHERE   CONVERT(VARCHAR, DATE_INS, 23) BETWEEN '{pDate1}' AND '{pDate2}'   AND STA_TYPE = '0'   ORDER BY Claim_ID ";
                    break;
                case "MNCM_2":
                    sql = $@"SELECT 'D035' AS BRANCH_ID, 'D035-แผนกรับคืนสินค้า C/N' AS BRANCH_NAME,Claim_ID AS  DOCNO, CONVERT(VARCHAR, DATE_INS, 23) AS DATE,
                        WHOIDINS + '-' + WHONAMEINS AS InsName,
                        RECIVE_STA AS Bill_TypeBchStatus,
                        RECIVE_DESC AS BchRemark,
                        CASE WHEN RECIVE_STA = '1' THEN CONVERT(VARCHAR,RECIVE_DATE,23) ELSE '' END  AS BchDate,
                        CASE WHEN RECIVE_STA = '1' THEN RECIVE_WHOID+char(10)+RECIVE_WHONAME ELSE '' END AS BchReciveNAME,
                        DESCRIPTION AS REMARK, Bill_SpcType, STADOC,
                        '1'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'1' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  , 'D035' AS DPT_ID  ,'MNCM_2' AS ISA 
                FROM    SHOP_JOBCLAIM WITH(NOLOCK)
                WHERE   CONVERT(VARCHAR, DATE_INS, 23) BETWEEN '{pDate1}' AND '{pDate2}'   AND STA_TYPE = '2'   ORDER BY Claim_ID ";
                    break;
                case "MNCM_3":
                    sql = $@"SELECT 'D035' AS BRANCH_ID, 'D035-แผนกรับคืนสินค้า C/N' AS BRANCH_NAME,Claim_ID AS  DOCNO, CONVERT(VARCHAR, DATE_INS, 23) AS DATE,
                        WHOIDINS + '-' + WHONAMEINS AS InsName,
                        RECIVE_STA AS Bill_TypeBchStatus,
                        RECIVE_DESC AS BchRemark,
                        CASE WHEN RECIVE_STA = '1' THEN CONVERT(VARCHAR,RECIVE_DATE,23) ELSE '' END  AS BchDate,
                        CASE WHEN RECIVE_STA = '1' THEN RECIVE_WHOID+char(10)+RECIVE_WHONAME ELSE '' END AS BchReciveNAME,
                        DESCRIPTION AS REMARK, Bill_SpcType, STADOC,
                        '1'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'1' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  , 'D035' AS DPT_ID  ,'MNCM_3' AS ISA 
                FROM    SHOP_JOBCLAIM WITH(NOLOCK)
                WHERE   CONVERT(VARCHAR, DATE_INS, 23) BETWEEN '{pDate1}' AND '{pDate2}'   AND STA_TYPE = '3'   ORDER BY Claim_ID ";
                    break;
                case "MNRD":
                    sql = BillOutClass.FindBillForReport_ByTypeBill(pTypeBill, pDate1, pDate2, pCondition, pIS);
                    //sql = $@"SELECT MNRDBRANCH AS BRANCH_ID, MNRDBRANCH + '-' + BRANCH_NAME AS BRANCH_NAME, MNRDDOCNO AS DOCNO, CONVERT(VARCHAR, MNRDDATE, 23) AS DATE,
                    //        MNRDUSERCODE + CHAR(10) + SPC_NAME AS InsName,
                    //        '1' AS Bill_TypeBchStatus,
                    //        ShipDoc+CHAR(10)+ MNRDSHID AS BchRemark,
                    //        '' AS BchDate,
                    //        '' AS BchReciveNAME,
                    //        CASE [TYPEPRODUCT] WHEN '0' THEN 'รถจัดส่ง' ELSE 'รถห้องเย็น' END  + CHAR(10) + MNRDREMARK AS REMARK,'1'  AS Bill_SpcType,
                    //        MNRDSTADOC AS STADOC,
                    //        '0'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                    //        '0' AS ComMN  ,MNRDBRANCH AS DPT_ID   ,'MNRD' AS ISA
                    //FROM    SHOP_MNRD_HD WITH(NOLOCK)
                    //  INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDBRANCH = SHOP_BRANCH.BRANCH_ID
                    //  INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNRD_HD.MNRDUSERCODE = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC'
                    //WHERE   CONVERT(VARCHAR, MNRDDATE, 23) BETWEEN '{pDate1}' AND '{pDate2}'  AND BRANCH_ID = '{pCondition}'  
                    //ORDER BY MNRDBRANCH,MNRDDOCNO ";
                    break;
                case "MNRM":
                    sql = $@"SELECT SentToBch AS BRANCH_ID, SentToBch + '-' + SentToBchName AS BRANCH_NAME, DOCNO, CONVERT(VARCHAR, DATE, 23) AS DATE,
                        UserID + '-' + UserName AS InsName,
                        Bill_ReciveSta AS Bill_TypeBchStatus,
                        CASE Bill_ReciveSta WHEN '1' THEN Bill_TypeBchRemark ELSE '' END AS BchRemark,
                        CONVERT(VARCHAR, Bill_ReciveDate, 23) AS BchDate,
                        CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn + char(10) + Bill_ReciveWhoName ELSE '' END AS BchReciveNAME,
                        REMARK, Bill_SpcType, STADOC,
                        '1'  AS STA_RECIVE,'0' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT,
                        '0' AS ComMN  ,BranchID AS DPT_ID   ,'MNRM' AS ISA
                FROM    SHOP_MNRM WITH(NOLOCK)
                WHERE   CONVERT(VARCHAR, DATE, 23) BETWEEN '{pDate1}' AND '{pDate2}'  AND BranchID LIKE '{pCondition}'  ORDER BY BranchID,DOCNO ";
                    break;
                default: break;
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Set Head For Report
        public static void SetHeadGrid_ForShowImageReport(RadGridView radGridView_Show, int pColume)//pColume จำนวนคอลัมที่แสดง 2-3-5 เท่านั้น เพิ่ม 6 เป็นส่งซ่อม MNCM
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE", "วันที่บิล", 120));

            switch (pColume)
            {
                case 1:
                    // S
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));

                    //Set Color
                    ExpressionFormattingObject sBill_SPC1 = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC1);
                    break;
                case 2:
                    // S
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
                    // N
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", "รับของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE")));

                    ////Set Color
                    //ExpressionFormattingObject sBill_SPC2 = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC2);
                    DatagridClass.SetCellBackClolorByExpression("Bill_SPC", "Bill_SPC_Count = '0' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RECIVE = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE);
                    DatagridClass.SetCellBackClolorByExpression("Bill_MN", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);
                    break;
                case 3:
                    // S
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
                    // N
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", "รับของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE")));
                    // E
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_END", "ติดตั้ง", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Count", "Bill_END_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Path", "Bill_END_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_INSTALL", "STA_INSTALL")));

                    ////Set Color
                    //ExpressionFormattingObject sBill_SPC3 = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC3);
                    DatagridClass.SetCellBackClolorByExpression("Bill_SPC", "Bill_SPC_Count = '0' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RECIVE3 = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE3);
                    DatagridClass.SetCellBackClolorByExpression("Bill_MN", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_INSTALL = new ExpressionFormattingObject("MyCondition1", "Bill_END_Count = '0' AND STA_INSTALL = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_END"].ConditionalFormattingObjectList.Add(sSTA_INSTALL);
                    DatagridClass.SetCellBackClolorByExpression("Bill_END", "Bill_END_Count = '0' AND STA_INSTALL = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);
                    break;
                case 5:
                    // S
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
                    // N
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", "รับของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE")));
                    // E
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_END", "ติดตั้ง", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Count", "Bill_END_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_END_Path", "Bill_END_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_INSTALL", "STA_INSTALL")));
                    // O
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_OLD", "ส่งซาก", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLD_Count", "Bill_OLD_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLD_Path", "Bill_OLD_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RETURN", "STA_RETURN")));
                    // R
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_OLDSPC", "รับซาก", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLDSPC_Count", "Bill_OLDSPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLDSPC_Path", "Bill_OLDSPC_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RETURNRECIVE", "STA_RETURNRECIVE")));

                    ////Set Color
                    //ExpressionFormattingObject sBill_SPC5 = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC5);
                    DatagridClass.SetCellBackClolorByExpression("Bill_SPC", "Bill_SPC_Count = '0' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RECIVE5 = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE5);
                    DatagridClass.SetCellBackClolorByExpression("Bill_MN", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_INSTALLA = new ExpressionFormattingObject("MyCondition1", "Bill_END_Count = '0' AND STA_INSTALL = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_END"].ConditionalFormattingObjectList.Add(sSTA_INSTALLA);
                    DatagridClass.SetCellBackClolorByExpression("Bill_END", "Bill_END_Count = '0' AND STA_INSTALL = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RETURN = new ExpressionFormattingObject("MyCondition1", "Bill_OLD_Count = '0' AND STA_RETURN = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_OLD"].ConditionalFormattingObjectList.Add(sSTA_RETURN);
                    DatagridClass.SetCellBackClolorByExpression("Bill_OLD", "Bill_OLD_Count = '0' AND STA_RETURN = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RETURNRECIVE = new ExpressionFormattingObject("MyCondition1", " Bill_OLDSPC_Count = '0' AND STA_RETURNRECIVE = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_OLDSPC"].ConditionalFormattingObjectList.Add(sSTA_RETURNRECIVE);
                    DatagridClass.SetCellBackClolorByExpression("Bill_OLDSPC", " Bill_OLDSPC_Count = '0' AND STA_RETURNRECIVE = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);
                    break;
                case 6:
                    // S
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_SPC", "ส่งของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Count", "Bill_SPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_SPC_Path", "Bill_SPC_Path")));
                    // N
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_MN", "รับของ", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Count", "Bill_MN_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_MN_Path", "Bill_MN_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RECIVE", "STA_RECIVE")));

                    // R
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Bill_OLDSPC", "รับคืน", 80)));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLDSPC_Count", "Bill_OLDSPC_Count")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Bill_OLDSPC_Path", "Bill_OLDSPC_Path")));
                    radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_RETURNRECIVE", "STA_RETURNRECIVE")));

                    //Set Color
                    //ExpressionFormattingObject sBill_SPC6 = new ExpressionFormattingObject("MyCondition1", "Bill_SPC_Count = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_SPC"].ConditionalFormattingObjectList.Add(sBill_SPC6);
                    DatagridClass.SetCellBackClolorByExpression("Bill_SPC", "Bill_SPC_Count = '0' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RECIVE6 = new ExpressionFormattingObject("MyCondition1", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_MN"].ConditionalFormattingObjectList.Add(sSTA_RECIVE6);
                    DatagridClass.SetCellBackClolorByExpression("Bill_MN", "Bill_MN_Count = '0' AND STA_RECIVE = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    //ExpressionFormattingObject sSTA_RETURNRECIVE6 = new ExpressionFormattingObject("MyCondition1", " Bill_OLDSPC_Count = '0' AND STA_RETURNRECIVE = '1' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //radGridView_Show.Columns["Bill_OLDSPC"].ConditionalFormattingObjectList.Add(sSTA_RETURNRECIVE6);
                    DatagridClass.SetCellBackClolorByExpression("Bill_OLDSPC", " Bill_OLDSPC_Count = '0' AND STA_RETURNRECIVE = '1' ", ConfigClass.SetColor_Red(), radGridView_Show);

                    break;
                default:
                    break;
            }

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Bill_SpcType", "ตรวจ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ISA", "งบเบิก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("InsName", "ผู้ทำบิล", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Bill_TypeBchStatus", "สาขารับ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BchRemark", "หมายเหตุรับ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BchReciveNAME", "ผู้รับ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BchDate", "วันที่รับ", 120));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STADOC", "ยกเลิก")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_IMPORT", "IMPORT")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ComMN", "ComMN")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DPT_ID", "DPT_ID")));


            //ExpressionFormattingObject sBill_SpcType = new ExpressionFormattingObject("MyCondition1", "Bill_SpcType = '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //radGridView_Show.Columns["Bill_SpcType"].ConditionalFormattingObjectList.Add(sBill_SpcType);
            DatagridClass.SetCellBackClolorByExpression("Bill_SpcType", "Bill_SpcType = '0'   ", ConfigClass.SetColor_Red(), radGridView_Show);

            //ExpressionFormattingObject sBill_TypeBchStatus = new ExpressionFormattingObject("MyCondition1", "Bill_TypeBchStatus = '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //radGridView_Show.Columns["Bill_TypeBchStatus"].ConditionalFormattingObjectList.Add(sBill_TypeBchStatus);
            DatagridClass.SetCellBackClolorByExpression("Bill_TypeBchStatus", "Bill_TypeBchStatus = '0'   ", ConfigClass.SetColor_Red(), radGridView_Show);

            //ExpressionFormattingObject sSTA_IMPORT = new ExpressionFormattingObject("MyCondition1", "STA_IMPORT = '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sSTA_IMPORT);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", "STA_IMPORT = '0'   ", ConfigClass.SetColor_Red(), radGridView_Show);

            //ExpressionFormattingObject sComMN = new ExpressionFormattingObject("MyCondition1", "ComMN <> '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_SkyPastel() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sComMN);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", "ComMN <> '0'   ", ConfigClass.SetColor_SkyPastel(), radGridView_Show);

            //ExpressionFormattingObject sComService = new ExpressionFormattingObject("MyCondition1", " ComMN = '0'  AND  DPT_ID = 'D179' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PinkPastel() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sComService);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", " ComMN = '0'  AND  DPT_ID = 'D179' ", ConfigClass.SetColor_PinkPastel(), radGridView_Show);

            //// บิลยกเลิก
            //ExpressionFormattingObject sSTADOC = new ExpressionFormattingObject("MyCondition1", "STADOC = '0'    ", false)
            //{ CellBackColor = ConfigClass.SetColor_GrayPastel() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sSTADOC);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", "STADOC = '0'    ", ConfigClass.SetColor_GrayPastel(), radGridView_Show);

            radGridView_Show.TableElement.RowHeight = 110;
            //radGridView_Show.MasterTemplate.EnableFiltering = false;//การกรองที่หัวกริด
            radGridView_Show.MasterTemplate.AllowRowResize = false;
            radGridView_Show.MasterTemplate.AllowColumnResize = false;

            radGridView_Show.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
            radGridView_Show.MasterTemplate.Columns["DOCNO"].IsPinned = true;
            radGridView_Show.MasterTemplate.Columns["DATE"].IsPinned = true;
        }

        //Set Head For Report
        public static void SetHeadGrid_ForShowtextReport(RadGridView radGridView_Show)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE", "วันที่บิล", 120));

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NameProduct", "ชื่อสินค้า", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Unit", "หน่วย", 80));

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Bill_SpcType", "ตรวจ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ISA", "งบเบิก", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("InsName", "ผู้ทำบิล", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Bill_TypeBchStatus", "สาขารับ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BchRemark", "หมายเหตุรับ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BchReciveNAME", "ผู้รับ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BchDate", "วันที่รับ", 120));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STADOC", "ยกเลิก")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_IMPORT", "IMPORT")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ComMN", "ComMN")));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DPT_ID", "DPT_ID")));


            //ExpressionFormattingObject sBill_SpcType = new ExpressionFormattingObject("MyCondition1", "Bill_SpcType = '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //radGridView_Show.Columns["Bill_SpcType"].ConditionalFormattingObjectList.Add(sBill_SpcType);
            DatagridClass.SetCellBackClolorByExpression("Bill_SpcType", "Bill_SpcType = '0'   ", ConfigClass.SetColor_Red(), radGridView_Show);

            //ExpressionFormattingObject sBill_TypeBchStatus = new ExpressionFormattingObject("MyCondition1", "Bill_TypeBchStatus = '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //radGridView_Show.Columns["Bill_TypeBchStatus"].ConditionalFormattingObjectList.Add(sBill_TypeBchStatus);
            DatagridClass.SetCellBackClolorByExpression("Bill_TypeBchStatus", "Bill_TypeBchStatus = '0'   ", ConfigClass.SetColor_Red(), radGridView_Show);

            //ExpressionFormattingObject sSTA_IMPORT = new ExpressionFormattingObject("MyCondition1", "STA_IMPORT = '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_Red() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sSTA_IMPORT);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", "STA_IMPORT = '0'   ", ConfigClass.SetColor_Red(), radGridView_Show);

            //ExpressionFormattingObject sComMN = new ExpressionFormattingObject("MyCondition1", "ComMN <> '0'   ", false)
            //{ CellBackColor = ConfigClass.SetColor_SkyPastel() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sComMN);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", "ComMN <> '0'   ", ConfigClass.SetColor_SkyPastel(), radGridView_Show);

            //ExpressionFormattingObject sComService = new ExpressionFormattingObject("MyCondition1", " ComMN = '0'  AND  DPT_ID = 'D179' ", false)
            //{ CellBackColor = ConfigClass.SetColor_PinkPastel() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sComService);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", " ComMN = '0'  AND  DPT_ID = 'D179' ", ConfigClass.SetColor_PinkPastel(), radGridView_Show);

            // บิลยกเลิก
            //ExpressionFormattingObject sSTADOC = new ExpressionFormattingObject("MyCondition1", "STADOC = '0'    ", false)
            //{ CellBackColor = ConfigClass.SetColor_GrayPastel() };
            //radGridView_Show.Columns["DOCNO"].ConditionalFormattingObjectList.Add(sSTADOC);
            DatagridClass.SetCellBackClolorByExpression("DOCNO", "STADOC = '0'    ", ConfigClass.SetColor_GrayPastel(), radGridView_Show);

            radGridView_Show.TableElement.RowHeight = 50;
            radGridView_Show.MasterTemplate.AllowRowResize = false;
            radGridView_Show.MasterTemplate.AllowColumnResize = false;

            radGridView_Show.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
            radGridView_Show.MasterTemplate.Columns["DOCNO"].IsPinned = true;
            radGridView_Show.MasterTemplate.Columns["DATE"].IsPinned = true;

        }

        //Set Grid รูปตาม JOB
        public static void FindImageBill_ForShowImageReport(DataTable dt, RadGridView radGridView_Show, int pColume) //6 คืองานส่งซ่อม MNCM
        {

            radGridView_Show.DataSource = dt;
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                System.String findDpt = dt.Rows[i]["BRANCH_ID"].ToString();
                if (findDpt.StartsWith("D")) findDpt = "MN000";

                //ที่เก็บบิล
                string pPathMain = PathImageClass.pPathBillOut;
                if (dt.Rows[i]["DOCNO"].ToString().Substring(0, 3) == "IDM") pPathMain = PathImageClass.pPathBillOutIDM;
                else if (dt.Rows[i]["DOCNO"].ToString().Substring(0, 4) == "MNCM") pPathMain = PathImageClass.pPathAssetClaimLeave;
                else if (dt.Rows[i]["DOCNO"].ToString().Substring(0, 4) == "MNRD") pPathMain = PathImageClass.pPathCN;

                string bill_Path = pPathMain + @"\" + dt.Rows[i]["DATE"].ToString() + @"\" + findDpt;

                string bill_Find = "*_" + dt.Rows[i]["DOCNO"].ToString();
                DirectoryInfo DirInfo = new DirectoryInfo(bill_Path);

                switch (pColume)
                {
                    case 1:
                        //ขาส่งของ S
                        try
                        {
                            string findStr = "_S";
                            if (dt.Rows[i]["DOCNO"].ToString().Substring(0, 4) == "MNRD") findStr = "";

                            FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + findStr + @".*", SearchOption.AllDirectories);
                            string fullNameSPC = PathImageClass.pImageEmply;
                            if (FilesSPC.Length > 0) fullNameSPC = FilesSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + findStr + @".*";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                        }
                        break;
                    case 2:
                        //ขาส่งของ S
                        try
                        {
                            FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + "_S.*", SearchOption.AllDirectories);
                            string fullNameSPC = PathImageClass.pImageEmply;
                            if (FilesSPC.Length > 0) fullNameSPC = FilesSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + "_S.*";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                        }
                        //ขารับของ N
                        try
                        {
                            FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + "_N.*", SearchOption.AllDirectories);
                            string fullNameMN = PathImageClass.pImageEmply;
                            if (FilesMN.Length > 0) fullNameMN = FilesMN[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + "_N.*";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = FilesMN.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = "0";
                        }
                        break;
                    case 3:
                        //ขาส่งของ S
                        try
                        {
                            FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + "_S.*", SearchOption.AllDirectories);
                            string fullNameSPC = PathImageClass.pImageEmply;
                            if (FilesSPC.Length > 0) fullNameSPC = FilesSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + "_S.*";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                        }
                        //ขารับของ N
                        try
                        {
                            FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + "_N.*", SearchOption.AllDirectories);
                            string fullNameMN = PathImageClass.pImageEmply;
                            if (FilesMN.Length > 0) fullNameMN = FilesMN[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + "_N.*";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = FilesMN.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = "0";
                        }
                        //ขารับของ E
                        try
                        {
                            FileInfo[] FilesEND = DirInfo.GetFiles(bill_Find + "_E.*", SearchOption.AllDirectories);
                            string fullNameEND = PathImageClass.pImageEmply;
                            if (FilesEND.Length > 0) fullNameEND = FilesEND[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameEND);
                            radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = bill_Path + @"|" + bill_Find + "_E.*";
                            radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = FilesEND.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = "0";
                        }
                        break;
                    case 5:
                        //ขาส่งของ S
                        try
                        {
                            FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + "_S.*", SearchOption.AllDirectories);
                            string fullNameSPC = PathImageClass.pImageEmply;
                            if (FilesSPC.Length > 0) fullNameSPC = FilesSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + "_S.*";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                        }
                        //ขารับของ N
                        try
                        {
                            FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + "_N.*", SearchOption.AllDirectories);
                            string fullNameMN = PathImageClass.pImageEmply;
                            if (FilesMN.Length > 0) fullNameMN = FilesMN[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + "_N.*";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = FilesMN.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = "0";
                        }
                        //ขารับของ E
                        try
                        {
                            FileInfo[] FilesEND = DirInfo.GetFiles(bill_Find + "_E.*", SearchOption.AllDirectories);
                            string fullNameEND = PathImageClass.pImageEmply;
                            if (FilesEND.Length > 0) fullNameEND = FilesEND[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameEND);
                            radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = bill_Path + @"|" + bill_Find + "_E.*";
                            radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = FilesEND.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_END"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_END_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_END_Count"].Value = "0";
                        }
                        //ขารับของ O
                        try
                        {
                            FileInfo[] FilesOLD = DirInfo.GetFiles(bill_Find + "_O.*", SearchOption.AllDirectories);
                            string fullNameOLD = PathImageClass.pImageEmply;
                            if (FilesOLD.Length > 0) fullNameOLD = FilesOLD[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_OLD"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameOLD);
                            radGridView_Show.Rows[i].Cells["Bill_OLD_Path"].Value = bill_Path + @"|" + bill_Find + "_O.*";
                            radGridView_Show.Rows[i].Cells["Bill_OLD_Count"].Value = FilesOLD.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_OLD"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_OLD_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_OLD_Count"].Value = "0";
                        }
                        //ขารับของ R
                        try
                        {
                            FileInfo[] FilesOLDSPC = DirInfo.GetFiles(bill_Find + "_R.*", SearchOption.AllDirectories);
                            string fullNameOLDSPC = PathImageClass.pImageEmply;
                            if (FilesOLDSPC.Length > 0) fullNameOLDSPC = FilesOLDSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameOLDSPC);
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Path"].Value = bill_Path + @"|" + bill_Find + "_R.*";
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Count"].Value = FilesOLDSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Count"].Value = "0";
                        }
                        break;
                    case 6:
                        //ขาส่งของ S
                        try
                        {
                            FileInfo[] FilesSPC = DirInfo.GetFiles(bill_Find + "_S.*", SearchOption.AllDirectories);
                            string fullNameSPC = PathImageClass.pImageEmply;
                            if (FilesSPC.Length > 0) fullNameSPC = FilesSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameSPC);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = bill_Path + @"|" + bill_Find + "_S.*";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = FilesSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_SPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_SPC_Count"].Value = "0";
                        }
                        //ขารับของ N
                        try
                        {
                            FileInfo[] FilesMN = DirInfo.GetFiles(bill_Find + "_C.*", SearchOption.AllDirectories);
                            string fullNameMN = PathImageClass.pImageEmply;
                            if (FilesMN.Length > 0) fullNameMN = FilesMN[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameMN);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = bill_Path + @"|" + bill_Find + "_C.*";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = FilesMN.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_MN"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_MN_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_MN_Count"].Value = "0";
                        }
                        //ขารับของ R
                        try
                        {
                            FileInfo[] FilesOLDSPC = DirInfo.GetFiles(bill_Find + "_R.*", SearchOption.AllDirectories);
                            string fullNameOLDSPC = PathImageClass.pImageEmply;
                            if (FilesOLDSPC.Length > 0) fullNameOLDSPC = FilesOLDSPC[0].FullName;

                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(fullNameOLDSPC);
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Path"].Value = bill_Path + @"|" + bill_Find + "_R.*";
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Count"].Value = FilesOLDSPC.Length;
                        }
                        catch (Exception)
                        {
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Path"].Value = "";
                            radGridView_Show.Rows[i].Cells["Bill_OLDSPC_Count"].Value = "0";
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //ค้นบิลส่งคืนของสาขา RB-RG เพื่อ ADD JOB
        public static DataTable FindBill_ForAddJOB(string pDpt, string pBch)
        {
            string sql = $@"SELECT	BranchID AS BRANCH_ID,BranchID+'-'+BranchName AS BRANCH_NAME,DocNo,CONVERT(VARCHAR,DATE,23) AS DATE,
		                UserID+'-'+UserName AS InsName,
		                ISNULL(Bill_ReciveSta,'0') AS Bill_TypeBchStatus,
		                CASE ISNULL(Bill_ReciveSta,'0') WHEN '1' THEN 'CN รับสินค้าแล้ว ' WHEN '0' THEN 'CN ยังไม่ได้รับสินค้า '  END AS BchRemark,
		                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BchDate,
		                CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+'-'+Bill_ReciveWhoName ELSE '' END AS BchReciveNAME,
		                SENTTONAME + ' / ' +REMARK  AS REMARK,Bill_SpcType,
						CASE STADOC WHEN '3' THEN '0' ELSE '1' END AS STADOC,
						'1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT
                FROM	SHOP_MNRB WITH (NOLOCK)  
                        
                WHERE   StaDoc = '1'  AND ISNULL(Bill_JOB,'')=''  AND SentTo IN ({pDpt})
                        AND CONVERT(VARCHAR, DATE, 23) BETWEEN GETDATE()-30 AND GETDATE()
                        AND BranchID LIKE '{pBch}'
                UNION

                SELECT	BranchID AS BRANCH_ID,BranchID+'-'+BranchName AS BRANCH_NAME,DocNo,CONVERT(VARCHAR,DATE,23) AS DATE,
		                                UserID+'-'+UserName AS InsName,
		                                ISNULL(Bill_ReciveSta,'0') AS Bill_TypeBchStatus,
		                                CASE ISNULL(Bill_ReciveSta,'0') WHEN '1' THEN 'CN รับสินค้าแล้ว ' WHEN '0' THEN 'CN ยังไม่ได้รับสินค้า '  END AS BchRemark,
		                                CONVERT(VARCHAR,Bill_ReciveDate,23) AS BchDate,
		                                CASE Bill_ReciveSta WHEN '1' THEN Bill_ReciveWhoIn+'-'+Bill_ReciveWhoName ELSE '' END AS BchReciveNAME,
		                                SENTTONAME + ' / ' +REMARK  AS REMARK,Bill_SpcType,
						                CASE STADOC WHEN '3' THEN '0' ELSE '1' END AS STADOC,
						                '1' AS STA_RECIVE,'1' AS STA_INSTALL,'0' AS STA_RETURN,'0' AS STA_RETURNRECIVE,'1' AS STA_IMPORT
                        
                FROM	SHOP_MNRG WITH (NOLOCK)  
                         
                WHERE     StaDoc = '1' AND ISNULL(Bill_JOB,'')='' 
						                AND SentTo IN ({pDpt})
						                AND CONVERT(VARCHAR, DATE, 23) BETWEEN GETDATE()-30 AND GETDATE()
                                        AND BranchID LIKE '{pBch}'
                ORDER BY 		BRANCH_ID,DOCNO	  ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        
    }
}
