﻿namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    partial class Bill_RA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bill_RA));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.RadDateTimePicker_End = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadDateTimePicker_Begin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator10 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Refresh = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator8 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator9 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radGridView_Image = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_End)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(861, 611);
            this.radPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(861, 611);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.RadDateTimePicker_End);
            this.panel1.Controls.Add(this.RadDateTimePicker_Begin);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(664, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 605);
            this.panel1.TabIndex = 2;
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(11, 55);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Date.TabIndex = 60;
            this.radLabel_Date.Text = "ระบุวันที่";
            // 
            // RadDateTimePicker_End
            // 
            this.RadDateTimePicker_End.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_End.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_End.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_End.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_End.Location = new System.Drawing.Point(11, 106);
            this.RadDateTimePicker_End.Name = "RadDateTimePicker_End";
            this.RadDateTimePicker_End.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_End.TabIndex = 59;
            this.RadDateTimePicker_End.TabStop = false;
            this.RadDateTimePicker_End.Text = "07/08/2022";
            this.RadDateTimePicker_End.Value = new System.DateTime(2022, 8, 7, 0, 0, 0, 0);
            this.RadDateTimePicker_End.ValueChanged += new System.EventHandler(this.RadDateTimePicker_End_ValueChanged);
            // 
            // RadDateTimePicker_Begin
            // 
            this.RadDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_Begin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_Begin.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_Begin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_Begin.Location = new System.Drawing.Point(11, 79);
            this.RadDateTimePicker_Begin.Name = "RadDateTimePicker_Begin";
            this.RadDateTimePicker_Begin.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_Begin.TabIndex = 58;
            this.RadDateTimePicker_Begin.TabStop = false;
            this.RadDateTimePicker_Begin.Text = "21/12/2022";
            this.RadDateTimePicker_Begin.Value = new System.DateTime(2022, 12, 21, 0, 0, 0, 0);
            this.RadDateTimePicker_Begin.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Begin_ValueChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator10,
            this.radButtonElement_Refresh,
            this.commandBarSeparator7,
            this.radButtonElement_print,
            this.commandBarSeparator8,
            this.radButtonElement_pdf,
            this.commandBarSeparator9});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 36);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator10
            // 
            this.commandBarSeparator10.Name = "commandBarSeparator10";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator10, false);
            this.commandBarSeparator10.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Refresh
            // 
            this.radButtonElement_Refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButtonElement_Refresh.Name = "radButtonElement_Refresh";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Refresh, false);
            this.radButtonElement_Refresh.Text = "radButtonElement2";
            this.radButtonElement_Refresh.Click += new System.EventHandler(this.RadButtonElement_Refresh_Click);
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator7, false);
            this.commandBarSeparator7.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator7.UseCompatibleTextRendering = false;
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_print
            // 
            this.radButtonElement_print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_print.Name = "radButtonElement_print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_print, false);
            this.radButtonElement_print.Text = "radButtonElement1";
            this.radButtonElement_print.UseCompatibleTextRendering = false;
            this.radButtonElement_print.Click += new System.EventHandler(this.RadButtonElement_print_Click);
            // 
            // commandBarSeparator8
            // 
            this.commandBarSeparator8.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator8.Name = "commandBarSeparator8";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator8, false);
            this.commandBarSeparator8.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator8.UseCompatibleTextRendering = false;
            this.commandBarSeparator8.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_pdf
            // 
            this.radButtonElement_pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_pdf.Name = "radButtonElement_pdf";
            this.radStatusStrip1.SetSpring(this.radButtonElement_pdf, false);
            this.radButtonElement_pdf.Text = "radButtonElement1";
            this.radButtonElement_pdf.UseCompatibleTextRendering = false;
            this.radButtonElement_pdf.Click += new System.EventHandler(this.RadButtonElement_pdf_Click);
            // 
            // commandBarSeparator9
            // 
            this.commandBarSeparator9.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator9.Name = "commandBarSeparator9";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator9, false);
            this.commandBarSeparator9.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator9.UseCompatibleTextRendering = false;
            this.commandBarSeparator9.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(11, 154);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 0;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Image, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(655, 605);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radGridView_Image
            // 
            this.radGridView_Image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Image.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Image.Location = new System.Drawing.Point(3, 206);
            // 
            // 
            // 
            this.radGridView_Image.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Image.Name = "radGridView_Image";
            // 
            // 
            // 
            this.radGridView_Image.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Image.Size = new System.Drawing.Size(649, 371);
            this.radGridView_Image.TabIndex = 6;
            this.radGridView_Image.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Image_ViewCellFormatting);
            this.radGridView_Image.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Image_CellDoubleClick);
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.BackgroundImage = global::PC_Shop24Hrs.Properties.Resources._24hrs;
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(649, 197);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.SelectionChanged += new System.EventHandler(this.RadGridView_Show_SelectionChanged);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // Bill_RA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 611);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Bill_RA";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "รายการบิลซ่อมรถอู่นอก [RA]";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Bill_RA_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_End)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView radGridView_Image;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_End;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_Begin;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator8;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator9;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Refresh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator10;
    }
}
