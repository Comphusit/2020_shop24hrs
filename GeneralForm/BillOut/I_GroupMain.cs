﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Data;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class I_GroupMain : Telerik.WinControls.UI.RadForm
    {
        string pMainSave = "0";
        string pSubSave = "0";

        public I_GroupMain()
        {
            InitializeComponent();
        }
        //ในส่วนของกลุ่มหลัก
        void ClearDataMain()
        {
            radTextBox_MainID.Text = ""; radTextBox_MainID.Enabled = false;
            radTextBox_MainName.Text = ""; radTextBox_MainName.Enabled = false;
            radCheckBox_MainSta.CheckState = CheckState.Unchecked; radCheckBox_MainSta.Enabled = false;
            radTextBox_MainRmk.Text = ""; radTextBox_MainRmk.Enabled = false;
            radButton_MainSave.Enabled = false;
            pMainSave = "0";
        }
        //ในส่วนของกลุ่มย่อย
        void ClearDataSub()
        {
            try
            {
                radTextBox_MainID_Sub.Text = $@"{ radGridView_Main.CurrentRow.Cells["GROUPRO_ID"].Value}-{radGridView_Main.CurrentRow.Cells["GROUPRO_NAME"].Value}";
            }
            catch (Exception) { radTextBox_MainID_Sub.Text = ""; }

            radTextBox_SubID.Text = ""; radTextBox_SubID.Enabled = false;
            radTextBox_MainID_Sub.Enabled = false;
            radTextBox_SubName.Text = ""; radTextBox_SubName.Enabled = false;
            radCheckBox_SubSta.CheckState = CheckState.Unchecked; radCheckBox_SubSta.Enabled = false;
            radTextBox_SubRmk.Text = ""; radTextBox_SubRmk.Enabled = false;
            radButton_SubSave.Enabled = false;
            pSubSave = "0";
        }
        //Load Main
        private void I_GroupMain_Load(object sender, EventArgs e)
        {
        

            radButton_mainEdit.ButtonElement.ShowBorder = true; radButton_mainEdit.ButtonElement.ToolTipText = "แก้ไขกลุ่มหลัก";
            radButton_subEdit.ButtonElement.ShowBorder = true; radButton_subEdit.ButtonElement.ToolTipText = "แก้ไขกลุ่มย่อย";
            radButton_MainAdd.ButtonElement.ShowBorder = true; radButton_MainAdd.ButtonElement.ToolTipText = "เพิ่มกลุ่มหลัก";
            radButton_SubAdd.ButtonElement.ShowBorder = true; radButton_SubAdd.ButtonElement.ToolTipText = "เพิ่มกลุ่มย่อย";

            DatagridClass.SetDefaultRadGridView(radGridView_Main);
            radGridView_Main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPRO_ID", "รหัสงบ", 100));
            radGridView_Main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPRO_NAME", "คำอธิบาย", 450));
            radGridView_Main.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("GROUPRO_STA", "เปิดใช้"));
            radGridView_Main.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPRO_REMARK", "หมายเหตุ", 450));

            DatagridClass.SetDefaultRadGridView(radGridView_Sub);
            radGridView_Sub.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPRO_SUBID", "รหัสกลุ่มย่อย", 130));
            radGridView_Sub.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPRO_SUBNAME", "คำอธิบาย", 450));
            radGridView_Sub.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("GROUPRO_SUBSTA", "เปิดใช้"));
            radGridView_Sub.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GROUPRO_SUBREMARK", "หมายเหตุ", 450));

            radButton_MainSave.ButtonElement.ShowBorder = true; radButton_MainCanCle.ButtonElement.ShowBorder = true;
            radButton_SubSave.ButtonElement.ShowBorder = true; radButton_SabCancle.ButtonElement.ShowBorder = true;

            ClearDataMain();
            ClearDataSub();

            SetDGV_main();
            SetDGV_sub();
        }

        #region "Format Datagrid"

        private void RadGridView_Main_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Sub_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Sub_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Main_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Main_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Sub_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Main_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Sub_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //เปลี่ยนกลุ่มหลักให้ข้อมูลกลุ่มย่อยเปลี่ยนตาม
        private void RadGridView_Main_SelectionChanged(object sender, EventArgs e)
        {
            SetDGV_sub();
        }
        //แสดงกลุ่มหลัก
        void SetDGV_main()
        {
            radGridView_Main.DataSource = I_Class.Find_GroupMain("'0','1'");
        }
        //เลือกงบเบิกหลัก
        private void RadButton_MainAdd_Click(object sender, EventArgs e)
        {
            DataTable dt = BillOutClass.FindIS_AxForGroupMain();
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV()
            {
                dtData = dt
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radTextBox_MainID.Text = frm.pID;
                radTextBox_MainID.Enabled = false;
                radTextBox_MainName.Text = frm.pDesc;
                radTextBox_MainName.Enabled = false;
                radCheckBox_MainSta.CheckState = CheckState.Checked;
                radCheckBox_MainSta.Enabled = true;
                radTextBox_MainRmk.Text = "";
                radTextBox_MainRmk.Enabled = true;
                radButton_MainCanCle.Enabled = true;
                radButton_MainSave.Enabled = true;
                radTextBox_MainRmk.Focus();
                pMainSave = "1";
                return;
            }
            else
            {
                MsgBoxClass.MsgBoxShow_ChooseDataWarning("กลุ่มหลัก");
                ClearDataMain();
                ClearDataSub();
                return;
            }
        }
        //บันทึกงบเบิกหลัก
        private void RadButton_MainSave_Click(object sender, EventArgs e)
        {
            if (radTextBox_MainID.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ข้อมูลกลุ่มหลัก");
                radButton_MainAdd.Focus();
                return;
            }

            string sta_Main = "0";
            if (radCheckBox_MainSta.Checked == true) sta_Main = "1";

            string sqlIn;

            switch (pMainSave)
            {
                //1 insert 2 Edit
                case "1":
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("กลุ่มหลัก") != DialogResult.Yes) return;
                    sqlIn = I_Class.SHOP_RO_GROUP_InsertUpdate(radTextBox_MainID.Text, radTextBox_MainName.Text, sta_Main, radTextBox_MainRmk.Text);
                    break;
                case "2":
                    if (MsgBoxClass.MsgBoxShow_ConfirmEdit("กลุ่มหลัก") != DialogResult.Yes) return;
                    sqlIn = I_Class.SHOP_RO_GROUP_InsertUpdate(radTextBox_MainID.Text, "", sta_Main, radTextBox_MainRmk.Text);
                    break;
                default:
                    return;
                    //  break;
            }

            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                SetDGV_main();
                ClearDataMain();
            }


        }
        //แสดงกลุ่มย่อย
        void SetDGV_sub()
        {
            if (radGridView_Main.Rows.Count == 0) return;

            string grpMain;
            try
            {
                grpMain = radGridView_Main.CurrentRow.Cells["GROUPRO_ID"].Value.ToString();
            }
            catch (Exception) { grpMain = ""; }

            if (grpMain == "") return;

            radGridView_Sub.DataSource = I_Class.Find_GroupSub(grpMain, "'0','1'");

            radTextBox_MainID_Sub.Text = $@"{grpMain}-{radGridView_Main.CurrentRow.Cells["GROUPRO_NAME"].Value}";

            if (radButton_MainSave.Enabled == true) ClearDataMain();
        }
        //แก้ไขกลุ่มหลัก
        private void RadButton_mainEdit_Click(object sender, EventArgs e)
        {
            if (radGridView_Main.Rows.Count == 0) return;

            radTextBox_MainID.Text = radGridView_Main.CurrentRow.Cells["GROUPRO_ID"].Value.ToString(); radTextBox_MainID.Enabled = false;
            radTextBox_MainName.Text = radGridView_Main.CurrentRow.Cells["GROUPRO_NAME"].Value.ToString(); radTextBox_MainName.Enabled = false;
            radTextBox_MainRmk.Text = radGridView_Main.CurrentRow.Cells["GROUPRO_REMARK"].Value.ToString(); radTextBox_MainRmk.Enabled = true;

            if (radGridView_Main.CurrentRow.Cells["GROUPRO_STA"].Value.ToString() == "1") radCheckBox_MainSta.CheckState = CheckState.Checked;
            else radCheckBox_MainSta.CheckState = CheckState.Unchecked;

            radCheckBox_MainSta.Enabled = true;
            radButton_MainSave.Enabled = true;
            radTextBox_MainRmk.SelectAll();
            radTextBox_MainRmk.Focus();
            pMainSave = "2";
        }
        //เพิ่มข้อมูลกลุ่มย่อย
        private void RadButton_SubAdd_Click(object sender, EventArgs e)
        {
            if (radTextBox_MainID_Sub.Text == "")
            {
                MessageBox.Show("ต้องระบุกลุ่มหลักก่อนที่จะสามารถสามารถสร้างกลุ่มย่อยได้.", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            radTextBox_SubID.Text = ""; radTextBox_SubID.Enabled = false;
            radTextBox_SubName.Text = ""; radTextBox_SubName.Enabled = true;
            radTextBox_SubRmk.Text = ""; radTextBox_SubRmk.Enabled = true;
            radCheckBox_SubSta.CheckState = CheckState.Checked; radCheckBox_SubSta.Enabled = true;
            radButton_SubSave.Enabled = true;
            radTextBox_SubName.Focus();
            pSubSave = "1";
        }
        //แก้ไขช้อมูลกลุ่มย่อย
        private void RadButton_subEdit_Click(object sender, EventArgs e)
        {
            if (radGridView_Sub.Rows.Count == 0) return;

            radTextBox_SubID.Text = radGridView_Sub.CurrentRow.Cells["GROUPRO_SUBID"].Value.ToString();
            radTextBox_SubName.Text = radGridView_Sub.CurrentRow.Cells["GROUPRO_SUBNAME"].Value.ToString(); radTextBox_SubName.Enabled = true;
            radTextBox_SubRmk.Text = radGridView_Sub.CurrentRow.Cells["GROUPRO_SUBREMARK"].Value.ToString(); radTextBox_SubRmk.Enabled = true;

            if (radGridView_Sub.CurrentRow.Cells["GROUPRO_SUBSTA"].Value.ToString() == "1") radCheckBox_SubSta.CheckState = CheckState.Checked;
            else radCheckBox_SubSta.CheckState = CheckState.Unchecked;

            radCheckBox_SubSta.Enabled = true;
            radButton_SubSave.Enabled = true;
            radTextBox_SubName.SelectAll();
            radTextBox_SubName.Focus();
            pSubSave = "2";
        }
        //บันทึก SUb
        private void RadButton_SubSave_Click(object sender, EventArgs e)
        {
            string grpMain;
            try
            {
                grpMain = radGridView_Main.CurrentRow.Cells["GROUPRO_ID"].Value.ToString();
            }
            catch (Exception) { grpMain = ""; }

            if (grpMain == "") return;

            string sta_Sub = "0";
            if (radCheckBox_SubSta.Checked == true) sta_Sub = "1";

            string sqlIn;

            switch (pSubSave)
            {
                case "1":
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("กลุ่มย่อย") != DialogResult.Yes) return;
                    sqlIn = I_Class.SHOP_RO_GROUPSUB_InsertUpdate(grpMain, "", radTextBox_SubName.Text, sta_Sub, radTextBox_MainRmk.Text);
                    break;
                case "2":
                    if (radTextBox_SubID.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ข้อมูลกลุ่มย่อย");
                        radButton_SubAdd.Focus();
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmEdit("กลุ่มย่อย") != DialogResult.Yes) return;
                    sqlIn = I_Class.SHOP_RO_GROUPSUB_InsertUpdate(grpMain, radTextBox_SubID.Text, radTextBox_SubName.Text, sta_Sub, radTextBox_MainRmk.Text);
                    break;
                default:
                    return;
            }

            string T = ConnectionClass.ExecuteSQL_Main(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                SetDGV_sub();
                ClearDataSub();
            }

        }
        //sub claer
        private void RadButton_SabCancle_Click(object sender, EventArgs e)
        {
            ClearDataSub();
        }
        //main clear
        private void RadButton_MainCanCle_Click(object sender, EventArgs e)
        {
            ClearDataMain();
        }
        //change Main Group
        private void RadGridView_Sub_SelectionChanged(object sender, EventArgs e)
        {
            if (radButton_SubSave.Enabled == true) ClearDataSub();
        }
    }
}
