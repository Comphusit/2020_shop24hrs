﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Drawing.Printing;
using static PC_Shop24Hrs.Controllers.PrintClass;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class BillOut_FindBill_APV : Telerik.WinControls.UI.RadForm
    {
        // string pConfigDB;

        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();
        DataTable dtBch = new DataTable();

        readonly string _pType; // 0 shop 1 supc
        readonly string _pFromOpen;// MNRS หรือ MNRR
        readonly string _pPermission;//0 ลงรายการบัญชีไม่ได้ 1 ลงรายการบัญชีได้
        readonly string _pWH;
        public string pDocno;

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        string sDocno, sBranch, sNumBox, sLocation, spCopyColor, spRmk, spUpPRINTCOUNT, sPersonSave, sPersonPacking;
        int iBox, spCopy;
        DataTable sdataDetail = new DataTable();
        double sumLINEAMOUNT;
        public BillOut_FindBill_APV(string pType, string pFromOpen, string pPermission, string pWH = "")// 0 shop 1 supc
        {
            InitializeComponent();
            _pType = pType;
            _pFromOpen = pFromOpen;
            _pPermission = pPermission;
            _pWH = pWH;

            radDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy"; radDateTimePicker_End.CustomFormat = "dd/MM/yyyy";
        }
        //Load
        private void BillOut_FindBill_APV_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;//1
            barcode.BarHeight = 60;//38
            barcode.LeftMargin = 0;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;
            radStatusStrip1.SizingGrip = false;

            spUpPRINTCOUNT = "0";

            radButtonElement_print.ShowBorder = true;
            radButtonElement_print.ToolTipText = "Print";
            radButtonElement_print.Enabled = false;
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("C", "พิมพ์", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAUPAX", "ลงรายการ")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("StaPrcDoc", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATE", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENT", "คลังเบิก", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BILLAMOUNT", "ยอดรวม", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BOXQTY", "จำนวนลัง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PRINTCOUNT", "ครั้งที่พิมพ์", 80))); ;
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOINS", "ผู้บันทึก", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOINP", "ผู้จัดบิล", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEAX", "วันที่ลงรายการ", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOAX", "ผู้ลงรายการ", 200)));

            RadGridView_ShowHD.Columns["C"].IsVisible = false;
            RadGridView_ShowHD.Columns["C"].IsPinned = true;
            RadGridView_ShowHD.Columns["STAUPAX"].IsPinned = true;
            RadGridView_ShowHD.Columns["STADOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["StaPrcDoc"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "STAUPAX = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_PinkPastel() };
            this.RadGridView_ShowHD.Columns["STAUPAX"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["DOCNO"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["STADOC"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["StaPrcDoc"].ConditionalFormattingObjectList.Add(obj1);

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SALEPRICE", "ราคา/หน่วย", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ราคารวม", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN", "กลุ่มหลัก", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPSUB", "กลุ่มย่อย", 100)));

            RadButton_Search.ButtonElement.ShowBorder = true;

            radDateTimePicker_End.Value = DateTime.Now;

            switch (_pType)
            {
                case "0"://shop
                    RadCheckBox_Branch.CheckState = CheckState.Checked;
                    dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadDropDownList_Branch.DataSource = dtBch;
                    RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_Branch.ValueMember = "BRANCH_ID";
                    RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                    RadCheckBox_Branch.Text = "สาขา";

                    RadCheckBox_Branch.Enabled = false;
                    radCheckBox_Apv.Checked = false; radCheckBox_Apv.Visible = false;
                    radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-5);
                    radLabel_Detail.Visible = false;
                    break;
                case "1": // supc
                    RadCheckBox_Branch.CheckState = CheckState.Unchecked;
                    if (_pFromOpen == "MNRS_D")
                    {
                        dtBch = Models.DptClass.GetDpt_AllD();
                        RadDropDownList_Branch.DataSource = dtBch;
                        RadDropDownList_Branch.DisplayMember = "DESCRIPTION";
                        RadDropDownList_Branch.ValueMember = "NUM";
                        RadCheckBox_Branch.Text = "แผนก";
                    }
                    else
                    {

                        dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                        RadDropDownList_Branch.DataSource = dtBch;
                        RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                        RadDropDownList_Branch.ValueMember = "BRANCH_ID";
                        RadCheckBox_Branch.Text = "สาขา";
                    }
                    if (_pPermission == "0")
                    {
                        radLabel_Detail.Text = "สีชมพู << บิลยังไม่ลงรายการ | Click ช่อง ยกเลิก >> ยกเลิกบิล || DoubleClick >> รายละเอียด/ตรวจบิล || DoubleClick ที่ช่องกลุ่มหลัก >> เปลี่ยนงบเบิก";
                    }
                    else
                    {
                        radLabel_Detail.Text = "สีชมพู << บิลยังไม่ลงรายการ | Click ช่อง ลงรายการ >> ส่งข้อมูลเข้า AX | Click ช่อง ยกเลิก >> ยกเลิกบิล || DoubleClick >> รายละเอียด/ตรวจบิล || DoubleClick ที่ช่องกลุ่มหลัก >> เปลี่ยนงบเบิก";
                    }

                    RadCheckBox_Branch.Enabled = true;
                    radCheckBox_Apv.Checked = true; radCheckBox_Apv.Visible = true;
                    radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-15);
                    radLabel_Detail.Visible = true;
                    break;
                default:
                    break;
            }
            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }
            string sqlSelect = string.Empty;
            this.Cursor = Cursors.WaitCursor;
            switch (_pFromOpen)
            {
                case "MNRS_MN":
                    radButtonElement_print.Enabled = true;
                    RadGridView_ShowHD.Columns["C"].IsVisible = true;
                    string pConWH = "";
                    if (_pWH != "")
                    {
                        pConWH = " AND INVENT = '" + _pWH + @"' ";
                    }
                    sqlSelect = string.Format(@"SELECT	'พิมพ์บิล' AS C,STAUPAX,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_ID,DOCNO,
                        CONVERT(varchar,DATE,23) AS DATE,INVENT,
		                WHOIDINS+'-'+WHONAMEINS AS WHOINS,
                        WHOIDPACKING+'-'+WHONAMEPACKING AS WHOINP,
		                BILLAMOUNT,BOXQTY,REMARK,PRINTCOUNT,		
		                CASE STAUPAX WHEN '1' THEN WHOIDUPAX+'-'+WHONAMEUPAX  ELSE '' END AS WHOAX,
		                CASE STAUPAX WHEN '1' THEN CONVERT(VARCHAR,DATEUPAX,23)  ELSE '' END AS DATEAX,
                        CASE STADOC WHEN '0' THEN '1' ELSE '0' END AS STADOC ,'1'  AS StaPrcDoc
                    FROM	SHOP_RO_MNRSHD WITH (NOLOCK)
                    WHERE	STADOC = '1' AND BRANCH_ID LIKE 'MN%'  " + pConWH + @"  
                        AND CONVERT(varchar,DATE,23) BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + "' AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + "'  ");
                    if (RadCheckBox_Branch.Checked == true)
                    { sqlSelect += " AND BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"' "; }

                    if (radCheckBox_Apv.Checked == true)
                    { sqlSelect += " AND STAUPAX = '0'  "; }
                    sqlSelect += " ORDER BY DOCNO DESC ";
                    break;

                case "MNRS_D":
                    string pConWHD = "";
                    if (_pWH != "")
                    {
                        pConWHD = " AND INVENT = '" + _pWH + @"' ";
                    }
                    sqlSelect = string.Format(@"SELECT	STAUPAX,BRANCH_ID+'-'+BRANCH_NAME AS BRANCH_ID,DOCNO,
                        CONVERT(varchar,DATE,23) AS DATE,INVENT,
		                WHOIDINS+'-'+WHONAMEINS AS WHOINS,
                        WHOIDPACKING+'-'+WHONAMEPACKING AS WHOINP,
		                BILLAMOUNT,BOXQTY,REMARK,PRINTCOUNT,		
		                CASE STAUPAX WHEN '1' THEN WHOIDUPAX+'-'+WHONAMEUPAX  ELSE '' END AS WHOAX,
		                CASE STAUPAX WHEN '1' THEN CONVERT(VARCHAR,DATEUPAX,23)  ELSE '' END AS DATEAX,
                        CASE STADOC WHEN '0' THEN '1' ELSE '0' END AS STADOC ,'1'  AS StaPrcDoc
                    FROM	SHOP_RO_MNRSHD WITH (NOLOCK)
                    WHERE	STADOC = '1' AND BRANCH_ID LIKE 'D%'  " + pConWHD + @"  
                        AND CONVERT(varchar,DATE,23) BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + "' AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + "'  ");
                    if (RadCheckBox_Branch.Checked == true)
                    { sqlSelect += " AND BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"' "; }

                    if (radCheckBox_Apv.Checked == true)
                    { sqlSelect += " AND STAUPAX = '0'  "; }
                    sqlSelect += " ORDER BY DOCNO DESC ";
                    break;
                case "MNRR":
                    //           sqlSelect = string.Format(@"			SELECT	MNRRUpAX AS STAUPAX ,
                    //MNRRBranchTO+'-'+CASE WHEN ISNULL(SHOP_BRANCH.BRANCH_NAME,'')='' THEN DIMENSIONS.DESCRIPTION ELSE SHOP_BRANCH.BRANCH_NAME END AS BRANCH_ID,
                    //MNRRDocNo AS DOCNO,
                    //               CONVERT(varchar,MNRRDate,23) AS DATE,MNRRBranch AS INVENT,
                    //         MNRRUserCode+'-'+MNRRNAMECH AS WHOINS,
                    //         Grand AS BILLAMOUNT,'1' AS BOXQTY,MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END AS REMARK,'1' AS PRINTCOUNT,		
                    //         CASE MNRRUpAX WHEN '1' THEN MNRRWhoApv+'-'+EMPLTABLE.SPC_NAME  ELSE '' END AS WHOAX,
                    //         CASE MNRRUpAX WHEN '1' THEN CONVERT(VARCHAR,MNRRDateApv,23)  ELSE '' END AS DATEAX,
                    //               CASE MNRRStaDoc WHEN '3' THEN '1' ELSE '0' END AS STADOC ,MNRRStaPrcDoc AS StaPrcDoc  
                    //           FROM	Shop_MNRR_HD WITH (NOLOCK)
                    //       LEFT OUTER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  SHOP_BRANCH.BRANCH_ID
                    //                         INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_MNRR_HD.MNRRBranchTO =  DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' ANd DIMENSIONS.DIMENSIONCODE = '0'
                    //                         LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON Shop_MNRR_HD.MNRRWhoApv = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'   
                    //           WHERE	MNRRStaDoc = '1' ");
                    //           sqlSelect += " AND CONVERT(varchar,MNRRDate,23)  BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + "' AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + "'  ";

                    //           if (RadCheckBox_Branch.Checked == true)
                    //           { sqlSelect += " AND MNRRBranch = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"' "; }

                    //           if (radCheckBox_Apv.Checked == true)
                    //           {
                    //               sqlSelect += "  AND MNRRUpAX = '0'  ";
                    //           }
                    //           else
                    //           {
                    //               sqlSelect += "  AND MNRRUpAX IN ('0','1')  ";
                    //           }
                    //           sqlSelect += " ORDER BY MNRRDocNo DESC ";

                    string bchID = "";
                    if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
                    string staApv = "";
                    if (radCheckBox_Apv.Checked == true) staApv = "0";

                    sqlSelect = BillOutClass.GetData_MNRR_HD(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), bchID, staApv);
                    break;

                default:
                    break;
            }


            dtDataHD = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            string sqlSelect = string.Empty;
            switch (_pFromOpen)
            {
                case "MNRS_MN":
                    sqlSelect = string.Format(@"
                    SELECT	SHOP_RO_MNRSDT.DOCNO,ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,UNITID,SALEPRICE,QTY AS QTY,LINEAMOUNT,
		                    BRANCH_ID,CONVERT(varchar,SHOP_RO_MNRSHD.DATE,23) AS DATE,INVENT,REMARK,ITEMID,INVENTDIMID,WHOIDPACKING     
                    FROM	SHOP_RO_MNRSDT WITH (NOLOCK) INNER JOIN SHOP_RO_MNRSHD WITH (NOLOCK)
		                    ON SHOP_RO_MNRSDT.DOCNO = SHOP_RO_MNRSHD.DOCNO
                    WHERE	SHOP_RO_MNRSDT.STADOC = '1' AND SHOP_RO_MNRSDT.DOCNO = '" + _pDocno + @"' 
                    ORDER BY GROUPMAIN,GROUPSUB ");
                    break;
                case "MNRS_D":
                    sqlSelect = string.Format(@"
                    SELECT	SHOP_RO_MNRSDT.DOCNO,ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,UNITID,SALEPRICE,QTY AS QTY,LINEAMOUNT,
		                    BRANCH_ID,CONVERT(varchar,SHOP_RO_MNRSHD.DATE,23) AS DATE,INVENT,REMARK,ITEMID,INVENTDIMID,WHOIDPACKING     
                    FROM	SHOP_RO_MNRSDT WITH (NOLOCK) INNER JOIN SHOP_RO_MNRSHD WITH (NOLOCK)
		                    ON SHOP_RO_MNRSDT.DOCNO = SHOP_RO_MNRSHD.DOCNO
                    WHERE	SHOP_RO_MNRSDT.STADOC = '1' AND SHOP_RO_MNRSDT.DOCNO = '" + _pDocno + @"' 
                    ORDER BY GROUPMAIN,GROUPSUB ");
                    break;
                case "MNRR":
                    //sqlSelect = string.Format(@"
                    //SELECT	Shop_MNRR_DT.MNRRDocNo AS DOCNO,MNRRBarcode AS ITEMBARCODE,MNRRName AS SPC_ITEMNAME,MNRRGroup AS GROUPMAIN,
                    //  MNRRGroupSub AS GROUPSUB,MNRRQtyUnitID AS UNITID,MNRRPrice AS SALEPRICE,MNRRQtyOrder  AS QTY,MNRRPriceSum AS LINEAMOUNT,
                    //        MNRRBranchTO AS BRANCH_ID,CONVERT(varchar,MNRRDate,23) AS DATE,MNRRBranch AS INVENT,
                    //        Shop_MNRR_HD.MNRRRemark + CASE WHEN ISNULL(CUST_ID,'') = '' THEN '' ELSE ' ' + CUST_ID + '-' + CUST_NAME END AS REMARK,
                    //  MNRRItemID AS ITEMID,MNRRItemDim AS INVENTDIMID     
                    //FROM	Shop_MNRR_DT WITH (NOLOCK)
                    //  INNER JOIN Shop_MNRR_HD WITH (NOLOCK) ON Shop_MNRR_DT.MNRRDOCNO = Shop_MNRR_HD.MNRRDOCNO  
                    //  LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTCOSTCENTER  WITH (NOLOCK)   ON Shop_MNRR_DT.MNRRGroup = SPC_INVENTCOSTCENTER.SPC_INVENTCOSTCENTERID   

                    //WHERE	MNRRSta = '1'    AND Shop_MNRR_DT.MNRRDocNo = '" + _pDocno + @"' AND MNRRQtyOrder <> 0  
                    //ORDER BY MNRRGroup  ");
                    sqlSelect = BillOutClass.GetData_MNRR_DT(_pDocno);
                    break;
                default:
                    break;
            }

            dtDataDT = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
                return;
            }
            if (RadGridView_ShowHD.CurrentRow.Index == -1)
            {
                return;
            }
            if (RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() == "")
            {
                return;
            }
            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }

        private void RadButtonElement_print_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (_pFromOpen != "MNRS_MN") return;
            if (_pType == "0") { return; }
            if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1") { return; } //ยกเลิกรายการ
            if (RadGridView_ShowHD.CurrentRow.Cells["STAUPAX"].Value.ToString() == "1") { return; } //ลงรายการ ลงรายการแล้วเท่ากับปริ๊นแล้ว
            if (RadGridView_ShowHD.CurrentRow.Cells["StaPrcDoc"].Value.ToString() == "0") { return; } //ยืนยัน

            //DataTable dt_Result =
            if (RadGridView_ShowHD.Rows.Count > 0)
            {
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {

                    PrintDocument_printBox.PrintController = printController;
                    PrintDocument_printBox.PrinterSettings = printDialog1.PrinterSettings;

                    printDocument1.PrintController = printController;
                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                    PrintDocument_printBill.PrintController = printController;
                    PrintDocument_printBill.PrinterSettings = printDialog1.PrinterSettings;

                    sDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
                    sBranch = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                    sLocation = RadGridView_ShowHD.CurrentRow.Cells["INVENT"].Value.ToString();
                    iBox = Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["BOXQTY"].Value.ToString());

                    sdataDetail = dtDataDT;
                    spCopyColor = "";
                    spRmk = RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value.ToString();
                    spCopy = Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["PRINTCOUNT"].Value.ToString());
                    sumLINEAMOUNT = Convert.ToDouble(RadGridView_ShowHD.CurrentRow.Cells["BILLAMOUNT"].Value.ToString());
                    sPersonSave = RadGridView_ShowHD.CurrentRow.Cells["WHOINS"].Value.ToString();
                    sPersonPacking = RadGridView_ShowHD.CurrentRow.Cells["WHOINP"].Value.ToString();
                    spCopy += 1;
                    spCopyColor = "บิลฟ้า"; spUpPRINTCOUNT = "1";
                    PrintDocument_printBill.Print();

                    if (SystemClass.SystemDptID != "D179")
                    {
                        if (SystemClass.SystemDptID != "D156")
                        {
                            spCopyColor = "บิลชมพู"; spUpPRINTCOUNT = "0";
                            PrintDocument_printBill.Print();

                            for (int j = 0; j < iBox; j++)
                            {
                                sNumBox = "จำนวนลัง  " + Convert.ToString(j + 1) + "/" + Convert.ToString(iBox);
                                PrintDocument_printBox.Print();
                                for (int k = 0; k < 2; k++)
                                {
                                    printDocument1.Print();
                                }
                            }
                        }
                    }

                }
                this.DialogResult = DialogResult.Yes;
            }

        }

        //คลิกรับบิล
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (_pType == "0") return;

            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;
            if (RadGridView_ShowDT.Rows.Count == 0) return;


            switch (e.Column.Name)
            {
                case "STAUPAX":
                    if (_pPermission == "0") { return; }
                    if (_pType == "0") { return; }
                    if (RadGridView_ShowHD.Rows.Count == 0) { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["STAUPAX"].Value.ToString() == "1") { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1") { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["StaPrcDoc"].Value.ToString() == "0") { return; }
                    string docnoAX = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
                    if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(docnoAX, "ลงรายการบัญชี") == DialogResult.No)
                    { return; }

                    string T_AX;

                    if (_pFromOpen == "MNRS_MN")
                    {
                        // SendAX();
                        //T_AX = SmartRO.SmartRO_Class.SendData_AX(dtDataDT, "SHOP_RO_MNRSHD", "STAUPAX", "DOCNO");
                        T_AX = ConnectionClass.ExecuteMain_AX_24_SameTime(I_Class.SHOP_RO_MNRS_UpdateSendAX(docnoAX, "SHOP_RO_MNRSHD", "STAUPAX", "DOCNO"), AX_SendData.Save_SPC_INVENTJOURNALTABLE(dtDataDT));
                    }
                    else if (_pFromOpen == "MNRS_D")
                    {
                        //T_AX = SmartRO.SmartRO_Class.SendData_AX(dtDataDT, "SHOP_RO_MNRSHD", "STAUPAX", "DOCNO");
                        T_AX = ConnectionClass.ExecuteMain_AX_24_SameTime(I_Class.SHOP_RO_MNRS_UpdateSendAX(docnoAX, "SHOP_RO_MNRSHD", "STAUPAX", "DOCNO"), AX_SendData.Save_SPC_INVENTJOURNALTABLE(dtDataDT));
                    }
                    else//MNRR
                    {
                        //T_AX = SmartRO.SmartRO_Class.SendData_AX(dtDataDT, "Shop_MNRR_HD", "MNRRUpAX", "MNRRDocNo");
                        T_AX = ConnectionClass.ExecuteMain_AX_24_SameTime(I_Class.SHOP_RO_MNRS_UpdateSendAX(docnoAX, "Shop_MNRR_HD", "MNRRUpAX", "MNRRDocNo"), AX_SendData.Save_SPC_INVENTJOURNALTABLE(dtDataDT));
                    }
                    MsgBoxClass.MsgBoxShow_SaveStatus(T_AX);
                    if (T_AX == "")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["STAUPAX"].Value = "1";
                        dtDataHD.AcceptChanges();
                    }
                    break;

                case "STADOC":
                    if (_pType == "0") { return; }
                    if (RadGridView_ShowHD.Rows.Count == 0) { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1") { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["STAUPAX"].Value.ToString() == "1") { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["StaPrcDoc"].Value.ToString() == "0") { return; }
                    string docnoSTA = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
                    if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(docnoSTA, "ยกเลิกบิล") == DialogResult.No)
                    { return; }

                    string sqlUp;

                    string pDocno = dtDataDT.Rows[0]["DOCNO"].ToString();
                    if (_pFromOpen == "MNRS_MN")
                    {
                        sqlUp = $"UPDATE SHOP_RO_MNRSHD SET STADOC =  '0',Bill_SpcType = '1' WHERE DOCNO = '" + pDocno + @"' ";
                    }
                    else if (_pFromOpen == "MNRS_D")
                    { sqlUp = $"UPDATE SHOP_RO_MNRSHD SET STADOC =  '0',Bill_SpcType = '1' WHERE DOCNO = '" + pDocno + @"' "; }
                    else // MNRR
                    {
                        sqlUp = $"UPDATE Shop_MNRR_HD SET MNRRStaDoc =  '3',Bill_SpcType = '1' WHERE MNRRDocNo = '" + pDocno + @"' ";
                    }

                    string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value = "1";
                        dtDataHD.AcceptChanges();
                    }
                    break;
                case "C":
                    if (_pType == "0") { return; }
                    if (RadGridView_ShowHD.Rows.Count == 0) return;
                    if (_pFromOpen != "MNRS_MN") return;

                    if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1") { return; } //ยกเลิกรายการ
                    // if (RadGridView_ShowHD.CurrentRow.Cells["STAUPAX"].Value.ToString() == "1") { return; } //ลงรายการ ลงรายการแล้วเท่ากับปริ๊นแล้ว
                    if (RadGridView_ShowHD.CurrentRow.Cells["StaPrcDoc"].Value.ToString() == "0") { return; } //ยืนยัน

                    //DataTable dt_Result =
                    if (RadGridView_ShowHD.Rows.Count > 0)
                    {
                        //DialogResult result = printDialog1.ShowDialog();
                        //if (result == DialogResult.OK)
                        //{
                        this.Cursor = Cursors.WaitCursor;
                        PrintDocument_printBox.PrintController = printController;
                        PrintDocument_printBox.PrinterSettings = printDialog1.PrinterSettings;

                        //printDocument1.PrintController = printController;
                        //printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                        PrintDocument_printBill.PrintController = printController;
                        PrintDocument_printBill.PrinterSettings = printDialog1.PrinterSettings;

                        sDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
                        sBranch = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                        sLocation = RadGridView_ShowHD.CurrentRow.Cells["INVENT"].Value.ToString();
                        iBox = Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["BOXQTY"].Value.ToString());

                        sdataDetail = dtDataDT;
                        spCopyColor = "";
                        spRmk = RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value.ToString();
                        spCopy = Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["PRINTCOUNT"].Value.ToString());
                        sumLINEAMOUNT = Convert.ToDouble(RadGridView_ShowHD.CurrentRow.Cells["BILLAMOUNT"].Value.ToString());
                        sPersonSave = RadGridView_ShowHD.CurrentRow.Cells["WHOINS"].Value.ToString();
                        sPersonPacking = RadGridView_ShowHD.CurrentRow.Cells["WHOINP"].Value.ToString();
                        spCopy += 1;
                        spCopyColor = "บิลฟ้า"; spUpPRINTCOUNT = "1";
                        PrintDocument_printBill.Print();

                        if (SystemClass.SystemDptID != "D179")
                        {
                            if (SystemClass.SystemDptID != "D156")
                            {
                                spCopyColor = "บิลชมพู"; spUpPRINTCOUNT = "0";
                                PrintDocument_printBill.Print();

                                for (int j = 0; j < iBox; j++)
                                {
                                    sNumBox = "จำนวนลัง  " + Convert.ToString(j + 1) + "/" + Convert.ToString(iBox);
                                    PrintDocument_printBox.Print();

                                    //printDocument1.Print();
                                    //if (j != 0) printDocument1.Print();

                                    //for (int k = 0; k < 2; k++)
                                    //{
                                    //    printDocument1.Print();
                                    //}
                                }
                            }
                        }
                        SetDGV_HD();
                        this.Cursor = Cursors.Default;
                        // }
                        //this.DialogResult = DialogResult.Yes;
                    }

                    break;
                default:
                    break;
            }

        }
        //CheckState
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true)
            { RadDropDownList_Branch.Enabled = true; }
            else { RadDropDownList_Branch.Enabled = false; }
        }

        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            BillOut_Detail frm = new BillOut_Detail(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
            if (frm.ShowDialog(this) == DialogResult.OK) { }
        }

        private void RadGridView_ShowDT_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            if (_pType == "0") { return; }
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            if (RadGridView_ShowHD.CurrentRow.Cells["STAUPAX"].Value.ToString() == "1") { return; }
            if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1") { return; }
            if (RadGridView_ShowHD.CurrentRow.Cells["StaPrcDoc"].Value.ToString() == "0") { return; }
            string docnoAX = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();

            switch (e.Column.Name)
            {
                case "GROUPMAIN":
                    string sql = string.Format(@"SELECT	[GROUPRO_ID] AS DATA_ID,[GROUPRO_NAME] AS DATA_DESC,[GROUPRO_STA],[GROUPRO_REMARK]
                        FROM	[SHOP_RO_GROUPMAIN] WITH (NOLOCK) 
                        WHERE   GROUPRO_STA IN ('1')  ORDER BY [GROUPRO_ID] ");
                    DataTable dtIS = ConnectionClass.SelectSQL_Main(sql);
                    FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV { dtData = dtIS };
                    if (frm.ShowDialog(this) == DialogResult.Yes)
                    {
                        string sqlUp;
                        if (_pFromOpen == "MNRS_MN")
                        {
                            sqlUp = String.Format(@"UPDATE SHOP_RO_MNRSDT SET GROUPMAIN = '" + frm.pID + @"' 
                                    WHERE   DOCNO = ' " + docnoAX + @"' AND ITEMBARCODE = '" + RadGridView_ShowDT.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ");
                        }
                        else if (_pFromOpen == "MNRS_D")
                        {
                            sqlUp = String.Format(@"UPDATE SHOP_RO_MNRSDT SET GROUPMAIN = '" + frm.pID + @"' 
                                    WHERE   DOCNO = ' " + docnoAX + @"' AND ITEMBARCODE = '" + RadGridView_ShowDT.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ");
                        }
                        else
                        {
                            sqlUp = String.Format(@"UPDATE Shop_MNRR_DT SET MNRRGroup = '" + frm.pID + @"' 
                                    WHERE MNRRBarcode = '" + RadGridView_ShowDT.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' AND MNRRDocNo = '" + docnoAX + @"'");
                        }
                        string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "")
                        {
                            RadGridView_ShowDT.CurrentRow.Cells["GROUPMAIN"].Value = frm.pID;
                        }
                        return;
                    }
                    break;
                default:
                    break;
            }
        }

        private void PrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            //string[] ss = sBranch.Split('-');
            //PrintClass.Print_Bch(e, ss[0], ss[1]);

            //int Y = 0;
            //Y += 20;

            //string[] ss = sBranch.Split('-');
            //e.Graphics.DrawString(ss[0], new Font(new FontFamily("Tahoma"), 50), Brushes.Black, 0, Y);
            //Y += 70;


            //Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            //StringFormat stringFormat = new StringFormat()
            //{
            //    Alignment = StringAlignment.Near,
            //    LineAlignment = StringAlignment.Near
            //};
            //e.Graphics.DrawString(ss[1], new Font(new FontFamily("Tahoma"), 40), Brushes.Black, rect1, stringFormat);
        }

        private void PrintDocument_printBill_PrintPage(object sender, PrintPageEventArgs e)
        {
            Var_Print_MNRS var = new Var_Print_MNRS
            {
                SDocno = sDocno,
                SBranch = sBranch,
                SNumBox = sNumBox,
                SLocation = sLocation,
                SpCopyColor = spCopyColor,
                SpRmk = spRmk,
                SpUpPRINTCOUNT = spUpPRINTCOUNT,
                SPersonSave = sPersonSave,
                SPersonPacking = sPersonPacking,
                IBox = iBox,
                SpCopy = spCopy,
                SumLINEAMOUNT = sumLINEAMOUNT,
                SdataDetail = sdataDetail
            };

            PrintClass.Print_MNRS("bill", e, var);
            //barcode.Data = sDocno;
            //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            //int Y = 0;
            //e.Graphics.DrawString("พิมพ์ครั้งที่ " + spCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + spCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString(sBranch, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            //Y += 80;
            //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("คลังเบิก " + sLocation, SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //for (int i = 0; i < sdataDetail.Rows.Count; i++)
            //{
            //    Y += 20;
            //    e.Graphics.DrawString((i + 1).ToString() + ".(" + (Convert.ToDouble(sdataDetail.Rows[i]["QTY"].ToString()).ToString("#,#0.00")).ToString() + " X " +
            //                      sdataDetail.Rows[i]["UNITID"].ToString() + ")   " +
            //                      sdataDetail.Rows[i]["ITEMBARCODE"].ToString(),
            //         SystemClass.printFont, Brushes.Black, 1, Y);
            //    Y += 15;
            //    e.Graphics.DrawString(sdataDetail.Rows[i]["SPC_ITEMNAME"].ToString(),
            //        SystemClass.printFont, Brushes.Black, 0, Y);
            //}

            //Y += 15;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 15;
            //e.Graphics.DrawString("จำนวนทั้งหมด " + iBox.ToString() + "  ลัง", SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 15;
            //e.Graphics.DrawString("รวม " + sdataDetail.Rows.Count.ToString() + "  รายการ/" +
            //    "ยอดเงิน " + (sumLINEAMOUNT.ToString("#,#0.00")).ToString() + "  บาท", SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 20;
            //e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 15;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 30;
            //e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            //StringFormat stringFormat = new StringFormat()
            //{
            //    Alignment = StringAlignment.Near,
            //    LineAlignment = StringAlignment.Near
            //};
            //e.Graphics.DrawString(spRmk, SystemClass.printFont, Brushes.Black, rect1, stringFormat);

            //e.Graphics.PageUnit = GraphicsUnit.Inch;

            if (spUpPRINTCOUNT == "1")
            {
                string upStr = string.Format(@"UPDATE SHOP_RO_MNRSHD SET PRINTCOUNT = '" + spCopy + "' WHERE DOCNO = '" + sDocno + "' ");
                ConnectionClass.ExecuteSQL_Main(upStr);

            }

        }

        private void PrintDocument_printBox_PrintPage(object sender, PrintPageEventArgs e)
        {
            //barcode.Data = sDocno;
            //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            //int Y = 0;

            //Y += 20;
            //e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + sLocation + "]", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString(sBranch, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            //Y += 80;
            //e.Graphics.DrawString(sNumBox, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //e.Graphics.PageUnit = GraphicsUnit.Inch;
            Var_Print_MNRS var = new Var_Print_MNRS
            {
                SDocno = sDocno,
                SBranch = sBranch,
                SNumBox = sNumBox,
                SLocation = sLocation,
                SpCopyColor = spCopyColor,
                SpRmk = spRmk,
                SpUpPRINTCOUNT = spUpPRINTCOUNT,
                SPersonSave = sPersonSave,
                SPersonPacking = sPersonPacking,
                IBox = iBox,
                SpCopy = spCopy,
                SumLINEAMOUNT = sumLINEAMOUNT,
                SdataDetail = sdataDetail
            };
            PrintClass.Print_MNRS("box", e, var);
        }

        //document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pFromOpen + _pPermission);
        }

    }
}
