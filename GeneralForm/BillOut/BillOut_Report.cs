﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class BillOut_Report : Telerik.WinControls.UI.RadForm
    {
        private DataTable dtBch = new DataTable();
        private int pColume;

        private string pBchID = "BRANCH_ID";

        readonly string _pType;
        readonly string _pTextOrImage;

        //Load
        public BillOut_Report(string pType, string pTextOrImage = "1")//_pTextOrImage 0 text 1 Image
        {
            InitializeComponent();
            _pType = pType;
            _pTextOrImage = pTextOrImage;

        }
        //Load Main
        private void BillOut_Report_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_IS);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-1), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_End.Value = DateTime.Now;

            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_SPC.ButtonElement.Font = SystemClass.SetFontGernaral; radCheckBox_SPC.Checked = false;
            radCheckBox_MN.ButtonElement.Font = SystemClass.SetFontGernaral; radCheckBox_MN.Checked = false;
            radCheckBox_IS.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadDropDownList_Branch.DropDownListElement.DropDownWidth = 300;

            if (SystemClass.SystemBranchID == "MN000")
            {
                dtBch = BranchClass.GetBranchAll("'1','2','4'", "'1','0'");
                RadCheckBox_Branch.Checked = false; RadCheckBox_Branch.Enabled = true;
            }
            else
            {
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Checked = true; RadCheckBox_Branch.Enabled = false;
            }

            radDropDownList_IS.Visible = false; radCheckBox_IS.Visible = false;

            string pBchName = "NAME_BRANCH";

            switch (_pType)
            {
                case "MNRS":
                    pColume = 3;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = true; radCheckBox_SPC.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        radCheckBox_SPC.Visible = false; radCheckBox_SPC.CheckState = CheckState.Unchecked;
                        radCheckBox_MN.Visible = true; radCheckBox_MN.CheckState = CheckState.Checked;
                    }
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | สีฟ้า >>ComMN(ค้างตรวจ) | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNIO":
                    pColume = 2;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = true; radCheckBox_SPC.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        radCheckBox_SPC.Visible = false; radCheckBox_SPC.CheckState = CheckState.Unchecked;
                        radCheckBox_MN.Visible = true; radCheckBox_MN.CheckState = CheckState.Checked;
                    }
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "Detail_MNIO":
                    //pColume = 2;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = true; radCheckBox_SPC.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        radCheckBox_SPC.Visible = false; radCheckBox_SPC.CheckState = CheckState.Unchecked;
                        radCheckBox_MN.Visible = true; radCheckBox_MN.CheckState = CheckState.Checked;
                    }
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNOT":
                    pColume = 3;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = true; radCheckBox_SPC.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        radCheckBox_SPC.Visible = false; radCheckBox_SPC.CheckState = CheckState.Unchecked;
                        radCheckBox_MN.Visible = true; radCheckBox_MN.CheckState = CheckState.Checked;
                    }
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "Detail_MNOT":
                    //pColume = 3;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = true; radCheckBox_SPC.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        radCheckBox_SPC.Visible = false; radCheckBox_SPC.CheckState = CheckState.Unchecked;
                        radCheckBox_MN.Visible = true; radCheckBox_MN.CheckState = CheckState.Checked;
                    }
                    radLabel_F2.Text = "สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล";
                    break;
                case "IMN":
                    pColume = 5; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    radLabel_F2.Text = "สีแดง >>บิลไม่ Import(ค้างตรวจ) | สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "FAL":
                    pColume = 5;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = true; radCheckBox_SPC.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        radCheckBox_SPC.Visible = true; radCheckBox_SPC.CheckState = CheckState.Unchecked;
                        radCheckBox_MN.Visible = true; radCheckBox_MN.CheckState = CheckState.Checked;
                    }
                    radLabel_F2.Text = "สีแดง >>บิลไม่ Import(ค้างตรวจ) | สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNII":
                    pColume = 0; radCheckBox_SPC.Visible = true; radCheckBox_MN.Visible = false;
                    radCheckBox_SPC.Enabled = false; radCheckBox_SPC.CheckState = CheckState.Checked;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "ISPC":
                    pColume = 5;
                    RadCheckBox_Branch.Text = "ระบุแผนก";
                    dtBch = Models.DptClass.GetDpt_AllD();
                    pBchID = "NUM";
                    pBchName = "DESCRIPTION_SHOW";
                    radCheckBox_MN.Visible = false; radCheckBox_SPC.Visible = false;
                    SetIS(); radDropDownList_IS.Visible = true; radCheckBox_IS.Visible = true;
                    radLabel_F2.Text = "สีแดง >>บิลไม่ Import(ค้างตรวจ) | สีชมพู >>ComService(ค้างตรวจ) | สีฟ้า >>ComMN(ค้างตรวจ) | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "MNRG":
                    pColume = 3; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNRB":
                    pColume = 3; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNRH":
                    pColume = 1; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNRR":
                    pColume = 1; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "MNPZ":
                    pColume = 2;
                    RadCheckBox_Branch.Text = "ระบุแผนก";
                    dtBch = Models.DptClass.GetDpt_AllD();
                    pBchID = "NUM";
                    pBchName = "DESCRIPTION_SHOW";
                    radCheckBox_MN.Visible = false; radCheckBox_SPC.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "IDM":
                    pColume = 1;
                    RadCheckBox_Branch.Text = "ระบุแผนก";
                    dtBch = Models.DptClass.GetDpt_AllD();
                    pBchID = "NUM";
                    pBchName = "DESCRIPTION_SHOW";
                    radCheckBox_MN.Visible = false; radCheckBox_SPC.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "MNRZ":
                    pColume = 1;
                    RadCheckBox_Branch.Text = "ระบุแผนก";
                    dtBch = Models.DptClass.GetDpt_AllD();
                    pBchID = "NUM";
                    pBchName = "DESCRIPTION_SHOW";
                    radCheckBox_MN.Visible = false; radCheckBox_SPC.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "MNCM_1":
                    pColume = 2; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    RadDropDownList_Branch.Visible = false; RadCheckBox_Branch.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNCM_0":
                    pColume = 6; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    RadDropDownList_Branch.Visible = false; RadCheckBox_Branch.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "MNCM_2":
                    pColume = 6; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    RadDropDownList_Branch.Visible = false; RadCheckBox_Branch.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                case "MNCM_3":
                    pColume = 6; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    RadDropDownList_Branch.Visible = false; RadCheckBox_Branch.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;

                case "MNRD":
                    pColume = 1;
                    radLabel_F2.Text = "สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    radCheckBox_MN.Visible = false; radCheckBox_SPC.Visible = false;
                    break;

                case "MNRM":
                    pColume = 2; radCheckBox_SPC.Visible = false; radCheckBox_MN.Visible = false;
                    radLabel_F2.Text = "สีแดง >>รูปไม่ครบตามเงื่อนไข | สีเทา >>บิลยกเลิก | Double Click >>ดูรายการบิล | Double Click ที่รูป >>ดูรูปขนาดใหญ่";
                    break;
                default:
                    break;
            }

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = pBchName;
            RadDropDownList_Branch.ValueMember = pBchID;
            RadDropDownList_Branch.Enabled = false;

            if (_pTextOrImage == "1") BillOut_Class.SetHeadGrid_ForShowImageReport(RadGridView_Show, pColume); else BillOut_Class.SetHeadGrid_ForShowtextReport(RadGridView_Show);

        }

        void SetIS()
        {
            radDropDownList_IS.DataSource = BillOutClass.Find_IS();
            radDropDownList_IS.DisplayMember = "SPC_INVENTCOSTCENTERNAME";
            radDropDownList_IS.ValueMember = "SPC_INVENTCOSTCENTERID";
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //เลือกสาขา
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //LoadData
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            string pIS = "";
            string pCon = "M%";
            if (pBchID == "NUM") pCon = "D%";

            if (radCheckBox_SPC.Checked == true) pCon = "D%";
            if (radCheckBox_MN.Checked == true) pCon = "M%";
            if (RadCheckBox_Branch.Checked == true) pCon = RadDropDownList_Branch.SelectedValue.ToString();
            if (radCheckBox_IS.Checked == true) pIS = radDropDownList_IS.SelectedValue.ToString();


            DataTable dt = BillOut_Class.FindBillForReport_ByTypeBill(_pType,
                radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), pCon, pIS);

            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลบิล");
                RadGridView_Show.DataSource = dt;
                dt.AcceptChanges();
                this.Cursor = Cursors.Default;
                return;
            }

            if (_pTextOrImage == "1")
            {
                BillOut_Class.FindImageBill_ForShowImageReport(dt, RadGridView_Show, pColume);
            }
            else
            {
                RadGridView_Show.DataSource = dt;
                dt.AcceptChanges();
            }

            this.Cursor = Cursors.Default;
        }
        //open
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Bill_SPC":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_SPC_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_SPC_Path"].Value.ToString());
                    break;
                case "Bill_MN":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_MN_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_END":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_END_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_END_Path"].Value.ToString());
                    break;
                case "Bill_OLD":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_MN_Path"].Value.ToString());
                    break;
                case "Bill_OLDSPC":
                    ImageClass.OpenShowImage(RadGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Count"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["Bill_OLDSPC_Path"].Value.ToString());
                    break;
                default:
                    //ถ้ายังไม่ Import ให้เปิดหน้าจอ Import ก่อน
                    if (RadGridView_Show.CurrentRow.Cells["STA_IMPORT"].Value.ToString() == "0")
                    {
                        JOB.Bill.BillAXAddJOB frmImport = new JOB.Bill.BillAXAddJOB
                            (RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                            RadGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString(), "0", "", "", "", "", "",
                            RadGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["ComMN"].Value.ToString());
                        frmImport.ShowDialog(this);
                        if (frmImport.sSendDialog == "1")
                        {
                            RadGridView_Show.CurrentRow.Cells["STA_IMPORT"].Value = "1";
                        }
                    }
                    else
                    {

                        BillOut_Detail frm = new BillOut_Detail(RadGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString());
                        if (frm.ShowDialog(this) == DialogResult.OK)
                        {
                            RadGridView_Show.CurrentRow.Cells["Bill_SpcType"].Value = 1;
                        }
                    }
                    break;
            }
        }

        //Date Change
        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Date Change
        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadCheckBox_SPC_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_SPC.CheckState == CheckState.Checked)
            {
                RadCheckBox_Branch.CheckState = CheckState.Unchecked;
                RadCheckBox_Branch.Enabled = false;
            }
            else
            {
                RadCheckBox_Branch.Enabled = true;
            }
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายงานหมวด " + _pType, RadGridView_Show, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pType);
        }
    }
}
