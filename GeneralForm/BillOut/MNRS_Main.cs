﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Drawing.Printing;
using static PC_Shop24Hrs.Controllers.PrintClass;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class MNRS_Main : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        private DataTable dt = new DataTable();
        private int iRowDT = 0;

        private DataTable dtPrintAgain = new DataTable();

        private string pCopyColor;
        private int pCopy;
        private string pRmk;
        private int iBox;
        private string pUpPRINTCOUNT;
        private string sNumBox;
        private double sumLINEAMOUNT;

        private string pPackingID;
        private string pPackingName;
        readonly string _pTypeOpen;
        readonly string _pWH;

        readonly string _pMN;
        readonly string _pMNNAME;
        readonly string _pJOBID;

        private DataTable dtDate;
        readonly PrintController printController = new StandardPrintController();
        public MNRS_Main(string pTypeOpen, string pWH, string pMN = "", string pMNNAME = "", string pJOBID = "")
        {
            InitializeComponent();

            _pTypeOpen = pTypeOpen;
            _pWH = pWH;
            _pMN = pMN;
            _pJOBID = pJOBID;
            _pMNNAME = pMNNAME;

            RadCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Printer);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Invent);

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 270));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "ปริมาณ", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALEPRICE", "ราคา/หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ยอดรวม", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("LINENUM", "ลำดับ"));

            radGridView_Show.MasterTemplate.ShowHeaderCellButtons = false;
            radGridView_Show.MasterTemplate.ShowGroupedColumns = false;
            radGridView_Show.MasterTemplate.EnableFiltering = false;

            radButton_New.ButtonElement.ShowBorder = true;
            radButton_New.ButtonElement.ToolTipText = "สร้างเอกสารใหม่";
            radButton_CheckBranch.ButtonElement.ShowBorder = true;

            if (_pTypeOpen == "SHOP")
            {
                radLabel_Dpt.Text = "สาขา"; radButton_CheckBranch.ButtonElement.ToolTipText = "ค้นหาสาขา";
            }
            else
            {
                radLabel_Dpt.Text = "แผนก"; radButton_CheckBranch.ButtonElement.ToolTipText = "ค้นหาแผนก";
            }

            ////การแสดงยอดรวม แถวบนสุด
            GridViewSummaryItem summaryItem = new GridViewSummaryItem
            {
                Name = "LINEAMOUNT",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };

            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);

            SetPrinter();
        }
        private void MNRS_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;
            RadCheckBox_Apv.CheckState = CheckState.Unchecked;

            dtDate = Controllers.DateTimeSettingClass.GetDateTime();

            ClearData();
            SetDGV();
            SetInvent();

            if (_pJOBID != "")
            {
                radTextBox_BranchID.Text = _pMN;
                radLabel_BranchName.Text = _pMNNAME;
                radLabel_JobID.Text = _pJOBID;
                radTextBox_BranchID.Enabled = false;
                radTextBox_Item.Focus();
            }

        }
        void SetDGV()
        {
            dt = I_Class.SHOP_RO_MNRS_FindPrint("1", "");
            radGridView_Show.DataSource = dt;
        }
        //set printer
        void SetPrinter()
        {
            PrinterSettings prt = new PrinterSettings();
            String pkInstalledPrinters;
            int iRow = 0;

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                radDropDownList_Printer.Items.Add(pkInstalledPrinters);
                if (prt.PrinterName == pkInstalledPrinters)
                {
                    iRow = i;
                }
            }

            radDropDownList_Printer.SelectedIndex = iRow;
        }

        //Check Branch
        private void RadButton_CheckBranch_Click(object sender, EventArgs e)
        {
            DataTable dt;
            if (_pTypeOpen == "SHOP") dt = BranchClass.GetBranchAll_ByConditions("'1'", " '1'", ""); else dt = Models.DptClass.GetDptForShow();

            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV()
            {
                dtData = dt
            };

            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radTextBox_BranchID.Text = frm.pID;
                radLabel_BranchName.Text = frm.pDesc;
                radTextBox_Item.Focus();
            }
            else
            {
                radLabel_BranchName.Text = "";
                radTextBox_BranchID.Text = "";
                radTextBox_BranchID.Focus();
            }
        }
        //Set Invent
        void SetInvent()
        {
            string pCon_WH = "";
            string whDefault = "RETAILAREA";
            if (_pWH != "")
            {
                if (_pWH == "COM-MN") pCon_WH = $@"  AND INVENTLOCATIONID IN ( '{_pWH}','WH-A','RETAILAREA'  ) ";
                else pCon_WH = $@"  AND INVENTLOCATIONID = '{_pWH}'  ";

                whDefault = _pWH;
            }
            radDropDownList_Invent.DataSource = Models.InventClass.Find_InventSupc(pCon_WH);

            radDropDownList_Invent.ValueMember = "INVENTLOCATIONID";
            radDropDownList_Invent.DisplayMember = "INVENTLOCATIONNAME";

            radDropDownList_Invent.SelectedValue = whDefault;

        }
        //ClearData
        void ClearData()
        {
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            if (dtPrintAgain.Rows.Count > 0) dtPrintAgain.Rows.Clear();
            dt.AcceptChanges();
            iRowDT = 0;

            pRmk = "";
            iBox = 0;
            pUpPRINTCOUNT = "0";
            sNumBox = "";
            pCopy = 0;
            radLabel_JobID.Text = "";
            radLabel_X.Visible = false;
            radDropDownList_Printer.Enabled = true;
            radDropDownList_Invent.Enabled = true;
            radButton_CheckBranch.Enabled = true;
            radTextBox_BranchID.Enabled = true; radTextBox_BranchID.Text = "";
            radLabel_Qty.Text = "";
            radLabel_BranchName.Text = "";
            radTextBox_Item.Enabled = true; radTextBox_Item.Text = "";
            radLabel_Docno.Text = "";
            radTextBox_BranchID.Focus();

            if (_pMN == "D177")
            {
                radTextBox_BranchID.Text = "177"; RadCheckBox_Apv.Checked = true; CheckBranch_Dpt();
                radButton_CheckBranch.Enabled = false;
                radTextBox_Item.Focus();
            }
        }

        #region "SetRowGrid"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        //close sorting
        private void RadGridView_Show_SortChanging(object sender, GridViewCollectionChangingEventArgs e)
        {
            e.Cancel = false;
        }
        //Bch
        private void RadTextBox_BranchID_KeyUp(object sender, KeyEventArgs e)
        {
            CheckBranch_Dpt();
        }
        void CheckBranch_Dpt()
        {
            if (radTextBox_BranchID.Text.Length == 3)
            {
                if (_pTypeOpen == "SHOP")
                {
                    DataTable dtBch = BranchClass.GetDetailBranchByID("MN" + radTextBox_BranchID.Text.Trim());
                    if (dtBch.Rows.Count > 0)
                    {
                        if (dtBch.Rows[0]["BRANCH_ID"].ToString() == "MN000")
                        {
                            radTextBox_BranchID.Text = "";
                            radTextBox_BranchID.Focus();
                            return;
                        }
                        radTextBox_BranchID.Enabled = false;
                        radTextBox_BranchID.Text = dtBch.Rows[0]["BRANCH_ID"].ToString();
                        radLabel_BranchName.Text = dtBch.Rows[0]["BRANCH_NAME"].ToString();
                        radTextBox_Item.Focus();
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("สาขา");
                        radTextBox_BranchID.Text = "";
                        radTextBox_BranchID.Focus();
                        return;
                    }
                }
                else
                {
                    DataTable dtBch = ConnectionClass.SelectSQL_Main($@" DptClass_GetDptDetail_ByDptID 'D{radTextBox_BranchID.Text.Trim()}' ");
                    if (dtBch.Rows.Count > 0)
                    {
                        radTextBox_BranchID.Enabled = false;
                        radTextBox_BranchID.Text = dtBch.Rows[0]["NUM"].ToString();
                        radLabel_BranchName.Text = dtBch.Rows[0]["DESCRIPTION"].ToString();
                        radTextBox_Item.Focus();
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("แผนก");
                        radTextBox_BranchID.Text = "";
                        radTextBox_BranchID.Focus();
                        return;
                    }
                }
            }
            else
            {
                return;
            }
        }
        //textbox event
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Shift && e.KeyCode == Keys.F11) ShiftF11();

            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (radTextBox_Item.Text == "")
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                        radTextBox_Item.Focus();
                        return;
                    }
                    //check branch
                    if ((radTextBox_BranchID.Text == "") || (radLabel_BranchName.Text == ""))
                    {
                        if (_pTypeOpen == "SHOP") MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("สาขา - ชื่อสาขา");
                        else MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("แผนก - ชื่อแผนก");

                        radTextBox_BranchID.SelectAll();
                        radTextBox_BranchID.Focus();
                        return;
                    }
                    EnterTextbox();
                    break;

                case Keys.PageDown:
                    if (radTextBox_Item.Text == "") return;
                    if ((radTextBox_BranchID.Text == "D177") && (SystemClass.SystemDptID == "D011"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การเบิกสินค้าของฝ่ายผลิต-โรงงาน{Environment.NewLine}ไม่สามารถใช้งานฟังก์ชั่นนี้ได้"); radTextBox_Item.SelectAll(); return;
                    }

                    int qty;
                    try
                    {
                        qty = Convert.ToInt32((radTextBox_Item.Text));
                    }
                    catch (Exception) { qty = 0; }
                    //check qty
                    if (qty <= 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("จำนวนที่ระบุต้องมากกว่า 0 เท่านั้น.");
                        radTextBox_Item.SelectAll();
                        radTextBox_Item.Focus();
                        return;
                    }
                    radLabel_X.Visible = true;
                    radLabel_Qty.Text = qty.ToString();
                    radTextBox_Item.SelectAll();
                    radTextBox_Item.Focus();
                    break;

                case Keys.F2: F2(); break;

                case Keys.F3: F3(); break;

                case Keys.Home: Home(); break;

                default:
                    return;
            }
        }
        // Sava HD
        void SaveHD(DataTable dtBarcode, double priceNet, double weight)
        {
            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID("MNRS", "-", "MNRS", "1");
            string staInStall = "0";
            if (radDropDownList_Invent.SelectedValue.ToString() == "COM-MN") staInStall = "1";
            if (SystemClass.SystemComMinimart == "1") staInStall = "1";

            string T = ConnectionClass.ExecuteSQL2Str_Main(
                I_Class.SHOP_RO_MNRS_SaveHD(billMaxNO, radTextBox_BranchID.Text, radLabel_BranchName.Text, radDropDownList_Invent.SelectedValue.ToString(), staInStall, _pJOBID, "0"),
                I_Class.SHOP_RO_MNRS_SaveDT(1, billMaxNO, dtBarcode, priceNet, weight));

            if (T == "")
            {
                radDropDownList_Invent.Enabled = false;
                radTextBox_BranchID.Enabled = false;
                radButton_CheckBranch.Enabled = false;
                iRowDT = 1;

                DataRow row = dt.NewRow();
                row[0] = dtBarcode.Rows[0]["ITEMBARCODE"].ToString();
                row[1] = dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString();
                row[2] = weight;
                row[3] = dtBarcode.Rows[0]["UNITID"].ToString();
                row[4] = Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString());
                row[5] = priceNet;
                row[6] = iRowDT;
                dt.Rows.Add(row);
                dt.AcceptChanges();

                radLabel_X.Visible = false;
                radLabel_Qty.Text = "";
                radLabel_Docno.Text = billMaxNO;
                radTextBox_Item.Text = ""; radTextBox_Item.Focus();
                radGridView_Show.Rows[dt.Rows.Count - 1].IsCurrent = true;
            }
            else MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Save DT
        void SaveDT(DataTable dtBarcode, double priceNet, double weight)
        {
            iRowDT += 1;

            string T = ConnectionClass.ExecuteSQL_Main(I_Class.SHOP_RO_MNRS_SaveDT(iRowDT, radLabel_Docno.Text, dtBarcode, priceNet, weight));// ConnectionClass.ExecuteSQL_Main(sqlDT);
            if (T == "")
            {
                DataRow dr = dt.NewRow();
                dr[0] = dtBarcode.Rows[0]["ITEMBARCODE"].ToString();
                dr[1] = dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString();
                dr[2] = weight;
                dr[3] = dtBarcode.Rows[0]["UNITID"].ToString();
                dr[4] = Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString());
                dr[5] = priceNet;
                dr[6] = iRowDT;
                dt.Rows.Add(dr);
                dt.AcceptChanges();

                //billAmount += priceNet;
                radLabel_X.Visible = false;
                radLabel_Qty.Text = "";
                radTextBox_Item.Text = ""; radTextBox_Item.Focus();
                radGridView_Show.Rows[dt.Rows.Count - 1].IsCurrent = true;
            }
            else
            {
                iRowDT -= 1;
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //Update DT
        void UpdateDT(int iRows, DataTable dtBarcode, double priceNet, double weight)
        {
            //iRowDT += 1;

            string T = ConnectionClass.ExecuteSQL_Main(I_Class.SHOP_RO_MNRS_UpdateDT(radLabel_Docno.Text, dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                priceNet, weight, weight * Convert.ToDouble(dtBarcode.Rows[0]["QTY"].ToString())));// 
            if (T == "")
            {

                dt.Rows[iRows]["QTY"] = weight + double.Parse(dt.Rows[iRows]["QTY"].ToString());
                dt.Rows[iRows]["LINEAMOUNT"] = priceNet + double.Parse(dt.Rows[iRows]["LINEAMOUNT"].ToString());
                dt.AcceptChanges();

                //billAmount += priceNet;
                radLabel_X.Visible = false;
                radLabel_Qty.Text = "";
                radTextBox_Item.Text = ""; radTextBox_Item.Focus();
                radGridView_Show.Rows[dt.Rows.Count - 1].IsCurrent = true;
            }
            else
            {
                iRowDT -= 1;
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //New Doment
        private void RadButton_New_Click(object sender, EventArgs e)
        {
            if (dt.Rows.Count > 0)
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการไม่ใช้เอกสารที่ค้างอยู่ {Environment.NewLine}{radLabel_Docno.Text} ?.") == DialogResult.No) return;
            }

            ClearData();
        }
        //Select Change
        private void RadDropDownList_Invent_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_BranchID.Focus();
        }
        //F2
        void F2()
        {
            if (dt.Rows.Count == 0) ClearData();

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกบิลเลขที่ " + radLabel_Docno.Text + " ?.") == DialogResult.No) return;

            ClearData();
        }
        //F3
        void F3()
        {

            if (dt.Rows.Count == 0) return;

            if (radGridView_Show.CurrentRow.Cells[0].Value.ToString() == "") return;

            string desc = dt.Rows[radGridView_Show.CurrentRow.Index]["ITEMBARCODE"] + " " + dt.Rows[radGridView_Show.CurrentRow.Index]["SPC_ITEMNAME"] +
                " จำนวน  " + dt.Rows[radGridView_Show.CurrentRow.Index]["QTY"] + "  " + dt.Rows[radGridView_Show.CurrentRow.Index]["UNITID"];

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกรายการ " + Environment.NewLine + desc + " ?.") == DialogResult.No) return;

            string T = I_Class.SHOP_RO_MNRS_Void(radLabel_Docno.Text, int.Parse(dt.Rows[radGridView_Show.CurrentRow.Index]["LINENUM"].ToString()));
            if (T == "")
            {
                dt.Rows[radGridView_Show.CurrentRow.Index].Delete();
                dt.AcceptChanges();
            }
            else MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //F3 On DGV
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3: F3(); break;
                case Keys.Escape: radTextBox_Item.Focus(); break;
                default:
                    break;
            }
        }
        //Enter
        void EnterTextbox()
        {
            double priceNet = 0;
            double weight;

            DataTable dtBarcode = BillOutClass.Find_ItemRoByItembarcode(radTextBox_Item.Text);
            if (dtBarcode.Rows.Count == 0)
            {
                if (radTextBox_Item.Text.Length == 13) dtBarcode = BillOutClass.Find_ItemRoByItembarcode(radTextBox_Item.Text.Substring(0, 7)); else return;

                if (dtBarcode.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_Item.SelectAll();
                    radTextBox_Item.Focus();
                    return;
                }
                else
                {
                    //Checkd กลุ่มเบิก
                    if (dtBarcode.Rows[0]["STA24"].ToString() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าที่ระบุไม่อยู่ในการตั้งค่าการเบิก เช็คบาร์โค้ดใหม่อีกครั้ง");
                        radTextBox_Item.SelectAll();
                        radTextBox_Item.Focus();
                        return;
                    }

                    DataTable checkPriceNet_Weight = ItembarcodeClass.FindPriceNetWeight_ByBarcode(radTextBox_Item.Text,
                        dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString(),
                        Convert.ToDouble(Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString())), 0);

                    priceNet = Convert.ToDouble(checkPriceNet_Weight.Rows[0]["priceNet"].ToString());
                    weight = Convert.ToDouble(checkPriceNet_Weight.Rows[0]["weight"].ToString());
                }
            }
            else
            {
                //Checkd กลุ่มเบิก
                if (dtBarcode.Rows[0]["GROUPMAIN"].ToString() == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าที่ระบุไม่อยู่ในการตั้งค่าการเบิก เช็คบาร์โค้ดใหม่อีกครั้ง");
                    radTextBox_Item.SelectAll();
                    radTextBox_Item.Focus();
                    return;
                }

                //check สถานะสินค้า
                if (dtBarcode.Rows[0]["SPC_ITEMACTIVE"].ToString() == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าบาร์โค้ดนี้ อยู่ในสถานะไม่เคลื่อนไหว ไม่สามารถทำรายการเบิกได้.");
                    radTextBox_Item.SelectAll();
                    radTextBox_Item.Focus();
                    return;
                }

                try
                { weight = Convert.ToDouble(radLabel_Qty.Text); }
                catch (Exception)
                { weight = 1; }

                switch (dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString())
                {
                    case "1": //ราคาบังคับใช้
                        priceNet = weight * (Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString()));
                        break;

                    case "2"://ราคาแก้ไขได้
                        FormShare.InputData frm = new FormShare.InputData("0",
                             dtBarcode.Rows[0]["ITEMBARCODE"].ToString() + " - " + dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                             "จำนวน", dtBarcode.Rows[0]["UNITID"].ToString())
                        {
                            pInputData = String.Format("0.00", dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString())
                        };

                        if (frm.ShowDialog(this) == DialogResult.Yes)
                        {
                            priceNet = weight * Convert.ToDouble(frm.pInputData);
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าเป็นประเภทราคาแบบแก้ไขได้ ต้องระบุราคาเพื่อทำบิลเท่านั้น.");
                            return;
                        }
                        break;
                    case "3"://ราคาเครื่องชั้่ง
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าเป็นประเภทเครื่องชั่ง ให้ระบุบาร์โค้ด 13 หลักเพื่อทำบิลเท่านั้น.");
                        return;
                    case "4":
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าเป็นประเภทเครื่องชั่ง[น้ำหนัก] ให้ระบุบาร์โค้ด 13 หลักเพื่อทำบิลเท่านั้น.");
                        return;
                    case "5":
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าเป็นประเภทเครื่องชั่ง[เครื่องชั่งแพ็ค] ให้ระบุบาร์โค้ด 13 หลักเพื่อทำบิลเท่านั้น.");
                        return;
                    default:
                        break;
                }

            }

            if (Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString()) == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ยังไม่มีการตั้งราคาสินค้าในช่องเงินสด{Environment.NewLine}ให้ดำเนินการให้เรียบร้อยก่อนทำบิลเบิก");
                return;
            }
            //Save
            if (dt.Rows.Count == 0)
            {
                if (radLabel_Docno.Text == "") SaveHD(dtBarcode, priceNet, weight); else SaveDT(dtBarcode, priceNet, weight);
            }
            else
            {
                //Check Line ซ้ำ
                if (radTextBox_BranchID.Text == "D177")
                {
                    int cRow = 0; string sta = "0";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ITEMBARCODE"].ToString() == dtBarcode.Rows[0]["ITEMBARCODE"].ToString())
                        {
                            cRow = i; sta = "1";
                        }
                    }

                    if (sta == "0") SaveDT(dtBarcode, priceNet, weight); else UpdateDT(cRow, dtBarcode, priceNet, weight);
                }
                else
                {
                    SaveDT(dtBarcode, priceNet, weight);
                }
            }

        }
        //Shift F11
        void ShiftF11()
        {
            if (radGridView_Show.Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ยังมีรายการบิลค้างในหน้าจอ ไม่สามารถพิมพิ์บิลซ้ำได้ จบบิลนี้ให้เรียบร้อยก่อนใช้งานเมนูนี้.");
                return;
            }

            string pFix = "0";
            if (_pWH == "WH-ICE") pFix = "1";

            MNRS_HOME frm = new MNRS_HOME("", "1", pFix);
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                pRmk = frm.pRmk;
                iBox = frm.pQtyBox;
                pPackingID = frm.pPackingID;
                pPackingName = frm.pPackingName;
                dtPrintAgain = frm.dt;

                I_Class.SHOP_RO_MNRS_UpdateBox("0", dtPrintAgain.Rows[0]["DOCNO"].ToString(), frm.pQtyBox, frm.pRmk, 0, pPackingID, pPackingName, "");
                PrintDocBill("1");
            }
        }
        //Home
        void Home()
        {
            if (dt.Rows.Count == 0) return;

            string pFix = "0";
            if (_pWH == "WH-ICE") pFix = "1";

            MNRS_HOME frm = new MNRS_HOME(radLabel_Docno.Text, "0", pFix);

            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                sumLINEAMOUNT = I_Class.SHOP_RO_MNRS_SumAmount(radLabel_Docno.Text);
                pRmk = frm.pRmk;
                iBox = frm.pQtyBox;
                pPackingID = frm.pPackingID;
                pPackingName = frm.pPackingName;
                string T = I_Class.SHOP_RO_MNRS_UpdateBox("1", radLabel_Docno.Text, frm.pQtyBox, frm.pRmk, sumLINEAMOUNT, pPackingID, pPackingName,
                    AX_SendData.Save_EXTERNALLIST_SPC_SHIPMENTINVOICE(radLabel_Docno.Text, dtDate.Rows[0]["Date"].ToString(), radTextBox_BranchID.Text, radLabel_BranchName.Text)); //ConnectionClass.ExecuteSQL_Main(sqlUp);
                if (T == "")
                {
                    //SPC_EXTERNALLIST
                    //SmartRO_Class.SendData_EXTERNALLIST(radLabel_Docno.Text, dtDate.Rows[0]["Date"].ToString(), radTextBox_BranchID.Text, radLabel_BranchName.Text);
                    //AX_SendData.Save_EXTERNALLIST_SPC_SHIPMENTINVOICE(radLabel_Docno.Text, dtDate.Rows[0]["Date"].ToString(), radTextBox_BranchID.Text, radLabel_BranchName.Text);
                    //Send AX
                    if (RadCheckBox_Apv.Checked == true)
                    {
                        string T_AX = ConnectionClass.ExecuteMain_AX_24_SameTime(
                            I_Class.SHOP_RO_MNRS_UpdateSendAX(radLabel_Docno.Text, "SHOP_RO_MNRSHD", "STAUPAX", "DOCNO"),
                            AX_SendData.Save_SPC_INVENTJOURNALTABLE(I_Class.SHOP_RO_MNRS_FindForSendAX(radLabel_Docno.Text)));
                        if (T_AX != "") MsgBoxClass.MsgBoxShow_SaveStatus(T_AX);
                    }
                    PrintDocBill("0");
                    if (_pJOBID != "") this.Close();
                }
                else MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
            else
            {
                radTextBox_Item.Focus();
                return;
            }


        }
        //print
        void PrintDocBill(string pStaPrint)
        {

            if (pStaPrint == "1")// print Again
            {
                PrintDocument_printBillAgain.PrintController = printController;
                PrintDocument_printBoxAgain.PrintController = printController;

                PrintDocument_printBillAgain.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;
                PrintDocument_printBoxAgain.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;

                //printDocument1.PrintController = printController;
                //printDocument1.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;
                pCopy = Convert.ToInt32(dtPrintAgain.Rows[0]["PRINTCOUNT"].ToString()) + 1;

                pCopyColor = "บิลฟ้า"; pUpPRINTCOUNT = "1";
                PrintDocument_printBillAgain.Print();

                if (SystemClass.SystemDptID != "D179")
                {
                    if (SystemClass.SystemDptID != "D156")
                    {
                        pCopyColor = "บิลชมพู"; pUpPRINTCOUNT = "0";
                        PrintDocument_printBillAgain.Print();

                        for (int i = 0; i < iBox; i++)
                        {
                            sNumBox = "จำนวนลัง  " + Convert.ToString(i + 1) + "/" + Convert.ToString(iBox);
                            PrintDocument_printBoxAgain.Print();
                            //if (SystemClass.SystemDptID == "D062") //ปริ้นรหัส+ชื่อสาขา
                            //{
                            //    for (int k = 0; k < 2; k++)
                            //    {
                            //        printDocument1.Print();
                            //    }
                            //}
                        }
                    }

                }
            }
            else
            {
                PrintDocument_printBill.PrintController = printController;
                PrintDocument_printBox.PrintController = printController;

                PrintDocument_printBill.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;
                PrintDocument_printBox.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;

                //printDocument1.PrintController = printController;
                //printDocument1.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;
                pCopy = 1;

                pCopyColor = "บิลฟ้า"; pUpPRINTCOUNT = "1";
                PrintDocument_printBill.Print();

                if (SystemClass.SystemDptID != "D179")
                {
                    if (SystemClass.SystemDptID != "D156")
                    {
                        pCopyColor = "บิลชมพู"; pUpPRINTCOUNT = "0";
                        PrintDocument_printBill.Print();

                        for (int i = 0; i < iBox; i++)
                        {
                            sNumBox = "จำนวนลัง  " + Convert.ToString(i + 1) + "/" + Convert.ToString(iBox);
                            PrintDocument_printBox.Print();
                            //if (SystemClass.SystemDptID == "D062") //ปริ้นรหัส+ชื่อสาขา
                            //{
                            //    for (int k = 0; k < 2; k++)
                            //    {
                            //        printDocument1.Print();
                            //    }
                            //}
                        }
                    }
                }
            }


            ClearData();
        }
        //print Bill
        private void PrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {

            //barcode.Data = radLabel_Docno.Text;
            //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            //int Y = 0;
            //e.Graphics.DrawString("พิมพ์ครั้งที่ " + pCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + pCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString(radTextBox_BranchID.Text + "-" + radLabel_BranchName.Text, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            //Y += 67;
            //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("คลังเบิก " + radDropDownList_Invent.SelectedItem.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            //{
            //    Y += 20;
            //    e.Graphics.DrawString((i + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[i].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
            //                      radGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + ")   " +
            //                      radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
            //         SystemClass.printFont, Brushes.Black, 1, Y);
            //    Y += 15;
            //    e.Graphics.DrawString(radGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
            //        SystemClass.printFont, Brushes.Black, 0, Y);
            //}

            //Y += 15;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 15;
            //e.Graphics.DrawString("จำนวนทั้งหมด " + iBox.ToString() + "  ลัง", SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 15;
            //e.Graphics.DrawString("รวม " + radGridView_Show.Rows.Count.ToString() + "  รายการ/" +
            //    "ยอดเงิน " + (sumLINEAMOUNT.ToString("#,#0.00")).ToString() + "  บาท", SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 20;
            //e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 15;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 30;
            //e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            //StringFormat stringFormat = new StringFormat()
            //{
            //    Alignment = StringAlignment.Near,
            //    LineAlignment = StringAlignment.Near
            //};
            //e.Graphics.DrawString(pRmk, SystemClass.printFont, Brushes.Black, rect1, stringFormat);

            //e.Graphics.PageUnit = GraphicsUnit.Inch;
            Var_Print_MNRS var = new Var_Print_MNRS
            {
                SDocno = radLabel_Docno.Text,
                SBranch = radTextBox_BranchID.Text + "-" + radLabel_BranchName.Text,
                SNumBox = sNumBox,
                SLocation = radDropDownList_Invent.SelectedItem.Text,
                SpCopyColor = pCopyColor,
                SpRmk = pRmk,
                SpUpPRINTCOUNT = pUpPRINTCOUNT,
                SPersonSave = SystemClass.SystemUserID + "-" + SystemClass.SystemUserName,
                SPersonPacking = pPackingID + "-" + pPackingName,
                IBox = iBox,
                SpCopy = pCopy,
                SumLINEAMOUNT = sumLINEAMOUNT,
                SdataDetail = dt
            };

            PrintClass.Print_MNRS("bill", e, var);


            if (pUpPRINTCOUNT == "1")
            {
                string upStr = string.Format(@"UPDATE SHOP_RO_MNRSHD SET PRINTCOUNT = '" + pCopy + "' WHERE DOCNO = '" + radLabel_Docno.Text + "' ");
                ConnectionClass.ExecuteSQL_Main(upStr);
            }
        }
        //Print Box
        private void PrintDocument2_PrintPage(object sender, PrintPageEventArgs e)
        {
            //barcode.Data = radLabel_Docno.Text;
            //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            //int Y = 0;

            //Y += 20;
            //e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + radDropDownList_Invent.SelectedValue.ToString() + "]", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString(radTextBox_BranchID.Text + "-" + radLabel_BranchName.Text, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            //Y += 67;
            //e.Graphics.DrawString(sNumBox, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //e.Graphics.PageUnit = GraphicsUnit.Inch;
            Var_Print_MNRS var = new Var_Print_MNRS
            {
                SDocno = radLabel_Docno.Text,
                SBranch = radTextBox_BranchID.Text + "-" + radLabel_BranchName.Text,
                SNumBox = sNumBox,
                SLocation = radDropDownList_Invent.SelectedValue.ToString(),
            };
            PrintClass.Print_MNRS("box", e, var);
        }

        private void RadTextBox_BranchID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Shift && e.KeyCode == Keys.F11) ShiftF11();
        }

        private void PrintDocument3_PrintPage(object sender, PrintPageEventArgs e)
        {
            //barcode.Data = dtPrintAgain.Rows[0]["DOCNO"].ToString();
            //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            //int Y = 0;

            //e.Graphics.DrawString("พิมพ์ครั้งที่ " + pCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา/แผนก [" + pCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString(dtPrintAgain.Rows[0]["BRANCH_ID"].ToString() + "-" + dtPrintAgain.Rows[0]["BRANCH_NAME"].ToString(), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            //Y += 67;
            //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("คลังเบิก " + dtPrintAgain.Rows[0]["INVENT"].ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //for (int i = 0; i < dtPrintAgain.Rows.Count; i++)
            //{
            //    Y += 20;
            //    e.Graphics.DrawString((i + 1).ToString() + ".(" + (Convert.ToDouble(dtPrintAgain.Rows[i]["QTY"].ToString()).ToString("#,#0.00")).ToString() + " X " +
            //                      dtPrintAgain.Rows[i]["UNITID"].ToString() + ")   " +
            //                      dtPrintAgain.Rows[i]["ITEMBARCODE"].ToString(),
            //         SystemClass.printFont, Brushes.Black, 1, Y);
            //    Y += 15;
            //    e.Graphics.DrawString(dtPrintAgain.Rows[i]["SPC_ITEMNAME"].ToString(),
            //        SystemClass.printFont, Brushes.Black, 0, Y);
            //}

            //Y += 15;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 15;
            //e.Graphics.DrawString("รวม " + dtPrintAgain.Rows.Count.ToString() + "  รายการ/" +
            //    "ยอดเงิน " + (Convert.ToDouble(dtPrintAgain.Rows[0]["BILLAMOUNT"].ToString()).ToString("#,#0.00")).ToString() + "  บาท", SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 20;
            //e.Graphics.DrawString("ผู้บันทึก : " + dtPrintAgain.Rows[0]["WHOIDINS"].ToString() + "-" + dtPrintAgain.Rows[0]["WHONAMEINS"].ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
            //Y += 15;
            //e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 30;
            //e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            //StringFormat stringFormat = new StringFormat()
            //{
            //    Alignment = StringAlignment.Near,
            //    LineAlignment = StringAlignment.Near
            //};
            //e.Graphics.DrawString(pRmk, SystemClass.printFont, Brushes.Black, rect1, stringFormat);

            //e.Graphics.PageUnit = GraphicsUnit.Inch;

            Var_Print_MNRS var = new Var_Print_MNRS
            {
                SDocno = dtPrintAgain.Rows[0]["DOCNO"].ToString(),
                SBranch = dtPrintAgain.Rows[0]["BRANCH_ID"].ToString() + "-" + dtPrintAgain.Rows[0]["BRANCH_NAME"].ToString(),
                SNumBox = sNumBox,
                SLocation = dtPrintAgain.Rows[0]["INVENT"].ToString(),
                SpCopyColor = pCopyColor,
                SpRmk = pRmk,
                SpUpPRINTCOUNT = pUpPRINTCOUNT,
                SPersonSave = dtPrintAgain.Rows[0]["WHOIDINS"].ToString() + "-" + dtPrintAgain.Rows[0]["WHONAMEINS"].ToString(),
                SPersonPacking = pPackingID + "-" + pPackingName,
                IBox = iBox,
                SpCopy = pCopy,
                SumLINEAMOUNT = Convert.ToDouble(dtPrintAgain.Rows[0]["BILLAMOUNT"].ToString()),
                SdataDetail = dtPrintAgain,
            };

            PrintClass.Print_MNRS("bill", e, var);
            if (pUpPRINTCOUNT == "1")
            {
                string upStr = string.Format(@"UPDATE SHOP_RO_MNRSHD SET PRINTCOUNT = '" + pCopy + "' WHERE DOCNO = '" + dtPrintAgain.Rows[0]["DOCNO"].ToString() + "' ");
                ConnectionClass.ExecuteSQL_Main(upStr);
            }
        }

        //Print
        private void PrintDocument_printBoxAgain_PrintPage(object sender, PrintPageEventArgs e)
        {
            //barcode.Data = dtPrintAgain.Rows[0]["DOCNO"].ToString();
            //Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            //int Y = 0;

            //Y += 20;
            //e.Graphics.DrawString("บิลเบิกสินค้าส่งสาขา [" + dtPrintAgain.Rows[0]["INVENT"].ToString() + "]", SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString(dtPrintAgain.Rows[0]["BRANCH_ID"].ToString() + "-" + dtPrintAgain.Rows[0]["BRANCH_NAME"].ToString(), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            //Y += 20;
            //e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            //Y += 67;
            //e.Graphics.DrawString(sNumBox, SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 20;
            //e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            //Y += 15;
            //e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //e.Graphics.PageUnit = GraphicsUnit.Inch;
            Var_Print_MNRS var = new Var_Print_MNRS
            {
                SDocno = dtPrintAgain.Rows[0]["DOCNO"].ToString(),
                SBranch = dtPrintAgain.Rows[0]["BRANCH_ID"].ToString() + "-" + dtPrintAgain.Rows[0]["BRANCH_NAME"].ToString(),
                SNumBox = sNumBox,
                SLocation = dtPrintAgain.Rows[0]["INVENT"].ToString(),
            };
            PrintClass.Print_MNRS("box", e, var);

        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeOpen);
        }

        private void PrintDocument1_PrintPage_1(object sender, PrintPageEventArgs e)
        {
            //int Y = 0;
            //Y += 20;

            //string branchId;
            //string branchName;
            //if(pCopy == 1)
            //{
            //    branchId = radTextBox_BranchID.Text;
            //    branchName = radLabel_BranchName.Text;
            //}
            //else
            //{
            //    branchId = dtPrintAgain.Rows[0]["BRANCH_ID"].ToString(); 
            //    branchName = dtPrintAgain.Rows[0]["BRANCH_NAME"].ToString();
            //}
            //e.Graphics.DrawString(branchId, new Font(new FontFamily("Tahoma"), 50), Brushes.Black, 0, Y);
            //Y += 70;

            //Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            //StringFormat stringFormat = new StringFormat()
            //{
            //    Alignment = StringAlignment.Near,
            //    LineAlignment = StringAlignment.Near
            //};
            //e.Graphics.DrawString(branchName, new Font(new FontFamily("Tahoma"), 40), Brushes.Black, rect1, stringFormat);
        }
    }
}

