﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class MNRS_HOME : Telerik.WinControls.UI.RadForm
    {
        public DataTable dt = new DataTable();

        public string pRmk;
        public int pQtyBox;
        public string pPackingID;
        public string pPackingName;
        readonly string sDocno;
        readonly string sType;//0 ปกติ 1 พิมซ้ำ
        readonly string sFix;//0 บังคับ ระบุทั้งหมด 1 ไม่บังคับ

        public MNRS_HOME(string _sDocno, string _sType, string _sFix)
        {
            InitializeComponent();
            sDocno = _sDocno;
            sType = _sType;
            sFix = _sFix;
        }
        //load form
        private void MNRS_HOME_Load(object sender, EventArgs e)
        {

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radTextBox_EmpName.ReadOnly = true;

            if (sType == "0")
            {
                this.Text = "ระบุหมายเหตุ - จำนวนลัง";
                radLabel_Show.Visible = false;
                radTextBox_Docno1.Enabled = false;
                radTextBox_Docno2.Enabled = false;
                radTextBox_Docno1.Text = sDocno.Replace("MNRS", "").Substring(0, 6);
                radTextBox_Docno2.Text = sDocno.Replace("MNRS", "").Substring(6, 6);
                radTextBox_QtyBox.Text = ""; radTextBox_QtyBox.Enabled = false;
                radTextBox_Remark.Text = ""; radTextBox_Remark.Enabled = true;
                radTextBox_EmpID.Text = ""; radTextBox_EmpID.Enabled = false;
                radTextBox_EmpName.Text = ""; radTextBox_EmpName.Enabled = false;
                radButton_Save.Enabled = false;
                radTextBox_Remark.Focus();
            }
            else
            {
                this.Text = "ระบุเลขที่บิลที่ต้องการพิมพ์ซ้ำ";
                radLabel_Show.Visible = true;
                radTextBox_Docno1.Text = DateTimeSettingClass.GetDateShortForRunningBill();
                radTextBox_Docno1.Enabled = false;
                radTextBox_Docno2.Enabled = true; radTextBox_Docno2.Text = "";
                radTextBox_QtyBox.Enabled = false; radTextBox_QtyBox.Text = "";
                radTextBox_Remark.Enabled = false; radTextBox_Remark.Text = "";
                radTextBox_EmpID.Enabled = false; radTextBox_EmpID.Text = "";
                radTextBox_EmpName.Enabled = false; radTextBox_EmpName.Text = "";
                radTextBox_Docno2.Focus();
            }

        }
        //No
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //OK
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (sFix == "0")
            {
                if (radTextBox_Remark.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("หมายเหตุ");
                    radTextBox_Remark.Focus();
                    return;
                }

                if ((radTextBox_QtyBox.Text == "") || (Convert.ToInt32(radTextBox_QtyBox.Text) == 0))
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("จำนวนลัง");
                    radTextBox_QtyBox.Focus();
                    return;
                }

                if ((radTextBox_EmpID.Text == "") || (radTextBox_EmpName.Text == ""))
                {
                    MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ผู้จัดบิล");
                    radTextBox_EmpID.Focus();
                    return;
                }

                pRmk = radTextBox_Remark.Text;
                pQtyBox = Convert.ToInt32(radTextBox_QtyBox.Text);
                pPackingID = radTextBox_EmpID.Text;
                pPackingName = radTextBox_EmpName.Text;
            }
            else
            {

                pRmk = radTextBox_Remark.Text;
                pQtyBox = 0;
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Enter Docno
        private void RadTextBox_Docno2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Docno2.Text == "")
                {
                    radTextBox_Docno2.Focus();
                    return;
                }

                radTextBox_Docno2.Text = Convert.ToInt32(radTextBox_Docno2.Text).ToString("D6");
                string docno = "MNRS" + radTextBox_Docno1.Text + radTextBox_Docno2.Text;

                dt = I_Class.SHOP_RO_MNRS_FindPrint("0", docno);
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่บิล");
                    radTextBox_Docno2.SelectAll();
                    radTextBox_Docno2.Focus();
                    return;
                }

                if (dt.Rows[0]["STADOC"].ToString() == "3")
                {
                    MessageBox.Show("เอกสารถูกยกเลิกเรียบร้อยแล้ว ไม่สามารถพิมพ์ซ้ำได้",
                        SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    radTextBox_Docno2.SelectAll();
                    radTextBox_Docno2.Focus();
                    return;
                }

                radTextBox_Remark.Text = dt.Rows[0]["REMARK"].ToString(); radTextBox_Remark.Enabled = true;
                radTextBox_QtyBox.Text = Convert.ToInt32(dt.Rows[0]["BOXQTY"].ToString()).ToString("#,#"); radTextBox_QtyBox.Enabled = true;
                radTextBox_EmpID.Text = dt.Rows[0]["WHOIDPACKING"].ToString();
                radTextBox_EmpName.Text = dt.Rows[0]["WHONAMEPACKING"].ToString();
                radButton_Save.Focus();
            }

        }
        //Set Enter
        private void RadTextBox_Remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Remark.Enabled = false;
                if (SystemClass.SystemDptID == "D179")
                {
                    radTextBox_EmpID.Text = SystemClass.SystemUserID_M; radTextBox_EmpID.Enabled = false;
                    radTextBox_EmpName.Text = SystemClass.SystemUserName; radTextBox_EmpName.Enabled = false;
                    radTextBox_QtyBox.Text = "1"; radTextBox_QtyBox.Enabled = false;
                    radButton_Save.Enabled = true;
                    radButton_Save.Focus();
                    return;
                }
                radTextBox_QtyBox.Enabled = true;
                radTextBox_QtyBox.Focus();
            }
        }
        //Qty Box
        private void RadTextBox_QtyBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_QtyBox.Enabled = false;
                if (SystemClass.SystemDptID != "D062")
                {
                    radTextBox_EmpID.Text = SystemClass.SystemUserID_M; radTextBox_EmpID.Enabled = false;
                    radTextBox_EmpName.Text = SystemClass.SystemUserName; radTextBox_EmpName.Enabled = false;
                    radButton_Save.Enabled = true; radButton_Save.Focus();
                    return;
                }
                else
                {
                    radTextBox_EmpID.Enabled = true;
                    radTextBox_EmpID.Focus();
                }
       
            }
        }
        // ผู้จัดบิล
        private void RadTextBox_EmpID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_EmpID.Text == "")
                {
                    radTextBox_EmpID.Focus();
                    return;
                }

                dt = Class.Models.EmplClass.GetEmployee_Altnum(radTextBox_EmpID.Text);
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน");
                    radTextBox_Docno2.SelectAll();
                    radTextBox_Docno2.Focus();
                    return;
                }

                radTextBox_EmpID.Enabled = false;
                radTextBox_EmpID.Text = dt.Rows[0]["EMPLID"].ToString();
                radTextBox_EmpName.Text = dt.Rows[0]["SPC_NAME"].ToString();
                radButton_Save.Enabled = true;
                radButton_Save.Focus();
            }
        }
    }
}
