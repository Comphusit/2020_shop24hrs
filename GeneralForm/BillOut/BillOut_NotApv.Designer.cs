﻿namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    partial class BillOut_NotApv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillOut_NotApv));
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radRadioButton_MNRM = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNCM_3 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNCM_2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNCM_1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNCM_0 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_IDM = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNRZ = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNRH = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNRG = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNRB = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNPZ = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNRR = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNOT = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_MNIO = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_FAL = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_IF = new Telerik.WinControls.UI.RadRadioButton();
            this.RadRadioButton_MNRS = new Telerik.WinControls.UI.RadRadioButton();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_IDM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNPZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNOT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_FAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_IF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_MNRS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 597);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            this.RadGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.radRadioButton_MNRM);
            this.panel1.Controls.Add(this.radRadioButton_MNCM_3);
            this.panel1.Controls.Add(this.radRadioButton_MNCM_2);
            this.panel1.Controls.Add(this.radRadioButton_MNCM_1);
            this.panel1.Controls.Add(this.radRadioButton_MNCM_0);
            this.panel1.Controls.Add(this.radRadioButton_IDM);
            this.panel1.Controls.Add(this.radRadioButton_MNRZ);
            this.panel1.Controls.Add(this.radRadioButton_MNRH);
            this.panel1.Controls.Add(this.radRadioButton_MNRG);
            this.panel1.Controls.Add(this.radRadioButton_MNRB);
            this.panel1.Controls.Add(this.radRadioButton_MNPZ);
            this.panel1.Controls.Add(this.radRadioButton_MNRR);
            this.panel1.Controls.Add(this.radRadioButton_MNOT);
            this.panel1.Controls.Add(this.radRadioButton_MNIO);
            this.panel1.Controls.Add(this.radRadioButton_FAL);
            this.panel1.Controls.Add(this.radRadioButton_IF);
            this.panel1.Controls.Add(this.RadRadioButton_MNRS);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 2;
            // 
            // radRadioButton_MNRM
            // 
            this.radRadioButton_MNRM.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNRM.Location = new System.Drawing.Point(16, 296);
            this.radRadioButton_MNRM.Name = "radRadioButton_MNRM";
            this.radRadioButton_MNRM.Size = new System.Drawing.Size(65, 19);
            this.radRadioButton_MNRM.TabIndex = 47;
            this.radRadioButton_MNRM.Text = "MNRM";
            // 
            // radRadioButton_MNCM_3
            // 
            this.radRadioButton_MNCM_3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNCM_3.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_MNCM_3.Location = new System.Drawing.Point(16, 443);
            this.radRadioButton_MNCM_3.Name = "radRadioButton_MNCM_3";
            this.radRadioButton_MNCM_3.Size = new System.Drawing.Size(149, 19);
            this.radRadioButton_MNCM_3.TabIndex = 46;
            this.radRadioButton_MNCM_3.Text = "MNCM [ลูกค้ามือถือ]";
            // 
            // radRadioButton_MNCM_2
            // 
            this.radRadioButton_MNCM_2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNCM_2.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_MNCM_2.Location = new System.Drawing.Point(16, 418);
            this.radRadioButton_MNCM_2.Name = "radRadioButton_MNCM_2";
            this.radRadioButton_MNCM_2.Size = new System.Drawing.Size(141, 19);
            this.radRadioButton_MNCM_2.TabIndex = 45;
            this.radRadioButton_MNCM_2.Text = "MNCM [ลูกค้าคอม]";
            // 
            // radRadioButton_MNCM_1
            // 
            this.radRadioButton_MNCM_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNCM_1.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_MNCM_1.Location = new System.Drawing.Point(16, 393);
            this.radRadioButton_MNCM_1.Name = "radRadioButton_MNCM_1";
            this.radRadioButton_MNCM_1.Size = new System.Drawing.Size(113, 19);
            this.radRadioButton_MNCM_1.TabIndex = 44;
            this.radRadioButton_MNCM_1.Text = "MNCM [นำทิ้ง]";
            // 
            // radRadioButton_MNCM_0
            // 
            this.radRadioButton_MNCM_0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNCM_0.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_MNCM_0.Location = new System.Drawing.Point(16, 368);
            this.radRadioButton_MNCM_0.Name = "radRadioButton_MNCM_0";
            this.radRadioButton_MNCM_0.Size = new System.Drawing.Size(124, 19);
            this.radRadioButton_MNCM_0.TabIndex = 43;
            this.radRadioButton_MNCM_0.Text = "MNCM [ส่งซ่อม]";
            // 
            // radRadioButton_IDM
            // 
            this.radRadioButton_IDM.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_IDM.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_IDM.Location = new System.Drawing.Point(16, 343);
            this.radRadioButton_IDM.Name = "radRadioButton_IDM";
            this.radRadioButton_IDM.Size = new System.Drawing.Size(49, 19);
            this.radRadioButton_IDM.TabIndex = 42;
            this.radRadioButton_IDM.Text = "IDM";
            // 
            // radRadioButton_MNRZ
            // 
            this.radRadioButton_MNRZ.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNRZ.Location = new System.Drawing.Point(16, 318);
            this.radRadioButton_MNRZ.Name = "radRadioButton_MNRZ";
            this.radRadioButton_MNRZ.Size = new System.Drawing.Size(61, 19);
            this.radRadioButton_MNRZ.TabIndex = 41;
            this.radRadioButton_MNRZ.Text = "MNRZ";
            // 
            // radRadioButton_MNRH
            // 
            this.radRadioButton_MNRH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNRH.Location = new System.Drawing.Point(16, 271);
            this.radRadioButton_MNRH.Name = "radRadioButton_MNRH";
            this.radRadioButton_MNRH.Size = new System.Drawing.Size(63, 19);
            this.radRadioButton_MNRH.TabIndex = 40;
            this.radRadioButton_MNRH.Text = "MNRH";
            // 
            // radRadioButton_MNRG
            // 
            this.radRadioButton_MNRG.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNRG.Location = new System.Drawing.Point(16, 246);
            this.radRadioButton_MNRG.Name = "radRadioButton_MNRG";
            this.radRadioButton_MNRG.Size = new System.Drawing.Size(63, 19);
            this.radRadioButton_MNRG.TabIndex = 39;
            this.radRadioButton_MNRG.Text = "MNRG";
            // 
            // radRadioButton_MNRB
            // 
            this.radRadioButton_MNRB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNRB.Location = new System.Drawing.Point(16, 221);
            this.radRadioButton_MNRB.Name = "radRadioButton_MNRB";
            this.radRadioButton_MNRB.Size = new System.Drawing.Size(62, 19);
            this.radRadioButton_MNRB.TabIndex = 38;
            this.radRadioButton_MNRB.Text = "MNRB";
            // 
            // radRadioButton_MNPZ
            // 
            this.radRadioButton_MNPZ.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNPZ.Location = new System.Drawing.Point(16, 196);
            this.radRadioButton_MNPZ.Name = "radRadioButton_MNPZ";
            this.radRadioButton_MNPZ.Size = new System.Drawing.Size(60, 19);
            this.radRadioButton_MNPZ.TabIndex = 37;
            this.radRadioButton_MNPZ.Text = "MNPZ";
            // 
            // radRadioButton_MNRR
            // 
            this.radRadioButton_MNRR.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNRR.Location = new System.Drawing.Point(16, 171);
            this.radRadioButton_MNRR.Name = "radRadioButton_MNRR";
            this.radRadioButton_MNRR.Size = new System.Drawing.Size(63, 19);
            this.radRadioButton_MNRR.TabIndex = 36;
            this.radRadioButton_MNRR.Text = "MNRR";
            // 
            // radRadioButton_MNOT
            // 
            this.radRadioButton_MNOT.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNOT.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_MNOT.Location = new System.Drawing.Point(16, 146);
            this.radRadioButton_MNOT.Name = "radRadioButton_MNOT";
            this.radRadioButton_MNOT.Size = new System.Drawing.Size(62, 19);
            this.radRadioButton_MNOT.TabIndex = 35;
            this.radRadioButton_MNOT.Text = "MNOT";
            // 
            // radRadioButton_MNIO
            // 
            this.radRadioButton_MNIO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_MNIO.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_MNIO.Location = new System.Drawing.Point(16, 121);
            this.radRadioButton_MNIO.Name = "radRadioButton_MNIO";
            this.radRadioButton_MNIO.Size = new System.Drawing.Size(60, 19);
            this.radRadioButton_MNIO.TabIndex = 34;
            this.radRadioButton_MNIO.Text = "MNIO";
            // 
            // radRadioButton_FAL
            // 
            this.radRadioButton_FAL.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_FAL.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_FAL.Location = new System.Drawing.Point(16, 96);
            this.radRadioButton_FAL.Name = "radRadioButton_FAL";
            this.radRadioButton_FAL.Size = new System.Drawing.Size(45, 19);
            this.radRadioButton_FAL.TabIndex = 33;
            this.radRadioButton_FAL.Text = "FAL";
            // 
            // radRadioButton_IF
            // 
            this.radRadioButton_IF.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRadioButton_IF.ForeColor = System.Drawing.Color.Black;
            this.radRadioButton_IF.Location = new System.Drawing.Point(16, 70);
            this.radRadioButton_IF.Name = "radRadioButton_IF";
            this.radRadioButton_IF.Size = new System.Drawing.Size(41, 19);
            this.radRadioButton_IF.TabIndex = 32;
            this.radRadioButton_IF.Text = "I-F";
            // 
            // RadRadioButton_MNRS
            // 
            this.RadRadioButton_MNRS.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadRadioButton_MNRS.Location = new System.Drawing.Point(16, 46);
            this.RadRadioButton_MNRS.Name = "RadRadioButton_MNRS";
            this.RadRadioButton_MNRS.Size = new System.Drawing.Size(61, 19);
            this.RadRadioButton_MNRS.TabIndex = 31;
            this.RadRadioButton_MNRS.Text = "MNRS";
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 484);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 30;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_F2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 606);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(617, 19);
            this.radLabel_F2.TabIndex = 52;
            this.radLabel_F2.Text = "<html>[แสดงเฉพาะบิลที่ Import แล้วเท่านั้น] สีแดง &gt;&gt; ยังไม่รับสินค้า | Doub" +
    "le Click &gt;&gt; รายการบิลและตรวจบิล</html>";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_excel,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator1});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.ToolTipText = "Export To Excel";
            this.radButtonElement_excel.UseCompatibleTextRendering = false;
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.UseCompatibleTextRendering = false;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // BillOutNotApv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BillOut_NotApv";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "รายการบิลค้างตรวจทั้งหมด";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BillOut_NotApv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNCM_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_IDM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNPZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNRR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNOT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_MNIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_FAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_IF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadRadioButton_MNRS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.RadRadioButton RadRadioButton_MNRS;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_FAL;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_IF;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNIO;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNOT;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNRR;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNRG;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNRB;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNPZ;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNRH;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNRZ;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_IDM;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNCM_1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNCM_0;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNCM_3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNCM_2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_MNRM;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
    }
}
