﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class MNRO_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();

        //readonly string _pType; // 0 shop 1 supc

        public string pDocno;
        readonly string _pCase;

        public MNRO_FindData(string pCase = "")
        {
            InitializeComponent();
            _pCase = pCase;
            //_pType = pType;
        }
        //Load
        private void MNRO_FindData_Load(object sender, EventArgs e)
        {
            radDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy"; radDateTimePicker_End.CustomFormat = "dd/MM/yyyy";

            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButtonElement_CheckAll.ShowBorder = true; RadButtonElement_CheckAll.ToolTipText = "อนุมัติบิลทั้งหมด";
            if (_pCase != "D062")
            {
                RadButtonElement_CheckAll.Enabled = false;
            }

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_GrpMain.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAAPV", "อนุมัติ")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN", "กลุ่มหลัก", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPSUB", "กลุ่มย่อย", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAPRCDOC", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDUPD", "ผู้ยืนยัน", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOCANCLE", "ผู้ยกเลิก", 200)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDAPV", "ผู้อนุมัติ", 200)));
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;
            RadGridView_ShowHD.Columns["STAAPV"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("QTY", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            RadGridView_ShowDT.Columns["QTY"].FormatString = "{0:#,##0.00}";

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000")
            {
                RadCheckBox_Branch.Checked = false;
                dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                RadCheckBox_Branch.Enabled = true;
                RadButton_Choose.Visible = false;
                radCheckBox_Apv.Checked = true;
                radLabel_Detail.Visible = true;
            }
            else
            {
                RadCheckBox_Branch.Checked = true;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadButton_Choose.Visible = true;
                RadCheckBox_Branch.Enabled = false;
                radCheckBox_Apv.Checked = false;
                radLabel_Detail.Visible = false;
            }


            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_End.Value = DateTime.Now;
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-2);

            Set_GropMain();
            SetDGV_HD(" TOP 100 ");
        }
        //Set HD
        void SetDGV_HD(string _pTop)
        {
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }

            string bch = "";
            if (RadCheckBox_Branch.Checked == true) bch = RadDropDownList_Branch.SelectedValue.ToString();
            string grpMain = "";
            if (RadCheckBox_GrpMain.Checked == true) grpMain = radDropDownList_MainGroup.SelectedValue.ToString();
            string apv = "";
            if (radCheckBox_Apv.Checked == true) apv = "1";

            dtDataHD = I_Class.SHOP_RO_FindHD(_pTop, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                bch, grpMain, apv);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            dtDataDT = I_Class.SHOP_RO_FindDT("0", _pDocno);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD("");
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
                return;
            }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        //sub grp
        void Set_GropMain()
        {
            DataTable dt = I_Class.Find_GroupMain("'1'");
            radDropDownList_MainGroup.DataSource = dt;
            radDropDownList_MainGroup.ValueMember = "GROUPRO_ID";
            radDropDownList_MainGroup.DisplayMember = "GROUPRO_NAME";
        }
        //Group Main
        private void RadCheckBox_GrpMain_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpMain.Checked == true) radDropDownList_MainGroup.Enabled = true; else radDropDownList_MainGroup.Enabled = false;
        }
        //คลิกรับบิล
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (e.Column.Name)
            {
                case "STAAPV":
                    if (SystemClass.SystemBranchID != "MN000") return;
                    if (RadGridView_ShowHD.Rows.Count == 0) return;

                    string docno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();

                    if (RadGridView_ShowHD.CurrentRow.Cells["STAPRCDOC"].Value.ToString() == "0")
                    {
                        if ((MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(docno, " ยืนยันการอนุมัติบิลที่สาขายังไม่ได้ยืนยัน") == DialogResult.No)) return;
                    }
                    else
                    {
                        if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(docno, "อนุมัติ") == DialogResult.No) return;
                    }

                    string sqlUp = $"UPDATE SHOP_RO_MNROHD SET STAAPV =  '1' WHERE DOCNO = '{ docno }' ";
                    string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["STAAPV"].Value = "1";
                        dtDataHD.AcceptChanges();
                    }
                    break;
                default:
                    break;
            }

        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_CheckAll_Click(object sender, EventArgs e)
        {
            if (_pCase == "D062")
            {
                if (SystemClass.SystemBranchID != "MN000") return;
                //if (SystemClass.SystemDptID == "D062") return; 
                if (RadGridView_ShowHD.Rows.Count == 0) return;

                ArrayList sql = new ArrayList();

                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["STAPRCDOC"].Value.ToString() == "1") //break;
                    //if (RadGridView_ShowHD.CurrentRow.Cells["STAPRCDOC"].Value.ToString() == "1")
                    {
                        string docno = RadGridView_ShowHD.Rows[i].Cells["DOCNO"].Value.ToString();
                        string sqlUp = $@"  UPDATE  SHOP_RO_MNROHD
                                            SET     STAAPV =  '1',
                                                    DATEAPV = CONVERT(NVARCHAR,GETDATE(),25),
		                                            WHOIDAPV = '{SystemClass.SystemUserID}',
		                                            WHONAMEAPV = '{SystemClass.SystemUserName}'
                                            WHERE   DOCNO = '{ docno }' ";
                        sql.Add(sqlUp);
                    }
                }

                if (sql.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีรายการที่สามารถบันทึกรับออเดอร์ได้{Environment.NewLine}เช็คข้อมูลที่แสดงทั้งหมดอีกครั้ง"); return;
                }

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@" ยืนยันการอนุมัติบิลทั้ง {Convert.ToString(sql.Count)} บิล ?{Environment.NewLine}[เฉพาะรายการที่สาขายืนยันแล้วเท่านั้น] ") == DialogResult.No) return;

                string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "") SetDGV_HD("");
            }

        }
    }
}
