﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class BillOut_NotApv : Telerik.WinControls.UI.RadForm
    {
        //Load
        public BillOut_NotApv()
        {
            InitializeComponent();
        }
        //Load Main
        private void BillOut_NotApv_Load(object sender, EventArgs e)
        {
            RadButton_Search.ButtonElement.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;
            radButtonElement_excel.ShowBorder = true;RadButtonElement_pdt.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Bill_SpcType", "ตรวจ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("TYPERETURN", "มีซาก"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Bill_ID", "เลขที่บิล", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE_INS", "วันที่บิล", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TypeBill", "งบเบิก", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RMK", "หมายเหตุ", 700));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ผู้ทำบิล", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STABCH", "สถานะรับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Bill_TypeBchRemark", "หมายเหตุการรับ", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BCHWhoName", "ผู้รับสินค้า", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BCHDATE", "วันที่รับ", 120));


            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "Bill_SpcType = 0 ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_Show.Columns["Bill_SpcType"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "STABCH = 0 ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_Show.Columns["Bill_ID"].ConditionalFormattingObjectList.Add(obj1);

        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            this.Cursor = Cursors.WaitCursor;
            if (RadRadioButton_MNRS.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRS"); }
            if (radRadioButton_IF.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("I"); }
            if (radRadioButton_FAL.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("FAL"); }
            if (radRadioButton_MNOT.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNOT"); }
            if (radRadioButton_MNIO.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNIO"); }

            if (radRadioButton_MNRR.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRR"); }
            if (radRadioButton_MNPZ.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNPZ"); }
            if (radRadioButton_MNRG.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRG"); }
            if (radRadioButton_MNRB.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRB"); }
            if (radRadioButton_MNRH.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRH"); }
            if (radRadioButton_MNRZ.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRZ"); }
            if (radRadioButton_IDM.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("IDM"); }
            if (radRadioButton_MNCM_0.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNCM_0"); }
            if (radRadioButton_MNCM_1.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNCM_1"); }
            if (radRadioButton_MNCM_2.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNCM_2"); }
            if (radRadioButton_MNCM_3.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNCM_3"); }

            if (radRadioButton_MNRM.IsChecked == true) { dt = BillOut_Class.FindAllBillNotApv_ByTypeID("MNRM"); }
            RadGridView_Show.DataSource = dt;
            this.Cursor = Cursors.Default;
        }

        //Check BillOut
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            BillOut_Detail frm = new BillOut_Detail(RadGridView_Show.CurrentRow.Cells["Bill_ID"].Value.ToString());
            if (frm.ShowDialog(this) == DialogResult.OK)
            {
                RadGridView_Show.CurrentRow.Cells["Bill_SpcType"].Value = 1;
            }
        }

        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.G))
            {
                var control = sender as RadGridView;
                var toggleStatus = !(control.ShowFilteringRow);
                control.ShowFilteringRow = toggleStatus;
                control.ShowHeaderCellButtons = !toggleStatus;
            }
        }
        //
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}
