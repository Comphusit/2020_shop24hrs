﻿namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    partial class BillOut_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillOut_Detail));
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radGridView_Bill = new Telerik.WinControls.UI.RadGridView();
            this.radTextBox_BillID = new Telerik.WinControls.UI.RadTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_WH = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_IS = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Add = new Telerik.WinControls.UI.RadButton();
            this.radLabel_BillDptCreate = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Job = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Money = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_D062 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_MN = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_D003 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radToggleSwitch_StaOld = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radToggleSwitch_StaBillCancle = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BillGroup = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Create = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radButton_Close = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radButton_Money = new Telerik.WinControls.UI.RadButton();
            this.radButton_CancleBill = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bill.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_WH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_IS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BillDptCreate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Money)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_D062)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_D003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_StaOld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_StaBillCancle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BillGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Create)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Money)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CancleBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radGridView_Show);
            this.radPanel2.Location = new System.Drawing.Point(9, 579);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(641, 143);
            this.radPanel2.TabIndex = 100;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(641, 143);
            this.radGridView_Show.TabIndex = 16;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting_1);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radGridView_Bill
            // 
            this.radGridView_Bill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Bill.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Bill.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView_Bill.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Bill.Name = "radGridView_Bill";
            // 
            // 
            // 
            this.radGridView_Bill.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Bill.Size = new System.Drawing.Size(778, 170);
            this.radGridView_Bill.TabIndex = 17;
            this.radGridView_Bill.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Bill_ViewCellFormatting);
            this.radGridView_Bill.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Bill_ConditionalFormattingFormShown);
            this.radGridView_Bill.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Bill_FilterPopupRequired);
            this.radGridView_Bill.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Bill_FilterPopupInitialized);
            // 
            // radTextBox_BillID
            // 
            this.radTextBox_BillID.AutoSize = false;
            this.radTextBox_BillID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_BillID.Location = new System.Drawing.Point(46, 4);
            this.radTextBox_BillID.Name = "radTextBox_BillID";
            this.radTextBox_BillID.Size = new System.Drawing.Size(169, 33);
            this.radTextBox_BillID.TabIndex = 0;
            this.radTextBox_BillID.Text = "MNIO192001000000";
            this.radTextBox_BillID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BillID_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_WH);
            this.panel1.Controls.Add(this.radLabel_IS);
            this.panel1.Controls.Add(this.radButton_Add);
            this.panel1.Controls.Add(this.radLabel_BillDptCreate);
            this.panel1.Controls.Add(this.radButton_Job);
            this.panel1.Controls.Add(this.radLabel_Money);
            this.panel1.Controls.Add(this.radLabel_D062);
            this.panel1.Controls.Add(this.radLabel_MN);
            this.panel1.Controls.Add(this.radLabel_D003);
            this.panel1.Controls.Add(this.radLabel13);
            this.panel1.Controls.Add(this.radLabel12);
            this.panel1.Controls.Add(this.radLabel11);
            this.panel1.Controls.Add(this.radLabel10);
            this.panel1.Controls.Add(this.radToggleSwitch_StaOld);
            this.panel1.Controls.Add(this.radToggleSwitch_StaBillCancle);
            this.panel1.Controls.Add(this.radLabel_Remark);
            this.panel1.Controls.Add(this.radLabel8);
            this.panel1.Controls.Add(this.radLabel_BillGroup);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radLabel_Create);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_BranchName);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBox_BillID);
            this.panel1.Location = new System.Drawing.Point(9, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(778, 400);
            this.panel1.TabIndex = 16;
            // 
            // radLabel_WH
            // 
            this.radLabel_WH.AutoSize = false;
            this.radLabel_WH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_WH.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_WH.Location = new System.Drawing.Point(669, 325);
            this.radLabel_WH.Name = "radLabel_WH";
            this.radLabel_WH.Size = new System.Drawing.Size(100, 23);
            this.radLabel_WH.TabIndex = 66;
            this.radLabel_WH.Text = "หมวดบิล";
            this.radLabel_WH.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel_IS
            // 
            this.radLabel_IS.AutoSize = false;
            this.radLabel_IS.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_IS.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_IS.Location = new System.Drawing.Point(569, 68);
            this.radLabel_IS.Name = "radLabel_IS";
            this.radLabel_IS.Size = new System.Drawing.Size(93, 23);
            this.radLabel_IS.TabIndex = 31;
            // 
            // radButton_Add
            // 
            this.radButton_Add.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_Add.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Add.Location = new System.Drawing.Point(12, 5);
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.Size = new System.Drawing.Size(30, 30);
            this.radButton_Add.TabIndex = 65;
            this.radButton_Add.Text = "radButton3";
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // radLabel_BillDptCreate
            // 
            this.radLabel_BillDptCreate.AutoSize = false;
            this.radLabel_BillDptCreate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_BillDptCreate.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BillDptCreate.Location = new System.Drawing.Point(221, 9);
            this.radLabel_BillDptCreate.Name = "radLabel_BillDptCreate";
            this.radLabel_BillDptCreate.Size = new System.Drawing.Size(430, 23);
            this.radLabel_BillDptCreate.TabIndex = 45;
            this.radLabel_BillDptCreate.Text = "แผนกรับผิดชอบ";
            // 
            // radButton_Job
            // 
            this.radButton_Job.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton_Job.ForeColor = System.Drawing.Color.Black;
            this.radButton_Job.Location = new System.Drawing.Point(668, 353);
            this.radButton_Job.Name = "radButton_Job";
            this.radButton_Job.Size = new System.Drawing.Size(101, 32);
            this.radButton_Job.TabIndex = 43;
            this.radButton_Job.Text = "JOB";
            this.radButton_Job.ThemeName = "Fluent";
            this.radButton_Job.Click += new System.EventHandler(this.RadButton_Job_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Job.GetChildAt(0))).Text = "JOB";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Job.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Job.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Job.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Job.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Job.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Job.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Job.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton_Job.GetChildAt(0).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Italic;
            // 
            // radLabel_Money
            // 
            this.radLabel_Money.AutoSize = false;
            this.radLabel_Money.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Money.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Money.Location = new System.Drawing.Point(85, 351);
            this.radLabel_Money.Name = "radLabel_Money";
            this.radLabel_Money.Size = new System.Drawing.Size(566, 43);
            this.radLabel_Money.TabIndex = 42;
            this.radLabel_Money.Text = "หัก";
            this.radLabel_Money.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel_D062
            // 
            this.radLabel_D062.AutoSize = false;
            this.radLabel_D062.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_D062.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_D062.Location = new System.Drawing.Point(85, 308);
            this.radLabel_D062.Name = "radLabel_D062";
            this.radLabel_D062.Size = new System.Drawing.Size(566, 43);
            this.radLabel_D062.TabIndex = 41;
            this.radLabel_D062.Text = "เบิกภายใน";
            this.radLabel_D062.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_D062.TextWrap = false;
            // 
            // radLabel_MN
            // 
            this.radLabel_MN.AutoSize = false;
            this.radLabel_MN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_MN.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_MN.Location = new System.Drawing.Point(85, 259);
            this.radLabel_MN.Name = "radLabel_MN";
            this.radLabel_MN.Size = new System.Drawing.Size(684, 43);
            this.radLabel_MN.TabIndex = 40;
            this.radLabel_MN.Text = "สาขา";
            this.radLabel_MN.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel_D003
            // 
            this.radLabel_D003.AutoSize = false;
            this.radLabel_D003.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_D003.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_D003.Location = new System.Drawing.Point(85, 210);
            this.radLabel_D003.Name = "radLabel_D003";
            this.radLabel_D003.Size = new System.Drawing.Size(684, 43);
            this.radLabel_D003.TabIndex = 39;
            this.radLabel_D003.Text = "รปภ";
            this.radLabel_D003.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.Location = new System.Drawing.Point(14, 350);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(72, 19);
            this.radLabel13.TabIndex = 38;
            this.radLabel13.Text = "รายการหัก";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(13, 307);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(66, 19);
            this.radLabel12.TabIndex = 37;
            this.radLabel12.Text = "ผู้ตรวจบิล";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel11.Location = new System.Drawing.Point(13, 258);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(50, 19);
            this.radLabel11.TabIndex = 36;
            this.radLabel11.Text = "ผู้รับบิล";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(14, 209);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(63, 19);
            this.radLabel10.TabIndex = 35;
            this.radLabel10.Text = "รปภ./CN";
            // 
            // radToggleSwitch_StaOld
            // 
            this.radToggleSwitch_StaOld.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch_StaOld.Location = new System.Drawing.Point(669, 67);
            this.radToggleSwitch_StaOld.Name = "radToggleSwitch_StaOld";
            this.radToggleSwitch_StaOld.OffText = "ไม่มีซาก";
            this.radToggleSwitch_StaOld.OnText = "มีซาก";
            this.radToggleSwitch_StaOld.ReadOnly = true;
            this.radToggleSwitch_StaOld.Size = new System.Drawing.Size(101, 33);
            this.radToggleSwitch_StaOld.TabIndex = 34;
            this.radToggleSwitch_StaOld.Tag = "";
            this.radToggleSwitch_StaOld.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.None;
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch_StaOld.GetChildAt(0))).ThumbOffset = 81;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(22)))), ((int)(((byte)(11)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(31)))), ((int)(((byte)(21)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(41)))), ((int)(((byte)(28)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).Text = "มีซาก";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(1)))), ((int)(((byte)(1)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(1))).Text = "ไม่มีซาก";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch_StaOld.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radToggleSwitch_StaBillCancle
            // 
            this.radToggleSwitch_StaBillCancle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radToggleSwitch_StaBillCancle.Location = new System.Drawing.Point(669, 24);
            this.radToggleSwitch_StaBillCancle.Name = "radToggleSwitch_StaBillCancle";
            this.radToggleSwitch_StaBillCancle.OffText = "บิลปกติ";
            this.radToggleSwitch_StaBillCancle.OnText = "บิลยกเลิก";
            this.radToggleSwitch_StaBillCancle.ReadOnly = true;
            this.radToggleSwitch_StaBillCancle.Size = new System.Drawing.Size(101, 33);
            this.radToggleSwitch_StaBillCancle.TabIndex = 33;
            this.radToggleSwitch_StaBillCancle.Tag = "";
            this.radToggleSwitch_StaBillCancle.ToggleStateMode = Telerik.WinControls.UI.ToggleStateMode.None;
            ((Telerik.WinControls.UI.RadToggleSwitchElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0))).ThumbOffset = 81;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(49)))), ((int)(((byte)(77)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(73)))), ((int)(((byte)(97)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(110)))), ((int)(((byte)(116)))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).Text = "บิลยกเลิก";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Red;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(1))).Text = "บิลปกติ";
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.ToggleSwitchPartElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            ((Telerik.WinControls.UI.ToggleSwitchThumbElement)(this.radToggleSwitch_StaBillCancle.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.AutoSize = false;
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Remark.Location = new System.Drawing.Point(85, 102);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(685, 102);
            this.radLabel_Remark.TabIndex = 32;
            this.radLabel_Remark.Text = "หมายเหตุ";
            this.radLabel_Remark.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(14, 101);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 19);
            this.radLabel8.TabIndex = 31;
            this.radLabel8.Text = "หมายเหตุ";
            // 
            // radLabel_BillGroup
            // 
            this.radLabel_BillGroup.AutoSize = false;
            this.radLabel_BillGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_BillGroup.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BillGroup.Location = new System.Drawing.Point(498, 70);
            this.radLabel_BillGroup.Name = "radLabel_BillGroup";
            this.radLabel_BillGroup.Size = new System.Drawing.Size(65, 23);
            this.radLabel_BillGroup.TabIndex = 30;
            this.radLabel_BillGroup.Text = "หมวดบิล";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(427, 72);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(59, 19);
            this.radLabel6.TabIndex = 29;
            this.radLabel6.Text = "หมวดบิล";
            // 
            // radLabel_Create
            // 
            this.radLabel_Create.AutoSize = false;
            this.radLabel_Create.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Create.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Create.Location = new System.Drawing.Point(85, 70);
            this.radLabel_Create.Name = "radLabel_Create";
            this.radLabel_Create.Size = new System.Drawing.Size(336, 23);
            this.radLabel_Create.TabIndex = 28;
            this.radLabel_Create.Text = "ผู้ทำเบิก";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.AutoSize = false;
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Date.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Date.Location = new System.Drawing.Point(498, 41);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(164, 23);
            this.radLabel_Date.TabIndex = 27;
            this.radLabel_Date.Text = "วันที่บิล";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(427, 43);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(51, 19);
            this.radLabel2.TabIndex = 24;
            this.radLabel2.Text = "วันที่บิล";
            // 
            // radLabel_BranchName
            // 
            this.radLabel_BranchName.AutoSize = false;
            this.radLabel_BranchName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_BranchName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BranchName.Location = new System.Drawing.Point(85, 41);
            this.radLabel_BranchName.Name = "radLabel_BranchName";
            this.radLabel_BranchName.Size = new System.Drawing.Size(336, 23);
            this.radLabel_BranchName.TabIndex = 26;
            this.radLabel_BranchName.Text = "แผนกเบิก";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(14, 72);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(56, 19);
            this.radLabel3.TabIndex = 25;
            this.radLabel3.Text = "ผู้ทำเบิก";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(13, 42);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(66, 19);
            this.radLabel1.TabIndex = 23;
            this.radLabel1.Text = "แผนกเบิก";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radGridView_Bill);
            this.panel2.Location = new System.Drawing.Point(9, 408);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(778, 170);
            this.panel2.TabIndex = 18;
            // 
            // radButton_Close
            // 
            this.radButton_Close.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Close.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Close.Location = new System.Drawing.Point(5, 110);
            this.radButton_Close.Name = "radButton_Close";
            this.radButton_Close.Size = new System.Drawing.Size(120, 31);
            this.radButton_Close.TabIndex = 44;
            this.radButton_Close.Text = "ปิด";
            this.radButton_Close.ThemeName = "Fluent";
            this.radButton_Close.Click += new System.EventHandler(this.RadButton_Close_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Close.GetChildAt(0))).Text = "ปิด";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Close.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Close.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Save.Location = new System.Drawing.Point(5, 76);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(120, 31);
            this.radButton_Save.TabIndex = 43;
            this.radButton_Save.Text = "บิลเรียบร้อย";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บิลเรียบร้อย";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radButton_Money);
            this.panel3.Controls.Add(this.radButton_CancleBill);
            this.panel3.Controls.Add(this.radButton_Close);
            this.panel3.Controls.Add(this.radButton_Save);
            this.panel3.Location = new System.Drawing.Point(656, 579);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(131, 143);
            this.panel3.TabIndex = 19;
            // 
            // radButton_Money
            // 
            this.radButton_Money.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Money.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Money.Location = new System.Drawing.Point(5, 41);
            this.radButton_Money.Name = "radButton_Money";
            this.radButton_Money.Size = new System.Drawing.Size(120, 31);
            this.radButton_Money.TabIndex = 46;
            this.radButton_Money.Text = "บิลมีรายการหัก";
            this.radButton_Money.ThemeName = "Fluent";
            this.radButton_Money.Click += new System.EventHandler(this.RadButton_Money_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Money.GetChildAt(0))).Text = "บิลมีรายการหัก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Money.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Money.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(1).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.radButton_Money.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 12F);
            // 
            // radButton_CancleBill
            // 
            this.radButton_CancleBill.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_CancleBill.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_CancleBill.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_CancleBill.Location = new System.Drawing.Point(5, 6);
            this.radButton_CancleBill.Name = "radButton_CancleBill";
            this.radButton_CancleBill.Size = new System.Drawing.Size(120, 31);
            this.radButton_CancleBill.TabIndex = 45;
            this.radButton_CancleBill.Text = "บิลยกเลิก";
            this.radButton_CancleBill.ThemeName = "Fluent";
            this.radButton_CancleBill.Click += new System.EventHandler(this.RadButton_CancleBill_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_CancleBill.GetChildAt(0))).Text = "บิลยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_CancleBill.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton_CancleBill.GetChildAt(0).GetChildAt(2))).BottomWidth = 2F;
            // 
            // BillOutDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Close;
            this.ClientSize = new System.Drawing.Size(790, 725);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BillOut_Detail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายละเอียดบิลเบิก";
            this.Load += new System.EventHandler(this.BillOut_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bill.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BillID)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_WH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_IS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BillDptCreate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Money)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_D062)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_D003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_StaOld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_StaBillCancle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BillGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Create)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Money)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CancleBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadGridView radGridView_Bill;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadTextBox radTextBox_BillID;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchName;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadLabel radLabel_Create;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_StaBillCancle;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_StaOld;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel_D003;
        private Telerik.WinControls.UI.RadLabel radLabel_MN;
        private Telerik.WinControls.UI.RadLabel radLabel_D062;
        private Telerik.WinControls.UI.RadLabel radLabel_Money;
        protected Telerik.WinControls.UI.RadButton radButton_Close;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private System.Windows.Forms.Panel panel3;
        protected Telerik.WinControls.UI.RadButton radButton_CancleBill;
        protected Telerik.WinControls.UI.RadButton radButton_Money;
        protected Telerik.WinControls.UI.RadButton radButton_Job;
        private Telerik.WinControls.UI.RadLabel radLabel_BillGroup;
        private Telerik.WinControls.UI.RadLabel radLabel_BillDptCreate;
        private Telerik.WinControls.UI.RadButton radButton_Add;
        private Telerik.WinControls.UI.RadLabel radLabel_IS;
        private Telerik.WinControls.UI.RadLabel radLabel_WH;
    }
}
