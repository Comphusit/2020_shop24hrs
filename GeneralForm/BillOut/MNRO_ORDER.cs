﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    public partial class MNRO_ORDER : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDGV = new DataTable();

        string pApv;
        public MNRO_ORDER()
        {
            InitializeComponent();
        }
        //Load Main
        private void MNRO_ORDER_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpMain);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpSub);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "สั่ง"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 300));
            RadGridView_Show.Columns["QTY"].FormatString = "{0:#,##0.00}";

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่ม";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหา";
            radButtonElement_pdt.ShowBorder = true; radButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Apv.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            ClearData();
        }
        //GroupMain
        void Set_GroupMain()
        {
            RadDropDownList_GrpMain.DataSource = I_Class.Find_GroupMain("'1'");
            RadDropDownList_GrpMain.ValueMember = "GROUPRO_ID";
            RadDropDownList_GrpMain.DisplayMember = "GROUPRO_NAME";
        }
        //แสดงกลุ่มย่อย
        void Set_GroupSub()
        {
            string grpMain;
            try
            {
                grpMain = RadDropDownList_GrpMain.SelectedValue.ToString();
            }
            catch (Exception) { grpMain = ""; }

            RadDropDownList_GrpSub.DataSource = I_Class.Find_GroupSub(grpMain, "'1'");
            RadDropDownList_GrpSub.ValueMember = "GROUPRO_SUBID";
            RadDropDownList_GrpSub.DisplayMember = "GROUPRO_SUBNAME";
        }
        //Set DGV
        void Set_DGV()
        {
            string grpSub;
            try
            {
                grpSub = RadDropDownList_GrpSub.SelectedValue.ToString();
            }
            catch (Exception) { grpSub = ""; }


            dtDGV = BillOutClass.Find_ItemRoByGrp(RadDropDownList_GrpMain.SelectedValue.ToString(), grpSub);
            RadGridView_Show.DataSource = dtDGV;

            if (dtDGV.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("สินค้าในกลุ่มที่เลือก");
                return;
            }
            RadDropDownList_GrpMain.Enabled = false;
            RadDropDownList_GrpSub.Enabled = false;
            RadButton_Search.Enabled = false;
            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก"; radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red(); pApv = "0";
            RadButton_Save.Enabled = true;
        }
        //Clear
        void ClearData()
        {
            if (dtDGV.Rows.Count > 0) { dtDGV.Rows.Clear(); dtDGV.AcceptChanges(); }

            Set_GroupMain(); Set_GroupSub();
            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = "";
            pApv = "0";
            RadDropDownList_GrpMain.Enabled = true; RadDropDownList_GrpSub.Enabled = true; RadButton_Search.Enabled = true;

            radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = Color.Transparent;

            RadButton_Cancel.Enabled = false;
            RadButton_Save.Enabled = false;
            RadButton_Apv.Enabled = false;
            RadButton_Search.Focus();
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            Set_DGV();
        }

        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Select Group Main
        private void RadDropDownList_GrpMain_SelectedValueChanged(object sender, EventArgs e)
        {
            Set_GroupSub();
        }
        //Set choose Data
        void SetDataSTA()
        {
            DataRow[] drSTA = dtDGV.Select($@"ITEMBARCODE = '{RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value}'");
            string pSta = "0"; double pQty = 0;

            if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
            {
                InputData frmSTA = new InputData("0",
                                       RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                       "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                { pInputData = "1" };
                if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                {
                    pSta = "1";
                    pQty = Convert.ToDouble(frmSTA.pInputData);
                }
            }
            else
            {
                pSta = "0";
                pQty = 0;
            }

            if (radLabel_Docno.Text != "")
            {
                if (I_Class.SHOP_RO_UpdateQty(radLabel_Docno.Text, dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["ITEMBARCODE"].ToString(), pSta, pQty) != "") { MsgBoxClass.MsgBoxShow_SaveStatus("จำนวนเบิก"); return; }
            }

            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
            dtDGV.AcceptChanges();
        }
        //choose
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) return;
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0") return;

                    DataRow[] drQTY = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                    InputData frmQTY = new InputData("0",
                                              RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                              "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() };
                    if (frmQTY.ShowDialog(this) == DialogResult.Yes)
                    {
                        string pSta; double pQty = Convert.ToDouble(frmQTY.pInputData);
                        if (Convert.ToDouble(frmQTY.pInputData) == 0)
                        { pSta = "0"; }
                        else { pSta = "1"; }
                        if (radLabel_Docno.Text != "")
                        {
                            if (I_Class.SHOP_RO_UpdateQty(radLabel_Docno.Text, dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["ITEMBARCODE"].ToString(), pSta, pQty) != "") { MsgBoxClass.MsgBoxShow_SaveStatus("จำนวนเบิก"); return; }
                        }
                        dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["STA"] = pSta;
                        dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["QTY"] = pQty.ToString("#,##0.00");
                        dtDGV.AcceptChanges();
                    }

                    break;
                default:
                    return;
            }
        }
        //choose
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) { return; }
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                default:
                    return;
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            int iCountRow = 0;
            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                if (Convert.ToDouble(dtDGV.Rows[i]["QTY"]) > 0) iCountRow += 1;
            }
            if (iCountRow == 0)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ข้อมูลการเบิกสินค้า"); return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการเบิกสินค้า") == DialogResult.No) return;

            string grpSub;
            try
            {
                grpSub = RadDropDownList_GrpSub.SelectedValue.ToString();
            }
            catch (Exception) { grpSub = ""; }

            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNRO", "-", "MNRO", "1");
            ArrayList sqlIn = new ArrayList
            {
                $@"INSERT INTO [SHOP_RO_MNROHD] ( [DOCNO],[GROUPMAIN],[GROUPSUB],[BRANCH_ID],[BRANCH_NAME],[WHOIDINS],[WHONAMEINS] ) 
                    VALUES ('{maxDocno}','{RadDropDownList_GrpMain.SelectedValue}',
                    '{grpSub}', '{SystemClass.SystemBranchID}', '{SystemClass.SystemBranchName}',
                    '{SystemClass.SystemUserID}', '{SystemClass.SystemUserName}') "
            };

            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                sqlIn.Add($@"INSERT INTO [SHOP_RO_MNRODT] ( [DOCNO],[LINENUM],[ITEMBARCODE],[SPC_ITEMNAME],[STA],[QTY],[UNITID] ) 
                    VALUES ('{maxDocno}','{i + 1}',
                    '{dtDGV.Rows[i]["ITEMBARCODE"]}','{ConfigClass.ChecKStringForImport(dtDGV.Rows[i]["SPC_ITEMNAME"].ToString())}',
                    '{dtDGV.Rows[i]["STA"]}','{double.Parse(dtDGV.Rows[i]["QTY"].ToString())}','{dtDGV.Rows[i]["UNITID"]}') ");
            }
            string T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                RadButton_Apv.Enabled = true;
                RadButton_Cancel.Enabled = true;
                RadButton_Save.Enabled = false;
            }

        }
        //ยืนยัน
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยืนยัน") == DialogResult.No) return;

            string T = I_Class.SHOP_RO_UpdateBill("1", radLabel_Docno.Text); //onnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยืนยัน");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = true;
            }
        }

        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            //string sql = string.Format(@"SELECT STAAPV FROM  SHOP_RO_MNROHD WHERE DOCNO = '" + radLabel_Docno.Text + @"'  ");
            DataTable dtSelect = I_Class.SHOP_RO_FindDT("2", radLabel_Docno.Text);// ConnectionClass.SelectSQL_Main(sql);
            if (dtSelect.Rows[0]["STAAPV"].ToString() == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"บิลเลขที่ {radLabel_Docno.Text} ได้ยืนยันการรับไปแล้วจากแผนกเบิกภายใน{Environment.NewLine}ถ้าไม่ต้องการสินค้าเบิกในบิลนี้ ให้ติดต่อแผนกเบิกสินค้าภายในโดยตรง.");
                return;
            }

            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยกเลิก") == DialogResult.No) return;

            //string sqlCancle = string.Format(@"UPDATE SHOP_RO_MNROHD  SET STADOC = '3',DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
            //                WHOIDUPD = '" + SystemClass.SystemUserID + @"',WHONAMEUPD = '" + SystemClass.SystemUserName + @"' 
            //                WHERE DOCNO = '" + radLabel_Docno.Text + @"'  ");

            string T = I_Class.SHOP_RO_UpdateBill("0", radLabel_Docno.Text); //ConnectionClass.ExecuteSQL_Main(sqlCancle);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยกเลิก");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }
        }

        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNRO_FindData frm = new MNRO_FindData();
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                SetDGV_Load(frm.pDocno);
            }
        }
        void SetDGV_Load(string pDocno)
        {
            //string sqlSelect = string.Format(@"SELECT	SHOP_RO_MNROHD.DOCNO,
            //  SHOP_RO_MNROHD.GROUPMAIN,SHOP_RO_MNROHD.GROUPSUB,BRANCH_ID,BRANCH_NAME,STADOC,STAPRCDOC,STAAPV,
            //  SHOP_RO_MNRODT.ITEMBARCODE,SPC_ITEMNAME,SHOP_RO_MNRODT.STA,QTY,UNITID,SHOP_RO_ITEM.REMARK
            //FROM	SHOP_RO_MNROHD WITH (NOLOCK) 
            //   INNER JOIN SHOP_RO_MNRODT WITH (NOLOCK) ON SHOP_RO_MNROHD.DOCNO = SHOP_RO_MNRODT.DOCNO
            //   LEFT OUTER JOIN SHOP_RO_ITEM WITH (NOLOCK) ON SHOP_RO_MNRODT.ITEMBARCODE = SHOP_RO_ITEM.ITEMBARCODE
            //WHERE	SHOP_RO_MNROHD.DOCNO = '" + pDocno + @"'
            //ORDER BY SHOP_RO_MNRODT.LINENUM");
            dtDGV = I_Class.SHOP_RO_FindDT("1", pDocno);// ConnectionClass.SelectSQL_Main(sqlSelect);

            RadGridView_Show.DataSource = dtDGV;
            dtDGV.AcceptChanges();

            Set_GroupMain(); Set_GroupSub();
            RadDropDownList_GrpMain.SelectedValue = dtDGV.Rows[0]["GROUPMAIN"].ToString();
            RadDropDownList_GrpSub.SelectedValue = dtDGV.Rows[0]["GROUPSUB"].ToString();
            radLabel_Docno.Text = dtDGV.Rows[0]["DOCNO"].ToString();

            RadDropDownList_GrpMain.Enabled = false; RadDropDownList_GrpSub.Enabled = false; RadButton_Search.Enabled = false;
            // check อนุมัติ
            if (dtDGV.Rows[0]["STAAPV"].ToString() == "1")
            {
                radLabel_StatusBill.Text = "สถานะบิล : เบิกภายในอนุมัติแล้ว"; radLabel_StatusBill.ForeColor = Color.Black;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = false;
                RadButton_Save.Enabled = false;
            }
            else
            {
                switch (dtDGV.Rows[0]["STADOC"].ToString())
                {
                    case "1":
                        if (dtDGV.Rows[0]["STAPRCDOC"].ToString() == "1")
                        {
                            radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                            RadButton_Apv.Enabled = false;
                            RadButton_Cancel.Enabled = true;
                            RadButton_Save.Enabled = false;
                        }
                        else
                        {
                            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                            RadButton_Apv.Enabled = true;
                            RadButton_Cancel.Enabled = true;
                            RadButton_Save.Enabled = false;
                        }
                        break;
                    case "3":
                        radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                        RadButton_Apv.Enabled = false;
                        RadButton_Cancel.Enabled = false;
                        RadButton_Save.Enabled = false;
                        break;
                    default:
                        break;
                }
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
