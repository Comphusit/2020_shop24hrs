﻿//CheckOK
using PC_Shop24Hrs.Controllers;
using System;
using System.Collections;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    class I_Class
    {
        #region SetGroup
        //ค้นหา group Main
        public static DataTable Find_GroupMain(string _pSta)// _pSta ใช้หรือไม่ใช่
        {
            string sql = $@"
                SELECT	[GROUPRO_ID],[GROUPRO_NAME],[GROUPRO_STA],[GROUPRO_REMARK]
                FROM	[SHOP_RO_GROUPMAIN] WITH (NOLOCK) 
                WHERE   GROUPRO_STA IN ({_pSta}) 
                ORDER BY [GROUPRO_ID]";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหา group sub จาก Group Main
        public static DataTable Find_GroupSub(string _pGrpMainID, string _pSta) //_pSta  ใช้หรือไม่ใช่
        {
            string sql = $@"
                SELECT  GROUPRO_SUBID,GROUPRO_SUBNAME,GROUPRO_SUBSTA,GROUPRO_SUBREMARK 
                FROM    SHOP_RO_GROUPSUB WITH (NOLOCK) 
                WHERE   GROUPRO_ID = '{_pGrpMainID}' AND  GROUPRO_SUBSTA IN ({_pSta}) ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        #endregion

        #region OldCODE
        ////ส่งข้อมูลบิลเบิกเข้า AX 
        //public static string SendData_AX(DataTable dtDataDT, string pTable, string pFiledAX, string pFiledDocno)
        //{
        //    //string inStrHD;            string inStrDT;
        //    ArrayList inStrAX = new ArrayList();
        //    string mnrs = string.Empty;
        //    int seqNo = 0;
        //    int mndocno = 0;

        //    for (int i = 0; i < dtDataDT.Rows.Count; i++)
        //    {
        //        if (mnrs != dtDataDT.Rows[i]["GROUPMAIN"].ToString())
        //        {
        //            mnrs = dtDataDT.Rows[i]["GROUPMAIN"].ToString();
        //            seqNo = 0;
        //            mndocno += 1;
        //            inStrAX.Add(string.Format(@"INSERT INTO SPC_INVENTJOURNALTABLE (DATAAREAID,RECVERSION,RECID,
        //            JOURNALID,DESCRIPTION,TRANSDATE,JOURNALTYPE,
        //            EMPLID,REMARKS,INVENTLOCATIONIDTO,INVENTLOCATIONIDFROM,INVENTCOSTCENTERID,
        //            JOBID,REFJOURNALID,DIMENSION, 
        //            DIMENSION2_,DIMENSION3_) VALUES ( 'SPC','1','1',
        //            '" + dtDataDT.Rows[i]["DOCNO"].ToString() + "-" + Convert.ToString(mndocno) + @"',
        //            'สมุดเบิกสินค้า-" + dtDataDT.Rows[i]["BRANCH_ID"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["DATE"].ToString() + @"',
        //            '0','" + SystemClass.SystemUserID_M + @"',
        //            '" + dtDataDT.Rows[i]["REMARK"].ToString() + @"','',
        //            '" + dtDataDT.Rows[i]["INVENT"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["GROUPMAIN"].ToString() + @"','','',
        //            '" + dtDataDT.Rows[i]["BRANCH_ID"].ToString() + @"','',''  )"));

        //            inStrAX.Add(string.Format(@"INSERT INTO SPC_INVENTJOURNALTRANS (DATAAREAID,RECVERSION,RECID,
        //            JOURNALID,LINENUM,TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY,INVENTTRANSID, 
        //            INVENTSERIALID,INVENTBATCHID,COSTPRICE) VALUES ('SPC','1','1',
        //            '" + dtDataDT.Rows[i]["DOCNO"].ToString() + "-" + Convert.ToString(mndocno) + @"','" + (seqNo + 1) + @"',
        //            '" + dtDataDT.Rows[i]["DATE"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["ITEMBARCODE"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["ITEMID"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["INVENTDIMID"].ToString() + @"',
        //            '" + Convert.ToDouble(dtDataDT.Rows[i]["QTY"].ToString()) * (-1) + @"',
        //            '" + dtDataDT.Rows[i]["DOCNO"].ToString() + Convert.ToString(i + 1) + @"','','',0 )"));
        //            seqNo += 1;
        //        }
        //        else
        //        {
        //            inStrAX.Add(string.Format(@"INSERT INTO SPC_INVENTJOURNALTRANS (DATAAREAID,RECVERSION,RECID,
        //            JOURNALID,LINENUM,TRANSDATE,ITEMBARCODE,ITEMID,INVENTDIMID,QTY,INVENTTRANSID, 
        //            INVENTSERIALID,INVENTBATCHID,COSTPRICE) VALUES ('SPC','1','1',
        //            '" + dtDataDT.Rows[i]["DOCNO"].ToString() + "-" + Convert.ToString(mndocno) + @"','" + (seqNo + 1) + @"',
        //            '" + dtDataDT.Rows[i]["DATE"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["ITEMBARCODE"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["ITEMID"].ToString() + @"',
        //            '" + dtDataDT.Rows[i]["INVENTDIMID"].ToString() + @"',
        //            '" + Convert.ToDouble(dtDataDT.Rows[i]["QTY"].ToString()) * (-1) + @"',
        //            '" + dtDataDT.Rows[i]["DOCNO"].ToString() + Convert.ToString(i + 1) + @"','','',0 )"));
        //            seqNo += 1;
        //        }
        //    }

        //    ArrayList inStr24 = new ArrayList
        //    {
        //        $@"UPDATE SHOP_RO_MNRSHD SET STAUPAX =  '1' WHERE DOCNO = '{ dtDataDT.Rows[0]["DOCNO"]}' "
        //    };
        //    return ConnectionClass.ExecuteMain_AX_24_SameTime(inStr24, inStrAX);
        //}

        ////ส่งข้อมูลเข้า SPC_EXTERNALLIST
        //public static string SendData_EXTERNALLIST(string pDocno, string pDate, string pBchID, string pBchName)
        //{
        //    //string sqlIn = string.Format(@"INSERT INTO SPC_EXTERNALLIST (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID) 
        //    //            VALUES ('SPC_SHIPMENTINVOICE','LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID','1|10|" + pDocno + @"|SPC',
        //    //            'LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT',
        //    //            '1|10|" + pDate + @"|" + pDocno + @"|" + SystemClass.SystemUserID + @"|" + pBchName + @"|" + pDate + @"|" + pDate + @"|" + pBchID + @"',
        //    //            CONVERT(NVARCHAR,'CREATE'),'SPC','1',CONVERT(BIGINT," + pDocno.Substring(4, 12) + @"1) )
        //    //            ");
        //    //inStrAX.Add(AX_SendData.Save_EXTERNALLIST("CONVERT(NVARCHAR,'CREATE')", "SPC_SHIPMENTINVOICE",
        //    //        "LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID",
        //    //        $@"1|10|{dtDataDT.Rows[i]["DOCNO"]}|SPC",
        //    //        "LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT",
        //    //        $@"'1|10|{dtDataDT.Rows[i]["DATE"]}|{dtDataDT.Rows[i]["DOCNO"]}|{SystemClass.SystemUserID}|{bchName}|{dtDataDT.Rows[i]["DATE"]}|{dtDataDT.Rows[i]["DATE"]}|{dtDataDT.Rows[i]["BRANCH_ID"]}", $@"CONVERT(BIGINT,{dtDataDT.Rows[i]["DOCNO"].ToString().Substring(4, 12)}1)"));

        //    string sqlIn = Class.AX_SendData.Save_EXTERNALLIST("CONVERT(NVARCHAR,'CREATE')", "SPC_SHIPMENTINVOICE",
        //        "LINENUM|DOCUMENTTYPE|DOCUMENTNUM|DATAAREAID",
        //        $@"1|10|{pDocno}|SPC",
        //        "LINENUM|DOCUMENTTYPE|DOCUMENTDATE|DOCUMENTNUM|EMPLCHECKER|NAME|SHIPPINGDATEREQ|TRANSDATE|CUSTACCOUNT",
        //        $@"'1|10|{pDate}|{pDocno}|{SystemClass.SystemUserID}|{pBchName}|{pDate}|{pDate}|{pBchID}", $@"CONVERT(BIGINT,{pDocno.Substring(4, 12)}1)");

        //    return ConnectionClass.ExecuteSQL_MainAX(sqlIn);
        //}
        #endregion

        #region SHOP_RO_GROUP/SUB
        // Edit - Insert Group Sub
        public static string SHOP_RO_GROUPSUB_InsertUpdate(string grpMain, string grpSubID, string grpSubName, string sta_Sub, string rmk)
        {
            string sqlIn;
            if (grpSubID == "")
            {
                string GroupSubID = Class.ConfigClass.GetMaxINVOICEID(grpMain, "-", "GR", "3");
                sqlIn = $@" 
                INSERT INTO SHOP_RO_GROUPSUB (GROUPRO_SUBID,GROUPRO_ID,GROUPRO_SUBNAME,  
                            GROUPRO_SUBSTA,GROUPRO_SUBREMARK,WHOIDINS,WHONAMEINS )
                VALUES      ('{GroupSubID}','{grpMain}','{grpSubName}','{sta_Sub}','{rmk}', '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}' ) ";

            }
            else
            {
                sqlIn = $@"
                Update  SHOP_RO_GROUPSUB  
                SET     GROUPRO_SUBSTA = '{sta_Sub}',GROUPRO_SUBREMARK = '{rmk}',
                        GROUPRO_SUBNAME = '{grpSubName}',
                        DATEUPD = CONVERT(VARCHAR,GETDATE(),25),WHOIDUPD = '{SystemClass.SystemUserID}',
                        WHONAMEUPD = '{SystemClass.SystemUserName}'
                WHERE   GROUPRO_SUBID  = '{grpSubID}' AND GROUPRO_ID = '{grpMain}' ";
            }
            return sqlIn;
        }
        // Edit - Insert Group Main
        public static string SHOP_RO_GROUP_InsertUpdate(string grpMain, string grpMainName, string sta_Main, string rmk)
        {
            string sqlIn;
            if (grpMainName != "")
            {
                sqlIn = $@"
                INSERT INTO SHOP_RO_GROUPMAIN ( [GROUPRO_ID],[GROUPRO_NAME],[GROUPRO_STA],[GROUPRO_REMARK],[WHOIDINS],[WHONAMEINS] )  
                VALUES      ('{grpMain}','{grpMainName}', '{sta_Main}', '{rmk}', 
                            '{SystemClass.SystemUserID}', '{SystemClass.SystemUserName}') ";
            }
            else
            {
                sqlIn = $@"
                Update  SHOP_RO_GROUPMAIN  
                SET     GROUPRO_STA = '{sta_Main}',GROUPRO_REMARK = '{rmk}',DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                        WHOIDUPD = '{SystemClass.SystemUserID}',
                        WHONAMEUPD = '{SystemClass.SystemUserName}' 
                WHERE   GROUPRO_ID  = '{grpMain}' ";
            }
            return sqlIn;
        }
        #endregion

        #region SHOP_RO_ITEM
        //Update Insert Delete SHOP_RO_ITEM
        public static string SHOP_RO_ITEM_InsertUpdate(string pCase0In1Up2Del, string barcode, string grbMain, string grbSub, string zone, string rmk, string sta)
        {
            string sql;
            if (pCase0In1Up2Del == "0")//Insert
            {
                sql = $@"
                INSERT INTO SHOP_RO_ITEM (ITEMBARCODE,GROUPMAIN,GROUPSUB,ZONE,REMARK,WHOIDINS,WHONAMEINS,STA ) 
                VALUES  ('{barcode}','{grbMain}','{grbSub}','{zone}','{rmk}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{sta}' ) ";
            }
            else if (pCase0In1Up2Del == "1") // Update
            {
                sql = $@"
                UPDATE  SHOP_RO_ITEM 
                SET     GROUPMAIN = '{grbMain}',GROUPSUB = '{grbSub}' ,ZONE = '{zone}',REMARK = '{rmk}' ,
                        DATEUPD = GETDATE(),WHOIDUPD = '{SystemClass.SystemUserID}',WHONAMEUPD = '{SystemClass.SystemUserName}' ,
                        STA = '{sta}'
                WHERE   ITEMBARCODE = '{barcode}' ";
            }
            else // delete  
            {
                sql = $@"DELETE SHOP_RO_ITEM     WHERE ITEMBARCODE = '{barcode}'   ";
            }
            return sql;
        }
        #endregion

        #region SHOP_RO_MNRSHD
        //Find Dt For SendAX
        public static DataTable SHOP_RO_MNRS_FindForSendAX(string docno)
        {
            string sql = $@"
                        SELECT	SHOP_RO_MNRSDT.DOCNO,ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,UNITID,SALEPRICE,QTY,LINEAMOUNT,
		                        BRANCH_ID,CONVERT(varchar,SHOP_RO_MNRSHD.DATE,23) AS DATE,INVENT,REMARK,ITEMID,INVENTDIMID     
                        FROM	SHOP_RO_MNRSDT WITH (NOLOCK) 
                                INNER JOIN SHOP_RO_MNRSHD WITH (NOLOCK) ON SHOP_RO_MNRSDT.DOCNO = SHOP_RO_MNRSHD.DOCNO
                        WHERE	SHOP_RO_MNRSDT.STADOC = '1' AND SHOP_RO_MNRSDT.DOCNO = '{docno}' 
                        ORDER BY GROUPMAIN,GROUPSUB ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Find For Print
        public static DataTable SHOP_RO_MNRS_FindPrint(string pCase, string docno)
        {
            string sql;
            if (pCase == "0")
            {
                sql = $@"
                    SELECT	SHOP_RO_MNRSHD.DOCNO,WHOIDINS,WHONAMEINS,BRANCH_ID,BRANCH_NAME,INVENT,
                            BILLAMOUNT,BOXQTY,REMARK,PRINTCOUNT,SHOP_RO_MNRSHD.STADOC,
                            ITEMBARCODE,SPC_ITEMNAME,QTY,UNITID,WHOIDPACKING,WHONAMEPACKING
                    FROM	SHOP_RO_MNRSHD	WITH (NOLOCK) 
                            INNER JOIN SHOP_RO_MNRSDT WITH (NOLOCK)   ON SHOP_RO_MNRSHD.DOCNO = SHOP_RO_MNRSDT.DOCNO
                    WHERE	SHOP_RO_MNRSHD.DOCNO = '{docno}' 
                            AND SHOP_RO_MNRSDT.STADOC = '1'
                    ORDER BY LINENUM ";
            }
            else
            {
                sql = $@"
                    SELECT	ITEMBARCODE,SPC_ITEMNAME,QTY,UNITID,SALEPRICE,LINEAMOUNT,LINENUM
                    FROM	SHOP_RO_MNRSDT WITH (NOLOCK)
                    WHERE	DOCNO = ''";
            }

            return ConnectionClass.SelectSQL_Main(sql);

        }
        //ผลรวมจำนวนเงินทั้งหมด
        public static double SHOP_RO_MNRS_SumAmount(string docno)
        {
            DataTable dtSumLineAmount = ConnectionClass.SelectSQL_Main($@"
                    SELECT	ISNULL(SUM(LINEAMOUNT),0) AS 	LINEAMOUNT 
                    FROM	SHOP_RO_MNRSDT WITH (NOLOCK)
                    WHERE	Docno = '{docno}' AND STADOC = '1'
                ");
            return Convert.ToDouble(dtSumLineAmount.Rows[0]["LINEAMOUNT"].ToString());
        }
        //SaveHD
        public static string SHOP_RO_MNRS_SaveHD(string docno, string bchID, string bchName, string invent, string staInStall, string jobID, string STA)
        {
            string sql = $@"
                INSERT INTO SHOP_RO_MNRSHD 
                            ( DOCNO,WHOIDINS,WHONAMEINS,BRANCH_ID,BRANCH_NAME,INVENT,DPTIDINS,DPTNAMEINS,STA_INSTALL,JOB_NUMBER,STADOC, PRINTCOUNT )
                VALUES      ('{docno}', '{SystemClass.SystemUserID}', '{SystemClass.SystemUserName}', '{bchID}',
                            '{bchName}','{invent}','{SystemClass.SystemDptID}','{SystemClass.SystemDptName}',
                            '{staInStall}','{jobID}','{STA}',0 ) ";
            return sql;
        }
        //SaveDT
        public static string SHOP_RO_MNRS_SaveDT(int iRows, string docno, DataTable dtBarcode, double priceNet, double weight)
        {
            string sqlDT = $@" 
                INSERT INTO SHOP_RO_MNRSDT (DOCNO,LINENUM,ITEMID,INVENTDIMID,
                            ITEMBARCODE,SPC_ITEMNAME,GROUPMAIN,GROUPSUB,UNITID,
                            SALEPRICE,QTY,INVENTQTY,LINEAMOUNT,DATE ) 
                VALUES (    '{docno}','{iRows}','{dtBarcode.Rows[0]["ITEMID"]}',
                            '{dtBarcode.Rows[0]["INVENTDIMID"]}',
                            '{dtBarcode.Rows[0]["ITEMBARCODE"]}',
                            '{dtBarcode.Rows[0]["SPC_ITEMNAME"]}',
                            '{dtBarcode.Rows[0]["GROUPMAIN"]}',
                            '{dtBarcode.Rows[0]["GROUPSUB"]}',
                            '{dtBarcode.Rows[0]["UNITID"]}',
                            '{Convert.ToDouble(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString())}',
                            '{weight}',
                            '{weight * Convert.ToDouble(dtBarcode.Rows[0]["QTY"].ToString())}',
                            '{priceNet}',GETDATE() ) ";
            return sqlDT;
        }
        //UpdateDT
        public static string SHOP_RO_MNRS_UpdateDT(string docno, string barcode, double priceNet, double weight, double weightAll)
        {
            string sqlDT = $@"  
                UPDATE      SHOP_RO_MNRSDT
                SET         QTY = QTY + '{weight}',INVENTQTY = INVENTQTY + '{weightAll}',
                            LINEAMOUNT = LINEAMOUNT+'{priceNet}'
                WHERE       DOCNO = '{docno}' AND ITEMBARCODE = '{barcode}' ";
            return sqlDT;
        }
        //Void SHOP_RO_MNRS
        public static string SHOP_RO_MNRS_Void(string docno, int lineNum)
        {
            string sqlUp = $@"
            UPDATE  SHOP_RO_MNRSDT 
            SET     STADOC = '3' ,DATE = GETDATE() 
            WHERE   DOCNO = '{docno}' AND LINENUM = '{lineNum}' ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        //Update Box SHOP_RO_MNRS
        public static string SHOP_RO_MNRS_UpdateBox(string pCase0UpBox1UpGrand, string docno, int boxQty, string rmk, double grand, string packingEmpId, string packingEmpName, string sqlAX)
        {
            //string sqlUp;
            ArrayList sqlUp = new ArrayList();
            ArrayList sqlInAX = new ArrayList
            {
                sqlAX
            };

            if (pCase0UpBox1UpGrand == "0")
            {
                sqlUp.Add($@"
                UPDATE  SHOP_RO_MNRSHD 
                SET     STADOC = '1' ,REMARK = '{rmk}',BOXQTY = '{boxQty}' ,WHOIDPACKING = '{packingEmpId}',WHONAMEPACKING = '{packingEmpName}'
                WHERE   DOCNO = '{docno}'   ");
            }
            else
            {
                sqlUp.Add($@"
                UPDATE  SHOP_RO_MNRSHD 
                SET     STADOC = '1' ,REMARK = '{rmk}',BOXQTY = '{boxQty}',BILLAMOUNT = '{grand}',WHOIDPACKING = '{packingEmpId}',WHONAMEPACKING = '{packingEmpName}'
                WHERE   DOCNO = '{docno}'   ");
            }

            //return ConnectionClass.ExecuteSQL_Main(sqlUp);
            return ConnectionClass.ExecuteMain_AX_24_SameTime(sqlUp, sqlInAX);
        }
        //Update Send To AX
        public static ArrayList SHOP_RO_MNRS_UpdateSendAX(string docno, string tblName, string filedNameSet, string filedNameWhere)
        {
            ArrayList inStr24 = new ArrayList
            {
                $@"UPDATE {tblName} SET {filedNameSet} =  '1' WHERE {filedNameWhere} = '{docno}' "
            };
            return inStr24;
        }
        #endregion

        #region SHOP_MNRO
        //ค้นหาจำนวนสินค้าที่สั่งไปแล้วตามวัน
        public static DataTable SHOP_RO_ItemOrder(string date)
        {
            string sql = $@"
                    SELECT	BRANCH_ID,ITEMBARCODE,SUM(QTY) AS QTY  
                    FROM	SHOP_RO_MNROHD WITH (NOLOCK) 
                            INNER JOIN SHOP_RO_MNRODT WITH (NOLOCK)    ON SHOP_RO_MNRODT.DOCNo = SHOP_RO_MNROHD.DOCNo   
                     WHERE	 STADOC = '1' AND STAAPV = '1' AND STA = '1'  
                            AND CONVERT(VARCHAR,DATE,23) = '{date}'   
                     GROUP BY BRANCH_ID,ITEMBARCODE   
                     ORDER BY BRANCH_ID,ITEMBARCODE ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable SHOP_RO_FindHD(string topRows, string date1, string date2, string bchID, string grpMain, string apv)
        {
            string Conbch = "", ConGrpMain = "", ConApv = "";
            if (bchID != "") Conbch = $@" AND BRANCH_ID = '{bchID}' ";
            if (grpMain != "") ConGrpMain = $@" AND SHOP_RO_MNROHD.GROUPMAIN = 'grpMain' ";
            if (apv != "") ConApv = $@" AND STAAPV = '0' AND STADOC = '1' AND STAPRCDOC IN ('1','0') ";

            string sqlSelect = $@" 
                    SELECT {topRows}	DOCNO,CONVERT(VARCHAR,DATE,23) AS DOCDATE,
                            GROUPMAIN + '-' + GROUPRO_NAME AS GROUPMAIN,GROUPSUB + '-' + GROUPRO_SUBNAME AS GROUPSUB,
                            BRANCH_ID + '-' + BRANCH_NAME AS BRANCH_NAME,
		                    [SHOP_RO_MNROHD].WHOIDINS + '-' + [SHOP_RO_MNROHD].WHONAMEINS AS WHOIDINS,
		                    CASE STADOC WHEN '0' THEN '1' ELSE '0' END AS STADOC,
                            CASE STADOC WHEN '3' THEN [SHOP_RO_MNROHD].WHOIDUPD+'-'+[SHOP_RO_MNROHD].WHONAMEUPD  ELSE '' END AS WHOCANCLE,
                            STAPRCDOC,CASE STAPRCDOC WHEN '1' THEN [SHOP_RO_MNROHD].WHOIDUPD+'-'+[SHOP_RO_MNROHD].WHONAMEUPD ELSE '' END AS WHOIDUPD,
                            STAAPV,[SHOP_RO_MNROHD].WHOIDAPV+'-'+[SHOP_RO_MNROHD].WHONAMEAPV AS WHOIDAPV  
                     FROM	[SHOP_RO_MNROHD] WITH (NOLOCK)
		                    LEFT OUTER JOIN SHOP_RO_GROUPMAIN WITH (NOLOCK) ON [SHOP_RO_MNROHD].GROUPMAIN = SHOP_RO_GROUPMAIN.GROUPRO_ID
		                    LEFT OUTER JOIN SHOP_RO_GROUPSUB WITH (NOLOCK) ON [SHOP_RO_MNROHD].GROUPSUB = SHOP_RO_GROUPSUB.GROUPRO_SUBID
                                AND [SHOP_RO_MNROHD].GROUPMAIN  = SHOP_RO_GROUPSUB.GROUPRO_ID 
                    WHERE	CONVERT(VARCHAR,DATE,23) BETWEEN '{date1}'  AND '{date2}' 
                            {Conbch}  {ConGrpMain} {ConApv}
                    ORDER BY DOCNO DESC
                    ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }

        public static DataTable SHOP_RO_FindDT(string pCase, string docno)//pCase 0 เฉพาะที่สั่ง 1 ทั้งหมด 2 เชคสถานะ
        {
            string sql = "";
            switch (pCase)
            {
                case "0":
                    sql = $@"
                        SELECT  ITEMBARCODE,SPC_ITEMNAME,QTY,UNITID 
                        FROM    [SHOP_RO_MNRODT] WITH (NOLOCK) 
                        WHERE   DOCNO = '{docno}' AND STA = '1'  ";
                    break;
                case "1":
                    sql = $@"
                        SELECT	SHOP_RO_MNROHD.DOCNO,
		                    SHOP_RO_MNROHD.GROUPMAIN,SHOP_RO_MNROHD.GROUPSUB,BRANCH_ID,BRANCH_NAME,STADOC,STAPRCDOC,STAAPV,
		                    SHOP_RO_MNRODT.ITEMBARCODE,SPC_ITEMNAME,SHOP_RO_MNRODT.STA,QTY,UNITID,SHOP_RO_ITEM.REMARK
                        FROM	SHOP_RO_MNROHD WITH (NOLOCK) 
			                        INNER JOIN SHOP_RO_MNRODT WITH (NOLOCK) ON SHOP_RO_MNROHD.DOCNO = SHOP_RO_MNRODT.DOCNO
			                        LEFT OUTER JOIN SHOP_RO_ITEM WITH (NOLOCK) ON SHOP_RO_MNRODT.ITEMBARCODE = SHOP_RO_ITEM.ITEMBARCODE
                        WHERE	SHOP_RO_MNROHD.DOCNO = '{docno}'
                        ORDER BY SHOP_RO_MNRODT.LINENUM ";
                    break;
                case "2":
                    sql = $@" SELECT STAAPV FROM  SHOP_RO_MNROHD WHERE DOCNO = '{docno}'  ";
                    break;
                default:
                    break;
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Update สถานะบิล
        public static string SHOP_RO_UpdateBill(string pCase, string docno)//pCase 0 ยกเลิก 1 ยืนยัน
        {
            string sql = "";
            switch (pCase)
            {
                case "0":
                    sql = $@"
                        UPDATE  SHOP_RO_MNROHD  
                        SET     STADOC = '3',DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                WHOIDUPD = '{SystemClass.SystemUserID}',WHONAMEUPD = '{SystemClass.SystemUserName}' 
                        WHERE   DOCNO = '{docno}'  ";
                    break;
                case "1":
                    sql = $@" 
                        UPDATE  SHOP_RO_MNROHD 
                        SET     STAPRCDOC = '1',DATEUPD = CONVERT(VARCHAR,GETDATE(),25), 
                                 WHOIDUPD = '{SystemClass.SystemUserID}',WHONAMEUPD = '{SystemClass.SystemUserName}' 
                        WHERE   DOCNO = '{docno}'  ";
                    break;
                default:
                    break;
            }
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        //Update จำนวนสั่ง
        public static string SHOP_RO_UpdateQty(string docno, string barcode, string sta, double qty)
        {
            string sqlUp = $@" UPDATE SHOP_RO_MNRODT SET STA = '{sta}',QTY = '{qty}' 
                            WHERE DOCNO = '{docno}' AND ITEMBARCODE = '{barcode}' ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }

        #endregion
    }
}