﻿namespace PC_Shop24Hrs.GeneralForm.BillOut
{
    partial class I_GroupMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(I_GroupMain));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_MainName = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_MainSave = new Telerik.WinControls.UI.RadButton();
            this.radButton_MainCanCle = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radCheckBox_MainSta = new System.Windows.Forms.CheckBox();
            this.radButton_mainEdit = new Telerik.WinControls.UI.RadButton();
            this.radButton_MainAdd = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_MainRmk = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_MainID = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radGridView_Main = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radGridView_Sub = new Telerik.WinControls.UI.RadGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radCheckBox_SubSta = new System.Windows.Forms.CheckBox();
            this.radButton_subEdit = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_MainID_Sub = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_SubAdd = new Telerik.WinControls.UI.RadButton();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_SubRmk = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_SubID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_SubName = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_SabCancle = new Telerik.WinControls.UI.RadButton();
            this.radButton_SubSave = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_MainSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_MainCanCle)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_mainEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_MainAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainRmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Main.MasterTemplate)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Sub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Sub.MasterTemplate)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_subEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainID_Sub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SubAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SubRmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SubID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SubName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SabCancle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SubSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(5, 70);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 19);
            this.radLabel1.TabIndex = 17;
            this.radLabel1.Text = "คำอธิบาย";
            // 
            // radTextBox_MainName
            // 
            this.radTextBox_MainName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_MainName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MainName.Location = new System.Drawing.Point(5, 88);
            this.radTextBox_MainName.Name = "radTextBox_MainName";
            this.radTextBox_MainName.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_MainName.TabIndex = 7;
            // 
            // radButton_MainSave
            // 
            this.radButton_MainSave.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_MainSave.Location = new System.Drawing.Point(3, 222);
            this.radButton_MainSave.Name = "radButton_MainSave";
            this.radButton_MainSave.Size = new System.Drawing.Size(92, 32);
            this.radButton_MainSave.TabIndex = 9;
            this.radButton_MainSave.Text = "บันทึก";
            this.radButton_MainSave.ThemeName = "Fluent";
            this.radButton_MainSave.Click += new System.EventHandler(this.RadButton_MainSave_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_MainSave.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_MainSave.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_MainSave.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainSave.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainSave.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainSave.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainSave.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainSave.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_MainCanCle
            // 
            this.radButton_MainCanCle.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_MainCanCle.Location = new System.Drawing.Point(98, 222);
            this.radButton_MainCanCle.Name = "radButton_MainCanCle";
            this.radButton_MainCanCle.Size = new System.Drawing.Size(92, 32);
            this.radButton_MainCanCle.TabIndex = 10;
            this.radButton_MainCanCle.Text = "ยกเลิก";
            this.radButton_MainCanCle.ThemeName = "Fluent";
            this.radButton_MainCanCle.Click += new System.EventHandler(this.RadButton_MainCanCle_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_MainCanCle.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_MainCanCle.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainCanCle.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainCanCle.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainCanCle.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainCanCle.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainCanCle.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_MainCanCle.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radCheckBox_MainSta);
            this.panel1.Controls.Add(this.radButton_mainEdit);
            this.panel1.Controls.Add(this.radButton_MainAdd);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radTextBox_MainRmk);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radTextBox_MainID);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBox_MainName);
            this.panel1.Controls.Add(this.radButton_MainCanCle);
            this.panel1.Controls.Add(this.radButton_MainSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(664, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 271);
            this.panel1.TabIndex = 5;
            // 
            // radCheckBox_MainSta
            // 
            this.radCheckBox_MainSta.AutoSize = true;
            this.radCheckBox_MainSta.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_MainSta.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_MainSta.Location = new System.Drawing.Point(5, 118);
            this.radCheckBox_MainSta.Name = "radCheckBox_MainSta";
            this.radCheckBox_MainSta.Size = new System.Drawing.Size(88, 20);
            this.radCheckBox_MainSta.TabIndex = 57;
            this.radCheckBox_MainSta.Text = "เปิดใช้งาน";
            this.radCheckBox_MainSta.UseVisualStyleBackColor = true;
            // 
            // radButton_mainEdit
            // 
            this.radButton_mainEdit.BackColor = System.Drawing.Color.Transparent;
            this.radButton_mainEdit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_mainEdit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButton_mainEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_mainEdit.Location = new System.Drawing.Point(34, 39);
            this.radButton_mainEdit.Name = "radButton_mainEdit";
            this.radButton_mainEdit.Size = new System.Drawing.Size(26, 26);
            this.radButton_mainEdit.TabIndex = 56;
            this.radButton_mainEdit.Text = "radButton2";
            this.radButton_mainEdit.Click += new System.EventHandler(this.RadButton_mainEdit_Click);
            // 
            // radButton_MainAdd
            // 
            this.radButton_MainAdd.BackColor = System.Drawing.Color.Transparent;
            this.radButton_MainAdd.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_MainAdd.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_MainAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_MainAdd.Location = new System.Drawing.Point(5, 39);
            this.radButton_MainAdd.Name = "radButton_MainAdd";
            this.radButton_MainAdd.Size = new System.Drawing.Size(26, 26);
            this.radButton_MainAdd.TabIndex = 53;
            this.radButton_MainAdd.Text = "radButton1";
            this.radButton_MainAdd.Click += new System.EventHandler(this.RadButton_MainAdd_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(5, 140);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(65, 19);
            this.radLabel4.TabIndex = 23;
            this.radLabel4.Text = "หมายเหตุ";
            // 
            // radTextBox_MainRmk
            // 
            this.radTextBox_MainRmk.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_MainRmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MainRmk.Location = new System.Drawing.Point(5, 161);
            this.radTextBox_MainRmk.Multiline = true;
            this.radTextBox_MainRmk.Name = "radTextBox_MainRmk";
            // 
            // 
            // 
            this.radTextBox_MainRmk.RootElement.StretchVertically = true;
            this.radTextBox_MainRmk.Size = new System.Drawing.Size(175, 53);
            this.radTextBox_MainRmk.TabIndex = 8;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(3, 2);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(73, 19);
            this.radLabel3.TabIndex = 20;
            this.radLabel3.Text = "งบเบิกหลัก";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(70, 22);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(32, 19);
            this.radLabel2.TabIndex = 19;
            this.radLabel2.Text = "รหัส";
            // 
            // radTextBox_MainID
            // 
            this.radTextBox_MainID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_MainID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MainID.Location = new System.Drawing.Point(67, 44);
            this.radTextBox_MainID.Name = "radTextBox_MainID";
            this.radTextBox_MainID.Size = new System.Drawing.Size(113, 21);
            this.radTextBox_MainID.TabIndex = 18;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(867, 601);
            this.radPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.25457F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.74543F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(867, 601);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Main, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(861, 277);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radGridView_Main
            // 
            this.radGridView_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Main.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Main.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Main.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Main.Name = "radGridView_Main";
            // 
            // 
            // 
            this.radGridView_Main.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Main.Size = new System.Drawing.Size(655, 271);
            this.radGridView_Main.TabIndex = 0;
            this.radGridView_Main.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Main_ViewCellFormatting);
            this.radGridView_Main.SelectionChanged += new System.EventHandler(this.RadGridView_Main_SelectionChanged);
            this.radGridView_Main.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Main_ConditionalFormattingFormShown);
            this.radGridView_Main.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Main_FilterPopupRequired);
            this.radGridView_Main.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Main_FilterPopupInitialized);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.Controls.Add(this.radGridView_Sub, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 286);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(861, 312);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // radGridView_Sub
            // 
            this.radGridView_Sub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Sub.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Sub.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Sub.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Sub.Name = "radGridView_Sub";
            // 
            // 
            // 
            this.radGridView_Sub.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Sub.Size = new System.Drawing.Size(655, 306);
            this.radGridView_Sub.TabIndex = 1;
            this.radGridView_Sub.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Sub_ViewCellFormatting);
            this.radGridView_Sub.SelectionChanged += new System.EventHandler(this.RadGridView_Sub_SelectionChanged);
            this.radGridView_Sub.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Sub_ConditionalFormattingFormShown);
            this.radGridView_Sub.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Sub_FilterPopupRequired);
            this.radGridView_Sub.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Sub_FilterPopupInitialized);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.radCheckBox_SubSta);
            this.panel2.Controls.Add(this.radButton_subEdit);
            this.panel2.Controls.Add(this.radTextBox_MainID_Sub);
            this.panel2.Controls.Add(this.radButton_SubAdd);
            this.panel2.Controls.Add(this.radLabel5);
            this.panel2.Controls.Add(this.radTextBox_SubRmk);
            this.panel2.Controls.Add(this.radLabel6);
            this.panel2.Controls.Add(this.radLabel7);
            this.panel2.Controls.Add(this.radTextBox_SubID);
            this.panel2.Controls.Add(this.radLabel8);
            this.panel2.Controls.Add(this.radTextBox_SubName);
            this.panel2.Controls.Add(this.radButton_SabCancle);
            this.panel2.Controls.Add(this.radButton_SubSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel2.Location = new System.Drawing.Point(664, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(194, 306);
            this.panel2.TabIndex = 6;
            // 
            // radCheckBox_SubSta
            // 
            this.radCheckBox_SubSta.AutoSize = true;
            this.radCheckBox_SubSta.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_SubSta.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_SubSta.Location = new System.Drawing.Point(7, 152);
            this.radCheckBox_SubSta.Name = "radCheckBox_SubSta";
            this.radCheckBox_SubSta.Size = new System.Drawing.Size(88, 20);
            this.radCheckBox_SubSta.TabIndex = 58;
            this.radCheckBox_SubSta.Text = "เปิดใช้งาน";
            this.radCheckBox_SubSta.UseVisualStyleBackColor = true;
            // 
            // radButton_subEdit
            // 
            this.radButton_subEdit.BackColor = System.Drawing.Color.Transparent;
            this.radButton_subEdit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_subEdit.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radButton_subEdit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButton_subEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_subEdit.Location = new System.Drawing.Point(36, 73);
            this.radButton_subEdit.Name = "radButton_subEdit";
            // 
            // 
            // 
            this.radButton_subEdit.RootElement.AutoSize = true;
            this.radButton_subEdit.RootElement.FocusBorderColor = System.Drawing.Color.Black;
            this.radButton_subEdit.Size = new System.Drawing.Size(26, 26);
            this.radButton_subEdit.TabIndex = 55;
            this.radButton_subEdit.Text = "radButton2";
            this.radButton_subEdit.Click += new System.EventHandler(this.RadButton_subEdit_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_subEdit.GetChildAt(0))).Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_subEdit.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_subEdit.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_subEdit.GetChildAt(0))).Text = "radButton2";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_subEdit.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_subEdit.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Layouts.ImageAndTextLayoutPanel)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1))).AutoSize = true;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1).GetChildAt(1))).AutoSize = true;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 9.75F);
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.radButton_subEdit.GetChildAt(0).GetChildAt(3))).AutoSize = true;
            // 
            // radTextBox_MainID_Sub
            // 
            this.radTextBox_MainID_Sub.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_MainID_Sub.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MainID_Sub.Location = new System.Drawing.Point(7, 31);
            this.radTextBox_MainID_Sub.Name = "radTextBox_MainID_Sub";
            this.radTextBox_MainID_Sub.Size = new System.Drawing.Size(173, 21);
            this.radTextBox_MainID_Sub.TabIndex = 54;
            // 
            // radButton_SubAdd
            // 
            this.radButton_SubAdd.BackColor = System.Drawing.Color.Transparent;
            this.radButton_SubAdd.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_SubAdd.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_SubAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_SubAdd.Location = new System.Drawing.Point(7, 73);
            this.radButton_SubAdd.Name = "radButton_SubAdd";
            this.radButton_SubAdd.Size = new System.Drawing.Size(26, 26);
            this.radButton_SubAdd.TabIndex = 53;
            this.radButton_SubAdd.Text = "radButton2";
            this.radButton_SubAdd.Click += new System.EventHandler(this.RadButton_SubAdd_Click);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(7, 174);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(65, 19);
            this.radLabel5.TabIndex = 23;
            this.radLabel5.Text = "หมายเหตุ";
            // 
            // radTextBox_SubRmk
            // 
            this.radTextBox_SubRmk.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_SubRmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SubRmk.Location = new System.Drawing.Point(7, 195);
            this.radTextBox_SubRmk.Multiline = true;
            this.radTextBox_SubRmk.Name = "radTextBox_SubRmk";
            // 
            // 
            // 
            this.radTextBox_SubRmk.RootElement.StretchVertically = true;
            this.radTextBox_SubRmk.Size = new System.Drawing.Size(173, 53);
            this.radTextBox_SubRmk.TabIndex = 12;
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(3, 2);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(72, 19);
            this.radLabel6.TabIndex = 20;
            this.radLabel6.Text = "งบเบิกย่อย";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(69, 56);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(32, 19);
            this.radLabel7.TabIndex = 19;
            this.radLabel7.Text = "รหัส";
            // 
            // radTextBox_SubID
            // 
            this.radTextBox_SubID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_SubID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SubID.Location = new System.Drawing.Point(69, 78);
            this.radTextBox_SubID.Name = "radTextBox_SubID";
            this.radTextBox_SubID.Size = new System.Drawing.Size(111, 21);
            this.radTextBox_SubID.TabIndex = 18;
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(7, 104);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(65, 19);
            this.radLabel8.TabIndex = 17;
            this.radLabel8.Text = "คำอธิบาย";
            // 
            // radTextBox_SubName
            // 
            this.radTextBox_SubName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_SubName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SubName.Location = new System.Drawing.Point(7, 123);
            this.radTextBox_SubName.Name = "radTextBox_SubName";
            this.radTextBox_SubName.Size = new System.Drawing.Size(173, 21);
            this.radTextBox_SubName.TabIndex = 11;
            // 
            // radButton_SabCancle
            // 
            this.radButton_SabCancle.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_SabCancle.Location = new System.Drawing.Point(100, 254);
            this.radButton_SabCancle.Name = "radButton_SabCancle";
            this.radButton_SabCancle.Size = new System.Drawing.Size(92, 32);
            this.radButton_SabCancle.TabIndex = 14;
            this.radButton_SabCancle.Text = "ยกเลิก";
            this.radButton_SabCancle.ThemeName = "Fluent";
            this.radButton_SabCancle.Click += new System.EventHandler(this.RadButton_SabCancle_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SabCancle.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SabCancle.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SabCancle.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SabCancle.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SabCancle.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SabCancle.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SabCancle.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_SubSave
            // 
            this.radButton_SubSave.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_SubSave.Location = new System.Drawing.Point(2, 254);
            this.radButton_SubSave.Name = "radButton_SubSave";
            this.radButton_SubSave.Size = new System.Drawing.Size(92, 32);
            this.radButton_SubSave.TabIndex = 13;
            this.radButton_SubSave.Text = "บันทึก";
            this.radButton_SubSave.ThemeName = "Fluent";
            this.radButton_SubSave.Click += new System.EventHandler(this.RadButton_SubSave_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SubSave.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SubSave.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SubSave.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SubSave.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // SmartRO_GroupMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 601);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "I_GroupMain";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "กลุ่มการเบิก";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.I_GroupMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_MainSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_MainCanCle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_mainEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_MainAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainRmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Main.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Main)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Sub.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Sub)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_subEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MainID_Sub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SubAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SubRmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SubID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SubName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SabCancle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SubSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MainName;
        protected Telerik.WinControls.UI.RadButton radButton_MainSave;
        protected Telerik.WinControls.UI.RadButton radButton_MainCanCle;
        protected System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Main;
        private Telerik.WinControls.UI.RadGridView radGridView_Sub;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MainID;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MainRmk;
        private Telerik.WinControls.UI.RadButton radButton_MainAdd;
        protected System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadButton radButton_SubAdd;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SubRmk;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SubID;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SubName;
        protected Telerik.WinControls.UI.RadButton radButton_SabCancle;
        protected Telerik.WinControls.UI.RadButton radButton_SubSave;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MainID_Sub;
        private Telerik.WinControls.UI.RadButton radButton_subEdit;
        private Telerik.WinControls.UI.RadButton radButton_mainEdit;
        private System.Windows.Forms.CheckBox radCheckBox_MainSta;
        private System.Windows.Forms.CheckBox radCheckBox_SubSta;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    }
}
