﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.Promotion
{
    public partial class Promotion_AddImg : Telerik.WinControls.UI.RadForm
    {
        readonly string _pCase;
        readonly string _pPermission;
        public Promotion_AddImg(string pCase, string pPermission) //1 : ดูได้อย่างเดียว, 0 : เพิ่มข้อมูล
        {
            InitializeComponent();
            _pCase = pCase;
            _pPermission = pPermission;
            
            if (_pPermission != "0") panel1.Visible = false; //เช็คสาขา

            radButton_Cancel.ButtonElement.ShowBorder = true; radButton_Save.ButtonElement.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มป้ายโปรโมชัน";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date1, DateTime.Now.AddDays(7), DateTime.Now.AddDays(120));

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FORMNAME", "รหัส", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "รายละเอียด", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันหมดอายุ", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("FILENAME", "รูป"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CDATE", "เช็ควันหมดโปรโมชัน"));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image1", "รูป", 200)));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOINS", "พนักงาน", 200));
            RadGridView_Show.TableElement.RowHeight = 120;

            DatagridClass.SetCellBackClolorByExpression("DATEINS", "CDATE <> '1' ", ConfigClass.SetColor_Red(), RadGridView_Show);
            radLabel1.Text = "สีแดง << โปรโมชันหมดอายุ | DoubleClick ที่รูป >> เพื่อดูรูปใหญ่";
        }
        //ClearData
        void ClearData()
        {
            radTextBox_ID.Enabled = false; radTextBox_ID.Text = "";
            radTextBox_Detail.Enabled = false; radTextBox_Detail.Text = "";
            pictureBox_Show.Image = null;
            radBrowseEditor_choose.Value = null;
            radButton_Cancel.Enabled = false;
            radButton_Save.Enabled = false;
            radDateTimePicker_Date1.Enabled = false;
            radBrowseEditor_choose.Enabled = false;
        }
        //Load Main
        private void ConfigBranch_GenaralDetail_Load(object sender, EventArgs e)
        {
            Set_DGV();
        }

        //Set DGV
        void Set_DGV()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dt = FormClass.DataFormSHOP_DOCUMENT("",_pCase);
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                RadGridView_Show.Rows[i].Cells["Image1"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dt.Rows[i]["FILENAME"].ToString());
            }
            ClearData();
            this.Cursor = Cursors.Default;
        }
        //Reset All
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        //Insert
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            UpFileToFolder();
        }

        private void UpFileToFolder()
        {
            string datefolder = radDateTimePicker_Date1.Value.ToString("yyyy-MM-dd");
            string path = $@"{PathImageClass.pPathPromotion}{datefolder}"; 
            string fileName = radBrowseEditor_choose.Value.ToString();

            if (!Directory.Exists(path)) Directory.CreateDirectory(path);//สร้างโฟร์เดอร์วันที่หมดโปรโมชัน
            string PATHFILE = $@"{path}\{radTextBox_ID.Text}.jpg";

            try
            {
                if (!ImageClass.CheckFileExit(PATHFILE)) File.Copy(fileName, PATHFILE); //copy ไฟล์ไปไว้ในโฟร์เดอร์ \\192.168.100.77\ImageMinimark\CheckPrice\Promotion\{วันที่หมดโปรโมชัน}

                string SAVE = FormClass.Document_Save(radTextBox_ID.Text, "00022", PATHFILE, _pCase, $@"'{datefolder}'", radTextBox_Detail.Text);
                MsgBoxClass.MsgBoxShow_SaveStatus(SAVE);
                if (SAVE == "")
                {
                    ClearData();
                    Set_DGV();
                }
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"มีปัญหาในการ copy File ลองใหม่อีกครั้ง {Environment.NewLine}{ex.Message}");
                throw;
            }
        }

        //ForInsert
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            radTextBox_ID.Enabled = true; radTextBox_ID.MaxLength = 5; radTextBox_ID.Focus();
            radTextBox_Detail.Enabled = false;
            radDateTimePicker_Date1.Enabled = false;
            radBrowseEditor_choose.Enabled = false;
            radButton_Cancel.Enabled = true;
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //ID Enter
        private void RadTextBox_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_ID.Text == "") return;
                DataTable dtName = FormClass.DataFormSHOP_DOCUMENT(radTextBox_ID.Text,_pCase);
                if (dtName.Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสโปรโมชันนี้มีอยู่แล้ว ไม่ต้องเพิ่มซ้ำ.");
                    radTextBox_ID.Text = ""; radTextBox_ID.Focus();
                    return;
                }

                radTextBox_ID.Enabled = false;
                radTextBox_Detail.Enabled = true;
                radTextBox_Detail.Focus();
            }
        }
        //Detail Enter
        private void RadTextBox_Detail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Detail.Text == "") return;
                radTextBox_Detail.Enabled = false;
                radDateTimePicker_Date1.Enabled = true;
                radDateTimePicker_Date1.Focus();
                radBrowseEditor_choose.Enabled = true;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "ป้ายโปรโมชัน");
        }

        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;

            try
            {
                this.pictureBox_Show.Image = ImageClass.ScaleImageData_SendPath(this.radBrowseEditor_choose.Value);
                radBrowseEditor_choose.Enabled = false;
                radButton_Save.Enabled = true;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถแสดงรูปที่เลือกได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
                return;
            }
        }

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Image1":
                    System.Diagnostics.Process.Start(RadGridView_Show.CurrentRow.Cells["FILENAME"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
    }
}
