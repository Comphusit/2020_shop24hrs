﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Promotion
{
    public partial class Promotion_Report : Telerik.WinControls.UI.RadForm
    {

        readonly string _pPerMission;
        readonly string _pTypeReport;
        //0-สินค้าแถม
        //1-เปิด-ปิด โปรโมชั่น
        //2-ลูกค้าที่เช่าของตลาดนัดสาขาใหญ่ สำหรับแจกคูปอง

        DataTable dtBch;
        DataTable dt_Data = new DataTable();

        //Load
        public Promotion_Report(string pTypeReport, string pPerMission)
        {
            InitializeComponent();

            _pTypeReport = pTypeReport;
            _pPerMission = pPerMission;
        }
        //Load
        private void Promotion_Report_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_A.ToolTipText = "เพิ่มข้อมูลลูกค้า"; radButtonElement_A.ShowBorder = true;

            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;


            radDateTimePicker_D2.Visible = true;
            radDateTimePicker_D1.Visible = true;
            radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
            radLabel_Date.Visible = true;
            RadCheckBox_1.Visible = true;
            RadDropDownList_1.Visible = true;
            radLabel_Detail.Visible = true;


            if (SystemClass.SystemBranchID == "MN000")
            {
                RadCheckBox_1.Enabled = true;
                dtBch = BranchClass.GetBranchAll("'1','4'", " '1'");
            }
            else
            {
                RadCheckBox_1.CheckState = CheckState.Checked;
                RadCheckBox_1.Enabled = false;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
            }

            RadDropDownList_1.DataSource = dtBch;
            RadDropDownList_1.DisplayMember = "NAME_BRANCH";
            RadDropDownList_1.ValueMember = "BRANCH_ID";


            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Text = "สีแดง >> ไม่ได้แถมสินค้าให้ลูกค้า"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "วันที่ขาย";
                    radButtonElement_A.Enabled = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateSale", "วันที่บิล", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนแถม", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "แคชเชียร์", 220)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REAMARK", "หมายเหตุไม่แถม", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEALIAS", "โปรโมชั่น", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STAFREE", "STAFREE")));

                    RadGridView_ShowHD.MasterTemplate.Columns["INVENTLOCATIONID"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["INVOICEID"].IsPinned = true;

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "STAFREE = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["INVOICEID"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["NAMEALIAS"].ConditionalFormattingObjectList.Add(obj2);

                    break;
                case "1":
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Text = "วันที่ดำเนินการ";
                    radButtonElement_A.Enabled = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PROMID", "รหัสโปร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PROMNAMEONRECEIPT", "ชื่อบนใบเสร็จ", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่ดำเนินการ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ผู้ที่ดำเนินการ", 220)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 250)));

                    break;
                case "2":
                    RadCheckBox_1.Visible = false; RadDropDownList_1.Visible = false;
                    radLabel_Date.Visible = false;
                    radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    radLabel_Detail.Visible = true;
                    radButtonElement_A.Enabled = true;
                    if (_pPerMission == "0") radLabel_Detail.Text = "สีแดง > ยังไมได้จ่ายคูปองลูกค้า"; else radLabel_Detail.Text = "DoubleClick >> สำหรับการบันทึกจ่ายคูปอง | สีแดง > ยังไมได้จ่ายคูปองลูกค้า";

                    RadCheckBox_1.Enabled = true; RadCheckBox_1.Visible = true; RadCheckBox_1.Checked = true;
                    RadCheckBox_1.Text = "เฉพาะที่ยังไม่แจกคูปอง";
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("YEAR", "ปี", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MONTH", "เดือน", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE", "โซน", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSITION", "ตำแหน่ง", 70)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "รหัสลูกค้า", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TEL", "เบอร์โทร", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LINEID", "LineID", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DISCOUNT", "ยอดคูปอง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOUPD", "ผู้จ่าย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAMEUPD", "ชื่อผู้ที่จ่ายคูปอง", 220)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEUPD", "วันที่-เวลา จ่ายคูปอง", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ชื่อผู้บันทึก", 220)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINSA", "วันที่-เวลา", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "STA")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CLICK", "กดยกเลิก", 80)));

                    ExpressionFormattingObject obj2_1 = new ExpressionFormattingObject("MyCondition1", "STA = '0' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["AccountNum"].ConditionalFormattingObjectList.Add(obj2_1);
                    RadGridView_ShowHD.Columns["Name"].ConditionalFormattingObjectList.Add(obj2_1);
                    RadGridView_ShowHD.Columns["REMARK"].ConditionalFormattingObjectList.Add(obj2_1);

                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0":
                    //string pCon1 = "", pCon2 = "";
                    //if (RadCheckBox_1.Checked == true)
                    //{
                    //    pCon1 = " AND INVENTLOCATIONID = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
                    //    pCon2 = " AND XXX_POSLINE.POSGROUP  = '" + RadDropDownList_1.SelectedValue.ToString() + @"'   ";
                    //}

                    //string sql = string.Format(@"
                    //SELECT	TMP.INVENTLOCATIONID,BRANCH_NAME,CONVERT(VARCHAR,INVOICEDATE,23) AS INVOICEDATE1 ,INVOICEID,
		                  //  BARCODE.ITEMBARCODE, BARCODE.SPC_ITEMNAME, '1' AS QTY,'' AS SALESUNIT,'' AS NAMEALIAS, 
                    //        TMP.CASHIERID + ' ' + TMP.SPC_NAME AS SPC_NAME,TMP.REAMARK ,'0' AS STAFREE,CONVERT(VARCHAR,DateSale,25) AS DateSale 
                    //FROM	(SELECT POSGROUP AS INVENTLOCATIONID,BRANCH_NAME ,  
		                  //  SUBSTRING(DESCRIPTION, 1, CHARINDEX(';', DESCRIPTION, 0) - 1) AS ITEMBARCODE, dbo.split(DESCRIPTION,';',3) AS REAMARK, 
		                  //  CASHIERID, EMPLTABLE.SPC_NAME, INVOICEID,CONVERT(varchar,CONVERT(DATE,SYSUSERLOG.CREATEDDATETIME),23)   AS INVOICEDATE,CONVERT(VARCHAR,SYSUSERLOG.CREATEDDATETIME,25) AS DateSale 
		                  //  FROM SYSUSERLOG WITH (NOLOCK) INNER JOIN EMPLTABLE WITH (NOLOCK) ON SYSUSERLOG.CASHIERID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'   
		                  //  INNER JOIN Shop_Branch WITH (NOLOCK) ON SYSUSERLOG.POSGROUP = Shop_Branch .BRANCH_ID  
                    //WHERE        (KBMODIFIERS = N'None')  AND (KEYNAME = N'PageUp') AND (CONVERT(DATE,SYSUSERLOG.CREATEDDATETIME) 
		                  //  BETWEEN '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"') 
		                  //  GROUP BY POSGROUP, SUBSTRING(DESCRIPTION, 1, CHARINDEX(';', DESCRIPTION, 0) - 1), dbo.split(DESCRIPTION,';',3), CASHIERID, 
		                  //  EMPLTABLE.SPC_NAME, INVOICEID, BRANCH_NAME,SYSUSERLOG.CREATEDDATETIME,(CONVERT(DATE,SYSUSERLOG.CREATEDDATETIME)) ) AS TMP  
		                  //  INNER JOIN INVENTITEMBARCODE AS BARCODE ON BARCODE.ITEMBARCODE = TMP.ITEMBARCODE WHERE BARCODE.DATAAREAID = 'SPC'   
                    //        " + pCon1 + @"
                    //        AND INVENTLOCATIONID != 'RT'  

                    //UNION 

                    //SELECT	XXX_POSLINE.POSGROUP AS INVENTLOCATIONID,BRANCH_NAME,CONVERT(VARCHAR,XXX_POSTABLE.INVOICEDATE,23) AS INVOICEDATE ,XXX_POSTABLE.INVOICEID, 
		                  //  XXX_POSLINE.ITEMBARCODE,XXX_POSLINE.NAME AS SPC_ITEMNAME,XXX_POSLINE.QTY,XXX_POSLINE.SALESUNIT,XXX_POSLINE.NAMEALIAS, 
		                  //  CASHIERID + ' ' + SPC_NAME AS SPC_NAME,'' AS REAMARK,'1' AS STAFREE,CONVERT(VARCHAR,XXX_POSLINE.CREATEDDATETIME,25) as DateSale  
                    //FROM	XXX_POSTABLE WITH (NOLOCK)  
		                  //  INNER JOIN XXX_POSLINE  WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID  
		                  //  INNER JOIN EMPLTABLE WITH (NOLOCK) ON XXX_POSTABLE.CASHIERID = EMPLTABLE.EMPLID 
		                  //  INNER JOIN Shop_Branch WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = Shop_Branch.BRANCH_ID  
                    //WHERE	SUPPITEMGROUPID = '/FREEITEM/' 
		                  //  AND XXX_POSTABLE.INVOICEDATE BETWEEN '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"'
		                  //  AND XXX_POSLINE.INVOICEDATE BETWEEN '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"'
		                  //  AND XXX_POSLINE.POSGROUP LIKE 'MN%' 
		                  //  AND XXX_POSLINE.DOCUTYPE = '1' AND XXX_POSLINE.SIGN = '1' 
		                  //  AND EMPLTABLE.DATAAREAID = N'SPC'  
                    //        " + pCon2 + @"
                    //        AND XXX_POSLINE.POSGROUP  != 'RT' 

                    //ORDER BY   INVENTLOCATIONID,ITEMBARCODE,DateSale

                    //");
                    string bch = "";
                    if (RadCheckBox_1.Checked == true) bch = RadDropDownList_1.SelectedValue.ToString();
                    dt_Data = PosSaleClass.Promotion_DataAll(bch, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_POSRetail707(sql);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    break;
                case "1":
                    string pCon = "";
                    if (RadCheckBox_1.Checked == true)
                    {
                        pCon = " AND INVENTLOCATIONID = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
                    }

                    string sql1 = string.Format(@"
                        SELECT  [PROMID],[PROMNAMEONRECEIPT],[INVENTLOCATIONID],[NAME],[REMARK],
                                CONVERT(VARCHAR,[DATEINS],25) AS DATEINS,[WHOINS] + '-' +[WHONAMEINS] AS WHONAMEINS 
                        FROM    [SHOP_RETAILPROMOTIONHISTORY] WITH (NOLOCK)
                        WHERE   CONVERT(VARCHAR,DATEINS,23) BETWEEN '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' 
                                AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"'   " + pCon + @"
                        ORDER BY DATEINS
                    ");
                    dt_Data = ConnectionClass.SelectSQL_Main(sql1);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    break;

                case "2":
                    string pConRent = "";
                    if (RadCheckBox_1.Checked == true)
                    {
                        pConRent = " WHERE STA = '0' ";
                    }
                    string sql2 = $@"
                        SELECT	*,CONVERT(VARCHAR,DATEINS,25) AS DATEINSA,'Click' AS Click	FROM	SHOP_CUSTOMER_RENT WITH (NOLOCK)
                        {pConRent}
                        ORDER BY YEAR,MONTH,ACCOUNTNUM
                    ";
                    dt_Data = ConnectionClass.SelectSQL_Main(sql2);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    break;
                default:
                    break;
            }

            this.Cursor = Cursors.Default;

        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now;
            if (_pTypeReport == "2") SetDGV_HD();

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked)
            {
                RadDropDownList_1.Enabled = true;
            }
            else
            {
                RadDropDownList_1.Enabled = false;
            }
        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (_pTypeReport != "2") return;
            if (_pPerMission == "0") return;

            if (e.Column.Name == "CLICK") return;

            string pCase = "0";
            if (RadGridView_ShowHD.CurrentRow.Cells["STA"].Value.ToString() != "0")
            {
                //MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้านี้มีบันทึกการจ่ายคูปองไปเรียบร้อยแล้ว" + Environment.NewLine + "ไม่สามารถบันทึกข้อมูลซ้ำได้อีก.");
                //return;
                pCase = "1";
            }

            string position = RadGridView_ShowHD.CurrentRow.Cells["POSITION"].Value.ToString();
            string accountNum = RadGridView_ShowHD.CurrentRow.Cells["ACCOUNTNUM"].Value.ToString();
            double grand = Convert.ToDouble(RadGridView_ShowHD.CurrentRow.Cells["DISCOUNT"].Value.ToString());
            string month = RadGridView_ShowHD.CurrentRow.Cells["MONTH"].Value.ToString();
            string year = RadGridView_ShowHD.CurrentRow.Cells["YEAR"].Value.ToString();

            Promotion_ShowDataDGV showDataDGV = new Promotion_ShowDataDGV(pCase, accountNum, position, grand, month, year);
            if (showDataDGV.ShowDialog() == DialogResult.Yes)
            {
                RadGridView_ShowHD.CurrentRow.Cells["STA"].Value = "1";
                RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value = "จ่ายคูปองแล้ว";
                RadGridView_ShowHD.CurrentRow.Cells["WHOUPD"].Value = SystemClass.SystemUserID_M;
                RadGridView_ShowHD.CurrentRow.Cells["WHONAMEUPD"].Value = SystemClass.SystemUserName;
                RadGridView_ShowHD.CurrentRow.Cells["DATEUPD"].Value = DateTime.Now.ToString();
            }
        }
        //เพิ่มคูปอง
        private void RadButtonElement_A_Click(object sender, EventArgs e)
        {
            GeneralForm.LineCoupon.LineCoupon_Add add = new LineCoupon.LineCoupon_Add();
            if (add.ShowDialog() == DialogResult.Yes)
            {
            }
            SetDGV_HD();
        }

        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (_pTypeReport != "2") return;
            if (_pPerMission == "0") return;
            //
            if (e.Column.Name == "CLICK")
            {
                string accountNum = RadGridView_ShowHD.CurrentRow.Cells["ACCOUNTNUM"].Value.ToString();
                string accountName = RadGridView_ShowHD.CurrentRow.Cells["Name"].Value.ToString();
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการยกเลืกการจ่ายคูปอง{Environment.NewLine}รหัสลูกค้า {accountNum}{Environment.NewLine}ชื่อลูกค้า{accountName}?") == DialogResult.No) return;

                string position = RadGridView_ShowHD.CurrentRow.Cells["POSITION"].Value.ToString();

                string dateins = RadGridView_ShowHD.CurrentRow.Cells["DATEINSA"].Value.ToString();
                string month = RadGridView_ShowHD.CurrentRow.Cells["MONTH"].Value.ToString();
                string year = RadGridView_ShowHD.CurrentRow.Cells["YEAR"].Value.ToString();

                string sql = $@"
                    UPDATE	SHOP_CUSTOMER_RENT	
                    SET     STA = '2' 
                    WHERE   MONTH = '{month}' AND YEAR = '{year}' AND POSITION = '{position}' AND ACCOUNTNUM = '{accountNum}' AND DATEINS = '{dateins}'
                ";

                string resualt = ConnectionClass.ExecuteSQL_Main(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                if (resualt == "") SetDGV_HD();

            }

        }
    }
}
