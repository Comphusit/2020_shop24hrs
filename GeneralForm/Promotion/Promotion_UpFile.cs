﻿//CheckOK
using System;
using System.IO;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Promotion
{
    public partial class Promotion_UpFile : Telerik.WinControls.UI.RadForm
    {
        readonly string _pPath;
        public Promotion_UpFile(string pPath)
        {
            InitializeComponent();
            _pPath = pPath;
        }
        //clear
        void ClearData()
        {
            radDateTimePicker_End.Value = DateTime.Now.AddDays(7);
            radDateTimePicker_End.MinDate = DateTime.Now.AddDays(1);
            radButton_Upload.Enabled = false;
            radBrowseEditor_choose.Enabled = true;
            if (!(radBrowseEditor_choose is null)) radBrowseEditor_choose.Value = null;

            if (pictureBox_Show.Image != null)
            {
                pictureBox_Show.Image.Dispose();
                pictureBox_Show.Image = null;
            }
        }
        //Load
        private void Promotion_UpFile_Load(object sender, EventArgs e)
        {
            ClearData();
            radBrowseEditor_choose.BrowseElement.Font = SystemClass.SetFontGernaral;
            radBrowseEditor_choose.BrowseElement.BrowseButton.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now.AddDays(7), DateTime.Now.AddDays(365));

            this.radBrowseEditor_choose.DialogType = BrowseEditorDialogType.OpenFileDialog;
            this.radBrowseEditor_choose.ReadOnly = true;

            radBrowseEditor_choose.BrowseElement.ToolTipText = "เลือกรูปที่ต้องการ";
            radButton_OpenView.ButtonElement.ShowBorder = true; radButton_OpenView.ButtonElement.ToolTipText = "เปลี่ยนรูปใหม่";
            radButton_Upload.ButtonElement.ShowBorder = true; radButton_Upload.ButtonElement.ToolTipText = "Upload";
            radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            radButton_Upload.ButtonElement.Font = SystemClass.SetFontGernaral;
            radDateTimePicker_End.MinDate = DateTime.Now.AddDays(1);

        }

        //Value Change
        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;

            try
            {
                this.pictureBox_Show.Image = ImageClass.ScaleImageData_SendPath(this.radBrowseEditor_choose.Value);
                radBrowseEditor_choose.Enabled = false;
                radButton_Upload.Enabled = true;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถแสดงรูปที่เลือกได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
                return;
            }

        }
        //value change
        private void RadBrowseEditor_choose_ValueChanging(object sender, Telerik.WinControls.UI.ValueChangingEventArgs e)
        {
            try
            {
                e.Cancel = !File.Exists(e.NewValue.ToString());
                return;
            }
            catch (Exception)
            {
                return;
            }
        }
        //browse
        private void RadBrowseEditor_choose_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = (OpenFileDialog)radBrowseEditor_choose.Dialog;
            dialog.Filter = "JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg";
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        ////Upload
        private void RadButton_Upload_Click(object sender, EventArgs e)
        {
            //Check File 
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == ""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกรูปให้เรียบร้อยก่อนการ Upload.");
                return;
            }
            //Upload
            try
            {

                //check Path
                if (Directory.Exists(_pPath) == false) Directory.CreateDirectory(_pPath);

                string pFileName = radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + "-" +
                    DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "") + "_" + SystemClass.SystemUserID + @".jpg";
                File.Copy(radBrowseEditor_choose.Value.ToString(), _pPath + pFileName);

                pictureBox_Show.Image.Dispose();
                pictureBox_Show.Image = null;

                MsgBoxClass.MsgBoxShowButtonOk_Imformation("Upload รูปเสร็จสมบูรณ์.");

                ClearData();

            }
            catch (Exception ex)
            {
               MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Upload รูปได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
                return;
            }
        }

        private void RadButton_OpenView_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}

