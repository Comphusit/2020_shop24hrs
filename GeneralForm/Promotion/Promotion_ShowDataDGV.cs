﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Promotion
{
    public partial class Promotion_ShowDataDGV : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData;
        readonly string _pCase;
        readonly string _cstID;
        readonly string _position;
        readonly double _grand;
        readonly string _month;
        readonly string _year;

        public Promotion_ShowDataDGV(string pCase, string cstID, string position, double grand, string month, string year)
        {
            InitializeComponent();
            _pCase = pCase;
            _cstID = cstID;
            _position = position;
            _grand = grand;
            _month = month;
            _year = year;
        }

        private void Promotion_ShowDataDGV_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("CC", "เลือก", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STA_CONTROL", "รหัสคูปอง", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COUPONVALUE", "มูลค่า", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขาที่ใช้ได้", 150)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 400)));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMEBEGIN", "วันที่เริ่มใช้", 160));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMEEND", "วันที่สิ้นสุด", 160));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("COUPONNUMBER", "COUPONNUMBER"));

            radGridView_Show.MasterTemplate.Columns["CC"].IsPinned = true;

            radLabel_Head.Text = $@"ยอดรวมต้องจ่ายคูปอง {_grand:#,#0.00} บาท";
            string sql;
            if (_pCase == "0")
            {
                sql = $@"
                SELECT	'0' AS CC,COUPONVALUE,REMARK,DATETIMEBEGIN,DATETIMEEND,TYPEUSE_NAME,BRANCH_NAME,STA_CONTROL,COUPONNUMBER
                FROM	SHOP_COUPONONLINE WITH (NOLOCK)
                WHERE	STA_CONTROL != ''
		                AND DATETIMEEND > GETDATE()
		                AND STA_ACTIVE = '1'
            ";
                radButton_Save.Enabled = true;
                radLabel_Head.ForeColor = ConfigClass.SetColor_Red();
            }
            else
            {
                sql = $@"
                    SELECT	'1' AS CC, 
		                    COUPONVALUE,REMARK,DATETIMEBEGIN,DATETIMEEND,TYPEUSE_NAME,BRANCH_NAME,STA_CONTROL,SHOP_COUPONONLINE_CUST.COUPONNUMBER
                    FROM	SHOP_COUPONONLINE_CUST WITH (NOLOCK) INNER JOIN SHOP_COUPONONLINE WITH (NOLOCK)
		                    ON SHOP_COUPONONLINE_CUST.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
                    WHERE	MONTH = '{_month}' AND YEAR = '{_year}'
		                    AND CUSTACCOUNT = '{_cstID}' AND POSITION = '{_position}'
                ";
                radButton_Save.Enabled = false;
                radLabel_Head.ForeColor = ConfigClass.SetColor_Blue();
            }

            dtData = ConnectionClass.SelectSQL_POSRetail707(sql);
            radGridView_Show.DataSource = dtData;
            dtData.AcceptChanges();
        }
        //close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //choose
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            ArrayList sqlAX = new ArrayList();
            ArrayList sql24 = new ArrayList();

            int iCheck = 0;
            double iGrand = 0;

            foreach (GridViewRowInfo item in radGridView_Show.Rows)
            {
                if (item.Cells["CC"].Value.ToString() == "1")
                {
                    iCheck++;
                    iGrand += Convert.ToDouble(item.Cells["COUPONVALUE"].Value.ToString());
                    sqlAX.Add($@"
                    INSERT INTO     SHOP_COUPONONLINE_CUST(CUSTACCOUNT,LINECOUPON,COUPONNUMBER,
                                    MONTH,YEAR,POSITION,
                                    WHOINS,WHONAMEINS)
                    values          ('{_cstID}','{item.Cells["STA_CONTROL"].Value}','{item.Cells["COUPONNUMBER"].Value}',
                                    '{_month}','{_year}','{_position}',
                                    '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}')
                    ");

                    sqlAX.Add($@"
                        UPDATE SHOP_COUPONONLINE   SET MAXUSE = MAXUSE + 1
                        WHERE COUPONNUMBER = '{item.Cells["COUPONNUMBER"].Value}'
                    ");
                }
            }

            if (iCheck == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุคูปองที่จ่ายให้เรียบร้อยก่อนกดบันทึก");
                return;
            }

            if (_grand != iGrand)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ยอดคูปองที่ต้องได้และได้รับไม่เท่ากัน {Environment.NewLine} เช็คข้อมูลที่ระบุใหม่อีกครั้ง ก่อนการบันทึก");
                return;
            }

            sql24.Add($@"
                UPDATE  SHOP_CUSTOMER_RENT
                SET     STA = '1',REMARK = 'จ่ายคูปองแล้ว',WHOUPD = '{SystemClass.SystemUserID_M}',WHONAMEUPD = '{SystemClass.SystemUserName}',DATEUPD = GETDATE()
                WHERE   Position = '{_position}' AND AccountNum = '{_cstID}'
            ");

            string result = ConnectionClass.Execute_SameTime_2Server(sql24, IpServerConnectClass.ConSelectMain, sqlAX, IpServerConnectClass.ConMainRatail707);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else
            {
                return;
            }
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion
        //Choose coupon
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pCase == "1") return;

            if (radGridView_Show.Rows.Count == 0) return;
            if (e.Column.Name != "CC") return;

            if (radGridView_Show.CurrentRow.Cells["CC"].Value.ToString() == "1") radGridView_Show.CurrentRow.Cells["CC"].Value = "0"; else radGridView_Show.CurrentRow.Cells["CC"].Value = "1";

        }
    }
}
