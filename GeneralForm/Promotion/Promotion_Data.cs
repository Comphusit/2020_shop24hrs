﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Promotion
{
    public partial class Promotion_Data : Telerik.WinControls.UI.RadForm
    {
        DataTable dtData = new DataTable();
        DataTable dtBarcode = new DataTable();
        readonly DataTable dtBchSTK = new DataTable();
        DataTable dtBch = new DataTable();
        readonly string _pPermission;
        string pPromIdSelect;
        //Load
        public Promotion_Data(string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load
        private void Promotion_Data_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowBch);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);


            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButtonElement_Excel.ShowBorder = true; RadButtonElement_Excel.ToolTipText = "Export Excel";

            RadGridView_ShowBch.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("STOCK", "สต็อก", 80)));
            RadGridView_ShowBch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
            RadGridView_ShowBch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 160)));
            RadGridView_ShowBch.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("USECONFIRM", "ยืนยัน")));
            RadGridView_ShowBch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "ยืนยัน")));
            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "STA = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowBch.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowBch.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowBch.Columns["STOCK"].FormatString = "{0:#,##0.00}";

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PromId", "รหัสโปร", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PROMNAMEONRECEIPT", "ชื่อบนใบเสร็จ", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ซื้อจำนวน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "ซื้อปริมาณ", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LIFEFROM", "เริ่มโปร", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LIFETO", "สิ้นสุดโปร", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BARCODEFREE", "บาร์โค้ด", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEONRECEIPT", "ชื่อของแถม", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYFREE", "จำนวนแถม", 110)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYHIGHEST", "จำนวนสูงสุดในบิล", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Description", "จัดซื้อ", 200)));
            RadGridView_ShowHD.Columns["PromId"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemId", "รหัสสินค้า", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTDIMID", "มิติสินค้า", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 400)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

            dtBchSTK.Columns.Add("STOCK");
            dtBchSTK.Columns.Add("BRANCH_ID");
            dtBchSTK.Columns.Add("BRANCH_NAME");
            dtBchSTK.Columns.Add("USECONFIRM"); // 0 ไม่ต้องใช้การยืนยัน (พิมพสลิปท้ายบิลเลย) 1 ต้องยืนยันการแถม
            dtBchSTK.Columns.Add("STA");// 0 ไม่เปิดใช้งาน 1 เปิดใช้งาน

            if (SystemClass.SystemBranchID == "MN000")
            {
                dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                radLabel_Detail.Text = "สีแดง >> สาขาที่ปิดโปร | DoubleClick สาขา >> ปิด-เปิด โปร";
            }
            else
            {
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                radLabel_Detail.Text = "ต้องการ ปิด-เปิด โปรโมชั่น >> ติดต่อ โปรโมชั่น Tel.1602 หรือ CenterShop Tel.1022 ";
            }

            ClearTxt();
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            //string pCon = "";
            //if (SystemClass.SystemBranchID != "MN000")
            //{
            //    pCon = " AND	SPC_RetailPromotionLocation.INVENTLOCATIONID  = '" + SystemClass.SystemBranchID + @"' ";
            //}
            //string strSql = string.Format(@"
            //SELECT	SPC_RetailPromotionTable.PromId,PROMNAME ,PROMNAMEONRECEIPT ,BLOCKED ,ItemBuyerGroupId,
            //  ItemBuyerGroupId + '-' + CASE WHEN ItemBuyerGroupId = '' THEN '' ELSE Description END AS Description ,AMOUNT ,QTY,
            //  CONVERT(VARCHAR,LIFEFROM,23) AS LIFEFROM ,CONVERT(VARCHAR,LIFETO,23) AS LIFETO ,BARCODEFREE ,NAMEONRECEIPT ,QTYFREE ,QTYHIGHEST,BLOCKED  
            //FROM	SPC_RetailPromotionTable with (Nolock) 
            //  INNER JOIN SPC_RetailPromotionLine with (Nolock) ON SPC_RetailPromotionTable.PromId = SPC_RetailPromotionLine.PromId 
            //  INNER JOIN SPC_RetailPromotionLocation with (Nolock) ON SPC_RetailPromotionTable.PromId = SPC_RetailPromotionLocation.PromId 
            //  INNER JOIN InventLocation WITH (NOLOCK) ON SPC_RetailPromotionLocation.INVENTLOCATIONID =  InventLocation.INVENTLOCATIONID 
            //  LEFT OUTER JOIN InventBuyerGroup WITH (NOLOCK) ON SPC_RetailPromotionTable.ItemBuyerGroupId = InventBuyerGroup.GROUP_  
            //            AND InventBuyerGroup.DATAAREAID = 'SPC' 
            //WHERE	 LIFETO >= GETDATE() AND SPC_RetailPromotionLocation.DATAAREAID = 'SPC' AND SPC_RetailPromotionLine.DATAAREAID = 'SPC'  
            //  AND SPC_RetailPromotionTable.DATAAREAID = 'SPC'  AND InventLocation.DATAAREAID = 'SPC'    
            //  AND  BLOCKED = '0'  AND	SPC_RetailPromotionLocation.INVENTLOCATIONID  != 'RETAILAREA' " + pCon + @"
            //GROUP BY SPC_RetailPromotionTable.PromId,PROMNAME ,PROMNAMEONRECEIPT ,BLOCKED ,ItemBuyerGroupId,
            //  CASE WHEN ItemBuyerGroupId = '' THEN '' ELSE Description END   ,
            //  AMOUNT ,QTY,LIFEFROM ,LIFETO ,BARCODEFREE ,NAMEONRECEIPT ,QTYFREE ,QTYHIGHEST   
            //ORDER BY LIFEFROM  
            //");
            // 
            dtData = PosSaleClass.Promotion_DataHD(); //ConnectionClass.SelectSQL_MainAX(strSql);
            RadGridView_ShowHD.DataSource = dtData;
            dtData.AcceptChanges();

            if (dtData.Rows.Count > 0)
            {
                RadGridView_ShowHD.Rows[0].Cells[0].IsSelected = true;
                SetBarcode(
                        RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString(),
                        RadGridView_ShowHD.CurrentRow.Cells["BARCODEFREE"].Value.ToString());
                pPromIdSelect = RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString();
            }
            this.Cursor = Cursors.Default;
        }
        //Check Barcode + Stk Branch
        void SetBarcode(string pProID, string pBarcode)
        {
            this.Cursor = Cursors.WaitCursor;
            //string sql1 = string.Format(@"
            //    SELECT	SPC_RetailPromotionLocation.INVENTLOCATIONID,NAME ,USECONFIRM 
            //    FROM	SPC_RetailPromotionLocation with (Nolock) 
            //      INNER JOIN InventLocation WITH (NOLOCK) ON SPC_RetailPromotionLocation.INVENTLOCATIONID =  InventLocation.INVENTLOCATIONID  
            //    WHERE	PromId = '" + pProID + @"' 
            //    ORDER BY INVENTLOCATIONID
            //");

            //string sql2 = string.Format(@"
            //    SELECT	LINENUM ,SPC_RetailPromotionLine.ItemId,SPC_RetailPromotionLine.INVENTDIMID ,INVENTITEMBARCODE.SPC_ITEMNAME ,SPC_RetailPromotionLine.UNITID 
            //    FROM	SPC_RetailPromotionLine with (Nolock)	 
            //      INNER JOIN INVENTITEMBARCODE WITH (NOLOCK) ON  SPC_RetailPromotionLine.ItemId = INVENTITEMBARCODE.ITEMID 
            //      AND SPC_RetailPromotionLine.INVENTDIMID = INVENTITEMBARCODE.INVENTDIMID 
            //      AND SPC_RetailPromotionLine.UNITID = INVENTITEMBARCODE.UNITID  
            //    WHERE	PromId = '" + pProID + @"' AND SPC_RetailPromotionLine.DATAAREAID = 'SPC' AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
            //    UNION 
            //    SELECT	LINENUM ,SPC_RetailPromotionLine.ItemId,INVENTDIMID AS INVENTDIMID ,ITEMNAME ,SPC_RetailPromotionLine.UNITID  
            //    FROM	SPC_RetailPromotionLine with (Nolock)	 
            //      INNER JOIN INVENTTABLE  WITH (NOLOCK) ON  SPC_RetailPromotionLine.ItemId = INVENTTABLE.ITEMID 
            //    WHERE	PromId = '" + pProID + @"' AND SPC_RetailPromotionLine.DATAAREAID = 'SPC' AND INVENTTABLE.DATAAREAID = 'SPC' 
            //    ORDER BY LINENUM
            //");

            // AND INVENTDIMID = 'AllBlank' 
            //FindStock_ByBarcode_AllBranch
            DataTable dtMN = PosSaleClass.Promotion_DataDT("0", pProID); //ConnectionClass.SelectSQL_MainAX(sql1);
            dtBarcode = PosSaleClass.Promotion_DataDT("1", pProID);// ConnectionClass.SelectSQL_MainAX(sql2);
            DataTable dtSTK = ItembarcodeClass.FindStock_ByBarcode_AllBranch(pBarcode);

            if (dtBchSTK.Rows.Count > 0) { dtBchSTK.Rows.Clear(); }

            for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
            {
                DataRow[] rStock = dtSTK.Select(" INVENTLOCATIONID = '" + dtBch.Rows[iBch]["BRANCH_ID"].ToString() + @"' ");
                DataRow[] rSale = dtMN.Select(" INVENTLOCATIONID = '" + dtBch.Rows[iBch]["BRANCH_ID"].ToString() + @"' ");

                string q = "0", sta = "0";
                if (rSale.Length > 0)
                {
                    q = rSale[0]["USECONFIRM"].ToString();
                    sta = "1";
                }
                double stk = 0;
                if (rStock.Length > 0)
                {
                    stk = double.Parse(rStock[0]["AVAILPHYSICAL"].ToString());
                }

                dtBchSTK.Rows.Add(
                    stk.ToString("#,#0.00"),
                    dtBch.Rows[iBch]["BRANCH_ID"].ToString(),
                    dtBch.Rows[iBch]["BRANCH_NAME"].ToString(),
                    q,
                    sta
                );
            }

            RadGridView_ShowBch.DataSource = dtBchSTK;
            dtBchSTK.AcceptChanges();

            RadGridView_ShowDT.DataSource = dtBarcode;
            dtBarcode.AcceptChanges();

            this.Cursor = Cursors.Default;
        }



        //Clear
        void ClearTxt()
        {
            pPromIdSelect = "";
            if (dtBchSTK.Rows.Count > 0) { dtBchSTK.Rows.Clear(); }
            if (dtBarcode.Rows.Count > 0) { dtBchSTK.Rows.Clear(); }
            if (dtData.Rows.Count > 0) { dtBchSTK.Rows.Clear(); }
            if (dtBch.Rows.Count > 0) { dtBchSTK.Rows.Clear(); }

        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString() == pPromIdSelect) return;

            SetBarcode(RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString(),
                RadGridView_ShowHD.CurrentRow.Cells["BARCODEFREE"].Value.ToString());
            pPromIdSelect = RadGridView_ShowHD.CurrentRow.Cells["BARCODEFREE"].Value.ToString();
        }
        //เปิด-ปิดโปร
        private void RadGridView_ShowBch_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (RadGridView_ShowBch.Rows.Count == 0) return;
            if (RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value.ToString() == "") return;
            if (_pPermission == "0") return;

            //if (CheckEXTERNALLIST708(RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString(), RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value.ToString()) > 0)
            if (AX_SendData.EXTERNALLIST_Check("SPC_RetailPromotionLocation", $@"{RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value}|{RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value}") > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("โปรโมชั่นของสาขากำลังดำเนินการอยู่ ไม่สามารถแก้ไขได้" + Environment.NewLine +
                    "รอให้ระบบดำเนินการเรียบร้อยแล้ว จึงจะแก้ไขใหม่ได้อีกครั้ง");
                return;
            }

            string sta = RadGridView_ShowBch.CurrentRow.Cells["STA"].Value.ToString();
            if (sta == "0")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการเปิดโปรโมชั่น " + Environment.NewLine +
                    RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString() +
                    RadGridView_ShowHD.CurrentRow.Cells["PROMNAMEONRECEIPT"].Value.ToString() + Environment.NewLine +
                    "สาขา " + RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + "-" +
                    RadGridView_ShowBch.CurrentRow.Cells["BRANCH_Name"].Value.ToString()) == DialogResult.No)
                {
                    return;
                }

                OpenPromotion(RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString(),
                    RadGridView_ShowHD.CurrentRow.Cells["PROMNAMEONRECEIPT"].Value.ToString(),
                    RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                    RadGridView_ShowBch.CurrentRow.Cells["BRANCH_Name"].Value.ToString());
            }
            else
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการปิดโปรโมชั่น " +
                   RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString() +
                   RadGridView_ShowHD.CurrentRow.Cells["PROMNAMEONRECEIPT"].Value.ToString() + Environment.NewLine +
                   "สาขา " + RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + "-" +
                   RadGridView_ShowBch.CurrentRow.Cells["BRANCH_Name"].Value.ToString()) == DialogResult.No)
                {
                    return;
                }

                ClosePromotion(RadGridView_ShowHD.CurrentRow.Cells["PromId"].Value.ToString(),
                                   RadGridView_ShowHD.CurrentRow.Cells["PROMNAMEONRECEIPT"].Value.ToString(),
                                   RadGridView_ShowBch.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                                   RadGridView_ShowBch.CurrentRow.Cells["BRANCH_Name"].Value.ToString());
            }

        }
        //InSert ExternalList AX
        string SaveInsert(string pProID, string pProName, string pBch, string pBchName, string pRmk, string pStaAX)
        {
            //string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "");
            ArrayList sql_InsertAX = new ArrayList
            {
                //string.Format(@"
                //    INSERT INTO SPC_EXTERNALLIST 
                //    (TABLENAME,PKFIELDNAME,PKFIELDVALUE,FIELDNAME,FIELDVALUE,METHODNAME,DATAAREAID,RECVERSION,RECID) 
                //    VALUES (
                //    'SPC_RetailPromotionLocation','PROMID|INVENTLOCATIONID','" + pProID + @"|" + pBch + @"','PROMID|INVENTLOCATIONID|USECONFIRM',
                //    '" + pProID + @"|" + pBch + @"|1',CONVERT(NVARCHAR,'" + pStaAX + @"'),'SPC','1',
                //    CONVERT(BIGINT," + recID + @") )
                //")

                AX_SendData.Save_EXTERNALLIST_SPC_RetailPromotionLocation(pStaAX,pProID,pBch)
            };

            ArrayList sql_Insert24 = new ArrayList
            {
                string.Format(@"
                      INSERT INTO SHOP_RETAILPROMOTIONHISTORY(PROMID, PROMNAMEONRECEIPT, INVENTLOCATIONID, NAME,  WHOINS, WHONAMEINS, REMARK) VALUES (  
                        '" + pProID + @"','" + pProName + @"','" + pBch + @"','" + pBchName + @"',
                        '" + SystemClass.SystemUserID + @"', '" + SystemClass.SystemUserName + @"', '" + pRmk + @"') ")
            };

            return ConnectionClass.ExecuteMain_AX_24_SameTime(sql_Insert24, sql_InsertAX);
        }
        ////Check ExternalList
        //int CheckEXTERNALLIST708(string pProID, string pBch)
        //{
        //    string sql_CheckLog708 = string.Format(@"
        //    select * from SPC_EXTERNALLIST WITH (NOLOCK)
        //    where	 TABLENAME = 'SPC_RetailPromotionLocation'  AND CONVERT(NVARCHAR,PKFIELDVALUE) = N'" + pProID + "|" + pBch + @"' AND LOG = '' 
        //    ");
        //    return ConnectionClass.SelectSQL_MainAX(sql_CheckLog708).Rows.Count;
        //}
        //open
        void OpenPromotion(string pProID, string pProName, string pBch, string pBchName)
        {
            string rmk;
            FormShare.ShowRemark _showRemark = new FormShare.ShowRemark("1")
            {
                pDesc = "หมายเหตุ : เปิดโปรโมชั่น [บังคับ]"
            };
            if (_showRemark.ShowDialog(this) == DialogResult.Yes)
            {
                rmk = "Open - " + _showRemark.pRmk;
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับการเปิดโปรโมชั่น ต้องระบุเหตุผลก่อนบันทึกทุกครั้ง");
                return;
            }


            string T = SaveInsert(pProID, pProName, pBch, pBchName, rmk, "CREATE");
            if (T == "")
            {
                RadGridView_ShowBch.CurrentRow.Cells["USECONFIRM"].Value = "1";
                RadGridView_ShowBch.CurrentRow.Cells["STA"].Value = "1";
            }
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //close
        void ClosePromotion(string pProID, string pProName, string pBch, string pBchName)
        {

            string rmk;
            FormShare.ShowRemark _showRemark = new FormShare.ShowRemark("1")
            {
                pDesc = "หมายเหตุ : ปิดโปรโมชั่น [บังคับ]"
            };
            if (_showRemark.ShowDialog(this) == DialogResult.Yes)
            {
                rmk = "Close - " + _showRemark.pRmk;
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับการปิดโปรโมชั่น ต้องระบุเหตุผลก่อนบันทึกทุกครั้ง");
                return;
            }


            string T = SaveInsert(pProID, pProName, pBch, pBchName, rmk, "DELETE");
            if (T == "")
            {
                RadGridView_ShowBch.CurrentRow.Cells["USECONFIRM"].Value = "0";
                RadGridView_ShowBch.CurrentRow.Cells["STA"].Value = "0";
            }
            MsgBoxClass.MsgBoxShow_SaveStatus(T);

        }

        #region "ROW-FONT"
        private void RadGridView_ShowHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowBch_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowBch_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_ShowHD_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_ShowHD_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowBch_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowHD_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowBch_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}
