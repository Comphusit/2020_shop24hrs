﻿using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Advertise
{
    public partial class Adverstise : Telerik.WinControls.UI.RadForm
    {
        
        //Load
        public Adverstise()
        {
            InitializeComponent();
        }
        //Load
        private void Adverstise_Load(object sender, EventArgs e)
        {

         
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            ConditionalFormattingForm form = (ConditionalFormattingForm)sender;
            DatagridClass.SetFormFont(form.Controls);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            BaseFilterPopup popup = (BaseFilterPopup)e.FilterPopup;
            DatagridClass.SetMenuFont(popup.Items);
            popup.Font = SystemClass.SetFontGernaral;
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            if (e.FilterPopup is RadListFilterPopup popup)
            {
                popup.MenuTreeElement.TreeView.NodeFormatting -= DatagridClass.TreeView_NodeFormatting;
                popup.MenuTreeElement.TreeView.NodeFormatting += DatagridClass.TreeView_NodeFormatting;
            }
        }
        #endregion
    }
}
