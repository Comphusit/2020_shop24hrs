﻿namespace PC_Shop24Hrs.GeneralForm.Advertise
{
    partial class Adverstise_Manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Adverstise_Manage));
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edite = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Img = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Tranf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.RadGridView_Detail = new Telerik.WinControls.UI.RadGridView();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radBrowseEditor_File = new Telerik.WinControls.UI.RadBrowseEditor();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.radCheckedListBox_Branch = new Telerik.WinControls.UI.RadCheckedListBox();
            this.radButton_CheckAll = new Telerik.WinControls.UI.RadButton();
            this.radToggleSwitch_ShowMedai = new Telerik.WinControls.UI.RadToggleSwitch();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_FieSize = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Success = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radDateTimePicker_DatetimeBegine = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_DatetimeEnd = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Status = new Telerik.WinControls.UI.RadDropDownList();
            this.radOpenFileDialog1 = new Telerik.WinControls.UI.RadOpenFileDialog();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Detail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBrowseEditor_File)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedListBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CheckAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_ShowMedai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_FieSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Success)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DatetimeBegine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DatetimeEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Status)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_Add,
            this.commandBarSeparator2,
            this.radButtonElement_Edite,
            this.commandBarSeparator1,
            this.radButtonElement_Img,
            this.commandBarSeparator4,
            this.radButtonElement_Tranf,
            this.commandBarSeparator5,
            this.radButtonElement_pdf});
            this.radStatusStrip.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip.Name = "radStatusStrip";
            this.radStatusStrip.Size = new System.Drawing.Size(288, 34);
            this.radStatusStrip.TabIndex = 0;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.UseCompatibleTextRendering = false;
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edite
            // 
            this.radButtonElement_Edite.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edite.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edite.Name = "radButtonElement_Edite";
            this.radStatusStrip.SetSpring(this.radButtonElement_Edite, false);
            this.radButtonElement_Edite.Text = "radButtonElement1";
            this.radButtonElement_Edite.Click += new System.EventHandler(this.RadButtonElement_Edite_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Img
            // 
            this.radButtonElement_Img.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Img.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radButtonElement_Img.Name = "radButtonElement_Img";
            this.radStatusStrip.SetSpring(this.radButtonElement_Img, false);
            this.radButtonElement_Img.Text = "radButtonElement1";
            this.radButtonElement_Img.Click += new System.EventHandler(this.RadButtonElement_Img_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Tranf
            // 
            this.radButtonElement_Tranf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Tranf.Image = global::PC_Shop24Hrs.Properties.Resources.link;
            this.radButtonElement_Tranf.Name = "radButtonElement_Tranf";
            this.radStatusStrip.SetSpring(this.radButtonElement_Tranf, false);
            this.radButtonElement_Tranf.Text = "radButtonElement1";
            this.radButtonElement_Tranf.Click += new System.EventHandler(this.RadButtonElement_Tranf_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_pdf
            // 
            this.radButtonElement_pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_pdf.Name = "radButtonElement_pdf";
            this.radStatusStrip.SetSpring(this.radButtonElement_pdf, false);
            this.radButtonElement_pdf.Text = "radButtonElement1";
            this.radButtonElement_pdf.Click += new System.EventHandler(this.RadButtonElement_Pdf_Click);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.RadGridView_Detail);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox2.HeaderText = "รายละเอียดตามสาขา";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(574, 309);
            this.radGroupBox2.TabIndex = 56;
            this.radGroupBox2.Text = "รายละเอียดตามสาขา";
            // 
            // RadGridView_Detail
            // 
            this.RadGridView_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Detail.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Detail.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.RadGridView_Detail.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Detail.Name = "RadGridView_Detail";
            // 
            // 
            // 
            this.RadGridView_Detail.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Detail.Size = new System.Drawing.Size(570, 289);
            this.RadGridView_Detail.TabIndex = 0;
            this.RadGridView_Detail.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Detail.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_Detail_CellBeginEdit);
            this.RadGridView_Detail.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Detail_CellEditorInitialized);
            this.RadGridView_Detail.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Detail_CellEndEdit);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.RadGridView_Show);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "ไฟล์ทั้งหมด";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(574, 309);
            this.radGroupBox1.TabIndex = 55;
            this.radGroupBox1.Text = "ไฟล์ทั้งหมด";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(570, 289);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.RadGridView_Show_CurrentRowChanged);
            this.RadGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(574, 622);
            this.radSplitContainer1.TabIndex = 52;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radGroupBox1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(574, 309);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2160919F, 0F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(188, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radGroupBox2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 313);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(574, 309);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.216092F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(-188, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.radPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(577, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(294, 616);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radBrowseEditor_File);
            this.radPanel1.Controls.Add(this.radSplitContainer2);
            this.radPanel1.Controls.Add(this.radToggleSwitch_ShowMedai);
            this.radPanel1.Controls.Add(this.radTextBox_Remark);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.radLabel_FieSize);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.radLabel_Success);
            this.radPanel1.Controls.Add(this.radButton_Save);
            this.radPanel1.Controls.Add(this.radButton_Cancel);
            this.radPanel1.Controls.Add(this.radDateTimePicker_DatetimeBegine);
            this.radPanel1.Controls.Add(this.radDateTimePicker_DatetimeEnd);
            this.radPanel1.Controls.Add(this.radLabel_Date);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.radDropDownList_Status);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(3, 51);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(288, 562);
            this.radPanel1.TabIndex = 0;
            // 
            // radBrowseEditor_File
            // 
            this.radBrowseEditor_File.Location = new System.Drawing.Point(12, 13);
            this.radBrowseEditor_File.Name = "radBrowseEditor_File";
            this.radBrowseEditor_File.Size = new System.Drawing.Size(265, 20);
            this.radBrowseEditor_File.TabIndex = 0;
            this.radBrowseEditor_File.ValueChanging += new Telerik.WinControls.UI.ValueChangingEventHandler(this.RadBrowseEditor1_ValueChanging);
            this.radBrowseEditor_File.ValueChanged += new System.EventHandler(this.RadBrowseEditor1_ValueChanged);
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Location = new System.Drawing.Point(12, 79);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(265, 242);
            this.radSplitContainer2.TabIndex = 1;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.pictureBox);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(265, 0);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.5F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -129);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(265, 0);
            this.pictureBox.TabIndex = 95;
            this.pictureBox.TabStop = false;
            this.pictureBox.DoubleClick += new System.EventHandler(this.PictureBox_DoubleClick);
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.radCheckedListBox_Branch);
            this.splitPanel4.Controls.Add(this.radButton_CheckAll);
            this.splitPanel4.Location = new System.Drawing.Point(0, 4);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(265, 238);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.5F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 129);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // radCheckedListBox_Branch
            // 
            this.radCheckedListBox_Branch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radCheckedListBox_Branch.Location = new System.Drawing.Point(3, 37);
            this.radCheckedListBox_Branch.Name = "radCheckedListBox_Branch";
            this.radCheckedListBox_Branch.Size = new System.Drawing.Size(259, 198);
            this.radCheckedListBox_Branch.TabIndex = 1;
            // 
            // radButton_CheckAll
            // 
            this.radButton_CheckAll.Location = new System.Drawing.Point(3, 9);
            this.radButton_CheckAll.Name = "radButton_CheckAll";
            this.radButton_CheckAll.Size = new System.Drawing.Size(75, 22);
            this.radButton_CheckAll.TabIndex = 0;
            this.radButton_CheckAll.Text = "Check all";
            this.radButton_CheckAll.Click += new System.EventHandler(this.RadButton_CheckAll_Click);
            // 
            // radToggleSwitch_ShowMedai
            // 
            this.radToggleSwitch_ShowMedai.Location = new System.Drawing.Point(12, 47);
            this.radToggleSwitch_ShowMedai.Name = "radToggleSwitch_ShowMedai";
            this.radToggleSwitch_ShowMedai.Size = new System.Drawing.Size(64, 20);
            this.radToggleSwitch_ShowMedai.TabIndex = 102;
            this.radToggleSwitch_ShowMedai.ValueChanged += new System.EventHandler(this.RadToggleSwitch_ShowMedai_ValueChanged);
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radTextBox_Remark.AutoScroll = true;
            this.radTextBox_Remark.Location = new System.Drawing.Point(82, 443);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox_Remark.Size = new System.Drawing.Size(195, 58);
            this.radTextBox_Remark.TabIndex = 4;
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(12, 443);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 19);
            this.radLabel3.TabIndex = 100;
            this.radLabel3.Text = "คำอธิบาย";
            // 
            // radLabel_FieSize
            // 
            this.radLabel_FieSize.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_FieSize.Location = new System.Drawing.Point(95, 47);
            this.radLabel_FieSize.Name = "radLabel_FieSize";
            this.radLabel_FieSize.Size = new System.Drawing.Size(50, 19);
            this.radLabel_FieSize.TabIndex = 96;
            this.radLabel_FieSize.Text = "0 bytes";
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(12, 365);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(23, 19);
            this.radLabel1.TabIndex = 88;
            this.radLabel1.Text = "ถึง";
            // 
            // radLabel_Success
            // 
            this.radLabel_Success.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radLabel_Success.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radLabel_Success.Location = new System.Drawing.Point(168, 47);
            this.radLabel_Success.Name = "radLabel_Success";
            this.radLabel_Success.Size = new System.Drawing.Size(53, 19);
            this.radLabel_Success.TabIndex = 87;
            this.radLabel_Success.Text = "Sussess";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(56, 515);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(156, 515);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDateTimePicker_DatetimeBegine
            // 
            this.radDateTimePicker_DatetimeBegine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radDateTimePicker_DatetimeBegine.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_DatetimeBegine.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_DatetimeBegine.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_DatetimeBegine.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_DatetimeBegine.Location = new System.Drawing.Point(82, 332);
            this.radDateTimePicker_DatetimeBegine.Name = "radDateTimePicker_DatetimeBegine";
            this.radDateTimePicker_DatetimeBegine.Size = new System.Drawing.Size(195, 21);
            this.radDateTimePicker_DatetimeBegine.TabIndex = 1;
            this.radDateTimePicker_DatetimeBegine.TabStop = false;
            this.radDateTimePicker_DatetimeBegine.Text = "22/05/2020";
            this.radDateTimePicker_DatetimeBegine.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_DatetimeBegine.ValueChanged += new System.EventHandler(this.RadDateTimePicker_DatetimeBegine_ValueChanged);
            // 
            // radDateTimePicker_DatetimeEnd
            // 
            this.radDateTimePicker_DatetimeEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radDateTimePicker_DatetimeEnd.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_DatetimeEnd.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_DatetimeEnd.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_DatetimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_DatetimeEnd.Location = new System.Drawing.Point(82, 365);
            this.radDateTimePicker_DatetimeEnd.Name = "radDateTimePicker_DatetimeEnd";
            this.radDateTimePicker_DatetimeEnd.Size = new System.Drawing.Size(195, 21);
            this.radDateTimePicker_DatetimeEnd.TabIndex = 2;
            this.radDateTimePicker_DatetimeEnd.TabStop = false;
            this.radDateTimePicker_DatetimeEnd.Text = "22/05/2020";
            this.radDateTimePicker_DatetimeEnd.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_DatetimeEnd.ValueChanged += new System.EventHandler(this.RadDateTimePicker_DatetimeEnd_ValueChanged);
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(12, 332);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(27, 19);
            this.radLabel_Date.TabIndex = 70;
            this.radLabel_Date.Text = "เริ่ม";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(12, 404);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(47, 19);
            this.radLabel2.TabIndex = 80;
            this.radLabel2.Text = "สถานะ";
            // 
            // radDropDownList_Status
            // 
            this.radDropDownList_Status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radDropDownList_Status.DropDownAnimationEnabled = false;
            this.radDropDownList_Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Status.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Status.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Status.Location = new System.Drawing.Point(82, 404);
            this.radDropDownList_Status.Name = "radDropDownList_Status";
            this.radDropDownList_Status.Size = new System.Drawing.Size(195, 21);
            this.radDropDownList_Status.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.Controls.Add(this.toolStrip1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radSplitContainer1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Location = new System.Drawing.Point(0, 622);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(574, 20);
            this.toolStrip1.TabIndex = 53;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Adverstise_Manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "Adverstise_Manage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adverstise_Manage";
            this.Load += new System.EventHandler(this.Adverstise_Manage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Detail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radBrowseEditor_File)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedListBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_CheckAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radToggleSwitch_ShowMedai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_FieSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Success)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DatetimeBegine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DatetimeEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Status)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edite;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_pdf;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Status;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_DatetimeEnd;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_DatetimeBegine;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGridView RadGridView_Detail;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Success;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadOpenFileDialog radOpenFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox;
        private Telerik.WinControls.UI.RadLabel radLabel_FieSize;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadButton radButton_CheckAll;
        private Telerik.WinControls.UI.RadCheckedListBox radCheckedListBox_Branch;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadToggleSwitch radToggleSwitch_ShowMedai;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadBrowseEditor radBrowseEditor_File;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Img;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Tranf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private System.Windows.Forms.ToolStrip toolStrip1;
    }
}
