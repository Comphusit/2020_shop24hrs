﻿using System;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.FileDialogs;
using Telerik.Windows.Diagrams.Core;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.GeneralForm.Advertise
{
    public partial class Adverstise_Manage : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_file = new DataTable();
        DataTable dt_detail = new DataTable();
        DataTable dtBranch = new DataTable();
        readonly DataTable Branch = new DataTable();
        ushort sizeFile;
        string fileFullname;
        FileDataState UpdateSql;
        Flash flash;
        string pathImageFull;
        readonly string pathImage = @"\\192.168.100.77\ImageMinimark\AdvtFiles\";
        readonly string[] mediaExtensions = {
            ".AVI",".WMV",".MPEG",".MOV",".DrvX",".DAT",".FLV",".MP4" //etc
        };
        readonly string[] imageExtensions = {
            ".PNG", ".JPG", ".JPEG", ".BMP", ".GIF",".TIFF" //etc
        };
        enum FileDataState
        {
            Added,
            Edite
        }
        public enum AdvFolder {
            PHOTO,
            VDO
        }
        public Adverstise_Manage()
        {
            InitializeComponent();
        }
        //Load
        private void Adverstise_Manage_Load(object sender, EventArgs e)
        {
            //radStatusStrip
            radStatusStrip.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edite.ShowBorder = true;
            radButtonElement_Img.ShowBorder = true;
            radButtonElement_Tranf.ShowBorder = true;
            radButtonElement_pdf.ShowBorder = true;
            radButton_CheckAll.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultFontGroupBox(radGroupBox1);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox2);

            this.radCheckedListBox_Branch.Font = SystemClass.SetFontGernaral;
            ListViewDetailColumn nameColumn = new ListViewDetailColumn("Name")
            {
                Width = 150
            };
            this.radCheckedListBox_Branch.Columns.Add(nameColumn);

            this.radToggleSwitch_ShowMedai.Value = false;
            this.radLabel_Success.Visible = false;

            this.radSplitContainer2.EnableCollapsing = true;
            this.radSplitContainer2.UseSplitterButtons = true;
            this.radBrowseEditor_File.ReadOnly = false;
            this.radBrowseEditor_File.BrowseElement.ShowClearButton = true;

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-CA");
            System.Globalization.DateTimeFormatInfo dateTimeFormat = new System.Globalization.DateTimeFormatInfo() {
                AMDesignator = string.Empty,
                PMDesignator = string.Empty
            };
            culture.DateTimeFormat = dateTimeFormat;
            this.radDateTimePicker_DatetimeBegine.Culture = culture;
            this.radDateTimePicker_DatetimeBegine.Format = DateTimePickerFormat.Custom;
            this.radDateTimePicker_DatetimeBegine.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            (this.radDateTimePicker_DatetimeBegine.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).ShowTimePicker = true;
            (this.radDateTimePicker_DatetimeBegine.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMinSize = new System.Drawing.Size(330, 320);
            (this.radDateTimePicker_DatetimeBegine.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMaxSize = new System.Drawing.Size(330, 320);
            this.radDateTimePicker_DatetimeBegine.Value = DateTime.Now;

            this.radDateTimePicker_DatetimeEnd.Culture = culture;
            this.radDateTimePicker_DatetimeEnd.Format = DateTimePickerFormat.Custom;
            this.radDateTimePicker_DatetimeEnd.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            (this.radDateTimePicker_DatetimeEnd.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).ShowTimePicker = true;
            (this.radDateTimePicker_DatetimeEnd.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMinSize = new System.Drawing.Size(330, 320);
            (this.radDateTimePicker_DatetimeEnd.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMaxSize = new System.Drawing.Size(330, 320);
            this.radDateTimePicker_DatetimeEnd.Value = DateTime.Now;

            this.toolStrip1.BackColor = Color.FromArgb(173, 244, 215);
            this.toolStrip1.Font = new Font(new FontFamily("Tahoma"), 9.75f);
            ToolStripLabel toolTiplalel = new ToolStripLabel
            {
                Text = @"สีม่วง >> ไฟล์หมดอายุ | ดับเบิ้ลคลิกที่ FashId เพิ่อปรับสถานะไฟล์หมดอายุ."
            };
            toolStrip1.Items.Add(toolTiplalel);

            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            this.InitializeGrid();
            this.InitializeDataListBranch();
            this.SetDroupDownStatus();
            this.SetToolTipPanel();
            this.Getfileflash();
            //this.CheckFlashExpire(GetFlashExpire());
            this.Clear(true);
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            ConditionalFormattingForm form = (ConditionalFormattingForm)sender;
            DatagridClass.SetFormFont(form.Controls);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            BaseFilterPopup popup = (BaseFilterPopup)e.FilterPopup;
            DatagridClass.SetMenuFont(popup.Items);
            popup.Font = SystemClass.SetFontGernaral;
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            if (e.FilterPopup is RadListFilterPopup popup)
            {
                popup.MenuTreeElement.TreeView.NodeFormatting -= DatagridClass.TreeView_NodeFormatting;
                popup.MenuTreeElement.TreeView.NodeFormatting += DatagridClass.TreeView_NodeFormatting;
            }
        }
        #endregion

        #region Methode
        private GridViewDataColumn AppendNewColumn(string columnType, string labelName)
        {
            GridViewDataColumn newColumn = new GridViewDateTimeColumn() {
                FormatString = "{0:dd/MM/yyyy HH:mm:ss}",
                TextAlignment = ContentAlignment.MiddleRight,
                FieldName = "DateTime",
                Name = columnType,
                HeaderText = labelName,
                Width = 150
            };
            newColumn.FieldName = columnType;
            return newColumn;
        }
        private void SetToolTipPanel() {
            radButtonElement_Add.ToolTipText = "เพิ่มไฟล์";
            radButtonElement_Edite.ToolTipText = "แก้ไขข้อมูล";
            radButtonElement_Img.ToolTipText = "เช็คไฟล์หมดอายุ";
            radButtonElement_Tranf.ToolTipText = "โอนไฟล์ไปยังเครื่องเล่น";
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน";
        }
        private void Clear(bool enable) {
            this.radBrowseEditor_File.BrowseElement.Text = "";
            this.radLabel_FieSize.Text = "";
            this.radLabel_Success.Text = "";
            this.radToggleSwitch_ShowMedai.ReadOnly = enable;
            this.radToggleSwitch_ShowMedai.Value = false;
            this.radBrowseEditor_File.BrowseElement.BrowseButton.Enabled = !enable;
            this.radBrowseEditor_File.BrowseElement.ReadOnly = enable;
            this.radDateTimePicker_DatetimeBegine.ReadOnly = enable;
            this.radDateTimePicker_DatetimeEnd.ReadOnly = enable;
            this.radTextBox_Remark.ReadOnly = enable;
            this.radTextBox_Remark.Text = "";
            this.radDropDownList_Status.SelectedIndex = -1;
            this.radDropDownList_Status.ReadOnly = enable;
            this.radCheckedListBox_Branch.Enabled = !enable;
            this.radButton_CheckAll.Enabled = !enable;
            this.radDateTimePicker_DatetimeBegine.Value = DateTime.Now;
            this.radDateTimePicker_DatetimeEnd.Value = DateTime.Now;
            this.radCheckedListBox_Branch.UncheckAllItems();
            this.flash = new Flash();
            UpdateSql = FileDataState.Added;
        }
        private void InitializeGrid() {
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.TableElement.RowHeight = 35;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHID", "FLASHID", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROMOTIONID", "PROMOTIONID", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHNAME", "FLASHNAME", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHFILENAME", "FLASHFILENAME", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHDETAIL", "FLASHDETAIL", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHSIZE", "FLASHSIZE (KB)", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHREMARK", "คำอธิบาย", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHDEPT", "แผนก", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEBEGIN", "เริ่มเล่น", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEEND", "สิ้นสุด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STATUSID", "STATUSID"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHSTATUS", "สถานะ", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FLASHSHOW", "ตัวอย่าง", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("FLASHEXPIRE", "FLASHEXPIRE"));

            RadGridView_Show.Columns["FLASHID"].IsPinned = true;
            RadGridView_Show.Columns["PROMOTIONID"].IsPinned = true;
            RadGridView_Show.Columns["FLASHNAME"].IsPinned = true;
            RadGridView_Show.MasterTemplate.Columns.ForEach(radGridViewColmn => { dt_file.Columns.Add(radGridViewColmn.FieldName); });

            DatagridClass.SetDefaultRadGridView(RadGridView_Detail);
            RadGridView_Detail.EnableFiltering = false;
            //RadGridView_Detail.ReadOnly = false;
            RadGridView_Detail.AllowAddNewRow = false;
            RadGridView_Detail.TableElement.RowHeight = 35;
            RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("CHOOSE", "ใช้งาน", 70));
            RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
            RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 200));
            //RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMEBEGIN", "เริ่มเล่น", 150));
            //RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMEEND", "สิ้นสุด", 150));
            RadGridView_Detail.MasterTemplate.Columns.Add(AppendNewColumn("DATETIMEBEGIN", "เริ่มเล่น"));
            RadGridView_Detail.MasterTemplate.Columns.Add(AppendNewColumn("DATETIMEEND", "สิ้นสุด"));
            RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STATUSFILEID", "STATUSFILEID"));
            RadGridView_Detail.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("STATUSFILE", "สถานะ", 70));
            RadGridView_Detail.Columns["CHOOSE"].IsPinned = true;
            RadGridView_Detail.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_Detail.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_Detail.MasterTemplate.Columns.ForEach(radGridViewColmn => { dt_detail.Columns.Add(radGridViewColmn.FieldName); });
            this.GetGridViewSummary("CHOOSE", GridAggregateFunction.Sum);
        }
        private void InitializeDataListBranch()
        {
            this.radCheckedListBox_Branch.DataSource = this.CreateProducts();
            this.radCheckedListBox_Branch.DisplayMember = "Name";
            this.radCheckedListBox_Branch.ValueMember = "Id";
            this.radCheckedListBox_Branch.CheckedMember = "CheckState";
        }
        private void SetDroupDownStatus()
        {
            radDropDownList_Status.DropDownListElement.ListElement.Font = SystemClass.SetFontGernaral;
            radDropDownList_Status.DropDownListElement.Font = SystemClass.SetFontGernaral;
            radDropDownList_Status.DropDownListElement.TextBox.ForeColor = Color.Blue;
            radDropDownList_Status.DropDownListElement.TextBox.Font = SystemClass.SetFontGernaral;

            DataTable dtstatus = new DataTable();
            dtstatus.Columns.Add("ID");
            dtstatus.Columns.Add("VALUE");
            dtstatus.Rows.Add("0", "ไม่ใช้งาน");
            dtstatus.Rows.Add("1", "ใช้งาน");
            dtstatus.Rows.Add("2", "ใช้งานด่วน");

            radDropDownList_Status.DataSource = dtstatus;
            radDropDownList_Status.DisplayMember = "VALUE";
            radDropDownList_Status.ValueMember = "ID";
            radDropDownList_Status.SelectedIndex = 1;
        }
        private IEnumerable CreateProducts()
        {
            List<Branch> branch = new List<Branch>();
            foreach (DataRow row in dtBranch.Rows)
            {
                Branch tmp = new Branch() { Id = row["BRANCH_ID"].ToString(), Name = " " + row["BRANCH_ID"].ToString() + "-" + row["BRANCH_NAME"].ToString(), CheckState = CheckState.Unchecked };
                branch.Add(tmp);
            }
            return branch;
        }
        private string GetFilename(string _flashid, DateTime _stastdate, DateTime _enddate)
        {
            return $"{_flashid},{_stastdate.ToString("dd-MM-yyyy")}_{_enddate.ToString("dd-MM-yyyy")}";
        }
        private string GetMediaFile()
        {
            if (this.IsMediaFile(radBrowseEditor_File.BrowseElement.Text) == true)
            {
                return "VDO";
            }
            else
            {
                if (this.IsImageFile(radBrowseEditor_File.BrowseElement.Text) == true)
                {
                    return "IMG";
                }
                else
                {
                    return "";
                }
            }
        }
        bool IsMediaFile(string path)
        {
            return -1 != Array.IndexOf(mediaExtensions, Path.GetExtension(path).ToUpperInvariant());
        }
        bool IsImageFile(string path)
        {
            return -1 != Array.IndexOf(imageExtensions, Path.GetExtension(path).ToUpperInvariant());
        }
        private void FitLayoutPanel(bool value)
        {
            SplitPanel panelPix = splitPanel3;
            if (value == false)
            {
                panelPix.SizeInfo.AbsoluteSize = new System.Drawing.Size(0, 0);
            }
            else
            {
                panelPix.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Absolute;
                panelPix.SizeInfo.AbsoluteSize = new System.Drawing.Size(150, 150);
            }
        }
        private void UpdatePanelInfo(GridViewRowInfo currentRow)
        {
            if (currentRow != null && !(currentRow is GridViewNewRowInfo))
            {
                this.radDateTimePicker_DatetimeBegine.ReadOnly = false;
                this.radDateTimePicker_DatetimeEnd.ReadOnly = false;
                this.radTextBox_Remark.ReadOnly = false;
                this.radDropDownList_Status.ReadOnly = false;
                this.radCheckedListBox_Branch.Enabled = true;
                this.radButton_CheckAll.Enabled = true;
                this.radToggleSwitch_ShowMedai.ReadOnly = false;

                this.radBrowseEditor_File.BrowseElement.Text = this.GetSafeString(currentRow.Cells["FLASHFILENAME"].Value);
                this.radDateTimePicker_DatetimeBegine.Value = Convert.ToDateTime(this.GetSafeString(currentRow.Cells["DATEBEGIN"].Value));
                this.radDateTimePicker_DatetimeEnd.Value = Convert.ToDateTime(this.GetSafeString(currentRow.Cells["DATEEND"].Value));
                this.radDropDownList_Status.SelectedValue = this.GetSafeString(currentRow.Cells["STATUSID"].Value);
                this.radTextBox_Remark.Text = this.GetSafeString(currentRow.Cells["FLASHREMARK"].Value);

                foreach (var row in dt_detail.Select("CHOOSE = '1'"))
                {
                    ((List<Branch>)radCheckedListBox_Branch.DataSource).Where(l => l.Id == row["BRANCH_ID"].ToString()).ForEach(c => c.CheckState = CheckState.Checked);
                }

                try
                {
                    if (this.GetSafeString(currentRow.Cells["FLASHDETAIL"].Value).Equals("IMG"))
                    {
                        pathImageFull = pathImage + AdvFolder.PHOTO.ToString();
                    }
                    else {
                        pathImageFull = pathImage + AdvFolder.VDO.ToString();
                    }

                    fileFullname = pathImageFull + @"\" + this.radBrowseEditor_File.BrowseElement.Text;
                    Image image = Image.FromFile(fileFullname);
                    this.pictureBox.Image = image;
                    this.pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                catch (Exception)
                {
                    this.pictureBox.Image = new Bitmap(10, 10);
                }
            }
            else
            {
                this.Clear(true);
            }
        }
        private string GetSafeString(object value)
        {
            if (value == null) { return string.Empty; }
            return value.ToString();
        }
        private string GetPath(string flashfilename, string type)
        {
            //AdvFolder advType = (AdvFolder)Enum.Parse(typeof(AdvFolder), type);  //
            if (type.Equals("VDO")) {
                return $"{pathImage}{AdvFolder.VDO}\\{flashfilename}";
            }
            else {
                return $"{pathImage}{AdvFolder.PHOTO}\\{flashfilename}";
            }
        }
        string[] GedBranchPlay(string flashid)
        {
            string sql = string.Format(@"
                SELECT  [BRANCH_ID]
                FROM    [dbo].[SHOP_ADVERTISEBRANCH] WITH(NOLOCK)
                WHERE   [FLASHID] = '{0}'
                ", flashid);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            List<string> list = new List<string>();
            foreach (DataRow row in dt.Rows) {
                list.Add(row["BRANCH_ID"].ToString());
            }
            return list.ToArray();
        }
        List<Advertisopen> Advertisopen()
        {
            string sql = string.Format(@"
                    SELECT [BRANCH_ID]
                          ,[COM_NAME]
                          ,[VERSION]
                          ,[OPENDATE]
                          ,[OPENTIME]
                          ,[CLOSEDATE]
                          ,[CLOSETIME]
                      FROM [dbo].[SHOP_ADVERTISEOPEN]");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            var Tmp = new List<Advertisopen>();
            foreach (DataRow row in dt.Rows) {
                Advertisopen adv = new Advertisopen() {
                    Branch = row["BRANCH_ID"].ToString(),
                    Comname = row["COM_NAME"].ToString()
                };
                Tmp.Add(adv);
            }
            return Tmp;
        }
        void TranferfileToComname(string[] branch, List<Advertisopen> advlist, Flash flash_current)
        {
            this.Cursor = Cursors.WaitCursor;
            AdvFolder folder = flash_current.FlashType.Equals("VDO") ? AdvFolder.VDO : AdvFolder.PHOTO;
            string pathsource = pathImage + folder + @"\" + flash_current.FlashName;
            foreach (var br in branch)
            {
                foreach (var list in advlist.Where(a => a.Branch == br))
                {
                    try
                    {
                        System.Net.NetworkInformation.Ping myPing = new System.Net.NetworkInformation.Ping();
                        System.Net.NetworkInformation.PingReply reply = myPing.Send(list.Comname, 1000);

                        if (reply.Status.ToString().Equals("Success"))
                        {
                            //MessageBox.Show(reply.Status.ToString() + "\n"+ list.Comname + "\n" + br);
                            string pathdestination = @"\\" + list.Comname + @"\c$\AdvtFiles\" + folder + @"\" + flash_current.FlashName;
                            FileInfo fi = new FileInfo(pathdestination);
                            if (!fi.Exists)
                            {
                                //copy
                                File.Copy(pathsource, pathdestination);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                }
            }
            this.Cursor = Cursors.Default;
        }
        private List<Flash> GetFlashExpire()
        {
            DataRow[] dt_Expire = dt_file.Select("FLASHEXPIRE = '1' AND STATUSID = '1'");
            var Exps = new List<Flash>();
            foreach (DataRow row in dt_Expire)
            {
                Flash fl = new Flash()
                {
                    FlashId = row["FLASHID"].ToString(),
                    FlashName = row["FLASHFILENAME"].ToString(),
                    FlashType = row["FLASHDETAIL"].ToString()
                };
                Exps.Add(fl);
            }
            return Exps;
        }
        private void CheckFlashExpire(List<Flash> flashExps) 
        {
            if (flashExps.Count > 0)
            {
                if (MessageBox.Show("มีไฟล์หมดอายุการใช้งาน คุณต้องการลบทั้งหมดหรือไม่?.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Cursor = Cursors.WaitCursor;
                    foreach (var exp in flashExps) {
                        try
                        {
                            string pathFile_new = pathImage + @"BACKUP\\" + exp.FlashName;
                            File.Move(GetPath(exp.FlashName, exp.FlashType), pathFile_new);
                            //UpdateStatus
                            string ret = UpdateStatusMedia(exp.FlashId);
                            if (string.IsNullOrEmpty(ret))
                            {
                                //MsgBoxClass.MsgBoxShowButtonOk_Imformation("ลบรูปเรียบร้อย");
                                //this.GetData(GetLoadFileExp());
                                //return;
                            }
                            else
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error(ret);
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                        catch (Exception)
                        {
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                    this.Cursor = Cursors.Default;
                }
            }
        }
        private string UpdateStatusMedia(string flashid)
        {
            ArrayList arrayList = new ArrayList();
            string sqlUpd_Adv = string.Format(@"
                    UPDATE dbo.SHOP_ADVERTISE 
                    SET     [FALSHSTATUS] = '0'
                            ,[DATEUP] = CONVERT(VARCHAR, GETDATE(), 23)
                            ,[TIMEUP] = CONVERT(VARCHAR, GETDATE(), 108)
                            ,[WHOUP] = '{0}'
                    WHERE FLASHID = '{1}'"
                    , SystemClass.SystemUserID_M
                    , flashid);
            arrayList.Add(sqlUpd_Adv);
            string SqlUpd_AdvBranch = string.Format(@"
                    UPDATE dbo.SHOP_ADVERTISEBRANCH 
                    SET     [STATUSFILE]= '0'
                            ,[DATEUP]= CONVERT(VARCHAR, GETDATE(), 23)
                            ,[TIMEUP]= CONVERT(VARCHAR, GETDATE(), 108)
                            ,[WHOUP]= '{0}'
                    WHERE FLASHID = '{1}'"
                , SystemClass.SystemUserID_M
                , flashid);
            arrayList.Add(SqlUpd_AdvBranch);
            return ConnectionClass.ExecuteSQL_ArrayMain(arrayList);
        }
        #endregion

        #region SQL
        private void Getfileflash()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_file.Rows.Count > 0) dt_file.Rows.Clear();
            string sql = string.Format(@"
                    SELECT  [FLASHID],[PROMOTIONID],[FLASHNAME],[FLASHFILENAME],[FLASHDETAIL],[FLASHSIZE],[FLASHREMARK],[FLASHDEPT]
                            ,convert(datetime,(ISNULL(convert(varchar, DATEBEGIN, 23)+ ' ' +convert(varchar, TIMEBEGIN, 108),''))) AS DATEBEGIN
		                    ,convert(datetime,(ISNULL(convert(varchar, DATEEND, 23)+ ' ' +convert(varchar, TIMEEND, 108),''))) AS DATEEND
                            ,FALSHSTATUS AS STATUSID
                            ,CASE WHEN FALSHSTATUS = '1' THEN 'ใช้งาน' WHEN FALSHSTATUS = '2' THEN 'ใช้ด่วน' ELSE 'ไม่ใช้งาน' END AS FLASHSTATUS
                            ,CASE WHEN convert(datetime,(ISNULL(convert(varchar, DATEEND, 23)+ ' ' +convert(varchar, TIMEEND, 108),''))) < GETDATE() THEN '1' ELSE '0' END AS FLASHEXPIRE
                    FROM    SHOP_ADVERTISE WITH(NOLOCK)
                    WHERE   FALSHSTATUS in ('0','1','2') AND [FLASHNAME] != N'test'   --AND convert(datetime,(ISNULL(convert(varchar, DATEEND, 23)+ ' ' +convert(varchar, TIMEEND, 108),''))) >= convert(date,GETDATE())
                    ORDER BY [FLASHID]
                    ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            foreach (DataRow row in dt.Rows)
            {
                DataRow newRow = dt_file.NewRow();
                newRow["FLASHID"] = row["FLASHID"].ToString();
                newRow["PROMOTIONID"] = row["PROMOTIONID"].ToString();
                newRow["FLASHNAME"] = row["FLASHNAME"].ToString();
                newRow["FLASHFILENAME"] = row["FLASHFILENAME"].ToString();
                newRow["FLASHDETAIL"] = row["FLASHDETAIL"].ToString();
                newRow["FLASHREMARK"] = row["FLASHREMARK"].ToString();
                newRow["FLASHSIZE"] = row["FLASHSIZE"].ToString();
                newRow["FLASHDEPT"] = row["FLASHDEPT"].ToString();
                newRow["DATEBEGIN"] = row["DATEBEGIN"].ToString();
                newRow["DATEEND"] = row["DATEEND"].ToString();
                newRow["STATUSID"] = row["STATUSID"].ToString();
                newRow["FLASHSTATUS"] = row["FLASHSTATUS"].ToString();
                newRow["FLASHSHOW"] = "Click";
                newRow["FLASHEXPIRE"] = row["FLASHEXPIRE"].ToString();
                dt_file.Rows.Add(newRow);
            }

            RadGridView_Show.DataSource = dt_file;
            this.Cursor = Cursors.Default;

            //กำหนดเงื่อนไขเพื่อกำหนดสี cell
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("STATUSID", " STATUSID = '1' AND FLASHEXPIRE = '1'", true)
            {
                CellBackColor = ConfigClass.SetColor_PurplePastel()
            };
            this.RadGridView_Show.Columns["FLASHID"].ConditionalFormattingObjectList.Add(obj1);
        }
        private DataTable Getfiledetail(string fileid)
        {
            string sql = string.Format(@"
                 SELECT   CASE WHEN FLASHID IS NULL THEN '0' ELSE '1' END AS [CHOOSE],SHOP_BRANCH.BRANCH_ID,BRANCH_NAME
	                      ,CASE WHEN FLASHID IS NULL THEN 'Unknown'ELSE FLASHID END AS FLASHID
						  ,CASE WHEN STATUSFILE IS NULL THEN '0' ELSE STATUSFILE END AS STATUSFILEID
						  ,CASE WHEN STATUSFILE = '1' THEN 'ใช้งาน' WHEN STATUSFILE = '2' THEN 'ใช้งานด่วน' ELSE 'ไม่ใช้งาน' END AS STATUSFILE
                          ,ISNULL(TMP.PROMOTIONID,'') AS  PROMOTIONID
                          ,ISNULL(TMP.DATEBIGIN,'')  AS DATEBEGIN
                          ,ISNULL(TMP.DATEEND,'') AS DATEEND
						  ,convert(datetime,(ISNULL(convert(varchar, TMP.DATEBIGIN, 23)+ ' ' +convert(varchar, TMP.TIMEBEGIN, 108),''))) AS DATETIMEBEGIN
						  ,convert(datetime,(ISNULL(convert(varchar, TMP.DATEEND, 23)+ ' ' +convert(varchar, TMP.TIMEEND, 108),''))) AS DATETIMEEND
                 FROM   SHOP_BRANCH WITH (NOLOCK)	LEFT JOIN (
	                        SELECT [BRANCH_ID]
                                    ,[SHOP_ADVERTISEBRANCH].[FLASHID]
                                    ,[SHOP_ADVERTISEBRANCH].[PROMOTIONID]
                                    ,[SHOP_ADVERTISEBRANCH].[DATEBIGIN]
                                    ,[SHOP_ADVERTISEBRANCH].[DATEEND]
                                    ,[STATUSFILE]
							        ,ISNULL(SHOP_ADVERTISEBRANCH.[TIMEBEGIN],'') AS [TIMEBEGIN]
							        ,ISNULL(SHOP_ADVERTISEBRANCH.[TIMEEND],'') AS [TIMEEND]
		                    FROM    SHOP_ADVERTISEBRANCH WITH (NOLOCK) INNER JOIN 
                                    [SHOP_ADVERTISE] WITH (NOLOCK)  ON [SHOP_ADVERTISEBRANCH].FLASHID = [SHOP_ADVERTISE].[FLASHID] 
			                WHERE   [SHOP_ADVERTISEBRANCH].FLASHID = '{0}'  
                        )TMP   ON TMP.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                 WHERE  BRANCH_STAOPEN = '1' AND BRANCH_STA IN ('1')", fileid);
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private string InsertFileflash(string _flashid, string _flashtype, string _flashname)
        {
            string sqlIns_Adv = string.Format(@"
                    INSERT INTO SHOP_ADVERTISE 
                            ([FLASHID],[PROMOTIONID],[FLASHNAME],[FLASHFILENAME],[FLASHDETAIL]
                            ,[FLASHSIZE],[FLASHREMARK],[FLASHDEPT],[DATEBEGIN],[TIMEBEGIN]
                            ,[DATEEND],[TIMEEND],[FALSHSTATUS],[DATEIN],[TIMEIN]
                            ,[WHOIN],[DATEUP],[TIMEUP],[WHOUP])
                    VALUES  
                            ('{0}', '{1}', '{2}', '{3}', '{4}'
                            ,{5}, '{6}', '{7}', '{8}', '{9}'
                            ,'{10}', '{11}', '{12}',convert(varchar,getdate(),23) ,convert(varchar,getdate(),108)
                            ,'{13}',convert(varchar,getdate(),23) ,convert(varchar,getdate(),108),'{13}'
                    )"
                    , _flashid
                    , radTextBox_Remark.Text
                    , radTextBox_Remark.Text
                    , _flashname
                    , _flashtype
                    , sizeFile
                    , radTextBox_Remark.Text
                    , SystemClass.SystemDptID
                    , radDateTimePicker_DatetimeBegine.Value.ToString("yyyy-MM-dd")
                    , radDateTimePicker_DatetimeBegine.Value.ToString("HH:mm:ss")
                    , radDateTimePicker_DatetimeEnd.Value.ToString("yyyy-MM-dd")
                    , radDateTimePicker_DatetimeEnd.Value.ToString("HH:mm:ss")
                    , radDropDownList_Status.SelectedValue.ToString()
                    , SystemClass.SystemUserID_M);

            ArrayList arrayList = new ArrayList() { sqlIns_Adv };
            foreach (var branch in ((List<Branch>)radCheckedListBox_Branch.DataSource).Where(chk => chk.CheckState == CheckState.Checked))
            {
                string sqlIns_AdvBranch = string.Format(@"
                        INSERT INTO [dbo].[SHOP_ADVERTISEBRANCH]
                               ([BRANCH_ID],[FLASHID],[PROMOTIONID],[DATEBIGIN],[TIMEBEGIN]
                               ,[DATEEND],[TIMEEND],[STATUSFILE],[DATEIN],[TIMEIN]
                               ,[WHOIN],[DATEUP],[TIMEUP],[WHOUP])
                        VALUES
                               ('{0}','{1}','{2}','{3}','{4}'
                               ,'{5}','{6}','{7}',CONVERT(VARCHAR, GETDATE(), 23),CONVERT(VARCHAR, GETDATE(), 108)
                               ,'{8}',CONVERT(VARCHAR, GETDATE(), 23),CONVERT(VARCHAR, GETDATE(), 108),'{8}'
                        )"
                        , branch.Id
                        , _flashid
                        , radTextBox_Remark.Text
                        , radDateTimePicker_DatetimeBegine.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeBegine.Value.ToString("HH:mm:ss")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("HH:mm:ss")
                        , radDropDownList_Status.SelectedValue.ToString()
                        , SystemClass.SystemUserID_M);
                arrayList.Add(sqlIns_AdvBranch);
            }
            return ConnectionClass.ExecuteSQL_ArrayMain(arrayList);
        }
        private string UpDateFileflash(string _newFilename)
        {
            List<Branch> list = ((List<Branch>)radCheckedListBox_Branch.DataSource).Where(chk => chk.CheckState == CheckState.Checked).ToList();
            ArrayList arrayList = new ArrayList();
            if (list.Count == 0)
            {
                string sqlUpd_Adv = string.Format(@"
                    UPDATE dbo.SHOP_ADVERTISE 
                    SET     [FLASHREMARK] = '{0}'
                            ,[DATEBEGIN] = '{1}'
                            ,[TIMEBEGIN] = '{2}'
                            ,[DATEEND] = '{3}'
                            ,[TIMEEND] = '{4}'
                            ,[FALSHSTATUS] = '{5}'
                            ,[DATEUP] = CONVERT(VARCHAR, GETDATE(), 23)
                            ,[TIMEUP] = CONVERT(VARCHAR, GETDATE(), 108)
                            ,[WHOUP] = '{6}'
                    WHERE FLASHID = '{7}'"
                        , radTextBox_Remark.Text
                        , radDateTimePicker_DatetimeBegine.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeBegine.Value.ToString("HH:mm:ss")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("HH:mm:ss")
                        , "0"
                        , SystemClass.SystemUserID_M
                        , flash.FlashId);
                arrayList.Add(sqlUpd_Adv);
                //updbranchไม่ใช้
            }
            else
            {
                string sqlUpd_Adv = string.Format(@"
                    UPDATE dbo.SHOP_ADVERTISE 
                    SET     [FLASHREMARK] = '{0}'
                            ,[FLASHFILENAME] = '{1}'
                            ,[DATEBEGIN] = '{2}'
                            ,[TIMEBEGIN] = '{3}'
                            ,[DATEEND] = '{4}'
                            ,[TIMEEND] = '{5}'
                            ,[FALSHSTATUS] = '{6}'
                            ,[DATEUP] = CONVERT(VARCHAR, GETDATE(), 23)
                            ,[TIMEUP] = CONVERT(VARCHAR, GETDATE(), 108)
                            ,[WHOUP] = '{7}'
                    WHERE FLASHID = '{8}'"
                    , radTextBox_Remark.Text
                    , _newFilename
                    , radDateTimePicker_DatetimeBegine.Value.ToString("yyyy-MM-dd")
                    , radDateTimePicker_DatetimeBegine.Value.ToString("HH:mm:ss")
                    , radDateTimePicker_DatetimeEnd.Value.ToString("yyyy-MM-dd")
                    , radDateTimePicker_DatetimeEnd.Value.ToString("HH:mm:ss")
                    , radDropDownList_Status.SelectedValue.ToString()
                    , SystemClass.SystemUserID_M
                    , flash.FlashId);
                arrayList.Add(sqlUpd_Adv);

                foreach (var branch in list)
                {
                    bool checkbranch = this.CheckBranch(branch.Id, flash.FlashId);
                    string SqlUpd_AdvBranch;
                    if (checkbranch == true)
                    {
                        SqlUpd_AdvBranch = string.Format(@"
                        UPDATE dbo.SHOP_ADVERTISEBRANCH 
                        SET     [DATEBIGIN] = '{0}'
                               ,[TIMEBEGIN]= '{1}'
                               ,[DATEEND]= '{2}'
                               ,[TIMEEND]= '{3}'
                               ,[STATUSFILE]= '{4}'
                               ,[DATEUP]= CONVERT(VARCHAR, GETDATE(), 23)
                               ,[TIMEUP]= CONVERT(VARCHAR, GETDATE(), 108)
                               ,[WHOUP]= '{5}'
                        WHERE FLASHID = '{6}' AND BRANCH_ID = '{7}'"
                        , radDateTimePicker_DatetimeBegine.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeBegine.Value.ToString("HH:mm:ss")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("HH:mm:ss")
                        , radDropDownList_Status.SelectedValue.ToString()
                        , SystemClass.SystemUserID_M
                        , flash.FlashId
                        , branch.Id);
                    }
                    else
                    {
                        SqlUpd_AdvBranch = string.Format(@"
                        INSERT INTO [dbo].[SHOP_ADVERTISEBRANCH]
                               ([BRANCH_ID],[FLASHID],[PROMOTIONID],[DATEBIGIN],[TIMEBEGIN]
                               ,[DATEEND],[TIMEEND],[STATUSFILE],[DATEIN],[TIMEIN]
                               ,[WHOIN],[DATEUP],[TIMEUP],[WHOUP])
                        VALUES
                               ('{0}','{1}','{2}','{3}','{4}'
                               ,'{5}','{6}','{7}',CONVERT(VARCHAR, GETDATE(), 23),CONVERT(VARCHAR, GETDATE(), 108)
                               ,'{8}',CONVERT(VARCHAR, GETDATE(), 23),CONVERT(VARCHAR, GETDATE(), 108),'{8}'
                        )"
                        , branch.Id
                        , flash.FlashId
                        , radTextBox_Remark.Text
                        , radDateTimePicker_DatetimeBegine.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeBegine.Value.ToString("HH:mm:ss")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DatetimeEnd.Value.ToString("HH:mm:ss")
                        , radDropDownList_Status.SelectedValue.ToString()
                        , SystemClass.SystemUserID_M);
                    }
                    arrayList.Add(SqlUpd_AdvBranch);
                }
            }
            return ConnectionClass.ExecuteSQL_ArrayMain(arrayList);
        }
        private string GetFlashId(string typeFile)
        {
            string sql = string.Format(@"
                    SELECT  CASE
		                    WHEN YEAR(GETDATE()-1) = YEAR(GETDATE()) THEN 
			                    CONCAT('F',SUBSTRING(convert(varchar,YEAR(GETDATE())),3,2),'-',FORMAT((ISNULL(MAX(SUBSTRING(FLASHID,5,5)),0)+1),'00000')) 
		                    ELSE 
			                    CONCAT('F',SUBSTRING(convert(varchar,YEAR(GETDATE())),3,2),'-','00001')
		                    END AS FLASH_ID
                    FROM    SHOP_ADVERTISE WITH (NOLOCK) 
                    WHERE	FLASHID LIKE 'F%'", typeFile);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return dt.Rows.Count > 0 ? dt.Rows[0]["FLASH_ID"].ToString() : "";
        }
        private bool CheckBranch(string _branchid, string _flashid)
        {
            string sql = string.Format(@"SELECT BRANCH_ID FROM SHOP_ADVERTISEBRANCH WITH (NOLOCK) WHERE FLASHID = '{0}' AND	BRANCH_ID = '{1}'", _flashid, _branchid);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return dt.Rows.Count > 0 ? true : false;
        }
        #endregion

        #region Event
        private void RadGridView_Show_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow != null && e.CurrentRow.Index > -1)
            {
                GridViewRowInfo currentRow = this.RadGridView_Show.CurrentRow;
                dt_detail = this.Getfiledetail(currentRow.Cells["FLASHID"].Value.ToString());
                RadGridView_Detail.DataSource = dt_detail;
                this.Clear(true);
            }
            else
            {
                dt_detail.Rows.Clear();
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            //insert
            this.Cursor = Cursors.WaitCursor;
            if (UpdateSql == FileDataState.Added) {
                if (string.IsNullOrEmpty(radBrowseEditor_File.BrowseElement.Text) || radBrowseEditor_File.BrowseElement.Text.Equals("(none)")) {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("กรุณาระบุไฟล์ต้นทางก่อนบันทึก.");
                    this.Cursor = Cursors.Default;
                    return;
                }
                string type = this.GetMediaFile();
                if (string.IsNullOrEmpty(type)) {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถระบุประเภทไฟล์ได้ ต้องเป็นไฟล์รูปภาพหรือวีดีโอเท่านั้น กรุณาเช็คไฟล์ของคุณอีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }
                //insert-shopadv
                string flashId = this.GetFlashId(type);
                if (string.IsNullOrEmpty(type))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถระบุ FLASHID ได้ กรุณาติดต่อผู้ดูแลระบบ");
                    this.Cursor = Cursors.Default;
                    return;
                }
                else
                {
                    AdvFolder advFolder = type.Equals("VDO") ? AdvFolder.VDO : AdvFolder.PHOTO;
                    string sourceFile = fileFullname.Trim();
                    string filenameNew = this.GetFilename(flashId, radDateTimePicker_DatetimeBegine.Value, radDateTimePicker_DatetimeEnd.Value) + Path.GetExtension(sourceFile);
                    string destinationFile = $"{pathImage}{advFolder.ToString()}\\{filenameNew}";
                    try
                    {
                        File.Copy(sourceFile, destinationFile, true);
                        string return_Inserttransection = this.InsertFileflash(flashId, type, filenameNew);
                        if (!string.IsNullOrEmpty(return_Inserttransection))
                        {
                            File.Delete(destinationFile);
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกข้อมูลเรียบร้อย.");
                            this.Clear(true);
                            this.Getfileflash();
                            //dt_file = this.Getfileflash();
                            //RadGridView_Show.DataSource = dt_file;
                        }
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error(ex.Message);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
            }
            //update
            else {
                string newFilename = this.GetFilename(flash.FlashId, radDateTimePicker_DatetimeBegine.Value, radDateTimePicker_DatetimeEnd.Value) + Path.GetExtension(fileFullname);
                string return_Updatetransection = this.UpDateFileflash(newFilename);
                if (!string.IsNullOrEmpty(return_Updatetransection))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้.\n" + return_Updatetransection);
                    this.Cursor = Cursors.Default;
                    return;
                }
                else {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกข้อมูลเรียบร้อย.");
                    if (pictureBox.Image != null)
                    {
                        pictureBox.Image.Dispose();
                        pictureBox.Image = null;
                    }
                    //แก้ไขไฟล์ต้นทาง
                    FileInfo fi = new FileInfo(fileFullname);
                    // Check if file is there  
                    if (fi.Exists)
                    {
                        string expNew = pathImageFull + "\\" + newFilename;
                        // Move file with a new name. Hence renamed.  
                        fi.MoveTo(expNew);
                    }
                    //dt_file = this.Getfileflash();
                    //RadGridView_Show.DataSource = dt_file;
                    this.Getfileflash();
                    this.Clear(true);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }
            this.Cursor = Cursors.Default;
        }
        private void RadButton_CheckAll_Click(object sender, EventArgs e)
        {
            if (this.radButton_CheckAll.Text == "Check all")
            {
                this.radCheckedListBox_Branch.CheckAllItems();
                this.radButton_CheckAll.Text = "Uncheck all";
            }
            else
            {
                this.radCheckedListBox_Branch.UncheckAllItems();
                this.radButton_CheckAll.Text = "Check all";
            }
        }
        private void RadToggleSwitch_ShowMedai_ValueChanged(object sender, EventArgs e)
        {
            this.splitPanel3.Visible = this.radToggleSwitch_ShowMedai.Value;
            this.FitLayoutPanel(this.radToggleSwitch_ShowMedai.Value);
        }
        private void RadBrowseEditor1_ValueChanging(object sender, ValueChangingEventArgs e)
        {
            //e.Cancel = !File.Exists(e.NewValue.ToString());
        }
        private void RadBrowseEditor1_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.radBrowseEditor_File.BrowseElement.TextBoxItem.Text) && !this.radBrowseEditor_File.BrowseElement.TextBoxItem.Text.Equals("(none)"))
            {
                try
                {
                    RadBrowseEditorElement radBrowseEditorElement = (RadBrowseEditorElement)sender;
                    fileFullname = radBrowseEditorElement.AccessibleName;
                    //OpenFileDialog openFileDialog = (OpenFileDialog)this.radBrowseEditor1.Dialog;
                    //this.radBrowseEditor1.BrowseElement.Text = openFileDialog.SafeFileName;

                    //var fileLength = (new FileInfo(fileFullname).Length) / 1024;
                    this.radLabel_FieSize.Text = RoundFileSizeConverter.ConvertToString(new FileInfo(fileFullname).Length, null);
                    var size = (new FileInfo(fileFullname).Length) / 1024;

                    if ((this.IsMediaFile(this.radBrowseEditor_File.BrowseElement.TextBoxItem.Text) == true && size < 30000) ||
                            (this.IsImageFile(this.radBrowseEditor_File.BrowseElement.TextBoxItem.Text) == true && size < 5000))
                    {
                        sizeFile = (ushort)size;
                        this.radBrowseEditor_File.ReadOnly = false;
                        this.radBrowseEditor_File.BrowseElement.ShowClearButton = true;
                        this.radLabel_Success.Visible = true; this.radLabel_Success.Text = "Sussess";
                        try
                        {
                            Image image = Image.FromFile(fileFullname);
                            this.pictureBox.Image = image;
                            this.pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                        }
                        catch (Exception)
                        {
                            this.pictureBox.Image = new Bitmap(10, 10);
                        }
                    }
                
                    else {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไฟล์รูปควรมีขนาดไม่เกิน 5,000 KB และไฟล์วีดีโอควรมีขนาดไม่เกิน 30,000 KB");
                        this.radBrowseEditor_File.BrowseElement.TextBoxItem.Text = "";
                        this.radBrowseEditor_File.BrowseElement.ShowClearButton = false;
                    }
                }
                catch (Exception)
                {
                    return;
                }
            }
            else {
                this.radBrowseEditor_File.BrowseElement.ShowClearButton = false;
            }
        }
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            this.Clear(false);
            this.radBrowseEditor_File.BrowseElement.Focus();
            this.radDropDownList_Status.SelectedIndex = 1;
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Clear(true);
        }
        private void RadButtonElement_Edite_Click(object sender, EventArgs e)
        {
            UpdateSql = FileDataState.Edite;
            flash = new Flash() {
                FlashId= this.RadGridView_Show.CurrentRow.Cells["FLASHID"].Value.ToString(),
                FlashName = this.RadGridView_Show.CurrentRow.Cells["FLASHFILENAME"].Value.ToString(),
                FlashType = this.RadGridView_Show.CurrentRow.Cells["FLASHDETAIL"].Value.ToString()
            };
            UpdatePanelInfo(this.RadGridView_Show.CurrentRow);
        }
        private void RadButtonElement_Img_Click(object sender, EventArgs e)
        {
            using (AdvertiseExp mnObj = new AdvertiseExp(pathImage, GetFlashExpire()))
            {
                if (mnObj.ShowDialog(this) == DialogResult.OK)
                {
                    this.Getfileflash();
                }
                else {
                    this.Getfileflash();
                }
            } 
        }
        private void RadButtonElement_Pdf_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
        private void PictureBox_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(fileFullname);
            }
            catch (Exception ex) {
                MsgBoxClass.MsgBoxShowButtonOk_Warning(ex.Message);
                return;
            }
        }
        private void RadDateTimePicker_DatetimeBegine_ValueChanged(object sender, EventArgs e)
        {
            radDateTimePicker_DatetimeBegine.Value = radDateTimePicker_DatetimeBegine.Value > radDateTimePicker_DatetimeEnd.Value ? radDateTimePicker_DatetimeEnd.Value : radDateTimePicker_DatetimeBegine.Value;
        }
        private void RadDateTimePicker_DatetimeEnd_ValueChanged(object sender, EventArgs e)
        {
            radDateTimePicker_DatetimeEnd.MinDate = radDateTimePicker_DatetimeBegine.Value;
        }
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == RadGridView_Show.Columns["FLASHSHOW"] && e.RowIndex > -1)
            {
                try
                {
                    System.Diagnostics.Process.Start(this.GetPath(RadGridView_Show.CurrentRow.Cells["FLASHFILENAME"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["FLASHDETAIL"].Value.ToString()));
                }
                catch (Exception)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบไฟล์ ไฟล์อาจถูกลบแล้วเนื่องจากหมดอายุการใช้งาน.");
                    return;
                }
            }
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == RadGridView_Show.Columns["FLASHID"] && e.RowIndex > -1 )
            {
                if (Convert.ToDateTime(RadGridView_Show.CurrentRow.Cells["DATEEND"].Value.ToString()) < DateTime.Now && RadGridView_Show.CurrentRow.Cells["STATUSID"].Value.ToString().Equals("1"))
                {
                    if (MessageBox.Show("คุณต้องการปรับสถานะ และลบไฟล์หรือไม่?.\n" + RadGridView_Show.CurrentRow.Cells["FLASHID"].Value.ToString(), SystemClass.SystemHeadprogram, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        try
                        {
                            string Filedest = pathImage + @"BACKUP\" + RadGridView_Show.CurrentRow.Cells["FLASHFILENAME"].Value.ToString();
                            string Filesource = this.GetPath(RadGridView_Show.CurrentRow.Cells["FLASHFILENAME"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["FLASHDETAIL"].Value.ToString());
                            File.Move(Filesource, Filedest);
                            //UpdateStatus
                            string ret = UpdateStatusMedia(RadGridView_Show.CurrentRow.Cells["FLASHID"].Value.ToString());
                            if (string.IsNullOrEmpty(ret))
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ลบรูปเรียบร้อย");
                                this.Getfileflash();
                                //return;
                            }
                            else
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error(ret);
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                        catch (Exception)
                        {
                            this.Cursor = Cursors.Default;
                            return;
                        }
                        this.Cursor = Cursors.Default;
                    }
                }
            }
        }
        //GetGridViewSummary
        private void GetGridViewSummary(string Col, GridAggregateFunction gridAggregate)
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem(Col, "ใช้งาน : {0:0}", gridAggregate);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.RadGridView_Detail.SummaryRowsTop.Add(summaryRowItem);
        }
        private void RadGridView_Detail_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (!RadGridView_Detail.CurrentRow.Cells["CHOOSE"].Value.ToString().Equals("1")) { e.Cancel = true; }

            else if(!(e.Column.FieldName == "DATETIMEBEGIN" || e.Column.FieldName == "DATETIMEEND")) { e.Cancel = true; }
        }
        private void RadGridView_Detail_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            //RadDateTimeEditor editor = this.RadGridView_Detail.ActiveEditor as RadDateTimeEditor;
            if (this.RadGridView_Detail.ActiveEditor is RadDateTimeEditor)
            {
                //Pick up one of the default formats
                ((RadDateTimeEditorElement)((RadDateTimeEditor)this.RadGridView_Detail.ActiveEditor).EditorElement).Format = DateTimePickerFormat.Custom;
                //Or set a custom date format
                ((RadDateTimeEditorElement)((RadDateTimeEditor)this.RadGridView_Detail.ActiveEditor).EditorElement).CustomFormat = "dd-MM-yyyy HH:mm:ss";
                ((RadDateTimeEditorElement)((RadDateTimeEditor)this.RadGridView_Detail.ActiveEditor).EditorElement).ShowTimePicker = true;

                ((RadDateTimeEditorElement)((RadDateTimeEditor)this.RadGridView_Detail.ActiveEditor).EditorElement).MaxSize = new System.Drawing.Size(430, 320);

            }
        }
        private void RadGridView_Detail_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                try
                {
                    RadGridView_Detail.CurrentCell.Value = RadGridView_Detail.CurrentCell.Value.ToString();
                    if (RadGridView_Detail.CurrentRow.Cells["CHOOSE"].Value.ToString().Equals("1"))
                    {
                        string sql = null;
                        switch (e.Column.FieldName)
                        {
                            case "DATETIMEBEGIN":
                                sql = string.Format(@"
                                UPDATE dbo.SHOP_ADVERTISEBRANCH 
                                SET     [DATEBIGIN] = '{0}'
                                        ,[TIMEBEGIN]= '{1}'
                                        ,[DATEUP]= CONVERT(VARCHAR, GETDATE(), 23)
                                        ,[TIMEUP]= CONVERT(VARCHAR, GETDATE(), 108)
                                        ,[WHOUP]= '{2}'
                                WHERE FLASHID = '{3}' AND BRANCH_ID = '{4}'"
                                , Convert.ToDateTime(RadGridView_Detail.CurrentRow.Cells["DATETIMEBEGIN"].Value).ToString("yyyy-MM-dd")
                                , Convert.ToDateTime(RadGridView_Detail.CurrentRow.Cells["DATETIMEBEGIN"].Value).ToString("HH:mm:ss")
                                , SystemClass.SystemUserID_M
                                , RadGridView_Show.CurrentRow.Cells["FLASHID"].Value.ToString()
                                , RadGridView_Detail.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                                break;
                            case "DATETIMEEND":
                                sql = string.Format(@"
                                UPDATE dbo.SHOP_ADVERTISEBRANCH 
                                SET     [DATEEND]= '{0}'
                                        ,[TIMEEND]= '{1}'
                                        ,[DATEUP]= CONVERT(VARCHAR, GETDATE(), 23)
                                        ,[TIMEUP]= CONVERT(VARCHAR, GETDATE(), 108)
                                        ,[WHOUP]= '{2}'
                                WHERE FLASHID = '{3}' AND BRANCH_ID = '{4}'"
                                , Convert.ToDateTime(RadGridView_Detail.CurrentRow.Cells["DATETIMEEND"].Value).ToString("yyyy-MM-dd")
                                , Convert.ToDateTime(RadGridView_Detail.CurrentRow.Cells["DATETIMEEND"].Value).ToString("HH:mm:ss")
                                , SystemClass.SystemUserID_M
                                , RadGridView_Show.CurrentRow.Cells["FLASHID"].Value.ToString()
                                , RadGridView_Detail.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                                break;
                            default:
                                break;
                        }
                        if (!string.IsNullOrEmpty(sql))
                        {
                            string ret = ConnectionClass.ExecuteSQL_Main(sql);
                            if (!string.IsNullOrEmpty(ret))
                            {
                                MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //return 
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void RadButtonElement_Tranf_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ไฟล์ที่เลือกจะถูกโอนไปยังเครื่องเล่นทุกสาขาที่กำหนดไว้ ต้องการโอนไฟล์ไปยังเครื่องเล่น?.") == DialogResult.Yes)
            {
                Flash flash = new Flash() {
                FlashId = this.RadGridView_Show.CurrentRow.Cells["FLASHID"].Value.ToString(),
                FlashType = this.RadGridView_Show.CurrentRow.Cells["FLASHDETAIL"].Value.ToString(),
                FlashName = this.RadGridView_Show.CurrentRow.Cells["FLASHFILENAME"].Value.ToString()
                };
                this.TranferfileToComname(GedBranchPlay(flash.FlashId),Advertisopen(), flash);
            }
            else {
                return;
            }

        }
        #endregion
    }
    internal class Advertisopen
    {
        public string Comname { get; set; }
        public string Branch { get; set; }
    }
    internal class Branch
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public CheckState CheckState { get; set; }
    }
    internal class Flash
    {
        public string FlashId { get; set; }
        public string FlashType { get; set; }
        public string FlashName { get; set; }
    }
    public class FileMedia
    {
        public string FlashId { get; set; }
        public string FileName { get; set; }
        public string FileFullName { get; set; }
        public string DateExp { get; set; }

    }
}
