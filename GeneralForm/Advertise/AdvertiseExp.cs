﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Advertise
{
    public partial class AdvertiseExp : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dt = new DataTable("dt");
        readonly string _pathImage;
        readonly List<Flash> _flashexps;
        public AdvertiseExp(string pathImage, IEnumerable flashexps)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pathImage = pathImage;
            _flashexps = (List<Flash>)flashexps;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            { 
                base.OnKeyDown(e); 
            }           
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            ConditionalFormattingForm form = (ConditionalFormattingForm)sender;
            DatagridClass.SetFormFont(form.Controls);
        }
        //load
        private void AdvertiseExp_Load(object sender, EventArgs e)
        {
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("FlashId", "FlashId"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("FileFullName", "ชื่อไฟล์"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("FileName", "ชื่อไฟล์", 400));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DateExp", "วันที่หมดอายุ", 100));
            radGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Command", "Watch", "", 30));
            radGridView_Show.MasterTemplate.Columns.Add(AppendNewColumn("Command", "Delete", "", 30));

            dt.Columns.Add("FlashId");
            dt.Columns.Add("FileFullName");
            dt.Columns.Add("FileName");
            dt.Columns.Add("DateExp");
            dt.Columns.Add("Watch");
            dt.Columns.Add("Delete");
            this.GetData(GetLoadFileExp());
        }
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == radGridView_Show.Columns["Delete"] && e.RowIndex > -1)
            {
                try
                {
                    if (MessageBox.Show("ยืนยันการลบข้อมูล ? " + radGridView_Show.CurrentRow.Cells["FileName"].Value.ToString()
                        , SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        //File.Delete(radGridView_Show.CurrentRow.Cells["FileFullName"].Value.ToString());
                        string pathFile_new = _pathImage + "BACKUP\\" + radGridView_Show.CurrentRow.Cells["FileName"].Value.ToString();
                        File.Move(radGridView_Show.CurrentRow.Cells["FileFullName"].Value.ToString(), pathFile_new);
                        //UpdateStatus
                        string ret = UpdateStatusMedia(radGridView_Show.CurrentRow.Cells["FlashId"].Value.ToString());
                        if (string.IsNullOrEmpty(ret))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ลบรูปเรียบร้อย");
                            this.GetData(GetLoadFileExp());
                            return;
                        }
                        else {
                            MsgBoxClass.MsgBoxShowButtonOk_Error(ret);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error(ex.Message);
                    return;
                }
            }

            if (e.Column == radGridView_Show.Columns["Watch"] && e.RowIndex > -1)
            {
                if (string.IsNullOrEmpty(radGridView_Show.CurrentRow.Cells["FileFullName"].Value.ToString()))
                {
                    return;
                }
                else
                {
                    System.Diagnostics.Process.Start(radGridView_Show.CurrentRow.Cells["FileFullName"].Value.ToString());
                }
            }
        }
        private void GetData(List<FileMedia> _fileMedia) {
            if(dt.Rows.Count> 0) dt.Rows.Clear();
            foreach (var file in _fileMedia)
            {
                DataRow row = dt.NewRow();
                row["FlashId"] = file.FlashId;
                row["FileFullName"] = file.FileFullName;
                row["FileName"] = file.FileName;
                row["DateExp"] = file.DateExp;
                row["Watch"] = "ดู";
                row["Delete"] = "ลบ";
                dt.Rows.Add(row);
            }
            radGridView_Show.DataSource = dt;
        }
        private List<FileMedia> GetLoadFileExp()
        {
            string pathImg = _pathImage + Adverstise_Manage.AdvFolder.PHOTO;
            string pathVdo = _pathImage + Adverstise_Manage.AdvFolder.VDO;
            DirectoryInfo DirInfoImag = new DirectoryInfo(pathImg);
            DirectoryInfo DirInfoVdo = new DirectoryInfo(pathVdo);

            List<FileMedia> medias = new List<FileMedia>();
            foreach (var fi in _flashexps) {
                FileMedia file;
                try
                {
                    if (fi.FlashType.Equals("VDO"))
                    {
                        FileInfo[] vdo = DirInfoVdo.GetFiles(string.Format(@"*{0}", fi.FlashName), SearchOption.AllDirectories);
                        file = new FileMedia()
                        {
                            FlashId = fi.FlashId,
                            FileFullName = vdo[0].FullName,
                            FileName = fi.FlashName,
                            DateExp = vdo[0].Name.Split(',')[1].Split('_', '.')[1].ToString()
                        };
                    }
                    else
                    {
                        FileInfo[] img = DirInfoImag.GetFiles(string.Format(@"*{0}", fi.FlashName), SearchOption.AllDirectories);
                        file = new FileMedia()
                        {
                            FlashId = fi.FlashId,
                            FileFullName = img[0].FullName,
                            FileName = fi.FlashName,
                            DateExp = img[0].Name.Split(',')[1].Split('_', '.')[1].ToString()
                        };
                    }
                    medias.Add(file);
                }
                catch(Exception) { 
                
                }
            }
            return medias;
        }
        private string UpdateStatusMedia(string flashid) 
        {
            ArrayList arrayList = new ArrayList();
            string sqlUpd_Adv = string.Format(@"
                    UPDATE dbo.SHOP_ADVERTISE 
                    SET     [FALSHSTATUS] = '0'
                            ,[DATEUP] = CONVERT(VARCHAR, GETDATE(), 23)
                            ,[TIMEUP] = CONVERT(VARCHAR, GETDATE(), 108)
                            ,[WHOUP] = '{0}'
                    WHERE FLASHID = '{1}'"
                    , SystemClass.SystemUserID_M
                    , flashid);
            arrayList.Add(sqlUpd_Adv);
                string SqlUpd_AdvBranch = string.Format(@"
                    UPDATE dbo.SHOP_ADVERTISEBRANCH 
                    SET     [STATUSFILE]= '0'
                            ,[DATEUP]= CONVERT(VARCHAR, GETDATE(), 23)
                            ,[TIMEUP]= CONVERT(VARCHAR, GETDATE(), 108)
                            ,[WHOUP]= '{0}'
                    WHERE FLASHID = '{1}'"
                    , SystemClass.SystemUserID_M
                    , flashid);
            arrayList.Add(SqlUpd_AdvBranch);
            return ConnectionClass.ExecuteSQL_ArrayMain(arrayList);
        }
        private GridViewDataColumn AppendNewColumn(string columnType, string fieldName, string labelName, int _width)
        {
            GridViewDataColumn newColumn = null;
            switch (columnType)
            {
                case "Text":
                    newColumn = new GridViewTextBoxColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        HeaderTextAlignment = ContentAlignment.MiddleLeft,
                        TextAlignment = ContentAlignment.MiddleLeft,
                        Width = _width,
                        WrapText = true
                    };
                    break;
                case "Command":
                    newColumn = new GridViewCommandColumn()
                    {
                        Name = fieldName,
                        FieldName = fieldName,
                        HeaderText = labelName,
                        HeaderTextAlignment = ContentAlignment.MiddleCenter,
                        TextAlignment = ContentAlignment.MiddleCenter,
                        Width = _width,
                        WrapText = true
                    };
                    break;
            }
            return newColumn;
        }
    }
}
