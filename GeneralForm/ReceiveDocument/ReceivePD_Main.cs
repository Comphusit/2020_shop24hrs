﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Linq;

namespace PC_Shop24Hrs.GeneralForm.ReceiveDocument
{
    public partial class ReceivePD_Main : Telerik.WinControls.UI.RadForm
    {

        public ReceivePD_Main()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        //OnKeyEvent
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadDateTimePicker_OrderBegin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_OrderBegin, radDateTimePicker_OrderEnd);
        }

        private void RadDateTimePicker_OrderEnd_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_OrderBegin, radDateTimePicker_OrderEnd);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion
        //Load Main
        #region Event
        private void ReceivePD_Main_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่เอกสาร", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTID", "จัดซื้อ", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "ชื่อจัดซื้อ", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VENDORID", "ผู้จำหน่าย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VENDORNAME", "ชื่อผู้จำหน่วย", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERID", "ผู้รับ", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERNAME", "ชื่อผู้รับ", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่", 200));

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderBegin, DateTime.Today, DateTime.Now.Date);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderEnd, DateTime.Today, DateTime.Now.Date);

            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มเอกสาร";
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;

            GetGridViewSummary();
            SetEnabled(panel_Show.Controls);

            ClearDataPanel();
            //FindData();
            RadGridView_Show.DataSource = FindData("0", "");
        }
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearDataPanel();
            radTextBox_Doc.Enabled = true;
            radTextBox_Doc.Focus();
        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearDataPanel();
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            SaveData();
        }
        private void RadTextBox_Doc_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        if (!string.IsNullOrEmpty(radTextBox_Doc.Text))
                        {

                            if (FindData("1", radTextBox_Doc.Text.Trim()).Rows.Count > 0)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ใบคืนสินค้าใบนี้มี [{ radTextBox_Doc.Text.Trim()}] ข้อมูลอยู่แล้ว{Environment.NewLine}ไม่สามารถบันทึกรับได้อีก.");
                                radTextBox_Doc.SelectAll();
                                return;
                            }

                            DataTable dtDoc = PurchClass.GetDetailPD_ByDocno(radTextBox_Doc.Text.Trim());
                            if (dtDoc.Rows.Count == 0)
                            {
                                MsgBoxClass.MsgBoxShow_FindRecordNoData($@"เลขที่ {radTextBox_Doc.Text.Trim()}");
                                return;
                            }

                            radTextBox_Doc.Text = dtDoc.Rows[0]["PurchId"].ToString();
                            radLabel_Vendid.Text = dtDoc.Rows[0]["OrderAccount"].ToString();
                            radLabel_VendName.Text = dtDoc.Rows[0]["PurchName"].ToString();
                            radLabel_Puch.Text = dtDoc.Rows[0]["DIMENSION"].ToString();
                            radLabel_PuchName.Text = dtDoc.Rows[0]["DESCRIPTION"].ToString();

                            radTextBox_Doc.Enabled = false;
                            radButton_Save.Enabled = true;
                            radButton_Cancel.Enabled = true;
                            radTextBox_Remark.Enabled = true;
                            radTextBox_Remark.Focus();

                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("เลขที่ PD");
                            radTextBox_Doc.Focus();
                        }
                    }

                    break;
                default:
                    radTextBox_Doc.Focus();
                    break;
            }

        }
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            //FindData();
            RadGridView_Show.DataSource = FindData("0", "");
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region Method
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("DEPTNAME", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void ClearDataPanel()
        {
            radTextBox_Doc.Text = string.Empty;
            radTextBox_Remark.Text = string.Empty;
            radLabel_Vendid.Text = string.Empty;
            radLabel_VendName.Text = string.Empty;
            radLabel_Puch.Text = string.Empty;
            radLabel_PuchName.Text = string.Empty;
            radTextBox_Doc.Enabled = false;
            radTextBox_Remark.Enabled = false;
            radButton_Save.Enabled = false;
            radButton_Cancel.Enabled = false;
        }
        private void SetEnabled(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;

            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
            }
        }
        private DataTable FindData(string pType, string docno)
        {
            string sql = "";
            switch (pType)
            {
                case "0":
                    sql = $@"
                        SELECT  DOCNO, DEPTID, DEPTNAME, VENDORID, VENDORNAME, REMARK, USERID, USERNAME, DATEINS
                        FROM    [SHOP_RECEIVE_CN] WITH(NOLOCK)
                        WHERE   convert(varchar,DATEINS,23) BETWEEN '{radDateTimePicker_OrderBegin.Value:yyyy-MM-dd}' AND '{radDateTimePicker_OrderEnd.Value:yyyy-MM-dd}'
                        ORDER BY  DATEINS DESC ";
                    break;
                case "1":
                    sql = $@"
                        SELECT ISNULL([DOCNO],'') AS DOCNO FROM [SHOP_RECEIVE_CN] WITH (NOLOCK)  WHERE  DOCNO = '{docno}' ";
                    break;
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }

        private void SaveData()
        {

            string sql = $@"
            INSERT INTO SHOP_RECEIVE_CN 
                    (  [DOCNO],[DEPTID],[DEPTNAME],
                    [VENDORID],[VENDORNAME],[REMARK],
                    [USERID],[USERNAME]) 
            values ('{radTextBox_Doc.Text.Trim()}','{radLabel_Puch.Text.Trim()}','{radLabel_PuchName.Text}',
                    '{radLabel_Vendid.Text.Trim()}','{radLabel_VendName.Text}','{radTextBox_Remark.Text.Replace(",", "")}',
                    '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";

            string returnExc = ConnectionClass.ExecuteSQL_Main(sql);
            if (returnExc == "")
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                ClearDataPanel();
                radTextBox_Doc.Enabled = true;
                radTextBox_Doc.Focus();
                //FindData();
                RadGridView_Show.DataSource = FindData("0", "");
            }
            else
            {
                radTextBox_Doc.Focus();
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
            }
        }

        #endregion


    }
}
