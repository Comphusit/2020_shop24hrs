﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.ReceiveDocument
{
    public partial class MNRH_Receive : Telerik.WinControls.UI.RadForm
    {
        //DataTable dt = new DataTable();// = new DataTable();
        //private readonly DataTable dtDept = new DataTable();// = new DataTable();
        //private readonly DataTable dtDoc = new DataTable();// = new DataTable();
        //  string DocNo;
        public MNRH_Receive()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        #region Event
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void MNRH_Receive_Load(object sender, EventArgs e)
        {

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มข้อมูล";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่เอกสาร", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE", "วันที่", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Qty", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Remark", "หมายเหตุ", 150));

            SetTextControls(tableLayoutPanel1.Controls);
            ClearData();
        }

        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                    //DocNo = "MN";
                    if (string.IsNullOrEmpty(radTextBox_Item.Text))
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุเลขที่เอกสารก่อนเท่านั้น");
                        radTextBox_Item.Focus();
                        return;
                    }
                    string docno = radTextBox_Item.Text.ToUpper();
                    if (radTextBox_Item.Text.Substring(0, 2).ToUpper() != "MN") docno = "MN" + radTextBox_Item.Text.ToUpper();

                    DataTable dt = FindData(docno);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["StaDoc"].ToString() == "3")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("เอกสารถูกยกเลิกไม่สามารถบันทึกรับได้.");
                            radTextBox_Item.SelectAll();
                            radTextBox_Item.Focus();
                            return;
                        }

                        if (dt.Rows[0]["Bill_ReciveSta"].ToString() == "1")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("เลขที่เอกสารได้ระบุส่งไปแล้ว เช็คใหม่อีกครั้ง.");
                            radTextBox_Item.SelectAll();
                            radTextBox_Item.Focus();
                            return;
                        }

                        string returnExc = SaveData(docno);
                        if (returnExc == "")
                        {
                            radGridView_Show.Rows.Add(dt.Rows[0]["DOCNO"].ToString()
                                , dt.Rows[0]["DATE"].ToString()
                                , dt.Rows[0]["BranchID"].ToString()
                                , dt.Rows[0]["BranchName"].ToString()
                                , dt.Rows[0]["Qty"].ToString()
                                , dt.Rows[0]["UNIT"].ToString()
                                , dt.Rows[0]["Remark"].ToString());

                            radTextBox_Item.Text = "";
                            radTextBox_Item.Focus();
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                            radTextBox_Item.SelectAll();
                            radTextBox_Item.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลของบิลที่ระบุ เช็คใหม่อีกครั้ง.");
                        radTextBox_Item.Text = "";
                        radTextBox_Item.Focus();
                        return;
                    }
                    break;
                default:
                    radTextBox_Item.Focus();
                    break;
            }
        }
        #endregion

        #region Method
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null) return;
            
            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
            }

            foreach (RadButton c in controlCollection.OfType<RadButton>())
            {
                c.ButtonElement.ShowBorder = true;
            }
        }
        private void ClearData()
        {
            radTextBox_Item.Text = "";
            radTextBox_Item.Focus();
            radTextBox_Item.Enabled = true;
            radButton_Save.Enabled = false;
            radButton_Cancel.Enabled = false;

            if (radGridView_Show.Rows.Count > 0) radGridView_Show.Rows.Clear();

        }
        private DataTable FindData(string pDocno)
        {
            string sql = $@"
                SELECT  SHOP_MNRH_HD.DOCNO,CONVERT(VARCHAR,DATE,23) AS DATE,BranchID ,BranchName ,
		                SUM(QTY) AS Qty,'ชุด' AS UNIT,ISNULL(Remark,'') AS Remark,StaDoc,Bill_ReciveSta 
                FROM	SHOP_MNRH_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNRH_DT WITH (NOLOCK) ON SHOP_MNRH_HD.Docno = SHOP_MNRH_DT.DocNo 
                WHERE	SHOP_MNRH_HD.DocNo = '{pDocno}' 
                Group BY SHOP_MNRH_HD.DOCNO,CONVERT(VARCHAR,DATE,23),BranchID ,BranchName ,Remark,StaDoc,Bill_ReciveSta ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private string SaveData(string docno)
        {
            string sql = $@"
                Update  SHOP_MNRH_HD 
                Set     Bill_ReciveSta = '1',
                        Bill_ReciveWhoIn = '{SystemClass.SystemUserID_M}' ,
                        Bill_ReciveWhoName = '{SystemClass.SystemUserName}',
                        Bill_ReciveDate = CONVERT(VARCHAR,GETDATE(),23), 
                        Bill_ReciveTime = CONVERT(VARCHAR,GETDATE(),24) 
                WHERE   DocNo = '{docno}' ";

            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        #endregion

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            string returnExportStatus = DatagridClass.ExportExcelGridView(this.Text, radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(returnExportStatus);
        }

    }
}