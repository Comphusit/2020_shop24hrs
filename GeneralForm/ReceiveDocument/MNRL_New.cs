﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.ReceiveDocument
{
    public partial class MNRL_New : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dt_data = new DataTable();
        private DataTable dtDept = new DataTable();
        private DataTable dtType = new DataTable();

        string pCopyColor, pSave = "0", BillNo = "";
        readonly string pHeadBillPrint = "ใบนำส่งเอกสารคืนแผนก";
        string Item;
        int pCopy = 0;

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        public string NameRecive;
        readonly DataTable _dtData;
        public MNRL_New(DataTable dtData)
        {
            InitializeComponent();
            _dtData = dtData;
            this.KeyPreview = true;
        }
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        #region Event
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }
        private void MNRL_New_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            DatagridClass.SetDefaultRadGridView(radGridView_Show);


            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "เลขที่เอกสาร", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "รายละเอียด", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNIT", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 300));
            radGridView_Show.MasterTemplate.EnableFiltering = false;

            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            radTextBox_Description.Font = SystemClass.SetFontGernaral;
            radTextBox_Description.ForeColor = ConfigClass.SetColor_Blue();

            radButton_FindDpt.ButtonElement.ToolTipText = "ค้นหาแผนก"; radButton_FindDpt.ButtonElement.ShowBorder = true;
            radButton_Claer.ButtonElement.ToolTipText = "ลบข้อมูล"; radButton_Claer.ButtonElement.ShowBorder = true;
            radButton_FindDoc.ButtonElement.ToolTipText = "ค้นหาเอกสารอื่นๆ"; radButton_FindDoc.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ToolTipText = "เพิ่มรายการ"; radButton_Add.ButtonElement.ShowBorder = true;


            dt_data.Columns.Add("ITEMBARCODE");
            dt_data.Columns.Add("ITEMNAME");
            dt_data.Columns.Add("QTY");
            dt_data.Columns.Add("UNIT");
            dt_data.Columns.Add("REMARK");

            SetTextControls(radGroupBox_DB.Controls);
            dtDept = Models.DptClass.GetDpt_All();
            GetDept("");
            dtType = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("35", "", "", "1");
            ClearData();

            if (_dtData.Rows.Count > 0) SetData();

        }
        //
        void SetData()
        {
            radGridView_Show.DataSource = _dtData;

            radGridView_Show.Columns["ITEMBARCODE"].IsVisible = false;
            radGridView_Show.Columns["UNIT"].IsVisible = false;

            radLabel_Docno.Text = _dtData.Rows[0]["DOCNO"].ToString();
            radTextBox_Description.Text = _dtData.Rows[0]["REMARKDOC"].ToString();

            GetDept(_dtData.Rows[0]["DeptID"].ToString());

            if (_dtData.Rows[0]["Bill_ReciveSta"].ToString() == "1")
            {
                radTextBox_Emp.Text = _dtData.Rows[0]["Bill_ReciveWhoIn"].ToString(); radTextBox_Emp.Enabled = false;
                radLabel_Name.Text = _dtData.Rows[0]["Bill_ReciveWhoName"].ToString(); radLabel_Name.Enabled = false;
                radButton_Save.Enabled = false;
            }
            else
            {
                radTextBox_Emp.Text = ""; radLabel_Name.Text = "";
                radTextBox_Emp.SelectAll();
                radTextBox_Emp.Focus();
            }

        }
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        private void RadButton_Claer_Click(object sender, EventArgs e)
        {
            ClearDataDT();
        }
      
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Qty.Text))
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter(radLabel3.Text);
                radTextBox_Qty.Focus();
                return;
            }

            InsertDt();
        }
        private void RadTextBox_Qty_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        private void RadTextBox_Item_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter://ระบุบาร์โค้ด [Enter]  
                    if (string.IsNullOrEmpty(radTextBox_Item.Text))
                    {
                        MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ระบุเลขที่เอกสารก่อนเท่านั้น");
                        radTextBox_Item.Focus();
                        return;
                    }

                    if (CheckDocument(radTextBox_Item.Text) > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("เลขที่เอกสารได้ระบุส่งไปแล้ว เช็คใหม่อีกครั้ง.");
                        radTextBox_Item.SelectAll();
                        return;
                    }

                    if (dt_data.Rows.Count > 0)
                    {
                        DataRow[] dr = dt_data.Select($@" ITEMBARCODE = '{radTextBox_Item.Text}' ");
                        if (dr.Length > 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("เลขที่เอกสารได้ระบุส่งไปแล้ว เช็คใหม่อีกครั้ง.");
                            radTextBox_Item.Focus();
                            return;
                        }
                    }
                    Item = radTextBox_Item.Text;
                    radTextBox_Qty.Text = "1";
                    radTextBox_Unit.Text = "ชุด";
                    radTextBox_Remark.Text = "";
                    radTextBox_Item.Enabled = false;
                    radTextBox_Qty.Enabled = false;
                    radTextBox_Remark.Enabled = false;
                    InsertDt();
                    break;
                default:
                    radTextBox_Item.Focus();
                    break;
            }
        }
        private void RadTextBox_Qty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(radTextBox_Qty.Text))
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter(radLabel3.Text);
                    radTextBox_Qty.Focus();
                    return;
                }

                if (radTextBox_Remark.Enabled == false)
                {
                    InsertDt();
                    return;
                }
                radTextBox_Remark.Focus();
            }
        }
        private void RadTextBox_Unit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(radTextBox_Qty.Text))
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter(radLabel3.Text);
                    radTextBox_Qty.Focus();
                    return;
                }
                InsertDt();
            }
        }
        private void RadTextBox_Remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Add.Focus();
        }
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            if (_dtData.Rows.Count > 0) return;

            //F2
            if (e.KeyCode == Keys.F2)
            {
                if (pSave == "0")
                {
                    string desc = radGridView_Show.CurrentRow.Cells["ITEMNAME"].Value.ToString() + "  จำนวน  " +
                    radGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() + "  " +
                    radGridView_Show.CurrentRow.Cells["UNIT"].Value.ToString();

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete(desc) == DialogResult.No) return;

                    radGridView_Show.Rows.Remove(radGridView_Show.CurrentRow);
                    ClearDataDT();
                }
            }
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (_dtData.Rows.Count > 0)
            {
                if ((radTextBox_Emp.Text == "") || (radLabel_Name.Text == ""))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุพนักงานรับบิลก่อนบันทึก.");
                    radTextBox_Emp.SelectAll();
                    radTextBox_Emp.Focus();
                    return;
                }

                string sql = $@"
                Update  SHOP_MNRL_HD 
                Set     WhoUp = '{SystemClass.SystemUserName}',
                        DateUp = GETDATE(), 
                        Bill_ReciveSta = '1',
                        Bill_ReciveWhoIn = '{radTextBox_Emp.Text.Trim()}',
                        Bill_ReciveWhoName = '{radLabel_Name.Text}',
                        Bill_ReciveDate = GETDATE() 
                WHERE   DocNo = '{_dtData.Rows[0]["DOCNO"]}' ";

                string returnExc = ConnectionClass.ExecuteSQL_Main(sql);
                if (returnExc == "")
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                    radTextBox_Emp.Enabled = false;
                    radButton_Save.Enabled = false;
                    NameRecive = radLabel_Name.Text;
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                    radTextBox_Emp.Focus();
                    return;
                }
            }
            else
            {
                if (radDropDownList_Dpt.SelectedIndex < 0) return;
                if (string.IsNullOrEmpty(Models.DptClass.GetDptName_ByDptID(radDropDownList_Dpt.SelectedValue.ToString())))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("เช็คข้อมูลแผนกให้ถูกต้องอีกครั้ง.");
                    radTextBox_Item.Focus();
                    return;
                }

                if (pSave == "0")
                {
                    if (radGridView_Show.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ไม่มีบรรทัดรายการ");
                        radTextBox_Item.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(radDropDownList_Dpt.Text))
                    {
                        MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("แผนกรับ");
                        radTextBox_Item.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(radTextBox_Description.Text))
                    {
                        if (MessageBox.Show("ยืนยันส่งเอกสารโดยไม่ระบุหมายเหตุู.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        {
                            radTextBox_Description.Focus();
                            return;
                        }
                    }

                    SaveData();
                }
                else
                {
                    pCopy += 1;
                    PrintData();
                    return;
                }
            }
             
        }
        private void RadDropDownList_Dpt_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Item.Focus();
        }
        private void RadButton_FindDpt_Click(object sender, EventArgs e)
        {
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtDept
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radDropDownList_Dpt.SelectedValue = frm.pID; //radTextBox_Cst.Focus();
            }
            else
            {
                radDropDownList_Dpt.SelectedIndex = -1; //radTextBox_Branch.Focus();
            }
        }
        private void RadButton_FindDoc_Click(object sender, EventArgs e)
        {

            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV
            {
                dtData = dtType
            };
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                radTextBox_Item.Text = frm.pDesc; //radTextBox_Cst.Focus();
                Item = "";
                radTextBox_Item.Enabled = false;
                radTextBox_Unit.Text = "ชุด";
                radTextBox_Unit.Enabled = false;
                radTextBox_Remark.Enabled = true;
                radTextBox_Qty.Enabled = true;
                radTextBox_Qty.Focus();
            }
            else
            {
                radTextBox_Item.Text = ""; //radTextBox_Branch.Focus();
            }
        }
        #endregion

        #region Method
        private void SetTextControls(Control.ControlCollection controlCollection)
        {
            if (controlCollection == null)
            {
                return;
            }
            foreach (RadLabel c in controlCollection.OfType<RadLabel>())
            {
                c.Font = SystemClass.SetFontGernaral_Bold;
            }

            foreach (RadButton c in controlCollection.OfType<RadButton>())
            {
                c.ButtonElement.ShowBorder = true;
            }
        }
        private void ClearData()
        {
            NameRecive = "";
            if (_dtData.Rows.Count == 0)
            {
                radLabel_Docno.Text = "";
                radTextBox_Item.Text = ""; radTextBox_Item.Enabled = true;

                radTextBox_Remark.Text = "";
                radTextBox_Qty.Text = "";
                radTextBox_Description.Text = "";

                radButton_Save.Enabled = true;
                radTextBox_Item.Enabled = true;

                radTextBox_Qty.Enabled = true;
                radTextBox_Remark.Enabled = true;
                radButton_Add.Enabled = true;
                radButton_Claer.Enabled = true;
                radTextBox_Description.Enabled = true;

                radTextBox_Item.Focus();
                if (dt_data.Rows.Count > 0) dt_data.Rows.Clear();

                if (radGridView_Show.Rows.Count > 0) radGridView_Show.Rows.Clear();
                radDropDownList_Dpt.SelectedIndex = -1;

                radTextBox_Emp.Visible = false; radLabel_emp.Visible = false;
                radLabel_Name.Text = "";
            }
            else
            {
                radLabel_Docno.Enabled = false;
                radTextBox_Item.Enabled = false;

                radTextBox_Remark.Enabled = false;
                radTextBox_Qty.Enabled = false;
                radTextBox_Description.Enabled = false;

                radButton_Save.Enabled = true;
                radTextBox_Item.Enabled = false;

                radTextBox_Qty.Enabled = false;
                radTextBox_Remark.Enabled = false;
                radButton_Add.Enabled = false;
                radButton_Claer.Enabled = false;
                radTextBox_Description.Enabled = false;
                radButton_FindDpt.Enabled = false; radButton_FindDoc.Enabled = false;
                radDropDownList_Dpt.Enabled = false; radTextBox_Unit.Enabled = false;
                radTextBox_Emp.Visible = true; radLabel_emp.Visible = true;
                radLabel_Name.Text = "";
            }

        }
        //Clear Rows
        private void ClearDataDT()
        {
            Item = "";
            radTextBox_Item.Text = ""; radTextBox_Item.Enabled = true; radTextBox_Item.Focus();
            radTextBox_Qty.Text = ""; radTextBox_Qty.Enabled = false;
            radTextBox_Unit.Text = "";
            radTextBox_Remark.Text = ""; radTextBox_Remark.Enabled = false;
        }
        private void ClearEnable()
        {
            radTextBox_Item.Enabled = false;
            radTextBox_Qty.Enabled = false;
            radButton_Add.Enabled = false;
            radButton_Claer.Enabled = false;
            radTextBox_Remark.Enabled = false;
            radButton_FindDpt.Enabled = false;
            radButton_FindDoc.Enabled = false;
            radDropDownList_Dpt.Enabled = false;
            radTextBox_Description.Enabled = false;
        }
        private void GetDept(string values)
        {
            if (dtDept.Rows.Count > 0)
            {
                radDropDownList_Dpt.DataSource = dtDept;
                radDropDownList_Dpt.ValueMember = "NUM";
                radDropDownList_Dpt.DisplayMember = "DESCRIPTION_SHOW";
            }
            if (values == "") radDropDownList_Dpt.SelectedIndex = 0;
            else radDropDownList_Dpt.SelectedValue = values;
        }
        
        private void InsertDt()
        {
            if (dt_data.Rows.Count > 19)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("จำกัดเอกสารแค่ 20 รายการต่อ 1 บิล กรุณาเปิดบิลใหม่.");
                return;
            }
            dt_data.Rows.Add(Item, radTextBox_Item.Text, radTextBox_Qty.Text, radTextBox_Unit.Text, radTextBox_Remark.Text);
            radGridView_Show.DataSource = dt_data;
            ClearDataDT();
        }
        private void SaveData()
        {
            ArrayList strSql = new ArrayList();
            BillNo = Class.ConfigClass.GetMaxINVOICEID("MNRL", "-", "MNRL", "1");

            if (BillNo != "")
            {
                string[] dptName = radDropDownList_Dpt.Text.Split('-');

                strSql.Add($@"INSERT INTO SHOP_MNRL_HD 
                        ( DocNo,DeptID,DeptName
                            ,UserID,UserName,StaDoc,Remark)
                        VALUES 
                        ( '{BillNo}','{radDropDownList_Dpt.SelectedValue}','{dptName[1]}'
                            ,'{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','1','{radTextBox_Description.Text.Replace(",", "")}')");

                for (int i = 0; i < dt_data.Rows.Count; i++)
                {
                    strSql.Add($@"
                    INSERT INTO SHOP_MNRL_DT 
                        ( DocNo,SeqNo,ItemName,Qty,WHOIN,REMARK)
                    VALUES  ( '{BillNo}','{i + 1}','{dt_data.Rows[i]["ITEMNAME"]}','{double.Parse(dt_data.Rows[i]["QTY"].ToString())}'
                            , '{SystemClass.SystemUserID}', '{dt_data.Rows[i]["REMARK"].ToString().Replace(", ", "")}') ");
                }
                string returnExc = ConnectionClass.ExecuteSQL_ArrayMain(strSql);
                if (returnExc == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation(string.Format($@"บันทึกเรียบ้อย {BillNo}"));
                    radLabel_Docno.Text = BillNo;
                    pSave = "1";
                    pCopy += 1;
                    ClearEnable();
                    PrintData();
                    return;
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                    radTextBox_Description.Focus();
                    return;
                }
            }
        }
        private int CheckDocument(string pItem)
        {
             
            string sql = $@"
                SELECT	SHOP_MNRL_DT.DocNo,SeqNo,ItemName,Qty 
                FROM	SHOP_MNRL_DT WITH (NOLOCK)  
		                INNER JOIN SHOP_MNRL_HD WITH (NOLOCK) ON SHOP_MNRL_DT.DocNo = SHOP_MNRL_HD.DocNo       
                WHERE	ItemName = '{pItem}'
		                AND  StaDoc = '1'
            ";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }

        private void RadTextBox_Emp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(radTextBox_Emp.Text))
                {
                    DataTable dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_Emp.Text);
                    if (dtEmp.Rows.Count > 0)
                    {
                        radTextBox_Emp.Text = dtEmp.Rows[0]["EMPLID"].ToString(); radTextBox_Emp.Enabled = false;
                        radLabel_Name.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                        radButton_Save.Focus();
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานที่ระบุไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                        radTextBox_Emp.SelectAll();
                        radTextBox_Emp.Focus();
                        return;
                    }
                }
            }
        }

        //print
        private void PrintData()
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                pCopyColor = "บิลชมพู"; printDocument1.Print();
                pCopyColor = "บิลฟ้า"; printDocument1.Print();
            }
        }
        //print
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = BillNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            if (pCopy > 1)
            {
                e.Graphics.DrawString("พิมพ์ครั้งที่ " + pCopy.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
                Y += 20;
            }
            e.Graphics.DrawString(pHeadBillPrint + " [" + pCopyColor + "].", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(radDropDownList_Dpt.Text, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                string Remark = radGridView_Show.Rows[i].Cells["REMARK"].Value.ToString();
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ".(" + radGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                  radGridView_Show.Rows[i].Cells["UNIT"].Value.ToString() + ")  " +
                                  radGridView_Show.Rows[i].Cells["ITEMNAME"].Value.ToString(),
                                    SystemClass.printFont, Brushes.Black, 0, Y);
                if (!string.IsNullOrEmpty(Remark))
                {
                    double line = (Remark.Length / 40) + 1;
                    double lth = 0;
                    double StringLength = 40;
                    for (int k = 0; k < line; k++)
                    {
                        if (k == line - 1)
                        {
                            StringLength = Remark.Length - ((line - 1) * 40);
                        }
                        if (k != 0)
                        {
                            lth += 40;
                        }
                        Y += 15;
                        e.Graphics.DrawString(Remark.Substring(int.Parse(lth.ToString()), int.Parse(StringLength.ToString())), SystemClass.printFont, Brushes.Black, 0, Y);
                    }
                }
            }
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("รวมจำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString() + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + "-" + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString(string.Format(@"หมายเหตุ : {0} ", radTextBox_Description.Text), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };

            e.Graphics.DrawString(radTextBox_Remark.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            Y += 30;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        #endregion
    }
}