﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.GeneralForm.ReceiveDocument
{
    public partial class MNRL_Main : Telerik.WinControls.UI.RadForm
    {

        public MNRL_Main()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

        #region ROWSNUMBER

        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        //Export
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            string returnExportStatus = DatagridClass.ExportExcelGridView(this.Text, radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(returnExportStatus);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadDateTimePicker_OrderBegin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_OrderBegin, radDateTimePicker_OrderEnd);
        }

        private void RadDateTimePicker_OrderEnd_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_OrderBegin, radDateTimePicker_OrderEnd);
        }
        #endregion


        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            if (e.RowIndex > -1)
            {

                switch (e.Column.Name)
                {
                    case "BILL_RECIVESTA":
                        CheckDetail(radGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString());
                        break;
                    default:
                        return;
                }
            }

        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            if (e.Column.Name == "BILL_RECIVESTA") return;

            CheckDetail(radGridView_Show.CurrentRow.Cells["DOCNO"].Value.ToString());
        }

        void CheckDetail(string docno)
        {
            string sql = $@"
                SELECT	SHOP_MNRL_DT.DocNo AS DOCNO,SeqNo,
		                ItemName AS ITEMNAME,Qty AS QTY,SHOP_MNRL_DT.REMARK AS REMARK,
		                SHOP_MNRL_HD.UserID, SHOP_MNRL_HD.UserName, DeptID, DeptName, SHOP_MNRL_HD.Remark AS REMARKDOC,
		                SHOP_MNRL_HD.Bill_ReciveSta, SHOP_MNRL_HD.Bill_ReciveWhoIn, SHOP_MNRL_HD.Bill_ReciveWhoName, SHOP_MNRL_HD.Bill_ReciveDate, SHOP_MNRL_HD.Bill_ReciveDate
                FROM	SHOP_MNRL_DT WITH(NOLOCK)
		                INNER JOIN  SHOP_MNRL_HD WITH(NOLOCK) ON SHOP_MNRL_DT.DocNo = SHOP_MNRL_HD.DocNo
                WHERE	SHOP_MNRL_DT.DocNo = '{docno}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลรายละเอียดบิล{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง.");
                return;
            }
            MNRL_New frm = new MNRL_New(dt);
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                FindData("");
            }
        }


        #region Events 

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void MNRL_Main_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.RowHeight = 35;
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("BILL_RECIVESTA", "รับ", 50));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("STADESC", "สถานะ", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATECREATE", "วันที่", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTID", "แผนก", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DEPTNAME", "ชื่อแผนก", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 170));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USERNAME", "สร้าง", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BILL_RECIVEWHONAME", "ผู้รับ", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BILL_RECIVEDATE", "วันที่รับ", 170));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOUPD", "ผู้บันทึกรับ", 200));

            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มข้อมูล";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            radButton_Find.ButtonElement.ShowBorder = true; radButton_Find.ButtonElement.ToolTipText = "ค้นหา";

            RadButton_Search.ButtonElement.Font = SystemClass.SetFontGernaral_Bold; RadButton_Search.ButtonElement.ShowBorder = true;
            radCheckBox_DocNo.ButtonElement.Font = SystemClass.SetFontGernaral_Bold; radCheckBox_DocNo.Checked = false;
            radTextBox_DocNo.Font = SystemClass.SetFontGernaral;
            radTextBox_DocNo.ForeColor = Color.Blue;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderBegin, DateTime.Now.AddDays(-30), DateTime.Now.Date);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_OrderEnd, DateTime.Today, DateTime.Now.Date);

            this.GetGridViewSummary();

            radStatusStrip1.SizingGrip = false;

            FindData("");
        }
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            MNRL_New frm_New = new MNRL_New(dt);
            frm_New.ShowDialog(this);
            FindData("");
        }


        private void RadCheckBox_DocNo_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_DocNo.Checked == true)
            {
                radTextBox_DocNo.Text = "";
                radTextBox_DocNo.Enabled = true;
                radTextBox_DocNo.Focus();
            }
            else
            {
                radTextBox_DocNo.Text = "";
                radTextBox_DocNo.Enabled = false;
            }
        }
        #endregion
        #region Methode
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("DOCNO", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }

        void FindData(string docno)
        {
            this.Cursor = Cursors.WaitCursor;
            string conditionDocno = "";
            if (docno != "") conditionDocno = $@" AND ItemName LIKE '%{docno}%' ";

            string sql = $@"
                SELECT	SHOP_MNRL_HD.DocNo AS DOCNO,CONVERT(VARCHAR,[Date],23) as DATECREATE,
		                DeptID AS DEPTID,DeptName AS DEPTNAME,
		                UserID AS USERID,UserName AS USERNAME,StaDoc AS STADOC,StaPrcDoc,
		                SHOP_MNRL_HD.Remark AS REMARK,CASE WHEN Bill_ReciveSta = '1' THEN WhoUp ELSE '' END AS WHOUPD,
		                CONVERT(VARCHAR,[DateUp],23) AS DATEUPD,Bill_ReciveSta AS BILL_RECIVESTA,Bill_ReciveWhoIn AS BILL_RECIVEWHOIN,Bill_ReciveWhoName AS BILL_RECIVEWHONAME,
		                CONVERT(VARCHAR,[Bill_ReciveDate],25) As BILL_RECIVEDATE ,'เปิด' AS  DETAIL, 
		                CASE Bill_ReciveSta WHEN '0' THEN 'ค้างรับ' ELSE 'รับแล้ว' END AS STADESC

                FROM	SHOP_MNRL_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNRL_DT WITH (NOLOCK)  ON SHOP_MNRL_DT.DocNo = SHOP_MNRL_HD.DocNo 

                WHERE	StaDoc = '1'
		                AND convert(varchar,[Date],23) BETWEEN '{radDateTimePicker_OrderBegin.Value:yyyy-MM-dd}' AND '{radDateTimePicker_OrderEnd.Value:yyyy-MM-dd}'
		                {conditionDocno}

                GROUP BY SHOP_MNRL_HD.DocNo,[Date],DeptID,DeptName,
		                UserID,UserName,StaDoc,StaPrcDoc,
		                SHOP_MNRL_HD.Remark,CASE WHEN Bill_ReciveSta = '1' THEN WhoUp ELSE '' END,
		                DateUp,Bill_ReciveSta,Bill_ReciveWhoIn,Bill_ReciveWhoName,Bill_ReciveDate
                ORDER BY  SHOP_MNRL_HD.DocNo  DESC ";
            radGridView_Show.DataSource = ConnectionClass.SelectSQL_Main(sql);
            this.Cursor = Cursors.Default;
        }

        #endregion
        //FindData
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            string docno = "";
            if (radCheckBox_DocNo.Checked == true)
            {
                if (radTextBox_DocNo.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุเลขที่เอกสารก่อนการค้นหา");
                    return;
                }
                docno = radTextBox_DocNo.Text.Trim();
            }
            FindData(docno);
        }


    }
}