﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;


namespace PC_Shop24Hrs.GeneralForm.MNPI
{
    public partial class MNPI_ReciveInvent : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        DataTable dtDGV = new DataTable();
        readonly DataTable dt19 = new DataTable();

        readonly string _pTypeID;
        readonly string _pTypeDesc;
        readonly string _pPermission;//0 สาขา 1 ผู้ที่จะตกลงได้
        readonly string sta_TRANSFERSTATUS = "2"; // สถานะ 0 สร้างแล้ว / 1 จัดส่ง / 2 รับแล้ว
        string pApv;
        string pEditEmp;

        public MNPI_ReciveInvent(string pTypeID, string pTypeDesc, string pPermission)
        {
            InitializeComponent();

            _pTypeID = pTypeID;
            _pTypeDesc = pTypeDesc;
            _pPermission = pPermission;
        }
        //Load Main
        private void MNPI_ReciveInvent_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "รับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVENT", "คลังสินค้า", 100));

            RadGridView_Show.Columns["QTY"].FormatString = "{0:#,##0.00}";

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000") dtBch = BranchClass.GetBranchAll("'1'", "'1'"); else dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            dt19.Columns.Add("ITEMBARCODE");
            dt19.Columns.Add("SPC_ITEMNAME");
            dt19.Columns.Add("STA");
            dt19.Columns.Add("QTY");
            dt19.Columns.Add("UNITID");
            dt19.Columns.Add("ITEMID");
            dt19.Columns.Add("INVENTDIMID");
            dt19.Columns.Add("INVENT");

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มข้อมูล";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Save.ButtonElement.ToolTipText = "อนุมัติเอกสาร";
            RadButton_Find.ButtonElement.ShowBorder = true; RadButton_Find.ButtonElement.ToolTipText = "โหลดข้อมูล";
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไข พขร + เด็กท้าย";

            ClearData();

            if (_pTypeID == "18")
            {
                radLabel_Barcode.Visible = false;
                radTextBox_Barcode.Visible = false;

                radLabel_Driver.Visible = true;
                radTextBox_EmpDriver.Visible = true;
                radLabel_Emp.Visible = true;
                radTextBox_Emp.Visible = true;

                if (SystemClass.SystemBranchID != "MN000")
                {
                    Set_DGV18();
                }
            }
            else
            {
                radLabel_Barcode.Visible = true;
                radTextBox_Barcode.Visible = true;
                radLabel_Driver.Visible = false;
                radTextBox_EmpDriver.Visible = false;
                radLabel_Emp.Visible = false;
                radTextBox_Emp.Visible = false;

                radButtonElement_Edit.Enabled = false;
                //commandBarSeparator2.Visibility = ElementVisibility.Hidden;
                commandBarSeparator2.Enabled = false;
                RadButton_Find.Visible = false;
            }

            RadButton_Cancel.Visible = false;
            if (_pPermission == "0")
            {
                radLabel_F2.Text = "Click >> ช่องรับ-ระบุจำนวนรับ | Double Click ช่องจำนวน-แก้ไขจำนวนรับ | ต้องการยกเลิกบิลให้ติดต่อแผนกดูแลบิลขาเข้า";
            }
            else
            {
                radLabel_F2.Text = "Click >> ช่องรับ-ระบุจำนวนรับ | Double Click ช่องจำนวน-แก้ไขจำนวนรับ | กดปุ่ม ยกเลิก >> ยกเลิกเอกสารทั้งบิล";
            }
        }

        //Set DGV
        void Set_DGV18()
        {
            this.Cursor = Cursors.WaitCursor;

            //string str = string.Format(@"
            //SELECT	ITEMID,INVENTDIMID,INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME,'0.00' AS QTY,UNITID,'0' AS STA,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC AS INVENT
            //FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
            //  INNER JOIN SHOP_CONFIGBRANCH_DETAIL  WITH (NOLOCK) ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = SHOP_CONFIGBRANCH_DETAIL.SHOW_ID
            //  INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = INVENTITEMBARCODE.ITEMBARCODE 
            //   AND INVENTITEMBARCODE.DATAAREAID = N'SPC' AND SPC_ITEMACTIVE = '1'
            //WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '" + _pTypeID + @"'
            //  AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
            //  AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
            //  AND BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"'
            //ORDER BY SPC_ITEMNAME
            //");

            dtDGV = ItembarcodeClass.MNPI_FindItem(_pTypeID, RadDropDownList_Branch.SelectedValue.ToString()); //ConnectionClass.SelectSQL_Main(str);
            RadGridView_Show.DataSource = dtDGV;
            dtDGV.AcceptChanges();

            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก"; radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red(); pApv = "0";
            RadButton_Save.Enabled = true;
            RadDropDownList_Branch.Enabled = false;

            radTextBox_EmpDriver.Focus();

            this.Cursor = Cursors.Default;
        }
        //Clear
        void ClearData()
        {

            if (dtDGV.Rows.Count > 0) { dtDGV.Rows.Clear(); dtDGV.AcceptChanges(); }
            if (dt19.Rows.Count > 0) { dt19.Rows.Clear(); dt19.AcceptChanges(); }

            radTextBox_Barcode.Text = ""; radTextBox_Barcode.Enabled = true;
            radTextBox_EmpDriver.Enabled = true; radTextBox_Emp.Enabled = true;
            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = "";
            radLabel_EmpDriverName.Text = ""; radLabel_EmpName.Text = "";
            radTextBox_Emp.Text = ""; radTextBox_EmpDriver.Text = "";
            pApv = "0";
            pEditEmp = "0";
            radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = Color.Transparent;

            RadDropDownList_Branch.Enabled = true;
            RadButton_Save.Text = "อนุมัติ";
            //RadButton_Save.Enabled = false;
            radButtonElement_Edit.Enabled = false;
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();

            RadDropDownList_Branch.SelectedIndex = 0;
        }
        //Set choose Data
        void SetDataSTA()
        {
            if (_pTypeID == "18")
            {
                DataRow[] drSTA = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                string pSta = "0"; double pQty = 0;

                if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
                {
                    InputData frmSTA = new InputData("0",
                                           RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                           "จำนวนที่รับ", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = "1" };
                    if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                    {
                        pSta = "1";
                        pQty = Convert.ToDouble(frmSTA.pInputData);
                    }
                }
                else
                {
                    pSta = "0";
                    pQty = 0;
                }

                dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
                dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
                dtDGV.AcceptChanges();
            }
            else
            {
                DataRow[] drSTA = dt19.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                string pSta = "0"; double pQty = 0;

                if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
                {
                    InputData frmSTA = new InputData("0",
                                           RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                           "จำนวนที่รับ", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = "1" };
                    if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                    {
                        pSta = "1";
                        pQty = Convert.ToDouble(frmSTA.pInputData);
                    }
                }
                else
                {
                    pSta = "0";
                    pQty = 0;
                }

                dt19.Rows[dt19.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
                dt19.Rows[dt19.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
                dt19.AcceptChanges();
            }
        }
        //choose
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) { return; }
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
                    {
                        return;
                    }

                    if (_pTypeID == "18")
                    {
                        DataRow[] drQTY = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                        InputData frmQTY = new InputData("0",
                                                  RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                                  "จำนวนที่ได้รับ", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                        { pInputData = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() };
                        if (frmQTY.ShowDialog(this) == DialogResult.Yes)
                        {
                            string pSta = "1"; ;
                            if (Convert.ToDouble(frmQTY.pInputData) == 0) pSta = "0";

                            double pQty = Convert.ToDouble(frmQTY.pInputData);


                            dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["STA"] = pSta;
                            dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["QTY"] = pQty.ToString("#,##0.00");
                            dtDGV.AcceptChanges();
                        }
                    }
                    else
                    {
                        DataRow[] drQTY = dt19.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                        InputData frmQTY = new InputData("0",
                                                  RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                                  "จำนวนที่ได้รับ", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                        { pInputData = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() };
                        if (frmQTY.ShowDialog(this) == DialogResult.Yes)
                        {
                            string pSta = "1"; ;
                            if (Convert.ToDouble(frmQTY.pInputData) == 0) pSta = "0";

                            double pQty = Convert.ToDouble(frmQTY.pInputData);


                            dt19.Rows[dt19.Rows.IndexOf(drQTY[0])]["STA"] = pSta;
                            dt19.Rows[dt19.Rows.IndexOf(drQTY[0])]["QTY"] = pQty.ToString("#,##0.00");
                            dt19.AcceptChanges();
                        }
                    }
                    break;
                default:
                    return;
            }
        }
        //choose
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) return;
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                default:
                    return;
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            switch (pApv)
            {
                case "0":
                    int iCountRow = 0;
                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                    {
                        if (Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) > 0)
                        { iCountRow += 1; }
                    }
                    if (iCountRow == 0)
                    {
                        MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ไม่มีข้อมูลการรับสินค้า");
                        return;
                    }

                    //CheckEMP
                    if (_pTypeID == "18")
                    {
                        if (radLabel_EmpDriverName.Text == "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลพนักงานขับรถให้เรียบร้อยก่อนบันทึก" + Environment.NewLine + "ลองใหม่อีกครั้ง.");
                            radTextBox_EmpDriver.Focus();
                            return;
                        }

                        if (radLabel_EmpName.Text == "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลพนักงานเด็กท้ายให้เรียบร้อยก่อนบันทึก" + Environment.NewLine + "ลองใหม่อีกครั้ง.");
                            radTextBox_Emp.Focus();
                            return;
                        }

                    }

                    string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNPI", "-", "MNPI", "1");
                    string bchID = RadDropDownList_Branch.SelectedValue.ToString();
                    string bchName = BranchClass.GetBranchNameByID(RadDropDownList_Branch.SelectedValue.ToString());

                    ArrayList sqlIn24 = new ArrayList
                        {
                            string.Format(@"
                                INSERT INTO [SHOP_MNPI_HD] ( 
                                [DOCNO],[WHOINS],[WHOINSNAME],[BRANCH_ID],[BRANCH_NAME],
                                [EMPLPICK],[EMPLPICKNAME],[EMPLCHECKER],[EMPLCHECKERNAME],[MACHINENAME],[STAPRCDOC],[TYPE_CONFIG]) 
                                VALUES ('" + maxDocno + @"','" + SystemClass.SystemUserID_M + @"','" + SystemClass.SystemUserName+ @"',
                                '" + bchID + @"', '" + bchName + @"',
                                '" + radTextBox_EmpDriver.Text + @"', '" +radLabel_EmpDriverName.Text + @"',
                                '" + radTextBox_Emp.Text + @"','" + radLabel_EmpName.Text + @"','" + SystemClass.SystemPcName + @"','1','" +_pTypeID + @"') ")
                        };

                    ArrayList sqlInAX = new ArrayList
                        {
                            AX_SendData.Save_POSTOTABLE20_HD(
                                sta_TRANSFERSTATUS,RadDropDownList_Branch.SelectedValue.ToString(),RadGridView_Show.Rows[0].Cells["INVENT"].Value.ToString(),
                                maxDocno,radTextBox_EmpDriver.Text,radTextBox_Emp.Text)
                            //string.Format(@"
                            //    INSERT INTO [SPC_POSTOTABLE20] ( 
                            //    FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                            //    INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,EMPLPICK,EMPLCHECKER)
                            //    VALUES ('98','" + SystemClass.SystemUserID_M  + @"','0','SPC','1','1','" + sta_TRANSFERSTATUS + @"',
                            //    '" + RadDropDownList_Branch.SelectedValue.ToString() + @"','" + RadGridView_Show.Rows[0].Cells["INVENT"].Value.ToString() + @"',
                            //    '" + maxDocno + @"',CONVERT(VARCHAR,GETDATE(),23),'" + maxDocno + @"',
                            //    '" + radTextBox_EmpDriver.Text  + @"','" + radTextBox_Emp.Text + @"' ) ")
                        };

                    int iRow = 1;
                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                    {
                        if (Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) > 0)
                        {
                            sqlIn24.Add(string.Format(@"
                            INSERT INTO [SHOP_MNPI_DT] ( 
                            [DOCNO],[LINENUM],[ITEMBARCODE],[SPC_NAME],[STA] ,[QTYORDER],[UNITID],[ITEMID],[INVENTDIMID],[INVENT]) 
                            VALUES ('" + maxDocno + @"','" + (iRow) + @"',
                            '" + RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                            '" + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                            '" + RadGridView_Show.Rows[i].Cells["STA"].Value.ToString() + @"','" + Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                            '" + RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                            '" + RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                            '" + RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                            '" + RadGridView_Show.Rows[i].Cells["INVENT"].Value.ToString() + @"') "));

                            sqlInAX.Add(AX_SendData.Save_POSTOTABLE20_DT(maxDocno, iRow, RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString(),
                                RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString(), RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                                RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                                Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()), RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString()));

                            //sqlInAX.Add(string.Format(@"
                            //INSERT INTO [SPC_POSTOLINE20] (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,
                            //ITEMID,NAME,SALESUNIT,ITEMBARCODE,INVENTDIMID,INVENTTRANSID,RECVERSION)  
                            //VALUES ('1','0','SPC','" + maxDocno + @"',CONVERT(VARCHAR,GETDATE(),23),'" + (iRow) + @"',
                            //'" + Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                            //'" + RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                            //'" + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                            //'" + RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                            //'" + RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                            //'" + RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                            //'" + maxDocno + "_" + (iRow) + @"','1')
                            //"));
                            iRow++;
                        }
                    }

                    String T = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn24, sqlInAX);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        radLabel_Docno.Text = maxDocno;
                        radLabel_StatusBill.Text = "สถานะบิล : อนุมัติแล้ว"; radLabel_StatusBill.ForeColor = Color.Black;
                        pApv = "1";
                        PrintData();
                        radTextBox_Barcode.Enabled = false;
                        radButtonElement_Edit.Enabled = true;
                    }
                    break;
                case "1":
                    PrintData();
                    break;
                default:
                    break;
            }

        }

        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNPI_FindData frm = new MNPI_FindData(_pTypeID, _pTypeDesc);
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                SetDGV_Load(frm.pDocno);
            }
        }
        void SetDGV_Load(string pDocno)
        {
            string sqlSelect = string.Format(@"
                SELECT	SHOP_MNPI_HD.DocNo AS DOCNO,
		                SHOP_MNPI_HD.BRANCH_ID AS BRANCH_ID,
		                SHOP_MNPI_HD.BRANCH_NAME AS BRANCH_NAME,STADOC AS STADOC,STAPRCDOC AS STAPRCDOC,
		                ITEMBARCODE AS ITEMBARCODE,SPC_NAME AS SPC_ITEMNAME,STA AS STA,QTYORDER AS QTY,UNITID AS UNITID,
                        EMPLPICK,EMPLPICKNAME,EMPLCHECKER,EMPLCHECKERNAME,CASE INVENT WHEN '0' THEN 'RETAILAREA' ELSE INVENT END AS INVENT,ITEMID,INVENTDIMID
                FROM	SHOP_MNPI_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNPI_DT WITH (NOLOCK) ON SHOP_MNPI_HD.DocNo = SHOP_MNPI_DT.DocNo
                WHERE	SHOP_MNPI_HD.DocNo = '" + pDocno + @"'
                ORDER BY LINENUM
            ");

            dtDGV = ConnectionClass.SelectSQL_Main(sqlSelect);

            RadGridView_Show.DataSource = dtDGV;
            dtDGV.AcceptChanges();


            radLabel_Docno.Text = dtDGV.Rows[0]["DOCNO"].ToString();

            if (_pTypeID == "18")
            {
                radTextBox_Emp.Text = dtDGV.Rows[0]["EMPLCHECKER"].ToString(); radTextBox_Emp.Enabled = false;
                radLabel_EmpName.Text = dtDGV.Rows[0]["EMPLCHECKERNAME"].ToString();

                radTextBox_EmpDriver.Text = dtDGV.Rows[0]["EMPLPICK"].ToString(); radTextBox_EmpDriver.Enabled = false;
                radLabel_EmpDriverName.Text = dtDGV.Rows[0]["EMPLPICKNAME"].ToString();
            }

            RadDropDownList_Branch.SelectedValue = dtDGV.Rows[0]["BRANCH_ID"].ToString();
            RadDropDownList_Branch.Enabled = false;
            // check อนุมัติ

            switch (dtDGV.Rows[0]["STADOC"].ToString())
            {
                case "1":
                    if (dtDGV.Rows[0]["STAPRCDOC"].ToString() == "1")
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : อนุมัติแล้ว"; radLabel_StatusBill.ForeColor = Color.Black;
                        radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                        pApv = "1"; RadButton_Save.Text = "พิมพ์ซ้ำ";

                        if (_pTypeID == "18") radButtonElement_Edit.Enabled = true; else radButtonElement_Edit.Enabled = false;


                        radTextBox_Barcode.Enabled = false;
                        if (_pPermission == "1")
                        {
                            RadButton_Cancel.Visible = true;
                        }
                    }
                    else
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน";
                        radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                        pApv = "0"; RadButton_Save.Text = "อนุมัติ"; radTextBox_Barcode.Enabled = true;
                    }
                    break;
                case "0":
                    radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก";
                    radLabel_StatusBill.ForeColor = Color.Red; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                    pApv = "3";
                    RadButton_Save.Text = "พิมพ์ซ้ำ";
                    RadButton_Save.Enabled = false;
                    radTextBox_Barcode.Enabled = false;
                    RadButton_Cancel.Visible = false;
                    radButtonElement_Edit.Enabled = false;
                    break;
                default:
                    break;
            }


        }
        //CheckEmp
        private void RadTextBox_EmpDriver_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_EmpDriver.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ข้อมูลพนักงานขับรถ");
                    radTextBox_EmpDriver.Focus();
                    return;
                }

                //DataTable dtEmp = EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_EmpDriver.Text);
                DataTable dtEmp = CheckEpm(radTextBox_EmpDriver.Text, "45");

                if (dtEmp.Rows.Count > 0)
                {
                    radLabel_EmpDriverName.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                    radTextBox_EmpDriver.Enabled = false;
                    radTextBox_Emp.Focus();
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบพนักงานขับรถตามรหัสที่ระบุ{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้งหรือติดต่อโรงน้ำแข็งเพื่อสอบถามข้อมูลเพิ่มเติม");
                    radLabel_EmpDriverName.Text = "";
                    radTextBox_EmpDriver.Text = "";
                    radTextBox_EmpDriver.Focus();
                }
            }
        }
        //Enter
        private void RadTextBox_Emp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Emp.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ข้อมูลพนักงานเด็กท้าย");
                    radTextBox_Emp.Focus();
                    return;
                }

                //DataTable dtEmp = EmplClass.GetEmployeeDetail_ByEmplID(radTextBox_Emp.Text);
                DataTable dtEmp = CheckEpm(radTextBox_Emp.Text, "46");
                if (dtEmp.Rows.Count > 0)
                {
                    radLabel_EmpName.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                    radTextBox_Emp.Enabled = false;
                    RadButton_Save.Focus();
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบพนักงานท้ายรถตามรหัสที่ระบุ{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้งหรือติดต่อโรงน้ำแข็งเพื่อสอบถามข้อมูลเพิ่มเติม");
                    radLabel_EmpName.Text = "";
                    radTextBox_Emp.Text = "";
                    radTextBox_Emp.Focus();
                }
            }

        }
        //Check Input Number
        private void RadTextBox_EmpDriver_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Check Input Number
        private void RadTextBox_Emp_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            if (SystemClass.SystemBranchID == "MN000")
            {
                System.Data.DataTable dt = BranchClass.GetDetailBranchByID(RadDropDownList_Branch.SelectedValue.ToString());
                if (dt.Rows[0]["BRANCH_OUTPHUKET"].ToString() == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("เมนูนี้เปิดใช้งานเฉพาะสาขาภายในจังหวัดภูเก็ตเท่านั้น");
                    return;
                }
                else
                {
                    Set_DGV18();
                }
            }
            else
            {
                Set_DGV18();
            }

        }

        //print
        void PrintData()
        {
            if (_pTypeID == "18")
            {
                if (pEditEmp == "1")
                {
                    //CheckEMP
                    if (radLabel_EmpDriverName.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลพนักงานขับรถให้เรียบร้อยก่อนแก้ไข" + Environment.NewLine + "ลองใหม่อีกครั้ง.");
                        radTextBox_EmpDriver.Focus();
                        return;
                    }

                    if (radLabel_EmpName.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลพนักงานเด็กท้ายให้เรียบร้อยก่อนแก้ไข" + Environment.NewLine + "ลองใหม่อีกครั้ง.");
                        radTextBox_Emp.Focus();
                        return;
                    }

                    string sqlUp = string.Format(@"
                    UPDATE  SHOP_MNPI_HD 
                    SET     [EMPLPICK] = '" + radTextBox_EmpDriver.Text + @"',[EMPLPICKNAME] = '" + radLabel_EmpDriverName.Text + @"',
                            [EMPLCHECKER] = '" + radTextBox_Emp.Text + @"',[EMPLCHECKERNAME] = '" + radLabel_EmpName.Text + @"',
                            [WHOUPD] = '" + SystemClass.SystemUserID + @"',[WHOUPDNAME] = '" + SystemClass.SystemUserName + @"',[DATEUPD] = CONVERT(VARCHAR,GETDATE(),25)
                    WHERE   DOCNO = '" + radLabel_Docno.Text + @"'
                ");

                    string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                    if (T != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        return;
                    }


                }
            }

            PrintDialog printDlg = new PrintDialog();
            if (printDlg.ShowDialog(this) == DialogResult.OK)
            {
                PrintDocument.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDlg.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument.PrinterSettings = printDlg.PrinterSettings;
                PrintDocument.Print();
                RadButton_Save.Text = "พิมพ์ซ้ำ";
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string pHead = "บิลรับน้ำแข็ง [โรงน้ำแข็ง SUPC].";

            if (_pTypeID != "18")
            {
                pHead = "บิลรับอาหารกล่องประจำวัน.";
            }

            barcode.Data = radLabel_Docno.Text;
            Bitmap barcodeInBitmap0 = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            string bch = RadDropDownList_Branch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(RadDropDownList_Branch.SelectedValue.ToString());

            Y += 20;
            e.Graphics.DrawString(pHead, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(bch + "-" + bchName, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap0, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                if (Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) > 0)
                {
                    Y += 20;
                    e.Graphics.DrawString((i + 1).ToString() + ".(" + RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                      RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + ")   " +
                                      RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                         SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString(" " + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                        SystemClass.printFont, Brushes.Black, 0, Y);
                }
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            if (_pTypeID == "18")
            {
                Y += 15;
                e.Graphics.DrawString("พขร. " + radTextBox_EmpDriver.Text + "-" + radLabel_EmpDriverName.Text, SystemClass.printFont, Brushes.Black, 10, Y);
                Y += 20;
                e.Graphics.DrawString("เด็กท้าย. " + radTextBox_Emp.Text + "-" + radLabel_EmpName.Text, SystemClass.printFont, Brushes.Black, 10, Y);
            }

            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + " " + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        //EditEmp
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            //case 18
            RadButton_Save.Text = "แก้ไข";

            pEditEmp = "1";
            radTextBox_Emp.Enabled = true; radTextBox_EmpDriver.Enabled = true;
            radLabel_EmpDriverName.Text = "";
            radLabel_EmpName.Text = "";
            radTextBox_EmpDriver.Focus();
        }
        //branch Select
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_pTypeID == "19") radTextBox_Barcode.Focus(); else RadButton_Find.Focus();
        }

        //Enter
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Barcode.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("บาร์โค้ด");
                    radTextBox_Barcode.Focus();
                    return;
                }

                this.Cursor = Cursors.WaitCursor;

                string sql = string.Format(@" CashDesktop_ReciveFood '0', 
                    '" + radTextBox_Barcode.Text + @"','" + RadDropDownList_Branch.SelectedValue.ToString() + @"' ");
                DataTable dt = ConnectionClass.SelectSQL_Main(sql);
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_Barcode.Text = "";
                    radTextBox_Barcode.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }
                //check ซ้ำในรายการ
                DataRow[] drCheck = dt19.Select("ITEMBARCODE = '" + dt.Rows[0]["ITEMBARCODE"].ToString() + "'");
                if (drCheck.Length > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้า " + dt.Rows[0]["ITEMBARCODE"].ToString() + "-" + dt.Rows[0]["SPC_ITEMNAME"].ToString() +
                        Environment.NewLine + "มีรายการอยู่แล้ว ให้แก้ไขจำนวนแทนการเพิ่มใหม่");
                    radTextBox_Barcode.Text = "";
                    radTextBox_Barcode.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }

                InputData _input = new InputData("0", dt.Rows[0]["ITEMBARCODE"].ToString() + "-" + dt.Rows[0]["SPC_ITEMNAME"].ToString(),
                    "จำนวนที่ได้รับ", dt.Rows[0]["UNITID"].ToString());
                if (_input.ShowDialog(this) == DialogResult.Yes)
                {
                    if (Double.Parse(_input.pInputData) > 10000)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"จำนวนที่ระบุมากเกินไป{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง");
                        radTextBox_Barcode.Text = "";
                        radTextBox_Barcode.Focus();
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    dt19.Rows.Add(
                        dt.Rows[0]["ITEMBARCODE"].ToString(),
                        dt.Rows[0]["SPC_ITEMNAME"].ToString(), "1",
                        _input.pInputData,
                        dt.Rows[0]["UNITID"].ToString(),
                        dt.Rows[0]["ITEMID"].ToString(),
                        dt.Rows[0]["INVENTDIMID"].ToString(), "RETAILAREA"
                        );

                    RadGridView_Show.DataSource = dt19;
                    dt19.AcceptChanges();

                    if (dt19.Rows.Count == 1)
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก"; radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red(); pApv = "0";
                        RadButton_Save.Enabled = true;
                        RadDropDownList_Branch.Enabled = false;
                    }

                    radTextBox_Barcode.Text = "";
                    radTextBox_Barcode.Focus();
                }
                else
                {

                    radTextBox_Barcode.Text = "";
                    radTextBox_Barcode.Focus();
                }

                this.Cursor = Cursors.Default;

            }
        }
        //คู่มือ
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pTypeDesc + _pPermission);
        }
        //ยกเลิกทั้งบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") { return; }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกข้อมูลทั้งหมด ทุกรายการ" + Environment.NewLine + @"เลขที่ " + radLabel_Docno.Text +
                Environment.NewLine + "ไม่สามารถแก้ไขรายการได้อีก.") == DialogResult.No)
            { return; }

            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNPI", "-", "MNPI", "1");

            ArrayList sqlIn24 = new ArrayList
                        {
                            string.Format(@"
                                UPDATE  [SHOP_MNPI_HD] 
                                SET     REMARK = REMARK +  ' " + maxDocno + @"',STADOC='0',
                                        DATEUPD = GETDATE(),WHOUPD='" + SystemClass.SystemUserID + @"',WHOUPDNAME='" + SystemClass.SystemUserName + @"'
                                WHERE   DOCNO = '" + radLabel_Docno.Text + @"'
                            ")
                        };

            ArrayList sqlInAX = new ArrayList
                        {
                            //string.Format(@"
                            //    INSERT INTO [SPC_POSTOTABLE20] ( 
                            //    FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                            //    INVENTLOCATIONIDFROM, INVENTLOCATIONIDTO, TRANSFERID, SHIPDATE, VOUCHERID,EMPLPICK,EMPLCHECKER)
                            //    VALUES ('98','" + SystemClass.SystemUserID_M  + @"','0','SPC','1','1','" + sta_TRANSFERSTATUS + @"',
                            //    '" + RadDropDownList_Branch.SelectedValue.ToString() + @"','" + RadGridView_Show.Rows[0].Cells["INVENT"].Value.ToString() + @"',
                            //    '" + maxDocno + @"',CONVERT(VARCHAR,GETDATE(),23),'" + maxDocno + @"',
                            //    '" + radTextBox_EmpDriver.Text  + @"','" + radTextBox_Emp.Text + @"' ) ")
                             
                                AX_SendData.Save_POSTOTABLE20_HD(
                                sta_TRANSFERSTATUS,RadGridView_Show.Rows[0].Cells["INVENT"].Value.ToString(),RadDropDownList_Branch.SelectedValue.ToString(),
                                maxDocno,radTextBox_EmpDriver.Text,radTextBox_Emp.Text)

                        };

            int iRow = 1;
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                if (Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) > 0)
                {
                    //sqlInAX.Add(string.Format(@"
                    //        INSERT INTO [SPC_POSTOLINE20] (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,
                    //        ITEMID,NAME,SALESUNIT,ITEMBARCODE,INVENTDIMID,INVENTTRANSID,RECVERSION)  
                    //        VALUES ('1','0','SPC','" + maxDocno + @"',CONVERT(VARCHAR,GETDATE(),23),'" + (iRow) + @"',
                    //        '" + Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                    //        '" + RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                    //        '" + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                    //        '" + RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                    //        '" + RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                    //        '" + RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                    //        '" + maxDocno + "_" + (iRow) + @"','1')
                    //    "));

                    sqlInAX.Add(AX_SendData.Save_POSTOTABLE20_DT(maxDocno, iRow, RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString(),
                            RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString(), RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                            RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                            Convert.ToDouble(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()), RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString()));

                    iRow++;
                }
            }

            String T = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn24, sqlInAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ยกเลิก"; radLabel_StatusBill.ForeColor = Color.Red;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel();
                pApv = "3";
                radTextBox_Barcode.Enabled = false;
                radButtonElement_Edit.Enabled = false;
                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }
        }
        //ค้นหา พนักงาน เด็กท้าย 46 / ขับรถ 45

        DataTable CheckEpm(string emplID, string type)
        {
            string sql = $@"
            SELECT	[SHOW_ID],[SHOW_NAME] AS SPC_NAME,[SHOW_DESC],[REMARK]
            FROM	[SHOP_CONFIGBRANCH_GenaralDetail] WITH (NOLOCK)
            WHERE	[TYPE_CONFIG] = '{type}'
		            AND STA = '1'  AND STAUSE = '1'
		            AND SHOW_ID LIKE '%{emplID}'
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

    }
}


