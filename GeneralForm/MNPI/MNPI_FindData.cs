﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNPI
{
    public partial class MNPI_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();

        readonly string _pTypeID;
        readonly string _pTypeDesc;

        public string pDocno;

        public MNPI_FindData(string pTypeID, string pTypeDesc)
        {
            InitializeComponent();
            _pTypeID = pTypeID;
            _pTypeDesc = pTypeDesc;
        }
        //Load
        private void MNPI_FindData_Load(object sender, EventArgs e)
        {
            this.Text = "ข้อมูลการรับ " + _pTypeDesc;
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-2), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAPRCDOC", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDUPD", "ผู้ยืนยัน/ยกเลิก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "หมายเหตุ", 150)));

            if (_pTypeID == "18")
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLPICKNAME", "พนักงานขับรถ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLCHECKERNAME", "พนักงานท้ายรถ", 200)));
            }
            RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;
            //RadGridView_ShowHD.Columns["STADOC"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENT", "คลังเบิก", 130)));

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000")
            {
                RadCheckBox_Branch.Checked = false;
                dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                RadCheckBox_Branch.Enabled = true;
                radCheckBox_Apv.Checked = false;

            }
            else
            {
                RadCheckBox_Branch.Checked = true;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Enabled = false;
                radCheckBox_Apv.Checked = false;

            }

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-2);
            radDateTimePicker_End.Value = DateTime.Now;
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }
            string sqlSelect = string.Format(@"
                SELECT	DOCNO AS DOCNO,CONVERT(VARCHAR,DATE,23) AS DOCDATE,
		                BRANCH_ID AS BRANCH_ID,BRANCH_NAME AS BRANCH_NAME,WHOINS+' ' + WHOINSNAME AS WHOIDINS,
		                CASE WHEN STADOC = '0' THEN '1' ELSE '0' END AS STADOC,STAPRCDOC AS STAPRCDOC,
		                ISNULL(MACHINENAME,'') AS RMK,
		                CASE STAPRCDOC WHEN '1' THEN WHOINS + ' '+ WHOINSNAME 
			                ELSE CASE WHEN STADOC = '0' THEN WHOUPD + ' '+ WHOUPDNAME  ELSE '' END  END AS WHOIDUPD,
                        EMPLPICK+' ' + EMPLPICKNAME AS EMPLPICKNAME,EMPLCHECKER+' '+ EMPLCHECKERNAME AS EMPLCHECKERNAME
                FROM	SHOP_MNPI_HD WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,DATE,23) BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
                        AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + @"'   
                        AND TYPE_CONFIG = '" + _pTypeID + @"'
            ");

            if (RadCheckBox_Branch.Checked == true)
            {
                sqlSelect += " AND BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"' ";
            }
            if (radCheckBox_Apv.Checked == true)
            {
                sqlSelect += " AND STADOC = '1'  AND STAPRCDOC = '0'  ";
            }
            sqlSelect += " ORDER BY DOCNO DESC ";

            dtDataHD = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            string sqlSelect = string.Format(@"
                SELECT	ITEMBARCODE AS ITEMBARCODE,SPC_NAME AS SPC_ITEMNAME,QTYORDER AS QtyOrder,UNITID AS UNITID,INVENT
                FROM	SHOP_MNPI_DT WITH (NOLOCK)
                WHERE	DOCNO = '" + _pDocno + @"'
                        AND STA = '1'
                ORDER BY LINENUM
                   ");
            dtDataDT = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() == "")) { return; }

            if (RadGridView_ShowHD.Rows.Count == 0) { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pTypeDesc);
        }
    }
}
