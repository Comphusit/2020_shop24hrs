﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNTF
{
    public partial class MNTF_Order : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dt_PO = new DataTable();
        string pApv;

        readonly string _pTypeOrderOpen;// เปน Desc เลย
        public MNTF_Order(string pTypeOrderOpen)
        {
            InitializeComponent();

            _pTypeOrderOpen = pTypeOrderOpen;
        }
        //Load Main
        private void MNTF_Order_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดเอกสารใหม่";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadButton_Apv.ButtonElement.ShowBorder = true; RadButton_Apv.ButtonElement.ToolTipText = "ยืนยันการสั่ง ";
            RadButton_Cancel.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ToolTipText = "ยกเลิกการสั่ง ";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Lock);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_BchFrom);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_BchTO);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 500));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("Price", "ราคา"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Purchase", "แผนกจัดซื้อ", 200));

            RadGridView_Show.MasterTemplate.EnableFiltering = false;

            DatagridClass.SetCellBackClolorByExpression("QtyOrder", "QtyOrder = '0.00' ", ConfigClass.SetColor_Red(), RadGridView_Show);

            dt_PO.Columns.Add("ITEMID");
            dt_PO.Columns.Add("INVENTDIMID");
            dt_PO.Columns.Add("ITEMBARCODE");
            dt_PO.Columns.Add("SPC_ITEMNAME");
            dt_PO.Columns.Add("Price");
            dt_PO.Columns.Add("QtyOrder");
            dt_PO.Columns.Add("UNITID");
            dt_PO.Columns.Add("QTY");
            dt_PO.Columns.Add("Purchase");

            ClearData();
        }

        void Set_DropDown()
        {
            DataTable dtWhTO = Models.InventClass.Find_InventSupc("");
            radDropDownList_BchTO.DataSource = dtWhTO;
            radDropDownList_BchTO.DisplayMember = "INVENTLOCATIONNAME";
            radDropDownList_BchTO.ValueMember = "INVENTLOCATIONID";
            radDropDownList_BchTO.SelectedValue = "RETAILAREA";

            DataTable dtWh = Models.InventClass.Find_InventSupc("");
            radDropDownList_BchFrom.DataSource = dtWh;
            radDropDownList_BchFrom.DisplayMember = "INVENTLOCATIONNAME";
            radDropDownList_BchFrom.ValueMember = "INVENTLOCATIONID";
            radDropDownList_BchFrom.SelectedValue = "WH-A";

            RadDropDownList_Lock.DataSource = POClass.GetLock_SUPC();
            RadDropDownList_Lock.DisplayMember = "CODE_TXT";
            RadDropDownList_Lock.ValueMember = "CODE";
        }

        //Clear
        void ClearData()
        {
            if (dt_PO.Rows.Count > 0) { dt_PO.Rows.Clear(); dt_PO.AcceptChanges(); }

            Set_DropDown();
            radLabel_Docno.Text = "";
            pApv = "0";

            RadDropDownList_Lock.Enabled = true;

            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
            radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red();
            radLabel_StatusBill.BackColor = Color.Transparent;
            radLabel_StatusBill.Text = "";
            RadDropDownList_Lock.Enabled = true;
            radDropDownList_BchTO.Enabled = true;
            radDropDownList_BchFrom.Enabled = true;

            radTextBox_Barcode.Enabled = true;
            RadButton_Cancel.Enabled = false;
            RadButton_Apv.Enabled = false;
            radTextBox_Barcode.Focus();
        }

        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
            Set_DropDown();
        }
        //Get MNTF ForSendAX
        DataTable GetDataDetailMNTF_ForSendAX(string pDocno)
        {
            string sql = $@"
            select	MNTFSeqNo AS LineNum,SHOP_MNTF_HD.MNTFDOCNO AS DOCNO,MNTFWHSoure AS WHSoure,MNTFWHDestination AS WHDestination,MNTFLock AS Lock,
		            MNTFItemID AS ITEMID,MNTFDimid AS INVENTDIM,MNTFBarcode AS ITEMBARCODE,MNTFName AS SPC_ITEMNAME,
		            MNTFQtyOrder AS QtyOrder,MNTFUnitID AS UnitID,MNTFFactor AS QTY,MNTFUserCode,   
		            MNTFPrice AS PRICE,MNTFSPCNAME AS RMK,MNTFUSERCODE AS WHOINS,CONVERT(varchar,SHOP_MNTF_HD.MNTFDate,23) AS DateIns,
                    CASE SHOP_MNTF_HD.MNTFDepart WHEN 'D101' THEN '1' ELSE '0' END AS STA_DPT
   
            FROM	SHOP_MNTF_HD with (nolock)  
		            INNER JOIN SHOP_MNTF_DT with (NOLOCK)  ON SHOP_MNTF_HD.MNTFDOCNO = SHOP_MNTF_DT.MNTFDOCNO    
		
            WHERE	MNTFQtyOrder != '0' 
		            AND SHOP_MNTF_HD.MNTFDOCNO = '{pDocno}'   

            ORDER BY MNTFSeqNo   ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ยืนยัน
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            string lockList = "";
            try
            {
                lockList = RadDropDownList_Lock.SelectedValue.ToString();
            }
            catch (Exception) { }
            string sqlUpWH = string.Format(@" 
                            UPDATE  SHOP_MNTF_HD  
                            SET     MNTFWHSoure = '" + radDropDownList_BchFrom.SelectedValue.ToString() + @"',
                                    MNTFWHDestination = '" + radDropDownList_BchTO.SelectedValue.ToString() + @"',
                                    MNTFLock = '" + lockList + @"'
                            WHERE   MNTFDocNo = '" + radLabel_Docno.Text + @"'  ");
            string T_UpWH = ConnectionClass.ExecuteSQL_Main(sqlUpWH);
            if (T_UpWH == "")
            {
                if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยืนยันการสั่ง") == DialogResult.No) { return; }

                DataTable dtForSend = GetDataDetailMNTF_ForSendAX(radLabel_Docno.Text);
                if (dtForSend.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการสั่งสินค้า " + radLabel_Docno.Text);
                    return;
                }
                ArrayList sqlUp = new ArrayList
                {
                    string.Format(@" 
                                UPDATE  SHOP_MNTF_HD  
                                SET     MNTFStaPrcDoc = '1',MNTFStaApvDoc = '1',
                                        MNTFDateApv = CONVERT(VARCHAR,GETDATE(),25),
                                        MNTFWhoApv = '" + SystemClass.SystemUserID + @"',MNTFWhoApvName = '" + SystemClass.SystemUserName + @"' 
                                WHERE   MNTFDocNo = '" + radLabel_Docno.Text + @"'  ")
                };


                string T = AX_SendData.Save_POSTOTABLE20(sqlUp, dtForSend, "0", dtForSend.Rows[0]["STA_DPT"].ToString());
                MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยืนยันการสั่ง");

                if (T == "")
                {
                    radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel();
                    pApv = "1";
                    radTextBox_Barcode.Enabled = false; RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                    radDropDownList_BchFrom.Enabled = false; radDropDownList_BchTO.Enabled = false; RadDropDownList_Lock.Enabled = false;
                    return;
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T_UpWH);
                return;
            }


        }
        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยกเลิกการสั่ง") == DialogResult.No) { return; }

            string sqlCancle = string.Format(@"
                            UPDATE  SHOP_MNTF_HD 
                            SET     MNTFStaDoc = '0',
                                    MNTFDateUp = CONVERT(VARCHAR,GETDATE(),25),
                                    MNTFWhoUp = '" + SystemClass.SystemUserID + @"',MNTFWhoUpName = '" + SystemClass.SystemUserName + @"' 
                            WHERE   MNTFDocNo = '" + radLabel_Docno.Text + @"'  ");
            String T = ConnectionClass.ExecuteSQL_Main(sqlCancle);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยกเลิกการสั่ง");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                radTextBox_Barcode.Enabled = false; RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
            }
        }
        //Find Data
        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNTF_FindData _mnpo_FindData = new MNTF_FindData("0");
            if (_mnpo_FindData.ShowDialog(this) == DialogResult.Yes)
            {
                SetDGV_Load(_mnpo_FindData.pDocno);
            }
        }
        //set Data Find
        void SetDGV_Load(string pDocno)
        {
            string sqlSelect = string.Format(@"
                SELECT	SHOP_MNTF_HD.MNTFDocNo,MNTFWHSoure,MNTFWHDestination,MNTFLock,
                        MNTFStaDoc AS STADOC,MNTFStaPrcDoc AS STAPRCDOC,MNTFStaApvDoc AS ApvDoc,
		                MNTFSeqNo,MNTFItemID AS ITEMID,MNTFDimid AS INVENTDIMID,MNTFBarcode AS ITEMBARCODE,
		                MNTFName AS SPC_ITEMNAME,FORMAT(CONVERT(float,ISNULL(MNTFQtyOrder,'0')), 'N') AS QtyOrder,
                        MNTFUnitID AS UNITID,MNTFFactor AS QTY,MNTFPurchase AS Purchase,FORMAT(CONVERT(float,ISNULL(MNTFPrice,'0')), 'N') AS MNTFPrice
                FROM	SHOP_MNTF_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNTF_DT WITH (NOLOCK) ON SHOP_MNTF_HD.MNTFDocNo = SHOP_MNTF_DT.MNTFDocNo
                WHERE	SHOP_MNTF_HD.MNTFDocNo = '" + pDocno + @"'
                ORDER BY MNTFSeqNo");
            DataTable dt = ConnectionClass.SelectSQL_Main(sqlSelect);
            if (dt_PO.Rows.Count > 0) { dt_PO.Rows.Clear(); }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt_PO.Rows.Add(
                    dt.Rows[i]["ITEMID"].ToString(), dt.Rows[i]["INVENTDIMID"].ToString(),
                    dt.Rows[i]["ITEMBARCODE"].ToString(), dt.Rows[i]["SPC_ITEMNAME"].ToString(),
                    dt.Rows[i]["MNTFPrice"].ToString(),
                    dt.Rows[i]["QtyOrder"].ToString(),
                    dt.Rows[i]["UNITID"].ToString(),
                    string.Format("{0:0.00}", Convert.ToDouble(dt.Rows[i]["QTY"].ToString())),
                    dt.Rows[i]["Purchase"].ToString());
            }

            RadGridView_Show.DataSource = dt_PO;
            dt_PO.AcceptChanges();

            Set_DropDown();
            RadDropDownList_Lock.SelectedValue = dt.Rows[0]["MNTFLock"].ToString(); RadDropDownList_Lock.Enabled = false;
            radDropDownList_BchTO.SelectedValue = dt.Rows[0]["MNTFWHDestination"].ToString(); RadDropDownList_Lock.Enabled = false;
            radDropDownList_BchFrom.SelectedValue = dt.Rows[0]["MNTFWHSoure"].ToString(); RadDropDownList_Lock.Enabled = false;
            radLabel_Docno.Text = dt.Rows[0]["MNTFDocNo"].ToString();

            switch (dt.Rows[0]["STADOC"].ToString())
            {
                case "1":
                    if (dt.Rows[0]["STAPRCDOC"].ToString() == "1")
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                        RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                        radTextBox_Barcode.Enabled = false; radDropDownList_BchTO.Enabled = false; radDropDownList_BchFrom.Enabled = false; RadDropDownList_Lock.Enabled = false;
                    }
                    else
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel(); pApv = "0";
                        RadButton_Apv.Enabled = true; RadButton_Cancel.Enabled = true;
                        radTextBox_Barcode.Enabled = true;
                        radDropDownList_BchTO.Enabled = true; radDropDownList_BchFrom.Enabled = true; RadDropDownList_Lock.Enabled = true;
                    }
                    break;
                case "0":
                    radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                    RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false; radDropDownList_BchTO.Enabled = false; radDropDownList_BchFrom.Enabled = false; RadDropDownList_Lock.Enabled = false;
                    radTextBox_Barcode.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        //Enter + F4 In Itembarcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radDropDownList_BchFrom.SelectedValue.ToString() == radDropDownList_BchTO.SelectedValue.ToString())
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("คลังต้นทางและคลังปลายทางไม่สามารถเลือกเหมือนกันได้" + Environment.NewLine + @"ลองใหม่อีกครั้ง.");
                    return;
                }

                Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(radTextBox_Barcode.Text.Trim());
                if (!(dtBarcode.GetItembarcodeStatus))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
                SetDataInputBarcode(radTextBox_Barcode.Text.Trim());
            }

            else if (e.KeyCode == Keys.F4)
            {
                if (radDropDownList_BchFrom.SelectedValue.ToString() == radDropDownList_BchTO.SelectedValue.ToString())
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("คลังต้นทางและคลังปลายทางไม่สามารถเลือกเหมือนกันได้" + Environment.NewLine + @"ลองใหม่อีกครั้ง.");
                    return;
                }

                string pCon;
                if (radTextBox_Barcode.Text != "")
                { pCon = "AND SPC_ITEMNAME LIKE '%" + radTextBox_Barcode.Text.Replace(" ", "%") + @"%' "; }
                else { pCon = ""; }

                ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(pCon);
                if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                {
                    Data_ITEMBARCODE dtBarcode = ShowDataDGV_Itembarcode.items;
                    SetDataInputBarcode(dtBarcode.Itembarcode_ITEMBARCODE);
                }
                else
                {
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
            }
        }
        //Choose Barcode And Input QtyOrder
        void SetDataInputBarcode(string pBarcode)
        {

            Itembarcode.ItembarcodeDetail _itembarcodeDetail = new Itembarcode.ItembarcodeDetail
                           (pBarcode, radDropDownList_BchFrom.SelectedValue.ToString(), "0", "0", pBarcode);
            if (_itembarcodeDetail.ShowDialog(this) == DialogResult.Yes)
            {
                //check ว่า Barcode ที่เลือกอยู่ใน DGV แล้วหรือยัง
                if (RadGridView_Show.Rows.Count > 0)
                {
                    //ITEMBARCODE
                    DataRow[] dr = dt_PO.Select("ITEMID = '" + _itembarcodeDetail.sID + "' AND INVENTDIMID = '" + _itembarcodeDetail.sDim + "'  ");
                    if (dr.Length > 0)
                    {
                        foreach (DataRow item in dr)
                        {
                            if (double.Parse(item["QtyOrder"].ToString()) > 0)
                            {
                                MessageBox.Show("รายการที่เลือก มีอยู่ในบิลเรียบร้อยแล้ว ไม่สามารถเพิ่มรายการได้ " + Environment.NewLine +
                                                                                    "[ให้แก้ไขจำนวนการสั่งใหม่ แทนการเพิ่มรายการ]", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                }
                //ระบุจำนวน
                FormShare.InputData _inputdata = new FormShare.InputData("0",
                _itembarcodeDetail.sBarcode + "-" + _itembarcodeDetail.sSpc_Name, "จำนวนที่ต้องการสั่ง", _itembarcodeDetail.sUnitID)
                { pInputData = "1.00" };
                if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                {
                    if (dt_PO.Rows.Count == 0)
                    {
                        SaveDataHD(_itembarcodeDetail.sID, _itembarcodeDetail.sDim, _itembarcodeDetail.sBarcode,
                        _itembarcodeDetail.sSpc_Name, Convert.ToDouble(_inputdata.pInputData),
                        _itembarcodeDetail.sUnitID, Convert.ToDouble(_itembarcodeDetail.sFactor),
                        _itembarcodeDetail.sPurchase, _itembarcodeDetail.sPrice);
                    }
                    else
                    {
                        SaveDataDT(_itembarcodeDetail.sID, _itembarcodeDetail.sDim, _itembarcodeDetail.sBarcode,
                         _itembarcodeDetail.sSpc_Name, Convert.ToDouble(_inputdata.pInputData),
                         _itembarcodeDetail.sUnitID, Convert.ToDouble(_itembarcodeDetail.sFactor),
                         _itembarcodeDetail.sPurchase, _itembarcodeDetail.sPrice);
                    }
                }
                else
                {
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
            }
            else
            {
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
            }
        }
        //Check Values In DGV IS NULL + ยืนยันหรือยัง
        string CheckValuesInDGV()
        {
            string rr = "1";
            switch (pApv)
            {
                case "0":
                    if ((RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "") ||
                           (RadGridView_Show.CurrentRow.Index == -1) ||
                           (RadGridView_Show.Rows.Count == 0))
                    {
                        rr = "1";//กรณีไม่มีค่า
                    }
                    else
                    {
                        rr = "0";//กรณีที่มีค่าให้ทำต่อได้
                    }
                    break;
                case "1":
                    rr = "1";
                    break;
                case "3":
                    rr = "1";
                    break;
                default:
                    break;
            }
            return rr;
        }
        //CellDoubleClick
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QtyOrder"://แก้ไขจำนวนสั่ง
                    if (CheckValuesInDGV() == "1")
                    {
                        return;
                    }

                    FormShare.InputData _inputdata = new FormShare.InputData("0",
                         RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                         "จำนวนที่ต้องการสั่ง", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    {
                        pInputData = String.Format("{0:0.00}", Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString()))
                    };

                    if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                    {
                        //ในกรณีที่บันทึกแล้ว
                        String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                                UPDATE  SHOP_MNTF_DT
                                SET     MNTFQtyOrder = '" + _inputdata.pInputData + @"',MNTFDateUp = CONVERT(VARCHAR,GETDATE(),25),
                                        MNTFWhoUp = '" + SystemClass.SystemUserID + @"',MNTFWhoUpName = '" + SystemClass.SystemUserName + @"' 
                                WHERE   MNTFDocNo = '" + radLabel_Docno.Text + @"' 
                                        AND MNTFBarcode = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ")
                        );
                        if (T != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(T);
                            return;
                        }

                        RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value = String.Format("{0:0.00}", Convert.ToDouble(_inputdata.pInputData));
                        dt_PO.AcceptChanges();
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                    }
                    else
                    {
                        radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                    }
                    break;
                default:
                    break;
            }
        }
        //Rows and Font
        private void RadGridView_Show_ViewCellFormatting_1(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        //กรณีที่ต้องการยเลิกการสั่งทั้งรายการ
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    if (CheckValuesInDGV() == "1")
                    {
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete("ไม่สั่งรายการ " +
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" +
                        RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString() +
                         " จำนวน " + RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString() +
                         "   " + RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                        == DialogResult.No)
                    {
                        return;
                    }

                    String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                            UPDATE  SHOP_MNTF_DT 
                            SET     MNTFQtyOrder = '0',MNTFDateUp = CONVERT(VARCHAR,GETDATE(),25),
                                    MNTFWhoUp = '" + SystemClass.SystemUserID + @"',MNTFWhoUpName = '" + SystemClass.SystemUserName + @"' 
                            WHERE   MNTFDocNo = '" + radLabel_Docno.Text + @"' 
                                    AND MNTFBarcode = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ")
                                    );
                    if (T != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        return;
                    }


                    RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value = String.Format("{0:0.00}", 0);
                    dt_PO.AcceptChanges();
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;
                default:
                    break;
            }
        }
        //Save Data HD
        void SaveDataHD(string sID, string sDim, string sBarcode, string sSpc_Name,
            double pInputData, string sUnitID, double sFactor, string sPurchase, double sPrice)
        {

            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNTF", "-", "MNTF", "1");

            ArrayList sqlIn = new ArrayList
            {
                String.Format(@"
                    INSERT INTO SHOP_MNTF_HD 
	                    ([MNTFDocNo],[MNTFUserCode],[MNTFUserCodeName],[MNTFTypeOrder],[MNTFWHSoure],[MNTFWHDestination]
	                    ,[MNTFLock],[MNTFSPCNAME],[MNTFDepart],MNTFDepartName )
                    VALUES ('" + maxDocno + @"','"+SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','6',
                    '" + radDropDownList_BchFrom.SelectedValue.ToString()  + @"','" +  radDropDownList_BchTO.SelectedValue.ToString()  + @"',
                    '"+ RadDropDownList_Lock.SelectedValue.ToString() + @"',
                    '" + SystemClass.SystemPcName + @"','" + SystemClass.SystemDptID + @"','" + SystemClass.SystemDptName + "') ")
            };


            sqlIn.Add(String.Format(@"
                    INSERT INTO SHOP_MNTF_DT 
                        ([MNTFDocNo],[MNTFSeqNo],[MNTFItemID],[MNTFDimid]
                       ,[MNTFBarcode],[MNTFName],[MNTFQtyOrder],[MNTFUnitID],[MNTFPrice]
                       ,[MNTFFactor],[MNTFWhoIn],MNTFWhoInName,MNTFPurchase ) 
                    VALUES ('" + maxDocno + @"','1',
                       '" + sID + @"',
                       '" + sDim + @"',
                       '" + sBarcode + @"',
                       '" + sSpc_Name.Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                       '" + Convert.ToDouble(pInputData) + @"',
                       '" + sUnitID + @"',
                       '" + Convert.ToDouble(sPrice) + @"',
                       '" + Convert.ToDouble(sFactor) + @"',
                       '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                       '" + sPurchase + @"') "));

            String T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                pApv = "0";// pSave = "1";
                RadButton_Apv.Enabled = true;
                RadButton_Cancel.Enabled = true;

                dt_PO.Rows.Add(sID, sDim, sBarcode,
                       sSpc_Name,
                       String.Format("{0:0.00}", Convert.ToDouble(sPrice)),
                       String.Format("{0:0.00}", Convert.ToDouble(pInputData)),
                       sUnitID,
                       String.Format("{0:0.00}", Convert.ToDouble(sFactor)),
                       sPurchase);
                RadGridView_Show.DataSource = dt_PO;
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //Save Data DT
        void SaveDataDT(string sID, string sDim, string sBarcode, string sSpc_Name,
            double pInputData, string sUnitID, double sFactor, string sPurchase, double sPrice)
        {
            String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                   INSERT INTO SHOP_MNTF_DT 
                        ([MNTFDocNo],[MNTFSeqNo],[MNTFItemID],[MNTFDimid]
                       ,[MNTFBarcode],[MNTFName],[MNTFQtyOrder],[MNTFUnitID],[MNTFPrice]
                       ,[MNTFFactor],[MNTFWhoIn],MNTFWhoInName,MNTFPurchase ) 
                    VALUES ('" + radLabel_Docno.Text + @"','" + (RadGridView_Show.Rows.Count + 1) + @"',
                       '" + sID + @"',
                       '" + sDim + @"',
                       '" + sBarcode + @"',
                       '" + sSpc_Name.Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                       '" + Convert.ToDouble(pInputData) + @"',
                       '" + sUnitID + @"',
                       '" + Convert.ToDouble(sPrice) + @"',
                       '" + Convert.ToDouble(sFactor) + @"',
                       '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                       '" + sPurchase + @"') "));
            if (T == "")
            {
                dt_PO.Rows.Add(sID, sDim, sBarcode,
                      sSpc_Name,
                      String.Format("{0:0.00}", Convert.ToDouble(sPrice)),
                      String.Format("{0:0.00}", Convert.ToDouble(pInputData)),
                      sUnitID,
                      String.Format("{0:0.00}", Convert.ToDouble(sFactor)),
                      sPurchase);
                RadGridView_Show.DataSource = dt_PO;
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //Export Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายการสั่งสินค้า " + radLabel_Docno.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Select Change
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_Barcode.Focus();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}

