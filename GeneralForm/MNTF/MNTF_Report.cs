﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNTF
{
    public partial class MNTF_Report : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeReport;

        //string tblHD;
        //string tblDT;
        //0-รายงานการจำนวนรายการเบิก
        //Load
        public MNTF_Report(string pTypeReport)
        {
            InitializeComponent();

            _pTypeReport = pTypeReport;
        }
        //Load
        private void MNTF_Report_Load(object sender, EventArgs e)
        {
            //tblHD = "SHOP_MNPO_HD";
            //tblDT = "SHOP_MNPO_DT";

            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

            RadButton_Search.ButtonElement.ShowBorder = true;

            radRadioButton_Retail.ButtonElement.Font = SystemClass.SetFontGernaral;
            radRadioButton_MN098.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radDateTimePicker_D2.Visible = true;
            radDateTimePicker_D1.Visible = true;
            radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
            radLabel_Date.Visible = true;

            radLabel_Detail.Visible = true;


            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Text = "สีชมพู >> วันที่มีรายการเบิกสินค้า"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "วันที่ทำรายการ";
                    break;
                default:
                    break;
            }

            ClearTxt();
        }

        ////ค้นหาการทำบิลไป MN _HD
        //DataTable FindRetailHD(string pDate1, string pDate2)
        //{
        //    string sql = string.Format(@"
        //        SELECT	EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME
        //        FROM	SHOP2013TMP.dbo.InventTransferJour   with (nolock) 
        //          INNER JOIN SHOP2013TMP.dbo.InventTransferJourLine  with (nolock) ON InventTransferJour.TRANSFERID = InventTransferJourLine.TRANSFERID   
        //          INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON InventTransferJour.UpdatedBy = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
        //        WHERE	CONVERT (VARCHAR,InventTransferJour.createdDateTime,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"'
        //          AND InventTransferJour.DATAAREAID = N'SPC' AND InventTransferJourLine.DATAAREAID = N'SPC' 
        //          AND InventLocationIdFrom = 'WH-A' AND InventLocationIdTO = 'RETAILAREA' AND DlvTermId = 'DRUG' 
        //          AND UpdateType = '0'  
        //        GROUP BY EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME  
        //        ORDER BY EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME  
        //    ");
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        ////ค้นหาการทำบิลไป MN _DT
        //DataTable FindRetailDT(string pDate1, string pDate2)
        //{
        //    string sql = string.Format(@"
        //        SELECT	CONVERT (VARCHAR,InventTransferJour.createdDateTime,23) AS DateI,EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME,COUNT(SPC_ITEMBARCODE) AS COUNT_LINE 
        //        FROM	SHOP2013TMP.dbo.InventTransferJour   with (nolock) 
        //          INNER JOIN SHOP2013TMP.dbo.InventTransferJourLine  with (nolock) ON InventTransferJour.TRANSFERID = InventTransferJourLine.TRANSFERID   
        //          INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON InventTransferJour.UpdatedBy = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC' 
        //        WHERE	CONVERT (VARCHAR,InventTransferJour.createdDateTime,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"'
        //          AND InventTransferJour.DATAAREAID = N'SPC' AND InventTransferJourLine.DATAAREAID = N'SPC' 
        //          AND InventLocationIdFrom = 'WH-A' AND InventLocationIdTO = 'RETAILAREA' AND DlvTermId = 'DRUG' 
        //          AND UpdateType = '0'  
        //        GROUP BY CONVERT (VARCHAR,InventTransferJour.createdDateTime,23),EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME  
        //    ");
        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //ค้นหาการทำเบิกจากMN _HD
        DataTable FindMN098HD(string pDate1, string pDate2)
        {
            //string sql = string.Format(@"
            //    SELECT	MNPOUserCode AS EMPLID,SPC_NAME
            //    FROM 	SHOP_MNPO_HD WITH (NOLOCK) 
            //      INNER JOIN SHOP_MNPO_DT WITH (NOLOCK) ON SHOP_MNPO_HD.MNPODocNo = SHOP_MNPO_DT.MNPODocNo  
            //      INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNPO_HD.MNPOUserCode = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC' 
            //    WHERE	CONVERT(VARCHAR,MNPODate,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"' 
            //      AND MNPOBranch = 'MN098' 
            //    GROUP BY MNPOUserCode,SPC_NAME
            //    ORDER BY MNPOUserCode,SPC_NAME
            //");
            string sql = $@"
                SELECT	MNPOUserCode AS EMPLID,MNPOUserNameCode AS SPC_NAME
                FROM 	SHOP_MNPO_HD WITH (NOLOCK) 
                WHERE	CONVERT(VARCHAR,MNPODate,23) BETWEEN '{pDate1}' AND '{pDate2}'
		                AND MNPOBranch = 'MN098' 
                GROUP BY MNPOUserCode,MNPOUserNameCode
                ORDER BY MNPOUserCode,MNPOUserNameCode ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาการทำเบิกจากMN _DT
        DataTable FindMN098DT(string pDate1, string pDate2)
        {
            //string sql = string.Format(@"
            //    SELECT	CONVERT(NVARCHAR,MNPODate,23) AS DateI,MNPOUserCode AS EMPLID,SPC_NAME,COUNT(MNPOBarcode) AS COUNT_LINE 
            //    FROM 	SHOP_MNPO_HD WITH (NOLOCK) 
            //      INNER JOIN SHOP_MNPO_DT WITH (NOLOCK) ON SHOP_MNPO_HD.MNPODocNo = SHOP_MNPO_DT.MNPODocNo  
            //      INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_MNPO_HD.MNPOUserCode = EMPLTABLE.EMPLID AND DATAAREAID = N'SPC' 
            //    WHERE	CONVERT(VARCHAR,MNPODate,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"' 
            //      AND MNPOBranch = 'MN098' 
            //    GROUP BY CONVERT(NVARCHAR,MNPODate,23),MNPOUserCode,SPC_NAME  
            //");
            string sql = string.Format(@"
                SELECT	CONVERT(NVARCHAR,MNPODate,23) AS DateI,MNPOUserCode AS EMPLID,MNPOUserNameCode AS SPC_NAME,COUNT(MNPOBarcode) AS COUNT_LINE 
                FROM 	SHOP_MNPO_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNPO_DT WITH (NOLOCK) ON SHOP_MNPO_HD.MNPODocNo = SHOP_MNPO_DT.MNPODocNo  
                WHERE	CONVERT(VARCHAR,MNPODate,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"' 
		                AND MNPOBranch = 'MN098' 
                GROUP BY CONVERT(NVARCHAR,MNPODate,23),MNPOUserCode,MNPOUserNameCode  
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;

            switch (_pTypeReport)
            {
                case "0":

                    this.Cursor = Cursors.WaitCursor;
                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 300)));

                    RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
                    RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

                    double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
                    if (intDay == 0)
                    {
                        intDay = 1;
                    }

                    //เพิ่มคอลัม
                    int i_Column = 0;

                    for (int iDay = 0; iDay < intDay; iDay++)
                    {
                        var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                        string HeadColumnCount = "D" + pDate.Replace("-", "V");

                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, pDate, 120)));

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                           new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 ", false)
                           { CellBackColor = ConfigClass.SetColor_PinkPastel() });

                        i_Column += 1;
                    }

                    //Check กลุ่ม
                    if (i_Column == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการทำรายการเบิก");
                        this.Cursor = Cursors.Default;
                        return;
                    }

                    string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
                    string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

                    DataTable dtEmpHD = new DataTable();
                    DataTable dtEmpDT = new DataTable();
                    if (radRadioButton_MN098.CheckState == CheckState.Checked)
                    {
                        dtEmpHD = FindMN098HD(pDate1, pDate2);
                        dtEmpDT = FindMN098DT(pDate1, pDate2);
                    }
                    if (radRadioButton_Retail.CheckState == CheckState.Checked)
                    {
                        dtEmpHD = POClass.MNTF_FindRetailHD(pDate1, pDate2);
                        dtEmpDT = POClass.MNTF_FindRetailDT(pDate1, pDate2);
                    }

                    for (int iEmp = 0; iEmp < dtEmpHD.Rows.Count; iEmp++)
                    {
                        string empID = dtEmpHD.Rows[iEmp]["EMPLID"].ToString();
                        string empName = dtEmpHD.Rows[iEmp]["SPC_NAME"].ToString();
                        RadGridView_ShowHD.Rows.Add(empID, empName);

                        int iCountColume = 2;
                        for (int iD = 0; iD < intDay; iD++)
                        {
                            var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");
                            DataRow[] dr = dtEmpDT.Select(" EMPLID = '" + empID + @"' AND  DateI = '" + pDate + "' ");
                            int cLine = 0;
                            if (dr.Length > 0)
                            {
                                cLine = int.Parse(dr[0]["COUNT_LINE"].ToString());
                            }

                            RadGridView_ShowHD.Rows[iEmp].Cells[iCountColume].Value = cLine.ToString();
                            iCountColume++;
                        }
                    }

                    break;

                default:
                    break;
            }


            this.Cursor = Cursors.Default;

        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }

            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
