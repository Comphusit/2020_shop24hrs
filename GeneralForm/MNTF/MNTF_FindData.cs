﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNTF
{
    public partial class MNTF_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();

        public string pDocno;

        readonly string _pPermission; //0 = Lock แผนก 1= ทุกแผนก
        public MNTF_FindData(string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load
        private void MNTF_FindData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAPRCDOC", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNTFWHSoure", "ต้นทาง", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNTFWHDestination", "ปลายทาง", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNTFLock", "ล็อก", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNTFDateApv", "วันที่ยืนยัน", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDUPD", "ผู้ยืนยัน", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNTFDepart", "แผนกเปิด", 210)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "หมายเหตุ", 150)));

            RadGridView_ShowHD.Columns["STAPRCDOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["STADOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Purchase", "แผนกจัดซื้อ", 300)));

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_Begin.Value = DateTime.Now;
            radDateTimePicker_End.Value = DateTime.Now;
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            string pCon = "";
            switch (SystemClass.SystemDptID)
            {
                case "D044":
                    pCon = " AND MNTFDepart = 'D044' ";
                    break;
                case "D104":
                    pCon = " AND MNTFDepart = 'D104' ";
                    break;
                case "D101":
                    pCon = " AND MNTFDepart = 'D101' ";
                    break;
                default:
                    break;
            }


            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }
            string sqlSelect = string.Format(@"
                SELECT	MNTFDOCNO AS DOCNO,CONVERT(VARCHAR,MNTFDATE,25) AS DOCDATE,
		                MNTFWHSoure,MNTFWHDestination,MNTFLock,MNTFUserCode+' ' + MNTFUserCodeName AS WHOIDINS,
		                CASE WHEN MNTFStaDoc = '0' THEN '1' ELSE '0' END AS STADOC,MNTFStaPrcDoc AS STAPRCDOC,
		                MNTFSPCNAME  AS RMK,
		                CASE MNTFStaPrcDoc WHEN '1' THEN MNTFWhoApv + ' '+ MNTFWhoApvName 
		                ELSE CASE WHEN MNTFStaDoc = '0' THEN MNTFWhoUp + ' '+ MNTFWhoUpName  ELSE '' END  END 
		                AS WHOIDUPD,MNTFDepart + ' ' + MNTFDepartName AS MNTFDepart ,CONVERT(VARCHAR,MNTFDateApv,25) AS MNTFDateApv 
                FROM	SHOP_MNTF_HD WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,MNTFDATE,23) BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
                        AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + @"'   " + pCon + @"
            ");


            if (radCheckBox_Apv.Checked == true)
            {
                sqlSelect += " AND MNTFStaDoc = '1'  AND MNTFStaPrcDoc = '0' AND MNTFStaApvDoc = '0' ";
            }
            sqlSelect += " ORDER BY MNTFDOCNO DESC ";

            dtDataHD = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            //,FORMAT(CONVERT(float,ISNULL(MNTFPrice,'0')), 'N')  AS PRICE,
            string sqlSelect = string.Format(@"
                SELECT	MNTFBarcode AS ITEMBARCODE,MNTFName AS SPC_ITEMNAME,MNTFQtyOrder AS QtyOrder,
		                MNTFUnitID AS UNITID,MNTFPurchase AS Purchase 
                FROM	SHOP_MNTF_DT WITH (NOLOCK)
                WHERE	MNTFDOCNO = '" + _pDocno + @"'
                ORDER BY MNTFSeqNo
                   ");
            dtDataDT = ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() == "")) return;

            if (RadGridView_ShowHD.Rows.Count == 0) { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}
