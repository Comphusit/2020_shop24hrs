﻿//CheckOK
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.MNPZ
{
    public static class MNPZClass
    {
         
        public static DataTable GetData_RewardMain(string RewardID)
        {
            string sql = $@"
                SELECT  SPC_RewardTable.REWARDID, CONVERT(VARCHAR,SPC_RewardTable.TRANSDATE,23) as TRANSDATE, 
		                ACCOUNTTYPE, ACCOUNTNUM, DIMENSION, DIMENSION2_, DIMENSION3_, 
		                SPC_RewardTable.DESCRIPTION AS DESC_HD, PROJNAME, POSTED, SPC_RewardTable.MODIFIEDDATETIME, SPC_RewardTable.MODIFIEDBY, 
		                SPC_RewardTable.CREATEDDATETIME, SPC_RewardTable.CREATEDBY, 
		                DOCUMENTNUM, DOCUMENTDATE, NUMBERSEQUENCEGROUP, 
		                SPC_RewardTable.TAXINVOICE,INCLTAX, SPC_RewardTable.REWARDGROUPID, TAXWITHHOLD,
		                case RECEIVESTATUS when '3' then 'ได้รับแล้ว' else 'เปิดค้างไว้' end as RECEIVESTATUS, 
		                FROMDATE, TODATE, CAMPAIGNAMOUNTCUR, CAMPAIGNQTY, REBATEID, REMARKS, APOINTMENTJOURNAL, RECEIVEEMPLID ,
		                LINENUM,SPC_RewardLine.DESCRIPTION,DOCUMENTREF, SPC_RewardLine.REWARDGROUPID,AMOUNTCUR,  
		                REWARDLISTID,OffsetAccountType,OFFSETACCOUNTNUM,  
		                SPC_RewardLine.TAXINVOICE, QTY,'0.00' as EDIT, ISNULL(ITEMBARCODE,'') as ITEMBARCODE  
                FROM	SPC_RewardTable  WITH (NOLOCK)
		                INNER JOIN SPC_RewardLine  WITH (NOLOCK) ON SPC_RewardTable.RewardId = SPC_RewardLine.REWARDID 
                WHERE  SPC_RewardTable.RewardId = '{RewardID}'
		                AND SPC_RewardTable.DATAAREAID = N'SPC'
		                AND SPC_RewardLine.DATAAREAID = N'SPC'
                ORDER BY LINENUM ";
            return ConnectionClass.SelectSQL_MainAX(sql);
        }
        public static DataTable GetData_RewardSHOP(string RewardID)
        {
            string sql = $@"
                SELECT	SHOP_REWARDTABLE.MNREWARDID, LINENUM, SHOP_REWARDLINE.DESCRIPTION,DOCUMENTREF, SHOP_REWARDLINE.REWARDGROUPID,AMOUNTCUR,  
		                REWARDLISTID,OffsetAccountType,OFFSETACCOUNTNUM,  
		                SHOP_REWARDLINE.TAXINVOICE,  CONVERT(INT,QTY) as QTY,  EDIT, 
		                ISNULL(ITEMBARCODE,'') as ITEMBARCODE  ,REMARK,SHOP_REWARDTABLE.REWARDID
                FROM	SHOP_REWARDTABLE  WITH (NOLOCK) 
		                INNER JOIN  SHOP_REWARDLINE WITH (NOLOCK)  on SHOP_REWARDTABLE.MNREWARDID=SHOP_REWARDLINE.MNREWARDID 
                WHERE  SHOP_REWARDTABLE.REWARDID = '{RewardID}' 
		                and stadoc='1' 
                ORDER BY LINENUM ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static string Save_REWARDTABLE(string docno, string rewordID, string dptID, string rmk)
        {
            string sql = $@" 
                insert into  SHOP_REWARDTABLE
                                (MNREWARDID, REWARDID, DIMENSION, DIMENSIONNAME, 
                                REMARK, WHOIN,WHONAMEIN)
                values ('{docno}','{rewordID}','{dptID}','{Class.Models.DptClass.GetDptName_ByDptID(dptID)}',
                               '{rmk}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}') ";
            return sql;
        }
        public static string Save_REWARDLINE(string docno, string rewordID, int lineNum, string REWARDGROUPID, string REWARDLISTID, string OFFSETACCOUNTTYPE,
                        string OFFSETACCOUNTNUM, double AMOUNTCUR, string DOCUMENTREF, string DESCRIPTION, string TAXINVOICE,
                        double QTY, double EDIT, string ITEMBARCODE)
        {
            string sql = $@"
                insert into SHOP_REWARDLINE 
                        (MNREWARDID, REWARDID, LINENUM, REWARDGROUPID, REWARDLISTID, OFFSETACCOUNTTYPE, 
                        OFFSETACCOUNTNUM, AMOUNTCUR, DOCUMENTREF, DESCRIPTION, TAXINVOICE, 
                        QTY, EDIT, ITEMBARCODE, WHOIN) 
                values ('{docno}','{rewordID}', '{lineNum}', '{REWARDGROUPID}', '{REWARDLISTID}', '{OFFSETACCOUNTTYPE}', 
                        '{OFFSETACCOUNTNUM}', '{AMOUNTCUR}', '{DOCUMENTREF}', '{DESCRIPTION.Replace("'","")}', '{TAXINVOICE}', 
                        '{QTY}', '{EDIT}', '{ITEMBARCODE}', '{SystemClass.SystemUserID}')";
            return sql;
        }
    }
}
