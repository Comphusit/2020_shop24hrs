﻿namespace PC_Shop24Hrs.GeneralForm.MNPZ
{ 
    public partial class MNPZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNPZ));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radCheckBox_TaxWithhold = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Posted = new Telerik.WinControls.UI.RadCheckBox();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radLabel_Number = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Status = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Condition = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Group = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Supp = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_PZ = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Docno = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_YM = new Telerik.WinControls.UI.RadTextBox();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_StatusBill = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_TaxWithhold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Posted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Number)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Condition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Group)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Supp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_YM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StatusBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(2, 48);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(24, 19);
            this.radLabel1.TabIndex = 17;
            this.radLabel1.Text = "PZ";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radCheckBox_TaxWithhold);
            this.panel1.Controls.Add(this.radCheckBox_Posted);
            this.panel1.Controls.Add(this.radTextBox_Remark);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.radLabel_Number);
            this.panel1.Controls.Add(this.radLabel_Status);
            this.panel1.Controls.Add(this.radLabel_Condition);
            this.panel1.Controls.Add(this.radLabel_Group);
            this.panel1.Controls.Add(this.radLabel_Dept);
            this.panel1.Controls.Add(this.radLabel_Supp);
            this.panel1.Controls.Add(this.radLabel_PZ);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.radButton_Cancel);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.Label15);
            this.panel1.Controls.Add(this.Label17);
            this.panel1.Controls.Add(this.Label19);
            this.panel1.Controls.Add(this.Label21);
            this.panel1.Controls.Add(this.Label23);
            this.panel1.Controls.Add(this.Label13);
            this.panel1.Controls.Add(this.Label11);
            this.panel1.Controls.Add(this.Label9);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radTextBox_Docno);
            this.panel1.Controls.Add(this.radTextBox_YM);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 5;
            // 
            // radCheckBox_TaxWithhold
            // 
            this.radCheckBox_TaxWithhold.Location = new System.Drawing.Point(130, 441);
            this.radCheckBox_TaxWithhold.Name = "radCheckBox_TaxWithhold";
            this.radCheckBox_TaxWithhold.Size = new System.Drawing.Size(15, 15);
            this.radCheckBox_TaxWithhold.TabIndex = 174;
            // 
            // radCheckBox_Posted
            // 
            this.radCheckBox_Posted.Location = new System.Drawing.Point(130, 419);
            this.radCheckBox_Posted.Name = "radCheckBox_Posted";
            this.radCheckBox_Posted.Size = new System.Drawing.Size(15, 15);
            this.radCheckBox_Posted.TabIndex = 173;
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(5, 527);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(186, 54);
            this.radTextBox_Remark.TabIndex = 2;
            this.radTextBox_Remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Remark_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(2, 506);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 172;
            this.label1.Text = "หมายเหตุ";
            // 
            // radLabel_Number
            // 
            this.radLabel_Number.AutoSize = false;
            this.radLabel_Number.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Number.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Number.Location = new System.Drawing.Point(5, 483);
            this.radLabel_Number.Name = "radLabel_Number";
            this.radLabel_Number.Size = new System.Drawing.Size(178, 23);
            this.radLabel_Number.TabIndex = 171;
            this.radLabel_Number.Text = "Number";
            // 
            // radLabel_Status
            // 
            this.radLabel_Status.AutoSize = false;
            this.radLabel_Status.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Status.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Status.Location = new System.Drawing.Point(5, 392);
            this.radLabel_Status.Name = "radLabel_Status";
            this.radLabel_Status.Size = new System.Drawing.Size(178, 23);
            this.radLabel_Status.TabIndex = 168;
            this.radLabel_Status.Text = "Status";
            // 
            // radLabel_Condition
            // 
            this.radLabel_Condition.AutoSize = false;
            this.radLabel_Condition.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Condition.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Condition.Location = new System.Drawing.Point(5, 305);
            this.radLabel_Condition.Name = "radLabel_Condition";
            this.radLabel_Condition.Size = new System.Drawing.Size(178, 64);
            this.radLabel_Condition.TabIndex = 170;
            this.radLabel_Condition.Text = "Condition";
            this.radLabel_Condition.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel_Group
            // 
            this.radLabel_Group.AutoSize = false;
            this.radLabel_Group.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Group.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Group.Location = new System.Drawing.Point(5, 256);
            this.radLabel_Group.Name = "radLabel_Group";
            this.radLabel_Group.Size = new System.Drawing.Size(178, 23);
            this.radLabel_Group.TabIndex = 168;
            this.radLabel_Group.Text = "Group";
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.AutoSize = false;
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dept.Location = new System.Drawing.Point(5, 206);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(178, 23);
            this.radLabel_Dept.TabIndex = 169;
            this.radLabel_Dept.Text = "Dept";
            // 
            // radLabel_Supp
            // 
            this.radLabel_Supp.AutoSize = false;
            this.radLabel_Supp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Supp.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Supp.Location = new System.Drawing.Point(5, 156);
            this.radLabel_Supp.Name = "radLabel_Supp";
            this.radLabel_Supp.Size = new System.Drawing.Size(178, 23);
            this.radLabel_Supp.TabIndex = 168;
            this.radLabel_Supp.Text = "Supp";
            // 
            // radLabel_PZ
            // 
            this.radLabel_PZ.AutoSize = false;
            this.radLabel_PZ.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_PZ.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_PZ.Location = new System.Drawing.Point(2, 107);
            this.radLabel_PZ.Name = "radLabel_PZ";
            this.radLabel_PZ.Size = new System.Drawing.Size(187, 23);
            this.radLabel_PZ.TabIndex = 167;
            this.radLabel_PZ.Text = "PZ";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.AutoSize = false;
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Date.Location = new System.Drawing.Point(2, 80);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(187, 23);
            this.radLabel_Date.TabIndex = 166;
            this.radLabel_Date.Text = "Date";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(99, 591);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 4;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(1, 591);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label15.ForeColor = System.Drawing.Color.Black;
            this.Label15.Location = new System.Drawing.Point(2, 465);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(100, 16);
            this.Label15.TabIndex = 160;
            this.Label15.Text = "เลขที่การเข้าพบ";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label17.ForeColor = System.Drawing.Color.Black;
            this.Label17.Location = new System.Drawing.Point(2, 441);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(105, 16);
            this.Label17.TabIndex = 159;
            this.Label17.Text = "หักภาษี ณ ที่จ่าย";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label19.ForeColor = System.Drawing.Color.Black;
            this.Label19.Location = new System.Drawing.Point(2, 418);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(100, 16);
            this.Label19.TabIndex = 158;
            this.Label19.Text = "ลงรายการบัญชี";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label21.ForeColor = System.Drawing.Color.Black;
            this.Label21.Location = new System.Drawing.Point(2, 372);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(99, 16);
            this.Label21.TabIndex = 156;
            this.Label21.Text = "สถานะรับรางวัล";
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label23.ForeColor = System.Drawing.Color.Black;
            this.Label23.Location = new System.Drawing.Point(2, 283);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(98, 16);
            this.Label23.TabIndex = 154;
            this.Label23.Text = "เงื่อนไขรายการ";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label13.ForeColor = System.Drawing.Color.Black;
            this.Label13.Location = new System.Drawing.Point(2, 234);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(94, 16);
            this.Label13.TabIndex = 152;
            this.Label13.Text = "กลุ่มของรางวัล";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label11.ForeColor = System.Drawing.Color.Black;
            this.Label11.Location = new System.Drawing.Point(2, 184);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(43, 16);
            this.Label11.TabIndex = 151;
            this.Label11.Text = "แผนก";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label9.ForeColor = System.Drawing.Color.Black;
            this.Label9.Location = new System.Drawing.Point(2, 134);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(66, 16);
            this.Label9.TabIndex = 149;
            this.Label9.Text = "ผู้จำหน่าย";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(80, 46);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(14, 22);
            this.radLabel3.TabIndex = 19;
            this.radLabel3.Text = "-";
            // 
            // radTextBox_Docno
            // 
            this.radTextBox_Docno.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Docno.Location = new System.Drawing.Point(97, 45);
            this.radTextBox_Docno.MaxLength = 7;
            this.radTextBox_Docno.Name = "radTextBox_Docno";
            this.radTextBox_Docno.Size = new System.Drawing.Size(86, 24);
            this.radTextBox_Docno.TabIndex = 1;
            this.radTextBox_Docno.Text = "1234567";
            this.radTextBox_Docno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Docno_KeyDown);
            // 
            // radTextBox_YM
            // 
            this.radTextBox_YM.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_YM.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_YM.Location = new System.Drawing.Point(30, 45);
            this.radTextBox_YM.MaxLength = 4;
            this.radTextBox_YM.Name = "radTextBox_YM";
            this.radTextBox_YM.Size = new System.Drawing.Size(48, 24);
            this.radTextBox_YM.TabIndex = 0;
            this.radTextBox_YM.Text = "2202";
            this.radTextBox_YM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_YM_KeyDown);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.radButtonElement_Print,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(192, 36);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่มเอกสาร";
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Print
            // 
            this.radButtonElement_Print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_Print.Name = "radButtonElement_Print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Print, false);
            this.radButtonElement_Print.Text = "";
            this.radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร";
            this.radButtonElement_Print.Click += new System.EventHandler(this.RadButtonElement_Print_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radPanel2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.Transparent;
            this.radPanel2.Controls.Add(this.radLabel_F2);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(3, 606);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(617, 19);
            this.radPanel2.TabIndex = 2;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(0, 0);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(617, 19);
            this.radLabel_F2.TabIndex = 51;
            this.radLabel_F2.Text = "<html></html>";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_StatusBill, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Docno, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(617, 24);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // radLabel_StatusBill
            // 
            this.radLabel_StatusBill.AutoSize = false;
            this.radLabel_StatusBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_StatusBill.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_StatusBill.Location = new System.Drawing.Point(205, 4);
            this.radLabel_StatusBill.Name = "radLabel_StatusBill";
            this.radLabel_StatusBill.Size = new System.Drawing.Size(408, 16);
            this.radLabel_StatusBill.TabIndex = 23;
            this.radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(4, 4);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(194, 16);
            this.radLabel_Docno.TabIndex = 21;
            this.radLabel_Docno.Text = "MNPZ200401000000";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 567);
            this.RadGridView_Show.TabIndex = 4;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_Show_CellBeginEdit);
            this.RadGridView_Show.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellEndEdit);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            this.RadGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // MNPZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MNPZ";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ใบรับของรางวัล [PZ]";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MNPZ_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_TaxWithhold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Posted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Number)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Condition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Group)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Supp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_YM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StatusBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel1;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadLabel radLabel_StatusBill;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label9;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Docno;
        private Telerik.WinControls.UI.RadTextBox radTextBox_YM;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadLabel radLabel_Number;
        private Telerik.WinControls.UI.RadLabel radLabel_Status;
        private Telerik.WinControls.UI.RadLabel radLabel_Condition;
        private Telerik.WinControls.UI.RadLabel radLabel_Group;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel_Supp;
        private Telerik.WinControls.UI.RadLabel radLabel_PZ;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        internal System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_TaxWithhold;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Posted;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Print;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
    }
}
 