﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;


namespace PC_Shop24Hrs.GeneralForm.MNPZ
{
    public partial class MNPZ : Telerik.WinControls.UI.RadForm
    {
        //readonly DataTable dt_PO = new DataTable();
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        string DOCNO, MNPZColors;//MNPZDOCNO
        string pSave;


        public MNPZ()
        {
            InitializeComponent();
        }
        //Load Main
        private void MNPZ_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดเอกสารใหม่";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร";

            radButton_Save.ButtonElement.ShowBorder = true; radButton_Cancel.ButtonElement.ShowBorder = true;

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("LINENUM", "ลำดับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จน. ทั้งหมด", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("EDIT", "จน. นำออก", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "คำอธิบาย", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTREF", "เอกสารอ้างอิง", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("REWARDGROUPID", "กลุ่มของรางวัล", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNTCUR", "มูลค่า", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddMaskTextBoxColum_AddManual("REWARDLISTID", "รายการของรางวัล", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("OffsetAccountType", "ชนิดบัญชี"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("OFFSETACCOUNTNUM", "บัญชี", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TAXINVOICE", "รายการมีภาษี"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 120));
            radLabel_F2.Text = $@"ระบุเลขที่เอกสาร PZ Enter | ระบุจำนวนที่ต้องการนำของออก >> บันทึก | ถ้านำออกทั้งบิล กด Insert ที่ข้อมูลหรือช่องหมายเหตุ";
            ClearData("");
        }


        //SetFontInRadGridview
        #region SetFontInRadGridview

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            Controllers.FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData("");
        }
        #endregion

        #region"CLEAR AND FUNCTION"


        //Clear
        void ClearData(string status)
        {
            if (status == "SAVED")
            {
                radCheckBox_Posted.Enabled = false; radCheckBox_TaxWithhold.Enabled = false;
                radButtonElement_Print.Enabled = true;
                radLabel_StatusBill.Text = "สถานะบิล : บันทึก";
                radLabel_StatusBill.ForeColor = Color.Blue;
                radTextBox_Remark.Enabled = false;
                //radLabel_Docno.Text = MNPZDOCNO;
                radButton_Save.Enabled = false;
                radButton_Cancel.Enabled = true;
                radTextBox_YM.Enabled = false;
                radTextBox_Docno.Enabled = false;
                pSave = "0";
            }
            else if (status == "ADD")
            {
                radCheckBox_Posted.Enabled = false; radCheckBox_TaxWithhold.Enabled = false;
                radButtonElement_Print.Enabled = false;
                radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
                radLabel_StatusBill.ForeColor = Color.Red;
                radTextBox_Remark.Enabled = true;
                RadGridView_Show.ReadOnly = false;
                RadGridView_Show.AllowAddNewRow = false;
                radButton_Save.Enabled = true;
                radButton_Cancel.Enabled = true;
                radTextBox_YM.Enabled = false;
                radTextBox_Docno.Enabled = false;
                pSave = "1";
            }
            else
            {
                radCheckBox_Posted.Enabled = false; radCheckBox_TaxWithhold.Enabled = false;

                if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.DataSource = null;

                radButtonElement_Print.Enabled = false;

                radLabel_Docno.Text = string.Empty;

                radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
                radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red();
                radLabel_StatusBill.BackColor = Color.Transparent;
                radLabel_StatusBill.Text = "";

                radTextBox_YM.Text = DateTime.Now.ToString("yyMM");
                radTextBox_YM.Enabled = true;
                radTextBox_Docno.Text = string.Empty;
                radTextBox_Docno.Enabled = true;

                radLabel_Date.Text = string.Empty;
                radLabel_PZ.Text = string.Empty;
                radLabel_Supp.Text = string.Empty;
                radLabel_Dept.Text = string.Empty;
                radLabel_Group.Text = string.Empty;
                radLabel_Condition.Text = string.Empty;
                radLabel_Status.Text = string.Empty;
                radLabel_Number.Text = string.Empty;

                radTextBox_Remark.Text = string.Empty;
                pSave = "0";
                radButton_Save.Enabled = false;
                radTextBox_Docno.Focus();
            }

        }

        #endregion

        #region"FIND PZ"

        private void RadTextBox_Docno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {                
                radTextBox_Docno.Text = radTextBox_Docno.Text.PadLeft(6, '0');
                DOCNO = $@"PZ{ radTextBox_YM.Text}-{radTextBox_Docno.Text}";// GetDocno(radTextBox_YM.Text, radTextBox_Docno.Text);
                DataTable DtPZ_HD = MNPZClass.GetData_RewardMain(DOCNO);
                if (DtPZ_HD.Rows.Count > 0)
                {
                    radLabel_Date.Text = DtPZ_HD.Rows[0]["TransDate"].ToString();
                    radLabel_PZ.Text = DtPZ_HD.Rows[0]["RewardId"].ToString();
                    radLabel_Supp.Text = DtPZ_HD.Rows[0]["AccountNum"].ToString();
                    radLabel_Dept.Text = DtPZ_HD.Rows[0]["Dimension"].ToString();
                    radLabel_Group.Text = DtPZ_HD.Rows[0]["RewardGroupId"].ToString();
                    radLabel_Condition.Text = DtPZ_HD.Rows[0]["DESC_HD"].ToString();
                    radLabel_Status.Text = DtPZ_HD.Rows[0]["ReceiveStatus"].ToString();
                    radLabel_Number.Text = DtPZ_HD.Rows[0]["ApointmentJournal"].ToString();

                    if (DtPZ_HD.Rows[0]["Posted"].ToString() == "1") radCheckBox_Posted.Checked = true;

                    if (DtPZ_HD.Rows[0]["TaxWithhold"].ToString() == "1") radCheckBox_TaxWithhold.Checked = true;

                    DataTable DSHOPtPZ_DT = MNPZClass.GetData_RewardSHOP(DOCNO);

                    if (DSHOPtPZ_DT.Rows.Count > 0)
                    {
                        RadGridView_Show.DataSource = DSHOPtPZ_DT;
                        radLabel_Docno.Text = DSHOPtPZ_DT.Rows[0]["MNREWARDID"].ToString();
                        radTextBox_Remark.Text = DSHOPtPZ_DT.Rows[0]["REMARK"].ToString();
                        ClearData("SAVED");
                    }
                    else
                    {
                        RadGridView_Show.DataSource = DtPZ_HD;
                        ClearData("ADD");
                        radTextBox_Remark.Text = string.Empty;
                        radTextBox_Remark.Focus();
                    }

                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลเอกสาร PZ ที่ต้องการค้นหา");
                    return;
                }
            }
        }
        #endregion
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData("");
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = radLabel_Docno.Text;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            e.Graphics.DrawString("เอกสารใบรับของรางวัล.[" + MNPZColors + @"]", SystemClass.SetFont12, Brushes.Black, 5, Y);
            Y += 30;
            e.Graphics.DrawString(radLabel_Docno.Text, SystemClass.SetFont12, Brushes.Black, 30, Y);
            Y += 35;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 70;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            for (int r = 0; r < RadGridView_Show.RowCount; r++)
            {
                Double EDIT = Double.Parse(RadGridView_Show.Rows[r].Cells["EDIT"].Value.ToString());
                if (EDIT > 0)
                {
                    Y += 15;
                    e.Graphics.DrawString(int.Parse(EDIT.ToString()) + @" " +
                       RadGridView_Show.Rows[r].Cells["DESCRIPTION"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                }
            }

            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("เอกสารอ้างอิง " + DOCNO, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้เบิก " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemDptID + "-" + SystemClass.SystemDptName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 30;
            e.Graphics.DrawString("ผู้รับสินค้า_________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("_______________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

            Y += 20;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 280, 100);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(radTextBox_Remark.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);

        }

        private void RadTextBox_YM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Docno.SelectAll();
                radTextBox_Docno.Focus();
            }

        }
        #region "PRINT"

        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            DialogResult dr = printDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                MNPZColors = "บิลสีชมพู";
                printDocument1.Print();
                MNPZColors = "บิลสีฟ้า";
                printDocument1.Print();
                MNPZColors = "บิลสีเขียว";
                printDocument1.Print();

            }
        }

        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            if (pSave != "1") return;
            if ((e.Column.Name != "EDIT")) e.Cancel = true;
        }

        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            if (pSave != "1") return;
            switch (e.Column.Name)
            {
                case "EDIT":
                    try
                    {
                        if (Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["EDIT"].Value.ToString()) > Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString()))
                            RadGridView_Show.CurrentRow.Cells["EDIT"].Value = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString();
                        else RadGridView_Show.CurrentRow.Cells["EDIT"].Value = RadGridView_Show.CurrentRow.Cells["EDIT"].Value.ToString();
                    }
                    catch (Exception)
                    {
                        RadGridView_Show.CurrentRow.Cells["EDIT"].Value = "0.00";
                        return;
                    }

                    RadGridView_Show.CurrentRow.Cells["EDIT"].Value = double.Parse(RadGridView_Show.CurrentRow.Cells["EDIT"].Value.ToString()).ToString("#,#0.00");
                    break;

                default:
                    break;
            }
        }

        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert) SetQtyAll();
        }

        void SetQtyAll()
        {
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                RadGridView_Show.Rows[i].Cells["EDIT"].Value = double.Parse(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()).ToString("#,#0.00");
            }
        }

        private void RadTextBox_Remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert) SetQtyAll();
        }
        #endregion

        #region "INSERT"
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
             
            double edit = 0;
            for (int r = 0; r < RadGridView_Show.Rows.Count; r++)
            {
                edit += double.Parse(RadGridView_Show.Rows[r].Cells["EDIT"].Value.ToString());
            }
            if (edit == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่มีรายการสินค้าที่ต้องการบันทึก{Environment.NewLine}เช็คจำนวนที่ต้องการนำออกอีกครั้ง");
                return;
            }
            ArrayList sqlIn = new ArrayList();

            string docno = Class.ConfigClass.GetMaxINVOICEID("MNPZ", "-", "MNPZ", "1");
            sqlIn.Add(MNPZClass.Save_REWARDTABLE(docno, DOCNO, radLabel_Dept.Text, radTextBox_Remark.Text));

            for (int i = 0; i < RadGridView_Show.RowCount; i++)
            {
                sqlIn.Add(MNPZClass.Save_REWARDLINE(docno, DOCNO, i+1,
                                        RadGridView_Show.Rows[i].Cells["REWARDGROUPID"].Value.ToString(),
                                        RadGridView_Show.Rows[i].Cells["REWARDLISTID"].Value.ToString(),
                                        RadGridView_Show.Rows[i].Cells["OFFSETACCOUNTTYPE"].Value.ToString(),
                                        RadGridView_Show.Rows[i].Cells["OFFSETACCOUNTNUM"].Value.ToString(),
                                        double.Parse(RadGridView_Show.Rows[i].Cells["AMOUNTCUR"].Value.ToString()),
                                        RadGridView_Show.Rows[i].Cells["DOCUMENTREF"].Value.ToString(),
                                        RadGridView_Show.Rows[i].Cells["DESCRIPTION"].Value.ToString(),
                                        RadGridView_Show.Rows[i].Cells["TAXINVOICE"].Value.ToString(),
                                        double.Parse(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()),
                                        double.Parse(RadGridView_Show.Rows[i].Cells["EDIT"].Value.ToString()),
                                        RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString()));
            }

            string StaInset = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(StaInset);
            if (StaInset == "") { ClearData("SAVED"); radLabel_Docno.Text = docno; }
        }
        #endregion

    }
}

