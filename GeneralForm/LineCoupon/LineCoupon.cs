﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.FormShare.ShowData;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.LineCoupon
{
    public partial class LineCoupon : Telerik.WinControls.UI.RadForm
    {
        Data_ITEMBARCODE dtBarcode;
        DataTable dtDT = new DataTable();
        readonly string _pCouponNumber; // 0 Insert  
        string pCaseCheckControl;
        public LineCoupon(string pCouponNumber)
        {
            InitializeComponent();
            _pCouponNumber = pCouponNumber;
        }
        //Load
        private void LineCoupon_Load(object sender, EventArgs e)
        {

            RadButton_Find.ButtonElement.ShowBorder = true;
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_Save.ButtonElement.ShowBorder = true; radButton_Cancel.ButtonElement.ShowBorder = true;
            radCheckBox_EmpUse.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Control.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            DatagridClass.SetDefaultRadGridView(radGridView_Bch);

            radGridView_Bch.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "เลือก"));
            radGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100)));
            radGridView_Bch.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 180)));

            DatagridClass.SetDefaultRadGridView(radGridView_Barcode);
            radGridView_Barcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
            radGridView_Barcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 200)));
            radGridView_Barcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));
            radGridView_Barcode.MasterTemplate.ShowFilteringRow = false;

            dtDT.Columns.Add("ITEMBARCODE");
            dtDT.Columns.Add("SPC_ITEMNAME");
            dtDT.Columns.Add("UNITID");

            if (_pCouponNumber == "0")
            {
                SetBch();
                ClearTxt();
            }
            else
            {
                SelectCoupon();
            }
        }
        //ClearData
        void ClearTxt()
        {
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now.AddDays(365));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now.AddDays(365));

            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;

            radDateTimePicker_T1.Format = DateTimePickerFormat.Custom;
            radDateTimePicker_T1.Format = DateTimePickerFormat.Time;
            radDateTimePicker_T1.CustomFormat = "HH:mm:ss"; radDateTimePicker_T1.ShowUpDown = true;

            radDateTimePicker_T2.Format = DateTimePickerFormat.Custom;
            radDateTimePicker_T2.Format = DateTimePickerFormat.Time;
            radDateTimePicker_T2.Value = Convert.ToDateTime("23:59:59");
            radDateTimePicker_T2.CustomFormat = "HH:mm:ss"; radDateTimePicker_T2.ShowUpDown = true;


            radioButton_Bill.Checked = true;
            radioButton_Barcode.Checked = false;

            radTextBox_barcode.Text = ""; radTextBox_barcode.Enabled = false;
            RadButton_Find.Enabled = false;

            radTextBox_Grand.Text = "";
            radCheckBox_EmpUse.CheckState = CheckState.Unchecked;

            radTextBox_GrandCoupon.Text = ""; radTextBox_GrandCoupon.Enabled = false;
            radTextBox_MaxCoupon.Text = ""; radTextBox_MaxCoupon.Enabled = false;
            radTextBox_Remaek.Text = ""; radTextBox_Remaek.Enabled = false;
            radTextBox_Control.Text = ""; radTextBox_Control.Enabled = true;
            radCheckBox_Control.Checked = false; radCheckBox_Control.Enabled = true;
            pCaseCheckControl = "0";

            radioButton_AllBch.Checked = true;
            radioButton_Bch.Checked = false;
            radioButton_MN.Checked = false;

            radButton_Save.Enabled = false;

            radLabel_Docno.Text = "";
            radTextBox_GrandCoupon.Focus();

        }
        //Select
        void SelectCoupon()
        {
            string sql = @"
            SELECT	COUPONNUMBER,CONVERT(VARCHAR,DATETIMEBEGIN,23) AS DATETIMEBEGIN,CONVERT(VARCHAR,DATETIMEEND,23) AS DATETIMEEND,
                    CONVERT(VARCHAR,DATETIMEBEGIN,24) AS T1,CONVERT(VARCHAR,DATETIMEEND,24) AS T2,
		            TYPEUSE,TYPEUSE_NAME,BRANCH,BRANCH AS BRANCH_ID,BRANCH_NAME,MAXUSE,MINIMUM_AMOUNT,COUPONVALUE,STA_ACTIVE,REMARK,
		            ALLOW_EMP,'1' AS STA,STA_CONTROL 
            FROM	SHOP_COUPONONLINE WITH (NOLOCK)
            WHERE	COUPONNUMBER = '" + _pCouponNumber + @"'
            ";
            DataTable dt = ConnectionClass.SelectSQL_POSRetail707(sql);

            string sqlDT = @"SELECT [LINENUM],[ITEMBARCODE],[SPC_ITEMNAME],[UNITID] 
            FROM	SHOP_COUPONONLINE_DT WITH (NOLOCK)
            WHERE	COUPONNUMBER = '" + _pCouponNumber + @"'
            ";
            dtDT = ConnectionClass.SelectSQL_POSRetail707(sqlDT);
            radGridView_Barcode.DataSource = dtDT;
            dtDT.AcceptChanges();

            radLabel_Docno.Text = _pCouponNumber;

            radDateTimePicker_D2.Value = Convert.ToDateTime(dt.Rows[0]["DATETIMEEND"].ToString());
            radDateTimePicker_D1.Value = Convert.ToDateTime(dt.Rows[0]["DATETIMEBEGIN"].ToString());


            radDateTimePicker_T1.Format = DateTimePickerFormat.Custom;
            radDateTimePicker_T1.Format = DateTimePickerFormat.Time;
            radDateTimePicker_T1.CustomFormat = "HH:mm:ss"; radDateTimePicker_T1.ShowUpDown = true;
            radDateTimePicker_T1.Value = Convert.ToDateTime(dt.Rows[0]["T1"].ToString());

            radDateTimePicker_T2.Format = DateTimePickerFormat.Custom;
            radDateTimePicker_T2.Format = DateTimePickerFormat.Time;
            radDateTimePicker_T2.CustomFormat = "HH:mm:ss"; radDateTimePicker_T2.ShowUpDown = true;
            radDateTimePicker_T2.Value = Convert.ToDateTime(dt.Rows[0]["T2"].ToString());

            if (dt.Rows[0]["TYPEUSE"].ToString() == "")
            {
                radioButton_Bill.Checked = true;
                radioButton_Barcode.Checked = false;
            }
            else
            {
                radioButton_Bill.Checked = false;
                radioButton_Barcode.Checked = true;
            }
            radioButton_Bill.Enabled = false; radioButton_Barcode.Enabled = false;
            RadButton_Find.Enabled = false;
            radTextBox_barcode.Text = dt.Rows[0]["TYPEUSE"].ToString(); radTextBox_barcode.Enabled = false;
          
            radTextBox_Grand.Text = dt.Rows[0]["MINIMUM_AMOUNT"].ToString();
            if (dt.Rows[0]["ALLOW_EMP"].ToString() == "1") radCheckBox_EmpUse.CheckState = CheckState.Checked; else radCheckBox_EmpUse.CheckState = CheckState.Unchecked;

            radTextBox_GrandCoupon.Text = dt.Rows[0]["COUPONVALUE"].ToString();
            radTextBox_MaxCoupon.Text = dt.Rows[0]["MAXUSE"].ToString();
            radTextBox_Remaek.Text = dt.Rows[0]["REMARK"].ToString();

            if (dt.Rows[0]["STA_CONTROL"].ToString() != "")
            {
                radCheckBox_Control.Checked = true;
                radTextBox_Control.Text = dt.Rows[0]["STA_CONTROL"].ToString();
                radTextBox_Control.Enabled = false; radCheckBox_Control.Enabled = false;
                pCaseCheckControl = "1";
            }
            else
            {
                radCheckBox_Control.Checked = false; radCheckBox_Control.Enabled = true;
                radTextBox_Control.Text = ""; radTextBox_Control.Enabled = true;
                pCaseCheckControl = "0";
            }

            if (dt.Rows[0]["BRANCH"].ToString() == "")
            {
                radioButton_AllBch.Checked = true;
                radioButton_Bch.Checked = false;
                radioButton_MN.Checked = false;
                SetBch();
                for (int i = 0; i < radGridView_Bch.Rows.Count; i++)
                {
                    radGridView_Bch.Rows[i].Cells["STA"].Value = "1";
                }
            }
            else
            {
                if ((dt.Rows.Count == 1) && (dt.Rows[0]["BRANCH"].ToString() == "RT"))
                {
                    radioButton_AllBch.Checked = false;
                    radioButton_Bch.Checked = true;
                    radioButton_MN.Checked = false;
                }
                radGridView_Bch.DataSource = dt;
                dt.AcceptChanges();
            }
            radioButton_AllBch.Enabled = false;
            radioButton_Bch.Enabled = false;
            radioButton_MN.Enabled = false;
            radGridView_Bch.ReadOnly = true;
        }
        //Bch
        void SetBch()
        {
            string sql = @"
                SELECT	BRANCH_ID,BRANCH_NAME ,'0' AS STA
                FROM	SHOP_BRANCH WITH (NOLOCK) 
                WHERE	BRANCH_STA IN ('2','1','4') and  BRANCH_STAOPEN IN ('1') 
		                AND BRANCH_ID != 'MN000'
                ORDER BY BRANCH_ID";
            DataTable dtBch = ConnectionClass.SelectSQL_Main(sql);
            radGridView_Bch.DataSource = dtBch;
            dtBch.AcceptChanges();
        }
        //Clear
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (_pCouponNumber == "0")
            {
                InsertData();
            }
            else
            {
                UpdateData();
            }
        }
        //Document
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        #region "SelectType"
        //ส่วนลดท้ายบิล
        private void RadioButton_Bill_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Bill.Checked == true)
            {
                radTextBox_barcode.Text = ""; radTextBox_barcode.Enabled = false;
                RadButton_Find.Enabled = false;
                //radLabel_Name.Text = "";radLabel_Unit.Text = "";
                radTextBox_Grand.Enabled = true;
                radTextBox_Grand.Focus();
            }
        }
        //ส่วนลดตามบาร์โค้ด
        private void RadioButton_Barcode_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Barcode.Checked == true)
            {
                radTextBox_barcode.Text = ""; radTextBox_barcode.Enabled = true;
                RadButton_Find.Enabled = true;
                //radLabel_Name.Text = "";radLabel_Unit.Text = "";
                radTextBox_barcode.Focus();
            }
        }
        #endregion

        #region "CheckNumber"
        //CheckNumber
        private void RadTextBox_Grand_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //CheckNumber
        private void RadTextBox_GrandCoupon_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //CheckNumber
        private void RadTextBox_MaxCoupon_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        #endregion

        #region "CheckBch"
        //All Bch
        private void RadioButton_AllBch_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_AllBch.Checked == true)
            {
                foreach (GridViewRowInfo item in radGridView_Bch.Rows)
                {
                    item.Cells["STA"].Value = "1";
                }
            }
        }
        //Select Bch
        private void RadioButton_Bch_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Bch.Checked == true)
            {
                foreach (GridViewRowInfo item in radGridView_Bch.Rows)
                {
                    item.Cells["STA"].Value = "0";
                }
            }
        }
        //MN Only
        private void RadioButton_MN_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_MN.Checked == true)
            {
                foreach (GridViewRowInfo item in radGridView_Bch.Rows)
                {
                    if (item.Cells["BRANCH_ID"].Value.ToString() == "RT")
                    {
                        item.Cells["STA"].Value = "0";
                    }
                    else
                    {
                        item.Cells["STA"].Value = "1";
                    }
                }
            }
        }
        //Choose Bch
        private void RadGridView_Bch_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (radioButton_Bch.Checked == true)
            {
                switch (e.Column.Name)
                {
                    case "STA":
                        if (radGridView_Bch.CurrentRow.Cells["STA"].Value.ToString() == "0")
                        {
                            radGridView_Bch.CurrentRow.Cells["STA"].Value = "1";
                        }
                        else
                        {
                            radGridView_Bch.CurrentRow.Cells["STA"].Value = "0";
                        }
                        break;
                    default:
                        break;
                }
            }

        }


        #endregion

        #region "GridCellFormat"
        private void RadGridView_Barcode_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Bch_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Bch_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Bch_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Bch_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        #region "Barcode"
        //FindBarcode
        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode("");
            if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
            {
                this.dtBarcode = ShowDataDGV_Itembarcode.items;
                radTextBox_barcode.Text = "";
                SetFindEnter();
            }
            else
            {
                radTextBox_barcode.Text = "";
                radTextBox_barcode.SelectAll();
            }
        }

        private void RadTextBox_barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_barcode.Text == "") return;

                this.Cursor = Cursors.WaitCursor;
                Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(radTextBox_barcode.Text);
                this.dtBarcode = dtBarcode;
                if (!(dtBarcode.GetItembarcodeStatus))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_barcode.SelectAll();
                    radTextBox_barcode.Focus();
                }
                else
                {
                    SetFindEnter();
                }
                this.Cursor = Cursors.Default;
            }
        }
        //Set Enter
        void SetFindEnter()
        {
            this.Cursor = Cursors.WaitCursor;

            dtDT.Rows.Add(this.dtBarcode.Itembarcode_ITEMBARCODE.ToString(),
                this.dtBarcode.Itembarcode_SPC_ITEMNAME.ToString(),
                this.dtBarcode.Itembarcode_UNITID.ToString());
            radGridView_Barcode.DataSource = dtDT;
            dtDT.AcceptChanges();

            radTextBox_Grand.Text = this.dtBarcode.Itembarcode_SPC_PRICEGROUP3.ToString();
            //radTextBox_barcode.Enabled = false;
            radTextBox_Grand.Enabled = true;
            //radTextBox_Grand.SelectAll();
            //radTextBox_Grand.Focus();
            radTextBox_barcode.Text = "";
            radTextBox_barcode.Focus();
            this.Cursor = Cursors.Default;
        }
        #endregion

        //Insert
        void InsertData()
        {
            string D_From = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + " " + radDateTimePicker_T1.Value.ToString("HH:mm:ss") + ".000";
            string D_TO = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + " " + radDateTimePicker_T2.Value.ToString("HH:mm:ss") + ".000";
            if (D_From == D_TO)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("สำหรับ วันที่-เวลา เริ่มใช้ - สิ้นสุด ต้องไม่เท่ากัน" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
                return;
            }

            if (radCheckBox_Control.Checked == true)
            {
                if (radTextBox_Control.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุรหัสสินค้าควมคุมก่อนการบันทึก.");
                    radTextBox_Control.Focus();
                    return;
                }
                else
                {
                    if (CheckControl(radTextBox_Control.Text) > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"รหัสสินค้าควมคุมที่ระบุซ้ำ  {Environment.NewLine} ไม่สามารถที่จะตั้งซ้ำกับชื่อเดิมได้");
                        radTextBox_Control.SelectAll();
                        radTextBox_Control.Focus();
                        return;
                    }
                }
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกข้อมูลคูปอง เนื่องจากไม่สามารถแก้ไขข้อมูลได้อีก ?.") == DialogResult.No) return;


            string typebill;
            string typebillName;
            if (radioButton_Barcode.Checked == true)
            {
                if (radGridView_Barcode.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุบาร์โค้ดสินค้าให้เรียบร้อยก่อนบันทึก" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
                    return;
                }
                typebill = "GroupBarcode";
                typebillName = "บาร์โค้ด";
            }
            else
            {
                typebill = "";
                typebillName = "ส่วนลดท้ายบิล";
            }

            ArrayList sqlIn = new ArrayList();
            //string couponNumber = Class.ConfigClass.GetMaxINVOICEID("LINE", "-", "LINE", "1");
            string couponNumber = Class.ConfigClass.GetMaxINVOICEID("LC", "", "LC", "12");

            string allowEmp = "0";
            if (radCheckBox_EmpUse.CheckState == CheckState.Checked) { allowEmp = "1"; }

            if (radioButton_AllBch.Checked == true)
            {
                sqlIn.Add(@"
                    INSERT INTO [dbo].[SHOP_COUPONONLINE]
                               ([COUPONNUMBER]
                               ,[DATETIMEBEGIN],[DATETIMEEND]
                               ,[TYPEUSE],[TYPEUSE_NAME],[BRANCH],[BRANCH_NAME],[MAXUSE],[COUPONVALUE],[MINIMUM_AMOUNT]
                               ,[WHOID],[WHONAME],[REMARK],[ALLOW_EMP],[STA_CONTROL])
                    VALUES ('" + couponNumber + @"',
                            '" + D_From + @"','" + D_TO + @"',
                            '" + typebill + @"','" + typebillName + @"','','ทุกสาขา','" + radTextBox_MaxCoupon.Text + @"','" + radTextBox_GrandCoupon.Text + @"','" + radTextBox_Grand.Text + @"',
                            '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                            '" + radTextBox_Remaek.Text + @"','" + allowEmp + @"','" + radTextBox_Control.Text + @"'
                    )
                    ");
            }
            else
            {
                foreach (GridViewRowInfo item in radGridView_Bch.Rows)
                {
                    if (item.Cells["STA"].Value.ToString() == "1")
                    {
                        sqlIn.Add(@"
                        INSERT INTO [dbo].[SHOP_COUPONONLINE]
                                   ([COUPONNUMBER]
                                   ,[DATETIMEBEGIN],[DATETIMEEND]
                                   ,[TYPEUSE],[TYPEUSE_NAME],[BRANCH],[BRANCH_NAME],[MAXUSE],[COUPONVALUE],[MINIMUM_AMOUNT]
                                   ,[WHOID],[WHONAME],[REMARK],[ALLOW_EMP],[STA_CONTROL])
                        VALUES ('" + couponNumber + @"',
                                '" + D_From + @"','" + D_TO + @"',
                                '" + typebill + @"','" + typebillName + @"','" + item.Cells["BRANCH_ID"].Value.ToString() + @"','" + item.Cells["BRANCH_NAME"].Value.ToString() + @"','" + radTextBox_MaxCoupon.Text + @"','" + radTextBox_GrandCoupon.Text + @"','" + radTextBox_Grand.Text + @"',
                                '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                                '" + radTextBox_Remaek.Text + @"','" + allowEmp + @"','" + radTextBox_Control.Text + @"'
                        )
                        ");
                    }
                }
            }

            if (radioButton_Barcode.Checked == true)
            {
                for (int i = 0; i < radGridView_Barcode.Rows.Count; i++)
                {
                    sqlIn.Add(@"
                        INSERT INTO [dbo].[SHOP_COUPONONLINE_DT]
                                   ([COUPONNUMBER],[LINENUM],[ITEMBARCODE],[SPC_ITEMNAME],[UNITID])
                        VALUES ('" + couponNumber + @"',
                                '" + (i + 1) + @"',
                                '" + radGridView_Barcode.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                                '" + radGridView_Barcode.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString() + @"',
                                '" + radGridView_Barcode.Rows[i].Cells["UNITID"].Value.ToString() + @"'
                        )
                        ");
                }

            }
            //เพิ่มการควมคุมการใช้คูปองกับลูกค้า
            if (radCheckBox_Control.Checked == true)
            {
                string MM = Convert.ToDateTime(radDateTimePicker_D1.Value).Month.ToString();
                string YYYY = Convert.ToDateTime(radDateTimePicker_D1.Value).Year.ToString();
                sqlIn.Add($@"
                    INSERT INTO     SHOP_COUPONONLINE_CUST(CUSTACCOUNT,LINECOUPON,COUPONNUMBER,
                                    MONTH,YEAR,POSITION,
                                    WHOINS,WHONAMEINS)
                    values          ('CONTROL','{radTextBox_Control.Text}','{couponNumber}',
                                    '{MM}','{YYYY}','0',
                                    '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}')
                    ");
            }

            string result = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlIn, IpServerConnectClass.ConMainRatail707);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }
        //Insert
        void UpdateData()
        {
            string D_From = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + " " + radDateTimePicker_T1.Value.ToString("HH:mm:ss") + ".000";
            string D_TO = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + " " + radDateTimePicker_T2.Value.ToString("HH:mm:ss") + ".000";
            if (D_From == D_TO)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("สำหรับ วันที่-เวลา เริ่มใช้ - สิ้นสุด ต้องไม่เท่ากัน" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
                return;
            }

            ArrayList sqlIn = new ArrayList();

            if (pCaseCheckControl == "0")
            {
                if (radCheckBox_Control.Checked == true)
                {
                    if (radTextBox_Control.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุรหัสสินค้าควมคุมก่อนการบันทึก.");
                        radTextBox_Control.Focus();
                        return;
                    }
                    else
                    {
                        if (CheckControl(radTextBox_Control.Text) > 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"รหัสสินค้าควมคุมที่ระบุซ้ำ  {Environment.NewLine} ไม่สามารถที่จะตั้งซ้ำกับชื่อเดิมได้");
                            radTextBox_Control.SelectAll();
                            radTextBox_Control.Focus();
                            return;
                        }
                        else
                        {
                            //เพิ่มการควมคุมการใช้คูปองกับลูกค้า
                            string MM = Convert.ToDateTime(radDateTimePicker_D1.Value).Month.ToString();
                            string YYYY = Convert.ToDateTime(radDateTimePicker_D1.Value).Year.ToString();
                            sqlIn.Add($@"
                            INSERT INTO     SHOP_COUPONONLINE_CUST(CUSTACCOUNT,LINECOUPON,COUPONNUMBER,
                                            MONTH,YEAR,POSITION,
                                            WHOINS,WHONAMEINS)
                            values          ('CONTROL','{radTextBox_Control.Text}','{radLabel_Docno.Text}',
                                            '{MM}','{YYYY}','0',
                                            '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}')
                            ");
                        }
                    }
                }
            }


            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการแก้ไขข้อมูลคูปอง ?.") == DialogResult.No) return;


            string allowEmp = "0";
            if (radCheckBox_EmpUse.CheckState == CheckState.Checked) { allowEmp = "1"; }

            sqlIn.Add(@"
                    UPDATE  [dbo].[SHOP_COUPONONLINE]
                    SET     DATETIMEBEGIN = '" + D_From + @"',DATETIMEEND = '" + D_TO + @"',
                            MAXUSE = '" + radTextBox_MaxCoupon.Text + @"',
                            COUPONVALUE = '" + radTextBox_GrandCoupon.Text + @"',MINIMUM_AMOUNT = '" + radTextBox_Grand.Text + @"',
                            REMARK = '" + radTextBox_Remaek.Text + @"',ALLOW_EMP = '" + allowEmp + @"',
                            STA_CONTROL = '" + radTextBox_Control.Text + @"',
                            WHOIDUP = '" + SystemClass.SystemUserID + @"',WHONAMEUP = '" + SystemClass.SystemUserName + @"',
                            UPDATEDATETIME = CONVERT(VARCHAR,GETDATE(),25) 
                    WHERE   [COUPONNUMBER] = '" + radLabel_Docno.Text + @"'
                    ");


            string result = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlIn, IpServerConnectClass.ConMainRatail707);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }
        private void RadTextBox_Grand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Grand.Text == "") return;
                if (Convert.ToDouble(radTextBox_Grand.Text) == 0) { MsgBoxClass.MsgBoxShowButtonOk_Warning("ยอดที่ต้องซื้อต้องมากกว่า 0 เท่านั้น."); return; }
                radTextBox_GrandCoupon.Enabled = true;
                radTextBox_GrandCoupon.Focus();
            }
        }

        private void RadTextBox_GrandCoupon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_GrandCoupon.Text == "") return;
                if (Convert.ToDouble(radTextBox_GrandCoupon.Text) == 0) { MsgBoxClass.MsgBoxShowButtonOk_Warning("มูลค่าคูปองต้องมากกว่า 0 เท่านั้น."); return; }
                radTextBox_MaxCoupon.Enabled = true;
                radTextBox_MaxCoupon.Focus();
            }
        }

        private void RadTextBox_MaxCoupon_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_MaxCoupon.Text == "") return;
                if (Convert.ToDouble(radTextBox_MaxCoupon.Text) == 0) { MsgBoxClass.MsgBoxShowButtonOk_Warning("จำนวนคูปองต้องมากกว่า 0 เท่านั้น."); return; }
                radTextBox_Remaek.Enabled = true;
                radTextBox_Remaek.Focus();
                radButton_Save.Enabled = true;
            }
        }

        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }

        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }

        private void RadCheckBox_Control_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Control.Checked == true)
            {
                radTextBox_Control.Text = "";
                radTextBox_Control.Enabled = true;
                radTextBox_Control.Focus();
            }
            else
            {
                radTextBox_Control.Text = "";
                radTextBox_Control.Enabled = false;
            }
        }

        int CheckControl(string pCon)
        {
            string sql = $@"
            SELECT  *   FROM    SHOP_COUPONONLINE WITH (NOLOCK)
            WHERE   STA_CONTROL = '{pCon}'
            ";
            return ConnectionClass.SelectSQL_POSRetail707(sql).Rows.Count;
        }
    }
}