﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Data;
using PC_Shop24Hrs.Class;


namespace PC_Shop24Hrs.GeneralForm.LineCoupon
{
    public partial class LineCoupon_Main : Telerik.WinControls.UI.RadForm
    {
        private DataTable dt = new DataTable();
        //Load
        public LineCoupon_Main()
        {
            InitializeComponent();

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขคูปอง";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่มคูปอง";
            RadButtonElement_refresh.ShowBorder = true; RadButtonElement_refresh.ToolTipText = "refresh";
            radStatusStrip1.SizingGrip = false;
            radCheckBox_Use.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Use.ButtonElement.ShowBorder = true;

            radLabel_Detail.Text = "กด + >> เพิ่มคูปอง | DoubleClick ช่องสถานะเพื่อปิดการใช้งานคูปอง";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COUPONNUMBER", "เลขที่คูปอง", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMEBEGIN", "วันที่เริ่มใช้", 160));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMEEND", "วันที่สิ้นสุด", 160));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH", "สาขาที่ใช้ได้", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขาที่ใช้ได้", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TYPEUSE", "บาร์โค้ด"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TYPEUSE_NAME", "ชื่อประเภท"));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MINIMUM_AMOUNT", "ซื้อขั้นต่ำ", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUPONVALUE", "มูลค่าคูปอง", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MAXUSE", "จำนวน[ใบ]", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA_DESC", "สถานะ", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("STA_CONTROL", "รหัสควบคุม", 100));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 500));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_ACTIVE", "สถานะ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("ALLOW_EMP", "พนักงาน", 80));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOID", "รหัสผู้สร้าง", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAME", "ชื่อผู้สร้าง", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDATETIME", "วันที่สร้าง", 160));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOIDUP", "รหัสผู้แก้ไข", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAMEUP", "ชื่อผู้แก้ไข", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UPDATEDATETIME", "วันที่แก้ไข", 160));

            ExpressionFormattingObject o_1 = new ExpressionFormattingObject("MyCondition1", "STA_ACTIVE = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_Show.Columns["COUPONNUMBER"].ConditionalFormattingObjectList.Add(o_1);
            RadGridView_Show.Columns["BRANCH"].ConditionalFormattingObjectList.Add(o_1);
            RadGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(o_1);
            RadGridView_Show.Columns["STA_DESC"].ConditionalFormattingObjectList.Add(o_1);

            DatagridClass.SetCellBackClolorByExpression("STA_CONTROL", "STA_CONTROL <> '' ", ConfigClass.SetColor_YellowPastel(), RadGridView_Show);

            radCheckBox_Use.Checked = true;
        }
        //Load Main
        private void LineCoupon_Main_Load(object sender, EventArgs e)
        {
            string sql = $@"
                UPDATE	SHOP_COUPONONLINE	SET STA_ACTIVE = '0'
                WHERE	DATETIMEEND < GETDATE() AND STA_ACTIVE = '1' 
            ";
            ConnectionClass.ExecuteSQL_POSRetail707(sql);

            Set_DGV();
        }

        //Set DGV
        void Set_DGV()
        {
            this.Cursor = Cursors.WaitCursor;
            string pCon = "";
            if (radCheckBox_Use.Checked == true)
            {
                pCon = " WHERE STA_ACTIVE = '1' ";
            }
            string sql = string.Format($@"
            SELECT	COUPONNUMBER,DATETIMEBEGIN,DATETIMEEND,
		            TYPEUSE,TYPEUSE_NAME,
		            BRANCH,BRANCH_NAME,
		            MAXUSE,MINIMUM_AMOUNT,COUPONVALUE,WHOID,WHONAME,CREATEDATETIME,REMARK,STA_ACTIVE,
                    CASE STA_ACTIVE WHEN '0' THEN 'ปิดใช้' ELSE 'เปิดใช้' END AS STA_DESC ,ALLOW_EMP ,
                    ISNULL(WHOIDUP,'') AS WHOIDUP,ISNULL(WHONAMEUP,'') AS WHONAMEUP,
                    ISNULL(UPDATEDATETIME,'') AS UPDATEDATETIME,STA_CONTROL 
            FROM	[SHOP_COUPONONLINE] WITH (NOLOCK)
            {pCon}
            ORDER BY COUPONNUMBER DESC  
            ");
            dt = ConnectionClass.SelectSQL_POSRetail707(sql);
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //double Click
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "COUPONNUMBER":
                    LineCoupon frm_LineCoupon = new LineCoupon(RadGridView_Show.CurrentRow.Cells["COUPONNUMBER"].Value.ToString());
                    if (frm_LineCoupon.ShowDialog() == DialogResult.Yes)
                    {
                        Set_DGV();
                    }
                    break;
                case "STA_DESC":
                    string deseSta, sta;
                    if (RadGridView_Show.CurrentRow.Cells["STA_ACTIVE"].Value.ToString() == "0")
                    {
                        sta = "1";
                        deseSta = "เปิด";
                    }
                    else
                    {
                        sta = "0";
                        deseSta = "ปิด";
                    }

                    string bchName = RadGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString();
                    string docno = RadGridView_Show.CurrentRow.Cells["COUPONNUMBER"].Value.ToString();

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการ '{deseSta}การใช้คูปอง' สำหรับเลขที่ " + docno + Environment.NewLine +
                        "ของ " + bchName + "  ?.") == DialogResult.No) return;

                    string bchID = RadGridView_Show.CurrentRow.Cells["BRANCH"].Value.ToString();
                    string sql = $@"
                                UPDATE  SHOP_COUPONONLINE 
                                SET     STA_ACTIVE = '{sta}',WHOIDUP = '" + SystemClass.SystemUserID + @"',WHONAMEUP = '" + SystemClass.SystemUserName + @"',UPDATEDATETIME = GETDATE() 
                                WHERE   COUPONNUMBER = '" + docno + @"' AND  BRANCH = '" + bchID + @"'
                    ";

                    string result = ConnectionClass.ExecuteSQL_POSRetail707(sql);
                    if (result == "")
                    {
                        RadGridView_Show.CurrentRow.Cells["STA_ACTIVE"].Value = $@"{sta}";
                        RadGridView_Show.CurrentRow.Cells["STA_DESC"].Value = $@"{deseSta}ใช้";
                    }
                    MsgBoxClass.MsgBoxShow_SaveStatus(result);

                    break;
                default:
                    break;
            }
        }
        //add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            LineCoupon frm_LineCoupon = new LineCoupon("0");
            if (frm_LineCoupon.ShowDialog() == DialogResult.Yes)
            {
                Set_DGV();
            }
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
        //refresh
        private void RadButtonElement_refresh_Click(object sender, EventArgs e)
        {
            Set_DGV();
        }
        //Edit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            LineCoupon frm_LineCoupon = new LineCoupon(RadGridView_Show.CurrentRow.Cells["COUPONNUMBER"].Value.ToString());
            if (frm_LineCoupon.ShowDialog() == DialogResult.Yes)
            {
                Set_DGV();
            }

        }
        //refresh

        private void RadCheckBox_Use_CheckStateChanged(object sender, EventArgs e)
        {
            Set_DGV();
        }


    }
}
