﻿namespace PC_Shop24Hrs.GeneralForm.LineCoupon
{
    partial class LineCoupon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LineCoupon));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.radCheckBox_Control = new Telerik.WinControls.UI.RadCheckBox();
            this.radTextBox_Control = new Telerik.WinControls.UI.RadTextBox();
            this.radGridView_Barcode = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_EmpUse = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_GrandCoupon = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_MaxCoupon = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton_MN = new System.Windows.Forms.RadioButton();
            this.radioButton_Bch = new System.Windows.Forms.RadioButton();
            this.radioButton_AllBch = new System.Windows.Forms.RadioButton();
            this.radGridView_Bch = new Telerik.WinControls.UI.RadGridView();
            this.RadButton_Find = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radioButton_Barcode = new System.Windows.Forms.RadioButton();
            this.radioButton_Bill = new System.Windows.Forms.RadioButton();
            this.radDateTimePicker_T2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_D2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_T1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_D1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Bath = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remaek = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Grand = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Control)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Control)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Barcode.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_EmpUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_GrandCoupon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MaxCoupon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bch.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Find)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_T2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_T1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Bath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Grand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Control);
            this.radGroupBox_DB.Controls.Add(this.radGridView_Barcode);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Docno);
            this.radGroupBox_DB.Controls.Add(this.radCheckBox_EmpUse);
            this.radGroupBox_DB.Controls.Add(this.radLabel8);
            this.radGroupBox_DB.Controls.Add(this.radLabel5);
            this.radGroupBox_DB.Controls.Add(this.radLabel7);
            this.radGroupBox_DB.Controls.Add(this.radLabel6);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_GrandCoupon);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_MaxCoupon);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.panel1);
            this.radGroupBox_DB.Controls.Add(this.RadButton_Find);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_barcode);
            this.radGroupBox_DB.Controls.Add(this.radioButton_Barcode);
            this.radGroupBox_DB.Controls.Add(this.radioButton_Bill);
            this.radGroupBox_DB.Controls.Add(this.radDateTimePicker_T2);
            this.radGroupBox_DB.Controls.Add(this.radDateTimePicker_D2);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Controls.Add(this.radDateTimePicker_T1);
            this.radGroupBox_DB.Controls.Add(this.radDateTimePicker_D1);
            this.radGroupBox_DB.Controls.Add(this.RadButton_pdt);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Bath);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Remaek);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Grand);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radLabel_1);
            this.radGroupBox_DB.Controls.Add(this.radLabel4);
            this.radGroupBox_DB.Controls.Add(this.radCheckBox_Control);
            this.radGroupBox_DB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(820, 748);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // radCheckBox_Control
            // 
            this.radCheckBox_Control.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_Control.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_Control.Location = new System.Drawing.Point(332, 577);
            this.radCheckBox_Control.Name = "radCheckBox_Control";
            this.radCheckBox_Control.Size = new System.Drawing.Size(64, 19);
            this.radCheckBox_Control.TabIndex = 96;
            this.radCheckBox_Control.Text = "ควบคุม";
            this.radCheckBox_Control.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Control_CheckStateChanged);
            // 
            // radTextBox_Control
            // 
            this.radTextBox_Control.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Control.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Control.Location = new System.Drawing.Point(399, 574);
            this.radTextBox_Control.MaxLength = 5;
            this.radTextBox_Control.Name = "radTextBox_Control";
            this.radTextBox_Control.Size = new System.Drawing.Size(73, 25);
            this.radTextBox_Control.TabIndex = 95;
            this.radTextBox_Control.Text = "A1450";
            // 
            // radGridView_Barcode
            // 
            this.radGridView_Barcode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Barcode.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Barcode.Location = new System.Drawing.Point(12, 242);
            // 
            // 
            // 
            this.radGridView_Barcode.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Barcode.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Barcode.Name = "radGridView_Barcode";
            // 
            // 
            // 
            this.radGridView_Barcode.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Barcode.Size = new System.Drawing.Size(464, 224);
            this.radGridView_Barcode.TabIndex = 94;
            this.radGridView_Barcode.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Barcode_ViewCellFormatting);
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(12, 27);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(280, 19);
            this.radLabel_Docno.TabIndex = 93;
            this.radLabel_Docno.Text = "LINE210422000004";
            // 
            // radCheckBox_EmpUse
            // 
            this.radCheckBox_EmpUse.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_EmpUse.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_EmpUse.Location = new System.Drawing.Point(12, 577);
            this.radCheckBox_EmpUse.Name = "radCheckBox_EmpUse";
            this.radCheckBox_EmpUse.Size = new System.Drawing.Size(169, 19);
            this.radCheckBox_EmpUse.TabIndex = 92;
            this.radCheckBox_EmpUse.Text = "อนุญาติให้พนักงานใช้ได้";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(12, 480);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(142, 19);
            this.radLabel8.TabIndex = 91;
            this.radLabel8.Text = "ยอดที่ต้องซื้อ [Enter]";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(12, 186);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(128, 19);
            this.radLabel5.TabIndex = 90;
            this.radLabel5.Text = "บาร์โค้ด [Enter]";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(270, 514);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(70, 19);
            this.radLabel7.TabIndex = 89;
            this.radLabel7.Text = "บาท";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(270, 545);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(70, 19);
            this.radLabel6.TabIndex = 88;
            this.radLabel6.Text = "ใบ";
            // 
            // radTextBox_GrandCoupon
            // 
            this.radTextBox_GrandCoupon.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_GrandCoupon.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_GrandCoupon.Location = new System.Drawing.Point(155, 508);
            this.radTextBox_GrandCoupon.MaxLength = 5;
            this.radTextBox_GrandCoupon.Name = "radTextBox_GrandCoupon";
            this.radTextBox_GrandCoupon.Size = new System.Drawing.Size(99, 25);
            this.radTextBox_GrandCoupon.TabIndex = 1;
            this.radTextBox_GrandCoupon.Text = "0054450";
            this.radTextBox_GrandCoupon.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_GrandCoupon_TextChanging);
            this.radTextBox_GrandCoupon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_GrandCoupon_KeyDown);
            // 
            // radTextBox_MaxCoupon
            // 
            this.radTextBox_MaxCoupon.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_MaxCoupon.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MaxCoupon.Location = new System.Drawing.Point(155, 539);
            this.radTextBox_MaxCoupon.MaxLength = 5;
            this.radTextBox_MaxCoupon.Name = "radTextBox_MaxCoupon";
            this.radTextBox_MaxCoupon.Size = new System.Drawing.Size(99, 25);
            this.radTextBox_MaxCoupon.TabIndex = 2;
            this.radTextBox_MaxCoupon.Text = "0054450";
            this.radTextBox_MaxCoupon.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_MaxCoupon_TextChanging);
            this.radTextBox_MaxCoupon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_MaxCoupon_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(12, 543);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(142, 19);
            this.radLabel2.TabIndex = 86;
            this.radLabel2.Text = "จำนวนคูปอง [Enter]";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton_MN);
            this.panel1.Controls.Add(this.radioButton_Bch);
            this.panel1.Controls.Add(this.radioButton_AllBch);
            this.panel1.Controls.Add(this.radGridView_Bch);
            this.panel1.Location = new System.Drawing.Point(480, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(328, 671);
            this.panel1.TabIndex = 85;
            // 
            // radioButton_MN
            // 
            this.radioButton_MN.AutoSize = true;
            this.radioButton_MN.Location = new System.Drawing.Point(152, 38);
            this.radioButton_MN.Name = "radioButton_MN";
            this.radioButton_MN.Size = new System.Drawing.Size(112, 20);
            this.radioButton_MN.TabIndex = 83;
            this.radioButton_MN.TabStop = true;
            this.radioButton_MN.Text = "เฉพาะมินิมาร์ท";
            this.radioButton_MN.UseVisualStyleBackColor = true;
            this.radioButton_MN.CheckedChanged += new System.EventHandler(this.RadioButton_MN_CheckedChanged);
            // 
            // radioButton_Bch
            // 
            this.radioButton_Bch.AutoSize = true;
            this.radioButton_Bch.Location = new System.Drawing.Point(12, 38);
            this.radioButton_Bch.Name = "radioButton_Bch";
            this.radioButton_Bch.Size = new System.Drawing.Size(79, 20);
            this.radioButton_Bch.TabIndex = 82;
            this.radioButton_Bch.TabStop = true;
            this.radioButton_Bch.Text = "ระบุสาขา";
            this.radioButton_Bch.UseVisualStyleBackColor = true;
            this.radioButton_Bch.CheckedChanged += new System.EventHandler(this.RadioButton_Bch_CheckedChanged);
            // 
            // radioButton_AllBch
            // 
            this.radioButton_AllBch.AutoSize = true;
            this.radioButton_AllBch.Location = new System.Drawing.Point(12, 12);
            this.radioButton_AllBch.Name = "radioButton_AllBch";
            this.radioButton_AllBch.Size = new System.Drawing.Size(172, 20);
            this.radioButton_AllBch.TabIndex = 81;
            this.radioButton_AllBch.TabStop = true;
            this.radioButton_AllBch.Text = "ทุกสาขา [รวมสาขาใหญ่]";
            this.radioButton_AllBch.UseVisualStyleBackColor = true;
            this.radioButton_AllBch.CheckedChanged += new System.EventHandler(this.RadioButton_AllBch_CheckedChanged);
            // 
            // radGridView_Bch
            // 
            this.radGridView_Bch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Bch.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Bch.Location = new System.Drawing.Point(4, 64);
            // 
            // 
            // 
            this.radGridView_Bch.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Bch.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Bch.Name = "radGridView_Bch";
            // 
            // 
            // 
            this.radGridView_Bch.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Bch.Size = new System.Drawing.Size(320, 604);
            this.radGridView_Bch.TabIndex = 79;
            this.radGridView_Bch.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Bch_ViewCellFormatting);
            this.radGridView_Bch.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Bch_CellClick);
            this.radGridView_Bch.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Bch_ConditionalFormattingFormShown);
            this.radGridView_Bch.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Bch_FilterPopupRequired);
            this.radGridView_Bch.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Bch_FilterPopupInitialized);
            // 
            // RadButton_Find
            // 
            this.RadButton_Find.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_Find.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_Find.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_Find.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_Find.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_Find.Location = new System.Drawing.Point(230, 209);
            this.RadButton_Find.Name = "RadButton_Find";
            this.RadButton_Find.Size = new System.Drawing.Size(26, 26);
            this.RadButton_Find.TabIndex = 84;
            this.RadButton_Find.Text = "radButton3";
            this.RadButton_Find.Click += new System.EventHandler(this.RadButton_Find_Click);
            // 
            // radTextBox_barcode
            // 
            this.radTextBox_barcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_barcode.Location = new System.Drawing.Point(12, 209);
            this.radTextBox_barcode.MaxLength = 30;
            this.radTextBox_barcode.Name = "radTextBox_barcode";
            this.radTextBox_barcode.Size = new System.Drawing.Size(217, 25);
            this.radTextBox_barcode.TabIndex = 82;
            this.radTextBox_barcode.Text = "0054450";
            this.radTextBox_barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_barcode_KeyDown);
            // 
            // radioButton_Barcode
            // 
            this.radioButton_Barcode.AutoSize = true;
            this.radioButton_Barcode.Location = new System.Drawing.Point(155, 160);
            this.radioButton_Barcode.Name = "radioButton_Barcode";
            this.radioButton_Barcode.Size = new System.Drawing.Size(141, 20);
            this.radioButton_Barcode.TabIndex = 81;
            this.radioButton_Barcode.TabStop = true;
            this.radioButton_Barcode.Text = "ส่วนลดการซื้อสินค้า";
            this.radioButton_Barcode.UseVisualStyleBackColor = true;
            this.radioButton_Barcode.CheckedChanged += new System.EventHandler(this.RadioButton_Barcode_CheckedChanged);
            // 
            // radioButton_Bill
            // 
            this.radioButton_Bill.AutoSize = true;
            this.radioButton_Bill.Location = new System.Drawing.Point(12, 160);
            this.radioButton_Bill.Name = "radioButton_Bill";
            this.radioButton_Bill.Size = new System.Drawing.Size(109, 20);
            this.radioButton_Bill.TabIndex = 80;
            this.radioButton_Bill.TabStop = true;
            this.radioButton_Bill.Text = "ส่วนลดท้ายบิล";
            this.radioButton_Bill.UseVisualStyleBackColor = true;
            this.radioButton_Bill.CheckedChanged += new System.EventHandler(this.RadioButton_Bill_CheckedChanged);
            // 
            // radDateTimePicker_T2
            // 
            this.radDateTimePicker_T2.CustomFormat = "hh:mm:ss";
            this.radDateTimePicker_T2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_T2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_T2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_T2.Location = new System.Drawing.Point(155, 129);
            this.radDateTimePicker_T2.Name = "radDateTimePicker_T2";
            this.radDateTimePicker_T2.ShowUpDown = true;
            this.radDateTimePicker_T2.Size = new System.Drawing.Size(99, 21);
            this.radDateTimePicker_T2.TabIndex = 78;
            this.radDateTimePicker_T2.TabStop = false;
            this.radDateTimePicker_T2.Text = "12:00:00";
            this.radDateTimePicker_T2.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // radDateTimePicker_D2
            // 
            this.radDateTimePicker_D2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_D2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D2.Location = new System.Drawing.Point(12, 129);
            this.radDateTimePicker_D2.Name = "radDateTimePicker_D2";
            this.radDateTimePicker_D2.Size = new System.Drawing.Size(128, 21);
            this.radDateTimePicker_D2.TabIndex = 77;
            this.radDateTimePicker_D2.TabStop = false;
            this.radDateTimePicker_D2.Text = "22/05/2020";
            this.radDateTimePicker_D2.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_D2.ValueChanged += new System.EventHandler(this.RadDateTimePicker_D2_ValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(12, 56);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(175, 19);
            this.radLabel1.TabIndex = 75;
            this.radLabel1.Text = "ระบุวันที่-เวลา เริ่มใช้";
            // 
            // radDateTimePicker_T1
            // 
            this.radDateTimePicker_T1.CustomFormat = "hh:mm:ss";
            this.radDateTimePicker_T1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_T1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_T1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_T1.Location = new System.Drawing.Point(155, 81);
            this.radDateTimePicker_T1.Name = "radDateTimePicker_T1";
            this.radDateTimePicker_T1.ShowUpDown = true;
            this.radDateTimePicker_T1.Size = new System.Drawing.Size(99, 21);
            this.radDateTimePicker_T1.TabIndex = 74;
            this.radDateTimePicker_T1.TabStop = false;
            this.radDateTimePicker_T1.Text = "12:00:00";
            this.radDateTimePicker_T1.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // radDateTimePicker_D1
            // 
            this.radDateTimePicker_D1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDateTimePicker_D1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D1.Location = new System.Drawing.Point(12, 81);
            this.radDateTimePicker_D1.Name = "radDateTimePicker_D1";
            this.radDateTimePicker_D1.Size = new System.Drawing.Size(128, 21);
            this.radDateTimePicker_D1.TabIndex = 73;
            this.radDateTimePicker_D1.TabStop = false;
            this.radDateTimePicker_D1.Text = "22/05/2020";
            this.radDateTimePicker_D1.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_D1.ValueChanged += new System.EventHandler(this.RadDateTimePicker_D1_ValueChanged);
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(450, 20);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 72;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_Bath
            // 
            this.radLabel_Bath.AutoSize = false;
            this.radLabel_Bath.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Bath.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Bath.Location = new System.Drawing.Point(270, 480);
            this.radLabel_Bath.Name = "radLabel_Bath";
            this.radLabel_Bath.Size = new System.Drawing.Size(58, 23);
            this.radLabel_Bath.TabIndex = 70;
            this.radLabel_Bath.Text = "บาท";
            // 
            // radTextBox_Remaek
            // 
            this.radTextBox_Remaek.AcceptsReturn = true;
            this.radTextBox_Remaek.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Remaek.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remaek.Location = new System.Drawing.Point(12, 623);
            this.radTextBox_Remaek.Multiline = true;
            this.radTextBox_Remaek.Name = "radTextBox_Remaek";
            // 
            // 
            // 
            this.radTextBox_Remaek.RootElement.StretchVertically = true;
            this.radTextBox_Remaek.Size = new System.Drawing.Size(462, 69);
            this.radTextBox_Remaek.TabIndex = 3;
            this.radTextBox_Remaek.Tag = "";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(12, 603);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(217, 19);
            this.radLabel3.TabIndex = 68;
            this.radLabel3.Text = "หมายเหตุ";
            // 
            // radTextBox_Grand
            // 
            this.radTextBox_Grand.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Grand.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Grand.Location = new System.Drawing.Point(155, 477);
            this.radTextBox_Grand.MaxLength = 5;
            this.radTextBox_Grand.Name = "radTextBox_Grand";
            this.radTextBox_Grand.Size = new System.Drawing.Size(99, 25);
            this.radTextBox_Grand.TabIndex = 0;
            this.radTextBox_Grand.Text = "0054450";
            this.radTextBox_Grand.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Grand_TextChanging);
            this.radTextBox_Grand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Grand_KeyDown);
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(286, 704);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(120, 32);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(414, 704);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(120, 32);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(12, 513);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(128, 19);
            this.radLabel_1.TabIndex = 39;
            this.radLabel_1.Text = "มูลค่าคูปอง [Enter]";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(12, 108);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(175, 19);
            this.radLabel4.TabIndex = 76;
            this.radLabel4.Text = "ระบุวันที่-เวลา สิ้นสุด";
            // 
            // LineCoupon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(820, 748);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LineCoupon";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "สร้างคูปอง";
            this.Load += new System.EventHandler(this.LineCoupon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Control)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Control)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Barcode.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_EmpUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_GrandCoupon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MaxCoupon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bch.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Bch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Find)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_T2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_T1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Bath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Grand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Grand;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remaek;
        private Telerik.WinControls.UI.RadLabel radLabel_Bath;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_T1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_T2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D2;
        private Telerik.WinControls.UI.RadGridView radGridView_Bch;
        private System.Windows.Forms.RadioButton radioButton_Barcode;
        private System.Windows.Forms.RadioButton radioButton_Bill;
        private Telerik.WinControls.UI.RadTextBox radTextBox_barcode;
        private Telerik.WinControls.UI.RadButton RadButton_Find;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton_AllBch;
        private System.Windows.Forms.RadioButton radioButton_Bch;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_GrandCoupon;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MaxCoupon;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private System.Windows.Forms.RadioButton radioButton_MN;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_EmpUse;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadGridView radGridView_Barcode;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Control;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Control;
    }
}
