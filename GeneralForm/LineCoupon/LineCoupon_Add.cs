﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.GeneralForm.LineCoupon
{
    public partial class LineCoupon_Add : Telerik.WinControls.UI.RadForm
    {
        string accountNum;
        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
       

        #endregion
        //string pTypeInput
        public LineCoupon_Add()
        {
            InitializeComponent();
        }

        private void LineCoupon_Add_Load(object sender, EventArgs e)
        {

            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            ClearTxt();


            radTextBox_CustID.Focus();
        }
        //ClearData
        void ClearTxt()
        {

            radLabel_Name.Text = "";
            radLabel_1Desc.Text = "";

            radLabel_1Desc.Text = "";

            radTextBox_CustID.Text = ""; accountNum = "";
            radTextBox_CustID.Enabled = true;

            radButton_Save.Enabled = false;
            radTextBox_LineID.Enabled = false; radTextBox_LineID.Text = "";
            radTextBox_Tel.Enabled = false; radTextBox_Tel.Text = "";
            radTextBox_Grand.Enabled = false; radTextBox_Grand.Text = "";
            radTextBox_Net.Enabled = false; radTextBox_Net.Text = "";
            radTextBox_Remark.Enabled = false; radTextBox_Remark.Text = "";

            radTextBox_CustID.Focus();
        }


        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string sqlIns = $@"
            INSERT INTO [dbo].[SHOP_CUSTOMER_RENT]
                   ([MONTH],[YEAR],[ZONE]
                   ,[POSITION],[ACCOUNTNUM],[NAME]
                   ,[TEL],[LINEID],[TOTAL],[DISCOUNT],[WHOIDINS],[WHONAMEINS],[REMARK_DESC]
                   )
             VALUES
                   ('{DateTime.Now.Month}','{DateTime.Now.Year}','3',
                    'Line@','{accountNum}','{radLabel_1Desc.Text}',
                    '{radTextBox_Tel.Text}','{radTextBox_LineID.Text}','{radTextBox_Grand.Text}',
                    '{radTextBox_Net.Text}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{radTextBox_Remark.Text}')
            ";
            String resault = ConnectionClass.ExecuteSQL_Main(sqlIns);
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "")
            {
                ClearTxt();
                return;
            }
        }


        private void RadButton_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }

        //Enter Customer
        private void RadTextBox_CustID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CustID.Text == "") return;
                if (radTextBox_CustID.Text.Length < 6) return;

                DataTable dt = Models.CustomerClass.FindCust_ByCustID("", $@" AND ACCOUNTNUM = 'C{radTextBox_CustID.Text.Trim()}' ");
                    //ConnectionClass.SelectSQL_Main(
                    //$@" SELECT	ACCOUNTNUM,NAME,NAMEALIAS,PHONE
                    //    FROM	SHOP2013TMP.dbo.CUSTTABLE WITH (NOLOCK)
                    //    WHERE	ACCOUNTNUM = 'C{radTextBox_CustID.Text}' "
                    //);
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสลูกค้า");
                    radTextBox_CustID.SelectAll();
                    radTextBox_CustID.Focus();
                    return;
                }

                accountNum = dt.Rows[0]["ACCOUNTNUM"].ToString(); radTextBox_CustID.Enabled = false;
                radLabel_Name.Text = dt.Rows[0]["NAMEALIAS"].ToString();
                radLabel_1Desc.Text = dt.Rows[0]["NAME"].ToString();
                radTextBox_Tel.Text = dt.Rows[0]["PHONE"].ToString();
                radTextBox_Tel.Enabled = true; radTextBox_Tel.SelectAll(); radTextBox_Tel.Focus();
            }
        }

        private void RadTextBox_Tel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Tel.Text == "") return;
                radTextBox_LineID.Enabled = true; radTextBox_Tel.Enabled = false; radTextBox_LineID.Focus();
            }
        }

        private void RadTextBox_LineID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_LineID.Text == "") return;
                radTextBox_Grand.Enabled = true; radTextBox_LineID.Enabled = false; radTextBox_Grand.Focus();
            }
        }

        private void RadTextBox_Grand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Grand.Text == "") return;
                radTextBox_Net.Enabled = true; radTextBox_Grand.Enabled = false; radTextBox_Net.Focus();
            }
        }

        private void RadTextBox_Net_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Net.Text == "") return;
                radTextBox_Remark.Enabled = true; radTextBox_Remark.Focus();
                radButton_Save.Enabled = true; radTextBox_Net.Enabled = false;
            }
        }

        private void RadTextBox_CustID_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_Grand_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_Net_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
    }
}