﻿
using System;
using System.Data;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public class EmplScan
    {
        public string DOCID { get; set; } = "";
        public string EMPLID { get; set; } = "";
        public string EMPLNAME { get; set; } = "";
        public string POSITION { get; set; } = "";
        public string BRANCHID { get; set; } = "";
        public string DPT { get; set; } = "";
        public string EMPLOUT { get; set; } = "";
        public string PRODUCT { get; set; } = "";
        public double OIL { get; set; } = 0;
        public string SCANINTIME { get; set; } = "";
        public double Pay_Hotel { get; set; } = 0;
        public double Pay_AddOn { get; set; } = 0;
    }

    public class TimeKeeperClass
    {

        //คนพิการ
        public static bool GetEmplScanCard_ByEmplBranch(string ALTNUM, string BranchID)
        {
            string str = $@"
                Select	* 
                from	SPC_WS_EMPLSCANHISTTABLE WITH (NOLOCK)  
                WHERE	ISUSECARD = '1' 
                        AND ALTNUM  = '{ALTNUM}'  AND REFTAFFID = '{BranchID}'";
            DataTable dt = ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConHROS);
            bool CheckEmpl = true;
            if (dt.Rows.Count > 0) CheckEmpl = false;
            return CheckEmpl;
        }

        //Check จำนวนการพัก
        public static int GetTimeBreakPW_IDCARD(string PW_IDTimeKeeper)
        {
            string str = $@"  
                SELECT  COUNT(PW_IDCARD)+1 AS COUNTPW_IDCARD
                FROM    Shop_TimeBreak with(nolock)  
                WHERE   PW_DATE_IN = CONVERT(VARCHAR,GETDATE(),23) 
                        AND PW_IDTimeKeeper = '{PW_IDTimeKeeper}' 
                        AND PW_STATUS = '0' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(str);
            int PW_IDCARD = 0;
            if (dt.Rows.Count > 0) PW_IDCARD = int.Parse(dt.Rows[0]["COUNTPW_IDCARD"].ToString());
            return PW_IDCARD;
        }

        ////รายงานสแกนนิ้ว 1.30
        //public static DataTable GetScanHalfHour(string DateStart, string DateEnd, string Dpt)
        //{
        //    string SelectDept = "AND SCANID = '" + SystemClass.SystemBranchID + @"' "; ;
        //    if (SystemClass.SystemBranchID == "MN000") SelectDept = "AND NUM IN ( '" + Dpt + @"') ";

        //    string str = $@"
        //            DECLARE @DateBegin Date = '{DateStart}';
        //            DECLARE @DateEnd Date = '{DateEnd}';  

        //            select  SCANID AS BRANCH_ID,
        //              SHOP_BRANCH.BRANCH_NAME,
        //                    EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME ,DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,  
        //                    CONVERT(VARCHAR, CONVERTDATETIME, 25) AS CONVERTDATETIME   
        //            from  	SCANFINGER_TAFFDT WITH (NOLOCK)
        //              INNER JOIN SHOP.SHOP24HRS.dbo.SHOP_BRANCH WITH (NOLOCK) ON SCANFINGER_TAFFDT.SCANID = SHOP_BRANCH.BRANCH_ID
        //              INNER JOIN SHOP.SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SCANFINGER_TAFFDT.PWCARD = EMPLTABLE.ALTNUM AND EMPLTABLE.DATAAREAID = N'SPC'
        //              INNER JOIN SHOP.SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON  EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = 0
        //            WHERE	CONVERTDATE BETWEEN @DateBegin AND @DateEnd  
        //                    AND LOCATIONDATA = '2' 
        //                    AND SCANFINGER_TAFFDT.PWCARD <> ' ' 
        //                    {SelectDept}
        //            ORDER BY DIMENSIONS.NUM,EMPLTABLE.EMPLID,CONVERTDATETIME";
        //    DataTable dt = ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConHROS);
        //    return dt;
        //}
        ////รายงานสแกนนิ้ว ช่างก่อสร้าง
        //public static DataTable GetScanD041(string DateStart, string DateEnd, string Dpt)
        //{
        //    string SelectDept = "AND SCANID = '" + SystemClass.SystemBranchID + @"' "; ;
        //    if (SystemClass.SystemBranchID == "MN000") SelectDept = "AND NUM IN ( '" + Dpt + @"') ";

        //    string str = $@"
        //            DECLARE @DateBegin Date = '{DateStart}';
        //            DECLARE @DateEnd Date = '{DateEnd}';  

        //            select  SCANID AS BRANCH_ID,
        //              SHOP_BRANCH.BRANCH_NAME,
        //                    EMPLTABLE.EMPLID,EMPLTABLE.SPC_NAME ,DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,  
        //                    CONVERT(VARCHAR, CONVERTDATETIME, 25) AS CONVERTDATETIME   
        //            from  	SPC_WS_TAFFDTNEWSITE_SHOP24 WITH (NOLOCK)
        //              INNER JOIN SHOP.SHOP24HRS.dbo.SHOP_BRANCH WITH (NOLOCK) ON SPC_WS_TAFFDTNEWSITE_SHOP24.SCANID = SHOP_BRANCH.BRANCH_ID
        //              INNER JOIN SHOP.SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SPC_WS_TAFFDTNEWSITE_SHOP24.PWCARD = EMPLTABLE.ALTNUM AND EMPLTABLE.DATAAREAID = N'SPC'
        //              INNER JOIN SHOP.SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON  EMPLTABLE.DIMENSION = DIMENSIONS.NUM  AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = 0
        //            WHERE	CONVERTDATE BETWEEN @DateBegin AND @DateEnd  
        //                    AND LOCATIONDATA = '2' 
        //                    AND PWCARD <> ' ' 
        //                    {SelectDept}
        //            ORDER BY DIMENSIONS.NUM,EMPLTABLE.EMPLID,CONVERTDATETIME";
        //    DataTable dt = ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConHROS);
        //    return dt;
        //}


        #region "PCA"
        //เช็คข้อมูลของ PC ที่เข้าสาขา
        public static DataTable GetPCA_DateTimeNow(string PCA, string Branch)
        {
            string Sql = $@"
            select	SHOW_ID as EMPLID,SHOW_NAME as SPC_NAME,
                    SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC as DESCRIPTION ,
                    SHOP_CONFIGBRANCH_GenaralDetail.STA , ISNULL(PW_ID,'0') as PW_ID,PW_DATETIME_IN	 
            from    SHOP_CONFIGBRANCH_GenaralDetail  WITH (NOLOCK) 
                    LEFT OUTER JOIN 
                    (
	                    SELECT * FROM  SHOP_TIMEKEEPERPC with (nolock) 
	                    WHERE   PW_STATUS = 1 AND PW_TYPE = '1'
		                        AND PW_IDCARD = '{PCA}' 
		                        AND PW_BRANCH_IN = '{Branch}'  
		                        AND PW_DATE_IN = CONVERT(VARCHAR,GETDATE (),23) 
                    )Shop_TimeKeeper    ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID  =  Shop_TimeKeeper.PW_IDCARD   
            WHERE	SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = '{PCA}' 
                    AND TYPE_CONFIG='31' 
            ORDER BY EMPLID ";
            return ConnectionClass.SelectSQL_Main(Sql);
        }

        #endregion

        //ดึงข้อมูลทีมจัดร้าน
        public static bool GetEmplCenter(string Empl, string Branch)
        {
            string Sql = $@"PDA_MNSV_FindData '0','%{Empl}','{Branch}',''";

            DataTable dt = ConnectionClass.SelectSQL_Main(Sql);
            bool rsu = false;

            if (dt.Rows.Count > 0)
            {
                if (int.Parse(dt.Rows[0]["STA_MNSV"].ToString()) > 0)
                {
                    rsu = true;
                }
            }
            return rsu;
        }

        //จำนวนวันที่มาทำงานแต่ละเดือนทุกคน
        public static DataTable GetDayOfWorkByMouth(string yYear)
        {
            string sql = $@"
                SELECT	PW_IDCARD AS EMPLID,FORMAT(PW_DATE_IN,'MM') AS M_MONTH,COUNT(*) AS C_DAY
                FROM	SHOP_TIMEKEEPER WITH (NOLOCK)
                WHERE	PW_TYPE = '1'  {yYear}
                GROUP BY PW_IDCARD,FORMAT(PW_DATE_IN,'MM')
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #region ScanDept
        public static string ScanDept_Save(string Id, string IdM, string Name, string Num, string Possition, string Location, string Assistant)
        {
            return $@"
            INSERT INTO SHOP_SCANDEPT(
	            EMPLALTNUM, EMPLID, EMPLNAME, DIMENSION, DIMENSIONNAME,
	            CLIENT, USERCURRENT,
	            DESTINATION, EMPLIDREPLESE, SEQNO)
            VALUES(
	            '{Id}', '{IdM}', '{Name}', '{Num}', '{Possition}',
	            '{Environment.MachineName}', '{SystemClass.SystemUserName}',
	            '{Location}', '{Assistant}', '0')
            ";
        }

        public static DataTable DataFormSCANTAFF(string datestart, string dateend, string dimension)
        {
            string SHOP_EMPLOYEE = "";
            string DIMEN = $@" AND PWSECTION = '{dimension}' ";

            if (dimension == "32")
            {
                SHOP_EMPLOYEE = " INNER JOIN SHOP_EMPLOYEE WITH(NOLOCK) ON SHOP_SCANTAFF.PWCARD = SHOP_EMPLOYEE.EMP_ID AND SHOP_EMPLOYEE.EMP_STACOMMN IN  ( '1','2') ";
                DIMEN = " AND PWSECTION LIKE '32%' ";
            }

            string sql = $@"SELECT  PWEMPLOYEE,  'คุณ ' + PWFNAME +' '+ PWLNAME AS SPC_NAME,PWDPT,
		                            CONVERT(VARCHAR,PWDATE,23) AS PWDATE, CONVERT(VARCHAR,SCANINTIME,25) AS SCANINTIME,PWDESC, TIMEIN
                            FROM	SHOP_SCANTAFF WITH(NOLOCK)
                                    {SHOP_EMPLOYEE}
                            WHERE	CONVERT(VARCHAR,[PWDATE],23) BETWEEN '{datestart}' AND '{dateend}' 
		                            {DIMEN} 
                            ORDER BY PWDATE, PWEMPLOYEE";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable DataFormSCANDEPT(string dateTimestart, string EMPLID)
        {
            string sql = $@"SELECT	DISTINCT TOP 1 EMPLID, SHOP_SCANDEPT.DATESCAN, SHOP_SCANDEPT.TIMEISCAN, SHOP_SCANDEPT.DESTINATION, SHOP_SCANDEPT.EMPLIDREPLESE,
									SHOP_SCANDEPT.DATETIMESCAN                           
                            FROM	SHOP_SCANDEPT WITH(NOLOCK)
                            WHERE	DATETIMESCAN BETWEEN  '{dateTimestart}' AND DATEADD(HH,6,'{dateTimestart}') 
		                            AND EMPLID = '{EMPLID}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        #endregion

        #region ScanInSPC

        public static DataTable ScanInSPC_OutOffice(string trandate, string EmplId)
        {
            string sql = $@"
                                SELECT  CONVERT(nvarchar,IVZ_HROUTOFFICE.TrandDateTime,23) as TRANDDATETIME
		                                ,IVZ_HROUTOFFICE.DocId AS DOCID
		                                ,C_ount AS OUTOFFICEID
		                                ,IVZ_HROUTOFFICE.EmplId AS EMPLID
		                                ,IVZ_HROUTOFFICE.EmplCard AS EMPLCARD
		                                ,IVZ_HROUTOFFICE.Dimention AS DIMENSIONNAME
		                                ,IVZ_HROUTOFFICE.Dept AS DEPT
		                                ,IVZ_HROUTOFFICE.Reason AS REASON
		                                ,IVZ_HROUTOFFICE.HrApprovedOutDateTime AS HRAPPROVEDOUTDATATIME
                                FROM    [dbo].IVZ_HROUTOFFICE WITH (NOLOCK)
                                        INNER JOIN 
			                                (	SELECT  DocId,COUNT(*) AS C_ount
				                                FROM    [dbo].IVZ_HROUTOFFICE WITH (NOLOCK)
				                                WHERE   TRANDDATETIME = '{trandate}'
						                                AND IVZ_HROUTOFFICE.OutType = '1'
						                                AND	IVZ_HROUTOFFICE.HrApprovedOut = '2'
						                                AND	IVZ_HROUTOFFICE.HrApprovedIn = '1'
				                                GROUP BY DocId)TMP ON IVZ_HROUTOFFICE.DocId = TMP.DocId
                                WHERE   TRANDDATETIME = '{trandate}'
                                        AND IVZ_HROUTOFFICE.OutType = '1'
		                                AND	IVZ_HROUTOFFICE.HrApprovedOut = '2'
		                                AND	IVZ_HROUTOFFICE.HrApprovedIn = '1'
		                                AND IVZ_HROUTOFFICE.EmplCard = '{EmplId}'";

            //return ConnectionClass.SelectSQL_Main(sql);
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConHROS);
        }

        public static string ScanInSPC_Save(EmplScan EMPL,
            string maxDocnoRetail, int c_Retail, string maxDocnoWha, int c_Wha, string maxDocnoWhRice, int c_WhRice)
        {
            string sql = $@"
            INSERT INTO SHOP_SCANINSPC(
	                    DOCID, EMPLOUT, EMPLID, EMPLNAME, 
			            BRANCHID, BRANCHNAME, [DESCRIPTION],
	                    PRODUCTLIST, PETROBILL,
                        RETAILAREA,RETAILAREA_COPY,WHA,WHA_COPY,WHRICE,WHRICE_COPY,PAYHOTEL,PAYADDON
                        )
                        VALUES(
                        '{EMPL.DOCID}', '{EMPL.EMPLOUT}', '{EMPL.EMPLID}', '{EMPL.EMPLNAME}',
                        '{EMPL.BRANCHID}', '{EMPL.DPT}', '{EMPL.POSITION}',
                        '{EMPL.PRODUCT}', '{EMPL.OIL}',
                        '{maxDocnoRetail}','{c_Retail}','{maxDocnoWha}','{c_Wha}','{maxDocnoWhRice}','{c_WhRice}','{EMPL.Pay_Hotel}','{EMPL.Pay_AddOn}')";

            return sql;
        }

        public static string ScanInSPC_Update(string EMPLID, string dateTimeScanIns, string Assistant, string docno_Retail, string docno_Wha, string docno_Rice)
        {
            TimeSpan timeSpan = DateTime.Now - DateTime.Parse(dateTimeScanIns);
            string calTime = timeSpan.Days.ToString().PadLeft(2, '0') + " : " + timeSpan.Hours.ToString().PadLeft(2, '0') + " : " + timeSpan.Minutes.ToString().PadLeft(2, '0');

            string sql = $@"
                    SELECT	COUNT(*) AS C_Count	 FROM	SHOP_MNPO_DT WITH (NOLOCK)
                    WHERE	MNPODocNo IN( '{docno_Retail}','{docno_Wha}','{docno_Rice}')
                            AND MNPOBarcode != 'MN0002' 
            ";
            string count_Listproduct = ConnectionClass.SelectSQL_Main(sql).Rows[0]["C_Count"].ToString();

            return $@"
            UPDATE	SHOP_SCANINSPC
            SET		SCANOUTTIME = GETDATE(),
                    CALTIME = '{calTime}',
                    STATUS = '1',
                    LISTPRODUCT = '{count_Listproduct}'
                    {Assistant}
            WHERE	EMPLID = '{EMPLID}'
                    AND STATUS = '0'";
        }

        public static DataTable ScanInSPC_Detail(string pCase, string datestart, string dateend)
        {
            string sql = "";
            switch (pCase)
            {
                case "0": //TIMEKEEPERREPORT
                    sql = $@"SELECT	CONVERT(NVARCHAR,TRANDATE,23) AS TRANDATE,DOCID,EMPLOUT,EMPLID,EMPLNAME,
		                            BRANCHID,BRANCHNAME,DESCRIPTION,SCANINTIME,ISNULL(SCANOUTTIME, '') AS SCANOUTTIME,ISNULL(LISTPRODUCT,0) AS LISTPRODUCT,
		                            CALTIME,PRODUCTLIST,PETROBILL,PAYHOTEL,PAYADDON,EMPLIDREPLESE,STATUS,RETAILAREA,RETAILAREA_COPY,WHA,WHA_COPY,WHRICE,WHRICE_COPY
                            FROM	[SHOP_SCANINSPC] WITH (NOLOCK)
                            WHERE	TRANDATE BETWEEN '{datestart}' AND '{dateend}'
                            ORDER BY SCANINTIME DESC";
                    break;
                case "1": //D054 REPORT
                    sql = $@"SELECT	BRANCHID,BRANCHNAME,TRANDATE,COUNT(TRANDATE) AS C_COUNT
                            FROM	SHOP_SCANINSPC WITH (NOLOCK)
                            WHERE	TRANDATE BETWEEN '{datestart}' AND '{dateend}'
                            GROUP BY BRANCHID,BRANCHNAME,TRANDATE";
                    break;
                case "2"://datestartในตกรณีนี้เป็น condition เพิ่ม
                    sql = $@"SELECT	DOCID,EMPLOUT,EMPLID
		                            ,EMPLNAME,BRANCHID,BRANCHNAME,DESCRIPTION,TRANDATE
		                            ,SCANINTIME,ISNULL(SCANOUTTIME, '') AS SCANOUTTIME,PRODUCTLIST
		                            ,PETROBILL,BILLSHOP,LISTPRODUCT
                            FROM	[SHOP_SCANINSPC] WITH (NOLOCK)
                            WHERE	{datestart}";
                    break;
                default:
                    break;
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #endregion

        #region TimeKeeper
        //SQL 1.30ชม
        public static int SEQ_SCANFINGER_TAFFDT()
        {
            string Sql = $@"
            SELECT  case MAX(isnull(SEQ,'0')) when '' then '1' else SEQ +1 end as SEQ
            FROM    SCANFINGER_TAFFDT  WITH (NOLOCK)
            WHERE   SPCNAME = '{Environment.MachineName}'
                    AND CONVERT(VARCHAR, CONVERTDATE,23)= convert(varchar, getdate(), 23)
                    AND SCANID = '{SystemClass.SystemBranchID}'
            GROUP BY  SPCNAME,CONVERT(VARCHAR, CONVERTDATE, 23),SCANID,SEQ
            ORDER BY SEQ DESC";

            DataTable Dt = ConnectionClass.SelectSQL_SentServer(Sql, IpServerConnectClass.ConHROS);
            int seq = 1;
            if (Dt.Rows.Count > 0)
            {
                seq = int.Parse(Dt.Rows[0]["SEQ"].ToString());
            }
            return seq;
        }
        //SQL ช่าง
        public static int SEQ_SPC_WS_TAFFDTNEWSITE_SHOP24()
        {
            string Sql = $@" 
                SELECT  count(SEQ)+1 as SEQ  
                FROM    SPC_WS_TAFFDTNEWSITE_SHOP24  WITH (NOLOCK)
                WHERE   LOCATIONDATA='2'  AND  convert(varchar,CONVERTDATE,23) =convert(varchar, getdate() ,23)    
                        and scanid='{SystemClass.SystemBranchID}'  
                ORDER BY  seq DESC ";

            DataTable Dt = ConnectionClass.SelectSQL_SentServer(Sql, IpServerConnectClass.ConHROS);
            int seq = 1;
            if (Dt.Rows.Count > 0)
            {
                seq = int.Parse(Dt.Rows[0]["SEQ"].ToString());
            }
            return seq;
        }
        //Insert TimeKeeper
        public static string Save_TIMEKEEPER(string _pPageCase, Data_EMPLTABLE Empl, int BREAKCOUNT, string PW_IDCARD, string PW_TYPE)//BREAKCOUNT,PW_IDCARD สำหรับ 2/3/6
        {
            string sql;
            switch (_pPageCase)
            {
                case "1"://ลงเวลาเข้างาน
                    sql = $@"
                    INSERT INTO  SHOP_TIMEKEEPER   
                                (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
                                PW_WHOIN,PW_POSITION,PW_TYPE)  
                    VALUES  ('{Empl.EMPLTABLE_EMPLID_M}','{Empl.EMPLTABLE_ALTNUM}','{Empl.EMPLTABLE_SPC_NAME}','{Controllers.SystemClass.SystemBranchID}',
                            '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_POSSITION}','{PW_TYPE}')";
                    return ConnectionClass.ExecuteSQL_Main(sql);
                case "2"://ลงเวลาพัก
                    sql = $@"
                    INSERT INTO SHOP_TIMEBREAK   
                                (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
                                PW_WHOIN,PW_POSITION,PW_Count,PW_IDTimeKeeper ) 
                    VALUES ('{Empl.EMPLTABLE_EMPLID_M}', '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_SPC_NAME}', '{SystemClass.SystemBranchID}', 
                            '{Empl.EMPLTABLE_EMPLID}','{Empl.EMPLTABLE_POSSITION}','{BREAKCOUNT}','{PW_IDCARD}')";
                    return ConnectionClass.ExecuteSQL_Main(sql);
                case "3"://1.30
                    int seq = SEQ_SCANFINGER_TAFFDT();
                    sql = $@"INSERT INTO  SCANFINGER_TAFFDT  
                                                (NAME, SPCNAME, SEQ, PWCARD, TAFFDATESCAN,
                                                TAFFTIMESCAN, CONVERTDATE, CONVERTDATETIME, SCANID, INOUT,
                                                CREATENAME, DATESAVE, DATETIMESAVE, LOCATIONDATA)
                                                values(UPPER(REPLACE(Convert(Varchar, GetDate(), 6), ' ', '')) + '.TAFF',
                                                '{Environment.MachineName}', '{seq}', '{PW_IDCARD}',
                                                Convert(Varchar, GetDate(), 12), substring(replace(Convert(Varchar, GetDate(), 8), ':', ''), 1, 4),
                                                convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', 
                                                '{SystemClass.SystemBranchID}', 'I', '{System.Security.Principal.WindowsIdentity.GetCurrent().Name}',
                                                convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', '2') ";

                    return ConnectionClass.ExecuteSQL_SentServer(sql, IpServerConnectClass.ConHROS);
                case "4":// ตรวจสอบปริมาณพนักงาน
                    sql = $@" 
                    INSERT INTO SHOP_TIMEKEEPERCHECK 
                                (ALTNUM,EMPLID, EMPLNAME,
                                BRANCH_ID,BRANCH_NAME,WHOINS,WHONAMEINS)  
                    VALUES      ('{Empl.EMPLTABLE_ALTNUM}','{Empl.EMPLTABLE_EMPLID_M}', '{Empl.EMPLTABLE_SPC_NAME}',
                                '{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";
                    return ConnectionClass.ExecuteSQL_Main(sql);
                case "5"://PC
                    sql = $@"
                    INSERT INTO SHOP_TIMEKEEPERPC   (PW_IDCARD, PW_NAME, PW_BRANCH_IN,PW_WHOIN)
                    VALUES  ('{Empl.EMPLTABLE_EMPLID}','{Empl.EMPLTABLE_SPC_NAME}', '{SystemClass.SystemBranchID}','{SystemClass.SystemUserID}')";
                    return ConnectionClass.ExecuteSQL_Main(sql);
                case "6":// สแกนช่างก่อสร้าง.
                    int seq6 = SEQ_SPC_WS_TAFFDTNEWSITE_SHOP24();
                    sql = $@"INSERT INTO  SPC_WS_TAFFDTNEWSITE_SHOP24  
                                                (NAME, SPCNAME, SEQ, PWCARD, TAFFDATESCAN,
                                                TAFFTIMESCAN, CONVERTDATE, CONVERTDATETIME, SCANID, INOUT,
                                                CREATENAME, DATESAVE, DATETIMESAVE, LOCATIONDATA)
                                                values(UPPER(REPLACE(Convert(Varchar, GetDate(), 6), ' ', '')) + '.TAFF',
                                                '{Environment.MachineName}', '{seq6}', '{PW_IDCARD}',
                                                Convert(Varchar, GetDate(), 12), substring(replace(Convert(Varchar, GetDate(), 8), ':', ''), 1, 4),
                                                convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', 
                                                '{SystemClass.SystemBranchID}', 'I', '{System.Security.Principal.WindowsIdentity.GetCurrent().Name}',
                                                convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', '2')";
                    return ConnectionClass.ExecuteSQL_SentServer(sql, IpServerConnectClass.ConHROS);
                case "7"://สแกนช่างซ่อมต่างๆเข้าสาขา [IN]
                    sql = $@"Insert Into SHOP_TIMEKEEPER_TACHNICIAN
                                        (PW_IDCARD, PW_NAME, PW_BRANCH_IN,PW_BRANCHNAME_IN, PW_DATE_IN, PW_TIME_IN,
                                        PW_DATETIME_IN, PW_STATUS, PW_TYPE, PW_DATEIN, PW_TIMEIN, PW_WHOIN)
                                         values
                                        ('{Empl.EMPLTABLE_EMPLID}','{Empl.EMPLTABLE_SPC_NAME}', '{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}',
                                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), convert(varchar, getdate(), 25),
                                        '1', '1', convert(varchar, getdate(), 23), convert(varchar, getdate(), 24),'{SystemClass.SystemUserID}')";
                    return ConnectionClass.ExecuteSQL_Main(sql);

                default:
                    return "";
            }

        }
        //Update  TimeKeeper
        public static string Update_TIMEKEEPER(string _pPageCase, string PW_ID, string SpanDay, string SpanTime, string Bill, string Money, string rmk)
        {
            string sqlUp;
            switch (_pPageCase)
            {
                case "1":
                    string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNEP", "-", "MNEP", "1");
                    sqlUp = $@"UPDATE SHOP_TIMEKEEPER 
                        SET    PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
                               PW_DATE_OUT =convert(varchar,getdate(),23), 
                               PW_TIME_OUT = convert(varchar, getdate(), 24),
                               PW_DATETIME_OUT = convert(varchar, getdate(), 25), 
                               PW_SUMDATE = '{SpanDay}', 
                               PW_SUMTIME = '{SpanTime}',
                               PW_DATEOUT = convert(varchar, getdate(), 23), 
                               PW_TIMEOUT = convert(varchar, getdate(), 24),
                               PW_WHOOUT = '{SystemClass.SystemUserID}', 
                               PW_STATUS = '0',
                               PW_QTYBill = '{Bill}',
                               PW_MONEYUSEIN = '{Money}',
                               PW_MNEP =  '{maxDocno}'
                        WHERE  PW_ID =  '{PW_ID}' ";
                    return ConnectionClass.ExecuteSQL_Main(sqlUp);
                case "2":
                    sqlUp = $@" 
                    UPDATE  SHOP_TIMEBREAK 
                    SET     PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
                            PW_DATE_OUT =convert(varchar,getdate(),23), 
                            PW_TIME_OUT = convert(varchar, getdate(), 24),
                            PW_DATETIME_OUT = convert(varchar, getdate(), 25),
                            PW_SUMDATE = '{SpanDay}', 
                            PW_SUMTIME = '{SpanTime}',
                            PW_DATEOUT = convert(varchar, getdate(), 23), 
                            PW_TIMEOUT = convert(varchar, getdate(), 24),  
                            PW_WHOOUT = '{SystemClass.SystemUserID}', 
                            PW_STATUS = '0',
                            PW_REMARK = '{rmk}'
                    WHERE PW_ID = '{PW_ID}' ";
                    return ConnectionClass.ExecuteSQL_Main(sqlUp);
                case "5":
                    sqlUp = $@"
                        UPDATE SHOP_TIMEKEEPERPC
                        SET    PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
                                PW_DATE_OUT = convert(varchar, getdate(), 23),
                                PW_TIME_OUT = convert(varchar, getdate(), 24),
                                PW_DATETIME_OUT = convert(varchar, getdate(), 25),
                                PW_SUMDATE = '{SpanDay}',
                                PW_SUMTIME = '{SpanTime}',
                                PW_DATEOUT = convert(varchar, getdate(), 23),
                                PW_TIMEOUT = convert(varchar, getdate(), 24),
                                PW_WHOOUT = '{SystemClass.SystemUserID_M}',
                                PW_STATUS = '0'
                        WHERE   PW_ID = '{PW_ID}' ";
                    return ConnectionClass.ExecuteSQL_Main(sqlUp);
                default:
                    return "";
            }
        }

        public static DataTable Scan_ReportTAFF(string ALTNUM)
        {
            DateTime now = DateTime.Now;
            //DateTime now = new DateTime(2023, 2, 2);
            string DD = now.ToString("dd");
            string mm = now.ToString("MM");
            string YY = now.ToString("yyyy");

            string str = $@"
                    SELECT	*
                    FROM	[PWTIME2] WITH (NOLOCK)
                    WHERE	PWTIME0 = '{ALTNUM}'
                            AND PWYEAR = '{YY}'
		                    AND PWMONTH = '{mm}'
                            AND PWTIME{DD}1 = ''
                    ";
            return ConnectionClass.SelectSQL_SentServer(str, IpServerConnectClass.ConHROS);
        }
        #endregion
    }
}
