﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare;
using PC_Shop24Hrs.FormShare.ShowData;
using System;
using System.Data;
using System.Windows.Forms;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public partial class CheckSalary : Telerik.WinControls.UI.RadForm
    {
        Data_EMPLTABLE Empl;
        DateTime ShowData;

        //Load
        public CheckSalary()
        {
            InitializeComponent();
        }

        //Load
        private void CheckSalary_Load(object sender, EventArgs e)
        {
            ClearDefault();
        }

        //Clear Data
        void ClearDefault()
        {
            RadTextBox_Empl.Text = string.Empty;
            RadTextBox_Empl.Enabled = true;

            radLabel_Name.Text = "";
            radLabel_Dept.Text = "";
            radLabel_Possion.Text = "";

            RadTextBox_salary.Text = string.Empty;
            RadTextBox_salaryDetail.Text = string.Empty;

            RadTextBox_Money.Text = string.Empty;

            RadTextBox_salary.Enabled = false;
            RadTextBox_salaryDetail.Enabled = false;
            RadTextBox_Money.Enabled = false;

            Cursor.Current = Cursors.Default;
        }
 
        //Timer Clear Data
        private void Timer_Clear_Tick(object sender, EventArgs e)
        {
            if (ShowData.AddSeconds(6) < DateTime.Now)
            {
                ClearDefault();
                
                RadTextBox_Empl.Focus();
                Timer_Clear.Stop();
            }
        }

        private void RadTextBox_Empl_KeyDown(object sender, KeyEventArgs e)
        {
            //this.Cursor = Cursors.WaitCursor;
            if (e.KeyCode == Keys.Enter && RadTextBox_Empl.Text.Length == 7)
            {
                this.Empl = new Data_EMPLTABLE(RadTextBox_Empl.Text);
                if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                {
                    radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                    radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                    radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                    RadTextBox_Empl.Text = Empl.EMPLTABLE_EMPLID;
                    RadTextBox_Empl.Enabled = false;

                    //เช็คลายนิ้วมือผู้พิการ 
                    if (TimeKeeperClass.GetEmplScanCard_ByEmplBranch(this.Empl.EMPLTABLE_EMPLID, SystemClass.SystemBranchID.Replace("MN", "")))
                    {
                        FingerScan FingerScan = new FingerScan(this.Empl.EMPLTABLE_EMPLID, "รายได้ล่าสุด", radLabel_Name.Text, radLabel_Possion.Text, radLabel_Dept.Text);

                        if (FingerScan.ShowDialog() != DialogResult.Yes)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                            ClearDefault();
                            RadTextBox_Empl.Focus();
                            Cursor.Current = Cursors.Default; 
                            return;
                        }
                    }

                    DataTable Dt = Models.EmplClass.GetSalary_ByEmplID(Empl.EMPLTABLE_EMPLID_M);
                    if (Dt.Rows.Count > 0)
                    {
                        RadTextBox_salary.Text = Dt.Rows[0]["tTermCode"].ToString();
                        RadTextBox_salaryDetail.Text = Dt.Rows[0]["tTermDetail"].ToString();
                        RadTextBox_Money.Text = Convert.ToDecimal(Dt.Rows[0]["tEmpAmount"].ToString()).ToString("N2");
                        ShowData = DateTime.Now;
                        RadTextBox_Empl.Text = "";
                        Timer_Clear.Start();
                        Cursor.Current = Cursors.Default;
                    }
                    else
                    {
                        ClearDefault();
                        Cursor.Current = Cursors.Default;
                    }
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน");
                    ClearDefault();
                    Cursor.Current = Cursors.Default;
                    RadTextBox_Empl.SelectAll();
                }

            }
            this.Cursor = Cursors.Default;
        }
 

        //Check Number Only
        private void RadTextBox_Empl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
    }

}