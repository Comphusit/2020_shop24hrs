﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public partial class ConfirmCheckTimekeep : Telerik.WinControls.UI.RadForm
    {
        public string EmpID;
        public string EmpName;

        readonly string _pPermissionCheck;//0 คือ การเช็คตำแหน่ง 1 คือการเช็คสิทธิ์ในการเช็คสต็อก
        public ConfirmCheckTimekeep(string pPermissionCheck)
        {
            InitializeComponent();
            _pPermissionCheck = pPermissionCheck;
        }
        //load
        private void ConfirmCheckTimekeep_Load(object sender, EventArgs e)
        {
            radTextBox_EmpID.Text = "";
            radTextBox_PassWord.Text = "";

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radTextBox_EmpID.SelectAll();
            radTextBox_EmpID.Focus();
        }
        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //OK
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_EmpID.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสผู้ใช้ก่อน.");
                return;
            }
            if (string.IsNullOrEmpty(radTextBox_PassWord.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสผ่านก่อนเท่านั้น.");
                return;
            }
            CheckPassword(radTextBox_PassWord.Text);
        }
        //Enter Pass
        private void RadTextBox_PassWord_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (string.IsNullOrEmpty(radTextBox_PassWord.Text))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสผ่านก่อน Enter.");
                        return;
                    }
                    CheckPassword(radTextBox_PassWord.Text);
                    break;
                default:
                    break;
            }
        }
        //Enter Emp
        private void RadTextBox_EmpID_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (string.IsNullOrEmpty(radTextBox_EmpID.Text))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสผู้ใช้ก่อน Enter.");
                        return;
                    }
                    radTextBox_PassWord.SelectAll();
                    radTextBox_PassWord.Focus();
                    break;
                default:
                    break;
            }
        }
        //Number Only
        private void RadTextBox_EmpID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Check user+Pass+Brannh
        private void CheckPassword(String Password)
        {
            DataTable DtEmpl = Class.Models.EmplClass.GetEmployee(radTextBox_EmpID.Text);
            if (DtEmpl.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบรหัสพนักงานที่ระบุ ลองใหม่อีกครั้ง.");
                radTextBox_EmpID.Text = "";
                radTextBox_PassWord.Text = "";
                radTextBox_EmpID.Focus();
                return;
            }

            string Pass = Class.EncryptDataClass.DecodePassword(DtEmpl.Rows[0]["EMP_PASSWORD"].ToString());
            if (Pass != Password)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสผ่านไม่ถูกต้อง ลองใหม่อีกครั้ง.");
                radTextBox_PassWord.Text = "";
                radTextBox_PassWord.Focus();
                return;
            }

            switch (_pPermissionCheck)
            {
                case "0"://สิทธิ์ ผช.+ผจก
                    if (DtEmpl.Rows[0]["IVZ_HRPAResignationDate"].ToString() != "1900-01-01")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("พนักงานทีสถานะลาออกเรียบร้อยแล้ว ไม่สามารถเข้าใช้งานโปรแกรมได้.");
                        radTextBox_EmpID.Text = "";
                        radTextBox_PassWord.Text = "";
                        radTextBox_EmpID.Focus();
                        return;
                    }

                    if (DtEmpl.Rows[0]["IVZ_HROMPositionTitle"].ToString() == "03")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีสิทธิ์ในการบันทึกข้อมูล ต้องเป็นผู้จัดการหรือผู้ช่วยเท่านั้น");
                        radTextBox_EmpID.Text = "";
                        radTextBox_PassWord.Text = "";
                        radTextBox_EmpID.Focus();
                        return;
                    }
                    else
                    {
                        EmpID = DtEmpl.Rows[0]["EMPLID"].ToString();
                        EmpName = DtEmpl.Rows[0]["SPC_NAME"].ToString();
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    break;
                case "1"://สิทธิ์การเช็คสต็อก
                    if (DtEmpl.Rows[0]["EMP_TRANSTOCK"].ToString() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสผู้ใช้นี้ไม่มีสิทธิ์ในการลงรายการบัญชี" + Environment.NewLine + "สามารถติดต่อขอสิทธิ์เพิ่มเติมได้ที่ Centershop Tel.1022");
                        radTextBox_EmpID.Text = "";
                        radTextBox_PassWord.Text = "";
                        radTextBox_EmpID.Focus();
                        return;
                    }
                    else
                    {
                        EmpID = DtEmpl.Rows[0]["EMPLID"].ToString();
                        EmpName = DtEmpl.Rows[0]["SPC_NAME"].ToString();
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    break;
                default:
                    break;
            }


        }
    }
}
