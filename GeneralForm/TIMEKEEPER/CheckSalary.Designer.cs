﻿namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    partial class CheckSalary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckSalary));
            this.Timer_Clear = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_Empl = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Possion = new Telerik.WinControls.UI.RadLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RadTextBox_Money = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_salaryDetail = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_salary = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Money)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_salaryDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_salary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Timer_Clear
            // 
            this.Timer_Clear.Tick += new System.EventHandler(this.Timer_Clear_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadTextBox_Empl);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.radLabel_Dept);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel_Possion);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.RadLabel);
            this.panel1.Controls.Add(this.radLabel_Name);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 219);
            this.panel1.TabIndex = 3;
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.AutoSize = false;
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dept.Location = new System.Drawing.Point(102, 122);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(334, 23);
            this.radLabel_Dept.TabIndex = 31;
            this.radLabel_Dept.Text = "แผนก";
            this.radLabel_Dept.Visible = false;
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(3, 11);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(103, 23);
            this.radLabel7.TabIndex = 90;
            this.radLabel7.Text = "รหัสพนักงาน";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel3.Location = new System.Drawing.Point(12, 122);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(53, 23);
            this.radLabel3.TabIndex = 27;
            this.radLabel3.Text = "แผนก";
            this.radLabel3.Visible = false;
            // 
            // RadTextBox_Empl
            // 
            this.RadTextBox_Empl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_Empl.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Empl.Location = new System.Drawing.Point(105, 12);
            this.RadTextBox_Empl.MaxLength = 7;
            this.RadTextBox_Empl.Name = "RadTextBox_Empl";
            this.RadTextBox_Empl.Size = new System.Drawing.Size(140, 25);
            this.RadTextBox_Empl.TabIndex = 1;
            this.RadTextBox_Empl.Tag = "รหัสพนักงาน";
            this.RadTextBox_Empl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Empl_KeyDown);
            this.RadTextBox_Empl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Empl_KeyPress);
            // 
            // radLabel_Possion
            // 
            this.radLabel_Possion.AutoSize = false;
            this.radLabel_Possion.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Possion.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Possion.Location = new System.Drawing.Point(102, 88);
            this.radLabel_Possion.Name = "radLabel_Possion";
            this.radLabel_Possion.Size = new System.Drawing.Size(334, 23);
            this.radLabel_Possion.TabIndex = 30;
            this.radLabel_Possion.Text = "ตำแหน่ง";
            this.radLabel_Possion.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.RadTextBox_Money);
            this.groupBox2.Controls.Add(this.radLabel6);
            this.groupBox2.Controls.Add(this.radLabel1);
            this.groupBox2.Controls.Add(this.radLabel2);
            this.groupBox2.Controls.Add(this.RadTextBox_salaryDetail);
            this.groupBox2.Controls.Add(this.RadTextBox_salary);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(3, 49);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(466, 158);
            this.groupBox2.TabIndex = 88;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รายได้ล่าสุด";
            // 
            // RadTextBox_Money
            // 
            this.RadTextBox_Money.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_Money.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Money.Location = new System.Drawing.Point(102, 125);
            this.RadTextBox_Money.Name = "RadTextBox_Money";
            this.RadTextBox_Money.ReadOnly = true;
            this.RadTextBox_Money.Size = new System.Drawing.Size(171, 25);
            this.RadTextBox_Money.TabIndex = 2;
            this.RadTextBox_Money.Tag = "รหัสพนักงาน";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(9, 124);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(66, 23);
            this.radLabel6.TabIndex = 91;
            this.radLabel6.Text = "ยอดเงิน";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(8, 34);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(68, 23);
            this.radLabel1.TabIndex = 89;
            this.radLabel1.Text = "รหัสงวด";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(279, 129);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(40, 23);
            this.radLabel2.TabIndex = 88;
            this.radLabel2.Text = "บาท";
            // 
            // RadTextBox_salaryDetail
            // 
            this.RadTextBox_salaryDetail.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_salaryDetail.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_salaryDetail.Location = new System.Drawing.Point(102, 63);
            this.RadTextBox_salaryDetail.Multiline = true;
            this.RadTextBox_salaryDetail.Name = "RadTextBox_salaryDetail";
            this.RadTextBox_salaryDetail.ReadOnly = true;
            // 
            // 
            // 
            this.RadTextBox_salaryDetail.RootElement.StretchVertically = true;
            this.RadTextBox_salaryDetail.Size = new System.Drawing.Size(334, 53);
            this.RadTextBox_salaryDetail.TabIndex = 1;
            this.RadTextBox_salaryDetail.Tag = "รหัสพนักงาน";
            // 
            // RadTextBox_salary
            // 
            this.RadTextBox_salary.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadTextBox_salary.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_salary.Location = new System.Drawing.Point(102, 28);
            this.RadTextBox_salary.Name = "RadTextBox_salary";
            this.RadTextBox_salary.ReadOnly = true;
            this.RadTextBox_salary.Size = new System.Drawing.Size(140, 25);
            this.RadTextBox_salary.TabIndex = 0;
            this.RadTextBox_salary.Tag = "รหัสพนักงาน";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel4.Location = new System.Drawing.Point(12, 88);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(70, 23);
            this.radLabel4.TabIndex = 28;
            this.radLabel4.Text = "ตำแหน่ง";
            this.radLabel4.Visible = false;
            // 
            // RadLabel
            // 
            this.RadLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadLabel.Location = new System.Drawing.Point(12, 58);
            this.RadLabel.Name = "RadLabel";
            this.RadLabel.Size = new System.Drawing.Size(31, 23);
            this.RadLabel.TabIndex = 26;
            this.RadLabel.Text = "ชื่อ";
            this.RadLabel.Visible = false;
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(102, 58);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(334, 23);
            this.radLabel_Name.TabIndex = 29;
            this.radLabel_Name.Text = "ชื่อ";
            this.radLabel_Name.Visible = false;
            // 
            // CheckSalary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 219);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CheckSalary";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ตรวจสอบเงินเดือน";
            this.Load += new System.EventHandler(this.CheckSalary_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Money)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_salaryDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_salary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer Timer_Clear;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Empl;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Money;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_salaryDetail;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_salary;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Possion;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel RadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
    }
}
