﻿namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    partial class TimeKeeperForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeKeeperForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2_header = new System.Windows.Forms.Panel();
            this.radButton_refresh = new Telerik.WinControls.UI.RadButton();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_BreakTime = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Possion = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Empl = new Telerik.WinControls.UI.RadTextBox();
            this.panel_SubSave = new System.Windows.Forms.Panel();
            this.radLabel_Bath = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_SubCancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_SubSave = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Money = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Money = new Telerik.WinControls.UI.RadTextBox();
            this.panel_Detail = new System.Windows.Forms.Panel();
            this.radLabel_EmplIdConf = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_EmplIdConf = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.panel_Save = new System.Windows.Forms.Panel();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2_header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_refresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BreakTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).BeginInit();
            this.panel_SubSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Bath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SubCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SubSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Money)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Money)).BeginInit();
            this.panel_Detail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplIdConf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplIdConf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            this.panel_Save.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 570);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel2_header, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel_SubSave, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel_Detail, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel_Save, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(467, 570);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel2_header
            // 
            this.panel2_header.Controls.Add(this.pictureBox1);
            this.panel2_header.Controls.Add(this.radButton_refresh);
            this.panel2_header.Controls.Add(this.radButton_pdt);
            this.panel2_header.Controls.Add(this.radLabel_BreakTime);
            this.panel2_header.Controls.Add(this.radLabel_Dept);
            this.panel2_header.Controls.Add(this.radLabel3);
            this.panel2_header.Controls.Add(this.radLabel_Possion);
            this.panel2_header.Controls.Add(this.radLabel2);
            this.panel2_header.Controls.Add(this.radLabel_Name);
            this.panel2_header.Controls.Add(this.radLabel1);
            this.panel2_header.Controls.Add(this.RadLabel);
            this.panel2_header.Controls.Add(this.radTextBox_Empl);
            this.panel2_header.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2_header.Location = new System.Drawing.Point(33, 3);
            this.panel2_header.Name = "panel2_header";
            this.panel2_header.Size = new System.Drawing.Size(431, 154);
            this.panel2_header.TabIndex = 0;
            // 
            // radButton_refresh
            // 
            this.radButton_refresh.BackColor = System.Drawing.Color.Transparent;
            this.radButton_refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_refresh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_refresh.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_refresh.Location = new System.Drawing.Point(69, 33);
            this.radButton_refresh.Name = "radButton_refresh";
            this.radButton_refresh.Size = new System.Drawing.Size(26, 26);
            this.radButton_refresh.TabIndex = 73;
            this.radButton_refresh.Text = "radButton3";
            this.radButton_refresh.Click += new System.EventHandler(this.RadButton_refresh_Click);
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(400, 5);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 72;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_BreakTime
            // 
            this.radLabel_BreakTime.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_BreakTime.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BreakTime.Location = new System.Drawing.Point(206, 40);
            this.radLabel_BreakTime.Name = "radLabel_BreakTime";
            this.radLabel_BreakTime.Size = new System.Drawing.Size(79, 19);
            this.radLabel_BreakTime.TabIndex = 26;
            this.radLabel_BreakTime.Text = "BreakTime";
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dept.Location = new System.Drawing.Point(101, 117);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(43, 19);
            this.radLabel_Dept.TabIndex = 25;
            this.radLabel_Dept.Text = "แผนก";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(11, 117);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(43, 19);
            this.radLabel3.TabIndex = 22;
            this.radLabel3.Text = "แผนก";
            // 
            // radLabel_Possion
            // 
            this.radLabel_Possion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Possion.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Possion.Location = new System.Drawing.Point(101, 92);
            this.radLabel_Possion.Name = "radLabel_Possion";
            this.radLabel_Possion.Size = new System.Drawing.Size(57, 19);
            this.radLabel_Possion.TabIndex = 24;
            this.radLabel_Possion.Text = "ตำแหน่ง";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(11, 92);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(57, 19);
            this.radLabel2.TabIndex = 22;
            this.radLabel2.Text = "ตำแหน่ง";
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(101, 67);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(25, 19);
            this.radLabel_Name.TabIndex = 23;
            this.radLabel_Name.Text = "ชื่อ";
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.Color.Transparent;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(101, 9);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(130, 19);
            this.radLabel1.TabIndex = 22;
            this.radLabel1.Text = "ระบุพนักงาน[Enter]";
            // 
            // RadLabel
            // 
            this.RadLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel.Location = new System.Drawing.Point(11, 67);
            this.RadLabel.Name = "RadLabel";
            this.RadLabel.Size = new System.Drawing.Size(25, 19);
            this.RadLabel.TabIndex = 21;
            this.RadLabel.Text = "ชื่อ";
            // 
            // radTextBox_Empl
            // 
            this.radTextBox_Empl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Empl.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Empl.Location = new System.Drawing.Point(101, 34);
            this.radTextBox_Empl.MaxLength = 7;
            this.radTextBox_Empl.Name = "radTextBox_Empl";
            this.radTextBox_Empl.Size = new System.Drawing.Size(100, 25);
            this.radTextBox_Empl.TabIndex = 4;
            this.radTextBox_Empl.Text = "1106163";
            this.radTextBox_Empl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Empl_KeyDown);
            // 
            // panel_SubSave
            // 
            this.panel_SubSave.Controls.Add(this.radLabel_Bath);
            this.panel_SubSave.Controls.Add(this.RadButton_SubCancel);
            this.panel_SubSave.Controls.Add(this.RadButton_SubSave);
            this.panel_SubSave.Controls.Add(this.radLabel_Money);
            this.panel_SubSave.Controls.Add(this.radTextBox_Money);
            this.panel_SubSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_SubSave.Location = new System.Drawing.Point(33, 353);
            this.panel_SubSave.Name = "panel_SubSave";
            this.panel_SubSave.Size = new System.Drawing.Size(431, 214);
            this.panel_SubSave.TabIndex = 3;
            // 
            // radLabel_Bath
            // 
            this.radLabel_Bath.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Bath.Location = new System.Drawing.Point(195, 62);
            this.radLabel_Bath.Name = "radLabel_Bath";
            this.radLabel_Bath.Size = new System.Drawing.Size(33, 19);
            this.radLabel_Bath.TabIndex = 22;
            this.radLabel_Bath.Text = "บาท";
            this.radLabel_Bath.Visible = false;
            // 
            // RadButton_SubCancel
            // 
            this.RadButton_SubCancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_SubCancel.Location = new System.Drawing.Point(237, 3);
            this.RadButton_SubCancel.Name = "RadButton_SubCancel";
            this.RadButton_SubCancel.Size = new System.Drawing.Size(130, 32);
            this.RadButton_SubCancel.TabIndex = 1;
            this.RadButton_SubCancel.Text = "ยกเลิก";
            this.RadButton_SubCancel.ThemeName = "Fluent";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SubCancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SubCancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubCancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubCancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubCancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubCancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubCancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_SubSave
            // 
            this.RadButton_SubSave.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_SubSave.Location = new System.Drawing.Point(101, 3);
            this.RadButton_SubSave.Name = "RadButton_SubSave";
            this.RadButton_SubSave.Size = new System.Drawing.Size(130, 32);
            this.RadButton_SubSave.TabIndex = 0;
            this.RadButton_SubSave.Text = "บันทึก";
            this.RadButton_SubSave.ThemeName = "Fluent";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SubSave.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SubSave.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SubSave.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubSave.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SubSave.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_Money
            // 
            this.radLabel_Money.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Money.Location = new System.Drawing.Point(16, 41);
            this.radLabel_Money.Name = "radLabel_Money";
            this.radLabel_Money.Size = new System.Drawing.Size(57, 19);
            this.radLabel_Money.TabIndex = 19;
            this.radLabel_Money.Text = "MONEY";
            this.radLabel_Money.Visible = false;
            // 
            // radTextBox_Money
            // 
            this.radTextBox_Money.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Money.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Money.Location = new System.Drawing.Point(16, 60);
            this.radTextBox_Money.Name = "radTextBox_Money";
            this.radTextBox_Money.Size = new System.Drawing.Size(173, 21);
            this.radTextBox_Money.TabIndex = 1;
            this.radTextBox_Money.Visible = false;
            // 
            // panel_Detail
            // 
            this.panel_Detail.Controls.Add(this.radLabel_EmplIdConf);
            this.panel_Detail.Controls.Add(this.radTextBox_EmplIdConf);
            this.panel_Detail.Controls.Add(this.radLabel_Remark);
            this.panel_Detail.Controls.Add(this.radTextBox_Remark);
            this.panel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Detail.Location = new System.Drawing.Point(33, 203);
            this.panel_Detail.Name = "panel_Detail";
            this.panel_Detail.Size = new System.Drawing.Size(431, 144);
            this.panel_Detail.TabIndex = 1;
            // 
            // radLabel_EmplIdConf
            // 
            this.radLabel_EmplIdConf.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_EmplIdConf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmplIdConf.Location = new System.Drawing.Point(101, 3);
            this.radLabel_EmplIdConf.Name = "radLabel_EmplIdConf";
            this.radLabel_EmplIdConf.Size = new System.Drawing.Size(155, 19);
            this.radLabel_EmplIdConf.TabIndex = 23;
            this.radLabel_EmplIdConf.Text = "รหัสพนักงาน กด[enter]";
            // 
            // radTextBox_EmplIdConf
            // 
            this.radTextBox_EmplIdConf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_EmplIdConf.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmplIdConf.Location = new System.Drawing.Point(101, 24);
            this.radTextBox_EmplIdConf.MaxLength = 7;
            this.radTextBox_EmplIdConf.Name = "radTextBox_EmplIdConf";
            this.radTextBox_EmplIdConf.Size = new System.Drawing.Size(127, 21);
            this.radTextBox_EmplIdConf.TabIndex = 22;
            this.radTextBox_EmplIdConf.Visible = false;
            this.radTextBox_EmplIdConf.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmplIdConf_KeyDown);
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Remark.Location = new System.Drawing.Point(101, 52);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(65, 19);
            this.radLabel_Remark.TabIndex = 21;
            this.radLabel_Remark.Text = "REMARK";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(101, 75);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(321, 49);
            this.radTextBox_Remark.TabIndex = 2;
            // 
            // panel_Save
            // 
            this.panel_Save.Controls.Add(this.RadButton_Cancel);
            this.panel_Save.Controls.Add(this.RadButton_Save);
            this.panel_Save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Save.Location = new System.Drawing.Point(33, 163);
            this.panel_Save.Name = "panel_Save";
            this.panel_Save.Size = new System.Drawing.Size(431, 34);
            this.panel_Save.TabIndex = 4;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(237, 1);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(145, 32);
            this.RadButton_Cancel.TabIndex = 1;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(78, 1);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(145, 32);
            this.RadButton_Save.TabIndex = 0;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(291, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 74;
            this.pictureBox1.TabStop = false;
            // 
            // TimeKeeperForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 570);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimeKeeperForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimeKeeperForm";
            this.Load += new System.EventHandler(this.TimeKeeperForm_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2_header.ResumeLayout(false);
            this.panel2_header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_refresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BreakTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).EndInit();
            this.panel_SubSave.ResumeLayout(false);
            this.panel_SubSave.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Bath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SubCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SubSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Money)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Money)).EndInit();
            this.panel_Detail.ResumeLayout(false);
            this.panel_Detail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplIdConf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplIdConf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            this.panel_Save.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel_Detail;
        private System.Windows.Forms.Panel panel2_header;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Empl;
        private Telerik.WinControls.UI.RadLabel RadLabel;
        private System.Windows.Forms.Panel panel_SubSave;
        protected Telerik.WinControls.UI.RadButton RadButton_SubCancel;
        protected Telerik.WinControls.UI.RadButton RadButton_SubSave;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel_Money;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Money;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Possion;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private System.Windows.Forms.Panel panel_Save;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel_Bath;
        private Telerik.WinControls.UI.RadLabel radLabel_BreakTime;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplIdConf;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmplIdConf;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
        private Telerik.WinControls.UI.RadButton radButton_refresh;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
