﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.FormShare;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public partial class TimeKeeperForm : Telerik.WinControls.UI.RadForm
    {
        readonly string _PageCase, _typeTime;
        Data_EMPLTABLE Empl;
        int BREAKCOUNT;
        string PW_ID, PW_IDCARD, PW_DATETIME_IN;
        string SpanDay, SpanTime, SpanMinutes;
        string PageCaseDesc;
        //1 : ลงเวลาเข้างาน
        //2 : ลงเวลาพัก
        //3 : สแกน1.30น.
        //4 : ตรวจสอบปริมาณพนักงาน
        //5 : PC
        //6 : สแกนช่างก่อสร้าง.
        //[ไม่เปิดใช้งาน] 7 : สแกนช่างซ่อมต่างๆเข้าสาขา 

        public TimeKeeperForm(string PageCase, string typeTime = "1") // 1 คือลงเวลา เข้าออกงาน ปกติ 2 คือ ลงเวลาเข้าออกงาน PartTime
        {
            InitializeComponent();
            _PageCase = PageCase;
            _typeTime = typeTime;
        }
        //Clear
        private void ClearDefualt(string PageCase)
        {
            radTextBox_Empl.Text = string.Empty;
            radLabel_BreakTime.Text = string.Empty;
            radLabel_Name.Text = string.Empty;
            radLabel_Possion.Text = string.Empty;
            radLabel_Dept.Text = string.Empty;

            radTextBox_EmplIdConf.Text = string.Empty;
            radLabel_EmplIdConf.Text = string.Empty;

            radTextBox_Money.Text = "0.00";
            radTextBox_Remark.Text = string.Empty;

            switch (PageCase)
            {
                case "1"://ลงเวลาเข้า-ออกงาน
                    radLabel_BreakTime.Visible = false;

                    RadButton_Save.Visible = true;
                    RadButton_Cancel.Visible = true;

                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;

                    radLabel_Remark.Text = "สิ่งของที่นำมา";

                    radTextBox_Empl.Enabled = true;

                    RadButton_Save.Text = "ลงเวลาเข้างาน";
                    RadButton_Cancel.Text = "ลงเวลาออกงาน";
                    RadButton_Save.Enabled = false;
                    RadButton_Cancel.Enabled = false;
                    this.Size = new System.Drawing.Size(475, 280);
                    PageCaseDesc = "ลงเวลาเข้า-ออกงาน";
                    if (_typeTime == "2") PageCaseDesc = $@"ลงเวลาเข้า-ออกงาน{Environment.NewLine}[PartTime]";
                    if (_typeTime == "3") PageCaseDesc = $@"ลงเวลาเข้า-ออกงาน{Environment.NewLine}[OT วันหยุด]";
                    break;
                case "2"://ลงเวลาพัก
                    radLabel_BreakTime.Visible = true;
                    radLabel_BreakTime.Text = string.Empty;

                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;

                    radLabel_Remark.Text = "เหตุผลที่พักเกิน";
                    radTextBox_Remark.Enabled = true;
                    radLabel_Remark.Visible = false;
                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;
                    radTextBox_Remark.Visible = false;

                    radTextBox_Empl.Enabled = true;

                    RadButton_Save.Text = "ลงเวลาออก พัก";
                    RadButton_Cancel.Text = "ลงเวลาเข้า พัก";
                    RadButton_Save.Enabled = false;
                    RadButton_Cancel.Enabled = false;
                    this.Size = new System.Drawing.Size(475, 280);
                    PageCaseDesc = "ลงเวลาพัก";
                    break;
                case "3"://สแกน 1.30 น.
                    radLabel_BreakTime.Visible = false;
                    RadButton_SubSave.Visible = false;
                    RadButton_SubCancel.Visible = false;

                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;

                    radLabel_Remark.Visible = false;
                    radTextBox_Remark.Visible = false;
                    radLabel_Bath.Visible = false;
                    radTextBox_Empl.Enabled = true;
                    RadButton_Save.Visible = false;
                    RadButton_Cancel.Visible = false;
                    this.Size = new System.Drawing.Size(475, 280);
                    PageCaseDesc = "สแกน 1.30 น.";
                    break;
                case "4"://ตรวจสอบปริมาณพนักงาน
                    radLabel_BreakTime.Visible = false;
                    RadButton_SubSave.Visible = false;
                    RadButton_SubCancel.Visible = false;
                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;

                    radLabel_Remark.Visible = false;
                    radTextBox_Remark.Visible = false;
                    radLabel_Bath.Visible = false;
                    radTextBox_Empl.Enabled = true;

                    RadButton_Save.Visible = false;
                    RadButton_Cancel.Visible = false;
                    this.Size = new System.Drawing.Size(475, 280);
                    PageCaseDesc = "ตรวจสอบปริมาณพนักงาน";
                    break;

                case "5"://ลงเวลาPC
                    radTextBox_Empl.MaxLength = 3;
                    radLabel_BreakTime.Visible = true;
                    RadButton_SubSave.Visible = false;
                    RadButton_SubCancel.Visible = false;
                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;
                    radLabel_Remark.Visible = false;
                    radTextBox_Remark.Visible = false;
                    radLabel_Bath.Visible = false;

                    radTextBox_Empl.Enabled = true;

                    RadButton_Save.Text = "ลงเวลาเข้า";
                    RadButton_Cancel.Text = "ลงเวลาออก";
                    RadButton_Save.Enabled = false;
                    RadButton_Cancel.Enabled = false;

                    this.Size = new System.Drawing.Size(475, 280);
                    PageCaseDesc = "ลงเวลา PC";
                    break;

                case "6"://สแกนช่างก่อสร้าง.
                    radLabel_BreakTime.Visible = false;
                    RadButton_SubSave.Visible = false;
                    RadButton_SubCancel.Visible = false;

                    radLabel_EmplIdConf.Visible = false;
                    radTextBox_EmplIdConf.Visible = false;

                    radLabel_Remark.Visible = false;
                    radTextBox_Remark.Visible = false;
                    radLabel_Bath.Visible = false;

                    radTextBox_Empl.Enabled = true;
                    RadButton_Save.Visible = false;
                    RadButton_Cancel.Visible = false;
                    this.Size = new System.Drawing.Size(475, 280);
                    PageCaseDesc = "สแกนช่างก่อสร้าง";
                    break;
                    //case "7"://สแกนช่างซ่อมทั่วไป. 
                    //    radLabel_BreakTime.Visible = false;
                    //    RadButton_SubSave.Visible = false;
                    //    RadButton_SubCancel.Visible = false;
                    //    radLabel_EmplIdConf.Visible = false;
                    //    radTextBox_EmplIdConf.Visible = false;
                    //    radLabel_Remark.Visible = false;
                    //    radTextBox_Remark.Visible = false;
                    //    radLabel_Bath.Visible = false;
                    //    radTextBox_Empl.Enabled = true;
                    //    RadButton_Save.Visible = false;
                    //    RadButton_Cancel.Visible = false;
                    //    this.Size = new System.Drawing.Size(475, 280);
                    //    PageCaseDesc = "สแกนช่างซ่อมทั่วไป";
                    //    break;
            }
        }
        //คำนวณเวลา
        private string GetTimeSpan(string DateBegin, string spanType)
        {
            string SpanResult = string.Empty;
            TimeSpan Span = DateTime.Now - DateTime.Parse(DateBegin);
            if (spanType == "DAY") SpanResult = Span.Days.ToString();
            else if (spanType == "MINUTES") SpanResult = Span.TotalMinutes.ToString();
            else if (spanType == "TIME")
            {
                if (Span.Hours.ToString().Length < 2) SpanResult = "0" + Span.Hours.ToString();
                else SpanResult = Span.Hours.ToString();

                if (Span.Minutes.ToString().Length == 1) SpanResult = SpanResult + ":0" + Span.Minutes.ToString();
                else SpanResult = SpanResult + ":" + Span.Minutes.ToString();
            }

            return SpanResult;
        }
        //Form Load
        private void TimeKeeperForm_Load(object sender, EventArgs e)
        {
            radButton_pdt.ButtonElement.ShowBorder = true; radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            RadButton_SubSave.ButtonElement.ShowBorder = true;
            RadButton_SubCancel.ButtonElement.ShowBorder = true;
            radButton_refresh.ButtonElement.ShowBorder = true;

            radTextBox_Empl.SelectAll();
            ClearDefualt(_PageCase);

        }
        //Enter EmplID
        private void RadTextBox_Empl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Cursor.Current = Cursors.WaitCursor;

                PW_ID = string.Empty;
                PW_IDCARD = string.Empty;
                PW_DATETIME_IN = string.Empty;
                SpanDay = string.Empty;
                SpanTime = string.Empty;
                SpanMinutes = string.Empty;

                switch (_PageCase)
                {
                    case "1"://ลงเวลาเข้างาน
                        //DataTable Dt_checkEmplIn = Class.Models.EmplClass.GetEmployee(radTextBox_Empl.Text);
                        DataTable Dt_checkEmplIn;
                        if (_typeTime == "2") Dt_checkEmplIn = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_Empl.Text, "5");
                        else if (_typeTime == "3")
                        {
                            Dt_checkEmplIn = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_Empl.Text, "6");

                            //if (Dt_checkEmplIn.Rows[0]["PW_TYPE"].ToString() != "3")
                            //{
                            //    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้.");
                            //    radTextBox_Empl.SelectAll();
                            //    Cursor.Current = Cursors.Default;
                            //    return;
                            //}

                            //DataTable dt_checkTaff = TimeKeeperClass.Scan_ReportTAFF(radTextBox_Empl.Text);
                            //if (dt_checkTaff.Rows.Count == 0)
                            //{
                            //    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานมีกะทำงานปกติ ไม่สามารถสแกนนิ้ว OT วันหยุดได้.");
                            //    radTextBox_Empl.SelectAll();
                            //    Cursor.Current = Cursors.Default;
                            //    return;
                            //}
                        }
                        else Dt_checkEmplIn = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_Empl.Text);

                        if (Dt_checkEmplIn.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบพนักงานที่ป้อน เช็ครหัสใหม่อีกครั้ง.");
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        if (Dt_checkEmplIn.Rows[0]["TimeBreak"].ToString() != "0")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"พนักงานยังค้างการลงเวลาพักอยู่ (ยังไม่ได้ลงเวลากลับจากพัก){Environment.NewLine}ไม่สามารถลงเวลาออกงานได้.");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }
                        else
                        {
                            this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);
                            radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                            radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                            radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                            PW_DATETIME_IN = Dt_checkEmplIn.Rows[0]["PW_DATETIME_IN"].ToString();
                            radTextBox_Empl.Enabled = false;

                            //ออกงาน
                            if (Dt_checkEmplIn.Rows[0]["PW_ID"].ToString() != "0") ////Dt_checkEmplIn.Rows[0]["PW_TimeKeeper"].ToString()
                            {
                                if (Dt_checkEmplIn.Rows[0]["PW_TYPE"].ToString() != _typeTime)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"พนักงานระบุลงเวลาออกงานผิดหมวด{Environment.NewLine}เช็คข้อมูลอีกครั้งว่าพนักงานทำ PartTime หรือ OT วันหยุด หรือไม่.");
                                    ClearDefualt(_PageCase);
                                    radTextBox_Empl.SelectAll();
                                    radTextBox_Empl.Focus();
                                    Cursor.Current = Cursors.Default;
                                    return;
                                }
                                PW_ID = Dt_checkEmplIn.Rows[0]["PW_ID"].ToString(); //Dt_checkEmplIn.Rows[0]["PW_TimeKeeper"].ToString()
                                radTextBox_Money.Enabled = false;
                                radTextBox_Remark.Enabled = false;
                                RadButton_Save.Enabled = false;
                                RadButton_Cancel.Enabled = true;
                                RadButton_Cancel.Focus();
                                Invoke(new Action(() => RadButton_Cancel_Click(sender, e)));
                            }
                            else //เข้างาน
                            {
                                if (_typeTime == "3")
                                {
                                    DataTable dt_checkTaff = TimeKeeperClass.Scan_ReportTAFF(radTextBox_Empl.Text);
                                    if (dt_checkTaff.Rows.Count == 0)
                                    {
                                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานมีกะทำงานปกติ ไม่สามารถสแกนนิ้ว OT วันหยุดได้.");
                                        radTextBox_Empl.SelectAll();
                                        Cursor.Current = Cursors.Default;
                                        return;
                                    }
                                }

                                radTextBox_Money.Focus();
                                radTextBox_Money.Enabled = true;
                                radTextBox_Remark.Enabled = true;
                                RadButton_Save.Enabled = true;
                                RadButton_Cancel.Enabled = false;
                                RadButton_Save.Focus();
                                Invoke(new Action(() => RadButton_Save_Click(sender, e)));
                            }

                            Cursor.Current = Cursors.Default;
                        }

                        break;
                    case "2"://ลงเวลาพัก
                        Cursor.Current = Cursors.WaitCursor;
                        DataTable Dt_checkEmplBreakOut = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_Empl.Text);

                        if (Dt_checkEmplBreakOut.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบพนักงานที่ป้อน เช็ครหัสใหม่อีกครั้ง.");
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        if (Dt_checkEmplBreakOut.Rows[0]["PW_ID"].ToString() == "0") //Dt_checkEmplBreakOut.Rows[0]["PW_TimeKeeper"].ToString()
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานยังไม่ลงเวลาเข้างานจึงไม่สามารถลงเวลาพักได้.");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        if (Dt_checkEmplBreakOut.Rows[0]["PW_TYPE"].ToString() != "1")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"พนักงานระบุลงเวลาเข้างานเป็น PartTime ไม่จำเป็นต้องลงเวลาพัก.");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        PW_IDCARD = Dt_checkEmplBreakOut.Rows[0]["PW_ID"].ToString(); //Dt_checkEmplBreakOut.Rows[0]["PW_TimeKeeper"]
                        BREAKCOUNT = TimeKeeperClass.GetTimeBreakPW_IDCARD(PW_IDCARD);
                        if (BREAKCOUNT > 2)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("อนุญาติให้พักวันละ 2 ครั้งเท่านั้น.");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);
                        radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                        radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                        radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                        PW_DATETIME_IN = Dt_checkEmplBreakOut.Rows[0]["OUTBREAK"].ToString();
                        radTextBox_Empl.Enabled = false;

                        //กลับเข้ามา
                        if (Dt_checkEmplBreakOut.Rows[0]["TimeBreak"].ToString() != "0")
                        {
                            PW_ID = Dt_checkEmplBreakOut.Rows[0]["TimeBreak"].ToString();
                            SpanDay = GetTimeSpan(PW_DATETIME_IN, "DAY");
                            SpanTime = GetTimeSpan(PW_DATETIME_IN, "TIME");
                            SpanMinutes = GetTimeSpan(PW_DATETIME_IN, "MINUTES");
                            radLabel_BreakTime.Text = "รวมเวลา[วัน:ชม:นาที] " + SpanDay + ":" + SpanTime;

                            radTextBox_Remark.Enabled = false;
                            radTextBox_Money.Enabled = false;
                            RadButton_Save.Enabled = false;
                            RadButton_Cancel.Enabled = true;
                            RadButton_Cancel.Focus();
                        }
                        else //พัก
                        {
                            radTextBox_Money.Focus();
                            radTextBox_Money.Enabled = false;
                            radTextBox_Remark.Enabled = false;
                            RadButton_Save.Enabled = true;
                            RadButton_Cancel.Enabled = false;
                            RadButton_Save.Focus();
                            Invoke(new Action(() => RadButton_Save_Click(sender, e)));
                        }

                        Cursor.Current = Cursors.Default;
                        break;
                    case "3"://สแกน1.30น.

                        if (SystemClass.SystemBranchID != "MN000")
                        {
                            DataTable Dt_checkEmplIINOUT = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_Empl.Text);
                            if (Dt_checkEmplIINOUT.Rows.Count > 0 &&
                                Dt_checkEmplIINOUT.Rows[0]["PW_ID"].ToString() == "0") //Dt_checkEmplIINOUT.Rows[0]["PW_TimeKeeper"].ToString()
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานยังไม่ลงเวลาเข้างาน ไม่สามารถสแกน1.30น.ได้.");
                                ClearDefualt(_PageCase);
                                radTextBox_Empl.SelectAll();
                                Cursor.Current = Cursors.Default;
                                return;
                            }
                        }

                        this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);
                        if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                        {
                            radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                            radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                            radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                            radTextBox_Empl.Enabled = false;
                            Invoke(new Action(() => RadButton_Save_Click(sender, e)));
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน");

                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                        }

                        break;
                    case "4"://ตรวจสอบปริมาณพนักงาน
                        DataTable Dt_checkEmplIINOUT4 = Class.Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_Empl.Text);

                        if (Dt_checkEmplIINOUT4.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงานที่ระบุ");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }
                        if (Dt_checkEmplIINOUT4.Rows[0]["PW_ID"].ToString() == "0") //(Dt_checkEmplIINOUT4.Rows[0]["PW_TimeKeeper"].ToString()
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานยังไม่ลงเวลาเข้างาน ไม่สามารถลงเวลาตรวจสอบปริมาณพนักงานได้.");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);
                        if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                        {
                            radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                            radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                            radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                            radTextBox_Empl.Enabled = false;
                            Invoke(new Action(() => RadButton_Save_Click(sender, e)));
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน");

                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                        }
                        break;
                    case "5"://PC.
                        string PCAID = "PCA" + radTextBox_Empl.Text;
                        DataTable DtPCA = TimeKeeperClass.GetPCA_DateTimeNow(PCAID, SystemClass.SystemBranchID);

                        if (DtPCA.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูล PC");
                            ClearDefualt(_PageCase);
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                        if (DtPCA.Rows.Count > 0)
                        {
                            if (DtPCA.Rows[0]["STA"].ToString() == "0")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงาน PC มีสถานะลาออกแล้ว ไม่สามารถเข้าสาขาได้");
                                ClearDefualt(_PageCase);
                                Cursor.Current = Cursors.Default;
                                return;
                            }
                        }


                        this.Empl = new Data_EMPLTABLE(DtPCA);
                        PW_DATETIME_IN = DtPCA.Rows[0]["PW_DATETIME_IN"].ToString();

                        radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                        radLabel2.Text = string.Empty;
                        radLabel_Possion.Text = Empl.EMPLTABLE_EMPLID;
                        radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                        radTextBox_Empl.Enabled = false;
                        if (DtPCA.Rows[0]["PW_ID"].ToString() != "0")
                        {
                            PW_ID = DtPCA.Rows[0]["PW_ID"].ToString();
                            SpanDay = GetTimeSpan(PW_DATETIME_IN, "DAY");
                            SpanTime = GetTimeSpan(PW_DATETIME_IN, "TIME");
                            SpanMinutes = GetTimeSpan(PW_DATETIME_IN, "MINUTES");
                            radLabel_BreakTime.Text = "รวมเวลา[วัน:ชม:นาที] " + SpanDay + ":" + SpanTime; // "รวมเวลา " + SpanDay + ". " + SpanTime;

                            radTextBox_Remark.Enabled = false;
                            radTextBox_Money.Enabled = false;
                            RadButton_Save.Enabled = false;
                            RadButton_Cancel.Enabled = true;
                            RadButton_Cancel.Focus();
                        }
                        else
                        {
                            radTextBox_Money.Enabled = false;
                            radTextBox_Remark.Enabled = false;
                            RadButton_Save.Enabled = true;
                            RadButton_Cancel.Enabled = false;
                            RadButton_Save.Focus();
                        }
                        break;
                    case "6"://ช่างก่อสร้าง.
                        this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);
                        if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                        {
                            if (this.Empl.EMPLTABLE_NUM != "D041")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่ใช่พนักงานแผนกก่อสร้าง{Environment.NewLine}ไม่สามารถใช้งานเมนูนี้ได้");
                                return;
                            }

                            radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                            radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                            radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                            radTextBox_Empl.Enabled = false;
                            Invoke(new Action(() => RadButton_Save_Click(sender, e)));
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงานที่ระบุ");
                            ClearDefualt(_PageCase);
                            radTextBox_Empl.SelectAll();
                            radTextBox_Empl.Focus();
                        }

                        break;
                        //case "7"://ช่างซ่อมทั่วไป.
                        //    this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);
                        //    if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                        //    {
                        //        radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME;
                        //        radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION;
                        //        radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION;
                        //        radTextBox_Empl.Enabled = false;
                        //        Invoke(new Action(() => RadButton_Save_Click(sender, e)));
                        //    }
                        //    else
                        //    {
                        //        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน");
                        //        ClearDefualt(_PageCase);
                        //        radTextBox_Empl.SelectAll();
                        //    }

                        //    break;
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            #region OLD
            //switch (_PageCase)
            //{
            //    case "1":
            //        if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้
            //                    {Environment.NewLine}กรุณาใช้งานผ่านหน้าเอกสาร ทีมจัดร้านเข้าสาขา");
            //            return;
            //        }

            //        //if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "ลงเวลาเข้างาน"))
            //        //{
            //        //    MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //        //    ClearDefualt(_PageCase);
            //        //    radTextBox_Empl.Focus();
            //        //    return;
            //        //}

            //        //string InsInOut1 = $@"
            //        //INSERT INTO  SHOP_TIMEKEEPER   
            //        //            (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
            //        //            PW_WHOIN,PW_POSITION)  
            //        //VALUES  ('{Empl.EMPLTABLE_EMPLID_M}','{Empl.EMPLTABLE_ALTNUM}','{Empl.EMPLTABLE_SPC_NAME}','{Controllers.SystemClass.SystemBranchID}',
            //        //        '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_POSSITION}')" ;
            //        //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(InsInOut1));
            //        //ClearDefualt(_PageCase);
            //        //radTextBox_Empl.Focus();
            //        break;
            //    case "2":
            //        //if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "ลงเวลาออก พัก"))
            //        //{
            //        //    MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //        //    ClearDefualt(_PageCase);
            //        //    radTextBox_Empl.Focus();
            //        //    return;
            //        //}
            //        //string InsBreakOut = string.Format($@"
            //        //INSERT INTO SHOP_TIMEBREAK   
            //        //            (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
            //        //            PW_WHOIN,PW_POSITION,PW_Count,PW_IDTimeKeeper ) 
            //        //VALUES ('{Empl.EMPLTABLE_EMPLID_M}', '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_SPC_NAME}', '{SystemClass.SystemBranchID}', 
            //        //        '{Empl.EMPLTABLE_EMPLID}','{Empl.EMPLTABLE_POSSITION}','{BREAKCOUNT}','{PW_IDCARD}')");
            //        //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(InsBreakOut));                 
            //        //ClearDefualt(_PageCase);
            //        //radTextBox_Empl.Focus();
            //        break;

            //    case "3":
            //        //if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "สแกน 1.30 ชม."))
            //        //{
            //        //    MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //        //    ClearDefualt(_PageCase);
            //        //    radTextBox_Empl.Focus();
            //        //    return;
            //        //}

            //        //int seq = SEQ_SCANFINGER_TAFFDT(Environment.MachineName, SystemClass.SystemBranchID);
            //        //String InsScan3 = string.Format(@"INSERT INTO  SCANFINGER_TAFFDT  
            //        //                            (NAME, SPCNAME, SEQ, PWCARD, TAFFDATESCAN,
            //        //                            TAFFTIMESCAN, CONVERTDATE, CONVERTDATETIME, SCANID, INOUT,
            //        //                            CREATENAME, DATESAVE, DATETIMESAVE, LOCATIONDATA)
            //        //                            values(UPPER(REPLACE(Convert(Varchar, GetDate(), 6), ' ', '')) + '.TAFF',
            //        //                            '{0}', '{1}', '{2}',
            //        //                            Convert(Varchar, GetDate(), 12), substring(replace(Convert(Varchar, GetDate(), 8), ':', ''), 1, 4),
            //        //                            convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', 
            //        //                            '{3}', 'I', '{4}',
            //        //                            convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', '2')",
            //        //                        Environment.MachineName,
            //        //                        seq,
            //        //                        radTextBox_Empl.Text,
            //        //                        SystemClass.SystemBranchID,
            //        //                        System.Security.Principal.WindowsIdentity.GetCurrent().Name);

            //        //String Transection3 = ConnectionClass.ExecuteSQL_SentServer(InsScan3, IpServerConnectClass.ConHROS);
            //        //MsgBoxClass.MsgBoxShow_SaveStatus(Transection3);
            //        // ClearDefualt(_PageCase);
            //        //radTextBox_Empl.Focus();
            //        break;

            //    case "4":
            //        if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "เช็คปริมาณพนักงาน"))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //            ClearDefualt(_PageCase);
            //            radTextBox_Empl.Focus();
            //            return;
            //        }
            //        String InsScan4 = string.Format($@" 
            //        INSERT INTO SHOP_TIMEKEEPERCHECK 
            //                    (ALTNUM,EMPLID, EMPLNAME,
            //                    BRANCH_ID,BRANCH_NAME,WHOINS,WHONAMEINS)  
            //        VALUES      ('{this.Empl.EMPLTABLE_ALTNUM}','{this.Empl.EMPLTABLE_EMPLID_M}', '{this.Empl.EMPLTABLE_SPC_NAME}',
            //                    '{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')"
            //        );

            //        String Transection4 = ConnectionClass.ExecuteSQL_Main(InsScan4);
            //        MsgBoxClass.MsgBoxShow_SaveStatus(Transection4);
            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;

            //    case "5":
            //        String InsScan5 = string.Format($@"
            //        INSERT INTO SHOP_TIMEKEEPERPC   (PW_IDCARD, PW_NAME, PW_BRANCH_IN,PW_WHOIN)
            //        VALUES  ('{this.Empl.EMPLTABLE_EMPLID}','{this.Empl.EMPLTABLE_SPC_NAME}', '{SystemClass.SystemBranchID}','{SystemClass.SystemUserID}')");

            //        MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(InsScan5));
            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;
            //    case "6":
            //        //if (ScanFinger(Empl.EMPLTABLE_EMPLID, $@"ลงเวลาเข้างาน{Environment.NewLine}ช่างก่อสร้าง")) return;
            //        if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, $@"ลงเวลาเข้างาน{Environment.NewLine}ช่างก่อสร้าง"))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //            ClearDefualt(_PageCase);
            //            radTextBox_Empl.SelectAll();
            //            radTextBox_Empl.Focus();
            //            return;
            //        }
            //        int seq6 = SEQ_SPC_WS_TAFFDTNEWSITE_SHOP24(SystemClass.SystemBranchID);
            //        String InsScan6 = string.Format(@"INSERT INTO  SPC_WS_TAFFDTNEWSITE_SHOP24  
            //                                    (NAME, SPCNAME, SEQ, PWCARD, TAFFDATESCAN,
            //                                    TAFFTIMESCAN, CONVERTDATE, CONVERTDATETIME, SCANID, INOUT,
            //                                    CREATENAME, DATESAVE, DATETIMESAVE, LOCATIONDATA)
            //                                    values(UPPER(REPLACE(Convert(Varchar, GetDate(), 6), ' ', '')) + '.TAFF',
            //                                    '{0}', '{1}', '{2}',
            //                                    Convert(Varchar, GetDate(), 12), substring(replace(Convert(Varchar, GetDate(), 8), ':', ''), 1, 4),
            //                                    convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', 
            //                                    '{3}', 'I', '{4}',
            //                                    convert(varchar, getdate(), 23), substring(Convert(Varchar, GetDate(), 121), 0, 17) + ':00.000', '2')",
            //                                Environment.MachineName,
            //                                seq6,
            //                                radTextBox_Empl.Text,
            //                                SystemClass.SystemBranchID,
            //                                System.Security.Principal.WindowsIdentity.GetCurrent().Name);

            //        String Transection6 = ConnectionClass.ExecuteSQL_SentServer(InsScan6, IpServerConnectClass.ConHROS);
            //        MsgBoxClass.MsgBoxShow_SaveStatus(Transection6);

            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;
            //    case "7":
            //        if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, $@"ลงเวลาเข้า{Environment.NewLine}ช่างทั่วไป"))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //            ClearDefualt(_PageCase);
            //            radTextBox_Empl.Focus();
            //            return;
            //        }

            //        DataTable DtINOUT = INOUT_TACHNICIAN_SHOP24(this.Empl.EMPLTABLE_EMPLID, SystemClass.SystemBranchID);

            //        string statusINOUT = "IN", startTime = DateTime.Now.ToString(), endTime = DateTime.Now.ToString(); ;
            //        TimeSpan TIMEINOUT;
            //        int Day = 0, hours = 0, min = 0;

            //        if (DtINOUT.Rows.Count > 0)
            //        {
            //            if (DtINOUT.Rows[0]["PW_BRANCH_OUT"].ToString() == "")
            //            {
            //                statusINOUT = "OUT";
            //                startTime = DtINOUT.Rows[0]["PW_DATETIME_IN"].ToString();
            //                TIMEINOUT = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime));
            //                Day = TIMEINOUT.Days;
            //                hours = TIMEINOUT.Hours;
            //                min = TIMEINOUT.Minutes;

            //                if (Day > 0)
            //                {
            //                    statusINOUT = "IN";
            //                }
            //            }
            //        }

            //        String InsScan7 = "";
            //        if (statusINOUT == "IN")
            //        {
            //            InsScan7 = string.Format(@"Insert Into SHOP_TIMEKEEPER_TACHNICIAN
            //                            (PW_IDCARD, PW_NAME, PW_BRANCH_IN,PW_BRANCHNAME_IN, PW_DATE_IN, PW_TIME_IN,
            //                            PW_DATETIME_IN, PW_STATUS, PW_TYPE, PW_DATEIN, PW_TIMEIN, PW_WHOIN)
            //                             values
            //                            ('{0}','{1}', '{2}','{4}',
            //                            convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), convert(varchar, getdate(), 25),
            //                            '1', '1', convert(varchar, getdate(), 23), convert(varchar, getdate(), 24),'{3}')",
            //                             this.Empl.EMPLTABLE_EMPLID,
            //                             this.Empl.EMPLTABLE_SPC_NAME,
            //                             SystemClass.SystemBranchID,
            //                             SystemClass.SystemUserID, SystemClass.SystemBranchName);
            //        }
            //        else if (statusINOUT == "OUT")
            //        {
            //            InsScan7 = string.Format(@"update SHOP_TIMEKEEPER_TACHNICIAN set 
            //                            PW_BRANCH_OUT='{1}', 
            //                            PW_BRANCHNAME_OUT= '{6}',
            //                            PW_DATE_OUT=convert(varchar,getdate(),23), 
            //                            PW_TIME_OUT=convert(varchar,getdate(),24), 
            //                            PW_DATETIME_OUT=convert(varchar,getdate(),25),
            //                            PW_DATEOUT=convert(varchar,getdate(),23), 
            //                            PW_TIMEOUT=convert(varchar,getdate(),24), 
            //                            PW_WHOOUT='{2}' , 
            //                            PW_SUMDATE='{3}', 
            //                            PW_SUMTIME='{4}'
            //                            where  PW_IDCARD='{0}'  
            //                            AND  PW_BRANCH_IN='{1}' 
            //                            AND PW_DATE_IN='{5}'
            //                            AND PW_BRANCH_OUT is null ",
            //                            this.Empl.EMPLTABLE_EMPLID,
            //                            SystemClass.SystemBranchID,
            //                            SystemClass.SystemUserID,
            //                            Day,
            //                            hours + ":" + min,
            //                            DateTime.Parse(startTime).ToString("yyyy-MM-dd"), SystemClass.SystemBranchName);
            //        }

            //        String Transection7 = ConnectionClass.ExecuteSQL_Main(InsScan7);
            //        MsgBoxClass.MsgBoxShow_SaveStatus(Transection7);
            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;
            //}

            #endregion

            switch (_PageCase)
            {
                case "1":
                    if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้
                                {Environment.NewLine}กรุณาใช้งานผ่านหน้าเอกสาร ทีมจัดร้านเข้าสาขา");
                        return;
                    }
                    SaveTbl();
                    break;
                case "2":
                    SaveTbl();
                    break;
                case "3":
                    PW_IDCARD = radTextBox_Empl.Text;
                    SaveTbl();
                    break;
                case "4":
                    SaveTbl();
                    break;
                case "5":
                    SaveTbl();
                    break;
                case "6":
                    PW_IDCARD = radTextBox_Empl.Text;
                    SaveTbl();
                    break;
            }
        }
        void SaveTbl()
        {
            if (_PageCase != "5")
            {
                if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, PageCaseDesc))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                    ClearDefualt(_PageCase);
                    radTextBox_Empl.Focus();
                    return;
                }
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(TimeKeeperClass.Save_TIMEKEEPER(_PageCase, Empl, BREAKCOUNT, PW_IDCARD, _typeTime));
            ClearDefualt(_PageCase);
            radTextBox_Empl.Focus();
        }

        #region"IN-OUT"
        public string GetPOS_Bill707AndCash(string AccountNum, string PW_DATETIME_IN)
        {
            if (AccountNum == null) return "0,0";

            int BillTotal = 0;
            double MoneyTotal = 0;
            //string Sql = string.Format(@"  
            //                SELECT  ISNULL(COUNT(INVOICEACCOUNT),0) AS COUNT_BILL, ISNULL(SUM(INVOICEAMOUNT), 0) AS SUM_BILL 
            //                FROM	XXX_POSTABLE WITH (NOLOCK) 
            //                WHERE	INVOICEACCOUNT = '{0}' 
            //                        AND CREATEDDATETIME BETWEEN '{1}' and GETDATE() ",
            //                 AccountNum,
            //                 PW_DATETIME_IN);
            DataTable DtPOS = PosSaleClass.GetXXX_DetailByCstID("0", AccountNum, PW_DATETIME_IN);// ConnectionClass.SelectSQL_POSRetail707(Sql);

            if (DtPOS.Rows.Count > 0)
            {
                BillTotal = int.Parse(DtPOS.Rows[0]["COUNT_BILL"].ToString());
                MoneyTotal = Double.Parse(DtPOS.Rows[0]["SUM_BILL"].ToString());
            }

            return BillTotal.ToString() + @"," + MoneyTotal.ToString();
        }

        private void RadTextBox_EmplIdConf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Remark.Enabled = true;
                radTextBox_Remark.SelectAll();
            }
        }
        //Pdf
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _PageCase);
        }

        #endregion



        #region "scan ช่าง"
        ////SQL ช่าง
        //public int SEQ_SPC_WS_TAFFDTNEWSITE_SHOP24(string scanid)
        //{
        //    String Sql = string.Format(@" SELECT    count(SEQ)+1 as SEQ  FROM    SPC_WS_TAFFDTNEWSITE_SHOP24  
        //                    WHERE   LOCATIONDATA='2'  AND  convert(varchar,CONVERTDATE,23) =convert(varchar, getdate() ,23)    and scanid='{0}'  
        //                    ORDER BY  seq DESC", scanid);

        //    DataTable Dt = ConnectionClass.SelectSQL_SentServer(Sql, IpServerConnectClass.ConHROS);
        //    int seq = 1;
        //    if (Dt.Rows.Count > 0)
        //    {
        //        seq = int.Parse(Dt.Rows[0]["SEQ"].ToString());
        //    }
        //    return seq;
        //}
        ////SQL ช่างซ่อม
        //public DataTable INOUT_TACHNICIAN_SHOP24(string emplid, string branchid)
        //{
        //    String Sql = string.Format(@"SELECT 
        //            isnull(PW_IDCARD,'') as PW_IDCARD,
        //            isnull(PW_BRANCH_IN,'') as PW_BRANCH_IN,
        //            isnull(PW_DATE_IN,'') as PW_DATE_IN,
        //            isnull(PW_TIME_IN,'') as PW_TIME_IN,
        //            isnull(PW_DATETIME_IN,'') as PW_DATETIME_IN, 
        //            isnull(PW_BRANCH_OUT,'') as PW_BRANCH_OUT,
        //            isnull(PW_DATE_OUT,'') as PW_DATE_OUT , 
        //            isnull(PW_TIME_OUT,'') as PW_TIME_OUT , 
        //            isnull(PW_DATETIME_OUT,'') as PW_DATETIME_OUT  
        //            FROM SHOP_TIMEKEEPER_TACHNICIAN
        //            WHERE PW_IDCARD='{0}' AND  PW_BRANCH_IN='{1}'  
        //            ORDER BY PW_ID DESC", emplid, branchid);

        //    DataTable Dt = ConnectionClass.SelectSQL_SentServer(Sql, IpServerConnectClass.ConSelectMain);

        //    return Dt;
        //}
        #endregion

        //refresh
        private void RadButton_refresh_Click(object sender, EventArgs e)
        {
            radTextBox_Empl.SelectAll();
            ClearDefualt(_PageCase);
            radTextBox_Empl.Focus();
        }

        //private void RadButton_SubSave_Click(object sender, EventArgs e)
        //{
        //    switch (_PageCase)
        //    {
        //        case "1":

        //            //if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
        //            //{
        //            //    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้ กรุณาใช้งานผ่านหน้าเอกสาร");
        //            //    return;
        //            //}
        //            //if (ScanFinger(radTextBox_Empl.Text, "ลงเวลาออกงาน")) return;
        //            //double _Money = 0;
        //            //if (radTextBox_Money.Text != string.Empty)
        //            //{
        //            //    _Money = double.Parse(radTextBox_Money.Text);
        //            //}

        //            //string InsInOut1 = string.Format($@"
        //            //INSERT INTO  SHOP_TIMEKEEPER   
        //            //            (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
        //            //            PW_WHOIN,PW_POSITION)  
        //            //VALUES  ('{Empl.EMPLTABLE_EMPLID_M}','{Empl.EMPLTABLE_ALTNUM}','{Empl.EMPLTABLE_SPC_NAME}','{Controllers.SystemClass.SystemBranchID}',
        //            //        '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_POSSITION}')");
        //            //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(InsInOut1));
        //            //ClearDefualt(PageCase);
        //            //radTextBox_Empl.Focus();
        //            break;
        //        case "2":
        //            ////if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
        //            ////{
        //            ////    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้ กรุณาใช้งานผ่านหน้าเอกสาร");
        //            ////    return;
        //            ////}
        //            //if (ScanFinger(radTextBox_Empl.Text)) return;
        //            //string InsBreakOut = string.Format($@"
        //            //INSERT INTO SHOP_TIMEBREAK   
        //            //            (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
        //            //            PW_WHOIN,PW_POSITION,PW_Count,PW_IDTimeKeeper ) 
        //            //VALUES ('{Empl.EMPLTABLE_EMPLID_M}', '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_SPC_NAME}', '{SystemClass.SystemBranchID}', 
        //            //        '{Empl.EMPLTABLE_EMPLID}','{Empl.EMPLTABLE_POSSITION}','{BREAKCOUNT}','{PW_IDCARD}')");
        //            //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(InsBreakOut));
        //            //ClearDefualt(PageCase);
        //            //radTextBox_Empl.Focus();
        //            break;
        //    }


        //}

        //private void RadButton_SubCancel_Click(object sender, EventArgs e)
        //{
        //    //if (TimeKeeperClass.GetEmplScanCard_ByEmplBranch(radTextBox_Empl.Text, SystemClass.SystemBranchID.Replace("MN", "")))
        //    //{ 
        //    //    try
        //    //    {
        //    //        using (FingerScan FingerScan = new FingerScan(radTextBox_Empl.Text, "ลงเวลา"))
        //    //        {
        //    //            DialogResult dr = FingerScan.ShowDialog();
        //    //            if (dr != DialogResult.Yes)
        //    //            {
        //    //                MsgBoxClass.MsgBoxShowButtonOk_Error("กรุณาสแกนนิ้วใหม่อีกครั้ง");
        //    //                ClearDefualt(PageCase);
        //    //                radTextBox_Empl.SelectAll();
        //    //                return;
        //    //            }
        //    //        }

        //    //    }
        //    //    catch
        //    //    {
        //    //        MsgBoxClass.MsgBoxShowButtonOk_Error("กรุณาติดตั้งเครื่องสแกน");
        //    //        Cursor.Current = Cursors.Default;
        //    //        return;
        //    //    }
        //    //}


        //    switch (PageCase)
        //    {
        //        case "1":
        //            //if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
        //            //{
        //            //    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้
        //            //            {Environment.NewLine}กรุณาใช้งานผ่านหน้าเอกสาร ทีมจัดร้านเข้าสาขา");
        //            //    return;
        //            //}


        //            ////(string pBchID, string pDate, string pEmpID)
        //            //if (SystemClass.SystemBranchStaShelf == "1")
        //            //{
        //            //    DataTable dt = EmplClass.GetEmpAllShelf_ByDate("'" + SystemClass.SystemBranchID + "'",
        //            //        DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd"), " AND EMPLTABLE.EMPLID = '" + Empl.EMPLTABLE_EMPLID_M + "'", " AND SHELF_ID != 'SH000' ");
        //            //    if (dt.Rows.Count > 0)
        //            //    {// "'" + SystemClass.SystemBranchID + "'", DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd"
        //            //        ShowDataShelf_ByEmp _frm = new ShowDataShelf_ByEmp(dt, DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd")
        //            //      );//,
        //            //        //string.Format(@"AND EMPLTABLE.EMPLID = '{0}' ", Empl.EMPLTABLE_EMPLID_M));
        //            //        _frm.ShowDialog(this);
        //            //        if (_frm.iCheckRed > 0)
        //            //        {
        //            //            int iShelf = _frm.iCheckRed;
        //            //            MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานไม่สามารถลงเวลาออกได้ " + Environment.NewLine +
        //            //                "มีรายการชั้นวาง " + iShelf.ToString() + " ที่ยังไม่ได้ถ่ายรูป" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
        //            //            ClearDefualt("1");
        //            //            return;
        //            //        }
        //            //        ////if (_frm.ShowDialog(this) != DialogResult.Yes)
        //            //        ////{
        //            //        ////    return;
        //            //        ////}
        //            //    }
        //            //}

        //            //if (ScanFinger(radTextBox_Empl.Text)) return;
        //            //String Bill = "0";
        //            //String Money = "0";
        //            //SpanDay = GetTimeSpan(PW_DATETIME_IN, "DAY");
        //            //SpanTime = GetTimeSpan(PW_DATETIME_IN, "TIME");
        //            ////ซื้อสินค้าตามรหัสพนักงาน ถ้ารหัสพนักงานไม่มีรหัสลูกค้าจะไม่ไปหาบิลขาย
        //            //if (!(Empl.EMPLTABLE_EMP_AccountNum.Equals("")))
        //            //{
        //            //    String[] POSBillAndMoney = GetPOS_Bill707AndCash(Empl.EMPLTABLE_EMP_AccountNum,
        //            //       DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd hh:mm:ss")).Split(',');
        //            //    if (POSBillAndMoney.Length > 0)
        //            //    {
        //            //        Bill = POSBillAndMoney[0].ToString();
        //            //        Money = POSBillAndMoney[1].ToString();
        //            //    }
        //            //}

        //            //string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNEP", "-", "MNEP", "1");

        //            //String UpInOut = string.Format($@"
        //            //UPDATE SHOP_TIMEKEEPER 
        //            //SET    PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
        //            //       PW_DATE_OUT =convert(varchar,getdate(),23), 
        //            //       PW_TIME_OUT = convert(varchar, getdate(), 24),
        //            //       PW_DATETIME_OUT = convert(varchar, getdate(), 25), 
        //            //       PW_SUMDATE = '{SpanDay}', 
        //            //       PW_SUMTIME = '{SpanTime}',
        //            //       PW_DATEOUT = convert(varchar, getdate(), 23), 
        //            //       PW_TIMEOUT = convert(varchar, getdate(), 24),
        //            //       PW_WHOOUT = '{SystemClass.SystemUserID}', 
        //            //       PW_STATUS = '0',
        //            //       PW_QTYBill = '{Bill}',
        //            //       PW_MONEYUSEIN = '{Money}',
        //            //       PW_MNEP =  '{maxDocno}'
        //            //WHERE  PW_ID =  '{PW_ID}' ");
        //            //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(UpInOut));
        //            //ClearDefualt(PageCase);
        //            //radTextBox_Empl.Focus();
        //            break;
        //        case "2":

        //            //if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
        //            //{
        //            //    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้ กรุณาใช้งานผ่านหน้าเอกสาร");
        //            //    return;
        //            //}
        //            //if (ScanFinger(radTextBox_Empl.Text)) return;
        //            //if (radTextBox_Remark.Enabled == true && radTextBox_Remark.Text == string.Empty)
        //            //{
        //            //    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุเหตุผลการพักเกินก่อนการบันทึกข้อมูล");
        //            //    Cursor.Current = Cursors.Default;
        //            //    return;
        //            //}

        //            //if (radTextBox_Remark.Enabled == true)
        //            //{
        //            //    //SCAN CONFIRM
        //            //    // EMPLID TEXTBOX EMPLCONF
        //            //    //            If SumTime > "00:40" Then
        //            //    // Dim frm As New ConfirmUserFigerNew
        //            //    //    If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
        //            //    //        pIdSave = frm.pID
        //            //    //        EditSave()
        //            //    //    Else
        //            //    //        MessageBox.Show("ไม่สามารถลงเวลากลับได้เนื่องจากไม่มีสิทธิ์ในการบันทึก.", ClassVar.TextHeadName, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
        //            //    //        Me.Cursor = Cursors.Default
        //            //    //        Exit Sub
        //            //    //    End If
        //            //    //Else
        //            //}
        //            ////if (!(TimeKeeperClass.GetEmplScanCard_ByEmplBranch(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID.Replace("MN", ""))))
        //            ////{
        //            ////    //SCAN SHOW DIALOG
        //            ////}


        //            //String UpBreakIN = string.Format($@" 
        //            //UPDATE  SHOP_TIMEBREAK 
        //            //SET     PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
        //            //        PW_DATE_OUT =convert(varchar,getdate(),23), 
        //            //        PW_TIME_OUT = convert(varchar, getdate(), 24),
        //            //        PW_DATETIME_OUT = convert(varchar, getdate(), 25),
        //            //        PW_SUMDATE = '{SpanDay}', 
        //            //        PW_SUMTIME = '{SpanTime}',
        //            //        PW_DATEOUT = convert(varchar, getdate(), 23), 
        //            //        PW_TIMEOUT = convert(varchar, getdate(), 24),  
        //            //        PW_WHOOUT = '{SystemClass.SystemUserID}', 
        //            //        PW_STATUS = '0',
        //            //        PW_REMARK = '{radTextBox_Remark.Text}'
        //            //WHERE PW_ID = '{PW_ID}'");
        //            //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(UpBreakIN));
        //            //ClearDefualt(PageCase);
        //            //radTextBox_Empl.Focus();
        //            break;
        //    }
        //}


        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            #region "OLD"
            //switch (_PageCase)
            //{
            //    case "1":
            //        if (Empl.EMPLTABLE_NUM.Contains("D"))
            //        {
            //            if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
            //            {
            //                MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้
            //                    {Environment.NewLine}กรุณาใช้งานผ่านหน้าเอกสาร ทีมจัดร้านเข้าสาขา");
            //                return;
            //            }
            //        }

            //        if (SystemClass.SystemBranchStaShelf == "1")
            //        {
            //            DataTable dt = EmplClass.GetEmpAllShelf_ByDate("'" + SystemClass.SystemBranchID + "'",
            //                DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd"), " AND EMPLTABLE.EMPLID = '" + Empl.EMPLTABLE_EMPLID_M + "'", " AND SHELF_ID != 'SH000' ");
            //            if (dt.Rows.Count > 0)
            //            {
            //                ShowDataShelf_ByEmp _frm = new ShowDataShelf_ByEmp(dt, DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd"));
            //                _frm.ShowDialog(this);
            //                if (_frm.iCheckRed > 0)
            //                {
            //                    int iShelf = _frm.iCheckRed;
            //                    MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานไม่สามารถลงเวลาออกได้ " + Environment.NewLine +
            //                        "มีรายการชั้นวาง " + iShelf.ToString() + " ที่ยังไม่ได้ถ่ายรูป" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
            //                    ClearDefualt("1");
            //                    return;
            //                }
            //            }
            //        }

            //        if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "ลงเวลาออกงาน"))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //            ClearDefualt(_PageCase);
            //            radTextBox_Empl.Focus();
            //            return;
            //        }

            //        String Bill = "0";
            //        String Money = "0";
            //        SpanDay = GetTimeSpan(PW_DATETIME_IN, "DAY");
            //        SpanTime = GetTimeSpan(PW_DATETIME_IN, "TIME");
            //        //ซื้อสินค้าตามรหัสพนักงาน ถ้ารหัสพนักงานไม่มีรหัสลูกค้าจะไม่ไปหาบิลขาย
            //        if (!(Empl.EMPLTABLE_EMP_AccountNum.Equals("")))
            //        {
            //            String[] POSBillAndMoney = GetPOS_Bill707AndCash(Empl.EMPLTABLE_EMP_AccountNum,
            //               DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd hh:mm:ss")).Split(',');
            //            if (POSBillAndMoney.Length > 0)
            //            {
            //                Bill = POSBillAndMoney[0].ToString();
            //                Money = POSBillAndMoney[1].ToString();
            //            }
            //        }

            //        string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNEP", "-", "MNEP", "1");

            //        String UpInOut = string.Format($@"
            //        UPDATE SHOP_TIMEKEEPER 
            //        SET    PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
            //               PW_DATE_OUT =convert(varchar,getdate(),23), 
            //               PW_TIME_OUT = convert(varchar, getdate(), 24),
            //               PW_DATETIME_OUT = convert(varchar, getdate(), 25), 
            //               PW_SUMDATE = '{SpanDay}', 
            //               PW_SUMTIME = '{SpanTime}',
            //               PW_DATEOUT = convert(varchar, getdate(), 23), 
            //               PW_TIMEOUT = convert(varchar, getdate(), 24),
            //               PW_WHOOUT = '{SystemClass.SystemUserID}', 
            //               PW_STATUS = '0',
            //               PW_QTYBill = '{Bill}',
            //               PW_MONEYUSEIN = '{Money}',
            //               PW_MNEP =  '{maxDocno}'
            //        WHERE  PW_ID =  '{PW_ID}' ");
            //        MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(UpInOut));
            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;

            //    case "2":

            //        string rmk = "";
            //        if (double.Parse(SpanMinutes) > 40)
            //        {
            //            FormShare.ShowRemark frmRmk = new FormShare.ShowRemark("1");
            //            if (frmRmk.ShowDialog(this) == DialogResult.Yes)
            //            {
            //                rmk = frmRmk.pRmk;
            //                if (SystemClass.SystemUserPositionID == "03")
            //                {
            //                    GeneralForm.TimeKeeper.ConfirmCheckTimekeep frmConfirm = new GeneralForm.TimeKeeper.ConfirmCheckTimekeep("0");
            //                    if (frmConfirm.ShowDialog(this) != DialogResult.OK)
            //                    {
            //                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถบันทึกลงเวลาเข้าไม่ได้{Environment.NewLine}เนื่องจากไม่มีสิทธิ์ในการบันทึก{Environment.NewLine}[ต้องยืนยันด้วยตำแหน่งผู้จัดการหรือผู้ช่วยเท่านั้น");
            //                        this.Cursor = Cursors.Default;
            //                        return;
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                Cursor.Current = Cursors.Default;
            //                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุเหตุผลการพักเกินก่อนการบันทึกข้อมูล");
            //                return;
            //            }
            //        }

            //        if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "ลงเวลาเข้า พัก"))
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
            //            ClearDefualt(_PageCase);
            //            radTextBox_Empl.Focus();
            //            return;
            //        }

            //        String UpBreakIN = string.Format($@" 
            //        UPDATE  SHOP_TIMEBREAK 
            //        SET     PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
            //                PW_DATE_OUT =convert(varchar,getdate(),23), 
            //                PW_TIME_OUT = convert(varchar, getdate(), 24),
            //                PW_DATETIME_OUT = convert(varchar, getdate(), 25),
            //                PW_SUMDATE = '{SpanDay}', 
            //                PW_SUMTIME = '{SpanTime}',
            //                PW_DATEOUT = convert(varchar, getdate(), 23), 
            //                PW_TIMEOUT = convert(varchar, getdate(), 24),  
            //                PW_WHOOUT = '{SystemClass.SystemUserID}', 
            //                PW_STATUS = '0',
            //                PW_REMARK = '{rmk}'
            //        WHERE PW_ID = '{PW_ID}'");
            //        MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(UpBreakIN));
            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;
            //    case "5":
            //        String InsScan5 = string.Format($@"
            //            UPDATE SHOP_TIMEKEEPERPC
            //            SET    PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
            //                    PW_DATE_OUT = convert(varchar, getdate(), 23),
            //                    PW_TIME_OUT = convert(varchar, getdate(), 24),
            //                    PW_DATETIME_OUT = convert(varchar, getdate(), 25),
            //                    PW_SUMDATE = '{SpanDay}',
            //                    PW_SUMTIME = '{SpanTime}',
            //                    PW_DATEOUT = convert(varchar, getdate(), 23),
            //                    PW_TIMEOUT = convert(varchar, getdate(), 24),
            //                    PW_WHOOUT = '{SystemClass.SystemUserID_M}',
            //                    PW_STATUS = '0'
            //            WHERE   PW_ID = '{PW_ID}'");

            //        MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(InsScan5));
            //        ClearDefualt(_PageCase);
            //        radTextBox_Empl.Focus();
            //        break;
            //}

            #endregion

            string Bill = "0", Money = "0", rmk = "";

            switch (_PageCase)
            {
                case "1":

                    if (TimeKeeperClass.GetEmplCenter(Empl.EMPLTABLE_ALTNUM, SystemClass.SystemBranchID))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"พนักงานไม่สามารถลงเวลาเข้าออกงานหน้านี้ได้
                                {Environment.NewLine}กรุณาใช้งานผ่านหน้าเอกสาร ทีมจัดร้านเข้าสาขา");
                        return;
                    }
                    //สาขาที่มีชั้นวาง
                    if (SystemClass.SystemBranchStaShelf == "1")
                    {
                        DataTable dt = Models.EmplClass.GetEmpAllShelf_ByDate("'" + SystemClass.SystemBranchID + "'",
                            DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd"), Empl.EMPLTABLE_EMPLID_M, " AND SHELF_ID != 'SH000' ");
                        if (dt.Rows.Count > 0)
                        {
                            ShowDataShelf_ByEmp _frm = new ShowDataShelf_ByEmp(dt, DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd"));
                            _frm.ShowDialog(this);
                            if (_frm.iCheckRed > 0)
                            {
                                int iShelf = _frm.iCheckRed;
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานไม่สามารถลงเวลาออกได้ " + Environment.NewLine +
                                    "มีรายการชั้นวาง " + iShelf.ToString() + " ที่ยังไม่ได้ถ่ายรูป" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
                                ClearDefualt("1");
                                return;
                            }
                        }
                    }

                    if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, PageCaseDesc))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                        ClearDefualt(_PageCase);
                        radTextBox_Empl.Focus();
                        return;
                    }


                    SpanDay = GetTimeSpan(PW_DATETIME_IN, "DAY");
                    SpanTime = GetTimeSpan(PW_DATETIME_IN, "TIME");
                    //ซื้อสินค้าตามรหัสพนักงาน ถ้ารหัสพนักงานไม่มีรหัสลูกค้าจะไม่ไปหาบิลขาย
                    if (!(Empl.EMPLTABLE_EMP_AccountNum.Equals("")))
                    {
                        String[] POSBillAndMoney = GetPOS_Bill707AndCash(Empl.EMPLTABLE_EMP_AccountNum,
                           DateTime.Parse(PW_DATETIME_IN.ToString()).ToString("yyyy-MM-dd hh:mm:ss")).Split(',');
                        if (POSBillAndMoney.Length > 0)
                        {
                            Bill = POSBillAndMoney[0].ToString();
                            Money = POSBillAndMoney[1].ToString();
                        }
                    }

                    UpdateTbl(Bill, Money, rmk);
                    break;

                case "2":
                    if (double.Parse(SpanMinutes) > 40)
                    {
                        FormShare.ShowRemark frmRmk = new FormShare.ShowRemark("1");
                        if (frmRmk.ShowDialog(this) == DialogResult.Yes)
                        {
                            rmk = frmRmk.pRmk;
                            if (SystemClass.SystemUserPositionID == "03")
                            {
                                GeneralForm.TimeKeeper.ConfirmCheckTimekeep frmConfirm = new GeneralForm.TimeKeeper.ConfirmCheckTimekeep("0");
                                if (frmConfirm.ShowDialog(this) != DialogResult.OK)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถบันทึกลงเวลาเข้าไม่ได้{Environment.NewLine}เนื่องจากไม่มีสิทธิ์ในการบันทึก{Environment.NewLine}[ต้องยืนยันด้วยตำแหน่งผู้จัดการหรือผู้ช่วยเท่านั้น");
                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            Cursor.Current = Cursors.Default;
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุเหตุผลการพักเกินก่อนการบันทึกข้อมูล");
                            return;
                        }
                    }

                    if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "ลงเวลาเข้า พัก"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                        ClearDefualt(_PageCase);
                        radTextBox_Empl.Focus();
                        return;
                    }
                    UpdateTbl(Bill, Money, rmk);
                    break;
                case "5":
                    UpdateTbl(Bill, Money, rmk);
                    break;
            }

        }
        //Update
        void UpdateTbl(string Bill, string Money, string rmk)
        {
            MsgBoxClass.MsgBoxShow_SaveStatus(TimeKeeperClass.Update_TIMEKEEPER(_PageCase, PW_ID, SpanDay, SpanTime, Bill, Money, rmk));
            ClearDefualt(_PageCase);
            radTextBox_Empl.Focus();
        }

        private bool ScanFinger(string EmplID, string EmplName, string EmplPosition, string EmplDept, string staPage)
        {
            bool Rsu = true;
            //เช็คลายนิ้วมือผู้พิการ 
            if (TimeKeeperClass.GetEmplScanCard_ByEmplBranch(EmplID, SystemClass.SystemBranchID.Replace("MN", "")))
            {
                FormShare.FingerScan FingerScan = new FormShare.FingerScan(EmplID, staPage, EmplName, EmplPosition, EmplDept);

                if (FingerScan.ShowDialog() == DialogResult.Yes)
                {
                    Rsu = false;
                }
            }

            else
            {
                Rsu = false;
            }
            return Rsu;
        }
    }
}
