﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public partial class ShowDataShelf_ByEmp : Telerik.WinControls.UI.RadForm
    {
        public int iCheckRed;
        readonly DataTable dt;
        readonly string _pDate;
        
        public ShowDataShelf_ByEmp(DataTable dtE, string pDate)
        {
            InitializeComponent();
            _pDate = pDate;
            dt = dtE;
        }

        private void ShowDataShelf_ByEmp_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "โซน", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 160)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "ชั้นวาง", 80)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 300)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("MAXIMAGE", "กำหนด", 50)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CIMAGE", "รูปถ่าย", 50)));
            radGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "สถานะ")));

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "STA = '2' ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            radGridView_Show.Columns["SHELF_NAME"].ConditionalFormattingObjectList.Add(obj1);
            radGridView_Show.Columns["CIMAGE"].ConditionalFormattingObjectList.Add(obj1);
            radGridView_Show.Columns["MAXIMAGE"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "STA = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["SHELF_NAME"].ConditionalFormattingObjectList.Add(obj2);
            radGridView_Show.Columns["CIMAGE"].ConditionalFormattingObjectList.Add(obj2);
            radGridView_Show.Columns["MAXIMAGE"].ConditionalFormattingObjectList.Add(obj2);


            radGridView_Show.DataSource = dt;
            dt.AcceptChanges();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string pZone = dt.Rows[i]["ZONE_ID"].ToString();
                string pSH = dt.Rows[i]["SHELF_ID"].ToString();
                int MaxImage = Convert.ToInt32(dt.Rows[i]["MAXIMAGE"].ToString());
                string pathFileName = PathImageClass.pPathShelfNew + _pDate + @"\" + dt.Rows[i]["NUM"].ToString();
                DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);

                FileInfo[] Files;
                if (DirInfo.Exists)
                {
                    Files = DirInfo.GetFiles("*" + dt.Rows[i]["EMPLID"].ToString().Substring(1, 7) + @"_" + pZone + "#" + pSH + @".JPG", SearchOption.AllDirectories);
                    int cCheck = Files.Length;
                    if (cCheck == 0) { iCheckRed++; }
                    int chk = MaxImage - cCheck;
                    string sta = "2";
                    if (chk == 0) { sta = "0"; }//ไม่ได้ถ่ายรูปเลย
                    if (chk == MaxImage) { sta = "1"; } //ถ่ายรูปครบ
                    if (chk < 0) sta = "0";

                    radGridView_Show.Rows[i].Cells["CIMAGE"].Value = cCheck;
                    radGridView_Show.Rows[i].Cells["STA"].Value = sta;
                }
            }
        }

        //choose
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (iCheckRed > 0)
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
            else
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

    }
}
