﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.FormShare;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using PC_Shop24Hrs.GeneralForm.MNPO;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public partial class ScanDptTime : Telerik.WinControls.UI.RadForm
    {
        readonly EmplScan _emplScan;
        readonly string _PageCase;
        readonly string _StatusCase, _staOutAll;

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();
        string barcodePrint, whPrint, boxRuningPrint, routeBch, bchID, bchName, boxContainer,emplIDpacking, emplNamepacking;

        string OUTPHUKET;
        readonly EmplScan emplScanIn = new EmplScan { };

        Data_EMPLTABLE Empl;
        DataTable DtContainer;

        public string retail, wha, whrice, c_retail, c_wha, c_whrice;//ข้อมูลบิลที่ต้องการจัด

        // 0 - สแกนนิ้วเข้าแผนก
        // 1 - สแกนนิ้วสาขาเข้าสาขาใหญ่
        // IN - สแกนนิ้วขาเข้าสาขาใหญ่
        // OUT - สแกนนิ้วขาออกสาขาใหญ่

        public ScanDptTime(string PageCase, string StatusCase, EmplScan emplScan, string staOutAll = "0")
        {
            InitializeComponent();
            _PageCase = PageCase;
            _StatusCase = StatusCase;
            _emplScan = emplScan;
            _staOutAll = staOutAll;
        }

        private void ClearDefualt()
        {
            switch (_PageCase)
            {
                case "0": //SCANDEPT
                    radTextBox_Empl.Text = string.Empty;
                    radLabel_Name.Text = string.Empty;
                    radLabel_Possion.Text = string.Empty;
                    radLabel_OutOffice.Text = string.Empty;
                    radLabel_Dept.Text = string.Empty;
                    radTextBox_Empl.Enabled = true;
                    BindDropDownListLocation();
                    radTextBox_Empl.Focus();
                    break;
                case "1": //SCANINSPC
                    if (_StatusCase == "IN")
                    {
                        radTextBox_Empl.Enabled = true;
                        radTextBox_Empl.Focus();
                    }
                    radTextBox_Empl.Text = string.Empty;
                    radLabel_Name.Text = string.Empty;
                    radLabel_Possion.Text = string.Empty;
                    radLabel_OutOffice.Text = string.Empty;
                    radLabel_Dept.Text = string.Empty;
                    radLabel_OutOffice.Text = string.Empty;
                    radTextBox_Remark.Text = string.Empty; radTextBox_Remark.Enabled = false;
                    radTextBox_bill.Text = string.Empty; radTextBox_bill.Enabled = false;
                    radTextBox_Addon.Text = string.Empty; radTextBox_Addon.Enabled = false;
                    radTextBox_Hotel.Text = string.Empty; radTextBox_Hotel.Enabled = false;
                    radTextBox_Retail.Text = ""; radTextBox_Retail.Enabled = false;
                    radTextBox_wha.Text = ""; radTextBox_wha.Enabled = false;
                    radTextBox_whrice.Text = ""; radTextBox_whrice.Enabled = false;
                    RadButton_Save.Enabled = false;
                    break;
                default:
                    break;
            }

        }

        //Form Load
        private void TimeKeeperForm_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;//1
            barcode.BarHeight = 60;//38
            barcode.LeftMargin = 0;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radButton_refresh.ButtonElement.ShowBorder = true;
            radButton_pdt.ButtonElement.ShowBorder = true; radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_refresh.ButtonElement.ToolTipText = "refresh";
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Location);

            DtContainer = DtCheckContainer();

            switch (_PageCase)
            {
                case "0": //SCANDEPT
                    this.Size = new Size(560, 320);
                    radLabel8.Visible = false;
                    radLabel_OutOffice.Visible = false;
                    radLabel5.Visible = false;
                    radTextBox_Remark.Visible = false;
                    RadButton_Scan.Visible = false;
                    radTextBox_Empl.Text = string.Empty;
                    radLabel_Name.Text = string.Empty;
                    radLabel_Possion.Text = string.Empty;
                    radLabel_Dept.Text = string.Empty;
                    radLabel_OutOffice.Text = string.Empty;
                    radTextBox_Remark.Text = string.Empty;
                    radTextBox_bill.Text = string.Empty;
                    radTextBox_Addon.Text = string.Empty;
                    radTextBox_Hotel.Text = string.Empty;
                    BindDropDownListLocation();
                    break;
                case "1": //SCANINSPC

                    radLabel4.Visible = false;
                    radDropDownList_Location.Visible = false;
                    radTextBox_bill.Enabled = false;
                    radTextBox_Addon.Enabled = false;
                    radTextBox_Hotel.Enabled = false;

                    RadButton_Save.ButtonElement.ShowBorder = true;
                    RadButton_ScanOut.ButtonElement.ShowBorder = true;
                    RadButton_Scan.ButtonElement.ShowBorder = true; RadButton_Scan.ButtonElement.ToolTipText = "สแกนนิ้วออกแทนพนักงาน";

                    if (_StatusCase != "")
                    {
                        switch (_StatusCase)
                        {
                            case "IN": //SCANIN
                                this.Text = "สแกนนิ้วเข้าสาขาใหญ่.";
                                radTextBox_Empl.Text = string.Empty;
                                radLabel_Name.Text = string.Empty;
                                radLabel_Possion.Text = string.Empty;
                                radLabel_Dept.Text = string.Empty;
                                radLabel_OutOffice.Text = string.Empty;
                                radTextBox_Remark.Text = string.Empty;
                                radTextBox_bill.Text = string.Empty;
                                radTextBox_Hotel.Text = string.Empty;
                                radTextBox_Addon.Text = string.Empty;

                                radLabel5.Enabled = false;
                                radLabel6.Enabled = false;
                                radLabel11.Enabled = false;
                                radLabel12.Enabled = false;
                                radTextBox_Remark.Enabled = false;
                                RadButton_Scan.Visible = false;
                                radTextBox_Empl.Enabled = true;
                                RadButton_Save.Enabled = false;
                                radTextBox_Retail.Enabled = false;
                                radTextBox_wha.Enabled = false;
                                radTextBox_whrice.Enabled = false;
                                RadButton_ScanOut.Enabled = false;
                                radButton_printRetail.Visible = false;
                                radButton_printwha.Visible = false;
                                radButton_printrice.Visible = false;
                                radTextBox_Empl.Focus();
                                break;
                            case "OUT": //SCANOUT
                                this.Text = "สแกนนิ้วออกสาขาใหญ่.";

                                radTextBox_Empl.Text = _emplScan.EMPLID;
                                radLabel_Name.Text = _emplScan.EMPLNAME;
                                radLabel_Possion.Text = _emplScan.POSITION;
                                radLabel_Dept.Text = _emplScan.DPT;
                                radLabel_OutOffice.Text = _emplScan.EMPLOUT;
                                radTextBox_Remark.Text = _emplScan.PRODUCT;
                                radTextBox_bill.Text = _emplScan.OIL.ToString("N2");
                                radTextBox_Addon.Text = _emplScan.Pay_AddOn.ToString("N2");
                                radTextBox_Hotel.Text = _emplScan.Pay_Hotel.ToString("N2");

                                radTextBox_Retail.Text = c_retail; radLabel_retail.Text = retail;
                                radTextBox_wha.Text = c_wha; radLabel_wha.Text = wha;
                                radTextBox_whrice.Text = c_whrice; radLabel_whrice.Text = whrice;

                                if (int.Parse(c_retail) > 0) radButton_printRetail.Visible = true;
                                else radButton_printRetail.Visible = false;
                                if (int.Parse(c_wha) > 0) radButton_printwha.Visible = true;
                                else radButton_printwha.Visible = false;
                                if (int.Parse(c_whrice) > 0) radButton_printrice.Visible = true;
                                else radButton_printrice.Visible = false;

                                radLabel5.Enabled = true;
                                radLabel6.Enabled = true;
                                radLabel11.Enabled = true;
                                radLabel12.Enabled = true;
                                radButton_refresh.Visible = false;
                                radTextBox_Empl.Enabled = false;
                                radTextBox_Remark.Enabled = false;
                                RadButton_Save.Enabled = false;
                                radTextBox_Retail.Enabled = false;
                                radTextBox_wha.Enabled = false;
                                radTextBox_whrice.Enabled = false;
                                RadButton_ScanOut.Enabled = true;

                                if (_staOutAll == "1")
                                {
                                    radButton_printRetail.Visible = false; radButton_printwha.Visible = false; radButton_printrice.Visible = false;
                                    RadButton_ScanOut.Enabled = false;
                                    RadButton_Scan.Visible = false;
                                }
                                RadButton_ScanOut.Focus();
                                break;
                            default:
                                break;
                        }
                    }

                    break;
                default:
                    break;
            }

        }

        private void RadTextBox_Empl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Empl.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ต้องใส่รหัสพนักงานก่อน.");
                    ClearDefualt();
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;

                switch (_PageCase)
                {
                    case "0": //SCANDEPT

                        this.Empl = new Data_EMPLTABLE(radTextBox_Empl.Text);

                        if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                        {
                            radLabel_Name.Text = Empl.EMPLTABLE_SPC_NAME; //ชื่อ
                            radLabel_Possion.Text = Empl.EMPLTABLE_POSSITION; //ตำแหน่ง
                            radLabel_Dept.Text = Empl.EMPLTABLE_DESCRIPTION; //แผนก
                            radTextBox_Empl.Enabled = false;

                            Invoke(new Action(() => RadButton_Save_Click(sender, e))); //เรียกใช้ฟังก์ชัน Save
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน.");
                            ClearDefualt();
                        }
                        break;
                    case "1": //SCANINSPC

                        switch (_StatusCase)
                        {
                            case "IN": //SCANIN

                                string PW_IDCARD = $@" AND PW_IDCARD LIKE '%{radTextBox_Empl.Text}'";

                                DataTable dtScanIn = TimeKeeperClass.ScanInSPC_OutOffice(DateTime.Today.ToString("yyyy-MM-dd"), radTextBox_Empl.Text); //เช็คใบออกนอก
                                if (dtScanIn.Rows.Count > 0)
                                {
                                    emplScanIn.DOCID = dtScanIn.Rows[0]["DOCID"].ToString(); //เลขเอกสารออกนอก
                                    emplScanIn.EMPLOUT = dtScanIn.Rows[0]["OUTOFFICEID"].ToString(); //จำนวนพนักงาน

                                    DataTable dtDocid = TimeKeeperClass.ScanInSPC_Detail("2", $@" DOCID = '{emplScanIn.DOCID}'", ""); //เช็คเลขออกนอก
                                    if (dtDocid.Rows.Count > 0)
                                    {
                                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"เลขที่ใบออกนอก {emplScanIn.DOCID}{Environment.NewLine}มีพนักงานมาสแกนนิ้วเข้าสาขาใหญ่แล้ว{Environment.NewLine}ไม่ต้องสแกนซ้ำ.");
                                        ClearDefualt();
                                        radTextBox_Empl.Enabled = true;
                                        radTextBox_Empl.Focus();
                                        return;
                                    }
                                }
                                else
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานไม่มีข้อมูลในเอกสารออกนอก HrOnline ไม่สามารถสแกนเข้าสาขาใหญ่ได้.");
                                    radTextBox_Empl.Enabled = true;
                                    radTextBox_Empl.SelectAll();
                                    radTextBox_Empl.Focus();
                                    return;
                                }

                                DataTable dtScan24 = EmployeeClass.EmployeeTimeStampDetail(DateTime.Today.ToString("yyyy-MM-dd"), DateTime.Today.ToString("yyyy-MM-dd"), PW_IDCARD); //เช็คเข้างาน
                                if (dtScan24.Rows.Count == 0)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานไม่มีข้อมูลการสแกนนิ้วเข้างานที่มินิมาร์ท ไม่สามารถสแกนเข้าสาขาใหญ่ได้.");
                                    ClearDefualt();
                                    radTextBox_Empl.Enabled = true;
                                    radTextBox_Empl.Focus();
                                    return;
                                }

                                OUTPHUKET = "0";
                                DataTable dtBch = BranchClass.GetBranchSettingBranch(dtScan24.Rows[0]["BRANCH_ID"].ToString()); //เช็คสาขาต่างจังหวัด
                                if (dtBch.Rows[0]["BRANCH_OutPhuket"].ToString() == "1") OUTPHUKET = "1";

                                emplScanIn.EMPLID = dtScan24.Rows[0]["PW_IDCARD"].ToString(); //รหัสพนักงาน
                                emplScanIn.EMPLNAME = dtScan24.Rows[0]["PW_NAME"].ToString(); //ชื่อพนักงาน
                                emplScanIn.BRANCHID = dtScan24.Rows[0]["BRANCH_ID"].ToString(); //เลขสาขา
                                emplScanIn.DPT = dtScan24.Rows[0]["BRANCH_NAME"].ToString(); //แผนก
                                emplScanIn.POSITION = dtScan24.Rows[0]["DESCRIPTION"].ToString(); //ตำแหน่ง

                                radLabel_Name.Text = emplScanIn.EMPLNAME;
                                radLabel_Possion.Text = emplScanIn.POSITION;
                                radLabel_Dept.Text = emplScanIn.DPT;
                                radLabel_OutOffice.Text = emplScanIn.EMPLOUT;
                                radTextBox_Empl.Enabled = false;
                                radLabel5.Enabled = true;
                                radTextBox_Remark.Enabled = true;
                                radTextBox_Remark.Focus();

                                break;
                            case "OUT": //SCANOUT
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void RadTextBox_Remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Remark.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ต้องใส่ประเภทสินค้าก่อน.");
                    radTextBox_Remark.Focus();
                    return;
                }

                emplScanIn.PRODUCT = radTextBox_Remark.Text;
                radTextBox_Remark.Enabled = false;

                if (OUTPHUKET == "0")
                {
                    radLabel5.Enabled = false;
                    radTextBox_bill.Enabled = true;
                    radLabel6.Enabled = true;
                    radTextBox_bill.Focus();
                    return;
                }

                radTextBox_Retail.Enabled = true;
                radTextBox_Retail.Text = "";
                radTextBox_Retail.Focus();


            }
        }

        private void RadTextBox_bill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_bill.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ต้องใส่ค่าน้ำมันก่อน.");
                    radTextBox_bill.Focus();
                    return;
                }

                emplScanIn.OIL = float.Parse(radTextBox_bill.Text);
                radTextBox_bill.Enabled = false;
                radLabel6.Enabled = false;
                radLabel11.Enabled = true;
                radTextBox_Hotel.Enabled = true;
                radTextBox_Hotel.Text = "";
                radTextBox_Hotel.Focus();
            }
        }

        private void RadTextBox_Hotel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Hotel.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ต้องใส่ค่าที่พักก่อน.");
                    radTextBox_Hotel.Focus();
                    return;
                }

                emplScanIn.Pay_Hotel = float.Parse(radTextBox_Hotel.Text);
                radTextBox_Hotel.Enabled = false;
                radLabel11.Enabled = false;
                radLabel12.Enabled = true;
                radTextBox_Addon.Enabled = true;
                radTextBox_Addon.Text = "";
                radTextBox_Addon.Focus();
            }
        }

        private void RadTextBox_Addon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Addon.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ต้องใส่ค่าเบี้ยเลี้ยงก่อน.");
                    radTextBox_Addon.Focus();
                    return;
                }

                emplScanIn.Pay_AddOn = float.Parse(radTextBox_Addon.Text);
                radTextBox_Addon.Enabled = false;
                radLabel12.Enabled = false;
                radTextBox_Retail.Enabled = true;
                radTextBox_Retail.Text = "";
                radTextBox_Retail.Focus();
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            switch (_PageCase)
            {
                case "0": //SCANDEPT
                    #region "SCANDEPT"
                    DataTable CheckAss = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("43", $@"AND SHOW_ID = '{Empl.EMPLTABLE_EMPLID}'", "", "1"); //เช็คว่าต้องให้ผู้ช่วยสแกนแทนมั้ย
                    string location = radDropDownList_Location.SelectedItem[0].ToString(); //set location
                    string assistantquery = "";
                    //แทน
                    if (CheckAss.Rows.Count > 0)
                    {
                        string EmplDimension = CheckAss.Rows[0]["REMARK"].ToString();
                        if (EmplDimension == "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลการตั้งค่ารหัสแผนก ตรวจสอบการตั้งค่าอีกครั้ง.");
                            ClearDefualt();
                            return;
                        }

                        FormShare.InputData _inputData = new FormShare.InputData("0", "รหัสผู้ช่วยฯ", "ใส่รหัสผู้ช่วยฯ", ""); //ใส่รหัสผู้ช่วย
                        if (_inputData.ShowDialog(this) == DialogResult.Yes)
                        {
                            DataTable assistant = Models.EmplClass.GetEmployee_Altnum(_inputData.pInputData);

                            if (assistant.Rows.Count == 0) //เช็คสิทธิ์ผู้ช่วย
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบสิทธิ์ผู้ช่วย ตรวจสอบรหัสผู้ช่วยฯอีกครั้ง.");
                                ClearDefualt();
                                return;
                            }
                            if (assistant.Rows[0]["NUM"].ToString() != EmplDimension) //เช็คแผนก
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"พนักงานที่ระบุไม่ได้มีสิทธิ์สแกนนิ้วแทนได้{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง.");
                                ClearDefualt();
                                return;
                            }

                            if (ScanFinger(assistant.Rows[0]["ALTNUM"].ToString(), assistant.Rows[0]["SPC_NAME"].ToString(), assistant.Rows[0]["POSSITION"].ToString(), assistant.Rows[0]["DESCRIPTION"].ToString(), "สแกนบันทึกเวลา"))
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว.");
                                ClearDefualt();
                                return;
                            }

                            assistantquery = assistant.Rows[0]["ALTNUM"].ToString() + "-" + assistant.Rows[0]["SPC_NAME"].ToString();
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องใส่รหัสผู้ช่วยฯก่อน.");
                            ClearDefualt();
                            return;
                        }
                    }
                    else
                    {
                        if (ScanFinger(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_POSSITION, Empl.EMPLTABLE_DESCRIPTION, "สแกนบันทึกเวลา"))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว.");
                            ClearDefualt();
                            return;
                        }
                    }

                    MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(TimeKeeperClass.ScanDept_Save(Empl.EMPLTABLE_EMPLID, Empl.EMPLTABLE_EMPLID_M, Empl.EMPLTABLE_SPC_NAME, Empl.EMPLTABLE_NUM, Empl.EMPLTABLE_POSSITION, location, assistantquery)));
                    ClearDefualt();
                    #endregion
                    break;
                case "1": //SCANINSPC

                    switch (_StatusCase)
                    {
                        case "IN": //SCANIN

                            if (ScanFinger(emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.POSITION, emplScanIn.DPT, "สแกนนิ้วเข้าสาขาใหญ่"))
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                                return;
                            }

                            //string resault = ConnectionClass.ExecuteSQL_Main(TimeKeeperClass.ScanInSPC_Save(emplScanIn));
                            string maxDocnoRetail = "", maxDocnoWha = "", maxDocnoWhRice = "";
                            int c_Retail = int.Parse(radTextBox_Retail.Text), c_Wha = int.Parse(radTextBox_wha.Text), c_WhRice = int.Parse(radTextBox_whrice.Text);
                            ArrayList sql24 = new ArrayList();
                            ArrayList sqlAX = new ArrayList();

                            string container_Retail = "1", container_A = "1", container_Rice = "1";

                            if (c_Retail > 0)
                            {
                                maxDocnoRetail = Class.ConfigClass.GetMaxINVOICEID("MNPF", "-", "MNPF", "1");

                                if (DtContainer.Rows.Count > 0)
                                {
                                    DataRow[] dr = DtContainer.Select(" VARIABLENAME = 'RETAILAREA' ");
                                    if (dr.Length != 0) { container_Retail = dr[0]["SERVERIP"].ToString(); }
                                }

                                sqlAX.Add(SqlSendAXHD("AX", maxDocnoRetail, "RETAILAREA", emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.BRANCHID, $@"มินิมาร์ทจัดสินค้า-{emplScanIn.DPT}", ""));
                                sql24.Add(SqlSendAXHD("24", maxDocnoRetail, "RETAILAREA", emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.BRANCHID, $@"{emplScanIn.DPT}", container_Retail));
                                sqlAX.Add(SqlSendAXDT("AX", maxDocnoRetail, "RETAILAREA", emplScanIn.EMPLID));
                                sql24.Add(SqlSendAXDT("24", maxDocnoRetail, "RETAILAREA", emplScanIn.EMPLID));


                                if (container_Retail == "0")
                                {
                                    //Add To Container
                                    sql24.Add($@"
                                               INSERT INTO SHOP_WEB_PDTTRANSORDER_PICKING (ORDERNUM, TRANSID, DIMENSION, EMPLID, USERCREATE
                                               ,[INVENTLOCATIONIDFROM],[INVENTLOCATIONIDTO] ,[INVENTLOCATIONIDTONAME],STATYPEOPEN)
                                                VALUES(
                                                    N'{maxDocnoRetail}',N'{maxDocnoRetail}', N'{emplScanIn.BRANCHID}', N'{emplScanIn.EMPLID}', N'{SystemClass.SystemUserID_M}'
                                                    ,'RETAILAREA','{emplScanIn.BRANCHID}','{emplScanIn.DPT}','1' ) ");
                                }
                            }

                            if (c_Wha > 0)
                            {
                                maxDocnoWha = Class.ConfigClass.GetMaxINVOICEID("MNPF", "-", "MNPF", "1");
                                if (DtContainer.Rows.Count > 0)
                                {
                                    DataRow[] dr = DtContainer.Select(" VARIABLENAME = 'WH-A' ");
                                    if (dr.Length != 0) { container_A = dr[0]["SERVERIP"].ToString(); }
                                }

                                sqlAX.Add(SqlSendAXHD("AX", maxDocnoWha, "WH-A", emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.BRANCHID, $@"มินิมาร์ทจัดสินค้า-{emplScanIn.DPT}", ""));
                                sql24.Add(SqlSendAXHD("24", maxDocnoWha, "WH-A", emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.BRANCHID, $@"{emplScanIn.DPT}", container_A));
                                sqlAX.Add(SqlSendAXDT("AX", maxDocnoWha, "WH-A", emplScanIn.EMPLID));
                                sql24.Add(SqlSendAXDT("24", maxDocnoWha, "WH-A", emplScanIn.EMPLID));


                                if (container_A == "0")
                                {
                                    //Add To Container
                                    sql24.Add($@"
                                               INSERT INTO SHOP_WEB_PDTTRANSORDER_PICKING (ORDERNUM, TRANSID, DIMENSION, EMPLID, USERCREATE
                                               ,[INVENTLOCATIONIDFROM],[INVENTLOCATIONIDTO] ,[INVENTLOCATIONIDTONAME],STATYPEOPEN)
                                                VALUES(
                                                    N'{maxDocnoWha}',N'{maxDocnoWha}', N'{emplScanIn.BRANCHID}', N'{emplScanIn.EMPLID}', N'{SystemClass.SystemUserID_M}'
                                                    ,'WH-A','{emplScanIn.BRANCHID}','{emplScanIn.DPT}','1' ) ");
                                }
                            }

                            if (c_WhRice > 0)
                            {
                                maxDocnoWhRice = Class.ConfigClass.GetMaxINVOICEID("MNPF", "-", "MNPF", "1");
                                if (DtContainer.Rows.Count > 0)
                                {
                                    DataRow[] dr = DtContainer.Select(" VARIABLENAME = 'WH-RICE' ");
                                    if (dr.Length != 0) { container_Rice = dr[0]["SERVERIP"].ToString(); }
                                }

                                sqlAX.Add(SqlSendAXHD("AX", maxDocnoWhRice, "WH-RICE", emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.BRANCHID, $@"มินิมาร์ทจัดสินค้า-{emplScanIn.DPT}", ""));
                                sql24.Add(SqlSendAXHD("24", maxDocnoWhRice, "WH-RICE", emplScanIn.EMPLID, emplScanIn.EMPLNAME, emplScanIn.BRANCHID, $@"{emplScanIn.DPT}", container_Rice));
                                sqlAX.Add(SqlSendAXDT("AX", maxDocnoWhRice, "WH-RICE", emplScanIn.EMPLID));
                                sql24.Add(SqlSendAXDT("24", maxDocnoWhRice, "WH-RICE", emplScanIn.EMPLID));


                                if (container_Rice == "0")
                                {
                                    //Add To Container
                                    sql24.Add($@"
                                               INSERT INTO SHOP_WEB_PDTTRANSORDER_PICKING (ORDERNUM, TRANSID, DIMENSION, EMPLID, USERCREATE
                                               ,[INVENTLOCATIONIDFROM],[INVENTLOCATIONIDTO] ,[INVENTLOCATIONIDTONAME],STATYPEOPEN)
                                                VALUES(
                                                    N'{maxDocnoWhRice}',N'{maxDocnoWhRice}', N'{emplScanIn.BRANCHID}', N'{emplScanIn.EMPLID}', N'{SystemClass.SystemUserID_M}'
                                                    ,'WH-RICE','{emplScanIn.BRANCHID}','{emplScanIn.DPT}','1' ) ");
                                }
                            }


                            sql24.Add(TimeKeeperClass.ScanInSPC_Save(emplScanIn, maxDocnoRetail, c_Retail, maxDocnoWha, c_Wha, maxDocnoWhRice, c_WhRice));

                            string resault = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX);

                            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
                            if (resault == "")
                            {
                                DialogResult result = printDialog1.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    bchID = emplScanIn.BRANCHID;
                                    bchName = emplScanIn.DPT;
                                    emplIDpacking = emplScanIn.EMPLID;
                                    emplNamepacking = emplScanIn.EMPLNAME;
                                    printDocument1.PrintController = printController;
                                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                                    boxContainer = "0";
                                    DataTable dtRoute = LogisticClass.GetRouteAll_GroupBranch($@" AND ACCOUNTNUM = '{emplScanIn.BRANCHID}' ");//GetRouteExcel_ByDept
                                    if (dtRoute.Rows.Count == 0) routeBch = "";
                                    else
                                        routeBch = dtRoute.Rows[0]["ROUTEID"].ToString() + Environment.NewLine + dtRoute.Rows[0]["NAME"].ToString();

                                    for (int i = 0; i < c_Retail; i++)
                                    {
                                        whPrint = "RETAIL"; barcodePrint = maxDocnoRetail;
                                        boxRuningPrint = (i + 1).ToString() + "/" + c_Retail.ToString();
                                        boxContainer = container_Retail;
                                        printDocument1.Print();
                                    }
                                    for (int i = 0; i < c_Wha; i++)
                                    {
                                        whPrint = "WH-A"; barcodePrint = maxDocnoWha;
                                        boxRuningPrint = (i + 1).ToString() + "/" + c_Wha.ToString();
                                        boxContainer = container_A;
                                        printDocument1.Print();
                                    }
                                    for (int i = 0; i < c_WhRice; i++)
                                    {
                                        whPrint = "RICE"; barcodePrint = maxDocnoWhRice;
                                        boxRuningPrint = (i + 1).ToString() + "/" + c_WhRice.ToString();
                                        boxContainer = container_Rice;
                                        printDocument1.Print();
                                    }
                                }
                                this.DialogResult = DialogResult.Yes;
                            }

                            this.Close();
                            break;
                        case "OUT": //SCANOUT
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        //SendAX-Save24
        public static string SqlSendAXHD(string pCase, string docno, string wh, string emplID, string emplName, string bchID, string rmk, string dlvTerm)
        {
            if (pCase == "AX")
            {
                Var_POSTOTABLE varHD = new Var_POSTOTABLE
                {
                    FREIGHTSLIPTYPE = "98",
                    CASHIERID = emplID,
                    ALLPRINTSTICKER = "0",
                    TRANSFERSTATUS = "0",
                    INVENTLOCATIONIDTO = bchID,
                    INVENTLOCATIONIDFROM = wh,
                    TRANSFERID = docno,
                    SHIPDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                    VOUCHERID = docno,
                    REMARKS = rmk,
                    DlvTermId = dlvTerm
                };
                return AX_SendData.SaveHD_POSTOTABLE18(varHD);
            }
            else
            {
                Var_MNPO_HD var24HD = new Var_MNPO_HD
                {
                    MNPODocNo = docno,
                    MNPOUserCode = emplID,
                    MNPOUserNameCode = emplName,
                    MNPOBranch = bchID,
                    MNPOBranchName = rmk,
                    MNPOTypeOrder = "สาขา[จัดของสาขาใหญ่]",
                    MNPODptCode = bchID,
                    MNPORemark = "",
                    MNPOStaApvDoc = "1",
                    MNPOStaPrcDoc = "1",
                    MNPOWhoIn = emplID,
                    MNPODateApv = DateTime.Now.ToString("yyyy-MM-dd"),
                    MNPOTimeApv = DateTime.Now.ToString("HH:mm:ss"),
                    MNPOWhoApv = emplID,
                    MNPOWhoNameApv = emplName
                };
                return PO_Class.Save_MNPOHD(var24HD, dlvTerm);
            }
        }
        //SendAX-Save24
        public static string SqlSendAXDT(string pCase, string docno, string wh, string emplID)
        {
            if (pCase == "AX")
            {
                Var_POSTOLINE varDT = new Var_POSTOLINE()
                {
                    VOUCHERID = docno,
                    SHIPDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                    LINENUM = 1,
                    QTY = 1,
                    SALESPRICE = 0,
                    ITEMID = "P0084197",
                    NAME = "มินิมาร์ทจัดสินค้า",
                    SALESUNIT = "ชิ้น",
                    ITEMBARCODE = "MN0002",
                    INVENTDIMID = "DIM13-000566071",
                    INVENTTRANSID = $@"{docno}_{1}"
                };
                return AX_SendData.SaveDT_POSTOLINE18(varDT);
            }
            else
            {
                Var_MNPO_DT varDT24 = new Var_MNPO_DT()
                {
                    MNPODocNo = docno,
                    MNPOSeqNo = 1,
                    MNPOItemID = "P0084197",
                    MNPODimid = "DIM13-000566071",
                    MNPOBarcode = "MN0002",
                    MNPOName = "มินิมาร์ทจัดสินค้า",
                    MNPOQtyOrder = 1,
                    MNPOUnitID = "ชิ้น",
                    MNPOFactor = 1,
                    MNPODATEPO = "",
                    MNPODATEPR = "",
                    MNPOINVENT = wh,
                    MNPOWhoIn = emplID

                };
                return PO_Class.Save_MNPODT(varDT24);
            }
        }
        //ScanOut
        private void RadButton_ScanOut_Click(object sender, EventArgs e)
        {
            if (ScanFinger(_emplScan.EMPLID, _emplScan.EMPLNAME, _emplScan.POSITION, _emplScan.DPT, "สแกนนิ้วออกสาขาใหญ่"))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                this.Close();
                return;
            }

            string resault = ConnectionClass.ExecuteSQL_Main(TimeKeeperClass.ScanInSPC_Update(_emplScan.EMPLID, _emplScan.SCANINTIME, "",
                retail, wha, whrice));
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void RadButton_Scan_Click(object sender, EventArgs e)
        {
            string ASSISTANT;
            FormShare.InputData _inputData = new FormShare.InputData("0", "รหัสพนักงานสแกนแทน", "ใส่รหัสพนักงานสแกนแทน", ""); //ใส่รหัสพนักงาน เฉพาะ D054
            if (_inputData.ShowDialog(this) == DialogResult.Yes)
            {
                DataTable assistant = Models.EmplClass.GetEmployee_Altnum(_inputData.pInputData);

                if (assistant.Rows.Count == 0) //เช็คสิทธิ์ผู้ช่วย
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบสิทธิ์ผู้ช่วย ตรวจสอบรหัสผู้ช่วยฯอีกครั้ง.");
                    return;
                }

                if (assistant.Rows[0]["NUM"].ToString() != "D054") //เช็คแผนก
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"พนักงานที่ระบุไม่ได้มีสิทธิ์สแกนนิ้วแทนได้{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง");
                    return;
                }

                if (ScanFinger(assistant.Rows[0]["ALTNUM"].ToString(), assistant.Rows[0]["SPC_NAME"].ToString(), assistant.Rows[0]["POSSITION"].ToString(), assistant.Rows[0]["DESCRIPTION"].ToString(), "สแกนบันทึกเวลา"))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว.");
                    return;
                }

                ASSISTANT = assistant.Rows[0]["ALTNUM"].ToString() + "-" + assistant.Rows[0]["SPC_NAME"].ToString();
            }
            else return;
            string assistantscan = $@" ,EMPLIDREPLESE = '{ASSISTANT}' ";
            string resault = ConnectionClass.ExecuteSQL_Main(TimeKeeperClass.ScanInSPC_Update(_emplScan.EMPLID, _emplScan.SCANINTIME, assistantscan, retail, wha, whrice));
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        #region PDF
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        #endregion

        //refresh
        private void RadButton_refresh_Click(object sender, EventArgs e)
        {
            ClearDefualt();
        }

        private void BindDropDownListLocation()
        {
            radDropDownList_Location.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("44", "", " ORDER BY SHOW_ID ", "1");
            radDropDownList_Location.ValueMember = "SHOW_ID";
            radDropDownList_Location.DisplayMember = "SHOW_NAME";
        }

        private bool ScanFinger(string EmplID, string EmplName, string EmplPosition, string EmplDept, string staPage)
        {
            bool Rsu = true;
            //เช็คลายนิ้วมือผู้พิการ 
            if (TimeKeeperClass.GetEmplScanCard_ByEmplBranch(EmplID, SystemClass.SystemBranchID.Replace("MN", "")))
            {
                FingerScan FingerScan = new FingerScan(EmplID, staPage, EmplName, EmplPosition, EmplDept);

                if (FingerScan.ShowDialog() == DialogResult.Yes)
                {
                    Rsu = false;
                }
            }

            else
            {
                Rsu = false;
            }
            return Rsu;

        }

        private void RadDropDownList_Location_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            radTextBox_Empl.Focus();
        }

        private void RadTextBox_Empl_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadButton_printwha_Click(object sender, EventArgs e)
        {
            RePrintBill(int.Parse(c_wha), "WH-A", radLabel_wha.Text);
        }

        private void RadButton_printrice_Click(object sender, EventArgs e)
        {
            RePrintBill(int.Parse(c_whrice), "RICE", radLabel_whrice.Text);
        }

        private void RadTextBox_Addon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }

        private void RadTextBox_Hotel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }

        private void RadButton_printRetail_Click(object sender, EventArgs e)
        {
            RePrintBill(int.Parse(c_retail), "RETAIL", radLabel_retail.Text);
        }

        void RePrintBill(int c_copy, string invent, string docno)
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                printDocument1.PrintController = printController;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                DataTable dtRoute = LogisticClass.GetRouteAll_GroupBranch($@" AND ACCOUNTNUM = '{_emplScan.BRANCHID}' ");//GetRouteExcel_ByDept
                if (dtRoute.Rows.Count == 0) routeBch = "";
                else routeBch = dtRoute.Rows[0]["ROUTEID"].ToString() + Environment.NewLine + dtRoute.Rows[0]["NAME"].ToString();

                boxContainer = "1";
                if (DtContainer.Rows.Count > 0)
                {
                    string wh = "WH-A";
                    if (invent == "RICE") { wh = "WH-RICE"; }
                    if (invent == "RETAIL") { wh = "RETAILAREA"; }

                    DataRow[] dr = DtContainer.Select($@" VARIABLENAME = '{wh}' ");
                    if (dr.Length != 0) { boxContainer = dr[0]["SERVERIP"].ToString(); }
                }

                whPrint = invent; barcodePrint = docno;
                bchID = _emplScan.BRANCHID;
                bchName = _emplScan.DPT;
                emplIDpacking = _emplScan.EMPLID;
                emplNamepacking = _emplScan.EMPLNAME;
                for (int i = 0; i < c_copy; i++)
                {
                    boxRuningPrint = (i + 1).ToString() + "/" + c_copy.ToString();
                    printDocument1.Print();
                }
            }
        }

        private void RadTextBox_bill_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }

        private void RadTextBox_Retail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;
        }

        private void RadTextBox_wha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;
        }

        private void RadTextBox_whrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;
        }

        private void RadTextBox_Retail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;

            if (radTextBox_Retail.Text == "")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการไม่เปิดบิลจัดคลัง RETAILAREA ?.") == DialogResult.No)
                {
                    radTextBox_Retail.SelectAll();
                    radTextBox_Retail.Focus();
                    return;
                }
                else
                {
                    radTextBox_Retail.Text = "0";
                }
            }

            radTextBox_Retail.Enabled = false;
            radTextBox_wha.Enabled = true;
            radTextBox_wha.Text = "";
            radTextBox_wha.Focus();

        }

        private void RadTextBox_wha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            if (radTextBox_wha.Text == "")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการไม่เปิดบิลจัดคลัง WH-A ?.") == DialogResult.No)
                {
                    radTextBox_wha.SelectAll();
                    radTextBox_wha.Focus();
                    return;
                }
                else
                {
                    radTextBox_wha.Text = "0";
                }
            }

            radTextBox_wha.Enabled = false;
            radTextBox_whrice.Enabled = true;
            radTextBox_whrice.Text = "";
            radTextBox_whrice.Focus();
        }

        private void RadTextBox_whrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            if (radTextBox_whrice.Text == "")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการไม่เปิดบิลจัดคลัง WH-RICE ?.") == DialogResult.No)
                {
                    radTextBox_whrice.SelectAll();
                    radTextBox_whrice.Focus();
                    return;
                }
                else
                {
                    radTextBox_whrice.Text = "0";
                }
            }

            radTextBox_whrice.Enabled = false;


            RadButton_Save.Enabled = true;
            RadButton_Save.Focus();//เรียกปุ่มSave
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Var_Print_MNPT_MNPF var = new Var_Print_MNPT_MNPF
            {
                Docno = barcodePrint,
                BranchID = bchID,
                BranchName = bchName,
                InventLocationID = whPrint,
                Route = routeBch,
                BOX = boxRuningPrint,
                EmplIDPacking = emplIDpacking,
                EmplNamePacking = emplNamepacking
            };

            PrintClass.Print_MNPT_MNPF("MNPF", e, var, "0", boxContainer);
        }

        DataTable DtCheckContainer()
        {
            return ConnectionClass.SelectSQL_Main($@" SELECT	*	FROM	SHOP_CONFIGDB WITH (NOLOCK) WHERE VARIABLENAME IN ('RETAILAREA','WH-A','WH-RICE') ");
        }
    }
}
