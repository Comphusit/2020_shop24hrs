﻿namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    partial class ScanDptTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanDptTime));
            this.panel2_header = new System.Windows.Forms.Panel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Addon = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Hotel = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_printrice = new Telerik.WinControls.UI.RadButton();
            this.radButton_printwha = new Telerik.WinControls.UI.RadButton();
            this.radButton_printRetail = new Telerik.WinControls.UI.RadButton();
            this.radLabel_whrice = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_wha = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_retail = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_whrice = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_wha = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Retail = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_ScanOut = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Scan = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel_OutOffice = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Location = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_bill = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_refresh = new Telerik.WinControls.UI.RadButton();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Dept = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Possion = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Empl = new Telerik.WinControls.UI.RadTextBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.panel2_header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Addon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Hotel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printwha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printRetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_whrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_wha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_retail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_whrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_wha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Retail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_ScanOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Scan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_OutOffice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_refresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2_header
            // 
            this.panel2_header.Controls.Add(this.radLabel12);
            this.panel2_header.Controls.Add(this.radLabel11);
            this.panel2_header.Controls.Add(this.radTextBox_Addon);
            this.panel2_header.Controls.Add(this.radTextBox_Hotel);
            this.panel2_header.Controls.Add(this.radButton_printrice);
            this.panel2_header.Controls.Add(this.radButton_printwha);
            this.panel2_header.Controls.Add(this.radButton_printRetail);
            this.panel2_header.Controls.Add(this.radLabel_whrice);
            this.panel2_header.Controls.Add(this.radLabel_wha);
            this.panel2_header.Controls.Add(this.radLabel_retail);
            this.panel2_header.Controls.Add(this.radTextBox_whrice);
            this.panel2_header.Controls.Add(this.radTextBox_wha);
            this.panel2_header.Controls.Add(this.radTextBox_Retail);
            this.panel2_header.Controls.Add(this.radLabel10);
            this.panel2_header.Controls.Add(this.radLabel9);
            this.panel2_header.Controls.Add(this.radLabel7);
            this.panel2_header.Controls.Add(this.RadButton_ScanOut);
            this.panel2_header.Controls.Add(this.RadButton_Scan);
            this.panel2_header.Controls.Add(this.RadButton_Save);
            this.panel2_header.Controls.Add(this.radLabel_OutOffice);
            this.panel2_header.Controls.Add(this.radLabel6);
            this.panel2_header.Controls.Add(this.radDropDownList_Location);
            this.panel2_header.Controls.Add(this.radTextBox_bill);
            this.panel2_header.Controls.Add(this.radLabel8);
            this.panel2_header.Controls.Add(this.radTextBox_Remark);
            this.panel2_header.Controls.Add(this.radLabel4);
            this.panel2_header.Controls.Add(this.radLabel5);
            this.panel2_header.Controls.Add(this.radButton_refresh);
            this.panel2_header.Controls.Add(this.radButton_pdt);
            this.panel2_header.Controls.Add(this.radLabel_Dept);
            this.panel2_header.Controls.Add(this.radLabel3);
            this.panel2_header.Controls.Add(this.radLabel_Possion);
            this.panel2_header.Controls.Add(this.radLabel2);
            this.panel2_header.Controls.Add(this.radLabel_Name);
            this.panel2_header.Controls.Add(this.radLabel1);
            this.panel2_header.Controls.Add(this.RadLabel);
            this.panel2_header.Controls.Add(this.radTextBox_Empl);
            this.panel2_header.Location = new System.Drawing.Point(12, 12);
            this.panel2_header.Name = "panel2_header";
            this.panel2_header.Size = new System.Drawing.Size(431, 522);
            this.panel2_header.TabIndex = 0;
            // 
            // radLabel12
            // 
            this.radLabel12.BackColor = System.Drawing.Color.Transparent;
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel12.Location = new System.Drawing.Point(11, 341);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(75, 19);
            this.radLabel12.TabIndex = 95;
            this.radLabel12.Text = "ค่าเบี้ยเลี้ยง";
            // 
            // radLabel11
            // 
            this.radLabel11.BackColor = System.Drawing.Color.Transparent;
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(11, 310);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(53, 19);
            this.radLabel11.TabIndex = 94;
            this.radLabel11.Text = "ค่าที่พัก";
            // 
            // radTextBox_Addon
            // 
            this.radTextBox_Addon.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Addon.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Addon.Location = new System.Drawing.Point(115, 336);
            this.radTextBox_Addon.MaxLength = 7;
            this.radTextBox_Addon.Name = "radTextBox_Addon";
            this.radTextBox_Addon.Size = new System.Drawing.Size(266, 25);
            this.radTextBox_Addon.TabIndex = 4;
            this.radTextBox_Addon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Addon_KeyDown);
            this.radTextBox_Addon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Addon_KeyPress);
            // 
            // radTextBox_Hotel
            // 
            this.radTextBox_Hotel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Hotel.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Hotel.Location = new System.Drawing.Point(115, 305);
            this.radTextBox_Hotel.MaxLength = 7;
            this.radTextBox_Hotel.Name = "radTextBox_Hotel";
            this.radTextBox_Hotel.Size = new System.Drawing.Size(266, 25);
            this.radTextBox_Hotel.TabIndex = 3;
            this.radTextBox_Hotel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Hotel_KeyDown);
            this.radTextBox_Hotel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Hotel_KeyPress);
            // 
            // radButton_printrice
            // 
            this.radButton_printrice.BackColor = System.Drawing.Color.Transparent;
            this.radButton_printrice.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_printrice.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_printrice.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_printrice.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_printrice.Location = new System.Drawing.Point(230, 439);
            this.radButton_printrice.Name = "radButton_printrice";
            this.radButton_printrice.Size = new System.Drawing.Size(26, 26);
            this.radButton_printrice.TabIndex = 91;
            this.radButton_printrice.Text = "radButton3";
            this.radButton_printrice.Click += new System.EventHandler(this.RadButton_printrice_Click);
            // 
            // radButton_printwha
            // 
            this.radButton_printwha.BackColor = System.Drawing.Color.Transparent;
            this.radButton_printwha.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_printwha.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_printwha.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_printwha.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_printwha.Location = new System.Drawing.Point(230, 405);
            this.radButton_printwha.Name = "radButton_printwha";
            this.radButton_printwha.Size = new System.Drawing.Size(26, 26);
            this.radButton_printwha.TabIndex = 90;
            this.radButton_printwha.Text = "radButton3";
            this.radButton_printwha.Click += new System.EventHandler(this.RadButton_printwha_Click);
            // 
            // radButton_printRetail
            // 
            this.radButton_printRetail.BackColor = System.Drawing.Color.Transparent;
            this.radButton_printRetail.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_printRetail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_printRetail.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButton_printRetail.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_printRetail.Location = new System.Drawing.Point(230, 373);
            this.radButton_printRetail.Name = "radButton_printRetail";
            this.radButton_printRetail.Size = new System.Drawing.Size(26, 26);
            this.radButton_printRetail.TabIndex = 89;
            this.radButton_printRetail.Text = "radButton3";
            this.radButton_printRetail.Click += new System.EventHandler(this.RadButton_printRetail_Click);
            // 
            // radLabel_whrice
            // 
            this.radLabel_whrice.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_whrice.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_whrice.Location = new System.Drawing.Point(263, 444);
            this.radLabel_whrice.Name = "radLabel_whrice";
            this.radLabel_whrice.Size = new System.Drawing.Size(144, 18);
            this.radLabel_whrice.TabIndex = 88;
            this.radLabel_whrice.Text = "<html>ใบ<span style=\"color: #ff0000\">***จำนวน copy/บิล</span></html>";
            // 
            // radLabel_wha
            // 
            this.radLabel_wha.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_wha.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_wha.Location = new System.Drawing.Point(263, 411);
            this.radLabel_wha.Name = "radLabel_wha";
            this.radLabel_wha.Size = new System.Drawing.Size(144, 18);
            this.radLabel_wha.TabIndex = 87;
            this.radLabel_wha.Text = "<html>ใบ<span style=\"color: #ff0000\">***จำนวน copy/บิล</span></html>";
            // 
            // radLabel_retail
            // 
            this.radLabel_retail.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_retail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_retail.Location = new System.Drawing.Point(262, 378);
            this.radLabel_retail.Name = "radLabel_retail";
            this.radLabel_retail.Size = new System.Drawing.Size(144, 18);
            this.radLabel_retail.TabIndex = 86;
            this.radLabel_retail.Text = "<html>ใบ<span style=\"color: #ff0000\">***จำนวน copy/บิล</span></html>";
            // 
            // radTextBox_whrice
            // 
            this.radTextBox_whrice.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_whrice.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_whrice.Location = new System.Drawing.Point(115, 439);
            this.radTextBox_whrice.MaxLength = 3;
            this.radTextBox_whrice.Name = "radTextBox_whrice";
            this.radTextBox_whrice.Size = new System.Drawing.Size(107, 25);
            this.radTextBox_whrice.TabIndex = 7;
            this.radTextBox_whrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_whrice_KeyDown);
            this.radTextBox_whrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_whrice_KeyPress);
            // 
            // radTextBox_wha
            // 
            this.radTextBox_wha.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_wha.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_wha.Location = new System.Drawing.Point(115, 406);
            this.radTextBox_wha.MaxLength = 3;
            this.radTextBox_wha.Name = "radTextBox_wha";
            this.radTextBox_wha.Size = new System.Drawing.Size(107, 25);
            this.radTextBox_wha.TabIndex = 6;
            this.radTextBox_wha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_wha_KeyDown);
            this.radTextBox_wha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_wha_KeyPress);
            // 
            // radTextBox_Retail
            // 
            this.radTextBox_Retail.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Retail.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Retail.Location = new System.Drawing.Point(115, 373);
            this.radTextBox_Retail.MaxLength = 3;
            this.radTextBox_Retail.Name = "radTextBox_Retail";
            this.radTextBox_Retail.Size = new System.Drawing.Size(107, 25);
            this.radTextBox_Retail.TabIndex = 5;
            this.radTextBox_Retail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Retail_KeyDown);
            this.radTextBox_Retail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Retail_KeyPress);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.ForeColor = System.Drawing.Color.Blue;
            this.radLabel10.Location = new System.Drawing.Point(11, 378);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(93, 19);
            this.radLabel10.TabIndex = 82;
            this.radLabel10.Text = "RETAILAREA";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.ForeColor = System.Drawing.Color.Blue;
            this.radLabel9.Location = new System.Drawing.Point(11, 444);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(70, 19);
            this.radLabel9.TabIndex = 81;
            this.radLabel9.Text = "WH-RICE";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.ForeColor = System.Drawing.Color.Blue;
            this.radLabel7.Location = new System.Drawing.Point(11, 411);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(46, 19);
            this.radLabel7.TabIndex = 80;
            this.radLabel7.Text = "WH-A";
            // 
            // RadButton_ScanOut
            // 
            this.RadButton_ScanOut.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_ScanOut.Location = new System.Drawing.Point(236, 476);
            this.RadButton_ScanOut.Name = "RadButton_ScanOut";
            this.RadButton_ScanOut.Size = new System.Drawing.Size(145, 32);
            this.RadButton_ScanOut.TabIndex = 9;
            this.RadButton_ScanOut.Text = "สแกนออก";
            this.RadButton_ScanOut.Click += new System.EventHandler(this.RadButton_ScanOut_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_ScanOut.GetChildAt(0))).Text = "สแกนออก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_ScanOut.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_ScanOut.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_ScanOut.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_ScanOut.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_ScanOut.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_ScanOut.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Scan
            // 
            this.RadButton_Scan.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_Scan.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_Scan.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_Scan.Image = global::PC_Shop24Hrs.Properties.Resources.contacts;
            this.RadButton_Scan.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_Scan.Location = new System.Drawing.Point(296, 70);
            this.RadButton_Scan.Name = "RadButton_Scan";
            this.RadButton_Scan.Size = new System.Drawing.Size(26, 26);
            this.RadButton_Scan.TabIndex = 74;
            this.RadButton_Scan.Text = "radButton3";
            this.RadButton_Scan.Click += new System.EventHandler(this.RadButton_Scan_Click);
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(77, 476);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(145, 32);
            this.RadButton_Save.TabIndex = 8;
            this.RadButton_Save.Text = "สแกนเข้า";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "สแกนเข้า";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_OutOffice
            // 
            this.radLabel_OutOffice.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_OutOffice.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_OutOffice.Location = new System.Drawing.Point(115, 178);
            this.radLabel_OutOffice.Name = "radLabel_OutOffice";
            this.radLabel_OutOffice.Size = new System.Drawing.Size(15, 19);
            this.radLabel_OutOffice.TabIndex = 27;
            this.radLabel_OutOffice.Text = "2";
            // 
            // radLabel6
            // 
            this.radLabel6.BackColor = System.Drawing.Color.Transparent;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(11, 279);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(57, 19);
            this.radLabel6.TabIndex = 26;
            this.radLabel6.Text = "ค่าน้ำมัน";
            // 
            // radDropDownList_Location
            // 
            this.radDropDownList_Location.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Location.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Location.Location = new System.Drawing.Point(115, 8);
            this.radDropDownList_Location.Name = "radDropDownList_Location";
            this.radDropDownList_Location.Size = new System.Drawing.Size(175, 25);
            this.radDropDownList_Location.TabIndex = 79;
            this.radDropDownList_Location.Text = "radDropDownList1";
            this.radDropDownList_Location.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDownList_Location_SelectedIndexChanged);
            // 
            // radTextBox_bill
            // 
            this.radTextBox_bill.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_bill.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_bill.Location = new System.Drawing.Point(115, 274);
            this.radTextBox_bill.MaxLength = 7;
            this.radTextBox_bill.Name = "radTextBox_bill";
            this.radTextBox_bill.Size = new System.Drawing.Size(266, 25);
            this.radTextBox_bill.TabIndex = 2;
            this.radTextBox_bill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_bill_KeyDown);
            this.radTextBox_bill.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_bill_KeyPress);
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(11, 178);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(66, 19);
            this.radLabel8.TabIndex = 26;
            this.radLabel8.Text = "จำนวนคน";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(115, 206);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(266, 62);
            this.radTextBox_Remark.TabIndex = 1;
            this.radTextBox_Remark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Remark_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.BackColor = System.Drawing.Color.Transparent;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(11, 12);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(87, 19);
            this.radLabel4.TabIndex = 77;
            this.radLabel4.Text = "ระบุปลายทาง";
            // 
            // radLabel5
            // 
            this.radLabel5.BackColor = System.Drawing.Color.Transparent;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(11, 203);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(87, 19);
            this.radLabel5.TabIndex = 23;
            this.radLabel5.Text = "ประเภทสินค้า";
            // 
            // radButton_refresh
            // 
            this.radButton_refresh.BackColor = System.Drawing.Color.Transparent;
            this.radButton_refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_refresh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_refresh.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_refresh.Location = new System.Drawing.Point(83, 69);
            this.radButton_refresh.Name = "radButton_refresh";
            this.radButton_refresh.Size = new System.Drawing.Size(26, 26);
            this.radButton_refresh.TabIndex = 73;
            this.radButton_refresh.Text = "radButton3";
            this.radButton_refresh.Click += new System.EventHandler(this.RadButton_refresh_Click);
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(400, 5);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 72;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_Dept
            // 
            this.radLabel_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dept.Location = new System.Drawing.Point(115, 153);
            this.radLabel_Dept.Name = "radLabel_Dept";
            this.radLabel_Dept.Size = new System.Drawing.Size(43, 19);
            this.radLabel_Dept.TabIndex = 25;
            this.radLabel_Dept.Text = "แผนก";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(11, 153);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(43, 19);
            this.radLabel3.TabIndex = 22;
            this.radLabel3.Text = "แผนก";
            // 
            // radLabel_Possion
            // 
            this.radLabel_Possion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Possion.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Possion.Location = new System.Drawing.Point(115, 128);
            this.radLabel_Possion.Name = "radLabel_Possion";
            this.radLabel_Possion.Size = new System.Drawing.Size(57, 19);
            this.radLabel_Possion.TabIndex = 24;
            this.radLabel_Possion.Text = "ตำแหน่ง";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(11, 128);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(57, 19);
            this.radLabel2.TabIndex = 22;
            this.radLabel2.Text = "ตำแหน่ง";
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(115, 103);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(25, 19);
            this.radLabel_Name.TabIndex = 23;
            this.radLabel_Name.Text = "ชื่อ";
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.Color.Transparent;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(115, 45);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(130, 19);
            this.radLabel1.TabIndex = 22;
            this.radLabel1.Text = "ระบุพนักงาน[Enter]";
            // 
            // RadLabel
            // 
            this.RadLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel.Location = new System.Drawing.Point(11, 103);
            this.RadLabel.Name = "RadLabel";
            this.RadLabel.Size = new System.Drawing.Size(25, 19);
            this.RadLabel.TabIndex = 21;
            this.RadLabel.Text = "ชื่อ";
            // 
            // radTextBox_Empl
            // 
            this.radTextBox_Empl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Empl.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Empl.Location = new System.Drawing.Point(115, 70);
            this.radTextBox_Empl.MaxLength = 7;
            this.radTextBox_Empl.Name = "radTextBox_Empl";
            this.radTextBox_Empl.Size = new System.Drawing.Size(175, 25);
            this.radTextBox_Empl.TabIndex = 0;
            this.radTextBox_Empl.Text = "1106163";
            this.radTextBox_Empl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Empl_KeyDown);
            this.radTextBox_Empl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Empl_KeyPress);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // ScanDptTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 543);
            this.Controls.Add(this.panel2_header);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScanDptTime";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimeKeeperForm";
            this.Load += new System.EventHandler(this.TimeKeeperForm_Load);
            this.panel2_header.ResumeLayout(false);
            this.panel2_header.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Addon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Hotel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printwha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_printRetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_whrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_wha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_retail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_whrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_wha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Retail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_ScanOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Scan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_OutOffice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_refresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Possion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2_header;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Empl;
        private Telerik.WinControls.UI.RadLabel RadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Possion;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
        private Telerik.WinControls.UI.RadButton radButton_refresh;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Location;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_OutOffice;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadButton RadButton_Scan;
        protected Telerik.WinControls.UI.RadButton RadButton_ScanOut;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox_bill;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadTextBox radTextBox_whrice;
        private Telerik.WinControls.UI.RadTextBox radTextBox_wha;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Retail;
        private Telerik.WinControls.UI.RadLabel radLabel_retail;
        private Telerik.WinControls.UI.RadLabel radLabel_whrice;
        private Telerik.WinControls.UI.RadLabel radLabel_wha;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.RadButton radButton_printRetail;
        private Telerik.WinControls.UI.RadButton radButton_printrice;
        private Telerik.WinControls.UI.RadButton radButton_printwha;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Addon;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Hotel;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel12;
    }
}
