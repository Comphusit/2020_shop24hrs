﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.TimeKeeper
{
    public partial class TimeKeeper_Report : Telerik.WinControls.UI.RadForm
    {
        readonly string _pReportScan;//130HR = รายงานสแกนนิ้ว 1.30 ,D041 = รายงานสแกนกิ้วช่างก่อสร้าง ,D110 = รายงานสแกนนิ้วเข้าแผนกผัก ,D054 = รายงานสแกนนิ้วเข้าสาขาใหญ่ , DO054_1 = รายงานค่าใช้จ่ายในการมาสาขาใหญ๋
                                     //string TableReportScan;
        readonly GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem();
        public TimeKeeper_Report(string pReportScan)
        {
            InitializeComponent();
            _pReportScan = pReportScan;
        }
        //Load Main
        private void TimeKeeper_Report_Load(object sender, EventArgs e)
        {

            RadCheckBox_Dept.Enabled = false; RadCheckBox_Dept.Checked = true;

            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Refrsh.ShowBorder = true; radButtonElement_Refrsh.ToolTipText = "ล้างข้อมูล";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "สแกนนิ้วเข้าสาขาใหญ่";


            RadButton_Search.ButtonElement.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;

            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_list); radDropDownList_list.Visible = false;
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            switch (_pReportScan)
            {
                case "130HR"://รายงานสแกนนิ้ว 1.30
                    radLabel_Detail.Visible = false;
                    radButtonElement_add.Enabled = false;
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date1, DateTime.Now.AddDays(0), DateTime.Now);
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date2, DateTime.Now.AddDays(0), DateTime.Now);

                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ", 180));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 60));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 300));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CONVERTDATETIME", "เวลาสแกน", 250));
                    break;
                case "D041"://,D041 = รายงานสแกนกิ้วช่างก่อสร้าง 
                    radLabel_Detail.Visible = false;
                    radButtonElement_add.Enabled = false;
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date1, DateTime.Now.AddDays(0), DateTime.Now);
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date2, DateTime.Now.AddDays(0), DateTime.Now);

                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ", 180));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 60));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 300));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CONVERTDATETIME", "เวลาสแกน", 250));
                    break;
                case "D110"://D110 = รายงานสแกนนิ้วเข้าแผนกผัก
                    ReportD110("0");
                    break;
                case "D054"://D054 = รายงานสแกนนิ้วเข้าสาขาใหญ่
                    radLabel_Detail.Text = "สีแดง << พนักงานยังอยู่ที่สาขาใหญ่ | DoubleClick ที่รายการ >> เพื่อสแกนนิ้วออก";
                    RadCheckBox_Dept.Visible = false; RadDropDownList_Dept.Visible = false;
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date1, DateTime.Now.AddDays(-1), DateTime.Now);
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date2, DateTime.Now, DateTime.Now);

                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TRANDATE", "วันที่", 120));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCID", "เอกสารออกนอก", 180));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("EMPLOUT", "จน.คนที่มา", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 220));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCHID", "รหัสสาขา", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 150));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 220));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SCANINTIME", "เวลาสแกนเข้า", 150));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SCANOUTTIME", "เวลาสแกนออก", 150));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LISTPRODUCT", "รายการที่จัด", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PRODUCTLIST", "รายการสินค้า", 300));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PETROBILL", "บิลน้ำมัน", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAYHOTEL", "ค่าที่พัก", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAYADDON", "ค่าเบี้ยเลี้ยง", 80));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("CALTIME", "วัน : ชั่วโมง : นาที", 150));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLIDREPLESE", "ผู้ช่วยสแกนแทน", 350));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STATUS", "สถานะ"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("RETAILAREA", "RETAILAREA"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("RETAILAREA_COPY", "RETAILAREA_COPY"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WHA", "WHA"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WHA_COPY", "WHA_COPY"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WHRICE", "WHRICE"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("WHRICE_COPY", "WHRICE_COPY"));

                    DatagridClass.SetCellBackClolorByExpression("DOCID", "STATUS = '0' ", ConfigClass.SetColor_Red(), RadGridView_Show);

                    FindData();
                    break;
                case "D054_1":
                    radLabel_Detail.Text = "แสดงข้อมูลรวม คือ ค่่าน้ำมัน+ค่าที่พัก+ค่าเบี้ยเลี้ยง | สามารถเลือกแสดงค้าใช้จ่ายได้ตามที่ต้องการ";
                    RadCheckBox_Dept.Enabled = true; RadCheckBox_Dept.Checked = true; RadCheckBox_Dept.ReadOnly = true; RadCheckBox_Dept.Text = "ระบุปี";
                    RadCheckBox_Dept.Visible = true; RadDropDownList_Dept.Visible = true;
                    radLabel1.Visible = false; radDateTimePicker_Date1.Visible = false; radDateTimePicker_Date2.Visible = false;
                    radDropDownList_list.Visible = true;
                    DataTable dtAddList = new DataTable();
                    dtAddList.Columns.Add("ID"); dtAddList.Columns.Add("DESC");
                    dtAddList.Rows.Add("PAYALL", "ทั้งหมด");
                    dtAddList.Rows.Add("PETROBILL", "ค่าน้ำมัน");
                    dtAddList.Rows.Add("PAYHOTEL", "ค่าที่พัก");
                    dtAddList.Rows.Add("PAYADDON", "ค่าเบี้ยเลี้ยง");
                    radDropDownList_list.DataSource = dtAddList;
                    radDropDownList_list.DisplayMember = "DESC";
                    radDropDownList_list.ValueMember = "ID";
                    radDropDownList_list.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
            SetDefault();
        }

        void SetDefault()
        {
            DataTable DtDropdown = new DataTable();
            if (_pReportScan == "D110")
            {
                DtDropdown.Columns.Add("PWSECTION", typeof(string));
                DtDropdown.Columns.Add("DESCRIPTION", typeof(string));

                if (SystemClass.SystemComMinimart == "1") DtDropdown.Rows.Add("32", "แผนกคอมพิวเตอร์");

                DtDropdown.Rows.Add("61", "แผนกจัดซื้อ-ผัก");
                DtDropdown.Rows.Add("124", "แผนกจัดซื้อ-ผลไม้");
                DtDropdown.Rows.Add("57", "แผนกจัดซื้อ-หมูไก่");

                RadDropDownList_Dept.DataSource = DtDropdown;
                RadDropDownList_Dept.ValueMember = "PWSECTION";
                RadDropDownList_Dept.DisplayMember = "DESCRIPTION";
                return;
            }

            string DeptValues = "", DeptDisplay = "", DeptDefault = "";

            if (SystemClass.SystemBranchID == "MN000")
            {
                if (_pReportScan == "D054_1")
                {
                    DtDropdown = DateTimeSettingClass.Config_YY();
                    DeptDisplay = "Y1";
                    DeptValues = "Y2";
                    DeptDefault = DateTime.Now.ToString("yyyy");

                    if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();
                    RadGridView_Show.SummaryRowsTop.Clear(); RadGridView_Show.DataSource = null;
                }
                else
                {
                    DtDropdown = Models.DptClass.GetDpt_AllD();
                    DeptValues = "NUM";
                    DeptDisplay = "DESCRIPTION_SHOW";
                    DeptDefault = SystemClass.SystemDptID;
                }

            }
            else
            {
                switch (_pReportScan)
                {
                    case "130HR":
                        DtDropdown = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                        DeptValues = "BRANCH_ID";
                        DeptDisplay = "NAME_BRANCH";
                        DeptDefault = SystemClass.SystemBranchID;
                        break;
                    case "D041":
                        DtDropdown = Models.DptClass.GetDptDetail_ByDptID("D041");
                        DeptValues = "NUM";
                        DeptDisplay = "DESCRIPTION";
                        DeptDefault = "D041";
                        break;

                    default:
                        break;
                }
            }

            RadDropDownList_Dept.DataSource = DtDropdown;
            RadDropDownList_Dept.ValueMember = DeptValues;
            RadDropDownList_Dept.DisplayMember = DeptDisplay;
            RadDropDownList_Dept.SelectedValue = DeptDefault;

        }

        //Export
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            MsgBoxClass.MsgBoxShow_SaveStatus(DatagridClass.ExportExcelGridView(this.Text + radDateTimePicker_Date1.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "1"));
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion


        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            //SetDataGridReport();
            FindData();
        }

        void FindData()
        {
            switch (_pReportScan)
            {
                case "130HR"://รายงานสแกนนิ้ว 1.30
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = Models.EmplClass.Scan_ReportGetScanHalfHour(radDateTimePicker_Date1.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_Date2.Value.ToString("yyyy-MM-dd"),
                                                        RadDropDownList_Dept.SelectedValue.ToString());
                    this.Cursor = Cursors.Default;
                    break;
                case "D041"://,D041 = รายงานสแกนกิ้วช่างก่อสร้าง 
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = Models.EmplClass.Scan_ReportGetScanD041(radDateTimePicker_Date1.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_Date2.Value.ToString("yyyy-MM-dd"),
                                                        RadDropDownList_Dept.SelectedValue.ToString());
                    this.Cursor = Cursors.Default;
                    break;
                case "D110"://D110 = รายงานสแกนนิ้วเข้าแผนกผัก
                    ReportD110("1");
                    break;
                case "D054"://D054 = รายงานสแกนนิ้วเข้าสาขาใหญ่
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = TimeKeeperClass.ScanInSPC_Detail("0", radDateTimePicker_Date1.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_Date2.Value.ToString("yyyy-MM-dd"));
                    this.Cursor = Cursors.Default;
                    break;
                case "D054_1":
                    this.Cursor = Cursors.WaitCursor;
                    string year = RadDropDownList_Dept.SelectedValue.ToString();

                    DataTable dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                    DataTable dtCountAll = ConnectionClass.SelectSQL_Main($@"
                            SELECT  BRANCHID,COUNT(BRANCHID) AS COUNTALL,MONTH(CONVERT(VARCHAR,TRANDATE,23)) AS MM,SUM(PETROBILL)+SUM(PAYHOTEL)+SUM(PAYADDON) AS PAYALL,
                                    SUM(PETROBILL) AS PETROBILL,SUM(PAYHOTEL) AS PAYHOTEL,SUM(PAYADDON) AS PAYADDON 
                            FROM	[SHOP_SCANINSPC] WITH (NOLOCK)
                            WHERE	YEAR(TRANDATE) = '{year}'
                            GROUP BY  BRANCHID,MONTH(CONVERT(VARCHAR,TRANDATE,23))
                        ");

                    if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();
                    RadGridView_Show.SummaryRowsTop.Clear(); RadGridView_Show.DataSource = null;
                    summaryRowItemA.Clear();
                    //create Columns
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัสสาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM", "รวมยอดเงิน", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COUNT", "จน.ครั้งที่มา", 100)));

                    RadGridView_Show.Columns["BRANCH_ID"].IsPinned = true;
                    RadGridView_Show.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_Show.Columns["SUM"].IsPinned = true;
                    RadGridView_Show.Columns["COUNT"].IsPinned = true;
                    
                    DateTime endDate = DateTime.Parse($@"{year}-12-31");
                    if (endDate > DateTime.Now) endDate = DateTime.Now;

                    DataTable MM = DateTimeSettingClass.GetMonthYearByDate($@"{year}-01-01", endDate.ToString("yyyy-MM-dd"));
                    for (int iC = 0; iC < MM.Rows.Count; iC++)
                    {
                        string headColumeName = MM.Rows[iC]["ThaiMonth"].ToString();
                        string headColumeValue = $@"{ MM.Rows[iC]["MonthNumber"]}";

                        RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM" + headColumeValue, $@"{headColumeName}", 100)));
                        DatagridClass.SetCellBackClolorByExpression($@"SUM{headColumeValue}", $@" SUM{headColumeValue} > 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_Show);

                        GridViewSummaryItem summaryItem = new GridViewSummaryItem("SUM" + headColumeValue, "{0:n2}", GridAggregateFunction.Sum);
                        summaryRowItemA.Add(summaryItem);
                    }

                    GridViewSummaryItem summaryItemC = new GridViewSummaryItem("COUNT", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemD = new GridViewSummaryItem("SUM", "{0:n2}", GridAggregateFunction.Sum);
                    summaryRowItemA.Add(summaryItemC);
                    summaryRowItemA.Add(summaryItemD);
                    RadGridView_Show.SummaryRowsTop.Add(summaryRowItemA);
                    RadGridView_Show.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    DatagridClass.SetCellBackClolorByExpression($@"SUM", $@" SUM > 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_Show);

                    RadGridView_Show.DataSource = dtBch;
                    string con = radDropDownList_list.SelectedValue.ToString();

                    for (int iR = 0; iR < RadGridView_Show.Rows.Count; iR++)
                    {
                        string emp = RadGridView_Show.Rows[iR].Cells["BRANCH_ID"].Value.ToString();
                        double allSum = 0;
                        double allCount = 0;

                        for (int iCC = 0; iCC < MM.Rows.Count; iCC++)
                        {
                            string headColumeValue = MM.Rows[iCC]["MonthNumber"].ToString();
                            DataRow[] drAll = dtCountAll.Select($@" BRANCHID = '{emp}'  AND  MM = '{headColumeValue}' ");

                            double iAll = 0, cAll = 0;
                            if (drAll.Length > 0)
                            {
                                iAll = Double.Parse(drAll[0][con].ToString());
                                cAll = Double.Parse(drAll[0]["COUNTALL"].ToString());
                            }
                            allSum += iAll;
                            allCount += cAll;
                            RadGridView_Show.Rows[iR].Cells["SUM" + headColumeValue].Value = iAll;
                        }
                        RadGridView_Show.Rows[iR].Cells["SUM"].Value = allSum;
                        RadGridView_Show.Rows[iR].Cells["COUNT"].Value = allCount;
                    }

                    this.Cursor = Cursors.Default;


                    break;
                default:
                    break;
            }

        }

        void ReportD110(string pType)
        {
            if (pType == "0")
            {
                //radLabel_Detail.Visible = false;
                radLabel_Detail.Text = "ข้อมูลการสแกนนิ้วเข้าแผนก";
                radButtonElement_add.Enabled = false;
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date1, DateTime.Now.AddDays(-30), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date2, DateTime.Now.AddDays(0), DateTime.Now);

                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ", 180));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONNAME", "ชื่อแผนก", 240));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SCANINTIME", "เวลาที่เข้างาน", 200));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMESCAN", "เวลาสแกนที่แผนก", 180));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHIFTID", "กะเข้างาน", 200));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CALDATETIME", "ความห่างของเวลากะ", 130));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CALSCANIN", "ห่างจากสแกนเข้างาน", 130));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESTINATION", "ปลายทาง", 180));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLIDREPLESE", "ผู้ช่วยสแกนแทน", 350));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DATETIMESCANDPT", "เวลาสแกนนิ้วเข้าแผนก"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TIMEIN", "เวลาสแกนนิ้วเข้างาน"));

            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();

                DataTable dtAll = TimeKeeperClass.DataFormSCANTAFF(radDateTimePicker_Date1.Value.ToString("yyyy-MM-dd"),
                                                    radDateTimePicker_Date2.Value.ToString("yyyy-MM-dd"), RadDropDownList_Dept.SelectedValue.ToString());

                for (int i = 0; i < dtAll.Rows.Count - 1; i++)
                {
                    string date = dtAll.Rows[i]["PWDATE"].ToString();
                    string empID = dtAll.Rows[i]["PWEMPLOYEE"].ToString();
                    string scanin = "-";

                    string scanElse = "", scanD = "", datetimescan = "";

                    if (dtAll.Rows[i]["SCANINTIME"].ToString() != "1900-01-01 00:00:00.000")
                    {
                        scanin = dtAll.Rows[i]["SCANINTIME"].ToString();
                        DataTable dtScanDpt = TimeKeeperClass.DataFormSCANDEPT(scanin, empID.Replace(" ", string.Empty));
                        if (dtScanDpt.Rows.Count > 0)
                        {
                            scanElse = dtScanDpt.Rows[0]["EMPLIDREPLESE"].ToString();
                            scanD = dtScanDpt.Rows[0]["DESTINATION"].ToString();
                            datetimescan = dtScanDpt.Rows[0]["DATETIMESCAN"].ToString();
                        }
                    }

                    string datediff1 = "หาค่าไม่ได้", datediff2 = "หาค่าไม่ได้";
                    if (datetimescan != "")
                    {
                        TimeSpan Span = DateTime.Parse(datetimescan) - DateTime.Parse(dtAll.Rows[i]["TIMEIN"].ToString());
                        TimeSpan Span2 = DateTime.Parse(datetimescan) - DateTime.Parse(dtAll.Rows[i]["SCANINTIME"].ToString());
                        datediff1 = Span.TotalMinutes.ToString("N2");
                        datediff2 = Span2.TotalMinutes.ToString("N2");
                    }

                    if (datetimescan == "") datetimescan = "ไม่พบการสแกนนิ้ว";

                    RadGridView_Show.Rows.Add(empID, dtAll.Rows[i]["SPC_NAME"].ToString(), dtAll.Rows[i]["PWDPT"].ToString(),
                         date,
                         scanin,
                         datetimescan,
                         dtAll.Rows[i]["PWDESC"].ToString(),
                         datediff1,
                         datediff2,
                         scanD,
                         scanElse,
                         datetimescan,
                         dtAll.Rows[i]["TIMEIN"].ToString());
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (_pReportScan)
            {
                case "D054":
                    //if (RadGridView_Show.CurrentRow.Cells["SCANOUTTIME"].Value.ToString() == "01/01/1900 00:00:00")
                    //{
                    EmplScan emplScan = new EmplScan
                    {
                        DOCID = RadGridView_Show.CurrentRow.Cells["DOCID"].Value.ToString(),
                        EMPLID = RadGridView_Show.CurrentRow.Cells["EMPLID"].Value.ToString(),
                        EMPLNAME = RadGridView_Show.CurrentRow.Cells["EMPLNAME"].Value.ToString(),
                        POSITION = RadGridView_Show.CurrentRow.Cells["DESCRIPTION"].Value.ToString(),
                        BRANCHID = RadGridView_Show.CurrentRow.Cells["BRANCHID"].Value.ToString(),
                        DPT = RadGridView_Show.CurrentRow.Cells["BRANCHNAME"].Value.ToString(),
                        EMPLOUT = RadGridView_Show.CurrentRow.Cells["EMPLOUT"].Value.ToString(),
                        PRODUCT = RadGridView_Show.CurrentRow.Cells["PRODUCTLIST"].Value.ToString(),
                        OIL = (double)RadGridView_Show.CurrentRow.Cells["PETROBILL"].Value,
                        Pay_AddOn = (double)RadGridView_Show.CurrentRow.Cells["PAYADDON"].Value,
                        Pay_Hotel = (double)RadGridView_Show.CurrentRow.Cells["PAYHOTEL"].Value,
                        SCANINTIME = RadGridView_Show.CurrentRow.Cells["SCANINTIME"].Value.ToString()
                    };
                    string staOutAll = "1";
                    if (RadGridView_Show.CurrentRow.Cells["SCANOUTTIME"].Value.ToString() == "01/01/1900 00:00:00") staOutAll = "0";
                    ScanDptTime scanDptTime = new ScanDptTime("1", "OUT", emplScan, staOutAll)
                    {
                        retail = RadGridView_Show.CurrentRow.Cells["RETAILAREA"].Value.ToString(),
                        c_retail = RadGridView_Show.CurrentRow.Cells["RETAILAREA_COPY"].Value.ToString(),
                        wha = RadGridView_Show.CurrentRow.Cells["WHA"].Value.ToString(),
                        c_wha = RadGridView_Show.CurrentRow.Cells["WHA_COPY"].Value.ToString(),
                        whrice = RadGridView_Show.CurrentRow.Cells["WHRICE"].Value.ToString(),
                        c_whrice = RadGridView_Show.CurrentRow.Cells["WHRICE_COPY"].Value.ToString()
                    };

                    if (scanDptTime.ShowDialog() == DialogResult.Yes)
                    {
                        FindData();
                    }
                    //}
                    break;
                default:
                    break;
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pReportScan);
        }

        private void RadButtonElement_Refrsh_Click(object sender, EventArgs e)
        {
            RadGridView_Show.DataSource = null;
        }

        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            if (_pReportScan == "D054")
            {
                EmplScan emplScan = new EmplScan { };
                ScanDptTime scanDptTime = new ScanDptTime("1", "IN", emplScan);
                if (scanDptTime.ShowDialog() == DialogResult.Yes) FindData();
            }
            else SetDefault();
        }


    }
}
