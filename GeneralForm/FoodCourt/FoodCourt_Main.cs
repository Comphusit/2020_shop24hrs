﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.ComponentModel;
using System.Drawing.Printing;
using System.IO;
using PC_Shop24Hrs.FormShare.ShowData;
using PC_Shop24Hrs.FormShare;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.FoodCourt
{
    public partial class FoodCourt_Main : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        //0 รายงานการใช้จ่ายในระบบ Foodcourt
        //1 รายงานยอดขายแต่ละวัน

        readonly string _pPermission;

        Data_EMPLTABLE Empl;
        //Load
        public FoodCourt_Main(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void FoodCourt_Main_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radStatusStrip1.SizingGrip = false;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false; RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;
            radLabel_Detail.Text = "";
            radTextBox_Id.Visible = false; radLabel3.Visible = false;

            switch (_pTypeReport)
            {
                case "0":  //0 รายงานการใช้จ่ายในระบบ Foodcourt

                    radLabel_Date.Text = "ระบุวันที่การใช้จ่าย";

                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-45);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    radTextBox_Id.Visible = true; radLabel3.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SELLERID", "รหัสผู้ขาย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SELLERNAME", "ชื่อผู้ขาย", 210)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "รหัสผู้ซื้อ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อผู้ซื้อ", 210)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PAIDDATE", "วันที่ซื้อ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STATUS_USE", "ประเภท", 80)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "เติมเงินสด", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("OLD_CASH", "เงินสดก่อนใช้", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PRICE_CASH", "ยอดใช้เงินสด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LAST_CASH", "เงินสดหลังใช้", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("OLD_CRADIT", "เครดิตก่อนใช้", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CREDIT_CASH", "ยอดใช้เครดิต", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LAST_CRADIT", "เครดิตหลังใช้", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STATUS_SALARY", "สถานะหักเงิน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_SALARY", "สถานะหักเงิน")));

                    this.RadGridView_ShowHD.SummaryRowsTop.Clear();

                    GridViewSummaryItem summaryItem0 = new GridViewSummaryItem("PRICE_CASH", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItem1 = new GridViewSummaryItem("CREDIT_CASH", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem0, summaryItem1 };

                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

                    DatagridClass.SetCellBackClolorByExpression("STATUS_SALARY", " STA_SALARY = '1' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("LINEAMOUNT", "LINEAMOUNT > 0 ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

                    RadButton_Search.Visible = false;
                    radTextBox_Id.Focus();
                    break;
                case "1"://1 รายงานยอดขายแต่ละวัน

                    RadCheckBox_1.Text = "รวมรายเดือน"; RadCheckBox_1.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่การใช้จ่าย";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-15);
                    radDateTimePicker_D2.Value = DateTime.Now;

                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MACHINE", "เครื่องขาย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PAIDDATE", "วันที่ขาย", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PRICE", "ยอดขายรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CASHPRICE", "ยอดขายเงินสด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CRADITPRICE", "ยอดขายเครดิต", 100)));

                    radLabel_Detail.Text = "Double Click รายการ >> พิมพ์สลิปการขาย";
                    RadButton_Search.Visible = true;
                    RadButton_Search.Focus();
                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0"://0 รายงานการใช้จ่ายในระบบ Foodcourt
                    string sql = $@"
                        SELECT	MACHINE,SELLERID,SELLERNAME,BUYERID,BUYERNAME,CONVERT(VARCHAR,PAIDDATE,25) AS PAIDDATE,
		                        IIF(PAYMENT=0,'CASH','CRADIT') AS STATUS_USE,0 AS LINEAMOUNT,
		                        IIF(PAYMENT=0,CASHOLD,0) AS OLD_CASH,
                                IIF(PAYMENT=0,CASHPRICE,0) AS PRICE_CASH,
		                        IIF(PAYMENT=0,CASHLAST,0) AS LAST_CASH,
		                        IIF(PAYMENT=1,CRADITOLD,0) AS OLD_CRADIT,
                                IIF(PAYMENT=1,CRADITPRICE,0) AS CREDIT_CASH,
		                        IIF(PAYMENT=1,CRADITLAST,0) AS LAST_CRADIT,
		                        IIF(PAYMENT=1,IIF(STA_APVSALARY=0,'ยังไม่หัก','หักแล้ว'),'') AS STATUS_SALARY,
		                        IIF(PAYMENT=1,IIF(STA_APVSALARY=0,'1','0'),'0') AS STA_SALARY
                        FROM	FoodCourt_LINE WITH (NOLOCK)
                        WHERE	STATUS = '1'
		                        AND BUYERID LIKE '%{radTextBox_Id.Text.Trim()}'
		                        AND CONVERT(VARCHAR,PAIDDATE,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                        UNION 

                        SELECT	POSNUMBER AS MACHINE,CASHIERID AS SELLERID,CASHIERNAME AS SELLERNAME,EMPLID AS BUYERID,SPC_NAME AS BUYERNAME,CONVERT(VARCHAR,DATETIMEUPD,25) AS PAIDDATE,
		                        'TOPUP' AS STATUS_USE,LINEAMOUNT,
		                        0 AS OLD_CASH,0 AS PRICE_CASH,0 AS LAST_CASH,0 AS OLD_CRADIT,0 AS CREDIT_CASH,0 AS LAST_CRADIT,'' AS STATUS_SALARY,0 AS STA_SALARY
                        FROM	FoodCourt_TopUp WITH (NOLOCK)
                        WHERE	STA = '1'
		                        AND EMPLID LIKE '%{radTextBox_Id.Text.Trim()}'
		                        AND CONVERT(VARCHAR,DATETIMEUPD,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                        ORDER BY PAIDDATE DESC ";
                    dt_Data = ConnectionClass.SelectSQL_Main(sql);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    string sqlA = $@" SELECT	SPC_CASH,SPC_CRADIT	FROM	FoodCourt_Main WITH (NOLOCK) WHERE	ALTNUM = '{radTextBox_Id.Text.Trim()}' ";
                    DataTable dtDesc = ConnectionClass.SelectSQL_Main(sqlA);
                    if (dtDesc.Rows.Count == 0) radLabel_Detail.Text = "ไม่มียอดเงินคงเหลือ ทั้งระบบเงินสดและเครดิต";
                    else radLabel_Detail.Text = $@"ยอดเครดิต คงเหลือ {Double.Parse(dtDesc.Rows[0]["SPC_CRADIT"].ToString()):N2}  ยอดเงินสด คงเหลือ {Double.Parse(dtDesc.Rows[0]["SPC_CASH"].ToString()):N2}";

                    this.Cursor = Cursors.Default;
                    break;
                case "1":
                    string sql1;
                    if (RadCheckBox_1.Checked == true)
                    {
                        sql1 = $@"
                            SELECT	SUBSTRING(CONVERT(VARCHAR,PAIDDATE,23),0,8) AS PAIDDATE,SUM(PRICE) AS PRICE,SUM(CASHPRICE) AS CASHPRICE,SUM(CRADITPRICE) AS CRADITPRICE
                            FROM	FoodCourt_LINE WITH (NOLOCK)
                            WHERE	STATUS = '1'
                            GROUP BY SUBSTRING(CONVERT(VARCHAR,PAIDDATE,23),0,8)
                            ORDER BY SUBSTRING(CONVERT(VARCHAR,PAIDDATE,23),0,8)  DESC  ";
                    }
                    else
                    {
                        sql1 = $@"
                            SELECT	CONVERT(VARCHAR,PAIDDATE,23) AS PAIDDATE,SUM(PRICE) AS PRICE,SUM(CASHPRICE) AS CASHPRICE,SUM(CRADITPRICE) AS CRADITPRICE
                            FROM	FoodCourt_LINE WITH (NOLOCK)
                            WHERE	STATUS = '1'
		                            AND CONVERT(VARCHAR,PAIDDATE,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                            GROUP BY CONVERT(VARCHAR,PAIDDATE,23) 
                            ORDER BY CONVERT(VARCHAR,PAIDDATE,23)  DESC  ";
                    }

                    dt_Data = ConnectionClass.SelectSQL_Main(sql1);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            radDateTimePicker_D1.MaxDate = radDateTimePicker_D2.Value;
            radDateTimePicker_D2.MinDate = radDateTimePicker_D1.Value;
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }

            if (_pTypeReport == "0")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-45);
                radDateTimePicker_D2.Value = DateTime.Now;
                radLabel_Detail.Text = "";
                radTextBox_Id.Text = ""; radTextBox_Id.Focus();
            }
            else if (_pTypeReport == "1")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-15);
                radDateTimePicker_D2.Value = DateTime.Now;
                RadButton_Search.Focus();
            }
            else
            {
                radDateTimePicker_D1.Value = DateTime.Now;
                radDateTimePicker_D2.Value = DateTime.Now;
                RadButton_Search.Focus();
            }
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (_pTypeReport == "1")
            {

                if (RadCheckBox_1.CheckState == CheckState.Checked)
                {
                    radLabel_Date.Visible = false;
                    radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                }
                else
                {
                    radLabel_Date.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                }
            }
            else
            {
                if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
            }
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pTypeReport == "1")
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["PAIDDATE"].Value.ToString() == "") return;
                //if (RadGridView_ShowHD.CurrentRow.Cells["PAIDDATE"].Value.ToString() == DateTime.Now.ToString("yyyy-MM-dd"))
                //{
                //    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถพิมพ์ข้อมูลในวันที่ปัจจุบันได้");
                //    return;
                //}

                PrintDialog _dai = new PrintDialog();
                if (_dai.ShowDialog(this) == DialogResult.OK)
                {
                    PrintDocument_send.PrintController = printController;
                    _dai.PrinterSettings.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    PrintDocument_send.PrinterSettings = _dai.PrinterSettings;
                    PrintDocument_send.Print();
                }
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        private void RadTextBox_Id_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadTextBox_Id_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_Id.Text.Length == 7))
            {
                this.Empl = new Data_EMPLTABLE(radTextBox_Id.Text);

                if (this.Empl.EMPLTABLE_EMPLID != "" && this.Empl.EMPLTABLE_EMPLID != null)
                {
                    FingerScan FingerScan = new FingerScan(this.Empl.EMPLTABLE_EMPLID, "ระบบ FoodCourt", this.Empl.EMPLTABLE_SPC_NAME, this.Empl.EMPLTABLE_POSSITION, this.Empl.EMPLTABLE_DESCRIPTION);

                    if (FingerScan.ShowDialog() != DialogResult.Yes)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว{Environment.NewLine}สามารถเข้าดูได้ผ่านสแกนนิ้วเท่านั้น");
                        Cursor.Current = Cursors.Default;
                        radTextBox_Id.SelectAll();
                        return;
                    }
                    else
                    {
                        SetDGV_HD();
                    }
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน.");
                    return;
                }
            }
        }
        //Printslip FoodCourt
        private void PrintDocument_send_PrintPage(object sender, PrintPageEventArgs e)
        {
            int Y = 0;
            e.Graphics.DrawString("ยอดขายระบบ Food Court", SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("ครัวมาลินี", SystemClass.printFont15, Brushes.Black, 80, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["PAIDDATE"].Value.ToString(), new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 45;
            e.Graphics.DrawString("ยอดขายรวม ", SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("ทั้งหมด [บาท]", SystemClass.printFont15, Brushes.Black, 0, Y);
            e.Graphics.DrawString(Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["PRICE"].Value.ToString()).ToString("N2"), SystemClass.printFont15, Brushes.Black, 150, Y);
            Y += 30;
            e.Graphics.DrawString("เงินสด [บาท]", SystemClass.printFont15, Brushes.Black, 0, Y);
            e.Graphics.DrawString(Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["CASHPRICE"].Value.ToString()).ToString("N2"), SystemClass.printFont15, Brushes.Black, 150, Y);
            Y += 30;
            e.Graphics.DrawString("เครดิต [บาท]", SystemClass.printFont15, Brushes.Black, 0, Y);
            e.Graphics.DrawString(Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["CRADITPRICE"].Value.ToString()).ToString("N2"), SystemClass.printFont15, Brushes.Black, 150, Y);
            Y += 30;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้พิมพ์ " + SystemClass.SystemUserID + " " + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
    }
}
