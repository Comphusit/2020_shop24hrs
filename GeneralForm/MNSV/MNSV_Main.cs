﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;
using System.Collections;


namespace PC_Shop24Hrs.GeneralForm.MNSV
{
    public partial class MNSV_Main : Telerik.WinControls.UI.RadForm
    {
        DataTable dtItems = new DataTable();
        readonly DataTable dtEmp = new DataTable();
        string pApv; //0 ยังไม่บันทึก 1 บันทึก 2 ยังไม่ลงเวลาเข้า 2 ยังไม่ลงเวลาออก
        //string pConfigDB;
        Data_EMPLTABLE Empl;
        int rowEmplScanIN = 0, rowEmplScanOUT = 0;

        public MNSV_Main()
        {
            InitializeComponent();
        }

        private void MNSV_Main_Load(object sender, EventArgs e)
        {
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Bch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_team);
            DatagridClass.SetDefaultRadGridView(RadGridView_Emp);
            DatagridClass.SetDefaultRadGridView(radGridView_Item);

            radLabel_Docno.Text = "";
            RadGridView_Emp.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("C", "เลือก", 60));
            if (SystemClass.SystemBranchID == "MN000")
            {
                RadGridView_Emp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STATIME", "ลงเวลา")));
            }
            else
            {
                RadGridView_Emp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STATIME", "ลงเวลา", 100)));

                DatagridClass.SetCellBackClolorByExpression("STATIME", "STATIME = 'IN' ", ConfigClass.SetColor_GreenPastel(), RadGridView_Emp);
                DatagridClass.SetCellBackClolorByExpression("STATIME", "STATIME = 'OUT' ", ConfigClass.SetColor_Red(), RadGridView_Emp);
            }
            RadGridView_Emp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 80)));
            RadGridView_Emp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 150)));
            RadGridView_Emp.MasterTemplate.Columns["C"].IsPinned = true;

            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSVDetail", "รายละเอียดงาน", 500)));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("MNSVOK", "เรียบร้อย", 80)));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("MNSVNO", "ไม่เรียบร้อย", 80)));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSVRemark", "หมายเหตุ", 250)));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNSVSeq", "SeqNo")));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameUp", "ผู้ประเมินงาน", 250)));

            dtEmp.Columns.Add("C");
            dtEmp.Columns.Add("EMPLID");
            dtEmp.Columns.Add("SPC_NAME");
            dtEmp.Columns.Add("POSSITION");
            dtEmp.Columns.Add("DESCRIPTION");
            dtEmp.Columns.Add("STATIME");

            dtItems.Columns.Add("MNSVDetail");
            dtItems.Columns.Add("MNSVOK");
            dtItems.Columns.Add("MNSVNO");
            dtItems.Columns.Add("MNSVRemark");
            dtItems.Columns.Add("MNSVSeq");
            dtItems.Columns.Add("WhoNameUp");

            if (SystemClass.SystemBranchID == "MN000")
            {
                radLabel_Detail.Text = "ระบุสาขา | เลือก ทีม | ระบุพนักงาน | ระบุ รายการงาน Enter | DoubleClick ช่องรายละเอียดงาน >> ลบรายการที่เลือก";
                radLabel_DescEmp.Text = "Click ช่องเลือก >> ระบุพนักงานไปทำงาน";
                RadButton_Save.Enabled = true;
                RadButton_Cancel.Enabled = true;
                button_add.Enabled = true;
            }
            else
            {
                radLabel_Detail.Text = "Click ช่องเรียบร้อย >> สำหรับงานเรียบร้อย | Click ช่องไม่เรียบร้อย >> สำหรับงานเรียบร้อยและระบุเหตุผล ";
                radLabel_DescEmp.Text = "DoubleClick ช่องลงเวลา >> ลงเวลาเข้า-ออกงาน";
                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = false;
                button_add.Enabled = false;
            }

            if (SystemClass.SystemBranchID != "MN000")
            {
                MNSV_FindData _mnsvFindData = new MNSV_FindData();
                if (_mnsvFindData.ShowDialog(this) == DialogResult.Yes)
                {
                    LoadData(_mnsvFindData.pDocno, _mnsvFindData.pBranchID, _mnsvFindData.pTeam, _mnsvFindData.pDetail,
                        _mnsvFindData.pDateIn, _mnsvFindData.pDateOut);
                }
                else
                {
                    ClearTxt();
                    SetBranch(SystemClass.SystemBranchID);
                    radTextBox_Desc.Enabled = false;
                }
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                ClearTxt();
                SetBranch(SystemClass.SystemBranchID);
                SetTeam("");
                SetEmpl();
                this.Cursor = Cursors.Default;
            }

        }
        //Clear
        void ClearTxt()
        {
            if (dtItems.Rows.Count > 0)
            {
                dtItems.Rows.Clear();
            }
            dtItems.AcceptChanges();
            radLabel_Out.Text = ""; radLabel_Out.BackColor = Color.Transparent;
            radLabel_In.Text = ""; radLabel_In.BackColor = Color.Transparent;
            pApv = "0";
            radDropDownList_team.Enabled = true;
            radDropDownList_Bch.Enabled = true;
            radLabel_Docno.Text = "";
            radTextBox_Desc.Text = "";
            radTextBox_Desc.Enabled = true;
            ///RadButton_Save.Enabled = false;
            radTextBox_Desc.SelectAll();
            radTextBox_Desc.Focus();
        }
        //Set Branch
        void SetBranch(string _pBch)
        {
            if (_pBch == "MN000")
            {
                radDropDownList_Bch.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
            }
            else
            {
                radDropDownList_Bch.DataSource = BranchClass.GetDetailBranchByID(_pBch);
            }
            radDropDownList_Bch.DisplayMember = "NAME_BRANCH";
            radDropDownList_Bch.ValueMember = "BRANCH_ID";
            radDropDownList_Bch.SelectedIndex = 0;
        }
        //DataChange
        void SetEmpl()
        {
            this.Cursor = Cursors.WaitCursor;
            string team = "T01";
            try
            {
                team = radDropDownList_team.SelectedValue.ToString();
            }
            catch (Exception) { }

            DataTable dt = MNSV_Class.MNSV_GetEmpByTeam(team);
            RadGridView_Emp.DataSource = dt;
            dt.AcceptChanges();
            this.Cursor = Cursors.Default;

        }
        //set Team
        void SetTeam(string _pCon)
        {
            radDropDownList_team.DataSource = MNSV_Class.MNSV_GetTeam(_pCon);
            radDropDownList_team.DisplayMember = "Team_NAME";
            radDropDownList_team.ValueMember = "Team_ID";
            if (_pCon == "") radDropDownList_team.SelectedValue = "T01";
        }

        #region "Formatting"
        private void RadGridView_Item_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Item_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Item_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Item_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        #endregion

        //เปลี่ยนทีม
        private void RadDropDownList_team_SelectedValueChanged(object sender, EventArgs e)
        {
            if (pApv == "0") SetEmpl();
        }
        //PDF
        private void Button_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        #region "ROWS"
        private void RadGridView_Emp_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Emp_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);

        }

        private void RadGridView_Emp_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Emp_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        #endregion
        //Claer
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (pApv == "0")
            {

            }
            switch (pApv)
            {
                case "0":
                    ClearTxt();
                    SetBranch(SystemClass.SystemBranchID);
                    SetTeam("");
                    SetEmpl();
                    break;
                case "1":
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกรายการของทีมจัดร้าน เลขที่ " + radLabel_Docno.Text + @" ?") == DialogResult.No) return;
                    string T = MNSV_Class.MNSV_DocCancle(radLabel_Docno.Text);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        ClearTxt();
                        SetBranch(SystemClass.SystemBranchID);
                        SetTeam("");
                        SetEmpl();
                    }
                    break;
                case "2":
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานมีการลงเวลาเข้าสาขาเรียบร้อยแล้ว ไม่สามารถแก้ไขได้อีก");
                    break;
                case "3":
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานมีการลงเวลาออกจากสาขาเรียบร้อยแล้ว ไม่สามารถแก้ไขได้อีก");
                    break;
                default:
                    break;
            }
        }
        //Edit
        private void RadGridView_Item_CellClick(object sender, GridViewCellEventArgs e)
        {

            switch (e.Column.Name)
            {
                case "MNSVOK": //กรณีงานเรียบร้อย
                    UpdateJobSta("1", "0");
                    break;
                case "MNSVNO": //กรณีงานไม่เรียบร้อย
                    UpdateJobSta("0", "1");
                    break;
                default:
                    break;
            }
        }
        //Update Rmk
        void UpdateJobSta(string staOK, string staNo)
        {
            if (pApv == "0") { return; }
            if (SystemClass.SystemBranchID == "MN000") return;

            if (pApv == "1")
            { MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานยังไม่มีการลงเวลาเข้าสาขา ยังไม่สามารถประเมินงานได้"); return; }

            if (pApv == "3")
            { MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานมีการลงเวลาออกจากสาขาเรียบร้อยแล้ว ไม่สามารถแก้ไขได้อีก"); return; }

            string rmk = "";
            if (staNo == "1")
            {
                FormShare.ShowRemark _ShowRemark = new FormShare.ShowRemark("1");
                if (_ShowRemark.ShowDialog(this) == DialogResult.No)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("การระบุงานไม่เรียบร้อย ต้องใส่เหตุผลทุกครั้ง");
                    return;
                }
                else
                {
                    rmk = _ShowRemark.pRmk;
                }
            }

            string T_OK = MNSV_Class.MNSV_DocUpdateJob(radLabel_Docno.Text, staOK, staNo, rmk, radGridView_Item.CurrentRow.Cells["MNSVSeq"].Value.ToString());
            MsgBoxClass.MsgBoxShow_SaveStatus(T_OK);
            if (T_OK == "")
            {
                radGridView_Item.CurrentRow.Cells["MNSVOK"].Value = staOK;
                radGridView_Item.CurrentRow.Cells["MNSVNO"].Value = staNo;
                radGridView_Item.CurrentRow.Cells["MNSVRemark"].Value = rmk;
                radGridView_Item.CurrentRow.Cells["WhoNameUp"].Value = SystemClass.SystemUserID + "-" + SystemClass.SystemUserName;
            }
        }
        //Add Bill
        private void Button_add_Click(object sender, EventArgs e)
        {
            if ((pApv == "0") && (radGridView_Item.Rows.Count > 0))
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยังมีรายการค้างบันทึก ยืนยันการไม่บันทึกข้อมูล ?") == DialogResult.No) return;
            }

            SetBranch(SystemClass.SystemBranchID);
            SetTeam("");
            SetEmpl();
            ClearTxt();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radGridView_Item.Rows.Count == 0) { return; }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกข้อมูล ?") == DialogResult.No) return;

            int countEmp = 0;
            for (int ii = 0; ii < RadGridView_Emp.Rows.Count; ii++)
            {
                if (RadGridView_Emp.Rows[ii].Cells["C"].Value.ToString() == "1") countEmp++;
            }

            if (countEmp == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีการระบุรหัสพนักงานเข้าสาขา" + Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง");
                return;
            }

            ArrayList str = new ArrayList();

            string maxdocno = Class.ConfigClass.GetMaxINVOICEID("MNSV", "-", "MNSV", "1");
            int iRows = 0;
            for (int i = 0; i < RadGridView_Emp.Rows.Count; i++)
            {
                if (RadGridView_Emp.Rows[i].Cells["C"].Value.ToString() == "1")
                {
                    iRows += 1;
                    str.Add(@"
                        INSERT INTO SHOP_MNSV_DT
                        ([MNSV_Docno],[MNSV_SeqNo],[MNSV_EmpID],[MNSV_EmpName])
                        VALUES('" + maxdocno + @"','" + iRows + @"','" + RadGridView_Emp.Rows[i].Cells["EmplID"].Value.ToString() + @"',
                        '" + RadGridView_Emp.Rows[i].Cells["SPC_NAME"].Value.ToString() + @"')
                    ");
                }
            }

            // int iRc = 0;
            for (int iRmk = 0; iRmk < radGridView_Item.Rows.Count; iRmk++)
            {
                //    iRc++;
                str.Add(@"
                        INSERT INTO SHOP_MNSV_RC
                            ([MNSV_Docno],[MNSVSeq],[MNSVDetail],[WhoIns])
                        VALUES('" + maxdocno + @"','" + radGridView_Item.Rows[iRmk].Cells["MNSVSeq"].Value.ToString() + @"',
                            '" + radGridView_Item.Rows[iRmk].Cells["MNSVDetail"].Value.ToString() + @"',
                            '" + SystemClass.SystemUserID + @"')
                    ");
            }

            string bchId = radDropDownList_Bch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(bchId);

            str.Add(@"
                INSERT INTO SHOP_MNSV_HD
                ([MNSV_Docno],[MNSV_Branch],[MNSV_BranchName],[MNSV_WhoID],[MNSV_WhoName],[MNSV_WhoPosition],[MNSV_WhoDpt],[MNSV_ComName],[MNSV_TEAM])
                VALUES('" + maxdocno + @"','" + bchId + @"','" + bchName + @"',
                '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + SystemClass.SystemUserPositionName + @"','" + SystemClass.SystemDptName + @"',
                '" + SystemClass.SystemPcName + @"','" + radDropDownList_team.SelectedValue.ToString() + @"')
            ");

            string T = ConnectionClass.ExecuteSQL_ArrayMain(str);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = maxdocno;
                pApv = "1";
                //radGridView_Item.Columns[1].IsVisible = false;
                radDropDownList_team.Enabled = false;
                radDropDownList_Bch.Enabled = false;
                RadButton_Save.Enabled = false;
            }

        }
        //Select Emp
        private void RadGridView_Emp_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (e.Column.Name)
            {
                case "C":
                    if (pApv != "0") return;

                    try
                    {
                        if (RadGridView_Emp.CurrentRow.Cells["C"].Value.ToString() == "0") RadGridView_Emp.CurrentRow.Cells["C"].Value = "1"; else RadGridView_Emp.CurrentRow.Cells["C"].Value = "0";
                    }
                    catch (Exception)
                    {
                        RadGridView_Emp.CurrentRow.Cells["C"].Value = "0";
                    }
                    break;

                default:
                    break;
            }
        }
        //Find
        private void Button_Find_Click(object sender, EventArgs e)
        {
            MNSV_FindData _mnsvFindData = new MNSV_FindData();
            if (_mnsvFindData.ShowDialog(this) == DialogResult.Yes)
            {
                LoadData(_mnsvFindData.pDocno, _mnsvFindData.pBranchID, _mnsvFindData.pTeam, _mnsvFindData.pDetail,
                    _mnsvFindData.pDateIn, _mnsvFindData.pDateOut);
            }
        }
        //LoadData
        void LoadData(string _pDocno, string _pBch, string _pTeam, DataTable _pDetail, string _pDateIn, string _pDateOut)
        {
            this.Cursor = Cursors.WaitCursor;

            if (dtEmp.Rows.Count > 0) dtEmp.Rows.Clear();

            pApv = "1";
            radLabel_Docno.Text = _pDocno;
            RadButton_Save.Enabled = false;

            if (_pDateOut != " ")
            {
                pApv = "3";
                radLabel_Out.Text = "ออก " + _pDateOut;
                radLabel_Out.BackColor = ConfigClass.SetColor_GreenPastel();
                radTextBox_Desc.Enabled = false;
            }
            else
            {
                if (_pDateIn != " ")
                {
                    pApv = "2";
                    radLabel_Out.Text = "ยังไม่ออกจากสาขา";
                    radLabel_Out.BackColor = ConfigClass.SetColor_Red();
                }
            }
            if (_pDateIn != " ")
            {
                radLabel_In.Text = "เข้า " + _pDateIn;
                radLabel_In.BackColor = ConfigClass.SetColor_GreenPastel();
            }
            else
            {
                radLabel_In.Text = "ยังไม่เข้าสาขา";
                radLabel_In.BackColor = ConfigClass.SetColor_Red();
            }

            SetBranch(_pBch);
            SetTeam(" AND SHOW_ID = '" + _pTeam + @"' ");
            dtItems = _pDetail;
            radGridView_Item.DataSource = dtItems;
            dtItems.AcceptChanges();


            string strEmp = string.Format(@"
                SELECT	'1' AS C,MNSV_EmpID AS EMPLID,MNSV_EmpName AS SPC_NAME,MNSV_EmpPosition AS POSSITION,MNSV_EmpDpt AS DESCRIPTION
                FROM	SHOP_MNSV_DT WITH (NOLOCK)
                        INNER JOIN SHOP_MNSV_HD WITH (NOLOCK) ON Shop_MNSV_DT.MNSV_Docno = Shop_MNSV_HD.MNSV_Docno
                WHERE	Shop_MNSV_DT.MNSV_Docno = '" + _pDocno + @"'
                ORDER BY MNSV_EmpID
            ");
            DataTable dt = ConnectionClass.SelectSQL_Main(strEmp);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string findTimeKeepter = string.Format(@"
                    SELECT	PW_IDCARD,CASE PW_STATUS WHEN '0' THEN 'OK' ELSE 'OUT' END AS STATIME
                    FROM	SHOP_TIMEKEEPER with (nolock) 
                    WHERE	PW_TYPE = '1' 
		                    AND PW_REMARK = '" + _pDocno + @"' 
                            AND PW_IDCARD = '" + dt.Rows[i]["EMPLID"].ToString() + @"'
                ");
                DataTable dtfindTimeKeepter = ConnectionClass.SelectSQL_Main(findTimeKeepter);
                string sta = "IN";
                if (dtfindTimeKeepter.Rows.Count > 0)
                {
                    sta = dtfindTimeKeepter.Rows[0]["STATIME"].ToString();
                }
                dtEmp.Rows.Add(dt.Rows[i]["C"].ToString(),
                    dt.Rows[i]["EMPLID"].ToString(),
                    dt.Rows[i]["SPC_NAME"].ToString(),
                    dt.Rows[i]["POSSITION"].ToString(),
                    dt.Rows[i]["DESCRIPTION"].ToString(),
                    sta);
            }

            RadGridView_Emp.DataSource = dtEmp;
            dt.AcceptChanges();

            //เช็คลงเวลาเข้าออก
            rowEmplScanIN = 0;
            rowEmplScanOUT = 0;
            for (int i = 0; i < RadGridView_Emp.RowCount; i++)
            {
                if (!(RadGridView_Emp.Rows[i].Cells["STATIME"].Value is null))
                {
                    if (RadGridView_Emp.Rows[i].Cells["STATIME"].Value.ToString() == "OUT") rowEmplScanIN += 1; else rowEmplScanOUT += 1;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //Delete    JOB
        private void RadGridView_Item_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "MNSVDetail":
                    if (radLabel_Out.Text != "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานมีการลงเวลาออกจากสาขาเรียบร้อยแล้ว ไม่สามารถแก้ไขได้อีก"); return;
                    }

                    if (pApv != "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับรายการงานที่เพิ่มและบันทึกไปแล้ว" + Environment.NewLine + @"ไม่สามารถลบรายการได้.");
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete(radGridView_Item.CurrentRow.Cells["MNSVDetail"].Value.ToString()) == DialogResult.No)
                    {
                        return;
                    }
                    this.dtItems.Rows.RemoveAt(radGridView_Item.CurrentRow.Index);
                    radTextBox_Desc.Focus();

                    if (dtItems.Rows.Count == 0) RadButton_Save.Enabled = false;

                    dtItems.AcceptChanges();
                    break;
                default:
                    break;

            }
        }
        //JOB
        private void RadTextBox_Desc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Desc.Text == "") return;

                if (pApv == "3")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("พนักงานมีการลงเวลาออกจากสาขาเรียบร้อยแล้ว ไม่สามารถแก้ไขได้อีก"); return;
                }

                double ii = Convert.ToDouble(radGridView_Item.Rows.Count) + 1;
                if (pApv == "0")
                {
                    dtItems.Rows.Add(radTextBox_Desc.Text, "0", "0", "", ii.ToString(), "");
                    if (dtItems.Rows.Count == 1)
                    {
                        RadButton_Save.Enabled = true;
                    }
                }
                else
                {
                    string T = MNSV_Class.MNSV_DocInsertJob(radLabel_Docno.Text, radTextBox_Desc.Text, ii);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        dtItems = MNSV_Class.MNSV_GetDataDT(radLabel_Docno.Text);

                    }
                }

                radGridView_Item.DataSource = dtItems;
                dtItems.AcceptChanges();

                radTextBox_Desc.SelectAll();
                radTextBox_Desc.Focus();
            }
        }
        //เช็คแบบประเมินการกดลงเวลาออก
        int CheckApvBch()
        {
            int rr = 0;
            for (int i = 0; i < radGridView_Item.Rows.Count; i++)
            {
                if ((radGridView_Item.Rows[i].Cells["MNSVOK"].Value.ToString() == "0") &&
                    (radGridView_Item.Rows[i].Cells["MNSVNO"].Value.ToString() == "0"))
                {
                    rr++;
                }
            }
            return rr;
        }
        //
        Boolean ScanFinger(string _pEmpID, string _pEmplName)
        {
            try
            {
                Empl = new Data_EMPLTABLE(_pEmpID);
                if (Empl.EMPLTABLE_EMPLID_M == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลพนักงาน ติดต่อ 8570");
                    Cursor.Current = Cursors.Default;
                    return false;
                }

                using (FormShare.FingerScan FingerScan = new FormShare.FingerScan(
                    Empl.EMPLTABLE_EMPLID, "ลงเวลา", _pEmplName, "ทีมจัดร้าน", "แผนกดูแลระบบงานมินิมาร์ทสาขา"))
                {
                    DialogResult dr = FingerScan.ShowDialog();
                    if (dr != DialogResult.Yes)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("กรุณาสแกนนิ้วใหม่อีกครั้ง");

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("กรุณาติดตั้งเครื่องสแกน");
                Cursor.Current = Cursors.Default;
                return false;
            }
        }
        //ลงเวลาเข้าออก
        private void RadGridView_Emp_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "STATIME":
                    //ลงเวลาเข้างาน
                    string empID = RadGridView_Emp.CurrentRow.Cells["EmplID"].Value.ToString();
                    string sta = RadGridView_Emp.CurrentRow.Cells["STATIME"].Value.ToString();

                    ArrayList strIn = new ArrayList();
                    if (sta == "IN")
                    {

                        if (ScanFinger(RadGridView_Emp.CurrentRow.Cells["EmplID"].Value.ToString().Substring(1, 7), RadGridView_Emp.CurrentRow.Cells["SPC_NAME"].Value.ToString()) == false)
                        {
                            return;
                        }

                        strIn.Add(string.Format($@"
                        INSERT INTO SHOP_TIMEKEEPER   
                                (PW_IDCARD,PW_PWCARD,PW_NAME,PW_BRANCH_IN,
                                PW_WHOIN,PW_POSITION,PW_REMARK)  
                        values  ('{Empl.EMPLTABLE_EMPLID_M}','{Empl.EMPLTABLE_ALTNUM}','{Empl.EMPLTABLE_SPC_NAME}','{Controllers.SystemClass.SystemBranchID}',
                                 '{Empl.EMPLTABLE_ALTNUM}', '{Empl.EMPLTABLE_POSSITION}','{radLabel_Docno.Text}')"));

                        //Update DT
                        strIn.Add(string.Format(@"  
                            UPDATE  SHOP_MNSV_DT 
                            SET     MNSV_IN = '1', MNSV_INDATE = convert(varchar,getdate(),23), 
                                    MNSV_INTIME = convert(varchar,getdate(),24), MNSV_INWHO = '" + SystemClass.SystemUserID + @"'
                            WHERE   MNSV_Docno='" + radLabel_Docno.Text + @"' AND MNSV_EmpID = '" + empID + @"' ")
                        );
                        //เช็ค row empl scan
                        rowEmplScanIN += 1;
                        if (rowEmplScanIN == RadGridView_Emp.RowCount)
                        {
                            //Update HD
                            strIn.Add(string.Format(@"  
                            UPDATE  SHOP_MNSV_HD 
                            SET     MNSV_IN = '1', MNSV_INDATE=convert(varchar,getdate(),23), 
                                    MNSV_INTIME=convert(varchar,getdate(),24), MNSV_INWHO = '" + SystemClass.SystemUserID + @"'
                            WHERE   MNSV_Docno='" + radLabel_Docno.Text + @"' ")
                            );
                        }

                        String tranctionIn = ConnectionClass.ExecuteSQL_ArrayMain(strIn);
                        if (tranctionIn == "")
                        {
                            RadGridView_Emp.CurrentRow.Cells["STATIME"].Value = "OUT";
                            if (rowEmplScanIN == RadGridView_Emp.RowCount)
                            {
                                pApv = "2";
                                radLabel_In.Text = "เข้า " + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss");
                                radLabel_In.BackColor = ConfigClass.SetColor_GreenPastel();
                            }
                        }
                    }
                    else if (sta == "OUT")
                    {
                        //ลงเวลาออกงาน
                        if (CheckINOUT(RadGridView_Emp, "IN"))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกออกได้ พนักงานยังลงเวลาเข้างานไม่ครบทุกคน");
                            return;
                        }

                        if (CheckApvBch() > 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ประเมินการทำงานให้เรียบร้อยก่อนการลงเวลาออกงานทุกครั้ง");
                            return;
                        }

                        if (ScanFinger(RadGridView_Emp.CurrentRow.Cells["EmplID"].Value.ToString().Substring(1, 7), RadGridView_Emp.CurrentRow.Cells["SPC_NAME"].Value.ToString()) == false)
                        {
                            return;
                        }

                        String PW_ID = "", PW_DATETIME_IN;
                        DataTable Dt_checkEmplIn = Class.Models.EmplClass.GetEmployee(empID.Substring(1, 7));
                        if (Dt_checkEmplIn.Rows.Count > 0)
                        {
                            PW_DATETIME_IN = Dt_checkEmplIn.Rows[0]["PW_DATETIME_IN"].ToString();
                            if (Dt_checkEmplIn.Rows[0]["PW_ID"].ToString() != "0")
                            {
                                PW_ID = Dt_checkEmplIn.Rows[0]["PW_ID"].ToString();
                            }
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลลงเวลาเข้างาน");
                            return;
                        }

                        string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNEP", "-", "MNEP", "1");
                        String SpanDay, SpanTime;
                        SpanDay = GetTimeSpan(PW_DATETIME_IN, "DAY");
                        SpanTime = GetTimeSpan(PW_DATETIME_IN, "TIME");

                        strIn.Add(string.Format($@"
                        UPDATE  SHOP_TIMEKEEPER 
                        SET     PW_BRANCH_OUT = '{SystemClass.SystemBranchID}',
                                PW_DATE_OUT =convert(varchar,getdate(),23), 
                                PW_TIME_OUT = convert(varchar, getdate(), 24),
                                PW_DATETIME_OUT = convert(varchar, getdate(), 25), 
                                PW_SUMDATE = '{SpanDay}', 
                                PW_SUMTIME = '{SpanTime}',
                                PW_DATEOUT = convert(varchar, getdate(), 23), 
                                PW_TIMEOUT = convert(varchar, getdate(), 24),
                                PW_WHOOUT = '{SystemClass.SystemUserID}', 
                                PW_STATUS = '0',
                                PW_QTYBill = '0',
                                PW_MONEYUSEIN = '0',
                                PW_MNEP =  '{maxDocno}'
                        WHERE   PW_ID =  '{PW_ID}' "));
                        //Update DT
                        strIn.Add(string.Format(@"  
                            UPDATE  SHOP_MNSV_DT 
                            SET     MNSV_OUT = '1', MNSV_OUTDATE = convert(varchar,getdate(),23), 
                                    MNSV_OUTTIME = convert(varchar,getdate(),24), MNSV_OUTWHO = '" + SystemClass.SystemUserID + @"'
                            WHERE   MNSV_Docno='" + radLabel_Docno.Text + @"' AND MNSV_EmpID = '" + empID + @"' ")
                        );


                        rowEmplScanOUT += 1;
                        if (rowEmplScanOUT == RadGridView_Emp.RowCount)
                        {
                            //Update HD
                            strIn.Add(string.Format(@"  
                                UPDATE  SHOP_MNSV_HD 
                                SET     MNSV_OUT = '1', MNSV_OUTDATE=convert(varchar,getdate(),23), 
                                        MNSV_OUTTIME=convert(varchar,getdate(),24), MNSV_OUTWHO = '" + SystemClass.SystemUserID + @"'
                                WHERE   MNSV_Docno='" + radLabel_Docno.Text + @"' ")
                        );
                        }
                        String tranction = ConnectionClass.ExecuteSQL_ArrayMain(strIn);
                        if (tranction == "")
                        {
                            RadGridView_Emp.CurrentRow.Cells["STATIME"].Value = "OK";
                            if (rowEmplScanOUT == RadGridView_Emp.RowCount)
                            {
                                pApv = "3";
                                radLabel_Out.Text = "ออก " + DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss");
                                radLabel_Out.BackColor = ConfigClass.SetColor_GreenPastel();
                            }
                        }
                    }


                    break;
                default:
                    break;
            }
        }
        //Check in out
        private bool CheckINOUT(RadGridView GridViewShow, String StatusINOUT)
        {
            bool Status = false;
            for (int i = 0; i < GridViewShow.RowCount; i++)
            {
                if (GridViewShow.Rows[i].Cells["STATIME"].Value.ToString() == StatusINOUT)
                {
                    Status = true;
                }
            }
            return Status;
        }
        //คำนวณเวลา
        private string GetTimeSpan(String DateBegin, String spanType)
        {
            String SpanResult = string.Empty;
            TimeSpan Span = DateTime.Now - DateTime.Parse(DateBegin);
            if (spanType == "DAY")
            {
                SpanResult = Span.Days.ToString();
            }
            else if (spanType == "MINUTES")
            {
                SpanResult = Span.TotalMinutes.ToString();
            }
            else if (spanType == "TIME")
            {
                if (Span.Hours.ToString().Length < 2)
                {
                    SpanResult = "0" + Span.Hours.ToString();
                }
                else
                {
                    SpanResult = Span.Hours.ToString();
                }
                if (Span.Minutes.ToString().Length == 1)
                {
                    SpanResult = SpanResult + ":0" + Span.Minutes.ToString();
                }
                else
                {
                    SpanResult = SpanResult + ":" + Span.Minutes.ToString();
                }
            }

            return SpanResult;
        }
    }
}
