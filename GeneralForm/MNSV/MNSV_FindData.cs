﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNSV
{
    public partial class MNSV_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();

        public string pDocno;
        public string pBranchID;
        public string pTeam;
        public string pDateIn;
        public string pDateOut;
        public DataTable pDetail;

        string docno;

        public MNSV_FindData()
        {
            InitializeComponent();
        }
        //Load
        private void MNSV_FindData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("MNSV_IN", "เข้า")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("MNSV_OUT", "ออก")));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_Branch", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_BranchName", "ชื่อสาขา", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_Docno", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_Date", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_WhoID", "ผู้บันทึก", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_WhoName", "ผู้ยืนยัน", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_WhoDpt", "แผนกบันทึก", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_TEAM", "ทีม", 50)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Team_NAME", "ชื่อทีม", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_INDATE", "เวลาเข้า", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_OUTDATE", "เวลาออก", 180)));


            RadGridView_ShowHD.Columns["MNSV_IN"].IsPinned = true;
            RadGridView_ShowHD.Columns["MNSV_OUT"].IsPinned = true;
            RadGridView_ShowHD.Columns["MNSV_Branch"].IsPinned = true;


            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSVDetail", "รายละเอียดงาน", 500)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("MNSVOK", "เรียบร้อย", 80)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("MNSVNO", "ไม่เรียบร้อย", 80)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSVRemark", "หมายเหตุ", 250)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNSVSeq", "SeqNo")));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameUp", "ผู้ประเมินงาน", 250)));

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_Begin.Value = DateTime.Now;
            radDateTimePicker_End.Value = DateTime.Now;
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            string pBch = "";
            if (SystemClass.SystemBranchID != "MN000")
            {
                pBch = " AND MNSV_Branch = '" + SystemClass.SystemBranchID + @"' ";
            }
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }

            dtDataHD = MNSV_Class.MNSV_GetDataHD(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), pBch);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            if (docno != _pDocno)
            {
                dtDataDT = MNSV_Class.MNSV_GetDataDT(_pDocno);
                RadGridView_ShowDT.DataSource = dtDataDT;
                pDetail = dtDataDT;
                dtDataDT.AcceptChanges();
                docno = _pDocno;
            }


        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["MNSV_Docno"].Value.ToString() == "")) return;

            if (RadGridView_ShowHD.Rows.Count == 0) { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["MNSV_Docno"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["MNSV_Docno"].Value.ToString();
            pBranchID = RadGridView_ShowHD.CurrentRow.Cells["MNSV_Branch"].Value.ToString();
            pTeam = RadGridView_ShowHD.CurrentRow.Cells["MNSV_TEAM"].Value.ToString();
            pDateIn = RadGridView_ShowHD.CurrentRow.Cells["MNSV_INDATE"].Value.ToString();
            pDateOut = RadGridView_ShowHD.CurrentRow.Cells["MNSV_OUTDATE"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
