﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNSV
{
    public partial class MNSV_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dtBch;
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        // 0 - รายงานค่าคอมทีมจัดร้าน
        //Load
        public MNSV_Report(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load
        private void MNSV_Report_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_pdf.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(30), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;

            switch (_pTypeReport)
            {
                case "0":
                    radDateTimePicker_D2.Visible = true;
                    radDateTimePicker_D1.Visible = true;
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Visible = false;
                    RadCheckBox_1.Visible = true;
                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.CheckState = CheckState.Unchecked;

                    if (SystemClass.SystemBranchID == "MN000") dtBch = BranchClass.GetBranchAll("'1','4'", " '1'"); else dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DataSource = dtBch;
                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Team_ID", "รหัสทีม", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Team_NAME", "ชื่อทีม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "พนักงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("C_WORK", "จำนวนงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PRICE", "ค่าคอม/งาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMALL", "รวมทั้งหมด", 100)));

                    break;

                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            switch (_pTypeReport)
            {
                case "0":
                    string pBch = "";
                    if (RadCheckBox_1.CheckState == CheckState.Checked) pBch = " AND MNSV_Branch = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
                    dt_Data = Models.EmplClass.MNSV_CommissionTeamAB(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pBch);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (_pTypeReport)
            {
                case "1":

                    break;

                default:
                    break;
            }
        }
        //Document
        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }


    }
}
