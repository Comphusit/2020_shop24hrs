﻿namespace PC_Shop24Hrs.GeneralForm.MNSV
{
    partial class MNSV_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNSV_Main));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_DescEmp = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Emp = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button_pdt = new System.Windows.Forms.Button();
            this.radDropDownList_Bch = new Telerik.WinControls.UI.RadDropDownList();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button_Find = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.radDropDownList_team = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Item = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Out = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_In = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DescEmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Emp.MasterTemplate)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Bch)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_team)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item.MasterTemplate)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Out)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_In)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_DescEmp, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.RadGridView_Emp, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(418, 653);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // radLabel_DescEmp
            // 
            this.radLabel_DescEmp.AutoSize = false;
            this.radLabel_DescEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_DescEmp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_DescEmp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DescEmp.ForeColor = System.Drawing.Color.Black;
            this.radLabel_DescEmp.Location = new System.Drawing.Point(3, 631);
            this.radLabel_DescEmp.Name = "radLabel_DescEmp";
            this.radLabel_DescEmp.Size = new System.Drawing.Size(412, 19);
            this.radLabel_DescEmp.TabIndex = 56;
            this.radLabel_DescEmp.Text = "<html>DoubleClick ลงเวลา &gt;&gt; ลงเวลาเข้าออกงาน</html>";
            // 
            // RadGridView_Emp
            // 
            this.RadGridView_Emp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Emp.Location = new System.Drawing.Point(3, 87);
            // 
            // 
            // 
            this.RadGridView_Emp.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Emp.Name = "RadGridView_Emp";
            // 
            // 
            // 
            this.RadGridView_Emp.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Emp.Size = new System.Drawing.Size(412, 538);
            this.RadGridView_Emp.TabIndex = 20;
            this.RadGridView_Emp.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Emp_ViewCellFormatting);
            this.RadGridView_Emp.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Emp_CellClick);
            this.RadGridView_Emp.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Emp_CellDoubleClick);
            this.RadGridView_Emp.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Emp_ConditionalFormattingFormShown);
            this.RadGridView_Emp.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Emp_FilterPopupRequired);
            this.RadGridView_Emp.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Emp_FilterPopupInitialized);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.button_pdt, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radDropDownList_Bch, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(412, 36);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // button_pdt
            // 
            this.button_pdt.BackColor = System.Drawing.Color.Transparent;
            this.button_pdt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_pdt.ForeColor = System.Drawing.Color.Black;
            this.button_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.button_pdt.Location = new System.Drawing.Point(3, 3);
            this.button_pdt.Name = "button_pdt";
            this.button_pdt.Size = new System.Drawing.Size(29, 28);
            this.button_pdt.TabIndex = 78;
            this.button_pdt.UseVisualStyleBackColor = false;
            this.button_pdt.Click += new System.EventHandler(this.Button_pdt_Click);
            // 
            // radDropDownList_Bch
            // 
            this.radDropDownList_Bch.AutoSize = false;
            this.radDropDownList_Bch.DropDownHeight = 150;
            this.radDropDownList_Bch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Bch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Bch.Location = new System.Drawing.Point(73, 3);
            this.radDropDownList_Bch.Name = "radDropDownList_Bch";
            this.radDropDownList_Bch.Size = new System.Drawing.Size(228, 28);
            this.radDropDownList_Bch.TabIndex = 0;
            this.radDropDownList_Bch.Text = "radDropDownList1";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.button_Find, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.button_add, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radDropDownList_team, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 45);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(412, 36);
            this.tableLayoutPanel2.TabIndex = 21;
            // 
            // button_Find
            // 
            this.button_Find.BackColor = System.Drawing.Color.Transparent;
            this.button_Find.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Find.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Find.ForeColor = System.Drawing.Color.Black;
            this.button_Find.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.button_Find.Location = new System.Drawing.Point(38, 3);
            this.button_Find.Name = "button_Find";
            this.button_Find.Size = new System.Drawing.Size(29, 28);
            this.button_Find.TabIndex = 81;
            this.button_Find.UseVisualStyleBackColor = false;
            this.button_Find.Click += new System.EventHandler(this.Button_Find_Click);
            // 
            // button_add
            // 
            this.button_add.BackColor = System.Drawing.Color.Transparent;
            this.button_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_add.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_add.ForeColor = System.Drawing.Color.Black;
            this.button_add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.button_add.Location = new System.Drawing.Point(3, 3);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(29, 28);
            this.button_add.TabIndex = 80;
            this.button_add.UseVisualStyleBackColor = false;
            this.button_add.Click += new System.EventHandler(this.Button_add_Click);
            // 
            // radDropDownList_team
            // 
            this.radDropDownList_team.AutoSize = false;
            this.radDropDownList_team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList_team.DropDownHeight = 150;
            this.radDropDownList_team.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_team.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_team.Location = new System.Drawing.Point(73, 3);
            this.radDropDownList_team.Name = "radDropDownList_team";
            this.radDropDownList_team.Size = new System.Drawing.Size(336, 30);
            this.radDropDownList_team.TabIndex = 19;
            this.radDropDownList_team.Text = "radDropDownList1";
            this.radDropDownList_team.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_team_SelectedValueChanged);
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Item, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(427, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(479, 653);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 631);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(473, 19);
            this.radLabel_Detail.TabIndex = 55;
            this.radLabel_Detail.Text = "<html>ระบุสาขา | เลือก ชั้นวาง เพื่อดูสินค้าทั้งหมด | กด + &gt;&gt; เพิ่มสินค้า |" +
    " กด / &gt;&gt; ลบสินค้า</html>";
            // 
            // radGridView_Item
            // 
            this.radGridView_Item.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Item.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Item.Location = new System.Drawing.Point(3, 88);
            // 
            // 
            // 
            this.radGridView_Item.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Item.Name = "radGridView_Item";
            // 
            // 
            // 
            this.radGridView_Item.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Item.Size = new System.Drawing.Size(473, 537);
            this.radGridView_Item.TabIndex = 18;
            this.radGridView_Item.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Item_ViewCellFormatting);
            this.radGridView_Item.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Item_CellClick);
            this.radGridView_Item.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Item_CellDoubleClick);
            this.radGridView_Item.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Item_ConditionalFormattingFormShown);
            this.radGridView_Item.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Item_FilterPopupRequired);
            this.radGridView_Item.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Item_FilterPopupInitialized);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.radTextBox_Desc, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.RadButton_Save, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.RadButton_Cancel, 2, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(473, 39);
            this.tableLayoutPanel6.TabIndex = 20;
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.AutoSize = false;
            this.radTextBox_Desc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Desc.Location = new System.Drawing.Point(3, 3);
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            this.radTextBox_Desc.Size = new System.Drawing.Size(307, 33);
            this.radTextBox_Desc.TabIndex = 0;
            this.radTextBox_Desc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Desc_KeyDown);
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadButton_Save.Location = new System.Drawing.Point(316, 4);
            this.RadButton_Save.Name = "RadButton_Save";
            // 
            // 
            // 
            this.RadButton_Save.RootElement.AutoSize = false;
            this.RadButton_Save.Size = new System.Drawing.Size(74, 32);
            this.RadButton_Save.TabIndex = 78;
            this.RadButton_Save.Text = "ยืนยัน";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "ยืนยัน";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(1))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FocusPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(3))).Font = new System.Drawing.Font("Tahoma", 12F);
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadButton_Cancel.Location = new System.Drawing.Point(396, 4);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(74, 32);
            this.RadButton_Cancel.TabIndex = 79;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(473, 34);
            this.panel1.TabIndex = 56;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.Controls.Add(this.radLabel_Out, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.radLabel_In, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.radLabel_Docno, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(473, 34);
            this.tableLayoutPanel7.TabIndex = 23;
            // 
            // radLabel_Out
            // 
            this.radLabel_Out.AutoSize = false;
            this.radLabel_Out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Out.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_Out.Location = new System.Drawing.Point(333, 3);
            this.radLabel_Out.Name = "radLabel_Out";
            this.radLabel_Out.Size = new System.Drawing.Size(137, 28);
            this.radLabel_Out.TabIndex = 25;
            this.radLabel_Out.Text = "ลงเวลาออก";
            // 
            // radLabel_In
            // 
            this.radLabel_In.AutoSize = false;
            this.radLabel_In.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_In.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radLabel_In.Location = new System.Drawing.Point(192, 3);
            this.radLabel_In.Name = "radLabel_In";
            this.radLabel_In.Size = new System.Drawing.Size(135, 28);
            this.radLabel_In.TabIndex = 24;
            this.radLabel_In.Text = "ลงเวลาเข้า";
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(3, 3);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(183, 28);
            this.radLabel_Docno.TabIndex = 22;
            this.radLabel_Docno.Text = "MNSV200401000000";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 424F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(909, 659);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // MNSV_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 659);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MNSV_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "งานทีมจัดร้าน";
            this.Load += new System.EventHandler(this.MNSV_Main_Load);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DescEmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Emp.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Emp)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Bch)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_team)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Out)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_In)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Bch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Item;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private System.Windows.Forms.Button button_pdt;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_team;
        private Telerik.WinControls.UI.RadGridView RadGridView_Emp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button_Find;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private Telerik.WinControls.UI.RadLabel radLabel_In;
        private Telerik.WinControls.UI.RadLabel radLabel_Out;
        private Telerik.WinControls.UI.RadLabel radLabel_DescEmp;
    }
}
