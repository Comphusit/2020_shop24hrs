﻿//CheckOK
using System.Data; 
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.MNSV
{
    class MNSV_Class
    {
        //ค้นหาข้อมูลพนักงาน ตามทีมที่กำหนด
        public static DataTable MNSV_GetEmpByTeam(string team)
        {
            string sql = string.Format(@"
                SELECT	'1' AS C,SHOP_CONFIGBRANCH_DETAIL.SHOW_ID AS EMPLID,SHOW_NAME AS SPC_NAME 
                FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) 
                        INNER JOIN (
							SELECT SHOW_ID
							FROM SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)  
							WHERE TYPE_CONFIG = '38' AND STA = '1')TMP ON SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = TMP.SHOW_ID
                WHERE	TYPE_CONFIG = '38' AND BRANCH_ID = '" + team + @"' AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1'
                ORDER BY SHOP_CONFIGBRANCH_DETAIL.SHOW_ID  ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาทีมตามเงื่อนไข
        public static DataTable MNSV_GetTeam(string _pCon)
        {
            string sql = string.Format(@"
                SELECT	SHOW_ID AS Team_ID,SHOW_NAME AS Team_NAME                         
                FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
                WHERE	TYPE_CONFIG = '37' " + _pCon + @"
                ORDER BY SHOW_ID ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
       
        //ค้นหาข้อมูลหลักของตามราง HD
        public static DataTable MNSV_GetDataHD(string date1, string date2, string pBch)
        {
            string sqlSelect = string.Format(@"
                SELECT	CONVERT(VARCHAR,MNSV_Date,23) AS MNSV_Date,MNSV_Docno,MNSV_Branch,MNSV_BranchName,MNSV_WhoID,MNSV_WhoName,MNSV_WhoDpt,
		                MNSV_IN,CASE MNSV_IN WHEN '0' THEN '' ELSE CONVERT(VARCHAR,MNSV_INDATE,23) END  + ' '+ MNSV_INTIME  AS MNSV_INDATE,MNSV_INTIME,
		                MNSV_OUT,CASE MNSV_OUT WHEN '0' THEN '' ELSE CONVERT(VARCHAR,MNSV_OUTDATE,23) END  + ' '+ MNSV_OUTTIME  AS MNSV_OUTDATE,MNSV_OUTTIME,
                        MNSV_TEAM,Team_NAME
                FROM	Shop_MNSV_HD WITH (NOLOCK)
                        LEFT OUTER JOIN (
		                    SELECT	SHOW_ID AS Team_ID,SHOW_NAME AS Team_NAME                         
		                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
		                    WHERE	TYPE_CONFIG = '37'
		                    )TEAM ON Shop_MNSV_HD.MNSV_TEAM = TEAM.Team_ID
                WHERE	MNSV_STA = '1' AND CONVERT(VARCHAR,MNSV_Date,23) BETWEEN '" + date1 + @"' 
                            AND '" + date2 + @"' " + pBch + @"
                ORDER BY MNSV_Docno DESC
            ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาข้อมูลรายละเอียดตามเลขที่บิล
        public static DataTable MNSV_GetDataDT(string docno)
        {
            string sqlSelect = string.Format(@"
                SELECT	MNSVDetail,MNSVOK,MNSVNO,MNSVRemark,MNSVSeq,ISNULL(WhoUp,'') + '-' + ISNULL(WhoNameUp,'') AS WhoNameUp
                FROM	SHOP_MNSV_RC WITH (NOLOCK)
                WHERE	MNSV_Docno = '" + docno + @"'
                ORDER BY MNSVSeq
                   ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ยกเลิกเอกสาร
        public static string MNSV_DocCancle(string docno)
        {
            string str = string.Format(@"
                    UPDATE  SHOP_MNSV_HD 
                    SET     MNSV_STA = '0'
                    WHERE   MNSV_DOCNO = '" + docno + @"'
                    ");
            return ConnectionClass.ExecuteSQL_Main(str);
        }
        //Update Job Detail
        public static string MNSV_DocUpdateJob(string docno, string staOK, string staNo, string rmk,string seqno)
        {
            string strUpOK = string.Format(@"
                        UPDATE  SHOP_MNSV_RC
                        SET     MNSVOK = '" + staOK + @"',MNSVNO = '" + staNo + @"',MNSVRemark = '" + rmk + @"',
                                WhoUp = '" + SystemClass.SystemUserID + @"',WhoNameUp = '" + SystemClass.SystemUserName + @"'
                        WHERE   MNSV_Docno = '" + docno + @"'
                                AND MNSVSeq = '" + seqno + @"'
                    ");
            return ConnectionClass.ExecuteSQL_Main(strUpOK);
        }
        //Insert JOB
        public static string MNSV_DocInsertJob(string docno,string rmk, double ii)
        {
            string Instr = string.Format(@"
                        INSERT INTO SHOP_MNSV_RC
                        ([MNSV_Docno],[MNSVSeq],[MNSVDetail],[WhoIns])
                        VALUES('" + docno + @"','" + ii + @"','" + rmk + @"',
                        '" + SystemClass.SystemUserID + @"')");
            return ConnectionClass.ExecuteSQL_Main(Instr);
        }
    }
}
