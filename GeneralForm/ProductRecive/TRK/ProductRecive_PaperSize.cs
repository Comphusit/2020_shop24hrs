﻿//CheckOK
using System;
using System.Windows.Forms;

namespace PC_Shop24Hrs.GeneralForm.ProductRecive.TRK
{
    public partial class ProductRecive_PaperSize : Telerik.WinControls.UI.RadForm
    {
        public int x, y;
        public ProductRecive_PaperSize()
        {
            InitializeComponent();
        }
        //load
        private void ProductRecive_PaperSize_Load(object sender, EventArgs e)
        {
            RadButton_A.ButtonElement.ShowBorder = true;
            RadButton_B.ButtonElement.ShowBorder = true;
            RadButton_C.ButtonElement.ShowBorder = true;
            RadButton_D.ButtonElement.ShowBorder = true;
            RadButton_E.ButtonElement.ShowBorder = true;
            RadButton_F.ButtonElement.ShowBorder = true;
            radLabel1.Focus();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RadButton_A_Click(object sender, EventArgs e)
        {
            x = 50;//45
            y = 0;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RadButton_B_Click(object sender, EventArgs e)
        {
            x = 430;//600
            y = 0;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RadButton_C_Click(object sender, EventArgs e)
        {
            x = 50;
            y = 180;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RadButton_D_Click(object sender, EventArgs e)
        {
            x = 430;
            y = 180;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RadButton_E_Click(object sender, EventArgs e)
        {
            x = 50;
            y = 700;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RadButton_F_Click(object sender, EventArgs e)
        {
            x = 430;
            y = 700;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
         
    }
}
