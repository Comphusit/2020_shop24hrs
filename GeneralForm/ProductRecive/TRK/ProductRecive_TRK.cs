﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.ProductRecive.TRK
{
    public partial class ProductRecive_TRK : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeBill;
        //1 คือ บิลปกติ
        //2 คือ บิลเบิก แต่ไม่ใช้แลัว ว่างไว้ก่อน
        //3 คือ ขายดิน
        //4 คือ BRP
        readonly string _pPermission;
        //0 PrintImage Only
        //1 CreateBA + PrintImage
        readonly string _pDptLocation;
        DataTable dt = new DataTable();
        //Load
        public ProductRecive_TRK(string pTypeBill, string pPermission, string pDptLocation)
        {
            InitializeComponent();
            _pTypeBill = pTypeBill;
            _pPermission = pPermission;
            _pDptLocation = pDptLocation;
        }
        //Load
        private void ProductRecive_TRK_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButton_Search.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์รูป";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-7), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadCheckBox_Date.ButtonElement.Font = SystemClass.SetFontGernaral;
            if (_pTypeBill == "4")
            {
                RadCheckBox_Date.Enabled = true;
                radTextBox_BA.Visible = true; radLabel_BA.Visible = true;
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICE", "INVOICE", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BADOCNO", "เลขที่บิล BA", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRKID", "เลขที่บิล BRP", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LICENSEPLATE", "ทะบียนรถ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDER", "ผู้จำหน่าย", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARKS", "หมายเหตุ", 350)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOCATIONMNID", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOCATIONMNNAME", "ชื่อแผนกรับ", 220)));
                RadGridView_ShowHD.Columns["INVOICE"].IsPinned = true;
                radLabel_Detail.Text = "DoubleClick >> ตัวอย่างรูป | เมนูด้านขวามือ >> พิมพ์รูป";
            }
            else
            {
                RadCheckBox_Date.Enabled = true; RadCheckBox_Date.Checked = true;
                radTextBox_BA.Visible = false; radLabel_BA.Visible = false;
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRKID", "เลขที่บิล TRK", 170)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียน", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLETYPE", "ชนิดรถ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATETIMECAP", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOCATIONMNID", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOCATIONMNNAME", "ชื่อแผนกรับ", 220)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVOICE", "INVOICE")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("AMOUNT", "AMOUNT")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARKS", "หมายเหตุ", 350)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TABNAME", "เครื่อง", 100)));

                if (_pTypeBill == "3")
                {
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CUSTID", "ลูกค้า", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CUSTNAME", "ชื่อลูกค้า", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CUSTRATES", "ราคา/คน", 80)));
                    RadGridView_ShowHD.Columns["VEHICLETYPE"].Width = 170;
                    RadGridView_ShowHD.Columns["REMARKS"].Width = 170;
                    radLabel_Detail.Text = "DoubleClick >> ตัวอย่างรูป | เมนูด้านขวามือ >> พิมพ์รูป";
                }
                else
                {
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEBA_EMPLID", "ผู้สร้าง BA", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEBA_EMPLNAME", "ชื่อผู้สร้าง BA", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEBA_INVOICE", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CREATEBA_AMOUNT", "ยอดเงิน", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CREATEBA_DATETIME", "วันที่สร้าง")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CREATEBA_STATUS", "CREATEBA_STATUS")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CREATEBA_PRINTNUM", "CREATEBA_PRINTNUM")));
                    radLabel_Detail.Text = "DoubleClick >> ดูรายละเอียดบิลและพิมพ์รูป | เมนูด้านขวามือ >> พิมพ์รูป";
                }
                RadGridView_ShowHD.Columns["TRKID"].IsPinned = true;
            }

            DatagridClass.SetDefaultRadGridView(radGridView_Image);

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "", 400));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "", 400));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));

            radGridView_Image.TableElement.RowHeight = 450;
            radGridView_Image.EnableFiltering = false;
        }

        void ClearTxt()
        {
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            RadGridView_ShowHD.DataSource = dt;
            dt.AcceptChanges();
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(-7);
        }
        void FindBillBRP()
        {
            string pCondate = "";
            if (RadCheckBox_Date.Checked == true) pCondate = $@" CONVERT(VARCHAR,DOCDATE,23)  BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
            string pConDocno = "";
            if (radTextBox_BA.Text.Trim() != "") pConDocno = $@" (BADOCNO LIKE '%{radTextBox_BA.Text.Trim()}%' OR DOCNO LIKE '%{radTextBox_BA.Text.Trim()}%' OR INVOICE LIKE '%{radTextBox_BA.Text.Trim()}%') ";
            if ((pCondate == "") && (pConDocno == ""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุเงื่อนไขการค้นหาข้อมูลก่อนทุกครั้ง.");
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            dt = ProductRecive_Class.GetDetailBRP(pCondate, pConDocno);
            RadGridView_ShowHD.DataSource = dt;
            dt.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        private void RadTextBox_BA_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode== Keys.Enter)
            {
                if (radTextBox_BA.Text.Trim() == "") return;
                FindBillBRP();
            }
          
        }
        void SetDGV()
        {
            if (_pTypeBill == "4")//BRP
            {
                FindBillBRP();
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                string pConDpt = $@" AND LOCATIONMNID LIKE '{_pDptLocation}' ";
                if (_pTypeBill == "3") pConDpt = "";

                dt = ProductRecive_Class.GetDetailTRK(_pTypeBill, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pConDpt);
                RadGridView_ShowHD.DataSource = dt;
                dt.AcceptChanges();
                this.Cursor = Cursors.Default;
            }

        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeBill + _pPermission);
        }
        //Double Click
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pPermission == "1")
            {
                if (RadGridView_ShowHD.Rows.Count == 0) return;
                if (RadGridView_ShowHD.CurrentRow.Index == -1) return;
                if (RadGridView_ShowHD.CurrentRow.Cells["TRKID"].Value.ToString() == "") return;

                ProductRecive_Image productRecive_Image = new ProductRecive_Image(_pPermission, RadGridView_ShowHD.CurrentRow.Cells["TRKID"].Value.ToString());
                if (productRecive_Image.ShowDialog() == DialogResult.Yes) { }
            }
            else
            {
                PrintPreview();
            }
        }
        //Print
        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            PrintPreview();
        }
        //Fuction Print
        void PrintPreview()
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;
            string trk = RadGridView_ShowHD.CurrentRow.Cells["TRKID"].Value.ToString();
            if (trk == "") return;

            this.Cursor = Cursors.WaitCursor;
            DataTable dt;
            string l1 = $@"{Environment.NewLine}{Environment.NewLine}{trk}";
            string l2 = $@"{RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value} - {RadGridView_ShowHD.CurrentRow.Cells["EMPLNAME"].Value}";
            string l3 = $@"{RadGridView_ShowHD.CurrentRow.Cells["LOCATIONMNID"].Value} - {RadGridView_ShowHD.CurrentRow.Cells["LOCATIONMNNAME"].Value}";
            string l4 = $@"{RadGridView_ShowHD.CurrentRow.Cells["REMARKS"].Value}";
            string l5;

            if (_pTypeBill == "4")
            {
                dt = ProductRecive_Class.GetDetailBRPByBill(trk);
                string ba = RadGridView_ShowHD.CurrentRow.Cells["BADOCNO"].Value.ToString();
                if (ba != "") ba = $@" | {ba}";
                string invoice = RadGridView_ShowHD.CurrentRow.Cells["INVOICE"].Value.ToString();
                if (invoice != "") invoice = $@" | เลขที่บิลผู้จำหน่าย {invoice}";
                l1 = $@"{Environment.NewLine}{Environment.NewLine}{trk}{ba}{invoice}";
                l3 = $@"{RadGridView_ShowHD.CurrentRow.Cells["VENDER"].Value}";
                l5 = $@"รับสินค้า {RadGridView_ShowHD.CurrentRow.Cells["DOCDATE"].Value}  พิมพ์ {DateTime.Now:yyyy-MM-dd HH:mm:ss}";
                if (l3 == "") l3 = l5;else l3 = l3 + " | " + l5;
            }
            else
            {
                dt = ProductRecive_Class.GetDetailTRKByBill(trk);
                l5 = $@"รับสินค้า {RadGridView_ShowHD.CurrentRow.Cells["DATETIMECAP"].Value}  พิมพ์ {DateTime.Now:yyyy-MM-dd HH:mm:ss}";
                l1 = l1 + " | " + l5;
            }
            ProductRecive_Class.SetDGVTrkForPrint(radGridView_Image, dt);
            radGridView_Image.PrintStyle = DatagridClass.SetGridForPrint(new GridPrintStyle(), false, true, 12);

            RadPrintDocument document = new RadPrintDocument();
            DatagridClass.SetPrintDocumentPO(document, $@"{l1}{Environment.NewLine}{l2}{Environment.NewLine}{l3}{Environment.NewLine}{l4}", 120, false, "", "B", " SuperCheap", 12, 10);

            document.AssociatedObject = radGridView_Image;

            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document)
            {
                StartPosition = FormStartPosition.CenterScreen,
                WindowState = FormWindowState.Maximized
            };

            dialog.ShowDialog();
            this.Cursor = Cursors.Default;
        }
        //View Cell
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


    }
}
