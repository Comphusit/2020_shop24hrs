﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.GeneralForm.ProductRecive.TRK
{
    public partial class ProductRecive_Image : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        readonly string _pPermission;
        //0 PrintImage Only
        //1 CreateBA + PrintImage

        readonly string _trkID;
        int xx, yy;
        int printNum;

        private string sta_Save;// 0 Insert 1 Update
        DataTable dt;
        //Load
        public ProductRecive_Image(string pPermission, string trkID)
        {
            InitializeComponent(); ;
            _pPermission = pPermission;
            _trkID = trkID;
        }
        //Load
        private void ProductRecive_Image_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์รูป";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขข้อมูล";

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "", 400));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));

            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "", 400));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));

            RadGridView_ShowHD.TableElement.RowHeight = 450;
            RadGridView_ShowHD.EnableFiltering = false;

            radLabel_Detail.Text = "ระบุ เลขที่บิลและจำนวนเงิน กดบันทึก >> เข้า AX | เมนูด้านขวามือ >> พิมพ์รูป"; radLabel_Date.Visible = true;

            this.Text = $@"รายละเอียดการรับสินค้า {_trkID}";
            this.Cursor = Cursors.WaitCursor;

            dt = ProductRecive_Class.GetDetailTRKByBill(_trkID);
            ProductRecive_Class.SetDGVTrkForPrint(RadGridView_ShowHD, dt);

            if (dt.Rows[0]["CREATEBA_STATUS"].ToString() == "1")//ส่งเข้า AX แล้ว
            {
                radTextBox_Invoice.Enabled = false; radTextBox_Invoice.Text = "เลขที่บิลผู้จำหน่าย";
                radTextBox_Amount.Enabled = false; radTextBox_Amount.Text = "ยอดเงินผู้จำหน่าย";
                radTextBox_Invoice.Text = dt.Rows[0]["CREATEBA_INVOICE"].ToString();
                radTextBox_Amount.Text = double.Parse(dt.Rows[0]["CREATEBA_AMOUNT"].ToString()).ToString("N2");
                printNum = int.Parse(dt.Rows[0]["CREATEBA_PRINTNUM"].ToString());
                RadButton_Save.Text = "พิมพ์ซ้ำ"; sta_Save = "1"; RadButton_Save.Enabled = true;
                RadButton_Cancel.Focus();
            }
            else
            {
                radTextBox_Invoice.Enabled = true; radTextBox_Invoice.Text = "เลขที่บิลผู้จำหน่าย [Enter]";
                radTextBox_Amount.Enabled = false; radTextBox_Amount.Text = "ยอดเงินผู้จำหน่าย [Enter]";
                radTextBox_Invoice.Text = dt.Rows[0]["INVOICE"].ToString();
                radTextBox_Amount.Text = double.Parse(dt.Rows[0]["AMOUNT"].ToString()).ToString("N2");
                printNum = 1;
                RadButton_Save.Text = "บันทึก"; sta_Save = "0"; RadButton_Save.Enabled = false;
                radTextBox_Invoice.Focus();
            }

            if (_pPermission == "0")
            {
                radTextBox_Invoice.Enabled = false;
                radTextBox_Amount.Enabled = false;
                RadButton_Save.Enabled = false;
            }
            this.Cursor = Cursors.Default;
        }

        //Enter Invoice
        private void RadTextBox_Invoice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Invoice.Text.Trim() == "") return;
                radTextBox_Amount.Enabled = true;
                radTextBox_Amount.SelectAll();
                radTextBox_Amount.Focus();
            }
        }
        //Enter Amont
        private void RadTextBox_Amount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Amount.Text.Trim() == "") return;
                if (double.Parse(radTextBox_Amount.Text.Trim()) == 0) return;
                RadButton_Save.Enabled = true;
                RadButton_Save.Focus();
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion


        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pPermission);
        }

        private void RadTextBox_Amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
        //Cell Double Click
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;

            switch (RadGridView_ShowHD.CurrentColumn.Name.ToString())
            {
                case "IMG1":
                    if (RadGridView_ShowHD.CurrentRow.Cells["P_IMG1"].Value.ToString() == "") return;
                    System.Diagnostics.Process.Start(RadGridView_ShowHD.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    break;
                case "IMG2":
                    if (RadGridView_ShowHD.CurrentRow.Cells["P_IMG2"].Value.ToString() == "") return;
                    System.Diagnostics.Process.Start(RadGridView_ShowHD.CurrentRow.Cells["P_IMG2"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //print
        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            RadGridView_ShowHD.PrintStyle = DatagridClass.SetGridForPrint(new GridPrintStyle(), false, true, 12);

            RadPrintDocument document = new RadPrintDocument();
            string l1 = $@"{Environment.NewLine}{Environment.NewLine}{_trkID}";
            string l2 = $@"{dt.Rows[0]["EMPLID"]} - {dt.Rows[0]["EMPLNAME"]}";
            string l3 = $@"{dt.Rows[0]["LOCATIONMNID"]} - {dt.Rows[0]["LOCATIONMNNAME"]}";
            string l4 = $@"{dt.Rows[0]["REMARKS"]}";

            DatagridClass.SetPrintDocumentPO(document, $@"{l1}{Environment.NewLine}{l2}{Environment.NewLine}{l3}{Environment.NewLine}{l4}", 120, false, "", "B", " SuperCheap", 12, 10);

            document.AssociatedObject = RadGridView_ShowHD;

            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document)
            {
                StartPosition = FormStartPosition.CenterScreen,
                WindowState = FormWindowState.Maximized
            };

            dialog.ShowDialog();
            this.Cursor = Cursors.Default;
        }
        //Print BA
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (sta_Save == "0")//Insert
            {
                string recID = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss").Replace("-", "").Replace(":", "").Replace(" ", "");
                string recive = dt.Rows[0]["DATECAP"].ToString();
                string MNID = dt.Rows[0]["LOCATIONMNID"].ToString();

                ArrayList sqlAX = new ArrayList
                {
                    AX_SendData.Save_SHIPCARRIERINVOICE("0",_trkID,radTextBox_Invoice.Text.Trim(),double.Parse(radTextBox_Amount.Text.Trim()),recID, recive, recive, MNID,"")
                };

                ArrayList sql = new ArrayList
                {
                    ProductRecive_Class.ProductRecive_TRK_UpdateInvoice(_trkID, radTextBox_Invoice.Text, double.Parse(radTextBox_Amount.Text.Trim()))
                };

                string tSave = ConnectionClass.Execute_SameTime_2Server(sqlAX, IpServerConnectClass.ConMainAX, sql, IpServerConnectClass.ConSupcAndroid);

                MsgBoxClass.MsgBoxShow_SaveStatus(tSave);
                if (tSave == "")
                {
                    PrintDocument();
                    RadButton_Save.Text = "พิมพ์ซ้ำ"; sta_Save = "1";
                    RadButton_Cancel.Focus();
                }
            }
            else if (sta_Save == "2")//Edit
            {
                ArrayList sql2 = new ArrayList
                {
                    ProductRecive_Class.ProductRecive_TRK_UpdateInvoice(_trkID, radTextBox_Invoice.Text, double.Parse(radTextBox_Amount.Text.Trim()))
                };
                ArrayList sqlAX2 = new ArrayList
                {
                    AX_SendData.Save_SHIPCARRIERINVOICE("2",_trkID,radTextBox_Invoice.Text.Trim(),double.Parse(radTextBox_Amount.Text.Trim()),"", "", "", "","")
                };

                string tSave2 = ConnectionClass.Execute_SameTime_2Server(sqlAX2, IpServerConnectClass.ConMainAX, sql2, IpServerConnectClass.ConSupcAndroid);

                MsgBoxClass.MsgBoxShow_SaveStatus(tSave2);
                if (tSave2 == "")
                {
                    PrintDocument();
                    sta_Save = "1";
                    RadButton_Cancel.Focus();
                }
            }
            else//print BA Again
            {
                PrintDocument();
            }
        }
        void PrintDocument()
        {
            ProductRecive_PaperSize productRecive_PaperSize = new ProductRecive_PaperSize();
            if (productRecive_PaperSize.ShowDialog() == DialogResult.OK)
            {
                DialogResult result = printDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    xx = productRecive_PaperSize.x;
                    yy = productRecive_PaperSize.y;
                    printDocument.PrintController = printController;
                    printDocument.PrinterSettings = printDialog.PrinterSettings;
                    printDocument.Print();

                    if (ProductRecive_Class.ProductRecive_TRK_UpdatePrint(_trkID) == "") printNum++;
                }
            }
        }
        //Close
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //Edit
        private void ฑadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            sta_Save = "2";
            RadButton_Save.Text = "แก้ไข"; RadButton_Save.Enabled = false;
            radTextBox_Invoice.Enabled = true;
            radTextBox_Invoice.Focus();
        }

        //Print BA
        private void PintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int x = e.MarginBounds.Left - 50 + xx;
            int y = e.MarginBounds.Top - 50 + yy;

            e.Graphics.DrawString(_trkID, SystemClass.SetFont12, Brushes.Black, x, y);
            e.Graphics.DrawString(dt.Rows[0]["DATETIMECAP"].ToString().Replace(".000", ""), SystemClass.SetFont12, Brushes.Black, x, y + 20);
            e.Graphics.DrawString(dt.Rows[0]["EMPLNAME"].ToString(), SystemClass.SetFont12, Brushes.Black, x, y + 35);
            e.Graphics.DrawString("---------- ผู้รับสินค้า ----------", SystemClass.SetFont12, Brushes.Black, x, y + 50);
            e.Graphics.DrawString($@"{radTextBox_Invoice.Text} ${double.Parse(radTextBox_Amount.Text.Trim()):N2}", SystemClass.SetFont12, Brushes.Black, x, y + 65);
            e.Graphics.DrawString("---------- พิมพ์ครั้งที่ " + printNum.ToString() + " ----------", SystemClass.SetFont12, Brushes.Black, x, y + 80);
        }
    }
}
