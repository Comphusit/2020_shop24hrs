﻿namespace PC_Shop24Hrs.GeneralForm.ProductRecive.DROID
{
    partial class ProductRecive_DROID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductRecive_DROID));
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radCheckBox_Vender = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_Vender = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_Stop = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Reset = new Telerik.WinControls.UI.RadButton();
            this.radButton_Start = new Telerik.WinControls.UI.RadButton();
            this.radGridView_Vender = new Telerik.WinControls.UI.RadGridView();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_Grp = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_Grp = new Telerik.WinControls.UI.RadDropDownList();
            this.RadButton_SaveAX = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox_Apv = new Telerik.WinControls.UI.RadCheckBox();
            this.RadCheckBox_Date = new Telerik.WinControls.UI.RadCheckBox();
            this.radDateTimePicker_D1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_D2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Stop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Reset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Vender.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Grp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Grp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SaveAX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Apv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 575);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ShowHD.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_ShowHD_CellBeginEdit);
            this.RadGridView_ShowHD.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellEndEdit);
            this.RadGridView_ShowHD.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellClick);
            this.RadGridView_ShowHD.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellDoubleClick);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 642F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 584);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 49);
            this.radLabel_Detail.TabIndex = 53;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radCheckBox_Vender);
            this.panel1.Controls.Add(this.radDropDownList_Vender);
            this.panel1.Controls.Add(this.radButton_Stop);
            this.panel1.Controls.Add(this.RadButton_Reset);
            this.panel1.Controls.Add(this.radButton_Start);
            this.panel1.Controls.Add(this.radGridView_Vender);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radCheckBox_Grp);
            this.panel1.Controls.Add(this.radDropDownList_Grp);
            this.panel1.Controls.Add(this.RadButton_SaveAX);
            this.panel1.Controls.Add(this.radCheckBox_Apv);
            this.panel1.Controls.Add(this.RadCheckBox_Date);
            this.panel1.Controls.Add(this.radDateTimePicker_D1);
            this.panel1.Controls.Add(this.radDateTimePicker_D2);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // radCheckBox_Vender
            // 
            this.radCheckBox_Vender.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Vender.Location = new System.Drawing.Point(12, 97);
            this.radCheckBox_Vender.Name = "radCheckBox_Vender";
            this.radCheckBox_Vender.Size = new System.Drawing.Size(103, 19);
            this.radCheckBox_Vender.TabIndex = 75;
            this.radCheckBox_Vender.Text = "ระบุผู้จำหน่าย";
            this.radCheckBox_Vender.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Vender_CheckStateChanged);
            // 
            // radDropDownList_Vender
            // 
            this.radDropDownList_Vender.DropDownAnimationEnabled = false;
            this.radDropDownList_Vender.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Vender.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Vender.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Vender.Location = new System.Drawing.Point(11, 122);
            this.radDropDownList_Vender.Name = "radDropDownList_Vender";
            this.radDropDownList_Vender.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Vender.TabIndex = 74;
            // 
            // radButton_Stop
            // 
            this.radButton_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Stop.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Stop.Location = new System.Drawing.Point(9, 466);
            this.radButton_Stop.Name = "radButton_Stop";
            this.radButton_Stop.Size = new System.Drawing.Size(175, 64);
            this.radButton_Stop.TabIndex = 73;
            this.radButton_Stop.Text = "หยุด (Auto)";
            this.radButton_Stop.ThemeName = "Fluent";
            this.radButton_Stop.Click += new System.EventHandler(this.RadButton_Stop_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Stop.GetChildAt(0))).Text = "หยุด (Auto)";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Stop.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Stop.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Stop.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Stop.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Stop.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Stop.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Reset
            // 
            this.RadButton_Reset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_Reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Reset.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Reset.Location = new System.Drawing.Point(9, 420);
            this.RadButton_Reset.Name = "RadButton_Reset";
            this.RadButton_Reset.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Reset.TabIndex = 63;
            this.RadButton_Reset.Text = "เริ่มใหม่ (Reset)";
            this.RadButton_Reset.ThemeName = "Fluent";
            this.RadButton_Reset.Click += new System.EventHandler(this.RadButton_Reset_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Reset.GetChildAt(0))).Text = "เริ่มใหม่ (Reset)";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Reset.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Reset.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Reset.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Reset.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Reset.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Reset.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Reset.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Start
            // 
            this.radButton_Start.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Start.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Start.Location = new System.Drawing.Point(9, 306);
            this.radButton_Start.Name = "radButton_Start";
            this.radButton_Start.Size = new System.Drawing.Size(175, 64);
            this.radButton_Start.TabIndex = 72;
            this.radButton_Start.Text = "เริ่ม (Auto)";
            this.radButton_Start.ThemeName = "Fluent";
            this.radButton_Start.Click += new System.EventHandler(this.RadButton_Start_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Start.GetChildAt(0))).Text = "เริ่ม (Auto)";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Start.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Start.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Start.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Start.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Start.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Start.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Start.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radGridView_Vender
            // 
            this.radGridView_Vender.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Vender.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Vender.Location = new System.Drawing.Point(9, 320);
            // 
            // 
            // 
            this.radGridView_Vender.MasterTemplate.ShowRowHeaderColumn = false;
            this.radGridView_Vender.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Vender.Name = "radGridView_Vender";
            // 
            // 
            // 
            this.radGridView_Vender.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Vender.Size = new System.Drawing.Size(176, 211);
            this.radGridView_Vender.TabIndex = 71;
            this.radGridView_Vender.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Vender_ViewCellFormatting);
            this.radGridView_Vender.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Vender_ConditionalFormattingFormShown);
            this.radGridView_Vender.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Vender_FilterPopupRequired);
            this.radGridView_Vender.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Vender_FilterPopupInitialized);
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel1.AutoSize = false;
            this.radLabel1.BackColor = System.Drawing.Color.Transparent;
            this.radLabel1.BorderVisible = true;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.ForeColor = System.Drawing.Color.Red;
            this.radLabel1.Location = new System.Drawing.Point(9, 537);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(177, 61);
            this.radLabel1.TabIndex = 70;
            this.radLabel1.Text = "สินค้าที่เกิน PO จะคิดเป็นสินค้าเพิ่ม จ/ซ นำไปจ่ายบิลไม่ได้ทุกกรณี (สินค้าแถมต้อง" +
    "แยกรายการก่อนทุกครั้ง)";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radCheckBox_Grp
            // 
            this.radCheckBox_Grp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Grp.Location = new System.Drawing.Point(12, 43);
            this.radCheckBox_Grp.Name = "radCheckBox_Grp";
            this.radCheckBox_Grp.Size = new System.Drawing.Size(80, 19);
            this.radCheckBox_Grp.TabIndex = 68;
            this.radCheckBox_Grp.Text = "ระบุจัดซื้อ";
            this.radCheckBox_Grp.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Grp_CheckStateChanged);
            // 
            // radDropDownList_Grp
            // 
            this.radDropDownList_Grp.DropDownAnimationEnabled = false;
            this.radDropDownList_Grp.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Grp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Grp.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Grp.Location = new System.Drawing.Point(11, 68);
            this.radDropDownList_Grp.Name = "radDropDownList_Grp";
            this.radDropDownList_Grp.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Grp.TabIndex = 67;
            this.radDropDownList_Grp.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDownList_Grp_SelectedIndexChanged);
            // 
            // RadButton_SaveAX
            // 
            this.RadButton_SaveAX.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_SaveAX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_SaveAX.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_SaveAX.Location = new System.Drawing.Point(9, 600);
            this.RadButton_SaveAX.Name = "RadButton_SaveAX";
            this.RadButton_SaveAX.Size = new System.Drawing.Size(176, 32);
            this.RadButton_SaveAX.TabIndex = 65;
            this.RadButton_SaveAX.Text = "อนุมัติเข้า AX";
            this.RadButton_SaveAX.ThemeName = "Fluent";
            this.RadButton_SaveAX.Click += new System.EventHandler(this.RadButton_SaveAX_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SaveAX.GetChildAt(0))).Text = "อนุมัติเข้า AX";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SaveAX.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_SaveAX.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SaveAX.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SaveAX.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SaveAX.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SaveAX.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_SaveAX.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radCheckBox_Apv
            // 
            this.radCheckBox_Apv.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox_Apv.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Apv.Location = new System.Drawing.Point(12, 152);
            this.radCheckBox_Apv.Name = "radCheckBox_Apv";
            this.radCheckBox_Apv.Size = new System.Drawing.Size(153, 19);
            this.radCheckBox_Apv.TabIndex = 64;
            this.radCheckBox_Apv.Text = "เฉพาะยังไม่ลงรายการ";
            this.radCheckBox_Apv.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // RadCheckBox_Date
            // 
            this.RadCheckBox_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Date.Location = new System.Drawing.Point(11, 177);
            this.RadCheckBox_Date.Name = "RadCheckBox_Date";
            this.RadCheckBox_Date.Size = new System.Drawing.Size(119, 19);
            this.RadCheckBox_Date.TabIndex = 63;
            this.RadCheckBox_Date.Text = "ระบุวันที่รับสินค้า";
            // 
            // radDateTimePicker_D1
            // 
            this.radDateTimePicker_D1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D1.Location = new System.Drawing.Point(11, 202);
            this.radDateTimePicker_D1.Name = "radDateTimePicker_D1";
            this.radDateTimePicker_D1.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_D1.TabIndex = 62;
            this.radDateTimePicker_D1.TabStop = false;
            this.radDateTimePicker_D1.Text = "24/05/2023";
            this.radDateTimePicker_D1.Value = new System.DateTime(2023, 5, 24, 0, 0, 0, 0);
            this.radDateTimePicker_D1.ValueChanged += new System.EventHandler(this.RadDateTimePicker_D1_ValueChanged);
            // 
            // radDateTimePicker_D2
            // 
            this.radDateTimePicker_D2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D2.Location = new System.Drawing.Point(11, 228);
            this.radDateTimePicker_D2.Name = "radDateTimePicker_D2";
            this.radDateTimePicker_D2.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_D2.TabIndex = 59;
            this.radDateTimePicker_D2.TabStop = false;
            this.radDateTimePicker_D2.Text = "24/05/2023";
            this.radDateTimePicker_D2.Value = new System.DateTime(2023, 5, 24, 0, 0, 0, 0);
            this.radDateTimePicker_D2.ValueChanged += new System.EventHandler(this.RadDateTimePicker_D2_ValueChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_add,
            this.commandBarSeparator1,
            this.radButtonElement_excel,
            this.commandBarSeparator4,
            this.RadButtonElement_pdt,
            this.commandBarSeparator5});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "Export To Excel";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(11, 258);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 0;
            this.RadButton_Search.Text = " ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = " ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // ProductRecive_DROID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "ProductRecive_DROID";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DROID";
            this.Load += new System.EventHandler(this.ProductRecive_DROID_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Stop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Reset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Vender.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Grp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Grp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_SaveAX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Apv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D1;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Date;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Apv;
        protected Telerik.WinControls.UI.RadButton RadButton_SaveAX;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Grp;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Grp;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Vender;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        protected Telerik.WinControls.UI.RadButton radButton_Start;
        protected Telerik.WinControls.UI.RadButton radButton_Stop;
        protected Telerik.WinControls.UI.RadButton RadButton_Reset;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Vender;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Vender;
    }
}
