﻿
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using System.Collections;
using PC_Shop24Hrs.FormShare;
using PC_Shop24Hrs.GeneralForm.FarmHouse;
using System.Drawing;
using System.Drawing.Printing;
using Telerik.WinControls.Data;

namespace PC_Shop24Hrs.GeneralForm.ProductRecive.DROID
{
    public partial class ProductRecive_DROID : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        DataTable dtVenderCheck = new DataTable();
        DataTable dtData = new DataTable();
        DataTable dtPrint = new DataTable();
        readonly DataTable dtPrintDT = new DataTable();
        double Print_sumAll;
        readonly int Print_numPage = 10;
        double Print_iPage, Print_PageALl;
        int Print_i;

        string staFactory;

        int tickLineNum;
        string tickSta, tickPO, tickInvoice;
        readonly DataTable dtTransLineNum = new DataTable();

        int cellLineNum;

        readonly string _pPermission;//0 ไม่สิดลงรายการบัญชี 1 สิดลงรายการบัญชี
        readonly string _pAutoRunPO;//0 ไม่ต้อง Run PO Auo 1 Run Auto

        readonly string _pTypeReport;
        //0 รับเข้าคลัง  
        //1 ออก Invoice ของโรงงาน 
        //2 เปรียบเทียบการรับส่งสินค้าระหว่างโรงงาน/คลัง A // 
        //3 ระบุ บรรทัด 
        //4 ระบบสินค้าโรงงานใหม่ แทน Type 1
        //5 เปรียบเทียบสินค้าโรงงาน ผลิต / ส่ง / รับ
        //6 รายงานสินค้าไม่มีน้ำหนัก
        //7 รายงานที่รับเข้าคลังแต่ไม่ลงรายการบัญชี
        //8 รายงานเช็ควันรับสินค้ากับวันที่รับเข้าคลัง

        //Load
        public ProductRecive_DROID(string pTypeReport, string permission, string pAutoRunPO)
        {
            InitializeComponent();

            _pPermission = permission;
            _pAutoRunPO = pAutoRunPO;
            _pTypeReport = pTypeReport;
        }
        //Load
        private void ProductRecive_DROID_Load(object sender, EventArgs e)
        {
            dtTransLineNum.Columns.Add("TRANSID");

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radStatusStrip1.SizingGrip = false;
            radCheckBox_Grp.Visible = false; radDropDownList_Grp.Visible = false;
            radCheckBox_Vender.Visible = false; radDropDownList_Vender.Visible = false;
            radCheckBox_Apv.Visible = false;
            RadCheckBox_Date.Visible = false;
            RadButton_Search.Visible = false;
            radGridView_Vender.Visible = false; radLabel1.Visible = false;
            radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
            RadButton_SaveAX.Visible = false;
            radButton_Start.Visible = false; radButton_Stop.Visible = false; RadButton_Reset.Visible = false;
            radButton_Start.ButtonElement.ShowBorder = true; radButton_Stop.ButtonElement.ShowBorder = true; RadButton_Reset.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Vender);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(radGridView_Vender);

            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Grp.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Vender.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Date.ButtonElement.Font = SystemClass.SetFontGernaral;

            if (_pTypeReport == "0")//0 รับเข้าคลัง 
            {
                radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true;
                radCheckBox_Vender.Visible = true; radDropDownList_Vender.Visible = true;
                radCheckBox_Apv.Visible = true;
                RadCheckBox_Date.Visible = true;
                RadButton_Search.Visible = true;
                //radGridView_Vender.Visible = true; 
                radLabel1.Visible = true;
                radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                RadButton_SaveAX.Visible = true;
                RadButton_SaveAX.Text = "อนุมัติเข้า AX";

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-5), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-5);


                RadCheckBox_Date.ReadOnly = true; RadCheckBox_Date.Checked = true;


                radDropDownList_Grp.DataSource = Models.DptClass.GetDpt_AllD();
                radDropDownList_Grp.ValueMember = "NUM";
                radDropDownList_Grp.DisplayMember = "DESCRIPTION";
                radDropDownList_Grp.SelectedValue = SystemClass.SystemDptID;

                if (radDropDownList_Grp.SelectedIndex == -1) radDropDownList_Grp.SelectedIndex = 0;


                if (_pPermission == "1")
                {
                    RadButton_SaveAX.Visible = true;
                    RadButton_SaveAX.ButtonElement.ShowBorder = true;
                    radCheckBox_Grp.Checked = false; radDropDownList_Grp.Enabled = false;
                }
                else
                {
                    RadButton_SaveAX.Visible = false; radCheckBox_Grp.Checked = true;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIMERECIVE", "เวลารับ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONTO", "คลังรับ", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTPO", "PO เปิดค้างไว้", 90)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่ใบสั่งซื้อ", 110)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTTRANSID", "รหัส LOT อ้างอิง AX", 130)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVE", "จำนวนรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVEALL", "รับทั้งหมด", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMPO", "สั่งทั้งหมด", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYBEFORERECIVE", "รับก่อนหน้า", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYDIFF", "ขาด-เกิน", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("QTYSCANDOC", "จำนวนในบิล")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEVICENAME", "เครื่อง", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDRECIVE", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMERECIVE", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONNAME", "ชื่อแผนกรับ", 220)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 350)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("REMARKDIFF", "หมายเหตุบิล")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SENDAXSTA", "บิล AX", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDSENDAX", "ผู้อนุมัติ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMESENDAX", "ชื่อผู้อนุมัติ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATETIMESENDAX", "วันที่อนุมัติ", 150)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTLOCATIONFROM", "คลังโอน")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CONFIGID", "CONFIGID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTSIZEID", "INVENTSIZEID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTCOLORID", "INVENTCOLORID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_ACTIVE", "STA_ACTIVE")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHBARCODE", "บาร์โค้ด PO", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PURCHFACTOR", "อัตราส่วน", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSID", "TRANSID", 180)));

                DatagridClass.SetCellBackClolorByExpression("PURCHID", "SENDAXSTA = '0' AND PURCHID <> '' AND SENDAXSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("PURCHID", "SENDAXSTA = '0' AND PURCHID = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "SENDAXSTA = '0' AND COUNTPO = '0' AND PURCHID = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("QTYRECIVEALL", "COUNTPO <> '0' AND QTYRECIVEALL <> SUMPO ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("QTYRECIVEALL", "PURCHID <> '' AND QTYRECIVEALL <> SUMPO ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("SUMPO", "PURCHID <> '' AND QTYRECIVEALL <> SUMPO ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("REMARK", "REMARK <> ''  ", ConfigClass.SetColor_GrayPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("QTYDIFF", "QTYDIFF <> 0  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("SPCITEMNAME", " STA_ACTIVE <> '0'  ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("QTYBEFORERECIVE", " QTYBEFORERECIVE <> '0'  ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

                RadGridView_ShowHD.Columns["DATERECIVE"].IsPinned = true;
                RadGridView_ShowHD.Columns["TIMERECIVE"].IsPinned = true;
                RadGridView_ShowHD.Columns["INVENTLOCATIONTO"].IsPinned = true;

                if (SystemClass.SystemComMinimart != "1")
                {
                    RadGridView_ShowHD.Columns["TRANSID"].IsVisible = false;
                    RadGridView_ShowHD.Columns["INVENTTRANSID"].IsVisible = false;
                }

                radLabel_Detail.Text = $@"สีแดง บาร์โค้ด >> สินค้าไม่มีใบ PO| สีแดง เลขที่ใบสั่งซื้อ >> ยังไม่ระบุ PO | สีแดง จำนวนรับ >> จำนวนใน PO ไม่เท่ากับจำนวนรับ | สีม่วง >> ระบุ PO แต่ยังไม่ส่งเข้า AX | สีฟ้า >> มีผลกับการออกใบแจ้งหนี้ | สีเขียว รับก่อนหน้า >> มีจำนวนรับแล้ว{Environment.NewLine}DoubleClick PO เปิดค้างไว้ >> ลบ PO ที่เลือกไว้| DoubleClick เลขที่ใบสั่งซื้อ >> เลือก PO | DoubleClick จำนวนรับ >> แก้ไขจำนวนรับเข้าคลัง [เฉพาะคลังเท่านั้น] | DoubleClick หมายเหตุ >> แก้ไขหมายเหตุ";

                //if (SystemClass.SystemComProgrammer == "1") RadButton_SaveAX.Enabled = true; else RadButton_SaveAX.Enabled = false;
                if (_pPermission == "1") { RadButton_SaveAX.Enabled = true; } else RadButton_SaveAX.Enabled = false;

                dtVenderCheck = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("57", "", "  ORDER BY REMARK ", "");

                //radGridView_Vender.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "จซ", 50)));
                //radGridView_Vender.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 70)));
                //radGridView_Vender.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ชื่อผู้จำหน่าย", 300)));
                //radGridView_Vender.Columns["REMARK"].IsPinned = true;
                //radGridView_Vender.DataSource = dtVenderCheck;

            }// 0 รับเข้าคลัง 

            if (_pTypeReport == "1")// 1 ออก Invoice ของโรงงาน
            {
                dtPrintDT.Columns.Add("ITEMBARCODE");
                dtPrintDT.Columns.Add("SPCITEMNAME");
                dtPrintDT.Columns.Add("QTYRECIVE");
                dtPrintDT.Columns.Add("UNITID");
                dtPrintDT.Columns.Add("COST_PRICE");
                dtPrintDT.Columns.Add("COST_ALL");
                dtPrintDT.Columns.Add("Print_i");

                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-1), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now);

                RadButton_SaveAX.Text = "บันทึกออกบิล";
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);

                RadCheckBox_Date.ReadOnly = true; RadCheckBox_Date.Checked = true;

                if (_pPermission == "1")
                {
                    RadButton_SaveAX.Visible = true;
                    RadButton_SaveAX.ButtonElement.ShowBorder = true;
                    radCheckBox_Grp.Checked = false; radDropDownList_Grp.Enabled = false;
                }
                else
                {
                    RadButton_SaveAX.Visible = false; radCheckBox_Grp.Checked = true;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่บิลส่งของ", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVE", "จำนวนรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COST_PRICE", "ต้นทุน", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COST_ALL", "ยอดรวม", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("QTY", "QTY")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTLOCATIONTO", "INVENTLOCATIONTO")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_COST", "STA_COST")));

                GridViewSummaryRowItem summaryRowItemCase7 = new GridViewSummaryRowItem
                {
                    new GridViewSummaryItem("COST_ALL", "{0:#,##0.00}", GridAggregateFunction.Sum)
                };
                RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemCase7);
                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                RadGridView_ShowHD.Columns["DATERECIVE"].IsPinned = true;
                RadGridView_ShowHD.Columns["PURCHID"].IsPinned = true;

                DatagridClass.SetCellBackClolorByExpression("COST_PRICE", "COST_PRICE = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("COST_ALL", "COST_ALL = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "STA_COST = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                radLabel_Detail.Text = $@"สีแดง ช่อง ต้นทุน >> โรงงานยังไม่ระบุราคาทุน | สีม่วง ช่อง บาร์โค้ด >> สินค้าไม่ได้ยิงออกจากโรงงาน | สินค้าต้องที่จะบันทึกออกบิลได้เมื่อต้นทุนเรียบร้อย และ ส่งเข้า AX ทั้งหมดแล้ว{Environment.NewLine}Double Click รายการ >> พิมพ์เอกสารการรับทั้งหมด (เป็นสลิป)";

                if (_pPermission == "1") RadButton_SaveAX.Enabled = true; else RadButton_SaveAX.Enabled = false;

            }// 1 ออก Invoice ของโรงงาน 

            if (_pTypeReport == "2")
            {
                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;
                radDateTimePicker_D2.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-1), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);

                RadCheckBox_Date.ReadOnly = true; RadCheckBox_Date.Checked = true;

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่ส่ง-รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STA_TYPE", "ส่งจาก", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE_SEND", "บาร์โค้ดส่ง/รับ", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMBARCODE_RECIVE", "บาร์โค้ดรับ")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_SEND", "จำนวนส่ง", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID_SEND", "หน่วยส่ง", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("FACTOR_SEND", "FACTOR_SEND")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("SUM_SEND", "SUM_SEND")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_RECIVE", "จำนวนรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID_RECIVE", "หน่วยรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("FACTOR_RECIVE", "FACTOR_RECIVE")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("SUM_RECIVE", "SUM_RECIVE")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM_DIFF", "จำนวนต่าง (ชิ้น)", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "รูปรับสินค้า", 250));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));

                RadGridView_ShowHD.Columns["TRANSDATE"].IsPinned = true;
                RadGridView_ShowHD.Columns["BUYERID"].IsPinned = true;

                DatagridClass.SetCellBackClolorByExpression("QTY_SEND", "QTY_SEND = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("UNITID_SEND", "QTY_SEND = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("STA_TYPE", "QTY_SEND = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("QTY_RECIVE", "QTY_RECIVE = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("UNITID_RECIVE", "QTY_RECIVE = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("SUM_DIFF", "SUM_DIFF < '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("SUM_DIFF", "SUM_DIFF > '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                radLabel_Detail.Text = $@"สีแดง ช่อง บาร์โค้ดส่ง >> โรงงานไม่ได้บันทึกส่งของ | สีแดง ช่อง จำนวนรับ >> คลังไม่ได้บันทึกรับของ |  สีแดง ช่อง จำนวนต่าง (ชิ้น) >> ขาด-เกิน สินค้าส่ง-รับ {Environment.NewLine} Double Click รูป >> เพื่อดูรูปสินค้าขนาดใหญ่";

                RadGridView_ShowHD.TableElement.RowHeight = 150;
                RadGridView_ShowHD.TableElement.TableHeaderHeight = 50;
            }// 2 เปรียบเทียบการรับส่งสินค้าระหว่างโรงงาน/คลัง A 

            if (_pTypeReport == "3")//3 run line
            {
                radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true;
                radCheckBox_Vender.Visible = true; radDropDownList_Vender.Visible = true;
                radCheckBox_Apv.Visible = true;
                RadCheckBox_Date.Visible = true;
                RadButton_Search.Visible = true;

                radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                RadButton_SaveAX.Visible = false; RadButton_SaveAX.Text = "ส่งบรรทัดเข้า AX";
                radButton_Start.Visible = true; radButton_Stop.Visible = true; RadButton_Reset.Visible = true;
                radButton_Start.Enabled = true; radButton_Stop.Enabled = false; RadButton_Reset.Enabled = false;
                RadButton_Search.ButtonElement.ShowBorder = true;


                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-5), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-5);

                RadCheckBox_Date.ReadOnly = true; RadCheckBox_Date.Checked = true;

                radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true;
                radDropDownList_Grp.DataSource = Models.DptClass.GetDpt_AllD();
                radDropDownList_Grp.ValueMember = "NUM";
                radDropDownList_Grp.DisplayMember = "DESCRIPTION";
                radDropDownList_Grp.SelectedValue = SystemClass.SystemDptID;

                if (radDropDownList_Grp.SelectedIndex == -1) radDropDownList_Grp.SelectedIndex = 0;


                if (_pPermission == "1")
                {
                    RadButton_SaveAX.Visible = true;
                    RadButton_SaveAX.ButtonElement.ShowBorder = true;
                    radCheckBox_Grp.Checked = false; radDropDownList_Grp.Enabled = false;
                }
                else
                {
                    RadButton_SaveAX.Visible = false; radCheckBox_Grp.Checked = true;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", $@"เลขที่บิล{Environment.NewLine}ผู้จำหน่าย", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINENUM", $@"ลำดับ{Environment.NewLine}บรรทัด", 80)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่ใบสั่งซื้อ", 110)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("INVENTTRANSID", "LOT", 150)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVE", "จำนวนรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYDIFF", "ขาด-เกิน", 80)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIMERECIVE", "เวลารับ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONTO", "คลังรับ", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 150)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TRANSID", "TRANSID")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEVICENAME", "เครื่อง", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDRECIVE", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMERECIVE", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONNAME", "ชื่อแผนกรับ", 220)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 350)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SENDAXSTA", "บิล AX", 140)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("SENDLINENUM", "สถานะลงรายการ", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDSENDLINENUM", "ผู้ลงรายการ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMESENDLINENUM", "ชื่อผู้ลงรายการ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATETIMESENDLINENUM", "วันที่ลงรายการ", 150)));

                DatagridClass.SetCellBackClolorByExpression("QTYDIFF", "QTYDIFF <> 0  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                DatagridClass.SetCellBackClolorByExpression("INVOICEID", " INVOICEID = ''  ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("LINENUM", " LINENUM = '0'  ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);

                RadGridView_ShowHD.Columns["LINENUM"].IsPinned = true;
                RadGridView_ShowHD.Columns["INVOICEID"].IsPinned = true;

                if (SystemClass.SystemComMinimart != "1") RadGridView_ShowHD.Columns["INVENTTRANSID"].IsVisible = false;

                RadGridView_ShowHD.TableElement.RowHeight = 60;
                RadGridView_ShowHD.TableElement.TableHeaderHeight = 60;

                SetReadOnly(false);

                radLabel_Detail.Text = $@"แสดงเฉพาะรายการที่ผูก PO แล้วเท่านั้น | สีเหลือง >> ยังไม่ระบุเลขที่บิลและบรรทัด | DoubleClick เลขที่บิล >> ระบุเลขที่บิลผู้จำหน่าย | DoubleClick ลำดับ >> เปลี่ยนแปลงลำดับโดยการระบุ[มีบิลผู้จำหน่ายแล้ว] {Environment.NewLine}กรณีใช้งานติ๊ก Auto >> กด เริ่มแล้วติ๊กในข่อง ลำดับบรรทัด เพื่อระบุจำนวน Auto และกด หยุด เมื่อระบุบรรทัดเรียบร้อย เพื่อระบุเลขที่บิลผู้จำหน่าย ";

                if (_pPermission == "1") RadButton_SaveAX.Enabled = true; else RadButton_SaveAX.Enabled = false;

            }// 3 ระบุ บรรทัด

            if (_pTypeReport == "4") // 4 ระบบสินค้าโรงงานใหม่ แทน Type 1
            {
                dtPrintDT.Columns.Add("ITEMBARCODE");
                dtPrintDT.Columns.Add("SPCITEMNAME");
                dtPrintDT.Columns.Add("QTYRECIVE");
                dtPrintDT.Columns.Add("UNITID");
                dtPrintDT.Columns.Add("COST_PRICE");
                dtPrintDT.Columns.Add("COST_ALL");
                dtPrintDT.Columns.Add("Print_i");

                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.Enabled = false;

                radCheckBox_Vender.Visible = true; radDropDownList_Vender.Visible = true;

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(0), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(0), DateTime.Now);

                RadButton_SaveAX.Text = "บันทึกออกบิล";
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);

                RadCheckBox_Date.ReadOnly = true; RadCheckBox_Date.Checked = true;

                if (_pPermission == "1")//เปลี่ยนให้มาอยู่ในมือ จัดซื้อ แทน
                {
                    RadButton_SaveAX.Visible = true;
                    RadButton_SaveAX.ButtonElement.ShowBorder = true;
                    radCheckBox_Grp.Checked = false; radDropDownList_Grp.Enabled = false;
                    radCheckBox_Vender.Checked = true; radCheckBox_Vender.Enabled = false;
                }
                else
                {
                    RadButton_SaveAX.Visible = false; radCheckBox_Grp.Checked = true;
                    radCheckBox_Vender.Enabled = true;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPR", "เลขที่บิล ใบแจ้งหนี้", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BA", "เลขที่บิล BA", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่บิลส่งของ", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVE", "จำนวนรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COST_PRICE", "ต้นทุน", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COST_ALL", "ยอดรวม", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("QTY", "QTY")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTLOCATIONTO", "INVENTLOCATIONTO")));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_COST", "STA_COST")));

                GridViewSummaryRowItem summaryRowItemCase7 = new GridViewSummaryRowItem
                {
                    new GridViewSummaryItem("COST_ALL", "{0:#,##0.00}", GridAggregateFunction.Sum)
                };
                RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemCase7);
                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                DatagridClass.SetCellBackClolorByExpression("COST_PRICE", "COST_PRICE = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("COST_ALL", "COST_ALL = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "STA_COST = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                radLabel_Detail.Text = $@"สีแดง ช่อง ต้นทุน >> โรงงานยังไม่ระบุราคาทุน | สีม่วง ช่อง บาร์โค้ด >> สินค้าไม่ได้ยิงออกจากโรงงาน | สินค้าต้องที่จะบันทึกออกบิลได้เมื่อต้นทุนเรียบร้อย และ ส่งเข้า AX ทั้งหมดแล้ว{Environment.NewLine}Double Click รายการ >> พิมพ์เอกสารการรับทั้งหมด (เป็นสลิป)";

                if (_pPermission == "1") RadButton_SaveAX.Enabled = true; else RadButton_SaveAX.Enabled = false;
            }// 4 ระบบสินค้าโรงงานใหม่ แทน Type 1

            if (_pTypeReport == "5") { FactoryAll("0"); }//5 เปรียบเทียบสินค้าโรงงาน ผลิต / ส่ง / รับ

            if (_pTypeReport == "6") { ReportNoWeight("0"); }//รายงานสินค้าไม่มีน้ำหนัก

            if (_pTypeReport == "7") { ReportNotApv("0"); ReportNotApv("1"); }//7 รายงานที่รับเข้าคลังแต่ไม่ลงรายการบัญชี

            if (_pTypeReport == "8") { ReportDateDiffReciveInventWithBA("0"); } //8 รายงานเช็ควันรับสินค้ากับวันที่รับเข้าคลัง

        }

        void ClearTxt()
        {
            if (_pTypeReport == "0")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-5);
                radDateTimePicker_D2.Value = DateTime.Now;
                if (dtData.Rows.Count > 0) dtData.Rows.Clear();
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
            }
            if (_pTypeReport == "1")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                if (dtData.Rows.Count > 0) dtData.Rows.Clear();
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
            }
            if (_pTypeReport == "2")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                if (dtData.Rows.Count > 0) dtData.Rows.Clear();
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
            }
            if (_pTypeReport == "4")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                if (dtData.Rows.Count > 0) dtData.Rows.Clear();
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();

                radDropDownList_Vender.Enabled = true;
                radDateTimePicker_D1.Enabled = true;
            }
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Vender_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Vender_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Vender_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Vender_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        #region "Fucntion"
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (_pTypeReport == "0") FindData();//0 รับเข้าคลัง 

            if (_pTypeReport == "1") FactoryRecive("0");// 1 ออก Invoice ของโรงงาน 

            if (_pTypeReport == "2") FactoryReciveCheck();// 2 เปรียบเทียบการรับส่งสินค้าระหว่างโรงงาน/คลัง A 

            if (_pTypeReport == "3") FindDataLineNum();// 3 ระบุ บรรทัด

            if (_pTypeReport == "4") FactoryReciveType4("0"); // 4 ระบบสินค้าโรงงานใหม่ แทน Type 1

            if (_pTypeReport == "5") FactoryAll("1"); //5 เปรียบเทียบสินค้าโรงงาน ผลิต / ส่ง / รับ

            if (_pTypeReport == "6") ReportNoWeight("1"); //6 สินค้าที่รับเข้าไม่มีน้ำหนัก/ปริมาตร

            if (_pTypeReport == "7") ReportNotApv("1");//7 รายงานที่รับเข้าคลังแต่ไม่ลงรายการบัญชี

            if (_pTypeReport == "8") ReportDateDiffReciveInventWithBA("1"); //8 รายงานเช็ควันรับสินค้ากับวันที่รับเข้าคลัง
        }
        //เปรียบเทียบสินค้า ส่ง/รับ ของสินค้าโรงงาน
        void FactoryReciveCheck()
        {
            this.Cursor = Cursors.WaitCursor;
            dtData = ProductRecive_Class.GetDatailFactory("2", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
            RadGridView_ShowHD.DataSource = dtData;
            dtData.AcceptChanges();
            if (dtData.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                this.Cursor = Cursors.Default;
                return;
            }
            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                string pPath = PathImageClass.pImageEmply;
                if (RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE_RECIVE"].Value.ToString() != "")
                {
                    DataTable dtImage = ProductRecive_Class.GetDatailFactory("3", RadGridView_ShowHD.Rows[i].Cells["TRANSDATE"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE_RECIVE"].Value.ToString());
                    if (dtImage.Rows.Count != 0) pPath = dtImage.Rows[0]["PATH"].ToString();
                }
                RadGridView_ShowHD.Rows[i].Cells["IMG1"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pPath);
                RadGridView_ShowHD.Rows[i].Cells["P_IMG1"].Value = pPath;
            }
            this.Cursor = Cursors.Default;
        }
        //รับสินค้าเข้าคลังโรงงาน
        void FactoryRecive(string pCase)
        {
            if (pCase == "0")
            {
                this.Cursor = Cursors.WaitCursor;
                dtData = ProductRecive.ProductRecive_Class.GetDatailFactory("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), "");
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }
                if (dtData.Rows[0]["PURCHID"].ToString() != "") RadButton_SaveAX.Enabled = false; else RadButton_SaveAX.Enabled = true;

                this.Cursor = Cursors.Default;
            }
            if (pCase == "1")
            {
                this.Cursor = Cursors.WaitCursor;
                int iRow0_Cost = 0;
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["COST_ALL"].Value.ToString() == "0") iRow0_Cost++;
                }

                if (iRow0_Cost != 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ยังมีรายการที่ต้นทุนสินค้าเป็น 0{Environment.NewLine}จัดการให้เรียบร้อยก่อนออกบิล");
                    this.Cursor = Cursors.Default;
                    return;
                }

                DataTable dtAx = ProductRecive.ProductRecive_Class.GetDatailFactory("1", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), "");
                int iRow0_AX = 0;
                if (dtAx.Rows.Count > 0) iRow0_AX = int.Parse(dtAx.Rows[0]["NOTSANDAX"].ToString());

                if (iRow0_AX != 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ยังมีรายการที่ลงรายการบัญชี เข้า AX ไม่เรียบร้อย{Environment.NewLine}จัดการให้เรียบร้อยก่อนออกบิล");
                    this.Cursor = Cursors.Default;
                    return;
                }
                //ออกบิล
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการออกบิลทั้งหมด{Environment.NewLine}ไม่สามารถแก้ไขรายการได้อีก?") == DialogResult.No)
                {
                    this.Cursor = Cursors.Default; return;
                }
                //


                this.RadGridView_ShowHD.SortDescriptors.Expression = "VENDID,BUYERID ASC";
                DataTable dtVenderHD = new DataTable();
                dtVenderHD.Columns.Add("VENDID");
                dtVenderHD.Columns.Add("VENDNAME");
                dtVenderHD.Columns.Add("BUYERID");
                dtVenderHD.Columns.Add("BUYERNAME");

                DataTable dtItem = new DataTable();
                dtItem.Columns.Add("VENDID");
                dtItem.Columns.Add("BUYERID");

                dtItem.Columns.Add("ITEMID");
                dtItem.Columns.Add("INVENTDIMID");
                dtItem.Columns.Add("ITEMBARCODE");
                dtItem.Columns.Add("SPCITEMNAME");
                dtItem.Columns.Add("QTY");
                dtItem.Columns.Add("QTYRECIVE");
                dtItem.Columns.Add("UNITID");
                dtItem.Columns.Add("COST_PRICE");
                dtItem.Columns.Add("COST_ALL");

                string venderCheck = "", buyerIDCheck = "";
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    string venderID = RadGridView_ShowHD.Rows[i].Cells["VENDID"].Value.ToString();
                    string buyerID = RadGridView_ShowHD.Rows[i].Cells["BUYERID"].Value.ToString();
                    if (venderCheck != venderID || buyerIDCheck != buyerID)
                    {
                        DataRow[] dr = dtVenderHD.Select($@" VENDID = '{venderID}' AND BUYERID = '{buyerID}'  ");
                        if (dr.Length == 0)
                        {
                            dtVenderHD.Rows.Add(venderID, RadGridView_ShowHD.Rows[i].Cells["VENDNAME"].Value.ToString(), buyerID, RadGridView_ShowHD.Rows[i].Cells["BUYERNAME"].Value.ToString());
                        }
                    }

                    dtItem.Rows.Add(venderID, buyerID,
                        RadGridView_ShowHD.Rows[i].Cells["ITEMID"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["INVENTDIMID"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["SPCITEMNAME"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["QTY"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["QTYRECIVE"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["UNITID"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["COST_PRICE"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["COST_ALL"].Value.ToString());

                    venderCheck = venderID;
                    buyerIDCheck = buyerID;
                }

                if (dtVenderHD.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีรายการในการออกบิล{Environment.NewLine}ลองใหม่อีกครั้ง"); this.Cursor = Cursors.Default; return;
                }

                ArrayList sql = new ArrayList();

                for (int iR = 0; iR < dtVenderHD.Rows.Count; iR++)
                {
                    string vID = dtVenderHD.Rows[iR]["VENDID"].ToString();
                    string bID = dtVenderHD.Rows[iR]["BUYERID"].ToString();
                    DataRow[] dr = dtItem.Select($@" VENDID = '{vID}' AND BUYERID = '{bID}' ");

                    string Docno = Class.ConfigClass.GetMaxINVOICEID("MNIV", "-", "MNIV", "1");

                    for (int iC = 0; iC < dr.Length; iC++)
                    {
                        Var_VENDERPR var_VENDERPR = new Var_VENDERPR
                        {
                            ItemID = dr[iC]["ITEMID"].ToString(),
                            InventItem = dr[iC]["INVENTDIMID"].ToString(),
                            ITEMBARCODE = dr[iC]["ITEMBARCODE"].ToString(),
                            SPC_ITEMNAME = dr[iC]["SPCITEMNAME"].ToString(),
                            QTY_EXCEL = Double.Parse(dr[iC]["QTY"].ToString()),
                            QTY_CN = Double.Parse(dr[iC]["COST_ALL"].ToString()),
                            UNITQTY = dr[iC]["QTY"].ToString(),
                            PURCHUNIT = dr[iC]["UNITID"].ToString(),
                            INVOICE = Docno,
                            PRICECOST = double.Parse(dr[iC]["COST_PRICE"].ToString())
                        };
                        sql.Add(FarmHouse_Class.FarmHouse_SaveVenderPR(vID, "MN000", iC + 1, Docno, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), var_VENDERPR));
                    }
                }

                string resualt = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                this.Cursor = Cursors.Default;
                if (resualt == "")
                { FactoryRecive("0"); RadButton_SaveAX.Enabled = false; }
            }
        }
        //รับสินค้าเข้าคลังโรงงาน  // 4 ระบบสินค้าโรงงานใหม่ แทน Type 1
        void FactoryReciveType4(string pCase)
        {
            if (pCase == "0")
            {
                this.Cursor = Cursors.WaitCursor;
                string venderID = "";
                if (radCheckBox_Vender.Checked == true) venderID = radDropDownList_Vender.SelectedValue.ToString();

                dtData = ProductRecive_Class.GetDatailFactory("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), venderID);
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();

                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }

                string invoice = "";
                string ba = "";
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    string purchID = RadGridView_ShowHD.Rows[i].Cells["PURCHID"].Value.ToString();
                    if (purchID != "")
                    {
                        if (invoice != purchID)
                        {
                            DataTable dtBA = PurchClass.FarmHouse_GetBA708(purchID);
                            if (dtBA.Rows.Count > 0) ba = dtBA.Rows[0]["NUMRECEIVE"].ToString(); else ba = "";
                        }
                    }
                    else ba = "";
                    RadGridView_ShowHD.Rows[i].Cells["BA"].Value = ba;
                    invoice = purchID;
                }

                //staFactory
                if (_pPermission == "1")
                {
                    if (RadGridView_ShowHD.Rows[0].Cells["MNPR"].Value.ToString() != "") RadButton_SaveAX.Enabled = false;
                    else
                    {
                        if (RadGridView_ShowHD.Rows[0].Cells["PURCHID"].Value.ToString() != "")
                        {
                            if (RadGridView_ShowHD.Rows[0].Cells["BA"].Value.ToString() != "")
                            {
                                RadButton_SaveAX.Enabled = true; RadButton_SaveAX.Text = "สร้างใบแจ้งหนี้"; staFactory = "2";
                            }
                            else RadButton_SaveAX.Enabled = false;
                        }
                        else
                        {
                            RadButton_SaveAX.Enabled = true;
                            RadButton_SaveAX.Text = "ยืนยันรายการ"; staFactory = "1";
                        }
                    }
                }
                radDropDownList_Vender.Enabled = false;
                radDateTimePicker_D1.Enabled = false;
                this.Cursor = Cursors.Default;
            }//ค้นหา

            if (pCase == "1")//ออกบิล เพื่อไปทำ BA
            {
                this.Cursor = Cursors.WaitCursor;
                int iRow0_Cost = 0;
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["COST_ALL"].Value.ToString() == "0") iRow0_Cost++;
                }

                if (iRow0_Cost != 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ยังมีรายการที่ต้นทุนสินค้าเป็น 0{Environment.NewLine}จัดการให้เรียบร้อยก่อนออกบิล");
                    this.Cursor = Cursors.Default;
                    return;
                }

                string venderSelectID = "";
                if (radCheckBox_Vender.Checked == true) venderSelectID = radDropDownList_Vender.SelectedValue.ToString();

                //ออกบิล
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการออกบิลทั้งหมด{Environment.NewLine}ไม่สามารถแก้ไขรายการได้อีก?") == DialogResult.No)
                {
                    this.Cursor = Cursors.Default; return;
                }

                this.RadGridView_ShowHD.SortDescriptors.Expression = "VENDID,BUYERID ASC";
                DataTable dtVenderHD = new DataTable();
                dtVenderHD.Columns.Add("VENDID");
                dtVenderHD.Columns.Add("VENDNAME");
                dtVenderHD.Columns.Add("BUYERID");
                dtVenderHD.Columns.Add("BUYERNAME");

                DataTable dtItem = new DataTable();
                dtItem.Columns.Add("VENDID");
                dtItem.Columns.Add("BUYERID");

                dtItem.Columns.Add("ITEMID");
                dtItem.Columns.Add("INVENTDIMID");
                dtItem.Columns.Add("ITEMBARCODE");
                dtItem.Columns.Add("SPCITEMNAME");
                dtItem.Columns.Add("QTY");
                dtItem.Columns.Add("QTYRECIVE");
                dtItem.Columns.Add("UNITID");
                dtItem.Columns.Add("COST_PRICE");
                dtItem.Columns.Add("COST_ALL");

                string venderCheck = "", buyerIDCheck = "";
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    string venderID = RadGridView_ShowHD.Rows[i].Cells["VENDID"].Value.ToString();
                    string buyerID = RadGridView_ShowHD.Rows[i].Cells["BUYERID"].Value.ToString();
                    if (venderCheck != venderID || buyerIDCheck != buyerID)
                    {
                        DataRow[] dr = dtVenderHD.Select($@" VENDID = '{venderID}' AND BUYERID = '{buyerID}'  ");
                        if (dr.Length == 0)
                        {
                            dtVenderHD.Rows.Add(venderID, RadGridView_ShowHD.Rows[i].Cells["VENDNAME"].Value.ToString(), buyerID, RadGridView_ShowHD.Rows[i].Cells["BUYERNAME"].Value.ToString());
                        }
                    }

                    dtItem.Rows.Add(venderID, buyerID,
                        RadGridView_ShowHD.Rows[i].Cells["ITEMID"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["INVENTDIMID"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["SPCITEMNAME"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["QTY"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["QTYRECIVE"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["UNITID"].Value.ToString(),
                        RadGridView_ShowHD.Rows[i].Cells["COST_PRICE"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["COST_ALL"].Value.ToString());

                    venderCheck = venderID;
                    buyerIDCheck = buyerID;
                }

                if (dtVenderHD.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีรายการในการออกบิล{Environment.NewLine}ลองใหม่อีกครั้ง"); this.Cursor = Cursors.Default; return;
                }

                ArrayList sql = new ArrayList();

                for (int iR = 0; iR < dtVenderHD.Rows.Count; iR++)
                {
                    string vID = dtVenderHD.Rows[iR]["VENDID"].ToString();
                    string bID = dtVenderHD.Rows[iR]["BUYERID"].ToString();
                    DataRow[] dr = dtItem.Select($@" VENDID = '{vID}' AND BUYERID = '{bID}' ");

                    string Docno = ConfigClass.GetMaxINVOICEID("MNIV", "-", "MNIV", "1");

                    for (int iC = 0; iC < dr.Length; iC++)
                    {
                        Var_VENDERPR var_VENDERPR = new Var_VENDERPR
                        {
                            ItemID = dr[iC]["ITEMID"].ToString(),
                            InventItem = dr[iC]["INVENTDIMID"].ToString(),
                            ITEMBARCODE = dr[iC]["ITEMBARCODE"].ToString(),
                            SPC_ITEMNAME = dr[iC]["SPCITEMNAME"].ToString(),
                            QTY_EXCEL = Double.Parse(dr[iC]["QTY"].ToString()),
                            QTY_CN = Double.Parse(dr[iC]["COST_ALL"].ToString()),
                            UNITQTY = dr[iC]["QTY"].ToString(),
                            PURCHUNIT = dr[iC]["UNITID"].ToString(),
                            INVOICE = Docno,
                            PRICECOST = double.Parse(dr[iC]["COST_PRICE"].ToString())
                        };
                        sql.Add(FarmHouse_Class.FarmHouse_SaveVenderPR(vID, "MN000", iC + 1, Docno, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), var_VENDERPR));
                    }
                }

                string resualt = AX_SendData.Save_SPC_INVENTJOURNALTABLE_TYPE2_Factory(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), sql, venderSelectID);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                this.Cursor = Cursors.Default;
                if (resualt == "")
                { FactoryReciveType4("0"); RadButton_SaveAX.Enabled = false; }

            }//ออกบิล เพื่อไปทำ BA

            if (pCase == "2")//ออกใบแจ้งหนี้
            {
                ArrayList sqlAX = new ArrayList();
                ArrayList sql24 = new ArrayList();

                string DateInput = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
                string Docno = Class.ConfigClass.GetMaxINVOICEID("MNPR", "-", "MNPR", "1");
                //PURCHSTATUS-- 3 อัพเดทใบแจ้งหนี้   1 เปิดค้างไว้
                Var_PURCHTABLE var_PURCHTABLE = new Var_PURCHTABLE()
                {
                    PURCHASETYPE = "3",
                    PURCHSTATUS = "3",//**3
                    TAXINVOICE = "0",
                    INCLTAX = "0",
                    PURCHID = Docno,
                    PURCHDATE = DateInput,
                    ORDERACCOUNT = radDropDownList_Vender.SelectedValue.ToString(),
                    INVENTLOCATIONID = "WH-CENTER",
                    DIMENSION = SystemClass.SystemDptID,
                    REMARK = "",
                    INVOICEID = RadGridView_ShowHD.Rows[0].Cells["PURCHID"].Value.ToString(),
                    INVOICEDATE = DateInput,
                    VENDORREF = RadGridView_ShowHD.Rows[0].Cells["PURCHID"].Value.ToString(),
                    NUMRECEIVE = RadGridView_ShowHD.Rows[0].Cells["BA"].Value.ToString()
                };

                sqlAX.Add(AX_SendData.Update_ShipCarrierInvoice(var_PURCHTABLE.INVOICEID, var_PURCHTABLE.NUMRECEIVE, var_PURCHTABLE.PURCHID));
                sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE(var_PURCHTABLE));
                sql24.Add($@"
                    UPDATE  SHOP_VENDERPR
                    SET     PD = '{Docno}'
                    WHERE   PURCHID = '{RadGridView_ShowHD.Rows[0].Cells["PURCHID"].Value}' ");

                int LineAX = 0;
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    LineAX += 1;
                    Var_PURCHLINE var_PURCHLINE = new Var_PURCHLINE
                    {
                        PURCHID = Docno,
                        PURCHDATE = DateInput,
                        LINENUM = LineAX,
                        BARCODE = RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                        NAME = RadGridView_ShowHD.Rows[i].Cells["SPCITEMNAME"].Value.ToString(),
                        REMARK = "",
                        PURCHQTY = double.Parse(RadGridView_ShowHD.Rows[i].Cells["QTYRECIVE"].Value.ToString()),
                        PURCHPRICE = double.Parse(RadGridView_ShowHD.Rows[i].Cells["COST_PRICE"].Value.ToString()),
                        MULTIDISCTXT = ""
                    };
                    sqlAX.Add(AX_SendData.SaveDT_PURCHLINE(var_PURCHLINE));
                }

                string resualt = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                this.Cursor = Cursors.Default;
                if (resualt == "")
                { FactoryReciveType4("0"); RadButton_SaveAX.Enabled = false; }

            }//ออกใบแจ้งหนี้
        }
        //5 เปรียบเทียบสินค้าโรงงาน ผลิต / ส่ง / รับ
        void FactoryAll(string pCase)
        {
            if (pCase == "0")
            {
                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;
                radDateTimePicker_D2.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-1), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);

                radCheckBox_Apv.Text = "ตามวันที่"; radCheckBox_Apv.Checked = true; radCheckBox_Apv.Visible = true;

                RadCheckBox_Date.ReadOnly = true; RadCheckBox_Date.Checked = true;

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่ผลิต-ส่ง-รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 180)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 400)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COST", "ต้นทุนปัจจุบัน", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PRICE", "ราคาขายเงินสด", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_MAKE", "จำนวนโรงงานผลิต", 110)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_SEND", "จำนวนโรงงานส่งคลัง", 130)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_RECIVE", "จำนวนคลังรับ", 110)));

                RadGridView_ShowHD.Columns["TRANSDATE"].IsPinned = true;
                RadGridView_ShowHD.Columns["BUYERID"].IsPinned = true;

                DatagridClass.SetCellBackClolorByExpression("COST", "COST = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("PRICE", "PRICE = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                GridViewSummaryItem summaryItemA = new GridViewSummaryItem("QTY_MAKE", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemB = new GridViewSummaryItem("QTY_SEND", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemC = new GridViewSummaryItem("QTY_RECIVE", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC };
                RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

                radLabel_Detail.Text = $@"ข้อมูลบาร์โค้ดที่แสดงผล คือ หน่วยย่อยเท่านั้น | ข้อมูลจำนวนทั้งหมด คือ หน่วยย่อยเท่านั้น{Environment.NewLine}ต้นทุนที่แสดงผล คือ ต้นทุนปัจจุบันตามหน่วยย่อย | ราคาขายเงินสด คือ ราคาขายปัจจุบันเงินสด";

                GroupDescriptor descriptor = new GroupDescriptor();
                descriptor.GroupNames.Add("BUYERID", System.ComponentModel.ListSortDirection.Ascending);
                descriptor.GroupNames.Add("BUYERNAME", System.ComponentModel.ListSortDirection.Ascending);
                RadGridView_ShowHD.GroupDescriptors.Add(descriptor);
            }
            if (pCase == "1")
            {
                this.Cursor = Cursors.WaitCursor;
                string staDate = "0";
                if (radCheckBox_Apv.Checked == true)
                {
                    staDate = "1";
                    RadGridView_ShowHD.Columns["TRANSDATE"].IsVisible = true;
                }
                else RadGridView_ShowHD.Columns["TRANSDATE"].IsVisible = false;

                dtData = ItembarcodeClass.GetDatailFactoryAll("0", staDate, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }

                DataTable dtAll = ItembarcodeClass.GetDatailFactoryAll("1", staDate, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    double qMake = 0, qSend = 0, qRecive = 0;
                    if (dtAll.Rows.Count > 0)
                    {
                        string whereDate = "";
                        if (radCheckBox_Apv.Checked == true) whereDate = $@" AND TRANSDATE = '{RadGridView_ShowHD.Rows[i].Cells["TRANSDATE"].Value}' ";

                        string itemID = RadGridView_ShowHD.Rows[i].Cells["ITEMID"].Value.ToString();
                        string inventDim = RadGridView_ShowHD.Rows[i].Cells["INVENTDIMID"].Value.ToString();

                        DataRow[] drMake = dtAll.Select($@" STA = 'MAKE' AND ITEMID = '{itemID}' AND INVENTDIMID = '{inventDim}' {whereDate} ");
                        if (drMake.Length > 0) qMake = double.Parse(drMake[0]["QTY"].ToString());

                        DataRow[] drSend = dtAll.Select($@" STA = 'SEND' AND ITEMID = '{itemID}' AND INVENTDIMID = '{inventDim}' {whereDate} ");
                        if (drSend.Length > 0) qSend = double.Parse(drSend[0]["QTY"].ToString());

                        DataRow[] drRecive = dtAll.Select($@" STA = 'RECIVE' AND ITEMID = '{itemID}' AND INVENTDIMID = '{inventDim}' {whereDate} ");
                        if (drRecive.Length > 0) qRecive = double.Parse(drRecive[0]["QTY"].ToString());

                        RadGridView_ShowHD.Rows[i].Cells["QTY_MAKE"].Value = qMake;
                        RadGridView_ShowHD.Rows[i].Cells["QTY_SEND"].Value = qSend;
                        RadGridView_ShowHD.Rows[i].Cells["QTY_RECIVE"].Value = qRecive;
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //value Change
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value Change
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T;
            if (_pTypeReport == "2") T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "2");
            else T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pPermission);
        }
        //Select Dpt
        private void RadCheckBox_Grp_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Grp.Checked == true) radDropDownList_Grp.Enabled = true; else radDropDownList_Grp.Enabled = false;
            if (radCheckBox_Vender.Checked == true) LoadVender();
        }
        #endregion

        #region SetReadOnly
        void SetReadOnly(Boolean staEdit)
        {
            RadGridView_ShowHD.ReadOnly = false;
            RadGridView_ShowHD.AllowAddNewRow = false;

            RadGridView_ShowHD.Columns["LINENUM"].ReadOnly = staEdit;
            RadGridView_ShowHD.Columns["INVOICEID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["PURCHID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["INVENTTRANSID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["ITEMBARCODE"].ReadOnly = true;
            RadGridView_ShowHD.Columns["SPCITEMNAME"].ReadOnly = true;
            RadGridView_ShowHD.Columns["QTYRECIVE"].ReadOnly = true;
            RadGridView_ShowHD.Columns["UNITID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["QTYDIFF"].ReadOnly = true;
            RadGridView_ShowHD.Columns["DATERECIVE"].ReadOnly = true;
            RadGridView_ShowHD.Columns["TIMERECIVE"].ReadOnly = true;
            RadGridView_ShowHD.Columns["INVENTLOCATIONTO"].ReadOnly = true;
            RadGridView_ShowHD.Columns["VENDID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["VENDNAME"].ReadOnly = true;
            RadGridView_ShowHD.Columns["BUYERID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["BUYERNAME"].ReadOnly = true;
            RadGridView_ShowHD.Columns["DEVICENAME"].ReadOnly = true;
            RadGridView_ShowHD.Columns["EMPLIDRECIVE"].ReadOnly = true;
            RadGridView_ShowHD.Columns["EMPLNAMERECIVE"].ReadOnly = true;
            RadGridView_ShowHD.Columns["DIMENSION"].ReadOnly = true;
            RadGridView_ShowHD.Columns["DIMENSIONNAME"].ReadOnly = true;
            RadGridView_ShowHD.Columns["REMARK"].ReadOnly = true;
            RadGridView_ShowHD.Columns["SENDAXSTA"].ReadOnly = true;
            RadGridView_ShowHD.Columns["TRANSID"].ReadOnly = true;
            RadGridView_ShowHD.Columns["SENDLINENUM"].ReadOnly = true;
            RadGridView_ShowHD.Columns["EMPLIDSENDLINENUM"].ReadOnly = true;
            RadGridView_ShowHD.Columns["EMPLNAMESENDLINENUM"].ReadOnly = true;
            RadGridView_ShowHD.Columns["DATETIMESENDLINENUM"].ReadOnly = true;
        }

        #endregion
        //Double Click
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pTypeReport == "0")
            {
                switch (e.Column.Name)
                {
                    case "PURCHID":
                        if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                        FindDatePO();
                        break;
                    case "INVENTTRANSID":
                        if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                        FindDatePO();
                        break;
                    case "COUNTPO":
                        #region "DelPO"
                        if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                        if (RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value.ToString() == "") return;
                        if (RadGridView_ShowHD.CurrentRow.Cells["SENDAXSTA"].Value.ToString() != "0") return;

                        if (_pPermission == "0")
                        {
                            if (SystemClass.SystemDptID != RadGridView_ShowHD.CurrentRow.Cells["BUYERID"].Value.ToString())
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำหนดสิทธิ์การแก้ไขเฉพาะสินค้าที่รับผิดชอบเท่านั้น{Environment.NewLine}แผนกจัดซื้อของสินค้าไม่ตรงกับแผนกของผู้ใช้ที่เปิดอยู่");
                                return;
                            }
                        }

                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบ PO ที่เลือกไว้ ?") == DialogResult.No) return;
                        //    ArrayList delPO = new ArrayList()
                        //{
                        // $@"
                        //    DELETE  [ProductRecive_LOGPO] 
                        //    WHERE   TRANSID = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ",
                        // $@"
                        //    UPDATE	[ProductRecive_DROID]
                        //    SET		PURCHID = '',INVENTTRANSID = '',STAAUTO = '1',
                        //            EMPLIDUPDATE='{SystemClass.SystemUserID_M}',EMPLNAMEUPDATE = '{SystemClass.SystemUserName}',DATETIMEUPDATE=GETDATE()
                        //    WHERE	[TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}'"
                        //};
                        //    string Ts = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(delPO, IpServerConnectClass.ConSupcAndroid);

                        string Ts = DeletePO(RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString());
                        if (Ts == "")
                        {
                            RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value = "";
                            RadGridView_ShowHD.CurrentRow.Cells["INVENTTRANSID"].Value = "";
                            RadGridView_ShowHD.CurrentRow.Cells["SUMPO"].Value = "0";
                            RadGridView_ShowHD.CurrentRow.Cells["QTYBEFORERECIVE"].Value = "0";
                            RadGridView_ShowHD.CurrentRow.Cells["QTYDIFF"].Value = (double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString()) - double.Parse(RadGridView_ShowHD.CurrentRow.Cells["SUMPO"].Value.ToString())).ToString("N2");
                        }
                        else MsgBoxClass.MsgBoxShow_SaveStatus(Ts);
                        #endregion
                        break;
                    case "REMARK":
                        #region "EditRMK"
                        if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                        if (RadGridView_ShowHD.CurrentRow.Cells["SENDAXSTA"].Value.ToString() != "0") return;

                        if (_pPermission == "0")
                        {
                            if (SystemClass.SystemDptID != RadGridView_ShowHD.CurrentRow.Cells["BUYERID"].Value.ToString())
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำหนดสิทธิ์การแก้ไขเฉพาะสินค้าที่รับผิดชอบเท่านั้น{Environment.NewLine}แผนกจัดซื้อของสินค้าไม่ตรงกับแผนกของผู้ใช้ที่เปิดอยู่");
                                return;
                            }
                        }

                        if (double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value.ToString()) == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"จำนวนสินค้าเป็น 0 ไม่สามารถดำเนินการใดๆต่อได้"); return;
                        }

                        double checkDiff = Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYDIFF"].Value.ToString());
                        string checkStr = "มากกว่า";
                        if (checkDiff < 0)
                        {
                            checkStr = "น้อยกว่า";
                            checkDiff *= -1;
                        }
                        ShowRemark _showRemark = new ShowRemark("1")
                        {
                            pDesc = $@"หมายเหตุ : การรับสินค้าเข้าคลัง{Environment.NewLine}ที่จำนวนรับ {checkStr} PO อยู่ {checkDiff:N2} ชิ้น",
                            pRmk = RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value.ToString()
                        };

                        if (_showRemark.ShowDialog(this) != DialogResult.Yes)
                        { MsgBoxClass.MsgBoxShowButtonOk_Error("ต้องระบุหมายเหตุก่อนบันทึกเท่านั้น"); return; }

                        if (RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value.ToString() == _showRemark.pRmk) return;
                        if (_showRemark.pRmk == "") return;

                        string sqlRmk = $@"
                        UPDATE	[ProductRecive_DROID]
                        SET		REMARK = '{_showRemark.pRmk}',
                                EMPLIDUPDATE='{SystemClass.SystemUserID_M}',EMPLNAMEUPDATE = '{SystemClass.SystemUserName}',DATETIMEUPDATE=GETDATE()
                        WHERE	[TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ";
                        string Trmk = ConnectionClass.ExecuteSQL_SentServer(sqlRmk, IpServerConnectClass.ConSupcAndroid);
                        if (Trmk == "")
                        {
                            RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value = _showRemark.pRmk;
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(Trmk);
                            return;
                        }
                        #endregion
                        break;
                    case "QTYRECIVE":
                        #region "EditQTYRECIVE"

                        if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                        if (_pPermission != "1") return;

                        if (RadGridView_ShowHD.CurrentRow.Cells["SENDAXSTA"].Value.ToString() != "0") return;

                        if (double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value.ToString()) == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"จำนวนสินค้าเป็น 0 ไม่สามารถดำเนินการใดๆต่อได้"); return;
                        }

                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการแก้ไขจำนวนที่รับเข้าคลัง") != DialogResult.Yes) return;

                        InputData frmSTA = new InputData("0",
                                          RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_ShowHD.CurrentRow.Cells["SPCITEMNAME"].Value.ToString(),
                                          "จำนวนที่ต้องการแก้ไข", RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value.ToString())
                        { pInputData = Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value.ToString()).ToString("N2") };
                        if (frmSTA.ShowDialog(this) != DialogResult.Yes) return;

                        double reciveQtyAll = Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTY"].Value.ToString()) * Double.Parse(frmSTA.pInputData);

                        string strITEMSTA = "";
                        ArrayList sqlUp = new ArrayList();
                        if (double.Parse(frmSTA.pInputData) == 0) strITEMSTA = ",ITEMSTA = '0' ";

                        //if (double.Parse(frmSTA.pInputData) == 0)
                        //{
                        //    strITEMSTA = ",ITEMSTA = '0' ";
                        //    sqlUp.Add($@"
                        //        DELETE  [ProductRecive_LOGPO] 
                        //        WHERE   [TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ");
                        //}
                        //else
                        //{
                        //    sqlUp.Add($@"
                        //    UPDATE  ProductRecive_LOGPO
                        //    SET     QTYRECIVE = '{frmSTA.pInputData}' ,QTYRECIVEALL  = '{reciveQtyAll}'
                        //    WHERE   [TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ");
                        //}


                        sqlUp.Add($@"
                                DELETE  [ProductRecive_LOGPO] 
                                WHERE   [TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ");

                        sqlUp.Add($@"
                        UPDATE	[ProductRecive_DROID]
                        SET		QTYRECIVE = '{frmSTA.pInputData}',PURCHID = '',INVENTTRANSID = '',STAAUTO = '1',
                                EMPLIDUPDATE='{SystemClass.SystemUserID_M}',EMPLNAMEUPDATE = '{SystemClass.SystemUserName}',DATETIMEUPDATE=GETDATE()
                                {strITEMSTA}
                        WHERE	[TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ");

                        string T = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlUp, IpServerConnectClass.ConSupcAndroid);
                        if (T == "")
                        {
                            RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value = double.Parse(frmSTA.pInputData).ToString("N2");
                            RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value = reciveQtyAll.ToString("N2");
                            //RadGridView_ShowHD.CurrentRow.Cells["QTYDIFF"].Value = (reciveQtyAll - double.Parse(RadGridView_ShowHD.CurrentRow.Cells["SUMPO"].Value.ToString())).ToString("N2");

                            RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value = "";
                            RadGridView_ShowHD.CurrentRow.Cells["INVENTTRANSID"].Value = "";
                            RadGridView_ShowHD.CurrentRow.Cells["SUMPO"].Value = "0";
                            RadGridView_ShowHD.CurrentRow.Cells["QTYBEFORERECIVE"].Value = "0";
                            RadGridView_ShowHD.CurrentRow.Cells["QTYDIFF"].Value = (double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString()) - double.Parse(RadGridView_ShowHD.CurrentRow.Cells["SUMPO"].Value.ToString())).ToString("N2");

                            //FindPOByLineAuto(RadGridView_ShowHD.CurrentRow.Index, double.Parse(frmSTA.pInputData), reciveQtyAll);
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(T);
                            return;
                        }
                        #endregion
                        break;
                    default:
                        break;
                }
            }//0 รับเข้าคลัง  

            if (_pTypeReport == "1")
            {
                if (RadGridView_ShowHD.Rows.Count == 0) return;

                this.Cursor = Cursors.WaitCursor;
                string purchID = RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value.ToString();
                if (purchID == "") return;
                dtPrint = ProductRecive.ProductRecive_Class.GetDatailFactory("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), purchID);
                if (dtPrint.Rows.Count != 0)
                {
                    DialogResult result = printDialog1.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        printDocument1.PrintController = printController;
                        System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("Roll Paper 80 X 3276 mm", 80, 32760);
                        printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                        printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                        double rowAll = dtPrint.Rows.Count;
                        double diffPage = rowAll / Print_numPage;
                        Print_PageALl = Math.Ceiling((diffPage * 100) / 100);

                        Print_sumAll = 0;
                        Print_iPage = 0;

                        Print_i = 0;
                        for (int i = 0; i < Print_PageALl; i++)
                        {
                            Print_iPage++;

                            double iC = Print_numPage;
                            if (Print_PageALl == 1) iC = rowAll;
                            else if (Print_iPage == Print_PageALl) iC = rowAll - ((i) * (Print_numPage));

                            if (dtPrintDT.Rows.Count > 0) dtPrintDT.Rows.Clear();

                            for (int iP = 0; iP < iC; iP++)
                            {
                                dtPrintDT.Rows.Add(dtPrint.Rows[Print_i]["ITEMBARCODE"].ToString(),
                                    dtPrint.Rows[Print_i]["SPCITEMNAME"].ToString(),
                                    dtPrint.Rows[Print_i]["QTYRECIVE"].ToString(), dtPrint.Rows[Print_i]["UNITID"].ToString(),
                                    dtPrint.Rows[Print_i]["COST_PRICE"].ToString(), dtPrint.Rows[Print_i]["COST_ALL"].ToString(), Print_i + 1);

                                Print_sumAll += Double.Parse(dtPrint.Rows[Print_i]["COST_ALL"].ToString());
                                Print_i++;
                            }
                            printDocument1.Print();
                        }
                    }
                }
                this.Cursor = Cursors.Default;
            }//1 ออก Invoice ของโรงงาน 

            if (_pTypeReport == "2")
            {
                if (RadGridView_ShowHD.Rows.Count == 0) return;


                switch (e.Column.Name)
                {
                    case "IMG1":
                        if (RadGridView_ShowHD.CurrentRow.Cells["P_IMG1"].Value.ToString() != "") System.Diagnostics.Process.Start(RadGridView_ShowHD.CurrentRow.Cells["P_IMG1"].Value.ToString());
                        break;
                    default:
                        break;
                }
            }// 2 เปรียบเทียบการรับส่งสินค้าระหว่างโรงงาน/คลัง A 

            if (_pTypeReport == "3")// 3 ระบุ บรรทัด
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["SENDLINENUM"].Value.ToString() != "0") return;

                switch (e.Column.Name)
                {
                    case "LINENUM":
                        if (RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value.ToString() == "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถลบรายการได้เนื่องจากยังไม่มีใบแจ้งหนี้");
                            return;
                        }
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบบรรทัดโดยปรับเป็น 0{Environment.NewLine}ในรายการที่เลือกไว้ ?") == DialogResult.No) return;

                        string sqlLineNum = $@"
                            UPDATE	[ProductRecive_LOGPO]
                            SET		LINENUM = '0',  
                                    EMPLIDINVOICE='{SystemClass.SystemUserID_M}',EMPLNAMEINVOICE = '{SystemClass.SystemUserName}',DATETIMEINVOICE=GETDATE()
                            WHERE	[TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}'   ";

                        string resualtLineNum = ConnectionClass.ExecuteSQL_SentServer(sqlLineNum, IpServerConnectClass.ConSupcAndroid);
                        if (resualtLineNum != "") MsgBoxClass.MsgBoxShow_SaveStatus(resualtLineNum);

                        break;
                    case "INVOICEID":
                        string invoidIDOld = RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value.ToString();
                        if (tickSta == "1") return;

                        InputData inputData = new InputData("1", "", "ระบุเลขที่บิลผู้จำหน่าย", "", "1")
                        {
                            pInputData = invoidIDOld
                        };

                        if (inputData.ShowDialog() != DialogResult.Yes) return;

                        //string conWhere = $@" TRANSID = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ";
                        //if (invoidIDOld != "") conWhere = $@" INVOICEID = '{invoidIDOld}' ";

                        string sqlInvoice = $@"
                            UPDATE	[ProductRecive_LOGPO]
                            SET		INVOICEID = '{inputData.pInputData}',
                                    EMPLIDINVOICE='{SystemClass.SystemUserID_M}',EMPLNAMEINVOICE = '{SystemClass.SystemUserName}',DATETIMEINVOICE=GETDATE()
                            WHERE	 TRANSID = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}'   ";

                        string resualtInvoice = ConnectionClass.ExecuteSQL_SentServer(sqlInvoice, IpServerConnectClass.ConSupcAndroid);
                        if (resualtInvoice != "") { MsgBoxClass.MsgBoxShow_SaveStatus(resualtInvoice); return; }
                        //if (invoidIDOld == "") RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value = inputData.pInputData;
                        //else FindDataLineNum();
                        RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value = inputData.pInputData;

                        break;
                    default:
                        break;
                }
            }// 3 ระบุ บรรทัด

            if (_pTypeReport == "4") // 4 ระบบสินค้าโรงงานใหม่ แทน Type 1
            {
                if (RadGridView_ShowHD.Rows.Count == 0) return;


                string purchID = RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value.ToString();
                if (purchID == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องยืนยันรายการให้เรียบร้อยก่อนพิมพ์เท่านั้น");
                    return;
                }

                if (RadGridView_ShowHD.CurrentRow.Cells["BA"].Value.ToString() != "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ในกรณีที่ออก BA แล้วไม่สามารถพิมพ์ซ้ำได้");
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                dtPrint = ProductRecive_Class.GetDatailFactory("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), purchID);
                if (dtPrint.Rows.Count != 0)
                {
                    DialogResult result = printDialog1.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        printDocument1.PrintController = printController;
                        System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("Roll Paper 80 X 3276 mm", 80, 32760);
                        printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                        printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                        double rowAll = dtPrint.Rows.Count;
                        double diffPage = rowAll / Print_numPage;
                        Print_PageALl = Math.Ceiling((diffPage * 100) / 100);

                        Print_sumAll = 0;
                        Print_iPage = 0;

                        Print_i = 0;
                        for (int i = 0; i < Print_PageALl; i++)
                        {
                            Print_iPage++;

                            double iC = Print_numPage;
                            if (Print_PageALl == 1) iC = rowAll;
                            else if (Print_iPage == Print_PageALl) iC = rowAll - ((i) * (Print_numPage));

                            if (dtPrintDT.Rows.Count > 0) dtPrintDT.Rows.Clear();

                            for (int iP = 0; iP < iC; iP++)
                            {
                                dtPrintDT.Rows.Add(dtPrint.Rows[Print_i]["ITEMBARCODE"].ToString(),
                                    dtPrint.Rows[Print_i]["SPCITEMNAME"].ToString(),
                                    dtPrint.Rows[Print_i]["QTYRECIVE"].ToString(), dtPrint.Rows[Print_i]["UNITID"].ToString(),
                                    dtPrint.Rows[Print_i]["COST_PRICE"].ToString(), dtPrint.Rows[Print_i]["COST_ALL"].ToString(), Print_i + 1);

                                Print_sumAll += Double.Parse(dtPrint.Rows[Print_i]["COST_ALL"].ToString());
                                Print_i++;
                            }
                            printDocument1.Print();
                        }
                    }
                }
                this.Cursor = Cursors.Default;
            }// 4 ระบบสินค้าโรงงานใหม่ แทน Type 1

            if (_pTypeReport == "7")//สินค้าที่รับเข้าคลังแต่ไม่ลงรายการบัญชี
            {
                switch (e.Column.Name)
                {
                    case "QTYRECIVEALL":
                        if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
                        if (_pPermission != "1") { if (SystemClass.SystemDptID != "D015") return; }


                        //if (RadGridView_ShowHD.CurrentRow.Cells["SENDAXSTA"].Value.ToString() != "0") return;

                        if (double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString()) == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"จำนวนสินค้าเป็น 0 ไม่สามารถดำเนินการใดๆต่อได้"); return;
                        }

                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบรายการที่รับเข้าคลังออก ?") != DialogResult.Yes) return;


                        ArrayList sqlUp = new ArrayList
                        {
                            $@"
                                DELETE  [ProductRecive_LOGPO] 
                                WHERE   [TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' ",

                            $@"
                                UPDATE	[ProductRecive_DROID]
                                SET		QTYRECIVE = '0',PURCHID = '',INVENTTRANSID = '',STAAUTO = '1',
                                        EMPLIDUPDATE='{SystemClass.SystemUserID_M}',EMPLNAMEUPDATE = '{SystemClass.SystemUserName}',DATETIMEUPDATE=GETDATE(),ITEMSTA = '0' 
                                WHERE	[TRANSID] = '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}' "
                        };

                        string T = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlUp, IpServerConnectClass.ConSupcAndroid);
                        if (T == "") ReportNotApv("1");
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(T);
                            return;
                        }
                        break;
                }
            }//สินค้าที่รับเข้าคลังแต่ไม่ลงรายการบัญชี
        }
        //Del PO
        string DeletePO(string transID)
        {
            ArrayList delPO = new ArrayList()
                    {
                     $@"
                        DELETE  [ProductRecive_LOGPO] 
                        WHERE   TRANSID = '{transID}' ",
                     $@"
                        UPDATE	[ProductRecive_DROID]
                        SET		PURCHID = '',INVENTTRANSID = '',STAAUTO = '1',
                                EMPLIDUPDATE='{SystemClass.SystemUserID_M}',EMPLNAMEUPDATE = '{SystemClass.SystemUserName}',DATETIMEUPDATE=GETDATE()
                        WHERE	[TRANSID] = '{transID}'"
                    };
            return ConnectionClass.ExecuteSQL_ArrayMain_SentServer(delPO, IpServerConnectClass.ConSupcAndroid);
        }
        //ออกเอกสาร โรงงาน
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = dtPrint.Rows[0]["PURCHID"].ToString();
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            e.Graphics.DrawString($@" หน้า {Print_iPage}/{Print_PageALl}", SystemClass.printFont15, Brushes.Black, 10, Y);
            Y += 30;
            e.Graphics.DrawString(" รับสินค้าโรงงานเข้าคลัง   " + dtPrint.Rows[0]["INVENTLOCATIONTO"].ToString(), SystemClass.SetFont12, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;

            e.Graphics.DrawString(dtPrint.Rows[0]["PURCHID"].ToString(), SystemClass.printFont15, Brushes.Black, 20, Y);
            Y += 30;
            e.Graphics.DrawString(dtPrint.Rows[0]["BUYERID"].ToString() + "-" + dtPrint.Rows[0]["BUYERNAME"].ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString(dtPrint.Rows[0]["VENDID"].ToString() + "-" + dtPrint.Rows[0]["VENDNAME"].ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("พิมพ์ " + DateTime.Now, SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
            //Y += 5;

            for (int iP = 0; iP < dtPrintDT.Rows.Count; iP++)
            {
                Y += 25;
                e.Graphics.DrawString(dtPrintDT.Rows[iP]["Print_i"].ToString() + ".  " + dtPrintDT.Rows[iP]["ITEMBARCODE"].ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 18;
                e.Graphics.DrawString(dtPrintDT.Rows[iP]["SPCITEMNAME"].ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 18;
                e.Graphics.DrawString("-" + (Convert.ToDouble(dtPrintDT.Rows[iP]["QTYRECIVE"].ToString()).ToString("#,#0.00")).ToString() + " (" + dtPrintDT.Rows[iP]["UNITID"].ToString() + ") X " +
                                      (Convert.ToDouble(dtPrintDT.Rows[iP]["COST_PRICE"].ToString()).ToString("#,#0.00")).ToString() + " =  " +
                                     (Convert.ToDouble(dtPrintDT.Rows[iP]["COST_ALL"].ToString()).ToString("#,#0.00")).ToString() + "  บาท",
                        SystemClass.printFont, Brushes.Black, 0, Y);
            }


            if (Print_iPage == Print_PageALl)
            {
                Y += 15;
                e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString("จำนวนทั้งหมด " + dtPrint.Rows.Count.ToString("N2") + "  รายการ", SystemClass.printFont15, Brushes.Black, 10, Y);
                Y += 25;
                e.Graphics.DrawString("ยอดเงิน      " + Print_sumAll.ToString("N2") + " บาท", SystemClass.printFont15, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString("ผู้พิมพ์บิล : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString("ชื่อผู้พิมพ์บิล " + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
            }
            else
            {
                Y += 30;
                e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString("******* ต่อหน้าถัดไป *******", SystemClass.printFont15, Brushes.Black, 10, Y);
            }
        }
        void FindData()
        {
            string staAX = "1";
            if (radCheckBox_Apv.Checked == true) staAX = "0";

            //DataTable dtData =
            string date1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string date2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            string buyerID = "";
            if (radCheckBox_Grp.Checked == true) buyerID = radDropDownList_Grp.SelectedValue.ToString();

            string venderIDLoad = "";
            if (radCheckBox_Vender.Checked == true) venderIDLoad = radDropDownList_Vender.SelectedValue.ToString();

            this.Cursor = Cursors.WaitCursor;
            dtData = ConnectionClass.SelectSQL_SentServer(ProductRecive_Class.GetDetailDroid(buyerID, "0", date1,
                       date2, staAX, _pPermission, venderIDLoad), IpServerConnectClass.ConSupcAndroid, 60);
            RadGridView_ShowHD.DataSource = dtData;
            dtData.AcceptChanges();
            if (dtData.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                this.Cursor = Cursors.Default;
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            DataTable dtPO = PurchClass.ProductRecive_GetPurchPO("0", ProductRecive_Class.GetDetailDroid(buyerID, "1", date1, date2, staAX, _pPermission, ""), "0");

            if (dtPO.Rows.Count == 0) { this.Cursor = Cursors.Default; return; }

            if (_pAutoRunPO == "1")
            {
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["PURCHID"].Value.ToString() != "") continue;

                    double Current_qtyRecive = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["QTYRECIVE"].Value.ToString());
                    double Current_qtyReciveAll = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["QTYRECIVEALL"].Value.ToString());

                    DataRow[] drPO = dtPO.Select($@" QTYORDERED = '{Current_qtyReciveAll}' AND ITEMID = '{RadGridView_ShowHD.Rows[i].Cells["ITEMID"].Value}'
                                                AND CONFIGID = '{RadGridView_ShowHD.Rows[i].Cells["CONFIGID"].Value}' 
                                                AND INVENTSIZEID = '{RadGridView_ShowHD.Rows[i].Cells["INVENTSIZEID"].Value}' 
                                                AND INVENTCOLORID = '{RadGridView_ShowHD.Rows[i].Cells["INVENTCOLORID"].Value}' ");


                    string Ipurch_purchID = "", Ipurch_inventTrandID = "", Ipurch_Barcode = "";
                    double Ipurch_Factor = 1;
                    double Ipurch_Qty = 0, Ipurch_QtyAll = 0, Iold_qtyReciveAll = 0;
                    int countPO = 0;
                    string venderID = "", venderName = "";

                    //countPO = drPO.Length;
                    if (drPO.Length == 1)// ในกรณีที่เจอ 1 PO ในเงื่อนไขที่จำนวนรับเข้ามาเกี่ยวข้อง
                    {
                        Ipurch_purchID = drPO[0]["PURCHID"].ToString();
                        Ipurch_inventTrandID = drPO[0]["INVENTTRANSID"].ToString();
                        Ipurch_Barcode = drPO[0]["BARCODE"].ToString();
                        Ipurch_Factor = Double.Parse(drPO[0]["FACTOR"].ToString());

                        Ipurch_Qty = Double.Parse(drPO[0]["PURCHQTY"].ToString());
                        Ipurch_QtyAll = Double.Parse(drPO[0]["QTYORDERED"].ToString());

                        Iold_qtyReciveAll = Double.Parse(drPO[0]["QTYRECIVEALL"].ToString());
                        venderID = drPO[0]["VENDACCOUNT"].ToString();
                        venderName = drPO[0]["PURCHNAME"].ToString();
                        countPO = 1;
                    }
                    #region "ในกรณีที่เจอ มากกว่า 1 PO กรอง เฉพาะสินค้ามิติเดียวกัน แต่มีหลายบรรทัด"
                    //ในกรณีที่เจอ มากกว่า 1 PO กรอง เฉพาะสินค้ามิติเดียวกัน แต่มีหลายบรรทัด
                    else
                    {
                        drPO = dtPO.Select($@" ITEMID = '{RadGridView_ShowHD.Rows[i].Cells["ITEMID"].Value}'
                                                AND CONFIGID = '{RadGridView_ShowHD.Rows[i].Cells["CONFIGID"].Value}' 
                                                AND INVENTSIZEID = '{RadGridView_ShowHD.Rows[i].Cells["INVENTSIZEID"].Value}' 
                                                AND INVENTCOLORID = '{RadGridView_ShowHD.Rows[i].Cells["INVENTCOLORID"].Value}' ");

                        string strCheckPOvender = "";
                        for (int iVCheck = 0; iVCheck < drPO.Length; iVCheck++)
                        {
                            if (strCheckPOvender != drPO[iVCheck]["PURCHID"].ToString())
                            {
                                countPO++;
                                strCheckPOvender = drPO[iVCheck]["PURCHID"].ToString();
                            }
                        }
                        //for (int iC = 0; iC < drPO.Length; iC++)
                        //{
                        //    Ipurch_Qty += Double.Parse(drPO[iC]["PURCHQTY"].ToString());
                        //    Ipurch_QtyAll += Double.Parse(drPO[iC]["QTYORDERED"].ToString());
                        //    Iold_qtyReciveAll += Double.Parse(drPO[iC]["QTYRECIVEALL"].ToString());

                        //    if (Ipurch_purchID != drPO[iC]["PURCHID"].ToString())
                        //    {
                        //        countPO++;
                        //        Ipurch_purchID = drPO[iC]["PURCHID"].ToString();
                        //        Ipurch_inventTrandID = drPO[0]["INVENTTRANSID"].ToString();
                        //        Ipurch_Barcode = drPO[0]["BARCODE"].ToString();
                        //        Ipurch_Factor = Double.Parse(drPO[0]["FACTOR"].ToString());
                        //        venderID = drPO[0]["VENDACCOUNT"].ToString();
                        //        venderName = drPO[0]["PURCHNAME"].ToString();
                        //    }
                        //}
                    }
                    #endregion

                    RadGridView_ShowHD.Rows[i].Cells["COUNTPO"].Value = countPO;
                    RadGridView_ShowHD.Rows[i].Cells["SUMPO"].Value = Ipurch_QtyAll;
                    RadGridView_ShowHD.Rows[i].Cells["QTYDIFF"].Value = Current_qtyReciveAll - Ipurch_QtyAll;

                    if ((countPO == 1) && (Ipurch_QtyAll == Current_qtyReciveAll))
                    {
                        if ((Current_qtyReciveAll + Iold_qtyReciveAll) > Ipurch_QtyAll) { }
                        else
                        {
                            string upVender = "";
                            if (venderID != RadGridView_ShowHD.Rows[i].Cells["VENDID"].Value.ToString()) upVender = $@",VENDID = '{venderID}',VENDNAME = '{venderName}' ";

                            string T = SaveLogPO(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString(),
                              Ipurch_purchID, Ipurch_inventTrandID,
                              Ipurch_Qty, Ipurch_QtyAll, Current_qtyRecive, Current_qtyReciveAll, "", upVender, "1", Ipurch_Barcode, Ipurch_Factor, Iold_qtyReciveAll);
                            if (T == "")
                            {
                                RadGridView_ShowHD.Rows[i].Cells["PURCHID"].Value = Ipurch_purchID;
                                RadGridView_ShowHD.Rows[i].Cells["INVENTTRANSID"].Value = Ipurch_inventTrandID;
                                RadGridView_ShowHD.Rows[i].Cells["SUMPO"].Value = Ipurch_QtyAll;
                                RadGridView_ShowHD.Rows[i].Cells["PURCHBARCODE"].Value = Ipurch_Barcode;
                                RadGridView_ShowHD.Rows[i].Cells["PURCHFACTOR"].Value = Ipurch_Factor;
                                RadGridView_ShowHD.Rows[i].Cells["VENDID"].Value = venderID;
                                RadGridView_ShowHD.Rows[i].Cells["VENDNAME"].Value = venderName;
                            }
                        }
                    }
                }
            }
            this.Cursor = Cursors.Default;
        }

        #region "FindPOByLineAuto"

        ////ค้นหา PO ฺLine
        //void FindPOByLineAuto(int rowsNumber, double qtyRecive, double qtyReciveAll)
        //{
        //    DataTable dtPO = PurchClass.ProductRecive_GetPurchPO("0", $@" '{RadGridView_ShowHD.Rows[rowsNumber].Cells["ITEMBARCODE"].Value}'", _pPermission);
        //    if (dtPO.Rows.Count == 0) return;
        //    DataRow[] drPO = dtPO.Select($@" QTYORDERED = '{qtyReciveAll}' AND ITEMID = '{RadGridView_ShowHD.Rows[rowsNumber].Cells["ITEMID"].Value}'
        //                                        AND CONFIGID = '{RadGridView_ShowHD.Rows[rowsNumber].Cells["CONFIGID"].Value}' 
        //                                        AND INVENTSIZEID = '{RadGridView_ShowHD.Rows[rowsNumber].Cells["INVENTSIZEID"].Value}' 
        //                                        AND INVENTCOLORID = '{RadGridView_ShowHD.Rows[rowsNumber].Cells["INVENTCOLORID"].Value}' ");

        //    string venderID = "", venderName = "";
        //    string Ipurch_purchID = "", Ipurch_inventTrandID = "", Ipurch_Barcode = "";
        //    double Ipurch_Factor = 1;
        //    double Ipurch_Qty = 0, Ipurch_QtyAll = 0, Iold_qtyReciveAll = 0;
        //    int countPO = 0;
        //    if (drPO.Length == 1)
        //    {
        //        Ipurch_purchID = drPO[0]["PURCHID"].ToString();
        //        Ipurch_inventTrandID = drPO[0]["INVENTTRANSID"].ToString();
        //        Ipurch_Qty = Double.Parse(drPO[0]["PURCHQTY"].ToString());
        //        Ipurch_QtyAll = Double.Parse(drPO[0]["QTYORDERED"].ToString());

        //        Ipurch_Barcode = drPO[0]["BARCODE"].ToString();
        //        Ipurch_Factor = Double.Parse(drPO[0]["FACTOR"].ToString());

        //        Iold_qtyReciveAll = Double.Parse(drPO[0]["QTYRECIVEALL"].ToString());

        //        venderID = drPO[0]["VENDACCOUNT"].ToString();
        //        venderName = drPO[0]["PURCHNAME"].ToString();
        //        countPO = 1;
        //    }

        //    RadGridView_ShowHD.Rows[rowsNumber].Cells["COUNTPO"].Value = countPO;
        //    RadGridView_ShowHD.Rows[rowsNumber].Cells["SUMPO"].Value = Ipurch_QtyAll;
        //    RadGridView_ShowHD.Rows[rowsNumber].Cells["QTYDIFF"].Value = qtyReciveAll - Ipurch_QtyAll;

        //    if ((countPO == 1) && (Ipurch_QtyAll == qtyReciveAll))
        //    {
        //        if ((qtyReciveAll + Iold_qtyReciveAll) > Ipurch_QtyAll) { }
        //        else
        //        {
        //            string upVender = "";
        //            if (venderID != RadGridView_ShowHD.Rows[rowsNumber].Cells["VENDID"].Value.ToString()) upVender = $@",VENDID = '{venderID}',VENDNAME = '{venderName}' ";

        //            string T = SaveLogPO(RadGridView_ShowHD.Rows[rowsNumber].Cells["TRANSID"].Value.ToString(),
        //              Ipurch_purchID, Ipurch_inventTrandID,
        //              Ipurch_Qty, Ipurch_QtyAll, qtyRecive, qtyReciveAll, "", upVender, "2", Ipurch_Barcode, Ipurch_Factor, Iold_qtyReciveAll);
        //            if (T == "")
        //            {
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["PURCHID"].Value = Ipurch_purchID;
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["INVENTTRANSID"].Value = Ipurch_inventTrandID;
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["SUMPO"].Value = Ipurch_QtyAll;
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["PURCHBARCODE"].Value = Ipurch_Barcode;
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["PURCHFACTOR"].Value = Ipurch_Factor;
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["VENDID"].Value = venderID;
        //                RadGridView_ShowHD.Rows[rowsNumber].Cells["VENDNAME"].Value = venderName;
        //            }
        //        }
        //    }
        //}

        #endregion
        //Set PurchID
        void FindDatePO()
        {

            if (RadGridView_ShowHD.CurrentRow.Cells["SENDAXSTA"].Value.ToString() != "0") return;

            if (double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value.ToString()) == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"จำนวนสินค้าเป็น 0 ไม่สามารถดำเนินการใดๆต่อได้"); return;
            }

            DataTable dtPO = PurchClass.ProductRecive_GetPurchPO("0", $@" '{RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value}' ", "1");

            if (dtPO.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบการสั่งซื้อของบาร์โค้ดที่ระบุ{Environment.NewLine}ติดต่อจัดซื้อเพื่อสอบถามข้อมูลเพิ่มเติม"); return;
            }

            string checkDptForEdit = "1";
            if (_pPermission == "0")
            {
                if (SystemClass.SystemDptID != RadGridView_ShowHD.CurrentRow.Cells["BUYERID"].Value.ToString()) checkDptForEdit = "0";
            }

            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV("3")
            {
                dtData = dtPO,
                pID = RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value.ToString(),
                pDesc = RadGridView_ShowHD.CurrentRow.Cells["INVENTTRANSID"].Value.ToString(),
                pID2 = checkDptForEdit,
                pID3 = RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString(),
                pDesc3 = RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value.ToString(),
                pSheet = RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString(),
                pBarcode = RadGridView_ShowHD.CurrentRow.Cells["PURCHBARCODE"].Value.ToString(),
                pFactor = Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["PURCHFACTOR"].Value.ToString())
            };
            if (frm.ShowDialog() != DialogResult.Yes) return;

            if (frm.pDesc3 == "R") { FindData(); return; }

            string upVender = "";
            if (frm.pID3 != RadGridView_ShowHD.CurrentRow.Cells["VENDID"].Value.ToString()) upVender = $@",VENDID = '{frm.pID3}',VENDNAME = '{frm.pDesc3}' ";

            string T = SaveLogPO(RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString(), frm.pID, frm.pDesc,
                        Double.Parse(frm.pID2), Double.Parse(frm.pDesc2),
                        Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVE"].Value.ToString()),
                        Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString()), frm.pSheet, upVender, "0",
                       frm.pBarcode, frm.pFactor, frm.pQtyBeforeRecive);
            if (T == "")
            {
                RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value = frm.pID;
                RadGridView_ShowHD.CurrentRow.Cells["INVENTTRANSID"].Value = frm.pDesc;

                RadGridView_ShowHD.CurrentRow.Cells["PURCHBARCODE"].Value = frm.pBarcode;
                RadGridView_ShowHD.CurrentRow.Cells["PURCHFACTOR"].Value = frm.pFactor;

                RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value = frm.pSheet;
                RadGridView_ShowHD.CurrentRow.Cells["VENDID"].Value = frm.pID3;
                RadGridView_ShowHD.CurrentRow.Cells["VENDNAME"].Value = frm.pDesc3;
                RadGridView_ShowHD.CurrentRow.Cells["SUMPO"].Value = Double.Parse(frm.pDesc2).ToString("N2");
                RadGridView_ShowHD.CurrentRow.Cells["QTYBEFORERECIVE"].Value = frm.pQtyBeforeRecive;
                RadGridView_ShowHD.CurrentRow.Cells["QTYDIFF"].Value = (Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["QTYRECIVEALL"].Value.ToString()) - Double.Parse(frm.pDesc2)).ToString("N2");
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                return;
            }
        }
        //Save Log
        string SaveLogPO(string transID, string purchID, string inventTransID, double purchQty, double purchQtyAll, double qtyRecive, double qtyReciveAll, string rmk, string addOnUpdate, string staAuto,
                        string barcode, double factor, double qtyBefore)
        {
            ArrayList sqlIn = new ArrayList
            {
                $@"
                    INSERT INTO [dbo].[ProductRecive_LOGPO]
                            ([TRANSID],[PURCHID],[INVENTTRANSID],[PURCHQTY],[PURCHQTYALL],[QTYRECIVE],[QTYRECIVEALL],
                            [EMPLID],[EMPLNAME],[PURCHBARCODE],[PURCHFACTOR],[QTYBEFORERECIVE])
                    VALUES
                            ('{transID}','{purchID}','{inventTransID}','{purchQty}','{purchQtyAll}','{qtyRecive}','{qtyReciveAll}',
                            '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}','{barcode}','{factor}','{qtyBefore}') ",
                $@"
                    UPDATE	[ProductRecive_DROID]
                    SET		PURCHID = '{purchID}',INVENTTRANSID = '{inventTransID}',REMARK='{rmk}',STAAUTO = '{staAuto}',
                            EMPLIDUPDATE='{SystemClass.SystemUserID_M}',EMPLNAMEUPDATE = '{SystemClass.SystemUserName}',DATETIMEUPDATE=GETDATE()
                            {addOnUpdate}
                    WHERE	[TRANSID] = '{transID}'   "
            };
            return ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlIn, IpServerConnectClass.ConSupcAndroid);
        }

        //ลงรายการบัญชี
        private void RadButton_SaveAX_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            //ลงรายการบัญชี สินค้าเข้าคลัง
            if (_pTypeReport == "0")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลงรายการบัญชี (AX) ?") == DialogResult.No) return;

                this.RadGridView_ShowHD.SortDescriptors.Expression = "INVENTLOCATIONFROM,INVENTLOCATIONTO,VENDID,BUYERID,EMPLIDRECIVE ASC";

                #region "DATATABLE"
                DataTable dtVenderHD = new DataTable();
                dtVenderHD.Columns.Add("INVENTLOCATIONFROM");
                dtVenderHD.Columns.Add("INVENTLOCATIONTO");
                dtVenderHD.Columns.Add("VENDID");
                dtVenderHD.Columns.Add("VENDNAME");
                dtVenderHD.Columns.Add("BUYERID");
                dtVenderHD.Columns.Add("PO");
                dtVenderHD.Columns.Add("EMPLIDRECIVE");
                dtVenderHD.Columns.Add("EMPLNAMERECIVE");

                DataTable dtItem = new DataTable();
                dtItem.Columns.Add("ITEMID");
                dtItem.Columns.Add("INVENTDIMID");
                dtItem.Columns.Add("ITEMBARCODE");
                dtItem.Columns.Add("QTY");
                dtItem.Columns.Add("TRANSID");
                dtItem.Columns.Add("INVENTTRANSID");
                dtItem.Columns.Add("EMPLIDRECIVE");
                dtItem.Columns.Add("EMPLNAMERECIVE");
                dtItem.Columns.Add("INVENTLOCATIONFROM");
                dtItem.Columns.Add("INVENTLOCATIONTO");
                dtItem.Columns.Add("VENDID");
                dtItem.Columns.Add("BUYERID");
                dtItem.Columns.Add("REMARK");
                dtItem.Columns.Add("INVENTONHAND");
                dtItem.Columns.Add("PO");
                #endregion

                string inventFromCheck = "", inventToCheck = "", venderCheck = "", buyerIDCheck = "", poCheck = "", emplReciveCheck = "";
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["SENDAXSTA"].Value.ToString() != "0") continue;
                    if (RadGridView_ShowHD.Rows[i].Cells["INVENTTRANSID"].Value.ToString() == "") continue;

                    //เพิ่มการเชค LOT
                    DataTable dtLot = PurchClass.ProductRecive_GetPurchPO("1", RadGridView_ShowHD.Rows[i].Cells["INVENTTRANSID"].Value.ToString(), "");
                    //กรณีไม่เจอ Lot เลย
                    if (dtLot.Rows.Count == 0)
                    {
                        DeletePO(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString()); continue;
                    }

                    DataTable dtLogPO = FindDataLogPO(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString());
                    //กรณีที่ไม่เจอใน ProductRecive_LOGPO + ProductRecive_DROID ให้ลบออกไปก่อน
                    if (dtLogPO.Rows.Count == 0)
                    {
                        DeletePO(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString()); continue;
                    }
                    //เชคว่าจำนวนสั่งเท่ากับที่เคยบันทึกไว้ไหม ถ้าไม่เท่าก็ลบไปก่อน
                    if (Double.Parse(dtLot.Rows[0]["QTYORDERED"].ToString()) != Double.Parse(dtLogPO.Rows[0]["PURCHQTYALL"].ToString()))
                    {
                        DeletePO(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString()); continue;
                    }
                    //CheckBarcode PO ที่เกบไว้ก่อน ถ้าไม่เท่าก็ update แล้วค่อยให้ลงรายการบัญชีใหม่
                    if (dtLot.Rows[0]["BARCODE"].ToString() != dtLogPO.Rows[0]["PURCHBARCODE"].ToString())
                    {
                        string AupPOBarcodeNew = $@"
                            UPDATE	[ProductRecive_LOGPO]
                            SET		PURCHBARCODE = '{dtLot.Rows[0]["BARCODE"]}',PURCHFACTOR = '{dtLot.Rows[0]["QTY"]}',PURCHQTY = '{dtLot.Rows[0]["PURCHQTY"]}'
                            WHERE	[TRANSID] = '{RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value}'   ";

                        string resualtupPOBarcodeNew = ConnectionClass.ExecuteSQL_SentServer(AupPOBarcodeNew, IpServerConnectClass.ConSupcAndroid);
                        if (resualtupPOBarcodeNew != "") MsgBoxClass.MsgBoxShow_SaveStatus(resualtupPOBarcodeNew);
                        continue;
                    }

                    string InventFromID = RadGridView_ShowHD.Rows[i].Cells["INVENTLOCATIONFROM"].Value.ToString();
                    string InventToID = RadGridView_ShowHD.Rows[i].Cells["INVENTLOCATIONTO"].Value.ToString();
                    string venderID = RadGridView_ShowHD.Rows[i].Cells["VENDID"].Value.ToString();
                    string buyerID = RadGridView_ShowHD.Rows[i].Cells["BUYERID"].Value.ToString();
                    string emplRecive = RadGridView_ShowHD.Rows[i].Cells["EMPLIDRECIVE"].Value.ToString();

                    //check กรณีสินค้ามี PO
                    string refPO = "", refInventTransID = "", remark = RadGridView_ShowHD.Rows[i].Cells["REMARK"].Value.ToString();
                    if (dtVenderCheck.Rows.Count > 0)
                    {
                        DataRow[] drVenderID = dtVenderCheck.Select($@" SHOW_ID = '{venderID}' ");
                        if (drVenderID.Length > 0)
                        {
                            refPO = RadGridView_ShowHD.Rows[i].Cells["PURCHID"].Value.ToString();
                            refInventTransID = RadGridView_ShowHD.Rows[i].Cells["INVENTTRANSID"].Value.ToString();
                        }
                        else remark += $@" [{RadGridView_ShowHD.Rows[i].Cells["PURCHID"].Value}]";
                    }

                    if (inventFromCheck != InventFromID || inventToCheck != InventToID || venderCheck != venderID || buyerIDCheck != buyerID || poCheck != refPO || emplReciveCheck != emplRecive)
                    {
                        DataRow[] dr = dtVenderHD.Select($@" INVENTLOCATIONFROM = '{InventFromID}' AND INVENTLOCATIONTO = '{InventToID}' AND VENDID = '{venderID}' AND BUYERID = '{buyerID}'  AND PO = '{refPO}' AND EMPLIDRECIVE = '{emplRecive}' ");
                        if (dr.Length == 0)
                        {
                            dtVenderHD.Rows.Add(InventFromID, InventToID, venderID, ConfigClass.ChecKStringForImport(RadGridView_ShowHD.Rows[i].Cells["VENDNAME"].Value.ToString()), buyerID, refPO, emplRecive, RadGridView_ShowHD.Rows[i].Cells["EMPLNAMERECIVE"].Value.ToString());
                        }
                    }

                    //กรณ๊ INVENTONHAND = 1 จะเป็นสินค้าเพิ่ม ไม่มีมีผลใน PO แต่จะรับเข้าคลังปกติ
                    //double qtySend = double.Parse(RadGridView_ShowHD.Rows[i].Cells["QTYRECIVE"].Value.ToString());//หน่วยเต็ม
                    //double qtyAddOn_PO = FindDataLogPOAddOn(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString());


                    double qtyAddOn_PO = 0;//จำนวนที่เกิน
                    double PO_Factor = 1;//อัตราส่วนในใบ PO
                    double PO_Qty = 0;
                    string PO_Barcode = "";//บาร์โค้ดในใบ PO

                    string Recive_ITEMID = RadGridView_ShowHD.Rows[i].Cells["ITEMID"].Value.ToString();// itemIDที่รับ
                    string Recive_INVENTDIMID = RadGridView_ShowHD.Rows[i].Cells["INVENTDIMID"].Value.ToString();// DIMที่รับ
                    string Recive_ITEMBARCODE = RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE"].Value.ToString();//บารโ์ค้ดที่รับ
                    string Recive_EMPLIDRECIVE = RadGridView_ShowHD.Rows[i].Cells["EMPLIDRECIVE"].Value.ToString();// พนง ที่รับ
                    string Recive_EMPLNAMERECIVE = RadGridView_ShowHD.Rows[i].Cells["EMPLNAMERECIVE"].Value.ToString();//พนง ที่รับ
                    string Recive_TRANSID = RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString();//
                    double Recive_QtyAll = double.Parse(RadGridView_ShowHD.Rows[i].Cells["QTYRECIVEALL"].Value.ToString()); //เป็นหน่วยย่อย
                    double Recive_Factor = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["QTY"].Value.ToString());//FacTor

                    //Check กรณีรับเกินทั้งทั้งรายการก่อน
                    if (dtLogPO.Rows[0]["STA_ADDON_ALL"].ToString() == "1")
                    {
                        dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, Recive_ITEMBARCODE,
                                   Double.Parse(dtLogPO.Rows[0]["ADDON_ALL"].ToString()) * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                   InventFromID, InventToID, venderID, buyerID, remark, "1", refPO);
                    }
                    else
                    {
                        if (dtLogPO.Rows.Count > 0)
                        {
                            qtyAddOn_PO = Double.Parse(dtLogPO.Rows[0]["ADDON"].ToString());//หน่วยย่อย
                            PO_Barcode = dtLogPO.Rows[0]["PURCHBARCODE"].ToString();
                            PO_Factor = Double.Parse(dtLogPO.Rows[0]["PURCHFACTOR"].ToString());
                            PO_Qty = Double.Parse(dtLogPO.Rows[0]["PURCHQTY"].ToString());
                        }

                        //กรณีรับสินค้าเกินจาก PO ที่รุบุ
                        if (qtyAddOn_PO > 0)
                        {
                            Recive_QtyAll -= qtyAddOn_PO;

                            double AddOn_Qty1 = qtyAddOn_PO % Recive_Factor; // ส่วนที่เหลือ ลงรายการผ่าน Itembarcode factor 1
                            int AddOn_Qty2 = (int)Math.Floor(qtyAddOn_PO / Recive_Factor); // ส่วนจำนวนเต็มให้ลงรายการผ่าน recive_Barcode

                            if (AddOn_Qty1 > 0)
                            {
                                DataTable dtBarcodeQty1 = ItembarcodeClass.FindItembarcodeAll_ByItemidDim(Recive_ITEMID, Recive_INVENTDIMID);
                                dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, dtBarcodeQty1.Rows[0]["ITEMBARCODE"].ToString(),
                                       AddOn_Qty1 * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                       InventFromID, InventToID, venderID, buyerID, remark, "1", refPO);
                            }
                            if (AddOn_Qty2 > 0)
                            {
                                dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, Recive_ITEMBARCODE,
                                       AddOn_Qty2 * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                       InventFromID, InventToID, venderID, buyerID, remark, "1", refPO);
                            }
                        }

                        //สำหรับการรับสินค้า ที่ <= Qty PO ต้องเช็ค Barcode PO and Recive ถ้าตรงกันลงรายการบัญชีได้//

                        //if Barcode PO != Recive จะต้องเอา QtyReiveAll/QtyFactor 
                        //ในส่วนของจำนวนเต็ม ให้ลงรายการ Barcode ที่ตรงกับ PO
                        //ในส่วนของจำนวนที่เหลือ ให้ลงรายการ Barcode ที่ตรงกับ Recive ได้เลย 

                        double qty = Recive_QtyAll / Recive_Factor;
                        if (qty == PO_Qty)//กรณีที่รับเข้า = PO 
                        {
                            dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, Recive_ITEMBARCODE,
                                qty * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                InventFromID, InventToID, venderID, buyerID, remark, "0", refPO);
                        }
                        else
                        {
                            if (PO_Barcode == "")//กรณีที่มีปัญหาในส่วนของโปรแกรมยังเปน version เดิม ไม่มี BarcodePO 
                            {
                                double PO_old1 = Recive_QtyAll % Recive_Factor; // ส่วนที่เหลือ ลงรายการผ่าน Itembarcode factor 1
                                int PO_old2 = (int)Math.Floor(Recive_QtyAll / Recive_Factor); // ส่วนจำนวนเต็มให้ลงรายการผ่าน recive_Barcode

                                if (PO_old1 > 0)
                                {
                                    DataTable dtBarcodeQty1 = ItembarcodeClass.FindItembarcodeAll_ByItemidDim(Recive_ITEMID, Recive_INVENTDIMID);
                                    dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, dtBarcodeQty1.Rows[0]["ITEMBARCODE"].ToString(),
                                           PO_old1 * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                           InventFromID, InventToID, venderID, buyerID, remark, "0", refPO);

                                }
                                if (PO_old2 > 0)
                                {
                                    dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, Recive_ITEMBARCODE,
                                           PO_old2 * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                           InventFromID, InventToID, venderID, buyerID, remark, "0", refPO);
                                }
                            }
                            else //ในกรณีมีบาร์โค้ดใน po แล้ว
                            {
                                if (Recive_Factor == PO_Factor)//Check Factor Recive + PO เท่ากันก็ลงรายการ บช Barcode ที่รับได้เลย
                                {
                                    dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, Recive_ITEMBARCODE,
                                                            qty * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                                            InventFromID, InventToID, venderID, buyerID, remark, "0", refPO);
                                }
                                else // กรณี Factor ไม่เท่ากัน ต้องเปลี่ยน Barcode and Qty ที่ลงรายการบัญชี
                                {
                                    double qty1 = Recive_QtyAll % PO_Factor; // ส่วนที่เหลือ ลงรายการผ่าน recive_Barcode
                                    int qty2 = (int)Math.Floor(Recive_QtyAll / PO_Factor); // ส่วนจำนวนเต็มให้ลงรายการผ่าน PO_Barcode

                                    if (qty1 > 0)// ส่วนที่เหลือ ลงรายการผ่าน recive_Barcode
                                    {
                                        dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, Recive_ITEMBARCODE,
                                                         qty1 * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                                         InventFromID, InventToID, venderID, buyerID, remark, "0", refPO);
                                    }

                                    if (qty2 > 0)
                                    {
                                        dtItem.Rows.Add(Recive_ITEMID, Recive_INVENTDIMID, PO_Barcode,
                                                         qty2 * (-1), Recive_TRANSID, refInventTransID, Recive_EMPLIDRECIVE, Recive_EMPLNAMERECIVE,
                                                         InventFromID, InventToID, venderID, buyerID, remark, "0", refPO);
                                    }
                                }
                            }
                        }

                    }

                    inventFromCheck = InventFromID;
                    inventToCheck = InventToID;
                    venderCheck = venderID;
                    buyerIDCheck = buyerID;
                    poCheck = refPO;
                    emplReciveCheck = emplRecive;
                }

                if (dtVenderHD.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีรายการค้างลงบัญชี{Environment.NewLine}ลองใหม่อีกครั้ง"); return;
                }

                string resualt = AX_SendData.Save_SPC_INVENTJOURNALTABLE_TYPE2(dtVenderHD, dtItem);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                if (resualt == "") FindData();

            }

            //กรณีสินค้าโรงงาน
            if (_pTypeReport == "1")
            {
                if (radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถออกบิลวันที่ปัจจุบันได้.");
                    return;
                }
                FactoryRecive("1");
            }

            // 3 ระบุ บรรทัด
            if (_pTypeReport == "3")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลงรายการบรรทัด (AX) ?") == DialogResult.No) return;

                DataTable dtSendAX = new DataTable();
                dtSendAX.Columns.Add("InventTransId");
                dtSendAX.Columns.Add("LineNum");
                dtSendAX.Columns.Add("InvoiceId");
                dtSendAX.Columns.Add("PurchId");
                dtSendAX.Columns.Add("TRANSID");
                dtSendAX.Columns.Add("VENDACCOUNT");

                for (int i3 = 0; i3 < RadGridView_ShowHD.Rows.Count; i3++)
                {
                    if (RadGridView_ShowHD.Rows[i3].Cells["SENDLINENUM"].Value.ToString() != "0") continue;
                    if (RadGridView_ShowHD.Rows[i3].Cells["LINENUM"].Value.ToString() == "0") continue;
                    if (RadGridView_ShowHD.Rows[i3].Cells["INVOICEID"].Value.ToString() == "") continue;

                    DataRow[] drLotID = dtSendAX.Select($@" InventTransId = '{RadGridView_ShowHD.Rows[i3].Cells["INVENTTRANSID"].Value}' AND InvoiceId = '{RadGridView_ShowHD.Rows[i3].Cells["INVOICEID"].Value}' ");
                    if (drLotID.Length > 0) continue;

                    dtSendAX.Rows.Add(RadGridView_ShowHD.Rows[i3].Cells["INVENTTRANSID"].Value.ToString(),
                                        RadGridView_ShowHD.Rows[i3].Cells["LINENUM"].Value.ToString(),
                                        RadGridView_ShowHD.Rows[i3].Cells["INVOICEID"].Value.ToString(),
                                        RadGridView_ShowHD.Rows[i3].Cells["PURCHID"].Value.ToString(),
                                        RadGridView_ShowHD.Rows[i3].Cells["TRANSID"].Value.ToString(),
                                        RadGridView_ShowHD.Rows[i3].Cells["VENDID"].Value.ToString());
                }

                if (dtSendAX.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีรายการบรรทัดค้างลงบัญชี{Environment.NewLine}ลองใหม่อีกครั้ง"); return;
                }

                string resualt = AX_SendData.SAVE_SPC_PurchReceiveJourLine(dtSendAX);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                if (resualt == "") FindData();

            }

            //กรณีสินค้าโรงงาน// 4 ระบบสินค้าโรงงานใหม่ แทน Type 1
            if (_pTypeReport == "4")
            {
                if (radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถออกบิลวันที่ปัจจุบันได้.");
                    return;
                }

                if (CheckDimensionVender() == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ผู้จำหน่ายที่เลือก {radDropDownList_Vender.SelectedItem[0]}{Environment.NewLine}ไม่ได้อยู่ภายใต้ความรับผิดชอบของผู้ใช้{Environment.NewLine}ไม่สามารถทำรายการได้");
                    return;
                }

                FactoryReciveType4(staFactory);
            }

        }

        //Check Dimension ก่อนการบันทึกข้อมูลของโรงงาน
        int CheckDimensionVender()
        {
            DataTable dtData = Models.VendTableClass.GetVENDTABLE_All(radDropDownList_Vender.SelectedValue.ToString(), "");
            if (dtData.Rows.Count == 0) return 0;
            else if (dtData.Rows[0]["DIMENSION"].ToString() != SystemClass.SystemDptID) return 0; else return 1;
        }


        //string SetAddOnItem(int iRowDGV, double qty, string barcode, string refPO, string onHand, string refInventTransID, string remark)
        //{
        //    string add = $@"
        //                {RadGridView_ShowHD.Rows[iRowDGV].Cells["ITEMID"].Value }, {RadGridView_ShowHD.Rows[iRowDGV].Cells["INVENTDIMID"].Value }, {barcode},
        //                {qty} * (-1),
        //                {RadGridView_ShowHD.Rows[iRowDGV].Cells["TRANSID"].Value}, {refInventTransID},
        //                {RadGridView_ShowHD.Rows[iRowDGV].Cells["EMPLIDRECIVE"].Value}, {RadGridView_ShowHD.Rows[iRowDGV].Cells["EMPLNAMERECIVE"].Value},
        //                {RadGridView_ShowHD.Rows[iRowDGV].Cells["INVENTLOCATIONFROM"].Value}, {RadGridView_ShowHD.Rows[iRowDGV].Cells["INVENTLOCATIONTO"].Value}, 
        //                {RadGridView_ShowHD.Rows[iRowDGV].Cells["VENDID"].Value}, {RadGridView_ShowHD.Rows[iRowDGV].Cells["BUYERID"].Value}, {remark},{onHand},{refPO}     
        //            ";
        //    return add;
        //}

        //ค้นหาจำนวนที่เกินใน PO

        DataTable FindDataLogPO(string transID)
        {
            //string sql = $@"
            //    SELECT	PURCHQTY,QTYRECIVE,QTYRECIVE - PURCHQTY AS ADDON
            //    FROM	ProductRecive_LOGPO WITH (NOLOCK)
            //    WHERE	QTYRECIVEALL > PURCHQTYALL
            //      AND TRANSID = '{transID}' ";
            //DataTable dt = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid);
            //if (dt.Rows.Count > 0) return Double.Parse(dt.Rows[0]["ADDON"].ToString()); else return 0;
            string sql = $@"
                SELECT	ProductRecive_LOGPO.PURCHQTY,ProductRecive_LOGPO.PURCHQTYALL,ProductRecive_LOGPO.QTYRECIVE,QTYRECIVEALL,
		                ProductRecive_LOGPO.PURCHBARCODE,ProductRecive_LOGPO.PURCHFACTOR,ITEMBARCODE,QTY,
		                ProductRecive_LOGPO.QTYRECIVEALL - ProductRecive_LOGPO.PURCHQTYALL AS ADDON,
                        IIF(ProductRecive_LOGPO.QTYBEFORERECIVE=ProductRecive_LOGPO.PURCHQTYALL,ProductRecive_LOGPO.QTYRECIVEALL,ProductRecive_LOGPO.QTYRECIVEALL - ProductRecive_LOGPO.PURCHQTYALL) AS ADDON_ALL,
                        IIF(ProductRecive_LOGPO.QTYBEFORERECIVE=ProductRecive_LOGPO.PURCHQTYALL,'1','0') AS STA_ADDON_ALL 
                FROM	ProductRecive_LOGPO WITH (NOLOCK)
		                INNER JOIN ProductRecive_DROID WITH (NOLOCK) ON ProductRecive_LOGPO.TRANSID = ProductRecive_DROID.TRANSID
                WHERE	ProductRecive_LOGPO.TRANSID  = '{transID}'
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid);
        }
        //Find LineNum  case 3
        void FindDataLineNum()
        {
            string staAX = "1";
            if (radCheckBox_Apv.Checked == true) staAX = "0";

            //DataTable dtData =
            string date1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string date2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            string buyerID = "";
            if (radCheckBox_Grp.Checked == true) buyerID = radDropDownList_Grp.SelectedValue.ToString();

            string venderIDLoad = "";
            if (radCheckBox_Vender.Checked == true) venderIDLoad = radDropDownList_Vender.SelectedValue.ToString();


            this.Cursor = Cursors.WaitCursor;
            dtData = ConnectionClass.SelectSQL_SentServer(ProductRecive.ProductRecive_Class.GetDetailDroid_LineNum(buyerID, date1,
                       date2, staAX, _pPermission, venderIDLoad), IpServerConnectClass.ConSupcAndroid, 60);
            RadGridView_ShowHD.DataSource = dtData;
            dtData.AcceptChanges();
            if (dtData.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.Default;
        }
        //Count Line Auto Start
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (_pTypeReport != "3") return;

            switch (e.Column.Name)
            {
                case "LINENUM":
                    if (tickSta == "1") // การกดเลือก
                    {
                        if (RadGridView_ShowHD.CurrentRow.Cells["SENDLINENUM"].Value.ToString() != "0") return;
                        string lineNum = RadGridView_ShowHD.CurrentRow.Cells["LINENUM"].Value.ToString();
                        string invoice = RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value.ToString();
                        if (lineNum != "0") return;

                        string po = RadGridView_ShowHD.CurrentRow.Cells["PURCHID"].Value.ToString();
                        if (tickPO != "")
                        {
                            if (po != tickPO)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"รายการที่เลือกมี PO ไม่ตรงกันกับรายการก่อนหน้า{Environment.NewLine}รายการที่เลือกคือ {po}{Environment.NewLine}รายการก่อนหน้าคือ {tickPO}{Environment.NewLine}ลองใหม่อีกครั้ง");
                                return;
                            }
                        }

                        tickPO = po;
                        tickLineNum += 1;
                        dtTransLineNum.Rows.Add(RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString());
                        if (invoice != "") tickInvoice = RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value.ToString();

                        string resualt = UpdateLineNum(RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString(), tickLineNum);
                        if (resualt == "") RadGridView_ShowHD.CurrentRow.Cells["LINENUM"].Value = tickLineNum;
                        else
                        { MsgBoxClass.MsgBoxShowButtonOk_Error(resualt); return; }
                    }
                    break;
                default:
                    break;
            }
        }
        //Reset
        private void RadButton_Reset_Click(object sender, EventArgs e)
        {
            tickLineNum = 999999;
            tickSta = "0";
            radButton_Start.Enabled = true; radButton_Stop.Enabled = false; RadButton_Reset.Enabled = false;
            RadButton_Search.Enabled = true; RadButton_SaveAX.Enabled = true;
            SetReadOnly(false);

            UpdateInVoiceLine($@" LINENUM = '0', ");
        }
        //Start Tick
        private void RadButton_Start_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            tickSta = "1";
            tickLineNum = 0;
            tickPO = "";
            tickInvoice = "";

            if (dtTransLineNum.Rows.Count > 0) dtTransLineNum.Rows.Clear();

            radButton_Start.Enabled = false; radButton_Stop.Enabled = true; RadButton_Reset.Enabled = true;
            RadButton_Search.Enabled = false; RadButton_SaveAX.Enabled = false;
            SetReadOnly(true);
        }
        //Edit By Line
        private void RadGridView_ShowHD_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pTypeReport != "3") return;
            if (tickSta == "1") return;
            if (e.Column.Name != "LINENUM") return;


            cellLineNum = 0;
            string line = RadGridView_ShowHD.CurrentRow.Cells["LINENUM"].Value.ToString();

            if (line.Equals("")) { cellLineNum = 0; return; }
            //if (RadGridView_ShowHD.CurrentRow.Cells["SENDLINENUM"].Value.ToString() != "0") { cellLineNum = line; return; }
            cellLineNum = int.Parse(line);

        }
        //Edit By Line
        private void RadGridView_ShowHD_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pTypeReport != "3") return;
            //if (RadGridView_ShowHD.CurrentRow.Cells["SENDLINENUM"].Value.ToString() != "0") return;
            if (tickSta == "1") return;
            if (e.Column.Name != "LINENUM") return;



            bool isNumerical = double.TryParse(RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString(), out _); //out double number
            if (isNumerical == false)
            {
                RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = "0.00"; return;
            }
            if (RadGridView_ShowHD.CurrentRow.Cells["SENDLINENUM"].Value.ToString() != "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ลงรายการบัญชีแล้ว ไม่สามารถแก้ไขได้{Environment.NewLine}ให้ทำการแก้ไขใน AX เท่านั้น");
                RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = cellLineNum; return;
            }


            int iLineNum = int.Parse(RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString());

            if ((RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value.ToString() == "") && (iLineNum != 0))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถแก้ไขรายการบรรทัดได้โดยวิธีการระบุตัวเลขได้{Environment.NewLine}เนื่องจากยังไม่ระบุเลขที่บิลผู้จำหน่าย{Environment.NewLine}ระบุให้เรียบร้อยก่อนการเรียงบรรทัด{Environment.NewLine}แล้วค่อยมาดำเนินการต่อ");
                RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = cellLineNum.ToString("N2");
                return;
            }

            if (UpdateLineNum(RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString(), iLineNum) != "")
                RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = cellLineNum.ToString("N2");
        }

        private void RadCheckBox_Vender_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Vender.Checked == true)
            {
                LoadVender();
                radDropDownList_Vender.Enabled = true;
            }
            else
            {
                radDropDownList_Vender.Enabled = false;
            }
        }
        //CheckVender
        void LoadVender()
        {
            if ((_pTypeReport == "0") || (_pTypeReport == "3"))
            {
                string dim = "";
                if (radCheckBox_Grp.Checked == true) dim = radDropDownList_Grp.SelectedValue.ToString();
                radDropDownList_Vender.DataSource = Models.VendTableClass.GetVENDTABLE_All("", dim);
                radDropDownList_Vender.ValueMember = "ACCOUNTNUM";
                radDropDownList_Vender.DisplayMember = "SHOW_NAME";
            }

            if (_pTypeReport == "4")
            {
                radDropDownList_Vender.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("60", "", "  ORDER BY REMARK ", "");
                radDropDownList_Vender.ValueMember = "SHOW_ID";
                radDropDownList_Vender.DisplayMember = "DESCSHOW";
            }
        }
        //Load Vender
        private void RadDropDownList_Grp_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (radCheckBox_Vender.Checked == true) LoadVender();
        }

        //End Tick
        private void RadButton_Stop_Click(object sender, EventArgs e)
        {
            tickLineNum = 999999;
            tickSta = "0";
            radButton_Start.Enabled = true; radButton_Stop.Enabled = false; RadButton_Reset.Enabled = false;
            RadButton_Search.Enabled = true; RadButton_SaveAX.Enabled = true;
            SetReadOnly(false);

            if (dtTransLineNum.Rows.Count == 0) return;

            InputData inputData = new InputData("1", "", "ระบุเลขที่บิลผู้จำหน่าย", "")
            {
                pInputData = tickInvoice
            };

            string sqlColumnUpdate;
            if (inputData.ShowDialog() == DialogResult.Yes) sqlColumnUpdate = $@" INVOICEID = '{inputData.pInputData}', ";
            else sqlColumnUpdate = $@" LINENUM = '0', ";

            UpdateInVoiceLine(sqlColumnUpdate);
        }
        //Update Data
        void UpdateInVoiceLine(string sqlColumnUpdate)
        {
            ArrayList sqlUpInvoice = new ArrayList();
            for (int iT = 0; iT < dtTransLineNum.Rows.Count; iT++)
            {
                sqlUpInvoice.Add($@"
                    UPDATE	[ProductRecive_LOGPO]
                    SET		{sqlColumnUpdate}
                            EMPLIDINVOICE='{SystemClass.SystemUserID_M}',EMPLNAMEINVOICE = '{SystemClass.SystemUserName}',DATETIMEINVOICE=GETDATE()
                    WHERE	[TRANSID] = '{dtTransLineNum.Rows[iT]["TRANSID"]}'   ");
            }

            string resualt = ConnectionClass.ExecuteSQL_ArrayMain_SentServer(sqlUpInvoice, IpServerConnectClass.ConSupcAndroid);
            if (resualt == "") FindDataLineNum();
            else
            { MsgBoxClass.MsgBoxShowButtonOk_Error(resualt); return; }
        }
        //INVOICEID+LineNum
        string UpdateLineNum(string transID, int lineNum)
        {
            string sqlIn = $@"
                    UPDATE	[ProductRecive_LOGPO]
                    SET		LINENUM = '{lineNum}',
                            EMPLIDINVOICE='{SystemClass.SystemUserID_M}',EMPLNAMEINVOICE = '{SystemClass.SystemUserName}',DATETIMEINVOICE=GETDATE()
                    WHERE	[TRANSID] = '{transID}'   ";

            return ConnectionClass.ExecuteSQL_SentServer(sqlIn, IpServerConnectClass.ConSupcAndroid);
        }
        //6 รายงานสินค้าที่รับเข้าไม่มีน้ำหนัก/ปริมาตร
        void ReportNoWeight(string pCase)
        {
            if (pCase == "0")
            {
                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;
                radDateTimePicker_D2.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-15), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(0), DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-15);
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);

                radCheckBox_Apv.Text = "เฉพาะไม่มีน้ำหนัก"; radCheckBox_Apv.Checked = true; radCheckBox_Apv.Visible = true;
                RadCheckBox_Date.Text = "เฉพาะไม่มีปริมาตร"; RadCheckBox_Date.Checked = true; RadCheckBox_Date.Visible = true;


                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TRANSID", "TRANSID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIMERECIVE", "เวลารับ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATION", "คลังรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("STATUS", "น้ำหนัก", 50)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("STATUS_CAPACITY", "ปริมาตร", 60)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 180)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEVICENAME", "เครื่อง", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDRECIVE", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMERECIVE", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONNAME", "ชื่อแผนกรับ", 220)));

                DatagridClass.SetCellBackClolorByExpression("STATUS", "STATUS = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("STATUS_CAPACITY", "STATUS_CAPACITY = '0'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                radLabel_Detail.Text = $@"สีแเดง น้ำหนัก >> สินค้ายังไม่มีนำหนัก | สีแเดง ปริมาตร >> สินค้ายังไม่มีปริมาตร(กว้าง*ยาว*สูง)";
            }
            if (pCase == "1")
            {
                this.Cursor = Cursors.WaitCursor;
                string staW = "0", staC = "0";
                if (radCheckBox_Apv.Checked == true) staW = "1";
                if (RadCheckBox_Date.Checked == true) staC = "1";

                dtData = ItembarcodeClass.ReportProductRecive_NoWeight(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), staW, staC);
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }

                this.Cursor = Cursors.Default;
            }
        }
        //7 รายงานที่รับเข้าคลังแต่ไม่ลงรายการบัญชี
        void ReportNotApv(string pCase)
        {
            if (pCase == "0")
            {
                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;
                radDateTimePicker_D2.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-15), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(0), DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-15); radDateTimePicker_D1.Visible = false;
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(0); radDateTimePicker_D2.Visible = false;

                radCheckBox_Apv.Visible = false; RadCheckBox_Date.Visible = false;


                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TRANSID", "TRANSID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIMERECIVE", "เวลารับ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONTO", "คลังรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่ใบสั่งซื้อ", 110)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTTRANSID", "รหัส LOT อ้างอิง AX", 130)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVEALL", "จำนวนรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 180)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEVICENAME", "เครื่อง", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDRECIVE", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMERECIVE", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONNAME", "ชื่อแผนกรับ", 220)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("DIFFDATE", "DAY")));

                DatagridClass.SetCellBackClolorByExpression("DATERECIVE", "DIFFDATE > 3  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("PURCHID", "PURCHID <> '' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                RadGridView_ShowHD.Columns["TRANSID"].IsPinned = true;
                RadGridView_ShowHD.Columns["DATERECIVE"].IsPinned = true;
                RadGridView_ShowHD.Columns["TIMERECIVE"].IsPinned = true;

                radLabel_Detail.Text = $@"สีแดง วันที่รับ >> ค้างลงรายการบัญชีมากกว่า 3 วัน | สีม่วง เลขที่ใบสั่งซื้อ >> มีใบสั่งซื้อแล้ว {Environment.NewLine}ข้อมูลแสดงผลตั้งแต่ 01-06-2023 เป็นต้นไป | DoubleClick จำนวนรับ >> ยกเลิกรายการ [เฉพาะคลังเท่านั้น]";
            }
            if (pCase == "1")
            {
                this.Cursor = Cursors.WaitCursor;
                dtData = ProductRecive_Class.ReportReciveInventNotSendAX();
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }

                this.Cursor = Cursors.Default;
            }
        }
        //8 รายงานเช็ควันรับสินค้ากับวันที่รับเข้าคลัง
        void ReportDateDiffReciveInventWithBA(string pCase)
        {
            if (pCase == "0")
            {
                RadButton_Search.Visible = true;
                radDateTimePicker_D1.Visible = true; RadCheckBox_Date.Visible = true;
                radDateTimePicker_D2.Visible = true;

                RadButton_Search.ButtonElement.ShowBorder = true;

                RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
                radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
                radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-15), DateTime.Now);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-3), DateTime.Now);

                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-15); 
                radDateTimePicker_D2.Value = DateTime.Now.AddDays(-3);  

                radCheckBox_Apv.Visible = false; RadCheckBox_Date.Visible = false;

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TRANSID", "TRANSID")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BA", "เลขที่ใบแจ้งหนี้", 110)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RECIVEDATEBA", "วันที่รับสินค้า", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATERECIVE", "วันที่รับเข้าคลัง", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEAX", "วันที่เข้า AX", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DATEDIFFBA",$@"จำนวนวันรับ{Environment.NewLine}รับสินค้า/รับเข้าคลัง", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DATEDIFFAX", $@"จำนวนวันรับ{Environment.NewLine}รับเข้าคลัง/เข้า AX", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INTERNALINVOICEID", "เลขที่เอกสาร", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONTO", "คลังรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่ใบสั่งซื้อ", 110)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTTRANSID", "รหัส LOT อ้างอิง AX", 130)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCITEMNAME", "ชื่อสินค้า", 320)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECIVE", "จำนวนรับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 180)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "รหัส", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DEVICENAME", "เครื่อง", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLIDRECIVE", "ผู้รับ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMERECIVE", "ชื่อผู้รับ", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนกรับ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSIONNAME", "ชื่อแผนกรับ", 220)));

                DatagridClass.SetCellBackClolorByExpression("DATEDIFFBA", "DATEDIFFBA > 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("DATEDIFFBA", "DATEDIFFBA < 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("BA", "BA = '' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("RECIVEDATEBA", "BA = '' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("DATEDIFFAX", "DATEDIFFAX <> 0 ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                RadGridView_ShowHD.Columns["TRANSID"].IsPinned = true;
                RadGridView_ShowHD.Columns["BA"].IsPinned = true;
                RadGridView_ShowHD.Columns["RECIVEDATEBA"].IsPinned = true;
                RadGridView_ShowHD.Columns["DATERECIVE"].IsPinned = true;
                RadGridView_ShowHD.Columns["DATEAX"].IsPinned = true;
                
                //RadGridView_ShowHD.Columns["STADESC"].IsPinned = true;

                radLabel_Detail.Text = $@"แสดงข้อมูลเฉพาะที่ลงรายการบัญชีแล้วเท่านั้น{Environment.NewLine}สีฟ้า >> ส่งเข้า AX หลังจากวันที่รับ | สีแดง >> เข้าคลังหลังจากรับสินค้ามากกว่า 1 วัน | สีเหลือง >> เข้าคลังก่อนการรับสินค้า | สีม่วง >> ยังไม่ออกใบแจ้งหนี้";
            }
            if (pCase == "1")
            {
                this.Cursor = Cursors.WaitCursor;
                dtData = PurchClass.ProductRecive_GetPurchPO("3", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                RadGridView_ShowHD.DataSource = dtData;
                dtData.AcceptChanges();
                if (dtData.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                    this.Cursor = Cursors.Default;
                    return;
                }

                this.Cursor = Cursors.Default;
            }
        }
    }
}
