﻿//CheckOK
using System.Data;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.ProductRecive
{
    class ProductRecive_Class
    {
        //ค้นหาข้อมูลบิลทั้งหมดตามวันที่
        public static DataTable GetDetailTRK(string typeBill, string date1, string date2, string pCon)
        {
            string sql = $@"
                SELECT	DISTINCT 
                        [TRKID],[VEHICLEID],[VEHICLETYPE],[TYPEBILL],[LOCATIONMNID],[LOCATIONMNNAME],
		                [TABNAME],[EMPLID],[EMPLNAME],[REMARKS],
		                [LATITUDE],[LONGTITUDE],CONVERT(VARCHAR,[DATETIMECAP],23) AS [DATETIMECAP], 
		                [CREATEBA_EMPLID],[CREATEBA_EMPLNAME],[CREATEBA_INVOICE],[CREATEBA_AMOUNT],[CREATEBA_DATETIME],[CREATEBA_STATUS],[CREATEBA_PRINTNUM],
                        [CUSTID],[CUSTNAME],[CUSTRATES]
                FROM	[ProductRecive_TRK] WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,[DATETIMECAP],23) BETWEEN '{date1}' AND '{date2}'
                        AND TYPEBILL = '{typeBill}' {pCon}
                ORDER BY [DATETIMECAP] DESC,[TRKID]
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid, 60);
        }
        //ค้นหารายละเอียดบิล trk
        public static DataTable GetDetailTRKByBill(string TRKID)
        {
            string sql = $@"
                SELECT	[TRKID],[LOCATIONMNID],[LOCATIONMNNAME],
		                [TABNAME],[EMPLID],[EMPLNAME],[REMARKS],[INVOICE],[AMOUNT],
		                CONVERT(VARCHAR,[DATETIMECAP],25) AS [DATETIMECAP],[PICSERVER], CONVERT(VARCHAR,[DATETIMECAP],23) AS [DATECAP],
		                [CREATEBA_INVOICE],[CREATEBA_AMOUNT],[CREATEBA_STATUS],[CREATEBA_PRINTNUM]
                FROM	[ProductRecive_TRK] WITH (NOLOCK)
                WHERE	TRKID = '{TRKID}'
                ORDER BY [DATETIMECAP]  
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid, 60);
        }
        //ค้นหาข้อมูลบิลทั้งหมด BA
        public static DataTable GetDetailBRP(string date, string docno)
        {
            string conOperation = "";
            if ((date != "") && (docno != "")) conOperation = " AND ";

            string sql = $@"
                SELECT	DISTINCT BADOCNO,DOCNO AS TRKID,CONVERT(VARCHAR,DOCDATE,23) AS DOCDATE,LICENSEPLATE,VENDER,REMARK AS REMARKS,INVOICE,
                        CREATEUSER AS EMPLID,SPC_NAME AS EMPLNAME,NUM AS LOCATIONMNID,DESCRIPTION AS LOCATIONMNNAME
                FROM	[TRAN_BILLACCEPTPIC] WITH (NOLOCK)
		                INNER JOIN EMPLTABLE WITH (NOLOCK) ON [TRAN_BILLACCEPTPIC].CREATEUSER = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = 'SPC'
		                INNER JOIN DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = 'SPC' AND DIMENSIONCODE = '0'
                WHERE	{date} {conOperation} {docno}
                ORDER BY CONVERT(VARCHAR,DOCDATE,23) DESC,DOCNO
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.Con112SRV, 60);
        }
        //ค้นหารายละเอียดบิล BA
        public static DataTable GetDetailBRPByBill(string docno)
        {
            string sql = $@"
                SELECT	BADOCNO,DOCNO,CONVERT(VARCHAR,DOCDATE,25) AS DATETIMECAP,LICENSEPLATE,VENDER,REMARK AS REMARKS,INVOICE,CREATEUSER AS EMPLID,PATH AS PICSERVER
                FROM	[TRAN_BILLACCEPTPIC] WITH (NOLOCK)
                WHERE	BADOCNO = '{docno}' OR DOCNO = '{docno}' OR INVOICE = '{docno}'
                ORDER BY LINENUM 
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.Con112SRV, 60);
        }
        //Update Invoice + Amount
        public static string ProductRecive_TRK_UpdateInvoice(string trkID, string invoice, double amount)
        {
            string sql = $@"
                    UPDATE  ProductRecive_TRK 
                    SET     [CREATEBA_EMPLID] = '{SystemClass.SystemUserID}',[CREATEBA_EMPLNAME] = '{SystemClass.SystemUserName}',
                            [CREATEBA_INVOICE] = '{invoice}',[CREATEBA_AMOUNT] = '{amount}',
                            [CREATEBA_DATETIME] = CONVERT(VARCHAR,GETDATE(),25) ,[CREATEBA_STATUS] = '1'
                    WHERE   [TRKID] = '{trkID}' ";
            return sql;
        }
        //Update พิมพ์ซ้ำ
        public static string ProductRecive_TRK_UpdatePrint(string trkID)
        {
            string sqlPrintNum = $@"
                    UPDATE  ProductRecive_TRK
                    SET     [CREATEBA_PRINTNUM] = CREATEBA_PRINTNUM + 1 
                    WHERE   [TRKID] = '{trkID}' ";
            return ConnectionClass.ExecuteSQL_SentServer(sqlPrintNum, IpServerConnectClass.ConSupcAndroid);
        }
        //Set data In grid for print
        public static void SetDGVTrkForPrint(RadGridView RadGridView_ShowHD, DataTable dt)
        {
            if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();

            int iRows = dt.Rows.Count / 2;
            if (dt.Rows.Count % 2 != 0) iRows += 1;

            int ii = 0;
            for (int iR = 0; iR < iRows; iR++)
            {
                AddRows(RadGridView_ShowHD, dt, ii, 1000);
                ii += 1;
                if (ii <= dt.Rows.Count - 1) AddRows(RadGridView_ShowHD, dt, ii, iR);
                ii += 1;
            }
        }
        //Add Rows
        public static void AddRows(RadGridView RadGridView_ShowHD, DataTable dt, int ii, int iR)
        {
            string reciveDate = dt.Rows[ii]["DATETIMECAP"].ToString();
            string path = dt.Rows[ii]["PICSERVER"].ToString();

            if (iR == 1000)
            {
                RadGridView_ShowHD.Rows.Add(ImageClass.ScaleImageDataGridViewSendScaleDrawTextInImage(350, 400, path, reciveDate), path, "", "");
            }
            else
            {
                RadGridView_ShowHD.Rows[iR].Cells["IMG2"].Value = ImageClass.ScaleImageDataGridViewSendScaleDrawTextInImage(350, 400, path, reciveDate);
                RadGridView_ShowHD.Rows[iR].Cells["P_IMG2"].Value = path;
            }
        }
        //Droid
        public static string GetDetailDroid(string buyerID, string pCase, string date1, string date2, string staAX, string pPermission, string venderID)
        {
            string conSta = "  ";
            if (staAX == "0") conSta = " AND SENDAXSTA  IN ('0') ";

            string conDpt = $@" AND DIMENSION = '{SystemClass.SystemDptID}' ";
            if (SystemClass.SystemDptID == "D030") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D008') ";

            if (pPermission == "0") conDpt = "";
            else
            {
                if ((SystemClass.SystemDptID == "D156") || (SystemClass.SystemComMinimart == "1")) conDpt = "";
                if (SystemClass.SystemDptID == "D015") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}') ";
                if (SystemClass.SystemDptID == "D051") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D151') ";
                if (SystemClass.SystemDptID == "D151") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D051') ";
            }

            string conBuyerID = "";
            if (buyerID != "") conBuyerID = $@" AND BUYERID = '{buyerID}' ";

            string conVenderID = "";
            if (venderID != "") conVenderID = $@" AND VENDID = '{venderID}' ";
            if (pCase == "0")
            {
                string sqlSelect = $@"
                SELECT	[ProductRecive_DROID].[TRANSID],
                        CONVERT(VARCHAR,[DATETIMERECIVE],23) AS [DATERECIVE],CONVERT(VARCHAR,[DATETIMERECIVE],24) AS [TIMERECIVE],[INVENTLOCATIONFROM],[INVENTLOCATIONTO],[EMPLIDRECIVE],[EMPLNAMERECIVE],
                        [DIMENSION],[DIMENSIONNAME],[DEVICENAME],
                        [VENDID],[VENDNAME],[ProductRecive_DROID].[PURCHID],[ProductRecive_DROID].[INVENTTRANSID],
                        ISNULL([ProductRecive_LOGPO].PURCHQTYALL,0) AS SUMPO ,
						IIF([ProductRecive_DROID].[INVENTTRANSID] != '',([ProductRecive_DROID].[QTYRECIVE]*[QTY])-ISNULL([ProductRecive_LOGPO].PURCHQTYALL,0),'0') AS QTYDIFF,
                        [ITEMSTA],[ITEMID],ProductRecive_DROID.[INVENTDIMID],[ITEMBARCODE],[SPCITEMNAME],
                        [QTY],[UNITID],[ProductRecive_DROID].[QTYRECIVE],[QTYSCANDOC],[QTYDIFF],([ProductRecive_DROID].[QTYRECIVE]*[QTY]) AS [QTYRECIVEALL],
                        [ProductRecive_DROID].[REMARK],[REMARKDIFF],
                        ProductRecive_DROID.[EMPLIDUPDATE],ProductRecive_DROID.[EMPLNAMEUPDATE],ProductRecive_DROID.[DATETIMEUPDATE],BUYERID,BUYERNAME,
                        [SENDAXSTA],[EMPLIDSENDAX],[EMPLNAMESENDAX],[DATETIMESENDAX],
                        REPLACE(INVENTDIM.CONFIGID,'''','') AS CONFIGID,REPLACE(INVENTDIM.INVENTSIZEID,'''','') AS INVENTSIZEID,REPLACE(INVENTDIM.INVENTCOLORID,'''','') AS INVENTCOLORID,
                        ISNULL(SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,'0') AS STA_ACTIVE ,
                        ISNULL([ProductRecive_LOGPO].PURCHBARCODE,'') AS PURCHBARCODE,ISNULL([ProductRecive_LOGPO].PURCHFACTOR ,1) AS PURCHFACTOR,
                        ISNULL([ProductRecive_LOGPO].INVOICEID,'') AS INVOICEID,ISNULL([ProductRecive_LOGPO].LINENUM,'') AS LINENUM,ProductRecive_LOGPO.QTYBEFORERECIVE 
                  FROM	[ProductRecive_DROID] WITH (NOLOCK)
                        INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK) ON [ProductRecive_DROID].INVENTDIMID = INVENTDIM.INVENTDIMID 
                        LEFT OUTER JOIN [ProductRecive_LOGPO]  WITH (NOLOCK) ON [ProductRecive_DROID].TRANSID = [ProductRecive_LOGPO].TRANSID 
                            AND [ProductRecive_DROID].[INVENTTRANSID] = [ProductRecive_LOGPO].[INVENTTRANSID] 
                        LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON [ProductRecive_DROID].[VENDID] = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID 
                            AND TYPE_CONFIG = '57' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1' 
                        LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail A60 WITH (NOLOCK) ON ProductRecive_DROID.VENDID = A60.SHOW_ID
			                AND A60.TYPE_CONFIG = '60' AND A60.STA = '1' 
                  WHERE	CONVERT(VARCHAR,[DATETIMERECIVE],23) BETWEEN '{date1}' AND '{date2}'
                        AND ITEMSTA = '1' 
                        AND ISNULL(A60.SHOW_ID,'0') = '0' 
                        {conSta} {conDpt} {conBuyerID} {conVenderID}
                  ORDER BY [ITEMID],ProductRecive_DROID.[INVENTDIMID],[ITEMBARCODE],[DATETIMERECIVE] ";
                return sqlSelect;
            }
            else
            {
                return $@"
                    SELECT	[ITEMBARCODE]
                    FROM	SupcAndroid.dbo.[ProductRecive_DROID] WITH (NOLOCK)
                    WHERE	CONVERT(VARCHAR,[DATETIMERECIVE],23) BETWEEN '{date1}' AND '{date2}'
		                    AND ITEMSTA = '1'   
                            {conSta} {conDpt} {conBuyerID}
                    GROUP BY [ITEMBARCODE] ";
            }
        }
        //ค้นหาสินค้าโรงงานออก Invoice
        public static DataTable GetDatailFactory(string pCase, string pDate, string purchID, string DPT = "")
        {
            string conPurchID = "";

            if (purchID != "")
            {
                if (purchID.Contains("MN")) conPurchID = $@" WHERE	PURCHID = '{purchID}' "; else conPurchID = $@" WHERE	VENDID = '{purchID}' ";
            }

            string sql = "";
            switch (pCase)
            {
                case "0":
                    #region "CODE"
                    sql = $@"
                    SELECT	ISNULL(PD,'') AS MNPR,ISNULL(PURCHID,'') AS PURCHID,IIF(ISNULL(PURCHID,'')='',COST_PRICE,PRICECOST) AS COST_PRICE,IIF(ISNULL(PURCHID,'')='',COST_ALL,CNQTY) AS COST_ALL,
		                    DATERECIVE,INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME,ITEMBARCODE,SPCITEMNAME,QTYRECIVE,UNITID  ,TMP.ITEMID,TMP.INVENTDIMID,QTY,STA_COST
                    FROM	(
		                    SELECT	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) AS DATERECIVE,
				                    INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME,ProductRecive_DROID.ITEMBARCODE,SPCITEMNAME,
				                    SUM(ProductRecive_DROID.QTYRECIVE) AS QTYRECIVE,ProductRecive_DROID.UNITID AS UNITID,
				                    CONVERT(DECIMAL(18,2),IIF((ISNULL(TMP_COST.COSTPRICE, 0)) = 0,(ISNULL([OrderProductPlastic_INGREDIENTTABLE].COSTPRICE, 0)),(ISNULL(TMP_COST.COSTPRICE, 0)))) AS COST_PRICE,
				                    (SUM(ProductRecive_DROID.QTYRECIVE)*CONVERT(DECIMAL(18,2),IIF((ISNULL(TMP_COST.COSTPRICE, 0)) = 0,(ISNULL([OrderProductPlastic_INGREDIENTTABLE].COSTPRICE, 0)),(ISNULL(TMP_COST.COSTPRICE, 0))))) AS COST_ALL
				                    ,ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,ProductRecive_DROID.QTY,IIF(ISNULL(TMP_COST.COSTPRICE, 0) = 0,'0','1')  AS STA_COST
		                    FROM	ProductRecive_DROID WITH (NOLOCK)
				                    INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
					                    AND TYPE_CONFIG = '60' AND STA = '1'
				                    LEFT OUTER JOIN [OrderProductPlastic_INGREDIENTTABLE]  WITH (NOLOCK) ON ProductRecive_DROID.ITEMBARCODE = [OrderProductPlastic_INGREDIENTTABLE].BARCODE
				                    LEFT OUTER JOIN 
				                    (
					                    SELECT	ITEMBARCODE,ITEMID,INVENTDIMID,COSTPRICE
					                    FROM	ProductRecive_FACTORY WITH (NOLOCK)
					                    WHERE	CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23) = '{pDate}' 
                                                AND QTYSEND > 0 AND ITEMSTA = 1 
					                    GROUP BY ITEMBARCODE,ITEMID,INVENTDIMID,COSTPRICE
				                    )TMP_COST ON ProductRecive_DROID.ITEMBARCODE = TMP_COST.ITEMBARCODE

				
		                    WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) = '{pDate}' AND ITEMSTA = '1' 

		                    GROUP BY CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23),
				                    INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME,ProductRecive_DROID.ITEMBARCODE,SPCITEMNAME,
				                    ProductRecive_DROID.UNITID
				                    ,CONVERT(DECIMAL(18,2),IIF((ISNULL(TMP_COST.COSTPRICE, 0)) = 0,(ISNULL([OrderProductPlastic_INGREDIENTTABLE].COSTPRICE, 0)),(ISNULL(TMP_COST.COSTPRICE, 0))))
				                    ,ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,ProductRecive_DROID.QTY,IIF(ISNULL(TMP_COST.COSTPRICE, 0) = 0,'0','1')
		                    )TMP 
		                    LEFT OUTER JOIN [SHOP24HRS].[dbo].[SHOP_VENDERPR]  WITH (NOLOCK) ON TMP.DATERECIVE = [SHOP_VENDERPR].ReciveDate AND [SHOP_VENDERPR].INVENTLOCATIONID = 'MN000'
			                    AND TMP.VENDID = [SHOP_VENDERPR].INVOICEACCOUNT AND TMP.ITEMBARCODE = [SHOP_VENDERPR].SPC_ITEMBARCODE
                              {conPurchID}
                    ORDER BY BUYERID,VENDID,ITEMBARCODE ";
                    #endregion
                    break;
                case "1":
                    sql = $@"
                SELECT	COUNT(SENDAXSTA) AS NOTSANDAX
                FROM	ProductRecive_DROID WITH (NOLOCK)
		                INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
			                AND TYPE_CONFIG = '60' AND STA = '1' AND ITEMSTA != '0' 
                WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) = '{pDate}'
		                AND	SENDAXSTA = '0' ";
                    break;
                case "2"://เปรียบเทียบการรับส่งสินค้าระหว่างโรงงาน/คลัง A
                    #region CODE                  
                    sql = $@"
                    SELECT	IIF(ISNULL(T1.ITEMID,'') = '',T2.DATE,T1.DATE) AS TRANSDATE,
		                    IIF(ISNULL(T1.ITEMID,'') = '',T2.VENDID,T1.VENDID) AS VENDID,
		                    IIF(ISNULL(T1.ITEMID,'') = '',T2.VENDNAME,T1.VENDNAME) AS VENDNAME,
		                    IIF(ISNULL(T1.BUYERID,'') = '',ISNULL(T2.BUYERID,''),ISNULL(T1.BUYERID,'')) AS BUYERID,
		                    IIF(ISNULL(T1.BUYERNAME,'') = '',ISNULL(T2.BUYERNAME,''),ISNULL(T1.BUYERNAME,'')) AS BUYERNAME,
		                    IIF(ISNULL(T1.ITEMID,'') = '',T2.ITEMID,T1.ITEMID) AS ITEMID,
		                    IIF(ISNULL(T1.ITEMID,'') = '',T2.INVENTDIMID,T1.INVENTDIMID) AS INVENTDIMID,
		                    IIF(ISNULL(T1.ITEMID,'') = '',T2.SPCITEMNAME,T1.SPCITEMNAME) AS SPCITEMNAME,

		                    'ส่ง : ' + ISNULL(T1.ITEMBARCODE,'') + CHAR(10) + 'รับ : ' + ISNULL(T2.ITEMBARCODE,'')  AS ITEMBARCODE_SEND,
		                    ISNULL(T2.ITEMBARCODE,'') AS ITEMBARCODE_RECIVE,
				
		                    ISNULL(T1.QTY,'0') AS QTY_SEND,
		                    ISNULL(T1.FACTOR,'0') AS FACTOR_SEND,
		                    ISNULL(T1.UNITID,'') AS UNITID_SEND,
		                    (ISNULL(T1.QTY,'0'))*(ISNULL(T1.FACTOR,'0')) AS SUM_SEND,

		                    ISNULL(T2.QTY,'0') AS QTY_RECIVE,
		                    ISNULL(T2.FACTOR,'0') AS FACTOR_RECIVE,
		                    ISNULL(T2.UNITID,'') AS UNITID_RECIVE ,
		                    (ISNULL(T2.QTY,'0'))*(ISNULL(T2.FACTOR,'0')) AS SUM_RECIVE,

		                    (ISNULL(T1.QTY,'0'))*(ISNULL(T1.FACTOR,'0'))-(ISNULL(T2.QTY,'0'))*(ISNULL(T2.FACTOR,'0')) AS SUM_DIFF,
                            IIF(ISNULL(T1.ITEMID,'') = '','ไม่มีการส่งของ',T1.STA_TYPE) AS STA_TYPE

                    FROM	
		                    (
		                    SELECT	CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23) AS DATE,
				                    VENDID,VENDNAME,'' AS BUYERID,'' AS BUYERNAME,
				                    ITEMID,INVENTDIMID,ITEMBARCODE,SPCITEMNAME,SUM(QTYSEND) AS QTY,QTY AS FACTOR,UNITID,IIF(TYPE=1,'โรงงานป่าคลอก','โรงงานสาขาใหญ่') AS STA_TYPE  
		                    FROM	ProductRecive_FACTORY WITH (NOLOCK)
		                    WHERE	CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23) BETWEEN '{pDate}' AND  '{purchID}' AND ITEMSTA = '1' 
		                    GROUP BY CONVERT(VARCHAR,ProductRecive_FACTORY.CREATEDATETIME,23),VENDID,VENDNAME,ITEMBARCODE,SPCITEMNAME,ITEMID,INVENTDIMID,QTY,UNITID,IIF(TYPE=1,'โรงงานป่าคลอก','โรงงานสาขาใหญ่') 
		                    )T1
		                    FULL OUTER  JOIN
		                    (
		                    SELECT	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) AS DATE,
				                    VENDID,VENDNAME,BUYERID,BUYERNAME,ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,ProductRecive_DROID.ITEMBARCODE,SPCITEMNAME,
				                    SUM(ProductRecive_DROID.QTYRECIVE) AS QTY,QTY AS FACTOR,ProductRecive_DROID.UNITID AS UNITID
		                    FROM	ProductRecive_DROID WITH (NOLOCK)
				                    INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
					                    AND TYPE_CONFIG = '60' AND STA = '1'
				
		                    WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) BETWEEN '{pDate}' AND  '{purchID}'  AND ITEMSTA = '1' 

		                    GROUP BY CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23),
				                    VENDID,VENDNAME,BUYERID,BUYERNAME,ProductRecive_DROID.ITEMBARCODE,SPCITEMNAME,
				                    ProductRecive_DROID.UNITID
	 
				                    ,ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,QTY 
		                    )T2 ON T1.ITEMID = T2.ITEMID AND T1.INVENTDIMID = T2.INVENTDIMID AND T1.DATE = T2.DATE

                    ORDER BY IIF(ISNULL(T1.BUYERID,'') = '',ISNULL(T2.BUYERID,''),ISNULL(T1.BUYERID,'')),ITEMID,INVENTDIMID,SPCITEMNAME ";
                    #endregion
                    break;
                case "3":
                    sql = $@"
                    SELECT	DOCNO,VENDER,PATH
                    FROM	SPC_TRANSPORT.DBO.TRAN_BILLACCEPTPIC WITH (NOLOCK)
                    WHERE	PATH LIKE '%_{purchID}.jpg'
		                    AND DOCDATE = '{pDate}' ";
                    break;
                case "4"://ลงรายการบัญชี HD
                    sql = $@"
                        SELECT	INVENTLOCATIONFROM,INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME, EMPLIDRECIVE
                        FROM	ProductRecive_DROID WITH (NOLOCK)
		                        INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
			                        AND TYPE_CONFIG = '60' AND STA = '1'			
                        WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) = '{pDate}' AND ITEMSTA = '1'  AND SENDAXSTA = '0'
                                AND VENDID = '{purchID}'
                        GROUP BY INVENTLOCATIONFROM,INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME, EMPLIDRECIVE 
                        ORDER BY INVENTLOCATIONFROM,INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME, EMPLIDRECIVE ";
                    break;
                case "5"://ลงรายการบัญชี DT
                    sql = $@"
                        SELECT	TRANSID,INVENTLOCATIONFROM,INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME,EMPLIDRECIVE,EMPLNAMERECIVE,ProductRecive_DROID.ITEMBARCODE,SPCITEMNAME,
		                        (ProductRecive_DROID.QTYRECIVE) AS QTYRECIVE,ProductRecive_DROID.UNITID AS UNITID,
		                        ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,ProductRecive_DROID.QTY ,ProductRecive_DROID.REMARK

                        FROM	ProductRecive_DROID WITH (NOLOCK)
		                        INNER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
			                        AND TYPE_CONFIG = '60' AND STA = '1'
				
                        WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) = '{pDate}' AND ITEMSTA = '1' AND SENDAXSTA = '0'
                                AND VENDID = '{purchID}' 
                        ORDER BY INVENTLOCATIONFROM,INVENTLOCATIONTO,VENDID,VENDNAME,BUYERID,BUYERNAME, EMPLIDRECIVE ";
                    break;
                case "6":
                    string GroupSub = "";
                    if (purchID != "") GroupSub = $@"AND GROUPID = '{purchID}'";
                    sql = $@"
            SELECT	ITEMBARCODE, SPC_ITEMNAME, UNITID, SHOP_MNOI_DT.ITEMID, SHOP_MNOI_DT.INVENTDIMID,
		            QTYORDER, ISNULL(QTYRECIVEALL,0) AS QTYRECIVEALL,
		            CONVERT(NVARCHAR,DATE_RECIVE,23) AS DATE_RECIVE
            FROM    SHOP24HRS.DBO.SHOP_MNOI_HD WITH(NOLOCK)
                    INNER JOIN SHOP24HRS.DBO.SHOP_MNOI_DT WITH(NOLOCK) ON SHOP_MNOI_HD.DOCNO = SHOP_MNOI_DT.DOCNO
		            LEFT JOIN (
			                SELECT	ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,ProductRecive_DROID.QTY AS FACTOR,
					                SUM(ProductRecive_DROID.QTYRECIVE) AS QTYRECIVE,
					                SUM(ProductRecive_DROID.QTY*ProductRecive_DROID.QTYRECIVE) AS  QTYRECIVEALL 
			                FROM	SupcAndroid.dbo.ProductRecive_DROID WITH (NOLOCK)
					                INNER JOIN SHOP24HRS.DBO.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON ProductRecive_DROID.VENDID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
						                AND TYPE_CONFIG = '60' AND STA = '1'
			                WHERE	CONVERT(VARCHAR,ProductRecive_DROID.DATETIMERECIVE,23) =  '{pDate}' AND ITEMSTA = '1' 
			                GROUP BY 	ProductRecive_DROID.ITEMID,ProductRecive_DROID.INVENTDIMID,ProductRecive_DROID.QTY) AS PRODUCT
			            ON SHOP_MNOI_DT.ITEMID = PRODUCT.ITEMID 
			            AND SHOP_MNOI_DT.INVENTDIMID = PRODUCT.INVENTDIMID
            WHERE   DATE_RECIVE = '{pDate}'
                    AND STA_DOC = '1'
                    AND DPTID = '{DPT}'
                    {GroupSub} 
            ORDER BY ITEMBARCODE ";
                    break;
                default:
                    break;
            }
            if (pCase == "3") return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.Con112SRV);
            else return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid);
        }
        //Droid Line Num
        public static string GetDetailDroid_LineNum(string buyerID, string date1, string date2, string staAX, string pPermission, string venderID)
        {
            string conSta = "  ";
            if (staAX == "0") conSta = " AND ISNULL(SENDLINENUM,'0 ')  IN ('0') ";

            string conDpt = $@" AND DIMENSION = '{SystemClass.SystemDptID}' ";
            if (SystemClass.SystemDptID == "D030") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D008') ";

            if (pPermission == "0") conDpt = "";
            if ((SystemClass.SystemDptID == "D156") || (SystemClass.SystemComMinimart == "1")) conDpt = "";

            string conBuyerID = "";
            if (buyerID != "") conBuyerID = $@" AND BUYERID = '{buyerID}' ";

            string conVenderID = "";
            if (venderID != "") conVenderID = $@" AND VENDID = '{venderID}' ";


            string sqlSelect = $@"
                SELECT	[ProductRecive_DROID].[TRANSID],
                        CONVERT(VARCHAR,[DATETIMERECIVE],23) AS [DATERECIVE],CONVERT(VARCHAR,[DATETIMERECIVE],24) AS [TIMERECIVE],[INVENTLOCATIONFROM],[INVENTLOCATIONTO],[EMPLIDRECIVE],[EMPLNAMERECIVE],
                        [DIMENSION],[DIMENSIONNAME],[DEVICENAME],
                        [VENDID],[VENDNAME],[ProductRecive_DROID].[PURCHID],[ProductRecive_DROID].[INVENTTRANSID],
                        ISNULL([ProductRecive_LOGPO].PURCHQTYALL,0) AS SUMPO ,
						IIF([ProductRecive_DROID].[INVENTTRANSID] != '',([ProductRecive_DROID].[QTYRECIVE]*[QTY])-ISNULL([ProductRecive_LOGPO].PURCHQTYALL,0),'0') AS QTYDIFF,
                        [ITEMSTA],[ITEMID],ProductRecive_DROID.[INVENTDIMID],[ITEMBARCODE],[SPCITEMNAME],
                        [QTY],[UNITID],[ProductRecive_DROID].[QTYRECIVE],[QTYSCANDOC],[QTYDIFF],([ProductRecive_DROID].[QTYRECIVE]*[QTY]) AS [QTYRECIVEALL],
                        [ProductRecive_DROID].[REMARK],[REMARKDIFF],
                        ProductRecive_DROID.[EMPLIDUPDATE],ProductRecive_DROID.[EMPLNAMEUPDATE],ProductRecive_DROID.[DATETIMEUPDATE],BUYERID,BUYERNAME,
                        [SENDAXSTA],[EMPLIDSENDAX],[EMPLNAMESENDAX],[DATETIMESENDAX],
                        REPLACE(INVENTDIM.CONFIGID,'''','') AS CONFIGID,REPLACE(INVENTDIM.INVENTSIZEID,'''','') AS INVENTSIZEID,REPLACE(INVENTDIM.INVENTCOLORID,'''','') AS INVENTCOLORID,
                        ISNULL(SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,'0') AS STA_ACTIVE ,
                        ISNULL([ProductRecive_LOGPO].PURCHBARCODE,'') AS PURCHBARCODE,ISNULL([ProductRecive_LOGPO].PURCHFACTOR ,1) AS PURCHFACTOR,
                        ISNULL([ProductRecive_LOGPO].INVOICEID,'') AS INVOICEID,ISNULL([ProductRecive_LOGPO].LINENUM,'') AS LINENUM,
                        SENDLINENUM,EMPLIDSENDLINENUM,EMPLNAMESENDLINENUM,DATETIMESENDLINENUM
                  FROM	[ProductRecive_DROID] WITH (NOLOCK)
                        INNER JOIN SHOP2013TMP.dbo.INVENTDIM WITH (NOLOCK) ON [ProductRecive_DROID].INVENTDIMID = INVENTDIM.INVENTDIMID 
                        LEFT OUTER JOIN [ProductRecive_LOGPO]  WITH (NOLOCK) ON [ProductRecive_DROID].TRANSID = [ProductRecive_LOGPO].TRANSID 
                            AND [ProductRecive_DROID].[INVENTTRANSID] = [ProductRecive_LOGPO].[INVENTTRANSID] 
                        LEFT OUTER JOIN SHOP24HRS.dbo.SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON [ProductRecive_DROID].[VENDID] = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID 
                            AND TYPE_CONFIG = '57' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1' 
                  WHERE	CONVERT(VARCHAR,[DATETIMERECIVE],23) BETWEEN '{date1}' AND '{date2}'
                        AND ITEMSTA = '1' AND SENDAXSTA != '0'
                        {conSta} {conDpt} {conBuyerID} {conVenderID}
                  ORDER BY  ISNULL([ProductRecive_LOGPO].INVOICEID,''),ISNULL([ProductRecive_LOGPO].LINENUM,''),
                        [ProductRecive_DROID].[PURCHID],[ITEMID],ProductRecive_DROID.[INVENTDIMID],[QTY],[ITEMBARCODE],[DATETIMERECIVE] ";
            return sqlSelect;

        }
        //7 รายงานที่รับเข้าคลังแต่ไม่ลงรายการบัญชี
        public static DataTable ReportReciveInventNotSendAX()
        {
            string conDpt = $@" AND BUYERID = '{SystemClass.SystemDptID}' ";
            if (SystemClass.SystemDptID == "D030") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D008') ";//รับสินค้า+ขนสินค้า
            if ((SystemClass.SystemDptID == "D156") || (SystemClass.SystemComMinimart == "1")) conDpt = "";
            if (SystemClass.SystemDptID == "D015") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}') ";//โรงหมี่
            if (SystemClass.SystemDptID == "D051") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D151') ";//บัญชี
            if (SystemClass.SystemDptID == "D151") conDpt = $@" AND DIMENSION IN ('{SystemClass.SystemDptID}','D051') ";//บัญชี

            string sql = $@"
                SELECT	DATEDIFF(DAY,CONVERT(VARCHAR,DATETIMERECIVE,25),GETDATE()) AS DIFFDATE,
		                CONVERT(VARCHAR,DATETIMERECIVE,23) AS DATERECIVE,CONVERT(VARCHAR,DATETIMERECIVE,24) AS TIMERECIVE,
		                TRANSID,INVENTLOCATIONTO,ITEMBARCODE,SPCITEMNAME,QTYRECIVE AS QTYRECIVEALL,UNITID,EMPLNAMERECIVE,DIMENSIONNAME,DIMENSION,
		                VENDID,VENDNAME,
		                PURCHID,INVENTTRANSID,BUYERID,BUYERNAME,DEVICENAME,EMPLIDRECIVE,EMPLNAMERECIVE
                FROM	ProductRecive_DROID WITH (NOLOCK)
                WHERE	ITEMSTA = '1' AND SENDAXSTA = '0'
		                AND CONVERT(varchar,DATETIMERECIVE,23) >= '2023-06-01' 
                        {conDpt}
                ORDER BY CONVERT(VARCHAR,DATETIMERECIVE,25)
            ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConSupcAndroid);
        }
    }
}
