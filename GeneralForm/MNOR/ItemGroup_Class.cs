﻿//CheckOK
using PC_Shop24Hrs.Controllers;
using System.Data;


namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public class Var_ITEMGROUP
    {
        public string ITEMGROUPID { get; set; } = "";
        public string ITEMGROUPNAME { get; set; } = "";
        public string ITEMGROUPDEPT { get; set; } = "";
        public string ITEMGROUPTYPE { get; set; } = "";
        public string ITEMGROUPTYPEREMARK { get; set; } = "";
        public string ITEMGROUPVEND { get; set; } = "";
        public string ITEMGROUPVENDNAME { get; set; } = "";
        public string ITEMGROUPDESC { get; set; } = "";
        public string ITEMGROUPREMARK { get; set; } = "";
        public string ITEMGROUPSTATUS1 { get; set; } = "";
        public string ITEMGROUPSTATUS1NAME { get; set; } = "";
        public string ITEMGROUPSTATUS2 { get; set; } = "";
        public string ITEMGROUPSTATUS2NAME { get; set; } = "";
        public string ITEMGROUPSTATUS3 { get; set; } = "";
        public string ITEMGROUPSTATUS3NAME { get; set; } = "";
        public string ITEMGROUPSTATUS4 { get; set; } = "";
        public string ITEMGROUPSTATUS4NAME { get; set; } = "";
        public string ITEMGROUPSTATUS5 { get; set; } = "";
        public string ITEMGROUPSTATUS5NAME { get; set; } = "";
        public string STA_SHOWSTOCK { get; set; } = "";
        public string STA_SHOWSALE { get; set; } = "";
    }
    public class Var_ITEMGROUPBARCODE
    {
        public string ITEMGROUPID { get; set; } = "";
        public string ITEMGROUPTYPE { get; set; } = "";
        public string ITEMDEPT { get; set; } = "";
        public string ITEMBARCODE { get; set; } = "";
        public string ITEMUNIT { get; set; } = "";
        public string ITEMSTATUS1 { get; set; } = "";
        public string ITEMSTATUS1NAME { get; set; } = "";
        public string ITEMREMARK { get; set; } = "";
        public double ITEMCOST { get; set; } = 0;
    }
    public class Var_ITEMGROUPSTOCK
    {
        public string ITEMGROUPID { get; set; } = "";
        public string ITEMGROUPTYPE { get; set; } = "";
        public string ITEMDEPT { get; set; } = "";
        public string ITEMBARCODE { get; set; } = "";
        public string ITEMUNIT { get; set; } = "";
        public string ITEMBRANCH { get; set; } = "";
        public string ITEMREMARK { get; set; } = "";
        public string ITEMSDATESTOCK { get; set; } = "";
    }
    class ItemGroup_Class
    {
        //ค้นหาการต้นทุนของสินค้าเฉพาะกลุ่ม เบียร์สิงห์ ลีโอ
        public static DataTable GetItemCost()
        {
            return ConnectionClass.SelectSQL_Main($@"
                        SELECT	SHOW_ID,ISNULL(REMARK,0) AS COST,BRANCH_ID 
                        FROM	SHOP_CONFIGBRANCH_DETAIL  WITH (NOLOCK)  
                        WHERE	TYPE_CONFIG ='50'
                        ORDER BY BRANCH_ID,SHOW_ID
                    ");
        }
        //กลุ่มสินค้าตามประเภทการตั้งค่า และเงื่อนไนเพิ่มเติม
        public static DataTable GetItemGroup(string Dept, string Typepage, string condition)
        {
            string Sql = string.Format($@" 
            SELECT  ITEMGROUPID ,  ITEMGROUPNAME ,ITEMGROUPID+' ' +ITEMGROUPNAME  as ITEMGROUPIDNAME,ISNULL(ITEMGROUPVEND,'') AS ITEMGROUPVEND,  ITEMGROUPVENDNAME,
                    ITEMGROUPDESC ,  ITEMGROUPREMARK ,isnull(ITEMGROUPSTATUS1,'0') as ITEMGROUPSTATUS1,
                    isnull(ITEMGROUPSTATUS2,'0') as ITEMGROUPSTATUS2,
                    isnull(ITEMGROUPSTATUS3,'0') as ITEMGROUPSTATUS3,
                    '' AS CheckGroup,STA_SHOWSTOCK,STA_SHOWSALE
            FROM    SHOP_ITEMGROUP WITH (NOLOCK)
            WHERE   ITEMGROUPDEPT = '{Dept}' and ITEMGROUPTYPE = '{Typepage}'  {condition} 
            ORDER BY ITEMGROUPID ");
            return ConnectionClass.SelectSQL_Main(Sql);
        }

       
        //กลุ่มสินค้าส่งสต๊อก แก้ไขไม่ได้ ถ้ามีรายการสินค้าในกลุ่ม ต้องไปลบรายการสินค้าทมั้งหมดออกก่อน
        public static int GetItemGroupStock(string groupID, string typePage, string deptID)
        {
            string sql = $@" 
                    SELECT  * 
                    FROM    SHOP_ITEMGROUPSTOCK WITH (NOLOCK) 
                    WHERE   ITEMGROUPID = '{groupID}' AND ITEMGROUPTYPE = '{typePage}' AND ITEMDEPT = '{deptID}'   ";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }
        //บันทึก กลุ่มสินค้า
        public static string Save_ItemGroup(string typeCase, Var_ITEMGROUP var_ITEMGROUP)
        {
            if (typeCase == "Edit")
            {
                return $@"
                UPDATE  SHOP_ITEMGROUP 
                SET     ITEMGROUPNAME = '{var_ITEMGROUP.ITEMGROUPNAME}',
                        ITEMGROUPVEND = '{var_ITEMGROUP.ITEMGROUPVEND}', ITEMGROUPVENDNAME = '{var_ITEMGROUP.ITEMGROUPVENDNAME}',
                        ITEMGROUPDESC = '{var_ITEMGROUP.ITEMGROUPDESC}', ITEMGROUPREMARK = '{var_ITEMGROUP.ITEMGROUPREMARK}',
                        ITEMGROUPSTATUS1 = '{var_ITEMGROUP.ITEMGROUPSTATUS1}', ITEMGROUPSTATUS1NAME = '{var_ITEMGROUP.ITEMGROUPSTATUS1NAME}',
                        ITEMGROUPSTATUS2 = '{var_ITEMGROUP.ITEMGROUPSTATUS2}', ITEMGROUPSTATUS2NAME = '{var_ITEMGROUP.ITEMGROUPSTATUS2NAME}',
                        ITEMGROUPSTATUS3 = '{var_ITEMGROUP.ITEMGROUPSTATUS3}', ITEMGROUPSTATUS3NAME = '{var_ITEMGROUP.ITEMGROUPSTATUS3NAME}',
                        WHOUP = '{SystemClass.SystemUserID}', WHONAMEUP = '{SystemClass.SystemUserName}', DATEUP = convert(varchar, getdate(), 25),
                        STA_SHOWSTOCK = '{var_ITEMGROUP.STA_SHOWSTOCK}', STA_SHOWSALE = '{var_ITEMGROUP.STA_SHOWSALE}'
                WHERE   ITEMGROUPID = '{var_ITEMGROUP.ITEMGROUPID}'
                        and ITEMGROUPDEPT = '{var_ITEMGROUP.ITEMGROUPDEPT}' AND ITEMGROUPTYPE = '{var_ITEMGROUP.ITEMGROUPTYPE}' ";
            }
            else
            {
                return $@"
                INSERT INTO SHOP_ITEMGROUP (
                ITEMGROUPID , ITEMGROUPNAME , ITEMGROUPDEPT , ITEMGROUPTYPE , ITEMGROUPTYPEREMARK , 
                ITEMGROUPVEND , ITEMGROUPVENDNAME, ITEMGROUPDESC , ITEMGROUPREMARK , 
                ITEMGROUPSTATUS1 , ITEMGROUPSTATUS1NAME ,
                ITEMGROUPSTATUS2 , ITEMGROUPSTATUS2NAME , 
                ITEMGROUPSTATUS3 , ITEMGROUPSTATUS3NAME , 
                ITEMGROUPSTATUS4 , ITEMGROUPSTATUS4NAME , 
                ITEMGROUPSTATUS5 , ITEMGROUPSTATUS5NAME , 
                WHOINS ,WHONAMEINS ,STA_SHOWSTOCK,STA_SHOWSALE
                )
                values( 
                '{var_ITEMGROUP.ITEMGROUPID}','{var_ITEMGROUP.ITEMGROUPNAME}','{var_ITEMGROUP.ITEMGROUPDEPT}','{var_ITEMGROUP.ITEMGROUPTYPE}','{var_ITEMGROUP.ITEMGROUPTYPEREMARK}' , 
                '{var_ITEMGROUP.ITEMGROUPVEND}','{var_ITEMGROUP.ITEMGROUPVENDNAME}','{var_ITEMGROUP.ITEMGROUPDESC}' , '{var_ITEMGROUP.ITEMGROUPREMARK}',
                '{var_ITEMGROUP.ITEMGROUPSTATUS1}','{var_ITEMGROUP.ITEMGROUPSTATUS1NAME}', 
                '{var_ITEMGROUP.ITEMGROUPSTATUS2}','{var_ITEMGROUP.ITEMGROUPSTATUS2NAME}' ,
                '{var_ITEMGROUP.ITEMGROUPSTATUS3}','{var_ITEMGROUP.ITEMGROUPSTATUS3NAME}' ,  
                '{var_ITEMGROUP.ITEMGROUPSTATUS4}','{var_ITEMGROUP.ITEMGROUPSTATUS4NAME}' ,
                '{var_ITEMGROUP.ITEMGROUPSTATUS5}','{var_ITEMGROUP.ITEMGROUPSTATUS5NAME}' ,  
                '{SystemClass.SystemUserID}' , '{SystemClass.SystemUserName}', '{var_ITEMGROUP.STA_SHOWSTOCK}' , '{var_ITEMGROUP.STA_SHOWSALE}' )";
            }
        }
        //จัดการสินค้าในกลุ่ม
        public static string Save_ItemGroupBarcode(string typeCase, Var_ITEMGROUPBARCODE var_ITEMBARCODE, string EditITEMGROUPID)
        {
            switch (typeCase)
            {
                case "Add": // ADD
                    return $@"
                        insert into  SHOP_ITEMGROUPBARCODE (
                            ITEMGROUPID ,  ITEMGROUPTYPE ,  ITEMDEPT ,  
                            ITEMBARCODE ,  ITEMUNIT , 
                            ITEMSTATUS1 ,  ITEMSTATUS1NAME ,
                            ITEMREMARK ,  WHOINS ,  WHONAMEINS , ITEMCOST   )
                        values(
                        '{var_ITEMBARCODE.ITEMGROUPID}', '{var_ITEMBARCODE.ITEMGROUPTYPE}', '{var_ITEMBARCODE.ITEMDEPT}', 
                        '{var_ITEMBARCODE.ITEMBARCODE}', '{var_ITEMBARCODE.ITEMUNIT}', 
                        '{var_ITEMBARCODE.ITEMSTATUS1}', '{var_ITEMBARCODE.ITEMSTATUS1NAME}',
                        '{var_ITEMBARCODE.ITEMREMARK}', '{SystemClass.SystemUserID_M}', '{SystemClass.SystemUserName}','{var_ITEMBARCODE.ITEMCOST}' 
                        )";
                case "Edit":
                    return $@" 
                        UPDATE  SHOP_ITEMGROUPBARCODE 
                        SET     ITEMGROUPID = '{EditITEMGROUPID}',
                                ITEMSTATUS1 = '{var_ITEMBARCODE.ITEMSTATUS1}',ITEMREMARK = '{var_ITEMBARCODE.ITEMREMARK}',
                                WHOUP = '{SystemClass.SystemUserID_M}' ,  WHONAMEUP = '{SystemClass.SystemUserName}' ,   
                                DATEUP = convert(varchar, getdate(), 25)  ,
                                ITEMCOST = '{var_ITEMBARCODE.ITEMCOST}'
                        WHERE   ITEMBARCODE = '{var_ITEMBARCODE.ITEMBARCODE}' 
                                AND ITEMGROUPTYPE = '{var_ITEMBARCODE.ITEMGROUPTYPE}' 
                                AND ITEMGROUPID = '{var_ITEMBARCODE.ITEMGROUPID}'  ";
                case "Del":
                    return $@" 
                        DELETE  FROM    SHOP_ITEMGROUPBARCODE 
                        WHERE   ITEMBARCODE = '{var_ITEMBARCODE.ITEMBARCODE}' 
                                AND ITEMGROUPTYPE = '{var_ITEMBARCODE.ITEMGROUPTYPE}' 
                                AND ITEMGROUPID = '{var_ITEMBARCODE.ITEMGROUPID}' ";
                default:
                    return "";

            }

        }
        //ตั้งค่าทุนตามสาขา
        public static string Save_ItemCostByBranchID(string bchID, string itemBarcode, string itemName, double cost)
        {
            return $@"
                    IF NOT EXISTS (	SELECT	SHOW_ID,REMARK,BRANCH_ID  FROM	SHOP_CONFIGBRANCH_DETAIL  WITH (NOLOCK)  
				                    WHERE	TYPE_CONFIG = '50' AND BRANCH_ID = '{bchID}' 
                                            AND SHOW_ID = '{itemBarcode}')
                    BEGIN
		                    INSERT INTO SHOP_CONFIGBRANCH_DETAIL
		                    ([SHOW_ID],[SHOW_NAME],
                                [SHOW_DESC],[REMARK],[TYPE_CONFIG],
                                [STATUS],[BRANCH_ID],[WHOIDINS],[WHONAMEINS])
		                    VALUES ('{itemBarcode}','{itemName}',
                                    '','{cost}','50',
                                '1','{bchID}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')
                    END
                    ELSE
		                    UPDATE	SHOP_CONFIGBRANCH_DETAIL
		                    SET		[REMARK] = '{cost}'
		                    WHERE	[BRANCH_ID] = '{bchID}' 
                                    AND SHOW_ID = '{itemBarcode}' AND TYPE_CONFIG ='50' ";
        }
        //บันทึกสินค้าสต็ฮก
        public static string Save_ItemGroupStock(string typeCase, Var_ITEMGROUPSTOCK var_ITEMGROUPSTOCK)
        {
            switch (typeCase)
            {
                case "Add":
                    return $@"
                        insert into SHOP_ITEMGROUPSTOCK
                                (ITEMGROUPID, ITEMGROUPTYPE, ITEMDEPT, 
                                ITEMBRANCH, ITEMBARCODE, ITEMUNIT, ITEMREMARK,
                                ITEMSDATESTOCK, WHOINS, WHONAMEINS)
                        values  ('{var_ITEMGROUPSTOCK.ITEMGROUPID}', '{var_ITEMGROUPSTOCK.ITEMGROUPTYPE}', '{var_ITEMGROUPSTOCK.ITEMDEPT}',
                                '{var_ITEMGROUPSTOCK.ITEMBRANCH}','{var_ITEMGROUPSTOCK.ITEMBARCODE}','{var_ITEMGROUPSTOCK.ITEMUNIT}', '{var_ITEMGROUPSTOCK.ITEMREMARK}',
                                '{var_ITEMGROUPSTOCK.ITEMSDATESTOCK}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";
                case "Del":
                    return $@"
                        DELETE  FROM SHOP_ITEMGROUPSTOCK 
                        WHERE   ITEMGROUPID ='{var_ITEMGROUPSTOCK.ITEMGROUPID}'
                                AND  ITEMGROUPTYPE ='{var_ITEMGROUPSTOCK.ITEMGROUPTYPE}'
                                AND  ITEMDEPT ='{var_ITEMGROUPSTOCK.ITEMDEPT}' 
                                AND  ITEMBARCODE ='{var_ITEMGROUPSTOCK.ITEMBARCODE}' 
                                AND  ITEMBRANCH='{var_ITEMGROUPSTOCK.ITEMBRANCH}' ";
                default:
                    return "";
            }

        }
    }

    class ItemGroupStockClass
    {
        public static string GetYear(int year)
        {
            string sql = string.Format(@"select year(convert(varchar, DATEADD (year,{0},getdate()),23)) as RYear",
                year);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            string years = string.Empty;
            if (dt.Rows.Count > 0)
            {
                years = dt.Rows[0]["RYear"].ToString();
            }
            return years;
        }
        public static DataTable GetAllYear()
        {
            DataTable DtYear = new DataTable();
            DtYear.Columns.Add("Value");
            DtYear.Columns.Add("Member");
            for (int i = -2; i < 3; i++)
            {
                string year = GetYear(i);
                DtYear.Rows.Add(year, year);
            }
            return DtYear;
        }

        public static DataTable GetAllMonth()
        {
            DataTable DtMonth = new DataTable();
            DtMonth.Columns.Add("Value");
            DtMonth.Columns.Add("Member");
            for (int i = 1; i < 13; i++)
            {
                DtMonth.Rows.Add(i.ToString("D2"), i.ToString("D2"));
            }
            return DtMonth;
        }

        public static DataTable GetItemStock_byDeptGroupId(string GroupID, string GroupType, string Dept,
            string MonthStock, string YearStock)
        {
            string sql;
            sql = string.Format(@"
                select  ITEMGROUPID, ITEMGROUPTYPE, ITEMDEPT, ITEMBRANCH,
                        ITEMBARCODE, ITEMUNIT, ITEMREMARK, ITEMSDATESTOCK, 
					    Day(ITEMSDATESTOCK) as Day,  Day(ITEMSDATESTOCK)+1 as ColumnsDay
                from    SHOP_ITEMGROUPSTOCK
                where   ITEMGROUPID = '{0}'
                        AND ITEMGROUPTYPE = '{1}'
                        AND ITEMDEPT = '{2}'
                        AND month(ITEMSDATESTOCK)='{3}'
                        AND year(ITEMSDATESTOCK)='{4}'
                Order by Day(ITEMSDATESTOCK) ",
                    GroupID, GroupType, Dept, MonthStock, YearStock);
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable GetItemStock_byDeptBarcode(string GroupID, string GroupType, string Dept, string Barcode)
        {
            string sql;
            sql = $@"select ITEMGROUPID, ITEMGROUPTYPE, ITEMDEPT, ITEMBRANCH,
                            ITEMBARCODE, ITEMUNIT, ITEMREMARK, ITEMSDATESTOCK, 
					        Day(ITEMSDATESTOCK) as Day,  Day(ITEMSDATESTOCK)+1 as ColumnsDay
                    from    SHOP_ITEMGROUPSTOCK
                    where   ITEMGROUPID = '{GroupID}'
                            AND ITEMGROUPTYPE = '{GroupType}'
                            AND ITEMDEPT = '{Dept}' 
                            AND ITEMBARCODE='{Barcode}'
                    Order by Day(ITEMSDATESTOCK) ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable GetItemStock_byDate(string GroupID, string GroupType, string Dept, string DateStock)
        {
            string sql;
            sql = string.Format(@"select ITEMGROUPID, ITEMGROUPTYPE, ITEMDEPT, ITEMBRANCH,
                    ITEMBARCODE, ITEMUNIT, ITEMREMARK, ITEMSDATESTOCK, 
					Day(ITEMSDATESTOCK) as Day,  Day(ITEMSDATESTOCK)+1 as ColumnsDay
                    from SHOP_ITEMGROUPSTOCK
                    where ITEMGROUPID = '{0}'
                    AND ITEMGROUPTYPE = '{1}'
                    AND ITEMDEPT = '{2}'
                    AND ITEMSDATESTOCK ='{3}' 
                    Order by Day(ITEMSDATESTOCK) ",
                    GroupID, GroupType, Dept, DateStock);
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }

}
