﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class ItemsGroup : Telerik.WinControls.UI.RadForm
    {
        private readonly string _TypePage, _TypePageName;

        private string TypeEdit;
        Data_VENDTABLE Vend;
        string statusBranchOrder = "0";
        string statusBranchStock = "0";
        string statusBranchSend = "0";

        //_TypePage ประเภทของกลุ่มสินค้า ส่งมา 2 ตัวเช่น 01
        //_TypePageName ชื่อกลุ่มสินค้า เช่น กลุ่มสั่งสินค้า

        public ItemsGroup(string TypePage, string TypePageName) //01 สั่ง 02 แลกแต้ม
        {
            InitializeComponent();

            _TypePage = TypePage;
            _TypePageName = TypePageName;
        }

        private void ItemsGroup_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);

            radButton_Save.ButtonElement.ShowBorder = true; radButton_Cancel.ButtonElement.ShowBorder = true;

            radButtonElement_Add.ToolTipText = "เพิ่ม"; radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ToolTipText = "แก้ไข"; radButtonElement_Edit.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            switch (_TypePage)
            {
                case "01"://ถ้าเป็นสินค้าสำหรับสั่ง ต้องแสดงฟิลนี้เพิ่มด้วย
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("ITEMGROUPSTATUS1", "สาขาสั่ง"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("ITEMGROUPSTATUS3", "ส่งสินค้า"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("ITEMGROUPSTATUS2", "ส่งสต๊อก"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA_SHOWSTOCK", "สต็อก"));
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA_SHOWSALE", "ยอดขาย"));
                    break;
                default:
                    break;
            }

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPID", "รหัสกลุ่ม", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPNAME", "ชื่อกลุ่ม", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPVEND", "ผู้จำหน่าย", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPVENDNAME", "ชื่อผู้จำหน่าย", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPDESC", "คำอธิบาย", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPREMARK", "หมายเหตุ", 200));

            TypeEdit = "Default";
            ClearData(TypeEdit);

            RadDropDownList_Dept.DataSource = MRTClass.GetDept(SystemClass.SystemDptID);
            RadDropDownList_Dept.DisplayMember = "DeptName";
            RadDropDownList_Dept.ValueMember = "NUM";
            RadDropDownList_Dept.SelectedIndex = 0;

            if (RadDropDownList_Dept.SelectedValue.ToString() != "") SetDatagrid(ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePage, ""));
        }
        #region FUNCTION 
        //ClearData
        void ClearData(string Type)
        {
            switch (Type)
            {
                case "Default":

                    radTextBox_ID.Text = string.Empty;
                    radTextBox_Name.Text = string.Empty;
                    RadTextBox_VEND.Text = string.Empty;
                    radTextBox_Desc.Text = string.Empty;
                    radTextBox_Remark.Text = string.Empty;
                    radLabel_VendName.Text = string.Empty;

                    radTextBox_ID.Enabled = false;
                    radTextBox_Name.Enabled = false;
                    RadTextBox_VEND.Enabled = false;
                    radTextBox_Desc.Enabled = false;
                    radTextBox_Remark.Enabled = false;

                    RadCheckBox_forOrder.Enabled = false;
                    CheckBox_Send.Enabled = false;
                    CheckBox_Stock.Enabled = false;
                    radButton_Save.Enabled = false;
                    radButton_Cancel.Enabled = false;
                    checkBox_ShowSale.Enabled = false;
                    checkBox_ShowStock.Enabled = false;
                    break;

                case "Add":

                    radTextBox_ID.Enabled = false; radTextBox_ID.Text = "";
                    radTextBox_Name.Enabled = true; radTextBox_Name.Text = "";
                    RadTextBox_VEND.Enabled = true; RadTextBox_VEND.Text = "";
                    radTextBox_Desc.Enabled = true; radTextBox_Desc.Text = "";
                    radTextBox_Remark.Enabled = true; radTextBox_Remark.Text = "";

                    RadCheckBox_forOrder.Enabled = true;
                    CheckBox_Send.Enabled = true; CheckBox_Send.Checked = false;
                    CheckBox_Stock.Enabled = true; CheckBox_Stock.Checked = false;
                    checkBox_ShowSale.Enabled = false;
                    checkBox_ShowStock.Enabled = false;
                    radButton_Save.Enabled = true;
                    radButton_Cancel.Enabled = true;

                    radTextBox_Name.SelectAll();
                    radTextBox_Name.Focus();
                    break;

                case "Edit":

                    radTextBox_ID.Enabled = false;
                    radTextBox_Name.Enabled = true;
                    RadTextBox_VEND.Enabled = true;
                    radTextBox_Desc.Enabled = true;
                    radTextBox_Remark.Enabled = true;

                    RadCheckBox_forOrder.Enabled = true;
                    CheckBox_Send.Enabled = true;
                    CheckBox_Stock.Enabled = true;
                    checkBox_ShowSale.Enabled = true;
                    checkBox_ShowStock.Enabled = true;

                    radButton_Save.Enabled = true;
                    radButton_Cancel.Enabled = true;

                    radTextBox_Name.SelectAll();
                    radTextBox_Name.Focus();
                    break;
            }

            //01 กลุ่มสั่ง, กลุ่มส่ง, ส่งสต๊อก
            //02 แลกแต้ม 
            switch (_TypePage)
            {
                case "01":
                    RadCheckBox_forOrder.Visible = true;
                    CheckBox_Send.Visible = true;
                    CheckBox_Stock.Visible = true;
                    checkBox_ShowSale.Visible = true;
                    checkBox_ShowStock.Visible = true;
                    break;
                case "02":
                    RadCheckBox_forOrder.Visible = false;
                    CheckBox_Send.Visible = false;
                    CheckBox_Stock.Visible = false;
                    checkBox_ShowSale.Visible = false;
                    checkBox_ShowStock.Visible = false;
                    break;
            }
        }


        void SetDatagrid(DataTable dtGroupItems)
        {
            RadGridView_Show.DataSource = dtGroupItems;
        }

        #endregion
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion

        #region EVEN

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            TypeEdit = "Add";
            ClearData(TypeEdit);
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count < 1)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเพิ่มกลุ่มสินค้า ก่อนเพิ่มสินค้า.");
                return;
            }

            TypeEdit = "Edit";
            ClearData(TypeEdit);
            RadGridView_Show_SelectionChanged(sender, e);
        }

        private void RadTextBox_VEND_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(RadTextBox_VEND.Text))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ รหัสผู้จำหน่าย ให้เรียบร้อย.");
                    RadTextBox_VEND.SelectAll();
                    RadTextBox_VEND.Focus();
                    return;
                }

                this.Vend = new Data_VENDTABLE(Class.Models.VendTableClass.GetVENDTABLE_All(RadTextBox_VEND.Text, RadDropDownList_Dept.SelectedValue.ToString()));

                if (!(string.IsNullOrEmpty(Vend.VENDTABLE_ACCOUNTNUM)))
                {
                    radLabel_VendName.Text = this.Vend.VENDTABLE_NAME;
                    radTextBox_Desc.SelectAll();
                    radTextBox_Desc.Focus();
                }
                else
                {

                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่เจอ ผู้จำหน่าย ที่ต้องการค้นหา.");
                    RadTextBox_VEND.SelectAll();
                    RadTextBox_VEND.Focus();
                    return;

                }
            }
            else if (e.KeyCode == Keys.F4)
            {

                using (ShowDataDGV_Vend ShowDataDGV_Vend = new ShowDataDGV_Vend(RadDropDownList_Dept.SelectedValue.ToString()))
                {
                    DialogResult dr = ShowDataDGV_Vend.ShowDialog();

                    if (dr == DialogResult.Yes)
                    {
                        this.Vend = ShowDataDGV_Vend.Vend;
                        RadTextBox_VEND.Text = this.Vend.VENDTABLE_ACCOUNTNUM;
                        radLabel_VendName.Text = this.Vend.VENDTABLE_NAME;
                    }
                }
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            TypeEdit = "Default";
            ClearData(TypeEdit);
        }

        private void RadTextBox_VEND_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_VEND.Text.Length < 7) radLabel_VendName.Text = string.Empty;
        }

        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedValue.ToString() != "") SetDatagrid(ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePage, ""));
        }

        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RadTextBox_VEND.SelectAll();
                RadTextBox_VEND.Focus();
            }
        }

        private void RadTextBox_Desc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_Remark.SelectAll();
                radTextBox_Remark.Focus();
            }
        }

        private void RadTextBox_Remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Save.Focus();
        }

        private void RadGridView_Show_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_Show.RowCount > 0 && RadGridView_Show.CurrentRow.Index > -1)//& TypeEdit == "Edit"
            {
                radTextBox_ID.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPID"].Value.ToString();
                radTextBox_Name.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPNAME"].Value.ToString();
                RadTextBox_VEND.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPVEND"].Value.ToString();
                radLabel_VendName.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPVENDNAME"].Value.ToString();
                radTextBox_Desc.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPDESC"].Value.ToString();
                radTextBox_Remark.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPREMARK"].Value.ToString();

                if (_TypePage == "01")
                {
                    if (RadGridView_Show.CurrentRow.Cells["ITEMGROUPSTATUS1"].Value.ToString() == "1") RadCheckBox_forOrder.Checked = true;
                    else RadCheckBox_forOrder.Checked = false;

                    if (RadGridView_Show.CurrentRow.Cells["ITEMGROUPSTATUS2"].Value.ToString() == "1") CheckBox_Stock.Checked = true;
                    else CheckBox_Stock.Checked = false;

                    if (RadGridView_Show.CurrentRow.Cells["ITEMGROUPSTATUS3"].Value.ToString() == "1") CheckBox_Send.Checked = true;
                    else CheckBox_Send.Checked = false;

                    if (RadGridView_Show.CurrentRow.Cells["STA_SHOWSALE"].Value.ToString() == "1") checkBox_ShowSale.Checked = true;
                    else checkBox_ShowSale.Checked = false;

                    if (RadGridView_Show.CurrentRow.Cells["STA_SHOWSTOCK"].Value.ToString() == "1") checkBox_ShowStock.Checked = true;
                    else checkBox_ShowStock.Checked = false;

                }

            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(radTextBox_Name.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อกลุ่มสินค้า ให้เรียบร้อย.");
                radTextBox_Name.SelectAll();
                radTextBox_Name.Focus();
                return;
            }
            if (!(String.IsNullOrEmpty(RadTextBox_VEND.Text)) & String.IsNullOrEmpty(radLabel_VendName.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ผู้จำหน่าย ให้เรียบร้อย.");
                RadTextBox_VEND.SelectAll();
                RadTextBox_VEND.Focus();
                return;
            }

            if (RadCheckBox_forOrder.Checked == true) statusBranchOrder = "1";

            if (CheckBox_Send.Checked == true) statusBranchSend = "1";

            if (CheckBox_Stock.Checked == true) statusBranchStock = "1";
            else
            {
                 
                if (ItemGroup_Class.GetItemGroupStock(radTextBox_ID.Text, _TypePage, RadDropDownList_Dept.SelectedValue.ToString()) > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่รายการสินค้าที่กำหนดสต๊อกอยู่ ไม่สามารถแก้ไข กลุ่มสินค้าส่งสต๊อกได้");
                    return;
                }

            }

            string showSale = "0", showStock = "0";
            if (checkBox_ShowSale.Checked) showSale = "1";
            if (checkBox_ShowStock.Checked) showStock = "1";

            if (TypeEdit == "Add") radTextBox_ID.Text = Class.ConfigClass.GetMaxINVOICEID(RadDropDownList_Dept.SelectedValue.ToString(), "", RadDropDownList_Dept.SelectedValue.ToString() + "_", "3");

            Var_ITEMGROUP var_ITEMGROUP = new Var_ITEMGROUP
            {
                ITEMGROUPID = radTextBox_ID.Text,
                ITEMGROUPNAME = radTextBox_Name.Text,
                ITEMGROUPDEPT = RadDropDownList_Dept.SelectedValue.ToString(),
                ITEMGROUPTYPE = _TypePage,
                ITEMGROUPTYPEREMARK = _TypePageName,
                ITEMGROUPVEND = RadTextBox_VEND.Text,
                ITEMGROUPVENDNAME = radLabel_VendName.Text,
                ITEMGROUPDESC = radTextBox_Desc.Text,
                ITEMGROUPREMARK = radTextBox_Remark.Text,
                ITEMGROUPSTATUS1 = statusBranchOrder,
                ITEMGROUPSTATUS1NAME = "สาขาสั่งสินค้า",
                ITEMGROUPSTATUS2 = statusBranchStock,
                ITEMGROUPSTATUS2NAME = "สาขาส่งสต๊อก",
                ITEMGROUPSTATUS3 = statusBranchSend,
                ITEMGROUPSTATUS3NAME = "ส่งสินค้าสาขา",
                ITEMGROUPSTATUS4 = "0",
                ITEMGROUPSTATUS4NAME = "",
                ITEMGROUPSTATUS5 = "0",
                ITEMGROUPSTATUS5NAME = "",
                STA_SHOWSTOCK = showStock,
                STA_SHOWSALE = showSale
            };

            string resualt = ConnectionClass.ExecuteSQL_Main(ItemGroup_Class.Save_ItemGroup(TypeEdit, var_ITEMGROUP));
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                TypeEdit = "Default";
                ClearData(TypeEdit);
                SetDatagrid(ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePage, ""));
            }

            this.Cursor = Cursors.Default;

            
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _TypePage);
        }

        private void RadCheckBox_forOrder_CheckedChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_forOrder.Checked == true)
            {
                RadCheckBox_forOrder.Checked = true;
                statusBranchOrder = "1";
            }
            else
            {
                RadCheckBox_forOrder.Checked = false;
                statusBranchOrder = "0";
            }
        }

        private void CheckBox_Send_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_Send.Checked == true)
            {
                CheckBox_Send.Checked = true;
                statusBranchSend = "1";
            }
            else
            {
                CheckBox_Send.Checked = false;
                statusBranchSend = "0";
            }
        }

        private void CheckBox_Stock_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_Stock.Checked == true)
            {
                CheckBox_Stock.Checked = true;
                statusBranchStock = "1";
            }
            else
            {
                CheckBox_Stock.Checked = false;
                statusBranchStock = "0";
            }
        }
        #endregion

    }
}
