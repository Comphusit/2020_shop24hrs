﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using PC_Shop24Hrs.FormShare.ShowData;
using PC_Shop24Hrs.FormShare;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class ItemsGroupBarcode : Telerik.WinControls.UI.RadForm
    {
        private readonly string _TypePage;//, _DptCode;
        private string TypeEdit;
        Data_ITEMBARCODE Items;
        readonly DataTable dtShow = new DataTable();
        double iRow;
        //_TypePage ประเภทของกลุ่มสินค้า ส่งมา 2 ตัวเช่น 01
        //ตั้งค่าคู่กับหน้า  ItemsGroup
        public ItemsGroupBarcode(string TypePage) //01 กลุ่มสินค้า 02 แลกแต้ม
        {
            InitializeComponent();
            _TypePage = TypePage;
        }
        private void ItemsGroupBarcode_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupItems);

            radButton_Save.ButtonElement.ShowBorder = true; radButton_Delete.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_Add.ToolTipText = "เพิ่ม"; radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ToolTipText = "แก้ไข"; radButtonElement_Edit.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            TypeEdit = "Default";

            SetDropdownlistDept(MRTClass.GetDept(SystemClass.SystemDptID));
            if (RadDropDownList_Dept.SelectedItems.Count > 0 && radDropDownList_GroupItems.SelectedItems.Count > 0) SetDatagridview();

            ClearData(TypeEdit);
        }

        #region FUNCTION
        //ตั้งค่า แผนก
        void SetDropdownlistDept(DataTable DtDimension)
        {
            RadDropDownList_Dept.DataSource = DtDimension;
            RadDropDownList_Dept.DisplayMember = "DeptName";
            RadDropDownList_Dept.ValueMember = "NUM";

            RadDropDownList_Dept.SelectedValue = SystemClass.SystemDptID;
            if (RadDropDownList_Dept.SelectedValue is null) RadDropDownList_Dept.SelectedIndex = 0;

        }
        //ตั้งค่ากลุ่ม
        void SetDropdownlistGroup(DataTable DtDimension)
        {
            radDropDownList_GroupItems.DataSource = DtDimension;
            radDropDownList_GroupItems.DisplayMember = "ITEMGROUPIDNAME";
            radDropDownList_GroupItems.ValueMember = "ITEMGROUPID";
        }
        //ตั้งค่า Grid
        void SetDatagridview()//DataTable DtItemBarcode
        {
            if (radDropDownList_GroupItems.SelectedValue.ToString() != "D034_01")
            {
                this.Cursor = Cursors.WaitCursor;
                radLabel_F2.Text = "กด + >> เพื่อเพิ่มบาร์โค้ดสินค้า | เลือกสินค้า กด / >> เพื่อแก้ไขสินค้า";
                if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();

                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "แผนก"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "แผนก"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMUNIT", "หน่วย", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTY", "อัตราส่วน"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_SalesPriceTypeNAME", "ประเภทบาร์โค๊ด"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("Description", "แผนก"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPID", "กลุ่มสินค้า"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPNAME", "กลุ่มสินค้า"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("ITEMSTATUS1", "ใช้งาน"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("EMPLID", "ผู้รับผิดชอบ"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("EMPLNAME", "ชื่อผู้รับผิดชอบ"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMREMARK", "หมายเหตุ"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COSTITEM", "ต้นทุนสินค้า", 120));

                if (_TypePage == "02") RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("SPC_REDEMPPOINT", "จำนวนแต้ม", 80));

                if ((RadDropDownList_Dept.SelectedValue.ToString() != "D034") || (radDropDownList_GroupItems.SelectedValue.ToString() == "D034_01"))
                {
                    RadGridView_Show.Columns["COSTITEM"].IsVisible = false;
                    radTextBox_Cost.Visible = false;
                    radLabel_Cost.Visible = false;
                }

                RadGridView_Show.DataSource = ItembarcodeClass.ItemGroup_GetItemBarcode(RadDropDownList_Dept.SelectedValue.ToString(), _TypePage,
                    radDropDownList_GroupItems.SelectedValue.ToString(), "");
                iRow = -1;
             
                this.Cursor = Cursors.Default;
                return;
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                radLabel_F2.Text = "กด + >> เพื่อเพิ่มบาร์โค้ดสินค้า | เลือกสินค้า กด / >> เพื่อแก้ไขสินค้า | DoubleClick ช่องต้นทุน >> แก้ไขต้นทุนสินค้า";

                if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();
                if (dtShow.Rows.Count > 0) dtShow.Rows.Clear();
                if (dtShow.Columns.Count > 0) dtShow.Columns.Clear();
                
                dtShow.Columns.Add("ITEMGROUPID");
                dtShow.Columns.Add("SPC_SalesPriceTypeNAME");
                dtShow.Columns.Add("ITEMBARCODE");
                dtShow.Columns.Add("SPC_ITEMNAME");
                dtShow.Columns.Add("ITEMSTATUS1");
                dtShow.Columns.Add("ITEMUNIT");
                dtShow.Columns.Add("COSTITEM");

                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPID", "กลุ่มสินค้า"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_SalesPriceTypeNAME", "ประเภทบาร์โค๊ด"));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250)));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("ITEMSTATUS1", "ใช้งาน"));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMUNIT", "หน่วย", 100)));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("COSTITEM", "ต้นทุนสินค้า"));

                RadGridView_Show.MasterTemplate.Columns["ITEMBARCODE"].IsPinned = true;
                RadGridView_Show.MasterTemplate.Columns["SPC_ITEMNAME"].IsPinned = true;

                RadGridView_Show.Columns["COSTITEM"].IsVisible = false;
                radTextBox_Cost.Enabled = false;

                DataTable dtProvinc = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("50", "", "", "1");

                foreach (DataRow row in dtProvinc.Rows)
                {
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(row["SHOW_ID"].ToString(),
                        "ต้นทุน " + row["SHOW_DESC"].ToString(), 200)));
                    dtShow.Columns.Add(row["SHOW_ID"].ToString());
                }

                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TT", "TT"));

                DataTable dtBarcode = ItembarcodeClass.ItemGroup_GetItemBarcode(RadDropDownList_Dept.SelectedValue.ToString(), _TypePage,
                    radDropDownList_GroupItems.SelectedValue.ToString(), "");

                foreach (DataRow row1 in dtBarcode.Rows)
                {
                    dtShow.Rows.Add(row1["ITEMGROUPID"].ToString(), row1["SPC_SalesPriceTypeNAME"].ToString(),
                        row1["ITEMBARCODE"].ToString(), row1["SPC_ITEMNAME"].ToString(), row1["ITEMSTATUS1"].ToString(),
                        row1["ITEMUNIT"].ToString(), row1["COSTITEM"].ToString());
                }

            
                DataTable dtCost = ItemGroup_Class.GetItemCost();

                for (int iR = 0; iR < dtShow.Rows.Count; iR++)
                {
                    foreach (DataRow row in dtProvinc.Rows)
                    {
                        DataRow[] dr = dtCost.Select($@" SHOW_ID = '{dtShow.Rows[iR]["ITEMBARCODE"]}' AND  BRANCH_ID = '{row["SHOW_ID"]}' ");
                        double cost = 0;
                        if (dr.Length > 0) cost = double.Parse(dr[0]["COST"].ToString());
                        dtShow.Rows[iR][row["SHOW_ID"].ToString()] = cost.ToString("N");
                    }
                }

               RadGridView_Show.DataSource = dtShow;
                dtShow.AcceptChanges();

                this.Cursor = Cursors.Default;
            }


        }
        // clear ข้อมูล
        void ClearData(String Type)
        {
            switch (Type)
            {
                case "Default":
                    radTextBox_ItemBarcode.Enabled = false;
                    RadTextBox_EmplId.Enabled = false;
                    radTextBox_Rmk.Enabled = false;

                    radCheckBox_forOrder.Enabled = false;
                    radTextBox_Cost.Enabled = false;
                    radButton_Save.Enabled = false;
                    radButton_Delete.Enabled = false;
                    break;

                case "Add":
                    radDropDownList_GroupItems.Enabled = true;

                    radTextBox_ItemBarcode.Text = ""; radTextBox_ItemBarcode.Enabled = true;
                    radLabel_ItemName.Text = ""; radLabel_ItemName.Enabled = true;
                    radTextBox_Rmk.Text = ""; radTextBox_Rmk.Enabled = true;
                    radLabel_Unit.Text = "";
                    radLabel_TypePrice.Text = "";

                    radCheckBox_forOrder.Enabled = true; radCheckBox_forOrder.Checked = true;

                    RadTextBox_EmplId.Enabled = true;

                    radButton_Save.Enabled = true;
                    radTextBox_Cost.Text = "0";

                    radTextBox_ItemBarcode.SelectAll();
                    radTextBox_ItemBarcode.Focus();
                    break;

                case "Edit":
                    radDropDownList_GroupItems.Enabled = false;

                    radTextBox_ItemBarcode.Enabled = false;
                    radTextBox_Rmk.Enabled = true;
                    radCheckBox_forOrder.Enabled = true;
                    radCheckBox_forOrder.Enabled = true;
                    radTextBox_Cost.Enabled = true;
                    radTextBox_Cost.SelectAll();
                    radTextBox_Cost.Focus();

                    radButton_Save.Enabled = true;
                    radButton_Delete.Enabled = true;
                    break;
            }
        }
        //เปลี่ยนแผนก
        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedItems.Count > 0)
            {
                SetDropdownlistGroup(ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePage, ""));
                if (RadDropDownList_Dept.SelectedItems.Count > 0 && radDropDownList_GroupItems.SelectedItems.Count > 0) SetDatagridview();
            }
        }
        #endregion

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion


        #region EVEN

        private void RadTextBox_ItemBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:

                    if (string.IsNullOrEmpty(radTextBox_ItemBarcode.Text))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ บาร์โค้ด ให้เรียบร้อย.");
                        radTextBox_ItemBarcode.SelectAll();
                        radTextBox_ItemBarcode.Focus();
                        return;
                    }
                    this.Items = new Data_ITEMBARCODE(radTextBox_ItemBarcode.Text);
                    if (string.IsNullOrEmpty(this.Items.Itembarcode_ITEMBARCODE))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่เจอบาร์โค๊ดที่ต้องการค้นหา.");
                        radTextBox_ItemBarcode.SelectAll();
                        radTextBox_ItemBarcode.Focus();
                        return;
                    }

                    radLabel_ItemName.Text = this.Items.Itembarcode_SPC_ITEMNAME;
                    radLabel_Unit.Text = this.Items.Itembarcode_UNITID;
                    radLabel_TypePrice.Text = this.Items.Itembarcode_SPC_SalesPriceTypeNAME;
                    radTextBox_Rmk.SelectAll();
                    radTextBox_Rmk.Focus();
                    break;
                case Keys.F4:
                    String SqlCondition = string.Format(@" AND INVENTTABLE.DIMENSION='{0}'",
                        RadDropDownList_Dept.SelectedValue.ToString());
                    using (FormShare.ShowData.ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new FormShare.ShowData.ShowDataDGV_Itembarcode(SqlCondition))
                    {
                        DialogResult dr = ShowDataDGV_Itembarcode.ShowDialog();

                        if (dr == DialogResult.Yes)
                        {
                            this.Items = ShowDataDGV_Itembarcode.items;
                            radTextBox_ItemBarcode.Text = this.Items.Itembarcode_ITEMBARCODE;
                            radLabel_ItemName.Text = this.Items.Itembarcode_SPC_ITEMNAME;
                            radLabel_Unit.Text = this.Items.Itembarcode_UNITID;
                            radLabel_TypePrice.Text = this.Items.Itembarcode_SPC_SalesPriceTypeNAME;
                        }
                    }
                    break;
            }
        }

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            TypeEdit = "Default"; ClearData(TypeEdit);
            if (radDropDownList_GroupItems.SelectedItems.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเพิ่มกลุ่มสินค้า ก่อนเพิ่มสินค้า.");
                return;
            }
            TypeEdit = "Add"; ClearData(TypeEdit);
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            TypeEdit = "Default"; ClearData(TypeEdit);
            if (radDropDownList_GroupItems.SelectedItems.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเพิ่มกลุ่มสินค้า ก่อนแก้ไขสินค้า.");
                return;
            }

            if (radTextBox_ItemBarcode.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเลือกรายการก่อนการกดแก้ไข.");
                return;
            }
            TypeEdit = "Edit"; ClearData(TypeEdit);
        }

        private void RadGridView_Show_SelectionChanged(object sender, EventArgs e)
        {
            if (iRow == RadGridView_Show.CurrentRow.Index) return;

            if (RadGridView_Show.Rows.Count > 0 && RadGridView_Show.CurrentRow.Index > -1)
            {
                radDropDownList_GroupItems.SelectedValue = RadGridView_Show.CurrentRow.Cells["ITEMGROUPID"].Value.ToString();

                radTextBox_ItemBarcode.Text = RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
                radLabel_ItemName.Text = RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString();
                radLabel_Unit.Text = RadGridView_Show.CurrentRow.Cells["ITEMUNIT"].Value.ToString();
                radLabel_TypePrice.Text = RadGridView_Show.CurrentRow.Cells["SPC_SalesPriceTypeNAME"].Value.ToString();
                radTextBox_Cost.Text = RadGridView_Show.CurrentRow.Cells["COSTITEM"].Value.ToString();
                iRow = RadGridView_Show.CurrentRow.Index;
                if (RadGridView_Show.CurrentRow.Cells["ITEMSTATUS1"].Value.ToString() == "1") radCheckBox_forOrder.Checked = true;
                else radCheckBox_forOrder.Checked = false;

                TypeEdit = "Default"; ClearData(TypeEdit);
            }
        }

        //ลบสินค้า
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (TypeEdit == "Edit")
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmDelete(Environment.NewLine +
                   "กลุ่มสินค้า " + RadGridView_Show.CurrentRow.Cells["ITEMGROUPID"].Value.ToString() + Environment.NewLine +
                   "บาร์โค๊ด " + radTextBox_ItemBarcode.Text + Environment.NewLine +
                   "ชื่อสินค้า " + radLabel_ItemName.Text + Environment.NewLine) == DialogResult.Yes)
                {
                    Var_ITEMGROUPBARCODE var_ITEMBARCODE = new Var_ITEMGROUPBARCODE
                    {
                        ITEMBARCODE = radTextBox_ItemBarcode.Text,
                        ITEMGROUPTYPE = _TypePage,
                        ITEMGROUPID = RadGridView_Show.CurrentRow.Cells["ITEMGROUPID"].Value.ToString()
                    };
               
                    string SqlTranctions = ConnectionClass.ExecuteSQL_Main(ItemGroup_Class.Save_ItemGroupBarcode("Del", var_ITEMBARCODE, ""));
                    MsgBoxClass.MsgBoxShow_SaveStatus(SqlTranctions);
                    if (SqlTranctions == "")
                    {
                        SetDatagridview();
                    }
                }
            }

            TypeEdit = "Default"; ClearData(TypeEdit);
            this.Cursor = Cursors.Default;
        }
        //บันทึก
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(radLabel_ItemName.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อกลุ่มสินค้า ให้เรียบร้อย.");
                radTextBox_ItemBarcode.SelectAll();
                radTextBox_ItemBarcode.Focus();
                return;
            }

            string statusBranchOrder = "0";

            if (radCheckBox_forOrder.Checked == true) statusBranchOrder = "1";

            if (double.TryParse(radTextBox_Cost.Text, out double costPrice) == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ต้นทุนที่ระบุไม่ถูกต้อง{Environment.NewLine}เช็คข้อมูลต้นทุนใหม่อีกครั้ง");
                radTextBox_Cost.Focus();
                return;
            }

            Var_ITEMGROUPBARCODE var_ITEMBARCODE = new Var_ITEMGROUPBARCODE
            {
                ITEMGROUPID = radDropDownList_GroupItems.SelectedValue.ToString(),
                ITEMGROUPTYPE = _TypePage,
                ITEMDEPT = RadDropDownList_Dept.SelectedValue.ToString(),
                ITEMBARCODE = radTextBox_ItemBarcode.Text,
                ITEMUNIT = radLabel_Unit.Text,
                ITEMSTATUS1 = statusBranchOrder,
                ITEMSTATUS1NAME = "ใช้งาน",
                ITEMREMARK = radTextBox_Rmk.Text,
                ITEMCOST = costPrice
            };
            string EditITEMGROUPID = radDropDownList_GroupItems.SelectedValue.ToString();
            string resualt = ConnectionClass.ExecuteSQL_Main(ItemGroup_Class.Save_ItemGroupBarcode(TypeEdit, var_ITEMBARCODE, EditITEMGROUPID));
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                SetDatagridview();
                TypeEdit = "Default"; ClearData(TypeEdit);
            }
            this.Cursor = Cursors.Default;
             
        }
        //doument
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _TypePage);
        }

        //Ket EmpID
        private void RadTextBox_EmplId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
        //เปลี่ยนกลุ่มสินค้า
        private void RadDropDownList_GroupItems_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedItems.Count > 0 && radDropDownList_GroupItems.SelectedItems.Count > 0) SetDatagridview();
        }


        #endregion

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (RadGridView_Show.ColumnCount > 0 && RadGridView_Show.RowCount > 0) if (RadGridView_Show.CurrentColumn.Index < 6) return;
            if (radDropDownList_GroupItems.SelectedValue.ToString() != "D034_01") return;

            double cost = double.Parse(RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString());
           
            InputData frmQTY = new InputData("0",
                                     RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                     "ราคา สำหรับ " + RadGridView_Show.Columns[e.ColumnIndex].HeaderText, RadGridView_Show.CurrentRow.Cells["ITEMUNIT"].Value.ToString())
            { pInputData = cost.ToString("N2") };

            if (frmQTY.ShowDialog(this) == DialogResult.Yes)
            {
                string res = ConnectionClass.ExecuteSQL_Main(ItemGroup_Class.Save_ItemCostByBranchID(RadGridView_Show.Columns[e.ColumnIndex].Name,
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                        Double.Parse(frmQTY.pInputData)));
                if (res == "")
                {
                    dtShow.Rows[e.RowIndex][e.Column.FieldName] = Convert.ToDouble(frmQTY.pInputData).ToString("#,#0.00");
                    dtShow.AcceptChanges();
                }
                else MsgBoxClass.MsgBoxShow_SaveStatus(res);
            }

        }
    }
}
