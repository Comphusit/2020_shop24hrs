﻿//CheckOK
using System;
using System.Linq;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.Windows.Diagrams.Core;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare;
using System.Collections;
using PC_Shop24Hrs.GeneralForm.MNOR;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class MNOR_LoadOrder : Telerik.WinControls.UI.RadForm
    {
        private DataTable dtBranch = new DataTable();
        DataTable data = new DataTable();

        string[] itembarcodeall;

        public MNOR_LoadOrder()
        {
            InitializeComponent();
        }
        //Load Main
        private void MNOR_LoadOrder_Load(object sender, EventArgs e)
        {
            radStatusStrip_Menu.SizingGrip = false;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Recieve, DateTime.Now.AddDays(1), DateTime.Now.AddDays(1));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Recieve2, DateTime.Now.AddDays(1), DateTime.Now.AddDays(7));
            radButtonElement_Clear.ShowBorder = true; radButtonElement_Clear.ToolTipText = "Clear";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            radButtonElement_SavePO.ShowBorder = true; radButtonElement_SavePO.ToolTipText = "บันทึก PO"; radButtonElement_SavePO.Enabled = false;
            RadButtonElement_pdf.ShowBorder = true; RadButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Search.ButtonElement.ShowBorder = true;
            radCheckBox_BchType.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_Branch.Checked = false;
            RadCheckBox_GrpSub.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_GrpSub.Checked = false;

            radCheckBox_routeBeer.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_SubGroup);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_BchType);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_routeBeer); radDropDownList_routeBeer.Enabled = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);


            SetDropdownlist();
            ClearData();
        }
        void ClearData()
        {
            radDateTimePicker_Recieve2.Value = DateTime.Now.AddDays(1);
            radDateTimePicker_Recieve.Value = DateTime.Now.AddDays(1);

            try
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemDptID;
            }
            catch (Exception)
            {
                RadDropDownList_Branch.SelectedIndex = 0;
            }

            if (RadDropDownList_Branch.SelectedIndex == -1) RadDropDownList_Branch.SelectedIndex = 0;

            if (data.Rows.Count > 0) data.Rows.Clear();

            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.Columns.Count > 0) { this.RadGridView_Show.Columns.Clear(); this.RadGridView_Show.SummaryRowsTop.Clear(); }

            RadCheckBox_Branch.ReadOnly = false;
            RadCheckBox_GrpSub.ReadOnly = false;
            radDateTimePicker_Recieve.Enabled = true; radDateTimePicker_Recieve2.Enabled = true;
            RadDropDownList_Branch.Enabled = true;
            radDropDownList_MainGroup.Enabled = true;


            if (radDropDownList_MainGroup.SelectedValue.ToString() == "D034")
            {
                radCheckBox_routeBeer.Enabled = true;
                RadCheckBox_GrpSub.Enabled = false; RadCheckBox_GrpSub.Checked = true; radDropDownList_routeBeer.Enabled = true;
                radDropDownList_SubGroup.Enabled = true;
            }
            else
            {
                radCheckBox_routeBeer.Enabled = false; radCheckBox_routeBeer.Checked = false;
                RadCheckBox_GrpSub.Enabled = true;
            }

        }
        void SetDropdownlist()
        {
            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            RadDropDownList_Branch.DataSource = dtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.Enabled = false;

            //radDropDownList_MainGroup.DataSource = ORClass.GetDptAll();
            radDropDownList_MainGroup.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("47", "", "", "1");
            radDropDownList_MainGroup.DisplayMember = "SHOW_NAME";
            radDropDownList_MainGroup.ValueMember = "SHOW_ID";
            radDropDownList_SubGroup.Enabled = false;

            radDropDownList_MainGroup.SelectedValue = SystemClass.SystemDptID;
            if (radDropDownList_MainGroup.SelectedValue is null) radDropDownList_MainGroup.SelectedIndex = 0;

            radDropDownList_BchType.DataSource = BranchClass.GetBranchType();
            radDropDownList_BchType.DisplayMember = "OUTPHUKET";
            radDropDownList_BchType.ValueMember = "BRANCH_OUTPHUKET";
            radDropDownList_BchType.Enabled = false;

            radDropDownList_routeBeer.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("49", "", "", "1");
            radDropDownList_routeBeer.DisplayMember = "SHOW_NAME";
            radDropDownList_routeBeer.ValueMember = "SHOW_ID";
        }
        //แสดงกลุ่มย่อย
        void SetGroupSub(string _pGrpmain)
        {
            radDropDownList_SubGroup.DataSource = ItemGroup_Class.GetItemGroup(_pGrpmain, "01", "  AND ITEMGROUPSTATUS1 = '1'  ");
            radDropDownList_SubGroup.ValueMember = "ITEMGROUPID";
            radDropDownList_SubGroup.DisplayMember = "ITEMGROUPIDNAME";
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //checkChange
        private void RadCheckBox_GrpSub_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpSub.Checked == true)
            {
                SetGroupSub(radDropDownList_MainGroup.SelectedValue.ToString());
                radDropDownList_SubGroup.Enabled = true;
            }
            else
            {
                radDropDownList_SubGroup.Enabled = false;
            }
        }
        //เลือกสาขา
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;

        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        private void RadGridView_Show_CustomFiltering(object sender, GridViewCustomFilteringEventArgs e)
        {
            try
            {
                e.Visible = Convert.ToDouble(e.Row.Cells["SUM"].Value.ToString()) > 0;
            }
            catch (Exception)
            {
                return;
            }
        }
        //LoadData
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_Show.Columns.Count > 0)
            {
                this.RadGridView_Show.Columns.Clear();
                this.RadGridView_Show.SummaryRowsTop.Clear();
            }
            //GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
            DataTable dtColume;// = dtBranch;
            DataTable dtItem = GetItem();
            DataTable dtItemOrder = MNOR_Class.GetItemOrder(radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd"), radDateTimePicker_Recieve2.Value.ToString("yyyy-MM-dd"));

            string pCon = "";
            if (radCheckBox_BchType.Checked == true) pCon = $@" AND BRANCH_OUTPHUKET = '{radDropDownList_BchType.SelectedValue}' ";
            if (radCheckBox_routeBeer.Checked == true) pCon += $@" AND SHOP_BRANCH_CONFIGDB.BRANCH_ROUTEBEER = '{radDropDownList_routeBeer.SelectedValue}' ";

            if (RadCheckBox_Branch.Checked == true)
                dtColume = BranchClass.GetDetailBranchByID(RadDropDownList_Branch.SelectedValue.ToString());
            else dtColume = BranchClass.GetBranchAll_ByConditions("'1'", "'1'", pCon);

            string[] Columns = new string[dtColume.Rows.Count];
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("SUM", "รวม", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("ITEMCOST", "ต้นทุน", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COST", "ราคารวม", 120));

            GridViewSummaryItem sumItemValue_TOTALCOM = new GridViewSummaryItem("SUM", "{0:#,##0.00}", GridAggregateFunction.Sum);
            GridViewSummaryItem sumItemValue_RECEIVESUM = new GridViewSummaryItem("COST", "{0:#,##0.00}", GridAggregateFunction.Sum);
            GridViewSummaryRowItem sumGroupCol1 = new GridViewSummaryRowItem(new GridViewSummaryItem[] {
                    sumItemValue_TOTALCOM,
                    sumItemValue_RECEIVESUM});


            if (radDropDownList_MainGroup.SelectedValue.ToString() != "D034")
            {
                RadGridView_Show.Columns["ITEMCOST"].IsVisible = false;
                RadGridView_Show.Columns["COST"].IsVisible = false;
            }

            RadGridView_Show.MasterTemplate.Columns["SUM"].FormatString = "{0:F2}";

            RadGridView_Show.Columns["ITEMBARCODE"].IsPinned = true;
            RadGridView_Show.Columns["SPC_ITEMNAME"].IsPinned = true;
            RadGridView_Show.Columns["SUM"].IsPinned = true;
             
            int count = 0;
            foreach (DataRow dataRow in dtColume.Rows)
            {
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight(dataRow["BRANCH_ID"].ToString(), dataRow["BRANCH_ID"].ToString() + "-" + dataRow["BRANCH_NAME"].ToString(), 100));
                RadGridView_Show.MasterTemplate.Columns[dataRow["BRANCH_ID"].ToString()].FormatString = "{0:F2}";
                GridViewSummaryItem AA = new GridViewSummaryItem(dataRow["BRANCH_ID"].ToString(), "{0:#,##0.00}", GridAggregateFunction.Sum);
                sumGroupCol1.Add(AA);
                Columns[count] = dataRow["BRANCH_ID"].ToString();
                count++;
            }
            RadGridView_Show.MasterTemplate.SummaryRowsTop.Add(sumGroupCol1);
            data = AddNewColumn(new string[] { "ITEMBARCODE", "SPC_ITEMNAME", "SUM", "UNITID", "ITEMCOST", "COST" }, new string[] { "ITEMBARCODE" });
            AppendNewColumn(data, Columns);
            itembarcodeall = new string[dtItem.Rows.Count];

            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                double SumQty = 0.0;
                DataRow dr = data.NewRow();
                dr["ITEMBARCODE"] = dtItem.Rows[i]["ITEMBARCODE"].ToString();
                dr["SPC_ITEMNAME"] = dtItem.Rows[i]["SPC_ITEMNAME"].ToString();
                dr["UNITID"] = dtItem.Rows[i]["UNITID"].ToString();
                dr["SUM"] = 0;
                dr["ITEMCOST"] = dtItem.Rows[i]["ITEMCOST"].ToString();
                itembarcodeall[i] = dtItem.Rows[i]["ITEMBARCODE"].ToString();

                foreach (var Column in Columns)
                {
                    dr[Column] = GetOrderByBranch(dtItem.Rows[i]["ITEMBARCODE"].ToString(), Column, dtItemOrder).ToString("#,#0.00");
                    SumQty += Convert.ToDouble(dr[Column]);
                }

                dr["SUM"] = SumQty.ToString("#,#0.00");
                dr["COST"] = (SumQty * Convert.ToDouble(dtItem.Rows[i]["ITEMCOST"].ToString())).ToString("#,#0.00");
                if (Convert.ToDouble(dr["SUM"]) > 0) data.Rows.Add(dr);

            }

            RadGridView_Show.DataSource = data;
            RadCheckBox_Branch.ReadOnly = true;
            RadCheckBox_GrpSub.ReadOnly = true;
            radDateTimePicker_Recieve.Enabled = false; radDateTimePicker_Recieve2.Enabled = false;
            RadDropDownList_Branch.Enabled = false;
            radDropDownList_MainGroup.Enabled = false;
            radDropDownList_SubGroup.Enabled = false; radCheckBox_routeBeer.Enabled = false; radDropDownList_routeBeer.Enabled = false;

            if (RadGridView_Show.DataMember != null)
            {
                foreach (var Column in Columns)
                {
                    double SumQty = 0.0;
                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                    {
                        SumQty += Convert.ToDouble(RadGridView_Show.Rows[i].Cells[Column].Value);
                    }
                    if (SumQty == 0)
                    {
                        RadGridView_Show.Columns[Column].IsVisible = false;
                    }
                }
            }

            if ((RadGridView_Show.Rows.Count > 0) && (radDropDownList_MainGroup.SelectedValue.ToString() == "D034")) radButtonElement_SavePO.Enabled = true;

            this.Cursor = Cursors.Default;
        }
        private DataTable GetItem()
        {
            DataTable dtret;
            if (RadCheckBox_GrpSub.Checked == true)
                dtret = ItembarcodeClass.ItemGroup_GetItemByGroupForOrder(radDropDownList_SubGroup.SelectedValue.ToString());
            else
                dtret = ItembarcodeClass.ItemGroup_GetItemByGroupForOrder(radDropDownList_MainGroup.SelectedValue.ToString() + "%");
            return dtret;
        }
       
        private double GetOrderByBranch(string barcode, string branchid, DataTable table, double qty = 0.0)
        {
            DataRow[] rows = table.Select(string.Format(@"MNORBarcode = '{0}' AND MNORBranch = '{1}'", barcode, branchid));
            foreach (var row in rows)
            {
                qty = Convert.ToDouble(row["MNORQtyOrder"]);
            }
            return qty;
        }

        
        DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null)
            {
                return newColumn;
            }
            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];

                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }
            return newColumn;
        }
        private void AppendNewColumn(DataTable table, string[] colNames)
        {
            colNames.ForEach(colName => table.Columns.Add(new DataColumn(colName, typeof(string))));
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.ColumnIndex > 5)
            {
                if (e.RowIndex > -1)
                {
                    MNOR_ITEM iTEM = new MNOR_ITEM()
                    {
                        BRANCH = e.Column.FieldName,
                        BARCODE = this.RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                        ITEMNAME = this.RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                        UNITID = this.RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString(),
                        QTY = Convert.ToDouble(e.Value),
                    };

                    //ในกรณีที่วันที่ดึงวันที่เดียวกัน เมื่อแก้ไขก็ให้เข้าใบ MNOR เลย
                    if (radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd") == radDateTimePicker_Recieve2.Value.ToString("yyyy-MM-dd"))
                    {
                        iTEM.DOCNO = MNOR_Class.GetDataOrderDT(iTEM.BRANCH, iTEM.BARCODE, radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd"));

                        InputData frm = new InputData("0", iTEM.BARCODE, iTEM.ITEMNAME, iTEM.UNITID);
                        if (frm.ShowDialog(this) == DialogResult.Yes)
                        {
                            iTEM.QTY = Convert.ToDouble(frm.pInputData);
                            string ret = MNOR_Class.UpdateQty(iTEM.DOCNO, iTEM.BARCODE, iTEM.QTY);
                            if (ret == "")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกเรียบร้อย.");
                                data.Rows[e.RowIndex][e.Column.FieldName] = iTEM.QTY.ToString("#,#0.00");
                                data.AcceptChanges();
                                SetDGV();
                            }
                            else
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error(ret); return;
                            }
                        }
                    }
                    //ในกรณีที่เลือกวันที่ในช่วงระหว่าง ก็ไม่เปลี่ยนใน DB แต่เปลี่ยนใน Grid
                    else
                    {
                        InputData frm = new InputData("0", iTEM.BARCODE, iTEM.ITEMNAME, iTEM.UNITID)
                        {
                            pInputData = iTEM.QTY.ToString()//String.Format("0.00", iTEM.QTY)
                        };
                        if (frm.ShowDialog(this) == DialogResult.Yes)
                        {
                            data.Rows[e.RowIndex]["SUM"] = ((Convert.ToDouble(data.Rows[e.RowIndex]["SUM"]) - iTEM.QTY) + Convert.ToDouble(frm.pInputData)).ToString("#,#0.00");

                            iTEM.QTY = Convert.ToDouble(frm.pInputData);

                            data.Rows[e.RowIndex][e.Column.FieldName] = iTEM.QTY.ToString("#,#0.00");

                            if (radDropDownList_MainGroup.SelectedValue.ToString() == "D034")
                            {
                                data.Rows[e.RowIndex]["COST"] = (Convert.ToDouble(data.Rows[e.RowIndex]["SUM"]) * Convert.ToDouble(data.Rows[e.RowIndex]["ITEMCOST"])).ToString("#,#0.00");
                            }
                            data.AcceptChanges();
                        }
                    }


                }
            }
        }
        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายการจัดสินค้าเบิก " + radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
 
        private void RadButtonElement_SavePO_Click(object sender, EventArgs e)
        {

            if (RadGridView_Show.Rows.Count == 0) { MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่มีข้อมูลสำหรับการบันทึกใบสั่งสินค้า{Environment.NewLine}เช็คข้อมูลแล้วลองใหม่อีกครั้ง."); return; }
            if (RadCheckBox_GrpSub.Checked == false) { MsgBoxClass.MsgBoxShowButtonOk_Error($@"การทำใบสั่งสินค้าต้องเลือกตามกลุ่มเท่านั้น{Environment.NewLine}เช็คข้อมูลแล้วลองใหม่อีกครั้ง."); return; }

            DataTable dtGrpSub = ItemGroup_Class.GetItemGroup($@"{radDropDownList_MainGroup.SelectedValue}", "01", $@"  AND ITEMGROUPID = '{radDropDownList_SubGroup.SelectedValue}'  ");
            string vender = dtGrpSub.Rows[0]["ITEMGROUPVEND"].ToString();

            if (MessageBox.Show("ยืนยันการสร้างใบสั่งซื้อเข้าระบบ AX ?.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;

            string rmk = "";
            if (vender != "")
            {
                FormShare.InputDate frmDate = new InputDate("วันที่ที่ต้องโอนเงิน", "บันทึก", "ยกเลิก", "dd-MM-yyyy", "");
                if (frmDate.ShowDialog(this) == DialogResult.Yes)
                {
                    rmk = " โอน " + frmDate.pInputDate + "  ธนาคารกสิกรไทย สำนักงานพหลโยธิน กระแสรายวัน 099-1-27299-2 สาขาใหญ่";
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ต้องระบุ วันที่ที่ต้องโอนเงินเท่านั้น{Environment.NewLine}ลองใหม่อีกครั้ง.");
                    return;
                }
            }

            Boolean checkCost0 = false;

            ArrayList sqlAX = new ArrayList();
            foreach (GridViewDataColumn dataColumn in RadGridView_Show.Columns)
            {
                //เช็คแล้วไม่ใช่ Columns จำนวนที่ต้องการบันทึกเอกสารใหม่
                if (!dataColumn.Name.ToString().Substring(0, 2).Equals("MN")) continue;
                //เช็คสาขาที่ไม่มีออเดอร์ ออก
                if (dataColumn.IsVisible == false) continue;

                string branchId = dataColumn.Name.ToString();
                //DataTable dtBch = BranchClass.GetConfigBranch($@" WHERE BRANCH_ID = '{branchId}' ");
                DataTable dtBch = BranchClass.GetBranchSettingBranch(branchId);
                string branchName = dtBch.Rows[0]["BRANCH_NAME"].ToString(); // BranchClass.GetBranchNameByID(branchId);

                string venderBill = vender;
                string vatINCTAX = "0";// 0 vat แยกนอก 1 vat รวมใน --กรณีของ D034_01 vat รวมใน
                if (vender == "") { venderBill = dtBch.Rows[0]["BRANCH_VENDERBEER"].ToString(); vatINCTAX = "1"; }
                if (venderBill == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"การตั้งค่า ผู้จำหน่าย สิงห์-ลีโอ ในหน้าจอสาขายังไม่เรียบร้อย{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้งก่อนทำบิล");
                    return;
                }

                string Docno = "";

                //Fix กรณีขอทัปปุด ที่เดียว
                if (branchId == "MN051")
                {
                    int sqlax51 = 0;
                    for (int iRow51 = 0; iRow51 < RadGridView_Show.Rows.Count; iRow51++)
                    {
                        double Qty = double.Parse(RadGridView_Show.Rows[iRow51].Cells[dataColumn.Index].Value.ToString());
                        if (Qty > 0)
                        {
                            string Barcode = RadGridView_Show.Rows[iRow51].Cells["ITEMBARCODE"].Value.ToString();
                            string Name = RadGridView_Show.Rows[iRow51].Cells["SPC_ITEMNAME"].Value.ToString();

                            double Price = double.Parse(RadGridView_Show.Rows[iRow51].Cells["ITEMCOST"].Value.ToString());
                            //if (vender == "") Price = FindCostBeer(Barcode, branchId);
                            if (vender == "") Price = MNOR_Class.FindCostPriceBeer(Barcode, branchId);

                            if (Price == 0) checkCost0 = true;
                            //ในกรณีที่เป็นทัปปุด บาร์โค้ดโซดา เท่านั้น
                            if ((RadGridView_Show.Rows[iRow51].Cells["ITEMBARCODE"].Value.ToString() == "8850999220024"))
                            {
                                Docno = Class.ConfigClass.GetMaxINVOICEID("MNPR", "-", "MNPR", "1");
                                //sqlAX.Add(SaveHD(Docno, "V014090", branchId, branchName, branchName));
                                Var_PURCHTABLE var_PURCHTABLE = new Var_PURCHTABLE
                                {
                                    PURCHASETYPE = "3",
                                    PURCHSTATUS = "1",
                                    TAXINVOICE = "1",
                                    INCLTAX = vatINCTAX,
                                    PURCHID = Docno,
                                    PURCHDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                                    ORDERACCOUNT = "V014090",
                                    INVENTLOCATIONID = branchId,
                                    DIMENSION = radDropDownList_MainGroup.SelectedValue.ToString(),
                                    REMARK = branchName + ' ' + rmk,
                                    INVOICEID = "",
                                    INVOICEDATE = "",
                                    VENDORREF = "",
                                    NUMRECEIVE = ""
                                };

                                //sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE("3", "1", "1", vatINCTAX, Docno, DateTime.Now.ToString("yyyy-MM-dd"), "V014090",
                                //                            branchId, radDropDownList_MainGroup.SelectedValue.ToString(), branchName + ' ' + rmk, "", "", "", ""));
                                sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE(var_PURCHTABLE));
                                //sqlAX.Add(SaveDT(Docno, 1, Qty, Price, Barcode, Name));
                                Var_PURCHLINE var_PURCHLINE = new Var_PURCHLINE
                                {
                                    PURCHID = Docno,
                                    PURCHDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                                    LINENUM = 1,
                                    BARCODE = Barcode,
                                    NAME = Name,
                                    REMARK = "",
                                    PURCHQTY = Qty,
                                    PURCHPRICE = Price
                                };
                                //sqlAX.Add(AX_SendData.SaveDT_PURCHTABLE(Docno, DateTime.Now.ToString("yyyy-MM-dd"), 1, Barcode, Name, Qty, Price, ""));
                                sqlAX.Add(AX_SendData.SaveDT_PURCHLINE(var_PURCHLINE));
                            }
                            else
                            {
                                sqlax51++;
                                if (sqlax51 == 1)
                                {
                                    Docno = Class.ConfigClass.GetMaxINVOICEID("MNPR", "-", "MNPR", "1");
                                    //sqlAX.Add(SaveHD(Docno, venderBill, branchId, branchName, branchName));
                                    Var_PURCHTABLE var_PURCHTABLE = new Var_PURCHTABLE
                                    {
                                        PURCHASETYPE = "3",
                                        PURCHSTATUS = "1",
                                        TAXINVOICE = "1",
                                        INCLTAX = vatINCTAX,
                                        PURCHID = Docno,
                                        PURCHDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                                        ORDERACCOUNT = venderBill,
                                        INVENTLOCATIONID = branchId,
                                        DIMENSION = radDropDownList_MainGroup.SelectedValue.ToString(),
                                        REMARK = branchName + ' ' + rmk,
                                        INVOICEID = "",
                                        INVOICEDATE = "",
                                        VENDORREF = "",
                                        NUMRECEIVE = ""
                                    };
                                    //sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE("3", "1", "1", vatINCTAX, Docno, DateTime.Now.ToString("yyyy-MM-dd"), venderBill,
                                    //    branchId, radDropDownList_MainGroup.SelectedValue.ToString(), branchName + ' ' + rmk, "", "", "", ""));
                                    sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE(var_PURCHTABLE));
                                }
                                //sqlAX.Add(SaveDT(Docno, 1, Qty, Price, Barcode, Name));
                                Var_PURCHLINE var_PURCHLINE = new Var_PURCHLINE
                                {
                                    PURCHID = Docno,
                                    PURCHDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                                    LINENUM = sqlax51,
                                    BARCODE = Barcode,
                                    NAME = Name,
                                    REMARK = "",
                                    PURCHQTY = Qty,
                                    PURCHPRICE = Price
                                };
                                //sqlAX.Add(AX_SendData.SaveDT_PURCHTABLE(Docno, DateTime.Now.ToString("yyyy-MM-dd"), sqlax51, Barcode, Name, Qty, Price, ""));
                                sqlAX.Add(AX_SendData.SaveDT_PURCHLINE(var_PURCHLINE));
                            }
                        }
                    }

                }
                else
                {
                    Docno = Class.ConfigClass.GetMaxINVOICEID("MNPR", "-", "MNPR", "1");
                    //sqlAX.Add(SaveHD(Docno, venderBill, branchId, branchName, rmk));
                    Var_PURCHTABLE var_PURCHTABLE = new Var_PURCHTABLE
                    {
                        PURCHASETYPE = "3",
                        PURCHSTATUS = "1",
                        TAXINVOICE = "1",
                        INCLTAX = vatINCTAX,
                        PURCHID = Docno,
                        PURCHDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                        ORDERACCOUNT = venderBill,
                        INVENTLOCATIONID = branchId,
                        DIMENSION = radDropDownList_MainGroup.SelectedValue.ToString(),
                        REMARK = branchName + ' ' + rmk,
                        INVOICEID = "",
                        INVOICEDATE = "",
                        VENDORREF = "",
                        NUMRECEIVE = ""
                    };
                    //sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE("3", "1", "1", vatINCTAX, Docno, DateTime.Now.ToString("yyyy-MM-dd"), venderBill,
                    //                    branchId, radDropDownList_MainGroup.SelectedValue.ToString(), branchName + ' ' + rmk, "", "", "", ""));
                    sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE(var_PURCHTABLE));

                    int iR = 0;
                    for (int iRow = 0; iRow < RadGridView_Show.Rows.Count; iRow++)
                    {
                        double Qty = double.Parse(RadGridView_Show.Rows[iRow].Cells[dataColumn.Index].Value.ToString());
                        if (Qty == 0) continue;
                        iR++;
                        string Barcode = RadGridView_Show.Rows[iRow].Cells["ITEMBARCODE"].Value.ToString();
                        string Name = RadGridView_Show.Rows[iRow].Cells["SPC_ITEMNAME"].Value.ToString();
                        double Price = double.Parse(RadGridView_Show.Rows[iRow].Cells["ITEMCOST"].Value.ToString());
                        //if (vender == "") Price = FindCostBeer(Barcode, branchId);
                        if (vender == "") Price = MNOR_Class.FindCostPriceBeer(Barcode, branchId);

                        if (Price == 0) checkCost0 = true;

                        //sqlAX.Add(SaveDT(Docno, iR, Qty, Price, Barcode, Name));
                        Var_PURCHLINE var_PURCHLINE = new Var_PURCHLINE
                        {
                            PURCHID = Docno,
                            PURCHDATE = DateTime.Now.ToString("yyyy-MM-dd"),
                            LINENUM = iR,
                            BARCODE = Barcode,
                            NAME = Name,
                            REMARK = "",
                            PURCHQTY = Qty,
                            PURCHPRICE = Price
                        };
                        //sqlAX.Add(AX_SendData.SaveDT_PURCHTABLE(Docno, DateTime.Now.ToString("yyyy-MM-dd"), iR, Barcode, Name, Qty, Price, ""));
                        sqlAX.Add(AX_SendData.SaveDT_PURCHLINE(var_PURCHLINE));
                    }

                }
            }

            if (checkCost0 == true)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"มีสินค้าบางรายการที่ราคาต้นทุนยังเป็น 0 {Environment.NewLine}ต้องจัดการการตั้งค่าให้เสร็จเรียบร้อยก่อนเท่านั้น.");
                return;
            }

            if (sqlAX.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่มีรายการสำหรับนำเข้าใบสั่งซื้อ{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง");
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            string res = ConnectionClass.ExecuteSQL_ArrayMainAX(sqlAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(res);
            this.Cursor = Cursors.Default;
        }
        //Double FindCostBeer(string pBarcode, string pBchID)
        //{
        //    string str = $@"
        //        SELECT	ISNULL(REMARK,0) AS COST
        //        FROM	SHOP_CONFIGBRANCH_DETAIL  WITH (NOLOCK)  
        //        WHERE	TYPE_CONFIG ='50' AND SHOW_ID = '{pBarcode}'	
        //          AND BRANCH_ID IN (SELECT BRANCH_PROVINCE FROM	SHOP_BRANCH WITH (NOLOCK) WHERE	BRANCH_ID = '{pBchID}')
        //    ";
        //    DataTable dt = ConnectionClass.SelectSQL_Main(str);
        //    if (dt.Rows.Count == 0) return 0;
        //    else return Double.Parse(dt.Rows[0]["COST"].ToString());
        //}
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        private void RadDropDownList_MainGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!(radDropDownList_MainGroup.SelectedValue is null))
            {
                SetGroupSub(radDropDownList_MainGroup.SelectedValue.ToString());

                if (radDropDownList_MainGroup.SelectedValue.ToString() == "D034")
                {
                    radCheckBox_routeBeer.Enabled = true;
                    RadCheckBox_GrpSub.Enabled = false; RadCheckBox_GrpSub.Checked = true;
                }
                else
                {
                    radCheckBox_routeBeer.Enabled = false; radCheckBox_routeBeer.Checked = false;
                    RadCheckBox_GrpSub.Enabled = true;
                }
            }
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadCheckBox_BchType_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_BchType.Checked == true) radDropDownList_BchType.Enabled = true;
            else radDropDownList_BchType.Enabled = false;
        }

        private void RadCheckBox_routeBeer_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_routeBeer.Checked == true) radDropDownList_routeBeer.Enabled = true;
            else radDropDownList_routeBeer.Enabled = false;
        }

        private void RadDateTimePicker_Recieve2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Recieve, radDateTimePicker_Recieve2);
        }

        private void RadDateTimePicker_Recieve_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Recieve, radDateTimePicker_Recieve2);
        }

    }
}

