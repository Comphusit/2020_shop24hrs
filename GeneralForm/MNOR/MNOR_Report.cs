﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.Drawing;
using PC_Shop24Hrs.GeneralForm.MNOR;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class MNOR_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;

        // 0 - สรุปใบสั่งซื้อ ทังหมด

        //Load
        public MNOR_Report(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load
        private void MNOR_Report_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์เอกสารใบสั่งซื้อทั้งหมด";
            radButtonElement_Report.ShowBorder = true; radButtonElement_Report.ToolTipText = "พิมพ์เอกสารรายงานทั้งหมด";
            RadButton_Search.ButtonElement.ShowBorder = true;


            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(radGridView_PO);
            DatagridClass.SetDefaultRadGridView(radGridView_Report);

            DatagridClass.SetDefaultFontDropDown(radDropDownList_SubGroup);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);

            radLabel_Date.Visible = false; radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;

            radDropDownList_MainGroup.Visible = false; radDropDownList_SubGroup.Visible = false;

            radLabel_Asset.Visible = false; radTextBox_Asset.Visible = false;

            switch (_pTypeReport)
            {
                case "0"://กรณีสรุปใบสั่งซื้อ

                    radLabel_Date.Visible = true; radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Detail.Text = "ระบุวันที่ >> เพื่อเพื่ออกข้อมูล กรณีต้องออกรายงานให้เลือกช่วงวันที่ในการดึงรายงาน";

                    radDropDownList_MainGroup.Visible = true; radDropDownList_SubGroup.Visible = true;
                    radDropDownList_MainGroup.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("47", "", "", "1");
                    radDropDownList_MainGroup.DisplayMember = "SHOW_NAME";
                    radDropDownList_MainGroup.ValueMember = "SHOW_ID";

                    radDropDownList_MainGroup.SelectedValue = SystemClass.SystemDptID;
                    if (radDropDownList_MainGroup.SelectedValue is null) radDropDownList_MainGroup.SelectedIndex = 0;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("RROWS", "ลำดับ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("OrderAccount", "ผู้จำหน่าย", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PURCHNAME", "ชื่อผู้จำหน่าย", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PurchId", $@"เลขที่ใบสั่งซื้อ", 140));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DELIVERYNAME", "ชื่อสาขา", 160));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_BEFOREVAT", "ยอดก่อน(VAT)", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_VAT", "ยอดรวม(VAT)", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("BB", "บัญชี", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PRT", "REPORT", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PO", "PO", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("IVZ_REMARKS", "Remark"));

                    GridViewSummaryItem sumItemValue_BEFOREVAT = new GridViewSummaryItem("SUM_BEFOREVAT", "{0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryItem sumItemValue_SUM_VAT = new GridViewSummaryItem("SUM_VAT", "{0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem sumGroupCol1 = new GridViewSummaryRowItem(new GridViewSummaryItem[] {
                    sumItemValue_BEFOREVAT,
                    sumItemValue_SUM_VAT});
                    RadGridView_ShowHD.MasterTemplate.SummaryRowsBottom.Add(sumGroupCol1);

                    //รายละเอียดรายงาน
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PRODUCT", "บาร์โค้ด"));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PURCHUNIT", "หน่วย"));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("NAME", "ชื่อสินค้า"));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_PO", "รวมซื้อ", 130));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_SALE", "รวมขาย", 130));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_I", "รวมเบิก", 130));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_TF", "รวมโอนย้าย", 130));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("STOCK", "คงเหลือ", 130));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Per_1", "%ขายเบิก/ซื้อ", 130));
                    radGridView_Report.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Per_2", "%ขายเบิกโอน/ซื้อ", 130));

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("PRODUCT", System.ComponentModel.ListSortDirection.Ascending);
                    descriptor.Format = "สินค้า {1}";

                    radGridView_Report.GroupDescriptors.Add(descriptor);

                    DatagridClass.SetCellBackClolorByExpression("Per_1", "Per_1 < 50 ", ConfigClass.SetColor_Red(), radGridView_Report);
                    DatagridClass.SetCellBackClolorByExpression("Per_2", "Per_2 < 50 ", ConfigClass.SetColor_Red(), radGridView_Report);


                    ClearTxt();

                    break;

                default:
                    break;
            }

        }

        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0":
                    string strVender = "", vatIn = "1";
                    //สรุป Vender
                    if (radDropDownList_SubGroup.SelectedValue.ToString() == "D034_01")
                    {
                        DataTable dtVender = MNOR_Class.GetVenderBeer();// ConnectionClass.SelectSQL_Main(sqlVender);
                        if (dtVender.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีข้อมูลผู้จำหน่ายที่ตั้งค่าไว้{Environment.NewLine}เช็คใหม่อีกครั้ง.");
                            this.Cursor = Cursors.Default;
                            return;
                        }

                        for (int i = 0; i < dtVender.Rows.Count; i++)
                        {
                            strVender = strVender + "'" + dtVender.Rows[i]["BRANCH_VENDERBEER"].ToString() + "',";
                        }
                        strVender = strVender.Substring(0, strVender.Length - 1);
                        radLabel_Detail.Text = "กดพิมพ์ >> พิมพ์รายละเอียดใบสั่งซื้อทีละใบ"; radButtonElement_print.ToolTipText = "พิมพ์ใบสั่งซื้อทั้งหมด";
                        vatIn = "0";
                    }
                    else
                    {
                        DataTable dtGrpSub = ItemGroup_Class.GetItemGroup($@"{radDropDownList_MainGroup.SelectedValue}", "01", $@"  AND ITEMGROUPID = '{radDropDownList_SubGroup.SelectedValue}'  ");
                        string vender = dtGrpSub.Rows[0]["ITEMGROUPVEND"].ToString();
                        if (vender == "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กลุ่มสินค้านี้ไม่ได้ตั้งผู้จำหน่ายไว้ ไม่สามารถดึงใบปะหน้าการโอนได้{Environment.NewLine}เช็คใหม่อีกครั้ง.");
                            this.Cursor = Cursors.Default;
                            return;
                        }
                        strVender = "'" + vender + "'";
                    }
                    //Find Data ใบสั่งซื้อ HD

                    dt_Data = MNOR_Class.FindDataPurchTable(vatIn, strVender, radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_MainAX(sql);
                    if (dt_Data.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลบิลใบสั่งซื้อสำหรับวันที่ระบุ{Environment.NewLine}ลองใหม่อีกครั้ง"); this.Cursor = Cursors.Default; return;
                    }
                    radLabel_Detail.Text = "กดพิมพ์ >> พิมพ์รายละเอียดรายงานทีละใบ | กดปุ่ม Excel >> เพื่อพิมพ์ใบปะหน้าหรือ Export Excel | DoubleClick เลขที่ใบสั่งซื้อ >> พิมพ์ทั้ง PO และ Report";
                    radButtonElement_print.ToolTipText = "พิมพ์รายงานทั้งหมด";
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;

                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_PO_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
        }
        private void RadGridView_Report_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement is GridGroupContentCellElement)
            {
                e.CellElement.Font = SystemClass.SetFontGernaral_Bold; ;
            }
            else
            {
                e.CellElement.Font = SystemClass.SetFontGernaral;
            }

        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); RadGridView_ShowHD.DataSource = dt_Data; dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            radTextBox_Asset.Text = "";
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (radDropDownList_SubGroup.SelectedValue.ToString() == "D034_01")
            {
                if (RadGridView_ShowHD.Rows.Count == 0) { return; }
                MsgBoxClass.MsgBoxShow_SaveStatus(DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1"));
                return;
            }
            else
            {
                FormShare.InputDate frmDate = new FormShare.InputDate("วันที่ที่สร้างบิลใบสั่งซื้อ", "ตกลง", "ยกเลิก", "yyyy-MM-dd","");
                if (frmDate.ShowDialog(this) == DialogResult.Yes)
                {
                    GridPrintStyle printStyle = new GridPrintStyle(this.RadGridView_ShowHD);
                    DatagridClass.SetGridForPrint(printStyle, true, false);
                    TableViewDefinitionPrintRenderer renderer = new TableViewDefinitionPrintRenderer(this.RadGridView_ShowHD);
                    renderer.PrintPages.Add(RadGridView_ShowHD.Columns["RROWS"], RadGridView_ShowHD.Columns["PurchId"],
                            RadGridView_ShowHD.Columns["INVENTLOCATIONID"], RadGridView_ShowHD.Columns["DELIVERYNAME"],
                            RadGridView_ShowHD.Columns["SUM_BEFOREVAT"], RadGridView_ShowHD.Columns["SUM_VAT"], RadGridView_ShowHD.Columns["BB"]);

                    printStyle.PrintRenderer = renderer;
                    RadGridView_ShowHD.PrintStyle = printStyle;

                    RadPrintDocument document = new RadPrintDocument();
                    string l1 = $@"{RadGridView_ShowHD.Rows[0].Cells["OrderAccount"].Value}  - {RadGridView_ShowHD.Rows[0].Cells["PURCHNAME"].Value}";
                    string l2 = $@"กำหนดโอน {frmDate.pInputDate}";

                    DatagridClass.SetPrintDocumentPO(document, l1 + Environment.NewLine + l2 + Environment.NewLine, 60, false, "", "B", " SuperCheap");

                    document.AssociatedObject = this.RadGridView_ShowHD;
                    RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document)
                    {
                        StartPosition = FormStartPosition.CenterScreen,
                        WindowState = FormWindowState.Maximized
                    };
                    dialog.ShowDialog();
                }
                else
                {
                    if (RadGridView_ShowHD.Rows.Count == 0) { return; }
                    MsgBoxClass.MsgBoxShow_SaveStatus(DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1"));
                    return;
                }

            }

        }
        void SetPODetail(string docno)
        {
            this.Cursor = Cursors.WaitCursor;

            if (radGridView_PO.Columns.Count > 0) radGridView_PO.Columns.Clear();
            if (radGridView_PO.Rows.Count > 0) radGridView_PO.Rows.Clear();

            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("RROWS", "ลำดับ", 60));
            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค้ด", 130));
            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 280));
            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PURCHUNIT", "หน่วย", 80));
            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTYORDERED", "จำนวนสั่ง", 100));
            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("PURCHPRICE", "ราคา/หน่วย", 130));
            radGridView_PO.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "จำนวนเงิน", 120));

            DataTable dtDetail = MNOR_Class.FindDataPurchLine(docno);// ConnectionClass.SelectSQL_MainAX(sqlDT);
            //radGridView_PO.DataSource = dtt;
            double count_Box = 0, sum_money = 0;
            for (int i = 0; i < dtDetail.Rows.Count; i++)
            {
                radGridView_PO.Rows.Add(dtDetail.Rows[i]["RROWS"].ToString(),
                    dtDetail.Rows[i]["BARCODE"].ToString(), dtDetail.Rows[i]["NAME"].ToString(),
                    dtDetail.Rows[i]["PURCHUNIT"].ToString(), Double.Parse(dtDetail.Rows[i]["QTYORDERED"].ToString()).ToString("#,##0.00"),
                     Double.Parse(dtDetail.Rows[i]["PURCHPRICE"].ToString()).ToString("#,##0.00"), Double.Parse(dtDetail.Rows[i]["LINEAMOUNT"].ToString()).ToString("#,##0.00"));
                count_Box += Double.Parse(dtDetail.Rows[i]["QTYORDERED"].ToString());
                sum_money += Double.Parse(dtDetail.Rows[i]["LINEAMOUNT"].ToString());
            }

            if (radDropDownList_SubGroup.SelectedValue.ToString() == "D034_01")
            {

                radGridView_PO.Rows.Add("", "", "", "รวมลัง", count_Box.ToString("#,##0.00"), "รวมจำนวนเงิน", ((sum_money * 100) / 107).ToString("#,##0.00"));
                radGridView_PO.Rows.Add("", "", "", "", "", "ภาษี 7.00 %", (((sum_money * 100) / 107) * 0.07).ToString("#,##0.00"));
                radGridView_PO.Rows.Add("", "", "", "", "", "รวมทั้งหมด", (((sum_money * 100) / 107) * 1.07).ToString("#,##0.00"));

            }
            else
            {
                radGridView_PO.Rows.Add("", "", "", "รวมลัง", count_Box.ToString("#,##0.00"), "รวมจำนวนเงิน", (sum_money).ToString("#,##0.00"));
                radGridView_PO.Rows.Add("", "", "", "", "", "ภาษี 7.00 %", (sum_money * 0.07).ToString("#,##0.00"));
                radGridView_PO.Rows.Add("", "", "", "", "", "รวมทั้งหมด", (sum_money * 1.07).ToString("#,##0.00"));

            }

            this.Cursor = Cursors.Default;
        }
        //print PO
        void PrintPO(string docno, string bchID, string bchName, string venderId, string venderName, string rmk)
        {
            SetPODetail(docno);
            this.radGridView_PO.PrintStyle = DatagridClass.SetGridForPrint(new GridPrintStyle(), false, true);

            RadPrintDocument document = new RadPrintDocument();
            string l1 = $@"ใบสั่งซื้อ {docno}";
            string l2 = $@"ผู้จำหน่าย {venderId} - {venderName}";
            string l3 = $@"ลงสินค้าที่ สาขา {bchID} - {bchName}";

            int rang = 70; if (rmk != "") { rang = 100; l3 += Environment.NewLine + Environment.NewLine + "หมายเหตุ : " + rmk; }

            string paperSize = "B"; if (radGridView_PO.Rows.Count <= 9) paperSize = "S";
            DatagridClass.SetPrintDocumentPO(document, l1 + Environment.NewLine + l2 + Environment.NewLine + l3,
                rang, false, "", paperSize, " SuperCheap");

            document.AssociatedObject = this.radGridView_PO;

            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document)
            {
                StartPosition = FormStartPosition.CenterScreen,
                WindowState = FormWindowState.Maximized
            };

            dialog.ShowDialog();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //print

        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (RadGridView_ShowHD.CurrentRow.Cells["PurchId"].Value.ToString() == "") { return; }
             
            switch (_pTypeReport)
            {
                case "0":
                    switch (e.Column.Name)
                    {
                        case "PRT":
                            SetPrint(RadGridView_ShowHD.CurrentRow.Index, "1");
                            //PrintReportPO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), docno);
                            break;
                        case "PO":
                            SetPrint(RadGridView_ShowHD.CurrentRow.Index, "0");
                            //PrintPO(docno, bchID, bchName, venderID, venderName, remark);
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }

        private void RadDropDownList_MainGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!(radDropDownList_MainGroup.SelectedValue is null))
            {
                radDropDownList_SubGroup.DataSource = ItemGroup_Class.GetItemGroup(radDropDownList_MainGroup.SelectedValue.ToString(), "01", "  AND ITEMGROUPSTATUS1 = '1'  ");
                radDropDownList_SubGroup.ValueMember = "ITEMGROUPID";
                radDropDownList_SubGroup.DisplayMember = "ITEMGROUPIDNAME";
            }

        }
        //Print PO All
        private void RadButtonElement_print_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                PrintPO(RadGridView_ShowHD.Rows[i].Cells["PurchId"].Value.ToString(),
                     RadGridView_ShowHD.Rows[i].Cells["INVENTLOCATIONID"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["DELIVERYNAME"].Value.ToString(),
                     RadGridView_ShowHD.Rows[i].Cells["OrderAccount"].Value.ToString(), RadGridView_ShowHD.CurrentRow.Cells["PURCHNAME"].Value.ToString(), "");
            }
        }

        void PrintReportPO(string D1, string D2, string Docno)
        {
            this.Cursor = Cursors.WaitCursor;
            //string sql = $@" SPCN_SumAllTransByPurchId '{D1}','{D2}','{Docno}','1' ";
            DataTable dt = MNOR_Class.FindReportSPCN_SumAllTransByPurchId(D1, D2, Docno);// ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess);

            if (radGridView_Report.Rows.Count > 0) radGridView_Report.Rows.Clear();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                double per1 = 1, per2 = 1;
                if (Double.Parse(dt.Rows[i]["TOTALPURCH"].ToString()) > 0) per1 = (Double.Parse(dt.Rows[i]["TOTALUSE_PERCENT"].ToString()) * 100);
                if (Double.Parse(dt.Rows[i]["TOTALPURCH"].ToString()) > 0) per2 = (Double.Parse(dt.Rows[i]["TOTALUSE_TFMN_PERCENT"].ToString()) * 100);

                radGridView_Report.Rows.Add(
                    dt.Rows[i]["PRODUCT"].ToString() + "-" + dt.Rows[i]["NAME"].ToString() + " [" + dt.Rows[i]["PURCHUNIT"].ToString() + "]",
                    dt.Rows[i]["PURCHUNIT"].ToString(), dt.Rows[i]["NAME"].ToString(),
                    Double.Parse(dt.Rows[i]["INVENTQTYPURCH_THIS"].ToString()) + Double.Parse(dt.Rows[i]["INVENTQTY_PURCH"].ToString()) + Double.Parse(dt.Rows[i]["INVENTQTY_PURCHCN"].ToString()),
                    Double.Parse(dt.Rows[i]["INVENTQTY_SALERT"].ToString()) + Double.Parse(dt.Rows[i]["INVENTQTY_SALEWS"].ToString()) + Double.Parse(dt.Rows[i]["INVENTQTY_SALEMN"].ToString()),
                    Double.Parse(dt.Rows[i]["INVENTQTY_ISSUE"].ToString()) + Double.Parse(dt.Rows[i]["INVENTQTY_ISSUECN"].ToString()),
                    Double.Parse(dt.Rows[i]["INVENTQTY_TRANS"].ToString()) + Double.Parse(dt.Rows[i]["INVENTQTY_TRANSCN"].ToString()),
                    Double.Parse(dt.Rows[i]["TOTALALL"].ToString()),
                    per1, per2
                    );
            }

            radGridView_Report.PrintStyle = DatagridClass.SetGridForPrint(new GridPrintStyle(), true, true);

            RadPrintDocument document = new RadPrintDocument();
            string l1 = $@"รายงานสรุปจำนวนซื้อ ขาย เบิก ใบสั่งโอนย้าย ตามใบสั่งซื้อ";
            string l2 = $@"{Docno}";
            string l3 = $@"จากวันที่ {D1} ถึง {D2}";

            string paperSize = "B"; if (dt.Rows.Count < 7) paperSize = "S";

            DatagridClass.SetPrintDocumentPO(document, l1 + Environment.NewLine +
                                                        l2 + Environment.NewLine + l3, 70, false,
                                                        "- ปริมาณที่แสดงเป็นปริมาณตามหน่วยเล็กสุด" + Environment.NewLine + "- คงเหลือ คิดจาก ซื้อ หักลบ ขาย เบิก", paperSize, " SuperCheap");

            document.AssociatedObject = this.radGridView_Report;
            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document)
            {
                StartPosition = FormStartPosition.CenterScreen,
                WindowState = FormWindowState.Maximized
            };
            this.Cursor = Cursors.Default;
            dialog.ShowDialog();


        }
        //พิมพ์สีแดง
        private void RadGridView_Report_PrintCellFormatting(object sender, PrintCellFormattingEventArgs e)
        {
            if (e.Row is GridViewDataRowInfo)
            {
                if ((e.Column.Name == "Per_1") || (e.Column.Name == "Per_2"))
                {
                    if ((Double)e.Row.Cells[e.Column.Name].Value < 50)
                    {
                        e.PrintCell.DrawFill = true;
                        e.PrintCell.ForeColor = Color.Red;
                    }
                }

            }
        }
        //พิมวงกลม
        private void RadGridView_Report_PrintCellPaint(object sender, PrintCellPaintEventArgs e)
        {
            if (e.Row is GridViewDataRowInfo)
            {
                if ((e.Column.Name == "Per_1") || (e.Column.Name == "Per_2"))
                {
                    int side = e.CellRect.Height - 6;
                    int x = e.CellRect.X + 3;
                    int y = e.CellRect.Y + 3;
                    Rectangle rect = new Rectangle(x, y, side, side);
                    Brush brush = Brushes.Black;

                    if ((Double)e.Row.Cells[e.Column.Name].Value < 50) brush = Brushes.Red;

                    e.Graphics.FillEllipse(brush, rect);
                }

            }
        }
        //Print Report All
        private void RadButtonElement_Report_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                PrintReportPO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                    radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                    RadGridView_ShowHD.Rows[i].Cells["PurchId"].Value.ToString());
            }
        }

        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (RadGridView_ShowHD.CurrentRow.Cells["PurchId"].Value.ToString() == "") { return; }


            switch (_pTypeReport)
            {
                case "0":
                    switch (e.Column.Name)
                    {
                        case "PurchId":
                            //PrintPO(docno, bchID, bchName, venderID, venderName, remark);
                            //PrintReportPO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), docno);
                            SetPrint(RadGridView_ShowHD.CurrentRow.Index, "2");
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }
        //staPrint 0 = printPO 1 = PrintReport 2 =  PrintPO+Report
        void SetPrint(int iRow, string staPrint)
        {
            string docno = RadGridView_ShowHD.Rows[iRow].Cells["PurchId"].Value.ToString();
            string bchID = RadGridView_ShowHD.Rows[iRow].Cells["INVENTLOCATIONID"].Value.ToString();
            string bchName = RadGridView_ShowHD.Rows[iRow].Cells["DELIVERYNAME"].Value.ToString();
            string venderID = RadGridView_ShowHD.Rows[iRow].Cells["OrderAccount"].Value.ToString();
            string venderName = RadGridView_ShowHD.Rows[iRow].Cells["PURCHNAME"].Value.ToString();
            string remark = RadGridView_ShowHD.Rows[iRow].Cells["IVZ_REMARKS"].Value.ToString();

            switch (staPrint)
            {
                case "0":
                    PrintPO(docno, bchID, bchName, venderID, venderName, remark);
                    break;
                case "1":
                    PrintReportPO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), docno);
                    break;
                case "2":
                    PrintPO(docno, bchID, bchName, venderID, venderName, remark);
                    PrintReportPO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), docno);
                    break;
                default:
                    break;
            }

        }
    }
}


