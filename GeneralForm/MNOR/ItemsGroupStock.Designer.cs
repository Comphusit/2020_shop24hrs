﻿namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    partial class ItemsGroupStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemsGroupStock));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton_GroupID = new System.Windows.Forms.RadioButton();
            this.radioButton_Item = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radioButton_AllBranch = new System.Windows.Forms.RadioButton();
            this.radioButton_BranchOut = new System.Windows.Forms.RadioButton();
            this.radioButton_BranchIn = new System.Windows.Forms.RadioButton();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Month = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Year = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Group = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_GroupItems = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel_Setting = new System.Windows.Forms.Panel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadDateTimePicker_Begin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_setStock = new Telerik.WinControls.UI.RadButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton_SetBranchALL = new System.Windows.Forms.RadioButton();
            this.radioButton_SetBranchOUT = new System.Windows.Forms.RadioButton();
            this.radioButton_SetBranchIN = new System.Windows.Forms.RadioButton();
            this.RadGridView_BarcodeOrGroup = new Telerik.WinControls.UI.RadGridView();
            this.office2010SilverTheme1 = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.RadButtonElement_Pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Month)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Year)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Group)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel_Setting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_setStock)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BarcodeOrGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BarcodeOrGroup.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(611, 591);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            //this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 628F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.radLabel_F2, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(617, 622);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 600);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(611, 19);
            this.radLabel_F2.TabIndex = 70;
            this.radLabel_F2.Text = resources.GetString("radLabel_F2.Text");
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.RadGridView_BarcodeOrGroup, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(632, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(194, 628);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.radDropDownList_Month);
            this.panel1.Controls.Add(this.radDropDownList_Year);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_Remark);
            this.panel1.Controls.Add(this.radLabel_Group);
            this.panel1.Controls.Add(this.radDropDownList_GroupItems);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadDropDownList_Dept);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 612);
            this.panel1.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton_GroupID);
            this.groupBox2.Controls.Add(this.radioButton_Item);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(218, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(114, 70);
            this.groupBox2.TabIndex = 79;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "เงื่อนไข";
            this.groupBox2.Visible = false;
            // 
            // radioButton_GroupID
            // 
            this.radioButton_GroupID.AutoSize = true;
            this.radioButton_GroupID.Location = new System.Drawing.Point(21, 40);
            this.radioButton_GroupID.Name = "radioButton_GroupID";
            this.radioButton_GroupID.Size = new System.Drawing.Size(85, 20);
            this.radioButton_GroupID.TabIndex = 3;
            this.radioButton_GroupID.Text = "กลุ่มสินค้า";
            this.radioButton_GroupID.UseVisualStyleBackColor = true;
            this.radioButton_GroupID.CheckedChanged += new System.EventHandler(this.RadioButton_GroupID_CheckedChanged);
            // 
            // radioButton_Item
            // 
            this.radioButton_Item.AutoSize = true;
            this.radioButton_Item.Checked = true;
            this.radioButton_Item.Location = new System.Drawing.Point(21, 18);
            this.radioButton_Item.Name = "radioButton_Item";
            this.radioButton_Item.Size = new System.Drawing.Size(59, 20);
            this.radioButton_Item.TabIndex = 2;
            this.radioButton_Item.TabStop = true;
            this.radioButton_Item.Text = "สินค้า";
            this.radioButton_Item.UseVisualStyleBackColor = true;
            this.radioButton_Item.CheckedChanged += new System.EventHandler(this.RadioButton_Item_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RadButton_pdt);
            this.groupBox1.Controls.Add(this.radioButton_AllBranch);
            this.groupBox1.Controls.Add(this.radioButton_BranchOut);
            this.groupBox1.Controls.Add(this.radioButton_BranchIn);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(218, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(113, 101);
            this.groupBox1.TabIndex = 78;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "สาขา";
            this.groupBox1.Visible = false;
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(115, 15);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 73;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radioButton_AllBranch
            // 
            this.radioButton_AllBranch.AutoSize = true;
            this.radioButton_AllBranch.Checked = true;
            this.radioButton_AllBranch.Location = new System.Drawing.Point(22, 66);
            this.radioButton_AllBranch.Name = "radioButton_AllBranch";
            this.radioButton_AllBranch.Size = new System.Drawing.Size(69, 20);
            this.radioButton_AllBranch.TabIndex = 2;
            this.radioButton_AllBranch.TabStop = true;
            this.radioButton_AllBranch.Text = "ทั้งหมด";
            this.radioButton_AllBranch.UseVisualStyleBackColor = true;
            // 
            // radioButton_BranchOut
            // 
            this.radioButton_BranchOut.AutoSize = true;
            this.radioButton_BranchOut.Location = new System.Drawing.Point(22, 44);
            this.radioButton_BranchOut.Name = "radioButton_BranchOut";
            this.radioButton_BranchOut.Size = new System.Drawing.Size(89, 20);
            this.radioButton_BranchOut.TabIndex = 1;
            this.radioButton_BranchOut.Text = "ต่างจังหวัด";
            this.radioButton_BranchOut.UseVisualStyleBackColor = true;
            this.radioButton_BranchOut.CheckedChanged += new System.EventHandler(this.RadioButton_BranchOut_CheckedChanged);
            // 
            // radioButton_BranchIn
            // 
            this.radioButton_BranchIn.AutoSize = true;
            this.radioButton_BranchIn.Location = new System.Drawing.Point(22, 22);
            this.radioButton_BranchIn.Name = "radioButton_BranchIn";
            this.radioButton_BranchIn.Size = new System.Drawing.Size(81, 20);
            this.radioButton_BranchIn.TabIndex = 0;
            this.radioButton_BranchIn.Text = "ในจังหวัด";
            this.radioButton_BranchIn.UseVisualStyleBackColor = true;
            this.radioButton_BranchIn.CheckedChanged += new System.EventHandler(this.RadioButton_BranchIn_CheckedChanged);
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(5, 200);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 31;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDropDownList_Month
            // 
            this.radDropDownList_Month.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Month.DropDownHeight = 124;
            this.radDropDownList_Month.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_Month.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Month.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDropDownList_Month.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Month.Location = new System.Drawing.Point(107, 167);
            this.radDropDownList_Month.Name = "radDropDownList_Month";
            this.radDropDownList_Month.Size = new System.Drawing.Size(74, 21);
            this.radDropDownList_Month.TabIndex = 77;
            this.radDropDownList_Month.Text = "01";
            this.radDropDownList_Month.Visible = false;
            // 
            // radDropDownList_Year
            // 
            this.radDropDownList_Year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Year.DropDownHeight = 124;
            this.radDropDownList_Year.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_Year.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Year.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDropDownList_Year.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Year.Location = new System.Drawing.Point(5, 167);
            this.radDropDownList_Year.Name = "radDropDownList_Year";
            this.radDropDownList_Year.Size = new System.Drawing.Size(77, 21);
            this.radDropDownList_Year.TabIndex = 76;
            this.radDropDownList_Year.Text = "2020";
            this.radDropDownList_Year.Visible = false;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(88, 169);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(13, 19);
            this.radLabel4.TabIndex = 74;
            this.radLabel4.Text = "-";
            this.radLabel4.Visible = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(6, 144);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(16, 19);
            this.radLabel3.TabIndex = 75;
            this.radLabel3.Text = "ปี";
            this.radLabel3.Visible = false;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(107, 144);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(38, 19);
            this.radLabel2.TabIndex = 73;
            this.radLabel2.Text = "เดือน";
            this.radLabel2.Visible = false;
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Remark.ForeColor = System.Drawing.Color.Red;
            this.radLabel_Remark.Location = new System.Drawing.Point(60, 53);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(120, 19);
            this.radLabel_Remark.TabIndex = 70;
            this.radLabel_Remark.Text = "เฉพาะจซ.ที่มีสินค้า";
            this.radLabel_Remark.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_Remark.Visible = false;
            // 
            // radLabel_Group
            // 
            this.radLabel_Group.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Group.Location = new System.Drawing.Point(5, 97);
            this.radLabel_Group.Name = "radLabel_Group";
            this.radLabel_Group.Size = new System.Drawing.Size(34, 19);
            this.radLabel_Group.TabIndex = 71;
            this.radLabel_Group.Text = "กลุ่ม";
            // 
            // radDropDownList_GroupItems
            // 
            this.radDropDownList_GroupItems.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_GroupItems.DropDownHeight = 124;
            this.radDropDownList_GroupItems.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_GroupItems.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_GroupItems.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_GroupItems.Location = new System.Drawing.Point(5, 118);
            this.radDropDownList_GroupItems.Name = "radDropDownList_GroupItems";
            this.radDropDownList_GroupItems.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_GroupItems.TabIndex = 70;
            this.radDropDownList_GroupItems.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_GroupItems_SelectedValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(5, 53);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(43, 19);
            this.radLabel1.TabIndex = 69;
            this.radLabel1.Text = "จัดซื้อ";
            // 
            // RadDropDownList_Dept
            // 
            this.RadDropDownList_Dept.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_Dept.DropDownHeight = 124;
            this.RadDropDownList_Dept.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.RadDropDownList_Dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadDropDownList_Dept.Location = new System.Drawing.Point(5, 72);
            this.RadDropDownList_Dept.Name = "RadDropDownList_Dept";
            this.RadDropDownList_Dept.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Dept.TabIndex = 68;
            this.RadDropDownList_Dept.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel_Setting);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 621);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(188, 1);
            this.panel2.TabIndex = 7;
            // 
            // panel_Setting
            // 
            this.panel_Setting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Setting.Controls.Add(this.radLabel7);
            this.panel_Setting.Controls.Add(this.RadDateTimePicker_Begin);
            this.panel_Setting.Controls.Add(this.radLabel6);
            this.panel_Setting.Controls.Add(this.RadButton_setStock);
            this.panel_Setting.Controls.Add(this.groupBox3);
            this.panel_Setting.Location = new System.Drawing.Point(0, 0);
            this.panel_Setting.Name = "panel_Setting";
            this.panel_Setting.Size = new System.Drawing.Size(188, 114);
            this.panel_Setting.TabIndex = 0;
            this.panel_Setting.Visible = false;
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(5, 24);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(33, 19);
            this.radLabel7.TabIndex = 81;
            this.radLabel7.Text = "วันที่";
            // 
            // RadDateTimePicker_Begin
            // 
            this.RadDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_Begin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_Begin.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_Begin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_Begin.Location = new System.Drawing.Point(5, 45);
            this.RadDateTimePicker_Begin.Name = "RadDateTimePicker_Begin";
            this.RadDateTimePicker_Begin.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_Begin.TabIndex = 83;
            this.RadDateTimePicker_Begin.TabStop = false;
            this.RadDateTimePicker_Begin.Text = "22/05/2020";
            this.RadDateTimePicker_Begin.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            //this.RadDateTimePicker_Begin.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Begin_ValueChanged);
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(5, 3);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(107, 19);
            this.radLabel6.TabIndex = 80;
            this.radLabel6.Text = "ตั้งค่าส่งรูปสต๊อก";
            // 
            // RadButton_setStock
            // 
            this.RadButton_setStock.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_setStock.Location = new System.Drawing.Point(4, 72);
            this.RadButton_setStock.Name = "RadButton_setStock";
            this.RadButton_setStock.Size = new System.Drawing.Size(175, 32);
            this.RadButton_setStock.TabIndex = 32;
            this.RadButton_setStock.Text = "ตั้งค่า";
            this.RadButton_setStock.ThemeName = "Fluent";
            this.RadButton_setStock.Click += new System.EventHandler(this.RadButton_setStock_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_setStock.GetChildAt(0))).Text = "ตั้งค่า";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_setStock.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_setStock.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_setStock.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_setStock.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_setStock.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_setStock.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_setStock.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton_SetBranchALL);
            this.groupBox3.Controls.Add(this.radioButton_SetBranchOUT);
            this.groupBox3.Controls.Add(this.radioButton_SetBranchIN);
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(185, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(147, 108);
            this.groupBox3.TabIndex = 79;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "สาขา";
            this.groupBox3.Visible = false;
            // 
            // radioButton_SetBranchALL
            // 
            this.radioButton_SetBranchALL.AutoSize = true;
            this.radioButton_SetBranchALL.Checked = true;
            this.radioButton_SetBranchALL.Location = new System.Drawing.Point(22, 66);
            this.radioButton_SetBranchALL.Name = "radioButton_SetBranchALL";
            this.radioButton_SetBranchALL.Size = new System.Drawing.Size(69, 20);
            this.radioButton_SetBranchALL.TabIndex = 2;
            this.radioButton_SetBranchALL.TabStop = true;
            this.radioButton_SetBranchALL.Text = "ทั้งหมด";
            this.radioButton_SetBranchALL.UseVisualStyleBackColor = true;
            // 
            // radioButton_SetBranchOUT
            // 
            this.radioButton_SetBranchOUT.AutoSize = true;
            this.radioButton_SetBranchOUT.Location = new System.Drawing.Point(22, 44);
            this.radioButton_SetBranchOUT.Name = "radioButton_SetBranchOUT";
            this.radioButton_SetBranchOUT.Size = new System.Drawing.Size(89, 20);
            this.radioButton_SetBranchOUT.TabIndex = 1;
            this.radioButton_SetBranchOUT.Text = "ต่างจังหวัด";
            this.radioButton_SetBranchOUT.UseVisualStyleBackColor = true;
            //            this.radioButton_SetBranchOUT.CheckedChanged += new System.EventHandler(this.RadioButton_SetBranchOUT_CheckedChanged);
            // 
            // radioButton_SetBranchIN
            // 
            this.radioButton_SetBranchIN.AutoSize = true;
            this.radioButton_SetBranchIN.Location = new System.Drawing.Point(22, 22);
            this.radioButton_SetBranchIN.Name = "radioButton_SetBranchIN";
            this.radioButton_SetBranchIN.Size = new System.Drawing.Size(81, 20);
            this.radioButton_SetBranchIN.TabIndex = 0;
            this.radioButton_SetBranchIN.Text = "ในจังหวัด";
            this.radioButton_SetBranchIN.UseVisualStyleBackColor = true;
            //this.radioButton_SetBranchIN.CheckedChanged += new System.EventHandler(this.RadioButton_SetBranchIN_CheckedChanged);
            // 
            // RadGridView_BarcodeOrGroup
            // 
            this.RadGridView_BarcodeOrGroup.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_BarcodeOrGroup.Location = new System.Drawing.Point(3, 626);
            // 
            // 
            // 
            this.RadGridView_BarcodeOrGroup.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.RadGridView_BarcodeOrGroup.Name = "RadGridView_BarcodeOrGroup";
            // 
            // 
            // 
            this.RadGridView_BarcodeOrGroup.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_BarcodeOrGroup.Size = new System.Drawing.Size(188, 1);
            this.RadGridView_BarcodeOrGroup.TabIndex = 8;
            this.RadGridView_BarcodeOrGroup.Visible = false;
            this.RadGridView_BarcodeOrGroup.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Barcode_ViewCellFormatting);
            this.RadGridView_BarcodeOrGroup.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_BarcodeOrGroup_CellClick);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.RadButtonElement_Pdf,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(186, 34);
            this.radStatusStrip1.TabIndex = 89;
            // 
            // RadButtonElement_Pdf
            // 
            this.RadButtonElement_Pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_Pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_Pdf.Name = "RadButtonElement_Pdf";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_Pdf, false);
            this.RadButtonElement_Pdf.Text = "radButtonElement1";
            this.RadButtonElement_Pdf.UseCompatibleTextRendering = false;
            this.RadButtonElement_Pdf.Click += new System.EventHandler(this.RadButtonElement_Pdf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // ItemsGroupStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ItemsGroupStock";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "วันที่ส่งสต๊อก";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ItemsGroupStock_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Month)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Year)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Group)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel_Setting.ResumeLayout(false);
            this.panel_Setting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_setStock)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BarcodeOrGroup.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_BarcodeOrGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel_Group;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_GroupItems;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        public Telerik.WinControls.UI.RadDropDownList RadDropDownList_Dept;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_BarcodeOrGroup;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Year;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Month;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton_GroupID;
        private System.Windows.Forms.RadioButton radioButton_Item;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_AllBranch;
        private System.Windows.Forms.RadioButton radioButton_BranchOut;
        private System.Windows.Forms.RadioButton radioButton_BranchIn;
        private System.Windows.Forms.Panel panel_Setting;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_Begin;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        protected Telerik.WinControls.UI.RadButton RadButton_setStock;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton_SetBranchALL;
        private System.Windows.Forms.RadioButton radioButton_SetBranchOUT;
        private System.Windows.Forms.RadioButton radioButton_SetBranchIN;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_Pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
    }
}
