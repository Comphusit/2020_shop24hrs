﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using PC_Shop24Hrs.GeneralForm.MNOR;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class MNOR_ORDER : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDGV = new DataTable();
        DataTable dtStockGroup;

        string pApv;

        string pStaOrderAgain;
        public MNOR_ORDER()
        {
            InitializeComponent();
        }
        //Load Main
        private void MNOR_ORDER_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpMain);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpSub);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now.AddDays(1));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "สั่ง"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY_UNITID", "อัตราส่วน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BlockSend", "BlockSend"));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("STOCK", "สต๊อกคงเหลือ", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALE", "ยอดขาย [หน่วยย่อย]", 150));

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "BlockSend = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_Show.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["STA"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["QTY"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["UNITID"].ConditionalFormattingObjectList.Add(obj2);

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000") dtBch = BranchClass.GetBranchAll("'1'", "'1'");
            else dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true;
            RadButtonElement_Find.ShowBorder = true;
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            radButtonElement_Excel.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            radDateTimePicker_Begin.MinDate = DateTime.Now.AddDays(-1);
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(1);
            Set_GroupMain();
            ClearData();
        }
        //GroupMain
        void Set_GroupMain()
        {
            RadDropDownList_GrpMain.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("47", "", "", "1");
            RadDropDownList_GrpMain.ValueMember = "SHOW_ID";//NUM
            RadDropDownList_GrpMain.DisplayMember = "SHOW_NAME";//DESCRIPTION
            RadDropDownList_GrpMain.SelectedIndex = 0;
        }
        //แสดงกลุ่มย่อย
        void Set_GroupSub()
        {
            string grpMain;
            try
            {
                grpMain = RadDropDownList_GrpMain.SelectedValue.ToString();
            }
            catch (Exception)
            {

                grpMain = "";
            }
            dtStockGroup = ItemGroup_Class.GetItemGroup(grpMain, "01", "  AND ITEMGROUPSTATUS1 = '1'  ");
            RadDropDownList_GrpSub.DataSource = dtStockGroup;
            RadDropDownList_GrpSub.ValueMember = "ITEMGROUPID";
            RadDropDownList_GrpSub.DisplayMember = "ITEMGROUPIDNAME";
            RadDropDownList_GrpSub.SelectedIndex = 0;
        }
        //Set DGV
        void Set_DGV()
        {
            if (RadDropDownList_GrpSub.Items.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("แผนกที่เลือกนั้น ไม่มีกลุ่มสินค้าที่สามารถสั่งได้" + Environment.NewLine + "ติดต่อจัดซื้อเจ้าของแผนกเพื่อแก้ไขหรือสอบถามข้อมูลเพิ่มเติม");
                return;
            }
            this.Cursor = Cursors.WaitCursor;

            dtDGV = ItembarcodeClass.ItemGroup_GetItemByGroupForOrder(RadDropDownList_GrpSub.SelectedValue.ToString());


            if (dtDGV.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("สินค้าในกลุ่มที่เลือก");
                this.Cursor = Cursors.Default;
                return;
            }

            RadGridView_Show.Columns["STOCK"].IsVisible = false;
            DataRow[] drStock = dtStockGroup.Select($@" STA_SHOWSTOCK = '1' AND ITEMGROUPID = '{RadDropDownList_GrpSub.SelectedValue}' ");
            if (drStock.Length > 0)
            {
                RadGridView_Show.Columns["STOCK"].IsVisible = true;
                for (int iStock = 0; iStock < dtDGV.Rows.Count; iStock++)
                {
                    //RadGridView_Show.Rows[iStock].Cells["STOCK"].Value = ItembarcodeClass.FindStock_ByBarcode(RadGridView_Show.Rows[iStock].Cells["ITEMBARCODE"].Value.ToString(),
                    //    RadDropDownList_Branch.SelectedValue.ToString());
                    dtDGV.Rows[iStock]["STOCK"] = ItembarcodeClass.FindStock_ByBarcode(dtDGV.Rows[iStock]["ITEMBARCODE"].ToString(),
                        RadDropDownList_Branch.SelectedValue.ToString());
                }
            }


            RadGridView_Show.Columns["SALE"].IsVisible = false;
            DataRow[] drSale = dtStockGroup.Select($@" STA_SHOWSALE = '1' AND ITEMGROUPID = '{RadDropDownList_GrpSub.SelectedValue}'   ");
            if (drSale.Length > 0)
            {
                RadGridView_Show.Columns["SALE"].IsVisible = true;
                int dd = 0;
                if (radTextBox_Sale.Text != "") dd = int.Parse(radTextBox_Sale.Text);
                DataTable dtSale = PosSaleClass.MNOR_FindSale(RadDropDownList_GrpSub.SelectedValue.ToString(), dd); //FindSale(RadDropDownList_GrpSub.SelectedValue.ToString());//
                for (int iSale = 0; iSale < dtDGV.Rows.Count; iSale++)
                {
                    double qtySale = 0;
                    if (dtSale.Rows.Count > 0)
                    {
                        DataRow[] checkdrSale = dtSale.Select($@" ITEMID = '{dtDGV.Rows[iSale]["ITEMID"]}' AND INVENTDIMID = '{dtDGV.Rows[iSale]["INVENTDIMID"]}' AND POSGROUP = '{RadDropDownList_Branch.SelectedValue}' ");
                        if (checkdrSale.Length > 0) qtySale = double.Parse(checkdrSale[0]["INVENQTY"].ToString());
                    }
                    dtDGV.Rows[iSale]["SALE"] = qtySale;
                }
            }


            if (RadDropDownList_GrpMain.SelectedValue.ToString() == "D034")
            {
                for (int i = 0; i < dtDGV.Rows.Count - 1; i++)
                {
                    //RadGridView_Show.Rows[i].Cells["BlockSend"].Value = CheckBlockSend(RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString());
                    dtDGV.Rows[i]["BlockSend"] = CheckBlockSend(dtDGV.Rows[i]["ITEMBARCODE"].ToString());
                }
            }
            dtDGV.AcceptChanges();
            RadGridView_Show.DataSource = dtDGV;

            RadDropDownList_GrpMain.Enabled = false;
            RadDropDownList_GrpSub.Enabled = false;
            RadButton_Search.Enabled = false;
            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก"; radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red(); pApv = "0";
            RadButton_Save.Enabled = true; radDateTimePicker_Begin.Enabled = false;
            RadDropDownList_Branch.Enabled = false;
            RadGridView_Show.ReadOnly = false;
            radTextBox_Sale.Enabled = false;
            CheckOrder(RadDropDownList_GrpSub.SelectedValue.ToString());
            this.Cursor = Cursors.Default;
        }



        //เช็คว่าสั่งไปหรือยัง ในวันที่ระบุ
        void CheckOrder(string grpSub)
        {
            string sql = string.Format(@"
            SELECT	CONVERT(VARCHAR,DATEINS,23) as MNORDate,CONVERT(VARCHAR,DATEINS,24) AS MNORTime   
            FROM	SHOP_MNOR_HD WITH (NOLOCK)  
            WHERE	DATE_RECIVE = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"'  
                    AND GROUPID = '" + grpSub + @"' 
                    AND STA_DOC = '1' 
		            AND BRANCH_ID = '" + SystemClass.SystemBranchID + @"'
            ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                radLabel_Order.Text = dt.Rows[0]["MNORDate"].ToString() + " " + dt.Rows[0]["MNORTime"].ToString();
                radLabel_Order.ForeColor = Color.Red;
                pStaOrderAgain = "1";
            }
            else
            {
                radLabel_Order.Text = "ยังไม่ได้สั่ง";
                radLabel_Order.ForeColor = Color.Blue;
                pStaOrderAgain = "0";
            }
        }
        //Clear
        void ClearData()
        {

            if (dtDGV.Rows.Count > 0) { dtDGV.Rows.Clear(); dtDGV.AcceptChanges(); }

            RadDropDownList_GrpMain.SelectedIndex = 0;
            Set_GroupSub();

            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = "";
            radLabel_Order.Text = "";
            pApv = "0"; pStaOrderAgain = "0";
            RadDropDownList_GrpMain.Enabled = true; RadDropDownList_GrpSub.Enabled = true; RadButton_Search.Enabled = true;

            radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = Color.Transparent;

            radDateTimePicker_Begin.Enabled = true; RadDropDownList_Branch.Enabled = true;
            RadButton_Cancel.Enabled = false;
            RadButton_Save.Enabled = false;

            RadButton_Search.Focus();
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            Set_DGV();
        }

        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Select Group Main
        private void RadDropDownList_GrpMain_SelectedValueChanged(object sender, EventArgs e)
        {
            Set_GroupSub();
        }
        //Set choose Data
        void SetDataSTA()
        {
            DataRow[] drSTA = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
            string pSta = "0"; double pQty = 0;

            if (RadGridView_Show.CurrentRow.Cells["BlockSend"].Value.ToString() == "1") return;

            if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
            {
                //เชคสต็อกเฉพาะของห้องเหล้า
                if (RadDropDownList_GrpMain.SelectedValue.ToString() == "D034")
                {
                    string msg = (CheckStockForSale(RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString())); ;
                    if (msg != "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning(msg);
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                    }

                }

                InputData frmSTA = new InputData("0",
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                        "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                { pInputData = "1" };
                if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                {
                    pSta = "1";
                    pQty = Convert.ToDouble(frmSTA.pInputData);
                }
            }
            else
            {
                pSta = "0";
                pQty = 0;
            }

            if (radLabel_Docno.Text != "")
            {
                string sqlUp = string.Format(@" UPDATE SHOP_MNOR_DT SET STA_SEND = '" + pSta + @"',QTYORDER = '" + pQty + @"' 
                            WHERE DOCNO = '" + radLabel_Docno.Text + @"' AND ITEMBARCODE = '" + dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["ITEMBARCODE"] + @"' ");
                if (ConnectionClass.ExecuteSQL_Main(sqlUp) != "") { MsgBoxClass.MsgBoxShow_SaveStatus("จำนวนสั่ง"); return; }
            }

            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
            dtDGV.AcceptChanges();
        }
        //choose
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) { return; }
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["BlockSend"].Value.ToString() == "1")
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                    }

                    if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
                    {
                        return;
                    }

                    //เช็คสต็อกเฉพาะของห้องเหล้า
                    if (RadDropDownList_GrpMain.SelectedValue.ToString() == "D034")
                    {
                        string msg = (CheckStockForSale(RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()));
                        if (msg != "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning(msg);
                            RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                        }

                    }

                    DataRow[] drQTY = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                    InputData frmQTY = new InputData("0",
                                              RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                              "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() };
                    if (frmQTY.ShowDialog(this) == DialogResult.Yes)
                    {
                        string pSta; double pQty = Convert.ToDouble(frmQTY.pInputData);
                        if (Convert.ToDouble(frmQTY.pInputData) == 0)
                        { pSta = "0"; }
                        else { pSta = "1"; }
                        if (radLabel_Docno.Text != "")
                        {
                            string sqlUp = string.Format(@" UPDATE SHOP_MNOR_DT SET STA_SEND = '" + pSta + @"',QTYORDER = '" + pQty + @"' 
                            WHERE DOCNO = '" + radLabel_Docno.Text + @"' AND ITEMBARCODE = '" + dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["ITEMBARCODE"] + @"' ");
                            if (ConnectionClass.ExecuteSQL_Main(sqlUp) != "") { MsgBoxClass.MsgBoxShow_SaveStatus("จำนวนสั่ง"); return; }
                        }
                        dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["STA"] = pSta;
                        dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["QTY"] = pQty.ToString("#,##0.00");
                        dtDGV.AcceptChanges();
                    }

                    break;
                default:
                    return;
            }
        }
        //choose
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) return;
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                default:
                    return;
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            int iCountRow = 0;
            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                if (Convert.ToDouble(dtDGV.Rows[i]["QTY"]) > 0) iCountRow += 1;
            }
            if (iCountRow == 0)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ไม่มีข้อมูลการสั่งสินค้า");
                return;
            }

            if (pStaOrderAgain == "1") if (MsgBoxClass.MsgBoxShow_ConfirmInsert("กลุ่มสินค้านี้ มีการสั่งไปก่อนหน้านี้แล้ว ยืนยันการสั่งเพิ่ม") == DialogResult.No) return;
                else if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการสั่งสินค้า ไม่สามารถแก้ไขรายการได้") == DialogResult.No) return;


            string grpSub;
            try
            {
                grpSub = RadDropDownList_GrpSub.SelectedValue.ToString();
            }
            catch (Exception)
            {
                grpSub = "";
            }

            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNOR", "-", "MNOR", "1");
            string bchID = RadDropDownList_Branch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(RadDropDownList_Branch.SelectedValue.ToString());

            ArrayList sqlIn = new ArrayList
            {
                 String.Format($@"
                    INSERT INTO SHOP_MNOR_HD ( 
                    [DOCNO],[DPTID],[GROUPID],[BRANCH_ID],[BRANCH_NAME],[WHOIDINS],[WHONAMEINS],[DATE_RECIVE],[STA_PRCDOC],[WHOIDUPD],[WHONAMEUPD]) 
                    VALUES ('" + maxDocno + @"','" + RadDropDownList_GrpMain.SelectedValue.ToString() + @"',
                    '" + grpSub + @"', '" + bchID + @"', '" + bchName + @"',
                    '" + SystemClass.SystemUserID + @"', '" + SystemClass.SystemUserName + @"','" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"','1',
                    '" + SystemClass.SystemUserID + @"', '" + SystemClass.SystemUserName + @"') ")
            };

            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                sqlIn.Add(String.Format(@"
                    INSERT INTO SHOP_MNOR_DT ( 
                    [DOCNO],[LINENUM],[ITEMBARCODE],[SPC_ITEMNAME],[STA_SEND],[QTYORDER],[UNITID],[ITEMID],[INVENTDIMID],[FACTOR],[WHOIDINS],[WHONAMEINS],[QTYSTOCK],[QTYSALE] ) 
                    VALUES ('" + maxDocno + @"','" + (i + 1) + @"',
                    '" + dtDGV.Rows[i]["ITEMBARCODE"].ToString() + @"','" + dtDGV.Rows[i]["SPC_ITEMNAME"].ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                    '" + dtDGV.Rows[i]["STA"].ToString() + @"','" + dtDGV.Rows[i]["QTY"].ToString() + @"',
                    '" + dtDGV.Rows[i]["UNITID"].ToString() + @"',
                    '" + dtDGV.Rows[i]["ITEMID"].ToString() + @"',
                    '" + dtDGV.Rows[i]["INVENTDIMID"].ToString() + @"',
                    '" + dtDGV.Rows[i]["QTY_UNITID"].ToString() + @"',
                    '" + SystemClass.SystemUserID + @"',
                    '" + SystemClass.SystemUserName + @"',
                    '" + dtDGV.Rows[i]["STOCK"].ToString() + @"',
                    '" + dtDGV.Rows[i]["SALE"].ToString() + @"'
                    ) "));
            }
            String T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                RadButton_Cancel.Enabled = true;
                RadButton_Save.Enabled = false;
                RadGridView_Show.ReadOnly = true;
            }

        }

        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"SELECT STA_APVDOC AS STAAPV FROM   SHOP_MNOR_HD WHERE DOCNO = '" + radLabel_Docno.Text + @"'  ");
            DataTable dtSelect = ConnectionClass.SelectSQL_Main(sql);
            if (dtSelect.Rows[0]["STAAPV"].ToString() == "1")
            {
                MessageBox.Show("บิลเลขที่ " + radLabel_Docno.Text + " ได้ยืนยันการรับไปแล้วจากแผนกจัดซื้อแล้ว" + Environment.NewLine + "ถ้าไม่ต้องการสินค้าในบิลนี้ ให้ติดต่อแผนกจัดซื้อโดยตรง.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยกเลิก") == DialogResult.No) { return; }

            string sqlCancle = string.Format(@"UPDATE SHOP_MNOR_HD  SET STA_DOC = '3',
                            DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                            WHOIDUPD = '" + SystemClass.SystemUserID + @"',WHONAMEUPD = '" + SystemClass.SystemUserName + @"' 
                            WHERE DOCNO = '" + radLabel_Docno.Text + @"'  ");
            String T = ConnectionClass.ExecuteSQL_Main(sqlCancle);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยกเลิก");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                // RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }
        }

        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNOR_FindData frm = new MNOR_FindData("1");
            if (frm.ShowDialog(this) == DialogResult.Yes) SetDGV_Load(frm.pDocno);
        }
        void SetDGV_Load(string pDocno)
        {
            string sqlSelect = string.Format(@"
                SELECT	SHOP_MNOR_HD.DOCNO,
		                SHOP_MNOR_HD.DPTID AS GROUPMAIN,SHOP_MNOR_HD.GROUPID AS GROUPSUB,BRANCH_ID,
					    BRANCH_NAME,STA_DOC AS STADOC,STA_PRCDOC AS STAPRCDOC,STA_APVDOC AS STAAPV,
		                ITEMBARCODE,SPC_ITEMNAME,STA_SEND AS STA,QTYORDER AS QTY,UNITID,SHOP_MNOR_DT.REMARK AS REMARK,
                        QTYSTOCK AS STOCK,QTYSALE AS SALE  
                FROM	SHOP_MNOR_HD WITH (NOLOCK) 
			            INNER JOIN SHOP_MNOR_DT WITH (NOLOCK) ON SHOP_MNOR_HD.DocNo = SHOP_MNOR_DT.DocNo
                WHERE	SHOP_MNOR_HD.DocNo = '" + pDocno + @"' AND STA_SEND = '1' 
                ORDER BY SHOP_MNOR_DT.[LINENUM]
            ");

            dtDGV = ConnectionClass.SelectSQL_Main(sqlSelect);

            RadGridView_Show.DataSource = dtDGV;
            dtDGV.AcceptChanges();

            //Set_GroupMain(); Set_GroupSub();
            RadDropDownList_GrpMain.SelectedValue = dtDGV.Rows[0]["GROUPMAIN"].ToString();
            RadDropDownList_GrpSub.SelectedValue = dtDGV.Rows[0]["GROUPSUB"].ToString();


            DataRow[] drStock = dtStockGroup.Select($@" STA_SHOWSTOCK = '1' AND ITEMGROUPID = '{RadDropDownList_GrpSub.SelectedValue}' ");
            if (drStock.Length > 0)
            {
                RadGridView_Show.Columns["STOCK"].IsVisible = true;
            }
            else RadGridView_Show.Columns["STOCK"].IsVisible = false;

            DataRow[] drSale = dtStockGroup.Select($@" STA_SHOWSALE = '1' AND ITEMGROUPID = '{RadDropDownList_GrpSub.SelectedValue}'   ");
            if (drSale.Length > 0)
            {
                RadGridView_Show.Columns["SALE"].IsVisible = true;
            }
            else RadGridView_Show.Columns["SALE"].IsVisible = false;


            radLabel_Docno.Text = dtDGV.Rows[0]["DOCNO"].ToString();

            RadDropDownList_GrpMain.Enabled = false; RadDropDownList_GrpSub.Enabled = false; RadButton_Search.Enabled = false;
            // check อนุมัติ
            if (dtDGV.Rows[0]["STAAPV"].ToString() == "1")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel(); pApv = "1";
                //RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = false;
                RadButton_Save.Enabled = false;
                radDateTimePicker_Begin.Enabled = false;
                RadGridView_Show.ReadOnly = true;
                radTextBox_Sale.Enabled = false;
            }
            else
            {
                switch (dtDGV.Rows[0]["STADOC"].ToString())
                {
                    case "1":
                        if (dtDGV.Rows[0]["STAPRCDOC"].ToString() == "1")
                        {
                            radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black;
                            radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                            //RadButton_Apv.Enabled = false;
                            RadButton_Cancel.Enabled = true;
                            RadButton_Save.Enabled = false;
                            radDateTimePicker_Begin.Enabled = false;
                            RadGridView_Show.ReadOnly = true;
                            radTextBox_Sale.Enabled = false;
                        }
                        else
                        {
                            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black;
                            radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                            //RadButton_Apv.Enabled = true;
                            RadButton_Cancel.Enabled = true;
                            RadButton_Save.Enabled = false;
                            radDateTimePicker_Begin.Enabled = false;
                            RadGridView_Show.ReadOnly = true;
                            radTextBox_Sale.Enabled = false;
                        }
                        break;
                    case "3":
                        radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black;
                        radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                        //RadButton_Apv.Enabled = false;
                        RadButton_Cancel.Enabled = false;
                        RadButton_Save.Enabled = false;
                        radDateTimePicker_Begin.Enabled = false;
                        RadGridView_Show.ReadOnly = true;
                        radTextBox_Sale.Enabled = false;
                        break;
                    default:
                        break;
                }
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายการสินค้าเบิก " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Edit Qty
        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["BlockSend"].Value.ToString() == "1")
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                    }

                    //เช็คสต็อก เฉพาะของห้องเหล้า
                    if (RadDropDownList_GrpMain.SelectedValue.ToString() == "D034")
                    {
                        string msg = (CheckStockForSale(RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()));
                        if (msg != "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning(msg);
                            RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                        }
                    }

                    try
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString()).ToString("N2");
                        if (Convert.ToDouble(RadGridView_Show.CurrentCell.Value) > 0)
                        {
                            RadGridView_Show.CurrentRow.Cells["STA"].Value = "1";
                        }
                        else
                        {
                            RadGridView_Show.CurrentRow.Cells["STA"].Value = "0";
                        }

                    }
                    catch (Exception)
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00";
                        RadGridView_Show.CurrentRow.Cells["STA"].Value = "0";
                        return;
                    }
                    break;
                case "STA":
                    SetDataSTA();
                    break;
                default:
                    break;
            }
        }
        //Open
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (pApv != "0") return;

            if ((e.Column.Name != "QTY"))
            {
                if (RadGridView_Show.MasterView.TableFilteringRow.Cells[0].IsCurrent == true) return;

                if (RadGridView_Show.MasterView.TableFilteringRow.Cells[1].IsCurrent == true) return;

                e.Cancel = true;
            }

            //this.RadGridView_Show.MasterView.TableFilteringRow.Cells[1].BeginEdit();
        }

        //Check สินค้าหยุดส่ง
        string CheckBlockSend(string barcode)
        {
            DataTable dtItem = ConnectionClass.SelectSQL_Main(" POClass_GetDetailItembarcode_ForPO '" + barcode + @"','" + RadDropDownList_Branch.SelectedValue.ToString() + @"' ");

            DataRow[] dr = dtItem.Select($@" ITEMBARCODE = '{barcode}' ");
            if (dr.Length > 0) return dr[0]["STA_NOTOrder"].ToString(); else return "0";
        }

        string CheckStockForSale(string barcode)
        {
            this.Cursor = Cursors.WaitCursor;
            string txt = "";
            double itemStock = ItembarcodeClass.FindStock_ByBarcode(barcode, RadDropDownList_Branch.SelectedValue.ToString());
            if (itemStock > 0)
            {
                double itemSale90 = 0;
                DataTable dtSale90 = PosSaleClass.GetSaleSumInventQty_ByItemInvent(barcode, RadDropDownList_Branch.SelectedValue.ToString(), 90);

                if (dtSale90.Rows.Count > 0)
                {
                    itemSale90 = Convert.ToDouble(dtSale90.Rows[0]["QTY"].ToString());
                }
                if ((itemStock - itemSale90) > 0)
                {
                    txt = "สินค้ามีสต็อก  >> " + itemStock.ToString("#,#0.00") + Environment.NewLine +
                        "ยอดขายเฉลี่ยวันละ " + (itemSale90 / 90).ToString("#,#0.00") + Environment.NewLine +
                        "เพียงพอที่จะขายมากกว่า 3 เดือน" + Environment.NewLine + "ไม่อนุญาติให้สั่งสินค้าเพิ่มทุกกรณี";
                }

            }
            this.Cursor = Cursors.Default;
            return txt;
        }
        //SaleDay
        private void RadDropDownList_GrpSub_SelectedValueChanged(object sender, EventArgs e)
        {
            radLabel_Sale.Enabled = false;
            radTextBox_Sale.Enabled = false; radTextBox_Sale.Text = "";

            string grp;
            try
            {
                grp = RadDropDownList_GrpSub.SelectedValue.ToString();
            }
            catch (Exception)
            {
                return;
            }

            DataRow[] drSale = dtStockGroup.Select($@" STA_SHOWSALE = '1' AND ITEMGROUPID = '{grp}'   ");
            if (drSale.Length > 0)
            {
                radLabel_Sale.Enabled = true;
                radTextBox_Sale.Enabled = true; radTextBox_Sale.Text = "0"; radTextBox_Sale.Focus();
            }
        }
        //Lock Number
        private void RadTextBox_Sale_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
