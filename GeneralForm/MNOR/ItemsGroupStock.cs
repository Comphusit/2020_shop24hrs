﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class ItemsGroupStock : Telerik.WinControls.UI.RadForm
    {
        private string TypeEdit, TypeCondition = "Barcode", SqlConditions;
        readonly string _TypePageID, _TypePageName;
        private int IndexRow = -1;
        //private string IndexRowBarcode = string.Empty, IndexRowUnitID = string.Empty;
        private string IndexRowGroupId = string.Empty, IndexRowGroupName = string.Empty;
        //private string GroupItems;
        //_TypePage ประเภทของกลุ่มสินค้า ส่งมา 2 ตัวเช่น 01
        //_TypePageName ชื่อกลุ่มสินค้า เช่น กลุ่มสั่งสินค้า
        //ตั้งค่าคู่กับหน้า  ItemsGroup
        public ItemsGroupStock(string TypePageId, string TypePageName)
        {
            InitializeComponent();
            _TypePageID = TypePageId;
            _TypePageName = TypePageName;

        }
        private void ItemsGroupStock_Load(object sender, EventArgs e)
        {
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupItems);

            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Year);
            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Month);

            DatagridClass.SetDefaultRadGridView(RadGridView_BarcodeOrGroup);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Begin, DateTime.Now, DateTime.Now.AddDays(90));
            RadButton_setStock.ButtonElement.ShowBorder = true; RadButton_Search.ButtonElement.ShowBorder = true;


            TypeEdit = "Default";
            ClearData(TypeEdit);
            ClearData(TypeCondition);

            SqlConditions = " AND ITEMGROUPSTATUS2 = '1' ";
            SetDropdownlistDept(MRTClass.GetDept(SystemClass.SystemDptID));
        }

        #region FUNCTION

        //ตั้งค่าเริ่มต้น Grid
        void SetGridViewGroup()
        {
            RadGridView_Show.DataSource = null;
            if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();
            RadGridView_BarcodeOrGroup.DataSource = null;
            if (RadGridView_BarcodeOrGroup.RowCount > 0) RadGridView_BarcodeOrGroup.Rows.Clear();
            if (RadGridView_BarcodeOrGroup.ColumnCount > 0) RadGridView_BarcodeOrGroup.Columns.Clear();

            RadGridView_BarcodeOrGroup.Visible = true;
            RadGridView_BarcodeOrGroup.EnableFiltering = false;
            RadGridView_BarcodeOrGroup.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("CheckGroup", "", 80));
            RadGridView_BarcodeOrGroup.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPID", "กลุ่ม", 120));
            RadGridView_BarcodeOrGroup.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPNAME", "ชื่อกลุ่ม", 180));
            RadGridView_BarcodeOrGroup.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPDESC", "คำอธิบาย", 100));

            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Branch", "ชื่อสาขา", 150));

            RadGridView_Show.Columns["BranchID"].IsPinned = true;
            RadGridView_Show.Columns["Branch"].IsPinned = true;
        }

        void SetGridViewBarcode()
        {
            RadGridView_Show.DataSource = null;
            if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();
            RadGridView_BarcodeOrGroup.DataSource = null;
            if (RadGridView_BarcodeOrGroup.RowCount > 0) RadGridView_BarcodeOrGroup.Rows.Clear();
            if (RadGridView_BarcodeOrGroup.ColumnCount > 0) RadGridView_BarcodeOrGroup.Columns.Clear();

            RadGridView_BarcodeOrGroup.Visible = false;

            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 180));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMUNIT", "หน่วย", 100));

            RadGridView_Show.Columns["ITEMBARCODE"].IsPinned = true;
            RadGridView_Show.Columns["SPC_ITEMNAME"].IsPinned = true;
            RadGridView_Show.Columns["ITEMUNIT"].IsPinned = true;
        }

        //สร้าง ตัวเลือกแผนก
        void SetDropdownlistDept(DataTable DtDimension)
        {
            RadDropDownList_Dept.DataSource = DtDimension;
            RadDropDownList_Dept.DisplayMember = "DeptName";
            RadDropDownList_Dept.ValueMember = "NUM";
        }

        //สร้าง ตัวเลือกกลุ่ม ตามแผนก และ ประเภท
        void SetDropdownlistGroup(DataTable DtDimension)
        {
            radDropDownList_GroupItems.DataSource = DtDimension;
            radDropDownList_GroupItems.DisplayMember = "ITEMGROUPIDNAME";
            radDropDownList_GroupItems.ValueMember = "ITEMGROUPID";
        }

        //สร้างปี 1 จากฟังชั่น
        void SetDropdownlistYear(DataTable DtYear)
        {
            radDropDownList_Year.DataSource = DtYear;
            radDropDownList_Year.DisplayMember = "Member";
            radDropDownList_Year.ValueMember = "Value";
            if (DtYear.Rows.Count > 0) radDropDownList_Year.SelectedValue = DateTime.Now.ToString("yyyy");
        }

        //สร้างเดือน จาก ฟังชั่น
        void SetDropdownlistMonth(DataTable DtMonth)
        {
            radDropDownList_Month.DataSource = DtMonth;
            radDropDownList_Month.DisplayMember = "Member";
            radDropDownList_Month.ValueMember = "Value";
            if (DtMonth.Rows.Count > 0) radDropDownList_Month.SelectedValue = DateTime.Now.ToString("MM");
        }

        //สร้าง default คอลัมภ์ ให้ Grid สาขาและวันที่ ////สร้าง datasouce barcode
        void SetDatagridColumnsAndRow(int Years, int Month, DataTable BranchAllOpen)
        {
            //สร้าง default คอลัมภ์ ให้ Grid สาขาและวันที่ 
            if (TypeCondition == "GroupItems")
            {
                SetGridViewGroup();
                int createDate = DateTime.DaysInMonth(Years, Month);
                for (int i = 1; i < createDate + 1; i++)
                {
                    String ColumnName = i.ToString("D2");// + "/ " + radDropDownList_Month.SelectedValue.ToString();
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox(ColumnName, ColumnName, 80));
                }

                if (BranchAllOpen.Rows.Count > 0)
                {
                    foreach (DataRow row in BranchAllOpen.Rows)
                    {
                        RadGridView_Show.Rows.Add(row["BRANCH_ID"], row["BRANCH_NAME"]);
                    }
                }

                if (RadDropDownList_Dept.SelectedValue.ToString() != "") RadGridView_BarcodeOrGroup.DataSource = ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePageID, SqlConditions);

            }
            else if (TypeCondition == "Barcode")
            {
                SetGridViewBarcode();
                for (int i = 0; i < BranchAllOpen.Rows.Count; i++)
                {
                    String ColumnID = BranchAllOpen.Rows[i]["BRANCH_ID"].ToString();
                    string bchName = BranchAllOpen.Rows[i]["BRANCH_NAME"].ToString();
                    if (ColumnID == "MN000") bchName = "ทุกสาขา";

                    String ColumnName = BranchAllOpen.Rows[i]["BRANCH_ID"].ToString() + Environment.NewLine +
                        bchName;

                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox(ColumnID, ColumnName, 80));

                }
                DataTable DtBarcode = ItembarcodeClass.ItemGroup_GetItemBarcodeByGroup(RadDropDownList_Dept.SelectedValue.ToString(),
                       radDropDownList_GroupItems.SelectedValue.ToString(),
                       _TypePageID);
                if (DtBarcode.Rows.Count > 0)
                {
                    for (int barcode = 0; barcode < DtBarcode.Rows.Count; barcode++)
                    {
                        RadGridView_Show.Rows.Add(DtBarcode.Rows[barcode]["ITEMBARCODE"].ToString(),
                            DtBarcode.Rows[barcode]["SPC_ITEMNAME"].ToString(),
                            DtBarcode.Rows[barcode]["ITEMUNIT"].ToString());

                        DataTable DtCheckBarcode = ItemGroupStockClass.GetItemStock_byDeptBarcode(
                            radDropDownList_GroupItems.SelectedValue.ToString(),
                            _TypePageID,
                            RadDropDownList_Dept.SelectedValue.ToString(),
                            DtBarcode.Rows[barcode]["ITEMBARCODE"].ToString());
                        if (DtCheckBarcode.Rows.Count > 0)
                        {
                            for (int i = 3; i < RadGridView_Show.Columns.Count; i++)
                            {
                                DataRow[] results = DtCheckBarcode.Select(String.Format(@"ITEMBARCODE = '{0}' AND ITEMBRANCH = '{1}' ",
                                DtBarcode.Rows[barcode]["ITEMBARCODE"].ToString(), RadGridView_Show.Columns[i].Name));

                                if (results.Length > 0)
                                {
                                    //กำหนดค่าเพื่อเลือก checked==true
                                    RadGridView_Show.Rows[RadGridView_Show.Rows.Count - 1].Cells[i].Value = "True";
                                }
                            }
                        }
                    }
                }


            }
        }

        //กำหนดค่่าเบื้องต้น
        void ClearData(String Type)
        {
            RadGridView_Show.DataSource = null;
            if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();

            RadGridView_BarcodeOrGroup.DataSource = null;
            if (RadGridView_BarcodeOrGroup.RowCount > 0) RadGridView_BarcodeOrGroup.Rows.Clear();
            if (RadGridView_BarcodeOrGroup.ColumnCount > 0) RadGridView_BarcodeOrGroup.Columns.Clear();

            switch (Type)
            {
                case "Default":
                    SetDropdownlistYear(ItemGroupStockClass.GetAllYear());
                    SetDropdownlistMonth(ItemGroupStockClass.GetAllMonth());
                    break;

                case "Add":
                    radDropDownList_GroupItems.Enabled = true;
                    break;

                case "Edit":
                    radDropDownList_GroupItems.Enabled = true;
                    break;
                case "Barcode":
                    radDropDownList_GroupItems.Visible = true;
                    radLabel_Group.Visible = true;
                    panel_Setting.Visible = false;
                    radLabel3.Visible = false;
                    radDropDownList_Month.Visible = false;
                    radLabel4.Visible = false;
                    radLabel2.Visible = false;
                    radDropDownList_Year.Visible = false;

                    break;
                case "GroupItems":
                    radDropDownList_GroupItems.Visible = false;
                    radLabel_Group.Visible = false;
                    panel_Setting.Visible = true;
                    radLabel3.Visible = true;
                    radDropDownList_Month.Visible = true;
                    radLabel4.Visible = true;
                    radLabel2.Visible = true;
                    radDropDownList_Year.Visible = true;
                    break;
            }
        }

        #endregion

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);

        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);

        }
        #endregion

        #region EVEN
        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (TypeCondition == "Barcode")
            {
                if (RadDropDownList_Dept.SelectedValue.ToString() != "") SetDropdownlistGroup(ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePageID, SqlConditions));
                else
                {
                    TypeEdit = "Default";
                    ClearData(TypeEdit);
                }
            }
            else if (TypeCondition == "GroupItems")
            {
                RadGridView_BarcodeOrGroup.DataSource =
                    ItemGroup_Class.GetItemGroup(RadDropDownList_Dept.SelectedValue.ToString(), _TypePageID, SqlConditions);
            }

        }
        private void RadDropDownList_GroupItems_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_GroupItems.SelectedValue == null)
            {
                TypeEdit = "Default";
                ClearData(TypeEdit);
            }

        }
        private void RadioButton_Item_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Item.Checked == true)
            {
                TypeCondition = "Barcode";
                ClearData(TypeCondition);
            }

        }
        private void RadioButton_GroupID_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_GroupID.Checked == true)
            {
                TypeCondition = "GroupItems";
                ClearData(TypeCondition);
            }
        }

        private void RadButton_setStock_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(IndexRowGroupId) || string.IsNullOrEmpty(IndexRowGroupName))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุกลุ่มสินค้า ที่ต้องการส่งสต๊อก ก่อนทุกครั้ง");
                return;
            }
            if (RadDateTimePicker_Begin.Value.Date < DateTime.Now.Date)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถตั้งค่าวันที่" + RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + Environment.NewLine +
                    "เนื่องจากเป็นวันที่น้อยกว่า ปัจจุบัน");
                return;
            }

            String BranchConditions = "";

            if (radioButton_SetBranchIN.Checked == true) BranchConditions = "AND BRANCH_OUTPHUKET='1'";
            else if (radioButton_SetBranchOUT.Checked == true) BranchConditions = "AND BRANCH_OUTPHUKET='0'";

            DataTable DtBranch = BranchClass.GetBranchAll_ByConditions("'1'", "'1'", BranchConditions);

            if (DtBranch.Rows.Count > 0)
            {
                //String SqlEx;
                String Dept, GroupID, Branch, DateStock;
                Dept = RadDropDownList_Dept.SelectedValue.ToString();
                GroupID = IndexRowGroupId;

                DateStock = RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
                DataTable DtGroupStock = ItemGroupStockClass.GetItemStock_byDate(GroupID, _TypePageID, Dept, DateStock);
                foreach (DataRow row in DtBranch.Rows)
                {
                    Branch = row["BRANCH_ID"].ToString();
                    DataRow[] results;
                    if (DtGroupStock.Rows.Count > 0) results = DtGroupStock.Select(String.Format("ITEMBRANCH = '{0}'  ", Branch));
                    else results = null;

                    if (results is null || results.Length == 0)
                    {
                        Var_ITEMGROUPSTOCK var_ITEMGROUPSTOCK = new Var_ITEMGROUPSTOCK
                        {
                            ITEMGROUPID = GroupID,
                            ITEMGROUPTYPE = _TypePageID,
                            ITEMDEPT = Dept,
                            ITEMBRANCH = Branch,
                            ITEMBARCODE = "",
                            ITEMUNIT = "",
                            ITEMREMARK = _TypePageName,
                            ITEMSDATESTOCK = DateStock
                        };


                        ConnectionClass.ExecuteSQL_Main(ItemGroup_Class.Save_ItemGroupStock("Add", var_ITEMGROUPSTOCK));
                    }

                }


                //ข้อมูลที่มีการตั้งค่าวันที่ส่งสต๊อกไว้ตามบาร์โค๊ด
                DataTable DtItemStock = ItemGroupStockClass.GetItemStock_byDeptGroupId(
                    IndexRowGroupId, _TypePageID,
                    RadDropDownList_Dept.SelectedValue.ToString(),
                    radDropDownList_Month.SelectedValue.ToString(),
                    radDropDownList_Year.SelectedValue.ToString());
                if (DtItemStock.Rows.Count > 0)
                {
                    for (int i = 0; i < RadGridView_Show.RowCount; i++)
                    {
                        //ค้นหาข้อมูลใน Datatable ตามวันที่
                        DataRow[] results = DtItemStock.Select(String.Format(@"ITEMBRANCH = '{0}' ",
                            RadGridView_Show.Rows[i].Cells[0].Value.ToString()));

                        if (results.Length > 0)
                        {
                            for (int r = 0; r < results.Length; r++)
                            {
                                int Columns = int.Parse(results[r].ItemArray[9].ToString());
                                //กำหนดค่าเพื่อเลือก checked==true
                                RadGridView_Show.Rows[i].Cells[Columns].Value = "True";
                            }

                        }
                    }
                }

            }
        }
        private void RadioButton_BranchIn_CheckedChanged(object sender, EventArgs e)
        {
            radioButton_SetBranchIN.Checked = true;
        }

        private void RadioButton_BranchOut_CheckedChanged(object sender, EventArgs e)
        {
            radioButton_SetBranchOUT.Checked = true;
        }


        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _TypePageID);
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            //ค้นหาข้อมูลตามเงื่อนไขที่มี
            if (RadDropDownList_Dept.SelectedValue == null)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีกลุ่มสินค้าในแผนกที่เลือก กรุณาตรวจสอบอีกครั้ง");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            if (radioButton_Item.Checked == true) TypeCondition = "Barcode";
            else TypeCondition = "GroupItems";

            String BranchConditions = "  ";

            if (radioButton_BranchIn.Checked == true) BranchConditions = "AND BRANCH_OUTPHUKET='1' ";// AND BRANCH_ORDERTOAX = '1'
            else if (radioButton_BranchOut.Checked == true) BranchConditions = "AND BRANCH_OUTPHUKET='0'  ";

            SetDatagridColumnsAndRow(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                          int.Parse(radDropDownList_Month.SelectedValue.ToString()),
                          BranchClass.GetBranchAll_ByConditions(" '2','1'", "'1'", BranchConditions));
            Cursor.Current = Cursors.Default;
        }

        private void RadButtonElement_Pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        //เลือกบาร์โค๊ดได้ 1 รายการ ในการกำหนมค่าสต๊อกตามวันที่
        private void RadGridView_BarcodeOrGroup_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                //เคลียร์บาร์โค๊ดเดิมเพื่อให้ได้สินค่าแค่ 1 รายการ
                if (IndexRow != -1 && IndexRow != e.RowIndex)
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("เลือกสินค้าได้ครั้งละ 1 กลุ่ม ยืนยันเลือกสินค้าตั้งค่าใหม่หรือไม่") == DialogResult.No) return;
                    else RadGridView_BarcodeOrGroup.Rows[IndexRow].Cells[e.ColumnIndex].Value = "False";
                }
                //สลับค่า true false ตามที่เลือก
                if (RadGridView_BarcodeOrGroup.CurrentRow.Cells[e.ColumnIndex].Value.ToString() == "True")
                {
                    IndexRowGroupId = string.Empty;
                    IndexRowGroupName = string.Empty;
                    IndexRow = -1;
                    RadGridView_BarcodeOrGroup.CurrentRow.Cells[e.ColumnIndex].Value = "False";
                }
                else
                {
                    //ตั้งค่าให้ตัวแปร เพื่อใช้สำหรับค้นหาวันที่ส่งสต๊อกตามบาร์โค๊ดที่เลือก 1 รายการ
                    IndexRowGroupId = RadGridView_BarcodeOrGroup.CurrentRow.Cells["ITEMGROUPID"].Value.ToString();
                    IndexRowGroupName = RadGridView_BarcodeOrGroup.CurrentRow.Cells["ITEMGROUPNAME"].Value.ToString();
                    IndexRow = e.RowIndex;
                    RadGridView_BarcodeOrGroup.CurrentRow.Cells[e.ColumnIndex].Value = "True";

                    //ข้อมูลที่มีการตั้งค่าวันที่ส่งสต๊อกไว้ตามบาร์โค๊ด
                    DataTable DtItemStock = ItemGroupStockClass.GetItemStock_byDeptGroupId(
                        IndexRowGroupId, _TypePageID,
                        RadDropDownList_Dept.SelectedValue.ToString(),
                        radDropDownList_Month.SelectedValue.ToString(),
                        radDropDownList_Year.SelectedValue.ToString());
                    if (DtItemStock.Rows.Count > 0)
                    {
                        for (int i = 0; i < RadGridView_Show.RowCount; i++)
                        {
                            //ค้นหาข้อมูลใน Datatable ตามวันที่
                            DataRow[] results = DtItemStock.Select(String.Format(@"ITEMBRANCH = '{0}' ",
                                RadGridView_Show.Rows[i].Cells[0].Value.ToString()));

                            if (results.Length > 0)
                            {
                                for (int r = 0; r < results.Length; r++)
                                {
                                    int Columns = int.Parse(results[r].ItemArray[9].ToString());
                                    //กำหนดค่าเพื่อเลือก checked==true
                                    RadGridView_Show.Rows[i].Cells[Columns].Value = "True";
                                }

                            }
                        }
                    }
                }
            }
        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (RadGridView_Show.Rows.Count > 0 && e.ColumnIndex > 1)
            {
                if (TypeCondition == "GroupItems")
                {
                    if (string.IsNullOrEmpty(IndexRowGroupId) || string.IsNullOrEmpty(IndexRowGroupName))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุกลุ่มสินค้า ที่ต้องการส่งสต๊อก ก่อนทุกครั้ง");
                        return;
                    }
                    //กำหนดค่าอัตโนมัติ ตามเงื่อนไข โดยวันที่ต้อง> datetime.now เท่านั้น
                    int DateNow = int.Parse(DateTime.Now.AddDays(0).ToString("dd"));
                    if (e.ColumnIndex - 1 < DateNow)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถแก้ไขวันที่ส่งสต๊อกย้อนหลังได้ กรุณาเลือกวันที่ใหม่." + Environment.NewLine +
                            "วันที่ปัจจุบัน " + DateTime.Now.AddDays(0).ToString("dd-MM-yyyy"));
                        return;
                    }

                    if (e.ColumnIndex > 1)
                    {
                        string SqlEx;
                        //TypePage
                        string Dept, GroupID, Branch, DateStock;
                        string CheckTrue;
                        Dept = RadDropDownList_Dept.SelectedValue.ToString();
                        GroupID = IndexRowGroupId;
                        Branch = RadGridView_Show.CurrentRow.Cells[0].Value.ToString();
                        DateStock = radDropDownList_Year.SelectedValue.ToString() + "-" +
                             radDropDownList_Month.SelectedValue.ToString() + "-" +
                             (e.ColumnIndex - 1).ToString("D2");

                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value == null) RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "False";

                        Var_ITEMGROUPSTOCK var_ITEMGROUPSTOCK = new Var_ITEMGROUPSTOCK
                        {
                            ITEMGROUPID = GroupID,
                            ITEMGROUPTYPE = _TypePageID,
                            ITEMDEPT = Dept,
                            ITEMBRANCH = Branch,
                            ITEMBARCODE = "",
                            ITEMUNIT = "",
                            ITEMREMARK = _TypePageName,
                            ITEMSDATESTOCK = DateStock
                        };

                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString() == "True")
                        {
                            SqlEx = ItemGroup_Class.Save_ItemGroupStock("Del", var_ITEMGROUPSTOCK);
                            CheckTrue = "False";
                        }
                        else
                        {
                            SqlEx = ItemGroup_Class.Save_ItemGroupStock("Add", var_ITEMGROUPSTOCK);
                            CheckTrue = "True";
                        }

                        string returnstr = ConnectionClass.ExecuteSQL_Main(SqlEx);
                        if (returnstr != "") MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                        else RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = CheckTrue;

                    }
                }
                else
                {
                    //TypeConditions==Barcode
                    if (e.ColumnIndex > 2)
                    {
                        //TypePage
                        String Dept, GroupID, Barcode, Branch;
                        Dept = RadDropDownList_Dept.SelectedValue.ToString();
                        GroupID = radDropDownList_GroupItems.SelectedValue.ToString();
                        Barcode = RadGridView_Show.CurrentRow.Cells[0].Value.ToString();
                        //UnitID = RadGridView_Show.CurrentRow.Cells[2].Value.ToString();
                        Branch = RadGridView_Show.CurrentCell.Data.FieldName;

                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value == null) RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "False";

                        Var_ITEMGROUPSTOCK var_ITEMGROUPSTOCK = new Var_ITEMGROUPSTOCK
                        {
                            ITEMGROUPID = GroupID,
                            ITEMGROUPTYPE = _TypePageID,
                            ITEMDEPT = Dept,
                            ITEMBRANCH = Branch,
                            ITEMBARCODE = Barcode,
                            ITEMUNIT = "",
                            ITEMREMARK = _TypePageName,
                            ITEMSDATESTOCK = ""
                        };
                        string sql, CheckTF = "True";
                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString() == "True")
                        {
                            sql = ItemGroup_Class.Save_ItemGroupStock("Del", var_ITEMGROUPSTOCK);
                            CheckTF = "False";
                        }
                        else sql = ItemGroup_Class.Save_ItemGroupStock("Add", var_ITEMGROUPSTOCK);
 
                        string resault = ConnectionClass.ExecuteSQL_Main(sql);
                        if (resault == "") RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = CheckTF;
                        else MsgBoxClass.MsgBoxShow_SaveStatus(resault);
                    }
                }
            }
        }

        private void RadGridView_Barcode_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
          
        }

        #endregion


    }

}
