﻿namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    partial class MNOR_LoadOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNOR_LoadOrder));
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radDateTimePicker_Recieve2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDropDownList_routeBeer = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_BchType = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_SubGroup = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_MainGroup = new Telerik.WinControls.UI.RadDropDownList();
            this.radStatusStrip_Menu = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Clear = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_SavePO = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.radDateTimePicker_Recieve = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radCheckBox_BchType = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadCheckBox_GrpSub = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_routeBeer = new Telerik.WinControls.UI.RadCheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Recieve2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_routeBeer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_BchType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_SubGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Recieve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_BchType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_GrpSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_routeBeer)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 602);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.CustomFiltering += new Telerik.WinControls.UI.GridViewCustomFilteringEventHandler(this.RadGridView_Show_CustomFiltering);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radDateTimePicker_Recieve2);
            this.panel1.Controls.Add(this.radDropDownList_routeBeer);
            this.panel1.Controls.Add(this.radDropDownList_BchType);
            this.panel1.Controls.Add(this.radDropDownList_SubGroup);
            this.panel1.Controls.Add(this.radDropDownList_MainGroup);
            this.panel1.Controls.Add(this.radStatusStrip_Menu);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Controls.Add(this.RadCheckBox_Branch);
            this.panel1.Controls.Add(this.radDateTimePicker_Recieve);
            this.panel1.Controls.Add(this.radCheckBox_BchType);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadCheckBox_GrpSub);
            this.panel1.Controls.Add(this.radCheckBox_routeBeer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 2;
            // 
            // radDateTimePicker_Recieve2
            // 
            this.radDateTimePicker_Recieve2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Recieve2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_Recieve2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_Recieve2.Location = new System.Drawing.Point(10, 294);
            this.radDateTimePicker_Recieve2.Name = "radDateTimePicker_Recieve2";
            this.radDateTimePicker_Recieve2.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_Recieve2.TabIndex = 46;
            this.radDateTimePicker_Recieve2.TabStop = false;
            this.radDateTimePicker_Recieve2.Text = "26/04/2020";
            this.radDateTimePicker_Recieve2.Value = new System.DateTime(2020, 4, 26, 9, 17, 9, 0);
            this.radDateTimePicker_Recieve2.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Recieve2_ValueChanged);
            // 
            // radDropDownList_routeBeer
            // 
            this.radDropDownList_routeBeer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_routeBeer.BackColor = System.Drawing.Color.White;
            this.radDropDownList_routeBeer.DropDownAnimationEnabled = false;
            this.radDropDownList_routeBeer.DropDownHeight = 124;
            this.radDropDownList_routeBeer.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_routeBeer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_routeBeer.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_routeBeer.Location = new System.Drawing.Point(10, 344);
            this.radDropDownList_routeBeer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_routeBeer.Name = "radDropDownList_routeBeer";
            // 
            // 
            // 
            this.radDropDownList_routeBeer.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_routeBeer.Size = new System.Drawing.Size(175, 25);
            this.radDropDownList_routeBeer.TabIndex = 44;
            this.radDropDownList_routeBeer.Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_routeBeer.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_routeBeer.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_routeBeer.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radDropDownList_BchType
            // 
            this.radDropDownList_BchType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_BchType.BackColor = System.Drawing.Color.White;
            this.radDropDownList_BchType.DropDownAnimationEnabled = false;
            this.radDropDownList_BchType.DropDownHeight = 124;
            this.radDropDownList_BchType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_BchType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_BchType.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_BchType.Location = new System.Drawing.Point(10, 214);
            this.radDropDownList_BchType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_BchType.Name = "radDropDownList_BchType";
            // 
            // 
            // 
            this.radDropDownList_BchType.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_BchType.Size = new System.Drawing.Size(175, 25);
            this.radDropDownList_BchType.TabIndex = 42;
            this.radDropDownList_BchType.Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_BchType.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_BchType.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_BchType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radDropDownList_SubGroup
            // 
            this.radDropDownList_SubGroup.DropDownAnimationEnabled = false;
            this.radDropDownList_SubGroup.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_SubGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_SubGroup.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_SubGroup.Location = new System.Drawing.Point(10, 166);
            this.radDropDownList_SubGroup.Name = "radDropDownList_SubGroup";
            this.radDropDownList_SubGroup.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_SubGroup.TabIndex = 40;
            this.radDropDownList_SubGroup.Text = "radDropDownList1";
            // 
            // radDropDownList_MainGroup
            // 
            this.radDropDownList_MainGroup.DropDownAnimationEnabled = false;
            this.radDropDownList_MainGroup.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_MainGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_MainGroup.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_MainGroup.Location = new System.Drawing.Point(10, 118);
            this.radDropDownList_MainGroup.Name = "radDropDownList_MainGroup";
            this.radDropDownList_MainGroup.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_MainGroup.TabIndex = 39;
            this.radDropDownList_MainGroup.Text = "radDropDownList1";
            this.radDropDownList_MainGroup.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_MainGroup_SelectedValueChanged);
            // 
            // radStatusStrip_Menu
            // 
            this.radStatusStrip_Menu.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip_Menu.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip_Menu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Clear,
            this.commandBarSeparator1,
            this.radButtonElement_Excel,
            this.commandBarSeparator2,
            this.radButtonElement_SavePO,
            this.commandBarSeparator3,
            this.RadButtonElement_pdf,
            this.commandBarSeparator4});
            this.radStatusStrip_Menu.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip_Menu.Name = "radStatusStrip_Menu";
            this.radStatusStrip_Menu.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip_Menu.TabIndex = 36;
            // 
            // radButtonElement_Clear
            // 
            this.radButtonElement_Clear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Clear.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Clear.Name = "radButtonElement_Clear";
            this.radStatusStrip_Menu.SetSpring(this.radButtonElement_Clear, false);
            this.radButtonElement_Clear.Text = "";
            this.radButtonElement_Clear.ToolTipText = "Clear";
            this.radButtonElement_Clear.Click += new System.EventHandler(this.RadButtonElement_Clear_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip_Menu.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip_Menu.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.ToolTipText = "บันทึก Excel";
            this.radButtonElement_Excel.UseCompatibleTextRendering = false;
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Menu.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_SavePO
            // 
            this.radButtonElement_SavePO.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_SavePO.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButtonElement_SavePO.Name = "radButtonElement_SavePO";
            this.radStatusStrip_Menu.SetSpring(this.radButtonElement_SavePO, false);
            this.radButtonElement_SavePO.Text = "radButtonElement1";
            this.radButtonElement_SavePO.ToolTipText = "บันทึก PO";
            this.radButtonElement_SavePO.Click += new System.EventHandler(this.RadButtonElement_SavePO_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip_Menu.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdf
            // 
            this.RadButtonElement_pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdf.Name = "RadButtonElement_pdf";
            this.radStatusStrip_Menu.SetSpring(this.RadButtonElement_pdf, false);
            this.RadButtonElement_pdf.Text = "radButtonElement1";
            this.RadButtonElement_pdf.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip_Menu.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(10, 95);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(61, 19);
            this.radLabel2.TabIndex = 35;
            this.radLabel2.Text = "กลุ่มหลัก";
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 383);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 30;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.DropDownAnimationEnabled = false;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Branch.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(10, 70);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Branch.TabIndex = 29;
            this.RadDropDownList_Branch.Text = "radDropDownList1";
            // 
            // RadCheckBox_Branch
            // 
            this.RadCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Branch.Location = new System.Drawing.Point(10, 47);
            this.RadCheckBox_Branch.Name = "RadCheckBox_Branch";
            this.RadCheckBox_Branch.Size = new System.Drawing.Size(76, 19);
            this.RadCheckBox_Branch.TabIndex = 28;
            this.RadCheckBox_Branch.Text = "ระบุสาขา";
            this.RadCheckBox_Branch.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Branch_CheckStateChanged);
            // 
            // radDateTimePicker_Recieve
            // 
            this.radDateTimePicker_Recieve.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Recieve.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_Recieve.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_Recieve.Location = new System.Drawing.Point(10, 267);
            this.radDateTimePicker_Recieve.Name = "radDateTimePicker_Recieve";
            this.radDateTimePicker_Recieve.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_Recieve.TabIndex = 26;
            this.radDateTimePicker_Recieve.TabStop = false;
            this.radDateTimePicker_Recieve.Text = "26/04/2020";
            this.radDateTimePicker_Recieve.Value = new System.DateTime(2020, 4, 26, 9, 17, 9, 0);
            this.radDateTimePicker_Recieve.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Recieve_ValueChanged);
            // 
            // radCheckBox_BchType
            // 
            this.radCheckBox_BchType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_BchType.Location = new System.Drawing.Point(10, 192);
            this.radCheckBox_BchType.Name = "radCheckBox_BchType";
            this.radCheckBox_BchType.Size = new System.Drawing.Size(122, 19);
            this.radCheckBox_BchType.TabIndex = 41;
            this.radCheckBox_BchType.Text = "ระบุประเภทสาขา";
            this.radCheckBox_BchType.CheckStateChanged += new System.EventHandler(this.RadCheckBox_BchType_CheckStateChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(10, 245);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(101, 19);
            this.radLabel1.TabIndex = 25;
            this.radLabel1.Text = "วันที่ขอรับสินค้า";
            // 
            // RadCheckBox_GrpSub
            // 
            this.RadCheckBox_GrpSub.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_GrpSub.Location = new System.Drawing.Point(10, 144);
            this.RadCheckBox_GrpSub.Name = "RadCheckBox_GrpSub";
            this.RadCheckBox_GrpSub.Size = new System.Drawing.Size(96, 19);
            this.RadCheckBox_GrpSub.TabIndex = 32;
            this.RadCheckBox_GrpSub.Text = "ระบุกลุ่มย่อย";
            this.RadCheckBox_GrpSub.CheckStateChanged += new System.EventHandler(this.RadCheckBox_GrpSub_CheckStateChanged);
            // 
            // radCheckBox_routeBeer
            // 
            this.radCheckBox_routeBeer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_routeBeer.Location = new System.Drawing.Point(10, 322);
            this.radCheckBox_routeBeer.Name = "radCheckBox_routeBeer";
            this.radCheckBox_routeBeer.Size = new System.Drawing.Size(149, 19);
            this.radCheckBox_routeBeer.TabIndex = 43;
            this.radCheckBox_routeBeer.Text = "ระบุสายส่งเหล้า-เบียร์";
            this.radCheckBox_routeBeer.CheckStateChanged += new System.EventHandler(this.RadCheckBox_routeBeer_CheckStateChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 611);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(617, 14);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>DoubleClick ช่องจำนวน &gt;&gt; แก้ไขจำนวน [กรณีเลือกวันที่วันเดียว จะปรับยอ" +
    "ดสั่งสาขา | กรณีเลือกช่วงวันที่ จะไม่ปรับยอดสาขาสั่ง]</html>";
            // 
            // MNOR_LoadOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MNOR_LoadOrder";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ข้อมูลการเบิกสินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MNOR_LoadOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Recieve2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_routeBeer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_BchType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_SubGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Recieve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_BchType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_GrpSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_routeBeer)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_GrpSub;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Branch;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Recieve;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Menu;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Clear;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_SavePO;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_MainGroup;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_SubGroup;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_BchType;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_BchType;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_routeBeer;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_routeBeer;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Recieve2;
    }
}
