﻿using System.Data;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    class MNOR_ITEM
    {
        public string BRANCH { get; set; } = "";
        public string BARCODE { get; set; } = "";
        public string ITEMNAME { get; set; } = "";
        public string UNITID { get; set; } = "";
        public double QTY { get; set; } = 0.0;
        public string DOCNO { get; set; } = "";
    }

    class MNOR_Class
    {
        public static DataTable FindMNORHD(string top, string date1, string date2, string routeBeer, string bchID,
            string dptID, string groupID, string outBch, string sqlWhereAddOn)
        {
            string tblRouteBeer = "";
            if (routeBeer != "") tblRouteBeer = $@" INNER JOIN SHOP_BRANCH_CONFIGDB WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = SHOP_BRANCH_CONFIGDB.BRANCH_ID AND BRANCH_ROUTEBEER = '{routeBeer}' ";

            string whereBch = "";
            if (bchID != "") whereBch = $@" AND SHOP_MNOR_HD.BRANCH_ID = '{bchID}' ";

            string whereGroup = "";
            if (groupID != "") whereGroup = $@" AND SHOP_MNOR_HD.GROUPID = '{groupID}' ";

            string whereDptID = "";
            if (dptID != "") whereDptID = $@" AND SHOP_MNOR_HD.DPTID = '{dptID}' ";

            string whereOutBch = "";
            if (outBch != "") whereOutBch = $@" AND BRANCH_OUTPHUKET = '{outBch}' ";

            string str = $@"
            SELECT  {top} DOCNO,CONVERT(VARCHAR,SHOP_MNOR_HD.DATEINS,23) AS DOCDATE,
                        SHOP_MNOR_HD.DPTID + '-' + DESCRIPTION AS GROUPMAIN, SHOP_MNOR_HD.GROUPID + '-' + ITEMGROUPNAME AS GROUPSUB,
			            SHOP_MNOR_HD.BRANCH_ID + '-' + ISNULL(SHOP_MNOR_HD.BRANCH_NAME,'') AS BRANCH_NAME,
		                SHOP_MNOR_HD.WHOIDINS + '-' + ISNULL(SHOP_MNOR_HD.WHONAMEINS,'') AS WHOIDINS,
		                CASE STA_DOC WHEN '3' THEN '1' ELSE '0' END AS STADOC,
                        CASE STA_DOC WHEN '3' THEN SHOP_MNOR_HD.WHOIDUPD +'-'+ISNULL(SHOP_MNOR_HD.WHONAMEUPD,'')  ELSE '' END AS WHOCANCLE,
                        STA_PRCDOC AS STAPRCDOC,CASE STA_DOC WHEN '1' THEN SHOP_MNOR_HD.WHOIDUPD+'-'+ISNULL(SHOP_MNOR_HD.WHONAMEUPD ,'')ELSE '' END AS WHOIDUPD,
                        STA_APVDOC AS STAAPV,SHOP_MNOR_HD.WHOIDAPV+'-'+ISNULL(SHOP_MNOR_HD.WHONAMEAPV,'') AS WHOIDAPV  ,
                        CONVERT(VARCHAR,DATE_RECIVE,23) AS MNORDateRecive,CASE SHOP_BRANCH.BRANCH_OUTPHUKET  WHEN '1' THEN 'ในจังหวัด' ELSE 'ต่างจังหวัด' END AS OUTPHUKET,
                        CONVERT(VARCHAR,DATE_APV,25) AS DATEAPV
            FROM	SHOP_MNOR_HD WITH (NOLOCK)
		            LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON SHOP_MNOR_HD.DPTID = DIMENSIONS.NUM AND DATAAREAID = N'SPC' AND DIMENSIONCODE = '0'
                    LEFT OUTER JOIN SHOP_ITEMGROUP WITH (NOLOCK) ON SHOP_MNOR_HD.GROUPID = SHOP_ITEMGROUP.ITEMGROUPID
                    INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNOR_HD.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                    {tblRouteBeer}
            WHERE	CONVERT(VARCHAR,DATE_RECIVE,23) BETWEEN '{date1}'  AND '{date2}' {whereBch}  {whereGroup} {whereDptID} {whereOutBch}
                    {sqlWhereAddOn}
            ORDER BY DOCNO DESC ";

            return ConnectionClass.SelectSQL_Main(str);
        }
        public static DataTable FindMNORDT(string docno)
        {
            return ConnectionClass.SelectSQL_Main($@"
                    SELECT	ITEMBARCODE,SPC_ITEMNAME,QtyOrder AS QTY, UNITID  
                    FROM	SHOP_MNOR_DT WITH (NOLOCK) 
                    WHERE	DOCNO = '{docno}' AND STA_SEND = '1' ");
        }
        public static string UpdateStaAPVMNOR(string docno)
        {
            return $@"
            UPDATE  SHOP_MNOR_HD 
            SET     STA_APVDOC =  '1' ,
                    DATE_APV = CONVERT(varchar,getdate(),(25)),
                    WHOIDAPV = '{SystemClass.SystemUserID}',WHONAMEAPV = '{SystemClass.SystemUserName}'
            WHERE DocNo = '{docno}' ";
        }
        //ต้นทุนของสินค้าจำนวนเบียร์สิงห์-ลีโอ ที่ขึ้นอยู่กับจังหวัดของสาขา
        public static double FindCostPriceBeer(string pBarcode, string pBchID)
        {
            string str = $@"
                SELECT	ISNULL(REMARK,0) AS COST
                FROM	SHOP_CONFIGBRANCH_DETAIL  WITH (NOLOCK)  
                WHERE	TYPE_CONFIG ='50' AND SHOW_ID = '{pBarcode}'	
		                AND BRANCH_ID IN (SELECT BRANCH_PROVINCE FROM	SHOP_BRANCH WITH (NOLOCK) WHERE	BRANCH_ID = '{pBchID}')
            ";
            DataTable dt = ConnectionClass.SelectSQL_Main(str);
            if (dt.Rows.Count == 0) return 0;
            else return double.Parse(dt.Rows[0]["COST"].ToString());
        }
        //ค้นหาข้อมูลของสินค้าที่สั่งมาในวันที่/สาขา/บาร์โค้ด -- เพิ่อแสดงค่าเลขที่เอกสาร
        public static string GetDataOrderDT(string branch, string barcode, string date, string Docno = "")
        {
            string sql = string.Format($@"
                    SELECT  SHOP_MNOR_HD.DOCNO AS MNORDocno,ITEMBARCODE AS MNORBarcode,SPC_ITEMNAME AS MNORName,QTYORDER AS MNORQtyOrder,UNITID AS MNORQtyUnitID 
                    FROM	SHOP_MNOR_HD WITH (NOLOCK) 
                            INNER JOIN  SHOP_MNOR_DT WITH (NOLOCK)  ON SHOP_MNOR_HD.DOCNO = SHOP_MNOR_DT.DOCNo   
                    WHERE	STA_DOC = '1' AND STA_APVDOC = '1'  
                            AND DATE_RECIVE = '{date}' AND MNORBarcode = '{barcode}' AND MNORBranch = '{branch}' 
                    ORDER BY BRANCH_ID,ITEMBARCODE ");

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) Docno = dt.Rows[0]["MNORDocno"].ToString();
            return Docno;
        }
        //ค้นหาข้อมูลสั่งหมดที่สั่งมา
        public static DataTable GetItemOrder(string pDate1, string pDate2)
        {
            string sql = $@"
                    SELECT	BRANCH_ID AS MNORBranch,ITEMBARCODE AS MNORBarcode,SUM(QTYORDER) AS MNORQtyOrder  
                    FROM    SHOP_MNOR_HD WITH (NOLOCK) 
                            INNER JOIN  SHOP_MNOR_DT WITH (NOLOCK)  ON SHOP_MNOR_HD.DOCNO = SHOP_MNOR_DT.DOCNO  
                    WHERE	STA_DOC = '1' AND STA_APVDOC = '1'  AND STA_SEND = '1'  
                            AND DATE_RECIVE between '{pDate1}' AND '{pDate2}' 
                    GROUP BY BRANCH_ID,ITEMBARCODE 
                    ORDER BY BRANCH_ID,ITEMBARCODE";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Update จำนวนที่สั่ง
        public static string UpdateQty(string docno, string barcode, double qty)
        {
            string sql = $@"
                    Update  SHOP_MNOR_DT Set QTYORDER = '{qty}',
                            DATEUPD =  convert(varchar,getdate(),25),
                            STA_SEND = '1',
                            WHOIDUPD = '{ SystemClass.SystemUserID}' ,WHONAMEUPD = '{ SystemClass.SystemUserName}'
                    WHERE   DOCNO = '{docno}' AND ITEMBARCODE = '{barcode}' ";
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        public static DataTable GetVenderBeer()
        {
            return ConnectionClass.SelectSQL_Main($@"
                SELECT  BRANCH_VENDERBEER
                FROM	SHOP_BRANCH_CONFIGDB WITH (NOLOCK)
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_BRANCH_CONFIGDB.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                WHERE	SHOP_BRANCH.BRANCH_STAOPEN = '1'  
		                AND BRANCH_VENDERBEER != ''
                GROUP BY BRANCH_VENDERBEER ");
        }
        //ค้นหาเอกสาร HD ของ PurchTable ทั้งหมด ของ 708
        public static DataTable FindDataPurchTable(string vatIn, string vender, string date1, string date2)
        {//SUM(LINEAMOUNT) AS SUM_BEFOREVAT,CAST(SUM(LINEAMOUNT)*1.07 AS decimal(15,2)) AS SUM_VAT,
            return ConnectionClass.SelectSQL_MainAX($@"
            DECLARE	@CASEVAT NVARCHAR(2) = '{vatIn}'
            SELECT	OrderAccount,PURCHNAME,PurchTable.PurchId,
                    CASE ISNULL(INVENTLOCATION.NAME,'')  WHEN '' THEN 'บริษัท ซุปเปอร์ชีป จำกัด' ELSE  ISNULL(INVENTLOCATION.NAME,'') END AS DELIVERYNAME,
		            CASE ISNULL(PurchTable.INVENTLOCATIONID,'') WHEN '' THEN 'MN000' ELSE ISNULL(PurchTable.INVENTLOCATIONID,'MN000')  END AS INVENTLOCATIONID,
                    CASE @CASEVAT WHEN '1'	THEN SUM(LINEAMOUNT) ELSE CAST((SUM(LINEAMOUNT)*100)/107 AS decimal(15,2)) END SUM_BEFOREVAT,
                    CASE @CASEVAT WHEN '1'	THEN CAST(SUM(LINEAMOUNT)*1.07 AS decimal(15,2)) ELSE SUM(LINEAMOUNT) END SUM_VAT,
                    '' AS BB,'Click' AS PRT,'Click' AS PO,
                    ROW_NUMBER() OVER (ORDER BY PurchTable.PurchId ) AS RROWS ,PurchTable.IVZ_REMARKS
            FROM	PurchTable WITH (NOLOCK)		
		            INNER JOIN PurchLine WITH (NOLOCK) ON PurchTable.PurchId = PurchLine.PurchId
                    LEFT OUTER JOIN INVENTLOCATION WITH (NOLOCK) ON PurchTable.INVENTLOCATIONID = INVENTLOCATION.INVENTLOCATIONID 
            WHERE	PurchTable.SPC_PurchDate BETWEEN '{date1}'  AND '{date2}'
		            AND OrderAccount IN ( {vender} )
		            AND PurchTable.DATAAREAID = N'SPC' AND PurchLine.DATAAREAID = N'SPC'
            GROUP BY OrderAccount,PURCHNAME,PurchTable.PurchId,PurchTable.DELIVERYNAME,
                    CASE ISNULL(PurchTable.INVENTLOCATIONID,'') WHEN '' THEN 'MN000' ELSE ISNULL(PurchTable.INVENTLOCATIONID,'MN000')  END ,
                    CASE ISNULL(INVENTLOCATION.NAME,'')  WHEN '' THEN 'บริษัท ซุปเปอร์ชีป จำกัด' ELSE  ISNULL(INVENTLOCATION.NAME,'') END,PurchTable.IVZ_REMARKS
            HAVING SUM(LINEAMOUNT) > 0
            ORDER BY PurchTable.PurchId ");
        }
        //ค้นหาเอกสาร DT ของ PurchTable ทั้งหมด ของ 708
        public static DataTable FindDataPurchLine(string docno)
        {
            return ConnectionClass.SelectSQL_MainAX($@"
            SELECT   BARCODE, NAME, CAST(PurchQty AS decimal(15, 2)) AS QTYORDERED, PURCHUNIT,
                    CAST(PURCHPRICE AS decimal(15, 2)) AS PURCHPRICE, CAST(LINEAMOUNT AS decimal(15, 2)) AS LINEAMOUNT,
                    ROW_NUMBER() OVER(ORDER BY LINENUM) AS RROWS
            FROM    PurchLine WITH(NOLOCK)
            WHERE   PurchId = '{docno}'   AND DATAAREAID = N'SPC'
            ORDER   BY LINENUM");
        }
        //report รายงานสรุปจำนวนซื้อ-ขาย-เบิก-ใบสั่งโอนย้าย ตามใบสั่งซื้อใน AX 705
        public static DataTable FindReportSPCN_SumAllTransByPurchId(string date1, string date2, string docno)
        {
            return ConnectionClass.SelectSQL_SentServer($@" SPCN_SumAllTransByPurchId '{date1}','{date2}','{docno}','1' ",
                IpServerConnectClass.ConMainReportProcess);
        }
    }

}
