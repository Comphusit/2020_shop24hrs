﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.MNOR
{
    public partial class MNOR_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();
        readonly string _pTypeOpen;
        public string pDocno;

        public MNOR_FindData(string pTypeOpen)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void MNOR_FindData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Recive.ShowBorder = true; radButtonElement_Recive.ToolTipText = "รับออเดอร์ทุกรายการ";

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now.AddDays(1));

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_GrpMain.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_BchType.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_routeBeer.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpMain);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_BchType);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_routeBeer); radDropDownList_routeBeer.Enabled = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAAPV", "รับออเดอร์")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OUTPHUKET", "ประเภทสาขา", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNORDateRecive", "วันที่รับสินค้า", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN", "จัดซื้อ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPSUB", "กลุ่มย่อย", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAPRCDOC", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDUPD", "ผู้ยืนยัน", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOCANCLE", "ผู้ยกเลิก", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDAPV", "ผู้รับออเดอร์", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEAPV", "วันเวลา รับออเดอร์", 180)));

            RadGridView_ShowHD.Columns["OUTPHUKET"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;
            RadGridView_ShowHD.Columns["STAAPV"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));


            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            DataTable dtBch;//= new DataTable();
            if (SystemClass.SystemBranchID == "MN000")
            {
                RadCheckBox_Branch.Checked = false;
                dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                RadCheckBox_Branch.Enabled = true;
                radButtonElement_Recive.Enabled = true;

                radCheckBox_Apv.Checked = true;
                radLabel_Detail.Visible = true;
                if (_pTypeOpen == "1") RadButton_Choose.Visible = true; else RadButton_Choose.Visible = false;
            }
            else
            {
                RadCheckBox_Branch.Checked = true;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadButton_Choose.Visible = true;
                RadCheckBox_Branch.Enabled = false;
                radCheckBox_Apv.Checked = false;
                radLabel_Detail.Visible = false;
                radButtonElement_Recive.Enabled = false;
            }

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            radDropDownList_BchType.DataSource = BranchClass.GetBranchType();
            radDropDownList_BchType.DisplayMember = "OUTPHUKET";
            radDropDownList_BchType.ValueMember = "BRANCH_OUTPHUKET";

            radDropDownList_routeBeer.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("49", "", "", "1");
            radDropDownList_routeBeer.DisplayMember = "SHOW_NAME";
            radDropDownList_routeBeer.ValueMember = "SHOW_ID";

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_End.Value = DateTime.Now.AddDays(1);
            radDateTimePicker_Begin.Value = DateTime.Now;

            Set_GroupMain();
            Set_GropMainSub();
            SetDGV_HD(" TOP 100 ");
        }
        //Set HD
        void SetDGV_HD(string _pTop)
        {
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }

            string sqlRouteBeer = "";
            if (radCheckBox_routeBeer.Checked == true) sqlRouteBeer = radDropDownList_routeBeer.SelectedValue.ToString();
            string bchID = "";
            if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();

            string dptID = "", groupID = "";
            if (RadCheckBox_GrpMain.Checked == true) groupID = radDropDownList_MainGroup.SelectedValue.ToString();
            else
            if (SystemClass.SystemBranchID == "MN000") dptID = RadDropDownList_GrpMain.SelectedValue.ToString();

            string outBch = "";
            if (radCheckBox_BchType.Checked == true) outBch = radDropDownList_BchType.SelectedValue.ToString();

            string sqlWhereAddOn = "";
            if (radCheckBox_Apv.Checked == true) sqlWhereAddOn = " AND STA_APVDOC = '0' AND STA_DOC = '1' AND STA_PRCDOC IN ('1','0') ";

            dtDataHD = MNOR_Class.FindMNORHD(_pTop,
                radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                sqlRouteBeer, bchID, dptID, groupID, outBch, sqlWhereAddOn); //ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.DataSource = dtDataHD;
          
            if (dtDataHD.Rows.Count == 0) if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }

        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            
            dtDataDT = MNOR_Class.FindMNORDT(_pDocno); //ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD("");
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;

            if (RadGridView_ShowHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
                return;

            }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //GroupMain
        void Set_GroupMain()
        {
            RadDropDownList_GrpMain.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("47", "", "", "1");
            RadDropDownList_GrpMain.ValueMember = "SHOW_ID";//NUM
            RadDropDownList_GrpMain.DisplayMember = "SHOW_NAME";
            RadDropDownList_GrpMain.SelectedIndex = 0;
        }
        //sub grp
        void Set_GropMainSub()
        {
            radDropDownList_MainGroup.DataSource = ItemGroup_Class.GetItemGroup(RadDropDownList_GrpMain.SelectedValue.ToString(), "01", "  AND ITEMGROUPSTATUS1 = '1'  ");
            radDropDownList_MainGroup.ValueMember = "ITEMGROUPID";
            radDropDownList_MainGroup.DisplayMember = "ITEMGROUPIDNAME";
            radDropDownList_MainGroup.SelectedIndex = 0;
        }
        //Group Main
        private void RadCheckBox_GrpMain_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpMain.Checked == true) radDropDownList_MainGroup.Enabled = true;
            else radDropDownList_MainGroup.Enabled = false;
        }
        //คลิกรับบิล
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (e.Column.Name)
            {
                case "STAAPV":
                    if (SystemClass.SystemBranchID != "MN000") return;
                    if (SystemClass.SystemDptID == "D054") return;

                    if (RadGridView_ShowHD.Rows.Count == 0) return;
                    if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("เอกสารถูกยกเลิก ไม่สามารถรับออเดอร์ได้.");
                        return;
                    }

                    string docno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();

                    if (RadGridView_ShowHD.CurrentRow.Cells["STAPRCDOC"].Value.ToString() == "0")
                    {
                        if ((MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(docno, " ยืนยันการอนุมัติบิลที่สาขายังไม่ได้ยืนยัน") == DialogResult.No)) return;
                    }
 
                    string T = ConnectionClass.ExecuteSQL_Main(MNOR_Class.UpdateStaAPVMNOR(docno));
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["STAAPV"].Value = "1";
                        RadGridView_ShowHD.CurrentRow.Cells["WHOIDAPV"].Value = SystemClass.SystemUserID + "-" + SystemClass.SystemUserName;
                        RadGridView_ShowHD.CurrentRow.Cells["DATEAPV"].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    break;
                default:
                    break;
            }

        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true;
            else RadDropDownList_Branch.Enabled = false;
        }
        //Main
        private void RadDropDownList_GrpMain_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_GrpMain.SelectedValue.ToString() == "D034") radCheckBox_routeBeer.Enabled = true;
            else { radCheckBox_routeBeer.Enabled = false; radCheckBox_routeBeer.Checked = false; }
        }

        private void RadDropDownList_GrpMain_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            Set_GropMainSub();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //รับออเดอร์ทั้งหมด
        private void RadButtonElement_Recive_Click(object sender, EventArgs e)
        {
            if (SystemClass.SystemBranchID != "MN000") return;
            if (SystemClass.SystemDptID == "D054") return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            ArrayList sql = new ArrayList();

            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                if (RadGridView_ShowHD.Rows[i].Cells["STADOC"].Value.ToString() == "1") break;
                if (RadGridView_ShowHD.Rows[i].Cells["STAPRCDOC"].Value.ToString() == "0") break;
                string docno = RadGridView_ShowHD.Rows[i].Cells["DOCNO"].Value.ToString();
                sql.Add(MNOR_Class.UpdateStaAPVMNOR(docno));
            }

            if (sql.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีรายการที่สามารถบันทึกรับออเดอร์ได้{Environment.NewLine}เช็คข้อมูลที่แสดงทั้งหมดอีกครั้ง"); return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการรับออเดอร์ทั้ง {Convert.ToString(sql.Count)} บิล ?{Environment.NewLine}[เฉพาะรายการที่สาขายืนยันแล้วเท่านั้น]") == DialogResult.No) return;

            string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "") SetDGV_HD("");
        }
        //ประเภทสาขา
        private void RadCheckBox_BchType_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_BchType.Checked == true) radDropDownList_BchType.Enabled = true;
            else radDropDownList_BchType.Enabled = false;
        }

        private void RadCheckBox_routeBeer_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_routeBeer.Checked == true) radDropDownList_routeBeer.Enabled = true;
            else radDropDownList_routeBeer.Enabled = false;
        }

    }
}
