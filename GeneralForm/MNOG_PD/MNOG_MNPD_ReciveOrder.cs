﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNOG_PD
{
    public partial class MNOG_MNPD_ReciveOrder : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pPermission; //0 ดูข้อมูลเฉยๆ 1 จัดซื้อบันทึกรับออเดอร์ 2 centershop บันทึกลูกค้ารับ
        readonly string _pTypeOpen;
        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        //Load
        public MNOG_MNPD_ReciveOrder(string pTypeOpen, string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void MNOG_MNPD_ReciveOrder_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            if (_pTypeOpen == "MNOG")
            {
                DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);

                radCheckBox_Grp.ButtonElement.Font = SystemClass.SetFontGernaral;

                radLabel_Detail.Visible = false;

                radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true;

                radLabel_Detail.Visible = true;
                radDropDownList_Grp.DataSource = Models.DptClass.GetDpt_PurchaseForMNOG();//.FindPurchaseForMNOG();
                radDropDownList_Grp.ValueMember = "NUM";
                radDropDownList_Grp.DisplayMember = "DESCRIPTION";

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("RECIVE_STA", "รับ")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOBranch", "สาขา", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocNo", "เลขที่บิล", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPODate", "วันที่สั่ง", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RECIVE_DATE", "ต้องการสินค้า", 90)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOBarcode", "บาร์โค้ด", 130)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOName", "ชื่อสินค้า", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNPOQtyOrder", "จำนวนสั่ง", 85)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOUnitID", "หน่วย", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "จัดซื้อ", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUMDESC", "แผนกจัดซื้อ", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RECIVE_RMK", "หมายเหตุไม่รับออเดอร์", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOAX", "OrderToAX", 210)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("MNPOStaPrcDoc", "ยืนยัน")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ผู้เปิด", 220)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุการสั่ง", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstId", "รหัสลูกค้า", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstName", "ชื่อลูกค้า", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstTel", "เบอร์โทร", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPOSeqNo", "LineNum")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STAA", "STA")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CstSTA", "ลูกค้ารับสินค้าแล้ว")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OPENBY", "OPENBY", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MAXDATEIN", "โทรหาลูกค้าล่าสุด", 120)));

                RadGridView_ShowHD.MasterTemplate.Columns["RECIVE_STA"].IsPinned = true;
                RadGridView_ShowHD.MasterTemplate.Columns["MNPOBranch"].IsPinned = true;
                RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;

                DatagridClass.SetCellBackClolorByExpression("MNPOBranch", "CstSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", "CstSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("DocNo", "CstSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                DatagridClass.SetCellBackClolorByExpression("MNPOBranch", " STAA = '2' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("BRANCH_NAME", " STAA = '2' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("DocNo", " STAA = '2' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("RECIVE_RMK", " STAA = '2' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("MAXDATEIN", " MAXDATEIN = '1900-01-01' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                switch (_pPermission)
                {
                    case "0":
                        radLabel_Detail.Text = "สีแดง >> จัดซื้อไม่รับออเดอร์ | สีม่วง >> ลูกค้ายังไม่ได้รับสินค้า";
                        break;
                    case "1":
                        radLabel_Detail.Text = "Click ช่องรับ >> เพื่อบันทึกรับหรือยืนยันออเดอร์ | สีแดง >> จัดซื้อไม่รับออเดอร์ | สีม่วง >> ลูกค้ายังไม่ได้รับสินค้า ";
                        break;
                    case "2":
                        radLabel_Detail.Text = "สีแดง >> จัดซื้อไม่รับออเดอร์ | สีม่วง >> ลูกค้ายังไม่ได้รับสินค้า | DoubleClick ช่องเลขที่บิล >> บันทึกลูกค้ารับแล้ว";
                        break;
                    default:
                        break;
                }
            }
            else//MNPD
            {
                radCheckBox_Grp.Visible = false;
                radDropDownList_Grp.Visible = false;
                radLabel_Detail.Visible = true;
                switch (_pPermission)
                {
                    case "0":
                        radLabel_Detail.Text = "สีม่วง >> ลูกค้ายังไม่ได้รับสินค้า | สีแดง >> จัดซื้อยังจัดสินค้าไม่เสร็จ | สีเหลือง >> จัดซื้อจัดบิลเสร็จเรียบร้อยแล้ว";
                        break;
                    case "1":
                        radLabel_Detail.Text = "สีม่วง >> ลูกค้ายังไม่ได้รับสินค้า | สีแดง >> จัดซื้อยังจัดสินค้าไม่เสร็จ | สีเหลือง >> จัดซื้อจัดบิลเสร็จเรียบร้อยแล้ว";
                        break;
                    case "2":
                        radLabel_Detail.Text = "สีม่วง >> ลูกค้ายังไม่ได้รับสินค้า  | สีแดง >> จัดซื้อยังจัดสินค้าไม่เสร็จ | สีเหลือง >> จัดซื้อจัดบิลเสร็จเรียบร้อยแล้ว | DoubleClick ช่องเลขที่บิล >> บันทึกลูกค้ารับแล้ว";
                        break;
                    default:
                        break;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Branch", "สาขา", 70)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocNo", "เลขที่บิล", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่สั่ง", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TYPEORDER", "การรอสินค้า", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "สถานะบิลจัดแผนก", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 130)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemName", "ชื่อสินค้า", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Qty", "จำนวนสั่ง", 85)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("StaPrcDoc", "ยืนยัน")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoShopName", "ผู้เปิด", 220)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุการสั่ง", 200)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstID", "รหัสลูกค้า", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Customer", "ชื่อลูกค้า", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Tel", "เบอร์โทร", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CstSTA", "ลูกค้ารับสินค้าแล้ว")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPOSeqNo", "LineNum")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OPENBY", "OPENBY", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MAXDATEIN", "โทรหาลูกค้าล่าสุด", 120)));

                RadGridView_ShowHD.MasterTemplate.Columns["Branch"].IsPinned = true;
                RadGridView_ShowHD.MasterTemplate.Columns["BranchName"].IsPinned = true;

                DatagridClass.SetCellBackClolorByExpression("Branch", "CstSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("BranchName", "CstSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("DocNo", "CstSTA = '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("MAXDATEIN", " MAXDATEIN = '1900-01-01' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                DatagridClass.SetCellBackClolorByExpression("SHOW_NAME", " SHOW_NAME <> 'จัดระบบเก่า'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("SHOW_NAME", " SHOW_NAME = 'ส่งเข้า AX เรียบร้อย'  ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            }

            ClearTxt();
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); this.RadGridView_ShowHD.FilterDescriptors.Clear(); }
            if (_pTypeOpen == "MNOG")
            {
                string pCon = "";
                if (radCheckBox_Grp.Checked == true) pCon = " AND NUMID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";
                if (SystemClass.SystemBranchID != "MN000") pCon = $@" AND BRANCH_ID = '{SystemClass.SystemBranchID}' ";

                dt_Data = MNOG_MNPD_Class.FindMNOG_DetailByDate(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pCon);
            }
            else
            {
                dt_Data = MNOG_MNPD_Class.FindMNPD_DetailByDate(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
            }
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;

        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(0);
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
            radCheckBox_Grp.Checked = false;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายการสั่งสินค้าลูกค้า [" + _pTypeOpen + @"] ", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //cellclick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pPermission != "2") return;

            if (RadGridView_ShowHD.CurrentRow.Cells["CstSTA"].Value.ToString() == "1") return;

            switch (e.Column.Name)
            {
                case "DocNo":
                    string docno = RadGridView_ShowHD.CurrentRow.Cells["DocNo"].Value.ToString();
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกลูกค้ารับสินค้าเรียบร้อยแล้ว" + Environment.NewLine +
                        "เลขที่บิล " + docno + " ?") == DialogResult.No) return;

                    string strUser = "ลูกค้ารับเรียบร้อยแล้ว -> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

                    string seqNo = RadGridView_ShowHD.CurrentRow.Cells["MNPOSeqNo"].Value.ToString();
                    string upSql;

                    if (_pTypeOpen == "MNOG")
                    {
                        upSql = string.Format(@"
                            UPDATE  SHOP_MNOG_DT
                            SET     RECIVE_STA = '1',
                                    REMARK = ISNULL(REMARK,'') + '" + strUser + @"'+CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']'  ,
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '" + SystemClass.SystemUserID + @"',WHOUPDNAME = '" + SystemClass.SystemUserName + @"'
                            WHERE   DOCNO = '" + docno + @"' 
                                    AND LINENUM = '" + seqNo + @"'
                            ");
                    }
                    else
                    {
                        upSql = string.Format(@"
                            UPDATE  SHOP_MNPD_DT
                            SET     STA_SHOP = '1',
                                    REMARK = ISNULL(REMARK,'') + '" + strUser + @"'+CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']'  ,
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '" + SystemClass.SystemUserID + @"',WHOUPDNAME = '" + SystemClass.SystemUserName + @"'
                            WHERE   DocNo = '" + docno + @"' 
                                    AND LINENUM = '" + seqNo + @"'
                            ");
                    }

                    string T = ConnectionClass.ExecuteSQL_Main(upSql);
                    if (T == "")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["CstSTA"].Value = "1";
                        RadGridView_ShowHD.CurrentRow.Cells["REMARK"].Value = strUser;
                    }
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);

                    break;
                default:
                    break;
            }

        }
        //กดรับออเดอร์ ของแผนกจัดซื้อ
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pPermission == "0") return;

            if (e.Column.Name == "RECIVE_STA")
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["RECIVE_STA"].Value.ToString() == "1") return;

                if (RadGridView_ShowHD.CurrentRow.Cells["MNPOStaPrcDoc"].Value.ToString() == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("บิลยังไม่มีการืนยันการสั่งออเดอร์ลูกค้า ไม่สามารถรับออเดอร์ได้.");
                    return;
                }

                if (RadGridView_ShowHD.CurrentRow.Cells["NUM"].Value.ToString() != SystemClass.SystemDptID)
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการรับออเดอร์ลูกค้า ต่างแผนก [สินค้าไม่ใช่แผนกของตัวเอง].") == DialogResult.No) return;
                }

                string sta, rmk;
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการรับออเดอร์ลูกค้า" + Environment.NewLine +
                    "กด Yes เพื่อยืนยันรับออเดอร์" + Environment.NewLine + "กด No เพื่อไม่รับออเดอร์ พร้อมระบุหมายเหตุการไม่รับออเดอร์") == DialogResult.No)
                {
                    ShowRemark _showRmk = new ShowRemark("1")
                    {
                        pDesc = "ระบุหมายเหตุที่ไม่รับออเดอร์"
                    };
                    if (_showRmk.ShowDialog(this) == DialogResult.Yes)
                    {
                        sta = "2"; rmk = _showRmk.pRmk;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("การยืนยันไม่รับออเดอร์ จะต้องระบุหมายเหตุทุกครั้ง ลองใหม่อีกครั้ง");
                        return;
                    }
                }
                else
                {
                    sta = "1"; rmk = "";
                }

                string sqlUp = string.Format(@"
                    UPDATE  SHOP_MNOG_DT 
                    SET     RECIVE_STA  = '" + sta + @"',RECIVE_RMK = '" + rmk + @"',
                            DATEUPD = convert(varchar,getdate(),25),WHOUPD = '" + SystemClass.SystemUserID + @"',WHOUPDNAME = '" + SystemClass.SystemUserName + @"'
                    WHERE   DOCNO = '" + RadGridView_ShowHD.CurrentRow.Cells["DocNo"].Value.ToString() + @"' 
                            AND LINENUM = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPOSeqNo"].Value.ToString() + @"'
                ");
                string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                if (T == "")
                {
                    RadGridView_ShowHD.CurrentRow.Cells["STAA"].Value = sta;
                    RadGridView_ShowHD.CurrentRow.Cells["RECIVE_STA"].Value = "1";
                    RadGridView_ShowHD.CurrentRow.Cells["RECIVE_RMK"].Value = rmk;
                }


                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pPermission);
        }
    }
}

