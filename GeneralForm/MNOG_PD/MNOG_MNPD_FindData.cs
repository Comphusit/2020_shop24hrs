﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.MNOG_PD
{
    public partial class MNOG_MNPD_FindData : Telerik.WinControls.UI.RadForm
    {

        readonly PrintController printController = new StandardPrintController();

        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();

        public string pDocno;
        readonly string _pTypeOpen;//MNPD-MNOG

        public MNOG_MNPD_FindData(string pTypeOpen)
        {
            InitializeComponent();

            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void MNOG_MNPD_FindData_Load(object sender, EventArgs e)
        {

            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAPRCDOC", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOBranch", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOBranchName", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 150)));

            if (_pTypeOpen == "MNOG") RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RECIVE_DATE", "วันที่รับ", 90)));
            else RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RECIVE_DATE", "สถานะจ่ายของ", 120)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDUPD", "ผู้ยืนยัน", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OPENBY", "แผนกเปิด", 210)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "หมายเหตุ", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstDetail", "รายละเอียดลูกค้า", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CountPrt", "พิมพ์", 80)));

            RadGridView_ShowHD.Columns["STAPRCDOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["STADOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;

            DatagridClass.SetCellBackClolorByExpression("DOCNO", "CountPrt = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("UNITID", "หน่วย", 100)));
            if (_pTypeOpen == "MNOG")
            {
                RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUMID", "แผนก", 80)));
                RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUMDesc", "จัดซื้อ", 300)));
            }

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_Begin.Value = DateTime.Now;
            radDateTimePicker_End.Value = DateTime.Now;

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000")
            {
                RadCheckBox_Branch.Checked = false;
                dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                RadCheckBox_Branch.Enabled = true;
                radLabel_Detail.Visible = true;
                radLabel_Detail.Text = "สีแดง >> สาขายังไม่พิมพ์บิล";
            }
            else
            {
                RadCheckBox_Branch.Checked = true;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Enabled = false;
                radLabel_Detail.Visible = true;
                radLabel_Detail.Text = "DoubleClick เลขที่บิล >> พิมพ์บิล | สีแดง >> สาขายังไม่พิมพ์บิล";
            }
            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            if (_pTypeOpen == "MNOG")
            {
                this.Text = "ข้อมูลการออเดอร์สินค้าลูกค้าของสด [MNOG]"; SetDGV_HD_MNOG();
            }
            else
            {
                this.Text = "ข้อมูลการออเดอร์สินค้าลูกค้าของทั่วไป [MNPD]"; SetDGV_HD_MNPD();
            }

        }
        //Set HD
        void SetDGV_HD_MNOG()
        {

            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }

            string pBch = "";
            if (SystemClass.SystemBranchID != "MN000")
            {
                pBch = $@" AND Branch_ID = '{SystemClass.SystemBranchID}' ";
            }
            else
            {
                if (RadCheckBox_Branch.Checked == true) pBch = $@" AND Branch_ID = '{RadDropDownList_Branch.SelectedValue}' ";
            }

            string pSta = "";
            if (radCheckBox_Apv.Checked == true) pSta = " AND StaDoc = '1'  AND StaPrcDoc = '0'  ";

            dtDataHD = MNOG_MNPD_Class.FindDetailHD_MNOG(
                 radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                 radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), pSta, pBch);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }

        //Set HD
        void SetDGV_HD_MNPD()
        {

            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }

            string pBch = "";
            if (SystemClass.SystemBranchID != "MN000")
            {
                pBch = " AND BRANCH_ID = '" + SystemClass.SystemBranchID + @"' ";
            }
            else
            {
                if (RadCheckBox_Branch.Checked == true)
                {
                    pBch = " AND BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"' ";
                }
            }

            string pSta = "";
            if (radCheckBox_Apv.Checked == true) pSta = " AND StaDoc = '1'  AND StaPrcDoc = '0'  ";

            dtDataHD = MNOG_MNPD_Class.FindDetailHD_MNPD(
                radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), pSta, pBch);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            if (_pTypeOpen == "MNOG") dtDataDT = MNOG_MNPD_Class.FindDetailDT_MNOG(_pDocno);
            else dtDataDT = MNOG_MNPD_Class.FindDetailDT_MNPD(_pDocno);


            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (_pTypeOpen == "MNOG") SetDGV_HD_MNOG(); else SetDGV_HD_MNPD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() == "")) { return; }

            if (RadGridView_ShowHD.Rows.Count == 0) { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Print
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {

            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (SystemClass.SystemBranchID == "MN000") return;

            if (RadGridView_ShowHD.CurrentRow.Cells["STADOC"].Value.ToString() == "1") return;

            if (RadGridView_ShowHD.CurrentRow.Cells["STAPRCDOC"].Value.ToString() == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลขที่บิล " + RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() + " ยังไม่ได้รับการยืนยัน" + Environment.NewLine +
                    "ให้ยืนยันให้เรียบร้อยก่อนพิมพ์");
                return;
            }

            switch (e.Column.Name)
            {
                case "DOCNO":
                    PrintData();
                    break;
                default:
                    break;
            }

        }
        //print
        void PrintData()
        {
            PrintDialog printDlg = new PrintDialog();
            if (printDlg.ShowDialog(this) == DialogResult.OK)
            {
                PrintDocument.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDlg.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument.PrinterSettings = printDlg.PrinterSettings;
                PrintDocument.Print();
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int countPrt = int.Parse(RadGridView_ShowHD.CurrentRow.Cells["CountPrt"].Value.ToString()) + 1;
            int Y = 0;


            string pHead = "ออเดอร์ลูกค้า [ของสด].";
            string upStr = $@"
                        UPDATE  SHOP_MNOG_HD  SET COUNT_PRINT = '{countPrt}' 
                        WHERE   DOCNO = '{RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value}' ";

            if (_pTypeOpen == "MNPD")
            {
                pHead = "ออเดอร์ลูกค้า [ของทั่วไป].";
                upStr = string.Format(@"
                        UPDATE SHOP_MNPD_HD  SET COUNT_PRINT = '" + countPrt + @"' 
                        WHERE  DOCNO = '" + RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() + "' ");
            }

            barcode.Data = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            Bitmap barcodeInBitmap0 = new Bitmap(barcode.drawBarcode());


            e.Graphics.DrawString("พิมพ์ครั้งที่ " + countPrt.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(pHead, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["MNPOBranch"].Value.ToString() + "-" + RadGridView_ShowHD.CurrentRow.Cells["MNPOBranchName"].Value.ToString(), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap0, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < RadGridView_ShowDT.Rows.Count; i++)
            {
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ".(" + RadGridView_ShowDT.Rows[i].Cells["QtyOrder"].Value.ToString() + " " +
                                  RadGridView_ShowDT.Rows[i].Cells["UNITID"].Value.ToString() + ")   " +
                                  RadGridView_ShowDT.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(" " + RadGridView_ShowDT.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("หมายเหตุ " + RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value.ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + RadGridView_ShowHD.CurrentRow.Cells["WHOIDUPD"].Value.ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ลูกค้า : " + RadGridView_ShowHD.CurrentRow.Cells["CstDetail"].Value.ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
            if (_pTypeOpen == "MNOG")
            {
                Y += 20;
                e.Graphics.DrawString("วันที่รับต้องการของ : " + RadGridView_ShowHD.CurrentRow.Cells["RECIVE_DATE"].Value.ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
            }
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;

            if (ConnectionClass.ExecuteSQL_Main(upStr) == "")
            {
                RadGridView_ShowHD.CurrentRow.Cells["CountPrt"].Value = countPrt;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}
