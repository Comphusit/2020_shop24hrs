﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;
using PC_Shop24Hrs.FormShare.ShowData;
using System.Drawing.Printing;
using PC_Shop24Hrs.FormShare;

namespace PC_Shop24Hrs.GeneralForm.MNOG_PD
{
    public partial class MNPD_Order : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        Data_CUSTOMER cust;

        public string pCstID;
        public string pCstName;
        public string pCstTel;

        readonly DataTable dt_PO = new DataTable();
        string pApv;

        readonly string _pOpenBy;
        public MNPD_Order(string pOpenBy)
        {
            InitializeComponent();

            _pOpenBy = pOpenBy;
        }
        //Load Main
        private void MNPD_Order_Load(object sender, EventArgs e)
        {
            RadButton_FindCst.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Bch);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 500));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน"));

            RadGridView_Show.MasterTemplate.EnableFiltering = false;
            DatagridClass.SetCellBackClolorByExpression("QtyOrder", "QtyOrder = '0.00' ", ConfigClass.SetColor_Red(), RadGridView_Show);

            dt_PO.Columns.Add("ITEMID");
            dt_PO.Columns.Add("INVENTDIMID");
            dt_PO.Columns.Add("ITEMBARCODE");
            dt_PO.Columns.Add("SPC_ITEMNAME");
            dt_PO.Columns.Add("QtyOrder");
            dt_PO.Columns.Add("UNITID");
            dt_PO.Columns.Add("QTY");

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดเอกสารใหม่";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";

            RadButton_Apv.ButtonElement.ShowBorder = true; RadButton_Apv.ButtonElement.ToolTipText = "ยืนยันการสั่ง ";
            RadButton_Cancel.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ToolTipText = "ยกเลิกการสั่ง ";

            ClearData();
        }

        void Set_DropDown()
        {
            if (SystemClass.SystemBranchID == "MN000") radDropDownList_Bch.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
            else radDropDownList_Bch.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

            radDropDownList_Bch.DisplayMember = "NAME_BRANCH";
            radDropDownList_Bch.ValueMember = "BRANCH_ID";

        }

        //Clear
        void ClearData()
        {
            if (dt_PO.Rows.Count > 0) { dt_PO.Rows.Clear(); dt_PO.AcceptChanges(); }

            Set_DropDown();
            radLabel_Docno.Text = "";
            pApv = "0";
            radTextBox_Tel.Text = pCstTel;
            radTextBox_CstID.Text = pCstID;
            radTextBox_CstName.Text = pCstName;
            radTextBox_rmk.Text = "";


            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
            radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red();
            radLabel_StatusBill.BackColor = Color.Transparent;
            radLabel_StatusBill.Text = "";

            //radDateTimePicker_D1.Enabled = true; 
            radTextBox_CstID.Enabled = false; radTextBox_CstName.Enabled = true;
            radTextBox_Tel.Enabled = true; radTextBox_rmk.Enabled = true; RadButton_FindCst.Enabled = true;

            radDropDownList_Bch.Enabled = true;

            radTextBox_Barcode.Enabled = true; radTextBox_Barcode.Text = "";
            RadButton_Cancel.Enabled = false;
            RadButton_Apv.Enabled = false;
            radTextBox_Barcode.Focus();
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview

        #endregion
        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
            Set_DropDown();
        }

        //ยืนยัน
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {

            if (dt_PO.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการสั่งสินค้าออเดอร์ลูกค้า" + radLabel_Docno.Text);
                return;
            }

            if (radTextBox_CstName.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("ชื่อลูกค้า");
                radTextBox_CstName.Focus();
                return;
            }

            if (radTextBox_Tel.Text == "")
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeForInsert("เบอร์โทรลูกค้า");
                radTextBox_Tel.Focus();
                return;
            }

            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยืนยันการสั่งออเดอร์ลูกค้า") == DialogResult.No) { return; }

            string pStaAX = "1"; // ลูกค้ารับสินค้าเลย 
            ChooseData _choose = new ChooseData("ลูกค้ารับสินค้าเลย" + Environment.NewLine + "(เอาสินค้าจากสาขาให้)", "ลูกค้ารอรับสินค้า" + Environment.NewLine + "(รอสินค้าจากสาขาใหญ่)");
            if (_choose.ShowDialog(this) == DialogResult.Yes)
            {
                if (_choose.sSendData == "0")
                {
                    pStaAX = "2"; //ลูกค้ารอรับสินค้า
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุการรับสินค้าของลูกค้าให้เรียบร้อย");
                return;
            }

            string T;

            string bch = radDropDownList_Bch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(radDropDownList_Bch.SelectedValue.ToString());
            ArrayList sqlUp24 = new ArrayList
                    {
                       string.Format($@" 
                            UPDATE  SHOP_MNPD_HD
                            SET     STAPRCDOC = '1',CUST_ID = '" + radTextBox_CstID.Text + @"',TYPEPRODUCT = '" + pStaAX + @"',
                                    Remark = '" + radTextBox_rmk.Text + @"',
                                    CUST_NAME = '" + radTextBox_CstName.Text + @"',CUST_TEL = '" + radTextBox_Tel.Text + @"',
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '" + SystemClass.SystemUserID_M + @"',WHOUPDNAME = '" + SystemClass.SystemUserName + @"',
                                    BRANCH_ID = '" + bch + @"',BRANCH_NAME = '" + bchName + @"'
                            WHERE   DOCNO = '" + radLabel_Docno.Text + @"'  ")
                    };

            if (pStaAX == "2")
            {
                //DataTable dtForSend = POClass.GetDataDetailMNPD_ForSendAX(radLabel_Docno.Text);
                //if (dtForSend.Rows.Count == 0)
                //{
                //    MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลออเดอร์สินค้าลูกค้า " + radLabel_Docno.Text);
                //    return;
                //}
                DataTable dtForSend = ConnectionClass.SelectSQL_Main($@" CashDesktop_OrderCust '5','{radLabel_Docno.Text}' ");
                sqlUp24.Add(ConnectionClass.SelectSQL_Main($@"  CashDesktop_OrderCust '6','{radLabel_Docno.Text}' ").Rows[0]["STRINSERT"].ToString());
                T = AX_SendData.Save_POSTOLINE10(sqlUp24, dtForSend, "98");
            }
            else
            {
                T = ConnectionClass.ExecuteSQL_ArrayMain(sqlUp24);
            }

            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยืนยันการสั่งออเดอร์ลูกค้า");

            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel();
                pApv = "1";
                radTextBox_Barcode.Enabled = false; RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                radDropDownList_Bch.Enabled = false;
                radTextBox_CstID.Enabled = false; radTextBox_CstName.Enabled = false;
                radTextBox_Tel.Enabled = false; radTextBox_rmk.Enabled = false; RadButton_FindCst.Enabled = false;

                if (SystemClass.SystemBranchID != "MN000")
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการพิมพ์สลิปออเดอร์ลูกค้า " + radLabel_Docno.Text + " หรือไม่ ?.") == DialogResult.Yes)
                    {
                        PrintData();
                    }
                    else return;
                }
                else return;
            }

        }
        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยกเลิกการสั่งออเดอร์ลูกค้า") == DialogResult.No) { return; }
            string sqlCancle = string.Format(@"
                            UPDATE  SHOP_MNPD_HD
                            SET     STADOC = '3',
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '" + SystemClass.SystemUserID_M + @"' ,WHOUPDNAME = '" + SystemClass.SystemUserName + @"'
                            WHERE   DOCNO = '" + radLabel_Docno.Text + @"'  ");
            String T = ConnectionClass.ExecuteSQL_Main(sqlCancle);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยกเลิกการสั่งออเดอร์ลูกค้า");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                radTextBox_Barcode.Enabled = false; RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                radDropDownList_Bch.Enabled = false;
                //radDateTimePicker_D1.Enabled = false; 
                radTextBox_CstID.Enabled = false; radTextBox_CstName.Enabled = false;
                radTextBox_Tel.Enabled = false; radTextBox_rmk.Enabled = false; RadButton_FindCst.Enabled = false;
            }
        }
        //Find Data
        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNOG_MNPD_FindData _mnpo_FindData = new MNOG_MNPD_FindData("MNPD");
            if (_mnpo_FindData.ShowDialog(this) == DialogResult.Yes)
            {
                SetDGV_Load(_mnpo_FindData.pDocno);
            }
        }
        //set Data Find
        void SetDGV_Load(string pDocno)
        {
            DataTable dt = MNOG_MNPD_Class.FindDetail_MNPD(pDocno);
            if (dt_PO.Rows.Count > 0) { dt_PO.Rows.Clear(); }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt_PO.Rows.Add(
                    dt.Rows[i]["ITEMID"].ToString(), dt.Rows[i]["INVENTDIMID"].ToString(),
                    dt.Rows[i]["ITEMBARCODE"].ToString(), dt.Rows[i]["SPC_ITEMNAME"].ToString(),
                    dt.Rows[i]["QtyOrder"].ToString(),
                    dt.Rows[i]["UNITID"].ToString(),
                    string.Format("{0:0.00}", Convert.ToDouble(dt.Rows[i]["QTY"].ToString()))
                    );
            }

            RadGridView_Show.DataSource = dt_PO;
            dt_PO.AcceptChanges();

            Set_DropDown();

            radDropDownList_Bch.SelectedValue = dt.Rows[0]["BRANCH"].ToString();
            radLabel_Docno.Text = dt.Rows[0]["DocNo"].ToString();

            radTextBox_CstID.Text = dt.Rows[0]["CstId"].ToString();
            radTextBox_CstName.Text = dt.Rows[0]["CstName"].ToString();
            radTextBox_Tel.Text = dt.Rows[0]["CstTel"].ToString();
            radTextBox_rmk.Text = dt.Rows[0]["Remark"].ToString();

            switch (dt.Rows[0]["STADOC"].ToString())
            {
                case "1":
                    if (dt.Rows[0]["STAPRCDOC"].ToString() == "1")
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                        RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                        radTextBox_Barcode.Enabled = false; radDropDownList_Bch.Enabled = false;
                        // radDateTimePicker_D1.Enabled = false; 
                        radTextBox_CstID.Enabled = false; radTextBox_CstName.Enabled = false;
                        radTextBox_Tel.Enabled = false; radTextBox_rmk.Enabled = false; RadButton_FindCst.Enabled = false;
                    }
                    else
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel(); pApv = "0";
                        RadButton_Apv.Enabled = true; RadButton_Cancel.Enabled = true;
                        radTextBox_Barcode.Enabled = true;
                        radDropDownList_Bch.Enabled = true;
                        //radDateTimePicker_D1.Enabled = true; 
                        radTextBox_CstID.Enabled = false; radTextBox_CstName.Enabled = true;
                        radTextBox_Tel.Enabled = true; radTextBox_rmk.Enabled = true; RadButton_FindCst.Enabled = true;
                    }
                    break;
                case "0":
                    radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                    RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false; radDropDownList_Bch.Enabled = false;
                    radTextBox_Barcode.Enabled = false;
                    //radDateTimePicker_D1.Enabled = false;
                    radTextBox_CstID.Enabled = false; radTextBox_CstName.Enabled = false;
                    radTextBox_Tel.Enabled = false; radTextBox_rmk.Enabled = false; RadButton_FindCst.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        //Enter + F4 In Itembarcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(radTextBox_Barcode.Text.Trim());
                if (!(dtBarcode.GetItembarcodeStatus))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
                SetDataInputBarcode(radTextBox_Barcode.Text.Trim());
            }

            else if (e.KeyCode == Keys.F4)
            {
                string pCon;
                if (radTextBox_Barcode.Text != "")
                { pCon = "AND SPC_ITEMNAME LIKE '%" + radTextBox_Barcode.Text.Replace(" ", "%") + @"%' "; }
                else { pCon = ""; }

                ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(pCon);
                if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                {
                    Data_ITEMBARCODE dtBarcode = ShowDataDGV_Itembarcode.items;
                    SetDataInputBarcode(dtBarcode.Itembarcode_ITEMBARCODE);
                }
                else
                {
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
            }
        }
        //Choose Barcode And Input QtyOrder
        void SetDataInputBarcode(string pBarcode)
        {
            DataTable dtItem = ConnectionClass.SelectSQL_Main(" POClass_GetDetailItembarcode_ForPO '" + pBarcode + @"','" + radDropDownList_Bch.SelectedValue.ToString() + @"' ");

            //Itembarcode.ItembarcodeDetail _itembarcodeDetail = new Itembarcode.ItembarcodeDetail(pBarcode, radDropDownList_Bch.SelectedValue.ToString(), "0", "0", pBarcode);
            Itembarcode.ItembarcodeDetail_Order _itembarcodeDetail = new Itembarcode.ItembarcodeDetail_Order(dtItem, "0", "0", radDropDownList_Bch.SelectedValue.ToString());

            if (_itembarcodeDetail.ShowDialog(this) == DialogResult.Yes)
            {
                string num = "";
                if (_itembarcodeDetail.sPurchase.Length > 3)
                {
                    num = _itembarcodeDetail.sPurchase.Substring(0, 4);
                }

                if (SystemClass.SystemPurchaseFresh.Contains(num) == true)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าเป็นสินค้าทั่วไป ไม่สามารถเปิดออเดอร์ของทั่วไป MNPD ได้" + Environment.NewLine +
                        "ให้เปิดออเดอร์สั่งของสด MNOG แทน");
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }

                //check ว่า Barcode ที่เลือกอยู่ใน DGV แล้วหรือยัง
                if (RadGridView_Show.Rows.Count > 0)
                {
                    //ITEMBARCODE
                    DataRow[] dr = dt_PO.Select("ITEMID = '" + _itembarcodeDetail.sID + "' AND INVENTDIMID = '" + _itembarcodeDetail.sDim + "'  ");
                    if (dr.Length > 0)
                    {
                        foreach (DataRow item in dr)
                        {
                            if (double.Parse(item["QtyOrder"].ToString()) > 0)
                            {
                                MessageBox.Show("รายการที่เลือก มีอยู่ในบิลเรียบร้อยแล้ว ไม่สามารถเพิ่มรายการได้ " + Environment.NewLine +
                                                                                    "[ให้แก้ไขจำนวนการสั่งใหม่ แทนการเพิ่มรายการ]",
                                                                                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                radTextBox_Barcode.SelectAll();
                                radTextBox_Barcode.Focus();
                                return;
                            }
                        }
                    }
                }

                //ระบุจำนวน
                FormShare.InputData _inputdata = new FormShare.InputData("0", _itembarcodeDetail.sBarcode + "-" + _itembarcodeDetail.sSpc_Name, "จำนวนที่ต้องการสั่ง", _itembarcodeDetail.sUnitID)
                { pInputData = "1.00" };
                if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                {
                    string invent = _itembarcodeDetail.sPOINVENT;
                    if (_itembarcodeDetail.sPOINVENT == "") invent = "RETAILAREA";
                    {

                    }
                    if (dt_PO.Rows.Count == 0)
                    {
                        SaveDataHD(_itembarcodeDetail.sID, _itembarcodeDetail.sDim, _itembarcodeDetail.sBarcode,
                        _itembarcodeDetail.sSpc_Name, Convert.ToDouble(_inputdata.pInputData),
                        _itembarcodeDetail.sUnitID, Convert.ToDouble(_itembarcodeDetail.sFactor),
                        Convert.ToDouble(_itembarcodeDetail.sPrice), _itembarcodeDetail.sDimension, _itembarcodeDetail.sPathImage, invent, _itembarcodeDetail.sVenderID
                        );
                    }
                    else
                    {
                        SaveDataDT(_itembarcodeDetail.sID, _itembarcodeDetail.sDim, _itembarcodeDetail.sBarcode,
                         _itembarcodeDetail.sSpc_Name, Convert.ToDouble(_inputdata.pInputData),
                         _itembarcodeDetail.sUnitID, Convert.ToDouble(_itembarcodeDetail.sFactor),
                         Convert.ToDouble(_itembarcodeDetail.sPrice), _itembarcodeDetail.sDimension, _itembarcodeDetail.sPathImage, invent, _itembarcodeDetail.sVenderID
                         );
                    }
                }
                else
                {
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
            }
            else
            {
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
            }
        }
        //Check Values In DGV IS NULL + ยืนยันหรือยัง
        string CheckValuesInDGV()
        {
            string rr = "1";
            switch (pApv)
            {
                case "0":
                    if ((RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "") ||
                           (RadGridView_Show.CurrentRow.Index == -1) ||
                           (RadGridView_Show.Rows.Count == 0))
                    {
                        rr = "1";//กรณีไม่มีค่า
                    }
                    else
                    {
                        rr = "0";//กรณีที่มีค่าให้ทำต่อได้
                    }
                    break;
                case "1":
                    rr = "1";
                    break;
                case "3":
                    rr = "1";
                    break;
                default:
                    break;
            }
            return rr;
        }
        //CellDoubleClick
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QtyOrder"://แก้ไขจำนวนสั่ง
                    if (CheckValuesInDGV() == "1")
                    {
                        return;
                    }

                    FormShare.InputData _inputdata = new FormShare.InputData("0",
                         RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                         "จำนวนที่ต้องการสั่ง", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    {
                        pInputData = String.Format("{0:0.00}", Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString()))
                    };

                    if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                    {

                        //ในกรณีที่บันทึกแล้ว
                        String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                                UPDATE	SHOP_MNPD_DT
                                SET     QTYORDER = '" + _inputdata.pInputData + @"',DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                        WHOUPD = '" + SystemClass.SystemUserID_M + @"',WHOUPDNAME = '" + SystemClass.SystemUserName + @"' 
                                WHERE   DocNo = '" + radLabel_Docno.Text + @"' 
                                        AND ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ")
                        );
                        if (T != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(T);
                            return;
                        }

                        RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value = String.Format("{0:0.00}", Convert.ToDouble(_inputdata.pInputData));
                        dt_PO.AcceptChanges();
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                    }
                    else
                    {
                        radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                    }
                    break;
                default:
                    break;
            }
        }
        //Rows and Font
        private void RadGridView_Show_ViewCellFormatting_1(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        //กรณีที่ต้องการยเลิกการสั่งทั้งรายการ
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    if (CheckValuesInDGV() == "1")
                    {
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete("ไม่สั่งรายการ " +
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" +
                        RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString() +
                         " จำนวน " + RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString() +
                         "   " + RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                        == DialogResult.No)
                    {
                        return;
                    }

                    string T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                            UPDATE  SHOP_MNPD_DT
                            SET     QTYORDER = '0',ITEM_STA = '3',DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '" + SystemClass.SystemUserID_M + @"' ,WHOUPDNAME = '" + SystemClass.SystemUserName + @"'  
                            WHERE   DOCNO = '" + radLabel_Docno.Text + @"' 
                                    AND ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ")
                                   );
                    if (T != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        return;
                    }


                    RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value = String.Format("{0:0.00}", 0);
                    dt_PO.AcceptChanges();
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;
                default:
                    break;
            }
        }
        //Save Data HD
        void SaveDataHD(string sID, string sDim, string sBarcode, string sSpc_Name,
            double pInputData, string sUnitID, double sFactor, double price, string sDptID, string pathImage, string invent, string venderID)
        {

            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNPD", "-", "MNPD", "1");
            string bch = radDropDownList_Bch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(radDropDownList_Bch.SelectedValue.ToString());
            ArrayList sqlIn = new ArrayList
            {
                 string.Format(@"
                    INSERT INTO SHOP_MNPD_HD
	                        (DOCNO,BRANCH_ID,BRANCH_NAME,
	                            Remark,WHOINS,WHOINSNAME,DPTID,OPENBY) 
                    VALUES ('" + maxDocno + @"','" +bch + @"','" + bchName + @"',
                    '" + radTextBox_rmk.Text  + @"','" + SystemClass.SystemUserID_M + @"','" + SystemClass.SystemUserName+ @"',
                    '" + SystemClass.SystemBranchID + @"','" + _pOpenBy + @"') ")
            };

            sqlIn.Add(string.Format(@"
                INSERT INTO SHOP_MNPD_DT
                        (DOCNO,LINENUM,ITEMID,INVENTDIMID,ITEMBARCODE,SPC_NAME,QTYORDER,PRICE,UNITID,FACTOR,WHOINS,WHOINSNAME,DPTCODE,PATHIMAGE,INVENT,VENDERID) 
                    VALUES ('" + maxDocno + @"','1',
                       '" + sID + @"',
                       '" + sDim + @"',
                       '" + sBarcode + @"',
                       '" + sSpc_Name.Replace("'", "").Replace("{", " ").Replace("}", " ") + @"',
                       '" + Convert.ToDouble(pInputData) + @"',
                       '" + Convert.ToDouble(price) + @"',
                       '" + sUnitID + @"',
                       '" + Convert.ToDouble(sFactor) + @"',
                       '" + SystemClass.SystemUserID_M + @"','" + SystemClass.SystemUserName + @"',
                       '" + sDptID + @"','" + pathImage + @"','" + invent + @"','" + venderID + @"'
                       ) "));


            string T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                pApv = "0";// pSave = "1";
                RadButton_Apv.Enabled = true;
                RadButton_Cancel.Enabled = true;

                dt_PO.Rows.Add(sID, sDim, sBarcode,
                       sSpc_Name,
                       string.Format("{0:0.00}", Convert.ToDouble(pInputData)),
                       sUnitID,
                       string.Format("{0:0.00}", Convert.ToDouble(sFactor))
                       );
                RadGridView_Show.DataSource = dt_PO;
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                radTextBox_Barcode.Focus(); return;
            }
        }
        //Save Data DT
        void SaveDataDT(string sID, string sDim, string sBarcode, string sSpc_Name,
            double pInputData, string sUnitID, double sFactor, double price, string sDptID, string pathImage, string invent, string venderID)
        {

            string T = ConnectionClass.ExecuteSQL_Main(string.Format(@"
                   INSERT INTO SHOP_MNPD_DT
                        (DOCNO,LINENUM,ITEMID,INVENTDIMID,ITEMBARCODE,SPC_NAME,QTYORDER,PRICE,UNITID,FACTOR,WHOINS,WHOINSNAME,DPTCODE,PATHIMAGE,INVENT,VENDERID) 
                    VALUES ('" + radLabel_Docno.Text + @"','" + (RadGridView_Show.Rows.Count + 1) + @"',
                       '" + sID + @"',
                       '" + sDim + @"',
                       '" + sBarcode + @"',
                       '" + sSpc_Name.Replace("'", "").Replace("{", " ").Replace("}", " ") + @"',
                       '" + Convert.ToDouble(pInputData) + @"',
                       '" + Convert.ToDouble(price) + @"',
                       '" + sUnitID + @"',
                       '" + Convert.ToDouble(sFactor) + @"',
                       '" + SystemClass.SystemUserID_M + @"' ,'" + SystemClass.SystemUserName + @"',
                       '" + sDptID + @"','" + pathImage + @"','" + invent + @"','" + venderID + @"'
                       ) "));



            if (T == "")
            {
                dt_PO.Rows.Add(sID, sDim, sBarcode,
                      sSpc_Name,
                      string.Format("{0:0.00}", Convert.ToDouble(pInputData)),
                      sUnitID,
                      string.Format("{0:0.00}", Convert.ToDouble(sFactor)));
                RadGridView_Show.DataSource = dt_PO;
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                radTextBox_Barcode.Focus(); return;
            }
        }
        //Export Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายการสั่งสินค้าลูกค้าของทั่วไป [MNPD] " + radLabel_Docno.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Select Change
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_Barcode.Focus();
        }

        private void RadButton_FindCst_Click(object sender, EventArgs e)
        {
            ShowDataDGV_Customer _showCst = new ShowDataDGV_Customer("");
            if (_showCst.ShowDialog(this) == DialogResult.Yes)
            {
                this.cust = _showCst.cust;
                radTextBox_CstID.Text = this.cust.Customer_ACCOUNTNUM;
                radTextBox_CstName.Text = this.cust.Customer_NAME;
                radTextBox_Tel.Text = this.cust.Customer_PHONE;
                radTextBox_rmk.Focus();
            }
        }

        private void RadDropDownList_Bch_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_Barcode.SelectAll();
            radTextBox_Barcode.Focus();
        }
        //print
        void PrintData()
        {
            PrintDialog printDlg = new PrintDialog();
            if (printDlg.ShowDialog(this) == DialogResult.OK)
            {
                PrintDocument.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDlg.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument.PrinterSettings = printDlg.PrinterSettings;
                PrintDocument.Print();
            }
        }
        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = radLabel_Docno.Text;
            Bitmap barcodeInBitmap0 = new Bitmap(barcode.drawBarcode());
            int countPrt = 1;
            int Y = 0;

            string bch = radDropDownList_Bch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(radDropDownList_Bch.SelectedValue.ToString());

            e.Graphics.DrawString("พิมพ์ครั้งที่ " + countPrt.ToString() + ".", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("ออเดอร์ลูกค้า [ของทั่วไป].", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(bch + "-" + bchName, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap0, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                Y += 20;
                e.Graphics.DrawString((i + 1).ToString() + ".(" + RadGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString() + " " +
                                  RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + ")   " +
                                  RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(" " + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("หมายเหตุ " + radTextBox_rmk.Text, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID_M + " " + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ลูกค้า : " + "(" + radTextBox_Tel.Text + @")  รหัส " + radTextBox_CstID.Text, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString(radTextBox_CstName.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับสินค้า__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;

            string upStr = string.Format(@"
                        UPDATE SHOP_MNPD_HD  SET COUNT_PRINT = '" + countPrt + @"' 
                        WHERE   DOCNO = '" + radLabel_Docno.Text + "' ");
            ConnectionClass.ExecuteSQL_Main(upStr);

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}

