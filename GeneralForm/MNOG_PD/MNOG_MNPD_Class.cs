﻿
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.MNOG_PD
{
    class MNOG_MNPD_Class
    {
        //ค้นหาการสั่งออเดอร์ลูกค้าทั้งหมด
        public static DataTable FindMNOG_DetailByDate(string pDate1, string pDate2, string pCon)
        {
            //string sql = $@"
            //        SELECT	SHOP_MNOG_HD.DOCNO AS DocNo,DOCTIME AS MNPOTime,CONVERT(VARCHAR,DOCDATE,23)+' ' +DOCTIME AS MNPODate,BRANCH_ID AS MNPOBranch,BRANCH_NAME,
            //         STAPRCDOC AS  MNPOStaApvDoc,STAPRCDOC AS MNPOStaPrcDoc, SHOP_MNOG_HD.WHOINS + '-' + SHOP_MNOG_HD.WHOINSNAME AS SPC_NAME,SHOP_MNOG_HD.REMARK AS REMARK,
            //          '('+ CUST_TEL  + ') ' + CUST_NAME + ' ' + CUST_ID as CstDetail,CONVERT(VARCHAR,RECIVE_DATE,23) AS RECIVE_DATE   ,
            //                CUST_ID AS CstId,CUST_TEL AS  CstTel,CUST_NAME AS  CstName,
            //          LINENUM AS MNPOSeqNo,ITEMBARCODE AS MNPOBarcode,SPC_ITEMNAME AS MNPOName,QTYORDER AS MNPOQtyOrder,UNITID AS MNPOUnitID, 
            //          CASE ISNULL(RECIVE_STA,'0') WHEN '2' THEN '1' ELSE ISNULL(RECIVE_STA,'0') END AS RECIVE_STA ,ISNULL(RECIVE_RMK,'') AS RECIVE_RMK,
            //          NUMID AS NUM,NUMDESC,ISNULL(PROCESS_ORDER,'') AS MNPOAX ,
            //                ISNULL(RECIVE_STA,'0') AS STAA ,
            //                ISNULL(CUSTRECIVE_STA,'0')  AS CstSTA ,OPENBY
            //        FROM	SHOP_MNOG_HD WITH (NOLOCK) 
            //          INNER JOIN SHOP_MNOG_DT WITH (NOLOCK)  ON SHOP_MNOG_HD.DOCNO = SHOP_MNOG_DT.DOCNO 
            //        WHERE	STADOC = '1'  AND QTYORDER > 0 
            //          AND CONVERT(VARCHAR,DOCDATE,23) between '{pDate1}'  AND '{pDate2}'
            //                {pCon}
            //        ORDER BY SHOP_MNOG_HD.DOCNO DESC ";

            string sql = $@"
                    SELECT	SHOP_MNOG_HD.DOCNO AS DocNo,DOCTIME AS MNPOTime,CONVERT(VARCHAR,DOCDATE,23)+' ' +DOCTIME AS MNPODate,BRANCH_ID AS MNPOBranch,BRANCH_NAME,
		                   STAPRCDOC AS  MNPOStaApvDoc,STAPRCDOC AS MNPOStaPrcDoc, SHOP_MNOG_HD.WHOINS + '-' + SHOP_MNOG_HD.WHOINSNAME AS SPC_NAME,SHOP_MNOG_HD.REMARK AS REMARK,
		                    '('+ CUST_TEL  + ') ' + CUST_NAME + ' ' + CUST_ID as CstDetail,CONVERT(VARCHAR,RECIVE_DATE,23) AS RECIVE_DATE   ,
                            CUST_ID AS CstId,CUST_TEL AS  CstTel,CUST_NAME AS  CstName,
		                    LINENUM AS MNPOSeqNo,ITEMBARCODE AS MNPOBarcode,SPC_ITEMNAME AS MNPOName,QTYORDER AS MNPOQtyOrder,UNITID AS MNPOUnitID, 
		                    CASE ISNULL(RECIVE_STA,'0') WHEN '2' THEN '1' ELSE ISNULL(RECIVE_STA,'0') END AS RECIVE_STA ,ISNULL(RECIVE_RMK,'') AS RECIVE_RMK,
		                    NUMID AS NUM,NUMDESC,ISNULL(PROCESS_ORDER,'') AS MNPOAX ,
                            ISNULL(RECIVE_STA,'0') AS STAA ,
                            ISNULL(CUSTRECIVE_STA,'0')  AS CstSTA ,OPENBY,
                            CONVERT(NVARCHAR,ISNULL(MAXDATEIN,''),23) AS MAXDATEIN
                    FROM	SHOP_MNOG_HD WITH (NOLOCK) 
		                    INNER JOIN SHOP_MNOG_DT WITH (NOLOCK)  ON SHOP_MNOG_HD.DOCNO = SHOP_MNOG_DT.DOCNO 
                            LEFT JOIN (
								SELECT  CustID,MAX(DATEIN) AS MAXDATEIN
								FROM	SHOP_CUSTOMER_TELORDER WITH (NOLOCK)
								GROUP BY CustID
							)TMP_Cst ON  SHOP_MNOG_HD.CUST_ID = TMP_Cst.CustID
                    WHERE	STADOC = '1'  AND QTYORDER > 0 
		                    AND CONVERT(VARCHAR,DOCDATE,23) between '{pDate1}'  AND '{pDate2}'
                            {pCon}
                    ORDER BY SHOP_MNOG_HD.DOCNO DESC ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาการสั่งออเดอร์ลูกค้าทั้งหมด
        public static DataTable FindMNPD_DetailByDate(string pDate1, string pDate2)
        {
            string sql = $@"
            SELECT	IIF(TYPEPRODUCT='2',IIF(ISNULL(ORDERSTATUS,'500')='500','จัดระบบเก่า',SHOW_NAME),'จัดระบบเก่า') AS SHOW_NAME,TMP.* FROM	(
                  SELECT	SHOP_MNPD_HD.DocNo,DocTime ,CONVERT(VARCHAR,DocDate ,23) + ' ' + DocTime AS DOCDATE,
		                    BRANCH_ID AS Branch,BRANCH_NAME AS BranchName,TYPEPRODUCT, 
		                    CASE TYPEPRODUCT WHEN '2' THEN 'ลูกค้ารอรับสินค้า' ELSE 'ลูกค้ารับสินค้าเลย' END AS TYPEORDER,
		                    CASE TYPEPRODUCT WHEN '2' THEN STA_SHOP ELSE '1' END AS CstSTA,
		                    SHOP_MNPD_HD.WHOINS + '-' + ISNULL(SHOP_MNPD_HD.WHOINSNAME,'') AS WhoShopName, 
		                    CUST_ID AS CstID ,CUST_NAME AS Customer ,CUST_TEL AS Tel,
		                    ITEMBARCODE AS Barcode,SPC_NAME AS ItemName,QTYORDER AS Qty,UnitID ,
		                    SHOP_MNPD_DT.REMARK,StaPrcDoc ,LINENUM AS MNPOSeqNo ,OPENBY,
                            CONVERT(NVARCHAR,ISNULL(MAXDATEIN,''),23) AS MAXDATEIN,SHOP_MNPD_DT.DOCNO + '_' + CONVERT(VARCHAR,SHOP_MNPD_DT.LINENUM) AS ID,SHOP_MNPD_DT.DPTCODE
                    FROM	SHOP_MNPD_HD WITH (NOLOCK) 
		                    INNER JOIN SHOP_MNPD_DT WITH (NOLOCK) ON SHOP_MNPD_HD.DocNo = SHOP_MNPD_DT.DocNo
                            LEFT JOIN (
								SELECT  CustID,MAX(DATEIN) AS MAXDATEIN
								FROM	SHOP_CUSTOMER_TELORDER WITH (NOLOCK)
								GROUP BY CustID
							)TMP_Cst ON  SHOP_MNPD_HD.CUST_ID = TMP_Cst.CustID
                    WHERE	StaDoc = '1' AND StaPrcDoc = '1' 
		                    AND DOCDate Between '{pDate1}' AND '{pDate2}'
            )TMP 
            LEFT OUTER JOIN SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON TMP.ID = SHOP_WEB_PDTTRANSORDER.TRANSID
		    LEFT OUTER	JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH(NOLOCK) ON SHOP_WEB_PDTTRANSORDER.ORDERSTATUS = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND  TYPE_CONFIG = '54' AND STA = '1'
            ORDER BY 	 Branch,DocNo 
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหาเอกสาร MNPD
        public static DataTable FindDetail_MNPD(string pDocno)
        {

            string sqlSelect = string.Format(@"
                 SELECT	SHOP_MNPD_HD.DocNo,BRANCH_ID AS BRANCH,
                        CASE StaDoc WHEN '3' THEN '0' ELSE '1' END AS STADOC,StaPrcDoc AS STAPRCDOC,
		                LINENUM AS SeqNo,ITEMID,INVENTDIMID AS INVENTDIMID,ITEMBARCODE,
		                SPC_NAME AS SPC_ITEMNAME,FORMAT(CONVERT(float,ISNULL(QTYORDER,'0')), 'N') AS QtyOrder,
                        UnitID AS UNITID,Factor AS QTY,CUST_TEL AS  CstTel,CUST_NAME AS CstName,CUST_ID AS CstId,SHOP_MNPD_HD.Remark
                FROM	SHOP_MNPD_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNPD_DT WITH (NOLOCK) ON SHOP_MNPD_HD.DocNo = SHOP_MNPD_DT.DocNo
                WHERE	SHOP_MNPD_HD.DocNo = '" + pDocno + @"'
                ORDER BY LINENUM
            ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาเอกสาร MNOG
        public static DataTable FindDetail_MNOG(string pDocno)
        {

            string sqlSelect = string.Format(@"
                SELECT	SHOP_MNOG_HD.DOCNO AS MNPODocNo,BRANCH_ID AS BRANCH,
                        CASE STADOC WHEN '3' THEN '0' ELSE '1' END AS STADOC,STAPRCDOC,STADOC AS ApvDoc,
		                LINENUM AS MNPOSeqNo,ITEMID,INVENTDIMID,ITEMBARCODE,
		                SPC_ITEMNAME,FORMAT(CONVERT(float,ISNULL(QTYORDER,'0')), 'N') AS QtyOrder,
                        UNITID,FACTOR AS QTY,CUST_TEL AS CstTel,CUST_NAME AS CstName,CUST_ID AS CstId,SHOP_MNOG_HD.REMARK AS MNPORemark,RECIVE_DATE ,NUMID,NUMDesc
                FROM	SHOP_MNOG_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNOG_DT WITH (NOLOCK) ON SHOP_MNOG_HD.DOCNO = SHOP_MNOG_DT.DOCNO
                WHERE	SHOP_MNOG_HD.DOCNO = '" + pDocno + @"'
                ORDER BY MNPOSeqNo
            ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาเอกสาร MNPD
        public static DataTable FindDetailHD_MNPD(string pDate1, string pDate2, string pSta, string pBch)
        {

            string sqlSelect = string.Format(@"
                 SELECT	DOCNO AS DOCNO,CONVERT(VARCHAR,DocDate,23) + ' ' + DocTime AS DOCDATE,
		                BRANCH_ID AS MNPOBranch ,BRANCH_NAME AS MNPOBranchName,WHOINS+' ' + ISNULL(WHOINSNAME,'') AS WHOIDINS,
		                CASE WHEN StaDoc = '3' THEN '1' ELSE '0' END AS STADOC,StaPrcDoc AS STAPRCDOC,
		                Remark  AS RMK,
		                CASE StaPrcDoc WHEN '1' THEN WHOUPD + ' '+ ISNULL(WHOUPDNAME,'') 
		                ELSE CASE WHEN StaDoc = '3' THEN WHOUPD + ' '+ ISNULL(WHOUPDNAME,'')  ELSE WHOINS+' ' + ISNULL(WHOINSNAME,'') END  END 
		                AS WHOIDUPD,OPENBY, 
                        '('+ CUST_TEL  + ') ' + CUST_NAME + ' ' + CUST_ID as CstDetail,COUNT_PRINT AS CountPrt ,
                        CASE TYPEPRODUCT WHEN '1' THEN 'ลูกค้ารับสินค้าเลย' ELSE 'ลูกค้ารอรับสินค้า' END AS RECIVE_DATE
                FROM	SHOP_MNPD_HD WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,DOCDATE,23)  BETWEEN '" + pDate1 + @"' 
                        AND '" + pDate2 + @"'   " + pSta + pBch + @"
                ORDER BY DOCNO DESC 
            ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาเอกสาร MNOG
        public static DataTable FindDetailHD_MNOG(string pDate1, string pDate2, string pSta, string pBch)
        {

            string sqlSelect = string.Format(@"
                SELECT	DOCNO,CONVERT(VARCHAR,DOCDATE,23) + ' ' + DOCTIME AS DOCDATE,
		               BRANCH_ID AS MNPOBranch,BRANCH_NAME AS MNPOBranchName,WHOINS+' ' + ISNULL(WHOINSNAME,'') AS WHOIDINS,
		                CASE WHEN STADOC = '3' THEN '1' ELSE '0' END AS STADOC,STAPRCDOC,
		                REMARK  AS RMK,
		                CASE STAPRCDOC WHEN '1' THEN CASE ISNULL(WHOUPDNAME,'') WHEN '' THEN WHOINS + ' '+ ISNULL(WHOINSNAME,'') ELSE WHOUPD + ' '+ ISNULL(WHOUPDNAME,'') END  
		                ELSE CASE WHEN STADOC = '3' THEN WHOUPD + ' '+ ISNULL(WHOUPDNAME,'')  ELSE WHOINS+' ' + ISNULL(WHOINSNAME,'') END  END 
		                AS WHOIDUPD,OPENBY,CONVERT(VARCHAR,RECIVE_DATE,23) AS RECIVE_DATE ,
                        '('+ CUST_TEL  + ') ' + CUST_NAME + ' ' + CUST_ID as CstDetail,COUNT_PRINT AS CountPrt 
                FROM	SHOP_MNOG_HD WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,DOCDATE,23) BETWEEN '" + pDate1 + @"' 
                        AND '" + pDate2 + @"'   " + pSta + pBch + @"
                ORDER BY DOCNO DESC
            ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาเอกสาร Detail MNPD
        public static DataTable FindDetailDT_MNPD(string pDocno)
        {

            string sqlSelect = string.Format(@"
                SELECT	ITEMBARCODE,SPC_NAME AS SPC_ITEMNAME,QtyOrder,
		                UnitID AS UNITID
                FROM	SHOP_MNPD_DT WITH (NOLOCK)
                WHERE	DOCNO = '" + pDocno + @"' AND QtyOrder > 0  AND ITEM_STA = '1'
                ORDER BY LINENUM
                   ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาเอกสาร Detail MNOG
        public static DataTable FindDetailDT_MNOG(string pDocno)
        {

            string sqlSelect = string.Format(@"
                SELECT	ITEMBARCODE,SPC_ITEMNAME,QtyOrder,UNITID,NUMID,NUMDesc 
                FROM	SHOP_MNOG_DT WITH (NOLOCK)
                WHERE	DOCNO = '" + pDocno + @"' AND QTYORDER > 0 
                ORDER BY LINENUM
                   ");
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //Update MNPD/MNOG 
        public static string Update_OrderCust(string pType, string docno, string itembarcode, string rmk)//0 PD 1 OG
        {
            string sql = "";
            switch (pType)
            {
                case "0":
                    sql = $@"
                            UPDATE SHOP_MNPD_DT
                            SET     STA_SHOP = '1',
                                    REMARK = ISNULL(REMARK,'') + '{rmk}'+CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']'  ,
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '{SystemClass.SystemUserID}',WHOUPDNAME = '{SystemClass.SystemUserName}'
                            WHERE   DOCNO = '{docno}' 
                                    AND ITEMBARCODE = '{itembarcode}' ";
                    break;
                case "1":
                    sql = $@"
                            UPDATE  SHOP_MNOG_DT
                            SET     CUSTRECIVE_STA = '1',
                                    REMARK = ISNULL(REMARK,'') + '{rmk}'+CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']'  ,
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '{SystemClass.SystemUserID}',WHOUPDNAME = '{SystemClass.SystemUserName}'
                            WHERE   DOCNO = '{docno}' 
                                    AND ITEMBARCODE = '{itembarcode}' ";
                    break;
                default:
                    break;
            }
            return sql;
        }
    }
}
