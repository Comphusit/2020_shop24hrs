﻿//CheckOK
using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.MA
{
    class MAReceiveClass
    {
        //check เลขที่เอกสารซ้ำ
        public static DataTable CheckMAReceiveBill(string Bill)
        {
            string sql = $@" 
            DECLARE @Docno NVARCHAR(50) = '{Bill}';

            SELECT  Docno, EmpIDReceive, EmplNameReceive AS SPC_NAME ,Price,PrintBill,BranchBill,RDateIn,
                    BranchBill,BranchName,RemarkBill,BillTrans ,RemarkVender,VenderName 
            FROM    Shop_ReciveDocument with(nolock) 
            where   Docno = @Docno
            ORDER BY RDatein desc ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหาเอกสารที่รับไปแล้วหรือยัง
        public static DataTable GetBillRecevie(string Branch, string Date, string vendor, string Docno)//typeBill = V และ MP
        {
            string docno = "";
            string typeBill = "V";
            if (vendor == "MP") { typeBill = "MP"; docno = $@" AND Docno = '{Docno}' "; }

            string sql = $@"
                DECLARE @bchID NVARCHAR(10) = '{Branch}';
                DECLARE @date NVARCHAR(12) = '{Date}';
                DECLARE @venID NVARCHAR(10) = '{vendor}';
                DECLARE @typeBill NVARCHAR(10) = '{typeBill}';

                SELECT  TOP 1 Docno,EmpIDReceive,EmplNameReceive AS SPC_NAME  , 
                        cast(Price as decimal(10,2)) as Price,PrintBill, 
                        BranchBill,BranchName,
                        RDateIn,RemarkBill,BillTrans,RemarkVender,VenderName as vendername,Shop_ReciveDocument.RECID
                FROM    Shop_ReciveDocument with (Nolock)
                WHERE   BranchBill = @bchID AND TypeBill =  @typeBill
                        AND RDateIn = @date
                        AND RemarkVender =  @venID 
                        {docno}
                ORDER BY  PrintBill DESC ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        //Insert ReciveDoc
        public static string Save_ReciveDocument(string pType0Insert1Update2Edit, Var_ReciveDocument var_ReciveDocument)
        {
            string sql = "";
            switch (pType0Insert1Update2Edit)
            {
                case "0":
                    sql = $@"
                    insert into shop_ReciveDocument(Docno, EmpIDReceive,EmplNameReceive, PrintBill,  
                                BranchBill,BranchName, RemarkBill, 
                                TypeBill, RemarkVender,VenderName,RDateIn, 
                                EmpIDAdmin, RemarkAdmin,
                                RWhoIn,Rtype, 
                                Price, BillTrans, RECID)
                    values(
                                '{var_ReciveDocument.Docno}','{var_ReciveDocument.EmpIDReceive}','{var_ReciveDocument.EmplNameReceive}','{var_ReciveDocument.PrintBill}',
                                '{var_ReciveDocument.BranchBill}','{var_ReciveDocument.BranchName}','{var_ReciveDocument.RemarkBill}',
                                '{var_ReciveDocument.TypeBill}','{var_ReciveDocument.RemarkVender}','{var_ReciveDocument.VenderName}','{var_ReciveDocument.RDateIn}',
                                '{var_ReciveDocument.EmpIDAdmin}','{var_ReciveDocument.RemarkAdmin}',
                                '{var_ReciveDocument.RWhoIn}','{var_ReciveDocument.Rtype}',
                                '{var_ReciveDocument.Price}','{var_ReciveDocument.BillTrans}','{var_ReciveDocument.RECID}')";
                    break;
                case "1":
                    sql = $@"
                    UPDATE      shop_ReciveDocument
                    SET         Docno = '{var_ReciveDocument.Docno}',EmpIDReceive = '{var_ReciveDocument.EmpIDReceive}',
                                EmplNameReceive = '{var_ReciveDocument.EmplNameReceive}',
                                EmpIDAdmin = '{var_ReciveDocument.EmpIDAdmin}',RemarkAdmin = '{var_ReciveDocument.RemarkAdmin}',
                                PrintBill = '{var_ReciveDocument.PrintBill}',RemarkBill = '{var_ReciveDocument.RemarkBill}',Price = '{var_ReciveDocument.Price}',
                                RDateUp = convert(varchar, getdate(), 23),RTimeUp = convert(varchar, getdate(), 24),RWhoUp = '{var_ReciveDocument.RWhoIn}'
                    WHERE       RECID = '{var_ReciveDocument.RECID}' ";
                    break;
                case "2":
                    sql = $@"
                    insert into Shop_ReciveDocumentEdit 
                                (Docno, BranchBill, EmpIDReceive, 
                                Price, RemarkBill,
                                Edit, EditEmpIDReceive, EdirPrice, EditRemark,
                                TypeBill, BillTrans, Type, Remark,WhoIn)
                    values      ('{var_ReciveDocument.Docno}', '{var_ReciveDocument.BranchBill}', '{var_ReciveDocument.EmpIDReceive}', 
                                '{var_ReciveDocument.Price}', '{var_ReciveDocument.RemarkBill}', 
                                '{var_ReciveDocument.PrintBill}','{var_ReciveDocument.EmpIDAdmin}', '{var_ReciveDocument.Price}', '{var_ReciveDocument.RemarkBill}',
                                '{var_ReciveDocument.TypeBill}', '{var_ReciveDocument.BillTrans}', '', '{var_ReciveDocument.Docno}','{var_ReciveDocument.RWhoIn}')";
                    break;
                default:
                    break;
            }
            return sql;
        }
    }

    public class Var_ReciveDocument
    {
        public string Docno { get; set; } = "";
        public string EmpIDReceive { get; set; } = "";
        public string EmplNameReceive { get; set; } = "";
        public int PrintBill { get; set; } = 0;
        public string BranchBill { get; set; } = "";
        public string BranchName { get; set; } = "";
        public string RemarkBill { get; set; } = "";
        public string TypeBill { get; set; } = "";
        public string RemarkVender { get; set; } = "";
        public string VenderName { get; set; } = "";
        public string RDateIn { get; set; } = "";
        public string EmpIDAdmin { get; set; } = "";
        public string RemarkAdmin { get; set; } = "";
        public string RWhoIn { get; set; } = "";
        public string Rtype { get; set; } = "";
        public double Price { get; set; } = 0;
        public string BillTrans { get; set; } = "";
        public string RECID { get; set; } = "";
    }
}
