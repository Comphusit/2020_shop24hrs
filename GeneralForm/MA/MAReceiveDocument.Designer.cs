﻿namespace PC_Shop24Hrs.GeneralForm.MA
{
    partial class MAReceiveDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MAReceiveDocument));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel_Center = new System.Windows.Forms.Panel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_AX = new Telerik.WinControls.UI.RadButton();
            this.radLabel_CenterHeader = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_CanterCencel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_CenterEdit = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_CenterDocno = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_CenterDocno = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CenterRemark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_CenterName = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CenterEmpl = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CenterMoney = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_CenterRemark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CenterMoney = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CenterEmpl = new Telerik.WinControls.UI.RadLabel();
            this.panel_Bch = new System.Windows.Forms.Panel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchEnter = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchBath = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchHeader = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_BranchCancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_BranchSave = new Telerik.WinControls.UI.RadButton();
            this.RadTextBox_BranchDocno = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_BranchDocno = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_BranchRemark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_BranchEmpl = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_BranchMoney = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_BranchRemark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchMoney = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchEmpl = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_VenderName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchName = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Vender = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Branch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_MA = new Telerik.WinControls.UI.RadLabel();
            this.PrintDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.PrintDocument2 = new System.Drawing.Printing.PrintDocument();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel_Center.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_AX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_CanterCencel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_CenterEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterDocno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterDocno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterEmpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterEmpl)).BeginInit();
            this.panel_Bch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchBath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_BranchCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_BranchSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchDocno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchDocno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchEmpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchRemark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchEmpl)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_VenderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(910, 632);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 615);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(904, 14);
            this.radLabel_Detail.TabIndex = 55;
            this.radLabel_Detail.Text = "<html>MA สีแดง จัดซื้อทำบิลเรียบร้อยแล้ว</html>";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.01639F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.98361F));
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 103);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(904, 506);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RadGridView_Show);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(527, 500);
            this.panel2.TabIndex = 1;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            this.RadGridView_Show.Size = new System.Drawing.Size(527, 500);
            this.RadGridView_Show.TabIndex = 55;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.DefaultValuesNeeded += new Telerik.WinControls.UI.GridViewRowEventHandler(this.RadGridView_Show_DefaultValuesNeeded);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.panel_Center, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel_Bch, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(536, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(365, 500);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // panel_Center
            // 
            this.panel_Center.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Center.Controls.Add(this.radLabel3);
            this.panel_Center.Controls.Add(this.radLabel2);
            this.panel_Center.Controls.Add(this.radButton_AX);
            this.panel_Center.Controls.Add(this.radLabel_CenterHeader);
            this.panel_Center.Controls.Add(this.RadButton_CanterCencel);
            this.panel_Center.Controls.Add(this.RadButton_CenterEdit);
            this.panel_Center.Controls.Add(this.radTextBox_CenterDocno);
            this.panel_Center.Controls.Add(this.radLabel_CenterDocno);
            this.panel_Center.Controls.Add(this.radTextBox_CenterRemark);
            this.panel_Center.Controls.Add(this.radLabel_CenterName);
            this.panel_Center.Controls.Add(this.radTextBox_CenterEmpl);
            this.panel_Center.Controls.Add(this.radTextBox_CenterMoney);
            this.panel_Center.Controls.Add(this.radLabel_CenterRemark);
            this.panel_Center.Controls.Add(this.radLabel_CenterMoney);
            this.panel_Center.Controls.Add(this.radLabel_CenterEmpl);
            this.panel_Center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Center.Location = new System.Drawing.Point(3, 253);
            this.panel_Center.Name = "panel_Center";
            this.panel_Center.Size = new System.Drawing.Size(359, 244);
            this.panel_Center.TabIndex = 0;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(287, 67);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(55, 19);
            this.radLabel3.TabIndex = 100;
            this.radLabel3.Text = "[Enter]";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(287, 32);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(55, 19);
            this.radLabel2.TabIndex = 99;
            this.radLabel2.Text = "[Enter]";
            // 
            // radButton_AX
            // 
            this.radButton_AX.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_AX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.radButton_AX.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_AX.Location = new System.Drawing.Point(3, 206);
            this.radButton_AX.Name = "radButton_AX";
            this.radButton_AX.Size = new System.Drawing.Size(88, 32);
            this.radButton_AX.TabIndex = 6;
            this.radButton_AX.Text = "AX";
            this.radButton_AX.ThemeName = "Fluent";
            this.radButton_AX.Click += new System.EventHandler(this.RadButton_AX_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_AX.GetChildAt(0))).Text = "AX";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_AX.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_AX.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_AX.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_AX.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_AX.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_AX.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(190)))), ((int)(((byte)(197)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_AX.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_CenterHeader
            // 
            this.radLabel_CenterHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CenterHeader.Location = new System.Drawing.Point(2, 3);
            this.radLabel_CenterHeader.Name = "radLabel_CenterHeader";
            this.radLabel_CenterHeader.Size = new System.Drawing.Size(86, 19);
            this.radLabel_CenterHeader.TabIndex = 98;
            this.radLabel_CenterHeader.Text = "เอกสารแก้ไข";
            // 
            // RadButton_CanterCencel
            // 
            this.RadButton_CanterCencel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_CanterCencel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_CanterCencel.Location = new System.Drawing.Point(234, 206);
            this.RadButton_CanterCencel.Name = "RadButton_CanterCencel";
            this.RadButton_CanterCencel.Size = new System.Drawing.Size(113, 32);
            this.RadButton_CanterCencel.TabIndex = 5;
            this.RadButton_CanterCencel.Text = "ยกเลิก";
            this.RadButton_CanterCencel.ThemeName = "Fluent";
            this.RadButton_CanterCencel.Click += new System.EventHandler(this.RadButton_CanterCencel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_CanterCencel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_CanterCencel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CanterCencel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CanterCencel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CanterCencel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CanterCencel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CanterCencel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_CenterEdit
            // 
            this.RadButton_CenterEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_CenterEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_CenterEdit.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_CenterEdit.Location = new System.Drawing.Point(119, 206);
            this.RadButton_CenterEdit.Name = "RadButton_CenterEdit";
            this.RadButton_CenterEdit.Size = new System.Drawing.Size(109, 32);
            this.RadButton_CenterEdit.TabIndex = 4;
            this.RadButton_CenterEdit.Text = "แก้ไข";
            this.RadButton_CenterEdit.ThemeName = "Fluent";
            this.RadButton_CenterEdit.Click += new System.EventHandler(this.RadButton_CenterEdit_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_CenterEdit.GetChildAt(0))).Text = "แก้ไข";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_CenterEdit.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_CenterEdit.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CenterEdit.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CenterEdit.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CenterEdit.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CenterEdit.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_CenterEdit.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_CenterDocno
            // 
            this.radTextBox_CenterDocno.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_CenterDocno.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CenterDocno.Location = new System.Drawing.Point(117, 31);
            this.radTextBox_CenterDocno.Name = "radTextBox_CenterDocno";
            this.radTextBox_CenterDocno.Size = new System.Drawing.Size(164, 21);
            this.radTextBox_CenterDocno.TabIndex = 0;
            this.radTextBox_CenterDocno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CenterDocno_KeyDown);
            // 
            // radLabel_CenterDocno
            // 
            this.radLabel_CenterDocno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CenterDocno.Location = new System.Drawing.Point(26, 33);
            this.radLabel_CenterDocno.Name = "radLabel_CenterDocno";
            this.radLabel_CenterDocno.Size = new System.Drawing.Size(84, 19);
            this.radLabel_CenterDocno.TabIndex = 93;
            this.radLabel_CenterDocno.Text = "เลขที่เอกสาร";
            // 
            // radTextBox_CenterRemark
            // 
            this.radTextBox_CenterRemark.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_CenterRemark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CenterRemark.Location = new System.Drawing.Point(117, 155);
            this.radTextBox_CenterRemark.Multiline = true;
            this.radTextBox_CenterRemark.Name = "radTextBox_CenterRemark";
            // 
            // 
            // 
            this.radTextBox_CenterRemark.RootElement.StretchVertically = true;
            this.radTextBox_CenterRemark.Size = new System.Drawing.Size(229, 47);
            this.radTextBox_CenterRemark.TabIndex = 3;
            // 
            // radLabel_CenterName
            // 
            this.radLabel_CenterName.AutoSize = false;
            this.radLabel_CenterName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CenterName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CenterName.Location = new System.Drawing.Point(117, 126);
            this.radLabel_CenterName.Name = "radLabel_CenterName";
            this.radLabel_CenterName.Size = new System.Drawing.Size(229, 19);
            this.radLabel_CenterName.TabIndex = 97;
            this.radLabel_CenterName.Text = "ชื่อ";
            // 
            // radTextBox_CenterEmpl
            // 
            this.radTextBox_CenterEmpl.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_CenterEmpl.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CenterEmpl.Location = new System.Drawing.Point(117, 97);
            this.radTextBox_CenterEmpl.MaxLength = 7;
            this.radTextBox_CenterEmpl.Name = "radTextBox_CenterEmpl";
            this.radTextBox_CenterEmpl.Size = new System.Drawing.Size(164, 21);
            this.radTextBox_CenterEmpl.TabIndex = 2;
            this.radTextBox_CenterEmpl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CenterEmpl_KeyDown);
            // 
            // radTextBox_CenterMoney
            // 
            this.radTextBox_CenterMoney.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_CenterMoney.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CenterMoney.Location = new System.Drawing.Point(117, 65);
            this.radTextBox_CenterMoney.Name = "radTextBox_CenterMoney";
            this.radTextBox_CenterMoney.Size = new System.Drawing.Size(164, 21);
            this.radTextBox_CenterMoney.TabIndex = 1;
            this.radTextBox_CenterMoney.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_CenterMoney_TextChanging);
            this.radTextBox_CenterMoney.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CenterMoney_KeyDown);
            // 
            // radLabel_CenterRemark
            // 
            this.radLabel_CenterRemark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CenterRemark.Location = new System.Drawing.Point(26, 148);
            this.radLabel_CenterRemark.Name = "radLabel_CenterRemark";
            this.radLabel_CenterRemark.Size = new System.Drawing.Size(65, 19);
            this.radLabel_CenterRemark.TabIndex = 96;
            this.radLabel_CenterRemark.Text = "หมายเหตุ";
            // 
            // radLabel_CenterMoney
            // 
            this.radLabel_CenterMoney.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CenterMoney.Location = new System.Drawing.Point(26, 67);
            this.radLabel_CenterMoney.Name = "radLabel_CenterMoney";
            this.radLabel_CenterMoney.Size = new System.Drawing.Size(54, 19);
            this.radLabel_CenterMoney.TabIndex = 94;
            this.radLabel_CenterMoney.Text = "ยอดเงิน";
            // 
            // radLabel_CenterEmpl
            // 
            this.radLabel_CenterEmpl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CenterEmpl.Location = new System.Drawing.Point(26, 99);
            this.radLabel_CenterEmpl.Name = "radLabel_CenterEmpl";
            this.radLabel_CenterEmpl.Size = new System.Drawing.Size(59, 19);
            this.radLabel_CenterEmpl.TabIndex = 95;
            this.radLabel_CenterEmpl.Text = "พนักงาน";
            // 
            // panel_Bch
            // 
            this.panel_Bch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Bch.Controls.Add(this.radLabel1);
            this.panel_Bch.Controls.Add(this.radLabel_BranchEnter);
            this.panel_Bch.Controls.Add(this.radLabel_BranchBath);
            this.panel_Bch.Controls.Add(this.radLabel_BranchHeader);
            this.panel_Bch.Controls.Add(this.RadButton_BranchCancel);
            this.panel_Bch.Controls.Add(this.RadButton_BranchSave);
            this.panel_Bch.Controls.Add(this.RadTextBox_BranchDocno);
            this.panel_Bch.Controls.Add(this.radLabel_BranchDocno);
            this.panel_Bch.Controls.Add(this.RadTextBox_BranchRemark);
            this.panel_Bch.Controls.Add(this.radLabel_EmplName);
            this.panel_Bch.Controls.Add(this.RadTextBox_BranchEmpl);
            this.panel_Bch.Controls.Add(this.RadTextBox_BranchMoney);
            this.panel_Bch.Controls.Add(this.radLabel_BranchRemark);
            this.panel_Bch.Controls.Add(this.radLabel_BranchMoney);
            this.panel_Bch.Controls.Add(this.radLabel_BranchEmpl);
            this.panel_Bch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Bch.Location = new System.Drawing.Point(3, 3);
            this.panel_Bch.Name = "panel_Bch";
            this.panel_Bch.Size = new System.Drawing.Size(359, 244);
            this.panel_Bch.TabIndex = 1;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(288, 116);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(55, 19);
            this.radLabel1.TabIndex = 88;
            this.radLabel1.Text = "[Enter]";
            // 
            // radLabel_BranchEnter
            // 
            this.radLabel_BranchEnter.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchEnter.Location = new System.Drawing.Point(288, 41);
            this.radLabel_BranchEnter.Name = "radLabel_BranchEnter";
            this.radLabel_BranchEnter.Size = new System.Drawing.Size(55, 19);
            this.radLabel_BranchEnter.TabIndex = 82;
            this.radLabel_BranchEnter.Text = "[Enter]";
            // 
            // radLabel_BranchBath
            // 
            this.radLabel_BranchBath.AutoSize = false;
            this.radLabel_BranchBath.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchBath.Location = new System.Drawing.Point(287, 78);
            this.radLabel_BranchBath.Name = "radLabel_BranchBath";
            this.radLabel_BranchBath.Size = new System.Drawing.Size(55, 19);
            this.radLabel_BranchBath.TabIndex = 87;
            this.radLabel_BranchBath.Text = "[Enter]";
            // 
            // radLabel_BranchHeader
            // 
            this.radLabel_BranchHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchHeader.Location = new System.Drawing.Point(3, 6);
            this.radLabel_BranchHeader.Name = "radLabel_BranchHeader";
            this.radLabel_BranchHeader.Size = new System.Drawing.Size(153, 19);
            this.radLabel_BranchHeader.TabIndex = 81;
            this.radLabel_BranchHeader.Text = "เอกสารรับสินค้ามินิมาร์ท";
            // 
            // RadButton_BranchCancel
            // 
            this.RadButton_BranchCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_BranchCancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_BranchCancel.Location = new System.Drawing.Point(234, 190);
            this.RadButton_BranchCancel.Name = "RadButton_BranchCancel";
            this.RadButton_BranchCancel.Size = new System.Drawing.Size(112, 32);
            this.RadButton_BranchCancel.TabIndex = 5;
            this.RadButton_BranchCancel.Text = "ยกเลิก";
            this.RadButton_BranchCancel.ThemeName = "Fluent";
            this.RadButton_BranchCancel.Click += new System.EventHandler(this.RadButton_BranchCancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_BranchCancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_BranchCancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchCancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchCancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchCancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchCancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchCancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_BranchSave
            // 
            this.RadButton_BranchSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_BranchSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_BranchSave.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_BranchSave.Location = new System.Drawing.Point(116, 190);
            this.RadButton_BranchSave.Name = "RadButton_BranchSave";
            this.RadButton_BranchSave.Size = new System.Drawing.Size(112, 32);
            this.RadButton_BranchSave.TabIndex = 3;
            this.RadButton_BranchSave.Text = "บันทึก";
            this.RadButton_BranchSave.ThemeName = "Fluent";
            this.RadButton_BranchSave.Click += new System.EventHandler(this.RadButton_BranchSave_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_BranchSave.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_BranchSave.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_BranchSave.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchSave.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchSave.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchSave.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchSave.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_BranchSave.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadTextBox_BranchDocno
            // 
            this.RadTextBox_BranchDocno.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadTextBox_BranchDocno.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BranchDocno.Location = new System.Drawing.Point(113, 39);
            this.RadTextBox_BranchDocno.Name = "RadTextBox_BranchDocno";
            this.RadTextBox_BranchDocno.Size = new System.Drawing.Size(168, 21);
            this.RadTextBox_BranchDocno.TabIndex = 0;
            this.RadTextBox_BranchDocno.Tag = "เลขที่เอกสาร";
            this.RadTextBox_BranchDocno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BranchDocno_KeyDown);
            // 
            // radLabel_BranchDocno
            // 
            this.radLabel_BranchDocno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchDocno.Location = new System.Drawing.Point(23, 41);
            this.radLabel_BranchDocno.Name = "radLabel_BranchDocno";
            this.radLabel_BranchDocno.Size = new System.Drawing.Size(84, 19);
            this.radLabel_BranchDocno.TabIndex = 81;
            this.radLabel_BranchDocno.Text = "เลขที่เอกสาร";
            // 
            // RadTextBox_BranchRemark
            // 
            this.RadTextBox_BranchRemark.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadTextBox_BranchRemark.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BranchRemark.Location = new System.Drawing.Point(112, 216);
            this.RadTextBox_BranchRemark.Multiline = true;
            this.RadTextBox_BranchRemark.Name = "RadTextBox_BranchRemark";
            // 
            // 
            // 
            this.RadTextBox_BranchRemark.RootElement.StretchVertically = true;
            this.RadTextBox_BranchRemark.Size = new System.Drawing.Size(234, 23);
            this.RadTextBox_BranchRemark.TabIndex = 3;
            this.RadTextBox_BranchRemark.Visible = false;
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.AutoSize = false;
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplName.Location = new System.Drawing.Point(113, 145);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(229, 19);
            this.radLabel_EmplName.TabIndex = 86;
            this.radLabel_EmplName.Text = "ชื่อ";
            // 
            // RadTextBox_BranchEmpl
            // 
            this.RadTextBox_BranchEmpl.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadTextBox_BranchEmpl.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BranchEmpl.Location = new System.Drawing.Point(113, 114);
            this.RadTextBox_BranchEmpl.MaxLength = 7;
            this.RadTextBox_BranchEmpl.Name = "RadTextBox_BranchEmpl";
            this.RadTextBox_BranchEmpl.Size = new System.Drawing.Size(168, 21);
            this.RadTextBox_BranchEmpl.TabIndex = 2;
            this.RadTextBox_BranchEmpl.Tag = "รหัสพนักงาน";
            this.RadTextBox_BranchEmpl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BranchEmpl_KeyDown);
            this.RadTextBox_BranchEmpl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_BranchEmpl_KeyPress);
            // 
            // RadTextBox_BranchMoney
            // 
            this.RadTextBox_BranchMoney.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadTextBox_BranchMoney.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BranchMoney.Location = new System.Drawing.Point(113, 76);
            this.RadTextBox_BranchMoney.Name = "RadTextBox_BranchMoney";
            this.RadTextBox_BranchMoney.Size = new System.Drawing.Size(168, 21);
            this.RadTextBox_BranchMoney.TabIndex = 1;
            this.RadTextBox_BranchMoney.Tag = "จำนวนเงิน";
            this.RadTextBox_BranchMoney.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_BranchMoney_TextChanging);
            this.RadTextBox_BranchMoney.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BranchMoney_KeyDown);
            // 
            // radLabel_BranchRemark
            // 
            this.radLabel_BranchRemark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchRemark.Location = new System.Drawing.Point(22, 218);
            this.radLabel_BranchRemark.Name = "radLabel_BranchRemark";
            this.radLabel_BranchRemark.Size = new System.Drawing.Size(65, 19);
            this.radLabel_BranchRemark.TabIndex = 85;
            this.radLabel_BranchRemark.Text = "หมายเหตุ";
            this.radLabel_BranchRemark.Visible = false;
            // 
            // radLabel_BranchMoney
            // 
            this.radLabel_BranchMoney.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchMoney.Location = new System.Drawing.Point(23, 78);
            this.radLabel_BranchMoney.Name = "radLabel_BranchMoney";
            this.radLabel_BranchMoney.Size = new System.Drawing.Size(54, 19);
            this.radLabel_BranchMoney.TabIndex = 82;
            this.radLabel_BranchMoney.Text = "ยอดเงิน";
            // 
            // radLabel_BranchEmpl
            // 
            this.radLabel_BranchEmpl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchEmpl.Location = new System.Drawing.Point(23, 117);
            this.radLabel_BranchEmpl.Name = "radLabel_BranchEmpl";
            this.radLabel_BranchEmpl.Size = new System.Drawing.Size(59, 19);
            this.radLabel_BranchEmpl.TabIndex = 84;
            this.radLabel_BranchEmpl.Text = "พนักงาน";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_VenderName);
            this.panel1.Controls.Add(this.radLabel_BranchName);
            this.panel1.Controls.Add(this.RadButton_pdt);
            this.panel1.Controls.Add(this.radLabel_Vender);
            this.panel1.Controls.Add(this.radLabel_Branch);
            this.panel1.Controls.Add(this.radLabel_MA);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(904, 94);
            this.panel1.TabIndex = 0;
            // 
            // radLabel_VenderName
            // 
            this.radLabel_VenderName.AutoSize = false;
            this.radLabel_VenderName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_VenderName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_VenderName.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radLabel_VenderName.Location = new System.Drawing.Point(605, 65);
            this.radLabel_VenderName.Name = "radLabel_VenderName";
            this.radLabel_VenderName.Size = new System.Drawing.Size(291, 25);
            this.radLabel_VenderName.TabIndex = 88;
            this.radLabel_VenderName.Text = "-";
            this.radLabel_VenderName.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel_BranchName
            // 
            this.radLabel_BranchName.AutoSize = false;
            this.radLabel_BranchName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BranchName.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radLabel_BranchName.Location = new System.Drawing.Point(604, 34);
            this.radLabel_BranchName.Name = "radLabel_BranchName";
            this.radLabel_BranchName.Size = new System.Drawing.Size(291, 25);
            this.radLabel_BranchName.TabIndex = 87;
            this.radLabel_BranchName.Text = "-";
            this.radLabel_BranchName.ThemeName = "VisualStudio2012Light";
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(3, 4);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 86;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_Vender
            // 
            this.radLabel_Vender.AutoSize = false;
            this.radLabel_Vender.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Vender.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Vender.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radLabel_Vender.Location = new System.Drawing.Point(526, 65);
            this.radLabel_Vender.Name = "radLabel_Vender";
            this.radLabel_Vender.Size = new System.Drawing.Size(73, 25);
            this.radLabel_Vender.TabIndex = 85;
            this.radLabel_Vender.Text = "V005450";
            this.radLabel_Vender.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel_Branch
            // 
            this.radLabel_Branch.AutoSize = false;
            this.radLabel_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Branch.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Branch.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radLabel_Branch.Location = new System.Drawing.Point(525, 34);
            this.radLabel_Branch.Name = "radLabel_Branch";
            this.radLabel_Branch.Size = new System.Drawing.Size(73, 25);
            this.radLabel_Branch.TabIndex = 84;
            this.radLabel_Branch.Text = "-";
            this.radLabel_Branch.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel_MA
            // 
            this.radLabel_MA.AutoSize = false;
            this.radLabel_MA.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_MA.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_MA.Location = new System.Drawing.Point(715, 9);
            this.radLabel_MA.Name = "radLabel_MA";
            this.radLabel_MA.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radLabel_MA.Size = new System.Drawing.Size(171, 19);
            this.radLabel_MA.TabIndex = 79;
            this.radLabel_MA.Text = "MA";
            this.radLabel_MA.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PrintDocument1
            // 
            this.PrintDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // MAReceiveDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 632);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MAReceiveDocument";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รับสินค้าMA";
            this.Load += new System.EventHandler(this.MAReceiveDocument_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel_Center.ResumeLayout(false);
            this.panel_Center.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_AX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_CanterCencel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_CenterEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterDocno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterDocno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterEmpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CenterMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CenterEmpl)).EndInit();
            this.panel_Bch.ResumeLayout(false);
            this.panel_Bch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchBath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_BranchCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_BranchSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchDocno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchDocno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchEmpl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BranchMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchRemark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchEmpl)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_VenderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_MA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Drawing.Printing.PrintDocument PrintDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchRemark;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchEmpl;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchMoney;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel_Center;
        private System.Windows.Forms.Panel panel_Bch;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BranchEmpl;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BranchMoney;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BranchRemark;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BranchDocno;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchDocno;
        protected Telerik.WinControls.UI.RadButton RadButton_BranchCancel;
        protected Telerik.WinControls.UI.RadButton RadButton_BranchSave;
        protected Telerik.WinControls.UI.RadButton RadButton_CanterCencel;
        protected Telerik.WinControls.UI.RadButton RadButton_CenterEdit;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CenterDocno;
        private Telerik.WinControls.UI.RadLabel radLabel_CenterDocno;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CenterRemark;
        private Telerik.WinControls.UI.RadLabel radLabel_CenterName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CenterEmpl;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CenterMoney;
        private Telerik.WinControls.UI.RadLabel radLabel_CenterRemark;
        private Telerik.WinControls.UI.RadLabel radLabel_CenterMoney;
        private Telerik.WinControls.UI.RadLabel radLabel_CenterEmpl;
        private Telerik.WinControls.UI.RadLabel radLabel_CenterHeader;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchHeader;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchBath;
        internal Telerik.WinControls.UI.RadLabel radLabel_Branch;
        internal Telerik.WinControls.UI.RadLabel radLabel_MA;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchEnter;
        protected Telerik.WinControls.UI.RadButton radButton_AX;
        internal Telerik.WinControls.UI.RadLabel radLabel_Vender;
        private System.Drawing.Printing.PrintDocument PrintDocument2;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadLabel radLabel_VenderName;
        internal Telerik.WinControls.UI.RadLabel radLabel_BranchName;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
    }
}
