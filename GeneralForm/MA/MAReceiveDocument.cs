﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Collections;
using PC_Shop24Hrs.GeneralForm.TimeKeeper;

namespace PC_Shop24Hrs.GeneralForm.MA
{
    public partial class MAReceiveDocument : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        //ข้อมูลเอกสารที่ได้มากจากการบันทึกไปแล้ว
        string MABill, RecIdBill;//, RecIdBill = "00000000";//pConfigDB,
        int PrintBill = 0;

        readonly string _pBchID, _pBchName, _pDateBill, _pVenID, _pVenName, _pDocno;
        readonly DataTable DtItems;

        //ข้อมูลเอกสารใหม่
        // String DocNo, Empl, EmplName, Remark;
        // Double Money = 0;

        //รหัสสำหรับผู้ต้องสแกนนิ้ว
        string ScanEmplId, ScanEmplName, ScanEmplPosition, ScanEmplDpt;
        //สำหรับการพิมพ์
        string Document, EmplIDR, EmplNameR, EmplRmk, copySlip, EmplIDC, EmplNameC;
        double Grand;

        //สถานะส่ง AX
        string staAX, sta24;//0 Insert 1 Edit
        string staLock;//0 OK 1 Lock ห้ามแก้ไข
        //String StatusLock, StatusINVOICEID;//StatusAX,

        //MA
        public MAReceiveDocument(String pDate, String pBchID, string pBchName, String pVenID, string pVenName, DataTable dtItemBarcode, string pDocno)
        {
            InitializeComponent();

            _pBchID = pBchID;
            _pBchName = pBchName;
            _pDateBill = pDate;
            _pVenID = pVenID;
            _pVenName = pVenName;
            _pDocno = pDocno;
            DtItems = dtItemBarcode;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 120)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 200)));
            if (SystemClass.SystemBranchID == "MN000")
            {
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนนับ", 80)));
            }
            else
            {
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนรับ", 80)));
            }
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));

            RadButton_BranchSave.ButtonElement.ShowBorder = true;
            RadButton_BranchCancel.ButtonElement.ShowBorder = true;
            RadButton_CenterEdit.ButtonElement.ShowBorder = true;
            RadButton_CanterCencel.ButtonElement.ShowBorder = true;

            panel1.BorderStyle = BorderStyle.FixedSingle;

            RadButton_BranchSave.ButtonElement.ShowBorder = true; RadButton_BranchSave.ButtonElement.ToolTipText = "บันทึก";
            RadButton_BranchCancel.ButtonElement.ShowBorder = true; RadButton_BranchCancel.ButtonElement.ToolTipText = "ยกเลิก";
            RadButton_CenterEdit.ButtonElement.ShowBorder = true; RadButton_CenterEdit.ButtonElement.ToolTipText = "แก้ไข";
            RadButton_CanterCencel.ButtonElement.ShowBorder = true; RadButton_CanterCencel.ButtonElement.ToolTipText = "ยกเลิก";
            radButton_AX.ButtonElement.ShowBorder = true; radButton_AX.ButtonElement.ToolTipText = "AX";


            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
        }

        //Load
        private void MAReceiveDocument_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            PrintBill = 0;
            radLabel_Branch.Text = _pBchID;
            radLabel_BranchName.Text = _pBchName;
            radLabel_Vender.Text = _pVenID;
            radLabel_VenderName.Text = _pVenName;
            radLabel_MA.Text = ""; MABill = "";

            //ข้อมูลใน GRID             //DtItems ข้อมูล Dt ที่ส่งเข้ามาให้ผ่าน ฟอร์ม 
            ClearDefault();
            RadGridView_Show.DataSource = DtItems;

            //ข้อมูลบิลรับสินค้า
            DataTable DtDoc24 = MAReceiveClass.GetBillRecevie(_pBchID, _pDateBill, _pVenID, _pDocno);//24

            staLock = "0"; sta24 = "0"; staAX = "0";
            //มีการรับเอกสารมาแล้ว
            if (DtDoc24.Rows.Count > 0)
            {
                MABill = DtDoc24.Rows[0]["BillTrans"].ToString();
                RecIdBill = DtDoc24.Rows[0]["RECID"].ToString();
                sta24 = "1";

                DataTable DtDocAX = PurchClass.CheckStatusAX_SPC_SHIPCARRIERINVOICE(MABill); // AX
                if (DtDocAX.Rows.Count > 0)
                {
                    radTextBox_CenterDocno.ForeColor = Color.Green;
                    staAX = "1";// StatusAX = "UPSQL";
                    staLock = DtDocAX.Rows[0]["LOCKED"].ToString();//StatusLock = DtStatusAX.Rows[0]["LOCKED"].ToString();
                }

                radLabel_MA.Text = MABill;
                RadTextBox_BranchDocno.Text = DtDoc24.Rows[0]["Docno"].ToString();
                RadTextBox_BranchMoney.Text = Double.Parse(DtDoc24.Rows[0]["Price"].ToString()).ToString("0.00#,##");
                RadTextBox_BranchEmpl.Text = DtDoc24.Rows[0]["EmpIDReceive"].ToString().Replace("M", "").Replace("D", "").Replace("P", "");
                radLabel_EmplName.Text = DtDoc24.Rows[0]["SPC_NAME"].ToString();
                PrintBill = int.Parse(DtDoc24.Rows[0]["PrintBill"].ToString());

                RadTextBox_BranchDocno.SelectAll();
                radTextBox_CenterDocno.Focus();


                if (SystemClass.SystemBranchID == "MN000")
                {
                    radTextBox_CenterDocno.Text = DtDoc24.Rows[0]["Docno"].ToString();
                    radTextBox_CenterMoney.Text = string.Format("{0:0.00}", Double.Parse(DtDoc24.Rows[0]["Price"].ToString()));
                    radTextBox_CenterEmpl.Text = "";//SystemClass.SystemUserID;
                    radLabel_CenterName.Text = ""; //SystemClass.SystemUserName;
                    radTextBox_CenterRemark.Text = DtDoc24.Rows[0]["RemarkBill"].ToString();
                    RadButton_CenterEdit.Enabled = false;
                    radTextBox_CenterEmpl.Focus();
                }

                if (staLock == "1")
                {
                    RadTextBox_BranchDocno.Enabled = false;
                    radTextBox_CenterDocno.Enabled = false;
                    radLabel_MA.ForeColor = Color.Red;
                }
            }
        }

        #region FUNCTION
        //clear Form
        void ClearDefault()
        {
            ScanEmplId = ""; ScanEmplName = ""; ScanEmplPosition = ""; ScanEmplDpt = "";
            Document = ""; EmplIDR = ""; EmplNameR = ""; EmplRmk = ""; Grand = 0; EmplIDC = ""; EmplNameC = "";

            radLabel_EmplName.Text = ""; radLabel_CenterName.Text = "";

            if (SystemClass.SystemBranchID != "MN000")
            {
                panel_Center.Visible = false;
                panel_Bch.Visible = true;

                RadTextBox_BranchDocno.Text = ""; RadTextBox_BranchDocno.Enabled = true;
                RadTextBox_BranchMoney.Text = ""; RadTextBox_BranchMoney.Enabled = false;
                RadTextBox_BranchEmpl.Text = ""; RadTextBox_BranchEmpl.Enabled = false;

                RadTextBox_BranchRemark.Text = "";

                RadButton_BranchSave.Enabled = false; RadButton_BranchCancel.Enabled = true;
            }
            else//center
            {
                panel_Center.Visible = true;
                panel_Bch.Enabled = false;

                radTextBox_CenterDocno.Enabled = true;
                radTextBox_CenterMoney.Enabled = false;
                radTextBox_CenterEmpl.Enabled = false;
                radLabel_CenterName.Text = "";
                radTextBox_CenterRemark.Enabled = false;

                RadButton_CenterEdit.Visible = true;
                RadButton_CanterCencel.Visible = true;

                radButton_AX.Visible = true;
                radButton_AX.Enabled = false;

                if (SystemClass.SystemComMinimart == "1") radButton_AX.Enabled = true;
            }
        }


        #endregion

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion

        //  #region EVEN

        //ระบุเลขที่เอกสาร
        private void RadTextBox_BranchDocno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && RadTextBox_BranchDocno.Text.Length > 0)
            {
                if (_pVenID == "MP")
                {
                    if (RadTextBox_BranchDocno.Text.Substring(0, 2) != "MP") RadTextBox_BranchDocno.Text = "MP" + RadTextBox_BranchDocno.Text;
                }
                //เช็คเลขที่เอกสารที่เคยรับแล้ว ตามสาขา ตามผู้จำหน่าย วันที่บิล
                DataTable DtMADocno = MAReceiveClass.CheckMAReceiveBill(RadTextBox_BranchDocno.Text);

                if (DtMADocno.Rows.Count > 0)
                {
                    //check branch
                    if (DtMADocno.Rows[0]["BranchBill"].ToString() != SystemClass.SystemBranchID)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"เลขที่เอกสาร {RadTextBox_BranchDocno.Text} รับโดยสาขา {DtMADocno.Rows[0]["BranchBill"]} แล้ว ไม่สามารถรับบิลซ้ำได้ ติดต่อ Centershop 1022.");
                        RadTextBox_BranchDocno.SelectAll();
                        RadTextBox_BranchDocno.Focus();
                        return;
                    }
                    //check vender
                    if (DtMADocno.Rows[0]["RemarkVender"].ToString() != _pVenID)
                    {
                        if (DtMADocno.Rows[0]["BranchBill"].ToString() != SystemClass.SystemBranchID)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"เลขที่เอกสาร {RadTextBox_BranchDocno.Text} รับโดยผู้จำหน่าย {DtMADocno.Rows[0]["RemarkVender"]} แล้ว ไม่สามารถรับบิลซ้ำได้ ติดต่อ Centershop 1022.");
                            RadTextBox_BranchDocno.SelectAll();
                            RadTextBox_BranchDocno.Focus();
                            return;
                        }
                    }
                    var numberOfDays = (DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")) -
                        DateTime.Parse(DtMADocno.Rows[0]["RDateIn"].ToString())).TotalDays;

                    if (_pVenID == "V005450")
                    {
                        if (numberOfDays > 1)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"เลขที่เอกสาร {RadTextBox_BranchDocno.Text} รับเมื่อวันที่ {DtMADocno.Rows[0]["RDateIn"]} แล้ว ไม่สามารถรับบิลซ้ำได้ ติดต่อ Centershop 1022");
                            RadTextBox_BranchDocno.SelectAll();
                            RadTextBox_BranchDocno.Focus();
                            return;
                        }
                    }
                    else
                    {
                        if (numberOfDays != 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"เลขที่เอกสาร {RadTextBox_BranchDocno.Text} รับเมื่อวันที่ {DtMADocno.Rows[0]["RDateIn"]} แล้ว ไม่สามารถรับบิลซ้ำได้ ติดต่อ Centershop 1022.");
                            RadTextBox_BranchDocno.SelectAll();
                            RadTextBox_BranchDocno.Focus();
                            return;
                        }
                    }

                    radLabel_MA.Text = DtMADocno.Rows[0]["BillTrans"].ToString();

                    RadTextBox_BranchMoney.Text = DtMADocno.Rows[0]["Price"].ToString();
                    RadTextBox_BranchEmpl.Text = DtMADocno.Rows[0]["EmpIDReceive"].ToString().Replace("D", "").Replace("M", "").Replace("P", "");
                    radLabel_EmplName.Text = DtMADocno.Rows[0]["SPC_NAME"].ToString();

                    RadTextBox_BranchDocno.SelectAll();
                    RadTextBox_BranchDocno.Focus();

                }

                RadTextBox_BranchDocno.Enabled = false;
                RadTextBox_BranchMoney.Enabled = true;
                RadTextBox_BranchMoney.Focus();
            }
        }

        private void RadButton_BranchSave_Click(object sender, EventArgs e)
        {
            //เช็คลายนิ้วมือผู้พิการ 
            if (TimeKeeperClass.GetEmplScanCard_ByEmplBranch(ScanEmplId, SystemClass.SystemBranchID.Replace("MN", "")))
            {
                FormShare.FingerScan FingerScan = new FormShare.FingerScan(ScanEmplId, "สแกนรับบิล", ScanEmplName, ScanEmplPosition, ScanEmplDpt);

                if (FingerScan.ShowDialog() != DialogResult.Yes)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"กรุณาสแกนนิ้วใหม่อีกครั้ง{Environment.NewLine}เนื่องจากไม่มีการยืนยันการสแกนนิ้ว");
                    RadTextBox_BranchEmpl.SelectAll();
                    RadTextBox_BranchEmpl.Focus();
                    return;
                }
            }

            Document = RadTextBox_BranchDocno.Text;
            EmplIDR = RadTextBox_BranchEmpl.Text;
            EmplNameR = radLabel_EmplName.Text;
            Grand = Double.Parse(RadTextBox_BranchMoney.Text);

            INSERT_shop_ReciveDocument(Document, EmplIDR, EmplNameR, "", Grand, "", "");
            RadButton_BranchSave.Enabled = false;
        }

        //Edit Data
        private void RadButton_CenterEdit_Click(object sender, EventArgs e)
        {

            if (radLabel_MA.ForeColor == Color.Red)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("แจ้งเอกสาร MA มีปัญหา ติดต่อ 8570 ");
                return;
            }

            if (radTextBox_CenterRemark.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุหมายเหตุในการแก้ไขบิลทุกครั้ง");
                radTextBox_CenterRemark.Focus();
                return;
            }

            Document = radTextBox_CenterDocno.Text;
            EmplIDC = radTextBox_CenterEmpl.Text;
            EmplIDR = RadTextBox_BranchEmpl.Text;
            EmplNameC = radLabel_CenterName.Text;
            EmplNameR = radLabel_EmplName.Text;
            EmplRmk = radTextBox_CenterRemark.Text;
            Grand = Double.Parse(radTextBox_CenterMoney.Text);
            INSERT_shop_ReciveDocument(Document, EmplIDR, EmplNameR, EmplRmk, Grand, EmplIDC, EmplNameC);
            radTextBox_CenterRemark.Enabled = false;

        }
        //CanCle
        private void RadButton_CanterCencel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void INSERT_shop_ReciveDocument(string DocNo, string Empl, string EmplName, string Remark, double Money, string EmplC, string EmplNameC)
        {
            if (PrintBill == 0) PrintBill = 1; else PrintBill += 1;

            if (radLabel_MA.Text == "")
            {
                MABill = Class.ConfigClass.GetMaxINVOICEID("MA", _pBchID.Replace("MN", ""), "MA", "9");
                if (MABill != "") RecIdBill = MABill.Replace("MA", "");
                radLabel_MA.Text = MABill;
            }

            string TypeBill = "V"; if (_pVenID == "MP") TypeBill = "MP";


            ArrayList sqlIn = new ArrayList();

            Var_ReciveDocument var_ReciveDocument = new Var_ReciveDocument
            {
                Docno = DocNo,
                EmpIDReceive = Empl,
                EmplNameReceive = EmplName,
                PrintBill = PrintBill,
                BranchBill = _pBchID,
                BranchName = _pBchName,
                RemarkBill = Remark,
                TypeBill = TypeBill,
                RemarkVender = _pVenID,
                VenderName = _pVenName,
                RDateIn = _pDateBill,
                EmpIDAdmin = "",
                RemarkAdmin = "",
                RWhoIn = SystemClass.SystemUserID,
                Rtype = "2",
                Price = Money,
                BillTrans = MABill,
                RECID = RecIdBill
            };

            if (sta24 == "0")
            {
                if (Empl == "") { Empl = EmplC; EmplName = EmplNameC; }
                var_ReciveDocument.EmpIDReceive = Empl;
                var_ReciveDocument.EmplNameReceive = EmplName;
                sqlIn.Add(MAReceiveClass.Save_ReciveDocument("0", var_ReciveDocument));
            }
            else
            {

                var_ReciveDocument.BillTrans = radLabel_MA.Text;
                var_ReciveDocument.TypeBill = "V";
                sqlIn.Add(MAReceiveClass.Save_ReciveDocument("2", var_ReciveDocument));

                if (RecIdBill != "00000000")
                {
                    if ((_pVenID != "MP"))
                    {
                        var_ReciveDocument.EmpIDAdmin = EmplC;
                        var_ReciveDocument.RemarkAdmin = EmplNameC;
                        sqlIn.Add(MAReceiveClass.Save_ReciveDocument("1", var_ReciveDocument));
                    }
                }
            }

            ////string pRecIDAX = RecIdBill;             if ((_pVenID == "V005450") || (_pVenID == "MP")) pRecIDAX = "000000000000000000000000";
            //ArrayList sqlAX = new ArrayList()
            //{ AX_SendData.Save_SHIPCARRIERINVOICE(staAX, MABill, DocNo, Money, RecIdBill,"","","")};

            ////string resault = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn, Save708AX_SPC_SHIPCARRIERINVOICE(staAX, DocNo, Money, RecIdBill));
            //string resault = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn, sqlAX);
            //MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            //if (resault == "") PrintDocument();


            string resault;
            if ((_pVenID == "V005450") || (_pVenID == "MP"))
            {
                resault = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            }
            else
            {
                ArrayList sqlAX = new ArrayList()
            { AX_SendData.Save_SHIPCARRIERINVOICE(staAX, MABill, DocNo, Money, RecIdBill,"","",SystemClass.SystemBranchID,_pVenID)};
                resault = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn, sqlAX);
            }
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "") PrintDocument();
        }

        //check ยอดเงิน
        private void RadTextBox_BranchMoney_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //check ยอดเงิน
        private void RadTextBox_CenterMoney_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }
        //checkEmp branch
        private void RadTextBox_BranchEmpl_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (RadTextBox_BranchEmpl.Text.Length > 0))
            {

                if (RadTextBox_BranchEmpl.Text.Length != 7)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุรหัสพนักงานให้ถูกต้อง");
                    RadTextBox_BranchEmpl.SelectAll();
                    RadTextBox_BranchEmpl.Focus();
                    return;
                }

                DataTable dtEmpl = Models.EmplClass.GetEmployee(RadTextBox_BranchEmpl.Text);
                if (dtEmpl.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงานที่ระบุ");
                    RadTextBox_BranchEmpl.SelectAll();
                    RadTextBox_BranchEmpl.Focus();
                    return;
                }

                ScanEmplId = dtEmpl.Rows[0]["EMPLID"].ToString();
                ScanEmplName = dtEmpl.Rows[0]["SPC_NAME"].ToString();
                ScanEmplPosition = dtEmpl.Rows[0]["POSSITION"].ToString();
                ScanEmplDpt = dtEmpl.Rows[0]["DESCRIPTION"].ToString();
                radLabel_EmplName.Text = ScanEmplName;//EmplClass.GetEmployeeName_ByEmplID_M(Te

                RadTextBox_BranchEmpl.Enabled = false;
                RadButton_BranchSave.Enabled = true;
                RadTextBox_BranchEmpl.Enabled = false;

                RadButton_BranchSave.Focus();

            }
        }
        //ระบุรหัส พนง center
        private void RadTextBox_CenterEmpl_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_CenterEmpl.Text.Length > 0))
            {
                if (radTextBox_CenterEmpl.Text.Length < 7)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุรหัสพนักงานให้ถูกต้อง");
                    radTextBox_CenterEmpl.SelectAll();
                    radTextBox_CenterEmpl.Focus();
                    return;
                }

                DataTable dtEmpl = Models.EmplClass.GetEmployee(radTextBox_CenterEmpl.Text);
                if (dtEmpl.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงานที่ระบุ");
                    radTextBox_CenterEmpl.SelectAll();
                    radTextBox_CenterEmpl.Focus();
                    return;
                }
                radTextBox_CenterEmpl.Enabled = false;
                radLabel_CenterName.Text = dtEmpl.Rows[0]["SPC_NAME"].ToString();
                RadButton_CenterEdit.Enabled = true;
                radTextBox_CenterRemark.Enabled = true;
                radTextBox_CenterRemark.Focus();
            }
        }
        //Check Num
        private void RadTextBox_BranchEmpl_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //check การใส่ยอดเงิน สาขา
        private void RadTextBox_BranchMoney_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (RadTextBox_BranchMoney.Text.Length > 0))
            {
                RadTextBox_BranchMoney.Enabled = false;
                RadTextBox_BranchEmpl.Enabled = true;
                RadTextBox_BranchEmpl.SelectAll();
                RadTextBox_BranchEmpl.Focus();
            }
        }

        private void RadTextBox_CenterDocno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CenterDocno.Enabled = false;
                radTextBox_CenterMoney.Enabled = true;
                radTextBox_CenterMoney.SelectAll();
                radTextBox_CenterMoney.Focus();
            }
        }
        //check ยอดเงิน Center
        private void RadTextBox_CenterMoney_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) && (radTextBox_CenterMoney.Text.Length > 0))
            {
                radTextBox_CenterMoney.Enabled = false;
                radTextBox_CenterEmpl.Enabled = true;
                radTextBox_CenterEmpl.SelectAll();
                radTextBox_CenterEmpl.Focus();
            }
        }
        //Document
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pVenID);
        }

        private void RadGridView_Show_DefaultValuesNeeded(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells["QTYBch"].Value = "-";
        }
        //SendToax Again
        private void RadButton_AX_Click(object sender, EventArgs e)
        {

            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_MainAX(AX_SendData.Save_SHIPCARRIERINVOICE(staAX, MABill, RadTextBox_BranchDocno.Text, double.Parse(RadTextBox_BranchMoney.Text), RecIdBill, "", "", radLabel_Branch.Text, radLabel_Vender.Text)));
        }

        //cancle
        private void RadButton_BranchCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Print Slip
        void PrintDocument()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
            printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
            PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;
            copySlip = "1";
            PrintDocument1.Print();
            copySlip = "2";
            PrintDocument1.Print();
        }
        //Print Document
        private void PrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            barcode.Data = MABill;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            string StatusBill = "";
            if (PrintBill > 1)
            {
                StatusBill = "(แก้ไข)";
            }
            e.Graphics.DrawString("พิมพ์ครั้งที่ " + PrintBill + "-" + copySlip, SystemClass.printFont, Brushes.Black, 80, Y);
            Y += 20;
            e.Graphics.DrawString("รับบิลผู้จำหน่าย " + StatusBill, SystemClass.SetFont12, Brushes.Black, 40, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("--------------รับสินค้า-------------", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString(radLabel_Branch.Text + "-" + radLabel_BranchName.Text, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 20;
            e.Graphics.DrawString("พิมพ์ " + DateTime.Now, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("เลขที่เอกสารผู้จำหน่าย", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString(Document, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("ยอดเงิน " + Grand.ToString("N2") + " บาท", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้รับบิล : " + EmplIDR, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("ชื่อ " + EmplNameR, SystemClass.printFont, Brushes.Black, 15, Y);

            if (SystemClass.SystemBranchID == "MN000")
            {
                Y += 25;
                e.Graphics.DrawString("ผู้แก้ไข " + EmplIDC, SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 20;
                e.Graphics.DrawString(EmplNameC, SystemClass.printFont, Brushes.Black, 15, Y);
            }

            Y += 25;
            e.Graphics.DrawString("ผู้จำหน่าย " + radLabel_Vender.Text, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString(radLabel_VenderName.Text, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("หมายเหตุ " + EmplRmk, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("________________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

        }

    }
}