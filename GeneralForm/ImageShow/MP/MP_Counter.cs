﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.MP
{
    public partial class MP_Counter : Telerik.WinControls.UI.RadForm
    {
        DirectoryInfo DirInfo;
        readonly DataTable dtSendMA = new DataTable();

        FileInfo[] FilesImage;
        DataTable dtItemBarcode = new DataTable();
        readonly string _pVoucherID;
        readonly string _pDate;
        readonly string _pBchID;
        readonly string _pBchName;
        readonly string _pTypeOpen;
        readonly string _pPerMission;

        //readonly string _pConfigDB;
        // readonly string _pSta; //0 ยังไม่นับ 1 นับแล้ว ไว้โหลดข้อมูลกันคนละ server
        //Load
        public MP_Counter(string pVoucherID, string pDate, string pBchID, string pBchName, string pTypeOpen, string pPerMission)
        {
            InitializeComponent();

            _pVoucherID = pVoucherID;
            _pDate = pDate;
            _pBchID = pBchID;
            _pBchName = pBchName;
            _pTypeOpen = pTypeOpen;
            _pPerMission = pPerMission;
        }
        private void MP_Counter_Load(object sender, EventArgs e)
        {
            radStatusStrip.SizingGrip = false;
            radButtonElement_Count.ShowBorder = true; radButtonElement_Count.ToolTipText = "บันทึกนับรูป";
            radButton_refresh.ShowBorder = true; radButton_refresh.ToolTipText = "ดึงข้อมูลใหม่";
            radButtonElement_MA.ShowBorder = true; radButtonElement_MA.ToolTipText = "พิมพ์ MA";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            this.Text = "รูปรับสินค้าเซลล์ส่ง [MP] " + _pDate + " / " + _pVoucherID + " [" + _pBchName + "]";

            DatagridClass.SetDefaultRadGridView(radGridView_Image);
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อสินค้า", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTYSUPC", "จำนวน_SUPC"));
            if (SystemClass.SystemBranchID == "MN000")
            {
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTYBCH", "จำนวนรับ"));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYCOUNT", "จำนวนนับ", 100));
            }
            else
            {
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYBCH", "จำนวนรับ", 100));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTYCOUNT", "จำนวนนับ"));
            }

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA", "STA"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "รูปด้านบน", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG1", "C_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "รูปด้านหน้า", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG2", "C_IMG2"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG3", "รูปด้านข้าง", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG3", "P_IMG3"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG3", "C_IMG3"));


            if (_pTypeOpen == "SHOP")
            {
                radLabel_F2.Text = "Double Click รูป >> ดูรูปขนาดใหญ่ | สีเขียว >> ของที่รับไม่มีในบิล | สีชมพู >> ไม่มีการรับของที่สาขา";
                radButtonElement_Count.Enabled = false;
            }
            else
            {
                radLabel_F2.Text = "Double Click รูป >> ดูรูปขนาดใหญ่ | Double Click จำนวนนับ >> เพื่อนับรูป | สีแดง >> ยังไม่ระบุจำนวนนับ | สีเขียว >> ของที่รับไม่มีในบิล | สีชมพู >> ไม่มีการรับของที่สาขา";
                if (_pPerMission == "1") radButtonElement_Count.Enabled = true; else radButtonElement_Count.Enabled = false;
            }

            radGridView_Image.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Image.TableElement.RowHeight = 300;
            radGridView_Image.TableElement.TableHeaderHeight = 50;

            DatagridClass.SetCellBackClolorByExpression("QTYCOUNT", "QTYCOUNT = 0 ", ConfigClass.SetColor_Red(), radGridView_Image);
            DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "QTYSUPC = 0 ", ConfigClass.SetColor_GreenPastel(), radGridView_Image);
            DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "QTYBCH = 0 ", ConfigClass.SetColor_PinkPastel(), radGridView_Image);

            DirInfo = new DirectoryInfo(PathImageClass.pPathMP + _pDate + @"\" + _pBchID + "");

            dtSendMA.Columns.Add("ITEMBARCODE");
            dtSendMA.Columns.Add("SPC_ITEMNAME");
            dtSendMA.Columns.Add("QTY");
            dtSendMA.Columns.Add("UNITID");

            FindImage();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview

        #endregion


        //จำนวนที่เซลล์ส่ง
        double FindQTY_MP(string pBarcode)
        {
            string sqlSUPC = string.Format(@"
            SELECT	ITEMBARCODE,NAME AS SPC_ITEMNAME,SALESUNIT AS UNITID,QTY AS QTYSUPC
            FROM 	SPC_POSTOLINE20_MP WITH (NOLOCK) 
		            INNER JOIN SPC_POSTOTABLE20_MP WITH (NOLOCK) ON SPC_POSTOLINE20_MP.VOUCHERID = SPC_POSTOTABLE20_MP.VOUCHERID 
            WHERE	SPC_POSTOTABLE20_MP.SHIPDATE = '" + _pDate + @"' and INVENTLOCATIONIDTO = '" + _pBchID + @"' 
                    AND SPC_POSTOTABLE20_MP.VOUCHERID = '" + _pVoucherID + @"' 	 
		            AND  ITEMBARCODE = '" + pBarcode + @"'
            ");
            DataTable dt = ConnectionClass.SelectSQL_SentServer(sqlSUPC, IpServerConnectClass.ConMP);
            if (dt.Rows.Count == 0) return 0; else return double.Parse(dt.Rows[0]["QTYSUPC"].ToString());
        }
        //จำนวนที่ Center นับ
        DataTable FindQTY_Count(string pBarcode)
        {
            string sqlSUPC = string.Format(@"
                    SELECT	ITEMBARCODE,SPC_ITEMNAME,QtyBch ,QtyCount,UNITID 	
					FROM	SHOP_CHECKMP WITH (NOLOCK) 
					WHERE	VOUCHERID = '" + _pVoucherID + @"' AND ITEMBARCODE = '" + pBarcode + @"' 
            ");
            return ConnectionClass.SelectSQL_Main(sqlSUPC);
        }
        //Find Image
        void FindImage()
        {
            this.Cursor = Cursors.WaitCursor;
            if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();
            FindBarcodeAllImage();
            int iCheckSave = 0;
            for (int i = 0; i < dtItemBarcode.Rows.Count; i++)
            {
                //MP-MN026-2020-09-18-090224-M1805147_MP139200918002,08851123240284,2,[B]
                string itembarcode = dtItemBarcode.Rows[i]["ITEMBARCODE"].ToString();
                string itembarcodeName = dtItemBarcode.Rows[i]["SPC_ITEMNAME"].ToString();
                string itembarcodeUnit = dtItemBarcode.Rows[i]["UNITID"].ToString();


                double qtySUPC = FindQTY_MP(itembarcode);

                DataTable dtSupc = FindQTY_Count(itembarcode);
                double qtyCount = 0;
                string itemSTA = "0";
                if (dtSupc.Rows.Count > 0)
                {
                    qtyCount = double.Parse(dtSupc.Rows[0]["QtyCount"].ToString());
                    itemSTA = "1";
                    iCheckSave++;
                }

                int qtyBch = 0;

                FileInfo[] FilesByBarcodeT = DirInfo.GetFiles("*" + _pVoucherID + @"," + itembarcode + @",*[T].JPG", SearchOption.AllDirectories);
                FileInfo[] FilesByBarcodeF = DirInfo.GetFiles("*" + _pVoucherID + @"," + itembarcode + @",*[F].JPG", SearchOption.AllDirectories);
                FileInfo[] FilesByBarcodeB = DirInfo.GetFiles("*" + _pVoucherID + @"," + itembarcode + @",*[B].JPG", SearchOption.AllDirectories);

                string[] AA;
                string[] item;


                string img1 = PathImageClass.pImageEmply;
                string p_img1 = ""; string c_img1 = "";

                string img2 = PathImageClass.pImageEmply;
                string p_img2 = ""; string c_img2 = "";

                string img3 = PathImageClass.pImageEmply;
                string p_img3 = ""; string c_img3 = "";

                //MP-MN026-2020-09-18-090224-M1805147_MP139200918002,08851123240284,2,[B]

                if (FilesByBarcodeT.Length > 0)
                {
                    img1 = FilesByBarcodeT[0].FullName;
                    p_img1 = FilesByBarcodeT[0].Name;
                    c_img1 = "1";
                    AA = FilesByBarcodeT[0].Name.Split('_');
                    item = AA[1].Split(',');
                    qtyBch = int.Parse(item[2]);
                }

                if (FilesByBarcodeF.Length > 0)
                {
                    img2 = FilesByBarcodeF[0].FullName;
                    p_img2 = FilesByBarcodeF[0].Name;
                    c_img2 = "1";
                    if (qtyBch == 0)
                    {
                        AA = FilesByBarcodeF[0].Name.Split('_');
                        item = AA[1].Split(',');
                        qtyBch = int.Parse(item[2]);
                    }
                }
                if (FilesByBarcodeB.Length > 0)
                {
                    img3 = FilesByBarcodeB[0].FullName;
                    p_img3 = FilesByBarcodeB[0].Name;
                    c_img3 = "1";

                    if (qtyBch == 0)
                    {
                        AA = FilesByBarcodeT[0].Name.Split('_');
                        item = AA[1].Split(',');
                        qtyBch = int.Parse(item[2]);
                    }
                }


                radGridView_Image.Rows.Add(itembarcode, itembarcodeName, qtySUPC, qtyBch, qtyCount, itemSTA, itembarcodeUnit,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img1), PathImageClass.pPathMP + _pDate + @"\" + _pBchID + @"|" + p_img1, c_img1,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img2), PathImageClass.pPathMP + _pDate + @"\" + _pBchID + @"|" + p_img2, c_img2,
                ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img3), PathImageClass.pPathMP + _pDate + @"\" + _pBchID + @"|" + p_img3, c_img3);

                double qtySend = qtyBch;
                if (SystemClass.SystemBranchID == "MN000") qtySend = qtyCount;

                dtSendMA.Rows.Add(itembarcode, itembarcodeName, qtySend.ToString("#,#0.00"), itembarcodeUnit);
            }

            if (_pTypeOpen == "SUPC")
            {
                if (iCheckSave == radGridView_Image.Rows.Count)
                {
                    radButtonElement_Count.Enabled = false;
                }
                else
                {
                    if (_pPerMission == "0") radButtonElement_Count.Enabled = false; else radButtonElement_Count.Enabled = true;
                }

            }

            this.Cursor = Cursors.Default;
        }
        // FindBarcode From AllImage
        void FindBarcodeAllImage()
        {
            FilesImage = DirInfo.GetFiles("*" + _pVoucherID + @",*.JPG", SearchOption.AllDirectories);

            string barcode = "";
            if (FilesImage.Length > 0)
            {
                foreach (FileInfo item in FilesImage)
                {
                    //Vender-MN033-2020-10-01-105352-M0911113_V001954#8850999111018,A,15,[B]
                    string[] AA = item.Name.Split('_');
                    string[] itemname = AA[1].Split(',');
                    barcode = barcode + "'" + itemname[1] + "',";
                }
            }
            if (barcode.Length == 0)
            {
                barcode = "''";
            }
            else { barcode = barcode.Substring(0, barcode.Length - 1); }
            string sql = string.Format(@"
                    SELECT	ITEMBARCODE,NAME AS SPC_ITEMNAME,SALESUNIT AS UNITID
                    FROM 	SPC_POSTOLINE20_MP WITH (NOLOCK) 
		                    INNER JOIN SPC_POSTOTABLE20_MP WITH (NOLOCK) ON SPC_POSTOLINE20_MP.VOUCHERID = SPC_POSTOTABLE20_MP.VOUCHERID 
                    WHERE	SPC_POSTOTABLE20_MP.SHIPDATE = '" + _pDate + @"' 
                            and INVENTLOCATIONIDTO = '" + _pBchID + @"' AND SPC_POSTOTABLE20_MP.VOUCHERID = '" + _pVoucherID + @"' 	 
                    UNION
                    SELECT	    ITEMBARCODE ,SPC_ITEMNAME,UNITID 
                    FROM	    INVENTITEMBARCODE WITH (NOLOCK)   
                    WHERE	    ITEMBARCODE IN  (" + barcode + @")
                            AND DATAAREAID = 'SPC'
                ");
            dtItemBarcode = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMP);
        }

        //Image Set Format
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //refresh
        private void RadButton_refresh_Click(object sender, EventArgs e)
        {
            FindImage();
        }
        //DoubleClick
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QTYCOUNT":
                    if (_pPerMission == "0") return;


                    if (radGridView_Image.CurrentRow.Cells["STA"].Value.ToString() != "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("รายการนี้มีการนับเรียบร้อยแล้ว ไม่สามารถบันทึกซ้ำได้.");
                        return;
                    }

                    FormShare.InputData _inputdata = new FormShare.InputData("0",
                         radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" + radGridView_Image.CurrentRow.Cells["SPC_NAME"].Value.ToString(),
                         "จำนวนที่นับได้ [ไม่สามารถแก้ไขได้]", radGridView_Image.CurrentRow.Cells["UNITID"].Value.ToString());
                    if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                    {
                        double QtyCount = double.Parse(_inputdata.pInputData);
                        double QtyBch = double.Parse(radGridView_Image.CurrentRow.Cells["QTYBCH"].Value.ToString());
                        if ((QtyCount != QtyBch))
                        {
                            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("จำนวนของสินค้าที่ลงของระหว่างสาขาและจำนวนนับไม่เท่ากัน" + Environment.NewLine +
                                "ยืนยันการนับ เพราะข้อมูลจะไม่สามารถแก้ไขใดๆ") == DialogResult.No)
                            {
                                return;
                            }
                        }
                        radGridView_Image.CurrentRow.Cells["QTYCOUNT"].Value = QtyCount;
                    }
                    break;
                case "IMG1":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG1"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    break;
                case "IMG2":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG2"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG2"].Value.ToString());
                    break;
                case "IMG3":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG3"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG3"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        int CheckQtyBchAndSend()
        {
            //check ก่อนว่าสาขาและส่ง จำนวนเท่ากันไหม
            int iCheck = 0;
            for (int i = 0; i < radGridView_Image.Rows.Count; i++)
            {
                if (double.Parse(radGridView_Image.Rows[i].Cells["QTYSUPC"].Value.ToString()) !=
                    double.Parse(radGridView_Image.Rows[i].Cells["QTYBCH"].Value.ToString()))
                {
                    iCheck++;
                }
            }
            return iCheck;
        }

        //Slip MA
        private void RadButtonElement_MA_Click(object sender, EventArgs e)
        {
            //เชค รูป
            if (radGridView_Image.Rows.Count == 0) return;
            //เชควันที่ปัจจุบันสำหรับสาขาห
            if (_pTypeOpen == "SHOP")
            {
                if (DateTime.Parse(_pDate).ToString("yyyy-MM-dd") != DateTime.Today.ToString("yyyy-MM-dd"))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถสร้างหรือแก้ไขสลิป MA ย้อนหลังได้" + Environment.NewLine + " ติดต่อ CenterShop Tel 1022 เพื่อแก้ไขบิลแทน.");
                    return;
                }
            }

            int iCheck = CheckQtyBchAndSend();
            if (iCheck > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่อนุญาติให้รับบิล เนื่องจากปริมาณการส่งและการรับไม่เท่ากัน{ Environment.NewLine}แก้ไขให้เรียบร้อยก่อนแล้วบันทึกรับใหม่อีกครั้ง");
                return;
            }
            //เข้า MA
            using (MA.MAReceiveDocument mAReceiveDocument = new MA.MAReceiveDocument(_pDate, _pBchID, _pBchName, "MP", "เซลล์ซุปเปอร์ชีป", dtSendMA, _pVoucherID))
            {

                DialogResult dr = mAReceiveDocument.ShowDialog();
                if (dr == DialogResult.Yes)
                {

                }
            }
        }
        //Count
        private void RadButtonElement_Count_Click(object sender, EventArgs e)
        {
            int iCheck = CheckQtyBchAndSend();
            if (iCheck > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่อนุญาติให้นับบิล เนื่องจากปริมาณการส่งและการรับไม่เท่ากัน{ Environment.NewLine}แก้ไขให้เรียบร้อยก่อนแล้วบันทึกรับใหม่อีกครั้ง");
                return;
            }

            string sqlMA = $@" 
                SELECT	BillTrans	FROM	SHOP_RECIVEDOCUMENT WITH (NOLOCK)
                WHERE	Docno = '{_pVoucherID}' ";
            DataTable dtMA = ConnectionClass.SelectSQL_Main(sqlMA);
            if (dtMA.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบเลขที่เอกสารการยืนยันรับบิลจากมินิมาร์ท{Environment.NewLine}ลองใหม่อีกครั้ง.");
                return;
            }
            if (dtMA.Rows[0]["BillTrans"].ToString() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบเลขที่เอกสารการยืนยันรับบิลจากมินิมาร์ท{Environment.NewLine}ลองใหม่อีกครั้ง.");
                return;
            }

            ArrayList sql24 = new ArrayList();

            int iRow = 0;
            //int iCheck = 0;
            for (int i = 0; i < radGridView_Image.Rows.Count; i++)
            {
                if (double.Parse(radGridView_Image.Rows[i].Cells["QTYCOUNT"].Value.ToString()) == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่อนุญาติให้รับบิล เนื่องจากระบุจำนวนนับยังไม่ครบทุกรายการ{Environment.NewLine}ระบุจำนวนนับให้เรียบร้อยก่อนแล้วบันทึกใหม่อีกครั้ง");
                    return;
                }
                iRow++;
                sql24.Add($@"
                Insert Into Shop_CheckMP(
                    [VOUCHERID],[LINENUM],[ITEMBARCODE],[SPC_ITEMNAME],
		            [QtyBch],[QtyCount],[QtySupc],[UNITID],
                    [WhoDpt],[WhoIns],[WhoInsName]) VALUES (
                    '{_pVoucherID}','{iRow}','{radGridView_Image.Rows[i].Cells["ITEMBARCODE"].Value}',
                    '{ConfigClass.ChecKStringForImport(radGridView_Image.Rows[i].Cells["SPC_NAME"].Value.ToString())}',
                    '{Double.Parse(radGridView_Image.Rows[i].Cells["QTYBCH"].Value.ToString())}',
                    '{Double.Parse(radGridView_Image.Rows[i].Cells["QTYCOUNT"].Value.ToString())}',
                    '{Double.Parse(radGridView_Image.Rows[i].Cells["QTYSUPC"].Value.ToString())}',
                    '{radGridView_Image.Rows[i].Cells["UNITID"].Value}',
                    '{SystemClass.SystemDptID}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}') ");
            }

            string sqlMP = string.Format(@"EXECUTE [dbo].[ConfirmVoucherMinimartTransfer] '" + _pVoucherID + @"'");
            DataTable dt_MP = ConnectionClass.SelectSQL_SentServer(sqlMP, IpServerConnectClass.ConMP);
            if (dt_MP.Rows.Count > 0)
            {
                if (dt_MP.Rows[0]["VOUCHERID"].ToString() == _pVoucherID)
                {
                    string T = ConnectionClass.ExecuteSQL_ArrayMain(sql24); MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "") radButtonElement_Count.Enabled = false;
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถส่งข้อมูลเข้า SERVER AX ได้{Environment.NewLine}ลองใหม่อีกครั้ง");
                return;
            }

        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
