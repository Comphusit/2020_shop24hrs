﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.MP
{
    public partial class MP_Main : Telerik.WinControls.UI.RadForm
    {      
        readonly string _pTypeOpen;
        readonly string _pPerMission;

        DataTable dtBch;
        DataTable dt_Data = new DataTable();

        //Load
        public MP_Main(string pTypeOpen,string pPerMission)
        {
            InitializeComponent();

            _pTypeOpen = pTypeOpen;
            _pPerMission = pPerMission;
        }
        //Load
        private void MP_Main_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_MA.ToolTipText = "รับบิล"; radButtonElement_MA.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
 
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;


            radDateTimePicker_D2.Visible = true;
            radDateTimePicker_D1.Visible = true;
            radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
            radLabel_Date.Visible = true; radLabel_Date.Text = "วันที่บิลเซลล์ส่ง [MP]";
            RadCheckBox_1.Visible = true;
            RadDropDownList_1.Visible = true;

            radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีแดง >> สาขายังไม่รับ | สีเขียว >> ยังไม่ตรวจสอบ | กด / >> เข้าหน้ารับบิลและตรวจสอบ";


            if (_pTypeOpen == "SUPC")
            {
                RadCheckBox_1.Enabled = true;
                dtBch = BranchClass.GetBranchAll("'1','4'", " '1'");
            }
            else
            {
                RadCheckBox_1.CheckState = CheckState.Checked;
                RadCheckBox_1.Enabled = false;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
            }

            RadDropDownList_1.DataSource = dtBch;
            RadDropDownList_1.DisplayMember = "NAME_BRANCH";
            RadDropDownList_1.ValueMember = "BRANCH_ID";
    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VOUCHERID", "เลขที่บิล", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPDATE", "วันที่บิล", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "VAN ", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("MN_STA", "สถานะการรับสาขา", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("STA", "สถานะการตรวจสอบ", 150)));

            RadGridView_ShowHD.MasterTemplate.Columns["INVENTLOCATIONIDTO"].IsPinned = true;
            RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;

            DatagridClass.SetCellBackClolorByExpression("VOUCHERID", "STA = '0' ", ConfigClass.SetColor_GreenPastel() ,RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("VOUCHERID", "MN_STA = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
         
            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0)   dt_Data.Rows.Clear();  


            string pCon2 = "";
            if (RadCheckBox_1.Checked == true) pCon2 = " AND INVENTLOCATIONIDTO = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";

            string sql = string.Format(@"
                    SELECT	INVENTLOCATIONIDTO,BRANCH_NAME,VOUCHERID,INVENTLOCATIONIDFROM,MN_STATUS,STATUS,CONVERT(VARCHAR,SHIPDATE,23) AS SHIPDATE 
		                    ,CASE MN_STATUS WHEN '1' THEN '1' WHEN '2' THEN '1' ELSE '0' END AS MN_STA
		                    ,CASE MN_STATUS WHEN '1' THEN '0' WHEN '2' THEN '1' ELSE '0' END AS STA
                    FROM 	SPC_POSTOTABLE20_MP WITH (NOLOCK)  INNER JOIN Shop_Branch WITH (NOLOCK) ON SPC_POSTOTABLE20_MP.INVENTLOCATIONIDTO = Shop_Branch.BRANCH_ID 
                    WHERE	SHIPDATE   between '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' 
                            AND'" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"' AND VOUCHERID LIKE 'MP%'  " + pCon2 + @"
                            AND SPC_POSTOTABLE20_MP.STATUS = '0'
                    ORDER BY INVENTLOCATIONIDTO 
                    ");
            dt_Data = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMP);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;

        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }
         //MA
        private void RadButtonElement_MA_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string voucher = RadGridView_ShowHD.CurrentRow.Cells["VOUCHERID"].Value.ToString();
            if (voucher == "") return;

            MP_Counter _mpCouter = new MP_Counter(voucher,
                RadGridView_ShowHD.CurrentRow.Cells["SHIPDATE"].Value.ToString(),
                RadGridView_ShowHD.CurrentRow.Cells["INVENTLOCATIONIDTO"].Value.ToString(),
                RadGridView_ShowHD.CurrentRow.Cells["BRANCH_NAME"].Value.ToString(), _pTypeOpen,_pPerMission);           
                if (_mpCouter.ShowDialog(this) == DialogResult.Yes)
                {

                }
            SetDGV_HD();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}
