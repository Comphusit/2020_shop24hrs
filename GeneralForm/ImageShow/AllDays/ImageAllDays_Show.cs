﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;
using System.Collections.Generic;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.AllDays
{
    public partial class ImageAllDays_Show : Telerik.WinControls.UI.RadForm
    {
        private DataTable dtBch = new DataTable();
        readonly string _pPermission;// 0 ไม่มีสิด 1 มี
        readonly string _pTypeOpen;// SHOP-SUPC
        readonly string _pTypeReport;

        //0 รูปถ่ายประจำวัน รายวัน ตามกลุ่มสินค้า 1// รูปถ่ายประจำวัน รายเดือน ตามกลุ่ม // 3 รูปถ่ายประจำวัน ตามช่วงวันที่ ทุกสาขา
        readonly List<RadGridView> ListgridExport = new List<RadGridView> { };

        string dateBeginLoad;
        //Load
        public ImageAllDays_Show(string pTypeOpen, string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(radGridView_Group);

            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("C", "/", 40));
            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 50));
            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "ชื่อ", 250));
            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MaxImage", "รูป", 50));
            radGridView_Group.MasterTemplate.Columns["C"].IsPinned = true;

        }
        //Load Main
        private void ImageAllDays_Show_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            RadButton_Search.ButtonElement.ShowBorder = true;
            radButtonElement_excel.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_Clear.ShowBorder = true; radButtonElement_Clear.ToolTipText = "ล้างข้อมูล";
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            if (_pTypeOpen == "SHOP") dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID); else dtBch = BranchClass.GetBranchAll("'1'", "'1'");
            ClearData();
        }
        void ClearData()
        {
            try
            {
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear();
                    RadGridView_ShowHD.Rows.Clear();
                }
            }
            catch (Exception) { }
            radGridView_Group.MasterTemplate.FilterDescriptors.Clear();
            radDateTimePicker_Begin.Value = DateTime.Now;
            switch (_pTypeReport)
            {
                case "0":
                    radDateTimePicker_Begin.Visible = true; radDateTimePicker_End.Visible = false;
                    RadDropDownList_Branch.Visible = false;
                    radDateTimePicker_Begin.Value = DateTime.Now;

                    if (_pPermission == "0") radLabel_F2.Text = "Double Click >> ดูรูปขนาดใหญ่"; else radLabel_F2.Text = "Double Click >> ดูรูปขนาดใหญ่ | กดปุ่ม Delete ที่รูป >> เพื่อลบรูป";

                    SetGroup();
                    break;
                case "1":
                    radDateTimePicker_Begin.Visible = true; radDateTimePicker_End.Visible = true;
                    RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.DataSource = dtBch;
                    RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_Branch.ValueMember = "BRANCH_ID";
                    radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-30);
                    radDateTimePicker_End.Value = DateTime.Now;

                    if (_pPermission == "0") radLabel_F2.Text = "Double Click >> ดูรูปขนาดใหญ่"; else radLabel_F2.Text = "Double Click >> ดูรูปขนาดใหญ่ | กดปุ่ม Delete ที่รูป >> เพื่อลบรูป";
                    SetGroup();
                    break;
                case "2":
                    radDateTimePicker_Begin.Visible = true; radDateTimePicker_End.Visible = true;
                    RadDropDownList_Branch.Visible = false;
                    radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-7);
                    radDateTimePicker_End.Value = DateTime.Now;
                    radLabel_F2.Text = "Double Click >> ดูรูปทั้งหมด";

                    SetGroup();
                    break;
                case "3":
                    radDateTimePicker_Begin.Visible = true; radDateTimePicker_End.Visible = true;
                    RadDropDownList_Branch.Visible = false;
                    radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-7);
                    radDateTimePicker_End.Value = DateTime.Now;

                    if (_pPermission == "0") radLabel_F2.Text = "Double Click >> ดูรูปขนาดใหญ่"; else radLabel_F2.Text = "Double Click >> ดูรูปขนาดใหญ่ | กดปุ่ม Delete ที่รูป >> เพื่อลบรูป";
                   
                    SetGroup();
                    break;
                default:
                    break;
            }
        }
        //load Group
        void SetGroup()
        {
            string sql = "";

            switch (_pTypeReport)
            {
                case "0":
                    string DaysOf = "", Days10 = "";
                    if (dateBeginLoad == radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"))
                    {
                        return;
                    }
                    switch (radDateTimePicker_Begin.Value.DayOfWeek)
                    {
                        case DayOfWeek.Sunday:
                            DaysOf = ",'0007'";
                            break;
                        case DayOfWeek.Monday:
                            DaysOf = ",'0001'";
                            break;
                        case DayOfWeek.Tuesday:
                            DaysOf = ",'0002'";
                            break;
                        case DayOfWeek.Wednesday:
                            DaysOf = ",'0003'";
                            break;
                        case DayOfWeek.Thursday:
                            DaysOf = ",'0004'";
                            break;
                        case DayOfWeek.Friday:
                            DaysOf = ",'0005'";
                            break;
                        case DayOfWeek.Saturday:
                            DaysOf = ",'0006'";
                            break;
                        default:
                            break;
                    }
                    switch (radDateTimePicker_Begin.Value.Day)
                    {
                        case 10:
                            Days10 = ",'0010'"; break;
                        case 15:
                            Days10 = ",'0015'"; break;
                        default:
                            break;
                    }
                    sql = string.Format(@"
                    SELECT	'0' AS 'C',SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage,SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID	
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
		                    INNER JOIN SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = SHOP_CONFIGBRANCH_DETAIL.SHOW_ID 
		                    INNER JOIN 
			                    (SELECT	SHOW_ID,SHOW_NAME
			                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
			                    WHERE	TYPE_CONFIG = '27' AND STA = '1' )Day_Take ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = Day_Take.SHOW_ID

                    WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '26' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1'
		                    AND SHOP_CONFIGBRANCH_DETAIL.STATUS = '1' 
		                    AND SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID IN ('0000','0008'" + DaysOf + Days10 + @")
                    GROUP BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage,SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID
                    ORDER BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                    ");
                    break;
                case "1":
                    sql = string.Format(@"
                    SELECT	'0' AS 'C',SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                    WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '26' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1'
                    GROUP BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage
                    ORDER BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                    ");
                    break;
                case "2":
                    sql = string.Format(@"
                    SELECT	'0' AS 'C',SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                    WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '26' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1'
                    GROUP BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage
                    ORDER BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                    ");
                    break;
                case "3":
                    sql = string.Format(@"
                    SELECT	'0' AS 'C',SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage
                    FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                    WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '26' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1'
                    GROUP BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,MaxImage
                    ORDER BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                    ");
                    break;
                default:
                    break;
            }

            dateBeginLoad = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            radGridView_Group.DataSource = dt;
            dt.AcceptChanges();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_ShowHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Group_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //Date Change
        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            switch (_pTypeReport)
            {
                case "0":
                    SetGroup();
                    break;
                default:
                    break;
            }
        }
        //Date Change
        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (_pTypeReport == "3")//กรณีการ Ecport หลาย Sheet
            {
                SetDGVForExportExcel();
                if (ListgridExport.Count == 0) { return; }
                string T = DatagridClass.ExportExcelGridViewAllDayImage("รูปถ่ายประจำวัน", ListgridExport, "2");
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
            else
            {
                if (RadGridView_ShowHD.Rows.Count == 0) { return; }
                string T = DatagridClass.ExportExcelGridView("Detail", RadGridView_ShowHD, "2");
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //ค้นหา
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "0":
                    LoadDataFixDate0();
                    break;
                case "1":
                    LoadDataLongDate1();
                    break;
                case "2":
                    LoadDataCountImage2();
                    break;
                case "3":
                    LoadDataCountImage3();
                    break;
                default:
                    break;
            }
        }
        //FindGroup
        DataTable FindGroupID(DateTime pDate, string pGrp, string pTypeCheck)
        {
            string DaysOf = "", Days10 = "";
            string sql = "";
            switch (pTypeCheck)
            {
                case "0":
                    // string DaysOf = "", Days10 = "";
                    switch (pDate.DayOfWeek)
                    {
                        case DayOfWeek.Sunday:
                            DaysOf = ",'0007'";
                            break;
                        case DayOfWeek.Monday:
                            DaysOf = ",'0001'";
                            break;
                        case DayOfWeek.Tuesday:
                            DaysOf = ",'0002'";
                            break;
                        case DayOfWeek.Wednesday:
                            DaysOf = ",'0003'";
                            break;
                        case DayOfWeek.Thursday:
                            DaysOf = ",'0004'";
                            break;
                        case DayOfWeek.Friday:
                            DaysOf = ",'0005'";
                            break;
                        case DayOfWeek.Saturday:
                            DaysOf = ",'0006'";
                            break;
                        default:
                            break;
                    }
                    switch (pDate.Day)
                    {
                        case 10:
                            Days10 = ",'0010'"; break;
                        case 15:
                            Days10 = ",'0015'"; break;
                        default:
                            break;
                    }

                    sql = string.Format(@"
                        SELECT	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID,BRANCH_ID AS Grp_ID,TMP.SHOW_DESC AS Grp_Desc
                        FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK)
		                        INNER JOIN (SELECT SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC
		                        FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
		                        WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '27' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1')TMP 
		                        ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = TMP.SHOW_ID
                        WHERE	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = '" + pGrp + @"' AND BRANCH_ID IN ('0000','0008'" + DaysOf + Days10 + @")
                        ORDER BY BRANCH_ID
                        ");
                    break;
                case "1":
                    sql = string.Format(@"
                        SELECT	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID,BRANCH_ID AS Grp_ID,TMP.SHOW_DESC AS Grp_Desc
                        FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK)
                          INNER JOIN (SELECT SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC
                          FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                          WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '27' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1')TMP 
                          ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = TMP.SHOW_ID
                        WHERE	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = '" + pGrp + @"' 
                        ORDER BY BRANCH_ID
                        ");
                    break;

                default:
                    break;
            }


            return ConnectionClass.SelectSQL_Main(sql);
        }
        //LoadDataFixDate0
        void LoadDataFixDate0()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pDate", "วันที่", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "สาขา", 170)));
            RadGridView_ShowHD.Columns["pDate"].IsPinned = true;
            RadGridView_ShowHD.Columns["pBchName"].IsPinned = true;
            RadGridView_ShowHD.TableElement.TableHeaderHeight = 100;
            RadGridView_ShowHD.TableElement.RowHeight = 200;

            //ช่วงคอลัม
            int i_Column = 0;
            foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
            {
                if (itemRow.Cells["C"].Value.ToString() == "1")
                {
                    string HeadColumn = itemRow.Cells["SHOW_ID"].Value.ToString();
                    string HeadText = itemRow.Cells["SHOW_DESC"].Value.ToString();

                    DataTable dtGrp = FindGroupID(radDateTimePicker_Begin.Value, HeadColumn, "0");
                    if (dtGrp.Rows.Count > 0)
                    {
                        int r_Count = Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString());

                        for (int iG = 0; iG < dtGrp.Rows.Count; iG++)
                        {
                            string grpID = dtGrp.Rows[iG]["Grp_ID"].ToString();
                            string grpDesc = dtGrp.Rows[iG]["Grp_Desc"].ToString();
                            i_Column += r_Count;
                            for (int i = 0; i < r_Count; i++)
                            {
                                string HeadColumnImage = grpID + HeadColumn + "_I" + Convert.ToString(i + 1);
                                string HeadColumnCount = grpID + HeadColumn + "_C" + Convert.ToString(i + 1);
                                string HeadColumnPath = grpID + HeadColumn + "_P" + Convert.ToString(i + 1);
                                string HeadTextDesc_Image = grpDesc + Environment.NewLine + HeadText + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";
                                string HeadTextDesc_Count = grpDesc + Environment.NewLine + HeadColumn + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";
                                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnImage, HeadTextDesc_Image, 150)));
                                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible(HeadColumnCount, HeadTextDesc_Count)));
                                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible(HeadColumnPath, "Path")));
                            }
                        }
                    }

                }
            }
            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("กลุ่มรูปถ่ายประจำวัน");
                this.Cursor = Cursors.Default;
                return;
            }
            //ค้นหารูปทั้งหมด
            for (int iB = 0; iB < dtBch.Rows.Count; iB++)
            {
                int iCountColume = 0;
                RadGridView_ShowHD.Rows.Add(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + Environment.NewLine + radDateTimePicker_Begin.Value.DayOfWeek,
                    dtBch.Rows[iB]["BRANCH_ID"].ToString() + Environment.NewLine + dtBch.Rows[iB]["BRANCH_NAME"].ToString());

                foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
                {
                    if (itemRow.Cells["C"].Value.ToString() == "1")
                    {
                        DataTable dtGrpFindImg = FindGroupID(radDateTimePicker_Begin.Value, itemRow.Cells["SHOW_ID"].Value.ToString(), "0");
                        for (int iG_img = 0; iG_img < dtGrpFindImg.Rows.Count; iG_img++)
                        {
                            DataTable dtData = FindImageInGroup(dtGrpFindImg.Rows[iG_img]["Grp_ID"].ToString(),
                               itemRow.Cells["SHOW_ID"].Value.ToString(),
                               radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                               dtBch.Rows[iB]["BRANCH_ID"].ToString());

                            for (int iImage = 0; iImage < Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString()); iImage++)
                            {
                                try
                                {
                                    RadGridView_ShowHD.Rows[iB].Cells[iCountColume + 2].Value =
                                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dtData.Rows[iImage]["FullName"].ToString());
                                    RadGridView_ShowHD.Rows[iB].Cells[iCountColume + 3].Value = "1";
                                    RadGridView_ShowHD.Rows[iB].Cells[iCountColume + 4].Value = dtData.Rows[iImage]["FileName"].ToString();
                                }
                                catch (Exception)
                                {
                                    //RadGridView_ShowHD.Rows[iB].Cells[iCountColume + 2].Value = DatagridClass.ScaleImageDataGridView150_200(PathImageClass.pImageEmply);
                                    RadGridView_ShowHD.Rows[iB].Cells[iCountColume + 3].Value = "0";
                                    RadGridView_ShowHD.Rows[iB].Cells[iCountColume + 4].Value = "";
                                }
                                iCountColume += 3;
                            }
                        }


                    }
                }
            }


            this.Cursor = Cursors.Default;
        }
        //LoadDataFixDate0
        void LoadDataLongDate1()
        {
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                //RadGridView_ShowHD.SummaryRowsTop.Clear();
            }


            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pDate", "วันที่", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "สาขา", 170)));
            RadGridView_ShowHD.Columns["pDate"].IsPinned = true;
            RadGridView_ShowHD.Columns["pBchName"].IsPinned = true;
            RadGridView_ShowHD.TableElement.TableHeaderHeight = 100;
            RadGridView_ShowHD.TableElement.RowHeight = 200;

            //เพิ่มคอลัม
            int i_Column = 0;
            string col_Express = "";
            foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
            {
                if (itemRow.Cells["C"].Value.ToString() == "1")
                {
                    string HeadColumn = itemRow.Cells["SHOW_ID"].Value.ToString();
                    string HeadText = itemRow.Cells["SHOW_DESC"].Value.ToString();
                    int r_Count = Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString());
                    i_Column += r_Count;

                    for (int i = 0; i < r_Count; i++)
                    {
                        string HeadColumnImage = HeadColumn + "_I" + Convert.ToString(i + 1);
                        string HeadColumnCount = HeadColumn + "_C" + Convert.ToString(i + 1);
                        string HeadColumnPath = HeadColumn + "_P" + Convert.ToString(i + 1);
                        string HeadTextDesc_Image = HeadText + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";
                        string HeadTextDesc_Count = HeadColumn + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";

                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnImage, HeadTextDesc_Image, 150)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible(HeadColumnCount, HeadTextDesc_Count)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible(HeadColumnPath, "Path")));


                        GridViewSummaryItem summaryItem = new GridViewSummaryItem(HeadColumnCount, "{0}", GridAggregateFunction.Sum);
                        summaryRowItem.Add(summaryItem);

                        if (col_Express == "")
                        {
                            col_Express += HeadColumnCount;
                        }
                        else
                        {
                            col_Express += "+" + HeadColumnCount;
                        }
                    }
                }
            }
            //Sum Image
            GridViewDecimalColumn col = new GridViewDecimalColumn
            {
                Name = "CountImage",
                HeaderText = "CountImage",
                IsVisible = false
            };
            RadGridView_ShowHD.MasterTemplate.Columns.Add(col);
            RadGridView_ShowHD.Columns["CountImage"].Expression = col_Express;

            //this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("กลุ่มรูปถ่ายประจำวัน");
                this.Cursor = Cursors.Default;
                return;
            }
            ////Add Data

            //  var dateDiff = radDateTimePicker_End.Value.Subtract(radDateTimePicker_Begin.Value);
            // int intDay = dateDiff.Days + 1;
            double intDay = Math.Ceiling((radDateTimePicker_End.Value - radDateTimePicker_Begin.Value).TotalDays) + 1;

            int countRowShow = 0;
            //จำนวนสาขา
            string bch_ID = RadDropDownList_Branch.SelectedValue.ToString();
            string bch_Name = RadDropDownList_Branch.SelectedItem[0].ToString();
            //จำนวนวันที่ดึง
            for (int iD = 0; iD < intDay; iD++)
            {
                var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");
                int iCountColume = 0;
                RadGridView_ShowHD.Rows.Add(pDate + Environment.NewLine + Convert.ToDateTime(pDate).DayOfWeek, bch_Name); ;
                countRowShow += 1;
                //จำนวนกลุ่ม
                foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
                {
                    if (itemRow.Cells["C"].Value.ToString() == "1")
                    {
                        DataTable dtGrpFindImg = FindGroupID(Convert.ToDateTime(pDate), itemRow.Cells["SHOW_ID"].Value.ToString(), "0");
                        if (dtGrpFindImg.Rows.Count == 0)
                        {
                            for (int iImage = 0; iImage < Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString()); iImage++)
                            {
                                RadGridView_ShowHD.Rows[countRowShow - 1].Cells[iCountColume + 3].Value = 0;
                                iCountColume += 3;
                            }
                        }
                        else
                        {
                            for (int iG_img = 0; iG_img < dtGrpFindImg.Rows.Count; iG_img++)
                            {
                                DataTable dtData = FindImageInGroup(dtGrpFindImg.Rows[iG_img]["Grp_ID"].ToString(),
                                   itemRow.Cells["SHOW_ID"].Value.ToString(),
                                   pDate,
                                   bch_ID);

                                for (int iImage = 0; iImage < Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString()); iImage++)
                                {
                                    try
                                    {
                                        RadGridView_ShowHD.Rows[countRowShow - 1].Cells[iCountColume + 2].Value =
                                          ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dtData.Rows[iImage]["FullName"].ToString());
                                        RadGridView_ShowHD.Rows[countRowShow - 1].Cells[iCountColume + 3].Value = 1;
                                        RadGridView_ShowHD.Rows[countRowShow - 1].Cells[iCountColume + 4].Value = dtData.Rows[iImage]["FileName"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                        RadGridView_ShowHD.Rows[countRowShow - 1].Cells[iCountColume + 3].Value = 0;
                                        RadGridView_ShowHD.Rows[countRowShow - 1].Cells[iCountColume + 4].Value = "";
                                    }
                                    iCountColume += 3;
                                }
                            }
                        }

                    }
                }

            }

            //แถวที่ไม่มีข้อมูลให้ปิดออก
            for (int iCheck = 0; iCheck < RadGridView_ShowHD.Rows.Count; iCheck++)
            {
                if (Convert.ToUInt16(RadGridView_ShowHD.Rows[iCheck].Cells["CountImage"].Value.ToString()) == 0)
                {
                    RadGridView_ShowHD.Rows[iCheck].IsVisible = false;
                }
            }
            //คอลัมที่ไม่มีข้อมูลให้ปิดออก
            int iC_Hide = 2;
            for (int iColume = 0; iColume < summaryRowItem.Count; iColume++)
            {
                //GridViewSummaryRowItem summaryRowItem12 = this.RadGridView_ShowHD.SummaryRowsTop[0];
                GridViewSummaryItem summaryItem12 = summaryRowItem[iColume];
                object summary = summaryItem12.Evaluate(this.RadGridView_ShowHD.MasterTemplate);
                if (summary.ToString() == "0")
                {
                    RadGridView_ShowHD.Columns[iC_Hide].IsVisible = false;
                }
                iC_Hide += 3;
            }

            this.Cursor = Cursors.Default;
        }
          
        //LoadDataFixDate0
        void LoadDataCountImage2()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("pBchID", "สาขา")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "สาขา", 200)));
            RadGridView_ShowHD.Columns["pBchName"].IsPinned = true;
            RadGridView_ShowHD.TableElement.TableHeaderHeight = 100;

            //var dateDiff = radDateTimePicker_End.Value.Subtract(radDateTimePicker_Begin.Value);
            //int intDay = dateDiff.Days + 1;
            double intDay = Math.Ceiling((radDateTimePicker_End.Value - radDateTimePicker_Begin.Value).TotalDays) + 1;
            //int intDay = int.Parse(A);
            //เพิ่มคอลัม
            int i_Column = 0;
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                var pDate = radDateTimePicker_Begin.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
                {
                    if (itemRow.Cells["C"].Value.ToString() == "1")
                    {
                        string HeadColumnCount = itemRow.Cells["SHOW_ID"].Value.ToString() + "_" + pDate.Replace("-", "F");
                        string HeadTextDesc_Count = pDate + Environment.NewLine + itemRow.Cells["SHOW_DESC"].Value.ToString() +
                            Environment.NewLine + "[" + itemRow.Cells["MaxImage"].Value.ToString() + "]";

                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, HeadTextDesc_Count, 150)));

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                             new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 ", false)
                             { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                        i_Column += 1;
                    }
                }
            }

            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("กลุ่มรูปถ่ายประจำวัน");
                this.Cursor = Cursors.Default;
                return;
            }
            ////Add Data    //จำนวนวันที่ดึง
            for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
            {
                string bch_ID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                string bch_Name = dtBch.Rows[iBch]["BRANCH_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(bch_ID, bch_ID + "-" + bch_Name);

                int iCountColume = 2;
                for (int iD = 0; iD < intDay; iD++)
                {
                    var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");
                    //จำนวนกลุ่ม
                    foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
                    {
                        if (itemRow.Cells["C"].Value.ToString() == "1")
                        {

                            DataTable dtGrpFindImg = FindGroupID(Convert.ToDateTime(pDate), itemRow.Cells["SHOW_ID"].Value.ToString(), "0");
                            if (dtGrpFindImg.Rows.Count == 0)
                            {
                                RadGridView_ShowHD.Rows[iBch].Cells[iCountColume].Value = 0;
                            }
                            else
                            {
                                RadGridView_ShowHD.Rows[iBch].Cells[iCountColume].Value = CountImageInGroup(dtGrpFindImg.Rows[0]["Grp_ID"].ToString(),
                                itemRow.Cells["SHOW_ID"].Value.ToString(), pDate, bch_ID).ToString();
                            }
                            iCountColume += 1;
                        }
                    }

                }

            }
            this.Cursor = Cursors.Default;
        }
        //FindDataInGroup
        DataTable FindImageInGroup(String pDays, String pGroup, String pDate, String pBch)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FullName");
            dt.Columns.Add("FileName");
            string path = PathImageClass.pPathAllDay + pDays + @"\" + pGroup + @"\" + pDate + @"\" + pBch + "";
            if (Convert.ToDateTime(pDate) > Convert.ToDateTime(PathImageClass.pPathAllDayChangeServer))
            {
                path = path.Replace("192.168.100.27", "192.168.100.77");
            }

            DirectoryInfo DirInfo = new DirectoryInfo(path);
            if (DirInfo.Exists == false)
            {
                return dt;
            }
            else
            {
                FileInfo[] Files = DirInfo.GetFiles("*.JPG", SearchOption.AllDirectories);
                if (Files.Length == 0)
                {
                    return dt;
                }
                else
                {
                    for (int i = 0; i < Files.Length; i++)
                    {
                        dt.Rows.Add(Files[i].FullName, path + "|" + Files[i].Name);
                    }
                    return dt;
                }
            }
        }
        //FindDataInGroup
        int CountImageInGroup(String pDays, String pGroup, String pDate, String pBch)
        {
            string path = PathImageClass.pPathAllDay + pDays + @"\" + pGroup + @"\" + pDate + @"\" + pBch + "";
            if (Convert.ToDateTime(pDate) > Convert.ToDateTime(PathImageClass.pPathAllDayChangeServer))
            {
                path = path.Replace("192.168.100.27", "192.168.100.77");
            }

            DirectoryInfo DirInfo = new DirectoryInfo(path);
            if (DirInfo.Exists == false)
            {
                return 0;
            }
            else
            {
                FileInfo[] Files = DirInfo.GetFiles("*.JPG", SearchOption.AllDirectories);
                return Files.Length;
            }
        }
        //Choose Group
        private void RadGridView_Group_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (e.Column.Name)
            {
                case "C":
                    if (radGridView_Group.CurrentRow.Cells["C"].Value.ToString() == "1")
                    {
                        radGridView_Group.CurrentRow.Cells["C"].Value = "0";
                    }
                    else
                    {
                        radGridView_Group.CurrentRow.Cells["C"].Value = "1";
                    }
                    break;

                default:
                    break;
            }
        }
        //Big Image
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (_pTypeReport)
            {
                case "0":
                    int iK0 = RadGridView_ShowHD.CurrentCell.ColumnIndex;
                    if (RadGridView_ShowHD.CurrentRow.Cells[iK0 + 1].Value.ToString() == "1")
                    {
                        ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells[iK0 + 1].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells[iK0 + 2].Value.ToString());
                    }
                    break;
                case "1":
                    int iK1 = RadGridView_ShowHD.CurrentCell.ColumnIndex;
                    if (RadGridView_ShowHD.CurrentRow.Cells[iK1 + 1].Value.ToString() == "1")
                    {
                        ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells[iK1 + 1].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells[iK1 + 2].Value.ToString());
                    }
                    break;
                case "2":
                    if ((RadGridView_ShowHD.CurrentCell.ColumnIndex == 0) || (RadGridView_ShowHD.CurrentCell.ColumnIndex == 1))
                    { return; }
                    try { if (int.Parse((RadGridView_ShowHD.CurrentCell.Value.ToString())) == 0) { return; } }
                    catch (Exception) { return; }

                    string[] HeadT = RadGridView_ShowHD.CurrentCell.ColumnInfo.Name.Split('_');
                    DataTable dtGrpFindImg = FindGroupID(Convert.ToDateTime(HeadT[1].Replace("F", "-")), HeadT[0], "0");
                    if (dtGrpFindImg.Rows.Count == 0) { return; }

                    string path = PathImageClass.pPathAllDay + dtGrpFindImg.Rows[0]["Grp_ID"].ToString() + @"\" + HeadT[0] + @"\" + HeadT[1].Replace("F", "-") + @"\" +
                        RadGridView_ShowHD.CurrentRow.Cells["pBchID"].Value.ToString() + "|" + "*.JPG";
                    if (Convert.ToDateTime(HeadT[1].Replace("F", "-")) > Convert.ToDateTime(PathImageClass.pPathAllDayChangeServer))
                    {
                        path = path.Replace("192.168.100.27", "192.168.100.77");
                    }

                    ImageClass.OpenShowImage("1", path);
                    break;
                case "3":
                    int iK3 = RadGridView_ShowHD.CurrentCell.ColumnIndex;
                    if (RadGridView_ShowHD.CurrentRow.Cells[iK3 + 1].Value.ToString() == "1")
                    {
                        ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells[iK3 + 1].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells[iK3 + 2].Value.ToString());
                    }
                    break;
                default:
                    break;
            }


        }
        //Claer
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Delete
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (_pPermission == "0")
            {
                return;
            }
            if (_pTypeReport == "2")
            {
                return;
            }
            switch (_pPermission)
            {
                case "1":
                    switch (e.KeyCode)
                    {
                        case Keys.Delete:
                            int iK = RadGridView_ShowHD.CurrentCell.ColumnIndex;
                            if (RadGridView_ShowHD.CurrentRow.Cells[iK + 1].Value.ToString() == "0")
                            { return; }

                            if (MsgBoxClass.MsgBoxShow_ConfirmDelete("รูปที่เลือก - " + RadGridView_ShowHD.CurrentCell.ColumnInfo.HeaderText.ToString()) == DialogResult.No)
                            { return; }

                            string pathDel = RadGridView_ShowHD.CurrentRow.Cells[iK + 2].Value.ToString().Replace("|", @"\");
                            try
                            {
                                File.Delete(pathDel);
                            }
                            catch (Exception ex)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning(ex.Message); return;
                            }

                            break;

                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        //LoadData ทุกสาขา หลายกลุ่ม ตามช่วงวันนที่ แสดงผลตามกลุ่มก่อนเรียงวันที่ให้เสดแล้วขึ้นกลุ่มใหม่
        void LoadDataCountImage3()
        {

            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("pBchID", "สาขา")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "สาขา", 200)));
            RadGridView_ShowHD.Columns["pBchName"].IsPinned = true;
            RadGridView_ShowHD.TableElement.TableHeaderHeight = 120;
            RadGridView_ShowHD.TableElement.RowHeight = 200;

            int i_Column = 0;
            string col_Express = "";
            double intDay = Math.Ceiling((radDateTimePicker_End.Value - radDateTimePicker_Begin.Value).TotalDays) + 1;

            //Add คอลัม
            foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
            {
                if (itemRow.Cells["C"].Value.ToString() == "1")
                {

                    string HeadColumn = itemRow.Cells["SHOW_ID"].Value.ToString();
                    string HeadText = itemRow.Cells["SHOW_DESC"].Value.ToString();

                    for (int iD = 0; iD < intDay; iD++)
                    {
                        var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");
                        DataTable dtGrp = FindGroupID(Convert.ToDateTime(pDate), HeadColumn, "0");
                        if (dtGrp.Rows.Count > 0)
                        {
                            int r_Count = Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString());

                            for (int iG = 0; iG < dtGrp.Rows.Count; iG++)
                            {
                                string grpID = dtGrp.Rows[iG]["Grp_ID"].ToString();
                                string grpDesc = dtGrp.Rows[iG]["Grp_Desc"].ToString();


                                //i_Column += r_Count;
                                for (int i = 0; i < r_Count; i++)
                                {
                                    string HeadColumnImage = pDate + "_" + grpID + HeadColumn + "_I" + Convert.ToString(i + 1);
                                    string HeadColumnCount = pDate + "_" + grpID + HeadColumn + "_C" + Convert.ToString(i + 1);
                                    string HeadColumnPath = pDate + "_" + grpID + HeadColumn + "_P" + Convert.ToString(i + 1);
                                    string HeadTextDesc_Image = pDate + Environment.NewLine + grpDesc + Environment.NewLine + HeadText + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";
                                    string HeadTextDesc_Count = grpDesc + Environment.NewLine + HeadColumn + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";

                                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnImage, HeadTextDesc_Image, 150)));
                                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible(HeadColumnCount, HeadTextDesc_Count)));
                                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible(HeadColumnPath, "Path")));

                                    GridViewSummaryItem summaryItem = new GridViewSummaryItem(HeadColumnCount, "{0}", GridAggregateFunction.Sum);
                                    summaryRowItem.Add(summaryItem);

                                    if (col_Express == "")
                                    {
                                        col_Express += HeadColumnCount;
                                    }
                                    else
                                    {
                                        col_Express += "+" + HeadColumnCount;
                                    }

                                    i_Column += 3;
                                }
                            }
                        }

                    }

                }

            }


            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("กลุ่มรูปถ่ายประจำวัน");
                this.Cursor = Cursors.Default;
                return;
            }

            //////Add Data    //จำนวนวันที่ดึง
            for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
            {
                string bch_ID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                string bch_Name = dtBch.Rows[iBch]["BRANCH_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(bch_ID, bch_ID + "-" + bch_Name);

                int iCountColume = 0;
                //จำนวนกลุ่ม
                foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
                {
                    if (itemRow.Cells["C"].Value.ToString() == "1")
                    {
                        for (int iD = 0; iD < intDay; iD++)
                        {
                            var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");
                            DataTable dtGrpFindImg = FindGroupID(Convert.ToDateTime(pDate), itemRow.Cells["SHOW_ID"].Value.ToString(), "0");

                            for (int iG_img = 0; iG_img < dtGrpFindImg.Rows.Count; iG_img++)
                            {

                                DataTable dtData = FindImageInGroup(dtGrpFindImg.Rows[iG_img]["Grp_ID"].ToString(),
                                   itemRow.Cells["SHOW_ID"].Value.ToString(), pDate, bch_ID);
                                for (int iImage = 0; iImage < Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString()); iImage++)
                                {
                                    try
                                    {

                                        RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 2].Value =
                                            ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dtData.Rows[iImage]["FullName"].ToString());
                                        RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 3].Value = "1";
                                        RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 4].Value = dtData.Rows[iImage]["FileName"].ToString();
                                    }
                                    catch (Exception)
                                    {
                                        RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 3].Value = "0";
                                        RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 4].Value = "";
                                    }
                                    iCountColume += 3;
                                }
                            }
                        }
                    }

                }
            }

            //คอลัมที่ไม่มีข้อมูลให้ปิดออก
            int iC_Hide = 2;
            for (int iColume = 0; iColume < summaryRowItem.Count; iColume++)
            {
                //GridViewSummaryRowItem summaryRowItem12 = this.RadGridView_ShowHD.SummaryRowsTop[0];
                GridViewSummaryItem summaryItem12 = summaryRowItem[iColume];
                object summary = summaryItem12.Evaluate(this.RadGridView_ShowHD.MasterTemplate);
                if (summary.ToString() == "0")
                {
                    RadGridView_ShowHD.Columns[iC_Hide].IsVisible = false;
                }
                iC_Hide += 3;
            }

            this.Cursor = Cursors.Default;
        }
        //Set Grid For Export Excel
        void SetDGVForExportExcel()
        {
            this.Cursor = Cursors.WaitCursor;

            if (ListgridExport.Count > 0) { ListgridExport.Clear(); }

            RadGridView gridGrp;
            double intDay = Math.Ceiling((radDateTimePicker_End.Value - radDateTimePicker_Begin.Value).TotalDays) + 1;
            int i_Column = 0;
            string col_Express = "";

            //Add คอลัม
            foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
            {
                if (itemRow.Cells["C"].Value.ToString() == "1")
                {
                    string HeadColumn = itemRow.Cells["SHOW_ID"].Value.ToString();
                    string HeadText = itemRow.Cells["SHOW_DESC"].Value.ToString();

                    GridViewSummaryRowItem summaryRowItemExport = new GridViewSummaryRowItem();
                    summaryRowItemExport.Clear();
                    gridGrp = new RadGridView
                    {
                        Name = HeadText,
                    };

                    DatagridClass.SetDefaultRadGridView(gridGrp);

                    gridGrp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchID", "สาขา", 50)));
                    gridGrp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "ชื่อสาขา", 200)));

                    gridGrp.TableElement.TableHeaderHeight = 120;
                    gridGrp.TableElement.RowHeight = 200;

                    //สร้างคอลัมภ์ก่อน สำหรับแต่ละ sheet
                    for (int iD = 0; iD < intDay; iD++)
                    {
                        var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");
                        DataTable dtGrp = FindGroupID(Convert.ToDateTime(pDate), HeadColumn, "0");
                        if (dtGrp.Rows.Count > 0)
                        {
                            int r_Count = Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString());

                            for (int iG = 0; iG < dtGrp.Rows.Count; iG++)
                            {
                                string grpID = dtGrp.Rows[iG]["Grp_ID"].ToString();
                                string grpDesc = dtGrp.Rows[iG]["Grp_Desc"].ToString();

                                for (int i = 0; i < r_Count; i++)
                                {
                                    string HeadColumnImage = pDate + "_" + grpID + HeadColumn + "_I" + Convert.ToString(i + 1);
                                    string HeadColumnCount = pDate + "_" + grpID + HeadColumn + "_C" + Convert.ToString(i + 1);
                                    string HeadColumnPath = pDate + "_" + grpID + HeadColumn + "_P" + Convert.ToString(i + 1);
                                    string HeadTextDesc_Image = pDate + Environment.NewLine + grpDesc + Environment.NewLine + HeadText + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";
                                    string HeadTextDesc_Count = grpDesc + Environment.NewLine + HeadColumn + Environment.NewLine + "[" + Convert.ToString(i + 1) + "/" + r_Count.ToString() + "]";

                                    gridGrp.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnImage, HeadTextDesc_Image, 150)));
                                    gridGrp.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible(HeadColumnCount, HeadTextDesc_Count)));

                                    GridViewSummaryItem summaryItemExport = new GridViewSummaryItem(HeadColumnCount, "{0}", GridAggregateFunction.Sum);
                                    summaryRowItemExport.Add(summaryItemExport);

                                    if (col_Express == "")
                                    {
                                        col_Express += HeadColumnCount;
                                    }
                                    else
                                    {
                                        col_Express += "+" + HeadColumnCount;
                                    }

                                    i_Column += 2;
                                }
                            }
                        }

                    }

                    //////Add Data    //จำนวนวันที่ดึง
                    for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
                    {
                        string bch_ID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                        string bch_Name = dtBch.Rows[iBch]["BRANCH_NAME"].ToString();
                        gridGrp.Rows.Add(bch_ID, bch_Name);

                        int iCountColume = 0;

                        for (int iD = 0; iD < intDay; iD++)
                        {
                            var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");
                            DataTable dtGrpFindImg = FindGroupID(Convert.ToDateTime(pDate), itemRow.Cells["SHOW_ID"].Value.ToString(), "0");

                            for (int iG_img = 0; iG_img < dtGrpFindImg.Rows.Count; iG_img++)
                            {

                                DataTable dtData = FindImageInGroup(dtGrpFindImg.Rows[iG_img]["Grp_ID"].ToString(),
                                   itemRow.Cells["SHOW_ID"].Value.ToString(), pDate, bch_ID);
                                for (int iImage = 0; iImage < Convert.ToInt32(itemRow.Cells["MaxImage"].Value.ToString()); iImage++)
                                {
                                    try
                                    {

                                        gridGrp.Rows[iBch].Cells[iCountColume + 2].Value =
                                            ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dtData.Rows[iImage]["FullName"].ToString());
                                        gridGrp.Rows[iBch].Cells[iCountColume + 3].Value = 1;
                                    }
                                    catch (Exception) { }
                                    iCountColume += 2;
                                }
                            }
                        }
                    }

                    //คอลัมที่ไม่มีข้อมูลให้ปิดออก
                    int iC_Hide = 2;
                    for (int iColume = 2; iColume < summaryRowItemExport.Count; iColume++)
                    {
                        GridViewSummaryItem summaryItem12Export = summaryRowItemExport[iColume];
                        object summary = summaryItem12Export.Evaluate(gridGrp.MasterTemplate);
                        if (summary.ToString() == "0")
                        {
                            gridGrp.Columns[iC_Hide].IsVisible = false;
                        }
                        iC_Hide += 2;
                    }
                    ListgridExport.Add(gridGrp);
                }
            }
            this.Cursor = Cursors.Default;
        }

    }
}
