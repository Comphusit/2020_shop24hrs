﻿namespace PC_Shop24Hrs.GeneralForm.ImageShow.Vender
{
    partial class Vender_ToAX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vender_ToAX));
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_EmpID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmpName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Emp = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_InvoiceName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DatereceiveID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Datereceive = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_InvoiceID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_vender = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel_Invoice = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Invenlocation = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DuedateID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_InvoiceAmount = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_BranchEnter = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_PurchID = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_PrID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Amout = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Purch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Duedate = new Telerik.WinControls.UI.RadLabel();
            this.PrintDocument = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_InvoiceName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DatereceiveID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Datereceive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_InvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Invoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Invenlocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DuedateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_InvoiceAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PurchID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PrID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Amout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Purch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Duedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(623, 628);
            this.radGridView_Show.TabIndex = 6;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radLabel_EmpID);
            this.panel1.Controls.Add(this.radLabel_EmpName);
            this.panel1.Controls.Add(this.radLabel_Emp);
            this.panel1.Controls.Add(this.radLabel_InvoiceName);
            this.panel1.Controls.Add(this.radLabel_DatereceiveID);
            this.panel1.Controls.Add(this.radLabel_Datereceive);
            this.panel1.Controls.Add(this.radLabel_InvoiceID);
            this.panel1.Controls.Add(this.radLabel_vender);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.radLabel_Invoice);
            this.panel1.Controls.Add(this.radLabel_Invenlocation);
            this.panel1.Controls.Add(this.radLabel_DuedateID);
            this.panel1.Controls.Add(this.radLabel_InvoiceAmount);
            this.panel1.Controls.Add(this.radLabel_BranchEnter);
            this.panel1.Controls.Add(this.radLabel_PurchID);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radTextBox_PrID);
            this.panel1.Controls.Add(this.radLabel_Amout);
            this.panel1.Controls.Add(this.radLabel_Purch);
            this.panel1.Controls.Add(this.radLabel_Duedate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 2;
            // 
            // radLabel_EmpID
            // 
            this.radLabel_EmpID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmpID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmpID.Location = new System.Drawing.Point(58, 456);
            this.radLabel_EmpID.Name = "radLabel_EmpID";
            this.radLabel_EmpID.Size = new System.Drawing.Size(12, 19);
            this.radLabel_EmpID.TabIndex = 99;
            this.radLabel_EmpID.Text = "-";
            // 
            // radLabel_EmpName
            // 
            this.radLabel_EmpName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmpName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmpName.Location = new System.Drawing.Point(17, 481);
            this.radLabel_EmpName.Name = "radLabel_EmpName";
            this.radLabel_EmpName.Size = new System.Drawing.Size(12, 19);
            this.radLabel_EmpName.TabIndex = 98;
            this.radLabel_EmpName.Text = "-";
            // 
            // radLabel_Emp
            // 
            this.radLabel_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Emp.Location = new System.Drawing.Point(8, 456);
            this.radLabel_Emp.Name = "radLabel_Emp";
            this.radLabel_Emp.Size = new System.Drawing.Size(41, 19);
            this.radLabel_Emp.TabIndex = 97;
            this.radLabel_Emp.Text = "ผู้นับ :";
            // 
            // radLabel_InvoiceName
            // 
            this.radLabel_InvoiceName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_InvoiceName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_InvoiceName.Location = new System.Drawing.Point(17, 156);
            this.radLabel_InvoiceName.Name = "radLabel_InvoiceName";
            this.radLabel_InvoiceName.Size = new System.Drawing.Size(12, 19);
            this.radLabel_InvoiceName.TabIndex = 96;
            this.radLabel_InvoiceName.Text = "-";
            // 
            // radLabel_DatereceiveID
            // 
            this.radLabel_DatereceiveID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DatereceiveID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DatereceiveID.Location = new System.Drawing.Point(17, 381);
            this.radLabel_DatereceiveID.Name = "radLabel_DatereceiveID";
            this.radLabel_DatereceiveID.Size = new System.Drawing.Size(12, 19);
            this.radLabel_DatereceiveID.TabIndex = 95;
            this.radLabel_DatereceiveID.Text = "-";
            // 
            // radLabel_Datereceive
            // 
            this.radLabel_Datereceive.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Datereceive.Location = new System.Drawing.Point(8, 356);
            this.radLabel_Datereceive.Name = "radLabel_Datereceive";
            this.radLabel_Datereceive.Size = new System.Drawing.Size(54, 19);
            this.radLabel_Datereceive.TabIndex = 94;
            this.radLabel_Datereceive.Text = "วันที่รับ :";
            // 
            // radLabel_InvoiceID
            // 
            this.radLabel_InvoiceID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_InvoiceID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_InvoiceID.Location = new System.Drawing.Point(17, 331);
            this.radLabel_InvoiceID.Name = "radLabel_InvoiceID";
            this.radLabel_InvoiceID.Size = new System.Drawing.Size(12, 19);
            this.radLabel_InvoiceID.TabIndex = 93;
            this.radLabel_InvoiceID.Text = "-";
            // 
            // radLabel_vender
            // 
            this.radLabel_vender.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_vender.Location = new System.Drawing.Point(8, 306);
            this.radLabel_vender.Name = "radLabel_vender";
            this.radLabel_vender.Size = new System.Drawing.Size(117, 19);
            this.radLabel_vender.TabIndex = 92;
            this.radLabel_vender.Text = "เลขที่บิลผู้จำหน่าย :";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator1,
            this.radButtonElement_print,
            this.commandBarSeparator2,
            this.radButtonElement_Edit,
            this.commandBarSeparator3,
            this.RadButtonElement_pdf,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(192, 36);
            this.radStatusStrip1.TabIndex = 91;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_print
            // 
            this.radButtonElement_print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_print.Name = "radButtonElement_print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_print, false);
            this.radButtonElement_print.Text = "radButtonElement1";
            this.radButtonElement_print.UseCompatibleTextRendering = false;
            this.radButtonElement_print.Click += new System.EventHandler(this.RadButtonElement_print_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement1";
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButton_Edit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdf
            // 
            this.RadButtonElement_pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdf.Name = "RadButtonElement_pdf";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdf, false);
            this.RadButtonElement_pdf.Text = "radButtonElement1";
            this.RadButtonElement_pdf.Click += new System.EventHandler(this.RadButtonElement_pdf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radLabel_Invoice
            // 
            this.radLabel_Invoice.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Invoice.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Invoice.Location = new System.Drawing.Point(8, 131);
            this.radLabel_Invoice.Name = "radLabel_Invoice";
            this.radLabel_Invoice.Size = new System.Drawing.Size(59, 19);
            this.radLabel_Invoice.TabIndex = 90;
            this.radLabel_Invoice.Text = "V000001";
            // 
            // radLabel_Invenlocation
            // 
            this.radLabel_Invenlocation.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Invenlocation.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Invenlocation.Location = new System.Drawing.Point(8, 181);
            this.radLabel_Invenlocation.Name = "radLabel_Invenlocation";
            this.radLabel_Invenlocation.Size = new System.Drawing.Size(48, 19);
            this.radLabel_Invenlocation.TabIndex = 89;
            this.radLabel_Invenlocation.Text = "MN000";
            // 
            // radLabel_DuedateID
            // 
            this.radLabel_DuedateID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DuedateID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DuedateID.Location = new System.Drawing.Point(17, 431);
            this.radLabel_DuedateID.Name = "radLabel_DuedateID";
            this.radLabel_DuedateID.Size = new System.Drawing.Size(12, 19);
            this.radLabel_DuedateID.TabIndex = 88;
            this.radLabel_DuedateID.Text = "-";
            // 
            // radLabel_InvoiceAmount
            // 
            this.radLabel_InvoiceAmount.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_InvoiceAmount.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_InvoiceAmount.Location = new System.Drawing.Point(17, 281);
            this.radLabel_InvoiceAmount.Name = "radLabel_InvoiceAmount";
            this.radLabel_InvoiceAmount.Size = new System.Drawing.Size(12, 19);
            this.radLabel_InvoiceAmount.TabIndex = 88;
            this.radLabel_InvoiceAmount.Text = "-";
            // 
            // radLabel_BranchEnter
            // 
            this.radLabel_BranchEnter.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_BranchEnter.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_BranchEnter.Location = new System.Drawing.Point(8, 54);
            this.radLabel_BranchEnter.Name = "radLabel_BranchEnter";
            this.radLabel_BranchEnter.Size = new System.Drawing.Size(140, 35);
            this.radLabel_BranchEnter.TabIndex = 82;
            this.radLabel_BranchEnter.Text = "ระบุเลขที่ PR/PN\r\nให้เรียบร้อบก่อน [Enter]";
            // 
            // radLabel_PurchID
            // 
            this.radLabel_PurchID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_PurchID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_PurchID.Location = new System.Drawing.Point(17, 231);
            this.radLabel_PurchID.Name = "radLabel_PurchID";
            this.radLabel_PurchID.Size = new System.Drawing.Size(12, 19);
            this.radLabel_PurchID.TabIndex = 87;
            this.radLabel_PurchID.Text = "-";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(8, 525);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(175, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_PrID
            // 
            this.radTextBox_PrID.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_PrID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_PrID.Location = new System.Drawing.Point(8, 95);
            this.radTextBox_PrID.Name = "radTextBox_PrID";
            this.radTextBox_PrID.Size = new System.Drawing.Size(112, 21);
            this.radTextBox_PrID.TabIndex = 1;
            this.radTextBox_PrID.Tag = "เลขที่เอกสาร";
            this.radTextBox_PrID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_PrID_KeyDown);
            // 
            // radLabel_Amout
            // 
            this.radLabel_Amout.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Amout.Location = new System.Drawing.Point(8, 256);
            this.radLabel_Amout.Name = "radLabel_Amout";
            this.radLabel_Amout.Size = new System.Drawing.Size(60, 19);
            this.radLabel_Amout.TabIndex = 85;
            this.radLabel_Amout.Text = "ยอดรวม :";
            // 
            // radLabel_Purch
            // 
            this.radLabel_Purch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Purch.Location = new System.Drawing.Point(8, 206);
            this.radLabel_Purch.Name = "radLabel_Purch";
            this.radLabel_Purch.Size = new System.Drawing.Size(62, 19);
            this.radLabel_Purch.TabIndex = 82;
            this.radLabel_Purch.Text = "ใบสั่งซื้อ :";
            // 
            // radLabel_Duedate
            // 
            this.radLabel_Duedate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Duedate.Location = new System.Drawing.Point(8, 406);
            this.radLabel_Duedate.Name = "radLabel_Duedate";
            this.radLabel_Duedate.Size = new System.Drawing.Size(74, 19);
            this.radLabel_Duedate.TabIndex = 84;
            this.radLabel_Duedate.Text = "DUEDATE :";
            // 
            // PrintDocument
            // 
            this.PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_PrintPage);
            // 
            // Vender_ToAX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "Vender_ToAX";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "จับคู่บิลผู้จำหน่าย";
            this.Load += new System.EventHandler(this.Vender_ToAX_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_InvoiceName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DatereceiveID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Datereceive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_InvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Invoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Invenlocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DuedateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_InvoiceAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_BranchEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PurchID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PrID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Amout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Purch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Duedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_BranchEnter;
        private Telerik.WinControls.UI.RadLabel radLabel_PurchID;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        private Telerik.WinControls.UI.RadTextBox radTextBox_PrID;
        private Telerik.WinControls.UI.RadLabel radLabel_Amout;
        private Telerik.WinControls.UI.RadLabel radLabel_Purch;
        private Telerik.WinControls.UI.RadLabel radLabel_Duedate;
        private Telerik.WinControls.UI.RadLabel radLabel_DuedateID;
        private Telerik.WinControls.UI.RadLabel radLabel_InvoiceAmount;
        private System.Drawing.Printing.PrintDocument PrintDocument;
        private Telerik.WinControls.UI.RadLabel radLabel_Invoice;
        private Telerik.WinControls.UI.RadLabel radLabel_Invenlocation;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadLabel radLabel_DatereceiveID;
        private Telerik.WinControls.UI.RadLabel radLabel_Datereceive;
        private Telerik.WinControls.UI.RadLabel radLabel_InvoiceID;
        private Telerik.WinControls.UI.RadLabel radLabel_vender;
        private Telerik.WinControls.UI.RadLabel radLabel_InvoiceName;
        private Telerik.WinControls.UI.RadLabel radLabel_EmpName;
        private Telerik.WinControls.UI.RadLabel radLabel_Emp;
        private Telerik.WinControls.UI.RadLabel radLabel_EmpID;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
    }
}
