﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;
using System.Collections.Generic;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.Vender
{
    public partial class ImageVender_Show : Telerik.WinControls.UI.RadForm
    {
        private DataTable dtBch = new DataTable();
        //readonly string _pTypeOpen;// SHOP-SUPC
        readonly string _pPermission;

        string i_VenderID;
        string i_VenderName;
        string i_VenderImage;

        int iC;
        string dateBeginLoad;
        //Load
        public ImageVender_Show(string pPermission)
        {

            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load Main
        private void ImageVender_Show_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            RadButton_Search.ButtonElement.ShowBorder = true;
            radButtonElement_excel.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_Clear.ShowBorder = true; radButtonElement_Clear.ToolTipText = "ล้างข้อมูล";
            radButtonElement_MA.ShowBorder = true; radButtonElement_MA.ToolTipText = "บันทึกรับบิล MA";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(radGridView_Group);

            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("C", "/", 40));
            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 70));
            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ชื่อ", 250));
            radGridView_Group.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MaxImage", "รูป", 50));
            radGridView_Group.MasterTemplate.Columns["C"].IsPinned = true;

            ClearData();
        }
        void ClearData()
        {
            try
            {
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear();
                    RadGridView_ShowHD.Rows.Clear();
                }
            }
            catch (Exception) { }
            radGridView_Group.MasterTemplate.FilterDescriptors.Clear();

            radDateTimePicker_Begin.Visible = true; radDateTimePicker_End.Visible = true;

            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-3);
            radDateTimePicker_End.Value = DateTime.Now;

            radLabel_F2.Text = "สีแดง >> ยังไม่นับ | สีฟ้า >> นับแล้วยังไม่พิมพ์แต่มี PR-PN | สีชมพู >> นับแล้วยังไม่ทำ PR-PN | สีอ่อน >> พิมพ์เรียบร้อย | กด / >> เพื่อเข้ารับบิล";

            RadButton_Search.Enabled = true;
            radGridView_Group.Enabled = true;

            iC = 0; i_VenderName = ""; i_VenderID = ""; i_VenderImage = "";

            SetGroup();

        }
        //load Group
        void SetGroup()
        {
            dateBeginLoad = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            DataTable dt = Vender_ToAXClass.GetVenderAll_ForSend(""); 
            radGridView_Group.DataSource = dt;
            dt.AcceptChanges();
        }
      
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_ShowHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Group_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        //Date Change
        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);

        }
        //Date Change
        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        #endregion

        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("Detail", RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //ค้นหา
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            LoadDataCountImage();
        }
        //LoadData
        void LoadDataCountImage()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchID", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "ชื่อสาขา", 200)));
            RadGridView_ShowHD.Columns["pBchID"].IsPinned = true;
            RadGridView_ShowHD.Columns["pBchName"].IsPinned = true;
            RadGridView_ShowHD.TableElement.TableHeaderHeight = 100;
            //*************//
            double intDay = Math.Ceiling((radDateTimePicker_End.Value.Date - radDateTimePicker_Begin.Value.Date).TotalDays) + 1;
            if (intDay == 0) intDay = 1;

            //เพิ่มคอลัม
            int i_Column = 0;

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                var pDate = radDateTimePicker_Begin.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                foreach (GridViewRowInfo itemRow in radGridView_Group.Rows)
                {
                    if (itemRow.Cells["C"].Value.ToString() == "1")
                    {
                        string HeadColumnCount = itemRow.Cells["SHOW_ID"].Value.ToString() + "_" + pDate.Replace("-", "V");
                        string HeadTextDesc_Count = pDate + Environment.NewLine + itemRow.Cells["SHOW_NAME"].Value.ToString() +
                            Environment.NewLine + "[" + itemRow.Cells["MaxImage"].Value.ToString() + "]";

                        string HeadColumnCountC = itemRow.Cells["SHOW_ID"].Value.ToString() + "_" + pDate.Replace("-", "C");

                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, HeadTextDesc_Count, 150)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible(HeadColumnCountC, HeadTextDesc_Count)));

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                             new ExpressionFormattingObject("MyCondition2", HeadColumnCountC + " = 0 AND " + HeadColumnCount + " > 0", false)
                             { CellBackColor = ConfigClass.SetColor_Red() });

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                            new ExpressionFormattingObject("MyCondition2", HeadColumnCountC + " = 1 AND " + HeadColumnCount + " > 0", false)
                            { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                            new ExpressionFormattingObject("MyCondition2", HeadColumnCountC + " = 3 AND " + HeadColumnCount + " > 0", false)
                            { CellBackColor = ConfigClass.SetColor_PinkPastel() });

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                           new ExpressionFormattingObject("MyCondition2", HeadColumnCountC + " = 2 AND " + HeadColumnCount + " > 0", false)
                           { CellBackColor = ConfigClass.SetColor_Wheat() });


                        i_Column += 1;
                        i_VenderID = itemRow.Cells["SHOW_ID"].Value.ToString();
                        i_VenderName = itemRow.Cells["SHOW_NAME"].Value.ToString();
                        i_VenderImage = itemRow.Cells["MaxImage"].Value.ToString();
                    }
                }
            }

            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ผู้จำหน่าย");
                this.Cursor = Cursors.Default;
                return;
            }

            //SetBranchByVender(i_VenderID);
            dtBch = Vender_ToAXClass.GetVenderAll_ForSend(i_VenderID);

            ////Add Data    //จำนวนวันที่ดึง
            for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
            {
                string bch_ID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                string bch_Name = dtBch.Rows[iBch]["BRANCH_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(bch_ID, bch_Name);

                int iCountColume = 2;
                for (int iD = 0; iD < intDay; iD++)
                {
                    var pDate = radDateTimePicker_Begin.Value.AddDays(iD).ToString("yyyy-MM-dd");

                    RadGridView_ShowHD.Rows[iBch].Cells[iCountColume].Value = CountImageInGroup(pDate, bch_ID, i_VenderID).ToString();
                    RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 1].Value = CheckData(pDate, bch_ID, i_VenderID).ToString();
                    iCountColume += 2;

                }

            }
            RadButton_Search.Enabled = false;
            radGridView_Group.Enabled = false;
            this.Cursor = Cursors.Default;
        }

        //FindDataInGroup
        int CountImageInGroup(String pDate, String pBch, string pVenderID)
        {
            string path = PathImageClass.pPathVender + pDate + @"\" + pBch + "";
            DirectoryInfo DirInfo = new DirectoryInfo(path);
            if (DirInfo.Exists == false) return 0;
            else
            {
                FileInfo[] Files = DirInfo.GetFiles("*" + pVenderID + @"#*.JPG", SearchOption.AllDirectories);
                return Files.Length;
            }
        }
        //ดึงข้อมูลต่างๆ เพื่อประกบสี
        int CheckData(string pDate, string pBchID, string pVender)
        {
            List<ItemCheck> Lists = Vender_ToAXClass.GetVenderCheck(pVender, pBchID, pDate);
            if (Lists.Count == 0) return 0;
 
            DataTable dtPN = Vender_ToAXClass.GetDataPRFormShop_VendorPNByLedgervoucher("", pVender, pBchID, pDate);
            if (dtPN.Rows.Count > 0) return 2;
 
            DataTable dtAX704 = PurchClass.VenderAX_GetDataPR(IpServerConnectClass.ConMainReportAX, pBchID, pDate, pVender, "");
            if (dtAX704.Rows.Count > 0) return 1;
            else return 3;

        }
        //Choose Group
        private void RadGridView_Group_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (e.Column.Name)
            {
                case "C":
                    if (radGridView_Group.CurrentRow.Cells["C"].Value.ToString() == "1")
                    {
                        radGridView_Group.CurrentRow.Cells["C"].Value = "0";
                        iC = 0;
                    }
                    else
                    {
                        if (iC == 1)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("มีการเลือกผู้จำหน่ายค้างอยู่ ไม่สามารถเลือกซ้ำได้ " + Environment.NewLine + "[ เลือกผู้จำหน่ายเดิมออกก่อนแล้วเลือกใหม่อีกครั้ง ]");
                            return;
                        }

                        radGridView_Group.CurrentRow.Cells["C"].Value = "1";
                        iC = 1;
                    }
                    break;

                default:
                    break;
            }
        }
        //Big Image
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }


            int iK1 = RadGridView_ShowHD.CurrentCell.ColumnIndex;
            if (RadGridView_ShowHD.CurrentRow.Cells[iK1 + 1].Value.ToString() == "1")
            {
                ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells[iK1 + 1].Value.ToString(),
                       RadGridView_ShowHD.CurrentRow.Cells[iK1 + 2].Value.ToString());
            }


        }
        //Claer
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Delete
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {


        }

        private void RadButtonElement_MA_Click(object sender, EventArgs e)
        {
            if (i_VenderID == "") return;

            try
            {
                int CImage = int.Parse(RadGridView_ShowHD.CurrentCell.Value.ToString());

                string[] date = RadGridView_ShowHD.CurrentCell.ColumnInfo.HeaderText.ToString().Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                ImageVender_Counter _frmCounter = new ImageVender_Counter(i_VenderID, i_VenderName,
                    date[0],
                    RadGridView_ShowHD.CurrentRow.Cells["pBchID"].Value.ToString(),
                    RadGridView_ShowHD.CurrentRow.Cells["pBchName"].Value.ToString(),
                    i_VenderImage, _pPermission);
                if (_frmCounter.ShowDialog(this) == DialogResult.Yes)
                {

                }
                LoadDataCountImage();
            }
            catch (Exception)
            {
                return;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
