﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.Vender
{
    public partial class Vender_ToAX : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        DataTable dt_DataGridShow = new DataTable();
        readonly Invoice invoice;
        readonly string __pPermission;
        int ItemCheckReceive;
        //Load
        public Vender_ToAX(string branchid, string branchname, string datereceive, string invoiceaccount, string invoiceaccountname, string pPermission)
        {
            InitializeComponent();
            this.KeyPreview = true;
            invoice = new Invoice()
            {
                RECEIVEDATE = datereceive
                ,
                INVENTLOCATIONID = branchid
                ,
                LOCATIONNAME = branchname
                ,
                INVOICEACCOUNT = invoiceaccount
                ,
                INVOICENAME = invoiceaccountname
            };
            __pPermission = pPermission;
        }
        private void Vender_ToAX_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdf.ShowBorder = true; RadButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์บิล";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขบิล";
            radButton_Save.ButtonElement.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;
            radGridView_Show.TableElement.RowHeight = 35;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.TableElement.MasterTemplate.EnableFiltering = false;
            radTextBox_PrID.Font = SystemClass.SetFontGernaral;
            this.Text = "บันทึก PR/PN ";
            this.SetDataStringEmpty();

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODE", "บาร์โค้ด", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTQTY", "จำนวน[ชิ้น]"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PURCHUNIT", "หน่วย", 100));
            dt_DataGridShow = AddNewColumn(new string[] { "ITEMID", "INVENTDIMID", "SPC_ITEMBARCODE", "NAME", "QTY", "INVENTQTY", "PURCHUNIT" }, new string[] { "SPC_ITEMBARCODE" });
            this.CreateDataTable();
            //จัดซื้อ
            if (__pPermission.Equals("0"))
            {
                radButtonElement_print.Enabled = false;
                radButtonElement_Edit.Enabled = false;
                radButton_Save.Enabled = false;
                radTextBox_PrID.Enabled = false;
            }
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion
        #region Method
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null)
            {
                return newColumn;
            }
            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];

                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }
            return newColumn;
        }
        private void SetDataStringEmpty()
        {
            radLabel_DuedateID.Text = string.Empty;
            radLabel_PurchID.Text = string.Empty;
            radLabel_InvoiceAmount.Text = string.Empty;
            radLabel_Invenlocation.Text = string.Empty;
            radLabel_Invoice.Text = string.Empty;
            radTextBox_PrID.Text = string.Empty;
            radLabel_InvoiceName.Text = string.Empty;
            radLabel_InvoiceID.Text = string.Empty;
            radLabel_DatereceiveID.Text = string.Empty;
            radTextBox_PrID.Focus();
            radTextBox_PrID.Enabled = true;
            radButton_Save.Enabled = false;
            radButtonElement_print.Enabled = false;
            radButtonElement_Edit.Enabled = false;

            invoice.INVOICID = "";
            invoice.LEDGERVOUCHER = "";
            invoice.PURCHID = "";
            invoice.DUEDATE = "";
            invoice.InvoiceAmount = 0;
        }
        private void CreateDataTable()
        {
            DataTable dt_pr = Vender_ToAXClass.GetDataPRFormShop_VendorPN(invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICEACCOUNT);
            if (dt_pr.Rows.Count == 0)
            {
                List<ItemCheck> Lists = Vender_ToAXClass.GetVenderCheck(invoice.INVOICEACCOUNT, invoice.INVENTLOCATIONID, invoice.RECEIVEDATE);
                ItemCheckReceive = Lists.Count;
                if (dt_DataGridShow.Rows.Count > 0) { dt_DataGridShow.Rows.Clear(); }
                if (radGridView_Show.Rows.Count > 0) { radGridView_Show.Rows.Clear(); }
                foreach (var list in Lists)
                {
                    dt_DataGridShow.Rows.Add(list.ITEMID
                        , list.INVENTDIMID
                        , list.SPC_ITEMBARCODE
                        , list.NAME
                        , list.QTY.ToString("#,#0.00")
                        , list.INVENTQTY.ToString("#,#0.00")
                        , list.PURCHUNIT);

                    radLabel_EmpID.Text = list.ReciveWhoIns;
                    radLabel_EmpName.Text = list.ReciveWhoNameIns;
                }

                this.radGridView_Show.DataSource = dt_DataGridShow;
                dt_pr = PurchClass.VenderAX_GetDataPR(IpServerConnectClass.ConMainReportAX, invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICEACCOUNT);  //Auto
                if (dt_pr.Rows.Count > 0)
                {
                    this.CreateDataGrid(dt_pr);
                    radButtonElement_print.Enabled = false;
                    radButton_Save.Enabled = true;
                }
                else
                {
                    this.SetDataStringEmpty();
                }
            }
            else
            {
                this.CreateDataGrid(dt_pr);
                radButtonElement_print.Enabled = true;
                radButton_Save.Enabled = false;
                radTextBox_PrID.Enabled = false;
            }
            //เช็คว่าบิลยกเลิกหรือยัง ถ้ายังไม่อนุญิติให้แก้ไข
            if (PurchClass.VenderAX_GetDataPRCheckCancle(radTextBox_PrID.Text.Trim(), IpServerConnectClass.ConMainReportAX))
            {
                radButtonElement_Edit.Enabled = true;
            }
            else if (PurchClass.VenderAX_GetDataPRCheckCancle(radTextBox_PrID.Text.Trim(), IpServerConnectClass.ConMainAX))
            {
                radButtonElement_Edit.Enabled = true;
            }
            else if (Vender_ToAXClass.CheckReceiveInvoice(invoice.INVOICEACCOUNT, invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICID))
            {
                radButtonElement_Edit.Enabled = true;
            }
            else
            {
                radButtonElement_Edit.Enabled = false;
            }
        }
        private void CreateDataGrid(DataTable _dt_pr)
        {
            if (dt_DataGridShow.Rows.Count > 0) { dt_DataGridShow.Rows.Clear(); dt_DataGridShow.AcceptChanges(); }

            foreach (DataRow row in _dt_pr.Rows)
            {
                dt_DataGridShow.Rows.Add(row["ITEMID"].ToString()
                    , row["INVENTDIMID"].ToString()
                    , row["SPC_ITEMBARCODE"].ToString()
                    , row["NAME"].ToString()
                    , Convert.ToDouble(row["QTY"]).ToString("#,#0.00")
                    , Convert.ToDouble(row["INVENTQTY"]).ToString("#,#0.00")
                    , row["PURCHUNIT"]);
            }
            radLabel_DuedateID.Text = Convert.ToDateTime(_dt_pr.Rows[0]["DUEDATE"]).ToString("yyyy-MM-dd");
            radLabel_PurchID.Text = _dt_pr.Rows[0]["PURCHID"].ToString();
            radLabel_InvoiceAmount.Text = Convert.ToDouble(_dt_pr.Rows[0]["InvoiceAmount"]).ToString("#,#0.00");
            radLabel_Invoice.Text = _dt_pr.Rows[0]["INVOICEACCOUNT"].ToString();
            radLabel_InvoiceID.Text = _dt_pr.Rows[0]["INVOICEID"].ToString();
            radTextBox_PrID.Text = _dt_pr.Rows[0]["LEDGERVOUCHER"].ToString();
            radLabel_Invenlocation.Text = invoice.LOCATIONNAME;
            radLabel_InvoiceName.Text = invoice.INVOICENAME;
            radLabel_DatereceiveID.Text = invoice.RECEIVEDATE;

            if (radLabel_EmpID.Text.Equals("-") || string.IsNullOrEmpty(radLabel_EmpID.Text))
            {
                radLabel_EmpID.Text = _dt_pr.Rows[0]["WhoIns"].ToString();
                radLabel_EmpName.Text = _dt_pr.Rows[0]["WhoInsName"].ToString();
            }

            invoice.INVOICID = _dt_pr.Rows[0]["INVOICEID"].ToString();
            invoice.LEDGERVOUCHER = _dt_pr.Rows[0]["LEDGERVOUCHER"].ToString();
            invoice.PURCHID = _dt_pr.Rows[0]["PURCHID"].ToString();
            invoice.DUEDATE = _dt_pr.Rows[0]["DUEDATE"].ToString();
            invoice.InvoiceAmount = Convert.ToDouble(_dt_pr.Rows[0]["InvoiceAmount"]);
            this.radGridView_Show.DataSource = dt_DataGridShow;
        }
        #endregion
        #region Event
        private void RadTextBox_PrID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // DataTable datatable = new DataTable();
                DataTable dt_prKey = PurchClass.VenderAX_GetDataPR(IpServerConnectClass.ConMainReportAX, invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICEACCOUNT, radTextBox_PrID.Text);
                if (dt_prKey.Rows.Count == 0)
                {
                    dt_prKey = PurchClass.VenderAX_GetDataPR(IpServerConnectClass.ConMainAX, invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICEACCOUNT, radTextBox_PrID.Text);
                }

                if (dt_prKey.Rows.Count > 0)
                {
                    if (!dt_prKey.Rows[0]["INVENTLOCATIONID"].ToString().Equals(invoice.INVENTLOCATIONID))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("บิลที่ระบุไม่ตรงกับสาขาที่เลือกไว้ เช็คข้อมูลใหม่อีกครั้ง.");
                        this.SetDataStringEmpty();
                        radTextBox_PrID.Focus(); radTextBox_PrID.SelectAll();
                    }
                    else if (!dt_prKey.Rows[0]["INVOICEACCOUNT"].ToString().Equals(invoice.INVOICEACCOUNT))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("บิลที่ระบุไม่ตรงกับผู้จำหน่ายที่เลือกไว้ เช็คข้อมูลใหม่อีกครั้ง.");
                        this.SetDataStringEmpty();
                        radTextBox_PrID.Focus(); radTextBox_PrID.SelectAll();
                    }
                    //เช็ควันที่ DUEDATE ก่อนเพิ่มบิลา
                    else if (Convert.ToDateTime(dt_prKey.Rows[0]["DUEDATE"].ToString()) < Convert.ToDateTime(invoice.RECEIVEDATE))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("วันที่รับ เลยวันที่บิล เช็คบิลใหม่อีกครั้ง.");
                        this.SetDataStringEmpty();
                        radTextBox_PrID.Focus(); radTextBox_PrID.SelectAll();
                    }
                    else
                    {
                        DataTable dtCheck = Vender_ToAXClass.GetDataPRFormShop_VendorPNByLedgervoucher(radTextBox_PrID.Text.ToUpper(), "", "", "");
                        if (dtCheck.Rows.Count == 0)
                        {
                            this.CreateDataGrid(dt_prKey);
                            radButton_Save.Enabled = true;
                            radButtonElement_print.Enabled = false;
                        }
                        else
                        {
                            if (dtCheck.Rows[0]["INVENTLOCATIONID"].ToString().Equals(invoice.INVENTLOCATIONID)
                                && dtCheck.Rows[0]["INVOICEACCOUNT"].ToString().Equals(invoice.INVOICEACCOUNT)
                                && Convert.ToDateTime(dtCheck.Rows[0]["RECIVEDATE"]).ToString("yyyy-MM-dd").Equals(invoice.RECEIVEDATE))
                            {
                                this.CreateDataGrid(dt_prKey);
                                radButton_Save.Enabled = false;
                                radButtonElement_print.Enabled = true;
                                //radButtonElement_Edit.Enabled = true;
                            }
                            else
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("เลขที่เอกสารมีการบันทึกไปแล้ว เช็คเลขที่ใหม่อีกครั้ง.");
                                this.SetDataStringEmpty();
                                radTextBox_PrID.Focus(); radTextBox_PrID.SelectAll();
                            }
                        }
                    }
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ค้นหาเลขที่เอกสารที่ไม่ระบุไม่เจอ เช็คเลขที่ใหม่อีกครั้ง.");
                    radTextBox_PrID.Focus(); radTextBox_PrID.SelectAll();
                }
            }
        }
        private void RadButtonElement_print_Click(object sender, EventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            if (printDlg.ShowDialog(this) == DialogResult.OK)
            {
                PrintDocument.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDlg.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument.PrinterSettings = printDlg.PrinterSettings;
                PrintDocument.Print();
            }
        }
        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            DataTable dt = PurchClass.VenderAX_GetdataPrint(invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICEACCOUNT, invoice.LEDGERVOUCHER);
            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลที่ระบุ เช็คเงื่อนไขใหม่อีกครั้ง.");
                return;
            }
            List<ItemReceive> list = new List<ItemReceive>();
            int CountDiff = 0;

            foreach (DataRow row in dt.Rows)
            {
                if (Convert.ToDouble(row["QTY"]) != Convert.ToDouble(row["QTY_BRANCH"]) || Convert.ToDouble(row["QTY_BRANCH"]) != Convert.ToDouble(row["QTY_SUPC"]))
                {
                    CountDiff += 1;
                    list = new List<ItemReceive>() {
                           new ItemReceive() { SPC_ITEMBARCODE = row["SPC_ITEMBARCODE"].ToString(),
                                                NAME = row["NAME"].ToString(),
                                                QTY = Convert.ToDouble(row["QTY"]),
                                                QTY_BRANCH = Convert.ToDouble(row["QTY_BRANCH"]),
                                                QTY_SUPC = Convert.ToDouble(row["QTY_SUPC"]),
                                                PURCHUNIT = row["PURCHUNIT"].ToString() }
                           };
                }

                if (string.IsNullOrEmpty(radLabel_EmpID.Text) || radLabel_EmpID.Text.Equals("-"))
                {
                    radLabel_EmpID.Text = row["ReciveWhoIns"].ToString();
                    radLabel_EmpName.Text = row["SPC_NAME"].ToString();
                }
            }

            int Y = 0;
            e.Graphics.DrawString("รับสินค้าผู้จำหน่ายส่งเอง", SystemClass.printFont15, Brushes.Black, 1, Y);
            Y += 30;
            e.Graphics.DrawString(invoice.INVOICEACCOUNT + " " + dt.Rows[0]["INVOICEACCOUNTNAME"].ToString(), SystemClass.printFont15, Brushes.Black, 1, Y);
            Y += 30;
            e.Graphics.DrawString(dt.Rows[0]["BRANCH_ID"].ToString().ToUpper() + "-" + dt.Rows[0]["BRANCH_NAME"].ToString(), SystemClass.printFont15, Brushes.Black, 1, Y);
            Y += 25;
            e.Graphics.DrawString("--------------------------------------------------", SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("เลขที่ใบแจ้งหนี้ : " + dt.Rows[0]["LEDGERVOUCHER"].ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("เลขที่ใบสั่งซื้อ : " + dt.Rows[0]["PURCHID"].ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("ยอดทั้งบิล (บาท) : " + Convert.ToDouble(dt.Rows[0]["InvoiceAmount"]).ToString("#,#0.00"), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("บิลผู้จำหน่าย : " + dt.Rows[0]["ReciveInvoice"].ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("วันที่รับสินค้า : " + invoice.RECEIVEDATE, SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("DUEDATE : " + Convert.ToDateTime(invoice.DUEDATE).ToString("yyyy-MM-dd"), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("พิมพ์ " + DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.ToString("hh:mm:ss"), SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("--------------------------------------------------", SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("รหัสพนักงานนับ  : " + radLabel_EmpID.Text, SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString(radLabel_EmpName.Text, SystemClass.SetFont12, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("--------------------------------------------------", SystemClass.printFont15, Brushes.Black, 0, Y);

            if (CountDiff == 0)
            {
                Y += 30;
                e.Graphics.DrawString("รับสินค้าครบทุกรายการ", SystemClass.SetFont12, Brushes.Black, 0, Y);
                Y += 20;
                e.Graphics.DrawString("จำนวนรายการทั้งหมด " + dt.Rows.Count.ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            }
            else
            {
                Y += 20;
                e.Graphics.DrawString("จำนวนรายการทั้งหมด " + dt.Rows.Count.ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
                Y += 15;
                int count = 0;
                foreach (var tmp in list)
                {
                    Y += 15;
                    e.Graphics.DrawString((count + 1).ToString() + "." + tmp.SPC_ITEMBARCODE + " " + tmp.NAME, SystemClass.SetFontGernaral, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("จำนวนในบิล : " + tmp.QTY + "   " + tmp.PURCHUNIT, SystemClass.SetFontGernaral, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("จำนวนสาขา : " + tmp.QTY_BRANCH + "   " + tmp.PURCHUNIT, SystemClass.SetFontGernaral, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("จำนวนในรูป : " + tmp.QTY_SUPC + "   " + tmp.PURCHUNIT, SystemClass.SetFontGernaral, Brushes.Black, 0, Y);
                }
            }
            Y += 25;
            e.Graphics.DrawString("--------------------------------------------------", SystemClass.SetFont12, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกข้อมูล [ไม่สามารถแก้ไขข้อมูลได้ทุกกรณี].") == DialogResult.Yes)
            {
                string ret = Vender_ToAXClass.InsertDataToShop_VenderPN(dt_DataGridShow, invoice);
                MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                if (ret.Equals(""))
                {
                    this.CreateDataGrid(Vender_ToAXClass.GetDataPRFormShop_VendorPN(invoice.INVENTLOCATIONID, invoice.RECEIVEDATE, invoice.INVOICEACCOUNT));
                    radButtonElement_print.Enabled = true;
                    //radButtonElement_Edit.Enabled = true;
                    radButton_Save.Enabled = false;
                    radTextBox_PrID.Enabled = false;
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
        private void RadButton_Edit_Click(object sender, EventArgs e)
        {
            //ต้องการแก้ไขบิลกรณีบันทึกไปแล้ว จะลบบิลเก่า
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการแก้ไขบิล บิลที่มีการบันทึกไปแล้วจะถูกลบออกจากระบบ.") == DialogResult.Yes)
            {
                if (!string.IsNullOrEmpty(radTextBox_PrID.Text))
                {
                    string ret = Vender_ToAXClass.DeletDataShop_VenderPN(radTextBox_PrID.Text.Trim());
                    MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                    if (ret.Equals(""))
                    {
                        this.SetDataStringEmpty();
                        this.CreateDataTable();
                    }
                }
            }
            else
            {
                return;
            }
        }
        #endregion

        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
