﻿//CheckOK
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.Vender
{
    class Vender_ToAXClass
    {
        //ค้นหา ผู้จำหน่ายที่ลงของสาขา
        public static DataTable GetVenderAll_ForSend(string pVender)
        {
            string conditionBch = "", sql;
            if (SystemClass.SystemBranchID != "MN000")
            {
                if (pVender == "")
                    conditionBch = $@" 
                        INNER JOIN  (SELECT	SHOW_ID FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) 
                            WHERE	BRANCH_ID = '{SystemClass.SystemBranchID}' AND STATUS = '1' AND TYPE_CONFIG = '2')BCH   ON SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = BCH.SHOW_ID ";

                else conditionBch = $@" AND SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = '{SystemClass.SystemBranchID }' ";
            }

            if (pVender == "")
                sql = $@"
                        SELECT	'0' AS 'C',SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_NAME,MaxImage
                        FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) {conditionBch}
                        WHERE	SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '2' AND SHOP_CONFIGBRANCH_GenaralDetail.STA = '1'
                        GROUP BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_NAME,MaxImage
                        ORDER BY SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID,SHOP_CONFIGBRANCH_GenaralDetail.SHOW_NAME";
            else
                sql = $@"
                        SELECT	SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID,BRANCH_NAME
                        FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) INNER JOIN SHOP_BRANCH WITH (NOLOCK)
		                        ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                        WHERE	STATUS = '1' AND TYPE_CONFIG = '2' AND SHOW_ID = '{pVender}'  {conditionBch}
                        ORDER BY SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหา จำนวนที่ลงของ
        public static DataTable GetDataPRFormShop_VendorPN(string _branch = "", string _date = "", string _vendorid = "")
        {
            //     string str = $@"
            //             SELECT	[INVENTLOCATIONID],[LEDGERVOUCHER],[INVOICEACCOUNT]
            //                     ,[LINENUM],[PURCHID],[DUEDATE],[InvoiceAmount] AS INVOICEAMOUNT
            //                     ,[ITEMID],[INVENTDIMID],[SPC_ITEMBARCODE]
            //                     ,[NAME],[QTY],[INVENTQTY],[PURCHUNIT]
            //                     ,[ReciveInvoice] AS INVOICEID
            //                     ,SHOP_VENDERPN.ReciveDate AS RECIVEDATE	
            //,ReciveWhoIns AS WhoIns
            //,ReciveWhoNameIns AS WhoInsName 
            //             FROM	SHOP_VENDERPN WITH (NOLOCK) 
            //                     INNER JOIN  SHOP_VENDERCHECK  WITH (NOLOCK) ON SHOP_VENDERPN.INVOICEACCOUNT = SHOP_VENDERCHECK.ReciveVender
            //    AND  SHOP_VENDERCHECK.ReciveVender = '{_vendorid}'
            //    AND SHOP_VENDERCHECK.ReciveBranchID = '{_branch}'
            //    AND SHOP_VENDERCHECK.ReciveDate  = '{_date}'
            //             WHERE   INVOICEACCOUNT = '{_vendorid}'
            //      AND INVENTLOCATIONID = '{_branch}'
            //      AND SHOP_VENDERPN.ReciveDate = '{_date}'

            // GROUP BY 
            //[INVENTLOCATIONID],[LEDGERVOUCHER],[INVOICEACCOUNT]
            //                     ,[LINENUM],[PURCHID],[DUEDATE],[InvoiceAmount]
            //                     ,[ITEMID],[INVENTDIMID],[SPC_ITEMBARCODE]
            //,[NAME],[QTY],[INVENTQTY],[PURCHUNIT],[ReciveInvoice]
            //,SHOP_VENDERPN.ReciveDate 
            //,ReciveWhoIns
            //,ReciveWhoNameIns ";

            string str = $@"
                    SELECT	[INVENTLOCATIONID],[LEDGERVOUCHER],[INVOICEACCOUNT]
                            ,[LINENUM],[PURCHID],[DUEDATE],[InvoiceAmount] AS INVOICEAMOUNT
                            ,[ITEMID],[INVENTDIMID],[SPC_ITEMBARCODE]
                            ,[NAME],[QTY],[INVENTQTY],[PURCHUNIT]
                            ,[ReciveInvoice] AS INVOICEID
                            ,SHOP_VENDERPN.ReciveDate AS RECIVEDATE	
							,ReciveWhoIns AS WhoIns
							,ReciveWhoNameIns AS WhoInsName 
                    FROM	SHOP_VENDERPN WITH (NOLOCK) 
                            LEFT OUTER JOIN 
							(
								SELECT *
								FROM	 SHOP_VENDERCHECK  WITH (NOLOCK)  
								WHERE   SHOP_VENDERCHECK.ReciveVender = '{_vendorid}'
										AND SHOP_VENDERCHECK.ReciveBranchID = '{_branch}'
										AND SHOP_VENDERCHECK.ReciveDate  = '{_date}' 
							)TMP ON SHOP_VENDERPN.SPC_ITEMBARCODE = TMP.ReciveItemBarcode
                    WHERE   INVOICEACCOUNT = '{_vendorid}'
					        AND INVENTLOCATIONID = '{_branch}'
					        AND SHOP_VENDERPN.ReciveDate = '{_date}'
                ";

            return ConnectionClass.SelectSQL_Main(str);
        }
        //ข้อมูลการผูกบิล
        public static DataTable GetDataPRFormShop_VendorPNByLedgervoucher(string _ledgerid, string pVender, string pBchID, string pDate)
        {
            string condition = $@" WHERE  LEDGERVOUCHER = '{_ledgerid}' ";
            if (_ledgerid == "") condition = $@" WHERE INVOICEACCOUNT = '{pVender}'  AND INVENTLOCATIONID = '{pBchID}' AND ReciveDate = '{pDate}' ";

            string str = $@"
                    SELECT	[INVENTLOCATIONID],[LEDGERVOUCHER],[INVOICEACCOUNT]
                            ,[LINENUM],[PURCHID],[DUEDATE],[InvoiceAmount] AS INVOICEAMOUNT
                            ,[ITEMID],[INVENTDIMID],[SPC_ITEMBARCODE],[NAME],[QTY],[INVENTQTY],[PURCHUNIT],[ReciveInvoice] AS INVOICEID
                            ,[ReciveDate] AS RECIVEDATE	
                    FROM	SHOP_VENDERPN WITH (NOLOCK) 
                    {condition} ";

            return ConnectionClass.SelectSQL_Main(str);
        }
      
        public static string InsertDataToShop_VenderPN(DataTable dt, Invoice _invoice)
        {
            ArrayList array = new ArrayList();
            int i = 1;
            foreach (DataRow row in dt.Rows)
            {

                string sql = string.Format(@"INSERT INTO SHOP_VENDERPN (
                                INVENTLOCATIONID,LEDGERVOUCHER,INVOICEACCOUNT,LINENUM,PURCHID
                                ,DUEDATE,InvoiceAmount,ITEMID,INVENTDIMID,SPC_ITEMBARCODE
                                ,NAME,QTY,INVENTQTY,PURCHUNIT
                                ,WhoIns,WhoInsName,ReciveDate,ReciveInvoice 
                            ) VALUES ('{0}','{1}','{2}','{3}','{4}'
                            ,'{5}','{6}','{7}','{8}','{9}'
                            ,'{10}','{11}','{12}','{13}'
                            ,'{14}','{15}','{16}','{17}')"
                            , _invoice.INVENTLOCATIONID, _invoice.LEDGERVOUCHER, _invoice.INVOICEACCOUNT, i, _invoice.PURCHID
                            , _invoice.DUEDATE, _invoice.InvoiceAmount, row["ITEMID"].ToString(), row["INVENTDIMID"].ToString(), row["SPC_ITEMBARCODE"].ToString()
                            , row["NAME"].ToString(), Convert.ToDouble(row["QTY"]), Convert.ToDouble(row["INVENTQTY"]), row["PURCHUNIT"].ToString()
                            , SystemClass.SystemUserID_M, SystemClass.SystemUserName, _invoice.RECEIVEDATE, _invoice.INVOICID);

                i += 1;
                array.Add(sql);
            }

            return ConnectionClass.ExecuteSQL_ArrayMain(array);
        }
        internal static string DeletDataShop_VenderPN(string _ledgerid)
        {
            string strdel = $@"DELETE FROM SHOP_VENDERPN WHERE LEDGERVOUCHER = '{_ledgerid}'";
            return ConnectionClass.ExecuteSQL_Main(strdel);
        }
        internal static List<ItemCheck> GetVenderCheck(string _vendorid, string _branchid, string _datereceive)
        {
            List<ItemCheck> List = new List<ItemCheck>();
            string sql = $@"
                SELECT	'' AS ITEMID,'' AS  INVENTDIMID,ReciveItemBarcode AS SPC_ITEMBARCODE,ReciveName AS NAME,ReciveQtySupc AS  QTY,
                        '1' AS INVENTQTY,ReciveUnit AS PURCHUNIT,ReciveWhoIns,ReciveWhoNameIns
                FROM	SHOP_VENDERCHECK WITH (NOLOCK)
                WHERE	ReciveVender = '{_vendorid}' AND ReciveBranchID = '{_branchid}' AND ReciveDate = '{_datereceive}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            foreach (DataRow row in dt.Rows)
            {
                ItemCheck Tmp = new ItemCheck()
                {
                    ITEMID = row["ITEMID"].ToString(),
                    INVENTDIMID = row["INVENTDIMID"].ToString(),
                    SPC_ITEMBARCODE = row["SPC_ITEMBARCODE"].ToString(),
                    NAME = row["NAME"].ToString(),
                    QTY = Convert.ToDouble(row["QTY"]),
                    INVENTQTY = Convert.ToDouble(row["INVENTQTY"]),
                    PURCHUNIT = row["PURCHUNIT"].ToString(),
                    ReciveWhoIns = row["ReciveWhoIns"].ToString(),
                    ReciveWhoNameIns = row["ReciveWhoNameIns"].ToString()
                };
                List.Add(Tmp);
            }
            return List;
        }
        public static bool CheckReceiveInvoice(string _vendorid, string _branchid, string _datereceive, string _invoiceid)
        {
            string sql = $@"
                    SELECT [Docno]
                          ,[BranchBill]
                          ,[RemarkBill]
                          ,[RemarkVender]
                          ,[EmpIDAdmin]
                          ,[RDateIn]
                          ,[RWhoIn]
                          ,[RDateUp]
                          ,[RWhoUp]
                          ,[Rtype]
                          ,[BillTrans]
                      FROM [dbo].[SHOP_RECIVEDOCUMENT] WITH (NOLOCK)
                      WHERE [BranchBill] = '{_branchid}' 
                          AND [RemarkVender] = '{_vendorid}'
                          AND [RDateIn] = '{_datereceive}'
                          AND [Docno] = '{_invoiceid}' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0) return true; else return false;
        }

        public static string Insert_VENDERCHECK(string bchID, string bchName, string venderID, string date, string itembarcode, string itemName, string unit, double qtyBch, double qtySpc)
        {
            string sql = $@"
                Insert Into SHOP_VENDERCHECK(
                    ReciveDate, ReciveVender, ReciveItemBarcode, ReciveName, 
                    ReciveQtyBch, ReciveQtySupc,  ReciveUnit,
                    ReciveWhoIns,ReciveWhoNameIns, ReciveBranchID,ReciveBranchName) 
                values (
                    '{date}','{venderID}','{itembarcode}','{itemName}',
                    '{qtyBch}','{qtySpc}','{unit}',
                    '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{bchID}','{bchName}')";
            return sql;
        }

        public static string Update_VENDERCHECK(string bchID, string venderID, string date, string itembarcode, double qty)
        {
            string sql = $@" 
                        Update  SHOP_VENDERCHECK 
                        SET     ReciveQtySupc = '{qty}' 
                        WHERE   ReciveDate = '{date}' 
                                AND ReciveVender = '{venderID}' 
                                AND ReciveBranchID = '{bchID}' 
                                AND ReciveItemBarcode = '{itembarcode}' 
                    ";
            return sql;
        }


    }

    class Invoice
    {
        public string INVENTLOCATIONID { get; set; } = "";
        public string LOCATIONNAME { get; set; } = "";
        public string LEDGERVOUCHER { get; set; } = "";
        public string INVOICEACCOUNT { get; set; } = "";
        public string INVOICENAME { get; set; } = "";
        public string INVOICID { get; set; } = "";
        public string PURCHID { get; set; } = "";
        public string RECEIVEDATE { get; set; } = "";
        public string DUEDATE { get; set; } = "";
        public double InvoiceAmount { get; set; } = 0.0;
        public string RECEIVESPC { get; set; } = "";
        public string RECEIVESPC_NAME { get; set; } = "";
    }
    class ItemReceive
    {
        public string SPC_ITEMBARCODE { get; set; }
        public string NAME { get; set; }
        public string PURCHUNIT { get; set; }
        public double QTY { get; set; }
        public double QTY_BRANCH { get; set; }
        public double QTY_SUPC { get; set; }
    }
    class ItemCheck
    {
        public string ITEMID { get; set; }
        public string INVENTDIMID { get; set; }
        public string SPC_ITEMBARCODE { get; set; }
        public string NAME { get; set; }
        public double QTY { get; set; }
        public double INVENTQTY { get; set; }
        public string PURCHUNIT { get; set; }
        public string ReciveWhoIns { get; set; }
        public string ReciveWhoNameIns { get; set; }
    }
}
