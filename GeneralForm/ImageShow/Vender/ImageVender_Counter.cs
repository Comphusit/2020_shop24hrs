﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;
using System.Drawing;
using System.Diagnostics;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.Vender
{
    public partial class ImageVender_Counter : Telerik.WinControls.UI.RadForm
    {
        DataTable dtItemBarcode = new DataTable();
        readonly DataTable dtSendMA = new DataTable();

        DirectoryInfo DirInfo;
        DirectoryInfo DirInfoVDO;

        FileInfo[] FilesImage;
        FileInfo[] FilesVDO;
        FileInfo[] FilesInvoice;

        readonly string _pVenderID;
        readonly string _pVenderName;
        readonly string _pDate;
        readonly string _pBchID;
        readonly string _pBchName;
        readonly string _pImage;
        //readonly string _pTypeOpen;
        readonly string _pPermission;
        //Load
        public ImageVender_Counter(string pVenderID, string pVenderName, string pDate, string pBchID, string pBchName, string pImage, string pPermission)//string pTypeOpen,
        {
            InitializeComponent();

            _pVenderID = pVenderID;
            _pVenderName = pVenderName;
            _pDate = pDate;
            _pBchID = pBchID;
            _pBchName = pBchName;
            _pImage = pImage;
            // _pTypeOpen = pTypeOpen;
            _pPermission = pPermission;
        }
        private void ImageVender_Counter_Load(object sender, EventArgs e)
        {
            radButton_Print.ShowBorder = true; radButton_Print.ToolTipText = "พิมพ์รูปภาพ";
            radButton_refresh.ShowBorder = true; radButton_refresh.ToolTipText = "ดึงข้อมูลใหม่";
            radButtonElement_MA.ShowBorder = true; radButtonElement_MA.ToolTipText = "พิมพ์ MA";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radStatusStrip.SizingGrip = false;

            this.Text = "รูปรับสินค้าตามบิลผู้จำหน่าย " + _pDate + " / " + _pVenderID + "-" + _pVenderName + " [" + _pBchID + "-" + _pBchName + "]";

            DatagridClass.SetDefaultRadGridView(radGridView_Image);
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อสินค้า", 250));

            if (SystemClass.SystemBranchID != "MN000")
            {
                radLabel_F2.Text = "Double Click รูป >> ดูรูปขนาดใหญ่ | Double Click บาร์โค้ด >> ดู VDO ลงสินค้า";
                radButton_Print.Enabled = false;

                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY_BCH", "จำนวนรับ", 100));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY_SPC", "จำนวนนับ"));
            }
            else
            {
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY_BCH", "จำนวน_BCH"));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY_SPC", "จำนวนนับ", 100));

                if (_pImage == "1")
                {
                    if (_pPermission == "1") radLabel_F2.Text = "สีแดง > ยังไม่นับ | Double Click รูป >> ดูรูปขนาดใหญ่ | Double Click จำนวนนับ >> เพื่อนับรูป";
                    else radLabel_F2.Text = "สีแดง > ยังไม่นับ |Double Click รูป >> ดูรูปขนาดใหญ่";
                }
                else
                {
                    if (_pPermission == "1") radLabel_F2.Text = "สีแดง > ยังไม่นับ |Double Click รูป >> ดูรูปขนาดใหญ่ | Double Click บาร์โค้ด >> ดู VDO ลงสินค้า | Double Click จำนวนนับ >> เพื่อนับรูป";
                    else radLabel_F2.Text = "สีแดง > ยังไม่นับ |Double Click รูป >> ดูรูปขนาดใหญ่ | Double Click บาร์โค้ด >> ดู VDO ลงสินค้า ";
                }

            }

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA", "STA"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "รูปด้านบน", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG1", "C_IMG1"));


            if (_pImage == "3")
            {
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG2", "รูปด้านหน้า", 250));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG2", "P_IMG2"));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG2", "C_IMG2"));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG3", "รูปด้านข้าง", 250));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG3", "P_IMG3"));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG3", "C_IMG3"));
            }


            radGridView_Image.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Image.TableElement.RowHeight = 300;
            radGridView_Image.TableElement.TableHeaderHeight = 50;
            DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "STA = '0' ", ConfigClass.SetColor_Red(), radGridView_Image);

            DirInfo = new DirectoryInfo(PathImageClass.pPathVender + _pDate + @"\" + _pBchID + "");
            DirInfoVDO = new DirectoryInfo(PathImageClass.pPathVDOVender + _pDate + @"\" + _pBchID + "");

            FilesInvoice = DirInfo.GetFiles("*" + _pVenderID + @",000.JPG", SearchOption.AllDirectories);


            dtSendMA.Columns.Add("ITEMBARCODE");
            dtSendMA.Columns.Add("SPC_ITEMNAME");
            dtSendMA.Columns.Add("QTY");
            dtSendMA.Columns.Add("UNITID");

            FindImage();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Image Set Format
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion

        //OK
        private void RadButtonElement_OK_Click(object sender, EventArgs e)
        {
            FindImage();
        }

        //Find Image
        void FindImage()
        {
            this.Cursor = Cursors.WaitCursor;
            if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();

            //DataTable dtItemBarcode = FindBarcodeAllImage();
            FindBarcodeAllImage();
            for (int i = 0; i < dtItemBarcode.Rows.Count; i++)
            {
                //Vender - MN033 - 2020 - 10 - 01 - 105352 - M0911113_V001954#8850999111018,A,15,[B]    ,[T],[F]
                string itembarcode = dtItemBarcode.Rows[i]["ITEMBARCODE"].ToString();
                string itembarcodeName = dtItemBarcode.Rows[i]["SPC_ITEMNAME"].ToString();
                string itembarcodeUnit = dtItemBarcode.Rows[i]["UNITID"].ToString();
                string itemQtySUPC = dtItemBarcode.Rows[i]["ReciveQtySupc"].ToString();
                string itemSTA = dtItemBarcode.Rows[i]["STA"].ToString();
                int qty = 0;
                if (_pImage == "3")
                {
                    FileInfo[] FilesByBarcodeT = DirInfo.GetFiles("*" + _pVenderID + @"#" + itembarcode + @",*[T].JPG", SearchOption.AllDirectories);
                    FileInfo[] FilesByBarcodeF = DirInfo.GetFiles("*" + _pVenderID + @"#" + itembarcode + @",*[F].JPG", SearchOption.AllDirectories);
                    FileInfo[] FilesByBarcodeB = DirInfo.GetFiles("*" + _pVenderID + @"#" + itembarcode + @",*[B].JPG", SearchOption.AllDirectories);

                    string[] AA;
                    string[] item;


                    string img1 = PathImageClass.pImageEmply;
                    string p_img1 = ""; string c_img1 = "";

                    string img2 = PathImageClass.pImageEmply;
                    string p_img2 = ""; string c_img2 = "";

                    string img3 = PathImageClass.pImageEmply;
                    string p_img3 = ""; string c_img3 = "";

                    if (FilesByBarcodeT.Length > 0)
                    {
                        img1 = FilesByBarcodeT[0].FullName;
                        p_img1 = FilesByBarcodeT[0].Name;
                        c_img1 = "1";
                        AA = FilesByBarcodeT[0].Name.Split('#');
                        item = AA[1].Split(',');
                        qty = int.Parse(item[2]);
                    }

                    if (FilesByBarcodeF.Length > 0)
                    {
                        img2 = FilesByBarcodeF[0].FullName;
                        p_img2 = FilesByBarcodeF[0].Name;
                        c_img2 = "1";
                        if (qty == 0)
                        {
                            AA = FilesByBarcodeF[0].Name.Split('#');
                            item = AA[1].Split(',');
                            qty = int.Parse(item[2]);
                        }
                    }
                    if (FilesByBarcodeB.Length > 0)
                    {
                        img3 = FilesByBarcodeB[0].FullName;
                        p_img3 = FilesByBarcodeB[0].Name;
                        c_img3 = "1";

                        if (qty == 0)
                        {
                            AA = FilesByBarcodeB[0].Name.Split('#');
                            item = AA[1].Split(',');
                            qty = int.Parse(item[2]);
                        }
                    }


                    radGridView_Image.Rows.Add(itembarcode, itembarcodeName, qty, itemQtySUPC, itemSTA, itembarcodeUnit,
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img1), PathImageClass.pPathVender + _pDate + @"\" + _pBchID + @"|" + p_img1, c_img1,
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img2), PathImageClass.pPathVender + _pDate + @"\" + _pBchID + @"|" + p_img2, c_img2,
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(img3), PathImageClass.pPathVender + _pDate + @"\" + _pBchID + @"|" + p_img3, c_img3);


                    double qtySend = double.Parse(itemQtySUPC);
                    if (SystemClass.SystemBranchID != "MN000") qtySend = qty;

                    dtSendMA.Rows.Add(itembarcode, itembarcodeName, qtySend.ToString("#,#0.00"), itembarcodeUnit);
                }
                else // รูปที่เป็น 1 รูป
                {
                    //Vender-MN022-2020-10-01-094519-M1709067_V000220#9106915400704,40
                    FileInfo[] FilesByBarcode = DirInfo.GetFiles("*" + _pVenderID + @"#" + itembarcode + @"*.JPG", SearchOption.AllDirectories);

                    string[] AA = FilesByBarcode[0].Name.Split('#');
                    string[] item = AA[1].Split(',');
                    string img1 = FilesByBarcode[0].FullName;
                    string p_img1 = FilesByBarcode[0].Name;

                    radGridView_Image.Rows.Add(itembarcode, itembarcodeName, int.Parse(item[1].Replace(".jpg", "")), itemQtySUPC, itemSTA, itembarcodeUnit,
                    Image.FromFile(img1), PathImageClass.pPathVender + _pDate + @"|" + p_img1, "1");

                    double qtySend = double.Parse(itemQtySUPC);
                    if (SystemClass.SystemBranchID != "MN000") qtySend = double.Parse(item[1].Replace(".jpg", ""));

                    dtSendMA.Rows.Add(itembarcode, itembarcodeName, qtySend.ToString("#,#0.00"), itembarcodeUnit);

                }
            }
            this.Cursor = Cursors.Default;
        }
        //FindBarcode From AllImage
        void FindBarcodeAllImage()
        {
            FilesImage = DirInfo.GetFiles("*" + _pVenderID + @"#*.JPG", SearchOption.AllDirectories);
            string barcode = "";
            if (FilesImage.Length > 0)
            {
                foreach (FileInfo item in FilesImage)
                {
                    //Vender-MN033-2020-10-01-105352-M0911113_V001954#8850999111018,A,15,[B]
                    string[] AA = item.Name.Split('_');
                    string[] itemname = AA[1].Split('#');
                    itemname = itemname[1].Split(',');
                    barcode = barcode + "'" + itemname[0] + "',";
                }
            }
            if (barcode.Length == 0) barcode = "''";
            else barcode = barcode.Substring(0, barcode.Length - 1);


            dtItemBarcode = ItembarcodeClass.VenderAX_CheckBarcode_ForRecive(_pDate, _pVenderID, _pBchID, barcode);// ConnectionClass.SelectSQL_Main(sql);
        }
        //print
        private void RadButton_Print_Click(object sender, EventArgs e)
        {
            if (radGridView_Image.Rows.Count == 0)
            { return; }

            int cc = 0;
            for (int i = 0; i < radGridView_Image.Rows.Count; i++)
            {
                if (radGridView_Image.Rows[i].Cells["STA"].Value.ToString() == "0") cc += 1;
            }

            if (cc > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ยังบันทีกข้อมูลการนับไม่เรียบร้อย ไม่สามารถพิมพ์สลิปได้");
                return;
            }

            Vender_ToAX frm_vender_ToAX = new Vender_ToAX(_pBchID, _pBchName, _pDate, _pVenderID, _pVenderName, _pPermission);
            if (frm_vender_ToAX.ShowDialog() == DialogResult.Yes)
            {

            }
        }

        //refresh
        private void RadButton_refresh_Click(object sender, EventArgs e)
        {
            FindImage();
        }
        //DoubleClick
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "ITEMBARCODE":
                    if (_pImage == "1")
                    {
                        return;
                    }
                    //VED - MN033 - 2020 - 10 - 01 - 105957 - M1705090_V001954#8850999009681,A,20,B
                    FilesVDO = DirInfoVDO.GetFiles("*" + _pVenderID + @"#" + radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @",*.mp4", SearchOption.AllDirectories);
                    if (FilesVDO.Length == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบ File VDO ของการลงลงสินค้า " + Environment.NewLine +
                          radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " " + radGridView_Image.CurrentRow.Cells["SPC_NAME"].Value.ToString());
                        return;
                    }

                    if (FilesVDO.Length == 1)
                    {
                        Process.Start(FilesVDO[0].FullName);
                        return;
                    }

                    DataTable dt = new DataTable();
                    dt.Columns.Add("DATA_ID");
                    dt.Columns.Add("FileName");
                    dt.Columns.Add("DATA_DESC");
                    foreach (FileInfo item in FilesVDO)
                    {
                        string[] A = item.Name.Split('_');
                        string[] B = A[0].Split('-');
                        string whoins;
                        DataTable dtEmp = Class.Models.EmplClass.GetEmployeeDetail_ByEmplID(B[6].Replace("M", "").Replace("D", "").Replace("P", ""));
                        if (dtEmp.Rows.Count == 0) whoins = "ไม่สามารถระบุผู้ถ่ายได้.";
                        else whoins = dtEmp.Rows[0]["EMPLID"].ToString() + " " + dtEmp.Rows[0]["SPC_NAME"].ToString();

                        dt.Rows.Add(item.FullName, item.Name, whoins);
                    }

                    FormShare.ShowData.ShowDataDGV _showData = new FormShare.ShowData.ShowDataDGV()
                    {
                        dtData = dt
                    };
                    if (_showData.ShowDialog(this) == DialogResult.Yes)
                    {
                        Process.Start(_showData.pID);
                        return;
                    }
                    break;
                case "QTY_SPC":
                    if (_pPermission == "0") return;

                    if (radGridView_Image.CurrentRow.Cells["STA"].Value.ToString() != "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("รายการนี้มีการนับเรียบร้อยแล้ว ไม่สามารถบันทึกซ้ำได้.");
                        return;
                    }

                    FormShare.InputData _inputdata = new FormShare.InputData("0",
                         radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" + radGridView_Image.CurrentRow.Cells["SPC_NAME"].Value.ToString(),
                         "จำนวนที่นับได้ [ไม่สามารถแก้ไขได้]", radGridView_Image.CurrentRow.Cells["UNITID"].Value.ToString());
                    if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                    {
                        string T = ConnectionClass.ExecuteSQL_Main(Vender_ToAXClass.Insert_VENDERCHECK(_pBchID, _pBchName, _pVenderID, _pDate,
                         radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                         radGridView_Image.CurrentRow.Cells["SPC_NAME"].Value.ToString(),
                         radGridView_Image.CurrentRow.Cells["UNITID"].Value.ToString(),
                         double.Parse(radGridView_Image.CurrentRow.Cells["QTY_BCH"].Value.ToString()), double.Parse(_inputdata.pInputData)));

                        if (T == "") radGridView_Image.CurrentRow.Cells["QTY_SPC"].Value = int.Parse(_inputdata.pInputData);
                        else MsgBoxClass.MsgBoxShow_SaveStatus(T);

                    }
                    break;
                case "IMG1":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG1"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    break;
                case "IMG2":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG2"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG2"].Value.ToString());
                    break;
                case "IMG3":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG3"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG3"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
       
        //Slip MA
        private void RadButtonElement_MA_Click(object sender, EventArgs e)
        {
            //เชค รูป
            if (radGridView_Image.Rows.Count == 0) return;
            //เชควันที่ปัจจุบันสำหรับสาขาห
            if (SystemClass.SystemBranchID != "MN000")
            {
                if (DateTime.Parse(_pDate).ToString("yyyy-MM-dd") != DateTime.Today.ToString("yyyy-MM-dd"))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถสร้างหรือแก้ไขสลิป MA ย้อนหลังได้" + Environment.NewLine + " ติดต่อ CenterShop 1022 เพื่อแก้ไขบิลแทน.");
                    return;
                }
            }

            using (MA.MAReceiveDocument mAReceiveDocument = new MA.MAReceiveDocument(_pDate, _pBchID, _pBchName, _pVenderID, _pVenderName, dtSendMA,""))
            {
                DialogResult dr = mAReceiveDocument.ShowDialog();
                if (dr == DialogResult.Yes)
                {

                }
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
