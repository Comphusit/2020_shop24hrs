﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.ControlProduct
{
    public partial class ControlProduct_Counter : Telerik.WinControls.UI.RadForm
    {
        DirectoryInfo DirInfo;

        FileInfo[] FilesImage;

        readonly string _pDate;
        readonly string _pBchID;
        readonly string _pBchName;
        readonly string _pTypeOpen;
        readonly string _pPermission;

        //Load
        public ControlProduct_Counter(string pDate, string pBchID, string pBchName, string pTypeOpen, string pPermission)
        {
            InitializeComponent();

            _pDate = pDate;
            _pBchID = pBchID;
            _pBchName = pBchName;
            _pTypeOpen = pTypeOpen;
            _pPermission = pPermission;
        }
        private void ControlProduct_Counter_Load(object sender, EventArgs e)
        {

            radButton_refresh.ShowBorder = true; radButton_refresh.ToolTipText = "ดึงข้อมูลใหม่";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";


            radStatusStrip.SizingGrip = false;

            this.Text = "สินค้าควบคุม [ส่งจาก SUPC] " + _pDate + " / " + " [" + _pBchID + "-" + _pBchName + "]";

            DatagridClass.SetDefaultRadGridView(radGridView_Image);
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 150));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 120));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อสินค้า", 250));

            if (_pTypeOpen == "SHOP")
            {
                radLabel_F2.Text = "Double Click รูป >> ดูรูปขนาดใหญ่";
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY_BCH", "จำนวนส่ง", 100));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY_SPC", "จำนวนนับ"));
            }
            else
            {
                radLabel_F2.Text = "Double Click รูป >> ดูรูปขนาดใหญ่ | Double Click จำนวนนับ >> เพื่อนับรูป";
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY_BCH", "จำนวนส่ง"));
                radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY_SPC", "จำนวนนับ", 100));
            }

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA", "STA"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("IMG1", "รูปถ่าย", 250));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("P_IMG1", "P_IMG1"));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("C_IMG1", "C_IMG1"));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("whoins", "ผู้รับสินค้า", 80));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("whoinsName", "ชื่อผู้รับสินค้า", 150));

            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ReciveWhoIns", "ผู้นับสินค้า", 80));
            radGridView_Image.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ReciveWhoInsName", "ชื่อผู้นับสินค้า", 150));

            radGridView_Image.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveDetailRemark", "หมายเหตุ", 400)));

            radGridView_Image.TableElement.MasterTemplate.EnableFiltering = false;
            radGridView_Image.TableElement.RowHeight = 300;
            radGridView_Image.TableElement.TableHeaderHeight = 50;

            DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", "STA = '0' ", ConfigClass.SetColor_Red(), radGridView_Image);

            DirInfo = new DirectoryInfo(PathImageClass.pPathCheckItemRecive + _pDate + @"\" + _pBchID + "");

            FindImage();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview

        #endregion

        //OK
        private void RadButtonElement_OK_Click(object sender, EventArgs e)
        {
            FindImage();
        }

        //Find Image
        void FindImage()
        {
            this.Cursor = Cursors.WaitCursor;
            if (radGridView_Image.Rows.Count > 0) radGridView_Image.Rows.Clear();

            FilesImage = DirInfo.GetFiles("*.JPG", SearchOption.AllDirectories);
            if (FilesImage.Length == 0) return;

            foreach (FileInfo item in FilesImage)
            {
                //ChkI-MN016-2020-10-14-211444-M1102045_030591024356-T2010591-0001128#8852007912402
                string[] AA = item.Name.Split('_');
                string[] BB = AA[0].Split('-');
                string whoins = BB[6];
                string whoinsName = Models.EmplClass.GetEmployeeDetail_ByEmplID(BB[6].Replace("M", "").Replace("P", "").Replace("D", "")).Rows[0]["SPC_NAME"].ToString();// EmplClass.GetEmployeeName_ByEmplID_M(BB[6]);
                string[] itemname = AA[1].Split('-');
                string boxid = itemname[0];
                string[] A1 = itemname[2].Split('#');
                string barcode = A1[1].Replace(".jpg", "");
                DataTable dtBarcode = BOXClass.ControlProductRecive_FindDataByRecord(boxid, _pBchID, barcode);

                radGridView_Image.Rows.Add(
                    dtBarcode.Rows[0]["DOCUMENTNUM"].ToString(),
                    dtBarcode.Rows[0]["BOXGROUP"].ToString(),
                    dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                    dtBarcode.Rows[0]["NAME"].ToString(),
                    dtBarcode.Rows[0]["QTY"].ToString(),
                    dtBarcode.Rows[0]["ReciveQtySupc"].ToString(),
                    dtBarcode.Rows[0]["STA"].ToString(),
                    dtBarcode.Rows[0]["UNITID"].ToString(),
                    ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(item.FullName), PathImageClass.pPathCheckItemRecive + _pDate + @"|" + item.Name, "1", whoins, whoinsName,
                    dtBarcode.Rows[0]["ReciveWhoIns"].ToString(),
                    dtBarcode.Rows[0]["ReciveWhoInsName"].ToString(),
                    dtBarcode.Rows[0]["ReciveDetailRemark"].ToString());
            }

            this.Cursor = Cursors.Default;
        }

        //Image Set Format
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //refresh
        private void RadButton_refresh_Click(object sender, EventArgs e)
        {
            FindImage();
        }
        //DoubleClick
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QTY_SPC":
                    if (_pPermission == "0") return;
                    if (radGridView_Image.CurrentRow.Cells["STA"].Value.ToString() != "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("รายการนี้มีการนับเรียบร้อยแล้ว ไม่สามารถบันทึกซ้ำได้.");
                        return;
                    }

                    FormShare.InputData _inputdata = new FormShare.InputData("0",
                        radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" + radGridView_Image.CurrentRow.Cells["SPC_NAME"].Value.ToString(),
                        "จำนวนที่นับได้ [ไม่สามารถแก้ไขได้]", radGridView_Image.CurrentRow.Cells["UNITID"].Value.ToString());
                    if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                    {
                        double q_Bch = double.Parse(radGridView_Image.CurrentRow.Cells["QTY_BCH"].Value.ToString());
                        double q_Spc = double.Parse(_inputdata.pInputData);

                        string T = SaveCounter(
                                  radGridView_Image.CurrentRow.Cells["DOCUMENTNUM"].Value.ToString(),
                                  radGridView_Image.CurrentRow.Cells["BOXGROUP"].Value.ToString(),
                                  radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                                  radGridView_Image.CurrentRow.Cells["SPC_NAME"].Value.ToString().Replace("'", ""),
                                  radGridView_Image.CurrentRow.Cells["UNITID"].Value.ToString(),
                                  q_Bch,
                                  q_Spc,
                                  radGridView_Image.CurrentRow.Cells["whoins"].Value.ToString(),
                                  radGridView_Image.CurrentRow.Cells["whoinsName"].Value.ToString());
                        if (T == "")
                        {
                            radGridView_Image.CurrentRow.Cells["QTY_SPC"].Value = int.Parse(_inputdata.pInputData);
                            radGridView_Image.CurrentRow.Cells["STA"].Value = radGridView_Image.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
                            radGridView_Image.CurrentRow.Cells["ReciveDetailRemark"].Value = "";
                            radGridView_Image.CurrentRow.Cells["ReciveWhoIns"].Value = SystemClass.SystemUserID;
                            radGridView_Image.CurrentRow.Cells["ReciveWhoInsName"].Value = SystemClass.SystemUserName;
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        }
                    }

                    break;
                case "IMG1":
                    ImageClass.OpenShowImage(radGridView_Image.CurrentRow.Cells["C_IMG1"].Value.ToString(),
                            radGridView_Image.CurrentRow.Cells["P_IMG1"].Value.ToString());
                    break;

                default:
                    break;
            }
        }
        //SaveCounter
        string SaveCounter(string pDocno, string pBox, string pBarcode, string pItemname,
            string pUnitid, double qtyBch, double qtySpc, string pWhoBch, string pWhoBchName)
        {

            string sql = string.Format(@"
                Insert Into SHOP_RECIVEITEMSUPC(
                    [ReciveDate],[ReciveDocno],[ReciveBox],[ReciveBranch],[ReciveBranchName],
                    [ReciveItemBarcode],[ReciveName],[ReciveQtySupc],
                    [ReciveQtySend],[ReciveUnit],[ReciveWhoBch],[ReciveWhoBchname],
                    [ReciveWhoIns],[ReciveWhoInsName],
                    [ReciveDetailRemark]) values (
                '" + _pDate + @"','" + pDocno + @"','" + pBox + @"','" + _pBchID + @"','" + _pBchName + @"',
                '" + pBarcode + @"','" + pItemname + @"','" + qtySpc + @"',
                '" + qtyBch + @"','" + pUnitid + @"','" + pWhoBch + @"','" + pWhoBchName + @"',
                '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','') "
             );
            return ConnectionClass.ExecuteSQL_Main(sql);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadGridView_Image_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Image_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Image_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
    }
}
