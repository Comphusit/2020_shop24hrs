﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.ImageShow.ControlProduct
{
    public partial class ControlProduct_Main : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeReport; //0 = เช็คนับรูป 1 = รายงาน
        readonly string _pTypeOpen;
        readonly string _pPerMission;

        DataTable dtBch;
        DataTable dtRpt = new DataTable();
        // DataTable dt_Data = new DataTable();

        //Load
        public ControlProduct_Main(string pTypeReport, string pTypeOpen, string pPerMission)
        {
            InitializeComponent();

            _pTypeOpen = pTypeOpen;
            _pPerMission = pPerMission;
            _pTypeReport = pTypeReport;
        }
        //Load
        private void ControlProduct_Main_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radButtonElement_MA.ShowBorder = true; radButtonElement_MA.ToolTipText = "รับบิล";
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

            radDateTimePicker_D1.Visible = true;
            radDateTimePicker_D2.Visible = true;
            radLabel_Date.Visible = true; radLabel_Date.Text = "วันที่ส่งสินค้า";
            RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;

            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีแดง >> ยังบันทึกนับไม่ครบรายการ | สีฟ้า >> บันทึกรับครบทุกรายการ | กด / >> เข้าหน้าบันทีกนับบิลและดูรูป";
                    break;
                case "1":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ReciveIDAUTO", "ID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveDate", "วันที่", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveBranch", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveBranchName", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveDocno", "เลขที่บิล", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveBox", "เลขที่ลัง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveItemBarcode", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveName", "ชื่อสินค้า", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("ReciveQtySend", "จำนวนส่ง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("ReciveQtySupc", "จำนวนนับ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("ReciveQtyDiff", "ผลต่าง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveUnit", "หน่วย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveWhoBchName", "ผู้รับรายการ", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveWhoInsName", "ผู้นับรายการ", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveDetailRemark", "หมายเหตุ", 400)));

                    RadGridView_ShowHD.TableElement.RowHeight = 45;

                    DatagridClass.SetCellBackClolorByExpression("ReciveQtyDiff", "ReciveQtyDiff  <> 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    radButtonElement_MA.Enabled = false;
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีแดง >> จำนวนรับและจำนวนนับไม่เท่ากัน | DoubleClick รายการ >> ระบุเหตุผลที่ข้อมูลของการส่งของและการนับไม่เท่ากัน";
                    break;
                default:
                    break;
            }


            if (_pTypeOpen == "SUPC")
            {
                RadCheckBox_1.Enabled = true;
                dtBch = BranchClass.GetBranchAll("'1'", " '1'");
            }
            else
            {
                RadCheckBox_1.CheckState = CheckState.Checked;
                RadCheckBox_1.Enabled = false;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
            }

            RadDropDownList_1.DataSource = dtBch;
            RadDropDownList_1.DisplayMember = "NAME_BRANCH";
            RadDropDownList_1.ValueMember = "BRANCH_ID";

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;

            switch (_pTypeReport)
            {
                case "0":

                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("pBchName", "ชื่อสาขา", 200)));
                    RadGridView_ShowHD.Columns["pBchID"].IsPinned = true;
                    RadGridView_ShowHD.Columns["pBchName"].IsPinned = true;
                    RadGridView_ShowHD.TableElement.TableHeaderHeight = 100;

                    double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays) + 1;
                    if (intDay == 0)
                    {
                        intDay = 1;
                    }

                    for (int iDay = 0; iDay < intDay; iDay++)
                    {
                        var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");

                        string HeadColumnCount = "IMG" + "_" + pDate.Replace("-", "V");
                        string HeadTextDesc_Count = pDate;
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, HeadTextDesc_Count, 120)));

                        string HeadColumnCountC = "DB" + "_" + pDate.Replace("-", "C");
                        string HeadTextDesc_CountC = pDate;
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible(HeadColumnCountC, HeadTextDesc_CountC)));

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                                  new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 AND " + HeadColumnCountC + " = 0 ", false)
                                  { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                        this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                                new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 AND " + HeadColumnCountC + " > 0 ", false)
                                { CellBackColor = ConfigClass.SetColor_Red() });

                    }
                    DataTable dtBchSelect;
                    if (RadCheckBox_1.CheckState == CheckState.Checked)
                    {
                        dtBchSelect = BranchClass.GetDetailBranchByID(RadDropDownList_1.SelectedValue.ToString());
                    }
                    else
                    {
                        dtBchSelect = BranchClass.GetBranchAll("'1'", " '1'");
                    }
                    ////Add Data    //จำนวนวันที่ดึง
                    for (int iBch = 0; iBch < dtBchSelect.Rows.Count; iBch++)
                    {
                        string bch_ID = dtBchSelect.Rows[iBch]["BRANCH_ID"].ToString();
                        string bch_Name = dtBchSelect.Rows[iBch]["BRANCH_NAME"].ToString();
                        RadGridView_ShowHD.Rows.Add(bch_ID, bch_Name);

                        int iCountColume = 2;
                        for (int iD = 0; iD < intDay; iD++)
                        {
                            var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");

                            int imgCount = CountImage(pDate, bch_ID);
                            int imgDB = imgCount - CountImageDB(pDate, bch_ID);

                            RadGridView_ShowHD.Rows[iBch].Cells[iCountColume].Value = imgCount.ToString();
                            RadGridView_ShowHD.Rows[iBch].Cells[iCountColume + 1].Value = imgDB.ToString();
                            iCountColume += 2;

                        }

                    }
                    break;
                case "1"://กรณีเป็นรายงาน


                    string pCon = "";
                    if (RadCheckBox_1.Checked == true) pCon = " AND ReciveBranch = '" + RadDropDownList_1.SelectedValue + @"' ";

                    string sqlRpt = string.Format(@"
                    SELECT  ReciveIDAUTO,CONVERT(VARCHAR, ReciveDate,23) AS ReciveDate, ReciveBranch, ReciveBranchName, ReciveDocno, ReciveBox,
                            ReciveItemBarcode, ReciveName, ReciveQtySend, ReciveQtySupc, ReciveQtyDiff, ReciveUnit, ReciveWhoBchName, ReciveWhoInsName, ReciveDetailRemark
                    FROM    SHOP_RECIVEITEMSUPC WITH(NOLOCK)
                    WHERE   ReciveDate BETWEEN '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"'
                            " + pCon + @"
                    ");

                    dtRpt = ConnectionClass.SelectSQL_Main(sqlRpt);
                    RadGridView_ShowHD.DataSource = dtRpt;
                    dtRpt.AcceptChanges();
                    break;
                default:
                    break;
            }

            this.Cursor = Cursors.Default;

        }

        //FindDataInGroup
        int CountImage(String pDate, String pBch)
        {
            string path = PathImageClass.pPathCheckItemRecive + pDate + @"\" + pBch + "";
            DirectoryInfo DirInfo = new DirectoryInfo(path);
            if (DirInfo.Exists == false)
            {
                return 0;
            }
            else
            {
                FileInfo[] Files = DirInfo.GetFiles("*.JPG", SearchOption.AllDirectories);
                return Files.Length;
            }
        }
        //FindDataInGroup
        int CountImageDB(String pDate, String pBch)
        {
            string sql = string.Format(@"
                SELECT	*	
                FROM	SHOP_RECIVEITEMSUPC WITH (NOLOCK)  
                WHERE	ReciveDate = '" + pDate + @"'
		                AND ReciveBranch = '" + pBch + @"'
            ");
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            switch (_pTypeReport)
            {
                case "0":
                    if (RadGridView_ShowHD.Rows.Count > 0) { RadGridView_ShowHD.Rows.Clear(); RadGridView_ShowHD.Columns.Clear(); }
                    break;
                case "1":
                    if (dtRpt.Rows.Count > 0)
                    {
                        dtRpt.Rows.Clear(); dtRpt.AcceptChanges();
                    }
                    break;
                default:
                    break;
            }



            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }

        //MA
        private void RadButtonElement_MA_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "") return;            
            if ((RadGridView_ShowHD.CurrentCell.ColumnIndex == 0) || (RadGridView_ShowHD.CurrentCell.ColumnIndex == 1)) return;          

            string[] date = RadGridView_ShowHD.CurrentCell.ColumnInfo.HeaderText.ToString().Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            ControlProduct_Counter _Couter = new ControlProduct_Counter(date[0],
                RadGridView_ShowHD.CurrentRow.Cells["pBchID"].Value.ToString(),
                RadGridView_ShowHD.CurrentRow.Cells["pBchName"].Value.ToString(),
                _pTypeOpen, _pPerMission);
            if (_Couter.ShowDialog(this) == DialogResult.Yes)
            {

            }
            SetDGV_HD();
        }

        //SaveCounter
        string SaveCounter(string ReciveIDAUTO, string pRmk)
        {
            string sql = string.Format(@"
                UPDATE  SHOP_RECIVEITEMSUPC
                SET     ReciveDetailRemark = '" + pRmk + @"' + ' [' + CONVERT(VARCHAR, GETDATE(), 25) + ']'
                WHERE   ReciveIDAUTO = '" + ReciveIDAUTO + @"'"
             );
            return ConnectionClass.ExecuteSQL_Main(sql);
        }

        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pPerMission == "0") return;

            if (_pTypeReport == "1")
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["ReciveDetailRemark"].Value.ToString() != "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("รายการนี้มีหมายเหตุในการนับไม่เท่ากันเรียบร้อยแล้ว" + Environment.NewLine + "ไม่สามารถระบุซ้ำได้อีก.");
                    return;
                }

                double q_spc;
                try
                {
                    q_spc = double.Parse(RadGridView_ShowHD.CurrentRow.Cells["ReciveQtyDiff"].Value.ToString());
                }
                catch (Exception) { return; }

                if (q_spc == 0) return;

                string rmk;
                FormShare.ShowRemark _showRemark = new FormShare.ShowRemark("1")
                {
                    pDesc = "หมายเหตุ : " + RadGridView_ShowHD.CurrentRow.Cells["ReciveName"].Value.ToString() + @""
                };

                if (_showRemark.ShowDialog(this) == DialogResult.Yes)
                {
                    rmk = _showRemark.pRmk +
                            Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                            " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ถ้าระบุจำนวนส่งของและการรับของไม่เท่ากัน" + Environment.NewLine +
                        "บังคับระบุหมายเหตุก่อนการบันทึกทุกครั้ง");
                    return;
                }

                string T = SaveCounter(
                                 RadGridView_ShowHD.CurrentRow.Cells["ReciveIDAUTO"].Value.ToString(), rmk);
                if (T == "")
                {

                    RadGridView_ShowHD.CurrentRow.Cells["ReciveDetailRemark"].Value = rmk;

                }
                else
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                }

            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
