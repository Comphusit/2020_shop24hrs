﻿namespace PC_Shop24Hrs.GeneralForm.ImageShow.ControlProduct
{
    partial class ControlProduct_Counter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlProduct_Counter));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Image = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButton_refresh = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(829, 634);
            this.radPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_F2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Image, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 612);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(823, 19);
            this.radLabel_F2.TabIndex = 53;
            this.radLabel_F2.Text = "<html>Double Click รูป &gt;&gt; ดูรูปขนาดใหญ่</html>";
            // 
            // radGridView_Image
            // 
            this.radGridView_Image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Image.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Image.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.radGridView_Image.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Image.Name = "radGridView_Image";
            // 
            // 
            // 
            this.radGridView_Image.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Image.Size = new System.Drawing.Size(823, 555);
            this.radGridView_Image.TabIndex = 6;
            this.radGridView_Image.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Image_ViewCellFormatting);
            this.radGridView_Image.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Image_CellDoubleClick);
            this.radGridView_Image.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Image_ConditionalFormattingFormShown);
            this.radGridView_Image.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Image_FilterPopupRequired);
            this.radGridView_Image.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Image_FilterPopupInitialized);
            // 
            // radStatusStrip
            // 
            this.radStatusStrip.AutoSize = false;
            this.radStatusStrip.BackColor = System.Drawing.SystemColors.Control;
            this.radStatusStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButton_refresh,
            this.commandBarSeparator5,
            this.RadButtonElement_pdt,
            this.commandBarSeparator1});
            this.radStatusStrip.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip.Name = "radStatusStrip";
            // 
            // 
            // 
            this.radStatusStrip.RootElement.AutoSize = false;
            this.radStatusStrip.RootElement.BorderHighlightColor = System.Drawing.Color.White;
            this.radStatusStrip.Size = new System.Drawing.Size(823, 42);
            this.radStatusStrip.TabIndex = 5;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButton_refresh
            // 
            this.radButton_refresh.AutoSize = true;
            this.radButton_refresh.BorderHighlightColor = System.Drawing.Color.White;
            this.radButton_refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_refresh.Name = "radButton_refresh";
            this.radButton_refresh.ShowBorder = false;
            this.radStatusStrip.SetSpring(this.radButton_refresh, false);
            this.radButton_refresh.Text = "เพิ่ม";
            this.radButton_refresh.ToolTipText = "เพิ่ม";
            this.radButton_refresh.UseCompatibleTextRendering = false;
            this.radButton_refresh.Click += new System.EventHandler(this.RadButton_refresh_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator5.UseCompatibleTextRendering = false;
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // ControlProduct_Counter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ControlProduct_Counter";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "รูปรับสินค้าควบคุม [ส่งจาก SUPC]";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ControlProduct_Counter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Image)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButton_refresh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView radGridView_Image;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
    }
}
