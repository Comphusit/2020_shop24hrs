﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Collections;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.MNFR
{
    public partial class MNFR_MAIN : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();


        readonly string _pIDCard;
        readonly string _pAccountID;
        readonly string _pAccountName;
        readonly string _pTypeScan;
        readonly double _pPoint;

        readonly DataTable _dtDGV;

        string cashID;
        string cashName;
        string cstIdCard;
        string cstNameCard;
        double pointUse;
        string pApv;
        //Load
        public MNFR_MAIN(string pIDCard, string pAccountName, string pAccountID, string pTypeScan, DataTable dtDGV)
        {
            InitializeComponent();
            _pIDCard = pIDCard;
            _pAccountID = pAccountID;
            _pTypeScan = pTypeScan;
            _dtDGV = dtDGV;
            _pAccountName = pAccountName;

            FormShare.ShowData.Data_CUSTOMER dtCst = new FormShare.ShowData.Data_CUSTOMER(_pAccountID);
            _pPoint = Convert.ToDouble(dtCst.Customer_SPC_REMAINPOINT);
        }
        //Load
        private void MNFR_MAIN_Load(object sender, EventArgs e)
        {
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            RadButton_Apv.ButtonElement.ShowBorder = true;
            cashID = ""; cashName = "";
            cstIdCard = ""; cstNameCard = "";
            pointUse = 0; pApv = "0";
            radLabel_pointuse.Text = "0.00";
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("STOCK", "สต็อก", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 500));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "สั่ง"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("SPC_REDEMPPOINT", "แต้มที่ใช้", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_CHANGE", "STA_CHANGE"));

            RadGridView_Show.Columns["STOCK"].FormatString = "{0:#,##0.00}";
            RadGridView_Show.Columns["QTY"].FormatString = "{0:#,##0.00}";
            RadGridView_Show.Columns["SPC_REDEMPPOINT"].FormatString = "{0:#,##0.00}";

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", " STA_CHANGE = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_Show.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["STOCK"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["UNITID"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["SPC_REDEMPPOINT"].ConditionalFormattingObjectList.Add(obj2);


            radTextBox_IdCash.Enabled = true;
            radTextBox_IdCard.Enabled = true;
            radTextBox_CstName.Enabled = true;

            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
            radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red();
            radLabel_StatusBill.BackColor = Color.Transparent;


            radLabel_point.Text = _pPoint.ToString("#,#0.00");
            radLabel_Account.Text = "รหัสลูกค้า : " + _pAccountID;

            radTextBox_IdCash.Text = ""; radLabel_Cash.Text = "";

            radTextBox_IdCard.Text = "";
            radTextBox_CstName.Text = "";

            RadGridView_Show.DataSource = _dtDGV;
            _dtDGV.AcceptChanges();

            if (_pTypeScan == "1")
            {
                SetScan();
            }

            radTextBox_IdCash.Focus();
        }
        //SetScan
        void SetScan()
        {
            radTextBox_IdCard.Text = _pIDCard;
            cstIdCard = _pIDCard;
            radTextBox_IdCard.Enabled = false;

            radTextBox_CstName.Text = _pAccountName;
            cstNameCard = _pAccountName;
            radTextBox_CstName.Enabled = false;

        }
        //เชคสถานะก่อนบันทึก
        string CheckStaForSave()
        {
            string T = "1";
            if (_pIDCard == "")
            {
                T = "0";
            }

            if ((cashID == "") || (cashName == ""))
            {
                T = "0";
            }

            if ((cstIdCard == "") || (cstNameCard == ""))
            {
                T = "0";
            }

            return T;
        }

        #region "ROWS DGV"

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        private void RadTextBox_IdCash_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_IdCash.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("รหัสผู้แลกแต้ม");
                    radTextBox_IdCash.Focus();
                    return;
                }

                DataTable dt = Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_IdCash.Text);
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงานแลกแต้มที่ระบุ");
                    radTextBox_IdCash.SelectAll();
                    radTextBox_IdCash.Focus();
                    return;
                }

                if (dt.Rows[0]["PW_ID"].ToString() == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานที่ระบุนั้น ไม่ได้อยู่ในช่วงเวลาทำงาน" + Environment.NewLine
                       + @"ไม่สามารถทำการแลกแต้มให้ลูกค้าได้" + Environment.NewLine + @"เช็ครหัสที่ระบุอีกครั้ง.");
                    radTextBox_IdCash.SelectAll();
                    radTextBox_IdCash.Focus();
                    return;
                }

                if (dt.Rows[0]["PW_BRANCH_IN"].ToString() != SystemClass.SystemBranchID)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานที่ระบุนั้น ไม่ได้ลงเวลาทำงานในสาขา " + SystemClass.SystemBranchID + " " + SystemClass.SystemBranchName + Environment.NewLine
                       + @"ไม่สามารถทำการแลกแต้มให้ลูกค้าได้" + Environment.NewLine + @"เช็ครหัสที่ระบุอีกครั้ง.");
                    radTextBox_IdCash.SelectAll();
                    radTextBox_IdCash.Focus();
                    return;
                }

                cashID = dt.Rows[0]["EMP_ID"].ToString();
                cashName = dt.Rows[0]["SPC_NAME"].ToString();
                radLabel_Cash.Text = cashName;
                radTextBox_IdCash.Enabled = false;
                radTextBox_IdCash.Focus();
            }
        }
        //number only
        private void RadTextBox_IdCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Enter ID
        private void RadTextBox_IdCard_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_IdCard.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("รหัสบัตรประชาชนลูกค้าหรือพาสปอร์ต");
                    radTextBox_IdCard.Focus();
                    return;
                }

                if (radTextBox_IdCard.Text != _pIDCard)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสบัตรประชาชนลูกค้าหรือพาสปอร์ตที่ระบุ ไม่ตรงกับข้อมูลที่มีในระบบ" + Environment.NewLine +
                        "เช็คข้อมูลลูกค้าอีกครั้ง ก่อนระบุข้อมูลใหม่");
                    radTextBox_IdCard.SelectAll();
                    radTextBox_IdCard.Focus();
                    return;
                }

                cstIdCard = radTextBox_IdCard.Text;
                radTextBox_IdCard.Enabled = false;
                radTextBox_CstName.Focus();
            }
        }
        //Enter Name
        private void RadTextBox_CstName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CstName.Text == "")
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("ชื่อลูกค้า");
                    radTextBox_CstName.Focus();
                    return;
                }

                cstNameCard = radTextBox_CstName.Text;
                radTextBox_CstName.Enabled = false;
                RadButton_Apv.Focus();
            }
        }

        //Set choose Data
        void SetDataSTA()
        {
            if (pApv == "1")
            {
                return;
            }

            //Check จำนวนสต็อกก่อนการแลกแต้ม
            if (RadGridView_Show.CurrentRow.Cells["STA_CHANGE"].Value.ToString() == "2")
            {
                double stk = ItembarcodeClass.FindStock_ByBarcode(RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(), SystemClass.SystemBranchID);
                if (stk > 0)
                {
                    RadGridView_Show.CurrentRow.Cells["STA_CHANGE"].Value = "1";
                }
                else
                {
                    RadGridView_Show.CurrentRow.Cells["STA_CHANGE"].Value = "0";
                }

                RadGridView_Show.CurrentRow.Cells["STOCK"].Value = stk.ToString("#,#0.00");
            }

            //เข้า
            if (RadGridView_Show.CurrentRow.Cells["STA_CHANGE"].Value.ToString() == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("จำนวนสต็อกของสินค้าที่เลือกไม่ถูกต้อง [ติดลบหรือเท่ากับ 0]" + Environment.NewLine +
                    "ไม่สามารถเลือกเพื่อแลกแต้มได้" + Environment.NewLine + "จัดการสต็อกให้เรียบร้อยก่อนทำรายการใหม่");
                return;
            }

            DataRow[] drSTA = _dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
            string pSta = "0"; double pQty = 0;

            if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
            {
                InputData frmSTA = new InputData("0",
                                       RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                       "จำนวนที่ต้องการแลก", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                { pInputData = "1" };
                if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                {
                    pQty = Convert.ToDouble(frmSTA.pInputData);

                    double pPoint = pQty * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["SPC_REDEMPPOINT"].Value.ToString());
                    pointUse += pPoint;
                    if (pointUse > _pPoint)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ลูกค้าไม่มีแต้มมากพอที่จะแลกสินค้าได้ ตามที่ระบุ" + Environment.NewLine +
                            "ลองใหม่อีกครั้ง");
                        pointUse -= pPoint;
                        pSta = "0";
                        pQty = 0;
                    }
                    else
                    {
                        pSta = "1";
                    }
                }
            }
            else
            {
                double pPoint = double.Parse(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString()) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["SPC_REDEMPPOINT"].Value.ToString());
                pointUse -= pPoint;
                pSta = "0";
                pQty = 0;
            }

            radLabel_pointuse.Text = pointUse.ToString("#,#0.00");

            _dtDGV.Rows[_dtDGV.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
            _dtDGV.Rows[_dtDGV.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
            _dtDGV.AcceptChanges();
        }
        //ระบุจำนวน
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "STA":
                    if (CheckStaForSave() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูล [ด้านขวา] ให้เรียบร้อยก่อนการระบุสินค้าที่ต้องการแลก");
                        return;
                    }

                    SetDataSTA();
                    break;
                default:
                    return;
            }
        }
        //แก้ไขจำนวน
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (pApv == "1")
            {
                return;
            }


            switch (e.Column.Name)
            {
                case "STA":

                    if (CheckStaForSave() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูล [ด้านขวา] ให้เรียบร้อยก่อนการระบุสินค้าที่ต้องการแลก");
                        return;
                    }


                    SetDataSTA();
                    break;
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
                    {
                        return;
                    }

                    if (CheckStaForSave() == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูล [ด้านขวา] ให้เรียบร้อยก่อนการระบุสินค้าที่ต้องการแลก");
                        return;
                    }

                    double pPointOld = double.Parse(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString()) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["SPC_REDEMPPOINT"].Value.ToString());


                    DataRow[] drQTY = _dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                    InputData frmQTY = new InputData("0",
                                              RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                              "จำนวนที่ต้องการแลก", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() };

                    if (frmQTY.ShowDialog(this) == DialogResult.Yes)
                    {
                        double pQty = Convert.ToDouble(frmQTY.pInputData);
                        double pPointNew = pQty * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["SPC_REDEMPPOINT"].Value.ToString());

                        if ((pointUse - pPointOld + pPointNew) > _pPoint)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("ลูกค้าไม่มีแต้มมากพอที่จะแลกสินค้าได้ ตามที่ระบุ" + Environment.NewLine +
                                "ลองใหม่อีกครั้ง");
                            radLabel_pointuse.Text = pointUse.ToString("#,#0.00");
                            return;
                        }
                        pointUse = pointUse - pPointOld + pPointNew;
                        string pSta;
                        if (Convert.ToDouble(frmQTY.pInputData) == 0)
                        { pSta = "0"; }
                        else { pSta = "1"; }

                        radLabel_pointuse.Text = pointUse.ToString("#,#0.00");

                        _dtDGV.Rows[_dtDGV.Rows.IndexOf(drQTY[0])]["STA"] = pSta;
                        _dtDGV.Rows[_dtDGV.Rows.IndexOf(drQTY[0])]["QTY"] = pQty.ToString("#,##0.00");
                        _dtDGV.AcceptChanges();
                    }

                    break;
                default:
                    return;
            }
        }
        //print
        void PrintData()
        {
            PrintDialog printDlg = new PrintDialog();
            if (printDlg.ShowDialog(this) == DialogResult.OK)
            {
                PrintDocument.PrintController = printController;

                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDlg.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument.PrinterSettings = printDlg.PrinterSettings;
                PrintDocument.Print();
            }
        }
        //อนุมัติ
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            if (pApv == "1")
            {
                PrintData();
                return;
            }

            if (CheckStaForSave() == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูล [ด้านขวา] ให้เรียบร้อยก่อนการอนุมัติบิล");
                return;
            }

            int iRows = 0;
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                if (RadGridView_Show.Rows[i].Cells["STA"].Value.ToString() == "1")
                {
                    iRows++;
                }
            }

            if (iRows == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่มีรายการสินค้าที่แลกแต้ม ไม่สามารถบันทึกข้อมูลได้" + Environment.NewLine +
                    "ระบุข้อมูลให้เรียบร้อยแล้วลองใหม่อีกครั้ง");
                return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการแลกแต้มสินค้า ไม่สามารถแก้ไขรายการได้") == DialogResult.No)
            {
                return;
            }

            FormShare.ShowData.Data_CUSTOMER dtCst = new FormShare.ShowData.Data_CUSTOMER(_pAccountID);
            double reCheckpoint = Convert.ToDouble(dtCst.Customer_SPC_REMAINPOINT);
            if (reCheckpoint < pointUse)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("เกิดข้อผิดพลาดในการบันทึกการแลกแต้ม" + Environment.NewLine +
                    "เช็คแต้มลูกค้าใหม่อีกครั้ง ก่อนทำรายการใหม่");
                return;
            }


            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNFR", "-", "MNFR", "1");
            double sPoint = _pPoint - pointUse;
            ArrayList sqlIn = new ArrayList
            {
                String.Format(@"
                    INSERT INTO SHOP_MNFR_REDEMPTABLE ( 
                    ACCOUNTNUM,REDEMPID,POINTTOTAL,
                    POINTCUR,POINTRemain,
                    CREATEDBY, INVENTLOCATIONID,INVENTSITEID, 
                    IDENTIFICATIONNUMBER, IDENTIFICATIONNAME,CREATEDBYNAME,INVENTLOCATIONNAME)
                    VALUES( '" +_pAccountID + @"','" + maxDocno + @"','" + _pPoint + @"',
                    '" + pointUse + @"','" + sPoint + @"',
                    '" + cashID + @"','" + SystemClass.SystemBranchID + @"','SPC',
                    '" + cstIdCard + @"','" + cstNameCard + @"','" + radLabel_Cash.Text + @"','" + SystemClass.SystemBranchName + @"'
                    )
                ")
            };

            ArrayList sqlAX = new ArrayList {
                String.Format(@"
                    INSERT INTO SPC_REDEMPTABLE00 
                    (ACCOUNTNUM,TRANSDATE,REDEMPID,
                    EMPLPOST,INVENTLOCATIONID,INVENTSITEID,RECID,DATAAREAID,
                    IDENTIFICATIONNUMBER,IDENTIFICATIONNAME) 
                    VALUES( '" + _pAccountID + @"',convert(varchar,getdate(),23),'" + maxDocno + @"',
                    '" + cashID + @"','" + SystemClass.SystemBranchID + @"','SPC','1','SPC',
                    '" + cstIdCard + @"','" + cstNameCard + @"'
                    )
                ") };

            int ii = 0;
            for (int i = 0; i < _dtDGV.Rows.Count; i++)
            {
                if (RadGridView_Show.Rows[i].Cells["STA"].Value.ToString() == "1")
                {
                    ii++;
                    double sumPoint = double.Parse(RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) * double.Parse(RadGridView_Show.Rows[i].Cells["SPC_REDEMPPOINT"].Value.ToString());
                    sqlIn.Add(String.Format(@"
                        INSERT INTO   SHOP_MNFR_REDEMPLINE  
                        (REDEMPID,REDEMPPOINT,POINTTOTAL,QTY,
                        ITEMBARCODE,LINENUM,UNITID,
                        INVENTDIMID,ITEMID, 
                        CREATEDBY, Name) VALUES (
                        '" + maxDocno + @"','" + double.Parse(RadGridView_Show.Rows[i].Cells["SPC_REDEMPPOINT"].Value.ToString()) + @"','" + sumPoint + @"','" + RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + @"',
                        '" + RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"','" + ii + @"','" + RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                        '" + RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"','" + RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                        '" + cashID + @"','" + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"')
                    "));

                    sqlAX.Add(string.Format(@"
                        INSERT INTO   SPC_REDEMPLINE00 
                        (REDEMPID,QTY,ITEMBARCODE,
                        LINENUM,Name,RECID,DATAAREAID,RECVERSION) VALUES (
                        '" + maxDocno + @"','" + RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + @"','" + RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                        '" + ii + @"','" + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"','1','SPC','1')
                    "));
                }
            }

            String T = ConnectionClass.ExecuteMain_AX_24_SameTime(sqlIn, sqlAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : อนุมัติแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel();
                pApv = "1";
                PrintData();
                RadButton_Apv.Text = "พิมพ์ซ้ำ";
            }
        }
        //print
        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            barcode.Data = radLabel_Docno.Text;
            Bitmap barcodeInBitmap0 = new Bitmap(barcode.drawBarcode());
            int Y = 0;

            e.Graphics.DrawString("ใบสินค้าแลกแต้ม.", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemBranchID + "-" + SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap0, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            int ii = 0;
            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                if (RadGridView_Show.Rows[i].Cells["STA"].Value.ToString() == "1")
                {
                    ii++;
                    Y += 20;
                    e.Graphics.DrawString((ii).ToString() + ".(" + RadGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + " " +
                                      RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + ")   " +
                                      RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                         SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString(" " + RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString(),
                        SystemClass.printFont, Brushes.Black, 0, Y);
                }
            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("ผู้บันทึก : " + SystemClass.SystemUserID + " " + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ลูกค้า : " + "" + _pAccountID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString(radTextBox_CstName.Text, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            double sPoint = _pPoint - pointUse;
            e.Graphics.DrawString("จำนวนแต้มก่อนใช้แลก : " + _pPoint.ToString("#,#0.00"), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("จำนวนแต้มที่ใช้แลก   : " + pointUse.ToString("#,#0.00"), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("จำนวนแต้มที่เหลือ    : " + sPoint.ToString("#,#0.00"), SystemClass.printFont, Brushes.Black, 10, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;


        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}
