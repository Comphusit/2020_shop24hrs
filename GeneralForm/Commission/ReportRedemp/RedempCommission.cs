﻿//CheckOK
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.Commission.ReportRedeem
{
    public partial class RedempCommission : Telerik.WinControls.UI.RadForm
    {
        DataTable dtBranch = new DataTable();
        string termCode, report_describe;

        readonly string _pType;//0 ข้อมูลรายพนักงาน 1 Report 2 ข้อมูลรายสาขาทุกสาขา
        readonly string _pOpen;//เปิดจาก
        DataTable dtPercent;
        public RedempCommission(string pOpen, string pType)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pOpen = pOpen;
            _pType = pType;
        }
        //กด ESC เพื่อปิดหน้าต่างนี้
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        //on load
        private void RedempCommission_Load(object sender, EventArgs e)
        {
            #region SetProperty
            radStatusStrip1.SizingGrip = false;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_Save.ToolTipText = "บันทึกข้อมูล"; radButtonElement_Save.ShowBorder = true;
            radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร"; radButtonElement_Print.ShowBorder = true;
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_pdf.ShowBorder = true;

            RadButton_Search.ButtonElement.ShowBorder = true;

            dtPercent = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("39", "", "", "1");

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Fisrt, DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(-1));
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            #endregion

            #region RadDropDownListBranch
            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            RadDropDownList_Branch.DataSource = dtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            #endregion

            radButtonElement_Print.Enabled = false;
            radButtonElement_Save.Enabled = false;

            if (_pOpen.Equals("SHOP"))
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                RadDropDownList_Branch.Enabled = false;
                RadCheckBox_Branch.Checked = true; RadCheckBox_Branch.ReadOnly = true;
                radButtonElement_excel.Enabled = false;
                radButtonElement_Save.Enabled = false;
                radButtonElement_Print.Enabled = false;
            }
            else
            {
                RadDropDownList_Branch.Visible = false; RadCheckBox_Branch.Visible = false;
            }

            switch (_pType)
            {
                case "0":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SUM", "ยอดสาขา"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 65));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDBY", "รหัส", 95));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDBYNAME", "ชื่อผู้แลก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 180));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NEW_CST", $@"จน.ลูกค้า", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAY_CST", $@"รวมลูกค้า", 80));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NEW_REDEM", $@"จน.บิล", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAY_REDIM", $@"รวมบิล", 80));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NEW_POINT", $@"จน.แต้ม", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAY_POINT", $@"รวมแต้ม", 80));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE", $@"ยอดรับ", 140));

                    GridViewSummaryItem summaryItem = new GridViewSummaryItem("RATE", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryItem1 = new GridViewSummaryRowItem { summaryItem };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryItem1);
                    if (_pOpen.Equals("SHOP"))
                        radLabel_Detail.Text = "อัตราการจ่าย จำนวนลูกค้า 50% จำนวนบิล 30% จำนวนแจ้ม 20% [คิดจากอัตราทะลุเป้าต่อสาขา]";
                    else
                        radLabel_Detail.Text = "กด + เพื่อบันทึกข้อมูลเข้าระบบ | กด พิมพ์ เพื่อพิมพ์เอกสารและส่งข้อมูลเข้ารอบจ่ายเงิน | อัตราการจ่าย จำนวนลูกค้า 50% จำนวนบิล 30% จำนวนแจ้ม 20%";
                    break;


                case "2":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 90));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อ", 120));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NEW_CST", $@"จำนวนลูกค้า{Environment.NewLine}เดือนนี้", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("OLD_CST", $@"จำนวนลูกค้า{Environment.NewLine}เดือนที่แล้ว", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE_CST", $@"%จำนวนลูกค้า", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAY_CST", $@"เฉลี่ย/ลูกค้า", 100));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NEW_REDEM", $@"จำนวนบิล{Environment.NewLine}เดือนนี้", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("OLD_REDEM", $@"จำนวนบิล{Environment.NewLine}เดือนที่แล้ว", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE_REDIM", $@"%จำนวนบิล", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAY_REDIM", $@"เฉลี่ย/บิล", 100));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NEW_POINT", $@"จำนวนแต้ม{Environment.NewLine}เดือนนี้", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("OLD_POINT", $@"จำนวนแต้ม{Environment.NewLine}เดือนที่แล้ว", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE_POINT", $@"%จำนวนแต้ม", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PAY_POINT", $@"เฉลี่ย/แต้ม", 100));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE", $@"ยอดค่าคอมที่ได้", 150));

                    GridViewSummaryItem summaryItemDT = new GridViewSummaryItem("RATE", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemDT = new GridViewSummaryRowItem { summaryItemDT };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemDT);

                    DatagridClass.SetCellBackClolorByExpression("RATE", " RATE = 0  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("RATE", " RATE = 3000  ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                    radLabel_Detail.Text = "เปรียบเทียบยอดแลกแต้มลูกค้ากับเดือนที่ผ่านมา | ช่องสุดท้ายเป็นยอดรวมจ่ายของแต่ละสาขา";
                    break;
            }
        }

        #region ROWS DGV
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        #region Event
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            string condition_branch = "";
            if (RadCheckBox_Branch.Checked) condition_branch = RadDropDownList_Branch.SelectedValue.ToString();

            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            switch (_pType)
            {
                case "0":
                    report_describe = @"พนักงานแลกแต้มลูกค้า";

                    if (CheckDataInsertRound() > 0)
                    {
                        radButtonElement_Print.Enabled = true;
                        radButtonElement_Save.Enabled = false;
                    }
                    else
                    {
                        radButtonElement_Save.Enabled = true;
                        radButtonElement_Print.Enabled = false;
                    }

                    CheckRateEMP();
                    break;
                case "2":
                    CheckRateBCH();
                    break;
            }
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });

            if (_pOpen.Equals("SHOP"))
            {
                radButtonElement_excel.Enabled = false;
                radButtonElement_Save.Enabled = false;
                radButtonElement_Print.Enabled = false;
            }
        }
        private void RadDateTimePicker_Fisrt_ValueChanged(object sender, EventArgs e)
        {
            radDateTimePicker_Fisrt.Value = new DateTime(radDateTimePicker_Fisrt.Value.Year, radDateTimePicker_Fisrt.Value.Month, 1);
        }
        private void RadDateTimePicker_Fisrt_ValueChanging(object sender, ValueChangingEventArgs e)
        {
            radDateTimePicker_Fisrt.Value = new DateTime(radDateTimePicker_Fisrt.Value.Year, radDateTimePicker_Fisrt.Value.Month, 1);
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string ret = DatagridClass.ExportExcelGridView(report_describe, RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(ret);
        }

        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.CheckState == CheckState.Checked)
            {
                if (!_pOpen.Equals("SHOP"))
                {
                    RadDropDownList_Branch.Enabled = true;
                }
            }
            else
            {
                RadDropDownList_Branch.Enabled = false;
            }
        }

        private void RadButtonElement_Save_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (radDateTimePicker_Fisrt.Value.ToString("yyyyy-MM") == DateTime.Now.ToString("yyyyy-MM"))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกข้อมูลได้ ไม่อยู่ในรอบจ่ายเงิน."); return;
            }

            if (CheckDataInsertRound() > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลได้บันทึกเรียบร้อยแล้ว.");
                radButtonElement_Print.Enabled = true; return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ค่าคอมของพนักงานแลกแต้ม") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;

            ArrayList arlist = new ArrayList();
            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                if (Double.Parse(RadGridView_ShowHD.Rows[i].Cells["RATE"].Value.ToString()) > 0)
                {
                    arlist.Add($@" 
                    INSERT INTO SHOP_EMPREDEMPCOMM 
                                (ROUNDDATE, BRANCHID, BRANCHNAME, 
                                EMPLID, EMPLNAME,
                                EMPLDESC,WHOINS,
                                REDMPCUS,REDMPCUS_PAY,
                                REDMPSUM,REDMPSUM_PAY,
                                REDMPPOIN,REDMPPOIN_PAY,
                                EMPLSUM)
                    VALUES (    '{radDateTimePicker_Fisrt.Value:yyyy-MM-dd}','{RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value}','{RadGridView_ShowHD.Rows[i].Cells["BRANCH_NAME"].Value}',
                                '{RadGridView_ShowHD.Rows[i].Cells["CREATEDBY"].Value}','{RadGridView_ShowHD.Rows[i].Cells["CREATEDBYNAME"].Value}',
                                '{RadGridView_ShowHD.Rows[i].Cells["POSSITION"].Value}','{SystemClass.SystemUserID_M}',
                                '{RadGridView_ShowHD.Rows[i].Cells["NEW_CST"].Value}','{RadGridView_ShowHD.Rows[i].Cells["PAY_CST"].Value}',
                                '{RadGridView_ShowHD.Rows[i].Cells["NEW_REDEM"].Value}','{RadGridView_ShowHD.Rows[i].Cells["PAY_REDIM"].Value}',
                                '{RadGridView_ShowHD.Rows[i].Cells["NEW_POINT"].Value}','{RadGridView_ShowHD.Rows[i].Cells["PAY_POINT"].Value}',
                                '{RadGridView_ShowHD.Rows[i].Cells["RATE"].Value}')"
                  );
                }
            }
            if (arlist.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลการจ่ายค่าคอมของการแลกแต้มในเดือนที่ระบุ{Environment.NewLine}ให้เช็คข้อมูลใหม่อรกครั้ง{Environment.NewLine}หรือไม่ก็ไม่มีพนักงานที่ต้องได้รับค่าคอม");
                this.Cursor = Cursors.Default;
                return;
            }

            string _ret = ConnectionClass.ExecuteSQL_ArrayMain(arlist);
            MsgBoxClass.MsgBoxShow_SaveStatus(_ret);
            if (_ret == "")
            {
                radButtonElement_Print.Enabled = true;
                radButtonElement_Save.Enabled = false;
            }
            this.Cursor = Cursors.Default;

        }

        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            if (_pType == "0")
            {
                if (CheckDataInsertRound() == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลยังไม่ได้บันทึก เช็คใหม่อีกครั้ง.");
                    return;
                }

                string YY = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Year.ToString();
                string MM = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Month.ToString();
                //termCode = CheckDataInsertRound701(first_date);
                if (termCode.Equals(""))
                {

                    string sqlSendAX = $@"
                        SELECT	EMPLID,SUM_PAY
                        FROM	(
		                        SELECT  EMPLID,SUM(EMPLSUM) AS SUM_PAY
		                        FROM	SHOP_EMPREDEMPCOMM WITH (NOLOCK)  
		                        WHERE	YEAR(ROUNDDATE) = '{YY}'  AND MONTH(ROUNDDATE) = '{MM}'
		                        GROUP BY EMPLID
		                        )TMP
                        WHERE	SUM_PAY > 0
                    ";
                    DataTable dtSendAX = ConnectionClass.SelectSQL_Main(sqlSendAX);
                    if (dtSendAX.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลพนักงานที่จะต้องจ่าย{Environment.NewLine}ค่าคอมพนักงานแลกแต้ม เช็คข้อมูลใหม่อีกครั้ง");
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ส่งข้อมูลเข้างวดเงินเดือน ไม่สามารถแก้ไขได้") == DialogResult.No) return;

                    ArrayList InStr = new ArrayList();
                    for (int i = 0; i < dtSendAX.Rows.Count; i++)
                    {
                        InStr.Add($@"
                                INSERT INTO tblTempInsertDt (tEmpCode,tEmpAmount ) VALUES (
                                '{dtSendAX.Rows[i]["EMPLID"]}','{dtSendAX.Rows[i]["SUM_PAY"]}') ");
                    }

                    string _ret = Insert701TermCode(InStr);
                    MsgBoxClass.MsgBoxShow_SaveStatus(_ret);
                    if (_ret == "") this.PrintDocument();

                }
                else
                {
                    this.PrintDocument();   //พิมพ์
                }
            }

        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pType);
        }
        #endregion

        #region Method
        // Check Data  Insert หรือยัง
        private int CheckDataInsertRound()
        {
            string YY = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Year.ToString();
            string MM = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Month.ToString();
            string sql = $@"
                SELECT  ROUNDDATE,tTermCode
                FROM	SHOP_EMPREDEMPCOMM WITH (NOLOCK)  
                WHERE	YEAR(ROUNDDATE) = '{YY}'  AND MONTH(ROUNDDATE) = '{MM}'
                GROUP BY ROUNDDATE,tTermCode ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            termCode = "";
            if (dt.Rows.Count > 0)
            {
                termCode = dt.Rows[0]["tTermCode"].ToString();
            }
            return dt.Rows.Count;
        }

        #region Insert701
        private string Insert701TermCode(ArrayList InStrAX)
        {
            string pDateF = radDateTimePicker_Fisrt.Value.ToString("yyyy-MM-dd");
            string pDateE = radDateTimePicker_Fisrt.Value.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");
            string ret;
            string termDetail = $@"ค่าคอม พนง มินิมาร์ท.แลกแต้มลูกค้า รอบ {pDateF} - {pDateE} ";

            //Dim sqlStr As String = ""
            string sqlUPSend = $@"EXEC SPCN_Salary_InsertFromTemp '{pDateF}','{pDateE}','{termDetail}' ";

            //701
            SqlConnection sqlConnection701 = new SqlConnection(IpServerConnectClass.Con701SRV);
            if (sqlConnection701.State == ConnectionState.Closed)
            {
                sqlConnection701.Open();
            }
            //  SqlCommand sqlCommand24 = new SqlCommand();
            SqlCommand sqlCommand701 = sqlConnection701.CreateCommand();
            SqlTransaction transaction701 = sqlConnection701.BeginTransaction("701");
            sqlCommand701.Connection = sqlConnection701;
            sqlCommand701.Transaction = transaction701;

            //119
            SqlConnection sqlConnection119 = new SqlConnection(IpServerConnectClass.ConSelectMain);
            if (sqlConnection119.State == ConnectionState.Closed)
            {
                sqlConnection119.Open();
            }
            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommand119 = sqlConnection119.CreateCommand();
            SqlTransaction transaction119 = sqlConnection119.BeginTransaction("119");
            sqlCommand119.Connection = sqlConnection119;
            sqlCommand119.Transaction = transaction119;

            try
            {
                for (int iAX = 0; iAX < InStrAX.Count; iAX++)
                {
                    sqlCommand701.CommandText = Convert.ToString(InStrAX[iAX]);
                    sqlCommand701.ExecuteNonQuery();
                }

                DataTable dt = new DataTable();
                sqlCommand701.CommandText = sqlUPSend;
                SqlDataAdapter adp = new SqlDataAdapter(sqlCommand701);
                adp.Fill(dt);
                if ((dt.Rows.Count == 0) || (dt.Rows[0][0].ToString() == ""))
                {
                    transaction701.Rollback();
                    transaction119.Rollback();
                    ret = "ไม่สามารถจัดการข้อมูลในส่วนของงวดเงินเดือน ค่าคอมออร์เดอร์ลูกค้าได้ ลองใหม่อีกครั้ง";
                }
                else
                {
                    termCode = dt.Rows[0][0].ToString();
                    string sqlUp24 = $@"
                        Update  SHOP_EMPREDEMPCOMM 
                        SET     STASENDDATA = '1',tTermCode = '{termCode}',DATEUPD = getdate(),WHOUPD = '{SystemClass.SystemUserID_M}'
                        WHERE	ROUNDDATE = '{radDateTimePicker_Fisrt.Value:yyyy-MM-dd}' ";

                    sqlCommand119.CommandText = sqlUp24;
                    sqlCommand119.ExecuteNonQuery();

                    transaction701.Commit();
                    transaction119.Commit();
                    //TermAX = ptCheckTerm;
                    ret = "";
                }
            }
            catch (Exception ex)
            {
                transaction701.Rollback();
                transaction119.Rollback();

                return ex.Message;
            }
            finally
            {
                radButtonElement_excel.Enabled = true;
                sqlConnection701.Close();
                sqlConnection119.Close();
            }
            return ret;
        }
        #endregion
        private int GetCountPrintDocument()
        {
            string YY = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Year.ToString();
            string MM = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Month.ToString();
            string sql = ($@"
                    SELECT  ISNULL(MAX(ISNULL(tNumberPrint,0)),0)+1 AS tNumberPrint 
                    FROM    SHOP_EMPREDEMPCOMM WITH (NOLOCK) 
                    WHERE	YEAR(ROUNDDATE) = '{YY}'  AND MONTH(ROUNDDATE) = '{MM}'
                    ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            return dt.Rows.Count > 0 ? Convert.ToInt32(dt.Rows[0]["tNumberPrint"]) : 0;
        }
        private void PrintDocument()
        {

            if (_pOpen.Equals("SHOP")) return;

            RadGridView_ShowHD.FilterDescriptors.Clear();

            GridPrintStyle style = new GridPrintStyle
            {
                FitWidthMode = PrintFitWidthMode.FitPageWidth,
                PrintAllPages = true,
                PrintGrouping = true,
                PrintHeaderOnEachPage = true,
                PrintSummaries = true,
                CellFont = new Font("Tahoma", 8, FontStyle.Regular),
                HeaderCellBackColor = Color.Transparent,
                HeaderCellFont = new Font("Tahoma", 10, FontStyle.Bold),
                CellPadding = new Padding(0, 0, 0, 0),
                SummaryCellBackColor = ConfigClass.SetColor_PurplePastel(),
                SummaryCellFont = new Font("Tahoma", 10, FontStyle.Bold)
            };

            RadGridView_ShowHD.PrintStyle = style;
            RadPrintDocument document = new RadPrintDocument();
            int numPrint = GetCountPrintDocument();
            DatagridClass.SetPrintDocument(document, string.Format($@"ค่าคอมแลกแต้มมินิมาร์ท [{termCode}] วันที่ {radDateTimePicker_Fisrt.Value:yyyy-MM-dd} - {radDateTimePicker_Fisrt.Value.AddMonths(1).AddDays(-1):yyyy-MM-dd}"), numPrint);

            document.AssociatedObject = this.RadGridView_ShowHD;
            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document);
            if (termCode == "")
            {
                dialog.ToolMenu.Visible = false;
                dialog.ToolCommandBar.Visible = false;
                dialog.ShowDialog();
            }
            else
            {
                dialog.ToolMenu.Visible = true;
                dialog.ToolCommandBar.Visible = true;

                string YY = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Year.ToString();
                string MM = DateTime.Parse(radDateTimePicker_Fisrt.Value.ToString()).Month.ToString();

                string sqlUpd = $@"
                        Update  SHOP_EMPREDEMPCOMM 
                        SET     tNumberPrint = '{numPrint}',DATEUPD = getdate(),WHOUPD = '{SystemClass.SystemUserID_M}'
                        WHERE   YEAR(ROUNDDATE) = '{YY}'  AND MONTH(ROUNDDATE) = '{MM}' ";
                string retUp = ConnectionClass.ExecuteSQL_Main(sqlUpd);
                if (retUp == "") dialog.ShowDialog();
                else MsgBoxClass.MsgBoxShow_SaveStatus(retUp);
            }
        }

        #endregion


        //ยอดการจ่ายในแต่ละสาขา
        void CheckRateBCH()
        {
            string condition_branch = "MN%";
            if (RadCheckBox_Branch.Checked) condition_branch = RadDropDownList_Branch.SelectedValue.ToString();

            string Y_NEW = radDateTimePicker_Fisrt.Value.Year.ToString();
            string M_NEW = radDateTimePicker_Fisrt.Value.Month.ToString();
            string Y_OLD = Y_NEW;
            string M_OLD = radDateTimePicker_Fisrt.Value.AddMonths(-1).Month.ToString();
            if (M_NEW == "1") { Y_OLD = radDateTimePicker_Fisrt.Value.AddYears(-1).Year.ToString(); }

            string sql = $@"
            DECLARE	@Y_NEW	AS NVARCHAR(10) = '{Y_NEW}'
            DECLARE	@M_NEW	AS NVARCHAR(10) = '{M_NEW}'
            DECLARE	@Y_OLD	AS NVARCHAR(10) = '{Y_OLD}'
            DECLARE	@M_OLD	AS NVARCHAR(10) = '{M_OLD}'
            DECLARE	@Bch	AS NVARCHAR(500) = '{condition_branch}'

            SELECT	DATA_NEW.INVENTLOCATIONID AS BRANCH_ID,DATA_NEW.INVENTLOCATIONNAME AS BRANCH_NAME,
		            ISNULL(DATA_NEW.COUNT_CST,0) AS NEW_CST,ISNULL(DATA_OLD.COUNT_CST,0) AS OLD_CST,
		            ISNULL(DATA_NEW.COUNT_REDEMPID,0) AS NEW_REDEM,ISNULL(DATA_OLD.COUNT_REDEMPID,0) AS OLD_REDEM,
		            ISNULL(DATA_NEW.SUM_POINT,0) AS NEW_POINT,ISNULL(DATA_OLD.SUM_POINT,0) AS OLD_POINT,
                    '0' AS RATE_CST,'0' AS RATE_REDIM,'0' AS RATE_POINT,'0' AS RATE,
                    '0' AS PAY_CST,'0' AS PAY_REDIM,'0' AS PAY_POINT,
                    '0' AS LINE_CST,'0' AS LINE_REDIM,'0' AS LINE_POINT
            FROM	(
		            SELECT	TMP1.INVENTLOCATIONID,TMP1.INVENTLOCATIONNAME,TMP2.COUNT_CST,TMP1.COUNT_REDEMPID,TMP1.SUM_POINT	
		            FROM	(
				            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,COUNT(REDEMPID) AS COUNT_REDEMPID,SUM(POINTCUR) AS SUM_POINT
				            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
				            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
				            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME
				            )TMP1 LEFT OUTER JOIN 
				            (
				            SELECT	INVENTLOCATIONID,COUNT(ACCOUNTNUM) AS COUNT_CST
				            FROM	(
						            SELECT	INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
						            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
						            GROUP BY INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            )TMP
				            GROUP BY INVENTLOCATIONID
				            )TMP2 ON TMP1.INVENTLOCATIONID = TMP2.INVENTLOCATIONID
		            )DATA_NEW
		            LEFT OUTER JOIN
		            (
		            SELECT	TMP1.INVENTLOCATIONID,TMP1.INVENTLOCATIONNAME,TMP2.COUNT_CST,TMP1.COUNT_REDEMPID,TMP1.SUM_POINT	
		            FROM	(
				            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,COUNT(REDEMPID) AS COUNT_REDEMPID,SUM(POINTCUR) AS SUM_POINT
				            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
				            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_OLD AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_OLD
				            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME
				            )TMP1 LEFT OUTER JOIN 
				            (
				            SELECT	INVENTLOCATIONID,COUNT(ACCOUNTNUM) AS COUNT_CST
				            FROM	(
						            SELECT	INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
						            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_OLD AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_OLD
						            GROUP BY INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            )TMP
				            GROUP BY INVENTLOCATIONID
				            )TMP2 ON TMP1.INVENTLOCATIONID = TMP2.INVENTLOCATIONID
		            )DATA_OLD ON DATA_NEW.INVENTLOCATIONID = DATA_OLD.INVENTLOCATIONID
            WHERE	DATA_NEW.INVENTLOCATIONID LIKE @Bch
            ORDER BY BRANCH_ID
            ";


            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_ShowHD.DataSource = dt;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                double NEW_CST = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["NEW_CST"].Value.ToString());
                double OLD_CST = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["OLD_CST"].Value.ToString());
                double P_CST = ((NEW_CST - OLD_CST) / OLD_CST) * 100;

                double NEW_REDEM = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["NEW_REDEM"].Value.ToString());
                double OLD_REDEM = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["OLD_REDEM"].Value.ToString());
                double P_REDEM = ((NEW_REDEM - OLD_REDEM) / OLD_REDEM) * 100;
                double NEW_POINT = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["NEW_POINT"].Value.ToString());
                double OLD_POINT = Double.Parse(RadGridView_ShowHD.Rows[i].Cells["OLD_POINT"].Value.ToString());
                double P_POINT = ((NEW_POINT - OLD_POINT) / OLD_POINT) * 100;

                RadGridView_ShowHD.Rows[i].Cells["RATE_CST"].Value = P_CST.ToString("N2");
                RadGridView_ShowHD.Rows[i].Cells["RATE_REDIM"].Value = P_REDEM.ToString("N2");
                RadGridView_ShowHD.Rows[i].Cells["RATE_POINT"].Value = P_POINT.ToString("N2");

                double Grand;
                if (OLD_CST == 0) Grand = 1500;
                else
                {
                    if ((P_CST > 80) || (P_REDEM > 80) || (P_POINT > 80)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '80' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 70) || (P_REDEM > 70) || (P_POINT > 70)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '70' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 60) || (P_REDEM > 60) || (P_POINT > 60)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '60' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 50) || (P_REDEM > 50) || (P_POINT > 50)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '50' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 40) || (P_REDEM > 40) || (P_POINT > 70)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '40' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 30) || (P_REDEM > 30) || (P_POINT > 30)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '30' ")[0]["MaxImage"].ToString());
                    else Grand = 0;
                }

                RadGridView_ShowHD.Rows[i].Cells["RATE"].Value = Grand.ToString("#,##0.00");

                double pCst = (Grand * 0.5) / NEW_CST;
                double pReDim = (Grand * 0.3) / NEW_REDEM;
                double pPoint = (Grand * 0.2) / NEW_POINT;

                RadGridView_ShowHD.Rows[i].Cells["PAY_CST"].Value = (pCst).ToString("N2");
                RadGridView_ShowHD.Rows[i].Cells["PAY_REDIM"].Value = (pReDim).ToString("N2");
                RadGridView_ShowHD.Rows[i].Cells["PAY_POINT"].Value = pPoint.ToString("N2");
            }
        }
        //ยอดการจ่ายต่อตน
        void CheckRateEMP()
        {
            string condition_branch = "MN%";
            if (RadCheckBox_Branch.Checked) condition_branch = RadDropDownList_Branch.SelectedValue.ToString();

            string Y_NEW = radDateTimePicker_Fisrt.Value.Year.ToString();
            string M_NEW = radDateTimePicker_Fisrt.Value.Month.ToString();
            string Y_OLD = Y_NEW;
            string M_OLD = radDateTimePicker_Fisrt.Value.AddMonths(-1).Month.ToString();
            if (M_NEW == "1") { Y_OLD = radDateTimePicker_Fisrt.Value.AddYears(-1).Year.ToString(); }

            string sql = $@"
            DECLARE	@Y_NEW	AS NVARCHAR(10) = '{Y_NEW}'
            DECLARE	@M_NEW	AS NVARCHAR(10) = '{M_NEW}'
            DECLARE	@Y_OLD	AS NVARCHAR(10) = '{Y_OLD}'
            DECLARE	@M_OLD	AS NVARCHAR(10) = '{M_OLD}'
            DECLARE	@Bch	AS NVARCHAR(500) = '{condition_branch}'

            SELECT	DATA_NEW.INVENTLOCATIONID AS BRANCH_ID,DATA_NEW.INVENTLOCATIONNAME AS BRANCH_NAME,
		            ISNULL(DATA_NEW.COUNT_CST,0) AS NEW_CST,ISNULL(DATA_OLD.COUNT_CST,0) AS OLD_CST,
		            ISNULL(DATA_NEW.COUNT_REDEMPID,0) AS NEW_REDEM,ISNULL(DATA_OLD.COUNT_REDEMPID,0) AS OLD_REDEM,
		            ISNULL(DATA_NEW.SUM_POINT,0) AS NEW_POINT,ISNULL(DATA_OLD.SUM_POINT,0) AS OLD_POINT
            
            FROM	(
		            SELECT	TMP1.INVENTLOCATIONID,TMP1.INVENTLOCATIONNAME,TMP2.COUNT_CST,TMP1.COUNT_REDEMPID,TMP1.SUM_POINT	
		            FROM	(
				            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,COUNT(REDEMPID) AS COUNT_REDEMPID,SUM(POINTCUR) AS SUM_POINT
				            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
				            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
				            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME
				            )TMP1 LEFT OUTER JOIN 
				            (
				            SELECT	INVENTLOCATIONID,COUNT(ACCOUNTNUM) AS COUNT_CST
				            FROM	(
						            SELECT	INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
						            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
						            GROUP BY INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            )TMP
				            GROUP BY INVENTLOCATIONID
				            )TMP2 ON TMP1.INVENTLOCATIONID = TMP2.INVENTLOCATIONID
		            )DATA_NEW
		            LEFT OUTER JOIN
		            (
		            SELECT	TMP1.INVENTLOCATIONID,TMP1.INVENTLOCATIONNAME,TMP2.COUNT_CST,TMP1.COUNT_REDEMPID,TMP1.SUM_POINT	
		            FROM	(
				            SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,COUNT(REDEMPID) AS COUNT_REDEMPID,SUM(POINTCUR) AS SUM_POINT
				            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
				            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_OLD AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_OLD
				            GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME
				            )TMP1 LEFT OUTER JOIN 
				            (
				            SELECT	INVENTLOCATIONID,COUNT(ACCOUNTNUM) AS COUNT_CST
				            FROM	(
						            SELECT	INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
						            WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_OLD AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_OLD
						            GROUP BY INVENTLOCATIONID,ACCOUNTNUM,CREATEDBY
						            )TMP
				            GROUP BY INVENTLOCATIONID
				            )TMP2 ON TMP1.INVENTLOCATIONID = TMP2.INVENTLOCATIONID
		            )DATA_OLD ON DATA_NEW.INVENTLOCATIONID = DATA_OLD.INVENTLOCATIONID
            WHERE	DATA_NEW.INVENTLOCATIONID LIKE @Bch
            ORDER BY BRANCH_ID
            ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            DataTable dtRate = new DataTable();
            dtRate.Columns.Add("BRANCH_ID");
            dtRate.Columns.Add("PAY_CST");
            dtRate.Columns.Add("PAY_REDIM");
            dtRate.Columns.Add("PAY_POINT");
            dtRate.Columns.Add("SUM");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                double NEW_CST = Double.Parse(dt.Rows[i]["NEW_CST"].ToString());
                double OLD_CST = Double.Parse(dt.Rows[i]["OLD_CST"].ToString());
                double P_CST = ((NEW_CST - OLD_CST) / OLD_CST) * 100;

                double NEW_REDEM = Double.Parse(dt.Rows[i]["NEW_REDEM"].ToString());
                double OLD_REDEM = Double.Parse(dt.Rows[i]["OLD_REDEM"].ToString());
                double P_REDEM = ((NEW_REDEM - OLD_REDEM) / OLD_REDEM) * 100;

                double NEW_POINT = Double.Parse(dt.Rows[i]["NEW_POINT"].ToString());
                double OLD_POINT = Double.Parse(dt.Rows[i]["OLD_POINT"].ToString());
                double P_POINT = ((NEW_POINT - OLD_POINT) / OLD_POINT) * 100;

                double Grand;
                if (OLD_CST == 0) Grand = 1500;
                else
                {
                    if ((P_CST > 80) || (P_REDEM > 80) || (P_POINT > 80)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '80' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 70) || (P_REDEM > 70) || (P_POINT > 70)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '70' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 60) || (P_REDEM > 60) || (P_POINT > 60)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '60' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 50) || (P_REDEM > 50) || (P_POINT > 50)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '50' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 40) || (P_REDEM > 40) || (P_POINT > 70)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '40' ")[0]["MaxImage"].ToString());
                    else if ((P_CST > 30) || (P_REDEM > 30) || (P_POINT > 30)) Grand = double.Parse(dtPercent.Select($@" SHOW_NAME = '30' ")[0]["MaxImage"].ToString());
                    else Grand = 0;
                }

                double pCst = (Grand * 0.5) / NEW_CST;
                double pReDim = (Grand * 0.3) / NEW_REDEM;
                double pPoint = (Grand * 0.2) / NEW_POINT;

                dtRate.Rows.Add(dt.Rows[i]["BRANCH_ID"].ToString(), pCst, pReDim, pPoint, Grand);
            }

            //string sqlEmp = $@"
            //DECLARE	@Y_NEW	AS NVARCHAR(10) = '{Y_NEW}'
            //DECLARE	@M_NEW	AS NVARCHAR(10) = '{M_NEW}'
            //DECLARE	@Bch	AS NVARCHAR(500) = '{condition_branch}'

            //SELECT	TMP1.INVENTLOCATIONID AS BRANCH_ID,TMP1.INVENTLOCATIONNAME AS BRANCH_NAME,TMP1.CREATEDBY,TMP1.CREATEDBYNAME,
            //  ISNULL(TMP2.COUNT_CST,0) AS NEW_CST,ISNULL(TMP1.COUNT_REDEMPID,0) AS NEW_REDEM,ISNULL(TMP1.SUM_POINT,0) AS NEW_POINT,
            //        ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') AS POSSITION
            //FROM	(
            //  SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,CREATEDBYNAME,COUNT(REDEMPID) AS COUNT_REDEMPID,SUM(POINTCUR) AS SUM_POINT
            //  FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
            //  WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
            //  GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,CREATEDBYNAME
            //  )TMP1 LEFT OUTER JOIN 
            //  (
            //  SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,COUNT(ACCOUNTNUM) AS COUNT_CST
            //  FROM	(
            //    SELECT	INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,ACCOUNTNUM
            //    FROM	[dbo].[SHOP_MNFR_REDEMPTABLE]	WITH (NOLOCK)
            //    WHERE	YEAR(CONVERT(VARCHAR,TRANSDATE,23)) = @Y_NEW AND MONTH(CONVERT(VARCHAR,TRANSDATE,23)) = @M_NEW
            //    GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY,ACCOUNTNUM
            //    )TMP
            //  GROUP BY INVENTLOCATIONID,INVENTLOCATIONNAME,CREATEDBY
            //  )TMP2 ON TMP1.CREATEDBY = TMP2.CREATEDBY AND TMP1.INVENTLOCATIONID = TMP2.INVENTLOCATIONID
            //        LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)   
            // ON TMP1.CREATEDBY = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE  and HRPPARTYPOSITIONTABLERELAT2226.DATAAREAID  = 'SPC' 
            // AND  DATEADD(HOUR, 7, VALIDTODATETIME) >= CAST(GETDATE() AS DATE) 
            //        LEFT OUTER JOIN  SHOP2013TMP.dbo.HRPPartyJobTableRelationship WITH (NOLOCK) 
            // ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID and HRPPartyJobTableRelationship.DATAAREAID  = 'SPC'
            //WHERE	TMP1.INVENTLOCATIONID LIKE @Bch
            //ORDER BY TMP1.INVENTLOCATIONID,TMP1.CREATEDBY
            //";

            DataTable dtEmp = EmployeeClass.CommissionRedemp(Y_NEW, M_NEW, condition_branch); //ConnectionClass.SelectSQL_Main(sqlEmp);
            RadGridView_ShowHD.DataSource = dtEmp;

            for (int ii = 0; ii < dtEmp.Rows.Count; ii++)
            {
                DataRow[] dr = dtRate.Select($@" BRANCH_ID = '{dtEmp.Rows[ii]["BRANCH_ID"]}' ");
                double C = double.Parse(dtEmp.Rows[ii]["NEW_CST"].ToString());
                double R = double.Parse(dtEmp.Rows[ii]["NEW_REDEM"].ToString());
                double P = double.Parse(dtEmp.Rows[ii]["NEW_POINT"].ToString());
                double dr_Cst = 0, dr_Redim = 0, dr_Point = 0, dr_Sum = 0;
                if (dr.Length > 0)
                {
                    dr_Cst = double.Parse(dr[0]["PAY_CST"].ToString());
                    dr_Redim = double.Parse(dr[0]["PAY_REDIM"].ToString());
                    dr_Point = double.Parse(dr[0]["PAY_POINT"].ToString());
                    dr_Sum = double.Parse(dr[0]["SUM"].ToString());
                }
                RadGridView_ShowHD.Rows[ii].Cells["PAY_CST"].Value = (C * dr_Cst).ToString("N2");
                RadGridView_ShowHD.Rows[ii].Cells["PAY_REDIM"].Value = (R * dr_Redim).ToString("N2");
                RadGridView_ShowHD.Rows[ii].Cells["PAY_POINT"].Value = (P * dr_Point).ToString("N2");
                RadGridView_ShowHD.Rows[ii].Cells["SUM"].Value = dr_Sum.ToString("N2");
                RadGridView_ShowHD.Rows[ii].Cells["RATE"].Value = Math.Floor(((C * dr_Cst) + (R * dr_Redim) + (P * dr_Point))).ToString("N2");

                if (dr_Cst == 0) RadGridView_ShowHD.Rows[ii].IsVisible = false; else RadGridView_ShowHD.Rows[ii].IsVisible = true;

            }
        }
    }
}
