﻿namespace PC_Shop24Hrs.GeneralForm.Commission
{
    partial class Commission_EmpDownReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Commission_EmpDownReport));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_ShowDT = new Telerik.WinControls.UI.RadGridView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_D2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_D1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_UpAX = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT.MasterTemplate)).BeginInit();
            this.RadGridView_ShowDT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowDT, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(658, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 613);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(652, 19);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>กดปุ่ม / เพื่อ Up งวดเงินเดือนก่อนพิมพ์ | กดพิมพ์ทุกครั้งจะบันทึกจำนวนครั้ง" +
    "ที่พิมพ์เสมอ [ถ้า Up งวดเงินเดือนแล้ว]</html>";
            // 
            // RadGridView_ShowDT
            // 
            this.RadGridView_ShowDT.Controls.Add(this.splitter1);
            this.RadGridView_ShowDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowDT.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowDT.Location = new System.Drawing.Point(3, 308);
            // 
            // 
            // 
            this.RadGridView_ShowDT.MasterTemplate.ShowRowHeaderColumn = false;
            this.RadGridView_ShowDT.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowDT.Name = "RadGridView_ShowDT";
            // 
            // 
            // 
            this.RadGridView_ShowDT.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowDT.Size = new System.Drawing.Size(652, 299);
            this.RadGridView_ShowDT.TabIndex = 18;
            this.RadGridView_ShowDT.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowDT_ViewCellFormatting);
            this.RadGridView_ShowDT.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_ShowDT_ConditionalFormattingFormShown);
            this.RadGridView_ShowDT.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_ShowDT_FilterPopupRequired);
            this.RadGridView_ShowDT.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_ShowDT_FilterPopupInitialized);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 299);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(652, 299);
            this.RadGridView_ShowHD.TabIndex = 17;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowHD_ViewCellFormatting);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_ShowHD_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_ShowHD_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_ShowHD_FilterPopupInitialized);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.radDateTimePicker_D2);
            this.panel1.Controls.Add(this.radDateTimePicker_D1);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(667, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(204, 636);
            this.panel1.TabIndex = 1;
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(14, 51);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Date.TabIndex = 60;
            this.radLabel_Date.Text = "ระบุวันที่";
            // 
            // radDateTimePicker_D2
            // 
            this.radDateTimePicker_D2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D2.Location = new System.Drawing.Point(14, 100);
            this.radDateTimePicker_D2.Name = "radDateTimePicker_D2";
            this.radDateTimePicker_D2.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_D2.TabIndex = 59;
            this.radDateTimePicker_D2.TabStop = false;
            this.radDateTimePicker_D2.Text = "22/05/2020";
            this.radDateTimePicker_D2.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_D2.ValueChanged += new System.EventHandler(this.RadDateTimePicker_D2_ValueChanged);
            // 
            // radDateTimePicker_D1
            // 
            this.radDateTimePicker_D1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D1.Location = new System.Drawing.Point(14, 73);
            this.radDateTimePicker_D1.Name = "radDateTimePicker_D1";
            this.radDateTimePicker_D1.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_D1.TabIndex = 58;
            this.radDateTimePicker_D1.TabStop = false;
            this.radDateTimePicker_D1.Text = "22/05/2020";
            this.radDateTimePicker_D1.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_D1.ValueChanged += new System.EventHandler(this.RadDateTimePicker_D1_ValueChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_add,
            this.commandBarSeparator2,
            this.radButtonElement_excel,
            this.commandBarSeparator1,
            this.radButtonElement_UpAX,
            this.commandBarSeparator4,
            this.radButtonElement_print,
            this.commandBarSeparator5,
            this.RadButtonElement_pdt,
            this.commandBarSeparator6});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(204, 36);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "Export To Excel";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_UpAX
            // 
            this.radButtonElement_UpAX.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_UpAX.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radButtonElement_UpAX.Name = "radButtonElement_UpAX";
            this.radStatusStrip1.SetSpring(this.radButtonElement_UpAX, false);
            this.radButtonElement_UpAX.Text = "radButtonElement_UpAX";
            this.radButtonElement_UpAX.Click += new System.EventHandler(this.RadButtonElement_UpAX_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_print
            // 
            this.radButtonElement_print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_print.Name = "radButtonElement_print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_print, false);
            this.radButtonElement_print.Text = "radButtonElement1";
            this.radButtonElement_print.Click += new System.EventHandler(this.RadButtonElement_print_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(14, 142);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 0;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // Commission_EmpDownReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "Commission_EmpDownReport";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ค่าคอมของพนักงานลงสินค้าต่างจังหวัด [สรุป]";
            this.Load += new System.EventHandler(this.Commission_EmpDownReport_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT)).EndInit();
            this.RadGridView_ShowDT.ResumeLayout(false);
            this.RadGridView_ShowDT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D1;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowDT;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.Splitter splitter1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_UpAX;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
    }
}
