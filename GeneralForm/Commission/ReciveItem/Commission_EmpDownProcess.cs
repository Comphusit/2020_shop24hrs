﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Commission
{
    public partial class Commission_EmpDownProcess : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeOpen;
        readonly string _pTypeReport;//0 ประมวลผลค่าคอมแลกแต้ม 1 ลบข้อมูลการรับลัง 2 ค่าคอม PartTime พนักงาน supc

        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        //Load
        public Commission_EmpDownProcess(string pTypeOpen, string pTypeReport)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
            _pTypeReport = pTypeReport;
        }
        //Load
        private void Commission_EmpDownProcess_Load(object sender, EventArgs e)
        {

            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            radTextBox_CarBig.Visible = false; radTextBox_CarSmall.Visible = false;
            radLabel_10.Visible = false; radLabel_6.Visible = false;
            radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false; radLabel_Date.Visible = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            switch (_pTypeReport)
            {
                case "0":
                    #region CODE0
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-15), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("DATEIN", "วันที่", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CLICK", "บันทึก", 80)));

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "STA <> 'OK' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_ShowHD.Columns["STA"].ConditionalFormattingObjectList.Add(obj2);

                    radTextBox_CarBig.Visible = true; radTextBox_CarSmall.Visible = true;
                    radLabel_10.Visible = true; radLabel_6.Visible = true;

                    radLabel_Detail.Text = "สีแดง > ยังไม่ได้ประมวลผล";
                    ClearTxt();
                    #endregion
                    break;
                case "1":
                    #region CODE1
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("DATEIN", "วันที่", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BOXALL", "จำนวนลังรวม", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CLICK", "ลบข้อมูล", 80)));

                    ClearTxt();
                    radLabel_Detail.Text = "กดที่ CLICK เพื่อลบข้อมูลตามวันที่ต้องการ | เมื่อลบเสร็จโปรแรกมจพ Refresh ข้อมูลให้ Auto เหลือเฉพาะวันที่มีข้อมูลเท่านั้น";
                    SetDGV_HD();
                    #endregion
                    break;
                case "2":
                    #region CODE2
                    radLabel_10.Visible = true; radLabel_10.Text = "รายได้ต่อชั่วโมง"; radTextBox_CarBig.Visible = true;
                    radLabel_6.Visible = true; radLabel_6.Text = "%ค่าคอมจากยอดขาย"; radTextBox_CarSmall.Visible = true;
                    radLabel_Date.Visible = true; radLabel_Date.Text = "ระบุวันที่ทำงาน";
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;

                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("DATEIN", "วันที่", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CLICK", "บันทึก", 80)));

                    ExpressionFormattingObject obj20 = new ExpressionFormattingObject("MyCondition2", "STA <> 'OK' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj20);
                    this.RadGridView_ShowHD.Columns["STA"].ConditionalFormattingObjectList.Add(obj20);

                    ClearTxt();
                    radLabel_Detail.Text = "สีแดง > ยังไม่ได้ประมวลผล";
                    #endregion
                    break;
                default:
                    break;
            }


        }
        //
        int CheckSave(string pDate, string pTypeReport)
        {
            string sql = $@"
                SELECT  * 
                FROM    Shop_EmpDownLO WITH (NOLOCK) 
                WHERE   RoundDate = '{pDate}' AND ISNULL(TypeReport,'0') = '{pTypeReport}' ";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }
        //processs
        void CheckBeforeProcess(string pTypeReport)
        {
            if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();

            this.Cursor = Cursors.WaitCursor;
            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0) intDay = 1; else intDay++;

            for (int i = 0; i < intDay; i++)
            {
                var pDate = radDateTimePicker_D1.Value.AddDays(i).ToString("yyyy-MM-dd");
                string pClick = "CLICK", sta = "NOT PROCESS";
                if (CheckSave(pDate, pTypeReport) > 0)
                {
                    pClick = ""; sta = "OK";
                }
                RadGridView_ShowHD.Rows.Add(pDate, sta, pClick);
            }
            this.Cursor = Cursors.Default;
        }
        //Set HD
        void SetDGV_HD()
        {
            switch (_pTypeReport)
            {
                case "0":
                    CheckBeforeProcess("0");
                    break;
                case "1":
                    this.Cursor = Cursors.WaitCursor;
                    string sql = $@" 
                        SELECT	CONVERT(VARCHAR,DATEIN,23) AS DATEIN,COUNT(*) AS BOXALL,'Click' AS CLICK
                        FROM	SHOP_RECIVEBOX WITH (NOLOCK)
                        GROUP BY CONVERT(VARCHAR,DATEIN,23) 
                        ORDER BY DATEIN
                    ";
                    RadGridView_ShowHD.DataSource = ConnectionClass.SelectSQL_Main(sql);
                    this.Cursor = Cursors.Default;
                    break;
                case "2":
                    CheckBeforeProcess("1");
                    break;
                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion


        //Clear
        void ClearTxt()
        {
            if (RadGridView_ShowHD.Rows.Count > 0) { RadGridView_ShowHD.Rows.Clear(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-15); radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
            if (_pTypeReport == "2")
            {
                radTextBox_CarBig.Text = "40";
                radTextBox_CarSmall.Text = "0.275";
            }
            else
            {
                radTextBox_CarBig.Text = "200";
                radTextBox_CarSmall.Text = "100";
            }

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //cellClick
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (e.Column.Name != "CLICK") return;



            switch (_pTypeReport)
            {
                case "0":
                    if (RadGridView_ShowHD.CurrentRow.Cells["STA"].Value.ToString() == "OK") return;

                    if (RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString() == "") return;

                    if (radTextBox_CarSmall.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลจำนวนค่าคอมของรถ 6 ล้อให้เรียบร้อย ก่อนกดประมวลผล");
                        radTextBox_CarSmall.Text = "100";
                        radTextBox_CarSmall.Focus();
                        return;
                    }

                    if (radTextBox_CarBig.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลจำนวนค่าคอมของรถ 10 ล้อให้เรียบร้อย ก่อนกดประมวลผล");
                        radTextBox_CarBig.Text = "200";
                        radTextBox_CarBig.Focus();
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ค่าคอมรถ 10 ล้อ" + radTextBox_CarBig.Text + " /คัน" + Environment.NewLine +
                        "ค่าคอมรถ  6 ล้อ" + radTextBox_CarSmall.Text + " /คัน") == DialogResult.No) return;

                    ProcessInsertDataType0(RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString(), radTextBox_CarBig.Text, radTextBox_CarSmall.Text);
                    break;
                case "1":
                    if (RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString() == "") return;
                    string date = RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString();
                    string boxall = RadGridView_ShowHD.CurrentRow.Cells["BOXALL"].Value.ToString();

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบข้อมูลการรับลังสินค้า{Environment.NewLine}จำนวน {boxall} ลัง ?") == DialogResult.No) return;
                    this.Cursor = Cursors.WaitCursor;
                    ArrayList sqlBackUp = new ArrayList() {
                     $@"
                        INSERT INTO [dbo].[SHOP_RECIVEBOX_BACKUP]
		                        ([ID_Auto],[BOXBRANCH],[BOXNUMBER],[DOCUMENTNUM],[SHIPMENTID],[MACHINENAME],[Stadoc_Recive],
		                        [Stadoc_Process],[BOXTYPE],[BOXBARCODE],[BOXSPCITEMNAME],[BOXQTY],[BOXUITID],[REMARK],
		                        [DATEIN],[TIMEIN],[WHOIN],[ProcessDATEIN],[ProcessTIMEIN],[BOXBRANCHBILL],[Stadoc_AX],
		                        [BOXSCN],[BOXpalette],[MNPRWhoEmpDown],[SHIPID_Recive],[LO_Recive],[TagNumber],[STA_BillEdit],[BOXQTY_Send])

                        SELECT	[ID_Auto],[BOXBRANCH],[BOXNUMBER],[DOCUMENTNUM],[SHIPMENTID],[MACHINENAME],[Stadoc_Recive],
		                        [Stadoc_Process],[BOXTYPE],[BOXBARCODE],[BOXSPCITEMNAME],[BOXQTY],[BOXUITID],[REMARK],
		                        [DATEIN],[TIMEIN],[WHOIN],[ProcessDATEIN],[ProcessTIMEIN],[BOXBRANCHBILL],[Stadoc_AX],
		                        [BOXSCN],[BOXpalette],[MNPRWhoEmpDown],[SHIPID_Recive],[LO_Recive],[TagNumber],[STA_BillEdit],[BOXQTY_Send]
                        FROM	SHOP_RECIVEBOX WITH (NOLOCK)
                        WHERE	CONVERT(VARCHAR,DATEIN,23)  = '{date}'
                    " };
                    sqlBackUp.Add($@"
                        DELETE	FROM	SHOP_RECIVEBOX 
                        WHERE	CONVERT(VARCHAR,DATEIN,23)  = '{date}'
                    ");
                    string result = ConnectionClass.ExecuteSQL_ArrayMain(sqlBackUp);
                    MsgBoxClass.MsgBoxShow_SaveStatus(result);
                    if (result == "") SetDGV_HD();
                    this.Cursor = Cursors.Default;
                    break;
                case "2":
                    #region CODE2
                    if (RadGridView_ShowHD.CurrentRow.Cells["STA"].Value.ToString() == "OK") return;
                    if (RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString() == "") return;

                    if (radTextBox_CarSmall.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลจำนวนค่าคอมของรายได้ต่อชั่วโมง ก่อนกดประมวลผล");
                        radTextBox_CarSmall.Text = "40";
                        radTextBox_CarSmall.Focus();
                        return;
                    }
                    if (radTextBox_CarBig.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุข้อมูลจำนวนค่าคอมของ %ยอดขาย ให้เรียบร้อย ก่อนกดประมวลผล");
                        radTextBox_CarBig.Text = "0.275";
                        radTextBox_CarBig.Focus();
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยัน ค่าคอม{Environment.NewLine}รายได้ต่อชั่วโมง {radTextBox_CarBig.Text} /ชม{Environment.NewLine}ค่าคอม%ยอดขาย {radTextBox_CarSmall.Text} /ยอดขายรวม {Environment.NewLine}ที่ระบุ ?") == DialogResult.No) return;

                    ProcessInsertDataType1(RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString(), radTextBox_CarBig.Text, radTextBox_CarSmall.Text);
                    #endregion
                    break;
                default:
                    break;
            }


        }
        //CountBox LO
        double CountBox_LO(string pLO)
        {
            //string sql = string.Format(@"
            //    SELECT	LOGISTICID,COUNT(LOGISTICID) AS SUM_BOX 
            //    FROM	SHOP2013TMP.dbo.SPC_SHIPMENTLINE  WITH (NOLOCK)  INNER jOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE  WITH (NOLOCK) ON SPC_SHIPMENTLINE.SHIPMENTID = SPC_SHIPMENTTABLE.SHIPMENTID  
            //          INNER JOIN SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK) ON SPC_SHIPMENTLINE.DOCUMENTNUM = SPC_BOXFACESHEET.DOCUMENTNUM   
            //          INNER JOIN SHOP2013TMP.dbo.SPC_VEHICLETABLE  WITH (NOLOCK) ON SPC_SHIPMENTTABLE.VEHICLEID = SPC_VEHICLETABLE.VEHICLEID   
            //    WHERE	LOGISTICID  = '" + pLO + @"' 
            //    GROUP BY LOGISTICID
            //");
            DataTable dt = LogisticClass.GetBoxSum_ByLO(pLO); //ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0) return 0; else return double.Parse(dt.Rows[0]["SUM_BOX"].ToString());

        }
        //check สาขาต่างจังหวัด
        string CheckBchOutPhuket(string pBch)
        {
            DataTable dt = BranchClass.GetDetailBranchByID(pBch);
            return dt.Rows[0]["BRANCH_OUTPHUKET"].ToString();
        }
        //Process Insert
        void ProcessInsertDataType0(string pDate, string pCarBig, string pCarSmall)
        {
            this.Cursor = Cursors.WaitCursor;
            //string str = string.Format(@"
            //    SELECT	CONVERT(VARCHAR,DATEIN,23) AS DATEIN,BOXBRANCH,SPC_SHIPMENTTABLE.VEHICLEID,SPC_VEHICLETABLE.NAME,
            //      SPC_SHIPMENTTABLE.LOGISTICID,COUNT(SPC_SHIPMENTTABLE.LOGISTICID) AS COUNT_BOX, 
            //      CASE WHEN SPC_VEHICLETABLE.NAME LIKE '%10 ล้อ%' THEN '" + pCarBig + @"' ELSE '" + pCarSmall + @"' END AS RATE, 
            //      MNPRWhoEmpDown,(COUNTBOX) AS COUNTBOX 
            //    FROM	SHOP2013TMP.dbo.SPC_SHIPMENTLINE  WITH (NOLOCK)  
            //       INNER jOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE  WITH (NOLOCK)	ON SPC_SHIPMENTLINE.SHIPMENTID = SPC_SHIPMENTTABLE.SHIPMENTID 
            //       INNER JOIN SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK) ON SPC_SHIPMENTLINE.DOCUMENTNUM = SPC_BOXFACESHEET.DOCUMENTNUM 
            //       INNER JOIN SHOP2013TMP.dbo.SPC_VEHICLETABLE  WITH (NOLOCK) ON SPC_SHIPMENTTABLE.VEHICLEID = SPC_VEHICLETABLE.VEHICLEID	
            //       INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SPC_SHIPMENTLINE.CUSTACCOUNT = SHOP_BRANCH.BRANCH_ID AND BRANCH_OUTPHUKET = '0' 
            //       INNER JOIN ( 
            //       SELECT	DATEIN,BOXBRANCH,SHIPMENTID,MNPRWhoEmpDown,COUNT(COUNTBOX) AS COUNTBOX 
            //       FROM (SELECT	CONVERT(VARCHAR,DATEIN,23) AS DATEIN,BOXBRANCH,BOXNUMBER,SHIPMENTID,MNPRWhoEmpDown,COUNT(MNPRWhoEmpDown) AS COUNTBOX  
            //       FROM	Shop_recivebox WITH (NOLOCK)	
            //       WHERE	MNPRWhoEmpDown IS NOT NULL AND CONVERT(VARCHAR,DATEIN,23) = '" + pDate + @"'  AND MNPRWhoEmpDown !=''
            //       GROUP BY BOXBRANCH,BOXNUMBER,SHIPMENTID,MNPRWhoEmpDown,DATEIN)TT  WHERE	SHIPMENTID != ''  
            //       GROUP BY BOXBRANCH,SHIPMENTID,MNPRWhoEmpDown,CONVERT(VARCHAR,DATEIN,23))TMPA ON SPC_SHIPMENTLINE.SHIPMENTID = TMPA.SHIPMENTID 
            //    GROUP BY BOXBRANCH,SPC_SHIPMENTTABLE.VEHICLEID,SPC_VEHICLETABLE.NAME,SPC_SHIPMENTTABLE.LOGISTICID,MNPRWhoEmpDown ,COUNTBOX,DATEIN 
            //");
            DataTable dt_LO = LogisticClass.GetCommissionForReciveBox(pDate, pCarBig, pCarSmall); //ConnectionClass.SelectSQL_Main(str);
            if (dt_LO.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการลงสินค้าของพนักงานต่างจังหวัด " + pDate);
                this.Cursor = Cursors.Default;
                return;
            }

            ArrayList inStr = new ArrayList();
            double PricePerBox;
            for (int i = 0; i < dt_LO.Rows.Count; i++)
            {
                if (dt_LO.Rows[i]["MNPRWhoEmpDown"].ToString() != "")
                {
                    double BoxAllLO = CountBox_LO(dt_LO.Rows[i]["LOGISTICID"].ToString());
                    PricePerBox = double.Parse(dt_LO.Rows[i]["RATE"].ToString()) / BoxAllLO;
                    string[] CountEmp = dt_LO.Rows[i]["MNPRWhoEmpDown"].ToString().Split('|');
                    double BoxEmp = double.Parse(dt_LO.Rows[i]["COUNTBOX"].ToString()) / CountEmp.Length;
                    double BoxAll = double.Parse(dt_LO.Rows[i]["COUNTBOX"].ToString()) * PricePerBox;
                    double PricePerEmp = BoxAll / CountEmp.Length;
                    DataTable dtBch = BranchClass.GetDetailBranchByID(dt_LO.Rows[i]["BOXBRANCH"].ToString());
                    DataTable dtEmp;
                    for (int ii = 0; ii < CountEmp.Length; ii++)
                    {
                        //dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(CountEmp[ii].Substring(1, 7));
                        dtEmp = Models.EmplClass.GetEmployeeDetail_ByEmplID(ConfigClass.ReplaceMDPForEmlpID(CountEmp[ii]));
                        try
                        {
                            //string MN = dtEmp.Rows[0]["NUM"].ToString().Substring(0, 2);
                            //string MN = dtEmp.Rows[0]["NUM"].ToString().Replace("MN", "").Replace("D", "");
                            if (dtEmp.Rows[0]["NUM"].ToString().Contains("MN") == true)
                            { //" + pConfigDB + @"
                                if (CheckBchOutPhuket(dtEmp.Rows[0]["NUM"].ToString()) == "0")
                                {
                                    inStr.Add(@"
                                    INSERT INTO Shop_EmpDownLO (RoundDate,BranchID,
                                    BranchName,
                                    LO,CarID,CarNet,
                                    EmplID, EmplName, EmplDept, 
                                    BoxNet, BoxSum, EmplSum, STAsendData) VALUES (
                                    '" + dt_LO.Rows[i]["DATEIN"].ToString() + @"','" + dtBch.Rows[0]["BRANCH_ID"].ToString() + @"',
                                    '" + dtBch.Rows[0]["BRANCH_ID"].ToString() + @"-" + dtBch.Rows[0]["BRANCH_NAME"].ToString() + @"',
                                    '" + dt_LO.Rows[i]["LOGISTICID"].ToString() + @"','" + dt_LO.Rows[i]["VEHICLEID"].ToString() + "-" + dt_LO.Rows[i]["NAME"].ToString() + @"',
                                    '" + dt_LO.Rows[i]["RATE"].ToString() + @"',
                                    '" + dtEmp.Rows[0]["EMPLID"].ToString() + @"','" + dtEmp.Rows[0]["SPC_NAME"].ToString() + @"',
                                    '" + dtEmp.Rows[0]["NUM"].ToString() + "-" + dtEmp.Rows[0]["DESCRIPTION"].ToString() + @"',
                                    '" + PricePerBox + @"','" + BoxEmp + @"','" + PricePerEmp + @"','0')
                                    ");
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(inStr);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                SetDGV_HD();
            }
            this.Cursor = Cursors.Default;
        }
        //Process Insert
        void ProcessInsertDataType1(string pDate, string hr, string percen)
        {
            this.Cursor = Cursors.WaitCursor;
            //string sqlParttimeDetail = $@"
            //            SELECT	PW_BRANCH_IN AS BRANCH_ID,SHOP_BRANCH.BRANCH_NAME,PW_IDCARD,PW_PWCARD,PW_NAME,PW_DATETIME_IN,PW_DATETIME_OUT,CONVERT(VARCHAR,PW_DATE_IN,23) AS PW_DATE_IN,
            //              SUM(DATEDIFF(hh,POSLOGINTRANS.SIGNINDATETIME,POSLOGINTRANS.SIGNOUTDATETIME)) AS Hour_LoginSale,
            //              DATEDIFF(hh,MIN(POSLOGINTRANS.SIGNINDATETIME),MAX(POSLOGINTRANS.SIGNOUTDATETIME)) AS HourMinMax_LoginSale,
            //                    DATEDIFF(hh,MIN(POSLOGINTRANS.SIGNINDATETIME),MAX(POSLOGINTRANS.SIGNOUTDATETIME))*{hr} AS SARALY_Hour,
            //              COUNT(XXX_POSTABLE.INVOICEID)  AS COUNT_BILL,SUM(XXX_POSTABLE.INVOICEAMOUNT) AS SUM_AMOUNT,
            //              FLOOR((SUM(XXX_POSTABLE.INVOICEAMOUNT)*{percen})/100)  AS SARALY_AMOUNT,
            //              (DATEDIFF(hh,MIN(POSLOGINTRANS.SIGNINDATETIME),MAX(POSLOGINTRANS.SIGNOUTDATETIME))*{hr})+(FLOOR((SUM(XXX_POSTABLE.INVOICEAMOUNT)*{percen})/100)) AS SARALY 

            //            FROM	SHOP_TIMEKEEPER WITH (NOLOCK)
            //              INNER JOIN SHOP2013TMP.dbo.POSLOGINTRANS WITH (NOLOCK) 
            //                ON SHOP_TIMEKEEPER.PW_IDCARD = POSLOGINTRANS.EMPLID AND SHOP_TIMEKEEPER.PW_BRANCH_IN = POSLOGINTRANS.POSGROUP 
            //              INNER JOIN SHOP_BRANCH WITH (NOLOCK) 
            //                ON SHOP_TIMEKEEPER.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID
            //                    LEFT OUTER JOIN SHOP2013TMP.dbo.XXX_POSTABLE WITH (NOLOCK) 
            //                        ON SHOP_TIMEKEEPER.PW_IDCARD = XXX_POSTABLE.CASHIERID 

            //            WHERE	PW_DATE_IN = '{pDate}'  
            //              AND PW_TYPE = '2' 
            //              AND POSLOGINTRANS.SIGNINDATETIME BETWEEN PW_DATETIME_IN AND PW_DATETIME_OUT 
            //                    AND XXX_POSTABLE.CREATEDDATETIME BETWEEN PW_DATETIME_IN AND PW_DATETIME_OUT 

            //            GROUP BY PW_BRANCH_IN,SHOP_BRANCH.BRANCH_NAME,PW_IDCARD,PW_PWCARD,PW_NAME,PW_DATETIME_IN,PW_DATETIME_OUT,CONVERT(VARCHAR,PW_DATE_IN,23)
            //            ORDER BY PW_BRANCH_IN , PW_IDCARD
            //        ";

            DataTable dtDetail = PosSaleClass.GetCommissionCashPartTime(pDate, percen, hr); //ConnectionClass.SelectSQL_Main(sqlParttimeDetail);

            ArrayList inStr = new ArrayList();
            if (dtDetail.Rows.Count == 0)
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmInsert($@"ไม่พบข้อมูลการทำงานของแคชเชียร์ PartTime {pDate}") == DialogResult.No)
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
                inStr.Add($@"
                        INSERT INTO Shop_EmpDownLO (RoundDate,BranchID,
                                    BranchName,
                                    LO,CarID,CarNet,
                                    EmplID, EmplName, EmplDept, 
                                    BoxNet, BoxSum, EmplSum, STAsendData,TypeReport,COUNT_BILL,SUM_AMOUNT) 
                        VALUES (    '{pDate}','','','','','','','','','','','','0','1','0','0')");
            }
            else
            {
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    inStr.Add($@"
                        INSERT INTO Shop_EmpDownLO (RoundDate,BranchID,
                                    BranchName,
                                    LO,CarID,CarNet,
                                    EmplID, EmplName, EmplDept, 
                                    BoxNet, BoxSum, EmplSum, STAsendData,TypeReport,COUNT_BILL,SUM_AMOUNT) 
                        VALUES (
                                    '{dtDetail.Rows[i]["PW_DATE_IN"]}','{dtDetail.Rows[i]["BRANCH_ID"]}',
                                    '{dtDetail.Rows[i]["BRANCH_ID"]}-{dtDetail.Rows[i]["BRANCH_NAME"]}',
                                    '{dtDetail.Rows[i]["PW_DATETIME_IN"]}','{dtDetail.Rows[i]["PW_DATETIME_OUT"]}','{dtDetail.Rows[i]["HourMinMax_LoginSale"]}',
                                    '{dtDetail.Rows[i]["PW_IDCARD"]}','{dtDetail.Rows[i]["PW_NAME"]}',
                                    '{dtDetail.Rows[i]["BRANCH_ID"]}-{dtDetail.Rows[i]["BRANCH_NAME"]}',
                                    '{dtDetail.Rows[i]["SARALY_Hour"]}','{dtDetail.Rows[i]["SARALY_AMOUNT"]}',
                                    '{dtDetail.Rows[i]["SARALY"]}','0','1',
                                    '{dtDetail.Rows[i]["COUNT_BILL"]}','{dtDetail.Rows[i]["SUM_AMOUNT"]}'
                                    )");
                }
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(inStr);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "") SetDGV_HD();
            this.Cursor = Cursors.Default;
        }
        private void RadTextBox_CarBig_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadTextBox_CarSmall_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && !(e.KeyChar != '.');
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = "ไม่อยู่ในเงื่อนไขที่ต้อง Export Excel";
            switch (_pTypeReport)
            {
                case "0":
                    T = DatagridClass.ExportExcelGridView("รายการประมวลผล การลงสินค้าต่างจังหวัด ", RadGridView_ShowHD, "1");
                    break;
                case "1":
                    T = DatagridClass.ExportExcelGridView("รายการลบข้อมูลการรับลังสินค้า ", RadGridView_ShowHD, "1");
                    break;
                case "2":
                    T = DatagridClass.ExportExcelGridView("รายการประมวลผล แคชเชียร์ PartTime ", RadGridView_ShowHD, "1");
                    break;
                default:
                    break;
            }
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }


    }
}

