﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Collections;
using System.Data.SqlClient;

namespace PC_Shop24Hrs.GeneralForm.Commission
{

    public partial class Commission_EmpDownReport : Telerik.WinControls.UI.RadForm
    {
        string TermAX;
        int TermNumberPrint;

        //0 ค่าคอมของพนักงานลงสินค้าต่างจังหวัด
        //1 ค่าคอมของพนักงาน PartTime
        //2 ค่าคอมของพนักงาน OT วันหยุด
        //3 ค่าเบี้ยเลี้ยง+ค่าที่พัก
        //4 ค่าคอมเพิ่มของตำแหน่งแคชเชียร์

        readonly string _pTypeOpen, _pTypeReport;

        DataTable dtDetail = new DataTable();
        DataTable dtSum = new DataTable();
        //Load
        public Commission_EmpDownReport(string pTypeOpen, string pReport)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
            _pTypeReport = pReport;
        }
        //Load
        private void Commission_EmpDownReport_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์เอกสาร";
            radButtonElement_UpAX.ShowBorder = true; radButtonElement_UpAX.ToolTipText = "ส่งเข้างวดเงินเดือน"; radButtonElement_UpAX.Enabled = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Search.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROWSNUMBER", "ลำดับ", 60)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 80)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ชื่อพนักงาน", 160)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplDept", "แผนก", 350)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("EmplSum", "ยอดทั้งหมด", 120)));

            GridViewSummaryItem summaryItemDT = new GridViewSummaryItem("EmplSum", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
            GridViewSummaryRowItem summaryRowItemDT = new GridViewSummaryRowItem { summaryItemDT };
            this.RadGridView_ShowDT.SummaryRowsTop.Add(summaryRowItemDT);
            this.RadGridView_ShowDT.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            radLabel_Detail.Text = "กดปุ่ม / เพื่อ Up งวดเงินเดือนก่อนพิมพ์ | กดพิมพ์ทุกครั้งจะบันทึกจำนวนครั้งที่พิมพ์เสมอ [ถ้า Up งวดเงินเดือนแล้ว]";
            switch (_pTypeReport)
            {
                case "0":

                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RoundDate", "วันที่", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LO", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CariD", "ทะเบียนรถ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CarNet", "ราคา/คัน", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ชื่อพนักงาน", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplDept", "แผนก", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BoxNet", "ราคา/ลัง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BoxSum", "จำนวนลัง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("EmplSum", "ยอดทั้งหมด", 120)));
                    GridViewSummaryItem summaryItemHD = new GridViewSummaryItem("EmplSum", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemHD = new GridViewSummaryRowItem { summaryItemHD };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemHD);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    break;

                case "1":

                    radLabel_Date.Visible = true; radLabel_Date.Text = "ระบุวันที่ทำงาน";
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radButtonElement_UpAX.Enabled = true;

                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RoundDate", "วันที่เข้างาน", 100)));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ชื่อ", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LO", "ลงเวลาเข้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CariD", "ลงเวลาออก", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_BILL", "จำนวน บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CarNet", "จำนวน ชม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BoxNet", "ค่าคอม ชม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM_AMOUNT", "ยอดขายรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BoxSum", "ค่าคอมยอดขาย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("EmplSum", "ยอดรวม", 100)));
                    GridViewSummaryItem summaryItemHD2 = new GridViewSummaryItem("EmplSum", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemHD2 = new GridViewSummaryRowItem { summaryItemHD2 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemHD2);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROWSNUMBER", "ลำดับ", 60)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 100)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ชื่อ", 200)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplDept", "แผนก", 350)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("EmplSum", "ยอดค่าคอม", 160)));
                    //GridViewSummaryItem summaryItemDT1 = new GridViewSummaryItem("EmplSum", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    //GridViewSummaryRowItem summaryRowItemDT1 = new GridViewSummaryRowItem { summaryItemDT1 };
                    //this.RadGridView_ShowDT.SummaryRowsTop.Add(summaryRowItemDT1);
                    //this.RadGridView_ShowDT.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
                    ClearTxt();

                    break;
                case "2"://OT
                    radLabel_Date.Visible = true; radLabel_Date.Text = "ระบุวันที่ทำงาน";
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radButtonElement_UpAX.Enabled = true;

                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-11), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_DATE_IN", "วันที่เข้างาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_BRANCH_IN", "รหัส", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อ", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "ลงเวลาเข้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "ลงเวลาออก", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_SUMTIME", "ลงเวลาออก", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BRANCH_CONFIGCOMMISSION_OT", "OT/วัน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COMMISSIONDAY", "ยอดรวม/วัน", 100)));

                    GridViewSummaryItem summaryItemHD3 = new GridViewSummaryItem("COMMISSIONDAY", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemHD3 = new GridViewSummaryRowItem { summaryItemHD3 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemHD3);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROWSNUMBER", "ลำดับ", 60)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 100)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ชื่อ", 200)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplDept", "แผนก", 350)));
                    //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("EmplSum", "ยอดค่าคอม", 160)));

                    //GridViewSummaryItem summaryItemDT2 = new GridViewSummaryItem("EmplSum", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    //GridViewSummaryRowItem summaryRowItemDT2 = new GridViewSummaryRowItem { summaryItemDT2 };
                    //this.RadGridView_ShowDT.SummaryRowsTop.Add(summaryRowItemDT2);

                    //this.RadGridView_ShowDT.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    radLabel_Detail.Text = "[ข้อมูลแสดงเฉพาะผู้ที่บันทึกตรวจตัวเท่านั้น] กดปุ่ม / เพื่อ Up งวดเงินเดือนก่อนพิมพ์ | กดพิมพ์ทุกครั้งจะบันทึกจำนวนครั้งที่พิมพ์เสมอ[ถ้า Up งวดเงินเดือนแล้ว]";
                    ClearTxt();

                    break;
                case "3":   //3 ค่าเบี้ยเลี้ยง+ค่าที่พัก
                    radLabel_Date.Visible = true; radLabel_Date.Text = "ระบุวันที่ทำงาน";
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radButtonElement_UpAX.Enabled = true;


                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-31), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_DATE_IN", "วันที่เข้างาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_BRANCH_IN", "รหัส", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อ", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "ลงเวลาเข้า", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "ลงเวลาออก", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PW_SUMTIME", "ลงเวลาออก", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 100)));

                    DatagridClass.SetCellBackClolorByExpression("STA", "STA = 'ไม่จ่าย' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    break;
                case "4"://4 ค่าคอมเพิ่มของตำแหน่งแคชเชียร์

                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-70), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-40), DateTime.Now.AddDays(-1));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "รหัส", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัส", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ชื่อพนักงาน", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SalesAmount", "ยอดขายรวม", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("EmplSum", "ค่าคอมยอดขาย", 120)));

                    DatagridClass.SetCellBackClolorByExpression("EmplSum", "SalesAmount < 100000 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    GridViewSummaryItem summaryItemHD4 = new GridViewSummaryItem("EmplSum", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemHD4 = new GridViewSummaryRowItem { summaryItemHD4 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemHD4);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    break;
                default:
                    break;
            }

            if (_pTypeOpen != "SUPC")
            {
                radLabel_Detail.Text = "สาขาไม่มีสิทธิ์ในการพิมพ์ข้อมูล";
                radButtonElement_print.Enabled = false;
            }

            ClearTxt();
        }
        //check รอบการประมวลผล
        int CheckSave(string pDate1, string pDate2, string pTypeReport)
        {
            string sql = $@"
                SELECT RoundDate 
                FROM  SHOP_EMPDOWNLO WITH (NOLOCK) 
                WHERE RoundDate BETWEEN '{pDate1}' AND '{pDate2}' 
                      AND ISNULL(TypeReport,'0') = '{pTypeReport}'
                GROUP BY RoundDate  ";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }
        //Set HD
        void SetDGV_HD()
        {
            switch (_pTypeReport)
            {
                case "0"://ลงสินค้าต่างจังหวัด
                    ProcessSaraly("0");
                    break;
                case "1":// part time 
                    ProcessSaraly("1");
                    break;
                case "2"://OT
                    ProcessSaralyOT();
                    break;
                case "3"://ค่าเบี้ยเลี้ยง+ค่าที่พัก
                    ProcessSaralyLocation();
                    break;
                case "4": //4 ค่าคอมเพิ่มของตำแหน่งแคชเชียร์
                    ProcessSaralyComCashierAddOn();
                    break;
                default:
                    break;
            }

        }
        //4 ค่าคอมเพิ่มของตำแหน่งแคชเชียร์
        void ProcessSaralyComCashierAddOn()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dtSum.Rows.Count > 0) { dtSum.Rows.Clear(); RadGridView_ShowDT.DataSource = dtSum; dtSum.AcceptChanges(); }
            if (dtDetail.Rows.Count > 0) { dtDetail.Rows.Clear(); RadGridView_ShowDT.DataSource = dtDetail; dtDetail.AcceptChanges(); }

            string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            dtDetail = EmployeeClass.CommissionCashierAddOn("0", pDate1, pDate2);
            RadGridView_ShowHD.DataSource = dtDetail;
            dtDetail.AcceptChanges();

            dtSum = EmployeeClass.CommissionCashierAddOn("1", pDate1, pDate2);// EmployeeClass.CommissionOT(pDate1, pDate2);
            RadGridView_ShowDT.DataSource = dtSum;
            dtSum.AcceptChanges();

            if (Check701(pDate1, pDate2, "2") > 0) radButtonElement_UpAX.Enabled = false;
            else radButtonElement_UpAX.Enabled = true;

            radDateTimePicker_D1.Enabled = false;
            radDateTimePicker_D2.Enabled = false;
            RadButton_Search.Enabled = false;

            if (dtSum.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลที่ค้นหา ลองใหม่อีกครั้ง.");

            this.Cursor = Cursors.Default;
        }
        //สำหรับการดึงค่าเบี้ยเลี้ยง+ค่าที่พัก
        void ProcessSaralyLocation()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dtSum.Rows.Count > 0) { dtSum.Rows.Clear(); RadGridView_ShowDT.DataSource = dtSum; dtSum.AcceptChanges(); }
            if (dtDetail.Rows.Count > 0) { dtDetail.Rows.Clear(); RadGridView_ShowDT.DataSource = dtDetail; dtDetail.AcceptChanges(); }

            string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            dtDetail = EmployeeClass.CommissionLocation("0", pDate1, pDate2);
            RadGridView_ShowHD.DataSource = dtDetail;
            dtDetail.AcceptChanges();

            dtSum = EmployeeClass.CommissionLocation("1", pDate1, pDate2);// EmployeeClass.CommissionOT(pDate1, pDate2);
            RadGridView_ShowDT.DataSource = dtSum;
            dtSum.AcceptChanges();

            if (Check701(pDate1, pDate2, "2") > 0) radButtonElement_UpAX.Enabled = false;
            else radButtonElement_UpAX.Enabled = true;

            radDateTimePicker_D1.Enabled = false;
            radDateTimePicker_D2.Enabled = false;
            RadButton_Search.Enabled = false;
            if (dtSum.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลที่ค้นหา ลองใหม่อีกครั้ง.");
            this.Cursor = Cursors.Default;
        }
        //สำหรับการดึงยอดขายคอมของ พนง OT
        void ProcessSaralyOT()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dtSum.Rows.Count > 0) { dtSum.Rows.Clear(); RadGridView_ShowDT.DataSource = dtSum; dtSum.AcceptChanges(); }
            if (dtDetail.Rows.Count > 0) { dtDetail.Rows.Clear(); RadGridView_ShowDT.DataSource = dtDetail; dtDetail.AcceptChanges(); }

            string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            string bch = "";
            if (SystemClass.SystemBranchID != "MN000") bch = $@" AND SHOP_TIMEKEEPER.PW_BRANCH_IN = '{SystemClass.SystemBranchID}' ";

            string sqlHD = $@"
                SELECT	PW_BRANCH_IN,BRANCH_NAME,CONVERT(VARCHAR,PW_DATE_IN,23) AS PW_DATE_IN,PW_IDCARD,PW_NAME,
		                PW_SUMDATE,PW_SUMDATE + ':' + PW_SUMTIME AS PW_SUMTIME,PW_STATUS,PW_MNEP_Check,BRANCH_CONFIGCOMMISSION_OT,
		                PW_DATETIME_IN,PW_DATETIME_OUT,
		                IIF(PW_SUMDATE <> 0,CAST(BRANCH_CONFIGCOMMISSION_OT AS DECIMAL(9,2)),
			                CASE	
					                WHEN CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2)) BETWEEN '6' AND '11' THEN BRANCH_CONFIGCOMMISSION_OT/2
					                WHEN CAST(SUBSTRING(PW_SUMTIME,1,2) AS DECIMAL(9,2)) >= 12 THEN BRANCH_CONFIGCOMMISSION_OT
					                ELSE '0'
			                END)
		                AS COMMISSIONDAY

                FROM	SHOP_TIMEKEEPER WITH (NOLOCK)
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_TIMEKEEPER.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID
                WHERE	PW_DATE_IN BETWEEN '{pDate1}' AND '{pDate2}'
		                AND PW_STATUS = '0'
		                AND PW_TYPE = '3'
                        AND PW_MNEP_Check = '1'
                        {bch}
                ORDER BY PW_BRANCH_IN,PW_IDCARD
            ";
            dtDetail = ConnectionClass.SelectSQL_Main(sqlHD);
            RadGridView_ShowHD.DataSource = dtDetail;
            dtDetail.AcceptChanges();

            dtSum = EmployeeClass.CommissionOT(pDate1, pDate2);
            RadGridView_ShowDT.DataSource = dtSum;
            dtSum.AcceptChanges();

            if (Check701(pDate1, pDate2, "2") > 0) radButtonElement_UpAX.Enabled = false;
            else radButtonElement_UpAX.Enabled = true;

            radDateTimePicker_D1.Enabled = false;
            radDateTimePicker_D2.Enabled = false;
            RadButton_Search.Enabled = false;
            if (dtSum.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลที่ค้นหา ลองใหม่อีกครั้ง.");
            this.Cursor = Cursors.Default;
        }
        void ProcessSaraly(string typeReport)
        {
            this.Cursor = Cursors.WaitCursor;
            if (dtSum.Rows.Count > 0) { dtSum.Rows.Clear(); RadGridView_ShowDT.DataSource = dtSum; dtSum.AcceptChanges(); }
            if (dtDetail.Rows.Count > 0) { dtDetail.Rows.Clear(); RadGridView_ShowDT.DataSource = dtDetail; dtDetail.AcceptChanges(); }

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0) intDay = 1; else intDay++;

            string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            if (CheckSave(pDate1, pDate2, typeReport) != intDay)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"วันที่ที่ระบุยังประมวลผลไม่ครบ ไม่สามารถออกรายงานได้{Environment.NewLine}ให้กลับไปหน้าจอประมวลผล แล้วดำเนินการให้เรียบร้อย");
                this.Cursor = Cursors.Default;
                return;
            }

            string pConBch = "";
            if (_pTypeOpen == "SHOP") pConBch = $@" AND BranchID = '{ SystemClass.SystemBranchID}' ";

            string sqlDetail = $@"
                        SELECT  CONVERT(VARCHAR, RoundDate, 23) AS RoundDate, BranchName, LO, CariD, CarNet, EmplID, EmplName, EmplDept, BoxNet, BoxSum, EmplSum ,COUNT_BILL,SUM_AMOUNT
                        FROM    SHOP_EMPDOWNLO WITH (NOLOCK) 
                        WHERE	RoundDate BETWEEN '{pDate1}' AND '{pDate2}' 
                                AND ISNULL(TypeReport,'0') = '{typeReport}' AND EmplSum > 0 {pConBch}
                        ORDER BY RoundDate, EmplDept, EmplID ";
            dtDetail = ConnectionClass.SelectSQL_Main(sqlDetail);
            RadGridView_ShowHD.DataSource = dtDetail;
            dtDetail.AcceptChanges();

            dtSum = EmployeeClass.CommissionReciveBox(pDate1, pDate2, pConBch, typeReport);
            RadGridView_ShowDT.DataSource = dtSum;
            dtSum.AcceptChanges();

            if (Check701(pDate1, pDate2, typeReport) > 0) radButtonElement_UpAX.Enabled = false;
            else radButtonElement_UpAX.Enabled = true;

            radDateTimePicker_D1.Enabled = false;
            radDateTimePicker_D2.Enabled = false;
            RadButton_Search.Enabled = false;
            if (dtSum.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลที่ค้นหา ลองใหม่อีกครั้ง.");
            this.Cursor = Cursors.Default;
        }
        //Clear
        void ClearTxt()
        {
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1); radDateTimePicker_D1.Enabled = true;
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1); radDateTimePicker_D2.Enabled = true;

            if (dtSum.Rows.Count > 0) { dtSum.Rows.Clear(); RadGridView_ShowDT.DataSource = dtSum; dtSum.AcceptChanges(); }
            if (dtDetail.Rows.Count > 0) { dtDetail.Rows.Clear(); RadGridView_ShowDT.DataSource = dtDetail; dtDetail.AcceptChanges(); }
            TermAX = ""; TermNumberPrint = 0;
            RadButton_Search.Enabled = true;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Find Data รอบเงินเดือน
        int Check701(string pDate1, string pDate2, string pTypeReport)
        {
            string sql = $@"
                SELECT	ISNULL(tTermCode,'') AS tTermCode ,ISNULL(tNumberprint,0) AS tNumberprint 
                FROM	SHOP_EMPDOWNLO WITH (NOLOCK)	
                WHERE	RoundDate BETWEEN '{pDate1}' AND '{pDate2}' 
                        AND ISNULL(TypeReport,'0') = '{pTypeReport}' 
		                AND tTermCode != ''
                GROUP BY tTermCode,tNumberprint ";
            DataTable dtTerm = ConnectionClass.SelectSQL_Main(sql);
            switch (dtTerm.Rows.Count)
            {
                case 0:
                    TermAX = ""; TermNumberPrint = 0; break;
                case 1:
                    TermAX = dtTerm.Rows[0]["tTermCode"].ToString(); TermNumberPrint = int.Parse(dtTerm.Rows[0]["tNumberprint"].ToString()); break;
                default:
                    break;
            }
            return dtTerm.Rows.Count;
        }

        #region "CellFormat"
        private void RadGridView_ShowHD_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (sender is null) throw new ArgumentNullException(nameof(sender));
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
        }
        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_ShowHD_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_ShowHD_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        private void RadButtonElement_print_Click(object sender, EventArgs e)
        {
            if (_pTypeOpen == "SHOP") return;
            RadGridView_ShowDT.FilterDescriptors.Clear();

            switch (_pTypeReport)
            {
                case "0":
                    PrintReport("0", $@"รายงานพนักงานลงสินค้าต่างจังหวัด");
                    break;
                case "1":
                    PrintReport("1", $@"รายงานแคชเชียร์ PartTime");
                    break;
                case "2":
                    PrintReport("2", $@"รายงาน OT วันหยุด");
                    break;
                case "3":
                    PrintReport("3", $@"รายงาน ค่าเบี้ยเลี้ยง+ค่าที่พัก");
                    break;
                case "4":
                    PrintReport("4", $@"รายงาน คอมแคชเชียร์รวม");
                    break;
                default:
                    break;
            }

        }

        void PrintReport(string typeReport, string desc)
        {
            this.RadGridView_ShowDT.SortDescriptors.Expression = "ROWSNUMBER ASC";
            GridPrintStyle style = new GridPrintStyle
            {
                FitWidthMode = PrintFitWidthMode.FitPageWidth,
                PrintAllPages = true,
                PrintGrouping = true,
                PrintHeaderOnEachPage = true,
                PrintSummaries = true,

                CellFont = new Font("Tahoma", 10, FontStyle.Regular),


                HeaderCellBackColor = Color.Transparent,
                HeaderCellFont = new Font("Tahoma", 12, FontStyle.Bold),

                SummaryCellBackColor = ConfigClass.SetColor_PurplePastel(),
                SummaryCellFont = new Font("Tahoma", 12, FontStyle.Bold)
            };

            this.RadGridView_ShowDT.PrintStyle = style;

            string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");

            RadPrintDocument document = new RadPrintDocument();
            //DatagridClass.SetPrintDocument(document, "งวด " + TermAX + " รายงานพนักงานลงสินค้าต่างจังหวัด [" + pDate1 + @" ถึง " + pDate2 + @"]", TermNumberPrint + 1);
            DatagridClass.SetPrintDocument(document, $@"งวด {TermAX} {desc} [{pDate1} ถึง {pDate2}]", TermNumberPrint + 1);

            document.AssociatedObject = this.RadGridView_ShowDT;
            RadPrintPreviewDialog dialog = new RadPrintPreviewDialog(document);
            if (TermAX == "")
            {
                dialog.ToolMenu.Visible = false;
                dialog.ToolCommandBar.Visible = false;
                dialog.ShowDialog();
                return;
            }
            else
            {
                dialog.ToolMenu.Visible = true;
                dialog.ToolCommandBar.Visible = true;

                TermNumberPrint += 1;
                string sqlUp = $@"
                            Update  SHOP_EMPDOWNLO 
                            SET     tNumberprint = '{TermNumberPrint}'
                            WHERE	RoundDate BETWEEN '{pDate1}' AND '{pDate2}' AND ISNULL(TypeReport,'0') = '{typeReport}'  ";

                string Tup = ConnectionClass.ExecuteSQL_Main(sqlUp);
                if (Tup == "") dialog.ShowDialog();
                else MsgBoxClass.MsgBoxShow_SaveStatus(Tup);
            }
        }

        void InsertAX(string typeReport, string desc)
        {
            //กรณี type 2/3 จะต้อง Insert Dataก่อน
            if ((typeReport == "3") || (typeReport == "2") || (typeReport == "4"))
            {
                ArrayList sql24 = new ArrayList();
                double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;
                if (intDay == 0) intDay = 1;
                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                    sql24.Add($@"INSERT INTO Shop_EmpDownLO (RoundDate,BranchID,
                                    BranchName,
                                    LO,CarID,CarNet,
                                    EmplID, EmplName, EmplDept, 
                                    BoxNet, BoxSum, EmplSum, STAsendData,TypeReport,COUNT_BILL,SUM_AMOUNT) 
                        VALUES (    '{pDate}','','','','','','','','','','','','0','{typeReport}','0','0')");
                }

                string T24 = ConnectionClass.ExecuteSQL_ArrayMain(sql24);
                if (T24 != "")
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(T24);
                    return;
                }
            }

            ArrayList InStr = new ArrayList();
            for (int i = 0; i < RadGridView_ShowDT.Rows.Count; i++)
            {
                InStr.Add($@"
                            INSERT INTO tblTempInsertDt (tEmpCode,tEmpAmount ) VALUES (
                            '{RadGridView_ShowDT.Rows[i].Cells["EmplID"].Value}','{RadGridView_ShowDT.Rows[i].Cells["EmplSum"].Value}') ");
            }
            string T = SendAX(InStr, desc, typeReport);
            if (T == "") radButtonElement_UpAX.Enabled = false;

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Send AX
        private void RadButtonElement_UpAX_Click(object sender, EventArgs e)
        {
            if (_pTypeOpen == "SHOP") return;
            RadGridView_ShowDT.FilterDescriptors.Clear();
            if (RadGridView_ShowDT.Rows.Count == 0) return;




            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ส่งข้อมูลเข้างวดเงินเดือน ไม่สามารถแก้ไขได้") == DialogResult.No) return;
            switch (_pTypeReport)
            {
                case "0":
                    InsertAX("0", $@"ค่าคอม พนง.ลงสินค้า MN ตจว.รอบ {radDateTimePicker_D1.Value:yyyy-MM-dd} ถึง {radDateTimePicker_D2.Value:yyyy-MM-dd}");
                    break;
                case "1":
                    InsertAX("1", $@"ค่าคอม แคชเชียร์ PartTime รอบ {radDateTimePicker_D1.Value:yyyy-MM-dd} ถึง {radDateTimePicker_D2.Value:yyyy-MM-dd}");
                    break;
                case "2":
                    InsertAX("2", $@"ค่าคอม แคชเชียร์ OT วันหยุด รอบ {radDateTimePicker_D1.Value:yyyy-MM-dd} ถึง {radDateTimePicker_D2.Value:yyyy-MM-dd}");
                    break;
                case "3":
                    InsertAX("3", $@"ค่าเบี้ยเลี้ยง+ค่าที่พัก รอบ {radDateTimePicker_D1.Value:yyyy-MM-dd} ถึง {radDateTimePicker_D2.Value:yyyy-MM-dd}");
                    break;
                case "4":
                    if ((SystemClass.SystemUserID_M != "0604049"))//(SystemClass.SystemUserID_M != "0104010") || (SystemClass.SystemUserID_M != "0804002") || (SystemClass.SystemUserID_M != "1308066") ||
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สิทธ์ส่งข้อมูล แคชเชียร์รวม OnTop เข้างวดเพื่อจ่ายเงินได้");
                        return;
                    }
                    InsertAX("4", $@"ค่าคอม แคชเชียร์รวม รอบ {radDateTimePicker_D1.Value:yyyy-MM-dd} ถึง {radDateTimePicker_D2.Value:yyyy-MM-dd}");
                    break;
                default:
                    break;
            }
        }

        //SendAX
        string SendAX(ArrayList InStrAX, string termDetail, string typeReport)
        {
            string pDate1 = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            string pDate2 = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");
            //string termDetail = "ค่าคอม พนง.ลงสินค้า MN ตจว.รอบ " + pDate1 + " ถึง " + pDate2;

            //Dim sqlStr As String = ""
            string sqlUPSend = "EXEC SPCN_Salary_InsertFromTemp '" + pDate1 + @"','" + pDate2 + @"','" + termDetail + @"' ";

            //1
            SqlConnection sqlConnection1 = new SqlConnection(IpServerConnectClass.Con701SRV);
            if (sqlConnection1.State == ConnectionState.Closed) sqlConnection1.Open();

            //  SqlCommand sqlCommand24 = new SqlCommand();
            SqlCommand sqlCommand1 = sqlConnection1.CreateCommand();
            SqlTransaction transaction1 = sqlConnection1.BeginTransaction("SampleTransaction");
            sqlCommand1.Connection = sqlConnection1;
            sqlCommand1.Transaction = transaction1;

            //2
            SqlConnection sqlConnection2 = new SqlConnection(IpServerConnectClass.ConSelectMain);
            if (sqlConnection2.State == ConnectionState.Closed) sqlConnection2.Open();

            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommand2 = sqlConnection2.CreateCommand();
            SqlTransaction transaction2 = sqlConnection2.BeginTransaction("SampleTransaction");
            sqlCommand2.Connection = sqlConnection2;
            sqlCommand2.Transaction = transaction2;

            try
            {
                for (int i24 = 0; i24 < InStrAX.Count; i24++)
                {
                    sqlCommand1.CommandText = Convert.ToString(InStrAX[i24]);
                    sqlCommand1.ExecuteNonQuery();
                }

                DataTable dt = new DataTable();
                sqlCommand1.CommandText = sqlUPSend;
                SqlDataAdapter adp = new SqlDataAdapter(sqlCommand1);
                adp.Fill(dt);
                if ((dt.Rows.Count == 0) || (dt.Rows[0][0].ToString() == ""))
                {
                    transaction1.Rollback();
                    transaction2.Rollback();
                    return "ไม่สามารถจัดการข้อมูลในส่วนของงวดเงินเดือนได้ลองใหม่อีกครั้ง";
                }

                string ptCheckTerm = dt.Rows[0][0].ToString();
                string sqlUp24 = $@"
                    Update  SHOP_EMPDOWNLO 
                    SET     STAsendData = '1',tTermCode = '{ptCheckTerm}'
                    WHERE	RoundDate BETWEEN '{pDate1}' AND '{pDate2}'  AND ISNULL(TypeReport,'0') = '{typeReport}'  ";

                sqlCommand2.CommandText = sqlUp24;
                sqlCommand2.ExecuteNonQuery();

                transaction1.Commit();
                transaction2.Commit();
                sqlConnection1.Close();
                sqlConnection2.Close();
                TermAX = ptCheckTerm;
                return "";
            }
            catch (Exception ex)
            {
                transaction1.Rollback();
                transaction2.Rollback();

                return ex.Message;
            }
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string strDesc = "";
            switch (_pTypeReport)
            {
                case "0":
                    strDesc = "รายการประมวลผล การลงสินค้าต่างจังหวัด";
                    break;
                case "1":
                    strDesc = "รายการประมวลผล รายได้แคชเชียร์ PartTime";
                    break;
                case "2":
                    strDesc = "รายการประมวลผล รายได้ OT วันหยุด";
                    break;
                case "3":
                    strDesc = "รายการประมวลผล ค่าเบี้ยเลี้ยง+ค่าที่พัก";
                    break;
                case "4":
                    strDesc = "รายการประมวลผล ค่าคอมแคชเชียร์รวม";
                    break;
                default:
                    break;
            }
            FormShare.ChooseData _choose = new FormShare.ChooseData("ข้อมูลละเอียด", "ข้อมูลสรุป");
            if (_choose.ShowDialog() == DialogResult.Yes)
            {
                string T;

                if (_choose.sSendData == "1") T = DatagridClass.ExportExcelGridView($@"{strDesc} [ข้อมูลละเอียด]", RadGridView_ShowHD, "1");
                else T = DatagridClass.ExportExcelGridView($@"{strDesc} [ข้อมูลสรุป]", RadGridView_ShowDT, "1");

                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeOpen);
        }
    }
}

