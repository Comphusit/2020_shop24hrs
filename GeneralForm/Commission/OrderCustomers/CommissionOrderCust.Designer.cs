﻿namespace PC_Shop24Hrs.GeneralForm.Commission.OrderCustomers
{
    partial class CommissionOrderCust
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommissionOrderCust));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.RadGridView_Commission = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadDateTimePicker_Begin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Percent = new Telerik.WinControls.UI.RadTextBox();
            this.radioButton_Percent = new System.Windows.Forms.RadioButton();
            this.radTextBox_Bill = new Telerik.WinControls.UI.RadTextBox();
            this.radioButton_Bill = new System.Windows.Forms.RadioButton();
            this.radTextBox_Order = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Order = new Telerik.WinControls.UI.RadLabel();
            this.RadCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.RadDateTimePicker_End = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Clear = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_comm = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadImageButtonElement_pdf = new Telerik.WinControls.UI.RadImageButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Commission)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Commission.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Percent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Order)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Order)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_End)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(658, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.RadGridView_Commission);
            this.panel3.Controls.Add(this.RadGridView_Show);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(652, 620);
            this.panel3.TabIndex = 19;
            // 
            // RadGridView_Commission
            // 
            this.RadGridView_Commission.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Commission.Location = new System.Drawing.Point(13, 219);
            // 
            // 
            // 
            this.RadGridView_Commission.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Commission.Name = "RadGridView_Commission";
            // 
            // 
            // 
            this.RadGridView_Commission.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Commission.Size = new System.Drawing.Size(622, 87);
            this.RadGridView_Commission.TabIndex = 18;
            this.RadGridView_Commission.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Commission_ViewCellFormatting);
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(13, 328);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(622, 87);
            this.RadGridView_Show.TabIndex = 17;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(3, 634);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(652, 1);
            this.panel2.TabIndex = 18;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadDateTimePicker_Begin);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.radTextBox_Order);
            this.panel1.Controls.Add(this.radLabel_Order);
            this.panel1.Controls.Add(this.RadCheckBox_Branch);
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.RadDateTimePicker_End);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(667, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(204, 636);
            this.panel1.TabIndex = 1;
            // 
            // RadDateTimePicker_Begin
            // 
            this.RadDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_Begin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_Begin.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_Begin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_Begin.Location = new System.Drawing.Point(11, 116);
            this.RadDateTimePicker_Begin.Name = "RadDateTimePicker_Begin";
            this.RadDateTimePicker_Begin.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_Begin.TabIndex = 60;
            this.RadDateTimePicker_Begin.TabStop = false;
            this.RadDateTimePicker_Begin.Value = new System.DateTime(((long)(0)));
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(156, 179);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(33, 19);
            this.radLabel1.TabIndex = 87;
            this.radLabel1.Text = "บาท";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radLabel3);
            this.groupBox2.Controls.Add(this.radLabel2);
            this.groupBox2.Controls.Add(this.radTextBox_Percent);
            this.groupBox2.Controls.Add(this.radioButton_Percent);
            this.groupBox2.Controls.Add(this.radTextBox_Bill);
            this.groupBox2.Controls.Add(this.radioButton_Bill);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(10, 207);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 80);
            this.groupBox2.TabIndex = 88;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "บิลขาย";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(147, 52);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(23, 19);
            this.radLabel3.TabIndex = 89;
            this.radLabel3.Text = "%";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(146, 26);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(33, 19);
            this.radLabel2.TabIndex = 88;
            this.radLabel2.Text = "บาท";
            // 
            // radTextBox_Percent
            // 
            this.radTextBox_Percent.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_Percent.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Percent.Location = new System.Drawing.Point(86, 50);
            this.radTextBox_Percent.Name = "radTextBox_Percent";
            this.radTextBox_Percent.Size = new System.Drawing.Size(56, 21);
            this.radTextBox_Percent.TabIndex = 88;
            this.radTextBox_Percent.Tag = "รหัสพนักงาน";
            this.radTextBox_Percent.Text = "1";
            this.radTextBox_Percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radioButton_Percent
            // 
            this.radioButton_Percent.AutoSize = true;
            this.radioButton_Percent.Checked = true;
            this.radioButton_Percent.Location = new System.Drawing.Point(6, 50);
            this.radioButton_Percent.Name = "radioButton_Percent";
            this.radioButton_Percent.Size = new System.Drawing.Size(77, 20);
            this.radioButton_Percent.TabIndex = 3;
            this.radioButton_Percent.TabStop = true;
            this.radioButton_Percent.Text = "ยอดขาย";
            this.radioButton_Percent.UseVisualStyleBackColor = true;
            // 
            // radTextBox_Bill
            // 
            this.radTextBox_Bill.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_Bill.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Bill.Location = new System.Drawing.Point(86, 24);
            this.radTextBox_Bill.Name = "radTextBox_Bill";
            this.radTextBox_Bill.Size = new System.Drawing.Size(56, 21);
            this.radTextBox_Bill.TabIndex = 87;
            this.radTextBox_Bill.Tag = "รหัสพนักงาน";
            this.radTextBox_Bill.Text = "5";
            this.radTextBox_Bill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radioButton_Bill
            // 
            this.radioButton_Bill.AutoSize = true;
            this.radioButton_Bill.Location = new System.Drawing.Point(6, 24);
            this.radioButton_Bill.Name = "radioButton_Bill";
            this.radioButton_Bill.Size = new System.Drawing.Size(60, 20);
            this.radioButton_Bill.TabIndex = 2;
            this.radioButton_Bill.Text = "ต่อบิล";
            this.radioButton_Bill.UseVisualStyleBackColor = true;
            // 
            // radTextBox_Order
            // 
            this.radTextBox_Order.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radTextBox_Order.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Order.Location = new System.Drawing.Point(96, 177);
            this.radTextBox_Order.Name = "radTextBox_Order";
            this.radTextBox_Order.Size = new System.Drawing.Size(56, 21);
            this.radTextBox_Order.TabIndex = 85;
            this.radTextBox_Order.Tag = "รหัสพนักงาน";
            this.radTextBox_Order.Text = "5";
            this.radTextBox_Order.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel_Order
            // 
            this.radLabel_Order.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Order.Location = new System.Drawing.Point(16, 179);
            this.radLabel_Order.Name = "radLabel_Order";
            this.radLabel_Order.Size = new System.Drawing.Size(60, 19);
            this.radLabel_Order.TabIndex = 86;
            this.radLabel_Order.Text = "ออร์เดอร์";
            // 
            // RadCheckBox_Branch
            // 
            this.RadCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Branch.Location = new System.Drawing.Point(11, 44);
            this.RadCheckBox_Branch.Name = "RadCheckBox_Branch";
            this.RadCheckBox_Branch.Size = new System.Drawing.Size(76, 19);
            this.RadCheckBox_Branch.TabIndex = 62;
            this.RadCheckBox_Branch.Text = "ระบุสาขา";
            this.RadCheckBox_Branch.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Branch_ToggleStateChanged);
            this.RadCheckBox_Branch.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Branch_CheckStateChanged);
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.DropDownAnimationEnabled = false;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Enabled = false;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Branch.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(10, 69);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Branch.TabIndex = 61;
            this.RadDropDownList_Branch.Text = "radDropDownList1";
            this.RadDropDownList_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Branch_SelectedValueChanged);
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(11, 93);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Date.TabIndex = 60;
            this.radLabel_Date.Text = "ระบุวันที่";
            // 
            // RadDateTimePicker_End
            // 
            this.RadDateTimePicker_End.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_End.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_End.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_End.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_End.Location = new System.Drawing.Point(11, 146);
            this.RadDateTimePicker_End.Name = "RadDateTimePicker_End";
            this.RadDateTimePicker_End.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_End.TabIndex = 59;
            this.RadDateTimePicker_End.TabStop = false;
            this.RadDateTimePicker_End.Value = new System.DateTime(((long)(0)));
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Clear,
            this.commandBarSeparator3,
            this.radButtonElement_comm,
            this.commandBarSeparator1,
            this.radButtonElement_excel,
            this.commandBarSeparator2,
            this.RadImageButtonElement_pdf,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(204, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // radButtonElement_Clear
            // 
            this.radButtonElement_Clear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Clear.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Clear.Name = "radButtonElement_Clear";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Clear, false);
            this.radButtonElement_Clear.Text = "";
            this.radButtonElement_Clear.Click += new System.EventHandler(this.RadButtonElement_Clear_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_comm
            // 
            this.radButtonElement_comm.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.radButtonElement_comm.Name = "radButtonElement_comm";
            this.radStatusStrip1.SetSpring(this.radButtonElement_comm, false);
            this.radButtonElement_comm.Text = "";
            this.radButtonElement_comm.Click += new System.EventHandler(this.RadButtonElement_comm_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadImageButtonElement_pdf
            // 
            this.RadImageButtonElement_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadImageButtonElement_pdf.Name = "RadImageButtonElement_pdf";
            this.radStatusStrip1.SetSpring(this.RadImageButtonElement_pdf, false);
            this.RadImageButtonElement_pdf.Text = "radImageButtonElement1";
            this.RadImageButtonElement_pdf.Click += new System.EventHandler(this.RadImageButtonElement_pdf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 293);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 0;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // CommissionOrderCust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "CommissionOrderCust";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายงานค่าคอมแลกแต้มลูกค้า";
            this.Load += new System.EventHandler(this.OrderCustCommission_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Commission.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Commission)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Percent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Order)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Order)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_End)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_End;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Branch;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Clear;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Order;
        private Telerik.WinControls.UI.RadLabel radLabel_Order;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Bill;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Percent;
        private System.Windows.Forms.RadioButton radioButton_Percent;
        private System.Windows.Forms.RadioButton radioButton_Bill;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_Begin;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_comm;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadImageButtonElement RadImageButtonElement_pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadGridView RadGridView_Commission;
    }
}
