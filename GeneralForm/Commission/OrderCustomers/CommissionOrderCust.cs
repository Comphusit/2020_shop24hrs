﻿using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Commission.OrderCustomers
{
    public partial class CommissionOrderCust : Telerik.WinControls.UI.RadForm
    {
        private string BranchID = "";
        TimeSpan DateBetween;
        bool CheckComm;
        string Salary_Term = string.Empty, termDetail = string.Empty;
        //Load
        public CommissionOrderCust()
        {
            InitializeComponent();
        }

        //Load
        private void OrderCustCommission_Load(object sender, EventArgs e)
        {

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Clear.ToolTipText = "ล้างข้อมูล"; radButtonElement_Clear.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true; radButtonElement_excel.Enabled = false;
            radButtonElement_comm.ToolTipText = "ส่งเข้างวดเงินเดือน"; radButtonElement_comm.ShowBorder = true; radButtonElement_comm.Enabled = false;

            RadButton_Search.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Begin, DateTime.Now.AddDays(-10), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultRadGridView(RadGridView_Commission);

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadGridView_Show.EnableFiltering = false;
            ClearDefault();
            if (SystemClass.SystemBranchID == "MN000") SetDropdownBranch(BranchClass.GetBranchAll("'1'", "'1'"));
            else SetDropdownBranch(BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID));

            if (SystemClass.SystemBranchID != "MN000") return;

            BranchID = "";
            this.Cursor = Cursors.WaitCursor;
            DataTable DtProcessSaveInvoiceid = PosSaleClass.GetCommissionProcessSaveInvoiceid();
            if (DtProcessSaveInvoiceid.Rows.Count > 0)
            {
                string sqlup = "", sqlTranscion = "";
                foreach (DataRow row in DtProcessSaveInvoiceid.Rows) if (sqlTranscion == "")
                    {
                        switch (row[0].ToString())
                        {
                            case "PD":
                                sqlup = $@"update SHOP_MNPD_HD set INVOICEID='{ row["INVOICEID"]}',  
                                            INVOICEDATE ='{row["INVOICEDATE"]}', 
                                            CASHIERID ='{row["CASHIERID"]}',  
                                            POSGROUP ='{row["POSGROUP"]}',  
                                            DATETIMESAVE =convert(varchar,getdate(),120),  
                                            WHOSAVE  ='{SystemClass.SystemUserID_M}'  
                                            where DocNo='{row["OrderDocno"]}' ";
                                break;

                            case "OG":
                                sqlup = $@"
                                update  SHOP_MNOG_HD 
                                SET     INVOICEID='{row["INVOICEID"]}',  INVOICEDATE ='{row["INVOICEDATE"]}',CASHIERID ='{row["CASHIERID"]}',POSGROUP ='{row["POSGROUP"]}',          
                                        DATETIMESAVE =convert(varchar,getdate(),120), WHOSAVE  ='{SystemClass.SystemUserID_M}'  
                                WHERE   DocNo='{row["OrderDocno"]}'";
                                break;
                        }
                        sqlTranscion = ConnectionClass.ExecuteSQL_Main(sqlup);
                    }

                MsgBoxClass.MsgBoxShow_SaveStatus(sqlTranscion);
            }

            RadGridView_Show.Visible = false;
            RadGridView_Commission.Visible = false;
            this.Cursor = Cursors.Default;

        }

        void SetDropdownBranch(DataTable DtBranch)
        {
            RadDropDownList_Branch.DataSource = DtBranch;
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
        }

        void ClearDefault()
        {
            if (SystemClass.SystemBranchID != "MN000")
            {
                RadCheckBox_Branch.Checked = true;
                RadDateTimePicker_End.Visible = false;
                radLabel_Order.Visible = false;
                radTextBox_Order.Visible = false;
                radLabel1.Visible = false;
                groupBox2.Visible = false;
                radButtonElement_comm.Enabled = false;
                radButtonElement_excel.Enabled = false;
            }
            else
            {
                RadCheckBox_Branch.Checked = false;
                RadDateTimePicker_End.Visible = true;
                radLabel_Order.Visible = true;
                radTextBox_Order.Visible = true;
                radLabel1.Visible = true;
                groupBox2.Visible = true;
                radButtonElement_comm.Enabled = false;
                radButtonElement_excel.Enabled = true;
            }
        }
        void ClearColumns()
        {
            radButtonElement_comm.Enabled = false;
            if (RadGridView_Commission.Rows.Count > 0) RadGridView_Commission.Rows.Clear();
            if (RadGridView_Commission.Columns.Count > 0) RadGridView_Commission.Columns.Clear();

            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 60));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ALTNUM", "รหัส", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPID", "รหัส", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 150));
            RadGridView_Show.Columns["BranchID"].IsPinned = true;
            RadGridView_Show.Columns["BranchName"].IsPinned = true;
            RadGridView_Show.Columns["ALTNUM"].IsPinned = true;
            RadGridView_Show.Columns["EMPID"].IsPinned = true;
            RadGridView_Show.Columns["SPC_NAME"].IsPinned = true;

            DateBetween = RadDateTimePicker_End.Value.Date - RadDateTimePicker_Begin.Value.Date;

            for (int iDate = 0; iDate < DateBetween.Days + 1; iDate++)
            {
                String ColumnsName = RadDateTimePicker_Begin.Value.AddDays(iDate).ToString("yyyy-MM-dd");
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("Order" + ColumnsName, ColumnsName, 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("Sale" + ColumnsName,
                    "ขาย " + Environment.NewLine + ColumnsName, 90));
            }

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ReceiveOrder", "รับออร์เดอร์", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("InvoiceID", "ทำบิลขาย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PriceItem", "ยอดขายรวม", 100));

            RadGridView_Show.MasterTemplate.Columns["ReceiveOrder"].FormatString = "{0:F2}";
            RadGridView_Show.MasterTemplate.Columns["InvoiceID"].FormatString = "{0:F2}";
            RadGridView_Show.MasterTemplate.Columns["PriceItem"].FormatString = "{0:F2}";



        }

        void ClearSumColumns()
        {
            if (RadGridView_Commission.Rows.Count > 0) RadGridView_Commission.Rows.Clear();
            if (RadGridView_Commission.Columns.Count > 0)
            {
                RadGridView_Commission.Columns.Clear();
                RadGridView_Commission.SummaryRowsTop.Clear();
            }

            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCHID", "รหัสสาขา", 80));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 250));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "รหัส", 80));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CASHIERNAME", "ชื่อพนักงาน", 180));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSISION", "ตำแหน่ง", 180));

            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("ORDERSBILL", "บิลออร์เดอร์", 100));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("CASHIERBILL", "บิลขาย", 100));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("MONEYSSALE", "ยอดขายรวม", 100));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("MONEYS", "จำนวนเงินสุทธิ(ค่าคอมรวม)", 150));
            RadGridView_Commission.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("MONEYS2", "จำนวนเงิน(ค่าคอมรวม)", 150));

            RadGridView_Commission.MasterTemplate.Columns["ORDERSBILL"].FormatString = "{0:F2}";
            RadGridView_Commission.MasterTemplate.Columns["CASHIERBILL"].FormatString = "{0:F2}";
            RadGridView_Commission.MasterTemplate.Columns["MONEYSSALE"].FormatString = "{0:F2}";
            RadGridView_Commission.MasterTemplate.Columns["MONEYS"].FormatString = "{0:F2}";
            RadGridView_Commission.MasterTemplate.Columns["MONEYS2"].FormatString = "{0:F2}";


            if (SystemClass.SystemBranchID.Equals("MN000")) RadGridView_Commission.Columns["MONEYS2"].IsVisible = false;

            GridViewSummaryItem summaryItem = new GridViewSummaryItem
            {
                Name = "MONEYS",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };

            this.RadGridView_Commission.SummaryRowsTop.Add(summaryRowItem);
        }

        private double MoneyFormat(double Money)
        {
            Double MoneyRsu = 0;
            String[] MoneySplit = Money.ToString().Split('.');
            if (MoneySplit.Length > 1)
            {
                if (Double.Parse(MoneySplit[1].ToString()) > 0 &&
                    Double.Parse(MoneySplit[1].ToString()) < 49)
                {
                    MoneyRsu = 0;
                }
                else if (Double.Parse(MoneySplit[1].ToString()) > 50 &&
                  Double.Parse(MoneySplit[1].ToString()) < 99)
                {
                    MoneyRsu = 50;
                }
                MoneyRsu = Double.Parse(MoneySplit[0].ToString()) + (MoneyRsu / 100);
            }
            else
            {
                MoneyRsu = Money;

            }

            return MoneyRsu;
        }

        string SendAX(ArrayList InStr)
        {

            string pDate1 = RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            string pDate2 = RadDateTimePicker_End.Value.ToString("yyyy-MM-dd");
            termDetail = "ค่าคอม ออร์เดอร์ลูกค้า " + pDate1 + " ถึง " + pDate2;

            string sqlUPSend = string.Format(@"EXEC SPCN_Salary_InsertFromTemp '{0}','{1}','{2}' ",
                                                 pDate1, pDate2, termDetail);

            //701
            SqlConnection sqlConnection701 = new SqlConnection(IpServerConnectClass.Con701SRV);
            if (sqlConnection701.State == ConnectionState.Closed)
            {
                sqlConnection701.Open();
            }
            //  SqlCommand sqlCommand24 = new SqlCommand();
            SqlCommand sqlCommand701 = sqlConnection701.CreateCommand();
            SqlTransaction transaction701 = sqlConnection701.BeginTransaction("701");
            sqlCommand701.Connection = sqlConnection701;
            sqlCommand701.Transaction = transaction701;

            //119
            SqlConnection sqlConnection119 = new SqlConnection(IpServerConnectClass.ConSelectMain);
            if (sqlConnection119.State == ConnectionState.Closed)
            {
                sqlConnection119.Open();
            }
            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommand119 = sqlConnection119.CreateCommand();
            SqlTransaction transaction119 = sqlConnection119.BeginTransaction("119");
            sqlCommand119.Connection = sqlConnection119;
            sqlCommand119.Transaction = transaction119;

            try
            {
                for (int i24 = 0; i24 < InStr.Count; i24++)
                {
                    sqlCommand701.CommandText = Convert.ToString(InStr[i24]);
                    sqlCommand701.ExecuteNonQuery();
                }

                DataTable dt = new DataTable();
                sqlCommand701.CommandText = sqlUPSend;
                SqlDataAdapter adp = new SqlDataAdapter(sqlCommand701);
                adp.Fill(dt);
                if ((dt.Rows.Count == 0) || (dt.Rows[0][0].ToString() == ""))
                {

                    transaction701.Rollback();
                    transaction119.Rollback();
                    return "ไม่สามารถจัดการข้อมูลในส่วนของงวดเงินเดือน ค่าคอมออร์เดอร์ลูกค้าได้ ลองใหม่อีกครั้ง";
                }

                Salary_Term = dt.Rows[0][0].ToString();
                String Round = RadDateTimePicker_Begin.Value.ToString("yyyy-MM");//DateTime.Now.AddDays(0).ToString("yyyy-MM");
                for (int iComm = 0; iComm < RadGridView_Commission.RowCount; iComm++)
                {
                    if (RadGridView_Commission.Rows[iComm].Cells["ORDERSBILL"].Value.ToString() != "")
                    {
                        sqlCommand119.CommandText = $@"
                        update  SHOP_MNOG_HD
                        set     COMMISSIONOG='1',COMMISSIONOGID='{Salary_Term}',WHOIDCOMMISSIONOG='{SystemClass.SystemUserID_M}',WHONAMECOMMISSIONOG='{SystemClass.SystemUserName}',
                                DATECOMMISSIONOG=convert(varchar,getdate(),23)
                        where   CONVERT(VARCHAR,DOCDATE,23) between '{pDate1}' and '{pDate2}' AND CASHIERID='{ RadGridView_Commission.Rows[iComm].Cells["CASHIERID"].Value}'  
                                AND STADOC!='3' AND isnull(COMMISSIONOG,'0')!='1' ";

                        sqlCommand119.ExecuteNonQuery();

                        sqlCommand119.CommandText = string.Format(@"update {0}SHOP_MNPD_HD set 
                        COMMISSIONPD='1',COMMISSIONPDID='{1}',WHOIDCOMMISSIONPD='{2}',WHONAMECOMMISSIONPD='{3}',
                        DATECOMMISSIONPD=convert(varchar,getdate(),23)
                        where DocDate between '{4}' and '{5}' AND WHOINS='{6}' 
                        AND staDoc!='3' AND isnull(COMMISSIONPD,'0')!='1' ", "", Salary_Term,
                            SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                            pDate1, pDate2,
                            RadGridView_Commission.Rows[iComm].Cells["CASHIERID"].Value.ToString());
                        sqlCommand119.ExecuteNonQuery();
                    }

                    if (RadGridView_Commission.Rows[iComm].Cells["CASHIERBILL"].Value.ToString() != "")
                    {
                        sqlCommand119.CommandText = $@"
                        update  SHOP_MNOG_HD
                        set     COMMISSIONPOS='1',COMMISSIONPOSID='{Salary_Term}',WHOIDCOMMISSIONPOS='{SystemClass.SystemUserID_M}',
                                WHONAMECOMMISSIONPOS='{SystemClass.SystemUserName}',
                                DATECOMMISSIONPOS=convert(varchar,getdate(),23)
                        where   CONVERT(VARCHAR,DOCDATE,23)  between '{pDate1}' and '{pDate2}' 
                                AND CASHIERID='{RadGridView_Commission.Rows[iComm].Cells["CASHIERID"].Value}'
                                AND STADOC!='3' AND isnull(COMMISSIONPOS,'0')!='1' ";

                        sqlCommand119.ExecuteNonQuery();

                        sqlCommand119.CommandText = $@"
                        update  SHOP_MNPD_HD 
                        set     COMMISSIONPOS='1',COMMISSIONPOSID='{Salary_Term}',WHOIDCOMMISSIONPOS='{SystemClass.SystemUserID_M}',
                                WHONAMECOMMISSIONPOS='{SystemClass.SystemUserName}',
                                DATECOMMISSIONPOS=convert(varchar,getdate(),23)
                        where   INVOICEDATE between '{pDate1}' and '{pDate2}' AND CASHIERID='{RadGridView_Commission.Rows[iComm].Cells["CASHIERID"].Value}'
                                AND staDoc!='3' AND isnull(COMMISSIONPOS,'0')!='1' ";
                        sqlCommand119.ExecuteNonQuery();
                    }
                    //RadDateTimePicker_Begin.Value.ToString("yyyy-MM")
                    sqlCommand119.CommandText = string.Format(@"
                        INSERT INTO SHOP_COMMISSIONORDER(COMMID, COMMROUND, COMMDATE, COMMTYPE, COMMTYPENAME, 
                        COMMBRANCH, COMMBRANCHNAME,
                        COMMEMPLID, COMMEMPLNAME, COMMEMPLPOSSISION, 
                        COMMORDER, COMMBILLSALE, COMMNETSALE, COMMMONEY, COMMWHOINS, COMMWHONAMEINS, COMMDATEINS)
                        VALUES('{0}','{1}', convert(varchar,getdate(),25), '{2}', '{3}', '{4}', '{5}', '{6}', 
                        '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', 
                        '{13}', '{14}', convert(varchar,getdate(),25))",
                        Salary_Term, Round, "01", "ORDERCUST",
                        RadGridView_Commission.Rows[iComm].Cells["BRANCHID"].Value.ToString(),
                        RadGridView_Commission.Rows[iComm].Cells["BRANCHNAME"].Value.ToString(),
                        RadGridView_Commission.Rows[iComm].Cells["CASHIERID"].Value.ToString(),
                        RadGridView_Commission.Rows[iComm].Cells["CASHIERNAME"].Value.ToString(),
                        RadGridView_Commission.Rows[iComm].Cells["POSSISION"].Value.ToString(),
                        Double.Parse(RadGridView_Commission.Rows[iComm].Cells["ORDERSBILL"].Value.ToString()),
                        Double.Parse(RadGridView_Commission.Rows[iComm].Cells["CASHIERBILL"].Value.ToString()),
                        Double.Parse(RadGridView_Commission.Rows[iComm].Cells["MONEYSSALE"].Value.ToString()),
                        Double.Parse(RadGridView_Commission.Rows[iComm].Cells["MONEYS"].Value.ToString()),
                        SystemClass.SystemUserID_M, SystemClass.SystemUserName);
                    sqlCommand119.ExecuteNonQuery();

                }

                transaction701.Commit();
                transaction119.Commit();

                //TermAX = ptCheckTerm;
                return "";
            }
            catch (Exception ex)
            {
                transaction701.Rollback();
                transaction119.Rollback();
                radButtonElement_comm.Enabled = true;
                radButtonElement_excel.Enabled = false;
                return ex.Message;
            }
            finally
            {
                radButtonElement_comm.Enabled = false;
                radButtonElement_excel.Enabled = true;
                sqlConnection701.Close();
                sqlConnection119.Close();
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกรอบเงินเดือนเรียบร้อยแล้ว รอบเงินเดือน " + Salary_Term);
            }
        }



        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }



        #region EVEN
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            ///เช็ครายการบันทึกตามรอบเดือนก่อนถ้ามีตามเดือนที่เลือกแล้วจะไม่บันทึกซ้ำและจะแสดงรายการที่บันทึกเท่านั้น
            CheckComm = CheckCommRound(RadDateTimePicker_Begin.Value.ToString("yyyy-MM"));
            if (CheckComm)
            {
                //}
                //if (!(SystemClass.SystemBranchID.Equals("MN000")))
                //{
                this.Cursor = Cursors.WaitCursor;
                if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();
                if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();
                RadGridView_Show.SummaryRowsTop.Clear();

                // RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMID", "รหัสงวด", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMBRANCH", "รหัสสาขา", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMBRANCHNAME", "ชื่อสาขา", 150));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMEMPLID", "รหัส", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMEMPLNAME", "ชื่อพนักงาน", 180));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMEMPLPOSSISION", "ตำแหน่ง", 180));

                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COMMORDER", "บิลออร์เดอร์", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COMMBILLSALE", "บิลขาย", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COMMNETSALE", "ยอดขายรวม", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COMMMONEY", "จำนวนเงินสุทธิ(ค่าคอมรวม)", 180));

                GridViewSummaryItem summaryItem = new GridViewSummaryItem("COMMMONEY", "รวม = {0:#,##0.00}", GridAggregateFunction.Sum);
                GridViewSummaryRowItem summaryItem1 = new GridViewSummaryRowItem { summaryItem };
                this.RadGridView_Show.SummaryRowsTop.Add(summaryItem1);

                //COMMID, COMMROUND, COMMDATE, COMMTYPE, COMMTYPENAME, COMMBRANCH, COMMBRANCHNAME, COMMEMPLID, COMMEMPLNAME,
                //COMMEMPLPOSSISION, COMMORDER, COMMBILLSALE, COMMNETSALE, COMMMONEY
                //RadGridView_Show.MasterTemplate.Columns["COMMORDER"].FormatString = "{0:F2}";
                //RadGridView_Show.MasterTemplate.Columns["COMMBILLSALE"].FormatString = "{0:F2}";
                //RadGridView_Show.MasterTemplate.Columns["COMMNETSALE"].FormatString = "{0:F2}";
                //RadGridView_Show.MasterTemplate.Columns["COMMMONEY"].FormatString = "{0:F2}";

                // RadGridView_Commission.Visible = false;

                string pDate1 = RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
                string pDate2 = RadDateTimePicker_End.Value.ToString("yyyy-MM-dd");
                termDetail = "ค่าคอม ออร์เดอร์ลูกค้า " + pDate1 + " ถึง " + pDate2;

                DataTable DtBranchComm = GetBranchCommission(SystemClass.SystemBranchID, RadDateTimePicker_Begin.Value.ToString("yyyy-MM"));
                Salary_Term = DtBranchComm.Rows[0]["COMMID"].ToString();
                if (DtBranchComm.Rows.Count > 0)
                {
                    foreach (DataRow rowBranchComm in DtBranchComm.Rows)
                    {
                        RadGridView_Show.Rows.Add(new object[] {rowBranchComm.ItemArray[5].ToString(),
                            rowBranchComm.ItemArray[6].ToString(),rowBranchComm.ItemArray[7].ToString(),
                            rowBranchComm.ItemArray[8].ToString(),rowBranchComm.ItemArray[9].ToString(),
                            Double.Parse(rowBranchComm.ItemArray[10].ToString()).ToString("N2"),
                            Double.Parse(rowBranchComm.ItemArray[11].ToString()).ToString("N2"),
                            Double.Parse(rowBranchComm.ItemArray[12].ToString()).ToString("N2"),
                            Double.Parse(rowBranchComm.ItemArray[13].ToString()).ToString("N2")});
                    }
                }
                RadGridView_Show.Visible = true; RadGridView_Show.Dock = DockStyle.Fill;
                RadGridView_Commission.Visible = false; RadGridView_Commission.Dock = DockStyle.None;
                this.Cursor = Cursors.Default;
                return;
            }
            this.Cursor = Cursors.WaitCursor;
            termDetail = "";
            Salary_Term = "";
            ClearColumns();
            //แคชเชียร์ที่เปิดออร์เดอร์และทำบิลขาย
            DataTable DtCash = EmployeeClass.CommissionGetCash_ByBranch(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadDateTimePicker_End.Value.ToString("yyyy-MM-dd"), BranchID);
            if (DtCash.Rows.Count > 0)
            {
                foreach (DataRow rowCash in DtCash.Rows)
                {
                    RadGridView_Show.Rows.Add(new object[] {rowCash.ItemArray[0].ToString(),
                    rowCash.ItemArray[1].ToString(),rowCash.ItemArray[4].ToString(),
                        rowCash.ItemArray[2].ToString(),rowCash.ItemArray[3].ToString()});
                }
            }

            //หาจำนวนบิลและยอดขายแต่ละวันตามแคชเชียร์
            DataTable DtCashBill = PosSaleClass.GetCommissionGetCountBillCash_ByBranch(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadDateTimePicker_End.Value.ToString("yyyy-MM-dd"), BranchID);
            if (DtCashBill.Rows.Count > 0)
            {
                // DocDate,Sale.POSGROUP as Branch,CASHIERID as Cash
                for (int iRow = 0; iRow < RadGridView_Show.Rows.Count; iRow++)
                {
                    //- (8 + DateBetween.Days) 
                    for (int iCol = 1; iCol < RadGridView_Show.Columns.Count - (8 + DateBetween.Days); iCol++)
                    {
                        int columns = (iCol * 2) + 3;
                        DataRow[] result = DtCashBill.Select(string.Format(@"DocDate = '{0}' and Branch='{1}' and Cash='{2}'",
                            RadGridView_Show.Columns[columns].HeaderText.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["BranchID"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["EMPID"].Value.ToString()));
                        if (result.Length > 0)
                        {
                            double Order, BillCount, billprice;
                            Order = double.Parse(result[0].ItemArray[3].ToString());
                            BillCount = double.Parse(result[0].ItemArray[4].ToString());
                            billprice = double.Parse(result[0].ItemArray[5].ToString());

                            RadGridView_Show.Rows[iRow].Cells[columns].Value = Order.ToString("N2");
                            if (radioButton_Bill.Checked == true)
                            {
                                if (BillCount == 0)
                                {
                                    RadGridView_Show.Rows[iRow].Cells[columns + 1].Value = "0.00";
                                }
                                else
                                {
                                    RadGridView_Show.Rows[iRow].Cells[columns + 1].Value = BillCount.ToString("N2");
                                }

                            }
                            if (radioButton_Percent.Checked == true)
                            {
                                if (billprice == 0)
                                {
                                    RadGridView_Show.Rows[iRow].Cells[columns + 1].Value = "0.00";
                                }
                                else
                                {
                                    RadGridView_Show.Rows[iRow].Cells[columns + 1].Value = billprice.ToString("N2");
                                }
                            }

                            double TotalOrder = 0, TotalBillCount = 0, Totalbillprice = 0;
                            if (RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 3].Value != null)
                                TotalOrder = Double.Parse(RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 3].Value.ToString());
                            if (RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 2].Value != null)
                                TotalBillCount = Double.Parse(RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 2].Value.ToString());
                            if (RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 1].Value != null)
                                Totalbillprice = Double.Parse(RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 1].Value.ToString());


                            RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 3].Value = (TotalOrder + Order).ToString("N2");

                            RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 2].Value = (TotalBillCount + BillCount).ToString("N2");
                            RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 1].Value = (Totalbillprice + billprice).ToString("N2");
                        }
                        else
                        {
                            RadGridView_Show.Rows[iRow].Cells[columns].Value = "0.00";
                            RadGridView_Show.Rows[iRow].Cells[columns + 1].Value = "0.00";

                            if (RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 3].Value == null)
                                RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 3].Value = "0.00";

                            if (RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 2].Value == null)
                                RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 2].Value = "0.00";

                            if (RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 1].Value == null)
                                RadGridView_Show.Rows[iRow].Cells[RadGridView_Show.ColumnCount - 1].Value = "0.00";
                        }
                    }
                }


                //รวมรายละเอียดเงินทีต้องได้ของแต่ละคน
                ClearSumColumns();
                Double Money_Order = 0, Money_Bill = 0, Money_Percent = 0;
                if (radTextBox_Order.Text != "")
                {
                    Money_Order = int.Parse(radTextBox_Order.Text);
                }

                if (radioButton_Bill.Checked == true)
                {
                    if (radTextBox_Bill.Text != "")
                    {
                        Money_Bill = int.Parse(radTextBox_Bill.Text);
                    }
                }
                if (radioButton_Percent.Checked == true)
                {
                    if (radTextBox_Percent.Text != "")
                    {
                        Money_Percent = Double.Parse(radTextBox_Percent.Text) / 100;
                    }
                }


                for (int iRowShow = 0; iRowShow < RadGridView_Show.RowCount; iRowShow++)
                {
                    String CheckCash = "0";
                    Double MoneyOrder = 0, MoneyBill = 0, MoneyBillMoney = 0;
                    int RowIndexComm = 0;

                    if (RadGridView_Commission.Rows.Count == 0)
                    {
                        //DataTable DtDept = EmplClass.GetDept_byEmplId(RadGridView_Show.Rows[iRowShow].Cells["ALTNUM"].Value.ToString());
                        //DataTable DtDept = EmplClass.GetEmployee(RadGridView_Show.Rows[iRowShow].Cells["ALTNUM"].Value.ToString());
                        DataTable DtDept = Models.EmplClass.GetEmployee_Altnum(RadGridView_Show.Rows[iRowShow].Cells["ALTNUM"].Value.ToString());
                        if (DtDept.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลแผนกพนักงาน " + Environment.NewLine +
                                "รหัส  " + RadGridView_Show.Rows[iRowShow].Cells["EMPID"].Value.ToString() + Environment.NewLine +
                                "ชื่อ   " + RadGridView_Show.Rows[iRowShow].Cells["SPC_NAME"].Value.ToString());
                            return;
                        }

                        RadGridView_Commission.Rows.Add(DtDept.Rows[0]["NUM"].ToString(),
                            DtDept.Rows[0]["DESCRIPTION"].ToString(),
                            RadGridView_Show.Rows[iRowShow].Cells["EMPID"].Value.ToString(),
                            RadGridView_Show.Rows[iRowShow].Cells["SPC_NAME"].Value.ToString(),
                            DtDept.Rows[0]["POSSITION"].ToString(),
                            "0", "0", "0", "0", "0");
                    }

                    for (int iRowComm = 0; iRowComm < RadGridView_Commission.RowCount; iRowComm++)//
                    {

                        if (RadGridView_Show.Rows[iRowShow].Cells["EMPID"].Value.ToString() == RadGridView_Commission.Rows[iRowComm].Cells["CASHIERID"].Value.ToString())
                        {
                            if (Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["ReceiveOrder"].Value.ToString()) != 0)
                            {
                                MoneyOrder += Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["ReceiveOrder"].Value.ToString());
                            }
                            if (Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["InvoiceID"].Value.ToString()) != 0)
                            {
                                MoneyBill += Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["InvoiceID"].Value.ToString());
                            }

                            if (Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["PriceItem"].Value.ToString()) != 0)
                            {
                                MoneyBillMoney += Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["PriceItem"].Value.ToString());
                            }
                            CheckCash = "1";
                            RowIndexComm = iRowComm;
                        }
                    }

                    Double Commission = 0;
                    if (MoneyOrder.Equals(0)) MoneyOrder = Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["ReceiveOrder"].Value.ToString());
                    if (MoneyBill.Equals(0)) MoneyBill = Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["InvoiceID"].Value.ToString());
                    if (MoneyBillMoney.Equals(0)) MoneyBillMoney = Double.Parse(RadGridView_Show.Rows[iRowShow].Cells["PriceItem"].Value.ToString());

                    if (radioButton_Bill.Checked == true)
                    {
                        Commission =
                            MoneyOrder * Money_Order +
                            MoneyBill * Money_Bill;
                    }
                    if (radioButton_Percent.Checked == true)
                    {
                        Commission =
                            MoneyOrder * Money_Order +
                            MoneyBillMoney * Money_Percent;
                    }

                    if (String.Format("{0:n}", Commission.ToString("N2")) == "0.00") continue;

                    if (CheckCash == "0")
                    {
                        //DataTable DtDept = EmplClass.GetDept_byEmplId(RadGridView_Show.Rows[iRowShow].Cells["ALTNUM"].Value.ToString());
                        //DataTable DtDept = EmplClass.GetEmployee(RadGridView_Show.Rows[iRowShow].Cells["ALTNUM"].Value.ToString());
                        DataTable DtDept = Models.EmplClass.GetEmployee_Altnum(RadGridView_Show.Rows[iRowShow].Cells["ALTNUM"].Value.ToString());
                        if (DtDept.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีข้อมูลแผนกพนักงาน " + Environment.NewLine +
                                "รหัส  " + RadGridView_Show.Rows[iRowShow].Cells["EMPID"].Value.ToString() + Environment.NewLine +
                                "ชื่อ   " + RadGridView_Show.Rows[iRowShow].Cells["SPC_NAME"].Value.ToString());
                            return;
                        }
                        RadGridView_Commission.Rows.Add(DtDept.Rows[0]["NUM"].ToString(),
                            DtDept.Rows[0]["DESCRIPTION"].ToString(),
                            RadGridView_Show.Rows[iRowShow].Cells["EMPID"].Value.ToString(),
                            RadGridView_Show.Rows[iRowShow].Cells["SPC_NAME"].Value.ToString(),
                            DtDept.Rows[0]["POSSITION"].ToString(),
                             String.Format("{0:n}", MoneyOrder),
                             String.Format("{0:n}", MoneyBill),
                             String.Format("{0:n}", MoneyBillMoney),
                             String.Format("{0:n}", Commission.ToString("N2")),
                            String.Format("{0:n}", Double.Parse(Commission.ToString()).ToString("N2")));

                        RadGridView_Commission.Rows[RadGridView_Commission.Rows.Count - 1].Cells["MONEYS"].Value =
                          String.Format("{0:n}", MoneyFormat(Double.Parse(RadGridView_Commission.Rows[RadGridView_Commission.Rows.Count - 1].Cells["MONEYS2"].Value.ToString())));
                        //String.Format("{0:n}", Double.Parse(MoneyTotal.ToString()).ToString("N2")) 
                    }
                    else
                    {

                        RadGridView_Commission.Rows[RowIndexComm].Cells["ORDERSBILL"].Value =
                           String.Format("{0:n}", Double.Parse(RadGridView_Commission.Rows[RowIndexComm].Cells["ORDERSBILL"].Value.ToString())
                           + MoneyOrder);
                        RadGridView_Commission.Rows[RowIndexComm].Cells["CASHIERBILL"].Value =
                             String.Format("{0:n}", Double.Parse(RadGridView_Commission.Rows[RowIndexComm].Cells["CASHIERBILL"].Value.ToString())
                             + MoneyBill);
                        RadGridView_Commission.Rows[RowIndexComm].Cells["MONEYSSALE"].Value =
                             String.Format("{0:n}", Double.Parse(RadGridView_Commission.Rows[RowIndexComm].Cells["MONEYSSALE"].Value.ToString())
                             + MoneyBillMoney);

                        RadGridView_Commission.Rows[RowIndexComm].Cells["MONEYS2"].Value =
                          String.Format("{0:n}", Double.Parse(RadGridView_Commission.Rows[RowIndexComm].Cells["MONEYS2"].Value.ToString())
                          + Commission);

                        RadGridView_Commission.Rows[RowIndexComm].Cells["MONEYS"].Value =
                          String.Format("{0:n}", MoneyFormat(Double.Parse(RadGridView_Commission.Rows[RowIndexComm].Cells["MONEYS2"].Value.ToString())));
                    }
                }
            }

            for (int iRow = 0; iRow < RadGridView_Commission.Rows.Count; iRow++)
            {
                if (double.Parse(RadGridView_Commission.Rows[iRow].Cells[8].Value.ToString()) < 1)
                {
                    RadGridView_Commission.Rows.RemoveAt(iRow);
                    iRow--;
                }
                else
                {
                    RadGridView_Commission.Rows[iRow].Cells[8].Value =
                       Math.Round(double.Parse(RadGridView_Commission.Rows[iRow].Cells[8].Value.ToString()));
                }
            }

            RadGridView_Show.Visible = false; RadGridView_Show.Dock = DockStyle.None;
            RadGridView_Commission.Visible = true; RadGridView_Commission.Dock = DockStyle.Fill;

            this.Cursor = Cursors.Default;

            if (RadGridView_Show.Rows.Count > 0 && RadGridView_Commission.Rows.Count > 0
                && SystemClass.SystemBranchID == "MN000" && !(CheckComm))
            {
                radButtonElement_comm.Enabled = true;
            }
        }

        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            string T;
            if (RadGridView_Commission.Visible)
                T = DatagridClass.ExportExcelGridView("ยังไม่บันทึกรอบจ่าย", RadGridView_Commission, "1");
            else
                T = DatagridClass.ExportExcelGridView(termDetail + "_" + Salary_Term, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (SystemClass.SystemBranchID == "MN000")
            {
                if (RadCheckBox_Branch.CheckState == CheckState.Checked)
                {
                    RadDropDownList_Branch.Enabled = true;
                    BranchID = RadDropDownList_Branch.SelectedValue.ToString();
                }
                else
                {
                    RadDropDownList_Branch.Enabled = false;
                    BranchID = "";
                }
            }
            else
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                BranchID = SystemClass.SystemBranchID;
            }
        }

        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearColumns();
        }

        private void RadButtonElement_comm_Click(object sender, EventArgs e)
        {
            ////ถามcenter รอบวันที่ลงค่าคอม ต้องเก็บรอบก่อนไม่งั้นจะซ้ำ
            ///เมื่อ center บันทึกแล้ว ให้ลองกดค้นหาเองดูก่อนว่ารายการยังแสดงใหม่ เพราะรายการที่บันทึกแล้วต้องไม่แสดงในรายการ
            ////เช็ครายการบันทึกรอบเดือนที่เลือก ก่อนบันทึกซ้ำ อย่าลืม
            if (RadGridView_Show.Rows.Count == 0 && RadGridView_Commission.Rows.Count == 0) return;

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ส่งข้อมูลเข้างวดเงินเดือน ไม่สามารถแก้ไขได้") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;
            ArrayList InStr = new ArrayList();
            for (int i = 0; i < RadGridView_Commission.Rows.Count; i++)
            {
                InStr.Add(string.Format(@"
                    INSERT INTO tblTempInsertDt (tEmpCode,tEmpAmount ) VALUES (
                    '{0}', '{1}')",
                    RadGridView_Commission.Rows[i].Cells["CASHIERID"].Value.ToString(),
                    RadGridView_Commission.Rows[i].Cells["MONEYS"].Value.ToString()));
            }
            if (InStr.Count > 0)
            {
                SendAX(InStr);
            }
            this.Cursor = Cursors.Default;
        }

        private void RadImageButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }


        private void RadCheckBox_Branch_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (SystemClass.SystemBranchID == "MN000")
            {
                if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = false; else RadDropDownList_Branch.Enabled = true;
            }
        }

        private void RadGridView_Commission_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true && RadDropDownList_Branch.SelectedValue.ToString() != "") BranchID = RadDropDownList_Branch.SelectedValue.ToString();
        }
        #endregion

        //ดึงข้อมูลที่บันทึกไว้
        DataTable GetBranchCommission(string Branch, string Date)
        {
            string conBranch = "";
            if (Branch != "MN000") conBranch = $@"AND COMMBRANCH = '{Branch}' ";
            string Sql = $@" 
                SELECT  COMMID, COMMROUND, COMMDATE, COMMTYPE, COMMTYPENAME, COMMBRANCH, COMMBRANCHNAME, COMMEMPLID, COMMEMPLNAME,
                        COMMEMPLPOSSISION, COMMORDER, COMMBILLSALE, COMMNETSALE, COMMMONEY  
                FROM    SHOP_COMMISSIONORDER WITH (NOLOCK)
                WHERE   COMMROUND = '{Date}' {conBranch}
                ORDER BY COMMBRANCH,COMMEMPLID ";
            return ConnectionClass.SelectSQL_Main(Sql);
        }
        //เช็ครอบเงินเดือนว่าเข้าหรือยัง
        bool CheckCommRound(string CommRound)
        {
            string Sql = $@"
                SELECT  COMMID 
                FROM    SHOP_COMMISSIONORDER  WITH (NOLOCK)
                WHERE   COMMROUND = '{CommRound}'";
            bool Rsu = false;
            if (ConnectionClass.SelectSQL_Main(Sql).Rows.Count > 0) Rsu = true;
            return Rsu;
        }
    }

}
