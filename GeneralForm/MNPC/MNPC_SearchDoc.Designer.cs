﻿namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    partial class MNPC_SearchDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNPC_SearchDoc));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_yy = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Docno = new Telerik.WinControls.UI.RadTextBox();
            this.radioButton_Barcode = new System.Windows.Forms.RadioButton();
            this.radioButton_Docno = new System.Windows.Forms.RadioButton();
            this.radTextBox_MM = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_dd = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_yy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_dd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(79, 131);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 5;
            this.radButton_Save.Text = "ค้นหา";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(175, 131);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 6;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Input.Location = new System.Drawing.Point(26, 35);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(47, 19);
            this.radLabel_Input.TabIndex = 24;
            this.radLabel_Input.Text = "MNPC";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(79, 29);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(201, 25);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.Tag = "";
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(26, 95);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(47, 19);
            this.radLabel1.TabIndex = 25;
            this.radLabel1.Text = "MNPC";
            // 
            // radTextBox_yy
            // 
            this.radTextBox_yy.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_yy.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_yy.Location = new System.Drawing.Point(79, 89);
            this.radTextBox_yy.MaxLength = 2;
            this.radTextBox_yy.Name = "radTextBox_yy";
            this.radTextBox_yy.Size = new System.Drawing.Size(30, 25);
            this.radTextBox_yy.TabIndex = 4;
            this.radTextBox_yy.Tag = "";
            this.radTextBox_yy.Text = "20";
            this.radTextBox_yy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.radTextBox_yy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_yy_KeyDown);
            // 
            // radTextBox_Docno
            // 
            this.radTextBox_Docno.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Docno.Location = new System.Drawing.Point(175, 89);
            this.radTextBox_Docno.Name = "radTextBox_Docno";
            this.radTextBox_Docno.Size = new System.Drawing.Size(105, 25);
            this.radTextBox_Docno.TabIndex = 2;
            this.radTextBox_Docno.Tag = "";
            this.radTextBox_Docno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Docno_KeyDown);
            // 
            // radioButton_Barcode
            // 
            this.radioButton_Barcode.AutoSize = true;
            this.radioButton_Barcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Barcode.Location = new System.Drawing.Point(26, 3);
            this.radioButton_Barcode.Name = "radioButton_Barcode";
            this.radioButton_Barcode.Size = new System.Drawing.Size(73, 20);
            this.radioButton_Barcode.TabIndex = 26;
            this.radioButton_Barcode.Text = "บาร์โค๊ด";
            this.radioButton_Barcode.UseVisualStyleBackColor = true;
            this.radioButton_Barcode.CheckedChanged += new System.EventHandler(this.RadioButton_Barcode_CheckedChanged);
            // 
            // radioButton_Docno
            // 
            this.radioButton_Docno.AutoSize = true;
            this.radioButton_Docno.Checked = true;
            this.radioButton_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Docno.Location = new System.Drawing.Point(26, 63);
            this.radioButton_Docno.Name = "radioButton_Docno";
            this.radioButton_Docno.Size = new System.Drawing.Size(99, 20);
            this.radioButton_Docno.TabIndex = 27;
            this.radioButton_Docno.TabStop = true;
            this.radioButton_Docno.Text = "เลขที่เอกสาร";
            this.radioButton_Docno.UseVisualStyleBackColor = true;
            this.radioButton_Docno.CheckedChanged += new System.EventHandler(this.RadioButton_Docno_CheckedChanged);
            // 
            // radTextBox_MM
            // 
            this.radTextBox_MM.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_MM.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_MM.Location = new System.Drawing.Point(111, 89);
            this.radTextBox_MM.MaxLength = 2;
            this.radTextBox_MM.Name = "radTextBox_MM";
            this.radTextBox_MM.Size = new System.Drawing.Size(30, 25);
            this.radTextBox_MM.TabIndex = 3;
            this.radTextBox_MM.Tag = "";
            this.radTextBox_MM.Text = "20";
            this.radTextBox_MM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.radTextBox_MM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_MM_KeyDown);
            // 
            // radTextBox_dd
            // 
            this.radTextBox_dd.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_dd.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_dd.Location = new System.Drawing.Point(143, 89);
            this.radTextBox_dd.MaxLength = 2;
            this.radTextBox_dd.Name = "radTextBox_dd";
            this.radTextBox_dd.Size = new System.Drawing.Size(30, 25);
            this.radTextBox_dd.TabIndex = 1;
            this.radTextBox_dd.Tag = "";
            this.radTextBox_dd.Text = "20";
            this.radTextBox_dd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.radTextBox_dd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_dd_KeyDown);
            // 
            // SearchDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(327, 190);
            this.Controls.Add(this.radTextBox_dd);
            this.Controls.Add(this.radTextBox_MM);
            this.Controls.Add(this.radioButton_Docno);
            this.Controls.Add(this.radioButton_Barcode);
            this.Controls.Add(this.radTextBox_Docno);
            this.Controls.Add(this.radTextBox_yy);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_Barcode);
            this.Controls.Add(this.radLabel_Input);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchDocument";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ค้นหา";
            this.Load += new System.EventHandler(this.MNPC_SearchDoc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_yy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_MM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_dd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_yy;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Docno;
        private System.Windows.Forms.RadioButton radioButton_Barcode;
        private System.Windows.Forms.RadioButton radioButton_Docno;
        private Telerik.WinControls.UI.RadTextBox radTextBox_MM;
        private Telerik.WinControls.UI.RadTextBox radTextBox_dd;
    }
}
