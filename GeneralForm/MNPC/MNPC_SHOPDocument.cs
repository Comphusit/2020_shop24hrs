﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    public partial class MNPC_SHOPDocument : Telerik.WinControls.UI.RadForm
    {
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly string _pType;//  SHOP หรือ  SUPC

        string MNPCDOCNO, MNPCDate, TypeProduct;

        Data_ITEMBARCODE Data_ITEMBARCODE;

        int BoxCount, BoxSeq;
        //Set
        public MNPC_SHOPDocument(string pType)
        {
            InitializeComponent();
            _pType = pType;
        }

        //Load Main
        private void MNPC_Document_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดเอกสารใหม่";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadButton_Apv.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SEQNO", "ลำดับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrderEdit", "จำนวนแก้ไข", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_SALESPRICETYPE", "ประเภทบาร์โค๊ด"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("FACTOR", "อัตราส่วน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICEUNIT", "ราคา:หน่วย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NET", "ยอดรวม", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK_NAME", "หมายเหตุ", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("REMARK_ID", "รหัสหมายเหตุ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนกรับผิดชอบ", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DEPT", "แผนกจัดซื้อ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_ITEMTYPE", "สถานะ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_BARCODEDISCOUNT", "STA_BARCODEDISCOUNT"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMBARCODE_DISDIS", "ITEMBARCODE_DISDIS"));

            DatagridClass.SetCellBackClolorByExpression("QtyOrderEdit", "QtyOrderEdit > '0' ", ConfigClass.SetColor_Red(), RadGridView_Show);

            GridViewSummaryItem summaryItem1 = new GridViewSummaryItem
            {
                Name = "NET",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem1 = new GridViewSummaryRowItem { summaryItem1 };
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem1);
            this.RadGridView_Show.BottomPinnedRowsMode = GridViewBottomPinnedRowsMode.Float;

            RadGridView_Show.MasterTemplate.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.ReadOnly = true;

            RadButton_Apv.Text = "ยืนยัน";
            if (_pType == "SUPC") RadButton_Apv.Text = "อนุมัติ";

            ClearData("");
            Set_Branch();
        }

        //Clear
        void ClearData(string status)
        {
            switch (status)
            {
                case "SAVE":
                    radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน";
                    radLabel_StatusBill.ForeColor = Color.Black;
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                    RadDropDownList_Branch.Enabled = false;
                    radTextBox_Barcode.Enabled = true;

                    RadButton_Cancel.Enabled = true;
                    RadButton_Apv.Enabled = true;
                    radTextBox_Barcode.SelectAll();
                    break;
                case "CONF":
                    radLabel_StatusBill.Text = "สถานะบิล : ยืนยัน";
                    radLabel_StatusBill.ForeColor = Color.Black;
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel();

                    RadDropDownList_Branch.Enabled = false;

                    if (_pType == "SHOP")
                    {
                        radTextBox_Barcode.Enabled = false;
                        RadButton_Cancel.Enabled = false;
                        RadButton_Apv.Enabled = false;
                    }
                    else
                    {
                        radTextBox_Barcode.Enabled = true;
                        RadButton_Cancel.Enabled = true;
                        RadButton_Apv.Enabled = true;
                    }

                    break;
                case "APV":
                    radLabel_StatusBill.Text = "สถานะบิล : อนุมัติ";
                    radLabel_StatusBill.ForeColor = Color.Black;
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();

                    RadDropDownList_Branch.Enabled = false;

                    radTextBox_Barcode.Enabled = false;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;
                    break;
                case "CENCEL":
                    radLabel_StatusBill.Text = "สถานะบิล : ยกเลิก";
                    radLabel_StatusBill.ForeColor = Color.Black;
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel();

                    RadDropDownList_Branch.Enabled = false;

                    radTextBox_Barcode.Enabled = false;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;
                    break;
                default:
                    MNPCDOCNO = ""; MNPCDate = "";// Branch = ""; BranchName = "";Docno = "";
                    BoxCount = 0; BoxSeq = 0; //Seq = 0;
                    TypeProduct = ""; //Box = "";
                    // PriceItemNet = 0;

                    if (RadGridView_Show.Rows.Count > 0)
                    {
                        RadGridView_Show.DataSource = null;
                        RadGridView_Show.Rows.Clear();
                    }
                    radLabel_Docno.Text = "MNPC" + DateTime.Now.ToString("yyMMdd") + @"000000";
                    radLabel_Type.Text = string.Empty;
                    //radLabel_PriceNet.Text = string.Empty;
                    radLabel_Box.Text = "-";

                    radLabel_StatusBill.Text = string.Empty;
                    radLabel_StatusBill.ForeColor = Color.Black;
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel();

                    RadDropDownList_Branch.Enabled = true;
                    RadDropDownList_Branch.Refresh();

                    radTextBox_Barcode.Enabled = true;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;

                    radLabel_EmplUp.Text = string.Empty;
                    radLabel_DateUp.Text = string.Empty;
                    break;

            }

            radTextBox_Barcode.Focus();
        }
        //Set DropDown
        void Set_Branch()
        {
            _ = DatagridClass.SetDropDownList_Branch(RadDropDownList_Branch, " '1','4' ");
        }

        #region "SET GRIDVIEW"


        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        #endregion

        //Add Docno
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData("");
            Set_Branch();
        }
        //Find Docno
        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            using (MNPC_FindData mNPC_FindData = new MNPC_FindData(_pType, "0"))
            {
                DialogResult dr = mNPC_FindData.ShowDialog();
                if (dr == DialogResult.Yes) SetGridAndData(MNPC_Class.GetPCDetailHD(mNPC_FindData.pDocno));
            }
        }
        //Set HD
        void SetGridAndData(DataTable DtPc)
        {
            if (DtPc.Rows.Count > 0)
            {
                ClearData(DtPc.Rows[0]["StaDoc"].ToString());

                MNPCDOCNO = DtPc.Rows[0]["DocNo"].ToString();
                radLabel_Docno.Text = DtPc.Rows[0]["DocNo"].ToString();
                radLabel_Type.Text = DtPc.Rows[0]["typeproduct"].ToString();

                radLabel_Box.Text = DtPc.Rows[0]["Box"].ToString();
                BoxCount = int.Parse(DtPc.Rows[0]["Box"].ToString());

                MNPCDate = DtPc.Rows[0]["DocDate"].ToString();

                RadDropDownList_Branch.SelectedValue = DtPc.Rows[0]["branch"].ToString();

                radLabel_EmplUp.Text = DtPc.Rows[0]["WhoName"].ToString();
                radLabel_DateUp.Text = DtPc.Rows[0]["DateUp"].ToString();

                if (RadGridView_Show.Rows.Count > 0)
                {
                    RadGridView_Show.DataSource = null;
                    RadGridView_Show.Rows.Clear();
                    RadGridView_Show.Refresh();
                }

                RadGridView_Show.DataSource = Models.DptClass.MNPC_GetPc_DT(radLabel_Docno.Text);  //MNPC_Class.GetPc_DT(radLabel_Docno.Text);
            }
            else
            {
                ClearData("");
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีเอกสารที่ต้องการค้นหา");
                return;

            }
        }
        //Apv or prc
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {

            if (_pType == "SHOP")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยัน ยืนยันเอกสารคืนสินค้า") == DialogResult.No) return;

                using (FormShare.InputData inputData = new FormShare.InputData("0", "ลังใส่สินค้าคืน", "จำนวน", "ลัง"))
                {
                    DialogResult dr = inputData.ShowDialog();
                    if (dr == DialogResult.Yes)
                    {
                        BoxCount = int.Parse(inputData.pInputData);
                        string resault = ConnectionClass.ExecuteSQL_Main(MNPC_Class.UpdateHD("0", radLabel_Docno.Text, BoxCount));
                        MsgBoxClass.MsgBoxShow_SaveStatus(resault);
                        if (resault == "")
                        {
                            if (BoxCount != 0) PrintDocument(BoxCount);
                            SetGridAndData(MNPC_Class.GetPCDetailHD(radLabel_Docno.Text));
                            ClearData("CONF");
                            return;
                        }
                    }
                }
            }
            else // CN-SUPC
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยัน อนุมัติเอกสารคืนสินค้า") == DialogResult.No) return;

                ArrayList SqlAx = new ArrayList();
                Var_POSTOTABLE varHD = new Var_POSTOTABLE
                {
                    FREIGHTSLIPTYPE = "98",
                    CASHIERID = SystemClass.SystemUserID_M,
                    ALLPRINTSTICKER = "0",
                    TRANSFERSTATUS = "2",
                    INVENTLOCATIONIDTO = "RETURN",
                    INVENTLOCATIONIDFROM = RadDropDownList_Branch.SelectedValue.ToString(),
                    TRANSFERID = MNPCDOCNO,
                    SHIPDATE = MNPCDate,
                    VOUCHERID = MNPCDOCNO
                };
                SqlAx.Add(AX_SendData.SaveHD_POSTOTABLE18(varHD));

                for (int i = 0; i < RadGridView_Show.RowCount; i++)
                {
                    double Qty = Double.Parse(RadGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString());
                    if (RadGridView_Show.Rows[i].Cells["QtyOrderEdit"].Value.ToString() != "0") Qty = Double.Parse(RadGridView_Show.Rows[i].Cells["QtyOrderEdit"].Value.ToString());
                    if (Qty != 0)
                    {
                        Var_POSTOLINE varDT = new Var_POSTOLINE()
                        {
                            VOUCHERID = MNPCDOCNO,
                            SHIPDATE = MNPCDate,
                            LINENUM = i + 1,
                            QTY = Qty,
                            SALESPRICE = Double.Parse(RadGridView_Show.Rows[i].Cells["PRICEUNIT"].Value.ToString()),
                            ITEMID = RadGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString(),
                            NAME = RadGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "").Replace("{", "").Replace("}", ""),
                            SALESUNIT = RadGridView_Show.Rows[i].Cells["UNITID"].Value.ToString(),
                            ITEMBARCODE = RadGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString(),
                            INVENTDIMID = RadGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString(),
                            INVENTTRANSID = $@"{MNPCDOCNO}_{i + 1}"//; //MNPCDOCNO + "_" + (i + 1).ToString()
                        };
                        SqlAx.Add(AX_SendData.SaveDT_POSTOLINE18(varDT));
                    }
                }

                ArrayList sql24 = new ArrayList
                {
                    MNPC_Class.UpdateHD("1", radLabel_Docno.Text, 0)
                };

                string resualt = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, SqlAx);
                MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                if (resualt == "") ClearData("APV");
            }
        }
        //TextBox Enter
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            int checkRows = 0;
            for (int iC = 0; iC < RadGridView_Show.RowCount; iC++)
            {
                if (double.Parse(RadGridView_Show.Rows[iC].Cells["QtyOrder"].Value.ToString()) > 0) checkRows++;
            }
            if (checkRows == 10)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถคืนสินค้าได้ เนื่องจาก 1 เอกสารมีสินค้าได้ 10 รายการเท่านั้น");
                return;
            }

            if (e.KeyCode == Keys.F4)
            {
                string SqlWhere;
                if (radTextBox_Barcode.Text != "")
                { SqlWhere = "AND SPC_ITEMNAME LIKE '%" + radTextBox_Barcode.Text.Replace(" ", "%") + @"%' "; }
                else { SqlWhere = ""; }

                ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(SqlWhere);
                if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                {

                    Data_ITEMBARCODE = ShowDataDGV_Itembarcode.items;
                    radTextBox_Barcode.Text = Data_ITEMBARCODE.Itembarcode_ITEMBARCODE;
                }
                else
                {
                    radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                }
            }
            else if (e.KeyCode == Keys.Enter && radTextBox_Barcode.Text.Length > 0)
            {
                Data_ITEMBARCODE = new Data_ITEMBARCODE(radTextBox_Barcode.Text);
                if (Data_ITEMBARCODE.Itembarcode_ITEMBARCODE == null)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการสินค้าที่ต้องการค้นหา กรุณาลองใหม่อีกครั้ง");
                    radTextBox_Barcode.Focus();
                    return;
                }

                if (Data_ITEMBARCODE.Itembarcode_SPC_ITEMACTIVE == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถคืนสินค้าได้ เนื่องจากเป็นสินค้าสถานะ หยุด ใช้งาน");
                    return;
                }

                using (MNPC_InputDetail MNPC_ShopDocumentDetail = new MNPC_InputDetail(
                                                                        Data_ITEMBARCODE.Itembarcode_ITEMBARCODE,
                                                                        Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME,
                                                                        Data_ITEMBARCODE.Itembarcode_priceNet,
                                                                        Data_ITEMBARCODE.Itembarcode_weight_SupercheapPrice,
                                                                        Data_ITEMBARCODE.Itembarcode_UNITID,
                                                                        Data_ITEMBARCODE.Itembarcode_SPC_SalesPriceType, ""))
                {
                    DialogResult dr = MNPC_ShopDocumentDetail.ShowDialog();
                    if (dr == DialogResult.Yes)
                    {
                        this.Data_ITEMBARCODE.Itembarcode_weight_SupercheapPrice = MNPC_ShopDocumentDetail.sWeight;
                        this.Data_ITEMBARCODE.Itembarcode_PriceUnit_Branch = MNPC_ShopDocumentDetail.sPrice;
                        this.Data_ITEMBARCODE.Itembarcode_priceNet = MNPC_ShopDocumentDetail.sNet;

                        ArrayList sqlIn = new ArrayList();
                        if (RadGridView_Show.Rows.Count == 0 && radLabel_Docno.Text.Substring(10, 6) == "000000")
                        {
                            using (ShowDataDGV showData = new ShowDataDGV())
                            {
                                showData.dtData = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("34", " AND STAUSE = '1' ", "", "1");
                                DialogResult DrshowData = showData.ShowDialog();
                                if (DrshowData == DialogResult.Yes)
                                {
                                    radLabel_Type.Text = showData.pDesc;
                                    TypeProduct = showData.pID;
                                }
                                else
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุประเภทในการคืนสินค้าทุกครั้ง !.");
                                    return;
                                }
                            }

                            string bchID = RadDropDownList_Branch.SelectedItem[1].ToString().Split('-')[0];
                            string bchName = RadDropDownList_Branch.SelectedItem[1].ToString().Split('-')[1];

                            MNPCDOCNO = Class.ConfigClass.GetMaxINVOICEID("MNPC", "-", "MNPC", "1");
                            MNPCDate = DateTime.Now.AddDays(0).ToString("yyyy-MM-dd");

                            sqlIn.Add(MNPC_Class.SaveHD(TypeProduct, MNPCDOCNO, bchID, bchName));
                            radLabel_Docno.Text = MNPCDOCNO;
                        }

                        sqlIn.Add(MNPC_Class.SaveDT(MNPCDOCNO, MNPC_ShopDocumentDetail.sReason, Data_ITEMBARCODE));
                        sqlIn.Add(MNPC_Class.UpdateHD("2", MNPCDOCNO, 0));

                        string StaInset = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
                        if (StaInset == "")
                        {
                            radLabel_Docno.Text = MNPCDOCNO;

                            RadGridView_Show.Rows.Add
                                (
                               RadGridView_Show.Rows.Count + 1,
                               this.Data_ITEMBARCODE.Itembarcode_ITEMID,
                               this.Data_ITEMBARCODE.Itembarcode_INVENTDIMID,
                               this.Data_ITEMBARCODE.Itembarcode_ITEMBARCODE,
                               this.Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME,
                               this.Data_ITEMBARCODE.Itembarcode_weight_SupercheapPrice.ToString("N2"),
                               "0", this.Data_ITEMBARCODE.Itembarcode_SPC_SalesPriceType,
                               this.Data_ITEMBARCODE.Itembarcode_QTY.ToString("N2"),
                               this.Data_ITEMBARCODE.Itembarcode_UNITID,
                               this.Data_ITEMBARCODE.Itembarcode_PriceUnit_Branch.ToString("N2"),
                               this.Data_ITEMBARCODE.Itembarcode_priceNet.ToString("N2"),
                               MNPC_ShopDocumentDetail.sReasonName,
                               MNPC_ShopDocumentDetail.sReason,
                               this.Data_ITEMBARCODE.Itembarcode_DESCRIPTION,
                               this.Data_ITEMBARCODE.Itembarcode_DIMENSION,
                               this.Data_ITEMBARCODE.Itembarcode_SPC_ITEMACTIVE
                               );

                            if (RadGridView_Show.Rows.Count == 1) ClearData("SAVE");
                            radTextBox_Barcode.SelectAll();
                            radTextBox_Barcode.Focus();
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีสามารถบันทึกรายการสินค้าใหม่ได้" + System.Environment.NewLine + StaInset);
                            ClearData("");
                            return;
                        }

                    }
                }
                this.Cursor = Cursors.Default;
            }
        }
        //print Docno
        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.RowCount < 1)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีเอกสาร ไม่สามารถพิมพ์เอกสารได้");
                return;
            }

            if (radLabel_Box.Text != "-") BoxCount = int.Parse(radLabel_Box.Text);

            PrintDocument(BoxCount);
        }
        //print Tag
        void PrintDocument(int BoxCount)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการพิมพ์ลัง ด้วยหรือไม่ " + Environment.NewLine +
                @"พิมพ์ลัง+เอกสาร = YES" + Environment.NewLine +
                @"พิมพ์เอกสาร    = NO") == DialogResult.Yes)
            {
                DialogResult dr = printDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                    printDocument1.Print();
                    for (int i = 0; i < BoxCount; i++)
                    {
                        BoxSeq = i + 1;
                        printDocument2.PrinterSettings = printDialog1.PrinterSettings;
                        printDocument2.Print();
                    }
                }
            }
            else
            {
                DialogResult dr = printDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                    printDocument1.Print();

                }
            }
        }
        //pdt
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
        //cxcel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("คืนสินค้า " + radLabel_Docno.Text, RadGridView_Show, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //แก้ไขจำนวน
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (RadButton_Apv.Enabled == false || RadButton_Cancel.Enabled == false) return;

            if ((e.ColumnIndex == 6 && _pType == "SUPC") || (e.ColumnIndex == 5 && _pType == "SHOP"))
            {
                Double Qty = 0;

                if (_pType == "SUPC")
                {
                    if (Double.Parse(RadGridView_Show.CurrentRow.Cells["QtyOrderEdit"].Value.ToString()) != 0)
                        Qty = Double.Parse(RadGridView_Show.CurrentRow.Cells["QtyOrderEdit"].Value.ToString());
                    else Qty = Double.Parse(RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString());

                }
                else
                {
                    if (Double.Parse(RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString()) != 0)
                        Qty = Double.Parse(RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString());
                }

                ArrayList sqlIn = new ArrayList();

                string pricenet = RadGridView_Show.CurrentRow.Cells["PRICEUNIT"].Value.ToString();
                if (RadGridView_Show.CurrentRow.Cells["SPC_SALESPRICETYPE"].Value.ToString() == "5") pricenet = RadGridView_Show.CurrentRow.Cells["NET"].Value.ToString();

                using (MNPC_InputDetail mNPC_ShopDocumentDetail = new MNPC_InputDetail(
                            RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                            RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                            Double.Parse(pricenet),
                            Qty,
                            RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString(),
                            RadGridView_Show.CurrentRow.Cells["SPC_SALESPRICETYPE"].Value.ToString(),
                            RadGridView_Show.CurrentRow.Cells["REMARK_ID"].Value.ToString()))
                {
                    DialogResult dr = mNPC_ShopDocumentDetail.ShowDialog();
                    if (dr == DialogResult.Yes)
                    {
                        sqlIn.Add(MNPC_Class.UpdateDT(_pType, MNPCDOCNO,
                                int.Parse(RadGridView_Show.CurrentRow.Cells["SEQNO"].Value.ToString()),
                                mNPC_ShopDocumentDetail.sWeight, mNPC_ShopDocumentDetail.sReason, mNPC_ShopDocumentDetail.sPrice));

                        if (RadGridView_Show.CurrentRow.Cells["STA_BARCODEDISCOUNT"].Value.ToString() != "0")
                        {
                            string barcode = RadGridView_Show.CurrentRow.Cells["STA_BARCODEDISCOUNT"].Value.ToString();
                            if (RadGridView_Show.CurrentRow.Cells["ITEMBARCODE_DISDIS"].Value.ToString() != "0")
                            {
                                barcode = RadGridView_Show.CurrentRow.Cells["ITEMBARCODE_DISDIS"].Value.ToString();
                            }
                            sqlIn.Add($@"
                                UPDATE	SHOP_BARCODEDISCOUNT	
                                SET	    QTY = '{ mNPC_ShopDocumentDetail.sWeight}',PRICE = {mNPC_ShopDocumentDetail.sPrice} * { mNPC_ShopDocumentDetail.sWeight} 
                                WHERE	ITEMBARCODE_DIS = '{barcode}' ");
                        }
                        //
                        sqlIn.Add(MNPC_Class.UpdateHD("2", MNPCDOCNO, 0));

                        string resualt = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
                        if (resualt != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(resualt); return;
                        }

                        if (mNPC_ShopDocumentDetail.sWeight == 0) RadGridView_Show.Rows.RemoveAt(e.RowIndex);
                        else
                        {
                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = mNPC_ShopDocumentDetail.sWeight;
                            RadGridView_Show.CurrentRow.Cells["PRICEUNIT"].Value = mNPC_ShopDocumentDetail.sPrice;
                            RadGridView_Show.CurrentRow.Cells["NET"].Value = string.Format("{0:0.00}", mNPC_ShopDocumentDetail.sNet);
                            RadGridView_Show.CurrentRow.Cells["REMARK_ID"].Value = mNPC_ShopDocumentDetail.sReason;
                            RadGridView_Show.CurrentRow.Cells["REMARK_NAME"].Value = mNPC_ShopDocumentDetail.sReasonName;
                        }
                    }
                }
            }
        }
        //ยกเลิกเอกสาร
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยัน ยกเลิกเอกสารคืนสินค้า") == DialogResult.No) return;

            string resualt = ConnectionClass.ExecuteSQL_Main(MNPC_Class.UpdateHD("3", radLabel_Docno.Text, 0));
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "") ClearData("CENCEL"); ;

        }
        //ค้นหาเอกสาร
        private void RadLabel_Docno_Click(object sender, EventArgs e)
        {
            using (MNPC_SearchDoc searchDocument = new MNPC_SearchDoc())
            {
                DialogResult dr = searchDocument.ShowDialog();
                if (dr == DialogResult.Yes) SetGridAndData(MNPC_Class.GetPCDetailHD(searchDocument.MNPCDocno));
            }
        }
        //print doc
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            String Docno = MNPCDOCNO.Substring(2, 14);
            barcode.Data = Docno;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            e.Graphics.DrawString("สำหรับสาขา", SystemClass.SetFont12, Brushes.Black, 60, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_Branch.SelectedItem[1].ToString(), SystemClass.SetFont12, Brushes.Black, 60, Y);
            Y += 25;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 80;
            e.Graphics.DrawString("เอกสารส่งคืน " + radLabel_Type.Text, SystemClass.SetFont12, Brushes.Black, 30, Y);
            Y += 30;
            e.Graphics.DrawString("พิมพ์ " + DateTime.Now, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            int iR = 1;
            for (int r = 0; r < RadGridView_Show.RowCount; r++)
            {
                Double QtyOrder = Double.Parse(RadGridView_Show.Rows[r].Cells["QtyOrder"].Value.ToString());
                if (QtyOrder > 0)
                {
                    Y += 15;
                    e.Graphics.DrawString(iR + @" " + RadGridView_Show.Rows[r].Cells["SPC_ITEMNAME"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString(RadGridView_Show.Rows[r].Cells["ITEMBARCODE"].Value.ToString() + @"  " +
                         RadGridView_Show.Rows[r].Cells["QtyOrder"].Value.ToString() + @"  " +
                         RadGridView_Show.Rows[r].Cells["UNITID"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString(RadGridView_Show.Rows[r].Cells["DEPT"].Value.ToString() + @" " +
                         RadGridView_Show.Rows[r].Cells["DESCRIPTION"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("เหตุผล : " + RadGridView_Show.Rows[r].Cells["REMARK_NAME"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 10;
                    iR++;
                }
            }
            e.Graphics.DrawString("________________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ทั้งหมด :  " + RadGridView_Show.RowCount + @"  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 15;
            e.Graphics.DrawString("________________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้พิมพ์ " + SystemClass.SystemUserID_M, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("________________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
        }
        //print box
        private void PrintDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //string Docno = MNPCDOCNO.Substring(4, 12);
            string Box = MNPCDOCNO.Substring(4, 12) + BoxSeq.ToString();
            barcode.Data = Box;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            e.Graphics.DrawString("คืนสินค้าสำหรับ CN", SystemClass.SetFont12, Brushes.Black, 45, Y);
            Y += 20;
            e.Graphics.DrawString(MNPCDOCNO, SystemClass.SetFont12, Brushes.Black, 40, Y);
            Y += 20;
            e.Graphics.DrawString(RadDropDownList_Branch.SelectedItem[1].ToString(), SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("จำนวน " + BoxCount + @"/" + BoxSeq, SystemClass.printFont, Brushes.Black, 80, Y);
            Y += 20;
            e.Graphics.DrawString(Box, SystemClass.printFont, Brushes.Black, 65, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 15, Y);
            Y += 70;
            e.Graphics.DrawString("พิมพ์ " + DateTime.Now, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 20;
            e.Graphics.DrawString("________________________________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
        }
    }
}

