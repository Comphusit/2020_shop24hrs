﻿//CheckOK
using System;
using System.Windows.Forms;


namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    public partial class MNPC_SearchDoc : Telerik.WinControls.UI.RadForm
    {

        public string MNPCDocno;
        public MNPC_SearchDoc()
        {
            InitializeComponent();
        }
        void ClearDefault(String Type)
        {
            if (Type == "BARCODE")
            {
                radioButton_Barcode.Checked = true;
                radTextBox_Barcode.Enabled = true;

                radioButton_Docno.Checked = false;
                radTextBox_yy.Text = DateTime.Today.ToString("yy");
                radTextBox_yy.Enabled = false;
                radTextBox_MM.Text = DateTime.Today.ToString("MM");
                radTextBox_MM.Enabled = false;
                radTextBox_dd.Text = DateTime.Today.ToString("dd");
                radTextBox_dd.Enabled = false;
                radTextBox_Docno.Enabled = false;

                radTextBox_Barcode.TabIndex = 0;
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
            }
            else if (Type == "DOCNO")
            {
                radioButton_Barcode.Checked = false;
                radTextBox_Barcode.Enabled = false;

                radioButton_Docno.Checked = true;
                radTextBox_yy.Text = DateTime.Today.ToString("yy");
                radTextBox_yy.Enabled = true;
                radTextBox_MM.Text = DateTime.Today.ToString("MM");
                radTextBox_MM.Enabled = true;
                radTextBox_dd.Text = DateTime.Today.ToString("dd");
                radTextBox_dd.Enabled = true;
                radTextBox_Docno.Enabled = true;

                radTextBox_dd.SelectAll();
                radTextBox_dd.Focus();
            }

        }
        //load
        private void MNPC_SearchDoc_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            ClearDefault("DOCNO");
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radioButton_Barcode.Checked == true) MNPCDocno = "MNPC" + radTextBox_Barcode.Text;

            if (radioButton_Docno.Checked == true) MNPCDocno = "MNPC" + radTextBox_yy.Text + radTextBox_MM.Text + radTextBox_dd.Text + MNPC_Class.CheckDocno(radTextBox_Docno.Text);

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) MNPCDocno = "MNPC" + radTextBox_Barcode.Text;
        }


        private void RadioButton_Barcode_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Barcode.Checked == true) ClearDefault("BARCODE");
            
        }

        private void RadioButton_Docno_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Docno.Checked == true) ClearDefault("DOCNO");
       
        }

        private void RadTextBox_Docno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MNPCDocno = "MNPC" + radTextBox_yy.Text + radTextBox_MM.Text + radTextBox_dd.Text + MNPC_Class.CheckDocno(radTextBox_Docno.Text);
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
            else if (e.KeyCode == Keys.Left)
            {
                radTextBox_dd.SelectAll();
                radTextBox_dd.Focus();
            }
        }
        private void RadTextBox_yy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Enter)
            {
                radTextBox_MM.SelectAll();
                radTextBox_MM.Focus();
            }

        }

        private void RadTextBox_MM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Enter)
            {
                radTextBox_dd.SelectAll();
                radTextBox_dd.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                radTextBox_yy.SelectAll();
                radTextBox_yy.Focus();
            }
        }

        private void RadTextBox_dd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Enter)
            {
                radTextBox_Docno.SelectAll();
                radTextBox_Docno.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                radTextBox_MM.SelectAll();
                radTextBox_MM.Focus();
            }
        }
    }
}
