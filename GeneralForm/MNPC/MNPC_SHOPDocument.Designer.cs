﻿namespace PC_Shop24Hrs.GeneralForm.MNPC
{ 
    public partial class MNPC_SHOPDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNPC_SHOPDocument));
            this.RadButton_Apv = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_Find = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_DateUp = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmplUp = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_StatusBill = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radLabel_Type = new Telerik.WinControls.UI.RadLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radLabel_Box = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument2 = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Apv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StatusBill)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Type)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadButton_Apv
            // 
            this.RadButton_Apv.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Apv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Apv.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Apv.Location = new System.Drawing.Point(13, 549);
            this.RadButton_Apv.Name = "RadButton_Apv";
            this.RadButton_Apv.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Apv.TabIndex = 2;
            this.RadButton_Apv.Text = "ยืนยัน";
            this.RadButton_Apv.ThemeName = "Fluent";
            this.RadButton_Apv.Click += new System.EventHandler(this.RadButton_Apv_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).Text = "ยืนยัน";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(13, 587);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Cancel.TabIndex = 3;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radTextBox_Barcode);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Apv);
            this.panel1.Controls.Add(this.RadButton_Cancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(726, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(204, 628);
            this.panel1.TabIndex = 5;
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_Branch.BackColor = System.Drawing.Color.White;
            this.RadDropDownList_Branch.DropDownAnimationEnabled = false;
            this.RadDropDownList_Branch.DropDownHeight = 124;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(7, 66);
            this.RadDropDownList_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            // 
            // 
            // 
            this.RadDropDownList_Branch.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 25);
            this.RadDropDownList_Branch.TabIndex = 19;
            this.RadDropDownList_Branch.Text = "สาขา";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.RadDropDownList_Branch.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.RadDropDownList_Branch.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "สาขา";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(8, 42);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(39, 19);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "สาขา";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(8, 128);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(8, 103);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(162, 19);
            this.radLabel2.TabIndex = 18;
            this.radLabel2.Text = "บาร์โค้ด [Enter] ชื่อ [F4]";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.RadButtonElement_Find,
            this.commandBarSeparator3,
            this.radButtonElement_excel,
            this.commandBarSeparator2,
            this.radButtonElement_Print,
            this.commandBarSeparator5,
            this.RadButtonElement_pdt,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(202, 36);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่มเอกสาร";
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_Find
            // 
            this.RadButtonElement_Find.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_Find.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButtonElement_Find.Name = "RadButtonElement_Find";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_Find, false);
            this.RadButtonElement_Find.Text = "radButtonElement1";
            this.RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            this.RadButtonElement_Find.Click += new System.EventHandler(this.RadButtonElement_Find_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.ToolTipText = "Export To Excel";
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Print
            // 
            this.radButtonElement_Print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_Print.Name = "radButtonElement_Print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Print, false);
            this.radButtonElement_Print.Text = "";
            this.radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร";
            this.radButtonElement_Print.Click += new System.EventHandler(this.RadButtonElement_Print_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(933, 634);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(717, 628);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.66667F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.33333F));
            this.tableLayoutPanel4.Controls.Add(this.radLabel6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radLabel_DateUp, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radLabel4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radLabel_EmplUp, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 601);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(711, 24);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.ForeColor = System.Drawing.Color.Black;
            this.radLabel6.Location = new System.Drawing.Point(308, 3);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(84, 18);
            this.radLabel6.TabIndex = 57;
            this.radLabel6.Text = "<html>วันที่แก้ไขล่าสุด</html>";
            // 
            // radLabel_DateUp
            // 
            this.radLabel_DateUp.AutoSize = false;
            this.radLabel_DateUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_DateUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_DateUp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DateUp.ForeColor = System.Drawing.Color.Black;
            this.radLabel_DateUp.Location = new System.Drawing.Point(398, 3);
            this.radLabel_DateUp.Name = "radLabel_DateUp";
            this.radLabel_DateUp.Size = new System.Drawing.Size(310, 18);
            this.radLabel_DateUp.TabIndex = 56;
            this.radLabel_DateUp.Text = "<html></html>";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(3, 3);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(74, 18);
            this.radLabel4.TabIndex = 55;
            this.radLabel4.Text = "<html><p>แก้ไขล่าสุด</p><p></p></html>";
            // 
            // radLabel_EmplUp
            // 
            this.radLabel_EmplUp.AutoSize = false;
            this.radLabel_EmplUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_EmplUp.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_EmplUp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_EmplUp.ForeColor = System.Drawing.Color.Black;
            this.radLabel_EmplUp.Location = new System.Drawing.Point(83, 3);
            this.radLabel_EmplUp.Name = "radLabel_EmplUp";
            this.radLabel_EmplUp.Size = new System.Drawing.Size(219, 18);
            this.radLabel_EmplUp.TabIndex = 54;
            this.radLabel_EmplUp.Text = "<html></html>";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 38);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(711, 557);
            this.RadGridView_Show.TabIndex = 4;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_StatusBill, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel3, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Docno, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(711, 29);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // radLabel_StatusBill
            // 
            this.radLabel_StatusBill.Dock = System.Windows.Forms.DockStyle.Right;
            this.radLabel_StatusBill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_StatusBill.ForeColor = System.Drawing.Color.Black;
            this.radLabel_StatusBill.Location = new System.Drawing.Point(639, 3);
            this.radLabel_StatusBill.Name = "radLabel_StatusBill";
            this.radLabel_StatusBill.Size = new System.Drawing.Size(69, 19);
            this.radLabel_StatusBill.TabIndex = 22;
            this.radLabel_StatusBill.Text = "สถานะบิล ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radLabel_Type);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(173, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(294, 23);
            this.panel2.TabIndex = 20;
            // 
            // radLabel_Type
            // 
            this.radLabel_Type.Dock = System.Windows.Forms.DockStyle.Left;
            this.radLabel_Type.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Type.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Type.Location = new System.Drawing.Point(0, 0);
            this.radLabel_Type.Name = "radLabel_Type";
            this.radLabel_Type.Size = new System.Drawing.Size(50, 23);
            this.radLabel_Type.TabIndex = 21;
            this.radLabel_Type.Text = "ของสด";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radLabel_Box);
            this.panel3.Controls.Add(this.radLabel3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(473, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(104, 23);
            this.panel3.TabIndex = 21;
            // 
            // radLabel_Box
            // 
            this.radLabel_Box.Dock = System.Windows.Forms.DockStyle.Right;
            this.radLabel_Box.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Box.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Box.Location = new System.Drawing.Point(29, 0);
            this.radLabel_Box.Name = "radLabel_Box";
            this.radLabel_Box.Size = new System.Drawing.Size(48, 23);
            this.radLabel_Box.TabIndex = 23;
            this.radLabel_Box.Text = "จำนวน";
            // 
            // radLabel3
            // 
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.ForeColor = System.Drawing.Color.Blue;
            this.radLabel3.Location = new System.Drawing.Point(77, 0);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(27, 23);
            this.radLabel3.TabIndex = 22;
            this.radLabel3.Text = " ลัง";
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.Dock = System.Windows.Forms.DockStyle.Left;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(3, 3);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(149, 23);
            this.radLabel_Docno.TabIndex = 19;
            this.radLabel_Docno.Text = "MNPC200709000001";
            this.radLabel_Docno.Click += new System.EventHandler(this.RadLabel_Docno_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument2
            // 
            this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument2_PrintPage);
            // 
            // MNPC_SHOPDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MNPC_SHOPDocument";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "คืนสินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MNPC_Document_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Apv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StatusBill)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Type)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton RadButton_Apv;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_Find;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Print;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadLabel radLabel_StatusBill;
        private Telerik.WinControls.UI.RadLabel radLabel_Type;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private Telerik.WinControls.UI.RadLabel radLabel_Box;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel_DateUp;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplUp;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
    }
}
 