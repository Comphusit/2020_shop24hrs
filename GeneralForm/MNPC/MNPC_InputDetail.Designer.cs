﻿namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    partial class MNPC_InputDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNPC_InputDetail));
            this.RadGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.RadDropDownList_ReasonPC = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox_QTY = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Price = new Telerik.WinControls.UI.RadTextBox();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel_unit = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox1)).BeginInit();
            this.RadGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_ReasonPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_QTY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGroupBox1
            // 
            this.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox1.Controls.Add(this.RadDropDownList_ReasonPC);
            this.RadGroupBox1.Controls.Add(this.radTextBox_QTY);
            this.RadGroupBox1.Controls.Add(this.radTextBox_Price);
            this.RadGroupBox1.Controls.Add(this.RadButton_Cancel);
            this.RadGroupBox1.Controls.Add(this.RadButton_Save);
            this.RadGroupBox1.Controls.Add(this.radLabel_unit);
            this.RadGroupBox1.Controls.Add(this.radLabel_Name);
            this.RadGroupBox1.Controls.Add(this.RadLabel6);
            this.RadGroupBox1.Controls.Add(this.RadLabel5);
            this.RadGroupBox1.Controls.Add(this.RadLabel3);
            this.RadGroupBox1.Controls.Add(this.RadLabel1);
            this.RadGroupBox1.Controls.Add(this.RadLabel2);
            this.RadGroupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadGroupBox1.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            this.RadGroupBox1.HeaderText = "";
            this.RadGroupBox1.Location = new System.Drawing.Point(12, 9);
            this.RadGroupBox1.Name = "RadGroupBox1";
            this.RadGroupBox1.Size = new System.Drawing.Size(360, 245);
            this.RadGroupBox1.TabIndex = 0;
            this.RadGroupBox1.ThemeName = "VisualStudio2012Light";
            // 
            // RadDropDownList_ReasonPC
            // 
            this.RadDropDownList_ReasonPC.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_ReasonPC.BackColor = System.Drawing.Color.White;
            this.RadDropDownList_ReasonPC.DropDownAnimationEnabled = false;
            this.RadDropDownList_ReasonPC.DropDownHeight = 124;
            this.RadDropDownList_ReasonPC.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_ReasonPC.Font = new System.Drawing.Font("Tahoma", 10F);
            this.RadDropDownList_ReasonPC.Location = new System.Drawing.Point(97, 155);
            this.RadDropDownList_ReasonPC.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadDropDownList_ReasonPC.Name = "RadDropDownList_ReasonPC";
            // 
            // 
            // 
            this.RadDropDownList_ReasonPC.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.RadDropDownList_ReasonPC.Size = new System.Drawing.Size(227, 25);
            this.RadDropDownList_ReasonPC.TabIndex = 1;
            this.RadDropDownList_ReasonPC.Text = "radDropDownList1";
            this.RadDropDownList_ReasonPC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadDropDownList_ReasonPC_KeyDown);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.RadDropDownList_ReasonPC.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.RadDropDownList_ReasonPC.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "radDropDownList1";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_ReasonPC.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radTextBox_QTY
            // 
            this.radTextBox_QTY.AutoSize = false;
            this.radTextBox_QTY.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_QTY.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_QTY.Location = new System.Drawing.Point(97, 118);
            this.radTextBox_QTY.Name = "radTextBox_QTY";
            // 
            // 
            // 
            this.radTextBox_QTY.RootElement.StretchVertically = false;
            this.radTextBox_QTY.Size = new System.Drawing.Size(164, 24);
            this.radTextBox_QTY.TabIndex = 0;
            this.radTextBox_QTY.Text = "1.00";
            this.radTextBox_QTY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.radTextBox_QTY.ThemeName = "VisualStudio2012Light";
            this.radTextBox_QTY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_QTY_KeyDown);
            // 
            // radTextBox_Price
            // 
            this.radTextBox_Price.AutoSize = false;
            this.radTextBox_Price.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Price.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Price.Location = new System.Drawing.Point(99, 87);
            this.radTextBox_Price.Name = "radTextBox_Price";
            // 
            // 
            // 
            this.radTextBox_Price.RootElement.StretchVertically = false;
            this.radTextBox_Price.Size = new System.Drawing.Size(164, 24);
            this.radTextBox_Price.TabIndex = 10;
            this.radTextBox_Price.TabStop = false;
            this.radTextBox_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.radTextBox_Price.ThemeName = "VisualStudio2012Light";
            this.radTextBox_Price.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Price_KeyDown);
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(185, 199);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(110, 32);
            this.RadButton_Cancel.TabIndex = 3;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(69, 199);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(110, 32);
            this.RadButton_Save.TabIndex = 2;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_unit
            // 
            this.radLabel_unit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_unit.Location = new System.Drawing.Point(284, 125);
            this.radLabel_unit.Name = "radLabel_unit";
            this.radLabel_unit.Size = new System.Drawing.Size(13, 19);
            this.radLabel_unit.TabIndex = 43;
            this.radLabel_unit.Text = "-";
            this.radLabel_unit.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_unit.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(99, 21);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(225, 60);
            this.radLabel_Name.TabIndex = 43;
            this.radLabel_Name.Text = "-";
            this.radLabel_Name.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_Name.ThemeName = "VisualStudio2012Light";
            // 
            // RadLabel6
            // 
            this.RadLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel6.Location = new System.Drawing.Point(284, 87);
            this.RadLabel6.Name = "RadLabel6";
            this.RadLabel6.Size = new System.Drawing.Size(33, 19);
            this.RadLabel6.TabIndex = 42;
            this.RadLabel6.Text = "บาท";
            this.RadLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.RadLabel6.ThemeName = "VisualStudio2012Light";
            // 
            // RadLabel5
            // 
            this.RadLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel5.Location = new System.Drawing.Point(15, 87);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(37, 19);
            this.RadLabel5.TabIndex = 2;
            this.RadLabel5.Text = "ราคา";
            this.RadLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.RadLabel5.ThemeName = "VisualStudio2012Light";
            // 
            // RadLabel3
            // 
            this.RadLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel3.Location = new System.Drawing.Point(15, 120);
            this.RadLabel3.Name = "RadLabel3";
            this.RadLabel3.Size = new System.Drawing.Size(48, 19);
            this.RadLabel3.TabIndex = 29;
            this.RadLabel3.Text = "จำนวน";
            this.RadLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.RadLabel3.ThemeName = "VisualStudio2012Light";
            // 
            // RadLabel1
            // 
            this.RadLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel1.Location = new System.Drawing.Point(12, 159);
            this.RadLabel1.Name = "RadLabel1";
            this.RadLabel1.Size = new System.Drawing.Size(67, 19);
            this.RadLabel1.TabIndex = 27;
            this.RadLabel1.Text = "เหตุผลคืน";
            this.RadLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.RadLabel1.ThemeName = "VisualStudio2012Light";
            // 
            // RadLabel2
            // 
            this.RadLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel2.Location = new System.Drawing.Point(15, 21);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(59, 19);
            this.RadLabel2.TabIndex = 25;
            this.RadLabel2.Text = "ชื่อสินค้า";
            this.RadLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.RadLabel2.ThemeName = "VisualStudio2012Light";
            // 
            // MNPC_ShopDocumentDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.RadButton_Cancel;
            this.ClientSize = new System.Drawing.Size(385, 266);
            this.Controls.Add(this.RadGroupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MNPC_ShopDocumentDetail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ข้อมูลสินค้าคืน";
            this.Load += new System.EventHandler(this.MNPC_InputDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox1)).EndInit();
            this.RadGroupBox1.ResumeLayout(false);
            this.RadGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_ReasonPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_QTY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal Telerik.WinControls.UI.RadGroupBox RadGroupBox1;
        internal Telerik.WinControls.UI.RadLabel RadLabel6;
        internal Telerik.WinControls.UI.RadLabel RadLabel5;
        internal Telerik.WinControls.UI.RadLabel RadLabel3;
        internal Telerik.WinControls.UI.RadLabel RadLabel1;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadLabel radLabel_unit;
        internal Telerik.WinControls.UI.RadLabel radLabel_Name;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        internal Telerik.WinControls.UI.RadTextBox radTextBox_QTY;
        internal Telerik.WinControls.UI.RadTextBox radTextBox_Price;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_ReasonPC;
    }
}
