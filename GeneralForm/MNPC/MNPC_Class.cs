﻿//CheckOK
using System.Data;
using PC_Shop24Hrs.Controllers;
using System;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    public static class MNPC_Class
    {

        public static DataTable GetPCDetailHD(string DOCNO)//string BranchType, string Branch,
        {
            string sql = $@"
                SELECT	DocNo, Grand,  SHOW_NAME as typeproduct,
		                case STADOC when '3' then 'CENCEL' else case STAPRCDOC when '0' then 'SAVE'  else 
			                case STACN when '0' then 'CONF' else 'APV' end end end as StaDoc,
		                case isnull(TagNumber,0) when '' then '0' else TagNumber end as Box,
		                branch,branchname,convert(varchar,DocDate,23) as DocDate,
		                WhoUp + ' ' + WhoNameUp as WhoName,Convert(Varchar,DateUp,23) + ' ' + TimeUp as DateUp
                FROM	SHOP_MNPC_HD WITH (NOLOCK)	
		                INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_MNPC_HD.TYPEPRODUCT=SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND TYPE_CONFIG = '34'
                where	DocNo = '{DOCNO}'  
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static string CheckDocno(string Docno)
        {
            return String.Format("{0:000000}", Int16.Parse(Docno));
        }

        #region"FIND PC"
        //JEEDS OK Get Data HD
        public static DataTable GetPc_HD(string Branch, string Status, string DateBegin, string DateEnd, string Docno)
        {
            string docnoCondition = "";
            if (Docno != "") docnoCondition = $@"AND SHOP_MNPC_HD.DOCNO = '{Docno}' ";
            string bchCondition = "";
            if (Branch != "") bchCondition = $@"AND BRANCH = '{Branch}' ";

            string staCondition = "";
            switch (Status)
            {
                case "SAVE":
                    staCondition = @" AND SHOP_MNPC_HD.StaPrcDoc = '1' AND SHOP_MNPC_HD.StaDoc = '0' AND SHOP_MNPC_HD.STACN = '0' ";
                    break;
                case "CONF":
                    staCondition = @" AND SHOP_MNPC_HD.StaPrcDoc = '1' AND SHOP_MNPC_HD.StaDoc = '1' AND SHOP_MNPC_HD.STACN = '0' ";
                    break;
                case "APV":
                    staCondition = @" AND SHOP_MNPC_HD.StaPrcDoc = '1' AND SHOP_MNPC_HD.StaDoc = '1' AND SHOP_MNPC_HD.STACN = '1' ";
                    break;
                case "CENCEL":
                    staCondition = @" AND SHOP_MNPC_HD.StaDoc = '3' ";
                    break;
            }

            string Sql = $@"
                SELECT   DOCNO,CONVERT(VARCHAR,DOCDATE,23) AS DOCDATE,
		                BRANCH AS BRANCH_ID,BRANCH_NAME,  
		                SHOW_NAME as TypeOrder,
		                CASE STADOC WHEN '1' THEN 'X' ELSE '/' END AS STADOC,
		                CASE STAPRCDOC WHEN '1' THEN '/' ELSE 'X' END AS STAPRCDOC,
		                CASE STACN WHEN '1' THEN '/' ELSE 'X' END AS STACN,
		                WhoIn+ ' '+whoNameIn as WHOIDINS,SHOP_MNPC_HD.REMARK AS RMK
                FROM	SHOP_MNPC_HD WITH (NOLOCK) 
		                INNER JOIN Shop_Branch WITH (NOLOCK) ON SHOP_MNPC_HD.Branch = Shop_Branch.BRANCH_ID 
		                INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_MNPC_HD.TYPEPRODUCT=SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                WHERE   convert(varchar,DocDate,23) BETWEEN '{DateBegin}' AND '{DateEnd}' and TYPE_CONFIG = '34'
                        {staCondition} {docnoCondition} {bchCondition}
                order by DOCNO DESC 
            ";

            return ConnectionClass.SelectSQL_Main(Sql);
        }
        

        #endregion
         
 

        #region "Report Group_Item AND Group_Reason"

        public static DataTable GetGroupItems(string DateBegin, string DateEnd, string DeptID)//, string ReasonItems
        {
            string dptCondition = "";
            if (DeptID != "") dptCondition = $@" AND SHOP_MNPC_DT.DeptCode ='{DeptID}' ";
            string Sql = $@"
                SELECT	Barcode,ItemName,UnitID   
                FROM	SHOP_MNPC_DT WITH (NOLOCK) 
                        INNER JOIN SHOP_MNPC_HD WITH (NOLOCK)  ON SHOP_MNPC_DT.Docno = SHOP_MNPC_HD.DocNo  
                WHERE   DOCDATE BETWEEN '{DateBegin}' AND '{DateEnd}'   
                        AND SHOP_MNPC_HD.StaDoc ='1' {dptCondition}
                GROUP BY Barcode,ItemName,UnitID  
                ORDER BY ItemName 
                ";
            return ConnectionClass.SelectSQL_Main(Sql);
        }
        //JEEDS OK 
        public static DataTable GetItemsQty(string dateBegin, string dateEnd, string DeptItems)// string ReasonItems
        {
            string dptCondition = "";
            if (DeptItems != "") dptCondition = $@" AND DeptCode = '{DeptItems}' ";

            //string reasionCondition = "";
            //if (ReasonItems != "") reasionCondition = $@" AND CNReason = '{ReasonItems}' ";
            //ItemName,UnitID,
            string sql = $@"
            SELECT	BRANCH,Barcode,
		            FORMAT(SUM(QTY),'N2') AS QtyCN,FORMAT(SUM(PriceNet),'N2') AS NetCN 
            FROM	SHOP_MNPC_HD WITH (NOLOCK)  
		            INNER JOIN SHOP_MNPC_DT WITH (NOLOCK)     ON SHOP_MNPC_HD.Docno = SHOP_MNPC_DT.DocNo  
            WHERE	DOCDATE BETWEEN '{dateBegin}' AND '{dateEnd}'  
		            AND StaDoc ='1' 
                    {dptCondition}  
            GROUP BY BRANCH,Barcode
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #endregion

        
        #region "Report MNPC TYPE"
        //รายงานคืนสินค้าตามประเภทการคืน JEEDS OK
        public static DataTable GetSumDocByType(string DateBegin, string DateEnd, string bchID)
        {
            string bchCondition = "";
            if (bchID != "") bchCondition = $@" AND BRANCH_ID = '{bchID}' ";
            string sql = $@"
                DECLARE @DateBegin NVARCHAR(20) = '{DateBegin}';
                DECLARE @DateEnd NVARCHAR(20) = '{DateEnd}'

                SELECT	BRANCH_ID,BRANCH_NAME ,
		                isnull(DocnoFresh,0)+isnull(DocnoPack,0) +isnull(DocnoBakery,0) as Docno,
		                isnull(TagFresh,0)+isnull(TagPack,0) +isnull(TagBakery,0) as Tag,
		                isnull(DocnoFresh,0) as DocnoFresh, isnull(TagFresh,0) as TagFresh,
		                isnull(DocnoPack,0) as DocnoPack, isnull(TagPack,0) as TagPack, 
		                isnull(DocnoBakery,0) as DocnoBakery,isnull(TagBakery,0) as TagBakery

                FROM	SHOP_BRANCH WITH (NOLOCK)
		                LEFT OUTER JOIN (
				                SELECT	branch,count(Docno) AS DocnoFresh,sum(TagNumber) AS TagFresh 
				                FROM	SHOP_MNPC_HD (NOLOCK)
				                WHERE	DocDate BETWEEN @DateBegin and @DateEnd
						                AND StaDoc='1' AND StaPrcDoc='1' AND TYPEPRODUCT='1'
				                GROUP BY branch )TypeFresh ON shop_branch.BRANCH_ID=TypeFresh.branch
		                LEFT OUTER JOIN (
				                SELECT	branch,count(Docno) as DocnoPack ,sum(TagNumber) as TagPack  
				                FROM	SHOP_MNPC_HD (NOLOCK)
				                WHERE	DocDate  between @DateBegin and @DateEnd
						                and StaDoc='1' and StaPrcDoc='1' and TYPEPRODUCT='0'
				                GROUP BY branch )TypePack ON shop_branch.BRANCH_ID=TypePack.branch
		                LEFT OUTER JOIN (
				                SELECT	branch,count(Docno) as DocnoBakery ,sum(TagNumber) as TagBakery 
				                FROM	SHOP_MNPC_HD (NOLOCK)
				                WHERE	DocDate  between @DateBegin and @DateEnd
						                and StaDoc='1' and StaPrcDoc='1' and TYPEPRODUCT='2'
				                GROUP BY branch 
			                )TypeBakery ON shop_branch.BRANCH_ID=TypeBakery.branch 

                WHERE	BRANCH_STA='1' and BRANCH_STAOPEN = '1' {bchCondition}

                ORDER BY BRANCH_ID
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #endregion


        public static string SaveHD(string typeProduct, string docno, string bchID, string bchName)
        {
            string sqlHD = $@"
            INSERT INTO SHOP_MNPC_HD (
                DocNo, UserCode, Branch, BranchName, 
                TYPEPRODUCT,WhoIn, WhoNameIn,SPCName,ProgramUse
            )values(
                '{docno}','{SystemClass.SystemUserID_M}', '{bchID}', '{bchName}',   
                '{typeProduct}','{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}','{SystemClass.SystemPcName}','PC') ";

            return sqlHD;
        }

        public static string SaveDT(string docno, string reason, Data_ITEMBARCODE Data_ITEMBARCODE)
        {
            string sqlDT = $@"
                INSERT INTO SHOP_MNPC_DT 
                            (DocNo,SeqNo,ItemID,Dimid,
                            Barcode,ItemName,StaItem,
                            DeptCode,Qty,UnitID,
                            Factor,QtySum,Price,
                            PriceNet,
                            StatusShop,WhoShop,WhoIn,SPC_SALESPRICETYPE,CNReason,WhoNameShop,WhoNameIN,ProgramUse ) 
                VALUES      ('{docno}',(SELECT COUNT(*)+1 FROM  [SHOP_MNPC_DT] WHERE	[DocNo] = '{docno}'),'{Data_ITEMBARCODE.Itembarcode_ITEMID}','{Data_ITEMBARCODE.Itembarcode_INVENTDIMID}',
                            '{Data_ITEMBARCODE.Itembarcode_ITEMBARCODE}','{Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME.Replace("'", "").Replace("{", "").Replace("}", "")}','1',
                            '{Data_ITEMBARCODE.Itembarcode_DIMENSION}','{Data_ITEMBARCODE.Itembarcode_weight_SupercheapPrice}','{Data_ITEMBARCODE.Itembarcode_UNITID}',
                            '{Data_ITEMBARCODE.Itembarcode_QTY}','{Data_ITEMBARCODE.Itembarcode_weight_SupercheapPrice}','{Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP3}',
                            '{Data_ITEMBARCODE.Itembarcode_weight_SupercheapPrice * Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP3}',
                            '0','{SystemClass.SystemUserID_M}','{SystemClass.SystemUserID_M}','{Data_ITEMBARCODE.Itembarcode_SPC_SalesPriceType}','{reason}','{SystemClass.SystemUserName}','{SystemClass.SystemUserName}','PC');
                            ";
            return sqlDT;
        }
        public static string UpdateDT(string pTypeOpen, string docno, int iRows, double qty, string reason, double price)
        {
            string UpdateCancleItem = "";
            if (qty == 0) UpdateCancleItem = " ,StaItem = '3' ";
            string UpdateSupc = "";
            if (pTypeOpen == "SUPC") UpdateSupc = $@" ,CheckPurchase = {qty},StatusPurchase = '1' ";

            return $@"
                        UPDATE  SHOP_MNPC_DT
                        SET     Qty = '{qty}',QtySum = Factor * {qty},
                                Price = {price},
                                PriceNet = {price} * {qty},CNReason = '{reason}',
                                DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),WhoUp = '{SystemClass.SystemUserID_M}',WhoNameUp = '{SystemClass.SystemUserName}'
                                {UpdateCancleItem} {UpdateSupc}
                        WHERE   DOCNO = '{docno}' AND  SEQNO = '{iRows}'
                    ";

            
        }

        public static string UpdateHD(string status, string docno, int sumTag)
        {
            switch (status)
            {
                case "0"://ยืนยันการคืน SHOP
                    return $@"
                        UPDATE  SHOP_MNPC_HD
                        SET     Grand = (SELECT SUM(pricenet) FROM  [SHOP_MNPC_DT] WHERE	[DocNo] = '{docno}'),
                                StaPrcDoc = '1',
                                TagNumber = '{sumTag}',
                                DateShop = convert(varchar,getdate(),23),TimeShop = convert(varchar,getdate(),24),
                                WhoShop = '{SystemClass.SystemUserID_M}',WhoNameShop = '{SystemClass.SystemUserName}',
                                DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),WhoUp = '{SystemClass.SystemUserID_M}',WhoNameUp = '{SystemClass.SystemUserName}'
                        WHERE   DocNo = '{docno}'
                        ";
                case "1"://อนุมัตเอกสาร CN- SUPC
                    return $@"
                        UPDATE  SHOP_MNPC_HD
                        SET     Grand = (SELECT SUM(pricenet) FROM  [SHOP_MNPC_DT] WHERE	[DocNo] = '{docno}'),
                                StaPrcDoc = '1', STACN = '1',MNPCAX = '1', StaDocLP = '1',
                                DateCN = convert(varchar,getdate(),23),TimeCN = convert(varchar,getdate(),24),WhoCN = '{SystemClass.SystemUserID_M}',WhoNameCN = '{SystemClass.SystemUserName}',
                                DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),WhoUp = '{SystemClass.SystemUserID_M}',WhoNameUp = '{SystemClass.SystemUserName}'
                        WHERE   DocNo = '{docno}'
                    ";
                case "2":// Update Grand
                    return $@"
                        UPDATE  SHOP_MNPC_HD
                        SET     Grand = (SELECT SUM(pricenet) FROM  [SHOP_MNPC_DT] WHERE	[DocNo] = '{docno}')
                        WHERE   DocNo = '{docno}'
                    ";
                case "3"://ยกเลิกเอกสาร
                    return $@"
                            UPDATE  SHOP_MNPC_HD
                            SET     Remark = Remark + ' [ยกเลิกเอกสารผ่าน PC]',StaDoc='3',StaPrcDoc = '0',
                                    DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),WhoUp = '{SystemClass.SystemUserID_M}',WhoNameUp = '{SystemClass.SystemUserName}'
                            WHERE    DocNo = '{docno}'
                            ";
                default:
                    return "";
            }
        }
    }
}


