﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    public partial class MNPC_InputDetail : Telerik.WinControls.UI.RadForm
    {
        readonly string _Barcode, _Itemname, _Unit, _Typebarcode;
        public string sReason, sReasonName;
        public Double sPrice, sWeight, sNet;

        public MNPC_InputDetail(string Barcode, string Itemname, Double Price, Double weight, string Unit, string Typebarcode, string Reason)
        {
            InitializeComponent();

            _Barcode = Barcode;
            _Itemname = Itemname;
            sPrice = Price;
            sWeight = weight;
            _Unit = Unit;
            _Typebarcode = Typebarcode;
            sReason = Reason;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_ReasonPC);
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
        }
        private void RadTextBox_Price_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radTextBox_QTY.Focus();
        }
        private void RadTextBox_QTY_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RadDropDownList_ReasonPC.Focus();
        }
        private void RadDropDownList_ReasonPC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RadButton_Save.Focus();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_QTY.Text.Trim().Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กรุณาระบุจำนวนสินค้าที่ต้องการคืน{Environment.NewLine}ลองใหม่อีกครั้ง");
                radTextBox_QTY.SelectAll();
                return;
            }
            if (!(Double.Parse(radTextBox_QTY.Text) > 0))
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการปรับจำนวนรายการสินค้านี้เป็น 0 ?") == DialogResult.No)
                {
                    radTextBox_QTY.SelectAll(); return;
                }
            }

            sPrice = Double.Parse(radTextBox_Price.Text);
            sWeight = Double.Parse(radTextBox_QTY.Text);
            sNet = Double.Parse(radTextBox_Price.Text) * Double.Parse(radTextBox_QTY.Text);


            sReason = RadDropDownList_ReasonPC.SelectedValue.ToString();
            sReasonName = RadDropDownList_ReasonPC.SelectedItem[1].ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        void SetDrobdownlist()
        {
            RadDropDownList_ReasonPC.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("16", " AND STAUSE = '1' ", "", "1");
            RadDropDownList_ReasonPC.DisplayMember = "SHOW_DESC";
            RadDropDownList_ReasonPC.ValueMember = "SHOW_ID";
        }
        private void MNPC_InputDetail_Load(object sender, EventArgs e)
        {
            //SetDrobdownlist(MNPC_Class.GetReasonPC("16"));
            SetDrobdownlist();
            radLabel_Name.Text = _Itemname;
            radTextBox_Price.Text = sPrice.ToString("N2"); radTextBox_Price.Enabled = false;
            radTextBox_QTY.Text = sWeight.ToString("N2"); radTextBox_QTY.Enabled = true;
            radLabel_unit.Text = _Unit;
            if (sReason != "") RadDropDownList_ReasonPC.SelectedValue = sReason;

            radTextBox_QTY.TabIndex = 0; radTextBox_QTY.Focus();

            if (_Typebarcode == "2")
            {
                radTextBox_Price.Enabled = true;
                radTextBox_Price.Text = "1.00";
                radTextBox_Price.TabIndex = 0;
                radTextBox_Price.Focus();
            }
             
        }
    }
}
