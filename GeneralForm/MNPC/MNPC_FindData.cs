﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    public partial class MNPC_FindData : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dtDataHD = new DataTable();
        readonly DataTable dtDataDT = new DataTable();

        readonly string _pType; // shop - supc 
        readonly string _pTypeReport0Find1Rpt; //  
        //0 ค้นหาเอกสาร
        //1 รายงานการคืนสินค้าแบบละเอียด

        public string pDocno;

        public MNPC_FindData(string pType, string pTypeReport0Find1Rpt)
        {
            InitializeComponent();
            _pType = pType;
            _pTypeReport0Find1Rpt = pTypeReport0Find1Rpt;
        }

        void Set_Branch()
        {
            if (_pType == "SHOP")
            {
                RadDropDownList_Branch.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Checked = true;
                RadDropDownList_Branch.Enabled = false;
            }
            else
            {
                RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll(" '1','4' ", " '1' ");
                RadCheckBox_Branch.Checked = false;
                RadDropDownList_Branch.Enabled = false;
            }

            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
        }


        //Load
        private void MNPC_FindData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButtonElement_pdt.ShowBorder = true;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Status.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-5), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TypeOrder", "ประเภท", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STAPRCDOC", "ยืนยัน", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STACN", "อนุมัติ", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STADOC", "ยกเลิก", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "หมายเหตุ", 200)));

            DatagridClass.SetCellBackClolorByExpression("STADOC", "STADOC = '/'  ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("STAPRCDOC", "STAPRCDOC = '/'  ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("STACN", "STACN = '/'  ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 80)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 70)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PRICEUNIT", "ราคา", 90)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("NET", "ราคารวม", 90)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK_NAME", "หมายเหตุการคืน", 200)));

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-5);
            radDateTimePicker_End.Value = DateTime.Now;

            Set_Branch();
            RadButton_Search_Click(sender, e);

            if (_pTypeReport0Find1Rpt == "1") RadButton_Choose.Visible = false;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }
        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }


        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            string bch = "";
            if (RadCheckBox_Branch.Checked == true) bch = RadDropDownList_Branch.SelectedValue.ToString();
            string sta = "";
            if (radCheckBox_Status.Checked == true)
            {
                if (radioButton_SAVE.Checked == true) sta = "SAVE";
                if (radioButton_CENCEL.Checked == true) sta = "CENCEL";
                if (radioButton_CONF.Checked == true) sta = "CONF";
                if (radioButton_APV.Checked == true) sta = "APV";
            }
            this.Cursor = Cursors.WaitCursor;
            RadGridView_ShowHD.DataSource = MNPC_Class.GetPc_HD(bch, sta,
                                                                radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                                radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                                ""); ;
            this.Cursor = Cursors.Default;
        }

        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            {
                if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString() == "")) return;

                if (RadGridView_ShowHD.Rows.Count == 0)
                { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }

                RadGridView_ShowDT.DataSource = Models.DptClass.MNPC_GetPc_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString()); //MNPC_Class.GetPc_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
            }
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }


        private void RadGridView_ShowHD_FilterChanged(object sender, GridViewCollectionChangedEventArgs e)
        {
            if (RadGridView_ShowHD.RowCount < 1) return;
        }

        private void RadCheckBox_Status_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Status.Checked == true)
            {
                radioButton_SAVE.Enabled = true;
                radioButton_CENCEL.Enabled = true;
                radioButton_CONF.Enabled = true;
                radioButton_APV.Enabled = true;

                radioButton_SAVE.Checked = true;
            }
            else
            {
                radioButton_SAVE.Enabled = false;
                radioButton_CENCEL.Enabled = false;
                radioButton_CONF.Enabled = false;
                radioButton_APV.Enabled = false;
            }
        }

        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true;
            else RadDropDownList_Branch.Enabled = false;
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
