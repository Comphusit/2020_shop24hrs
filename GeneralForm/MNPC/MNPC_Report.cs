﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNPC
{
    public partial class MNPC_Report : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeOpen; // shop - supc
        readonly string _pTypeReport;
        //0 รายงานการรับคืนสินค้า
        //1 รายงานการแก้ไขรายการสินค้าคืน
        //2 รายงานคืนสินค้าตามประเภทการคืน
        //3 รายงานการคืนสินค้า ตามแผนกจัดซื้อ
        //4 รายงานการคืนสินค้าตามบาร์โค๊ด
        //5 รายงานการคืนสินค้ารวมทั้งหมด
        public MNPC_Report(string pTypeOpen, string pTypeReport)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
            _pTypeReport = pTypeReport;
        }
        //Load
        private void MNPC_Report_Load(object sender, EventArgs e)
        {

            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-3), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadDropDownList_Dept.Visible = false; RadCheckBox_Dept.Visible = false;
            radioButton_Qty.Visible = false; radioButton_price.Visible = false;
            radLabel_price.Visible = false; radTextBox_Price.Visible = false;

            DataTable dtBch = new DataTable();
            switch (_pTypeOpen)
            {
                case "SHOP"://shop
                    RadCheckBox_Branch.Checked = true;
                    dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadCheckBox_Branch.Enabled = false;
                    break;
                case "SUPC": // supc
                    RadCheckBox_Branch.Checked = false;
                    dtBch = BranchClass.GetBranchAll("'1','4'", "'1'");
                    RadCheckBox_Branch.Enabled = true;
                    break;
                default:
                    break;
            }
            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            switch (_pTypeReport)
            {
                case "0"://รายงานการรับคืนสินค้า
                    radLabel_Detail.Text = "สีฟ้า >> สาขายังไม่ยืนยัน | สีเทา >> บิลยกเลิก | สีแดง >> CN ยังไม่รับ | สีขาว >> เข้า AX แล้ว";
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocNo", "เลขที่บิล", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STADOC", "สถานะ", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocDate", "วันที่", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ShopName", "ผู้สร้าง", 200)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPID", "เลขที่ส่งของ", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ShipDate", "วันที่ส่ง", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("TagNumber", "จน.ลังส่ง", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CountRecive", "จน.ลังรับ", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ShipName", "ผู้ส่ง", 200)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ShipCarID", "ทะเบียนรถ", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ShipDriverID", "พขร.", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateCN", "วันที่รับ CN", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CNName", "ผู้รับ CN.", 200)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TYPEPRODUCT", "ประเภท", 200)));

                    DatagridClass.SetCellBackClolorByExpression("STADOC", "STADOC = 'CN ไม่อนุมัติ' ", ConfigClass.SetColor_Red(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("STADOC", "STADOC = 'สาขาไม่ยืนยัน' ", ConfigClass.SetColor_SkyPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("STADOC", "STADOC = 'ยกเลิก' ", ConfigClass.SetColor_GrayPastel(), RadGridView_Show);

                    break;

                case "1"://รายงานการแก้ไขรายการสินค้าคืน
                    radLabel_Detail.Text = "สีแดง >> มีการแก้ไขจำนวน";
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocNo", "เลขที่บิล", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STADOC", "สถานะ", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocDate", "วันที่", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค๊ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemName", "ชื่อสินค้า", 300)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CheckPurchase", "จำนวนก่อนแก้", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("Qty", "จำนวนหลังแก้", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PriceNet", "ยอดรวม", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REASONNAME", "เหตุผลคืน", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 180)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 150)));

                    ExpressionFormattingObject obj12 = new ExpressionFormattingObject("MyCondition1", "CheckPurchase <> Qty ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_Show.Columns["CheckPurchase"].ConditionalFormattingObjectList.Add(obj12);
                    RadGridView_Show.Columns["Qty"].ConditionalFormattingObjectList.Add(obj12);

                    break;

                case "2"://รายงานคืนสินค้าตามประเภทการคืน
                    radLabel_Detail.Text = "สรุปการคืนสินค้าทั้งหมดตามประเภทการคืน";
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Docno", "เอกสาร", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Tag", "ลัง", 80)));

                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("DocnoFresh", "ของสด", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("TagFresh", "ลังของสด", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("DocnoPack", "ของแห้ง", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("TagPack", "ลังของแห้ง", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("DocnoBakery", "เบเกอรี่", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("TagBakery", "ลังเบเกอรี่", 100)));

                    GridViewSummaryItem summaryItem1 = new GridViewSummaryItem
                    {
                        Name = "Docno",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryItem summaryItem2 = new GridViewSummaryItem
                    {
                        Name = "Tag",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryItem summaryItem3 = new GridViewSummaryItem
                    {
                        Name = "DocnoFresh",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryItem summaryItem4 = new GridViewSummaryItem
                    {
                        Name = "TagFresh",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItem5 = new GridViewSummaryItem
                    {
                        Name = "DocnoPack",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryItem summaryItem6 = new GridViewSummaryItem
                    {
                        Name = "TagPack",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItem7 = new GridViewSummaryItem
                    {
                        Name = "DocnoBakery",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryItem summaryItem8 = new GridViewSummaryItem
                    {
                        Name = "TagBakery",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem1 = new GridViewSummaryRowItem { summaryItem1, summaryItem2, summaryItem3, summaryItem4, summaryItem5, summaryItem6, summaryItem7, summaryItem8 };
                    this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem1);
                    this.RadGridView_Show.BottomPinnedRowsMode = GridViewBottomPinnedRowsMode.Float;

                    break;
                case "3"://รายงานการคืนสินค้า ตามแผนกจัดซื้อ
                    radLabel_Detail.Text = "สีฟ้า >> สาขายังไม่ยืนยัน | สีเทา >> บิลยกเลิก | สีแดง >> CN ยังไม่รับ | สีม่วง >> สินค้าที่ลดราคาแล้ว | สีขาว >> เข้า AX แล้ว | DoubleClick รายการ >> เปิดดูออเดอร์";
                    SetDeptOpen();

                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Docno", "เลขที่บิล", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocDate", "วันที่", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StatusDoc", "สถานะ", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค๊ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemName", "ชื่อสินค้า", 250)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("Qty", "จำนวน", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PriceNet", "ยอดรวม", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REASONNAME", "เหตุผล", 200)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DPTID", "แผนก")));
                    //RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 180))); 
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUMDESC", "แผนกจัดซื้อ", 200)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "ผู้จำหน่าย", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 200)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TYPEPRODUCT", "ประเภท", 160)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BARCODEDISCOUNT", "BARCODEDISCOUNT")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));

                    DatagridClass.SetCellBackClolorByExpression("StatusDoc", "StatusDoc = 'CN ไม่อนุมัติ' ", ConfigClass.SetColor_Red(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("StatusDoc", "StatusDoc = 'สาขาไม่ยืนยัน' ", ConfigClass.SetColor_SkyPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("StatusDoc", "StatusDoc = 'ยกเลิก' ", ConfigClass.SetColor_GrayPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("StatusDoc", "BARCODEDISCOUNT = '1' ", ConfigClass.SetColor_PurplePastel(), RadGridView_Show);

                    break;
                case "4":// รายงานการคืนสินค้าตามบาร์โค๊ด
                    radLabel_Detail.Text = "สีเขียว >> จำนวนที่มากกว่า 0 | สีแดง >> จำนวนที่มากกว่าช่องที่กำหนด";
                    SetDeptOpen(); RadCheckBox_Dept.Enabled = false;
                    radLabel_price.Visible = true; radTextBox_Price.Visible = true;
                    radioButton_Qty.Visible = true; radioButton_price.Visible = true;

                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemName", "ชื่อสินค้า", 300)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 80)));

                    if (dtBch.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtBch.Rows.Count; i++)
                        {
                            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(
                                dtBch.Rows[i]["BRANCH_ID"].ToString(),
                                dtBch.Rows[i]["BRANCH_ID"].ToString() + System.Environment.NewLine + dtBch.Rows[i]["BRANCH_NAME"].ToString(), 100)));
                        }
                    }
                    RadGridView_Show.Columns["Barcode"].IsPinned = true;

                    break;
                case "5": //5 รายงานการคืนสินค้ารวมทั้งหมด
                    SetReportCNAll("0");
                    break;
                default:
                    break;
            }

            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-3);
            radDateTimePicker_End.Value = DateTime.Now;
        }

        void SetDeptOpen()
        {
            RadCheckBox_Dept.Visible = true; RadDropDownList_Dept.Visible = true;
            RadDropDownList_Dept.DataSource = Class.Models.DptClass.GetDpt_AllD();
            RadDropDownList_Dept.DisplayMember = "DESCRIPTION_SHOW";
            RadDropDownList_Dept.ValueMember = "NUM";
        }
        //5 รายงานการคืนสินค้ารวมทั้งหมด
        void SetReportCNAll(string pCase) //5 รายงานการคืนสินค้ารวมทั้งหมด
        {
            if (pCase == "0")
            {
                SetDeptOpen();
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 120)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QtySum", "จำนวน", 80)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 100)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PriceNet", "ยอดรวม", 80)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUMDESC", "แผนกจัดซื้อ", 250)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "ผู้จำหน่าย", 100)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 300)));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("NUM", "NUM")));

                GridViewSummaryItem summary051001 = new GridViewSummaryItem("PriceNet", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summary051001 };
                RadGridView_Show.SummaryRowsTop.Add(summaryRowItemA);
                radLabel_Detail.Text = "แสดงข้อมูลเป็นหน่วยย่อยเท่านั้น [เนื่องจากเป็นการรวมทุกหน่วยที่ได้ทำคืนมา] | ไม่รวมข้อมูลของสินค้าที่ทำลดราคา | DoubleClick รายการ >> เปิดดูออเดอร์ ";
                RadCheckBox_Dept.Checked = false;
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                string bchID = "", dpt = "";
                if (RadCheckBox_Dept.Checked == true) dpt = RadDropDownList_Dept.SelectedValue.ToString();
                if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
                RadGridView_Show.DataSource = Models.DptClass.MNPC_GetReportSumAllDim(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                    radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                    dpt, bchID);//MNPC_Class.GetReportDt
                this.Cursor = Cursors.Default;
            }
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);

        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);

        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            string bchID = "", dpt = "";
            if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
            switch (_pTypeReport)
            {
                case "0":
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = Models.EmplClass.MNPC_GetReport_MNPC(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                  radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                  bchID);//MNPC_Class.GetReport_MNPC
                    this.Cursor = Cursors.Default;
                    break;
                case "1":
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = Models.DptClass.MNPC_GetReportMNPCEditDt(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                        bchID);// MNPC_Class.GetReportMNPCEditDt
                    this.Cursor = Cursors.Default;
                    break;
                case "2"://รายงานคืนสินค้าตามประเภทการคืน
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = MNPC_Class.GetSumDocByType(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                        bchID);
                    this.Cursor = Cursors.Default;
                    break;
                case "3"://รายงานการคืนสินค้า ตามแผนกจัดซื้อ
                    this.Cursor = Cursors.WaitCursor;
                    if (RadCheckBox_Dept.Checked == true) dpt = RadDropDownList_Dept.SelectedValue.ToString();
                    RadGridView_Show.DataSource = Models.DptClass.MNPC_GetReportDt(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                        dpt, bchID);//MNPC_Class.GetReportDt
                    this.Cursor = Cursors.Default;
                    break;
                case "4":// รายงานการคืนสินค้าตามบาร์โค๊ด
                    this.Cursor = Cursors.WaitCursor;
                    if (RadCheckBox_Dept.Checked == true) dpt = RadDropDownList_Dept.SelectedValue.ToString();


                    DataTable DtBarcode = MNPC_Class.GetGroupItems(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                        dpt);
                    RadGridView_Show.DataSource = DtBarcode;

                    DataTable DtItems = MNPC_Class.GetItemsQty(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                                                        dpt);

                    //Branch
                    for (int cols = 3; cols < RadGridView_Show.ColumnCount; cols++)
                    {
                        //GREEN IF RESULT > 0.00
                        ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1",
                              RadGridView_Show.Columns[cols].Name + " > 0.00 ", false)
                        { CellBackColor = ConfigClass.SetColor_GreenPastel() };
                        RadGridView_Show.Columns[RadGridView_Show.Columns[cols].Name].ConditionalFormattingObjectList.Add(obj1);

                        //RED IF RESULT > TEXTBOX
                        if (radTextBox_Price.Text != string.Empty)
                        {
                            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2",
                                RadGridView_Show.Columns[cols].Name + " > " + Double.Parse(String.Format("{0:0.00}", radTextBox_Price.Text)).ToString("F"), false)
                            { CellBackColor = ConfigClass.SetColor_Red() };
                            RadGridView_Show.Columns[RadGridView_Show.Columns[cols].Name].ConditionalFormattingObjectList.Add(obj2);
                        }
                        //check ว่าจะแสดงช่องจำนวนหรือยอดเงิน
                        int lenChoose = 2;
                        if (radioButton_price.Checked == true) lenChoose = 3;
                        //Barcode
                        for (int rows = 0; rows < RadGridView_Show.RowCount; rows++)
                        {
                            DataRow[] resultsItem = DtItems.Select(@"BRANCH = '" + RadGridView_Show.Columns[cols].Name + @"' 
                                         AND Barcode = '" + RadGridView_Show.Rows[rows].Cells[0].Value.ToString() + @"' ");
                            if (resultsItem.Length > 0) RadGridView_Show.Rows[rows].Cells[cols].Value = resultsItem[0].ItemArray[lenChoose].ToString();
                            else RadGridView_Show.Rows[rows].Cells[cols].Value = "0.00";

                        }
                    }

                    this.Cursor = Cursors.Default;
                    break;
                case "5":
                    SetReportCNAll("1");
                    break;
                default:
                    break;
            }

        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายงานรับคืนสินค้า " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true;
            else RadDropDownList_Branch.Enabled = false;
        }

        private void RadCheckBox_Dept_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Dept.Checked == true) RadDropDownList_Dept.Enabled = true;
            else RadDropDownList_Dept.Enabled = false;
        }
        //Check Number
        private void RadTextBox_Price_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if ((_pTypeReport == "3") || (_pTypeReport == "5"))
            {

                if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

                MNPO.TranferItem_BranchReciveAX frm = new MNPO.TranferItem_BranchReciveAX("11", "SUPC", "", "", "")
                {
                    Text = "เช็คสถานะสินค้าส่งมินิมาร์ท ระบบจัดสินค้าทั้งหมด.",
                    _sItemID = RadGridView_Show.CurrentRow.Cells["ITEMID"].Value.ToString(),
                    _sInventDIM = RadGridView_Show.CurrentRow.Cells["INVENTDIMID"].Value.ToString(),
                    _sBchID = RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                    WindowState = FormWindowState.Maximized
                };
                if (_pTypeReport == "3") frm._sDptID = RadGridView_Show.CurrentRow.Cells["DPTID"].Value.ToString();
                if (_pTypeReport == "5") frm._sDptID = RadGridView_Show.CurrentRow.Cells["NUM"].Value.ToString();

                if (frm.ShowDialog() == DialogResult.OK) { }
            }
        }
    }
}
