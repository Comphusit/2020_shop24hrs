﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class ARMoneyCash : Telerik.WinControls.UI.RadForm
    {
        private object summary;
        readonly string _pTypeReport; //0 ยอดส่งเงิน ขาด-เกิน  1 รายงานการขาด เกิน
        //Load
        public ARMoneyCash(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load
        private void ARMoneyCash_Load(object sender, EventArgs e)
        {
            RadButtonElement_excel.ToolTipText = "Export To Excel"; RadButtonElement_excel.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;

            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Begin, DateTime.Now.AddDays(-3), DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_BranchBegin);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_BranchEnd);

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Empl.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            switch (_pTypeReport)
            {
                case "0"://ยอดส่งเงิน ขาด-เกิน
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERNAME", "ชื่อ แคชเชียร์", 180)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SLROUND", "รอบส่งเงิน", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("TOTALCOM", "ยอดคอมรับ", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("RECEIVESUM", "ยอดรับตามไอดี", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("RANK", "RANK")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("money_deductions", "เงิน ขาด-เกิน", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("reason_branch", "หมายเหตุ", 300)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOUPD", "ผู้บันทึกหมายเหตุ", 250)));

                    GridGroupByExpression groupExprCol1 = new GridGroupByExpression("POSGROUP Group By  POSGROUP ");
                    RadGridView_Show.GroupDescriptors.Add(groupExprCol1);

                    GridViewSummaryItem sumItemValue_TOTALCOM = new GridViewSummaryItem("TOTALCOM", "{0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryItem sumItemValue_RECEIVESUM = new GridViewSummaryItem("RECEIVESUM", "{0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryItem sumItemValue_TOTAL_RECEIVESUM = new GridViewSummaryItem("TOTAL_RECEIVESUM", "{0:#,##0.00}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem sumGroupCol1 = new GridViewSummaryRowItem(new GridViewSummaryItem[] {
                    sumItemValue_TOTALCOM,
                    sumItemValue_RECEIVESUM,
                    sumItemValue_TOTAL_RECEIVESUM});

                    RadGridView_Show.MasterTemplate.SummaryRowsTop.Add(sumGroupCol1);

                    //สีเปลี่ยน เมื่อมีเงินขาด
                    ConditionalFormattingObject cMoney = new ConditionalFormattingObject("deductions", ConditionTypes.Less, "0", "", true)
                    {
                        RowBackColor = ConfigClass.SetColor_PinkPastel(),
                        CellBackColor = ConfigClass.SetColor_PinkPastel()
                    };
                    RadGridView_Show.Columns["money_deductions"].ConditionalFormattingObjectList.Add(cMoney);

                    //สีเปลี่ยน เมื่อสาขาตอบเหตุผล
                    ConditionalFormattingObject cReason = new ConditionalFormattingObject("reason_branch", ConditionTypes.NotEqual, "", "", true)
                    {
                        RowBackColor = ConfigClass.SetColor_GreenPastel(),
                        CellBackColor = ConfigClass.SetColor_GreenPastel()
                    };

                    RadGridView_Show.Columns["reason_branch"].ConditionalFormattingObjectList.Add(cReason);

                    this.RadGridView_Show.MasterTemplate.ShowParentGroupSummaries = true;
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        RadButton_Search.Text = "ค้นหา";
                        radLabel_Detail.Text = "DoubleClick รายการ >> เพื่อตอบเหตุผล | สีชมพู >> ยอดเงินขาด-ส่งหัก | สีเขียว >> สาขาตอบเหตุผล";
                    }
                    else
                    {
                        RadButton_Search.Text = "ค้นหา";
                        radLabel_Detail.Text = "สีชมพู >> ยอดเงินขาด-ส่งหัก | สีเขียว >> สาขาตอบเหตุผล";
                    }
                    break;
                case "1"://รายงานยอดเงิน
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Composgroup", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Comtransdate", "วันที่", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Comcashierid", "แคชเชียร์", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ แคชเชียร์", 180)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Comcashier_com", "ยอดคอม", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Cashtotal_receivesum", "ยอดแคชเชียร์", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Money", "เงินขาด - เกิน", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Commoney_deductions", "ส่งหัก", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Comreason_branch", "เหตุผล", 400)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("status_deductions", "status_deductions")));

                    //สีเปลี่ยน
                    ConditionalFormattingObject c1 = new ConditionalFormattingObject("MyCondition1", ConditionTypes.Greater, "0", "", true)
                    {
                        CellBackColor = ConfigClass.SetColor_GreenPastel()
                    };
                    RadGridView_Show.Columns["Commoney_deductions"].ConditionalFormattingObjectList.Add(c1);


                    ConditionalFormattingObject c2 = new ConditionalFormattingObject("deductions", ConditionTypes.Less, "0", "", true)
                    {
                        CellBackColor = ConfigClass.SetColor_PinkPastel()
                    };
                    RadGridView_Show.Columns["Commoney_deductions"].ConditionalFormattingObjectList.Add(c2);

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("Composgroup", ListSortDirection.Ascending);
                    descriptor.GroupNames.Add("Comtransdate", ListSortDirection.Ascending);
                    RadGridView_Show.GroupDescriptors.Add(descriptor);

                    RadButton_Search.Text = "ค้นหา";
                    radLabel_Detail.Text = "สีชมพู >> เงินขาด | สีเขียว >> เงินเกิน";
                    break;
                default:
                    break;
            }

            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDropDownList_Branch(RadDropDownList_BranchBegin, "1,4");
            DatagridClass.SetDropDownList_Branch(radDropDownList_BranchEnd, "1,4");

            RadCheckBox_Branch.Checked = true; RadCheckBox_Branch.Enabled = false;
        }

        //ค้นหา
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            string cashID = "";
            if (radCheckBox_Empl.Checked == true)
            {
                if (radTextBox_Empl.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ต้องระบุรหัสแคชเชียร์{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง");
                    radTextBox_Empl.Focus(); return;
                }
                cashID = radTextBox_Empl.Text.Trim();
            }

            switch (_pTypeReport)
            {
                case "0"://ยอดส่งเงิน ขาด-เกิน
                    #region SQL
                    this.Cursor = Cursors.WaitCursor;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        //DataTable DtARSendMoney707 = ARClassA.GetMoneyAx707(
                        //       RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                        //       RadDropDownList_BranchBegin.SelectedValue.ToString(), radDropDownList_BranchEnd.SelectedValue.ToString(),
                        //       cashID);
                        DataTable DtARSendMoney707 = Models.ARClass.GetMoneyAx707(
                                RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                RadDropDownList_BranchBegin.SelectedValue.ToString(), radDropDownList_BranchEnd.SelectedValue.ToString(),
                                cashID);
                        if (DtARSendMoney707.Rows.Count == 0) return;

                        DataTable DtReasonSave = AR_Class.Check_ReasonSaveRoundSendMoney(
                                RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                RadDropDownList_BranchBegin.SelectedValue.ToString(), radDropDownList_BranchEnd.SelectedValue.ToString());

                        Double money_deductions, receivesum = 0, totalcom = 0;

                        for (int iRoundSendMoney = 0; iRoundSendMoney < DtARSendMoney707.Rows.Count; iRoundSendMoney++)
                        {
                            Double Dtreceivesum = 0, Dttotalcom = 0;

                            if (!(DtARSendMoney707.Rows[iRoundSendMoney]["receivesum"].ToString().Equals("")))
                                Dtreceivesum = Double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["receivesum"].ToString());

                            if (!(DtARSendMoney707.Rows[iRoundSendMoney]["totalcom"].ToString().Equals("")))
                                Dttotalcom = Double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["totalcom"].ToString());

                            if (DtARSendMoney707.Rows[iRoundSendMoney]["rank"].ToString() == "1")
                            {
                                receivesum = Dtreceivesum;
                                totalcom = Dttotalcom;
                                money_deductions = receivesum - totalcom;
                            }
                            else
                            {
                                receivesum += Dtreceivesum;
                                totalcom += Dttotalcom;
                                money_deductions = receivesum - totalcom;
                            }

                            //เช็คเฉพาะยอดที่ยังแคชเชียร์ไม่ได้ตอบเหตุผล 
                            DataRow[] result;
                            int Rowresult = 0;
                            if (DtReasonSave.Rows.Count > 0)
                            {
                                result = DtReasonSave.Select(string.Format(@"slround='{0}'", DtARSendMoney707.Rows[iRoundSendMoney]["SLROUND"].ToString()));
                                Rowresult = result.Length;
                            }

                            string TRANSDATE = RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                POSGROUP = "",
                                CASHIERID = "",
                                CASHIERNAME = "";
                            double CASHIER_COM = 0, TOTALCOM = 0;

                            ArrayList sqlins = new ArrayList();
                            if (Rowresult != 0)
                            {
                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["CASHIER_COM"].ToString()))
                                    CASHIER_COM = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["CASHIER_COM"].ToString());

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["TOTALCOM"].ToString()))
                                    TOTALCOM = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["TOTALCOM"].ToString());

                                double reciveSumA = 0;
                                if (DtARSendMoney707.Rows[iRoundSendMoney]["RECEIVESUM"].ToString() != "")
                                    reciveSumA = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["RECEIVESUM"].ToString());

                                double totalReciveSumA = 0;
                                if (DtARSendMoney707.Rows[iRoundSendMoney]["TOTAL_RECEIVESUM"].ToString() != "")
                                    totalReciveSumA = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["TOTAL_RECEIVESUM"].ToString());

                                sqlins.Add(AR_Class.UpdateRound_ReciveMoney(DtARSendMoney707.Rows[iRoundSendMoney]["SLROUND"].ToString(),
                                   CASHIER_COM, reciveSumA,
                                  totalReciveSumA, TOTALCOM));
                            }
                            else
                            {

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["TRANSDATE"].ToString()))
                                    TRANSDATE = DateTime.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["TRANSDATE"].ToString()).ToString("yyyy-MM-dd");

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["POSGROUP"].ToString()))
                                    POSGROUP = DtARSendMoney707.Rows[iRoundSendMoney]["POSGROUP"].ToString();

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["CASHIERID"].ToString()))
                                    CASHIERID = DtARSendMoney707.Rows[iRoundSendMoney]["CASHIERID"].ToString();

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["CASHIER_COM"].ToString()))
                                    CASHIER_COM = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["CASHIER_COM"].ToString());

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["CASHIERNAME"].ToString()))
                                    CASHIERNAME = DtARSendMoney707.Rows[iRoundSendMoney]["CASHIERNAME"].ToString();

                                if (Checkblank(DtARSendMoney707.Rows[iRoundSendMoney]["TOTALCOM"].ToString()))
                                    TOTALCOM = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["TOTALCOM"].ToString());

                                double reciveSum = 0;
                                if (DtARSendMoney707.Rows[iRoundSendMoney]["RECEIVESUM"].ToString() != "")
                                    receivesum = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["RECEIVESUM"].ToString());

                                double totalReciveSum = 0;
                                if (DtARSendMoney707.Rows[iRoundSendMoney]["TOTAL_RECEIVESUM"].ToString() != "")
                                    totalReciveSum = double.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["TOTAL_RECEIVESUM"].ToString());

                                Var_RECIVEMONEY varDetail = new Var_RECIVEMONEY()
                                {
                                    Transdate = TRANSDATE,
                                    SlRounddate = TRANSDATE,
                                    Posgroup = POSGROUP,
                                    Cashierid = CASHIERID,
                                    Cashier_com = CASHIER_COM,
                                    Cashiername = CASHIERNAME,
                                    Slround = DtARSendMoney707.Rows[iRoundSendMoney]["SLROUND"].ToString(),
                                    Receivesum = reciveSum,
                                    Total_receivesum = totalReciveSum,
                                    Rank = DtARSendMoney707.Rows[iRoundSendMoney]["RANK"].ToString(),
                                    Totalcom = TOTALCOM
                                };
                                sqlins.Add(AR_Class.Save_ReciveMoney(varDetail));

                            }
                            sqlins.Add(AR_Class.UpdateMoney_ReciveMoney(DtARSendMoney707.Rows[iRoundSendMoney]["cashierid"].ToString(),
                                DateTime.Parse(DtARSendMoney707.Rows[iRoundSendMoney]["TRANSDATE"].ToString()).ToString("yyyy-MM-dd"),
                                money_deductions));

                            string resualt = ConnectionClass.ExecuteSQL_ArrayMain(sqlins);
                            if (resualt != "") MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                        }

                    }

                    DataTable DtRoundSendMoney = AR_Class.GetRoundSendMoney(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                            RadDropDownList_BranchBegin.SelectedValue.ToString(), radDropDownList_BranchEnd.SelectedValue.ToString(), cashID);

                    RadGridView_Show.DataSource = DtRoundSendMoney;
                    this.Cursor = Cursors.Default;
                    #endregion
                    break;
                case "1":
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_Show.DataSource = AR_Class.GetMoneyCash_AllBranch(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                RadDropDownList_BranchBegin.SelectedValue.ToString(),
                                radDropDownList_BranchEnd.SelectedValue.ToString(), cashID);
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }

        }
        //
        private bool Checkblank(string data)
        {
            bool rsu = false;
            if (!(data.Equals(""))) rsu = true;
            return rsu;
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_CustomGrouping(object sender, GridViewCustomGroupingEventArgs e)
        {
            switch (_pTypeReport)
            {
                case "0":
                    e.GroupKey = e.Row.Cells["POSGROUP"].Value.ToString() + "" + e.Row.Cells["TRANSDATE"].Value.ToString();
                    break;
                default:
                    break;
            }

        }

        private void RadGridView_Show_GroupSummaryEvaluate(object sender, GroupSummaryEvaluationEventArgs e)
        {
            this.summary = e.Value;
        }
        //ตอบเงินผลเงินขาด
        void Reason()
        {
            if (SystemClass.SystemBranchID == "MN000") return;
            if (RadGridView_Show.CurrentRow.Cells["RANK"].Value.ToString() != "1") return;
            if (Double.Parse(RadGridView_Show.CurrentRow.Cells["money_deductions"].Value.ToString()) == 0) return;

            FormShare.ShowRemark _showRmk = new FormShare.ShowRemark("1")
            {
                pDesc = $@"ระบุเหตุผลเงินขาด :  {Double.Parse(RadGridView_Show.CurrentRow.Cells["money_deductions"].Value.ToString()):N2} บาท",
                pRmk = RadGridView_Show.CurrentRow.Cells["reason_branch"].Value.ToString()
            };

            if (_showRmk.ShowDialog(this) == DialogResult.Yes)
            {
                string sqlUp = AR_Class.UpdateReason_ReciveMoney(_showRmk.pRmk, RadGridView_Show.CurrentRow.Cells["CASHIERID"].Value.ToString(),
                    DateTime.Parse(RadGridView_Show.CurrentRow.Cells["TRANSDATE"].Value.ToString()).ToString("yyyy-MM-dd"),
                    Double.Parse(RadGridView_Show.CurrentRow.Cells["money_deductions"].Value.ToString()));

                string resault = ConnectionClass.ExecuteSQL_Main(sqlUp);
                MsgBoxClass.MsgBoxShow_SaveStatus(resault);
                if (resault == "") RadGridView_Show.CurrentRow.Cells["reason_branch"].Value = _showRmk.pRmk;
            }
        }


        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //CellDocubleClick
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (_pTypeReport)
            {
                case "0":
                    Reason();
                    break;
                default:
                    break;
            }

        }


        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //checkChange
        private void RadCheckBox_Empl_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Empl.Checked == true)
            {
                radTextBox_Empl.Enabled = true;
                radTextBox_Empl.Text = ""; radTextBox_Empl.Focus();
            }
            else
            {
                radTextBox_Empl.Enabled = false;
                radTextBox_Empl.Text = "";
            }
        }
    }
}
