﻿namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    partial class ARMoneyCash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ARMoneyCash));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radDropDownList_BranchEnd = new Telerik.WinControls.UI.RadDropDownList();
            this.radCheckBox_Empl = new Telerik.WinControls.UI.RadCheckBox();
            this.radTextBox_Empl = new Telerik.WinControls.UI.RadTextBox();
            this.RadCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.RadDropDownList_BranchBegin = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.RadDateTimePicker_Begin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_BranchEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_BranchBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 19);
            this.radLabel_Detail.TabIndex = 56;
            this.radLabel_Detail.Text = "<html>DoubleClick รายการ &gt;&gt; เพื่อตอบเหตุผล | สีชมพู &gt;&gt; ยอดเงินขาด-ส่ง" +
    "หัก | สีเขียว &gt;&gt; สาขาตอบเหตุผล</html>";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(662, 605);
            this.RadGridView_Show.TabIndex = 16;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.GroupSummaryEvaluate += new Telerik.WinControls.UI.GroupSummaryEvaluateEventHandler(this.RadGridView_Show_GroupSummaryEvaluate);
            this.RadGridView_Show.CustomGrouping += new Telerik.WinControls.UI.GridViewCustomGroupingEventHandler(this.RadGridView_Show_CustomGrouping);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radDropDownList_BranchEnd);
            this.panel1.Controls.Add(this.radCheckBox_Empl);
            this.panel1.Controls.Add(this.radTextBox_Empl);
            this.panel1.Controls.Add(this.RadCheckBox_Branch);
            this.panel1.Controls.Add(this.RadDropDownList_BranchBegin);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.RadDateTimePicker_Begin);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // radDropDownList_BranchEnd
            // 
            this.radDropDownList_BranchEnd.DropDownAnimationEnabled = false;
            this.radDropDownList_BranchEnd.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_BranchEnd.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_BranchEnd.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_BranchEnd.Location = new System.Drawing.Point(9, 99);
            this.radDropDownList_BranchEnd.Name = "radDropDownList_BranchEnd";
            this.radDropDownList_BranchEnd.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_BranchEnd.TabIndex = 76;
            this.radDropDownList_BranchEnd.Text = "radDropDownList1";
            // 
            // radCheckBox_Empl
            // 
            this.radCheckBox_Empl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Empl.Location = new System.Drawing.Point(10, 183);
            this.radCheckBox_Empl.Name = "radCheckBox_Empl";
            this.radCheckBox_Empl.Size = new System.Drawing.Size(104, 19);
            this.radCheckBox_Empl.TabIndex = 74;
            this.radCheckBox_Empl.Text = "รหัสแคชเชียร์";
            this.radCheckBox_Empl.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Empl_CheckStateChanged);
            // 
            // radTextBox_Empl
            // 
            this.radTextBox_Empl.Enabled = false;
            this.radTextBox_Empl.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Empl.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Empl.Location = new System.Drawing.Point(9, 211);
            this.radTextBox_Empl.Name = "radTextBox_Empl";
            this.radTextBox_Empl.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Empl.TabIndex = 75;
            // 
            // RadCheckBox_Branch
            // 
            this.RadCheckBox_Branch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RadCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Branch.Location = new System.Drawing.Point(11, 47);
            this.RadCheckBox_Branch.Name = "RadCheckBox_Branch";
            this.RadCheckBox_Branch.Size = new System.Drawing.Size(76, 19);
            this.RadCheckBox_Branch.TabIndex = 62;
            this.RadCheckBox_Branch.Text = "ระบุสาขา";
            this.RadCheckBox_Branch.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // RadDropDownList_BranchBegin
            // 
            this.RadDropDownList_BranchBegin.DropDownAnimationEnabled = false;
            this.RadDropDownList_BranchBegin.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_BranchBegin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_BranchBegin.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_BranchBegin.Location = new System.Drawing.Point(10, 72);
            this.RadDropDownList_BranchBegin.Name = "RadDropDownList_BranchBegin";
            this.RadDropDownList_BranchBegin.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_BranchBegin.TabIndex = 61;
            this.RadDropDownList_BranchBegin.Text = "radDropDownList1";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(11, 126);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(70, 19);
            this.radLabel_Date.TabIndex = 60;
            this.radLabel_Date.Text = "วันที่ส่งเงิน";
            // 
            // RadDateTimePicker_Begin
            // 
            this.RadDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_Begin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_Begin.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_Begin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_Begin.Location = new System.Drawing.Point(11, 150);
            this.RadDateTimePicker_Begin.Name = "RadDateTimePicker_Begin";
            this.RadDateTimePicker_Begin.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_Begin.TabIndex = 58;
            this.RadDateTimePicker_Begin.TabStop = false;
            this.RadDateTimePicker_Begin.Text = "02/08/2022";
            this.RadDateTimePicker_Begin.Value = new System.DateTime(2022, 8, 2, 0, 0, 0, 0);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.RadButtonElement_excel,
            this.commandBarSeparator1,
            this.RadButtonElement_pdt,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_excel
            // 
            this.RadButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.RadButtonElement_excel.Name = "RadButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_excel, false);
            this.RadButtonElement_excel.Text = "radButtonElement1";
            this.RadButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.FlipText = true;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(9, 254);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 0;
            this.RadButton_Search.Text = "บันทึก";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // ARMoneyCash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "ARMoneyCash";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายงานต่างๆ แผนกบัญชีลูกหนี้";
            this.Load += new System.EventHandler(this.ARMoneyCash_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_BranchEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_BranchBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_Begin;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_BranchBegin;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Branch;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Empl;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Empl;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_BranchEnd;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
    }
}
