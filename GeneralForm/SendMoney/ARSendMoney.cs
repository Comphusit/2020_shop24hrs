﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class ARSendMoney : Telerik.WinControls.UI.RadForm
    {
        private string TimeBegin, TimeEnd, DateBegin, DateEnd, Round;
        private string MNAR;
        DataTable dtDrawerNo, dtDrawerOk, dtDrawerEmp;
        int seq;
        Double Total { get; set; } = 0;
        Double Bank1000 { get; set; } = 0;
        Double Bank500 { get; set; } = 0;
        Double Bank100 { get; set; } = 0;
        Double Bank50 { get; set; } = 0;
        Double Bank20 { get; set; } = 0;
        Double Coins { get; set; } = 0;

        //Load
        public ARSendMoney()
        {
            InitializeComponent();
        }
        //Load
        private void ARSendMoney_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_SendMoney.ShowBorder = true; radButtonElement_SendMoney.ToolTipText = "ส่งเงินสด";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์เอกสาร";
            RadButton_Search.ButtonElement.ShowBorder = true;

            radStatusStrip2.SizingGrip = false;

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Now, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_No);
            DatagridClass.SetDefaultRadGridView(RadGridView_Ok);
            DatagridClass.SetDefaultRadGridView(RadGridView_Empl);

            //บน
            RadGridView_No.EnableFiltering = false;
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDRAWERID", "รอบชำระเงิน", 120)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Transdate", "วันที่ปิดรอบ", 80)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 60)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserDRAWERID", "พนักงาน", 220)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CASHSENT", "ยอดส่งเงินสด", 100)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CASHRECEIVE", "เงินสดค้างรับ", 100)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMRemark", "หมายเหตุ", 450)));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("NAME", "NAMEAR")));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPMDocno", "MNPMDocnoNO")));
            RadGridView_No.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("RemarkAcc", "SENDIDNO")));
            RadGridView_No.TableElement.RowHeight = 70;
            RadGridView_No.Columns["MNPMDRAWERID"].IsPinned = true;
            //ล่างซ้าย
            //RadGridView_Ok.EnableFiltering = false;
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDRAWERID", "รอบชำระเงิน", 120)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Transdate", "วันที่ปิดรอบ", 80)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 60)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserDRAWERID", "พนักงาน", 220)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CASHSENT", "ยอดส่งเงินสด", 100)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CASHRECEIVE", "เงินสดค้างรับ", 100)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMRemark", "หมายเหตุ", 500)));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("NAME", "NAMEAR")));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPMDocno", "MNPMDocnoNO")));
            RadGridView_Ok.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("RemarkAcc", "SENDIDNO")));
            RadGridView_Ok.TableElement.RowHeight = 70;
            RadGridView_Ok.Columns["MNPMDRAWERID"].IsPinned = true;
            //ล่างขวา
            RadGridView_Empl.EnableFiltering = false;
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Transdate", "วันที่ปิดรอบ", 80)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อ", 250)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SENDMONEYID", "จน.ส่ง", 40)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDRAWERID", "จน.ซอง", 50)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BANKNOTE1000", "BANK1000", 70)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BANKNOTE500", "BANK500", 70)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BANKNOTE100", "BANK100", 70)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BANKNOTE50", "BANK50", 70)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BANKNOTE20", "BANK20", 70)));
            RadGridView_Empl.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COIN1", "COINS", 70)));
            RadGridView_Empl.TableElement.RowHeight = 70;
            //สีเปลี่ยน 
            ConditionalFormattingObject cSENDMONEY = new ConditionalFormattingObject("SENDMONEYID", ConditionTypes.NotEqual, "", "", true)
            {
                RowBackColor = ConfigClass.SetColor_PurplePastel(),
                CellBackColor = ConfigClass.SetColor_PurplePastel()
            };

            RadGridView_Empl.Columns["SENDMONEYID"].ConditionalFormattingObjectList.Add(cSENDMONEY);

            ClearDefault();
        }

        #region EVEN
        void ClearDefault()
        {
            if (RadGridView_No.RowCount > 0) RadGridView_No.DataSource = null;

            if (RadGridView_Ok.RowCount > 0) RadGridView_Ok.DataSource = null;

            if (RadGridView_Empl.RowCount > 0) RadGridView_Empl.DataSource = null;

            TimeBegin = "";
            TimeEnd = "";
            Round = "1";

            if (radRadioButton_Round1.IsChecked == true)
            {
                Round = "1";
                TimeBegin = "19:31:00";
                TimeEnd = "08:30:59";
                DateBegin = RadDateTimePicker_Now.Value.AddDays(-1).ToString("yyyy-MM-dd");
                DateEnd = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd");
            }
            else if (radRadioButton_Round2.IsChecked == true)
            {
                Round = "2";
                TimeBegin = "08:31:00";
                TimeEnd = "15:00:59";
                DateBegin = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd");
                DateEnd = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd");
            }
            else if (radRadioButton_Round3.IsChecked == true)
            {
                Round = "3";
                TimeBegin = "15:01:00";
                TimeEnd = "19:30:59";
                DateBegin = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd");
                DateEnd = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd");
            }
        }

        void PrintDocument(string MNAR)
        {
            if (MNAR == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณากดบันทึก ส่งเงินสด ให้เรียบร้อย ก่อนกด พิมพ์เอกสาร.");
                radButtonElement_SendMoney.Focus();
                return;
            }

            DialogResult dr = printDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                printDocument1.Print();

            }
        }

        private void SumMoney(Double _Bank1000, Double _Bank500, Double _Bank100, Double _Bank50, Double _Bank20, Double _Coins)
        {
            this.Total = (_Bank1000 * 1000) + (_Bank500 * 500) + (_Bank100 * 100) + (_Bank50 * 50) + (_Bank20 * 20) + _Coins;
        }

        #endregion

        #region GRID FORMAT
        private void RadGridView_No_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);      
        }

        private void RadGridView_Ok_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Empl_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion


        #region EVEN
        private void RadGridView_Empl_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_Empl.RowCount > 0)
            {
                if (RadGridView_Empl.CurrentRow.Cells[1].Value.ToString().Substring(0, 4) != "MNAR")
                {
                    this.Bank1000 = Double.Parse(RadGridView_Empl.CurrentRow.Cells["BANKNOTE1000"].Value.ToString());
                    this.Bank500 = Double.Parse(RadGridView_Empl.CurrentRow.Cells["BANKNOTE500"].Value.ToString());
                    this.Bank100 = Double.Parse(RadGridView_Empl.CurrentRow.Cells["BANKNOTE100"].Value.ToString());
                    this.Bank50 = Double.Parse(RadGridView_Empl.CurrentRow.Cells["BANKNOTE50"].Value.ToString());
                    this.Bank20 = Double.Parse(RadGridView_Empl.CurrentRow.Cells["BANKNOTE20"].Value.ToString());
                    this.Coins = Double.Parse(RadGridView_Empl.CurrentRow.Cells["COIN1"].Value.ToString());
                    SumMoney(Bank1000, Bank500, Bank100, Bank50, Bank20, Coins);
                }
            }
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            ClearDefault();

            //dtDrawerNo = ARClassA.GetDrawer(DateBegin, DateEnd, TimeBegin, TimeEnd, "<");
            dtDrawerNo = Models.ARClass.GetDrawer(DateBegin, DateEnd, TimeBegin, TimeEnd, "<");
            RadGridView_No.DataSource = dtDrawerNo;

            //dtDrawerOk = ARClassA.GetDrawer(DateBegin, DateEnd, TimeBegin, TimeEnd, ">=");
            dtDrawerOk = Models.ARClass.GetDrawer(DateBegin, DateEnd, TimeBegin, TimeEnd, ">=");
            RadGridView_Ok.DataSource = dtDrawerOk;

            //dtDrawerEmp = ARClassA.GetSendMoneyToFinance(DateBegin + ' ' + TimeBegin, DateEnd + ' ' + TimeEnd);
            dtDrawerEmp = Models.ARClass.GetSendMoneyToFinance(DateBegin + ' ' + TimeBegin, DateEnd + ' ' + TimeEnd);

            Radgrid_empl(dtDrawerEmp);

            this.Cursor = Cursors.Default;
        }

        private void Radgrid_empl(DataTable dtDrawerEmp)
        {
            if (RadGridView_Empl.RowCount > 0) RadGridView_Empl.Rows.Clear();

            for (int iEmpRow = 0; iEmpRow < dtDrawerEmp.Rows.Count; iEmpRow++)
            {
                string Transdate = dtDrawerEmp.Rows[iEmpRow]["Transdate"].ToString();
                string NAME = dtDrawerEmp.Rows[iEmpRow]["NAME"].ToString();
                string MNPMDRAWERID = dtDrawerEmp.Rows[iEmpRow]["MNPMDRAWERID"].ToString();
                string BANKNOTE1000 = dtDrawerEmp.Rows[iEmpRow]["BANKNOTE1000"].ToString();
                string BANKNOTE500 = dtDrawerEmp.Rows[iEmpRow]["BANKNOTE500"].ToString();
                string BANKNOTE100 = dtDrawerEmp.Rows[iEmpRow]["BANKNOTE100"].ToString();
                string BANKNOTE50 = dtDrawerEmp.Rows[iEmpRow]["BANKNOTE50"].ToString();
                string BANKNOTE20 = dtDrawerEmp.Rows[iEmpRow]["BANKNOTE20"].ToString();
                string COIN1 = dtDrawerEmp.Rows[iEmpRow]["COIN1"].ToString();

                RadGridView_Empl.Rows.Add(new object[] { Transdate, NAME, "", MNPMDRAWERID, BANKNOTE1000, BANKNOTE500, BANKNOTE100, BANKNOTE50, BANKNOTE20, COIN1 });

                RadGridView_Empl.Rows[iEmpRow].Cells[1].Style.BackColor = Color.Red;

                string DATESENDMONEY = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd");

                int RowDefaultEmp = RadGridView_Empl.Rows.Count - 1;

                DataTable dtMNAR = AR_Class.GetMNAR(DATESENDMONEY, Transdate, NAME, Round);
                for (int iMNARRow = 0; iMNARRow < dtMNAR.Rows.Count; iMNARRow++)
                {
                    string SEQ = dtMNAR.Rows[iMNARRow]["SEQ"].ToString();
                    string SENDMONEYID = dtMNAR.Rows[iMNARRow]["SENDMONEYID"].ToString();
                    string BANK1000 = dtMNAR.Rows[iMNARRow]["BANK1000"].ToString();
                    string BANK500 = dtMNAR.Rows[iMNARRow]["BANK500"].ToString();
                    string BANK100 = dtMNAR.Rows[iMNARRow]["BANK100"].ToString();
                    string BANK50 = dtMNAR.Rows[iMNARRow]["BANK50"].ToString();
                    string BANK20 = dtMNAR.Rows[iMNARRow]["BANK20"].ToString();
                    string COINS = dtMNAR.Rows[iMNARRow]["COINS"].ToString();

                    RadGridView_Empl.Rows.Add(new object[] { SEQ, SENDMONEYID, "", "", BANK1000, BANK500, BANK100, BANK50, BANK20, COINS });

                    RadGridView_Empl.Rows[RowDefaultEmp].Cells["SENDMONEYID"].Value = SEQ;
                }
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        
        private void RadRadioButton_Round1_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Round1.IsChecked == true) Round = "1";
        }

        private void RadRadioButton_Round2_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Round2.IsChecked == true) Round = "2";
        }

        private void RadRadioButton_Round3_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Round3.IsChecked == true) Round = "3";
        }

        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            if (RadGridView_Empl.CurrentRow.Cells[1].Value.ToString().Substring(0, 4) == "MNAR")
            {
                MNAR = RadGridView_Empl.CurrentRow.Cells["NAME"].Value.ToString();
                seq = int.Parse(RadGridView_Empl.CurrentRow.Cells["SENDMONEYID"].Value.ToString());
                PrintDocument(MNAR);
                MNAR = string.Empty;
            }
        }

        private void RadButtonElement_SendMoney_Click(object sender, EventArgs e)
        {
            if (RadGridView_Empl.CurrentRow.Cells[1].Value.ToString().Substring(0, 4) == "MNAR") return;

            if (RadGridView_No.RowCount > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ยังมีรายการส่งเงินสดไม่เรียบร้อย ไม่สามารถพิมพ์ส่งเงินสดได้.");
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            using (ARSendMoney_Type typeSendMoney = new ARSendMoney_Type(
                this.Bank1000,
                this.Bank500,
                this.Bank100,
                this.Bank50,
                this.Bank20,
                this.Coins))
            {
                DialogResult dr = typeSendMoney.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    this.Bank1000 = typeSendMoney.Bank1000;
                    this.Bank500 = typeSendMoney.Bank500;
                    this.Bank100 = typeSendMoney.Bank100;
                    this.Bank50 = typeSendMoney.Bank50;
                    this.Bank20 = typeSendMoney.Bank20;
                    this.Coins = typeSendMoney.Coins;

                    string DATESENDMONEY = RadDateTimePicker_Now.Value.ToString("yyyy-MM-dd"),
                        DATETRANSMONEY = RadGridView_Empl.CurrentRow.Cells["Transdate"].Value.ToString(),
                        EMPLID = RadGridView_Empl.CurrentRow.Cells["NAME"].Value.ToString();

                    seq = AR_Class.GetSEQ(DATESENDMONEY, DATETRANSMONEY, EMPLID, Round);
                    SumMoney(Bank1000, Bank500, Bank100, Bank50, Bank20, Coins);

                    MNAR = Class.ConfigClass.GetMaxINVOICEID("MNAR", "-", "MNAR", "1");
                    Var_SendMoneyToFinance var_SendMoneyToFinance = new Var_SendMoneyToFinance
                    {
                        DATESENDMONEY = DATESENDMONEY,
                        DATETRANSMONEY = DATETRANSMONEY,
                        Bank1000 = this.Bank1000,
                        Bank500 = this.Bank500,
                        Bank100 = this.Bank100,
                        Bank50 = this.Bank50,
                        Bank20 = this.Bank20,
                        Coins = this.Coins,
                        Total = this.Total,
                        Seq = seq,
                        Round = Round,
                        EMPLID = EMPLID
                    };

                    string SqlTranction = ConnectionClass.ExecuteSQL_Main(AR_Class.Save_SendMoneyToFinance(MNAR, var_SendMoneyToFinance));
                    MsgBoxClass.MsgBoxShow_SaveStatus(SqlTranction);
                    if (SqlTranction == "")
                    {
                        PrintDocument(MNAR);
                        //dtDrawerEmp = ARClassA.GetSendMoneyToFinance(DateBegin + ' ' + TimeBegin, DateEnd + ' ' + TimeEnd);
                        dtDrawerEmp = Models.ARClass.GetSendMoneyToFinance(DateBegin + ' ' + TimeBegin, DateEnd + ' ' + TimeEnd);
                        ////แบบ 1 คน หลายยอด 
                        Radgrid_empl(dtDrawerEmp);

                        seq = 0;
                        this.Cursor = Cursors.Default;
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int Y = 0;

            BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear
            {
                Data = MNAR
            };
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            e.Graphics.DrawString("เอกสารส่งการเงิน ", SystemClass.SetFont12, Brushes.Black, 55, Y);
            Y += 20;
            e.Graphics.DrawString(MNAR + "[" + seq + "]", SystemClass.SetFont12, Brushes.Black, 50, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 30, Y);
            Y += 100;
            e.Graphics.DrawString("พิมพ์" + DateTime.Now, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 15;
            e.Graphics.DrawString("ธนบัตร 1000 : " + this.Bank1000 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 500  : " + this.Bank500 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 100  : " + this.Bank100 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 50   : " + this.Bank50 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 20   : " + this.Bank20 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("เหรียญ        : " + this.Coins + "  บาท", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ยอดรวม  " + this.Total.ToString("N2") + " บาท", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(RadGridView_Empl.CurrentRow.Cells["NAME"].Value.ToString(), SystemClass.SetFont12, Brushes.Black, 0, Y);
            //Y += 20;
            e.Graphics.PageUnit = GraphicsUnit.Inch;


        }
        #endregion
    }
}
