﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Printing;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class MNPM_Report : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        readonly string _pPermission;

        string headBill;
        // 4 - รายงานจำนวนซองส่งตามรอบ LO
        // 5 - รายงานการรับซองเงิน
        // 6 - รายงานซองส่งเงินของ พนักงานบัญชีลูกหนี้
        // 7 - รายงานรายละเอียดส่งซองเงิน

        //Load
        public MNPM_Report(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void MNPM_Report_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;


            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radStatusStrip1.SizingGrip = false;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;

            switch (_pTypeReport)
            {
                case "4":  // 4 - รายงานจำนวนซองส่งตามรอบ LO

                    radLabel_Date.Text = "ระบุวันที่เก็บซองเงิน";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMLO", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserCar", "พขร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ พขร", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMCar", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDate", "วันที่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMTime", "เวลา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMBranch", "สาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMCountPack", "จำนวนซอง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PATHVDO", "PATHVDO")));

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("MNPMLO", ListSortDirection.Ascending);
                    descriptor.Aggregates.Add("Sum(MNPMCountPack)");
                    descriptor.Format = "รอบ {1} มีซองเงินส่งมาทั้งหมด {2} ซอง.";
                    this.RadGridView_ShowHD.GroupDescriptors.Add(descriptor);
                    radLabel_Detail.Text = "สีแดง >> ไม่มีรายการบันทึก VDO รับซองเงิน[จัดส่ง] | Douclick รายการ >> ดู VDO รับซองเงิน[จัดส่ง]"; radLabel_Detail.Visible = true;

                    DatagridClass.SetCellBackClolorByExpression("MNPMLO", " PATHVDO= '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    break;
                case "5":  // 5 - รายงานการรับซองเงิน
                    radLabel_Detail.Text = "สีแดง >> จัดส่งยังไม่รับ | สีฟ้า >> บัญชียังไม่รับ";
                    radLabel_Date.Text = "ระบุวันที่ส่งซองเงิน";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RoundTrans", "รอบ", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMBranch", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDRAWERID", "เลขที่ซองส่งเงิน", 150)));
                    if (_pPermission == "0")
                    { RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPMGranchDRAWERID", "ยอดเงิน"))); }
                    else
                    { RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMGranchDRAWERID", "ยอดเงิน", 100))); }
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSFERDATE", "วันที่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ พขร", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StaTransDESC", "สถานะ [จัดส่ง]", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoTrans", "ผู้รับ [จัดส่ง]", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateTrans", "เวลารับ [จัดส่ง]", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StaAccDESC", "สถานะ [บัญชี]", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoAcc", "ผู้รับ [บัญชี]", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateAcc", "เวลารับ [บัญชี]", 150)));

                    DatagridClass.SetCellBackClolorByExpression("MNPMDRAWERID", "StaTransDESC = 'ไม่ได้รับ' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNPMDRAWERID", "StaAccDESC = 'ไม่ได้รับ' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                    GridViewSummaryItem summaryItem = new GridViewSummaryItem
                    {
                        Name = "MNPMGranchDRAWERID",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    break;
                case "6": // 6 - รายงานซองส่งเงินของ พนักงานบัญชีลูกหนี้
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Text = "ระบุวันที่ส่งเงิน";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SendStaDESC", "สถานะสาขาส่ง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SendAccDESC", "สถานะบัญชีรับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DRAWERID", "เลขที่ซองส่งเงิน", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงิน", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่รับซอง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplName", "ผู้ส่งซอง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplDepart", "แผนก", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Remark", "หมายเหตุ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ผู้รับซอง", 250)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMLO", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMCar", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserCar", "พขร", 100)));

                    GridViewSummaryItem summaryItem6_1 = new GridViewSummaryItem
                    {
                        Name = "LINEAMOUNT",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem6_1 = new GridViewSummaryRowItem { summaryItem6_1 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem6_1);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
                    break;
                case "7": // 7 - รายงานรายละเอียดส่งซองเงิน
                    radLabel_Detail.Text = "DoubleClick ที่รายการ >> พิมพ์ใบส่งซองเงิน "; radLabel_Detail.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000") radLabel_Detail.Text = "รายละเอียดข้อมูลการส่งซองเงิน";

                    radLabel_Date.Text = "ระบุวันที่ส่งเงิน";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDocno", "เลขที่ส่งซอง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMBranch", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Branch_NAME", "ชื่อสาขา", 120)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMLO", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDate", "วันที่ส่ง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMSendCount", "รอบรถ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMCountPack", "จำนวนซอง", 100)));
                    if (_pPermission == "0")
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("MNPMSum", "ยอดเงินรวม")));
                    }
                    else
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMSum", "ยอดเงินรวม", 100)));
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMCar", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMRemark", "หมายเหตุ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMESEND", "ผู้ส่งซอง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDateIn", "เวลาส่ง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UserCar", "พขร.", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDRAWERID", "เลขที่ซองส่งเงิน", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMNameDRAWERID", "แคชเชียร์", 250)));
                    if (_pPermission == "0")
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("MNPMGranchDRAWERID", "ยอดเงิน")));
                    }
                    else
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMGranchDRAWERID", "ยอดเงิน", 100)));
                    }

                    GroupDescriptor descriptor7 = new GroupDescriptor();
                    descriptor7.GroupNames.Add("MNPMDocno", ListSortDirection.Ascending);
                    descriptor7.Format = "รอบส่งเงิน {1}.";
                    this.RadGridView_ShowHD.GroupDescriptors.Add(descriptor7);

                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "4"://รายงานจำนวนซองส่งตามรอบ LO
                    //dt_Data = ARClassA.ReportSendMoneyByLO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlSelect4);
                    dt_Data = Models.ARClass.ReportSendMoneyByLO(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                    {
                        RadGridView_ShowHD.Rows[i].Cells["PATHVDO"].Value = CheckVDO_Receive(RadGridView_ShowHD.Rows[i].Cells["MNPMLO"].Value.ToString());
                    }
                    this.Cursor = Cursors.Default;
                    break;
                case "5"://รายงานการรับซองเงิน
                    //dt_Data = ARClassA.ReportReciveMoney(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect5);
                    dt_Data = Models.ARClass.ReportReciveMoney(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "6"://รายงานซองส่งเงินของ พนักงานบัญชีลูกหนี้
                    //dt_Data = ARClassA.ReportSendMoneyByAR(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlSelect6);
                    dt_Data = Models.ARClass.ReportSendMoneyByAR(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "7":// รายงานรายละเอียดส่งซองเงิน
                    //dt_Data = ARClassA.ReportDetailMoney(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlSelect7);
                    dt_Data = Models.ARClass.ReportDetailMoney(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            radDateTimePicker_D1.MaxDate = radDateTimePicker_D2.Value;
            radDateTimePicker_D2.MinDate = radDateTimePicker_D1.Value;
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;


            if (_pTypeReport == "7")
            {
                if (SystemClass.SystemBranchID == "MN000") return;
                if (RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString() == "") return;

                string sql = $@"SELECT	DRAWERID,TYPE_DESC
                FROM	SHOP_MNSM WITH (NOLOCk)  
                WHERE	MNPM = '{RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value}'";
                DataTable dtMNSV = ConnectionClass.SelectSQL_Main(sql);

                PrintDialog _dai = new PrintDialog();
                if (_dai.ShowDialog(this) == DialogResult.OK)
                {
                    PrintDocument_send.PrintController = printController;
                    printDocument_sta.PrintController = printController;

                    _dai.PrinterSettings.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    PrintDocument_send.PrinterSettings = _dai.PrinterSettings;
                    printDocument_sta.PrinterSettings = _dai.PrinterSettings;
                    PrintDocument_send.Print();

                    if (dtMNSV.Rows.Count > 0)
                    {
                        for (int ik = 0; ik < dtMNSV.Rows.Count; ik++)
                        {
                            headBill = dtMNSV.Rows[ik]["TYPE_DESC"].ToString();
                            printDocument_sta.Print();
                        }
                    }
                }
            }

            if (_pTypeReport == "4")
            {
                if (SystemClass.SystemBranchID != "MN000") return;
                if (RadGridView_ShowHD.CurrentRow.Cells["MNPMLO"].Value.ToString() == "") return;

                if (RadGridView_ShowHD.CurrentRow.Cells["PATHVDO"].Value.ToString() != "") System.Diagnostics.Process.Start(RadGridView_ShowHD.CurrentRow.Cells["PATHVDO"].Value.ToString());

            }

        }
        //Find VDO Receive MNPM
        string CheckVDO_Receive(string lo)
        {
            string path = "";
            DirectoryInfo DirInfo = new DirectoryInfo($@"{PathImageClass.pPathSH_CarCloseNewLO}\{lo}");
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(@"*.MP4", SearchOption.AllDirectories);
                if (Files.Length > 0) path = Files[0].FullName;
            }
            catch (Exception)
            {

            }
            return path;
        }
        //Print
        private void PrintDocument_send_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString();
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            e.Graphics.DrawString("เอกสารส่งซองเงิน [พิมพ์ซ้ำ]", SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["MNPMLO"].Value.ToString() +
                "  ทะเบียน " + RadGridView_ShowHD.CurrentRow.Cells["MNPMCar"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("พขร. " + RadGridView_ShowHD.CurrentRow.Cells["UserCar"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("จำนวน [ซอง] " + RadGridView_ShowHD.CurrentRow.Cells["MNPMCountPack"].Value.ToString(), SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("ผู้ส่ง " + RadGridView_ShowHD.CurrentRow.Cells["NAMESEND"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);

            Y += 20;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };

            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["MNPMRemark"].Value.ToString(), SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        //Pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        private void PrintDocument_sta_PrintPage(object sender, PrintPageEventArgs e)
        {
            int Y = 0;
            Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(headBill, new Font(new FontFamily("Tahoma"), 30), Brushes.Black, rect1, stringFormat);

            Y += 110;
            e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["MNPMLO"].Value.ToString() +
                "  ทะเบียน " + RadGridView_ShowHD.CurrentRow.Cells["MNPMCar"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("พขร. " + RadGridView_ShowHD.CurrentRow.Cells["UserCar"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("ผู้ส่ง " + RadGridView_ShowHD.CurrentRow.Cells["NAMESEND"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }

    }
}
