﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.ComponentModel;

namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class ARMoneyCash_Report : Telerik.WinControls.UI.RadForm
    {
        readonly string _TypeReport;// (LackOfMoney = เงินขาด, DeductedMoney = ยอดหัก , SummaryMoney = สรุปยอดส่งเงินขาด-เกิน )
        readonly string _pTypePermission;// 0 สิทธิ์ทั้งหมด 1 ล็อกเฉพาะสาข  4 คือ มินิมา์ทในส่วน supc
        ExpressionFormattingObject obj02;
        //Load
        public ARMoneyCash_Report(string TypeReport, string pTypePermission)
        {
            InitializeComponent();
            _TypeReport = TypeReport;
            _pTypePermission = pTypePermission;
        }
        //Load
        private void ARMoneyCash_Report_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Begin, DateTime.Now.AddDays(-12), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_End, DateTime.Now.AddDays(-2), DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            RadCheckBox_Branch.Checked = true; RadCheckBox_Branch.Enabled = false;

            if (SystemClass.SystemBranchID == "MN000")
            {
                RadCheckBox_Branch.Checked = false; RadCheckBox_Branch.Enabled = true;
            }
            else
            {
                RadCheckBox_Branch.Checked = true; RadCheckBox_Branch.Enabled = false;
            }

            if (_pTypePermission == "1")
            {
                _ = DatagridClass.SetDropDownList_Branch(RadDropDownList_Branch, "4");
                RadCheckBox_Branch.Checked = true; RadCheckBox_Branch.Enabled = false;

            }
            else _ = DatagridClass.SetDropDownList_Branch(RadDropDownList_Branch, "1,4");

            switch (_TypeReport)
            {
                case "LackOfMoney":
                    radLabel_Detail.Text = "สีแดง >> มียอดเงินขาด";
                    break;
                case "DeductedMoney":
                    radLabel_Detail.Text = "สีแดง >> มียอดหักเงิน";
                    break;
                case "SummaryMoney":
                    RadDateTimePicker_Begin.Value = DateTime.Now.AddDays(-3);
                    RadDateTimePicker_End.Value = DateTime.Now.AddDays(0);

                    radLabel_Detail.Text = "สีแดง >> มียอดเงินขาด | สีเขียว >> มียอดเงินเกิน";

                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Comtransdate", "วันที่", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Comcashierid", "แคชเชียร์", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ แคชเชียร์", 250)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Comcashier_com", "ยอดคอม", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Cashtotal_receivesum", "ยอดแคชเชียร์", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Money", "เงินขาด - เกิน", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("money_deductions", "ส่งหัก", 100)));

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("Comtransdate", ListSortDirection.Ascending);
                    RadGridView_Show.GroupDescriptors.Add(descriptor);
                    DatagridClass.SetCellBackClolorByExpression("Money", $@" Money < 0 ", ConfigClass.SetColor_Red(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression("Money", $@" Money > 0 ", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);


                    break;
                default:
                    break;
            }

        }

        void SetColumns()
        {
            for (int i = 4; i < RadGridView_Show.Columns.Count; i++)
            {
                RadGridView_Show.Columns[RadGridView_Show.Columns[i].Name.ToString()].ConditionalFormattingObjectList.Clear();
            }

            if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 80)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERNAME", "ชื่อ แคชเชียร์", 180)));

            for (var dt = RadDateTimePicker_Begin.Value.Date; dt <= RadDateTimePicker_End.Value.Date; dt = dt.AddDays(1))
            {
                string ColumnName = "F" + dt.ToString("MM-dd").Replace("-", "");
                RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(ColumnName, dt.ToString("MM-dd"), 100)));
                obj02 = new ExpressionFormattingObject("MyCondition1", $@" {ColumnName} < 0 ", false)
                { CellBackColor = ConfigClass.SetColor_Red() };
                RadGridView_Show.Columns[ColumnName].ConditionalFormattingObjectList.Add(obj02);
            }
        }
        //max min date
        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(RadDateTimePicker_Begin, RadDateTimePicker_End);
        }
        //max min date
        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(RadDateTimePicker_Begin, RadDateTimePicker_End);
        }
        //Find Data
        void SetReportLackOfMoney_DeductedMoney()
        {
            SetColumns();

            this.Cursor = Cursors.WaitCursor;
            string bchID = "";
            if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();

            DataTable DtCash = AR_Class.GetCashOfBranch(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                RadDateTimePicker_End.Value.ToString("yyyy-MM-dd"), bchID);
            RadGridView_Show.DataSource = DtCash;
            DtCash.AcceptChanges();

            if (RadGridView_Show.Rows.Count == 0) return;

            DataTable DtMoney = AR_Class.GetMoneyRound(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                RadDateTimePicker_End.Value.ToString("yyyy-MM-dd"), bchID);//, String _TypeReport

            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
            {
                for (int iC = 4; iC < RadGridView_Show.Columns.Count; iC++)
                {
                    DataRow[] dr = DtMoney.Select($@" cashierid = '{RadGridView_Show.Rows[i].Cells["CASHIERID"].Value}' 
                                    AND MMDDtransdate = '{RadGridView_Show.Columns[iC].HeaderText}' ");
                    double grandMoney = 0;
                    if (dr.Length > 0) grandMoney = double.Parse(dr[0][_TypeReport].ToString());

                    RadGridView_Show.Rows[i].Cells["F" + RadGridView_Show.Columns[iC].HeaderText.Replace("-", "")].Value = grandMoney.ToString("N2");

                }
            }

            this.Cursor = Cursors.Default;
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            switch (_TypeReport)
            {
                case "LackOfMoney":
                    SetReportLackOfMoney_DeductedMoney();
                    break;
                case "DeductedMoney":
                    SetReportLackOfMoney_DeductedMoney();
                    break;
                case "SummaryMoney":
                    this.Cursor = Cursors.WaitCursor;
                    string bchID = "";
                    if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
                    RadGridView_Show.DataSource = AR_Class.GetMoneyCash_ByMonth(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                            RadDateTimePicker_End.Value.ToString("yyyy-MM-dd"), bchID);
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }

        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //rows and Font
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //chang Check Bch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _TypeReport);
        }
    }
}
