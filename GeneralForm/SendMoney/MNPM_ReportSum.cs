﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class MNPM_ReportSum : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_DataHD = new DataTable();
        DataTable dt_DataDT = new DataTable();
        
        readonly string _pPermission;

        //Load
        public MNPM_ReportSum(string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load
        private void MNPM_ReportSum_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BCH", "สาขา")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASESELECT", "สาขา/สาย", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("Count_Pack", "ซองค้าง", 80)));
            if (_pPermission == "1")
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดค้าง", 100)));
            }
            else
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("LINEAMOUNT", "ยอดค้าง")));
            }
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastSend", "ส่งล่าสุด", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("THEN25", "> 40 ชม.", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("THEN40", "25>-<40 ชม.", 90)));
            RadGridView_ShowHD.TableElement.RowHeight = 70;
            RadGridView_ShowHD.Columns["BCH"].IsPinned = true;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "LINEAMOUNT > 400000 ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_ShowHD.Columns["CASESELECT"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["Count_Pack"].ConditionalFormattingObjectList.Add(obj1);
            DatagridClass.SetCellBackClolorByExpression("THEN25", "THEN25 > 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("THEN40", "THEN40 > 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

            GridViewSummaryItem summaryItem = new GridViewSummaryItem
            {
                Name = "LINEAMOUNT",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);


            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BCH_NAME", "สาขา", 180)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่สร้าง", 180)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DRAWERID", "รอบส่ง", 180)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CH_NAME", "แคชเชียร์", 180)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 180)));
            if (_pPermission == "1") { RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงิน", 180))); }
            else { RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("LINEAMOUNT", "ยอดเงิน"))); }

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTENAME_Desc", "สายรถ", 250)));
            RadGridView_ShowDT.TableElement.RowHeight = 70;
            RadGridView_ShowDT.Columns["BCH_NAME"].IsPinned = true;

            radRadioButtonElement_Branch.ShowBorder = true;
            radRadioButtonElement_Route.ShowBorder = true;

            SetDGV_HD();
        }

        //Set HD
        void SetDGV_HD()
        {
            if (dt_DataHD.Rows.Count > 0) dt_DataHD.Rows.Clear();
            this.Cursor = Cursors.WaitCursor;

            #region "OldCode"

            //string typeSql_1 = "", typeSql_2 = "";

            //if (radRadioButtonElement_Route.CheckState == CheckState.Checked)
            //{
            //    typeSql_1 = " ROUTEID AS BCH,ROUTEID + CHAR(10) + ROUTENAME AS CASESELECT, ";
            //    typeSql_2 = string.Format(@"LEFT OUTER JOIN  
            //         (
            //         SELECT	ACCOUNTNUM,SPC_ROUTINGPOLICY.ROUTEID,NAME  AS ROUTENAME  
            //         FROM	SHOP2013TMP.dbo.SPC_ROUTINGPOLICY  WITH (NOLOCK)  
            //          INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK)   ON SPC_ROUTINGPOLICY.ROUTEID = SPC_ROUTETABLE.ROUTEID   
            //         WHERE	SPC_ROUTINGPOLICY.DATAAREAID = 'SPC' AND SPC_ROUTETABLE.DATAAREAID = 'SPC' AND SPC_ROUTINGPOLICY.ROUTEID LIKE '9%' 
            //         )TMPROUTE ON TMP_YA.POSGROUP = TMPROUTE.ACCOUNTNUM  
            //         GROUP BY ROUTEID,ROUTENAME  ORDER BY ROUTEID");
            //}
            //if (radRadioButtonElement_Branch.CheckState == CheckState.Checked)
            //{
            //    typeSql_1 = " POSGROUP AS BCH,POSGROUP + CHAR(10) + BRANCH_NAME AS CASESELECT , ";
            //    typeSql_2 = " GROUP BY POSGROUP,BRANCH_NAME  ORDER BY POSGROUP,BRANCH_NAME ";
            //}

            //string sqlSelect = string.Format(@"
            // SELECT	" + typeSql_1 + @"COUNT(POSGROUP) AS Count_Pack,SUM(LINEAMOUNT) AS LINEAMOUNT,SUM(THEN25) AS THEN25,SUM(THEN40) AS THEN40 FROM	( 

            // SELECT CASE POSGROUP WHEN 'MN098' THEN 'MN004' WHEN 'MN998' THEN 'MN039' ELSE POSGROUP END AS POSGROUP,
            //        CASE POSGROUP WHEN 'MN098' THEN 'นาคา' WHEN 'MN998' THEN 'เชิงทะเล' ELSE BRANCH_NAME END AS BRANCH_NAME,
            //        XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT,   
            //  CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) > 40 THEN 1 ELSE 0 END AS THEN40, 
            //  CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) BETWEEN 25 AND 40 THEN 1 ELSE 0 END AS THEN25  
            // FROM	SHOP2013TMP.dbo.XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
            //  INNER JOIN  SHOP_BRANCH WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.POSGROUP = SHOP_BRANCH.BRANCH_ID  
            //  LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
            // WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPC'     
            //         AND POSGROUP NOT IN ('RT','MN997')  
            //         AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)   
            //         AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'  

            // UNION  

            // SELECT	 BranchID AS POSGROUP, BRANCHNAME,DRAWERID  ,LINEAMOUNT   
            //  ,CASE WHEN DATEDIFF(hh,TRANSDATETIME,GETDATE()) > 40 THEN 1 ELSE 0 END AS THEN40, 
            //  CASE WHEN DATEDIFF(hh,TRANSDATETIME,GETDATE()) BETWEEN 25 AND 40 THEN 1 ELSE 0 END AS THEN25  
            // FROM	SHOP_MNSM WITH (NOLOCk)     WHERE	SendSta = 0   

            // UNION 

            // SELECT	CASE POSGROUP WHEN 'MYA' THEN 'MN046' WHEN 'MYA30' THEN 'MN030' WHEN 'MYA38' THEN 'MN038' END AS POSGROUP,
            //  CASE POSGROUP WHEN 'MYA' THEN 'ร้านยา ถนนวิเศษ'  WHEN 'MYA30' THEN 'ร้านยา นาใน' WHEN 'MYA38' THEN  'ร้านยา ท่าเรือ' END AS BRANCH_NAME,
            //  XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT   
            //  ,CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) > 40 THEN 1 ELSE 0 END AS THEN40, 
            //  CASE WHEN DATEDIFF(hh,CREATEDDATETIME,GETDATE()) BETWEEN 25 AND 40 THEN 1 ELSE 0 END AS THEN25  
            // from	XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
            //  LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID   
            // WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPM'    
            //  AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)    
            //  AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'   

            // )TMP_YA    " + typeSql_2 + @"
            //");
            #endregion

            string pCase = "0";
            if (radRadioButtonElement_Route.CheckState == CheckState.Checked) pCase = "1";

            //dt_DataHD = ARClassA.GetDRAWERNotSend_ByCondition(pCase); //ConnectionClass.SelectSQL_Main(sqlSelect);
            dt_DataHD = Models.ARClass.GetDRAWERNotSend_ByCondition(pCase);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_DataHD;
            dt_DataHD.AcceptChanges();

            if (radRadioButtonElement_Branch.CheckState == CheckState.Checked)
            {
                for (int i = 0; i < dt_DataHD.Rows.Count; i++)
                {
                    RadGridView_ShowHD.Rows[i].Cells["LastSend"].Value = AR_Class.GetLastSend_ByBchID(dt_DataHD.Rows[i]["BCH"].ToString());
                }

                RadGridView_ShowHD.Columns["LastSend"].IsVisible = true;
            }
            else
            {
                RadGridView_ShowHD.Columns["LastSend"].IsVisible = false;
            }

            this.Cursor = Cursors.Default;
        }
        //Set DT
        void SetDGV_DT(string pstr)
        {

            this.Cursor = Cursors.WaitCursor;

            #region "OldCode"
            //string pCon = "";
            //if (radRadioButtonElement_Branch.CheckState == CheckState.Checked)
            //{
            //    pCon = " WHERE	POSGROUP = '" + pstr + @"' ";
            //}
            //if (radRadioButtonElement_Route.CheckState == CheckState.Checked)
            //{
            //    pCon = " WHERE	ROUTEID = '" + pstr + @"' ";
            //}

            //string sql = string.Format(@"
            //        SELECT	TMP_YA.*,TMPROUTE.*,ROUTEID+char(10)+NAME  AS ROUTENAME_Desc,EMPLID + char(10) + NAME AS CH_NAME  FROM	( 

            //             SELECT CASE POSGROUP WHEN 'MN098' THEN 'MN004' WHEN 'MN998' THEN 'MN039' ELSE POSGROUP END AS POSGROUP,
            //              CASE POSGROUP WHEN 'MN098' THEN 'นาคา' WHEN 'MN998' THEN 'เชิงทะเล' ELSE BRANCH_NAME END AS BRANCH_NAME, 
            //                    CASE POSGROUP WHEN 'MN098' THEN 'MN004' WHEN 'MN998' THEN 'MN039' ELSE POSGROUP END + CHAR(10) + 
            //                    BRANCH_NAME AS BCH_NAME,
            //              XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT,
            //              CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25) as CREATEDDATETIME,
            //              XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK   
            //              ,DATEDIFF(hour,GETDATE(),CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25))*-1  AS DIFF_A

            //             FROM	SHOP2013TMP.dbo.XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
            //              INNER JOIN  SHOP_BRANCH WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.POSGROUP = SHOP_BRANCH.BRANCH_ID  
            //              LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID  
            //             WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPC'     
            //               AND POSGROUP NOT IN ('RT')  
            //               AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)   
            //               AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'  
            //             UNION  

            //             SELECT	BranchID AS POSGROUP, BRANCHNAME,
            //                    BranchID + CHAR(10) + BRANCHNAME AS BCH_NAME,
            //                    DRAWERID  ,LINEAMOUNT   ,
            //              CONVERT(VARCHAR,TRANSDATETIME,25) as CREATEDDATETIME,
            //              EMPLID,EmplName AS NAME, REMARK,DATEDIFF(hour,GETDATE(),CONVERT(VARCHAR,TRANSDATETIME,25))*-1  AS DIFF_A
            //             FROM	SHOP_MNSM WITH (NOLOCk)     WHERE	SendSta = 0   

            //             UNION 

            //             SELECT	CASE POSGROUP WHEN 'MYA' THEN 'MN046' WHEN 'MYA30' THEN 'MN030' WHEN 'MYA38' THEN 'MN038' END AS POSGROUP,
            //              CASE POSGROUP WHEN 'MYA' THEN 'ร้านยา ถนนวิเศษ'  WHEN 'MYA30' THEN 'ร้านยา นาใน' WHEN 'MYA38' THEN  'ร้านยา ท่าเรือ' END AS BRANCH_NAME,
            //                    CASE POSGROUP WHEN 'MYA' THEN 'MN046' WHEN 'MYA30' THEN 'MN030' WHEN 'MYA38' THEN 'MN038' END + CHAR(10) + CASE POSGROUP WHEN 'MYA' THEN 'ร้านยา ถนนวิเศษ'  WHEN 'MYA30' THEN 'ร้านยา นาใน' WHEN 'MYA38' THEN  'ร้านยา ท่าเรือ' END  AS BCH_NAME,
            //              XXX_POSDRAWERCYCLETABLE.DRAWERID,CASHAMOUNT AS LINEAMOUNT  , 
            //              CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25) as CREATEDDATETIME,
            //              XXX_POSDRAWERCYCLETABLE.EMPLID, XXX_POSDRAWERCYCLETABLE.NAME, XXX_POSDRAWERCYCLETABLE.REMARK 
            //              ,DATEDIFF(hour,GETDATE(),CONVERT(VARCHAR,XXX_POSDRAWERCYCLETABLE.CREATEDDATETIME,25))*-1  AS DIFF_A

            //             from	XXX_POSDRAWERCYCLETABLE WITH (NOLOCK)   
            //              LEFT OUTER JOIN SHOP_MNPM_DT WITH (NOLOCK) ON XXX_POSDRAWERCYCLETABLE.DRAWERID = SHOP_MNPM_DT.MNPMDRAWERID   
            //             WHERE	XXX_POSDRAWERCYCLETABLE.DATAAREAID = 'SPM'    
            //              AND XXX_POSDRAWERCYCLETABLE.TRANSDATE BETWEEN Convert(varchar,GETDATE()-30,23) and Convert(varchar,getdate(),23)    
            //              AND ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,0) = '0'   



            //             )TMP_YA  

            //        LEFT OUTER JOIN  
            //        (

            //        SELECT	ACCOUNTNUM,SPC_ROUTINGPOLICY.ROUTEID,NAME  AS ROUTENAME  
            //        FROM	SHOP2013TMP.dbo.SPC_ROUTINGPOLICY  WITH (NOLOCK)  
            //          INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK)   ON SPC_ROUTINGPOLICY.ROUTEID = SPC_ROUTETABLE.ROUTEID   
            //        WHERE	SPC_ROUTINGPOLICY.DATAAREAID = 'SPC' AND SPC_ROUTETABLE.DATAAREAID = 'SPC' AND SPC_ROUTINGPOLICY.ROUTEID LIKE '9%' AND ISPRIMARY= '1'
            //        )TMPROUTE ON TMP_YA.POSGROUP = TMPROUTE.ACCOUNTNUM  

            //       " + pCon + @"

            //        ORDER BY POSGROUP ");



            #endregion

            string pCon = "";
            if (radRadioButtonElement_Branch.CheckState == CheckState.Checked) pCon = $@" WHERE	POSGROUP = '{pstr}' ";
            if (radRadioButtonElement_Route.CheckState == CheckState.Checked) pCon = $@" WHERE	ROUTEID = '{pstr}' ";

            //dt_DataDT = ARClassA.GetDetailDRAWERNotSend_ByCondition(pCon); //ConnectionClass.SelectSQL_Main(sql);
            dt_DataDT = Models.ARClass.GetDetailDRAWERNotSend_ByCondition(pCon);
            RadGridView_ShowDT.DataSource = dt_DataDT;
            dt_DataDT.AcceptChanges();

            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion
        //OK
        private void RadRadioButtonElement_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //ok
        private void RadRadioButtonElement_Route_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Change
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0)
            {
                if (dt_DataDT.Rows.Count > 0) { dt_DataDT.Rows.Clear(); dt_DataDT.AcceptChanges(); }
                return;
            }
            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;
            if (RadGridView_ShowHD.CurrentRow.Cells["BCH"].Value.ToString() == "") return;

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BCH"].Value.ToString());
        }
        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("ซองส่งเงินค้างส่ง", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}

