﻿namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    partial class ARSendMoney_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ARSendMoney_Type));
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.RadLabel_Total = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_Coins = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_20 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_50 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_100 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_500 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_1000 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Coins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_500)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_1000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadButton_Cancel);
            this.panel1.Controls.Add(this.RadButton_Save);
            this.panel1.Controls.Add(this.RadLabel_Total);
            this.panel1.Controls.Add(this.radLabel13);
            this.panel1.Controls.Add(this.radLabel12);
            this.panel1.Controls.Add(this.radLabel10);
            this.panel1.Controls.Add(this.RadTextBox_Coins);
            this.panel1.Controls.Add(this.radLabel11);
            this.panel1.Controls.Add(this.radLabel8);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.RadTextBox_20);
            this.panel1.Controls.Add(this.radLabel9);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.RadTextBox_50);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.RadTextBox_100);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadTextBox_500);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.RadTextBox_1000);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 305);
            this.panel1.TabIndex = 0;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(175, 261);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(100, 32);
            this.RadButton_Cancel.TabIndex = 88;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(61, 261);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(100, 32);
            this.RadButton_Save.TabIndex = 87;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadLabel_Total
            // 
            this.RadLabel_Total.AutoSize = false;
            this.RadLabel_Total.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel_Total.ForeColor = System.Drawing.Color.Blue;
            this.RadLabel_Total.Location = new System.Drawing.Point(122, 220);
            this.RadLabel_Total.Name = "RadLabel_Total";
            this.RadLabel_Total.Size = new System.Drawing.Size(128, 19);
            this.RadLabel_Total.TabIndex = 86;
            this.RadLabel_Total.Text = "00.00";
            this.RadLabel_Total.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.RadLabel_Total.TextWrap = false;
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel13.ForeColor = System.Drawing.Color.Blue;
            this.radLabel13.Location = new System.Drawing.Point(256, 220);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(33, 19);
            this.radLabel13.TabIndex = 85;
            this.radLabel13.Text = "บาท";
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel12.ForeColor = System.Drawing.Color.Blue;
            this.radLabel12.Location = new System.Drawing.Point(54, 220);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(57, 19);
            this.radLabel12.TabIndex = 83;
            this.radLabel12.Text = "ยอดรวม";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(256, 184);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(33, 19);
            this.radLabel10.TabIndex = 84;
            this.radLabel10.Text = "บาท";
            // 
            // RadTextBox_Coins
            // 
            this.RadTextBox_Coins.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_Coins.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Coins.Location = new System.Drawing.Point(122, 178);
            this.RadTextBox_Coins.Name = "RadTextBox_Coins";
            this.RadTextBox_Coins.Size = new System.Drawing.Size(128, 25);
            this.RadTextBox_Coins.TabIndex = 6;
            this.RadTextBox_Coins.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RadTextBox_Coins.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Coins_TextChanging);
            this.RadTextBox_Coins.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Coins_KeyDown);
            this.RadTextBox_Coins.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Coins_KeyUp);
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(54, 183);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(50, 19);
            this.radLabel11.TabIndex = 82;
            this.radLabel11.Text = "เหรียญ";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(256, 153);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(22, 19);
            this.radLabel8.TabIndex = 81;
            this.radLabel8.Text = "ใบ";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(256, 122);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(22, 19);
            this.radLabel6.TabIndex = 81;
            this.radLabel6.Text = "ใบ";
            // 
            // RadTextBox_20
            // 
            this.RadTextBox_20.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_20.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_20.Location = new System.Drawing.Point(122, 147);
            this.RadTextBox_20.Name = "RadTextBox_20";
            this.RadTextBox_20.Size = new System.Drawing.Size(128, 25);
            this.RadTextBox_20.TabIndex = 5;
            this.RadTextBox_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RadTextBox_20.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_20_TextChanging);
            this.RadTextBox_20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_20_KeyDown);
            this.RadTextBox_20.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_20_KeyUp);
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(54, 152);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(24, 19);
            this.radLabel9.TabIndex = 79;
            this.radLabel9.Text = "20";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(256, 91);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(22, 19);
            this.radLabel4.TabIndex = 81;
            this.radLabel4.Text = "ใบ";
            // 
            // RadTextBox_50
            // 
            this.RadTextBox_50.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_50.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_50.Location = new System.Drawing.Point(122, 116);
            this.RadTextBox_50.Name = "RadTextBox_50";
            this.RadTextBox_50.Size = new System.Drawing.Size(128, 25);
            this.RadTextBox_50.TabIndex = 4;
            this.RadTextBox_50.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RadTextBox_50.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_50_TextChanging);
            this.RadTextBox_50.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_50_KeyDown);
            this.RadTextBox_50.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_50_KeyUp);
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(54, 121);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(24, 19);
            this.radLabel7.TabIndex = 79;
            this.radLabel7.Text = "50";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(256, 60);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(22, 19);
            this.radLabel2.TabIndex = 81;
            this.radLabel2.Text = "ใบ";
            // 
            // RadTextBox_100
            // 
            this.RadTextBox_100.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_100.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_100.Location = new System.Drawing.Point(122, 85);
            this.RadTextBox_100.Name = "RadTextBox_100";
            this.RadTextBox_100.Size = new System.Drawing.Size(128, 25);
            this.RadTextBox_100.TabIndex = 3;
            this.RadTextBox_100.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RadTextBox_100.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_100_TextChanging);
            this.RadTextBox_100.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_100_KeyDown);
            this.RadTextBox_100.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_100_KeyUp);
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(54, 90);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(32, 19);
            this.radLabel5.TabIndex = 79;
            this.radLabel5.Text = "100";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(256, 29);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(22, 19);
            this.radLabel1.TabIndex = 78;
            this.radLabel1.Text = "ใบ";
            // 
            // RadTextBox_500
            // 
            this.RadTextBox_500.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_500.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_500.Location = new System.Drawing.Point(122, 54);
            this.RadTextBox_500.Name = "RadTextBox_500";
            this.RadTextBox_500.Size = new System.Drawing.Size(128, 25);
            this.RadTextBox_500.TabIndex = 2;
            this.RadTextBox_500.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RadTextBox_500.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_500_TextChanging);
            this.RadTextBox_500.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_500_KeyDown);
            this.RadTextBox_500.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_500_KeyUp);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(54, 59);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(32, 19);
            this.radLabel3.TabIndex = 79;
            this.radLabel3.Text = "500";
            // 
            // RadTextBox_1000
            // 
            this.RadTextBox_1000.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_1000.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_1000.Location = new System.Drawing.Point(122, 23);
            this.RadTextBox_1000.Name = "RadTextBox_1000";
            this.RadTextBox_1000.Size = new System.Drawing.Size(128, 25);
            this.RadTextBox_1000.TabIndex = 1;
            this.RadTextBox_1000.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RadTextBox_1000.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_1000_TextChanging);
            this.RadTextBox_1000.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_1000_KeyDown);
            this.RadTextBox_1000.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_1000_KeyUp);
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(54, 28);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(41, 19);
            this.radLabel_Date.TabIndex = 77;
            this.radLabel_Date.Text = "1000";
            // 
            // ARSendMoney_Type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.RadButton_Cancel;
            this.ClientSize = new System.Drawing.Size(351, 305);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ARSendMoney_Type";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุจำนวนเงิน";
            this.Load += new System.EventHandler(this.ARSendMoney_Type_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ARSendMoney_Type_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Coins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_500)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_1000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_1000;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Coins;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_20;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_50;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_100;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_500;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel RadLabel_Total;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
    }
}
