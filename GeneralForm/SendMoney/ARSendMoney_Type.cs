﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;


namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class ARSendMoney_Type : Telerik.WinControls.UI.RadForm
    {
        public Double Total { get; set; } = 0;
        public Double Bank1000 { get; set; } = 0;
        public Double Bank500 { get; set; } = 0;
        public Double Bank100 { get; set; } = 0;
        public Double Bank50 { get; set; } = 0;
        public Double Bank20 { get; set; } = 0;
        public Double Coins { get; set; } = 0;
        string PCase { get; set; } = "";
        string Sum { get; set; } = "";

        //Load
        public ARSendMoney_Type(Double _Bank1000, Double _Bank500, Double _Bank100, Double _Bank50, Double _Bank20, Double _Coins, string _pCase = "", string _sum = "")
        {
            InitializeComponent();
            Bank1000 = _Bank1000;
            Bank500 = _Bank500;
            Bank100 = _Bank100;
            Bank50 = _Bank50;
            Bank20 = _Bank20;
            Coins = _Coins;
            PCase = _pCase;
            Sum = _sum;
        }

        //Load
        private void ARSendMoney_Type_Load(object sender, EventArgs e)
        {
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            if (PCase == "1") RadButton_Save.Enabled = false;

            RadTextBox_1000.Text = Bank1000.ToString();
            RadTextBox_500.Text = Bank500.ToString();
            RadTextBox_100.Text = Bank100.ToString();
            RadTextBox_50.Text = Bank50.ToString();
            RadTextBox_20.Text = Bank20.ToString();
            RadTextBox_Coins.Text = Coins.ToString();
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }


        private void SumMoney(RadTextBox _Bank1000, RadTextBox _Bank500, RadTextBox _Bank100,
            RadTextBox _Bank50, RadTextBox _Bank20, RadTextBox _Coins)
        {
            if (_Bank1000.Text == "") Bank1000 = 0; else Bank1000 = Double.Parse(_Bank1000.Text);
            if (_Bank500.Text == "") Bank500 = 0; else Bank500 = Double.Parse(_Bank500.Text);
            if (_Bank100.Text == "") Bank100 = 0; else Bank100 = Double.Parse(_Bank100.Text);
            if (_Bank50.Text == "") Bank50 = 0; else Bank50 = Double.Parse(_Bank50.Text);
            if (_Bank20.Text == "") Bank20 = 0; else Bank20 = Double.Parse(_Bank20.Text);
            if (_Coins.Text == "") Coins = 0; else Coins = Double.Parse(_Coins.Text);

            Total = (Bank1000 * 1000) + (Bank500 * 500) + (Bank100 * 100) + (Bank50 * 50) + (Bank20 * 20) + Coins;
            RadLabel_Total.Text = Total.ToString("N2");
        }

        private void RadTextBox_1000_KeyUp(object sender, KeyEventArgs e)
        {
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }

        private void RadTextBox_500_KeyUp(object sender, KeyEventArgs e)
        {
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }

        private void RadTextBox_100_KeyUp(object sender, KeyEventArgs e)
        {
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }

        private void RadTextBox_50_KeyUp(object sender, KeyEventArgs e)
        {
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }

        private void RadTextBox_20_KeyUp(object sender, KeyEventArgs e)
        {
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }

        private void RadTextBox_Coins_KeyUp(object sender, KeyEventArgs e)
        {
            SumMoney(RadTextBox_1000, RadTextBox_500, RadTextBox_100, RadTextBox_50, RadTextBox_20, RadTextBox_Coins);
        }

        private void RadTextBox_1000_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_1000.Text == "") RadTextBox_1000.Text = "0";
                RadTextBox_500.SelectAll();
                RadTextBox_500.Focus();
            }
        }

        private void RadTextBox_500_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_500.Text == "") RadTextBox_500.Text = "0";
                RadTextBox_100.SelectAll();
                RadTextBox_100.Focus();
            }
        }

        private void RadTextBox_100_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_100.Text == "") RadTextBox_100.Text = "0";
                RadTextBox_50.SelectAll();
                RadTextBox_50.Focus();
            }
        }

        private void RadTextBox_50_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_50.Text == "") RadTextBox_50.Text = "0";
                RadTextBox_20.SelectAll();
                RadTextBox_20.Focus();
            }
        }

        private void RadTextBox_20_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_20.Text == "") RadTextBox_20.Text = "0";
                RadTextBox_Coins.SelectAll();
                RadTextBox_Coins.Focus();
            }
        }

        private void RadTextBox_Coins_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (PCase != "1") RadButton_Save.Focus();
                else
                {
                    if (Total != double.Parse(Sum))
                    {
                        RadButton_Save.Enabled = false;
                        RadButton_Cancel.Focus();
                    }
                    else
                    {
                        RadButton_Save.Enabled = true;
                        RadButton_Save.Focus();
                    }
                }
                
            }
        }

        private void RadTextBox_1000_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_500_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_100_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_50_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_20_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_Coins_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        private void ARSendMoney_Type_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.DialogResult = DialogResult.No;
            }
        }
    }
}
