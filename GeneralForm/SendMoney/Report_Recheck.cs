﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;
using System.Collections;
using System.Diagnostics;

namespace PC_Shop24Hrs.GeneralForm.SendMoney
{
    public partial class Report_Recheck : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pPermission; // 0 ไม่มีสิด 1 มีสิด
        readonly string _pTypeReport;//ประเภทรายงาน

        // 0 รายงานการส่งซองเงิน [VDO]
        // 1 รายงานการตรวจ VDO หน้าร้าน
        // 2 รายงานการโอนเงินเข้าบัญชี 

        int pInsert;
        //Load
        public Report_Recheck(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void Report_Recheck_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Clear.ShowBorder = true; radButtonElement_Clear.ToolTipText = "ล้างข้อมูล";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "บันทึกบิล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;

            if (_pPermission == "0") radButtonElement_add.Enabled = false; else radButtonElement_add.Enabled = true;

            switch (_pTypeReport)
            {
                case "0":

                    if (_pPermission == "0") radLabel_Detail.Text = "สีเหลือง >> ไม่บันทึกรายการ | สีแดง >> ไม่มี VDO | สีฟ้า >> ยังไม่บันทึกรายละเอียด | สีชมพู >> ตรวจเรียบร้อย | DoubleClick VDO >> ดู ";
                    else radLabel_Detail.Text = "สีเหลือง >> ไม่บันทึกรายการ | สีแดง >> ไม่มี VDO | สีฟ้า >> ยังไม่บันทึกรายละเอียด | สีชมพู >> ตรวจเรียบร้อย | DoubleClick VDO >> ดู | DoubleClick ช่องเลขที่หรือรายละเอียดการตรวจ >> บันทึกตรวจ";


                    radLabel_Detail.Visible = true;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("VDO", "VDO", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMBranch", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDocno", "เลขที่ส่งซอง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDate", "วันที่", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMLO", "LO", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMCountPack", "จำนวนซอง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserCar", "พขร.", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMCar", "ทะเบียนรถ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserCode", "ผู้ส่งซอง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMESEND", "ผู้ถ่าย VDO", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMRemark", "หมายเหตุ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "รายละเอียดการตรวจ", 450)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Path", "Path")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVOICEID", "INVOICEID")));
                    RadGridView_ShowHD.TableElement.RowHeight = 50;
                    //Freeze Column
                    for (int i = 0; i < 4; i++)
                    {
                        this.RadGridView_ShowHD.MasterTemplate.Columns[i].IsPinned = true;
                    }
                    DatagridClass.SetCellBackClolorByExpression("MNPMDocno", "RMK = '' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNPMDocno", "RMK <> '' ", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNPMDocno", " INVOICEID = '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("VDO", " VDO = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    break;
                case "1":
                    if (_pPermission == "0") radLabel_Detail.Text = "สีเหลือง >> ไม่บันทึกรายการ | สีแดง >> ไม่มี VDO | สีฟ้า >> ยังไม่บันทึกรายละเอียด | สีชมพู >> ตรวจเรียบร้อย | DoubleClick VDO >> ดู ";
                    else radLabel_Detail.Text = "สีเหลือง >> ไม่บันทึกรายการ | สีแดง >> ไม่มี VDO | สีฟ้า >> ยังไม่บันทึกรายละเอียด | สีชมพู >> ตรวจเรียบร้อย | DoubleClick VDO >> ดู | DoubleClick ช่องเลขที่หรือรายละเอียดการตรวจ >> บันทึกตรวจ";


                    radLabel_Detail.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("VDO", "VDO", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Datebill", "วันที่", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMESEND", "ผู้ถ่าย VDO", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "รายละเอียดการตรวจ", 450)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddHyperlinkColumn_AddManual("Path", "VDO", 400)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVOICEID", "INVOICEID")));
                    RadGridView_ShowHD.TableElement.RowHeight = 50;

                    //Freeze Column
                    for (int i = 0; i < 3; i++)
                    {
                        this.RadGridView_ShowHD.MasterTemplate.Columns[i].IsPinned = true;
                    }
                    DatagridClass.SetCellBackClolorByExpression("RMK", "RMK = '' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("RMK", "RMK <> '' ", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("RMK", " INVOICEID = '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("VDO", " VDO = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    break;

                case "2":

                    if (_pPermission == "0") radLabel_Detail.Text = "สีเหลือง >> ไม่บันทึกรายการ | สีแดง >> ไม่มีรูป | สีฟ้า >> ยังไม่บันทึกรายละเอียด | สีชมพู >> ตรวจเรียบร้อย | DoubleClick VDO >> ดูรูปขนาดใหญ่ ";
                    else radLabel_Detail.Text = "สีเหลือง >> ไม่บันทึกรายการ | สีแดง >> ไม่มีรูป | สีฟ้า >> ยังไม่บันทึกรายละเอียด | สีชมพู >> ตรวจเรียบร้อย | DoubleClick VDO >> ดูรูปขนาดใหญ่ | DoubleClick ช่องเลขที่หรือรายละเอียดการตรวจ >> บันทึกตรวจ";

                    radLabel_Detail.Visible = true;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMBranch", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDocno", "เลขที่ส่งซอง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDate", "วันที่", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMLO", "สถานะ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("MNPMSum", "ยอดรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMUserCode", "ผู้ส่งซอง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMESEND", "ผู้ถ่าย VDO", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMRemark", "หมายเหตุ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RMK", "รายละเอียดการตรวจ", 450)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Path", "Path")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVOICEID", "INVOICEID")));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("ImageC", "รูปใบโอน", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImageC", "CountImageC")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImageC", "PathImageC")));

                    DatagridClass.SetCellBackClolorByExpression("MNPMDocno", "RMK = '' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNPMDocno", "RMK <> '' ", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNPMDocno", " INVOICEID = '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("ImageC", " CountImageC = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    RadGridView_ShowHD.TableElement.RowHeight = 150;
                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            string pConBch = "";

            switch (_pTypeReport)
            {
                case "0":
                    this.Cursor = Cursors.WaitCursor;
                    if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
                    //if (SystemClass.SystemBranchID != "MN000") pConBch = " AND MNPMBranch = '" + SystemClass.SystemBranchID + @"'";

                    //string sqlSelect = string.Format(@"
                    // SELECT	MNPMBranch,BRANCH_NAME,MNPMDocno,CASE MNPMLO WHEN '1' THEN 'ฝากเงินเข้าธนาคาร' ELSE MNPMLO END AS MNPMLO,
                    //        CONVERT(VARCHAR,MNPMDate,23) AS MNPMDate,MNPMTime,MNPMUserCode + '-' + A.SPC_NAME AS MNPMUserCode,MNPMCountPack,
                    //  MNPMUserCar + '-' + B.SPC_NAME AS MNPMUserCar,MNPMCar,MNPMRemark,ISNULL(RMK,'') AS RMK,ISNULL(TMP.INVOICEID,'0') AS INVOICEID 
                    // FROM	SHOP_MNPM_HD WITH (NOLOCK)   
                    //  INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMBranch = SHOP_BRANCH.BRANCH_ID  
                    //        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE A WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCode = A.EMPLID AND A.DATAAREAID = N'SPC'
                    //  LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE B WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCar = B.EMPLID AND B.DATAAREAID = N'SPC'
                    //  LEFT OUTER JOIN 
                    //  ( SELECT	INVOICEID,ITEMBARCODE,DESCRIPTION  AS RMK
                    //   FROM	Shop_RecheckReciveItem WITH (NOLOCK)    	     
                    //   WHERE	Datebill = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"'     AND VAN = 'MNPM' AND SHIPMENTID IS NULL  )TMP ON SHOP_MNPM_HD.MNPMDocno = TMP.INVOICEID
                    // WHERE	MNPMDATE = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"'   " + pConBch + @"
                    //        AND MNPMLO != '1'
                    // ORDER BY MNPMDocno
                    //");

                    dt_Data = Models.EmplClass.MNPM_SendMoneyForRecheck("0",radDateTimePicker_D1.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlSelect);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        string dateIns = dt_Data.Rows[i]["MNPMDate"].ToString();
                        string bch = dt_Data.Rows[i]["MNPMBranch"].ToString();
                        string docno = dt_Data.Rows[i]["MNPMDocno"].ToString();

                        DataTable vdo = LoadVDO0(dateIns, bch, docno);
                        int pVdo = 0;
                        string pWho = "", pPath = "";
                        if (vdo.Rows.Count > 0)
                        {
                            pVdo = vdo.Rows.Count;
                            pWho = vdo.Rows[vdo.Rows.Count - 1]["DATA_DESC"].ToString();
                            pPath = vdo.Rows[vdo.Rows.Count - 1]["DATA_ID"].ToString();
                        }

                        RadGridView_ShowHD.Rows[i].Cells["VDO"].Value = pVdo;
                        RadGridView_ShowHD.Rows[i].Cells["NAMESEND"].Value = pWho;
                        RadGridView_ShowHD.Rows[i].Cells["Path"].Value = pPath;
                    }

                    SetStaInsert("MNPM", " AND SHIPMENTID  IS NULL ");
                    if (pInsert > 0)
                    {
                        radButtonElement_add.Enabled = false;
                    }
                    else
                    {
                        if (_pPermission == "1") radButtonElement_add.Enabled = true; else radButtonElement_add.Enabled = false;
                    }

                    this.Cursor = Cursors.Default;
                    break;
                case "1":
                    this.Cursor = Cursors.WaitCursor;
                    if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        pConBch = " AND BRANCH_ID = '" + SystemClass.SystemBranchID + @"'";
                    }
                    string sqlSelect1 = string.Format(@"
                    SELECT	'" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AS Datebill,BRANCH_ID,BRANCH_NAME,ISNULL(INVOICEID,'0') AS INVOICEID,BRANCH,RMK
                    FROM	SHOP_BRANCH WITH (NOLOCK) 
		                    LEFT OUTER JOIN
			                    (SELECT	INVOICEID,BRANCH,DESCRIPTION  AS RMK
			                    FROM	Shop_RecheckReciveItem WITH (NOLOCK)    	     
			                    WHERE	Datebill = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND VAN = 'VDO' )TMP ON SHOP_BRANCH.BRANCH_ID = TMP.BRANCH
                    WHERE	BRANCH_STA IN ('1') and  BRANCH_STAOPEN IN ('1')  " + pConBch + @"
                    ORDER BY BRANCH_ID 
                    ");

                    dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect1);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        string dateIns = dt_Data.Rows[i]["Datebill"].ToString();
                        string bch = dt_Data.Rows[i]["BRANCH_ID"].ToString();

                        DataTable vdo = LoadVDO1(dateIns, bch);
                        int pVdo = 0;
                        string pWho = "", pPath = "";
                        if (vdo.Rows.Count > 0)
                        {
                            pVdo = vdo.Rows.Count;
                            pWho = vdo.Rows[vdo.Rows.Count - 1]["DATA_DESC"].ToString();
                            pPath = vdo.Rows[vdo.Rows.Count - 1]["DATA_ID"].ToString();
                        }

                        RadGridView_ShowHD.Rows[i].Cells["VDO"].Value = pVdo;
                        RadGridView_ShowHD.Rows[i].Cells["NAMESEND"].Value = pWho;
                        RadGridView_ShowHD.Rows[i].Cells["Path"].Value = pPath;
                    }

                    SetStaInsert("VDO", "");
                    if (pInsert > 0)
                    { radButtonElement_add.Enabled = false; }
                    else { radButtonElement_add.Enabled = true; }

                    this.Cursor = Cursors.Default;

                    break;
                case "2":
                    this.Cursor = Cursors.WaitCursor;
                    if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }
                    //if (SystemClass.SystemBranchID != "MN000")
                    //{
                    //    pConBch = " AND MNPMBranch = '" + SystemClass.SystemBranchID + @"'";
                    //}

                    //string sqlSelect2 = string.Format(@"
                    // SELECT	MNPMBranch,BRANCH_NAME,MNPMDocno,CASE MNPMLO WHEN '1' THEN 'ฝากเงินเข้าธนาคาร' ELSE MNPMLO END AS MNPMLO,
                    //        CONVERT(VARCHAR,MNPMDate,23) AS MNPMDate,MNPMTime,MNPMUserCode + CHAR(10) + A.SPC_NAME AS MNPMUserCode,MNPMCountPack,MNPMSum,
                    //  MNPMUserCar + '-' + B.SPC_NAME AS MNPMUserCar,MNPMCar,MNPMRemark,ISNULL(RMK,'') AS RMK,ISNULL(TMP.INVOICEID,'0') AS INVOICEID 
                    // FROM	SHOP_MNPM_HD WITH (NOLOCK)   
                    //  INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMBranch = SHOP_BRANCH.BRANCH_ID  
                    //        INNER JOIN SHOP2013TMP.dbo.EMPLTABLE A WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCode = A.EMPLID AND A.DATAAREAID = N'SPC'
                    //  LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE B WITH (NOLOCK) ON SHOP_MNPM_HD.MNPMUserCar = B.EMPLID AND B.DATAAREAID = N'SPC'
                    //  LEFT OUTER JOIN 
                    //  ( SELECT	INVOICEID,ITEMBARCODE,DESCRIPTION  AS RMK
                    //   FROM	Shop_RecheckReciveItem WITH (NOLOCK)    	     
                    //   WHERE	Datebill = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"'     AND VAN = 'MNPM' AND SHIPMENTID  = '1'  )TMP ON SHOP_MNPM_HD.MNPMDocno = TMP.INVOICEID
                    // WHERE	MNPMDATE = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"'   " + pConBch + @"
                    //        AND MNPMLO = '1'
                    // ORDER BY MNPMDocno
                    //");

                    dt_Data = Models.EmplClass.MNPM_SendMoneyForRecheck("2", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlSelect2);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        string dateIns = dt_Data.Rows[i]["MNPMDate"].ToString();
                        string bch = dt_Data.Rows[i]["MNPMBranch"].ToString();
                        string docno = dt_Data.Rows[i]["MNPMDocno"].ToString();

                        string pathFullName = PathImageClass.pImageEmply;
                        string wantFile = "VSM-" + bch + @"*_" + docno + @"*.jpg"; ;
                        string pathFileName = PathImageClass.pPathVDOSendMoneyNew + dateIns;
                        DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                        FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                        string pWho = "";
                        if (Files.Length > 0)
                        {
                            pathFullName = Files[0].FullName;
                            string[] A = Files[0].Name.Split('_');
                            string[] B = A[0].Split('-');

                            DataTable dtEmp = Class.Models.EmplClass.GetEmployeeDetail_ByEmplID(B[6].Replace("M", "").Replace("D", "").Replace("P", ""));
                            if (dtEmp.Rows.Count == 0)
                            {
                                pWho = "ไม่สามารถระบุผู้ถ่ายได้.";
                            }
                            else
                            {
                                pWho = dtEmp.Rows[0]["EMPLID"].ToString() + Environment.NewLine + dtEmp.Rows[0]["SPC_NAME"].ToString();
                            }
                        }
                        RadGridView_ShowHD.Rows[i].Cells["NAMESEND"].Value = pWho;
                        RadGridView_ShowHD.Rows[i].Cells["ImageC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                        RadGridView_ShowHD.Rows[i].Cells["CountImageC"].Value = Files.Length;
                        RadGridView_ShowHD.Rows[i].Cells["PathImageC"].Value = pathFullName;
                    }

                    SetStaInsert("MNPM", " AND SHIPMENTID = '1' ");
                    if (pInsert > 0)
                    {
                        radButtonElement_add.Enabled = false;
                    }
                    else
                    {
                        if (_pPermission == "1") radButtonElement_add.Enabled = true; else radButtonElement_add.Enabled = false;
                    }

                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }

        }
        //ค้นหา VDO
        DataTable LoadVDO0(string pDate, string pBch, string pDocno)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DATA_ID");
            dt.Columns.Add("FileName");
            dt.Columns.Add("DATA_DESC");

            string wantFile;
            string pathFileName = PathImageClass.pPathVDOSendMoneyNew + pDate;
            DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
            if (DirInfo.Exists)
            {
                wantFile = "VSM-" + pBch + @"*_" + pDocno + @".MP4";
                FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);
                for (int i = 0; i < Files.Length; i++)
                {
                    string[] A = Files[i].Name.Split('_');
                    string[] B = A[0].Split('-');
                    string whoins;
                    DataTable dtEmp = Class.Models.EmplClass.GetEmployeeDetail_ByEmplID(B[6].Replace("M", "").Replace("D", "").Replace("P", ""));
                    if (dtEmp.Rows.Count == 0) whoins = "ไม่สามารถระบุผู้ถ่ายได้."; else whoins = dtEmp.Rows[0]["EMPLID"].ToString() + " " + dtEmp.Rows[0]["SPC_NAME"].ToString();
                    dt.Rows.Add(Files[i].FullName, Files[i].Name, whoins);
                }
            }
            return dt;
        }

        //ค้นหา VDO
        DataTable LoadVDO1(string pDate, string pBch)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DATA_ID");
            dt.Columns.Add("FileName");
            dt.Columns.Add("DATA_DESC");

            string wantFile;
            string pathFileName = PathImageClass.pPathVDOFront + pDate + @"\" + pBch;
            DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
            if (DirInfo.Exists)
            {
                wantFile = "*.MP4";
                FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);
                for (int i = 0; i < Files.Length; i++)
                {
                    string[] A = Files[i].Name.Split('-');
                    string[] B = A[6].Split('.');
                    string whoins;
                    DataTable dtEmp = Class.Models.EmplClass.GetEmployeeDetail_ByEmplID(B[0].Replace("M", "").Replace("D", "").Replace("P", ""));
                    if (dtEmp.Rows.Count == 0) whoins = "ไม่สามารถระบุผู้ถ่ายได้."; else whoins = dtEmp.Rows[0]["EMPLID"].ToString() + " " + dtEmp.Rows[0]["SPC_NAME"].ToString();
                    dt.Rows.Add(Files[i].FullName, Files[i].Name, whoins);
                }
            }
            return dt;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Check Insert STA
        void SetStaInsert(string pVan, string pSend)
        {
            if (_pPermission == "0")
            {
                pInsert = 1; return;
            }

            string sql = string.Format(@"
                SELECT	INVOICEID,ITEMBARCODE,DESCRIPTION  AS RMK
                FROM	Shop_RecheckReciveItem WITH (NOLOCK) 
			    WHERE	DATEBILL = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND VAN = '" + pVan + @"' 
                        " + pSend + @"
            ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt_Data.Rows.Count > 0) pInsert = dt.Rows.Count;

        }
        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            pInsert = 0;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "0":
                    if (Convert.ToDateTime(RadGridView_ShowHD.Rows[0].Cells["MNPMDate"].Value.ToString()).ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่อนุญาติให้บันทึกข้อมูล ณ วันปัจจุบัน [ต้องย้อนหลัง 1 วันเท่านั้น]"); return;
                    }
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการส่งซองเงิน [VDO]") == DialogResult.Yes)
                    {
                        ArrayList sql = new ArrayList();
                        for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                        {

                            sql.Add(String.Format(@"
                                INSERT INTO Shop_RecheckReciveItem (INVOICEID,DATEBILL,VAN,BRANCH,ITEMBARCODE,
                                            SPC_ITEMNAME,QtySend,QtyRecive,WHOINS,WHOINSNAME,DATEINS,TIMEINS) 
                                VALUES      ('" + RadGridView_ShowHD.Rows[i].Cells["MNPMDocno"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMDate"].Value.ToString() + @"',
                                        'MNPM',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMBranch"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMLO"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMRemark"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMCountPack"].Value.ToString() + @"',
                                        '0',
                                        '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                                        CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24))
                                "));

                        }

                        string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "")
                        {
                            pInsert = sql.Count;
                            radButtonElement_add.Enabled = false;
                        }
                    }
                    break;
                case "1":

                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการตรวจ VDO หน้าร้าน") == DialogResult.Yes)
                    {
                        ArrayList sql = new ArrayList();
                        for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                        {
                            sql.Add(String.Format(@"
                                INSERT INTO Shop_RecheckReciveItem (INVOICEID,DATEBILL,VAN,BRANCH,ITEMBARCODE,
                                            SPC_ITEMNAME,QtySend,QtyRecive,WHOINS,WHOINSNAME,DATEINS,TIMEINS) 
                                VALUES      ('" + RadGridView_ShowHD.Rows[i].Cells["Datebill"].Value.ToString().Replace("-", "") + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["Datebill"].Value.ToString() + @"',
                                        'VDO',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["Path"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["VDO"].Value.ToString() + @"',
                                        '0',
                                        '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                                        CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24))
                                "));

                        }

                        string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "")
                        {
                            pInsert = sql.Count;
                            radButtonElement_add.Enabled = false;
                        }
                    }
                    break;
                case "2":
                    if (Convert.ToDateTime(RadGridView_ShowHD.Rows[0].Cells["MNPMDate"].Value.ToString()).ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่อนุญาติให้บันทึกข้อมูล ณ วันปัจจุบัน [ต้องย้อนหลัง 1 วันเท่านั้น]");
                        return;
                    }
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการส่งซองเงิน [ฝากเงินเข้าบัญชี]") == DialogResult.Yes)
                    {
                        ArrayList sql = new ArrayList();
                        for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                        {

                            sql.Add(String.Format(@"
                                INSERT INTO Shop_RecheckReciveItem (INVOICEID,DATEBILL,VAN,BRANCH,ITEMBARCODE,
                                            SPC_ITEMNAME,QtySend,QtyRecive,WHOINS,WHOINSNAME,DATEINS,TIMEINS,SHIPMENTID) 
                                VALUES      ('" + RadGridView_ShowHD.Rows[i].Cells["MNPMDocno"].Value.ToString() + @"',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMDate"].Value.ToString() + @"',
                                        'MNPM',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMBranch"].Value.ToString() + @"',
                                        '1',
                                        '" + RadGridView_ShowHD.Rows[i].Cells["MNPMRemark"].Value.ToString() + @"',
                                        '1',
                                        '0',
                                        '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                                        CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'1')
                                "));

                        }

                        string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "")
                        {
                            pInsert = sql.Count;
                            radButtonElement_add.Enabled = false;
                        }
                    }
                    break;
                default:
                    break;
            }

        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (_pTypeReport)
            {
                case "0":
                    switch (e.Column.Name)
                    {
                        case "VDO":
                            if (RadGridView_ShowHD.CurrentRow.Cells["VDO"].Value.ToString() == "1")
                            {
                                Process.Start(RadGridView_ShowHD.CurrentRow.Cells["Path"].Value.ToString());
                            }
                            else if (Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["VDO"].Value) > 1)
                            {
                                string dateIns = RadGridView_ShowHD.CurrentRow.Cells["MNPMDate"].Value.ToString();
                                string bch = RadGridView_ShowHD.CurrentRow.Cells["MNPMBranch"].Value.ToString();
                                string docno = RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString();

                                DataTable vdo = LoadVDO0(dateIns, bch, docno);
                                FormShare.ShowData.ShowDataDGV _showData = new FormShare.ShowData.ShowDataDGV()
                                {
                                    dtData = vdo
                                };
                                if (_showData.ShowDialog(this) == DialogResult.Yes)
                                {
                                    Process.Start(_showData.pID);
                                }
                            }
                            break;
                        case "RMK":
                            if (_pPermission == "0") return;
                            SaveData_RMK_ReportType0();
                            break;
                        case "MNPMDocno":
                            if (_pPermission == "0") return;
                            SaveData_RMK_ReportType0();
                            break;
                        default:
                            break;
                    }
                    break;
                case "1":
                    switch (e.Column.Name)
                    {
                        case "VDO":
                            if (RadGridView_ShowHD.CurrentRow.Cells["VDO"].Value.ToString() == "1")
                            {
                                Process.Start(RadGridView_ShowHD.CurrentRow.Cells["Path"].Value.ToString());
                            }
                            else if (Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["VDO"].Value) > 1)
                            {
                                string dateIns = RadGridView_ShowHD.CurrentRow.Cells["Datebill"].Value.ToString();
                                string bch = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                                DataTable vdo = LoadVDO1(dateIns, bch);
                                FormShare.ShowData.ShowDataDGV _showData = new FormShare.ShowData.ShowDataDGV()
                                {
                                    dtData = vdo
                                };
                                if (_showData.ShowDialog(this) == DialogResult.Yes)
                                {
                                    Process.Start(_showData.pID);
                                }
                            }
                            break;
                        case "RMK":
                            if (_pPermission == "0") return;
                            SaveData_RMK_ReportType1();
                            break;
                        default:
                            break;
                    }

                    break;
                case "2":
                    switch (e.Column.Name)
                    {
                        case "ImageC":
                            if (RadGridView_ShowHD.CurrentRow.Cells["CountImageC"].Value.ToString() != "0") Process.Start(RadGridView_ShowHD.CurrentRow.Cells["PathImageC"].Value.ToString());
                            break;
                        case "RMK":
                            if (_pPermission == "0") return;
                            SaveData_RMK_ReportType2();
                            break;
                        case "MNPMDocno":
                            if (_pPermission == "0") return;
                            SaveData_RMK_ReportType2();
                            break;
                        default:
                            break;
                    }

                    break;
                default:
                    break;
            }


        }
        //บันทึกการตรวจ
        void SaveData_RMK_ReportType0()
        {
            if (RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value.ToString() != "")
            { return; }
            if (pInsert == 0)
            { return; }

            string sql = String.Format(@"select SHOW_ID,SHOW_NAME,SHOW_DESC,SHOW_NAME+','+SHOW_DESC as SHOW_NAMEDESC 
                    from SHOP_CONFIGBRANCH_GenaralDetail 
                    where  TYPE_CONFIG='30' and sta='1' 
                    order by SHOW_ID");
            DataTable dtReason = ConnectionClass.SelectSQL_Main(sql);

            ComMinimart.Tag.InputEmplReasonRemark _InputEmplReasonRemark = new ComMinimart.Tag.InputEmplReasonRemark(
                           RadGridView_ShowHD.CurrentRow.Cells["BRANCH_NAME"].Value.ToString() + " - " +
                           RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString() + Environment.NewLine +
                           "จำนวน " + RadGridView_ShowHD.CurrentRow.Cells["MNPMCountPack"].Value.ToString() + " ซอง",
                            "", "",
                            "หมายเหตุการตรวจ", dtReason, "SHOW_ID", "SHOW_NAMEDESC", "",
                            "หมายเหตุเพิ่มเติม", "", "")
            { pSta = "2" };
            if (_InputEmplReasonRemark.ShowDialog(this) == DialogResult.Yes)
            {
                string strUp = _InputEmplReasonRemark.ReasonName + " " + _InputEmplReasonRemark.pInputData +
                    Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                    " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
                String UpStr = string.Format(@"
                                        Update  Shop_RecheckReciveItem  
                                        SET     DESCRIPTION = '" + strUp + @"'+
                                                CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']' 
                                        WHERE   DATEBILL = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPMDate"].Value.ToString() + @"' AND 
                                                BRANCH = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPMBranch"].Value.ToString() + @"' AND VAN = 'MNPM' AND
                                                INVOICEID = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString() + @"' ");

                string T = ConnectionClass.ExecuteSQL_Main(UpStr);
                if (T == "")
                {
                    RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value = strUp;
                }
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }

        }
        //บันทึกการตรวจ
        void SaveData_RMK_ReportType2()
        {
            if (RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value.ToString() != "")
            { return; }
            if (pInsert == 0)
            { return; }

            string sql = String.Format(@"select SHOW_ID,SHOW_NAME,SHOW_DESC,SHOW_NAME+','+SHOW_DESC as SHOW_NAMEDESC 
                    from SHOP_CONFIGBRANCH_GenaralDetail 
                    where  TYPE_CONFIG='30' and sta='1' 
                    order by SHOW_ID");
            DataTable dtReason = ConnectionClass.SelectSQL_Main(sql);

            ComMinimart.Tag.InputEmplReasonRemark _InputEmplReasonRemark = new ComMinimart.Tag.InputEmplReasonRemark(
                           RadGridView_ShowHD.CurrentRow.Cells["BRANCH_NAME"].Value.ToString() + " - " +
                           RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString() + Environment.NewLine +
                           "",
                            "", "",
                            "หมายเหตุการตรวจ", dtReason, "SHOW_ID", "SHOW_NAMEDESC", "",
                            "หมายเหตุเพิ่มเติม", "", "")
            { pSta = "2" };
            if (_InputEmplReasonRemark.ShowDialog(this) == DialogResult.Yes)
            {
                string strUp = _InputEmplReasonRemark.ReasonName + " " + _InputEmplReasonRemark.pInputData +
                    Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                    " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
                String UpStr = string.Format(@"
                                        Update  Shop_RecheckReciveItem  
                                        SET     DESCRIPTION = '" + strUp + @"'+
                                                CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']' 
                                        WHERE   DATEBILL = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPMDate"].Value.ToString() + @"' AND 
                                                BRANCH = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPMBranch"].Value.ToString() + @"' AND VAN = 'MNPM' AND
                                                INVOICEID = '" + RadGridView_ShowHD.CurrentRow.Cells["MNPMDocno"].Value.ToString() + @"' 
                                               
                    ");
                string T = ConnectionClass.ExecuteSQL_Main(UpStr);
                if (T == "") RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value = strUp;
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }


        }
        //บันทึกการตรวจ
        void SaveData_RMK_ReportType1()
        {
            if (RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value.ToString() != "") return;
            if (pInsert == 0) return;


            FormShare.InputData _inputData = new FormShare.InputData("1",
                 "VDO หน้าร้าน : " + RadGridView_ShowHD.CurrentRow.Cells["BRANCH_NAME"].Value.ToString(),
                 "หมายเหตุการตรวจ", "")
            { pInputData = "เรียบร้อย" };
            if (_inputData.ShowDialog(this) == DialogResult.Yes)
            {
                string strUp = _inputData.pInputData + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
                String UpStr = string.Format(@"
                                        Update  Shop_RecheckReciveItem  
                                        SET     DESCRIPTION = '" + strUp + @"'+
                                                CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']' 
                                        WHERE   DATEBILL = '" + RadGridView_ShowHD.CurrentRow.Cells["Datebill"].Value.ToString() + @"' AND 
                                                BRANCH = '" + RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + @"' AND 
                                                VAN = 'VDO' ");

                string T = ConnectionClass.ExecuteSQL_Main(UpStr);
                if (T == "")
                {
                    RadGridView_ShowHD.CurrentRow.Cells["RMK"].Value = strUp;
                }
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Clear
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Docnument
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
    }
}
