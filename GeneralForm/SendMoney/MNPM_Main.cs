﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Collections;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public partial class MNPM_Main : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly DataTable dt_Grid = new DataTable();
        readonly DataTable dtPrint_MNSM = new DataTable();
        string headBill, headID;
        string MaxID;
        double MaxValue;
        double LOValue;
        int iC;
        //Load
        public MNPM_Main()
        {
            InitializeComponent();
        }
        //Load
        private void MNPM_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radStatusStrip1.SizingGrip = false;
            RadButton_Apv.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadCheckBox_coin.ButtonElement.Font = SystemClass.SetFontGernaral;
            dt_Grid.Columns.Add("C");
            dt_Grid.Columns.Add("DRAWERID");
            dt_Grid.Columns.Add("TRANSDATE");
            dt_Grid.Columns.Add("CREATEDDATETIME");
            dt_Grid.Columns.Add("EMPLID");
            dt_Grid.Columns.Add("NAME");
            dt_Grid.Columns.Add("REMARK");
            dt_Grid.Columns.Add("LINEAMOUNT");
            dt_Grid.Columns.Add("POSGROUP");
            dt_Grid.Columns.Add("NAMETYPE");
            dt_Grid.Columns.Add("DATEDIFF_TODAY");
            dt_Grid.Columns.Add("SlipID");
            dt_Grid.Columns.Add("STACANCLE");
            dt_Grid.Columns.Add("STANOMNPM");

            dtPrint_MNSM.Columns.Add("NAMETYPE");
            dtPrint_MNSM.Columns.Add("DRAWERID");

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMETYPE", "ประเภทซอง", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DRAWERID", "เลขที่ซอง", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่ซอง", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "เวลาส่งซอง", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "แคชเชียร์", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อแคชเชียร์", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 350)));

            if (SystemClass.SystemComProgrammer == "1")
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "จำนวนเงิน", 130)));
                RadGridView_ShowHD.Columns["LINEAMOUNT"].FormatString = "{0:#,##0.00}";
                GridViewSummaryItem summaryItem6 = new GridViewSummaryItem
                {
                    Name = "LINEAMOUNT",
                    Aggregate = GridAggregateFunction.Sum,
                    FormatString = "{0:n2}"
                };
                GridViewSummaryRowItem summaryRowItem6 = new GridViewSummaryRowItem { summaryItem6 };
                //summaryRowItem.Add(summaryItem);
                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem6);
            }
            else
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("LINEAMOUNT", "จำนวนเงิน")));
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("POSGROUP", "สาขา")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DATEDIFF_TODAY", "DATEDIFF_TODAY")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SlipID", "SlipID")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STACANCLE", "STACANCLE")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STANOMNPM", "STANOMNPM")));

            RadGridView_ShowHD.TableElement.RowHeight = 100;


            this.RadGridView_ShowHD.Columns["DRAWERID"].ConditionalFormattingObjectList.Add(
                      new ExpressionFormattingObject("MyCondition2", "DATEDIFF_TODAY" + " <> 0 ", false)
                      { CellBackColor = ConfigClass.SetColor_Red() });

            this.RadGridView_ShowHD.Columns["DRAWERID"].ConditionalFormattingObjectList.Add(
                    new ExpressionFormattingObject("MyCondition2", "SlipID" + " <> 0 ", false)
                    { CellBackColor = ConfigClass.SetColor_GreenPastel() });

            ClearTxt();
        }
        //FindData MaxValues
        void MaxAndLo_Value()
        {
            DataTable dtMax = ConnectionClass.SelectSQL_Main(
            string.Format(@"
                SELECT	BRANCH_SendMoney	FROM	SHOP_BRANCH_CONFIGDB WITH (NOLOCK)	
                WHERE	BRANCH_ID = '" + SystemClass.SystemBranchID + @"' 
            "));

            if (dtMax.Rows.Count == 0)
            {
                MaxValue = 0; return;
            }
            else
            {
                DataTable dtLO = ConnectionClass.SelectSQL_Main(string.Format(@"
                    SELECT	ISNULL(SUM(MNPMSum),0) AS SUMMN   
                     FROM	SHOP_MNPM_HD  WITH (NOLOCK)  
                     WHERE	MNPMLO = 'LO" + radTextBox_LO.Text + @"'
                "));
                if (dtLO.Rows.Count > 0)
                {
                    MaxValue = double.Parse(dtMax.Rows[0]["BRANCH_SendMoney"].ToString()) - double.Parse(dtLO.Rows[0]["SUMMN"].ToString());
                }
                else
                {
                    MaxValue = double.Parse(dtMax.Rows[0]["BRANCH_SendMoney"].ToString());
                }
            }
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dtData = PosSaleClass.XXX_POSDRAWER();

            if (dt_Grid.Rows.Count > 0)
            {
                dt_Grid.Rows.Clear();
                RadGridView_ShowHD.DataSource = dt_Grid;
                dt_Grid.AcceptChanges();
                this.Cursor = Cursors.Default;
            }
            foreach (DataRow item in dtData.Rows)
            {
                int SlipID;
                if (item["DRAWERID"].ToString().Contains("MNSM"))
                {
                    SlipID = int.Parse(item["SlipID"].ToString());
                }
                else
                {
                    SlipID = CheckSlipCount(item["DRAWERID"].ToString());
                }

                dt_Grid.Rows.Add("0",
                    item["DRAWERID"].ToString(), item["TRANSDATE"].ToString(),
                    item["CREATEDDATETIME"].ToString(), item["EMPLID"].ToString(),
                    item["NAME"].ToString(), item["REMARK"].ToString(),
                    item["LINEAMOUNT"].ToString(), item["POSGROUP"].ToString(),
                    item["NAMETYPE"].ToString(), item["DATEDIFF_TODAY"].ToString(),
                    SlipID.ToString(), item["STACANCLE"].ToString(), item["TYPE_STA"].ToString());
            }

            RadGridView_ShowHD.DataSource = dt_Grid;
            dt_Grid.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Check เงินเดือน
        int CheckSlipCount(string pDRAWERID)
        {
            string sql = string.Format(@" 
            SELECT	BARCODESLIP ,tSumAmount FROM	[dbo].[tblPrintBranchLog] WITH (NOLOCK) 
            WHERE	DRAWERID  =  '" + pDRAWERID + @"'  
                    OR DRAWERID2 = '" + pDRAWERID + @"'
                    OR DRAWERID3 = '" + pDRAWERID + @"'
                    OR DRAWERID4 = '" + pDRAWERID + @"'
                    OR DRAWERID5 = '" + pDRAWERID + @"'
                    OR DRAWERID6 = '" + pDRAWERID + @"'
            ");
            if (ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.Con701SRV).Rows.Count > 0) return 1; else return 0;

        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            MaxValue = 0; LOValue = 0; MaxID = ""; iC = 0;
            radTextBox_LO.Text = "";
            radLabel_CarID.Text = "";
            radLabel_CarName.Text = "";
            radLabel_ID.Text = "";
            radLabel_EmplName.Text = "";
            radTextBox_EmplID.Text = "";
            radTextBox_Remark.Text = "";
            RadButton_Apv.Enabled = false;
            RadCheckBox_coin.Enabled = false;
            radTextBox_EmplID.Enabled = false; radTextBox_EmplID.Text = "";
            radTextBox_Remark.Enabled = false;
            radTextBox_LO.Enabled = true;
            radRadioButton_LO.Enabled = true;
            radRadioButton_Transfer.Enabled = true; radRadioButton_LO.CheckState = CheckState.Checked;
            SetDGV_HD();
            radTextBox_LO.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (RadGridView_ShowHD.CurrentRow.Cells["DRAWERID"].Value.ToString() == "") return;

            if (e.Column.Name == "NAMETYPE")
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["STACANCLE"].Value.ToString() == "1")
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกซองเลขที่ " +
                        RadGridView_ShowHD.CurrentRow.Cells["DRAWERID"].Value.ToString() + @" ?") == DialogResult.No) return;

                    string sqlCancle = String.Format(@"
                        UPDATE  SHOP_MNSM 
                        Set     SendSta = '3',SendAcc = '3',WHOUpd = '" + SystemClass.SystemUserID + @"',
                                DATEUpd = convert(varchar,getdate(),23) , TIMEUpd = convert(varchar,getdate(),24) 
                        WHERE DRAWERID = '" + RadGridView_ShowHD.CurrentRow.Cells["DRAWERID"].Value.ToString() + @"'");
                    string T = ConnectionClass.ExecuteSQL_Main(sqlCancle);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        SetDGV_HD();
                    }
                    return;
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับซองส่งเงินปกติไม่สามารถยกเลิกรายการได้.");
                    return;
                }
            }
            else
            {
                PrintDialog _dai = new PrintDialog();
                if (_dai.ShowDialog(this) == DialogResult.OK)
                {
                    PrintDocument_Slip.PrintController = printController;
                    _dai.PrinterSettings.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    PrintDocument_Slip.PrinterSettings = _dai.PrinterSettings;
                    PrintDocument_Slip.Print();
                }
            }
        }
        //KeyPress LO
        private void RadTextBox_LO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '-')) e.Handled = true;
        }
        //Enter LO
        private void RadTextBox_LO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_LO.Text == "") return;

                DataTable dtLO = LogisticClass.GetLogisticDetail_GroupByLO("LO" + radTextBox_LO.Text);
                if (dtLO.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ LO" + radTextBox_LO.Text + " ที่ระบุ");
                    radTextBox_LO.SelectAll();
                    radTextBox_LO.Focus();
                    return;
                }

                radLabel_CarID.Text = dtLO.Rows[0]["EMPLDRIVER"].ToString();
                radLabel_CarName.Text = dtLO.Rows[0]["SPC_NAME"].ToString();
                radLabel_ID.Text = dtLO.Rows[0]["VEHICLEID"].ToString();
                radTextBox_LO.Enabled = false;
                RadCheckBox_coin.Enabled = true;
                radTextBox_EmplID.Enabled = true;
                radTextBox_EmplID.Text = SystemClass.SystemUserID;
                radTextBox_EmplID.SelectAll();
                MaxAndLo_Value();
                radTextBox_EmplID.Focus();
            }
        }

        private void RadTextBox_EmplID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;
        }

        private void RadTextBox_EmplID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_EmplID.Text == "") return;

                DataTable dtEmp = Models.EmplClass.GetEmployeeCheckTime_Altnum(radTextBox_EmplID.Text);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน " + radTextBox_EmplID.Text + " ที่ระบุ");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }


                if (dtEmp.Rows[0]["PW_ID"].ToString() == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานที่ระบุนั้น ไม่ได้อยู่ในช่วงเวลาทำงาน" + Environment.NewLine
                       + @"ไม่สามารถทำการส่งซองเงินได้" + Environment.NewLine + @"เช็ครหัสที่ระบุอีกครั้ง.");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                if (dtEmp.Rows[0]["PW_BRANCH_IN"].ToString() != SystemClass.SystemBranchID)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสพนักงานที่ระบุนั้น ไม่ได้ลงเวลาทำงานในสาขา " + SystemClass.SystemBranchID + " " + SystemClass.SystemBranchName + Environment.NewLine
                       + @"ไม่สามารถทำการส่งซองเงินได้" + Environment.NewLine + @"เช็ครหัสที่ระบุอีกครั้ง.");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                radTextBox_EmplID.Text = dtEmp.Rows[0]["EMP_ID"].ToString();
                radLabel_EmplName.Text = dtEmp.Rows[0]["SPC_NAME"].ToString();
                radTextBox_EmplID.Enabled = false;

                radTextBox_Remark.Enabled = true;
                radTextBox_Remark.Focus();

                RadButton_Apv.Enabled = true;

            }
        }
        //Save
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            if (radRadioButton_LO.CheckState == CheckState.Checked) if (radTextBox_LO.Text == "") return;

            if (radTextBox_EmplID.Text == "") return;

            int iMNSV_Check = 0;

            iC = 0;
            double iSumGrand = 0;
            foreach (GridViewRowInfo item in RadGridView_ShowHD.Rows)
            {
                if (item.Cells["C"].Value.ToString() == "1")
                {
                    if (item.Cells["STANOMNPM"].Value.ToString() == "0")
                    {
                        iC++;
                        if (item.Cells["SlipID"].Value.ToString() == "0")
                        {
                            iSumGrand += Double.Parse(item.Cells["LINEAMOUNT"].Value.ToString());
                        }
                    }
                    else
                    {
                        iMNSV_Check++;
                    }
                }
            }

            if ((iC == 0) && (iMNSV_Check == 0))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กำหนดการเลือกซองเงินก่อนการบันทึกทุกครั้ง."); return;
            }

            if ((iC == 0) && (iMNSV_Check > 0))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"การส่งซองเงินประเภทอื่นๆ{Environment.NewLine}ต้องส่งไปพร้อมกับซองเงินจริงเท่านั้น."); return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ซองส่งเงินที่ระบุทั้งหมด") == DialogResult.No) return;

            if (dtPrint_MNSM.Rows.Count > 0) dtPrint_MNSM.Rows.Clear();

            int iRound = CountRoundSend();

            ArrayList sql = new ArrayList();

            MaxID = Class.ConfigClass.GetMaxINVOICEID("MNPM", "-", "MNPM", "1");

            string StaTrans = "0", RemarkTrans = "", StaAcc = "0", RemarkAcc = "";
            string MNPMLO = "LO" + radTextBox_LO.Text;
            if (radRadioButton_Transfer.CheckState == CheckState.Checked)
            {
                StaTrans = "1";
                RemarkTrans = "ฝากเงินเข้าธนาคาร";
                StaAcc = "1";
                RemarkAcc = "ฝากเงินเข้าธนาคาร";
                MNPMLO = "1";
            }

            sql.Add(string.Format(@"
                Insert Into   SHOP_MNPM_HD   
                (MNPMDocno,MNPMUserCode,
                MNPMBranch,MNPMSendCount,MNPMUserCar,
                MNPMCar,MNPMCountPack,MNPMSum,
                MNPMRemark,MNPMWhoIn,MNPMLO
                ) values ('" + MaxID + @"','" + radTextBox_EmplID.Text + @"',
                '" + SystemClass.SystemBranchID + @"','" + iRound + @"','" + radLabel_CarID.Text + @"',
                '" + radLabel_ID.Text + @"','" + iC + @"','" + iSumGrand + @"',
                '" + radTextBox_Remark.Text.Replace("{", " ").Replace("}", " ").Replace("'", "") + @"','" + radTextBox_EmplID.Text + @"','" + MNPMLO + @"' )
            "));

            int iRowsC = 1;
            foreach (GridViewRowInfo itemIN in RadGridView_ShowHD.Rows)
            {

                if (itemIN.Cells["C"].Value.ToString() == "1")
                {
                    string sqlUpType = "";
                    if (itemIN.Cells["STANOMNPM"].Value.ToString() == "0")
                    {
                        sql.Add(string.Format(@"
                        Insert Into SHOP_MNPM_DT 
                            (MNPMDocno,MNPMSeqNo,MNPMDRAWERID,MNPMUserDRAWERID,MNPMNameDRAWERID,
                            MNPMGranchDRAWERID,MNPMRAWERIDDATE,MNPMRemark,
                            MNPOWhoIn,MNPM_SlipSaraly,StaTrans,RemarkTrans,StaAcc,RemarkAcc) 
                        values (
                        '" + MaxID + @"','" + iRowsC + @"','" + itemIN.Cells["DRAWERID"].Value.ToString() + @"',
                        '" + itemIN.Cells["EMPLID"].Value.ToString() + @"','" + itemIN.Cells["NAME"].Value.ToString() + @"',
                        '" + double.Parse(itemIN.Cells["LINEAMOUNT"].Value.ToString()) + @"',
                        '" + itemIN.Cells["CREATEDDATETIME"].Value.ToString() + @"','" + itemIN.Cells["REMARK"].Value.ToString().Replace("{", " ").Replace("}", " ").Replace("'", "") + @"',
                        '" + SystemClass.SystemUserID + @"','" + itemIN.Cells["SlipID"].Value.ToString() + @"','" + StaTrans + @"','" + RemarkTrans + @"','" + StaAcc + @"','" + RemarkAcc + @"') "));
                        iRowsC++;
                    }
                    else
                    {
                        sqlUpType = $@",MNPM = '{MaxID}' ";
                        dtPrint_MNSM.Rows.Add(itemIN.Cells["NAMETYPE"].Value.ToString(), itemIN.Cells["DRAWERID"].Value.ToString());
                    }

                    if (itemIN.Cells["DRAWERID"].Value.ToString().Contains("MNSM"))
                    {
                        sql.Add($@"
                                Update  SHOP_MNSM 
                                Set     SendSta = '1',WHOUpd = '{radTextBox_EmplID.Text}',
                                        DATEUpd = convert(varchar,getdate(),23) , TIMEUpd = convert(varchar,getdate(),24) {sqlUpType}
                                WHERE   DRAWERID = '{itemIN.Cells["DRAWERID"].Value}'
                        ");
                    }
                    if (itemIN.Cells["DRAWERID"].Value.ToString().Contains("MNEC"))
                    {
                        sql.Add($@"
                                Update  SHOP_MNEC_HD 
                                Set     MNECSandSta = '1',MNECSandStaWhoIns = '{radTextBox_EmplID.Text}',
                                        MNECSandStaWhoName = '{radLabel_EmplName.Text}',
                                        MNECSandStaDateIns = convert(varchar,getdate(),25),MNPM = '{MaxID}'
                                WHERE   MNECDocno = '{itemIN.Cells["DRAWERID"].Value}'
                        ");
                    }
                }
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                PrintSend();
                ClearTxt();
            }
        }
        void PrintSend()
        {
            PrintDialog _dai = new PrintDialog();
            if (_dai.ShowDialog(this) == DialogResult.OK)
            {
                PrintDocument_send.PrintController = printController;
                printDocument_sta.PrintController = printController;


                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                _dai.PrinterSettings.DefaultPageSettings.PaperSize = ps;

                PrintDocument_send.PrinterSettings = _dai.PrinterSettings;
                printDocument_sta.PrinterSettings = _dai.PrinterSettings;

                for (int i = 0; i < iC; i++)
                {
                    PrintDocument_send.Print();
                }
                if (dtPrint_MNSM.Rows.Count > 0)
                {
                    for (int ik = 0; ik < dtPrint_MNSM.Rows.Count; ik++)
                    {
                        headID = dtPrint_MNSM.Rows[ik]["DRAWERID"].ToString();
                        headBill = dtPrint_MNSM.Rows[ik]["NAMETYPE"].ToString();
                        printDocument_sta.Print();
                    }
                }
            }
        }
        //CountSend จำนวนรอบในการส่งวันนี้
        int CountRoundSend()
        {
            string sql = string.Format(@"
                SELECT	ISNULL(MAX(MNPMSendCount),0)+1 as MNPMSendCount 
                FROM	SHOP_MNPM_HD WITH (NOLOCK)
                WHERE 	MNPMDate = convert(varchar,getdate(),23) 	AND MNPMBranch = '" + SystemClass.SystemBranchID + @"'
            ");
            DataTable dtRound = ConnectionClass.SelectSQL_Main(sql);
            if (dtRound.Rows.Count == 0) return 1; else return int.Parse(dtRound.Rows[0]["MNPMSendCount"].ToString());
        }
        //print Slip
        private void PrintDocument_Slip_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            string DRAWERID = RadGridView_ShowHD.CurrentRow.Cells["DRAWERID"].Value.ToString();

            barcode.Data = DRAWERID;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            if (DRAWERID.Contains("MNSM"))
            {
                e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["NAMETYPE"].Value.ToString(), SystemClass.printFont15, Brushes.Black, 0, Y);
                Y += 25;
            }
            else if (DRAWERID.Contains("MNEC"))
            {
                e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["NAMETYPE"].Value.ToString(), new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
                Y += 40;
            }
            else
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["SlipID"].Value.ToString() == "1")
                {
                    e.Graphics.DrawString("     **** จ่ายเงินเดือน ****", SystemClass.printFont15, Brushes.Black, 0, Y);
                    Y += 25;
                }
            }

            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["TRANSDATE"].Value.ToString(), new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString(), new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 42;
            if (RadGridView_ShowHD.CurrentRow.Cells["POSGROUP"].Value.ToString() != SystemClass.SystemBranchID)
            {
                e.Graphics.DrawString(RadGridView_ShowHD.CurrentRow.Cells["POSGROUP"].Value.ToString() + " - " +
                    RadGridView_ShowHD.CurrentRow.Cells["NAMETYPE"].Value.ToString(), SystemClass.printFont15, Brushes.Black, 15, Y);
            }
            else
            {
                e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 15, Y);
            }
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 60;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;// DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss")
            e.Graphics.DrawString("ซองรอบที่ _____ วันที่ส่ง " + RadGridView_ShowHD.CurrentRow.Cells["CREATEDDATETIME"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("แคช " + RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString() + " " +
                RadGridView_ShowHD.CurrentRow.Cells["NAME"].Value.ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("เวลาส่งเงิน ___________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            //ก่อนเปลี่ยน POS ใหม่
            //string[] name = RadGridView_ShowHD.CurrentRow.Cells["NAME"].Value.ToString().Split(' ');
            //Y += 5;
            //e.Graphics.DrawString(name[1], new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            //Y += 40;
            //if (name[2] == "")
            //{
            //    try
            //    {
            //        e.Graphics.DrawString(name[3], new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            //    }
            //    catch (Exception) { }
            //}
            //else
            //{
            //    e.Graphics.DrawString(name[2], new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            //}

            string[] name = RadGridView_ShowHD.CurrentRow.Cells["NAME"].Value.ToString().Replace("คุณ ", "").Split(' ');
            Y += 5;
            e.Graphics.DrawString(name[0], new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString(name[1], new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);

            Y += 40;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        //Choose Pack
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            if (e.Column.Name == "C")
            {
                if (radRadioButton_LO.CheckState == CheckState.Checked)
                {
                    if (radLabel_CarID.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุเลขที่ LO ให้เรียบร้อย ก่อนการเลือกซองส่งเงิน.");
                        radTextBox_LO.SelectAll();
                        radTextBox_LO.Focus();
                        return;
                    }
                }


                if (radLabel_EmplName.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุรหัสพนักงานให้เรียบร้อย ก่อนการเลือกซองส่งเงิน.");
                    radTextBox_EmplID.SelectAll();
                    radTextBox_EmplID.Focus();
                    return;
                }

                if (MaxValue == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("การตั้งค่าการส่งซองเงินไม่เรียบร้อย" +
                        Environment.NewLine + "ติดต่อ ComMinimart Tel 8570 เพื่อดำเนินการ");
                    return;
                }

                if (radRadioButton_Transfer.CheckState == CheckState.Checked)
                {
                    if (RadGridView_ShowHD.CurrentRow.Cells["DRAWERID"].Value.ToString().Contains("MNSM"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("สำหรับเอกสารทั้งหมด ไม่สามารถ ฝากเข้าธนาคารได้.");
                        return;
                    }
                }


                if (RadGridView_ShowHD.CurrentRow.Cells["SlipID"].Value.ToString() == "1")
                {
                    if (RadGridView_ShowHD.CurrentRow.Cells["C"].Value.ToString() == "1")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "0";
                    }
                    else
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "1";
                    }
                    return;
                }
                else
                {
                    if (RadGridView_ShowHD.CurrentRow.Cells["C"].Value.ToString() == "1")
                    {
                        LOValue -= double.Parse(RadGridView_ShowHD.CurrentRow.Cells["LINEAMOUNT"].Value.ToString());
                        RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "0";
                    }
                    else
                    {
                        LOValue += double.Parse(RadGridView_ShowHD.CurrentRow.Cells["LINEAMOUNT"].Value.ToString());

                        if (LOValue > MaxValue)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถส่งซองเงินเลข " + RadGridView_ShowHD.CurrentRow.Cells["DRAWERID"].Value.ToString() + @" ได้ " +
                                Environment.NewLine + @" ให้ส่งซองเท่าที่เลือกก่อน สำหรับซองที่เหลือให้รอรอบรถถัดไป");
                            LOValue -= double.Parse(RadGridView_ShowHD.CurrentRow.Cells["LINEAMOUNT"].Value.ToString());
                            RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "0";
                        }
                        else
                        {
                            RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "1";
                        }
                    }
                }

                //check ประเภทการส่งซองเงิน
                int iC = 0;
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["SlipID"].Value.ToString() == "1") iC++;
                }

                if (iC == 1)
                {
                    radRadioButton_LO.Enabled = false;
                    radRadioButton_Transfer.Enabled = false;
                }
            }
        }

        private void RadCheckBox_coin_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_coin.Checked == true)
            {
                radTextBox_Remark.Text += RadCheckBox_coin.Text;
            }
            else
            {
                radTextBox_Remark.Text = radTextBox_Remark.Text.Replace(RadCheckBox_coin.Text, "");
            }
        }
        //print Send
        private void PrintDocument_send_PrintPage(object sender, PrintPageEventArgs e)
        {
            barcode.Data = MaxID;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            e.Graphics.DrawString("เอกสารส่งซองเงิน", SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;//
            e.Graphics.DrawString(headID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("LO" + radTextBox_LO.Text + "  ทะเบียน " + radLabel_ID.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("พขร. " + radLabel_CarID.Text + "-" + radLabel_CarName.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("จำนวน [ซอง] " + iC.ToString(), SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("ผู้ส่ง " + radTextBox_EmplID.Text + "-" + radLabel_EmplName.Text, SystemClass.printFont, Brushes.Black, 0, Y);

            Y += 20;
            e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect1 = new Rectangle(0, Y, 350, 200);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };

            e.Graphics.DrawString(radTextBox_Remark.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadRadioButton_Transfer_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_Transfer.CheckState == CheckState.Checked)
            {
                radTextBox_LO.Enabled = false;
                RadCheckBox_coin.Enabled = true;
                radTextBox_EmplID.Enabled = true;
                radTextBox_EmplID.Text = SystemClass.SystemUserID; radLabel_EmplName.Text = "";
                radTextBox_EmplID.SelectAll();
                radTextBox_Remark.Text = "ฝากเงินเข้าธนาคาร";
                MaxValue = 15000000;
                radTextBox_EmplID.Focus();
            }
        }

        private void PrintDocument_sta_PrintPage(object sender, PrintPageEventArgs e)
        {
            int Y = 0;
            Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(headBill, new Font(new FontFamily("Tahoma"), 30), Brushes.Black, rect1, stringFormat);

            Y += 110;
            e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(MaxID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("LO" + radTextBox_LO.Text + "  ทะเบียน " + radLabel_ID.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("พขร. " + radLabel_CarID.Text + "-" + radLabel_CarName.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("ผู้ส่ง " + radTextBox_EmplID.Text + "-" + radLabel_EmplName.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
    }
}
