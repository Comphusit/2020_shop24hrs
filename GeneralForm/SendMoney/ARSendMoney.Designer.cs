﻿namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    partial class ARSendMoney
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ARSendMoney));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radRadioButton_Round3 = new Telerik.WinControls.UI.RadRadioButton();
            this.RadGridView_No = new Telerik.WinControls.UI.RadGridView();
            this.radRadioButton_Round2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Round1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.RadDateTimePicker_Now = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radStatusStrip2 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_SendMoney = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_Empl = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_Ok = new Telerik.WinControls.UI.RadGridView();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_No)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_No.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Now)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip2)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Empl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Empl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ok.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(889, 626);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 604);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(883, 19);
            this.radLabel_Detail.TabIndex = 57;
            this.radLabel_Detail.Text = "<html>สีม่วง &gt;&gt; ค้างส่ง</html>";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(883, 595);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radRadioButton_Round3);
            this.panel2.Controls.Add(this.RadGridView_No);
            this.panel2.Controls.Add(this.radRadioButton_Round2);
            this.panel2.Controls.Add(this.radRadioButton_Round1);
            this.panel2.Controls.Add(this.radLabel_Date);
            this.panel2.Controls.Add(this.RadDateTimePicker_Now);
            this.panel2.Controls.Add(this.RadButton_Search);
            this.panel2.Controls.Add(this.radStatusStrip2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(676, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(204, 589);
            this.panel2.TabIndex = 3;
            // 
            // radRadioButton_Round3
            // 
            this.radRadioButton_Round3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Round3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Round3.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_Round3.Location = new System.Drawing.Point(15, 172);
            this.radRadioButton_Round3.Name = "radRadioButton_Round3";
            this.radRadioButton_Round3.Size = new System.Drawing.Size(168, 19);
            this.radRadioButton_Round3.TabIndex = 84;
            this.radRadioButton_Round3.Text = "รอบ3 > 15.01-19.30น.";
            this.radRadioButton_Round3.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Round3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Round3_ToggleStateChanged);
            // 
            // RadGridView_No
            // 
            this.RadGridView_No.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_No.Location = new System.Drawing.Point(58, 492);
            // 
            // 
            // 
            this.RadGridView_No.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_No.Name = "RadGridView_No";
            // 
            // 
            // 
            this.RadGridView_No.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_No.Size = new System.Drawing.Size(124, 73);
            this.RadGridView_No.TabIndex = 17;
            this.RadGridView_No.Visible = false;
            this.RadGridView_No.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_No_ViewCellFormatting);
            // 
            // radRadioButton_Round2
            // 
            this.radRadioButton_Round2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Round2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Round2.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_Round2.Location = new System.Drawing.Point(15, 145);
            this.radRadioButton_Round2.Name = "radRadioButton_Round2";
            this.radRadioButton_Round2.Size = new System.Drawing.Size(168, 19);
            this.radRadioButton_Round2.TabIndex = 83;
            this.radRadioButton_Round2.Text = "รอบ2 > 08.31-15.00น.";
            this.radRadioButton_Round2.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Round2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Round2_ToggleStateChanged);
            // 
            // radRadioButton_Round1
            // 
            this.radRadioButton_Round1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Round1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Round1.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_Round1.Location = new System.Drawing.Point(14, 117);
            this.radRadioButton_Round1.Name = "radRadioButton_Round1";
            this.radRadioButton_Round1.Size = new System.Drawing.Size(168, 19);
            this.radRadioButton_Round1.TabIndex = 82;
            this.radRadioButton_Round1.Text = "รอบ1 > 19.30-08.30น.";
            this.radRadioButton_Round1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Round1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Round1_ToggleStateChanged);
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(15, 54);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(70, 19);
            this.radLabel_Date.TabIndex = 78;
            this.radLabel_Date.Text = "วันที่ส่งเงิน";
            // 
            // RadDateTimePicker_Now
            // 
            this.RadDateTimePicker_Now.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_Now.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_Now.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_Now.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_Now.Location = new System.Drawing.Point(15, 78);
            this.RadDateTimePicker_Now.Name = "RadDateTimePicker_Now";
            this.RadDateTimePicker_Now.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_Now.TabIndex = 77;
            this.RadDateTimePicker_Now.TabStop = false;
            this.RadDateTimePicker_Now.Text = "07/08/2022";
            this.RadDateTimePicker_Now.Value = new System.DateTime(2022, 8, 7, 0, 0, 0, 0);
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(15, 211);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 76;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radStatusStrip2
            // 
            this.radStatusStrip2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator2,
            this.radButtonElement_SendMoney,
            this.commandBarSeparator4,
            this.radButtonElement_Print,
            this.commandBarSeparator1,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3});
            this.radStatusStrip2.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip2.Name = "radStatusStrip2";
            this.radStatusStrip2.Size = new System.Drawing.Size(204, 36);
            this.radStatusStrip2.TabIndex = 51;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_SendMoney
            // 
            this.radButtonElement_SendMoney.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_SendMoney.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.radButtonElement_SendMoney.Name = "radButtonElement_SendMoney";
            this.radStatusStrip2.SetSpring(this.radButtonElement_SendMoney, false);
            this.radButtonElement_SendMoney.Text = "radButtonElement1";
            this.radButtonElement_SendMoney.UseCompatibleTextRendering = false;
            this.radButtonElement_SendMoney.Click += new System.EventHandler(this.RadButtonElement_SendMoney_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Print
            // 
            this.radButtonElement_Print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_Print.Name = "radButtonElement_Print";
            this.radStatusStrip2.SetSpring(this.radButtonElement_Print, false);
            this.radButtonElement_Print.Text = "";
            this.radButtonElement_Print.Click += new System.EventHandler(this.RadButtonElement_Print_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip2.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.RadGridView_Empl, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.RadGridView_Ok, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(667, 589);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // RadGridView_Empl
            // 
            this.RadGridView_Empl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Empl.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Empl.Location = new System.Drawing.Point(3, 297);
            // 
            // 
            // 
            this.RadGridView_Empl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Empl.Name = "RadGridView_Empl";
            // 
            // 
            // 
            this.RadGridView_Empl.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Empl.Size = new System.Drawing.Size(661, 289);
            this.RadGridView_Empl.TabIndex = 17;
            this.RadGridView_Empl.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Empl_ViewCellFormatting);
            this.RadGridView_Empl.SelectionChanged += new System.EventHandler(this.RadGridView_Empl_SelectionChanged);
            // 
            // RadGridView_Ok
            // 
            this.RadGridView_Ok.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Ok.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Ok.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Ok.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.RadGridView_Ok.Name = "RadGridView_Ok";
            // 
            // 
            // 
            this.RadGridView_Ok.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Ok.Size = new System.Drawing.Size(661, 288);
            this.RadGridView_Ok.TabIndex = 17;
            this.RadGridView_Ok.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Ok_ViewCellFormatting);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // ARSendMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 626);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "ARSendMoney";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.ARSendMoney_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_No.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_No)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_Now)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip2)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Empl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Empl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ok.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_SendMoney;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadGridView RadGridView_Empl;
        private Telerik.WinControls.UI.RadGridView RadGridView_Ok;
        private Telerik.WinControls.UI.RadGridView RadGridView_No;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_Now;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Round3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Round2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Round1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    }
}
