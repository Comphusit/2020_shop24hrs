﻿namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    partial class MNPM_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNPM_Main));
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radRadioButton_Transfer = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_LO = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_EmplID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_ID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CarName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CarID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_LO = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_Apv = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadCheckBox_coin = new Telerik.WinControls.UI.RadCheckBox();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.PrintDocument_Slip = new System.Drawing.Printing.PrintDocument();
            this.PrintDocument_send = new System.Drawing.Printing.PrintDocument();
            this.printDocument_sta = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Transfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_LO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CarName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CarID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Apv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_coin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 605);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ShowHD.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellClick);
            this.RadGridView_ShowHD.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellDoubleClick);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 19);
            this.radLabel_Detail.TabIndex = 53;
            this.radLabel_Detail.Text = "<html>DoubleClick ที่รายการ &gt;&gt; พิมพ์สลิปส่งเงิน | DoubleClick ประเภทซอง &gt" +
    ";&gt; ยกเลิกซองส่งเงิน | สีแดง &gt;&gt; ควรส่งซองนี้ก่อน | สีเขียว &gt;&gt; สลิป" +
    "เงินเดือน</html>";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.radLabel_EmplName);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radTextBox_Remark);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radTextBox_EmplID);
            this.panel1.Controls.Add(this.radLabel_ID);
            this.panel1.Controls.Add(this.radLabel_CarName);
            this.panel1.Controls.Add(this.radLabel_CarID);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radTextBox_LO);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadButton_Apv);
            this.panel1.Controls.Add(this.RadButton_Cancel);
            this.panel1.Controls.Add(this.RadCheckBox_coin);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 100;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radRadioButton_Transfer);
            this.panel2.Controls.Add(this.radRadioButton_LO);
            this.panel2.Location = new System.Drawing.Point(11, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(178, 50);
            this.panel2.TabIndex = 76;
            // 
            // radRadioButton_Transfer
            // 
            this.radRadioButton_Transfer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Transfer.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_Transfer.Location = new System.Drawing.Point(10, 3);
            this.radRadioButton_Transfer.Name = "radRadioButton_Transfer";
            this.radRadioButton_Transfer.Size = new System.Drawing.Size(136, 19);
            this.radRadioButton_Transfer.TabIndex = 65;
            this.radRadioButton_Transfer.TabStop = false;
            this.radRadioButton_Transfer.Text = "ฝากเงินเข้าธนาคาร";
            this.radRadioButton_Transfer.CheckStateChanged += new System.EventHandler(this.RadRadioButton_Transfer_CheckStateChanged);
            // 
            // radRadioButton_LO
            // 
            this.radRadioButton_LO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_LO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_LO.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_LO.Location = new System.Drawing.Point(11, 26);
            this.radRadioButton_LO.Name = "radRadioButton_LO";
            this.radRadioButton_LO.Size = new System.Drawing.Size(142, 19);
            this.radRadioButton_LO.TabIndex = 64;
            this.radRadioButton_LO.Text = "ระบุรอบ LO [Enter]";
            this.radRadioButton_LO.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.AutoSize = false;
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_EmplName.Location = new System.Drawing.Point(6, 340);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(184, 23);
            this.radLabel_EmplName.TabIndex = 75;
            this.radLabel_EmplName.Text = ":";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(11, 393);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(65, 19);
            this.radLabel6.TabIndex = 74;
            this.radLabel6.Text = "หมายเหตุ";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.AcceptsReturn = true;
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(11, 418);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(179, 80);
            this.radTextBox_Remark.TabIndex = 2;
            this.radTextBox_Remark.Tag = "";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(10, 283);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(132, 19);
            this.radLabel5.TabIndex = 72;
            this.radLabel5.Text = "ระบุรหัสผู้ส่ง [Enter]";
            // 
            // radTextBox_EmplID
            // 
            this.radTextBox_EmplID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_EmplID.Location = new System.Drawing.Point(10, 308);
            this.radTextBox_EmplID.MaxLength = 7;
            this.radTextBox_EmplID.Name = "radTextBox_EmplID";
            this.radTextBox_EmplID.Size = new System.Drawing.Size(179, 25);
            this.radTextBox_EmplID.TabIndex = 1;
            this.radTextBox_EmplID.Text = "0054450";
            this.radTextBox_EmplID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_EmplID_KeyDown);
            this.radTextBox_EmplID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_EmplID_KeyPress);
            // 
            // radLabel_ID
            // 
            this.radLabel_ID.AutoSize = false;
            this.radLabel_ID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_ID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_ID.Location = new System.Drawing.Point(3, 201);
            this.radLabel_ID.Name = "radLabel_ID";
            this.radLabel_ID.Size = new System.Drawing.Size(187, 23);
            this.radLabel_ID.TabIndex = 70;
            this.radLabel_ID.Text = ":";
            // 
            // radLabel_CarName
            // 
            this.radLabel_CarName.AutoSize = false;
            this.radLabel_CarName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CarName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CarName.Location = new System.Drawing.Point(3, 168);
            this.radLabel_CarName.Name = "radLabel_CarName";
            this.radLabel_CarName.Size = new System.Drawing.Size(187, 23);
            this.radLabel_CarName.TabIndex = 69;
            this.radLabel_CarName.Text = ":";
            // 
            // radLabel_CarID
            // 
            this.radLabel_CarID.AutoSize = false;
            this.radLabel_CarID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CarID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CarID.Location = new System.Drawing.Point(3, 135);
            this.radLabel_CarID.Name = "radLabel_CarID";
            this.radLabel_CarID.Size = new System.Drawing.Size(186, 23);
            this.radLabel_CarID.TabIndex = 68;
            this.radLabel_CarID.Text = ":";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(6, 106);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(25, 19);
            this.radLabel2.TabIndex = 67;
            this.radLabel2.Text = "LO";
            // 
            // radTextBox_LO
            // 
            this.radTextBox_LO.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_LO.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_LO.Location = new System.Drawing.Point(33, 104);
            this.radTextBox_LO.Name = "radTextBox_LO";
            this.radTextBox_LO.Size = new System.Drawing.Size(156, 25);
            this.radTextBox_LO.TabIndex = 0;
            this.radTextBox_LO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_LO_KeyDown);
            this.radTextBox_LO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_LO_KeyPress);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(17, 515);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(128, 19);
            this.radLabel1.TabIndex = 65;
            this.radLabel1.Text = "ระบุรอบ LO [Enter]";
            this.radLabel1.Visible = false;
            // 
            // RadButton_Apv
            // 
            this.RadButton_Apv.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Apv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Apv.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Apv.Location = new System.Drawing.Point(10, 558);
            this.RadButton_Apv.Name = "RadButton_Apv";
            this.RadButton_Apv.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Apv.TabIndex = 3;
            this.RadButton_Apv.Text = "ยืนยัน";
            this.RadButton_Apv.ThemeName = "Fluent";
            this.RadButton_Apv.Click += new System.EventHandler(this.RadButton_Apv_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).Text = "ยืนยัน";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(10, 596);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Cancel.TabIndex = 4;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadCheckBox_coin
            // 
            this.RadCheckBox_coin.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_coin.Location = new System.Drawing.Point(10, 249);
            this.RadCheckBox_coin.Name = "RadCheckBox_coin";
            this.RadCheckBox_coin.Size = new System.Drawing.Size(137, 19);
            this.RadCheckBox_coin.TabIndex = 62;
            this.RadCheckBox_coin.Text = "ซองเงินแลกเหรียญ";
            this.RadCheckBox_coin.Visible = false;
            this.RadCheckBox_coin.CheckStateChanged += new System.EventHandler(this.RadCheckBox_coin_CheckStateChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_add,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator1});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "Export To Excel";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // PrintDocument_Slip
            // 
            this.PrintDocument_Slip.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_Slip_PrintPage);
            // 
            // PrintDocument_send
            // 
            this.PrintDocument_send.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_send_PrintPage);
            // 
            // printDocument_sta
            // 
            this.printDocument_sta.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_sta_PrintPage);
            // 
            // MNPM_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "MNPM_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ซองส่งเงิน";
            this.Load += new System.EventHandler(this.MNPM_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Transfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_LO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_EmplID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CarName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CarID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Apv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_coin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_coin;
        protected Telerik.WinControls.UI.RadButton RadButton_Apv;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_LO;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_CarID;
        private Telerik.WinControls.UI.RadLabel radLabel_ID;
        private Telerik.WinControls.UI.RadLabel radLabel_CarName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private System.Drawing.Printing.PrintDocument PrintDocument_Slip;
        private System.Drawing.Printing.PrintDocument PrintDocument_send;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_LO;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Transfer;
        private System.Drawing.Printing.PrintDocument printDocument_sta;
    }
}
