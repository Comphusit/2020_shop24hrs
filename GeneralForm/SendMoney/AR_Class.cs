﻿//CheckOK
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.MNPM
{
    public class Var_RECIVEMONEY
    {
        public string Transdate { get; set; } = "";
        public string SlRounddate { get; set; } = "";
        public string Posgroup { get; set; } = "";
        public string Cashierid { get; set; } = "";
        public double Cashier_com { get; set; } = 0;
        public string Cashiername { get; set; } = "";
        public string Slround { get; set; } = "";
        public double Receivesum { get; set; } = 0;
        public double Total_receivesum { get; set; } = 0;
        public string Rank { get; set; } = "";
        public double Totalcom { get; set; } = 0;
    }
    public class Var_SendMoneyToFinance
    {
        public string DATESENDMONEY { get; set; } = "";
        public string DATETRANSMONEY { get; set; } = "";
        public double Bank1000 { get; set; } = 0;
        public double Bank500 { get; set; } = 0;
        public double Bank100 { get; set; } = 0;
        public double Bank50 { get; set; } = 0;
        public double Bank20 { get; set; } = 0;
        public double Coins { get; set; } = 0;
        public double Total { get; set; } = 0;
        public int Seq { get; set; } = 1;
        public string Round { get; set; } = "";
        public string EMPLID { get; set; } = "";
    }
    public static class AR_Class
    {
        #region Report_MNPM      
        // รายงานยอดส่งเงินขาด - เกิน
        public static DataTable GetMoneyCash_ByMonth(string DateBegin, string DateEnd, string Branch)
        {
            string bchCodition = "";
            if (Branch != "") bchCodition = $@" AND POSGROUP = '{Branch}' ";
            string sql = $@"
                SELECT	Com.posgroup AS BRANCH_ID,BRANCH_NAME,
		                Com.cashierid as Comcashierid,cashiername as SPC_NAME,
		                convert(varchar, Com.transdate,23) as Comtransdate,
		                sum(Com.cashier_com) as Comcashier_com,
		                sum(Cash.total_receivesum) as Cashtotal_receivesum ,
		                sum(Cash.total_receivesum)- sum(Com.cashier_com)  as Money,  
		                sum(Com.money_deductions) as money_deductions
                FROM
		                (
			                SELECT	posgroup, cashierid,cashiername, cashier_com, reason_branch, money_deductions, transdate
			                FROM	Shop_ReceiveMoney WITH (NOLOCK)
			                WHERE	transdate between '{DateBegin}' and '{DateEnd}' AND rank = '1'
					                {bchCodition}
		                )Com INNER JOIN
		                (
			                SELECT	posgroup, cashierid, sum(total_receivesum) as total_receivesum, transdate
			                FROM	Shop_ReceiveMoney  WITH (NOLOCK)
			                WHERE	transdate between  '{DateBegin}' and '{DateEnd}'
					                {bchCodition}
			                GROUP BY transdate, posgroup, cashierid
		                )Cash ON com.posgroup=Cash.posgroup AND com.cashierid=Cash.cashierid AND com.transdate=Cash.transdate
		                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON Com.posgroup= SHOP_BRANCH.BRANCH_ID

                GROUP BY Com.posgroup , BRANCH_NAME, Com.cashierid  , cashiername  , Com.transdate
                ORDER BY Com.posgroup,Comcashierid,Comtransdate
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        // รายงานยอดส่งเงินขาด - เกิน สาขา
        public static DataTable GetMoneyCash_AllBranch(string DateBegin, string posgroupStart, string posgroupEnd, string cashID)
        {

            string cashIDCondition = "";
            if (cashID != "") cashIDCondition = $@" AND Com.cashierid = '{cashID}' ";
            string sql = $@"
                SELECT  Com.posgroup as Composgroup,BRANCH_NAME,
		                Com.cashierid as Comcashierid,
		                cashiername as SPC_NAME,Com.cashiername,
		                Convert(varchar,Com.transdate,23) as Comtransdate,  
		                Com.cashier_com as Comcashier_com,
		                Com.statusbranch as statusbranch,
		                Com.status_deductions as status_deductions,
		                Com.reason_branch as Comreason_branch,
		                Com.money_deductions as Commoney_deductions,  
		                Cash.posgroup as Cashposgroup,Cash.cashierid as Cashcashierid,
		                Cash.total_receivesum as Cashtotal_receivesum,
		                Cash.transdate as Cashtransdate,  
		                Cash.total_receivesum as  total_receivesum,
		                Cash.total_receivesum-Com.cashier_com as Money 
                FROM	(
			                SELECT	posgroup,cashierid,cashiername,cashier_com,reason_branch,money_deductions,transdate,statusbranch ,status_deductions 
			                FROM	Shop_ReceiveMoney  WITH (NOLOCK)
			                WHERE	transdate = '{DateBegin}'  AND rank='1'  
		                )Com INNER JOIN
		                (
			                SELECT	posgroup,cashierid,sum(receivesum) as total_receivesum,transdate 
			                FROM	Shop_ReceiveMoney  WITH (NOLOCK)
			                WHERE	transdate =  '{DateBegin}'
			                GROUP BY transdate,posgroup,cashierid
		                )Cash on com.posgroup = Cash.posgroup and com.cashierid=Cash.cashierid 
                        INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON Com.posgroup = SHOP_BRANCH.BRANCH_ID
		
                WHERE	Com.posgroup between '{posgroupStart}' and '{posgroupEnd}'
		                AND Cash.posgroup  between '{posgroupStart}' and '{posgroupEnd}'
                        {cashIDCondition}
                
                ORDER BY Comtransdate,Composgroup ,Comcashierid
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //GetLastSend
        public static string GetLastSend_ByBchID(string pBch)
        {
            string sql = $@"
                SELECT	TOP 1 CONVERT(VARCHAR,MNPMDate,23)+ ' ' +MNPMTime AS DATELASTSEND  
                FROM	SHOP_MNPM_HD WITH (NOLOCK)   
                WHERE	MNPMBranch = '{pBch}' 
                ORDER BY MNPMDate DESC,MNPMTime DESC ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0) return "ไม่พบประวัติการส่ง"; else return dt.Rows[0]["DATELASTSEND"].ToString();
        }
        #endregion

        #region "ARMoneyCash"

        //check ยอดเงินทั้งหมด
        public static DataTable GetRoundSendMoney(string DateTransDate, string BranchBegin, string BranchEnd, string CashID)
        {
            string sqlcondition = $@" totalcom,money_deductions ";
            if (SystemClass.SystemBranchID != "MN000")
                sqlcondition = $@"case  when money_deductions> 0 then 0 else money_deductions end as money_deductions ,case  when money_deductions> 0 then 0 else totalcom end as totalcom ";

            string cashIDCondition = "";
            if (CashID != "") cashIDCondition = $@" AND cashierid = '{CashID}' ";

            string sql = $@"
                SELECT	rank,CONVERT(VARCHAR,transdate,23) AS transdate, receivesum,total_receivesum, slRounddate, 
		                posgroup,BRANCH_NAME, cashierid, cashier_com, cashiername, slround,reason_branch,
		                CASE status_deductions WHEN '1' THEN ISNULL(who_up,'') + ' ' + ISNULL(whoName_up,'') ELSE '' END AS WHOUPD ,
                        {sqlcondition}
                FROM	Shop_ReceiveMoney WITH (NOLOCK)
                        INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON Shop_ReceiveMoney.posgroup = SHOP_BRANCH.BRANCH_ID 
                WHERE	transdate = '{DateTransDate}'
		                AND posgroup between '{BranchBegin}' and '{BranchEnd}'
                        {cashIDCondition}
                ORDER BY transdate, posgroup, cashierid, slround
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Check การ Insert รอบส่งเงิน
        public static DataTable Check_ReasonSaveRoundSendMoney(string DateTransDate, string BranchBegin, string BranchEnd)
        {
            string sql = $@"
                SELECT  * 
                FROM	Shop_ReceiveMoney WITH (NOLOCK)
                WHERE	transdate = '{DateTransDate}' AND posgroup between '{BranchBegin}' and '{BranchEnd}'
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #endregion

        #region"Report_ARAtBranch"
        //รายงานการส่งเงินตามแคชเชียร์
        public static DataTable GetCashOfBranch(string DateBegin, string DateEnd, string Branch)
        {
            string bchCondition = "";
            if (Branch != "") bchCondition = $@" AND posgroup = '{Branch}' ";
            string Sql = $@"
            SELECT  posgroup AS BRANCH_ID,BRANCH_NAME,CASHIERID, CASHIERNAME
			FROM    Shop_ReceiveMoney WITH(NOLOCK) 
                    INNER JOIN SHOP_BRANCH WITH(NOLOCK) ON Shop_ReceiveMoney.posgroup = SHOP_BRANCH.BRANCH_ID
            WHERE   Transdate between '{DateBegin}' and '{DateEnd}'
					{bchCondition}
            GROUP BY posgroup,BRANCH_NAME,cashierid,cashiername
			ORDER BY posgroup, BRANCH_NAME, cashierid, cashiername ";

            return ConnectionClass.SelectSQL_Main(Sql);
        }
        //รายงานการส่งเงินตามแคชเชียร์ ตามวันที่
        public static DataTable GetMoneyRound(string DateBegin, string DateEnd, string Branch)
        {
            string bchCondition = "";
            if (Branch != "") bchCondition = $@" AND posgroup = '{Branch}' ";
            string Sql = $@"
            SELECT * from (Select day(Transdate) AS DayT, posgroup, 
					transdate,FORMAT (transdate, 'MM-dd') as MMDDtransdate, cashierid, cashiername,
					SUM(receivesum) AS receivesum,
					SUM(total_receivesum) AS total_receivesum,
					SUM(totalcom) as totalcom, 
					SUM(receivesum) - SUM(totalcom) AS LackOfMoney,
				    SUM(money_deductions) AS DeductedMoney 
			FROM    Shop_ReceiveMoney WITH(NOLOCK)
            WHERE   Transdate between '{DateBegin}' and '{DateEnd}'
					{bchCondition}
            GROUP BY day(Transdate),posgroup,transdate,cashierid,cashiername)LackOfMoney where LackOfMoney<0 
            ORDER BY DayT, posgroup, cashierid ";
            return ConnectionClass.SelectSQL_Main(Sql);
        }
        #endregion

        #region "ARSendMoney"
        //บันทึกส่งเงินสด
        public static DataTable GetMNAR(string DATESENDMONEY, string DATETRANSMONEY, string EMPLID, string ROUND)
        {
            string EMP = $@" AND EMPLID = '{EMPLID}' ";
            if (ROUND == "4")
            {
                EMP = $@" AND SENDMONEYID = '{EMPLID}' ";
            }
            string sql = $@"
                SELECT  SENDMONEYID, DATEDOC, DATESENDMONEY, DATETRANSMONEY, ROUND, SEQ, EMPLID, 
		                BANK1000, BANK500, BANK100, BANK50, BANK20, COINS, NET
                FROM	Shop_SendMoneyToFinance WITH (NOLOCK)  
                WHERE	ROUND = '{ROUND}' AND STATUSRECEIVE!= '3'
		                AND DATESENDMONEY = '{DATESENDMONEY}'
		                AND DATETRANSMONEY = '{DATETRANSMONEY}'
		                {EMP}
                ORDER BY SEQ
                ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาลำดับการส่งเงิน
        public static int GetSEQ(string DATESENDMONEY, string DATETRANSMONEY, string EMPLID, string ROUND)
        {
            string sql = $@"
                SELECT	count(isnull(SEQ,0))+1 as SEQ 
                FROM	Shop_SendMoneyToFinance WITH (NOLOCK)
                WHERE	ROUND = '{ROUND}' AND STATUSRECEIVE!= '3'
		                AND DATESENDMONEY = '{DATESENDMONEY}'
		                AND DATETRANSMONEY = '{DATETRANSMONEY}'
		                AND EMPLID = '{EMPLID}'
                ORDER BY SEQ
                ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            int seq = 1;
            if (dt.Rows.Count > 0) seq = int.Parse(dt.Rows[0]["SEQ"].ToString());
            return seq;
        }


        #endregion

        #region Insert_UpdateData

        public static string Save_ReciveMoney(Var_RECIVEMONEY varDetail)
        {
            return $@"
                INSERT INTO	Shop_ReceiveMoney (transdate, slRounddate, posgroup, cashierid, 
                        cashier_com,cashiername, 
                        slround, receivesum,total_receivesum, rank, totalcom,
		                who_receive,who_branch, who_in,whoname_in) 
                values ('{varDetail.Transdate}','{varDetail.SlRounddate}','{varDetail.Posgroup}','{varDetail.Cashierid}',
                        '{varDetail.Cashier_com}','{varDetail.Cashiername}',
		                '{varDetail.Slround}','{varDetail.Receivesum}','{varDetail.Total_receivesum}','{varDetail.Rank}','{varDetail.Totalcom}',
		                '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserID_M}', 
		                '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}')";
        }
        //Update reason
        public static string UpdateReason_ReciveMoney(string remark, string cashID, string date, double money)
        {
            return $@"
                        UPDATE  Shop_ReceiveMoney 
                        SET     money_deductions = '{money}',status_deductions = '1',
                                ReasonId = '',  
                                reason_branch = '{remark}',
                                who_up = '{SystemClass.SystemUserID}',
                                whoname_up = '{SystemClass.SystemUserName}',
                                date_up = convert(varchar, getdate(), 23),
                                time_up = convert(varchar, getdate(), 24)
                        WHERE	cashierid = '{cashID}'
		                        and transdate = '{date}'
		                        and rank = '1' ";
        }
        //update New Data
        public static string UpdateRound_ReciveMoney(string round, double moneyCom, double reciveSum, double totalRecive, double totalCom)
        {
            return $@"
                        UPDATE  Shop_ReceiveMoney 
                        SET     cashier_com = '{moneyCom}',receivesum = '{reciveSum}',
                                total_receivesum = '{totalRecive}',  
                                totalcom = '{totalCom}',
                                statusrecieve = '0', 
                                who_in = '{SystemClass.SystemUserID}',
                                whoname_in = '{SystemClass.SystemUserName}',
                                date_in=convert(varchar,getdate(),23), time_in=convert(varchar,getdate(),24) ,
                                date_receive=convert(varchar,getdate(),23), time_receive=convert(varchar,getdate(),24)
                        WHERE	slround = '{round}'  ";

        }
        //Update money
        public static string UpdateMoney_ReciveMoney(string cashID, string date, double money)
        {
            return $@"
                        UPDATE  Shop_ReceiveMoney 
                        SET     money_deductions = '{money}'
                        WHERE	cashierid = '{cashID}'
		                        and transdate = '{date}'
		                        and rank = '1' ";
        }
        //insert SendMoneyToFinance
        public static string Save_SendMoneyToFinance(string MNAR, Var_SendMoneyToFinance var_SendMoneyToFinance)
        {
            return $@"
            INSERT INTO Shop_SendMoneyToFinance(
                    SENDMONEYID, DATESENDMONEY, DATETRANSMONEY, ROUND, 
                    EMPLID,
                    BANK1000, BANK500, BANK100, 
                    BANK50, BANK20, COINS,
                    Net,WHOSEND,WHOINS,SEQ)
            VALUES  ('{MNAR}','{var_SendMoneyToFinance.DATESENDMONEY}', '{var_SendMoneyToFinance.DATETRANSMONEY}', '{var_SendMoneyToFinance.Round}',
                    '{var_SendMoneyToFinance.EMPLID}',
                    '{var_SendMoneyToFinance.Bank1000}', '{var_SendMoneyToFinance.Bank500}', '{var_SendMoneyToFinance.Bank100}',
                    '{var_SendMoneyToFinance.Bank50}', '{var_SendMoneyToFinance.Bank20}', '{var_SendMoneyToFinance.Coins}',
                    '{var_SendMoneyToFinance.Total}','{SystemClass.SystemUserID_M}','{SystemClass.SystemUserID_M}','{var_SendMoneyToFinance.Seq}') ";
        }
        //Insert สลิป EMV/บัญชีลูกหนี้ ส่งเงิน
        public static string Save_MNSM(string mnsm, string emplID, string emplName, string dptID, string dptName, double grand, string remark, string typeDesc, string typeSta)
        {
            string sql = $@"
                Insert Into  SHOP_MNSM  
                        (BranchID,BranchName,DRAWERID,EmplID,EmplName,EmplNUM,EmplDepart,LINEAMOUNT,Remark,WhoINS,TYPE_DESC,TYPE_STA) 
                values ('{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}','{mnsm}',
                        '{emplID}','{emplName}','{dptID}','{dptName}',
                        '{grand}','{remark}','{SystemClass.SystemUserID}','{typeDesc}','{typeSta}') ";
            return ConnectionClass.ExecuteSQL_Main(sql);
        }

        #endregion


    }
}

