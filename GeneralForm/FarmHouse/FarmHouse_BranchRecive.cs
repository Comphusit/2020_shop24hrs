﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.FarmHouse
{
    public partial class FarmHouse_BranchRecive : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();

        readonly string _pTypeOpen;//SUPC - SHOP
        readonly string _pPermission;//0 view only --  1 มีสิด

        string selectedBchID, selectedDate;

        //Load
        public FarmHouse_BranchRecive(string pTypeOpen, string pPermission)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
            _pPermission = pPermission;
        }
        //Load
        private void FarmHouse_BranchRecive_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_Recive.ToolTipText = "รับบิล [พิมพ์ MA]"; radButtonElement_Recive.ShowBorder = true;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Search.ButtonElement.ShowBorder = true;

            RadDropDownList_1.DropDownListElement.Font = SystemClass.SetFontGernaral_Bold; RadDropDownList_1.Enabled = false;
            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(radGridView_Image);

            radGridView_Image.MasterTemplate.EnableFiltering = false;

            RadDropDownList_1.Visible = true;
            RadCheckBox_1.Visible = true;
            //DataTable dtBch = new DataTable();
            switch (_pTypeOpen)
            {
                case "SHOP":
                    RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;
                    radLabel_Detail.Text = "สีแดง >> มีการแก้ไขจำนวน | เลือกรายการ >> กด + เพื่อพิมพ์สลิปรับของ [MA] | Double Click รูป >> เพื่อดูรูปขนาดใหญ๋";

                    break;
                case "SUPC":
                    if (_pPermission == "0") radLabel_Detail.Text = "สีแดง >> มีการแก้ไขจำนวน | Double Click รูป >> เพื่อดูรูปขนาดใหญ๋";
                    else radLabel_Detail.Text = "สีแดง >> มีการแก้ไขจำนวน | Double Click >> เพื่อแก้ไขจำนวน | เลือกรายการ >> กด + เพื่อพิมพ์สลิปรับของ [MA] | Double Click รูป >> เพื่อดูรูปขนาดใหญ๋";

                    break;
                default: break;
            }

            DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");


            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveDate", "วันที่รับ", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveBranchID", "สาขา", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveBranchName", "ชื่อสาขา", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveItemBarcode", "บาร์โค้ด", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveName", "ชื่อสินค้า", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTYBch", "จำนวนสาขารับ", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QtyEdit", "จำนวนส่วนกลางแก้", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ReciveUnit", "หน่วย", 80)));

            RadGridView_ShowHD.MasterTemplate.Columns[0].IsPinned = true;
            RadGridView_ShowHD.MasterTemplate.Columns[1].IsPinned = true;

            DatagridClass.SetCellBackClolorByExpression("QtyEdit", "QtyEdit > 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

            GridViewSummaryItem summaryItem6 = new GridViewSummaryItem
            {
                Name = "QTYBch",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryItem summaryItem7 = new GridViewSummaryItem
            {
                Name = "QtyEdit",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem6 = new GridViewSummaryRowItem { summaryItem6, summaryItem7 };

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem6);

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            try
            {
                radGridView_Image.Columns.Clear();
                radGridView_Image.Rows.Clear();
            }
            catch (Exception) { }

            string pConBch = "";
            if (RadCheckBox_1.Checked == true) pConBch = RadDropDownList_1.SelectedValue.ToString();
            dt_Data = ConnectionClass.SelectSQL_Main(FarmHouse_Class.FarmHouse_BranchRecive(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                               radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pConBch, "V005450"));
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            if (dt_Data.Rows.Count > 0)
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["ReciveDate"].Value.ToString() == "") return;

                string selectBchid = RadGridView_ShowHD.CurrentRow.Cells["ReciveBranchID"].Value.ToString();
                string selectDate = RadGridView_ShowHD.CurrentRow.Cells["ReciveDate"].Value.ToString();

                if ((selectBchid == selectedBchID) && (selectDate == selectedDate)) return;

                ImageFind(selectBchid, selectDate);
            }

            this.Cursor = Cursors.Default;
        }
        //Find Image
        void ImageFind(string pBchID, string pDate)
        {
            try
            {
                radGridView_Image.Columns.Clear();
                radGridView_Image.Rows.Clear();
            }
            catch (Exception) { }

            string path = PathImageClass.pPathVender + pDate + @"\" + pBchID;
            if (!Directory.Exists(path)) return;

            DirectoryInfo DirInfo = new DirectoryInfo(path);
            FileInfo[] Files = DirInfo.GetFiles("*V005450*.JPG", SearchOption.AllDirectories);
            if (Files.Length > 0)
            {
                for (int i = 0; i < Files.Length; i++)
                {
                    radGridView_Image.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(
                         i.ToString(), "รูปที่ " + (i + 1).ToString(), 200)));
                    radGridView_Image.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PATH_" + i.ToString(), "PATH" + (i + 1).ToString())));
                }

                radGridView_Image.TableElement.RowHeight = 150;
                radGridView_Image.Rows.AddNew();

                int iC = 0;
                foreach (FileInfo item in Files)
                {
                    radGridView_Image.Rows[0].Cells[iC.ToString()].Value =
                        ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(item.FullName);
                    radGridView_Image.Rows[0].Cells["PATH_" + iC.ToString()].Value = path + "|" + item.Name;
                    iC++;
                }

            }

        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            selectedBchID = ""; selectedDate = "";
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดรับสินค้าฟาร์มเฮ้าส์", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true;
            else RadDropDownList_1.Enabled = false;
        }
        //MA
        private void RadButtonElement_Recive_Click(object sender, EventArgs e)
        {
            //Check Data
            if (RadGridView_ShowHD.RowCount == 0) return;
            //เชค รูป
            if (radGridView_Image.Rows.Count == 0) return;

            //เชควันที่ปัจจุบันสำหรับสาขาห
            string pDateRecive = RadGridView_ShowHD.CurrentRow.Cells["ReciveDate"].Value.ToString();
            if (SystemClass.SystemBranchID != "MN000")
            {
                if (DateTime.Parse(pDateRecive).ToString("yyyy-MM-dd") != DateTime.Today.ToString("yyyy-MM-dd"))
                {
                    if (DateTime.Parse(pDateRecive).ToString("yyyy-MM-dd") != DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถสร้างหรือแก้ไขสลิป MA ย้อนหลังได้" + Environment.NewLine + " ติดต่อ CenterShop 1022 เพื่อแก้ไขบิลแทน.");
                        return;
                    }
                }

            }

            using (MA.MAReceiveDocument mAReceiveDocument = new MA.MAReceiveDocument(pDateRecive,
                RadGridView_ShowHD.CurrentRow.Cells["ReciveBranchID"].Value.ToString(),
                RadGridView_ShowHD.CurrentRow.Cells["ReciveBranchName"].Value.ToString(),
                "V005450", "เพรซิเดนท์ เบเกอรี่ บจก.",
                FarmHouse_Class.FarmHouse_Recive(RadGridView_ShowHD.CurrentRow.Cells["ReciveBranchID"].Value.ToString(), pDateRecive, "V005450"),""))
            {
                DialogResult dr = mAReceiveDocument.ShowDialog();
                if (dr == DialogResult.Yes) return;

            }
        }

        //CellDouble
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if ((_pTypeOpen == "SUPC") && (_pPermission == "1"))
            {
                string date = e.Row.Cells["ReciveDate"].Value.ToString();
                string barcode = e.Row.Cells["ReciveItemBarcode"].Value.ToString();
                double qty = double.Parse(e.Row.Cells["QTYBch"].Value.ToString());
                if (double.Parse(e.Row.Cells["QtyEdit"].Value.ToString()) > 0) qty = double.Parse(e.Row.Cells["QtyEdit"].Value.ToString());

                FormShare.InputData _inputdata = new FormShare.InputData("0",
                   e.Row.Cells["ReciveBranchID"].Value.ToString() + '-' + e.Row.Cells["ReciveBranchName"].Value.ToString() + " [" + date + @"] " +
                   Environment.NewLine + barcode + '-' + e.Row.Cells["ReciveName"].Value.ToString(),
                    "จำนวนที่ต้องการแก้ไข", e.Row.Cells["ReciveUnit"].Value.ToString())
                {
                    pInputData = qty.ToString()
                };
                if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                {
                    if (qty == double.Parse(_inputdata.pInputData)) return;
                     
                    string T = ConnectionClass.ExecuteSQL_Main(ImageShow.Vender.Vender_ToAXClass.Update_VENDERCHECK(e.Row.Cells["ReciveBranchID"].Value.ToString(), "V005450", date, barcode, qty));
                    if (T == "")
                    {
                        e.Row.Cells["QtyEdit"].Value = _inputdata.pInputData;
                    }
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                }
            }
        }
        //ViewCellFormatting
        private void RadGridView_Image_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
            //e.CellElement.Font = SystemClass.SetFontGernaral;
        }
        // Big Image
        private void RadGridView_Image_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                ImageClass.OpenShowImage("1",
                               radGridView_Image.CurrentRow.Cells["PATH_" + radGridView_Image.CurrentColumn.Name].Value.ToString());
            }
            catch (Exception) { }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        //Check Image
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (e.Row.Cells["ReciveDate"].Value.ToString() == "") return;

            string selectBchid = e.Row.Cells["ReciveBranchID"].Value.ToString();
            string selectDate = e.Row.Cells["ReciveDate"].Value.ToString();

            if ((selectBchid == selectedBchID) && (selectDate == selectedDate))
            {
                try
                {
                    radGridView_Image.Columns.Clear();
                    radGridView_Image.Rows.Clear();
                }
                catch (Exception) { }
                return;
            }

            ImageFind(selectBchid, selectDate);
        }
    }
}
