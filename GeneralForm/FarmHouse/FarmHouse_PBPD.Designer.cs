﻿namespace PC_Shop24Hrs.GeneralForm.FarmHouse
{
    partial class FarmHouse_PBPD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FarmHouse_PBPD));
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton_PXS = new Telerik.WinControls.UI.RadButton();
            this.radButton_PB = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_PB = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Desc = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_PXS = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_PB2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_PB1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_PXS2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_PXS1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_PB = new Telerik.WinControls.UI.RadLabel();
            this.PrintDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Refrsh = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_PXS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_PB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PXS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PXS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PXS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 605);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.GroupSummaryEvaluate += new Telerik.WinControls.UI.GroupSummaryEvaluateEventHandler(this.RadGridView_ShowHD_GroupSummaryEvaluate);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 19);
            this.radLabel_Detail.TabIndex = 53;
            this.radLabel_Detail.Text = "<html>ระบุเลขที่ PB &gt;&gt; Enter | เลือกวันที่ของรอบการวางบิล [PB] กด เช็ค | เล" +
    "ือกวันที่ของรอบการวางใบลดหนี้ [PXS] กด เช็ค</html>";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.radButton_PXS);
            this.panel1.Controls.Add(this.radButton_PB);
            this.panel1.Controls.Add(this.radTextBox_PB);
            this.panel1.Controls.Add(this.radLabel_Desc);
            this.panel1.Controls.Add(this.radLabel_PXS);
            this.panel1.Controls.Add(this.radDateTimePicker_PB2);
            this.panel1.Controls.Add(this.radDateTimePicker_PB1);
            this.panel1.Controls.Add(this.radDateTimePicker_PXS2);
            this.panel1.Controls.Add(this.radDateTimePicker_PXS1);
            this.panel1.Controls.Add(this.RadButton_Save);
            this.panel1.Controls.Add(this.RadButton_Cancel);
            this.panel1.Controls.Add(this.radLabel_PB);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // radButton_PXS
            // 
            this.radButton_PXS.BackColor = System.Drawing.Color.Transparent;
            this.radButton_PXS.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_PXS.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radButton_PXS.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_PXS.Location = new System.Drawing.Point(150, 225);
            this.radButton_PXS.Name = "radButton_PXS";
            this.radButton_PXS.Size = new System.Drawing.Size(34, 48);
            this.radButton_PXS.TabIndex = 72;
            this.radButton_PXS.Text = "radButton3";
            this.radButton_PXS.Click += new System.EventHandler(this.RadButton_PXS_Click);
            // 
            // radButton_PB
            // 
            this.radButton_PB.BackColor = System.Drawing.Color.Transparent;
            this.radButton_PB.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_PB.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radButton_PB.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_PB.Location = new System.Drawing.Point(150, 130);
            this.radButton_PB.Name = "radButton_PB";
            this.radButton_PB.Size = new System.Drawing.Size(34, 48);
            this.radButton_PB.TabIndex = 71;
            this.radButton_PB.Text = "radButton3";
            this.radButton_PB.Click += new System.EventHandler(this.RadButton_PB_Click);
            // 
            // radTextBox_PB
            // 
            this.radTextBox_PB.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_PB.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_PB.Location = new System.Drawing.Point(10, 73);
            this.radTextBox_PB.Name = "radTextBox_PB";
            this.radTextBox_PB.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_PB.TabIndex = 0;
            this.radTextBox_PB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_PB_KeyDown);
            // 
            // radLabel_Desc
            // 
            this.radLabel_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Desc.Location = new System.Drawing.Point(10, 48);
            this.radLabel_Desc.Name = "radLabel_Desc";
            this.radLabel_Desc.Size = new System.Drawing.Size(103, 19);
            this.radLabel_Desc.TabIndex = 70;
            this.radLabel_Desc.Text = "ระบุ PB [Enter]";
            // 
            // radLabel_PXS
            // 
            this.radLabel_PXS.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_PXS.Location = new System.Drawing.Point(10, 200);
            this.radLabel_PXS.Name = "radLabel_PXS";
            this.radLabel_PXS.Size = new System.Drawing.Size(101, 19);
            this.radLabel_PXS.TabIndex = 68;
            this.radLabel_PXS.Text = "รอบวางบิล PXS";
            // 
            // radDateTimePicker_PB2
            // 
            this.radDateTimePicker_PB2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_PB2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_PB2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_PB2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_PB2.Location = new System.Drawing.Point(10, 157);
            this.radDateTimePicker_PB2.Name = "radDateTimePicker_PB2";
            this.radDateTimePicker_PB2.Size = new System.Drawing.Size(134, 21);
            this.radDateTimePicker_PB2.TabIndex = 2;
            this.radDateTimePicker_PB2.TabStop = false;
            this.radDateTimePicker_PB2.Text = "12/08/2022";
            this.radDateTimePicker_PB2.Value = new System.DateTime(2022, 8, 12, 0, 0, 0, 0);
            this.radDateTimePicker_PB2.ValueChanged += new System.EventHandler(this.RadDateTimePicker_PB2_ValueChanged);
            // 
            // radDateTimePicker_PB1
            // 
            this.radDateTimePicker_PB1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_PB1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_PB1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_PB1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_PB1.Location = new System.Drawing.Point(10, 130);
            this.radDateTimePicker_PB1.Name = "radDateTimePicker_PB1";
            this.radDateTimePicker_PB1.Size = new System.Drawing.Size(134, 21);
            this.radDateTimePicker_PB1.TabIndex = 1;
            this.radDateTimePicker_PB1.TabStop = false;
            this.radDateTimePicker_PB1.Text = "12/08/2022";
            this.radDateTimePicker_PB1.Value = new System.DateTime(2022, 8, 12, 0, 0, 0, 0);
            this.radDateTimePicker_PB1.ValueChanged += new System.EventHandler(this.RadDateTimePicker_PB1_ValueChanged);
            // 
            // radDateTimePicker_PXS2
            // 
            this.radDateTimePicker_PXS2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_PXS2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_PXS2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_PXS2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_PXS2.Location = new System.Drawing.Point(10, 252);
            this.radDateTimePicker_PXS2.Name = "radDateTimePicker_PXS2";
            this.radDateTimePicker_PXS2.Size = new System.Drawing.Size(134, 21);
            this.radDateTimePicker_PXS2.TabIndex = 4;
            this.radDateTimePicker_PXS2.TabStop = false;
            this.radDateTimePicker_PXS2.Text = "12/08/2022";
            this.radDateTimePicker_PXS2.Value = new System.DateTime(2022, 8, 12, 0, 0, 0, 0);
            this.radDateTimePicker_PXS2.ValueChanged += new System.EventHandler(this.RadDateTimePicker_PXS2_ValueChanged);
            // 
            // radDateTimePicker_PXS1
            // 
            this.radDateTimePicker_PXS1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_PXS1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_PXS1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_PXS1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_PXS1.Location = new System.Drawing.Point(10, 225);
            this.radDateTimePicker_PXS1.Name = "radDateTimePicker_PXS1";
            this.radDateTimePicker_PXS1.Size = new System.Drawing.Size(134, 21);
            this.radDateTimePicker_PXS1.TabIndex = 3;
            this.radDateTimePicker_PXS1.TabStop = false;
            this.radDateTimePicker_PXS1.Text = "12/08/2022";
            this.radDateTimePicker_PXS1.Value = new System.DateTime(2022, 8, 12, 0, 0, 0, 0);
            this.radDateTimePicker_PXS1.ValueChanged += new System.EventHandler(this.RadDateTimePicker_PXS1_ValueChanged);
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(10, 562);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Save.TabIndex = 62;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(10, 600);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Cancel.TabIndex = 63;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_PB
            // 
            this.radLabel_PB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_PB.Location = new System.Drawing.Point(10, 105);
            this.radLabel_PB.Name = "radLabel_PB";
            this.radLabel_PB.Size = new System.Drawing.Size(116, 19);
            this.radLabel_PB.TabIndex = 60;
            this.radLabel_PB.Text = "รอบการวางบิล PB";
            // 
            // PrintDocument1
            // 
            this.PrintDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Refrsh,
            this.commandBarSeparator3,
            this.radButtonElement_Excel,
            this.commandBarSeparator1,
            this.RadButtonElement_pdt,
            this.commandBarSeparator2});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 42);
            this.radStatusStrip1.TabIndex = 73;
            // 
            // radButtonElement_Refrsh
            // 
            this.radButtonElement_Refrsh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Refrsh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButtonElement_Refrsh.Name = "radButtonElement_Refrsh";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Refrsh, false);
            this.radButtonElement_Refrsh.Text = "radButtonElement1";
            this.radButtonElement_Refrsh.UseCompatibleTextRendering = false;
            this.radButtonElement_Refrsh.Click += new System.EventHandler(this.RadButtonElement_Refrsh_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.ToolTipText = "เพิ่ม";
            this.radButtonElement_Excel.UseCompatibleTextRendering = false;
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.UseCompatibleTextRendering = false;
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // FarmHouse_PBPD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "FarmHouse_PBPD";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FarmHouse_PB";
            this.Load += new System.EventHandler(this.FarmHouse_PBPD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_PXS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_PB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_PB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PXS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PXS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_PXS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadLabel radLabel_PB;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_PB2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_PB1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_PXS2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_PXS1;
        private Telerik.WinControls.UI.RadLabel radLabel_PXS;
        private Telerik.WinControls.UI.RadTextBox radTextBox_PB;
        private Telerik.WinControls.UI.RadLabel radLabel_Desc;
        private Telerik.WinControls.UI.RadButton radButton_PXS;
        private Telerik.WinControls.UI.RadButton radButton_PB;
        private System.Drawing.Printing.PrintDocument PrintDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Refrsh;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
    }
}
