﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.Collections;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.FarmHouse
{
    public partial class FarmHouse_ExcelToMNPR : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeVenderCode;//V005450 = FarmHouse, V051670 = Yakult 

        readonly DataTable DtSetGrid = new DataTable();
        string dptCode, staApvAX;
        DataTable DtItem = new DataTable();

        DataTable dtBchAll;
        //Load
        public FarmHouse_ExcelToMNPR(string pTypeVenderCode)
        {
            InitializeComponent();
            _pTypeVenderCode = pTypeVenderCode;
        }
        //Load
        private void FarmHouse_ExcelToMNPR_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;

            DtItem.Columns.Add("ITEMID");
            DtItem.Columns.Add("INVENTDIMID");
            DtItem.Columns.Add("ITEMBARCODE");
            DtItem.Columns.Add("SPC_ITEMNAME");
            DtItem.Columns.Add("UNITID");
            DtItem.Columns.Add("QTY");
            DtItem.Columns.Add("PRICE");
            DtItem.PrimaryKey = new DataColumn[] { DtItem.Columns["ITEMBARCODE"] };

            switch (_pTypeVenderCode)
            {
                case "V005450": dptCode = "D027"; staApvAX = "3"; break;
                case "V014483": dptCode = "D014"; staApvAX = "3"; break;
                case "V051670": dptCode = "D027"; staApvAX = "3"; break; //staApvAX = "1" : เปิดบิลค้างไว้;
                default: break;
            }

            DtItem = ItembarcodeClass.GetBarcode_ByPurchase(dptCode);

            DtSetGrid.Columns.Add("DateRecive");
            DtSetGrid.Columns.Add("INVOICE");
            DtSetGrid.Columns.Add("BRANCH_ID");
            DtSetGrid.Columns.Add("ItemID");
            DtSetGrid.Columns.Add("InventItem");
            DtSetGrid.Columns.Add("BRANCH_NAME");
            DtSetGrid.Columns.Add("ITEMBARCODE");
            DtSetGrid.Columns.Add("SPC_ITEMNAME");
            DtSetGrid.Columns.Add("QTY_EXCEL");
            DtSetGrid.Columns.Add("QTY_CN");
            DtSetGrid.Columns.Add("UNITQTY");
            DtSetGrid.Columns.Add("PURCHUNIT");
            DtSetGrid.Columns.Add("PRICE");
            DtSetGrid.Columns.Add("dis");
            DtSetGrid.Columns.Add("GRAND");
            DtSetGrid.Columns.Add("BA");
            DtSetGrid.Columns.Add("INVOICEAMOUNT");
            DtSetGrid.Columns.Add("CountRecive");
            DtSetGrid.Columns.Add("VENDER");
            DtSetGrid.PrimaryKey = new DataColumn[] { DtSetGrid.Columns["DtSetGrid"],
            DtSetGrid.Columns["BRANCH_ID"], DtSetGrid.Columns["INVOICE"], DtSetGrid.Columns["BA"] };

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;

            radLabel_Detail.Visible = true;
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateRecive", "วันที่รับ", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICE", "เลขที่ INVOICE", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ItemID", "รหัส")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("InventItem", "มิติ")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY_EXCEL", "จำนวนส่ง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY_CN", "จำนวนคืน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("UNITQTY", "อัตราส่วน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHUNIT", "หน่วย", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PRICE", "ราคา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DIS", "ส่วนลด%", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("GRAND", "รวม", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BA", "BA/MA", 130)));

            if (_pTypeVenderCode == "V014483")
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEAMOUNT", "ยอดบิล MA", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("CountRecive", "จน.รับสาขา", 60)));

                GroupDescriptor descriptorMN = new GroupDescriptor();
                descriptorMN.GroupNames.Add("BRANCH_ID", System.ComponentModel.ListSortDirection.Ascending);
                descriptorMN.Aggregates.Add("SUM(GRAND)*1.07");
                descriptorMN.Format = "{1}   ยอดรวมบิล >> {2:N2} ";
                RadGridView_ShowHD.GroupDescriptors.Add(descriptorMN);
            }

            SortDescriptor descriptor = new SortDescriptor
            {
                PropertyName = "BRANCH_ID",
                Direction = System.ComponentModel.ListSortDirection.Ascending
            };
            RadGridView_ShowHD.MasterTemplate.SortDescriptors.Add(descriptor);

            GridViewSummaryItem summaryItemD = new GridViewSummaryItem
            {
                Name = "QTY_EXCEL",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };

            GridViewSummaryItem summaryItemC = new GridViewSummaryItem
            {
                Name = "QTY_CN",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };

            GridViewSummaryItem summaryItemB = new GridViewSummaryItem
            {
                Name = "GRAND",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };

            GridViewSummaryItem summaryItemA = new GridViewSummaryItem
            {
                Name = "Sum",
                Aggregate = GridAggregateFunction.Sum,
                FormatString = "{0:n2}"
            };

            GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC, summaryItemD };
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            DatagridClass.SetCellBackClolorByExpression("BA", "BA = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

            if (_pTypeVenderCode == "V051670")
            {
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDER", "ผู้จัดจำหน่าย", 90)));

                DatagridClass.SetCellBackClolorByExpression("INVOICE", "BA = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("QTY_EXCEL", "QTY_EXCEL = 0.00 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("GRAND", "QTY_EXCEL = 0.00 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                radLabel4.Text = "เลือกไฟล์ Excel";
                radDateTimePicker_D1.Visible = false;
            }

            dtBchAll = BranchClass.GetBranchAll_ByConditions("'1','2'", " '1'", "");
            ClearTxt();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (DtSetGrid.Rows.Count > 0) { DtSetGrid.Rows.Clear(); DtSetGrid.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radBrowseEditor_choose.Enabled = true;
            radDateTimePicker_D1.Enabled = true;
            RadButton_Save.Enabled = false;
            radBrowseEditor_choose.Value = "";
            radLabel_Detail.Text = "";
        }

        //Choose File
        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            string DateInput = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");

            //Check ค่าว่างใน File ที่เลือก
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;

            if (_pTypeVenderCode != "V051670")
            {
                int getPR = FarmHouse_Class.FarmHouse_GetCheckPR(DateInput, _pTypeVenderCode);
                if (getPR > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"วันที่รับสินค้านี้ {DateInput} ได้ Import บิลเข้าเรียบร้อยแล้ว {Environment.NewLine}ไม่สามารถ Import บิลซ้ำได้");
                    ClearTxt();
                    return;
                }
            }
            

            //Clear Datatable
            if (DtSetGrid.Rows.Count > 0)
            {
                DtSetGrid.Rows.Clear();
                RadGridView_ShowHD.DataSource = DtSetGrid;
                DtSetGrid.AcceptChanges();
            }
            //Check File Excel
            if ((radBrowseEditor_choose.Value.Contains(".xls")) == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"File ที่ระบุไม่ใช่ File Excel เช็คใหม่อีกครั้ง {Environment.NewLine} { radBrowseEditor_choose.Value}");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }
            DataTable DtSet = new DataTable();
            radBrowseEditor_choose.Enabled = false; radDateTimePicker_D1.Enabled = false;

            int countBANull = 0;
            int countQtyNall = 0; // Yakult
            switch (_pTypeVenderCode)
            {
                case "V005450":
                    #region "V005450"                  
                    this.Cursor = Cursors.WaitCursor;
                    try
                    {
                        System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
                       ($@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{radBrowseEditor_choose.Value}';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                        System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection);
                        MyCommand.TableMappings.Add("Table", "Net-informations.com");
                        MyCommand.Fill(DtSet);

                        //Check วันที่ที่เลือกและ Excel
                        if (radDateTimePicker_D1.Value.ToString("dd/MM/yyyy") != DtSet.Rows[0][1].ToString())
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("วันที่ระบุในการรับสินค้าและวันที่ใน File Excel ไม่ตรงกัน เช็คใหม่อีกครั้ง." + Environment.NewLine +
                               "เลือก >> " + radDateTimePicker_D1.Value.ToString("dd/MM/yyyy") + " แต่ Excel >> " + DtSet.Rows[0][1].ToString());
                            ClearTxt();
                            this.Cursor = Cursors.Default;
                            return;
                        }

                        DataTable dtBA = PurchClass.FarmHouse_GetBA($@" AND RECEIVEDATE BETWEEN '{DateInput}'  AND '{ DateTime.Parse(DateInput).AddDays(7):yyyy-MM-dd}' ", "BA");

                        foreach (DataRow row_Excel in DtSet.Rows)
                        {
                            string BchID = "RETAILAREA";
                            string BchName = "ซุปเปอร์ชีป";
                            string ItemId = "";
                            string InventId = "";
                            double ItemQty = 0;

                            string xlsDate = row_Excel[1].ToString();
                            string xlsBchID = row_Excel[4].ToString().Trim().Replace(" ", "");
                            string xlsBranchName = row_Excel[5].ToString();
                            string xlsItembarcode = row_Excel[6].ToString();
                            string xlsSpcItemName = row_Excel[7].ToString();
                            string xlsUnit = row_Excel[9].ToString();
                            string xlsInvoice = row_Excel[3].ToString();
                            double xls_QtySale = Convert.ToDouble(row_Excel[8].ToString());
                            double xls_Price = Convert.ToDouble(row_Excel[10].ToString());
                            double xls_QtyCN = 0;
                            string dis = "";
                            ; try
                            {
                                xls_QtyCN = Convert.ToDouble(row_Excel[12].ToString());
                            }
                            catch (Exception) { }

                            //เกี่ยวกับคลัง
                            if (xlsBchID != "")
                            {
                                DataRow[] dtBchCheck = dtBchAll.Select($@" BRANCH_V005450 = '{xlsBchID}' ");
                                if (dtBchCheck.Length == 0)//(dtBch.Rows.Count == 0)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบสาขาที่ระบุใน File Excel เช็คอีกครั้งว่ามีสาขาเปิดใหม่หรือไม่.{Environment.NewLine}[{xlsBranchName}]");
                                    ClearTxt();
                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                                else
                                {
                                    BchID = dtBchCheck[0]["BRANCH_CHANNEL"].ToString(); //dtBch.Rows[0]["BRANCH_CHANNEL"].ToString();
                                    BchName = dtBchCheck[0]["BRANCH_NAME"].ToString(); //dtBch.Rows[0]["BRANCH_NAME"].ToString();
                                }

                                DataRow[] drItemId = DtItem.Select("ITEMBARCODE = '" + xlsItembarcode + @"'");
                                if (drItemId.Length > 0)
                                {
                                    ItemId = drItemId[0]["ITEMID"].ToString();
                                    InventId = drItemId[0]["INVENTDIMID"].ToString();
                                    ItemQty = Convert.ToDouble(drItemId[0]["QTY"].ToString());
                                }

                            }
                            //เลขที่ BA
                            string BA = ""; //FarmHouse_Class.FarmHouse_GetBA(xlsInvoice);
                            if (dtBA.Rows.Count > 0)
                            {
                                DataRow[] bbA = dtBA.Select($@" INVOICEID = '{xlsInvoice}' ");
                                if (bbA.Length > 0)
                                { BA = bbA[0]["NUMRECEIVE"].ToString(); }
                                else
                                {
                                    DataTable dtBAByInvoice = PurchClass.FarmHouse_GetBA($@" AND INVOICEID = '{xlsInvoice}' ", "BA");
                                    if (dtBAByInvoice.Rows.Count > 0) BA = dtBAByInvoice.Rows[0]["NUMRECEIVE"].ToString();
                                }
                            }
                            if (BA == "") countBANull += 1;

                            //Insert Datatable
                            DtSetGrid.Rows.Add(xlsDate,
                                                 xlsInvoice, BchID, ItemId, InventId, BchName, xlsItembarcode,
                                                 xlsSpcItemName, xls_QtySale.ToString("N2"),
                                                 xls_QtyCN.ToString("N2"), ItemQty.ToString("N2"),
                                                 xlsUnit, xls_Price, dis, (xls_Price * xls_QtySale).ToString("N2"), BA);
                        }

                        MyConnection.Close();
                        RadGridView_ShowHD.DataSource = DtSetGrid;
                        DtSetGrid.AcceptChanges();

                        radLabel_Detail.Text = "จำนวนแถว Excel >> " + DtSet.Rows.Count.ToString() + " | จำนวนแถว หน้าจอ >> " + DtSetGrid.Rows.Count.ToString() + " | ช่อง BA/MA สีแดง >> ไม่สามารถนำเข้าได้ เนื่องจากไม่มีการรับสินค้า";

                        if (DtSetGrid.Rows.Count == DtSet.Rows.Count)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("Import ข้อมูลจาก Excel เข้าเรียบร้อย.");
                            if (countBANull > 0) RadButton_Save.Enabled = false; else RadButton_Save.Enabled = true;
                            RadButton_Save.Focus();
                        }
                        else
                        {
                            RadButton_Save.Enabled = false;
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูล หน้าจอ กับ excel มีจำนวนไม่เท่ากัน ลองใหม่อีกครั้ง");
                        }

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถดำเนินการ Import Excel ได้ ลองใหม่อีกครั้ง.{ Environment.NewLine}{ex.Message}");
                        ClearTxt();
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    #endregion
                    break;
                case "V014483":
                    #region  "V014483"   
                    this.Cursor = Cursors.WaitCursor;
                    try
                    {
                        System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
                       ($@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{radBrowseEditor_choose.Value}';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                        System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection);
                        MyCommand.TableMappings.Add("Table", "Net-informations.com");
                        MyCommand.Fill(DtSet);

                        //Check วันที่ที่เลือกและ Excel
                        DateTime DD = DateTime.Parse(DtSet.Rows[0][8].ToString());
                        if (radDateTimePicker_D1.Value.ToString("dd/MM/yyyy") != DD.ToString("dd/MM/yyyy"))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"วันที่ระบุในการรับสินค้าและวันที่ใน File Excel ไม่ตรงกัน เช็คใหม่อีกครั้ง{Environment.NewLine}เลือก >> {radDateTimePicker_D1.Value:dd/MM/yyyy} แต่ Excel >> {DD:dd/MM/yyyy}");
                            ClearTxt();
                            this.Cursor = Cursors.Default;
                            return;
                        }

                        DataTable dtBA = PurchClass.FarmHouse_GetBA($@" AND CONVERT(VARCHAR,CREATEDDATETIME,23) BETWEEN '{DateInput}'  AND '{ DateTime.Parse(DateInput).AddDays(7):yyyy-MM-dd}' ", "MA");
                        DataTable dtCountRecive = FarmHouse_Class.FarmHouse_BranchReciveGroupCount(DateInput, _pTypeVenderCode);

                        foreach (DataRow row_Excel in DtSet.Rows)
                        {
                            string BchID = "";
                            string BchName = "";
                            string ItemId = "";
                            string InventId = "";
                            string countRecive = "0";
                            double ItemQty = 0;


                            string xlsDate = DateTime.Parse(DtSet.Rows[0][8].ToString()).ToString("dd/MM/yyyy");
                            string xlsBchID = row_Excel[6].ToString().Trim().Replace(" ", "");
                            string xlsBranchName = row_Excel[7].ToString();
                            string xlsItembarcode = row_Excel[10].ToString();
                            string xlsSpcItemName = row_Excel[11].ToString();
                            string xlsUnit = "";// row_Excel[9].ToString();
                            string xlsInvoice = row_Excel[9].ToString();
                            double xls_QtySale = Convert.ToDouble(row_Excel[12].ToString());
                            double xls_PriceSum = Convert.ToDouble(row_Excel[13].ToString());
                            double xls_QtyCN = 0;
                            string dis = "10%";
                            //เกี่ยวกับคลัง

                            DataRow[] dtBchCheck = dtBchAll.Select($@" BRANCH_V014483 = '{xlsBchID}' ");
                            if (dtBchCheck.Length == 0)//(dtBch.Rows.Count == 0)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบสาขาที่ระบุใน File Excel เช็คอีกครั้งว่ามีสาขาเปิดใหม่หรือไม่.{Environment.NewLine}[{xlsBranchName}]");
                                ClearTxt();
                                this.Cursor = Cursors.Default;
                                return;
                            }
                            else
                            {
                                BchID = dtBchCheck[0]["BRANCH_CHANNEL"].ToString();
                                BchName = dtBchCheck[0]["BRANCH_NAME"].ToString();
                            }

                            DataRow[] drItemId = DtItem.Select("ITEMBARCODE = '" + xlsItembarcode + @"'");
                            if (drItemId.Length == 0)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลบาร์โค้ด {xlsItembarcode}{Environment.NewLine}ให้เช็ค File Excel ใหม่อีกครั้ง");
                                ClearTxt();
                                this.Cursor = Cursors.Default;
                                return;
                            }


                            DataRow[] dtCheckCountRecive = dtCountRecive.Select($@" ReciveBranchID = '{BchID}' ");
                            if (dtCheckCountRecive.Length > 0) countRecive = dtCheckCountRecive[0]["ReciveCount"].ToString();


                            ItemId = drItemId[0]["ITEMID"].ToString();
                            InventId = drItemId[0]["INVENTDIMID"].ToString();
                            ItemQty = Convert.ToDouble(drItemId[0]["QTY"].ToString());
                            xlsUnit = drItemId[0]["UNITID"].ToString();


                            //เลขที่ BA
                            string BA = ""; double sumMA = 0;
                            if (dtBA.Rows.Count > 0)
                            {
                                DataRow[] bbA = dtBA.Select($@" INVOICEID = '{xlsInvoice}' ");
                                if (bbA.Length > 0)
                                {
                                    BA = bbA[0]["NUMRECEIVE"].ToString();
                                    sumMA = double.Parse(bbA[0]["INVOICEAMOUNT"].ToString());
                                }
                                else
                                {
                                    DataTable dtBAByInvoice = PurchClass.FarmHouse_GetBA($@" AND INVOICEID = '{xlsInvoice}' ", "MA");
                                    if (dtBAByInvoice.Rows.Count > 0)
                                    {
                                        BA = dtBAByInvoice.Rows[0]["NUMRECEIVE"].ToString();
                                        sumMA = double.Parse(dtBAByInvoice.Rows[0]["INVOICEAMOUNT"].ToString());
                                    }
                                }
                            }
                            if (BA == "") countBANull += 1;

                            double priceUnit = xls_PriceSum / xls_QtySale;
                            //Insert Datatable
                            DtSetGrid.Rows.Add(xlsDate,
                                                 xlsInvoice, BchID, ItemId, InventId, BchName, xlsItembarcode,
                                                 xlsSpcItemName,
                                                 xls_QtySale.ToString("N2"),
                                                 xls_QtyCN.ToString("N2"),
                                                 ItemQty.ToString("N2"),
                                                 xlsUnit,
                                                 priceUnit.ToString("N2"),
                                                 dis,
                                                 (((xls_PriceSum * 0.9))).ToString("N2"),
                                                 BA, sumMA.ToString("N2"), countRecive);


                        }

                        MyConnection.Close();

                        DtSetGrid.DefaultView.Sort = "BRANCH_ID DESC";
                        RadGridView_ShowHD.DataSource = DtSetGrid;
                        DtSetGrid.AcceptChanges();

                        radLabel_Detail.Text = "จำนวนแถว Excel >> " + DtSet.Rows.Count.ToString() + " | จำนวนแถว หน้าจอ >> " + DtSetGrid.Rows.Count.ToString() + " | ช่อง BA/MA สีแดง >> ไม่สามารถนำเข้าได้ เนื่องจากไม่มีการรับสินค้า";

                        if (DtSetGrid.Rows.Count == DtSet.Rows.Count)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation("Import ข้อมูลจาก Excel เข้าเรียบร้อย.");
                            if (countBANull > 0) RadButton_Save.Enabled = false; else RadButton_Save.Enabled = true;

                            RadButton_Save.Focus();
                        }
                        else
                        {
                            RadButton_Save.Enabled = false;
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูล หน้าจอ กับ excel มีจำนวนไม่เท่ากัน ลองใหม่อีกครั้ง");
                        }

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถดำเนินการ Import Excel ได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ ex.Message }");
                        ClearTxt();
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    #endregion
                    break;
                case "V051670":
                    #region  "V014483" 
                    this.Cursor = Cursors.WaitCursor;
                    try
                    {
                        string sheetName;
                        System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
                           ($@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{radBrowseEditor_choose.Value}';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                        MyConnection.Open();
                        DataTable dtExcelSheet = MyConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);

                        if (dtExcelSheet == null)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลใน File Excel ที่ระบุ{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง");
                            ClearTxt();
                            this.Cursor = Cursors.Default;
                            return;
                        }

                        if (dtExcelSheet.Rows.Count == 1) //ดึงข้อมูลใน Excel ในกรณีมีแค่ 1 เอกสารในไฟล์
                        {
                            System.Data.OleDb.OleDbDataAdapter MyCommand1 = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{dtExcelSheet.Rows[0]["TABLE_NAME"]}]  ", MyConnection);
                            MyCommand1.TableMappings.Add("Table", "Net-informations.com");
                            MyCommand1.Fill(DtSet);
                        }
                        else //ดึงข้อมูลใน Excel ในกรณีมีเอกสารมากกว่า 1 
                        {
                            ShowDataDGV frm = new ShowDataDGV("2")
                            { dtData = dtExcelSheet };

                            if (frm.ShowDialog(this) == DialogResult.Yes) sheetName = frm.pSheet;
                            else
                            {
                                ClearTxt();
                                this.Cursor = Cursors.Default;
                                return;
                            }

                            System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{sheetName}]  ", MyConnection);
                            MyCommand.TableMappings.Add("Table", "Net-informations.com");
                            MyCommand.Fill(DtSet);
                        }

                        if (DtSet.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลสำหรับ File Excel ที่เลือก{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง");
                            ClearTxt();
                            this.Cursor = Cursors.Default;
                            return;
                        }

                        foreach (DataRow row_Excel in DtSet.Rows)
                        {
                            string BchID = "";
                            string BchName = "";
                            string ItemId = "";
                            string InventId = "";
                            double ItemQty = 0;
                            string xlsDate = row_Excel[0].ToString();
                            string xlsBchID = row_Excel[4].ToString().Trim().Replace(" ", "");
                            string xlsBranchName = row_Excel[5].ToString();
                            string xlsItembarcode = "0000009867321";
                            string xlsSpcItemName = "ยาคูลท์";
                            string xlsUnit = "แพ็ค:50";
                            string xlsInvoice = row_Excel[1].ToString();
                            double xls_QtySale = 0;
                            if (row_Excel[6].ToString() != "") xls_QtySale = Convert.ToDouble(row_Excel[6].ToString());
                            if (xls_QtySale == 0) countQtyNall += 1;
                            double xls_Price = Convert.ToDouble(row_Excel[7].ToString()); //ราคาต่อหน่วย 400
                            double xls_QtyCN = 0;
                            string dis = "";
                            string vender = row_Excel[2].ToString(); ;
                            DataTable dtBA = new DataTable();
                            if (xlsInvoice != "") dtBA = PurchClass.FarmHouse_GetBA($@" AND INVOICEID = '{xlsInvoice}' ", "MA");

                            //เช็คเพิ่มข้อมูลซ้ำ
                            int getPR = FarmHouse_Class.FarmHouse_GetCheckPR(xlsDate, vender,$@"AND INVENTLOCATIONID = '{xlsBchID}' ");
                            if (getPR > 0)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"วันที่รับสินค้านี้ {xlsDate} สาขา {xlsBchID} - {xlsBranchName} {Environment.NewLine}ได้ Import บิลเข้าเรียบร้อยแล้ว {Environment.NewLine}ไม่สามารถ Import บิลซ้ำได้");
                                ClearTxt();
                                this.Cursor = Cursors.Default;
                                return;
                            }

                            //เกี่ยวกับคลัง
                            if (xlsBchID != "")
                            {
                                DataRow[] dtBchCheck = dtBchAll.Select($@" BRANCH_ID = '{xlsBchID}' ");
                                if (dtBchCheck.Length == 0)//(dtBch.Rows.Count == 0)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบสาขาที่ระบุใน File Excel เช็คอีกครั้งว่ามีสาขาเปิดใหม่หรือไม่.{Environment.NewLine}[{xlsBranchName}]");
                                    ClearTxt();
                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                                else
                                {
                                    BchID = dtBchCheck[0]["BRANCH_ID"].ToString();
                                    BchName = dtBchCheck[0]["BRANCH_NAME"].ToString();
                                }

                                DataRow[] drItemId = DtItem.Select("ITEMBARCODE = '" + xlsItembarcode + @"'");
                                if (drItemId.Length > 0)
                                {
                                    ItemId = drItemId[0]["ITEMID"].ToString();
                                    InventId = drItemId[0]["INVENTDIMID"].ToString();
                                    ItemQty = Convert.ToDouble(drItemId[0]["QTY"].ToString());
                                }

                            }
                            //เลขที่ BA
                            string BA = ""; //FarmHouse_Class.FarmHouse_GetBA(xlsInvoice);
                            if (dtBA.Rows.Count > 0)
                            {
                                DataRow[] bbA = dtBA.Select($@" INVOICEID = '{xlsInvoice}' ");
                                if (bbA.Length > 0) BA = bbA[0]["NUMRECEIVE"].ToString();
                                else
                                {
                                    DataTable dtBAByInvoice = PurchClass.FarmHouse_GetBA($@" AND INVOICEID = '{xlsInvoice}' ", "BA");
                                    if (dtBAByInvoice.Rows.Count > 0) BA = dtBAByInvoice.Rows[0]["NUMRECEIVE"].ToString();
                                }
                            }
                            if (BA == "") countBANull += 1;

                            //Insert Datatable
                            DtSetGrid.Rows.Add(xlsDate,
                                                 xlsInvoice, BchID, ItemId, InventId, BchName, xlsItembarcode,
                                                 xlsSpcItemName, xls_QtySale.ToString("N2"),
                                                 xls_QtyCN.ToString("N2"), ItemQty.ToString("N2"),
                                                 xlsUnit, xls_Price, dis, (xls_Price * xls_QtySale).ToString("N2"), BA, "", "", vender);
                        }

                        MyConnection.Close();
                        RadGridView_ShowHD.DataSource = DtSetGrid;
                        DtSetGrid.AcceptChanges();

                        radLabel_Detail.Text = "จำนวนแถว Excel >> " + DtSet.Rows.Count.ToString() + " | จำนวนแถว หน้าจอ >> " + DtSetGrid.Rows.Count.ToString() + " | ช่อง BA/MA สีแดง >> ไม่สามารถนำเข้าได้ เนื่องจากไม่มีการรับสินค้า";

                        if (DtSetGrid.Rows.Count == DtSet.Rows.Count)
                        {
                            if (countQtyNall > 0)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("จำนวนสินค้าเป็น 0 ตรวจสอบไฟล์ Excel อีกครั้ง.");
                                RadButton_Save.Enabled = false;
                            }
                            else if (countBANull > 0)
                            {
                                RadButton_Save.Enabled = false;
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ข้อมูลไม่ครบถ้วน ตรวจสอบไฟล์ Excel อีกครั้ง.");
                            }
                            else
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("Import ข้อมูลจาก Excel เข้าเรียบร้อย.");
                                RadButton_Save.Enabled = true;
                                RadButton_Save.Focus();
                            }
                        }
                        else
                        {
                            RadButton_Save.Enabled = false;
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูล หน้าจอ กับ excel มีจำนวนไม่เท่ากัน ลองใหม่อีกครั้ง");
                        }

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถดำเนินการ Import Excel ได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ ex.Message }");
                        ClearTxt();
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    #endregion
                    break;
                default:
                    break;
            }
        }

        //ยกเลิก
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeVenderCode);
        }

        //Save To DB
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count < 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกรายการได้ เนื่องจากไม่มีรายการสินค้า");
                return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert(" Import ข้อมูลตามหน้าจอ ") == DialogResult.No) return;

            this.RadGridView_ShowHD.SortDescriptors.Expression = "BRANCH_ID ASC";
            this.RadGridView_ShowHD.GroupDescriptors.Clear();


            ArrayList sql24 = new ArrayList();
            ArrayList sqlAX = new ArrayList();

            this.Cursor = Cursors.WaitCursor;
            //string DateInput = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            //string BranchID = "";
            //string BranchName;
            //int Line = 0;
            //int LineAX = 0;
            //string Docno = "";

            string DateInput = "";
            string VenderCode = "";
            if (_pTypeVenderCode != "V051670")
            {
                DateInput = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
                VenderCode = _pTypeVenderCode;
            }
            string BranchID = "";
            string BranchName;
            int Line = 0;
            int LineAX = 0;
            string Docno = "";

            foreach (GridViewRowInfo row in RadGridView_ShowHD.MasterView.Rows)
            {
                if ((BranchID == "") || (BranchID != row.Cells["BRANCH_ID"].Value.ToString()) || (_pTypeVenderCode == "V051670" && DateInput != row.Cells["DateRecive"].Value.ToString()))
                {
                    //BranchID = row.Cells["BRANCH_ID"].Value.ToString();
                    //BranchName = row.Cells["BRANCH_NAME"].Value.ToString();
                    //Line = 1; LineAX = 1;

                    //Docno = Class.ConfigClass.GetMaxINVOICEID("MNPR", "-", "MNPR", "1");
                    ////PURCHSTATUS-- 3 อัพเดทใบแจ้งหนี้   1 เปิดค้างไว้
                    //Var_PURCHTABLE var_PURCHTABLE = new Var_PURCHTABLE()
                    //{
                    //    PURCHASETYPE = "3",
                    //    PURCHSTATUS = staApvAX,
                    //    TAXINVOICE = "1",
                    //    INCLTAX = "0",
                    //    PURCHID = Docno,
                    //    PURCHDATE = DateInput,
                    //    ORDERACCOUNT = _pTypeVenderCode,
                    //    INVENTLOCATIONID = BranchID,
                    //    DIMENSION = dptCode,
                    //    REMARK = BranchID + " " + BranchName,
                    //    INVOICEID = row.Cells["INVOICE"].Value.ToString(),
                    //    INVOICEDATE = DateInput,
                    //    VENDORREF = row.Cells["INVOICE"].Value.ToString(),
                    //    NUMRECEIVE = row.Cells["BA"].Value.ToString()
                    //};
                    //sqlAX.Add(AX_SendData.Update_ShipCarrierInvoice(var_PURCHTABLE.INVOICEID, var_PURCHTABLE.NUMRECEIVE, var_PURCHTABLE.PURCHID));
                    //sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE(var_PURCHTABLE));


                    if (_pTypeVenderCode == "V051670")
                    {
                        DateInput = row.Cells["DateRecive"].Value.ToString();
                        VenderCode = row.Cells["VENDER"].Value.ToString();
                    }
                    BranchID = row.Cells["BRANCH_ID"].Value.ToString();
                    BranchName = row.Cells["BRANCH_NAME"].Value.ToString();
                    Line = 1; LineAX = 1;

                    Docno = Class.ConfigClass.GetMaxINVOICEID("MNPR", "-", "MNPR", "1");
                    //PURCHSTATUS-- 3 อัพเดทใบแจ้งหนี้   1 เปิดค้างไว้
                    Var_PURCHTABLE var_PURCHTABLE = new Var_PURCHTABLE()
                    {
                        PURCHASETYPE = "3",
                        PURCHSTATUS = staApvAX,
                        TAXINVOICE = "1",
                        INCLTAX = "0",
                        PURCHID = Docno,
                        PURCHDATE = DateInput,
                        ORDERACCOUNT = VenderCode,
                        INVENTLOCATIONID = BranchID,
                        DIMENSION = dptCode,
                        REMARK = BranchID + " " + BranchName,
                        INVOICEID = row.Cells["INVOICE"].Value.ToString(),
                        INVOICEDATE = DateInput,
                        VENDORREF = row.Cells["INVOICE"].Value.ToString(),
                        NUMRECEIVE = row.Cells["BA"].Value.ToString()
                    };
                    if (_pTypeVenderCode == "V051670")
                    {
                        var_PURCHTABLE.INCLTAX = "1";
                    }
                        sqlAX.Add(AX_SendData.Update_ShipCarrierInvoice(var_PURCHTABLE.INVOICEID, var_PURCHTABLE.NUMRECEIVE, var_PURCHTABLE.PURCHID));
                    sqlAX.Add(AX_SendData.SaveHD_PURCHTABLE(var_PURCHTABLE));
                }
                else
                {
                    if (_pTypeVenderCode == "V051670")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกรายการได้ ตรวจเช็ควันที่ใน Excel อีกครั้ง");
                        RadButton_Save.Enabled = false;
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }

                Var_VENDERPR var_VENDERPR = new Var_VENDERPR
                {
                    ItemID = row.Cells["ItemID"].Value.ToString(),
                    InventItem = row.Cells["InventItem"].Value.ToString(),
                    ITEMBARCODE = row.Cells["ITEMBARCODE"].Value.ToString(),
                    SPC_ITEMNAME = row.Cells["SPC_ITEMNAME"].Value.ToString(),
                    QTY_EXCEL = Double.Parse(row.Cells["QTY_EXCEL"].Value.ToString()),
                    QTY_CN = Double.Parse(row.Cells["QTY_CN"].Value.ToString()),
                    UNITQTY = row.Cells["UNITQTY"].Value.ToString(),
                    PURCHUNIT = row.Cells["PURCHUNIT"].Value.ToString(),
                    INVOICE = row.Cells["INVOICE"].Value.ToString(),
                    PRICECOST = double.Parse(row.Cells["PRICE"].Value.ToString())
                };
                //sql24.Add(FarmHouse_Class.FarmHouse_SaveVenderPR(_pTypeVenderCode, BranchID, Line, Docno, DateInput, var_VENDERPR));
                sql24.Add(FarmHouse_Class.FarmHouse_SaveVenderPR(VenderCode, BranchID, Line, Docno, DateInput, var_VENDERPR));
                if (Convert.ToDouble(row.Cells["QTY_EXCEL"].Value.ToString()) > 0)
                {
                    Var_PURCHLINE var_PURCHLINE = new Var_PURCHLINE
                    {
                        PURCHID = Docno,
                        PURCHDATE = DateInput,
                        LINENUM = LineAX,
                        BARCODE = row.Cells["ITEMBARCODE"].Value.ToString(),
                        NAME = row.Cells["SPC_ITEMNAME"].Value.ToString(),
                        REMARK = "",
                        PURCHQTY = double.Parse(row.Cells["QTY_EXCEL"].Value.ToString()),
                        PURCHPRICE = double.Parse(row.Cells["PRICE"].Value.ToString()),
                        MULTIDISCTXT = row.Cells["DIS"].Value.ToString()
                    };
                    sqlAX.Add(AX_SendData.SaveDT_PURCHLINE(var_PURCHLINE));
                    LineAX += 1;
                }
                Line += 1;
            }

            if (sql24.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ต้องบันทึก เช็คใหม่อีกครั้ง.");
                return;
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX));
            //MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_ArrayMain(sql24)); //ทดสอบบันทึกข้อมูล
            RadButton_Save.Enabled = false;

            this.Cursor = Cursors.Default;
        }
    }
}
