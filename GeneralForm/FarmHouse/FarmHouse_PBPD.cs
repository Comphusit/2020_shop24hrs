﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.FarmHouse
{
    public partial class FarmHouse_PBPD : Telerik.WinControls.UI.RadForm
    {
        string DocnoRef;

        readonly DataTable DtSetGrid = new DataTable();

        readonly string _pTypeReport; // PB = ใบวางบิล PD = ใบลดหนี้
        readonly string _pPermission; // สิทธิ์การบันทึก
        readonly string _pTypeVenderCode;//V005450 = FarmHouse

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        string bchID_PD;
        double GrandCN, GrandPD;
        string pSave;

        DataTable dtPD = new DataTable();

        //Load
        public FarmHouse_PBPD(string pTypeReport, string pPermission, string pTypeVenderCode)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
            _pTypeVenderCode = pTypeVenderCode;
        }
        //Load
        private void FarmHouse_PBPD_Load(object sender, EventArgs e)
        {
            radButtonElement_Refrsh.ShowBorder = true;
            radButtonElement_Excel.ShowBorder = true;
            RadButtonElement_pdt.ShowBorder = true;

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radLabel_PB.Visible = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_PB1, DateTime.Now, DateTime.Now); radDateTimePicker_PB1.Visible = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_PB2, DateTime.Now, DateTime.Now); radDateTimePicker_PB2.Visible = false;
            radLabel_PXS.Visible = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_PXS1, DateTime.Now, DateTime.Now); radDateTimePicker_PXS1.Visible = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_PXS2, DateTime.Now, DateTime.Now); radDateTimePicker_PXS2.Visible = false;

            radButton_PB.ButtonElement.ShowBorder = true; radButton_PB.Visible = false;
            radButton_PXS.ButtonElement.ShowBorder = true; radButton_PXS.Visible = false;

            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);


            switch (_pTypeReport)
            {
                case "PB":
                    DtSetGrid.Columns.Add("C");
                    DtSetGrid.Columns.Add("LedgerVoucher");
                    DtSetGrid.Columns.Add("PurchId");
                    DtSetGrid.Columns.Add("InventLocationId");
                    DtSetGrid.Columns.Add("IVZ_Remark");
                    DtSetGrid.Columns.Add("INVOICEID");
                    DtSetGrid.Columns.Add("INVOICEDATE");
                    DtSetGrid.Columns.Add("SALESBALANCE");
                    DtSetGrid.Columns.Add("SumTax");
                    DtSetGrid.Columns.Add("INVOICEAMOUNT");
                    DtSetGrid.Columns.Add("Name");

                    DtSetGrid.PrimaryKey = new DataColumn[] { DtSetGrid.Columns["PurchId"] };

                    radLabel_Desc.Text = "ระบุ PB [Enter]";
                    radLabel_PB.Text = "รอบการวางบิล PB"; radLabel_PB.Visible = true;
                    radDateTimePicker_PB1.Visible = true; radDateTimePicker_PB2.Visible = true;
                    radLabel_PXS.Text = "รอบวางบิล PXS"; radLabel_PXS.Visible = true;
                    radDateTimePicker_PXS1.Visible = true; radDateTimePicker_PXS2.Visible = true;
                    radButton_PB.Visible = true; radButton_PXS.Visible = true;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LedgerVoucher", "เลขที่ PR", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PurchId", "เลขที่ PO", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("InventLocationId", "สาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("IVZ_Remark", "เลขที่ BA", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "INVOICE", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่รับสินค้า", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SALESBALANCE", "ยอดรวม", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SumTax", "ภาษีขาย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("INVOICEAMOUNT", "ยอดทั้งหมด", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Name", "FileScan", 160)));

                    RadGridView_ShowHD.MasterTemplate.EnableSorting = false;

                    GridViewSummaryItem summaryItemC = new GridViewSummaryItem
                    {
                        Name = "SALESBALANCE",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItemB = new GridViewSummaryItem
                    {
                        Name = "SumTax",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItemA = new GridViewSummaryItem
                    {
                        Name = "INVOICEAMOUNT",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    radLabel_Detail.Text = "ระบุเลขที่ PB >> Enter | เลือกวันที่ของรอบการวางบิล [PB] กด เช็ค | เลือกวันที่ของรอบการวางใบลดหนี้ [PXS] กด เช็ค";
                    break;
                case "PD":
                    #region "PD"


                    DtSetGrid.Columns.Add("PURCHID");
                    DtSetGrid.Columns.Add("BarCode");
                    DtSetGrid.Columns.Add("NAME");
                    DtSetGrid.Columns.Add("SPC_PriceGroup3");
                    DtSetGrid.Columns.Add("PurchUnit");
                    DtSetGrid.Columns.Add("QTY");
                    DtSetGrid.Columns.Add("QTY_RECIVE");
                    DtSetGrid.Columns.Add("Diff");
                    DtSetGrid.Columns.Add("Sum");
                    DtSetGrid.Columns.Add("Grand");

                    radLabel_Desc.Text = "ระบุ PD [Enter]";
                    radLabel_PB.Text = "ระบุวันที่การคืน MN"; radLabel_PB.Visible = true;
                    radLabel_PXS.Text = "ระบุวันที่การคืน Retail"; radLabel_PXS.Visible = true;
                    radDateTimePicker_PXS1.Visible = true;
                    radDateTimePicker_PB1.Visible = true;
                    radButton_PB.Visible = true; radButton_PXS.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PURCHID", "เลขที่ PD", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BarCode", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SPC_PriceGroup3", "ราคาต้นทุน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PurchUnit", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนคืน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_RECIVE", "จำนวนได้คืน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("Diff", "ขาด-เกิน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Sum", "รวมเงินคืน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Grand", "รวมเงินได้คืน", 100)));

                    RadGridView_ShowHD.MasterTemplate.EnableSorting = false;

                    GridViewSummaryItem summaryItemD1 = new GridViewSummaryItem
                    {
                        Name = "Grand",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItemC1 = new GridViewSummaryItem
                    {
                        Name = "QTY",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItemB1 = new GridViewSummaryItem
                    {
                        Name = "QTY_RECIVE",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryItem summaryItemA1 = new GridViewSummaryItem
                    {
                        Name = "Sum",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };

                    GridViewSummaryRowItem summaryRowItemA1 = new GridViewSummaryRowItem { summaryItemA1, summaryItemB1, summaryItemC1, summaryItemD1 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA1);

                    DatagridClass.SetCellBackClolorByExpression("Diff", "Diff <> '0.00' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition2", "PURCHID = '' ", false)
                    { CellBackColor = ConfigClass.SetColor_SkyPastel() };
                    this.RadGridView_ShowHD.Columns["PURCHID"].ConditionalFormattingObjectList.Add(obj4);
                    this.RadGridView_ShowHD.Columns["Diff"].ConditionalFormattingObjectList.Add(obj4);
                    this.RadGridView_ShowHD.Columns["Grand"].ConditionalFormattingObjectList.Add(obj4);
                    this.RadGridView_ShowHD.Columns["QTY_RECIVE"].ConditionalFormattingObjectList.Add(obj4);
                    this.RadGridView_ShowHD.Columns["BarCode"].ConditionalFormattingObjectList.Add(obj4);

                    radLabel_Detail.Text = "ระบุเลขที่ PD >> Enter | เลือกวันที่ของใบลดหนี้ [PD] กด เช็ค | สีแดง >> สินค้าขาด | สีฟ้า >> สินค้าเกิน";
                    #endregion
                    break;
                default:
                    break;
            }

            ClearTxt();
        }

        #region "ROWS DGV"

        private void RadDateTimePicker_PB1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_PB1, radDateTimePicker_PB2);
        }

        private void RadDateTimePicker_PB2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_PB1, radDateTimePicker_PB2);
        }

        private void RadDateTimePicker_PXS1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_PXS1, radDateTimePicker_PXS2);
        }

        private void RadDateTimePicker_PXS2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_PXS1, radDateTimePicker_PXS2);
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (DtSetGrid.Rows.Count > 0) { DtSetGrid.Rows.Clear(); DtSetGrid.AcceptChanges(); }

            radDateTimePicker_PB1.Value = DateTime.Now; radDateTimePicker_PB2.Value = DateTime.Now;
            radDateTimePicker_PXS1.Value = DateTime.Now; radDateTimePicker_PXS2.Value = DateTime.Now;

            radDateTimePicker_PB1.Enabled = false; radDateTimePicker_PB2.Enabled = false;
            radDateTimePicker_PXS1.Enabled = false; radDateTimePicker_PXS2.Enabled = false;
            radButton_PB.Enabled = false; radButton_PXS.Enabled = false;

            RadButton_Save.Enabled = false; RadButton_Cancel.Enabled = true;
            GrandCN = 0; GrandPD = 0;
            DocnoRef = "";
            bchID_PD = "";
            radTextBox_PB.Enabled = true; radTextBox_PB.Text = ""; radTextBox_PB.Focus();
        }

        //Clear
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //ยกเลิก
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Enter PB Docno
        private void RadTextBox_PB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                switch (_pTypeReport)
                {
                    case "PB":
                        if (radTextBox_PB.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("เลขที่ PB");
                            radTextBox_PB.Text = ""; radTextBox_PB.Focus();
                            return;
                        }

                        DataTable dtPB = PurchClass.FarmHouse_GetPB("PB" + radTextBox_PB.Text.Trim().Replace("PB", ""), _pTypeVenderCode);
                        if (dtPB.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ PB ที่ระบุ");
                            radTextBox_PB.Text = ""; radTextBox_PB.Focus();
                            return;
                        }

                        radTextBox_PB.Enabled = false;
                        DocnoRef = dtPB.Rows[0]["PURCHBILLID"].ToString();
                        radDateTimePicker_PB1.Enabled = true; radDateTimePicker_PB2.Enabled = true;
                        radButton_PB.Enabled = true; radButton_PB.Focus();
                        break;
                    case "PD":
                        if (radTextBox_PB.Text == "")
                        {
                            MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("เลขที่ PD");
                            radTextBox_PB.Text = ""; radTextBox_PB.Focus(); return;
                        }
                        dtPD = PurchClass.FarmHouse_GetPDDetail("PD" + radTextBox_PB.Text, _pTypeVenderCode);
                        if (dtPD.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ PD ที่ระบุ");
                            radTextBox_PB.Text = ""; radTextBox_PB.Focus(); return;
                        }

                        radTextBox_PB.Enabled = false;
                        DocnoRef = dtPD.Rows[0]["PURCHID"].ToString();

                        string bchID = FindPD24("PD" + radTextBox_PB.Text);

                        if (pSave == "0")
                        {
                            radButton_PB.Enabled = true;
                            radButton_PXS.Enabled = true;
                        }
                        else
                        {
                            radButton_PB.Enabled = false;
                            FindDataAll(bchID);
                        }
                        break;
                    default:
                        break;
                }

            }
        }
        //ค้นหารายการทั้งหมด
        void FindDataAll(string bchID)
        {
            this.Cursor = Cursors.WaitCursor;

            string pDate;
            if (bchID == "") pDate = radDateTimePicker_PB1.Value.ToString("yyyy-MM-dd"); else pDate = radDateTimePicker_PXS1.Value.ToString("yyyy-MM-dd");

            DataTable dtCN = ItembarcodeClass.FarmHouse_FindCNExcel(pDate, bchID);

            foreach (DataRow item in dtPD.Rows)
            {
                Double QtyCN = 0, price = 0;
                DataRow[] dr = dtCN.Select("SPC_ITEMBARCODE = '" + item["BarCode"].ToString() + @"'");
                if (dr.Length > 0)
                {
                    QtyCN = Convert.ToDouble(dr[0]["CNQTY"].ToString());
                    price = Convert.ToDouble(dr[0]["COST"].ToString());
                }
                double QtyDiff = Convert.ToDouble(item["QTY"].ToString()) - QtyCN;

                double sum1 = Convert.ToDouble(item["QTY"].ToString()) * Convert.ToDouble(item["SPC_PriceGroup3"].ToString());
                double grand1 = QtyCN * Convert.ToDouble(item["SPC_PriceGroup3"].ToString());

                if (price == 0) price = double.Parse(item["SPC_PriceGroup3"].ToString());

                DtSetGrid.Rows.Add(
                    item["PURCHID"].ToString(), item["BarCode"].ToString(),
                    item["NAME"].ToString(),
                    price.ToString("N2"),
                    item["PurchUnit"].ToString(),
                    string.Format("{0:0.00}", item["QTY"].ToString()),
                    QtyCN.ToString("N2"), QtyDiff.ToString("N2"),
                    sum1.ToString("N2"), grand1.ToString("N2")
                    );
            }


            foreach (DataRow itemCN in dtCN.Rows)
            {
                DataRow[] dk = dtPD.Select("BarCode = '" + itemCN["SPC_ITEMBARCODE"].ToString() + @"'");
                if (dk.Length == 0)
                {
                    double qty = Convert.ToDouble(itemCN["CNQTY"].ToString());
                    double grand = qty * Convert.ToDouble(itemCN["COST"].ToString());
                    DtSetGrid.Rows.Add("",
                        itemCN["SPC_ITEMBARCODE"].ToString(),
                        itemCN["SPC_ITEMNAME"].ToString(),
                        string.Format("{0:0.00}", itemCN["COST"].ToString()),
                        itemCN["UnitId"].ToString(),
                        "0.00",
                        qty.ToString("N2"),
                        qty.ToString("N2"),
                        "0.00", grand.ToString("N2"));
                }
            }

            RadGridView_ShowHD.DataSource = DtSetGrid;
            DtSetGrid.AcceptChanges();


            this.Cursor = Cursors.Default;
            if (RadGridView_ShowHD.Rows.Count > 0)
            {
                RadButton_Save.Enabled = true;
                radButton_PB.Enabled = false;
                radDateTimePicker_PB1.Enabled = false;

                radButton_PXS.Enabled = false;
                radDateTimePicker_PXS1.Enabled = false;
            }

        }

        //FindPR
        private void RadButton_PB_Click(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "PB":
                    if (DocnoRef == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบเลขที่ PB ให้เรียบร้อยก่อนการดึงข้อมูล PR");
                        radTextBox_PB.Text = ""; radTextBox_PB.Focus();
                        return;
                    }
                    string pDate1 = radDateTimePicker_PB1.Value.ToString("yyyy-MM-dd");
                    string pDate2 = radDateTimePicker_PB2.Value.ToString("yyyy-MM-dd");

                    if (DtSetGrid.Rows.Count > 0)
                    {
                        DtSetGrid.Rows.Clear();
                        RadGridView_ShowHD.DataSource = DtSetGrid;
                        DtSetGrid.AcceptChanges();
                    }
                    this.Cursor = Cursors.WaitCursor;

                    DataTable dtMN = PurchClass.FarmHouse_GetPR_MN(pDate1, pDate2, _pTypeVenderCode);
                    for (int iR = 0; iR < dtMN.Rows.Count; iR++)
                    {
                        DtSetGrid.Rows.Add("1",
                                dtMN.Rows[iR]["LedgerVoucher"].ToString(),
                                dtMN.Rows[iR]["PurchId"].ToString(),
                                dtMN.Rows[iR]["InventLocationId"].ToString(),
                                dtMN.Rows[iR]["IVZ_Remark"].ToString(),
                                dtMN.Rows[iR]["INVOICEID"].ToString(),
                                dtMN.Rows[iR]["INVOICEDATE"].ToString(),
                                dtMN.Rows[iR]["SALESBALANCE"].ToString(),
                                dtMN.Rows[iR]["SumTax"].ToString(),
                                dtMN.Rows[iR]["INVOICEAMOUNT"].ToString(),
                                dtMN.Rows[iR]["Name"].ToString());
                    }
                    if (_pTypeVenderCode == "V005450")
                    {
                        DataTable dtRetail = PurchClass.FarmHouse_GetPR_Retail(pDate1, pDate2, _pTypeVenderCode);
                        for (int i = 0; i < dtRetail.Rows.Count; i++)
                        {
                            DtSetGrid.Rows.Add("1",
                                 dtRetail.Rows[i]["LedgerVoucher"].ToString(),
                                 dtRetail.Rows[i]["PurchId"].ToString(),
                                 dtRetail.Rows[i]["InventLocationId"].ToString(),
                                 dtRetail.Rows[i]["IVZ_Remark"].ToString(),
                                 dtRetail.Rows[i]["INVOICEID"].ToString(),
                                 dtRetail.Rows[i]["INVOICEDATE"].ToString(),
                                 dtRetail.Rows[i]["SALESBALANCE"].ToString(),
                                 dtRetail.Rows[i]["SumTax"].ToString(),
                                 dtRetail.Rows[i]["INVOICEAMOUNT"].ToString(),
                                 dtRetail.Rows[i]["Name"].ToString());
                        }
                    }

                    this.Cursor = Cursors.Default;

                    RadGridView_ShowHD.DataSource = DtSetGrid;
                    DtSetGrid.AcceptChanges();

                    if (DtSetGrid.Rows.Count > 0)
                    {
                        radButton_PB.Enabled = false;
                        radDateTimePicker_PB1.Enabled = false; radDateTimePicker_PB2.Enabled = false;
                        radDateTimePicker_PXS1.Enabled = true; radDateTimePicker_PXS2.Enabled = true;
                        radButton_PXS.Enabled = true; radButton_PXS.Focus();
                    }
                    break;
                case "PD":
                    bchID_PD = "";
                    FindDataAll("");
                    break;
                default:
                    break;
            }

        }
        //Find PXS
        private void RadButton_PXS_Click(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "PB":
                    this.Cursor = Cursors.WaitCursor;
                    string pDate1 = radDateTimePicker_PXS1.Value.ToString("yyyy-MM-dd");
                    string pDate2 = radDateTimePicker_PXS2.Value.ToString("yyyy-MM-dd");
                    DataTable dtPXS = PurchClass.FarmHouse_Get_PXS(pDate1, pDate2, _pTypeVenderCode);
                    for (int i = 0; i < dtPXS.Rows.Count; i++)
                    {
                        DtSetGrid.Rows.Add("1",
                            dtPXS.Rows[i]["LedgerVoucher"].ToString(),
                            dtPXS.Rows[i]["PurchId"].ToString(),
                            dtPXS.Rows[i]["InventLocationId"].ToString(),
                            dtPXS.Rows[i]["IVZ_Remark"].ToString(),
                            dtPXS.Rows[i]["INVOICEID"].ToString(),
                            dtPXS.Rows[i]["INVOICEDATE"].ToString(),
                            dtPXS.Rows[i]["SALESBALANCE"].ToString(),
                            dtPXS.Rows[i]["SumTax"].ToString(),
                            dtPXS.Rows[i]["INVOICEAMOUNT"].ToString(),
                            dtPXS.Rows[i]["Name"].ToString());
                    }

                    RadGridView_ShowHD.DataSource = DtSetGrid;
                    DtSetGrid.AcceptChanges();

                    radButton_PXS.Enabled = false;
                    radDateTimePicker_PXS1.Enabled = false; radDateTimePicker_PXS2.Enabled = false;
                    RadButton_Save.Enabled = true; RadButton_Save.Focus();
                    this.Cursor = Cursors.Default;
                    break;
                case "PD":
                    bchID_PD = "RETAILAREA";
                    FindDataAll("RETAILAREA");

                    break;
                default:
                    break;
            }
        }


        //ค้นหา PD ว่าใช้ไปหรือยัง
        string FindPD24(string pDocno)
        {
            DataTable dt = FarmHouse_Class.FarmHouse_GetPD24(pDocno);
            if (dt.Rows.Count > 0)
            {
                pSave = "1";
                radDateTimePicker_PB1.Enabled = false;
                radDateTimePicker_PB1.Value = Convert.ToDateTime(dt.Rows[0]["RECIVEDATE"].ToString());
                radLabel_Detail.Text = dt.Rows[0]["RemarkPD"].ToString();
                RadButton_Save.Text = "พิมพ์";
                if (dt.Rows[0]["INVENTLOCATIONID"].ToString() != "RETAILAREA") return "";
                else return dt.Rows[0]["INVENTLOCATIONID"].ToString();
            }
            else
            {
                pSave = "0";
                RadButton_Save.Text = "บันทึก";
                radDateTimePicker_PB1.Enabled = true; radDateTimePicker_PB1.Value = DateTime.Now;
                radDateTimePicker_PXS1.Enabled = true; radDateTimePicker_PXS1.Value = DateTime.Now;
                return "";
            }
        }
        //print
        void PrintData()
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                PrintDocument1.PrintController = printController;
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;
                PrintDocument1.Print();
            }

        }

        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "PB":
                    if (RadGridView_ShowHD.Rows.Count < 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกรายการได้ เนื่องจากไม่มีรายการสินค้า");
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert(" บันทึกใบวางบิล ") == DialogResult.No) return;

                    this.Cursor = Cursors.WaitCursor;
                    ArrayList sqlIn = new ArrayList();

                    for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                    {
                        if (RadGridView_ShowHD.Rows[i].Cells["C"].Value.ToString() == "1")
                        {
                            sqlIn.Add(AX_SendData.Save_PurchBillLine01(DocnoRef, i + 1, RadGridView_ShowHD.Rows[i].Cells["INVOICEID"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["LedgerVoucher"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["INVOICEDATE"].Value.ToString()));
                        }
                    }

                    string T = ConnectionClass.ExecuteSQL_ArrayMainAX(sqlIn);
                    if (T == "") RadButton_Save.Enabled = false;

                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    this.Cursor = Cursors.Default;
                    break;

                case "PD":
                    if (_pPermission != "1")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถใช้งานฟังก์ชั่นนี้ได้ เนื่องจากไม่มีสิทธิ์ในการใช้งาน{Environment.NewLine}[ ปกติแผนกที่จะพิมพ์ได้คือแผนกข้อมูลคอม ]");
                        return;
                    }

                    if (pSave == "1")
                    {
                        PrintData();
                        return;
                    }


                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("เลขที่ PD ที่ระบุ") == DialogResult.No) return;

                    this.Cursor = Cursors.WaitCursor;
                    string rmk = "";

                    if (GrandCN != GrandPD)
                    {
                        int r = 0;
                        for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                        {
                            if (Convert.ToDouble(RadGridView_ShowHD.Rows[i].Cells["Diff"].Value.ToString()) > 0)
                            {
                                r += 1;
                            }
                        }

                        if (r != 0)
                        {
                            FormShare.InputData _inputData = new FormShare.InputData("1",
                                 $@"เลขที่ {RadGridView_ShowHD.Rows[0].Cells["PURCHID"].Value} วันที่ {radDateTimePicker_PB1.Value:yyyy-MM-dd}", "หมายเหตุการตรวจ", "");

                            if (_inputData.ShowDialog(this) == DialogResult.Yes)
                            {
                                rmk = _inputData.pInputData;
                            }
                            else
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุเหตุผลในการคืนให้เรียบร้อยเพราะมีรายการที่แตกต่างกัน");
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                    }

                    string res = ConnectionClass.ExecuteSQL_Main(FarmHouse_Class.FarmHouse_UpdateVenderPR(DocnoRef, bchID_PD, radDateTimePicker_PB1.Value.ToString("yyyy-MM-dd"), rmk, "V005450"));
                    MsgBoxClass.MsgBoxShow_SaveStatus(res);
                    this.Cursor = Cursors.Default;
                    if (res == "")
                    {
                        pSave = "1";
                        radLabel_Detail.Text = rmk;
                        RadButton_Save.Text = "พิมพ์";
                        PrintData();
                        return;
                    }
                    break;
                default:
                    break;
            }

        }

        private void RadGridView_ShowHD_GroupSummaryEvaluate(object sender, GroupSummaryEvaluationEventArgs e)
        {
            if (e.SummaryItem.Name == "Sum") GrandCN = Convert.ToDouble(e.Value);
            if (e.SummaryItem.Name == "Grand") GrandPD = Convert.ToDouble(e.Value);
        }

        private void RadButtonElement_Refrsh_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            MsgBoxClass.MsgBoxShow_SaveStatus(DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1"));
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            barcode.Data = DocnoRef;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 0;
            int r = 0;

            if (bchID_PD == "") e.Graphics.DrawString("เอกสารเช็คการคืนสินค้า[มิินิมาร์ท]", SystemClass.printFont, Brushes.Black, 5, Y);
            else e.Graphics.DrawString("เอกสารเช็คการคืนสินค้า[สาขาใหญ่]", SystemClass.printFont, Brushes.Black, 5, Y);

            Y += 20;
            e.Graphics.DrawString("เลขที่ใบคืนสินค้า  " + DocnoRef + "", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่รับสินค้า " + radDateTimePicker_PB1.Value.ToString("yyyy-MM-dd"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 67;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 20;
            e.Graphics.DrawString("ผู้พิมพ์ - " + SystemClass.SystemUserID + " " + SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            if (GrandCN == GrandPD)
            {
                Y += 20;
                e.Graphics.DrawString("รับสินค้าครบทุกรายการ", SystemClass.printFont, Brushes.Black, 10, Y);
                Y += 15;
                e.Graphics.DrawString("จำนวนทั้งหมด  " + Convert.ToString(RadGridView_ShowHD.Rows.Count) + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
                Y += 15;
                e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            }
            else
            {
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (Convert.ToDouble(RadGridView_ShowHD.Rows[i].Cells["Diff"].Value.ToString()) > 0)
                    {
                        r += 1;

                        Y += 20;
                        e.Graphics.DrawString(Convert.ToInt32(i + 1) + "." + RadGridView_ShowHD.Rows[i].Cells["BarCode"].Value.ToString() + " - " +
                             RadGridView_ShowHD.Rows[i].Cells["NAME"].Value.ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
                        Y += 15;
                        e.Graphics.DrawString("จำนวนในบิล/รับจริง  : " + RadGridView_ShowHD.Rows[i].Cells["QTY"].Value.ToString() + " / " +
                            RadGridView_ShowHD.Rows[i].Cells["QTY_RECIVE"].Value.ToString(), SystemClass.printFont, Brushes.Black, 10, Y);
                        Y += 15;
                        e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    }
                }

                if (r == 0)
                {
                    Y += 20;
                    e.Graphics.DrawString("รับสินค้าครบทุกรายการ", SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด  " + Convert.ToString(RadGridView_ShowHD.Rows.Count) + "  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 15;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                }
                else
                {
                    Y += 20;
                    e.Graphics.DrawString("หมายเหตุ : ", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    Rectangle rect1 = new Rectangle(0, Y, 300, 200);
                    StringFormat stringFormat = new StringFormat()
                    {
                        Alignment = StringAlignment.Near,
                        LineAlignment = StringAlignment.Near
                    };

                    e.Graphics.DrawString(radLabel_Detail.Text, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
                }
            }

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
    }
}

