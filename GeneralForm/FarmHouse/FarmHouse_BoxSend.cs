﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;

namespace PC_Shop24Hrs.GeneralForm.FarmHouse
{
    public partial class FarmHouse_BoxSend : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();

        DataTable dtBch = new DataTable();

        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        readonly string _pTypeID;// รหัสกลุ่ม
        readonly string _pPermission;// 0 ไม่มีสิดบันทึก 1 มี
        public FarmHouse_BoxSend(string pTypeID, string pPermission)
        {
            InitializeComponent();
            _pTypeID = pTypeID;
            _pPermission = pPermission;
        }
        //Load
        private void FarmHouse_BoxSend_Load(object sender, EventArgs e)
        {
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดออเดอร์ใหม่";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";
            RadButton_Search.ButtonElement.ShowBorder = true; RadButton_Search.ButtonElement.ToolTipText = "ค้นหาข้อมูล";
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Save.ButtonElement.ToolTipText = "บันทึกข้อมูล";
            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์";

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadGridView_ShowHD.ReadOnly = false;

            RadCheckBox_Branch.Checked = false;
            dtBch = ConfigClass.FindData_BranchconfigByTypeID(_pTypeID);
            RadCheckBox_Branch.Enabled = true;

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAMEDESC"; RadDropDownList_Branch.ValueMember = "BRANCH_ID";
        }

        //Set Head GridView
        void SetGridView()
        {
            DataTable dtBchSend = ConfigClass.FindData_CONFIGBRANCH_QTY(_pTypeID, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));// Get_BranchQty();

            if (dtBchSend.Rows.Count > 0)
            {
                RadButton_Save.Enabled = false; //pEdit = "1"; 
                RadGridView_ShowHD.ReadOnly = true;
            }
            else RadButton_Save.Enabled = true;// pEdit = "0"; 

            if (RadCheckBox_Branch.CheckState == CheckState.Checked) dtBch = dtBch = BranchClass.GetDetailBranchByID(RadDropDownList_Branch.SelectedValue.ToString());
            else dtBch = ConfigClass.FindData_BranchconfigByTypeID(_pTypeID);// Get_AllBranch();


            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            string pColumn; string pColumnName;
            string pRow; string pRowName;
            string labelGrp1; string labelGrp2;

            DataTable dt_ShowID = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(_pTypeID, "", "", "1"); //Get_ShowID();

            DataTable dtColumn; DataTable dtRow;
            if (radRadioButton_Bch.CheckState == CheckState.Checked)
            {
                pColumn = "BRANCH_ID"; pColumnName = "BRANCH_NAME"; dtColumn = dtBch;
                pRow = "SHOW_ID"; pRowName = "SHOW_NAME"; dtRow = dt_ShowID;
                labelGrp1 = "บาร์โค้ด"; labelGrp2 = "ชื่อสินค้า";
            }
            else
            {
                pColumn = "SHOW_ID"; pColumnName = "SHOW_NAME"; dtColumn = dt_ShowID;
                pRow = "BRANCH_ID"; pRowName = "BRANCH_NAME"; dtRow = dtBch;
                labelGrp1 = "สาขา"; labelGrp2 = "ชื่อสาขา";
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(pRow, labelGrp1, 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(pRowName, labelGrp2, 170)));
            RadGridView_ShowHD.Columns[pRow].IsPinned = true; RadGridView_ShowHD.Columns[pRowName].IsPinned = true;

            ConditionalFormattingObject obj1 = new ConditionalFormattingObject("MyCondition", ConditionTypes.Equal, "X", "", false)
            { CellBackColor = ConfigClass.SetColor_Red() };

            foreach (DataRow row in dtColumn.Rows)
            {
                string headText = row[pColumn].ToString() + System.Environment.NewLine + row[pColumnName].ToString();
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter(row[pColumn].ToString(), headText, 180)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString()].FormatString = "{0:#,##0.00}";

                RadGridView_ShowHD.Columns[row[pColumn].ToString()].ConditionalFormattingObjectList.Add(obj1);

                GridViewSummaryItem summaryItem = new GridViewSummaryItem(row[pColumn].ToString(), "รวม = {0}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem);
            }

            DataTable dtBranchSet = ConfigClass.FindData_CONFIGBRANCH_DETAIL(_pTypeID); //Get_BranchSend();
            for (int iR = 0; iR < dtRow.Rows.Count; iR++)
            {
                RadGridView_ShowHD.Rows.Add(dtRow.Rows[iR][pRow], dtRow.Rows[iR][pRowName]);
                for (int iC = 0; iC < dtColumn.Rows.Count; iC++)
                {
                    string values_Set = "X";
                    DataRow[] dr = dtBranchSet.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' ");

                    if (dr.Length > 0)
                    {
                        DataRow[] drQty = dtBchSend.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' ");
                        if (drQty.Length > 0) values_Set = Convert.ToDouble(drQty[0]["QTY"].ToString()).ToString("N2");
                        else values_Set = "0.00";
                    }
                    RadGridView_ShowHD.Rows[iR].Cells[dtColumn.Rows[iC][pColumn].ToString()].Value = values_Set;// dr.Length;
                }
            }

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

            RadCheckBox_Branch.Enabled = false; RadDropDownList_Branch.Enabled = false;
            radRadioButton_Bch.Enabled = false; radRadioButton_Item.Enabled = false;
            radDateTimePicker_Begin.Enabled = false; RadButton_Search.Enabled = false;


        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetGridView();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true;
            else RadDropDownList_Branch.Enabled = false;
        }

        //Begin Edit
        private void RadGridView_ShowHD_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {

            if ((e.Column == RadGridView_ShowHD.Columns[0]) || (e.Column == RadGridView_ShowHD.Columns[1])) { e.Cancel = true; }
            try
            {
                if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "X") { e.Cancel = true; }
            }
            catch (Exception) { return; }

        }
        //EndEdit
        private void RadGridView_ShowHD_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            try
            {
                RadGridView_ShowHD.CurrentCell.Value = Convert.ToDouble(RadGridView_ShowHD.CurrentCell.Value.ToString()).ToString("N2");
            }
            catch (Exception) { return; }
        }
        //Number Only 
        private void RadGridView_ShowHD_CellValidated(object sender, CellValidatedEventArgs e)
        {

            if ((e.Column == RadGridView_ShowHD.Columns[0]) || (e.Column == RadGridView_ShowHD.Columns[1])) { return; }
            try
            {
                if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "X") return;
            }
            catch (Exception) { return; }


            if (!ConfigClass.IsNumber(e.Value.ToString()))
            {
                MsgBoxClass.MsgBoxShow_InputNumbersOnly("จำนวนส่ง");
                RadGridView_ShowHD.CurrentCell.Value = "0.00"; return;
            }
            else return;

        }
        //ClearDate
        void ClearData()
        {
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            }
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(1);
            //pEdit = "0";
            RadCheckBox_Branch.Enabled = true; RadDropDownList_Branch.Enabled = true;
            radRadioButton_Bch.Enabled = true; radRadioButton_Item.Enabled = true;
            radDateTimePicker_Begin.Enabled = true; RadButton_Search.Enabled = true;
            RadButton_Save.Enabled = false;
        }
        //เพิ่ม ข้อมูลใหม่
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (_pPermission == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("จำกัดสิทธิ์การใช้งาน" + Environment.NewLine + "เฉพาะเจ้าของแผนกเท่านั้นที่จะมีสิทธิ์ในการบันทึกจำนวนลังส่งมินิมาร์ท");
                return;
            }

            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการส่งฟาร์มเฮ้าส์ประจำวัน " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd")) == DialogResult.No)
            {
                return;
            }

            ArrayList strIn = new ArrayList();
            ArrayList strAX = new ArrayList();

            string showID = ""; string bchID = "";
            string billSend;

            int count_0 = 0;

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++) //สินค้า
            {
                if (radRadioButton_Bch.CheckState == CheckState.Checked)
                { showID = RadGridView_ShowHD.Rows[iR].Cells["SHOW_ID"].Value.ToString(); }
                else { bchID = RadGridView_ShowHD.Rows[iR].Cells["BRANCH_ID"].Value.ToString(); }

                for (int iC = 2; iC < RadGridView_ShowHD.Columns.Count; iC++) // สาขา
                {
                    double qty = 0;

                    if (RadGridView_ShowHD.Rows[iR].Cells[iC].Value.ToString() != "X")
                    {
                        qty = Convert.ToDouble(RadGridView_ShowHD.Rows[iR].Cells[iC].Value.ToString());
                    }

                    if (qty == 0) { count_0++; }


                    if (radRadioButton_Bch.CheckState == CheckState.Checked) { bchID = RadGridView_ShowHD.Columns[iC].Name; }
                    else { showID = RadGridView_ShowHD.Columns[iC].Name; }

                    billSend = showID + "-" + bchID.Replace("MN", "") + "-" + radDateTimePicker_Begin.Value.ToString("yyMMdd") + "-" + Convert.ToString(iR) + Convert.ToString(iC);
                    strIn.Add(ConfigClass.Save_CONFIGBRANCH_QTY(_pTypeID, billSend, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), bchID, showID, qty));
                    string bchName = BranchClass.GetBranchNameByID(bchID);
                    string recId = billSend.Replace("FH", "").Replace("-", "") + Convert.ToString(iC);// Convert.ToString(iC);
                    strAX.Add(AX_SendData.Save_EXTERNALLIST_SHIPMENTINVOICE(billSend, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), bchID, bchName, recId));
                }
            }

            if (count_0 > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ยังระบุจำนวนลังไม่ครบทุกสาขา" + Environment.NewLine + "ไม่สามารถบันทึกข้อมูลได้.");
                return;
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteMain_AX_24_SameTime(strIn, strAX));
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("FarmHouse Send " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //print All
        private void RadButtonElement_printAll_Click(object sender, EventArgs e)
        {
            if (RadButton_Save.Enabled == true) return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (RadGridView_ShowHD.CurrentRow.RowElementType != typeof(GridDataRowElement)) return;
            if ((RadGridView_ShowHD.CurrentCell.ColumnIndex == 0) || (RadGridView_ShowHD.CurrentCell.ColumnIndex == 1)) return;

            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;
            }
            else return;

            FormShare.ChooseData _ChooseData = new FormShare.ChooseData("ทั้งรายการ[ทุกสาขา]", "เฉพาะที่เลือก[1 รายการ]");
            if (_ChooseData.ShowDialog(this) == DialogResult.Yes)
            {
                string showID; string bchID;
                if (radRadioButton_Bch.CheckState == CheckState.Checked)
                {
                    showID = RadGridView_ShowHD.CurrentRow.Cells["SHOW_ID"].Value.ToString();
                    bchID = RadGridView_ShowHD.CurrentColumn.Name;
                }
                else
                {
                    showID = RadGridView_ShowHD.CurrentColumn.Name;
                    bchID = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                }

                if (_ChooseData.sSendData == "1") Print("1", bchID, showID);
                else
                {
                    if ((RadGridView_ShowHD.CurrentRow.Cells[0].IsSelected) && (RadGridView_ShowHD.CurrentRow.Cells[1].IsSelected)) { return; }
                    if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "X") { return; }
                    if (Convert.ToDouble(RadGridView_ShowHD.CurrentCell.Value.ToString()) == 0) { return; }

                    Print("0", bchID, showID);
                }
            }
        }
        //Print
        string pBchID, pBchName, pQty, pBillID;
        int iBoxR;

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        void Print(string pTypePrint, string pBch, string pBarcode) //pTypePrint 1 = All 0 = only Select
        {

            pBchID = ""; pBchName = ""; pQty = ""; pBillID = "";

            string pBchIDSend = "";
            if (pTypePrint != "1") pBchIDSend = pBch;

            DataTable dtPrint = FarmHouse_Class.FarmHouse_QtySendBox(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), pBarcode, pBchIDSend);
            if (dtPrint.Rows.Count == 0) return;
            PrintDocument1.PrintController = printController;
            for (int iR = 0; iR < dtPrint.Rows.Count; iR++)
            {
                pBchID = dtPrint.Rows[iR]["BRANCH_ID"].ToString();
                pBchName = dtPrint.Rows[iR]["BRANCH_NAME"].ToString();
                pQty = dtPrint.Rows[iR]["Qty"].ToString();
                pBillID = dtPrint.Rows[iR]["BILLSEND"].ToString();
                int iBox = Convert.ToInt32(pQty);
                iBoxR = 0;
                for (int i = 0; i < iBox; i++)
                {
                    iBoxR += 1;
                    PrintDocument1.Print();
                }
            }
        }
        //Print
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = pBillID; Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());
            int Y = 20;
            e.Graphics.DrawString("ฟาร์มเฮ้าส์ส่งมินิมาร์ท", SystemClass.printFont15, Brushes.Black, 30, Y);
            Y += 30;
            e.Graphics.DrawString(pBchID + "-" + pBchName, SystemClass.printFont15, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 110;
            e.Graphics.DrawString("จำนวนลัง " + Convert.ToString(iBoxR) + " / " + pQty, SystemClass.printFont15, Brushes.Black, 25, Y);
            Y += 30;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
        }


    }
}
