﻿using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.FarmHouse
{
    //For Insert VENDERPR
    public class Var_VENDERPR
    {
        public string ItemID { get; set; } = "";
        public string InventItem { get; set; } = "";
        public string ITEMBARCODE { get; set; } = "";
        public string SPC_ITEMNAME { get; set; } = "";
        public double QTY_EXCEL { get; set; } = 0;
        public double QTY_CN { get; set; } = 0;
        public string UNITQTY { get; set; } = "";
        public string PURCHUNIT { get; set; } = "";
        public string INVOICE { get; set; } = "";
        public double PRICECOST { get; set; } = 0;
    }
    class FarmHouse_Class
    {
        //ค้นหา PR ตามวันที่ ว่าได้บันทึกไปหรือยัง
        public static int FarmHouse_GetCheckPR(string pDate, string pVender, string condition = "")
        {
            string sql = $@" 
                SELECT	ReciveDate 
                FROM	Shop_VenderPR WITH (NOLOCK) 
                WHERE	ReciveDate = '{pDate}' AND INVOICEACCOUNT = '{pVender}' 
                        {condition} ";
            return ConnectionClass.SelectSQL_Main(sql).Rows.Count;
        }
        //ค้นหา PD ที่ระบุ ว่าได้บันทึกไปหรือยัง
        public static DataTable FarmHouse_GetPD24(string pPdDocno)
        {
            string str = $@"
                SELECT	    INVENTLOCATIONID,RECIVEDATE,RemarkPD	FROM	Shop_VenderPR WITH (NOLOCK) where	PD = '{pPdDocno}'  ";
            return ConnectionClass.SelectSQL_Main(str);
        }

        //Get Data Vender Check
        public static DataTable FarmHouse_Recive(string Branch, string Date, string vendor)
        {
            string sql = $@" 
                DECLARE @bchID NVARCHAR(10) = '{Branch}';
                DECLARE @date NVARCHAR(10) = '{Date}';
                DECLARE @venID NVARCHAR(10) = '{vendor}';

                SELECT  ReciveItemBarcode as ITEMBARCODE, ReciveName as SPC_ITEMNAME, ReciveQtyBch as QTY,ReciveUnit as UNITID
                FROM    SHOP_VENDERCHECK   with (Nolock)
                WHERE   ReciveBranchID = @bchID
                        AND ReciveDate = @date
                        AND ReciveVender = @venID  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหาจำนวนลังที่ส่ง
        public static DataTable FarmHouse_QtySendBox(string date, string barcode, string bchID)
        {
            string pConditionBch = "";
            if (bchID != "") pConditionBch = $@" AND SHOP_CONFIGBRANCH_QTY.BRANCH_ID = '{bchID}' ";

            string sql = $@"
                SELECT  SHOP_CONFIGBRANCH_QTY.SHOW_ID,SHOW_NAME AS SPC_ITEMNAME,SHOP_CONFIGBRANCH_QTY.BRANCH_ID,BRANCH_NAME,SHOP_CONFIGBRANCH_QTY.QTY,BILLSEND,SHOW_DESC AS UNITID  
                FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK) 
                            INNER JOIN SHOP_BRANCH WITH (NOLOCK)    ON SHOP_CONFIGBRANCH_QTY.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                            INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_CONFIGBRANCH_QTY.SHOW_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID 
                WHERE	SHOP_CONFIGBRANCH_QTY.TYPE_CONFIG = '24' 
		                AND CONVERT(VARCHAR,DATE_SEND,23) = '{date}'
                        AND SHOP_CONFIGBRANCH_QTY.SHOW_ID = '{barcode}' {pConditionBch} 
		                AND SHOP_CONFIGBRANCH_QTY.QTY > 0
                ORDER BY SHOP_CONFIGBRANCH_QTY.BRANCH_ID   ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //เช็คสินค้าที่สาขารับ
        public static string FarmHouse_BranchRecive(string date1, string date2, string bchID, string pVenderID)
        {
            string pConBchID = "";
            if (bchID != "") pConBchID = $@" AND ReciveBranchID = '{bchID}' ";

            string sql = $@"
                    select	CONVERT(VARCHAR,ReciveDate,23) AS ReciveDate,ReciveBranchID,ReciveBranchName,ReciveItemBarcode,ReciveName,
		                    ISNULL(ReciveQtyBch,0) AS QTYBch,ISNULL(ReciveQtySupc,0) AS QtyEdit,ReciveUnit	
                    FROM	[dbo].[SHOP_VENDERCHECK]  WITH (NOLOCK) 
                    WHERE	ReciveVender = '{pVenderID}'
		                    AND ReciveDate BETWEEN '{date1}' AND '{date2}'   {pConBchID}
                    ORDER BY ReciveDate,ReciveBranchID,ReciveItemBarcode  
                ";
            return sql;
        }
        //เช็คจำนวนสินค้าที่สาขารับ
        public static DataTable FarmHouse_BranchReciveGroupCount(string date, string pVenderID)
        {
            string sql = $@"
                    SELECT	ReciveBranchID,SUM(ReciveQtyBch) AS ReciveCount
                    FROM	SHOP_VENDERCHECK WITH (NOLOCK)
                    WHERE	ReciveDate = '{date}'
		                    AND ReciveVender = '{pVenderID}'
                    GROUP BY ReciveBranchID  
                ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Insert VenderPR
        public static string FarmHouse_SaveVenderPR(string venderID, string bchID, int iRows, string docno, string date, Var_VENDERPR var_VENDERPR)
        {
            string sql = $@" 
                            Insert Into SHOP_VENDERPR 
                                (INVOICEACCOUNT,INVENTLOCATIONID,  
                                 LINENUM, PURCHID, DueDate, 
                                 ITEMID, INVENTDIMID, 
                                 SPC_ITEMBARCODE, Name, 
                                 EXCELQTY,CNQTY,AXQTY, 
                                 INVENTQTY, PURCHUNIT, 
                                 ReciveInvoice, ReciveDate, 
                                 WhoIns,PRICECOST ) 
                             values('{venderID}','{bchID}', 
                                 '{iRows}', '{docno}','{date}',  
                                 '{var_VENDERPR.ItemID}', '{var_VENDERPR.InventItem}', 
                                 '{var_VENDERPR.ITEMBARCODE}','{var_VENDERPR.SPC_ITEMNAME}',
                                 '{var_VENDERPR.QTY_EXCEL}', '{var_VENDERPR.QTY_CN}',  '{var_VENDERPR.QTY_EXCEL}',  
                                 '{var_VENDERPR.UNITQTY}','{var_VENDERPR.PURCHUNIT}',
                                 '{var_VENDERPR.INVOICE}',
                                 '{date}',  '{SystemClass.SystemUserID_M}','{var_VENDERPR.PRICECOST}')  ";
            return sql;
        }

        //Update VenderPR
        public static string FarmHouse_UpdateVenderPR(string docno, string bchID, string date, string rmk, string venderID)
        {
            string conditionInventlocationID = " AND INVENTLOCATIONID != 'RETAILAREA' ";
            if (bchID != "") conditionInventlocationID = " AND INVENTLOCATIONID = 'RETAILAREA' ";

            string sql = $@"
                UPDATE  Shop_VenderPR 
                SET     PD = '{docno}',RemarkPD = '{rmk}' 
                WHERE   recivedate = '{date}' {conditionInventlocationID} 
                        AND INVOICEACCOUNT = '{venderID}' ";
            return sql;
        }
    }
}
