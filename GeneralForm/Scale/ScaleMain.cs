﻿
using System;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System.Collections.Generic;

namespace PC_Shop24Hrs.GeneralForm.Scale
{
    public partial class ScaleMain : Telerik.WinControls.UI.RadForm
    {
        DataTable ScaleTable = new DataTable("ScaleTable");
        DataTable DeleteScaleTable = new DataTable("DeleteScaleTable");
        DataTable data_scale = new DataTable();
        readonly string _Typespc, _Usespc, SystemServerDBScale = @"ScaleSHOP";
        string update;
        string ConnectionScale;  //เพิ่มลบสินค้า
        string ConnectionScale_TMaster; //ส่งราคา
        readonly int count = 45;
        string[] items, pricegroup;
        enum StatusUpdate
        {
            insert,      //0
            edit        //1
        }

        public ScaleMain(string Typespc, string Usespc)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _Typespc = Typespc;
            _Usespc = Usespc;
        }

        #region ROWSNUMBER
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        #region Events 
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void ScaleMain_Load(object sender, EventArgs e)
        {
            radGridView_Show.TableElement.RowHeight = 35;
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Price);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Purch);

            radCheckBox_Price.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Purch.ButtonElement.Font = SystemClass.SetFontGernaral;

            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่มสินค้า";
            radButtonElement_edit.ShowBorder = true; radButtonElement_edit.ToolTipText = "แก้ไขสินค้า";
            radButtonElement_send.ShowBorder = true; radButtonElement_send.ToolTipText = "ส่งราคา";
            radButtonElement_delet.ShowBorder = true; radButtonElement_delet.ToolTipText = "ลบสินค้า";
            radButtonElement_clear.ShowBorder = true; radButtonElement_clear.ToolTipText = "ลบสินค้ามินิมาร์ท";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radButton_Search.ButtonElement.ShowBorder = true;
            radStatusStrip1.SizingGrip = false;

            //GroupItem
            DatagridClass.SetDefaultFontDropDown(radDropDownList_exp);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Item);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_weigh);

            radCheckBox_Exp.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_SetscalMN.ButtonElement.Font = SystemClass.SetFontGernaral;
            radTextBox_Barcode.Font = SystemClass.SetFontGernaral;
            radTextBox_Barcode.ForeColor = Color.Blue;
            radLabel_spcName.Font = SystemClass.SetFontGernaral_Bold;
            radLabel_spcName.ForeColor = Color.Blue;
            RadButton_Update.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            //radDropDownList_Purch.DataSource = Scale2006Class.GetPurch();
            radDropDownList_Purch.DataSource = Models.ScaleClass.GetPurch();
            radDropDownList_Purch.DisplayMember = "DESCRIPTION_SHOW";
            radDropDownList_Purch.ValueMember = "NUM";

            radDropDownList_Price.DataSource = BranchClass.GetPriceLevel(); //QueryScaleClass.GetPriceGroup();
            radDropDownList_Price.DisplayMember = "LABEL";
            radDropDownList_Price.ValueMember = "COLUMNNAME";

            this.GetGridViewSummary();

            switch (_Typespc)
            {
                case "SUPC":
                    ConnectionScale_TMaster = IpServerConnectClass.ConScaleSUPC;
                    radCheckBox_Purch.Checked = true;
                    this.ClearPanelInfo();
                    this.CreateBindingList(count);

                    ConnectionScale = IpServerConnectClass.ConMainRatail707;                                              //ใช้จริง
                    radCheckBox_Price.Checked = true; radCheckBox_Price.ReadOnly = true; radButtonElement_clear.Enabled = false;
                    radCheckBox_Purch.Checked = true; radCheckBox_Purch.ReadOnly = true;
                    radDropDownList_Purch.DataSource = Models.DptClass.GetDpt_All();
                    radDropDownList_Price.SelectedValue = @"SPC_PriceGroup3";
                    radDropDownList_Purch.SelectedValue = SystemClass.SystemDptID;

                    this.BindDataGridSupcMaster();

                    pricegroup = GetPriceGroupMN();
                    radLabel_F2.Text = @"คอลัมน์ราคาขาย แสดงราคาตามเงื่อนไขที่เลือกด้านขวามือ";
                    break;
                case "SHOP":

                    ConnectionScale_TMaster = @"Data Source= " + SystemClass.SystemBranchServer + " ;Initial Catalog= " + SystemServerDBScale + ";User Id=sa;Password=Ca999999";

                    radButtonElement_add.Enabled = false; radButtonElement_edit.Enabled = false; radButtonElement_delet.Enabled = false;
                    radCheckBox_Purch.Visible = false; radDropDownList_Purch.Visible = false; radButtonElement_clear.Enabled = false;
                    radCheckBox_Price.Checked = true; radCheckBox_Price.ReadOnly = true;
                    radDropDownList_Price.Enabled = false; radDropDownList_Price.SelectedValue = SystemClass.SystemBranchPrice;
                    radButton_Search.Visible = false; radGroupBox_Item.Visible = false;
                    radLabel_F2.Text = @"กดปุ่มส่งราคา >> เข้าโปรแกรม Scale2006 เพื่อ Import ข้อมูลเข้าเครื่องชั่ง";
                    this.BindDataGridMaster();
                    this.GetDataScale();
                    break;
                default:
                    break;
            }

            string path = Application.StartupPath + @"\Scale2006.ini";
            try
            {
                if (System.IO.File.Exists(path))
                {
                }
                else
                {
                    string P = @"\\" + PathImageClass.pServerIpImg + @"\Minimart\Programe\Scale2006.ini";
                    System.IO.File.Copy(P, path);
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Methode
        void BindDataGridMaster()
        {
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("TYPE_SCALE", "ชั่งน้ำหนัก", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICE", "ราคาขาย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEEDIT", "วันที่ปรับราคา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TIMEEDIT", "เวลาที่ปรับราคา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBUYERGROUPID", "รหัส", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "จัดซื้อ", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("EXPDAY", "EXPDAY"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("EXPDAYPRINT", "EXPDAYPRINT"));

            data_scale = AddNewColumn(new string[] { "TYPE_SCALE", "ITEMBARCODE", "SPC_ITEMNAME", "UNITID", "PRICE", "DATEEDIT", "TIMEEDIT", "SPC_ITEMBUYERGROUPID", "DESCRIPTION", "EXPDAY", "EXPDAYPRINT" }
                                , new string[] { "ITEMBARCODE" });
        }
        void BindDataGridSupcMaster()
        {
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("TYPE_SCALE", "ชั่งน้ำหนัก", 70));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("TYPESEND", "กำหนดชั่งที่มินิมาร์ท", 130));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICE", "ราคาขาย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PriceGroup13", "ราคา SHOP1", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PriceGroup14", "ราคา SHOP2", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PriceGroup15", "ราคา SHOP3", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PriceGroup17", "ราคา SHOP4", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PriceGroup18", "ราคา SHOP5", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PriceGroup19", "ราคา SHOP6", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("DATEEDIT", "วันที่ปรับราคา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("TIMEEDIT", "เวลาที่ปรับราคา", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("SPC_ITEMBUYERGROUPID", "รหัส"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("DESCRIPTION", "จัดซื้อ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("EXPDAY", "EXPDAY"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("EXPDAYPRINT", "EXPDAYPRINT"));

            data_scale = AddNewColumn(new string[] { "TYPE_SCALE","TYPESEND","ITEMBARCODE", "SPC_ITEMNAME", "UNITID", "PRICE"
                            , "SPC_PriceGroup13", "SPC_PriceGroup14", "SPC_PriceGroup15", "SPC_PriceGroup17", "SPC_PriceGroup18",  "SPC_PriceGroup19","DATEEDIT", "TIMEEDIT", "SPC_ITEMBUYERGROUPID"
                            ,"DESCRIPTION","","EXPDAY","EXPDAYPRINT"}, new string[] { "ITEMBARCODE" });
        }
        DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null) return newColumn;

            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];

                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }

            return newColumn;
        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("SPC_ITEMNAME", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.radGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void GetDataScale()
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            if (ScaleTable.Rows.Count > 0) ScaleTable.Rows.Clear();
            if (data_scale.Rows.Count > 0) data_scale.Rows.Clear();
            if (radGridView_Show.Rows.Count > 0) radGridView_Show.Rows.Clear();

            switch (_Typespc)
            {
                case "SHOP":
                    //ScaleTable = Scale2006Class.GetDataScaleShopMain(SystemClass.SystemBranchPrice);
                    ScaleTable = Models.ScaleClass.GetDataScaleShopMain(SystemClass.SystemBranchPrice);
                    foreach (DataRow row in ScaleTable.Rows)
                    {
                        data_scale.Rows.Add(row["TYPE_SCALE"].ToString(),
                            row["ITEMBARCODE"].ToString(),
                            row["SPC_ITEMNAME"].ToString(),
                            row["UNITID"].ToString(),
                            Convert.ToDouble(row["PRICE"]).ToString("#,#0.00"),
                            row["DATEEDIT"].ToString(),
                            row["TIMEEDIT"].ToString(),
                            row["SPC_ITEMBUYERGROUPID"].ToString(),
                            row["DESCRIPTION"].ToString(),
                            row["EXPDAY"].ToString(),
                            Convert.ToInt32(row["EXPDAYPRINT"]).ToString());
                    }
                    break;

                case "SUPC":
                    //ScaleTable = Scale2006Class.GetDataScale(radDropDownList_Price.SelectedValue.ToString(), radDropDownList_Purch.SelectedValue.ToString());
                    ScaleTable = Models.ScaleClass.GetDataScale(radDropDownList_Price.SelectedValue.ToString(), radDropDownList_Purch.SelectedValue.ToString());
                    foreach (DataRow row in ScaleTable.Rows)
                    {
                        data_scale.Rows.Add(row["TYPE_SCALE"].ToString(),
                            row["TYPESEND"].ToString(),
                            row["ITEMBARCODE"].ToString(),
                            row["SPC_ITEMNAME"].ToString(),
                            row["UNITID"].ToString(),
                            Convert.ToDouble(row["PRICE"]).ToString("#,#0.00"),
                            Convert.ToDouble(row["SPC_PriceGroup13"]).ToString("#,#0.00"),
                            Convert.ToDouble(row["SPC_PriceGroup14"]).ToString("#,#0.00"),
                            Convert.ToDouble(row["SPC_PriceGroup15"]).ToString("#,#0.00"),
                            Convert.ToDouble(row["SPC_PriceGroup17"]).ToString("#,#0.00"),
                            Convert.ToDouble(row["SPC_PriceGroup18"]).ToString("#,#0.00"),
                            Convert.ToDouble(row["SPC_PriceGroup19"]).ToString("#,#0.00"),
                            row["DATEEDIT"].ToString(),
                            row["TIMEEDIT"].ToString(),
                            row["SPC_ITEMBUYERGROUPID"].ToString(),
                            row["DESCRIPTION"].ToString(),
                            row["EXPDAY"].ToString(),
                            Convert.ToInt32(row["EXPDAYPRINT"]).ToString());
                    }
                    break;
            }

            if (ScaleTable.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูลที่ต้องการ เช็คเงื่อนไขการดึงข้อมูลใหม่อีกครั้ง.");
            }
            else
            {
                radGridView_Show.DataSource = data_scale;
            }
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        /// <summary>
        /// SendScaleShop(ส่งราคาตาชั่งมินิมาร์ท)
        /// </summary>
        /// <param name="tablescalesend"></param>
        /// <returns></returns>
        private string SendScaleShop(DataTable tablescalesend)
        {
            foreach (DataRow row in ScaleTable.Rows)
            {
                if (row["SPC_ITEMNAME"].ToString().Length > 50)
                {
                    return string.Format(@"ชื่อสินค้า มีความยาวมากเกินไป กรุณาติดต่อจัดซื้อ. {0} ( {1} )", row["ITEMBARCODE"].ToString(), row["SPC_ITEMNAME"].ToString());
                }
            }
            ArrayList array = new ArrayList();
            ArrayList arrayLog = new ArrayList();
            //string sqlUpdateMasterProduct = @"Delete FROM T_MasterProduct";
            string sqlUpdateMasterProduct = @" truncate table T_MasterProduct";
            array.Add(sqlUpdateMasterProduct);
            foreach (DataRow row in ScaleTable.Rows)
            {
                ScaleClass Tmp = new ScaleClass()
                {
                    ITEMBARCODE = row["ITEMBARCODE"].ToString()
                    ,
                    SPC_ITEMNAME = row["SPC_ITEMNAME"].ToString()
                    ,
                    UNITID = row["ITEMBARCODE"].ToString()
                    ,
                    PRICE = Convert.ToDouble(row["PRICE"])
                    ,
                    TYPE_SCALE = row["TYPE_SCALE"].ToString()
                    ,
                    EXPDAYPRINT = row["EXPDAYPRINT"].ToString()
                };

                if (Tmp.TYPE_SCALE.Equals("1"))
                {
                    Tmp.WEIGHT = 1;
                    Tmp.QTYSYMBOL = "";
                }
                else
                {
                    Tmp.WEIGHT = 0;
                    Tmp.QTYSYMBOL = "PCs.";
                }

                if (Tmp.EXPDAYPRINT.Equals("1"))
                {
                    Tmp.EXPDAY = Convert.ToInt32(row["EXPDAY"]);
                    Tmp.EXPDAYPRINT = "1";
                }
                else
                {
                    Tmp.EXPDAY = 0;
                    Tmp.EXPDAYPRINT = "0";
                }

                string sqlInsertMasterProduct = string.Format(@"
                        INSERT INTO T_MasterProduct 
                                (SKU,STATUS,NAME1,NAME1_SIZE,NAME2,NAME2_SIZE
                                ,BARCODEFORMAT,BARCODE,PRICE,ITMDISCOUNT,ITMDISCOUNTAMT,WEIGHT
                                ,PLU_TARE,EXPDAY,EXPDAYPRINT,LABELFORMAT,QTYSYMBOL,SP_SIZE
                                ,SP1,SP2,SP3,SP4,SP5,SP6,SP2_L_SIZE,SP2_L1,SP2_L2,SP2_L3,SP2_L4,SP2_L5,SP2_L6,PlaceNumber)
                        VALUES
                                ('{0}','C','{1}','T1','','M2'
                                ,'06H','2 {0}','{2}','1','0','{3}'
                                ,'0','{4}','{5}','F1','{6}','T1'
                                ,'','','','','','','T1','','','','','','','')"
                        , Tmp.ITEMBARCODE.Substring(1, 6)
                        , Tmp.SPC_ITEMNAME
                        , Tmp.PRICE
                        , Tmp.WEIGHT
                        , Tmp.EXPDAY
                        , Tmp.EXPDAYPRINT
                        , Tmp.QTYSYMBOL);

                array.Add(sqlInsertMasterProduct);
            }

            arrayLog.Add(string.Format(@"
                        INSERT INTO SHOP_LOGSHOP24HRS
                                ([LogBranch],[LogBranchName],[LogType],[LogWhoID],[LogWhoIDName],[LogRow],[LogMachine],[LogTmp]) 
                        values 
                                ('{0}','{1}','SendScaleShop','{2}','{3}','{4}','{5}','{0}')"
                      , SystemClass.SystemBranchID
                      , SystemClass.SystemBranchName
                      , SystemClass.SystemUserID_M
                      , SystemClass.SystemUserName
                      , Convert.ToDouble(tablescalesend.Rows.Count)
                      , SystemClass.SystemPcName));

            //return ExecuteTwoTransections(array, ConnectionScale_TMaster, arrayLog, IpServerConnectClass.ConSelectMain);
            return ConnectionClass.Execute_SameTime_2Server(array, ConnectionScale_TMaster, arrayLog, IpServerConnectClass.ConSelectMain);
        }
        /// <summary>
        /// SendScaleSupc(ส่งราคาตาที่สาขาใหญ่)
        /// </summary>
        /// <param name="tablescalesend"></param>
        /// <returns></returns>
        private string SendScaleSupc(DataTable tablescalesend, string price)
        {
            ArrayList array = new ArrayList();
            ArrayList arrayLog = new ArrayList();

            string sqlUpdateMasterProduct = @"Delete FROM T_MasterProduct";
            array.Add(sqlUpdateMasterProduct);

            //ส่งราคาสาขาใหญ่
            if (price.Equals("SUPC"))
            {
                foreach (DataRow row in tablescalesend.Rows)
                {
                    ScaleClass Tmp = new ScaleClass()
                    {
                        ITEMBARCODE = row["ITEMBARCODE"].ToString(),
                        SPC_ITEMNAME = row["SPC_ITEMNAME"].ToString(),
                        UNITID = row["ITEMBARCODE"].ToString(),
                        TYPE_SCALE = row["TYPE_SCALE"].ToString(),
                        EXPDAYPRINT = Convert.ToInt32(row["EXPDAYPRINT"]).ToString(),
                        PRICE = Convert.ToDouble(row["PRICE"])
                    };

                    if (Tmp.TYPE_SCALE.Equals("1"))
                    {
                        Tmp.WEIGHT = 1;
                        Tmp.QTYSYMBOL = "";
                    }
                    else
                    {
                        Tmp.WEIGHT = 0;
                        Tmp.QTYSYMBOL = "No symbol";
                    }

                    if (Tmp.EXPDAYPRINT.Equals("1"))
                    {
                        Tmp.EXPDAY = Convert.ToInt32(row["EXPDAY"]);
                        Tmp.EXPDAYPRINT = "1";
                    }
                    else
                    {
                        Tmp.EXPDAY = 0;
                        Tmp.EXPDAYPRINT = "0";
                    }

                    string sqlInsertMasterProduct = string.Format(@"
                            INSERT INTO T_MasterProduct 
                                        (SKU,STATUS,NAME1,NAME1_SIZE,NAME2,NAME2_SIZE
                                        ,BARCODEFORMAT,BARCODE,PRICE,ITMDISCOUNT,ITMDISCOUNTAMT,WEIGHT
                                        ,PLU_TARE,EXPDAY,EXPDAYPRINT,LABELFORMAT,QTYSYMBOL,SP_SIZE
                                        ,SP1,SP2,SP3,SP4,SP5,SP6,SP2_L_SIZE,SP2_L1,SP2_L2,SP2_L3,SP2_L4,SP2_L5,SP2_L6,PlaceNumber)
                             VALUES
                                        ('{0}','C','{1}','T1','','M2'
                                         ,'06H','2 {0}','{2}','1','0','{3}'
                                         ,'0','{4}','{5}','F1','{6}','T1'
                                         ,'','','','','','','T1','','','','','','','')"
                            , Tmp.ITEMBARCODE.Substring(1, 6)
                            , Tmp.SPC_ITEMNAME
                            , Tmp.PRICE
                            , Tmp.WEIGHT
                            , Tmp.EXPDAY
                            , Tmp.EXPDAYPRINT
                            , Tmp.QTYSYMBOL);
                    array.Add(sqlInsertMasterProduct);
                }

                arrayLog.Add(string.Format(@"
                            INSERT INTO SHOP_LOGSHOP24HRS
                                        ([LogBranch],[LogBranchName],[LogType],[LogWhoID],[LogWhoIDName],[LogRow],[LogMachine]) 
                            values 
                                        ('{0}','{1}','SendScaleSUPC','{2}','{3}','{4}','{5}')"
                           , SystemClass.SystemDptID
                           , SystemClass.SystemDptName
                           , SystemClass.SystemUserID_M
                           , SystemClass.SystemUserName
                           , Convert.ToDouble(tablescalesend.Rows.Count)
                           , SystemClass.SystemPcName));
            }
            return ConnectionClass.Execute_SameTime_2Server(array, ConnectionScale_TMaster, arrayLog, IpServerConnectClass.ConSelectMain);
        }
        private string DeleteScale(DataTable tabledel)
        {
            ArrayList array = new ArrayList();
            ArrayList arrayLog = new ArrayList();
            string sqlUpdateMasterProduct = @"Delete FROM T_MasterProduct";
            array.Add(sqlUpdateMasterProduct);

            if (_Typespc.Equals("SHOP"))
            {
                foreach (DataRow row in tabledel.Rows)
                {
                    ScaleClass Tmp = new ScaleClass()
                    {
                        ITEMBARCODE = row["ITEMBARCODE"].ToString(),
                        SPC_ITEMNAME = row["SPC_ITEMNAME"].ToString(),
                        UNITID = row["ITEMBARCODE"].ToString(),
                        PRICE = Convert.ToDouble(row["PRICE"]),
                        TYPE_SCALE = row["TYPE_SCALE"].ToString(),
                        EXPDAYPRINT = Convert.ToInt32(row["EXPDAYPRINT"]).ToString()
                    };

                    if (Tmp.TYPE_SCALE.Equals("1"))
                    {
                        Tmp.WEIGHT = 1;
                        Tmp.QTYSYMBOL = "";
                    }
                    else
                    {
                        Tmp.WEIGHT = 0;
                        Tmp.QTYSYMBOL = "PCs.";
                    }

                    if (Tmp.EXPDAYPRINT.Equals("1"))
                    {
                        Tmp.EXPDAY = Convert.ToInt32(row["EXPDAY"]);
                        Tmp.EXPDAYPRINT = "1";
                    }
                    else
                    {
                        Tmp.EXPDAY = 0;
                        Tmp.EXPDAYPRINT = "0";
                    }

                    string sql = string.Format(@"
                            INSERT INTO T_MasterProduct 
                                    (SKU,STATUS,NAME1,NAME1_SIZE,NAME2,NAME2_SIZE
                                    ,BARCODEFORMAT,BARCODE,PRICE,ITMDISCOUNT,ITMDISCOUNTAMT,WEIGHT
                                    ,PLU_TARE,EXPDAY,EXPDAYPRINT,LABELFORMAT,QTYSYMBOL,SP_SIZE
                                    ,SP1,SP2,SP3,SP4,SP5,SP6,SP2_L_SIZE,SP2_L1,SP2_L2,SP2_L3,SP2_L4,SP2_L5,SP2_L6,PlaceNumber)
                            VALUES
                                    ('{0}','D','{1}','T1','','M2'
                                    ,'06H','2 {0}','0','1','0','{2}'
                                    ,'0','{3}','{4}','F1','{5}','T1'
                                    ,'','','','','','','T1','','','','','','','')"
                            , Tmp.ITEMBARCODE.Substring(1, 6)
                            , Tmp.SPC_ITEMNAME
                            , Tmp.WEIGHT
                            , Tmp.EXPDAY
                            , Tmp.EXPDAYPRINT
                            , Tmp.QTYSYMBOL);

                    array.Add(sql);
                }
                arrayLog.Add(string.Format(@"
                        INSERT INTO SHOP_LOGSHOP24HRS
                                ([LogBranch],[LogBranchName],[LogType],[LogWhoID],[LogWhoIDName],[LogRow],[LogMachine],[LogTmp]) 
                        values 
                                ('{0}','{1}','SendScaleShop','{2}','{3}','{4}','{5}','{0}')"
                              , SystemClass.SystemBranchID
                              , SystemClass.SystemBranchName
                              , SystemClass.SystemUserID_M
                              , SystemClass.SystemUserName
                              , Convert.ToDouble(tabledel.Rows.Count)
                              , SystemClass.SystemPcName));
            }

            else
            {
                ScaleCatl scale = new ScaleCatl(pricegroup);
                foreach (var prc in pricegroup)
                {
                    string catalog = GetCatalogScal(scale, prc);
                    foreach (DataRow row in tabledel.Rows)
                    {
                        ScaleClass Tmp = new ScaleClass()
                        {
                            ITEMBARCODE = row["ITEMBARCODE"].ToString(),
                            SPC_ITEMNAME = row["SPC_ITEMNAME"].ToString(),
                            UNITID = row["ITEMBARCODE"].ToString(),
                            PRICE = Convert.ToDouble(row["PRICE"]),
                            TYPE_SCALE = row["TYPE_SCALE"].ToString(),
                            EXPDAYPRINT = Convert.ToInt32(row["EXPDAYPRINT"]).ToString()
                        };

                        if (Tmp.TYPE_SCALE.Equals("1"))
                        {
                            Tmp.WEIGHT = 1;
                            Tmp.QTYSYMBOL = "";
                        }
                        else
                        {
                            Tmp.WEIGHT = 0;
                            Tmp.QTYSYMBOL = "PCs.";
                        }

                        if (Tmp.EXPDAYPRINT.Equals("1"))
                        {
                            Tmp.EXPDAY = Convert.ToInt32(row["EXPDAY"]);
                            Tmp.EXPDAYPRINT = "1";
                        }
                        else
                        {
                            Tmp.EXPDAY = 0;
                            Tmp.EXPDAYPRINT = "0";
                        }

                        string sql = string.Format(@"
                                INSERT INTO {6}T_MasterProduct 
                                        (SKU,STATUS,NAME1,NAME1_SIZE,NAME2,NAME2_SIZE
                                        ,BARCODEFORMAT,BARCODE,PRICE,ITMDISCOUNT,ITMDISCOUNTAMT,WEIGHT
                                        ,PLU_TARE,EXPDAY,EXPDAYPRINT,LABELFORMAT,QTYSYMBOL,SP_SIZE
                                        ,SP1,SP2,SP3,SP4,SP5,SP6,SP2_L_SIZE,SP2_L1,SP2_L2,SP2_L3,SP2_L4,SP2_L5,SP2_L6,PlaceNumber)
                                VALUES
                                        ('{0}','D','{1}','T1','','M2'
                                        ,'06H','2 {0}','0','1','0','{2}'
                                        ,'0','{3}','{4}','F1','{5}','T1'
                                        ,'','','','','','','T1','','','','','','','')"
                                , Tmp.ITEMBARCODE.Substring(1, 6)
                                , Tmp.SPC_ITEMNAME, Tmp.WEIGHT, Tmp.EXPDAY, Tmp.EXPDAYPRINT, Tmp.QTYSYMBOL, catalog);

                        array.Add(sql);
                    }
                    arrayLog.Add(string.Format(@"
                                INSERT INTO SHOP_LOGSHOP24HRS
                                        ([LogBranch],[LogBranchName],[LogType],[LogWhoID],[LogWhoIDName],[LogRow],[LogMachine],[LogTmp]) 
                                values 
                                        ('{0}','{1}','SendScaleShop','{2}','{3}','{4}','{5}','{0}')"
                                , radDropDownList_Purch.SelectedValue.ToString()
                                , Models.DptClass.GetDptName_ByDptID(radDropDownList_Purch.SelectedValue.ToString())
                                , SystemClass.SystemUserID_M
                                , SystemClass.SystemUserName
                                , Convert.ToDouble(tabledel.Rows.Count)
                                , SystemClass.SystemPcName));
                }
            }
            return ConnectionClass.Execute_SameTime_2Server(array, ConnectionScale_TMaster, arrayLog, IpServerConnectClass.ConSelectMain);
        }
        /// <summary>
        /// exp วันหมดอายุ
        /// </summary>
        /// <param name="count"></param>
        private void CreateBindingList(int count)
        {
            items = new string[count];
            for (int i = 0; i < count; i++)
            {
                items[i] = (i + 1).ToString();
            }
        }
        private void UpdatePanelInfo(GridViewRowInfo currentRow)
        {
            this.radDropDownList_exp.DataSource = items;
            this.radDropDownList_exp.SelectedIndex = -1;
            if (currentRow != null)
            //&& !(currentRow is GridViewNewRowInfo)
            {
                this.radTextBox_Barcode.Text = currentRow.Cells["ITEMBARCODE"].Value.ToString();
                this.radLabel_spcName.Text = currentRow.Cells["SPC_ITEMNAME"].Value.ToString();

                if (currentRow.Cells["TYPE_SCALE"].Value.ToString().Equals("1")) radRadioButton_scale.IsChecked = true; else radRadioButton_psc.IsChecked = true;

                if (!currentRow.Cells["EXPDAY"].Value.ToString().Equals("0"))
                {
                    radCheckBox_Exp.Checked = true;
                    radDropDownList_exp.SelectedValue = currentRow.Cells["EXPDAY"].Value.ToString();
                }

                if (currentRow.Cells["TYPESEND"].Value.ToString().Equals("1")) radCheckBox_SetscalMN.Checked = true;

                radTextBox_Barcode.Enabled = false;
                RadButton_Update.Enabled = true; RadButton_Cancel.Enabled = true;
                radRadioButton_scale.Enabled = true; radRadioButton_psc.Enabled = true;
                radCheckBox_Exp.Enabled = true; radCheckBox_SetscalMN.Enabled = true;
            }
            else
            {
                radTextBox_Barcode.TextBoxElement.TextBoxItem.HostedControl.Select();
                radTextBox_Barcode.Enabled = true;
                radTextBox_Barcode.Text = string.Empty;
                radLabel_spcName.Text = string.Empty;
            }
        }
        private void ClearPanelInfo()
        {
            radTextBox_Barcode.Enabled = false;
            radTextBox_Barcode.Text = string.Empty;
            radLabel_spcName.Text = string.Empty;
            radRadioButton_scale.Enabled = false;
            radRadioButton_psc.Enabled = false;
            RadButton_Update.Enabled = false;
            RadButton_Cancel.Enabled = false;
            radCheckBox_Exp.Enabled = false;
            radCheckBox_SetscalMN.Enabled = false;
            radDropDownList_exp.Enabled = false;
            radDropDownList_exp.SelectedIndex = -1;
            radRadioButton_scale.IsChecked = false; radRadioButton_psc.IsChecked = false; radCheckBox_SetscalMN.Checked = false;
            if (radCheckBox_Exp.Checked == true) { radCheckBox_Exp.Checked = false; radDropDownList_exp.SelectedIndex = -1; }
        }

        ////แก้ไขการ insert sql 
        //string ExecuteTwoTransections(ArrayList strScale, string _connBranch, ArrayList strLog, string _connLog)
        //{
        //    string resultMessage = ""; //เสร็จสมบูรณ์ให้ return ค่าว่าง

        //    SqlConnection sqlConnectionBranch = new SqlConnection(_connBranch);
        //    sqlConnectionBranch.Open();

        //    SqlCommand sqlCommand1 = sqlConnectionBranch.CreateCommand();
        //    SqlTransaction transaction1 = sqlConnectionBranch.BeginTransaction("SampleTransaction");
        //    sqlCommand1.Connection = sqlConnectionBranch;
        //    sqlCommand1.Transaction = transaction1;

        //    SqlConnection sqlConnectionLog = new SqlConnection(_connLog);
        //    sqlConnectionLog.Open();

        //    SqlCommand sqlCommand2 = sqlConnectionLog.CreateCommand();
        //    SqlTransaction transaction2 = sqlConnectionLog.BeginTransaction("SampleTransaction");
        //    sqlCommand2.Connection = sqlConnectionLog;
        //    sqlCommand2.Transaction = transaction2;

        //    string commandText1 = string.Join(";", strScale.ToArray());
        //    string commandText2 = string.Join(";", strLog.ToArray());
        //    int rowaffected = 0;
        //    int rowaffected2 = 0;

        //    try
        //    {
        //        sqlCommand1.CommandText = commandText1;
        //        rowaffected += sqlCommand1.ExecuteNonQuery();

        //        sqlCommand2.CommandText = commandText2;
        //        rowaffected2 += sqlCommand2.ExecuteNonQuery();

        //        var result1 = rowaffected > 0;
        //        var result2 = rowaffected2 > 0;

        //        if ((transaction1 != null && result1) && (transaction2 != null && result2))
        //        {
        //            transaction1.Commit();
        //            transaction2.Commit();

        //        }
        //        else
        //        {
        //            resultMessage = "transactions หมดอายุหรือ เงื่อนไขไม่ถูกต้อง";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (transaction1 != null)
        //        {
        //            transaction1.Rollback("SampleTransaction");
        //        }

        //        if (transaction2 != null)
        //        {

        //            transaction2.Rollback("SampleTransaction");
        //        }
        //        return ex.Message;
        //    }

        //    finally
        //    {

        //        sqlConnectionBranch.Dispose();
        //        sqlConnectionLog.Dispose();
        //    }

        //    return resultMessage;
        //}

        #endregion

        #region Event
        //ส่งราคา
        private void RadButtonElement_send_Click(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            string ret = "";
            if (ScaleTable.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่มีข้อมูลส่งเข้าตาชั่งเช็คข้อมูลใหม่อีกครั้ง.");
                this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                return;
            }
            else
            {
                switch (_Typespc)
                {
                    case "SUPC":
                        ret = SendScaleSupc(ScaleTable, _Usespc);
                        break;
                    case "SHOP":
                        ret = SendScaleShop(ScaleTable);
                        break;
                    default:
                        break;
                }

                if (string.IsNullOrEmpty(ret))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("การโอนข้อมูลเสร็จสมบูรณ์" + Environment.NewLine + " ให้เปิดโปรแกรม Scale2006 ที่หน้าจอ Desktop เพื่อส่งราคาเข้าตาชั่ง.");
                    this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                    return;
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error(ret + Environment.NewLine);
                    this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                    return;
                }
            }

        }
        //ลบข้อมูล
        private void RadButtonElement_clear_Click(object sender, EventArgs e)
        {
            //ลบข้อมูล
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            if (_Typespc.Equals("SUPC"))
            {
                string message = string.Format(@"ยืนยันการลบข้อมูลของจัดซื้อ {0} ออกจากเครื่องชั่ง", radDropDownList_Purch.Text);
                if (MessageBox.Show(message, SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                    return;
                }

                //DeleteScaleTable = Scale2006Class.GetDataDelete(radDropDownList_Purch.SelectedValue.ToString());
                DeleteScaleTable = Models.ScaleClass.GetDataDelete(radDropDownList_Purch.SelectedValue.ToString());
            }
            else
            {

                string message = string.Format(@"ยืนยันการลบข้อมูลราคาในเครื่องชั่ง");
                if (MessageBox.Show(message, SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                    return;
                }
                //DeleteScaleTable = Scale2006Class.GetDataDelete();
                DeleteScaleTable = Models.ScaleClass.GetDataDelete();
            }

            string ret = DeleteScale(DeleteScaleTable);
            if (string.IsNullOrEmpty(ret))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("การโอนข้อมูลเสร็จสมบูรณ์ สามารถเข้าโปรแกรม Scale เพื่อลบข้อมูลราคาในเครื่องชั่งได้.");
                try
                {
                    string ExeScalePath = @"C:\Program Files\EMC\Scale2006\Scale2006.exe";
                    System.Diagnostics.Process.Start(ExeScalePath);
                }
                catch
                {
                    try
                    {
                        string ExeScalePath = @"C:\Program Files (x86)\EMC\Scale2006\Scale2006.exe";
                        System.Diagnostics.Process.Start(ExeScalePath);
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถเปิดโปรแกรม Scale2006 จากโปรแกรม Shop24Hrs. กรุณาเปิดโปรแกรม Scale2006 ที่มีอยู่หน้า destop. " + Environment.NewLine + Environment.NewLine + ex);
                        this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                        return;
                    }
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("การโอนข้อมูลไม่เสร็จสมบูรณ์ ลองใหม่อีกครั้ง " + Environment.NewLine + ret);
                this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
                return;
            }
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        //เพิ่ม-แก้ไขข้อมูล
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            update = StatusUpdate.insert.ToString();
            this.UpdatePanelInfo(null);
        }
        private void RadButtonElement_edit_Click(object sender, EventArgs e)
        {
            if (!(this.radGridView_Show.CurrentRow is null))
            {
                update = StatusUpdate.edit.ToString();
                UpdatePanelInfo(this.radGridView_Show.CurrentRow);
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกสินค้าก่อนแก้ไขทุกครั้ง");
            }
        }
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.GetDataScale();
        }
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                //ScaleItem item = Scale2006Class.Itembarcode(radTextBox_Barcode.Text);
                ScaleItem item = Models.ScaleClass.Itembarcode(radTextBox_Barcode.Text);
                if (item.ITEMBARCODE.Equals(""))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลบาร์โค้ดสินค้าที่ระบุ หรือบาร์โค้ดไม่ถูกระบุว่าเป็นเครื่องขั่ง เช็คบาร์โค้ดใหม่อีกครั้ง.");
                    ClearPanelInfo();
                    radTextBox_Barcode.Enabled = true;
                }
                else
                {
                    if (!item.SCALEMASTER.Equals("0") && !item.SCALESENDSHOP.Equals("0"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("บาร์โค้ดสินค้าสามารถชั่งได้อยู่แล้วไม่จำเป็นต้องเพิ่มใหม่");
                        ClearPanelInfo();
                        radTextBox_Barcode.Enabled = true;
                    }
                    else if (!item.SCALEMASTER.Equals("0") && item.SCALESENDSHOP.Equals("0"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("บาร์โค้ดสินค้าสามารถชั่งได้อยู่แล้วที่สาขาใหญ่ไม่จำเป็นต้องเพิ่มใหม่ หากต้องการกำหนดให้ชั่งที่มินิมาร์ทได้ สามารถเลือกสินค้าที่ต้องการจากนั้นกดแก้ไขสินค้า แล้วเลือกกำหนดชั่งที่มินิมาร์ท แล้วบันทึก");
                        ClearPanelInfo();
                        radTextBox_Barcode.Enabled = true;
                    }
                    else
                    {
                        radTextBox_Barcode.Text = item.ITEMBARCODE;
                        radLabel_spcName.Text = item.SPC_ITEMNAME;
                        radTextBox_Barcode.Enabled = false;
                        radRadioButton_scale.Enabled = true; radRadioButton_psc.Enabled = true;
                        radRadioButton_scale.IsChecked = true;
                        radCheckBox_Exp.Enabled = true; radCheckBox_SetscalMN.Enabled = true;
                        RadButton_Update.Enabled = true; RadButton_Cancel.Enabled = true;
                    }
                }
            }
        }

        private void RadButton_Update_Click(object sender, EventArgs e)
        {

        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        private void RadTextBox_Barcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;
        }
        private void RadButtonElement_delet_Click(object sender, EventArgs e)
        {
            if (!(radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value is null))
            {
                string ret;
                ArrayList array = new ArrayList();
                ArrayList array707 = new ArrayList();

                string message = string.Format(@"ข้อมูลสินค้า {0}-{1} เป็นของจัดซื้ออื่น ยืนยันการต้องการลบ."
                                , radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()
                                , radGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString());
                if (MessageBox.Show(message, SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

                string message1 = string.Format(@"ยืนยันการลบข้อมูล {0}-{1} ออกจากสินค้าชั่ง [จะไม่สามารถชั่งสินค้าได้ทั้งที่ซุปเปอร์ชีปและมินิมาร์ท]."
                                , radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()
                                , radGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString());
                if (MessageBox.Show(message1, SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

                array.Add(string.Format(@"DELETE FROM {0}Shop_scaleMaster WHERE ItemBarcode = {1}", "", radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()));
                array707.Add(string.Format(@"DELETE FROM Shop_scaleMaster WHERE ItemBarcode = {0}", radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()));

                array.Add(string.Format(@"DELETE FROM {0}Shop_scaleSend WHERE ItemBarcode = {1}", "", radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()));
                array707.Add(string.Format(@"DELETE FROM Shop_scaleSend WHERE ItemBarcode = {0}", radGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()));

                ret = ConnectionClass.Execute_SameTime_2Server(array, IpServerConnectClass.ConSelectMain, array707, ConnectionScale);
                MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                if (ret.Equals("")) this.GetDataScale();
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("เลือกสินค้าก่อนเท่านั้น.");
                return;
            }
        }
        private void RadCheckBox_Exp_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Exp.Checked == true)
            {
                radDropDownList_exp.Enabled = true;
                radDropDownList_exp.SelectedIndex = 0;
            }
            else
            {
                radDropDownList_exp.Enabled = false;
                radDropDownList_exp.SelectedIndex = -1;
            }
        }
        private void RadCheckBox_Purch_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Purch.Checked == true)
            {
                radDropDownList_Purch.Enabled = true;
                radDropDownList_Purch.SelectedIndex = 0;
            }
            else
            {
                radDropDownList_Purch.Enabled = false;
                radDropDownList_Purch.SelectedIndex = -1;
            }
        }
        #endregion

        #region ClassEdit
        string[] GetPriceGroupMN()
        {
            string[] Tmp;
            //string sql = @"SELECT [FieldID],[ColumnName],[Label],[ID],[TYPE_DATA] FROM [dbo].[Shop_Price] where [ID] like 'MN%' and LEN(ID) = 4 order by [ID] ";

            DataTable table = BranchClass.GetPriceLevel(" where [ID] like 'MN%' and LEN(ID) = 4 "); //Controllers.ConnectionClass.SelectSQL_Main(sql);
            int i = 0; Tmp = new string[table.Rows.Count];
            foreach (DataRow row in table.Rows)
            {
                Tmp[i] = row["ColumnName"].ToString();
                i++;
            }
            return Tmp;
        }
        string GetCatalogScal(ScaleCatl scale, string prc)
        {
            var catalog = from l in scale.List where l.PriceID == prc select l;
            foreach (var c in catalog)
            {
                prc = c.ScaleCat;
            }
            return prc;
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.ClearPanelInfo();
            //resetsatatus 
            if (update.Equals(StatusUpdate.edit.ToString()))
            {
                update = "";
            }
            else
            {
                radTextBox_Barcode.Enabled = true;
            }
        }

        private void RadButton_Update_Click_1(object sender, EventArgs e)
        {
            if (radTextBox_Barcode.Text.Equals(""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุข้อมูลบาร์โค้ดก่อนทำการกดบันทึกข้อมูล.");
                return;
            }
            string ret = "";
            //กำหนด SUPC

            if (radRadioButton_scale.IsChecked == false && radRadioButton_psc.IsChecked == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("เลือกประเภทการชั่งการก่อนบันทึกข้อมูล.");
                return;
            }
            ScaleClass Tmp = new ScaleClass() { ITEMBARCODE = radTextBox_Barcode.Text, SPC_ITEMNAME = radLabel_spcName.Text };
            if (radRadioButton_scale.IsChecked == true)
            {
                Tmp.TYPE_SCALE = "1";
                Tmp.QTYSYMBOL = "";
            }
            else if (radRadioButton_psc.IsChecked == true)
            {
                Tmp.TYPE_SCALE = "2";
                Tmp.QTYSYMBOL = "Pcs";
            }
            if (radCheckBox_Exp.Checked == true)
            {
                Tmp.EXPDAYPRINT = "1";
                Tmp.EXPDAY = radDropDownList_exp.SelectedIndex + 1;
            }
            //กำหนดให้ชั่งที่มินิมาร์ท
            if (radCheckBox_SetscalMN.Checked == true)
            {
                Tmp.TYPESENDSCALESHOP = "SHOP";
            }

            if (update.Equals(StatusUpdate.edit.ToString()))
            {
                ret = UpdateBarcodeToScaleMaster(Tmp, ConnectionScale, IpServerConnectClass.ConSelectMain);
            }
            else if (update.Equals(StatusUpdate.insert.ToString()))
            {
                ret = InsertBarcodeToScaleMaster(Tmp, ConnectionScale, IpServerConnectClass.ConSelectMain);
            }

            if (ret.Equals(""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกข้อมูลเรียบร้อย.");
                this.GetDataScale();
                ClearPanelInfo();
                return;
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง." + ret);
                return;
            }
        }

        string UpdateBarcodeToScaleMaster(ScaleClass Item, string ConnectionScale, string ConnectionMain)
        {
            ArrayList array = new ArrayList();
            ArrayList array707 = new ArrayList();

            string Str = string.Format(@"
                    Update {5}Shop_ScaleMaster 
                    Set TYPE_SCALE = '{0}',
                        QTYSYMBOL = '{1}',
                        EXPDAY = '{2}',
                        EXPDAYPRINT = '{3}' 
                    WHERE ITEMBARCODE = '{4}'"
                    , Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT, Item.ITEMBARCODE, "");

            string Str707 = string.Format(@"
                    Update Shop_ScaleMaster 
                    Set TYPE_SCALE = '{0}',
                        QTYSYMBOL = '{1}',
                        EXPDAY = '{2}',
                        EXPDAYPRINT = '{3}' 
                    WHERE ITEMBARCODE = '{4}'"
                    , Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT, Item.ITEMBARCODE);

            array.Add(Str);
            array707.Add(Str707);

            //insert scalesend
            if (!Item.TYPESENDSCALESHOP.Equals("") && !CheckItemsendShop(Item.ITEMBARCODE).Equals(Item.ITEMBARCODE))
            {
                array.Add(string.Format(@"
                            INSERT INTO {2}Shop_scaleSend 
                                    (ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
                            VALUES 
                                    ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
                            , Item.ITEMBARCODE
                            , Controllers.SystemClass.SystemUserID_M
                            , ""));

                array707.Add(string.Format(@"
                            INSERT INTO Shop_scaleSend
                                    (ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
                            VALUES 
                                    ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
                            , Item.ITEMBARCODE
                            , Controllers.SystemClass.SystemUserID_M));
            }
            else if (Item.TYPESENDSCALESHOP.Equals("") && CheckItemsendShop(Item.ITEMBARCODE).Equals(Item.ITEMBARCODE))
            {
                array.Add(string.Format(@"DELETE FROM {0}Shop_scaleSend WHERE ItemBarcode = {1}", "", Item.ITEMBARCODE));
                array707.Add(string.Format(@"DELETE FROM Shop_scaleSend WHERE ItemBarcode = {0}", Item.ITEMBARCODE));
            }

            return ConnectionClass.Execute_SameTime_2Server(array707, ConnectionScale, array, ConnectionMain);
        }
        string InsertBarcodeToScaleMaster(ScaleClass Item, string ConnectionScale, string ConnectionMain)
        {
            ArrayList array = new ArrayList();
            ArrayList array707 = new ArrayList();
            string Str = string.Format(@"
                    INSERT INTO {6}Shop_ScaleMaster(ITEMBARCODE,SKU,BARCODE,TYPE_SCALE,QTYSYMBOL,EXPDAY,EXPDAYPRINT) 
                    VALUES ('{0}','{1}','2 {1}','{2}','{3}','{4}','{5}')"
                    , Item.ITEMBARCODE, Item.ITEMBARCODE.Substring(1, 6), Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT, "");

            string Str707 = string.Format(@"
                    INSERT INTO Shop_ScaleMaster(ITEMBARCODE,SKU,BARCODE,TYPE_SCALE,QTYSYMBOL,EXPDAY,EXPDAYPRINT) 
                    VALUES ('{0}','{1}','2 {1}','{2}','{3}','{4}','{5}')"
                    , Item.ITEMBARCODE, Item.ITEMBARCODE.Substring(1, 6), Item.TYPE_SCALE, Item.QTYSYMBOL, Item.EXPDAY, Item.EXPDAYPRINT);
            array.Add(Str);
            array707.Add(Str707);

            if (!Item.TYPESENDSCALESHOP.Equals(""))
            {
                array.Add(string.Format(@"
                            INSERT INTO {2}Shop_scaleSend(ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
                            VALUES 
                                    ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
                            , Item.ITEMBARCODE
                            , Controllers.SystemClass.SystemUserID_M
                            , ""));

                array707.Add(string.Format(@"
                            INSERT INTO Shop_scaleSend(ItemBarcode,TypeSend,DateIn,TimeIn,WhoIn) 
                            VALUES 
                                    ('{0}','SHOP',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{1}')"
                            , Item.ITEMBARCODE
                            , Controllers.SystemClass.SystemUserID_M));
            }
            return Controllers.ConnectionClass.Execute_SameTime_2Server(array707, ConnectionScale, array, ConnectionMain);
        }
        private static string CheckItemsendShop(string item)
        {
            //ScaleItem tmp = Scale2006Class.Itembarcode(item);
            ScaleItem tmp = Models.ScaleClass.Itembarcode(item);
            return tmp.SCALESENDSHOP;
        }
        #endregion

        #region ClassVar
        public class ScaleClass
        {
            public string TYPE_SCALE { get; set; } = "";
            public string ITEMBARCODE { get; set; } = "";
            public string SPC_ITEMNAME { get; set; } = "";
            public string UNITID { get; set; } = "";
            public string QTYSYMBOL { get; set; } = "";
            public double WEIGHT { get; set; }
            public string EXPDAYPRINT { get; set; } = "0";
            public int EXPDAY { get; set; } = 0;
            public double PRICE { get; set; } = 0.0;
            public string TYPESENDSCALESHOP { get; set; } = "";
        }
        public class ScaleItem
        {
            public string ITEMBARCODE { get; set; } = "";
            public string SPC_ITEMNAME { get; set; } = "";
            public string SCALEMASTER { get; set; } = "";
            public string SCALESENDSHOP { get; set; } = "";
        }
        public class ScaleCatl
        {

            private readonly string _SPC_PriceGroup13 = "ScaleMNPrice1";
            private readonly string _SPC_PriceGroup14 = "ScaleMNPrice2";
            private readonly string _SPC_PriceGroup15 = "ScaleMNPrice3";
            private readonly string _SPC_PriceGroup17 = "ScaleMNPrice4";
            private readonly string _SPC_PriceGroup18 = "ScaleMNPrice5";
            private readonly string _SPC_PriceGroup19 = "ScaleMNPrice6";
            public List<ScalePriceGroup> List { get; set; }
            public ScaleCatl(string[] str)
            {
                List = new List<ScalePriceGroup>() {
                new ScalePriceGroup(){ PriceID = str[0], ScaleCat = _SPC_PriceGroup13.ToString()},
                new ScalePriceGroup(){ PriceID = str[1], ScaleCat = _SPC_PriceGroup14.ToString()},
                new ScalePriceGroup(){ PriceID = str[2], ScaleCat = _SPC_PriceGroup15.ToString()},
                new ScalePriceGroup(){ PriceID = str[3], ScaleCat = _SPC_PriceGroup17.ToString()},
                new ScalePriceGroup(){ PriceID = str[4], ScaleCat = _SPC_PriceGroup18.ToString()},
                new ScalePriceGroup(){ PriceID = str[5], ScaleCat = _SPC_PriceGroup19.ToString()}};
        }
    }
    public class ScalePriceGroup
    {
        public string PriceID { get; set; }
        public string ScaleCat { get; set; }
    }
    #endregion
}
}