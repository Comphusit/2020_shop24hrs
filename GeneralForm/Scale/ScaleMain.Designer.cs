﻿namespace PC_Shop24Hrs.GeneralForm.Scale 
{
    partial class ScaleMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScaleMain));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_delet = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_send = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_clear = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radGroupBox_Item = new Telerik.WinControls.UI.RadGroupBox();
            this.RadButton_Update = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox_SetscalMN = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_exp = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_spcName = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_Exp = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox_weigh = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton_psc = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_scale = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel_Barcode = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_Purch = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_Purch = new Telerik.WinControls.UI.RadDropDownList();
            this.radCheckBox_Price = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_Price = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton_Search = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Item)).BeginInit();
            this.radGroupBox_Item.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_SetscalMN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_exp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_spcName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Exp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_weigh)).BeginInit();
            this.radGroupBox_weigh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_psc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_scale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Purch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Purch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_F2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radGridView_Show, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 606);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(617, 19);
            this.radLabel_F2.TabIndex = 53;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_add,
            this.commandBarSeparator2,
            this.radButtonElement_edit,
            this.commandBarSeparator3,
            this.radButtonElement_delet,
            this.commandBarSeparator4,
            this.radButtonElement_send,
            this.commandBarSeparator5,
            this.radButtonElement_clear,
            this.commandBarSeparator1,
            this.RadButtonElement_pdt,
            this.commandBarSeparator6});
            this.radStatusStrip1.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(617, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "Export To Excel";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_edit
            // 
            this.radButtonElement_edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_edit.Name = "radButtonElement_edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_edit, false);
            this.radButtonElement_edit.Text = "radButtonElement1";
            this.radButtonElement_edit.Click += new System.EventHandler(this.RadButtonElement_edit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_delet
            // 
            this.radButtonElement_delet.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_delet.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButtonElement_delet.Name = "radButtonElement_delet";
            this.radStatusStrip1.SetSpring(this.radButtonElement_delet, false);
            this.radButtonElement_delet.Text = "radButtonElement1";
            this.radButtonElement_delet.Click += new System.EventHandler(this.RadButtonElement_delet_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_send
            // 
            this.radButtonElement_send.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_send.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButtonElement_send.Name = "radButtonElement_send";
            this.radStatusStrip1.SetSpring(this.radButtonElement_send, false);
            this.radButtonElement_send.Text = "radButtonElement1";
            this.radButtonElement_send.UseCompatibleTextRendering = false;
            this.radButtonElement_send.Click += new System.EventHandler(this.RadButtonElement_send_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_clear
            // 
            this.radButtonElement_clear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_clear.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.radButtonElement_clear.Name = "radButtonElement_clear";
            this.radStatusStrip1.SetSpring(this.radButtonElement_clear, false);
            this.radButtonElement_clear.Text = "radButtonElement1";
            this.radButtonElement_clear.Click += new System.EventHandler(this.RadButtonElement_clear_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 45);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(617, 555);
            this.radGridView_Show.TabIndex = 16;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radGroupBox_Item);
            this.panel1.Controls.Add(this.radCheckBox_Purch);
            this.panel1.Controls.Add(this.radDropDownList_Purch);
            this.panel1.Controls.Add(this.radCheckBox_Price);
            this.panel1.Controls.Add(this.radDropDownList_Price);
            this.panel1.Controls.Add(this.radButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 1;
            // 
            // radGroupBox_Item
            // 
            this.radGroupBox_Item.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Item.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_Item.Controls.Add(this.RadButton_Update);
            this.radGroupBox_Item.Controls.Add(this.RadButton_Cancel);
            this.radGroupBox_Item.Controls.Add(this.radCheckBox_SetscalMN);
            this.radGroupBox_Item.Controls.Add(this.radDropDownList_exp);
            this.radGroupBox_Item.Controls.Add(this.radLabel_spcName);
            this.radGroupBox_Item.Controls.Add(this.radCheckBox_Exp);
            this.radGroupBox_Item.Controls.Add(this.radGroupBox_weigh);
            this.radGroupBox_Item.Controls.Add(this.radLabel_Barcode);
            this.radGroupBox_Item.Controls.Add(this.radTextBox_Barcode);
            this.radGroupBox_Item.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_Item.HeaderText = "ข้อมูลสินค้า";
            this.radGroupBox_Item.Location = new System.Drawing.Point(0, 182);
            this.radGroupBox_Item.Name = "radGroupBox_Item";
            this.radGroupBox_Item.Size = new System.Drawing.Size(194, 446);
            this.radGroupBox_Item.TabIndex = 0;
            this.radGroupBox_Item.Text = "ข้อมูลสินค้า";
            // 
            // RadButton_Update
            // 
            this.RadButton_Update.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Update.Location = new System.Drawing.Point(8, 337);
            this.RadButton_Update.Name = "RadButton_Update";
            this.RadButton_Update.Size = new System.Drawing.Size(85, 32);
            this.RadButton_Update.TabIndex = 72;
            this.RadButton_Update.Text = "บันทึก";
            this.RadButton_Update.ThemeName = "Fluent";
            this.RadButton_Update.Click += new System.EventHandler(this.RadButton_Update_Click_1);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Update.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Update.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Update.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Update.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Update.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Update.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Update.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Update.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(98, 337);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(87, 32);
            this.RadButton_Cancel.TabIndex = 73;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radCheckBox_SetscalMN
            // 
            this.radCheckBox_SetscalMN.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_SetscalMN.Location = new System.Drawing.Point(11, 227);
            this.radCheckBox_SetscalMN.Name = "radCheckBox_SetscalMN";
            this.radCheckBox_SetscalMN.Size = new System.Drawing.Size(143, 19);
            this.radCheckBox_SetscalMN.TabIndex = 71;
            this.radCheckBox_SetscalMN.Text = "กำหนดชั่งที่มินิมาร์ท";
            // 
            // radDropDownList_exp
            // 
            this.radDropDownList_exp.DropDownAnimationEnabled = false;
            this.radDropDownList_exp.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_exp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_exp.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_exp.Location = new System.Drawing.Point(10, 298);
            this.radDropDownList_exp.Name = "radDropDownList_exp";
            this.radDropDownList_exp.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_exp.TabIndex = 70;
            // 
            // radLabel_spcName
            // 
            this.radLabel_spcName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_spcName.Location = new System.Drawing.Point(11, 79);
            this.radLabel_spcName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_spcName.Name = "radLabel_spcName";
            this.radLabel_spcName.Size = new System.Drawing.Size(54, 19);
            this.radLabel_spcName.TabIndex = 69;
            this.radLabel_spcName.Text = "ชื่อสินค้า";
            // 
            // radCheckBox_Exp
            // 
            this.radCheckBox_Exp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Exp.Location = new System.Drawing.Point(11, 262);
            this.radCheckBox_Exp.Name = "radCheckBox_Exp";
            this.radCheckBox_Exp.Size = new System.Drawing.Size(120, 19);
            this.radCheckBox_Exp.TabIndex = 67;
            this.radCheckBox_Exp.Text = "พิมพ์วันหมดอายุ";
            this.radCheckBox_Exp.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Exp_CheckStateChanged);
            // 
            // radGroupBox_weigh
            // 
            this.radGroupBox_weigh.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_weigh.Controls.Add(this.radRadioButton_psc);
            this.radGroupBox_weigh.Controls.Add(this.radRadioButton_scale);
            this.radGroupBox_weigh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_weigh.HeaderText = "ประเภทการชั่ง";
            this.radGroupBox_weigh.Location = new System.Drawing.Point(10, 112);
            this.radGroupBox_weigh.Name = "radGroupBox_weigh";
            this.radGroupBox_weigh.Size = new System.Drawing.Size(175, 96);
            this.radGroupBox_weigh.TabIndex = 11;
            this.radGroupBox_weigh.Text = "ประเภทการชั่ง";
            // 
            // radRadioButton_psc
            // 
            this.radRadioButton_psc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_psc.Location = new System.Drawing.Point(17, 56);
            this.radRadioButton_psc.Name = "radRadioButton_psc";
            this.radRadioButton_psc.Size = new System.Drawing.Size(121, 19);
            this.radRadioButton_psc.TabIndex = 1;
            this.radRadioButton_psc.Text = "หน่วยนับขาย[Psc]";
            // 
            // radRadioButton_scale
            // 
            this.radRadioButton_scale.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_scale.Location = new System.Drawing.Point(17, 29);
            this.radRadioButton_scale.Name = "radRadioButton_scale";
            this.radRadioButton_scale.Size = new System.Drawing.Size(107, 19);
            this.radRadioButton_scale.TabIndex = 0;
            this.radRadioButton_scale.Text = "สินค้าชั่งน้ำหนัก";
            // 
            // radLabel_Barcode
            // 
            this.radLabel_Barcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Barcode.Location = new System.Drawing.Point(10, 21);
            this.radLabel_Barcode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radLabel_Barcode.Name = "radLabel_Barcode";
            this.radLabel_Barcode.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Barcode.TabIndex = 13;
            this.radLabel_Barcode.Text = "บาร์โค้ด";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Barcode.Location = new System.Drawing.Point(10, 45);
            this.radTextBox_Barcode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radTextBox_Barcode.MaxLength = 50;
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_Barcode.TabIndex = 12;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            this.radTextBox_Barcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Barcode_KeyPress);
            // 
            // radCheckBox_Purch
            // 
            this.radCheckBox_Purch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Purch.Location = new System.Drawing.Point(11, 67);
            this.radCheckBox_Purch.Name = "radCheckBox_Purch";
            this.radCheckBox_Purch.Size = new System.Drawing.Size(80, 19);
            this.radCheckBox_Purch.TabIndex = 66;
            this.radCheckBox_Purch.Text = "ระบุจัดซื้อ";
            this.radCheckBox_Purch.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Purch_CheckStateChanged);
            // 
            // radDropDownList_Purch
            // 
            this.radDropDownList_Purch.DropDownAnimationEnabled = false;
            this.radDropDownList_Purch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Purch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Purch.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Purch.Location = new System.Drawing.Point(10, 92);
            this.radDropDownList_Purch.Name = "radDropDownList_Purch";
            this.radDropDownList_Purch.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Purch.TabIndex = 65;
            // 
            // radCheckBox_Price
            // 
            this.radCheckBox_Price.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Price.Location = new System.Drawing.Point(11, 7);
            this.radCheckBox_Price.Name = "radCheckBox_Price";
            this.radCheckBox_Price.Size = new System.Drawing.Size(106, 19);
            this.radCheckBox_Price.TabIndex = 62;
            this.radCheckBox_Price.Text = "ระบุระดับราคา";
            // 
            // radDropDownList_Price
            // 
            this.radDropDownList_Price.DropDownAnimationEnabled = false;
            this.radDropDownList_Price.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Price.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Price.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Price.Location = new System.Drawing.Point(10, 32);
            this.radDropDownList_Price.Name = "radDropDownList_Price";
            this.radDropDownList_Price.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Price.TabIndex = 61;
            // 
            // radButton_Search
            // 
            this.radButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Search.Location = new System.Drawing.Point(11, 131);
            this.radButton_Search.Name = "radButton_Search";
            this.radButton_Search.Size = new System.Drawing.Size(175, 32);
            this.radButton_Search.TabIndex = 0;
            this.radButton_Search.Text = "ค้นหา";
            this.radButton_Search.ThemeName = "Fluent";
            this.radButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // ScaleMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ScaleMain";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ScaleShopCommit.";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ScaleMain_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Item)).EndInit();
            this.radGroupBox_Item.ResumeLayout(false);
            this.radGroupBox_Item.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_SetscalMN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_exp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_spcName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Exp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_weigh)).EndInit();
            this.radGroupBox_weigh.ResumeLayout(false);
            this.radGroupBox_weigh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_psc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_scale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Purch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Purch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Purch;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Purch;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Price;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Price;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_send;
        protected Telerik.WinControls.UI.RadButton radButton_Search;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_delet;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Item;
        private Telerik.WinControls.UI.RadLabel radLabel_Barcode;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_weigh;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_psc;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Exp;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_scale;
        private Telerik.WinControls.UI.RadLabel radLabel_spcName;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_exp;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_clear;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_SetscalMN;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        protected Telerik.WinControls.UI.RadButton RadButton_Update;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
    }
}
