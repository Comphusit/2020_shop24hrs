﻿namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    partial class ShelfEmpl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShelfEmpl));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.RadButtonElement_CheckAll = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdf = new Telerik.WinControls.UI.RadImageButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_WorkUser = new Telerik.WinControls.UI.RadButtonElement();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_ShelfOrEmpl = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RadCheckBox_EmplHR = new Telerik.WinControls.UI.RadCheckBox();
            this.RadCheckBox_EmplShelf = new Telerik.WinControls.UI.RadCheckBox();
            this.groupBox_Condition = new System.Windows.Forms.GroupBox();
            this.radioButton_Empl = new System.Windows.Forms.RadioButton();
            this.radioButton_Shelf = new System.Windows.Forms.RadioButton();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList_Month = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Year = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Group = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Zone = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShelfOrEmpl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShelfOrEmpl.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_EmplHR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_EmplShelf)).BeginInit();
            this.groupBox_Condition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Month)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Year)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Group)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(473, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radPanel1
            // 
            this.radPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel1.Controls.Add(this.radStatusStrip1);
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(3, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(467, 42);
            this.radPanel1.TabIndex = 4;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.RadButtonElement_CheckAll,
            this.commandBarSeparator2,
            this.RadButtonElement_pdf,
            this.commandBarSeparator4,
            this.RadButtonElement_WorkUser});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(467, 42);
            this.radStatusStrip1.TabIndex = 8;
            // 
            // RadButtonElement_CheckAll
            // 
            this.RadButtonElement_CheckAll.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_CheckAll.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.RadButtonElement_CheckAll.Name = "RadButtonElement_CheckAll";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_CheckAll, false);
            this.RadButtonElement_CheckAll.Text = "radButtonElement1";
            this.RadButtonElement_CheckAll.ToolTipText = "เพิ่ม";
            this.RadButtonElement_CheckAll.UseCompatibleTextRendering = false;
            this.RadButtonElement_CheckAll.Click += new System.EventHandler(this.RadButtonElement_CheckAll_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdf
            // 
            this.RadButtonElement_pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdf.Name = "RadButtonElement_pdf";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdf, false);
            this.RadButtonElement_pdf.Text = "radImageButtonElement1";
            this.RadButtonElement_pdf.Click += new System.EventHandler(this.RadButtonElement_pdf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_WorkUser
            // 
            this.RadButtonElement_WorkUser.Image = global::PC_Shop24Hrs.Properties.Resources.add_user;
            this.RadButtonElement_WorkUser.Name = "RadButtonElement_WorkUser";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_WorkUser, false);
            this.RadButtonElement_WorkUser.Text = "";
            this.RadButtonElement_WorkUser.Click += new System.EventHandler(this.RadButtonElement_WorkUser_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RadGridView_Show);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(467, 574);
            this.panel2.TabIndex = 5;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(467, 574);
            this.RadGridView_Show.TabIndex = 1;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.RadGridView_ShelfOrEmpl, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(482, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(344, 628);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // RadGridView_ShelfOrEmpl
            // 
            this.RadGridView_ShelfOrEmpl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShelfOrEmpl.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShelfOrEmpl.Location = new System.Drawing.Point(3, 203);
            // 
            // 
            // 
            this.RadGridView_ShelfOrEmpl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_ShelfOrEmpl.Name = "RadGridView_ShelfOrEmpl";
            // 
            // 
            // 
            this.RadGridView_ShelfOrEmpl.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShelfOrEmpl.Size = new System.Drawing.Size(338, 422);
            this.RadGridView_ShelfOrEmpl.TabIndex = 8;
            this.RadGridView_ShelfOrEmpl.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Barcode_ViewCellFormatting);
            this.RadGridView_ShelfOrEmpl.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShelfOrEmpl_CellClick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox_Condition);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.radDropDownList_Month);
            this.panel1.Controls.Add(this.radDropDownList_Year);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_Group);
            this.panel1.Controls.Add(this.radDropDownList_Zone);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(338, 194);
            this.panel1.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RadCheckBox_EmplHR);
            this.groupBox1.Controls.Add(this.RadCheckBox_EmplShelf);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(190, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(143, 86);
            this.groupBox1.TabIndex = 81;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ตั้งค่า";
            // 
            // RadCheckBox_EmplHR
            // 
            this.RadCheckBox_EmplHR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RadCheckBox_EmplHR.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.RadCheckBox_EmplHR.Location = new System.Drawing.Point(6, 47);
            this.RadCheckBox_EmplHR.Name = "RadCheckBox_EmplHR";
            this.RadCheckBox_EmplHR.Size = new System.Drawing.Size(120, 19);
            this.RadCheckBox_EmplHR.TabIndex = 1;
            this.RadCheckBox_EmplHR.Text = "พนักงานในระบบ";
            this.RadCheckBox_EmplHR.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.RadCheckBox_EmplHR.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_EmplHR_ToggleStateChanged);
            // 
            // RadCheckBox_EmplShelf
            // 
            this.RadCheckBox_EmplShelf.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RadCheckBox_EmplShelf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.RadCheckBox_EmplShelf.Location = new System.Drawing.Point(6, 22);
            this.RadCheckBox_EmplShelf.Name = "RadCheckBox_EmplShelf";
            this.RadCheckBox_EmplShelf.Size = new System.Drawing.Size(100, 19);
            this.RadCheckBox_EmplShelf.TabIndex = 0;
            this.RadCheckBox_EmplShelf.Text = "พนักงานดูแล";
            this.RadCheckBox_EmplShelf.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.RadCheckBox_EmplShelf.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_EmplShelf_ToggleStateChanged);
            // 
            // groupBox_Condition
            // 
            this.groupBox_Condition.Controls.Add(this.radioButton_Empl);
            this.groupBox_Condition.Controls.Add(this.radioButton_Shelf);
            this.groupBox_Condition.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox_Condition.Location = new System.Drawing.Point(188, 97);
            this.groupBox_Condition.Name = "groupBox_Condition";
            this.groupBox_Condition.Size = new System.Drawing.Size(143, 86);
            this.groupBox_Condition.TabIndex = 80;
            this.groupBox_Condition.TabStop = false;
            this.groupBox_Condition.Text = "เงื่อนไข";
            // 
            // radioButton_Empl
            // 
            this.radioButton_Empl.AutoSize = true;
            this.radioButton_Empl.Checked = true;
            this.radioButton_Empl.Location = new System.Drawing.Point(8, 23);
            this.radioButton_Empl.Name = "radioButton_Empl";
            this.radioButton_Empl.Size = new System.Drawing.Size(77, 20);
            this.radioButton_Empl.TabIndex = 3;
            this.radioButton_Empl.TabStop = true;
            this.radioButton_Empl.Text = "พนักงาน";
            this.radioButton_Empl.UseVisualStyleBackColor = true;
            this.radioButton_Empl.CheckedChanged += new System.EventHandler(this.RadioButton_Empl_CheckedChanged);
            // 
            // radioButton_Shelf
            // 
            this.radioButton_Shelf.AutoSize = true;
            this.radioButton_Shelf.Location = new System.Drawing.Point(8, 49);
            this.radioButton_Shelf.Name = "radioButton_Shelf";
            this.radioButton_Shelf.Size = new System.Drawing.Size(66, 20);
            this.radioButton_Shelf.TabIndex = 2;
            this.radioButton_Shelf.Text = "ชั้นวาง";
            this.radioButton_Shelf.UseVisualStyleBackColor = true;
            this.radioButton_Shelf.CheckedChanged += new System.EventHandler(this.RadioButton_Shelf_CheckedChanged);
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(9, 152);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 31;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radDropDownList_Month
            // 
            this.radDropDownList_Month.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Month.DropDownHeight = 124;
            this.radDropDownList_Month.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_Month.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Month.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDropDownList_Month.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Month.Location = new System.Drawing.Point(111, 119);
            this.radDropDownList_Month.Name = "radDropDownList_Month";
            this.radDropDownList_Month.Size = new System.Drawing.Size(74, 21);
            this.radDropDownList_Month.TabIndex = 77;
            this.radDropDownList_Month.Text = "01";
            this.radDropDownList_Month.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Month_SelectedValueChanged);
            // 
            // radDropDownList_Year
            // 
            this.radDropDownList_Year.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Year.DropDownHeight = 124;
            this.radDropDownList_Year.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_Year.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Year.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDropDownList_Year.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Year.Location = new System.Drawing.Point(9, 119);
            this.radDropDownList_Year.Name = "radDropDownList_Year";
            this.radDropDownList_Year.Size = new System.Drawing.Size(77, 21);
            this.radDropDownList_Year.TabIndex = 76;
            this.radDropDownList_Year.Text = "2020";
            this.radDropDownList_Year.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Year_SelectedValueChanged);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(92, 121);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(13, 19);
            this.radLabel4.TabIndex = 74;
            this.radLabel4.Text = "-";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(10, 96);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(16, 19);
            this.radLabel3.TabIndex = 75;
            this.radLabel3.Text = "ปี";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(111, 96);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(38, 19);
            this.radLabel2.TabIndex = 73;
            this.radLabel2.Text = "เดือน";
            // 
            // radLabel_Group
            // 
            this.radLabel_Group.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Group.Location = new System.Drawing.Point(9, 49);
            this.radLabel_Group.Name = "radLabel_Group";
            this.radLabel_Group.Size = new System.Drawing.Size(33, 19);
            this.radLabel_Group.TabIndex = 71;
            this.radLabel_Group.Text = "โซน";
            // 
            // radDropDownList_Zone
            // 
            this.radDropDownList_Zone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Zone.DropDownHeight = 124;
            this.radDropDownList_Zone.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_Zone.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_Zone.Location = new System.Drawing.Point(9, 70);
            this.radDropDownList_Zone.Name = "radDropDownList_Zone";
            this.radDropDownList_Zone.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Zone.TabIndex = 70;
            this.radDropDownList_Zone.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Zone_SelectedValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(9, 5);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(39, 19);
            this.radLabel1.TabIndex = 69;
            this.radLabel1.Text = "สาขา";
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_Branch.DropDownHeight = 124;
            this.RadDropDownList_Branch.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(9, 24);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Branch.TabIndex = 68;
            this.RadDropDownList_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // ShelfEmpl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShelfEmpl";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "จัดการผู้ดูแลชั้นวางสินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ShelfItems_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShelfOrEmpl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShelfOrEmpl)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_EmplHR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_EmplShelf)).EndInit();
            this.groupBox_Condition.ResumeLayout(false);
            this.groupBox_Condition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Month)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Year)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Group)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        public Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShelfOrEmpl;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Year;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Month;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private System.Windows.Forms.GroupBox groupBox_Condition;
        private System.Windows.Forms.RadioButton radioButton_Empl;
        private System.Windows.Forms.RadioButton radioButton_Shelf;
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_CheckAll;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadImageButtonElement RadButtonElement_pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_EmplShelf;
        private Telerik.WinControls.UI.RadLabel radLabel_Group;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_Zone;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_EmplHR;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_WorkUser;
    }
}
