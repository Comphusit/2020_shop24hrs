﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfItems_Manage : Telerik.WinControls.UI.RadForm
    {
        String name;

        readonly DataTable dt_Data = new DataTable();
        readonly DataTable dtExcel = new DataTable();
        readonly DataTable dtNameExcel = new DataTable();
        // readonly string _pTypeReport;

        // 4 - Import ชั้นวาง
        //Load
        public ShelfItems_Manage()
        {
            InitializeComponent();
        }
        //Load
        private void ShelfItems_Manage_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_pdf.ShowBorder = true;
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Save.ButtonElement.ToolTipText = "บันทึก";

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(radGridView_SheetName);


            dtExcel.Columns.Add("BRANCH_ID");
            dtExcel.Columns.Add("BRANCH_NAME");
            dtExcel.Columns.Add("ITEMBARCODE");
            dtExcel.Columns.Add("SPC_ITEMNAME");
            dtExcel.Columns.Add("ZONE_ID");
            dtExcel.Columns.Add("ZONE_NAME");
            dtExcel.Columns.Add("SHELF_ID");
            dtExcel.Columns.Add("SHELF_NAME");
            dtExcel.Columns.Add("STASAVE");

            dtNameExcel.Columns.Add("NameSheet");

            radLabel_Detail.Text = "";
            radLabel_Detail.Visible = true;


            radBrowseEditor_choose.Visible = true;
            RadButton_Save.Visible = true;

            radLabel_Detail.Text = "ระบุ File Excel >> เลือกแผ่นงาน กดบันทึก | สีม่วง >> สินค้ามีชั้นวางแล้ว | สีแดง ช่องโซน >> ไม่มีชื่อโซนที่ระบุ | สีแดง ช่องชั้นวาง >> ไม่มีชื่อชั้นวางที่ระบุ";

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_ITEMNAME", "ชื่อสินค้า")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "โซน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "ชั้นวาง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STASAVE", "STASAVE")));

            radGridView_SheetName.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NameSheet", "ชื่อแผ่นงาน", 200)));
            radGridView_SheetName.DataSource = dtExcel;

            RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_ShowHD.MasterTemplate.Columns["ITEMBARCODE"].IsPinned = true;
            RadGridView_ShowHD.MasterTemplate.Columns["SPC_ITEMNAME"].IsPinned = true;

            ExpressionFormattingObject o1_1 = new ExpressionFormattingObject("MyCondition1", "ZONE_NAME = 'ไม่พบข้อมูล' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["ZONE_NAME"].ConditionalFormattingObjectList.Add(o1_1);
            RadGridView_ShowHD.Columns["ZONE_ID"].ConditionalFormattingObjectList.Add(o1_1);

            ExpressionFormattingObject o2_1 = new ExpressionFormattingObject("MyCondition1", "SHELF_NAME = 'ไม่พบข้อมูล' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["SHELF_ID"].ConditionalFormattingObjectList.Add(o2_1);
            RadGridView_ShowHD.Columns["SHELF_NAME"].ConditionalFormattingObjectList.Add(o2_1);

            ExpressionFormattingObject o_1 = new ExpressionFormattingObject("MyCondition1", "STASAVE = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_PurplePastel() };
            RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(o_1);
            RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(o_1);

            ClearTxt();
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            name = "";
            RadButton_Save.Enabled = false;
            if (dtExcel.Rows.Count > 0) { dtExcel.Rows.Clear(); dtExcel.AcceptChanges(); }
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            if (dtNameExcel.Rows.Count > 0) { dtNameExcel.Rows.Clear(); dtNameExcel.AcceptChanges(); }
            radBrowseEditor_choose.Enabled = true;
            if (!(radBrowseEditor_choose is null))
            { radBrowseEditor_choose.Value = null; }
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

        }
        //Document
        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }


        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            //Check ค่าว่างใน File ที่เลือก
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) { return; }
            if (dtNameExcel.Rows.Count > 0) { dtNameExcel.Rows.Clear(); }
            this.Cursor = Cursors.WaitCursor;
            //Check File Excel
            if ((radBrowseEditor_choose.Value.Contains(".xls")) == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("File ที่ระบุไม่ใช่ File Excel เช็คใหม่อีกครั้ง. " + Environment.NewLine +
                    "[" + radBrowseEditor_choose.Value + @"]");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook excelBook = xlApp.Workbooks.Open(radBrowseEditor_choose.Value);

            String[] excelSheets = new String[excelBook.Worksheets.Count];
            int i = 0;
            foreach (Microsoft.Office.Interop.Excel.Worksheet wSheet in excelBook.Worksheets)
            {
                excelSheets[i] = wSheet.Name;
                dtNameExcel.Rows.Add(wSheet.Name);
                i++;
            }

            excelBook.Close(radBrowseEditor_choose.Value);
            radGridView_SheetName.DataSource = dtNameExcel;
            dtNameExcel.AcceptChanges();
            this.radGridView_SheetName.Rows[0].IsSelected = true;
            this.Cursor = Cursors.Default;
        }

        private void RadGridView_SheetName_SelectionChanged(object sender, EventArgs e)
        {
            if (radGridView_SheetName.Rows.Count == 0) { return; }

            try
            {
                if (name == radGridView_SheetName.CurrentRow.Cells["NameSheet"].Value.ToString())
                { return; }
                else
                { name = radGridView_SheetName.CurrentRow.Cells["NameSheet"].Value.ToString(); }
            }
            catch (Exception) { name = ""; }

            if (name == "")
            {
                if (dtExcel.Rows.Count > 0)
                {
                    dtExcel.Rows.Clear();
                    RadGridView_ShowHD.DataSource = dtExcel;
                    dtExcel.AcceptChanges();
                }
                return;
            }

            //Clear Datatable
            if (dtExcel.Rows.Count > 0)
            {
                dtExcel.Rows.Clear();
                RadGridView_ShowHD.DataSource = dtExcel;
                dtExcel.AcceptChanges();
            }

            this.Cursor = Cursors.WaitCursor;

            DataTable DtSet = new DataTable();
            radBrowseEditor_choose.Enabled = false;

            try
            {
                System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
               ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + radBrowseEditor_choose.Value +
               @"';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [" + name + "$]", MyConnection);
                MyCommand.TableMappings.Add("Table", "Net-informations.com");
                MyCommand.Fill(DtSet);

                string pStaSave = "0";
                int count_staSave = 0;

                foreach (DataRow row_Excel in DtSet.Rows)
                {
                    string BchID = SystemClass.SystemBranchID;
                    string BchName = SystemClass.SystemBranchName;

                    string ZoneID = row_Excel[0].ToString(); ;
                    string ShelfID = row_Excel[1].ToString(); ;
                    string xlsItembarcode = row_Excel[4].ToString(); ;

                    string spc_name = "ไม่พบข้อมูลสินค้า", stasave;
                    DataTable dtItembarcode = ShelfClass.GetInformationItembarcode_ByItembarcode(BchID, xlsItembarcode);
                    if (dtItembarcode.Rows.Count > 0)
                    {
                        spc_name = dtItembarcode.Rows[0]["SPC_ITEMNAME"].ToString();
                        stasave = dtItembarcode.Rows[0]["STAUP"].ToString();
                    }
                    else
                    {
                        //pStaSave = "1"; 
                        stasave = "0";
                    }

                    if (stasave == "1")
                    {
                        //    pStaSave = "1"; 
                        count_staSave++;
                    }

                    string zoneName = "ไม่พบข้อมูล", shelfName = "ไม่พบข้อมูล";
                    DataTable dtShelf = ShelfClass.GetInformationShelfName_ByShelfID(BchID, ZoneID, ShelfID);
                    if (dtShelf.Rows.Count > 0)
                    {
                        zoneName = dtShelf.Rows[0]["ZONE_NAME"].ToString();
                        shelfName = dtShelf.Rows[0]["SHELF_NAME"].ToString();
                    }
                    else
                    {
                        pStaSave = "1";
                    }
                    //Insert Datatable
                    dtExcel.Rows.Add(BchID, BchName, xlsItembarcode, spc_name, ZoneID, zoneName, ShelfID, shelfName, stasave);
                }

                MyConnection.Close();
                RadGridView_ShowHD.DataSource = dtExcel;
                dtExcel.AcceptChanges();

                if (dtExcel.Rows.Count != DtSet.Rows.Count)
                {
                    RadButton_Save.Enabled = false;
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูล หน้าจอ กับ excel มีจำนวนไม่เท่ากัน ลองใหม่อีกครั้ง");
                }

                if (pStaSave == "0")
                {
                    if (count_staSave == RadGridView_ShowHD.Rows.Count)
                    {
                        RadButton_Save.Enabled = false; RadButton_Save.Focus();
                    }
                    else
                    {
                        RadButton_Save.Enabled = true; RadButton_Save.Focus();
                    }
                }
                else
                {
                    RadButton_Save.Enabled = false; RadButton_Save.Focus();
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถดำเนินการ Import Excel ได้ ลองใหม่อีกครั้ง." + Environment.NewLine +
                    "[ " + ex.Message + @" ]");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }
        }


        #region ROWS


        private void RadGridView_SheetName_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_SheetName_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);

        }
        private void RadGridView_SheetName_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_SheetName_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
        //SaveData
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกข้อมูลสินค้าเข้าชั้นวาง ?") == DialogResult.No) return;
            {

                ArrayList strIn = new ArrayList();
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["STASAVE"].Value.ToString() == "0")
                    {
                        strIn.Add(string.Format(@"
                INSERT INTO [SHOP_SHELFITEMS]
                   ([SHELF_ID],[ZONE_ID],[BRANCH_ID],[BRANCH_NAME],
                    [ITEMBARCODE]
                   ,[WHOINS],[WHONAMEINS])
                VALUES (
                    '" + RadGridView_ShowHD.Rows[i].Cells["SHELF_ID"].Value.ToString() + @"',
                    '" + RadGridView_ShowHD.Rows[i].Cells["ZONE_ID"].Value.ToString() + @"',
                    '" + RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString() + @"',
                    '" + RadGridView_ShowHD.Rows[i].Cells["BRANCH_NAME"].Value.ToString() + @"',
                    '" + RadGridView_ShowHD.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                    '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"'
                )"));
                    }
                }

                if (strIn.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ทุกรายการมีข้อมูลเรียบร้อยแล้ว ไม่สามารถเพิ่มชั้นวางได้" + Environment.NewLine + "ต้องแก้ไขชั้นวางแทน");
                    return;
                }

                string T = ConnectionClass.ExecuteSQL_ArrayMain(strIn);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    RadButton_Save.Enabled = false;
                    for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                    {
                        RadGridView_ShowHD.Rows[i].Cells["STASAVE"].Value = "1";
                    }
                    dtExcel.AcceptChanges();
                }
            }
        }
    }
}
