﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.Windows.Diagrams.Core;
using PC_Shop24Hrs.Controllers;
using System.Drawing;
using System.Collections.Generic;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfReportSale : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeUser;
        DataTable dtBranch, dt_Data;
        string tiitle;
        string[] colDate;
        public ShelfReportSale(string pTypeUser)
        {
            InitializeComponent();
            _pTypeUser = pTypeUser;
        }

        #region SetDropdownlist
        void SetDropdownlistBranch(DataTable _dtBranch)
        {
            RadDropDownList_Branch.DataSource = _dtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
        }
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearData();
        }
        #endregion

        #region SetFontInRadGridview 
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement.ColumnInfo.FieldName.Substring(0, 3).Equals("QTY"))
            {
                if (radCheckBox_Sales.Checked == true && radCheckBox_Amount.Checked == true)
                {
                    e.CellElement.BackColor = Color.DarkGray;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
                }
            }
            else
            {
                e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
            }
        }
        #endregion

        #region SetGrid 
        private void SetDatagridColumnsAndRow()
        {
            if (RadDropDownList_Branch.SelectedValue == null)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเลือกสาขา ตรวจสอบข้อมูลอีกครั้ง.");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัสโซน", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "รหัสชั้นวาง", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SALES", "ยอดขาย", 100));
            RadGridView_Show.MasterTemplate.Columns.ForEach(radGridViewColmn => { radGridViewColmn.IsPinned = true; });
            //dt_Data = this.AddNewColumn(new string[] { "ZONE_ID", "ZONE_NAME", "SHELF_ID", "SHELF_NAME", "QTY", "SALES" });
            string[] colShelf = RadGridView_Show.MasterTemplate.Columns.ToList().Select(i => i.Name).ToArray();

            dt_Data = this.AddNewColumn(colShelf);

            colDate = this.GetColumnsDate(radDateTimePicker_DateEnd.Value.Subtract(radDateTimePicker_DateBegin.Value).Days + 1);

            #region AddcolumnDate
            colDate.ForEach(Colmn =>
            {
                dt_Data.Columns.Add(new DataColumn(Colmn, typeof(string)));
                if (Colmn.Substring(0, 3).Equals("QTY"))
                {
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight(Colmn
                       , Colmn.Replace("QTY", "").Substring(0, 2) + "-" + Colmn.Replace("QTY", "").Substring(2, 2) + "-" + Colmn.Replace("QTY", "").Substring(4, 4) + Environment.NewLine + "(จำนวนรวม)", 100));
                }
                else
                {
                    RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight(Colmn
                      , Colmn.Replace("SALE", "").Substring(0, 2) + "-" + Colmn.Replace("SALE", "").Substring(2, 2) + "-" + Colmn.Replace("SALE", "").Substring(4, 4) + Environment.NewLine + "(ยอดขายรวม)", 100));
                }
            });
            #endregion

            this.GetGridViewSummary(colDate, GridAggregateFunction.Sum);

            #region Sale
            dt_Data = this.GetDataSale(ShelfReportClass.GetDataSalebyBranch(RadDropDownList_Branch.SelectedValue.ToString(), radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd"))
                , colDate, ShelfReportClass.GetShef(RadDropDownList_Branch.SelectedValue.ToString()));

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ข้อมูลสินค้ายังไม่ถูกกำหนดในชั้นวาง.");
            }
            else
            {
                RadGridView_Show.DataSource = dt_Data;
            }
            #endregion

            this.SetHideradgridColumn("QTY", radCheckBox_Amount.Checked);
            this.SetHideradgridColumn("SALE", radCheckBox_Sales.Checked);

            Cursor.Current = Cursors.Default;
        }
        private DataTable GetDataSale(DataTable dtsale, string[] colms, List<SHELF> Listshelf)
        {
            DataTable _dt_Data = dt_Data;
            DataTable _dtsale = dtsale;
            List<SHELF> _Listshelf = Listshelf;
            if (_dtsale.Rows.Count > 0)
            {
                foreach (var shelf in _Listshelf)
                {
                    double sumQty = 0.0, sum = 0.0;
                    DataRow dr = _dt_Data.NewRow();
                    dr["ZONE_ID"] = shelf.ZONE_ID;
                    dr["ZONE_NAME"] = shelf.ZONE_NAME;
                    dr["SHELF_ID"] = shelf.SHELF_ID;
                    dr["SHELF_NAME"] = shelf.SHELF_NAME;

                    foreach (DataRow drsale in _dtsale.Select().Where(e => e["ZONE_ID"].ToString().Equals(shelf.ZONE_ID) && e["SHELF_ID"].ToString().Equals(shelf.SHELF_ID)))
                    {
                        colms.ForEach(c =>
                        {
                            string date = GetDateFormate(c);

                            if (Convert.ToDateTime(date).ToString("yyyy-MM-dd").Equals(Convert.ToDateTime(drsale["TimePeriod"]).ToString("yyyy-MM-dd")))
                            {
                                if (c.Substring(0, 3).Equals("QTY"))
                                {
                                    dr[c] = Convert.ToDouble(drsale["QTY_TOTAL"]).ToString("#,##0.00");
                                    sumQty += Convert.ToDouble(dr[c]);
                                }
                                else
                                {
                                    dr[c] = Convert.ToDouble(drsale["AMOUNT"]).ToString("#,##0.00");
                                    sum += Convert.ToDouble(dr[c]);
                                }
                            }
                        });
                    }

                    dr["QTY"] = sumQty.ToString("#,##0.00");
                    dr["SALES"] = sum.ToString("#,##0.00");
                    _dt_Data.Rows.Add(dr);
                }
            }
            return _dt_Data;
        }
        private string[] GetColumnsDate(int count)
        {
            string[] columnName = new string[count * 2];
            int tmp = 0;
            for (int i = 0; i < count; i++)
            {
                columnName[tmp] = "QTY" + radDateTimePicker_DateBegin.Value.AddDays(i).ToString("ddMMyyyy"); tmp++;
                columnName[tmp] = "SALE" + radDateTimePicker_DateBegin.Value.AddDays(i).ToString("ddMMyyyy"); tmp++;
            }
            return columnName;
        }
        DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null)
            {
                return newColumn;
            }
            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];

                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }
            return newColumn;
        }
        #endregion

        private void ShelfReportSale_Load(object sender, EventArgs e)
        {
            this.SetProperty();
            this.ClearData();
            //dtBranch = BranchClass.GetBranchAll("'1'", "'0','1'");

            dtBranch = ShelfReportClass.GetBranch();
            SetDropdownlistBranch(dtBranch);

            if (_pTypeUser.Equals("SHOP"))
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                RadDropDownList_Branch.ReadOnly = true;
            }
            radDateTimePicker_DateBegin.Value = DateTime.Now.AddDays(-7);
            radDateTimePicker_DateEnd.Value = DateTime.Now.AddDays(-1);
        }

        #region Method
        private void ClearData()
        {
            RadGridView_Show.DataSource = null;
            colDate = null;
            if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();
            this.RadGridView_Show.SummaryRowsTop.Clear();
        }
        private void SetProperty()
        {
            radButtonElement_Pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_Pdf.ShowBorder = true;
            radButtonElement_Excel.ToolTipText = "บันทึก excel"; radButtonElement_Excel.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            radCheckBox_group.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Amount.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Sales.ButtonElement.Font = SystemClass.SetFontGernaral;
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_DateBegin, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-1));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_DateEnd, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));

            this.tiitle = @"รายงานยอดขายสินค้าตามชั้นวางสินค้า";
            this.Text = this.tiitle;
            this.toolStrip1.BackColor = Color.FromArgb(173, 244, 215);
            this.toolStrip1.Font = new Font(new FontFamily("Tahoma"), 9.75f);
            ToolStripLabel toolTiplalel = new ToolStripLabel
            {
                Text = @"ช่องที่ไม่มีค่า คือไม่มียอดขายตามเงื่อนไขที่เลือก หรือยังไม่มีข้อมูลสินค้าตามชั้นวาง."
            };
            toolStrip1.Items.Add(toolTiplalel);
            radCheckBox_Sales.Checked = true;
        }
        private void GetGridViewSummary(string[] Colname, GridAggregateFunction gridAggregate)
        {
            GridViewSummaryItem[] items = new GridViewSummaryItem[Colname.Length + 2];
            int i = 0;
            foreach (var v in Colname)
            {
                GridViewSummaryItem summaryItem = new GridViewSummaryItem(v, "{0:0,0}", gridAggregate);
                items[i] = summaryItem;
                i++;
            }

            items[i] = new GridViewSummaryItem("QTY", "{0:0,0}", gridAggregate);
            items[i + 1] = new GridViewSummaryItem("SALES", "{0:0,0}", gridAggregate);

            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(items);
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void GroupByBranchID()
        {
            this.RadGridView_Show.GroupDescriptors.Clear();
            this.RadGridView_Show.GroupDescriptors.Add(new GridGroupByExpression("ZONE_ID as ZONE_ID format \"{0}\" Group By ZONE_ID"));
        }
        private void SetHideradgridColumn(string colname, bool able)
        {
            for (int r = 0; r < RadGridView_Show.Rows.Count; r++)
            {
                for (int c = 0; c < RadGridView_Show.Columns.Count; c++)
                {
                    if (RadGridView_Show.Columns[c].FieldName.Substring(0, 3).Equals(colname.Substring(0, 3)))
                    {
                        this.RadGridView_Show.Columns[c].IsVisible = able;
                    }
                }
            }
        }

        private string GetDateFormate(string date)
        {
            return date.Substring(date.Length - 8, 8).Substring(4, 4) + "-" + date.Substring(date.Length - 8, 8).Substring(2, 2) + "-" + date.Substring(date.Length - 8, 8).Substring(0, 2);
        }

        #endregion

        #region Button

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.ClearData();
            this.SetDatagridColumnsAndRow();
        }
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView(this.tiitle, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        private void RadButtonElement_Pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeUser);
        }

        #endregion

        #region CheckBox
        private void RadCheckBox_group_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            this.RadGridView_Show.SummaryRowsTop.Clear();
            if (radCheckBox_group.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.GroupByBranchID();
                this.GetGridViewSummary(colDate, GridAggregateFunction.Sum);
            }
            else
            {
                this.RadGridView_Show.GroupDescriptors.Clear();
                this.GetGridViewSummary(colDate, GridAggregateFunction.Sum);
            }
        }
        private void RadCheckBox_Amount_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Amount.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.SetHideradgridColumn("QTY", true);
            }
            else
            {
                this.SetHideradgridColumn("QTY", false);

                if (radCheckBox_Sales.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    radCheckBox_Sales.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
        }
        private void RadCheckBox_Sales_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Sales.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.SetHideradgridColumn("SALE", true);
            }
            else
            {
                this.SetHideradgridColumn("SALE", false);
                if (radCheckBox_Amount.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    radCheckBox_Amount.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
        }
        #endregion

        #region DateTimePicker
        private void RadDateTimePicker_DateBegin_ValueChanged(object sender, EventArgs e)
        {
            if (radDateTimePicker_DateBegin.Value > radDateTimePicker_DateEnd.Value)
            {
                radDateTimePicker_DateBegin.Value = radDateTimePicker_DateEnd.Value;
            }
        }
        private void RadDateTimePicker_DateEnd_ValueChanged(object sender, EventArgs e)
        {
            if (radDateTimePicker_DateEnd.Value < radDateTimePicker_DateBegin.Value)
            {
                radDateTimePicker_DateEnd.Value = radDateTimePicker_DateBegin.Value;
            }
        }
        #endregion

    }
}