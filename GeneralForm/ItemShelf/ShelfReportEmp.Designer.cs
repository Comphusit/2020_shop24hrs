﻿namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    partial class ShelfReportEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShelfReportEmp));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton_Shelf = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Branch = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Emp = new Telerik.WinControls.UI.RadRadioButton();
            this.radStatusStrip_Zone = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Pdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_DateEnd = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_DateBegin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.toolStrip_Label = new System.Windows.Forms.ToolStrip();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_time = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Shelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DateEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DateBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.radLabel_time);
            this.panel3.Controls.Add(this.radCheckBox_Branch);
            this.panel3.Controls.Add(this.radGroupBox1);
            this.panel3.Controls.Add(this.radStatusStrip_Zone);
            this.panel3.Controls.Add(this.radLabel_Date);
            this.panel3.Controls.Add(this.radDateTimePicker_DateEnd);
            this.panel3.Controls.Add(this.radDateTimePicker_DateBegin);
            this.panel3.Controls.Add(this.RadButton_Search);
            this.panel3.Controls.Add(this.radLabel9);
            this.panel3.Controls.Add(this.RadDropDownList_Branch);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel3.Location = new System.Drawing.Point(632, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(194, 628);
            this.panel3.TabIndex = 8;
            // 
            // radCheckBox_Branch
            // 
            this.radCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Branch.Location = new System.Drawing.Point(9, 69);
            this.radCheckBox_Branch.Name = "radCheckBox_Branch";
            this.radCheckBox_Branch.Size = new System.Drawing.Size(71, 18);
            this.radCheckBox_Branch.TabIndex = 86;
            this.radCheckBox_Branch.Text = "ระบุสาขา";
            this.radCheckBox_Branch.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Branch_ToggleStateChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radRadioButton_Shelf);
            this.radGroupBox1.Controls.Add(this.radRadioButton_Branch);
            this.radGroupBox1.Controls.Add(this.radRadioButton_Emp);
            this.radGroupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.HeaderText = "เงื่อนไข";
            this.radGroupBox1.Location = new System.Drawing.Point(9, 235);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(175, 114);
            this.radGroupBox1.TabIndex = 85;
            this.radGroupBox1.Text = "เงื่อนไข";
            // 
            // radRadioButton_Shelf
            // 
            this.radRadioButton_Shelf.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Shelf.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Shelf.Location = new System.Drawing.Point(14, 23);
            this.radRadioButton_Shelf.Name = "radRadioButton_Shelf";
            this.radRadioButton_Shelf.Size = new System.Drawing.Size(159, 18);
            this.radRadioButton_Shelf.TabIndex = 82;
            this.radRadioButton_Shelf.Text = "ค่าคอมตามชั้นวางตามสาขา";
            this.radRadioButton_Shelf.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Shelf.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadio_ToggleStateChanged);
            // 
            // radRadioButton_Branch
            // 
            this.radRadioButton_Branch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Branch.Location = new System.Drawing.Point(14, 81);
            this.radRadioButton_Branch.Name = "radRadioButton_Branch";
            this.radRadioButton_Branch.Size = new System.Drawing.Size(105, 18);
            this.radRadioButton_Branch.TabIndex = 84;
            this.radRadioButton_Branch.TabStop = false;
            this.radRadioButton_Branch.Text = "ค่าคอมตามสาขา";
            // 
            // radRadioButton_Emp
            // 
            this.radRadioButton_Emp.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Emp.Location = new System.Drawing.Point(14, 52);
            this.radRadioButton_Emp.Name = "radRadioButton_Emp";
            this.radRadioButton_Emp.Size = new System.Drawing.Size(121, 18);
            this.radRadioButton_Emp.TabIndex = 83;
            this.radRadioButton_Emp.TabStop = false;
            this.radRadioButton_Emp.Text = "ค่าคอมตามพนักงาน";
            // 
            // radStatusStrip_Zone
            // 
            this.radStatusStrip_Zone.AutoSize = false;
            this.radStatusStrip_Zone.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip_Zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip_Zone.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Excel,
            this.commandBarSeparator1,
            this.radButtonElement_Pdf,
            this.commandBarSeparator2});
            this.radStatusStrip_Zone.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip_Zone.Name = "radStatusStrip_Zone";
            this.radStatusStrip_Zone.Size = new System.Drawing.Size(192, 42);
            this.radStatusStrip_Zone.TabIndex = 81;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.UseCompatibleTextRendering = false;
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip_Zone.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Pdf
            // 
            this.radButtonElement_Pdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Pdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_Pdf.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement_Pdf.Name = "radButtonElement_Pdf";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Pdf, false);
            this.radButtonElement_Pdf.Text = "radButtonElement1";
            this.radButtonElement_Pdf.UseCompatibleTextRendering = false;
            this.radButtonElement_Pdf.Click += new System.EventHandler(this.RadButtonElement_Pdf_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Zone.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(9, 143);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Date.TabIndex = 72;
            this.radLabel_Date.Text = "ระบุวันที่";
            // 
            // radDateTimePicker_DateEnd
            // 
            this.radDateTimePicker_DateEnd.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_DateEnd.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_DateEnd.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_DateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_DateEnd.Location = new System.Drawing.Point(9, 192);
            this.radDateTimePicker_DateEnd.Name = "radDateTimePicker_DateEnd";
            this.radDateTimePicker_DateEnd.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_DateEnd.TabIndex = 71;
            this.radDateTimePicker_DateEnd.TabStop = false;
            this.radDateTimePicker_DateEnd.Text = "22/05/2020";
            this.radDateTimePicker_DateEnd.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_DateEnd.ValueChanged += new System.EventHandler(this.RadDateTimePicker_DateEnd_ValueChanged);
            // 
            // radDateTimePicker_DateBegin
            // 
            this.radDateTimePicker_DateBegin.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_DateBegin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_DateBegin.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_DateBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_DateBegin.Location = new System.Drawing.Point(9, 165);
            this.radDateTimePicker_DateBegin.Name = "radDateTimePicker_DateBegin";
            this.radDateTimePicker_DateBegin.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_DateBegin.TabIndex = 70;
            this.radDateTimePicker_DateBegin.TabStop = false;
            this.radDateTimePicker_DateBegin.Text = "22/05/2020";
            this.radDateTimePicker_DateBegin.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            this.radDateTimePicker_DateBegin.ValueChanged += new System.EventHandler(this.RadDateTimePicker_DateBegin_ValueChanged);
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(9, 368);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 31;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(9, 93);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(39, 19);
            this.radLabel9.TabIndex = 69;
            this.radLabel9.Text = "สาขา";
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_Branch.DropDownHeight = 124;
            this.RadDropDownList_Branch.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(9, 115);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Branch.TabIndex = 68;
            this.RadDropDownList_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.toolStrip_Label, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // toolStrip_Label
            // 
            this.toolStrip_Label.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip_Label.Location = new System.Drawing.Point(0, 608);
            this.toolStrip_Label.Name = "toolStrip_Label";
            this.toolStrip_Label.Size = new System.Drawing.Size(623, 20);
            this.toolStrip_Label.TabIndex = 6;
            this.toolStrip_Label.Text = "toolStrip1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RadGridView_Show);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(617, 602);
            this.panel2.TabIndex = 5;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 602);
            this.RadGridView_Show.TabIndex = 1;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            // 
            // radLabel_time
            // 
            this.radLabel_time.Location = new System.Drawing.Point(9, 407);
            this.radLabel_time.Name = "radLabel_time";
            this.radLabel_time.Size = new System.Drawing.Size(76, 18);
            this.radLabel_time.TabIndex = 87;
            this.radLabel_time.Text = "radLabel_time";
            this.radLabel_time.Visible = false;
            // 
            // ShelfReportEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShelfReportEmp";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ShelfReportSale";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ShelfReportEmp_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Shelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DateEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_DateBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        protected System.Windows.Forms.Panel panel3;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        public Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_DateEnd;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_DateBegin;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Zone;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Pdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Shelf;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Branch;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Emp;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Branch;
        private System.Windows.Forms.ToolStrip toolStrip_Label;
        private Telerik.WinControls.UI.RadLabel radLabel_time;
    }
}
