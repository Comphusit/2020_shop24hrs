﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using PC_Shop24Hrs.GeneralForm.MNOR;
using System.Collections;
using PC_Shop24Hrs.ComMinimart.Camera;
using Telerik.WinForms.Documents.Layout;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfEmpl : Telerik.WinControls.UI.RadForm
    {
        private string TypeEdit, TypeCondition = "Shelf", TypeSetting = "EmplSetting";
        //private int IndexRow = -1 ;
        //private string IndexRowId = string.Empty, IndexRowName = string.Empty;
        //private string Empl_M, Empl, EmplName;
        ShelfGetSetIndexClass shelfGetSetIndexClass = new ShelfGetSetIndexClass();
        ShelfGetSetClass shelfGetSetClass = new ShelfGetSetClass();
        bool Permission = false;
        //bool TypeEmpl = false;
        string Month = string.Empty;// radDropDownList_Month.SelectedValue.ToString();
        string Years = string.Empty;//radDropDownList_Year.SelectedValue.ToString();
        readonly string DateSelect = string.Empty;
        public ShelfEmpl()
        {
            InitializeComponent();

            //RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Zone);

            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Year);
            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Month);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShelfOrEmpl);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            DatagridClass.SetTextControls(RadCheckBox_EmplShelf.Controls, (int)(CollectionControl.RadCheckBox));

            RadButtonElement_CheckAll.ShowBorder = true; RadButtonElement_CheckAll.ToolTipText = "เลือกวันที่ทั้งหมดตามตารางกะ";
            RadButtonElement_WorkUser.ShowBorder = true; RadButtonElement_WorkUser.ToolTipText = "ตั้งค่าพนักงาน";
            RadButtonElement_pdf.ShowBorder = true; RadButtonElement_pdf.ToolTipText = "คู่มือ";
             
            RadButton_Search.ButtonElement.ShowBorder = true;
        }

        //กำหนดค่่าเบื้องต้น
        void ClearData(String Type)
        {
            radLabel_Group.Visible = false;
            radDropDownList_Zone.Visible = false;
            radLabel4.Visible = true;
            radLabel3.Visible = true;
            radLabel2.Visible = true;
            radDropDownList_Year.Visible = true;
            radDropDownList_Month.Visible = true;
            //TypeEmpl = true;
            RadCheckBox_EmplHR.Checked = true;
            //Permission = true;

            if (SystemClass.SystemComProgrammer == "0")
            {
                RadCheckBox_EmplHR.Visible = false;
                Permission = CheckPermission(SystemClass.SystemUserID);
            }

            if (Permission)
            {
                RadButtonElement_CheckAll.Enabled = false;
                RadButtonElement_WorkUser.Enabled = false;
                TypeSetting = "";
                RadCheckBox_EmplShelf.Checked = false;
                RadCheckBox_EmplShelf.Enabled = false;
                radLabel4.Visible = false;
                radLabel3.Visible = false;
                radLabel2.Visible = false;
                radDropDownList_Year.Visible = false;
                radDropDownList_Month.Visible = false;
                groupBox_Condition.Enabled = true;

                
                if (RadGridView_Show.RowCount > 0)
                {
                    RadGridView_Show.DataSource = null;
                    RadGridView_Show.Rows.Clear();
                }
                if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();

                if (RadGridView_ShelfOrEmpl.RowCount > 0)
                {
                    RadGridView_ShelfOrEmpl.DataSource = null;
                    RadGridView_ShelfOrEmpl.Rows.Clear();
                }
                if (RadGridView_ShelfOrEmpl.ColumnCount > 0) RadGridView_ShelfOrEmpl.Columns.Clear();

                SetDropdownlistYear(ItemGroupStockClass.GetAllYear());
                SetDropdownlistMonth(ItemGroupStockClass.GetAllMonth());
            }
            else
            {
                if (TypeSetting == "EmplSetting")
                {
                    RadButtonElement_CheckAll.Enabled = false;
                    RadButtonElement_WorkUser.Enabled = false;
                }
                else
                {
                    RadButtonElement_CheckAll.Enabled = true;
                    RadButtonElement_WorkUser.Enabled = true;
                }


                switch (Type)
                {
                    //Default :: เคลียร์ค่าสำหรับ การตั้งค่าวันทำงาน  
                    case "Default":
                        radDropDownList_Year.Enabled = true;
                        radDropDownList_Month.Enabled = true;
                        groupBox_Condition.Enabled = true;

                        RadGridView_Show.DataSource = null;
                        if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
                        if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();
                        RadGridView_ShelfOrEmpl.DataSource = null;
                        if (RadGridView_ShelfOrEmpl.RowCount > 0) RadGridView_ShelfOrEmpl.Rows.Clear();
                        if (RadGridView_ShelfOrEmpl.ColumnCount > 0) RadGridView_ShelfOrEmpl.Columns.Clear();
                        SetDropdownlistYear(ItemGroupStockClass.GetAllYear());
                        SetDropdownlistMonth(ItemGroupStockClass.GetAllMonth());
                        break;
                    //DefaultSetting :: เคลียร์ค่าสำหรับ การตั้งค่าพนักงานประจำชั้นวาง
                    case "DefaultSetting":
                        RadCheckBox_EmplShelf.Checked = true;
                        radLabel4.Visible = false;
                        radLabel3.Visible = false;
                        radLabel2.Visible = false;
                        radDropDownList_Year.Visible = false;
                        radDropDownList_Month.Visible = false;
                        groupBox_Condition.Enabled = true;

                        //IndexRow = -1;
                        //IndexRowId = string.Empty;
                        //IndexRowName = string.Empty;
                        RadGridView_Show.DataSource = null;
                        if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
                        if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();
                        RadGridView_ShelfOrEmpl.DataSource = null;
                        if (RadGridView_ShelfOrEmpl.RowCount > 0) RadGridView_ShelfOrEmpl.Rows.Clear();
                        if (RadGridView_ShelfOrEmpl.ColumnCount > 0) RadGridView_ShelfOrEmpl.Columns.Clear();
                        SetDropdownlistYear(ItemGroupStockClass.GetAllYear());
                        SetDropdownlistMonth(ItemGroupStockClass.GetAllMonth());
                        break;
                    //RadGridView_ShelfOrEmpl :: เคลียร์ Gridview RadGridView_ShelfOrEmpl ขวามือ
                    case "RadGridView_ShelfOrEmpl":
                        RadGridView_ShelfOrEmpl.DataSource = null;
                        if (RadGridView_ShelfOrEmpl.RowCount > 0) RadGridView_ShelfOrEmpl.Rows.Clear();
                        if (RadGridView_ShelfOrEmpl.ColumnCount > 0) RadGridView_ShelfOrEmpl.Columns.Clear();
                        break;
                    //RadGridView_Show :: เคลียร์ Gridview RadGridView_Show ซ้ายมือ
                    case "RadGridView_Show":
                        RadGridView_Show.DataSource = null;
                        if (RadGridView_Show.RowCount > 0) RadGridView_Show.Rows.Clear();
                        if (RadGridView_Show.ColumnCount > 0) RadGridView_Show.Columns.Clear();
                        break;
                }
            }
        }


        private bool CheckPermission(String EmplID)
        {
            DataTable DtPossision = Models.EmplClass.GetEmployee(EmplID); //EmplClass.GetDept_byEmplId(EmplID);
            bool Rsu = false;
            if (DtPossision.Rows.Count > 0)
            {
                //ไม่ใช่ผู้ช่วยหรือ ผจก 
                if (DtPossision.Rows[0]["IVZ_HROMPositionTitle"].ToString() != "02")
                {
                    Rsu = true;
                }
            }
            return Rsu;
        }

        private void CheckEmplEVENT(String Branch, String DateEventBegin, String DateEventEnd)
        {
            DataTable DtEmplEVENT = ShelfClass.GetEmplEVENT(Branch, DateEventBegin, DateEventEnd);

            if (DtEmplEVENT.Rows.Count > 0)
            {
                for (int iDtEmplEVENT = 0; iDtEmplEVENT < DtEmplEVENT.Rows.Count; iDtEmplEVENT++)
                {
                    if (DtEmplEVENT.Rows[iDtEmplEVENT]["PWUPDTYPEDESC"].ToString() != "FULL-DAY")
                    {
                        continue;
                    }

                    String SqlEx;
                    string DATE_EVENT = DtEmplEVENT.Rows[iDtEmplEVENT]["PWEVENT"].ToString();
                    string DATE_EVENTNAME = DtEmplEVENT.Rows[iDtEmplEVENT]["PWEVENTNAME"].ToString();
                    string EMPL_M = DtEmplEVENT.Rows[iDtEmplEVENT]["PWEMPLOYEE"].ToString();
                    string DATEADJ = DtEmplEVENT.Rows[iDtEmplEVENT]["CVPWDATEADJ"].ToString();

                    SqlEx = string.Format(@"	update SHOP_SHELFEMPLDATE set  DATE_EVENT='{0}',DATE_EVENTNAME='{1}',
                                WHOUP='{2}', WHONAMEUP='{3}', DATEUP=convert(varchar,getdate(),25)
                                where EMPL_ID='{4}' AND BRANCH_ID='{5}'   AND DATE_EMPL='{6}'  ",
                                DATE_EVENT,
                                DATE_EVENTNAME,
                                SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                                EMPL_M,
                                Branch,
                                DATEADJ);
                    String Tranctions = ConnectionClass.ExecuteSQL_Main(SqlEx);
                    if (Tranctions != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(Tranctions);
                        this.Cursor = Cursors.Default;
                        return;
                    }


                }
            }
        }




        #region "set Dropdownlist" 
        //สร้าง ตัวเลือกแผนก
        void SetDropdownlistBranch(DataTable DtBranch)
        {
            RadDropDownList_Branch.DataSource = DtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            if (SystemClass.SystemBranchID != "MN000")
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                RadDropDownList_Branch.Enabled = false;
            }
            else
            {
                RadDropDownList_Branch.Enabled = true;
            }

        }

        //สร้าง ตัวเลือกกลุ่ม ตามแผนก และ ประเภท
        void SetDropdownlistZone(DataTable DtZone)
        {
            radDropDownList_Zone.DataSource = DtZone;
            radDropDownList_Zone.DisplayMember = "ZONE_NAME";
            radDropDownList_Zone.ValueMember = "ZONE_ID";
        }

        //สร้างปี 1 จากฟังชั่น
        void SetDropdownlistYear(DataTable DtYear)
        {
            radDropDownList_Year.DataSource = DtYear;
            radDropDownList_Year.DisplayMember = "Member";
            radDropDownList_Year.ValueMember = "Value";
            if (DtYear.Rows.Count > 0) radDropDownList_Year.SelectedValue = DateTime.Now.ToString("yyyy");
        }

        //สร้างเดือน จาก ฟังชั่น
        void SetDropdownlistMonth(DataTable DtMonth)
        {
            radDropDownList_Month.DataSource = DtMonth;
            radDropDownList_Month.DisplayMember = "Member";
            radDropDownList_Month.ValueMember = "Value";
            if (DtMonth.Rows.Count > 0) radDropDownList_Month.SelectedValue = DateTime.Now.ToString("MM");
        }

        #endregion




        #region set grid Show And ShelfandEmpl 

        /// <summary>
        /// แสดงรายละเอียด พนักงานทั้งหมด ในตารางShow
        /// </summary>
        /// <param name="Years">ปี สำหรับตั้งค่า <param>
        /// <param name="Month">เดือน สำหรับตั้งค่า </param>
        private void SetGridShowEmpl(int Years, int Month)
        {
            //สร้าง column ใน RadGridView_Show 
            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPL_ID", "รหัสพนักงาน", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPL_NAME", "ชื่อ-นามสกุล", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 150));
            RadGridView_Show.Columns["EMPL_ID"].IsPinned = true;
            RadGridView_Show.Columns["EMPL_NAME"].IsPinned = true;
            RadGridView_Show.Columns["POSSITION"].IsPinned = true;

            //สร้าง column วันที่
            int createDate = DateTime.DaysInMonth(Years, Month);
            for (int i = 1; i < createDate + 1; i++)
            {
                String ColumnName = i.ToString("D2");
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox(ColumnName, ColumnName, 80));

            }
            //เพิ่ม row พนักงานในสาขา
            //DataTable DtEmpl = EmplClass.GetEmployeeAll_ByDptID(RadDropDownList_Branch.SelectedValue.ToString(), "=",
            //    "AND HRPPartyJobTableRelationship.DESCRIPTION not like '%ผู้ช่วย%' ");
            //DataTable DtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@" = '{RadDropDownList_Branch.SelectedValue}' ", " NOT LIKE '%ผู้ช่วย%' ", "");
            DataTable DtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@"{RadDropDownList_Branch.SelectedValue}", " NOT LIKE '%ผู้ช่วย%' ", "");
            if (DtEmpl.Rows.Count > 0)
            {
                foreach (DataRow row in DtEmpl.Rows)
                {
                    RadGridView_Show.Rows.Add(row["EMPLID"].ToString(), row["SPC_NAME"].ToString(), row["POSSITION"].ToString());
                }
            }

        }

        //ตั้งค่า grid show ตามพนักงานที่ดูแลเชลล์ มีวันที่
        private void SetGridShowShelf(int Years, int Month)
        {
            //สร้าง column ใน RadGridView_Show 
            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัส", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("shelf_id", "รหัส", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("shelf_name", "ชื่อ", 150));
            RadGridView_Show.Columns["ZONE_ID"].IsPinned = true;
            RadGridView_Show.Columns["ZONE_NAME"].IsPinned = true;
            RadGridView_Show.Columns["shelf_id"].IsPinned = true;
            RadGridView_Show.Columns["shelf_name"].IsPinned = true;

            //สร้าง column วันที่
            int createDate = DateTime.DaysInMonth(Years, Month);
            for (int i = 1; i < createDate + 1; i++)
            {
                String ColumnName = i.ToString("D2");// + "/ " + radDropDownList_Month.SelectedValue.ToString();
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox(ColumnName, ColumnName, 80));
            }
            //เพิ่ม row เชลล์ในสาขา
            DataTable DtShelf = ShelfClass.GetShelf_ByBranch(RadDropDownList_Branch.SelectedValue.ToString());
            //radDropDownList_Zone.SelectedValue.ToString()
            if (DtShelf.Rows.Count > 0)
            {
                foreach (DataRow row in DtShelf.Rows)
                {
                    RadGridView_Show.Rows.Add(row["ZONE_ID"].ToString(), row["ZONE_NAME"].ToString(),
                        row["shelf_id"].ToString(), row["shelf_name"].ToString());
                }
            }
        }


        #endregion

        #region"EmplSetting Grid"

        ////ตั้งค่า Grid ShelfOrEmpl
        private void SetShelf_ShelfOrEmplColumns()//int Years, int Month
        {
            //สร้าง column ใน RadGridView_ShelfOrEmpl 
            RadGridView_ShelfOrEmpl.DataSource = null;
            if (RadGridView_ShelfOrEmpl.RowCount > 0) RadGridView_ShelfOrEmpl.Rows.Clear();
            if (RadGridView_ShelfOrEmpl.ColumnCount > 0) RadGridView_ShelfOrEmpl.Columns.Clear();

            RadGridView_ShelfOrEmpl.Visible = true;
            RadGridView_ShelfOrEmpl.EnableFiltering = true;
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("CheckShelfEmpl", "", 40));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัส", 60));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 120));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("shelf_id", "รหัส", 60));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("shelf_name", "ชื่อ", 150));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_NUM", "จำนวนพนักงานตั้งค่า", 150));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CountEmpl", "จำนวนพนักงานจริง", 150));

            RadGridView_ShelfOrEmpl.Columns["CheckShelfEmpl"].IsPinned = true;

            //เชลล์ทั้งหมดในสาขาตามโซน 
            RadGridView_ShelfOrEmpl.DataSource = ShelfClass.GetShelfSettingColumns(RadDropDownList_Branch.SelectedValue.ToString());
            //ค้นหาข้อมูลพนักงาน เพื่อแสดงใน RadGridView_Show
            shelfGetSetIndexClass = new ShelfGetSetIndexClass();
        }


        ////ตั้งค่า แสดงพนักงาน Grid ShelfOrEmpl
        private void SetEmpl_ShelfOrEmplColumns()//int Years, int Month, string TypeGridSetting
        {
            RadGridView_ShelfOrEmpl.DataSource = null;
            if (RadGridView_ShelfOrEmpl.RowCount > 0) RadGridView_ShelfOrEmpl.Rows.Clear();
            if (RadGridView_ShelfOrEmpl.ColumnCount > 0) RadGridView_ShelfOrEmpl.Columns.Clear();
            //สร้าง column ใน RadGridView_ShelfOrEmpl 
            RadGridView_ShelfOrEmpl.Visible = true;
            RadGridView_ShelfOrEmpl.EnableFiltering = true;
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("CheckShelfEmpl", "", 60));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPL_ID", "รหัสพนักงาน", 90));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ-นามสกุล", 180));
            RadGridView_ShelfOrEmpl.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 180));

            //พนักงานทั้งหมดในสาขา ใส่ DataTable ใหม่เพื่ม columns : Checkempl สำหรับเลือก
            DataTable dtEmpl;
            if (RadCheckBox_EmplHR.Checked == true)
            {
                //dtEmpl = EmplClass.GetEmployeeAll_ByDptID(RadDropDownList_Branch.SelectedValue.ToString(), "=",
                //    "AND HRPPartyJobTableRelationship.DESCRIPTION not like '%ผู้จัดการ%'  ");
                //dtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@" = '{RadDropDownList_Branch.SelectedValue}' ", " NOT LIKE '%ผู้จัดการ%'  ", "");
                dtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@"{RadDropDownList_Branch.SelectedValue}", " NOT LIKE '%ผู้จัดการ%'  ", "");
            }
            else
            {
                dtEmpl = ShelfClass.GetEmplShelf(RadDropDownList_Branch.SelectedValue.ToString());
            }


            DataTable dtShelfOrEmpl = new DataTable();
            dtShelfOrEmpl.Columns.Add("CheckShelfEmpl");
            dtShelfOrEmpl.Columns.Add("EMPL_ID");
            dtShelfOrEmpl.Columns.Add("SPC_NAME");
            dtShelfOrEmpl.Columns.Add("POSSITION");
            for (int i = 0; i < dtEmpl.Rows.Count; i++)
            {
                dtShelfOrEmpl.Rows.Add("", dtEmpl.Rows[i]["EMPLID"].ToString(),
                    dtEmpl.Rows[i]["SPC_NAME"].ToString(),
                    dtEmpl.Rows[i]["POSSITION"].ToString());
            }
            RadGridView_ShelfOrEmpl.DataSource = dtShelfOrEmpl;
            shelfGetSetIndexClass = new ShelfGetSetIndexClass();
        }

        //ตั้งค่า columns grid show แสดงเชลล์ ตามพนักงานที่ดูแลเชลล์ 
        private void SetShelf_ColumnsGridShowShelfSetting()
        {
            //สร้าง column ใน RadGridView_Show 
            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัส", 60));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "รหัส", 60));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("SHELF_EMPL", "ตั้งค่า", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_NUM", "จำนวนพนักงานตั้งค่า", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CountEmpl", "จำนวนพนักงานจริง", 150));

            RadGridView_Show.Columns["ZONE_ID"].IsPinned = true;
            RadGridView_Show.Columns["ZONE_NAME"].IsPinned = true;
            RadGridView_Show.Columns["SHELF_ID"].IsPinned = true;
            RadGridView_Show.Columns["SHELF_NAME"].IsPinned = true;
            RadGridView_Show.Columns["SHELF_EMPL"].IsPinned = true;
            RadGridView_Show.Columns["EMP_NUM"].IsPinned = true;
            RadGridView_Show.Columns["CountEmpl"].IsPinned = true;
            //เพิ่ม row เชลล์ในสาขา

        }

        //ตั้งค่า grid show ตามพนักงาน 
        private void SetShelf_DataGridShowSetting()
        {

            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();

            //เพิ่ม row เชลล์ในสาขา
            DataTable DtShelf = ShelfClass.GetShelfSettingColumns(RadDropDownList_Branch.SelectedValue.ToString());
            //radDropDownList_Zone.SelectedValue.ToString()
            if (DtShelf.Rows.Count > 0)
            {
                foreach (DataRow row in DtShelf.Rows)
                {
                    RadGridView_Show.Rows.Add(row["ZONE_ID"].ToString(), row["ZONE_NAME"].ToString(),
                        row["SHELF_ID"].ToString(), row["SHELF_NAME"].ToString(),
                        "FALSE", row["EMP_NUM"].ToString(), row["CountEmpl"].ToString());
                }
            }
        }

        //ตั้งค่า columns grid show แสดงพนักงาน ตามเชลล์ 
        private void SetEmpl_ColumnsGridShowSetting()
        {
            //สร้าง column ใน RadGridView_Show 
            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPL_ID", "รหัส", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("SHELF_EMPL", "ตั้งค่า", 150));

            RadGridView_Show.Columns["EMPL_ID"].IsPinned = true;
            RadGridView_Show.Columns["SPC_NAME"].IsPinned = true;
            RadGridView_Show.Columns["POSSITION"].IsPinned = true;
            RadGridView_Show.Columns["SHELF_EMPL"].IsPinned = true;

        }
        //ตั้งค่า grid show ตามเชลล์
        private void SetEmpl_DataGridShowSetting()
        {
            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();

            //เพิ่ม row พนักงานในสาขา 
            //DataTable DtEmpl = EmplClass.GetEmployeeAll_ByDptID(RadDropDownList_Branch.SelectedValue.ToString(), "=",
            //   "AND HRPPartyJobTableRelationship.DESCRIPTION not like '%ผู้จัดการ%' ");
            //DataTable DtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@" = '{RadDropDownList_Branch.SelectedValue}' ", " NOT LIKE '%ผู้จัดการ%' ", "");
            DataTable DtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@"{RadDropDownList_Branch.SelectedValue}", " NOT LIKE '%ผู้จัดการ%' ", "");
            if (DtEmpl.Rows.Count > 0)
            {
                foreach (DataRow row in DtEmpl.Rows)
                {
                    RadGridView_Show.Rows.Add(row["EMPLID"].ToString(), row["SPC_NAME"].ToString(), row["POSSITION"].ToString(), "FALSE");
                }
            }
        }



        #endregion 
        private void ShelfItems_Load(object sender, EventArgs e)
        {
            //TypeEdit ตั้งค่าเพื่อ เคลียร์ค่า ::  DefaultSetting --> ตั้งค่าเริ่มต้น, 
            TypeEdit = "DefaultSetting";

            //ตั้งค่า setting หรือ ตามวัน
            //TypeSetting setting ::ตั้งค่า --> EmplSetting , วันทำงาน --> "" (ค่าว่าง) 
            TypeSetting = "EmplSetting";
            //TypeCondition เงื่อนไข :: พนักงาน --> Employee, ชั้นวาง --> Shelf
            TypeCondition = "Employee";
            ClearData(TypeEdit);


            SetDropdownlistBranch(Class.BranchClass.GetBranchAll("'1'", "'0','1'"));

            CheckEmplEVENT(SystemClass.SystemBranchID, DateTime.Now.AddDays(7).ToString("yyyy-MM-dd"), DateTime.Now.AddDays(0).ToString("yyyy-MM-dd"));
            Years = radDropDownList_Year.SelectedValue.ToString();
            Month = radDropDownList_Month.SelectedValue.ToString();
        }

        #region EVEN 
        private void RadDropDownList_Zone_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Zone.SelectedValue == null)
            {
                TypeEdit = "Default";
                ClearData(TypeEdit);
            }
        }

        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Branch.SelectedValue.ToString() != "")
            {
                //เลือกแผนกจะไปหาข้อมูลกลุ่มใหม่
                SetDropdownlistZone(ShelfClass.GetZone_ByBranch(RadDropDownList_Branch.SelectedValue.ToString()));
            }
            else
            {
                TypeEdit = "Default";
                ClearData(TypeEdit);
            }
        }

        private void RadioButton_Shelf_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Shelf.Checked == true)
            {
                TypeCondition = "Shelf";
                //TypeEdit = "Default";
                ClearData(TypeEdit);
            }
        }

        private void RadioButton_Empl_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Empl.Checked == true)
            {
                TypeCondition = "Employee";
                //TypeEdit = "Default";
                ClearData(TypeEdit);
            }
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (TypeSetting == "EmplSetting")
            {
                switch (TypeCondition)
                {
                    case "Employee":
                        //แสดงพนักงานในสาขา
                        //SetEmpl_ShelfOrEmplColumns(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                        //                 int.Parse(radDropDownList_Month.SelectedValue.ToString()), TypeSetting);
                        SetEmpl_ShelfOrEmplColumns();
                        if (RadGridView_Show.ColumnCount == 0) SetShelf_ColumnsGridShowShelfSetting();
                        SetShelf_DataGridShowSetting();
                        break;
                    case "Shelf":
                        //SetShelf_ShelfOrEmplColumns(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                        //                               int.Parse(radDropDownList_Month.SelectedValue.ToString()));
                        SetShelf_ShelfOrEmplColumns();
                        if (RadGridView_Show.ColumnCount == 0) SetEmpl_ColumnsGridShowSetting();
                        SetEmpl_DataGridShowSetting();
                        break;
                }

            }
            else
            {
                if (RadDropDownList_Branch.SelectedValue == null
               || radDropDownList_Zone.SelectedValue == null)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีกลุ่มสินค้าในแผนกที่เลือก กรุณาตรวจสอบอีกครั้ง");
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;

                ClearData("RadGridView_ShelfOrEmpl");
                ClearData("RadGridView_Show");
                switch (TypeCondition)
                {
                    case "Employee":
                        //SetEmpl_ShelfOrEmplColumns(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                        //                               int.Parse(radDropDownList_Month.SelectedValue.ToString()), TypeCondition);
                        SetEmpl_ShelfOrEmplColumns();
                        SetGridShowShelf(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                                                       int.Parse(radDropDownList_Month.SelectedValue.ToString()));

                        break;
                    case "Shelf":
                        //SetShelf_ShelfOrEmplColumns(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                        //                               int.Parse(radDropDownList_Month.SelectedValue.ToString()));
                        SetShelf_ShelfOrEmplColumns();
                        SetGridShowEmpl(int.Parse(radDropDownList_Year.SelectedValue.ToString()),
                                                       int.Parse(radDropDownList_Month.SelectedValue.ToString()));
                        break;
                }

                Cursor.Current = Cursors.Default;
                //SetDataGrid();
            }
        }

        private void RadButtonElement_Refresh_Click(object sender, EventArgs e)
        {
            //SetDataGrid();
        }

        private void RadButtonElement_CheckAll_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_Show.RowCount < 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาค้นหารายละเอียดชั้นวางและพนักงานดูแลก่อนตั้งค่าทุกครั้ง");
                return;
            }
            DataTable dtEmpl;
            string Del;

            Month = radDropDownList_Month.SelectedValue.ToString();
            Years = radDropDownList_Year.SelectedValue.ToString();

            //ถ้าเลือกพนักงานในเงื่อนไข พนักงาน และเลือกพนัหงาน1 คน จะจัดการพนักงานคนที่เท่านั้น ไม่ได้พนักงานทั้งหมด
            if (RadGridView_ShelfOrEmpl.RowCount > 0 && TypeCondition == "Employee")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ตั้งค่าพนักงาน ดูแลชั้นใหม่ 1 คน " + System.Environment.NewLine +
                    RadGridView_ShelfOrEmpl.CurrentRow.Cells[1].Value.ToString() +
                    RadGridView_ShelfOrEmpl.CurrentRow.Cells[2].Value.ToString() + " ใช่หรือไม่ ") == DialogResult.No)
                {
                    this.Cursor = Cursors.Default;
                    return;
                }

                String EMPLID = RadGridView_Show.CurrentRow.Cells[0].Value.ToString();
                //พนักงานทั้งหมดในสาขายกเว้น ผช , ผจก และพนักงานทีมีการกำหนดค่าวันทำงานแล้ว
                //dtEmpl = EmplClass.GetEmployeeAll_ByDptID(RadDropDownList_Branch.SelectedValue.ToString(), "=",
                //    string.Format(@"AND HRPPartyJobTableRelationship.DESCRIPTION not like '%ผู้จัดการ%' 
                //    AND EMPLID='{0}' ", EMPLID));
                //dtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@" = '{RadDropDownList_Branch.SelectedValue}' ", "NOT LIKE '%ผู้จัดการ%' ", EMPLID);
                dtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@"{RadDropDownList_Branch.SelectedValue}", "NOT LIKE '%ผู้จัดการ%' ", EMPLID);

                Del = $@"DELETE 
                        FROM    SHOP_SHELFEMPLDATE  
                        WHERE   MONTH(DATE_EMPL)= '{Month}' and YEAR(DATE_EMPL)= '{Years}'
                                AND EMPL_ID = '{EMPLID}'
                                AND BRANCH_ID = '{SystemClass.SystemBranchID}'";

                ////เช็คพนักงานตั้งค่าดูแลล็อคประจำวันหรือยัง 
                ////ถ้าตั้งค่าแล้วให้ลบตั้งแต่วันที่ปัจจุบัน เพื่อตั้งค่าใหม่ 
                ////ถ้ายังไม่ได้ตั้งค่า ให้ทำทั้งเดือน
                if (ShelfClass.CheckEmplMonthSetting(RadDropDownList_Branch.SelectedValue.ToString(),
                       Month, Years, EMPLID))
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("พนักงานมีการตั้งค่าประจำวันแล้ว ต้องการลบเพื่อตั้งค่าใหม่หรือไม่ ") != DialogResult.No)
                    {
                        string returnstr = ConnectionClass.ExecuteSQL_Main(Del);
                        if (returnstr != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                }
            }
            else
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยัน ตั้งค่าพนักงานทั้งหมดในสาขา เดือน " + Month + " ปี " + Years + "หรือไม่ ") == DialogResult.No)
                {
                    this.Cursor = Cursors.Default;
                    return;
                }
                //พนักงานทั้งหมดในสาขายกเว้น ผช , ผจก และพนักงานทีมีการกำหนดค่าวันทำงานแล้ว
                //dtEmpl = EmplClass.GetEmployeeAll_ByDptID(RadDropDownList_Branch.SelectedValue.ToString(), "=",
                //    string.Format(@"AND HRPPartyJobTableRelationship.DESCRIPTION not like '%ผู้จัดการ%' "));
                //dtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@" = '{RadDropDownList_Branch.SelectedValue}' ", "  NOT LIKE '%ผู้จัดการ%' ", "");
                dtEmpl = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@"{RadDropDownList_Branch.SelectedValue}", "  NOT LIKE '%ผู้จัดการ%' ", "");
                Del = string.Format(@"DELETE FROM SHOP_SHELFEMPLDATE  
                        WHERE MONTH(DATE_EMPL)= '{0}' and YEAR(DATE_EMPL)= '{1}' 
                        AND BRANCH_ID = '{2}' ",
                 Month,
                 Years,
                 SystemClass.SystemBranchID);
                //''AND DATE_EMPL> '{3}',  DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd")
                string returnstr = ConnectionClass.ExecuteSQL_Main(Del);
                if (returnstr != "")
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }



            ArrayList SqlIns = new ArrayList();
            if (dtEmpl.Rows.Count == 0) return;

            for (int iEmpl = 0; iEmpl < dtEmpl.Rows.Count; iEmpl++)
            {

                String EmplID = dtEmpl.Rows[iEmpl]["EMPLID"].ToString(),
                    EmplName = dtEmpl.Rows[iEmpl]["SPC_NAME"].ToString();

                /////หาพนักงานแต่ละคน ดูแลกี่ชั้นวาง
                DataTable dtShelf = ShelfClass.GetShelfEmplID(EmplID, SystemClass.SystemBranchID);
                if (dtShelf.Rows.Count == 0) continue;
                ////หาตารางกะพนักงานในเดือนนั้นๆ 
                DataTable dtEmplWork = ShelfClass.GetEmplWorkAllDate(EmplID, Years + "-" + Month + "-01");
                if (dtEmplWork.Rows.Count == 0) continue;

                DataTable dtEmplShelf = ShelfClass.GetEmplShelf(SystemClass.SystemBranchID, Month,
                                                  Years, EmplID);

                for (int iEmplWork = 0; iEmplWork < dtEmplWork.Rows.Count; iEmplWork++)
                {
                    String DateWork = DateTime.Parse(dtEmplWork.Rows[iEmplWork]["SHIFTDATE"].ToString()).ToString("yyyy-MM-dd");

                    String DATE_EVENT = "";// dtEmplWork.Rows[iEmplWork]["PWEVENT"].ToString();
                    String DATE_EVENTNAME = "";// dtEmplWork.Rows[iEmplWork]["PWEVENTNAME"].ToString();
                    String SHIFTID = dtEmplWork.Rows[iEmplWork]["SHIFTID"].ToString();



                    for (int iShelf = 0; iShelf < dtShelf.Rows.Count; iShelf++)
                    {

                        String Shelf = dtShelf.Rows[iShelf]["SHELF_ID"].ToString(),
                            ShelfName = dtShelf.Rows[iShelf]["SHELF_NAME"].ToString(),
                            Zone = dtShelf.Rows[iShelf]["ZONE_ID"].ToString();




                        DataRow[] rowDateEmpSetting = dtEmplShelf.Select(
                            string.Format(@"DATE_EMPL='{0}' AND ZONE_ID='{1}' AND SHELF_ID='{2}'",
                            DateWork, Zone, Shelf));
                        if (rowDateEmpSetting.Length > 0) continue;
                        String ins;
                        ////เปลี่ยนให้บันทึกพนักงานทุกวัน แยกสถานะ เพื่อง่ายต่อการตรวจสอบ
                        ins = string.Format(@"insert into SHOP_SHELFEMPLDATE
                                (SHELF_ID, SHELF_NAME, 
                                ZONE_ID, BRANCH_ID, 
                                EMPL_ID, EMPL_NAME, DATE_EMPL, REAMRK,
                                WHOINS, WHONAMEINS, DATEINS, 
                                WHOUP, WHONAMEUP, DATEUP,DATE_EVENT, DATE_EVENTNAME, SHIFTID)
                                values
                                ('{0}', '{1}', 
                                '{2}', '{3}', 
                                '{4}', '{5}', '{6}', '{7}', 
                                '{8}', '{9}', convert(varchar,getdate(),25), 
                                '{8}', '{9}', convert(varchar,getdate(),25),'{10}','{11}','{12}')",
                            Shelf, ShelfName,
                            Zone,
                            SystemClass.SystemBranchID,
                            EmplID, EmplName, DateWork, "",
                            SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                            DATE_EVENT, DATE_EVENTNAME, SHIFTID);
                        SqlIns.Add(ins);

                    }
                }


            }
            string Transction = ConnectionClass.ExecuteSQL_ArrayMain(SqlIns);
            MsgBoxClass.MsgBoxShow_SaveStatus(Transction);
            if (Transction.Equals(""))
            {
                RadButton_Search_Click(sender, e);
            }
            this.Cursor = Cursors.Default;
        }

        private void RadCheckBox_EmplShelf_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ClearData(" ");
            if (RadCheckBox_EmplShelf.Checked == true)
            {
                TypeSetting = "EmplSetting";
                TypeEdit = "DefaultSetting";
            }
            else
            {
                TypeSetting = "";
                TypeEdit = "Default";

            }
            ClearData(TypeEdit);
        }
        //ลบรายการทั้งหมด
        //private void RadButtonElement_ClearAll_Click(object sender, EventArgs e)
        //{
        //    String SqlEx;
        //    //TypePage
        //    //String Branch, Zone, Shelf, ShelfName, Empl, EmplName, DateShelf;
        //      shelfGetSetClass = new ShelfGetSetClass(RadDropDownList_Branch.SelectedValue.ToString(),
        //        TypeCondition,TypeSetting,
        //        RadGridView_Show,RadGridView_ShelfOrEmpl);
        //    if (TypeCondition == "Shelf")
        //    {
        //        if (string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowId) || string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowName))
        //        {
        //            MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุชั้นวางสินค้า ที่ต้องการตั้งค่า ก่อนทุกครั้ง");
        //            return;
        //        }
        //        //Branch = RadDropDownList_Branch.SelectedValue.ToString();
        //        //Zone = RadGridView_ShelfOrEmpl.CurrentRow.Cells["ZONE_ID"].Value.ToString();
        //        //Shelf = RadGridView_Show.CurrentRow.Cells["Shelf_ID"].Value.ToString();
        //        ////ShelfName = IndexRowName;
        //        //Empl = RadGridView_Show.CurrentRow.Cells[0].Value.ToString();
        //        ////EmplName = RadGridView_Show.CurrentRow.Cells[1].Value.ToString();
        //    }
        //    else
        //    {
        //        if (string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowId) || string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowName))
        //        {
        //            MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุพนักงาน ที่ต้องการตั้งค่า ก่อนทุกครั้ง");
        //            return;
        //        }
        //        //Branch = RadDropDownList_Branch.SelectedValue.ToString();
        //        //Zone = RadGridView_Show.CurrentRow.Cells["ZONE_ID"].Value.ToString();
        //        //Shelf = RadGridView_Show.CurrentRow.Cells["Shelf_ID"].Value.ToString();
        //        ////ShelfName = RadGridView_Show.CurrentRow.Cells[1].Value.ToString();
        //        //Empl = shelfGetSetIndexClass.IndexRowId;
        //        //EmplName = IndexRowName;
        //    }

        //    if (string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowId) || string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowName))
        //    {
        //        MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุชั้นวางสินค้า ที่ต้องการตั้งค่า ก่อนทุกครั้ง");
        //        return;
        //    }

        //    if (shelfGetSetIndexClass.IndexRowId != "" && RadGridView_Show.RowCount > 0)
        //    {
        //        SqlEx = string.Format(@"DELETE FROM SHOP_SHELFEMPLDATE
        //                WHERE MONTH(DATE_EMPL) = '{0}' and YEAR(DATE_EMPL)= '{1}'
        //                AND EMPL_ID = '{2}'
        //                AND BRANCH_ID = '{3}' AND DATE_EMPL> '{4}'",
        //                radDropDownList_Month.SelectedValue.ToString(),
        //                radDropDownList_Year.SelectedValue.ToString(),
        //                shelfGetSetClass.EMPL_M, 
        //                shelfGetSetClass.Branch,
        //                DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));

        //        //int Day = DateTime.Now.Day + 1;
        //        //for (int iColumnShow = Day; iColumnShow < RadGridView_Show.Columns.Count; iColumnShow++)
        //        //{
        //        //    if (RadGridView_Show.CurrentRow.Cells[iColumnShow].Value.ToString() != "False")
        //        //    {
        //        //        shelfGetSetClass.DateShelf = radDropDownList_Year.SelectedValue.ToString() + "-" +
        //        //             radDropDownList_Month.SelectedValue.ToString() + "-" +
        //        //             RadGridView_Show.Columns[iColumnShow].Name;

        //        //        SqlEx = string.Format(@"delete FROM SHOP_SHELFEMPLDATE
        //        //                    WHERE BRANCH_ID ='{0}'
        //        //                    AND  ZONE_ID ='{1}'
        //        //                    AND  SHELF_ID ='{2}' 
        //        //                    AND  EMPL_ID='{3}'
        //        //                    AND  DATE_EMPL='{4}'",
        //        //                    shelfGetSetClass.Branch,
        //        //                    shelfGetSetClass.Zone,
        //        //                    shelfGetSetClass.Shelf,
        //        //                    shelfGetSetClass.EMPL_M,
        //        //                    shelfGetSetClass.DateShelf);

        //        string returnstr = ConnectionClass.ExecuteSQL_Main(SqlEx);
        //        if (returnstr == "")
        //        {
        //            MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
        //            //GridViewRowInfo currentRow = RadGridView_ShelfOrEmpl.MasterView.CurrentRow;
        //            //GridViewCellEventArgs eventt = new GridViewCellEventArgs(satir, RadGridView_ShelfOrEmpl.Columns["SirketID"],???);
        //            //RadGridView_ShelfOrEmpl_CellClick(sender, currentRow);
        //            //this.RadGridView_ShelfOrEmpl.CellClick += new System.EventHandler(this.RadGridView_ShelfOrEmpl_CellClick);
        //            //RadGridView_ShelfOrEmpl
        //        }
        //        //        RadGridView_Show.CurrentRow.Cells[iColumnShow].Value = "False";
        //        //    }
        //        //}
        //    }
        //}

        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        //เลือกบาร์โค๊ดได้ 1 รายการ ในการกำหนมค่าสต๊อกตามวันที่
        private void RadGridView_ShelfOrEmpl_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                //เคลียร์บาร์โค๊ดเดิมเพื่อให้ได้สินค่าแค่ 1 รายการ
                if (shelfGetSetIndexClass.IndexRow != -1 && shelfGetSetIndexClass.IndexRow != e.RowIndex)
                {
                    String MessageQuesion = "";
                    if (radioButton_Empl.Checked == true) MessageQuesion = "เลือกพนักงานได้ครั้งละ 1 คน ยืนยันเลือกพนักงาน ตั้งค่าใหม่หรือไม่";
                    if (radioButton_Shelf.Checked == true) MessageQuesion = "เลือกชั้นวางได้ครั้งละ 1 เชลล์ ยืนยันเลือกชั้นวาง ตั้งค่าใหม่หรือไม่";

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult(MessageQuesion) == DialogResult.No)
                    {
                        return;
                    }
                    else
                    {
                        RadGridView_ShelfOrEmpl.Rows[shelfGetSetIndexClass.IndexRow].Cells["CheckShelfEmpl"].Value = "False";
                    }
                }

                //สลับค่า true false ตามที่เลือก
                if (RadGridView_ShelfOrEmpl.CurrentRow.Cells["CheckShelfEmpl"].Value.ToString() == "True")
                {
                    shelfGetSetIndexClass.IndexRowId = string.Empty;
                    shelfGetSetIndexClass.IndexRowName = string.Empty;
                    shelfGetSetIndexClass.IndexRow = -1;

                    //SetShelf_DataGridShowSetting();
                    RadGridView_ShelfOrEmpl.CurrentRow.Cells["CheckShelfEmpl"].Value = "False";
                    if (TypeSetting.Equals("EmplSetting"))
                    {
                        if (RadGridView_Show.RowCount > 0)
                        {
                            for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                            {
                                RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "False";
                            }
                        }
                    }
                    else
                    {
                        if (RadGridView_Show.RowCount > 0)
                        {

                            if (TypeCondition == "Shelf")
                            {
                                for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                {
                                    for (int iCol = 3; iCol < RadGridView_Show.ColumnCount; iCol++)
                                    {
                                        RadGridView_Show.Rows[i].Cells[iCol].Value = "False";
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                {
                                    for (int iCol = 4; iCol < RadGridView_Show.ColumnCount; iCol++)
                                    {
                                        RadGridView_Show.Rows[i].Cells[iCol].Value = "False";
                                    }
                                }
                            }

                        }
                    }
                }

                else
                {
                    string Condition;
                    switch (TypeSetting)
                    {
                        case "EmplSetting":
                            if (TypeCondition == "Shelf")
                            {
                                //SetEmpl_DataGridShowSetting();
                                //ตั้งค่าให้ตัวแปร เพื่อใช้สำหรับค้นหาวันที่ส่งสต๊อกตามบาร์โค๊ดที่เลือก 1 รายการ
                                shelfGetSetIndexClass.IndexRowId = RadGridView_ShelfOrEmpl.CurrentRow.Cells["shelf_id"].Value.ToString();
                                shelfGetSetIndexClass.IndexRowName = RadGridView_ShelfOrEmpl.CurrentRow.Cells["shelf_name"].Value.ToString();
                                shelfGetSetIndexClass.IndexRow = e.RowIndex;
                                RadGridView_ShelfOrEmpl.CurrentRow.Cells[e.ColumnIndex].Value = "True";

                                ////ข้อมูลที่มีการ วันที่ดูแลเชลล์แต่ละวันของพนักงานแต่ลัคน
                                Condition = string.Format(@"and shelf_id='{0}' and zone_id='{1}'", shelfGetSetIndexClass.IndexRowId,
                                    RadGridView_ShelfOrEmpl.CurrentRow.Cells["zone_id"].Value.ToString());
                                DataTable DtDateShelf = ShelfClass.GetShelfEmplSetting(RadDropDownList_Branch.SelectedValue.ToString(),
                                      Condition);
                                if (DtDateShelf.Rows.Count > 0)
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        DataRow[] results = DtDateShelf.Select(String.Format(@"empl_id = '{0}' ",
                                                 RadGridView_Show.Rows[i].Cells["EMPL_ID"].Value.ToString()));
                                        if (results.Length > 0)
                                        {
                                            //กำหนดค่าเพื่อเลือก checked==true
                                            RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "True";
                                        }
                                        else
                                        {
                                            RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "False";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "False";
                                    }
                                }
                            }
                            else if (TypeCondition == "Employee")
                            {
                                //SetShelf_DataGridShowSetting();
                                //ตั้งค่าให้ตัวแปร เพื่อใช้สำหรับค้นหาวันที่ส่งสต๊อกตามบาร์โค๊ดที่เลือก 1 รายการ
                                shelfGetSetIndexClass.IndexRowId = RadGridView_ShelfOrEmpl.CurrentRow.Cells["EMPL_ID"].Value.ToString();
                                shelfGetSetIndexClass.IndexRowName = RadGridView_ShelfOrEmpl.CurrentRow.Cells["SPC_NAME"].Value.ToString();
                                shelfGetSetIndexClass.IndexRow = e.RowIndex;
                                RadGridView_ShelfOrEmpl.CurrentRow.Cells[e.ColumnIndex].Value = "True";

                                ////ข้อมูลที่มีการ วันที่ดูแลเชลล์แต่ละวันของพนักงานแต่ลัคน
                                Condition = string.Format(@"and empl_id='{0}'", shelfGetSetIndexClass.IndexRowId);
                                DataTable DtShelfEmplSetting = ShelfClass.GetShelfEmplSetting(RadDropDownList_Branch.SelectedValue.ToString(),
                                      Condition);
                                if (DtShelfEmplSetting.Rows.Count > 0)
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        DataRow[] results = DtShelfEmplSetting.Select(String.Format(@"shelf_id = '{0}' and ZONE_ID = '{1}'  ",
                                                 RadGridView_Show.Rows[i].Cells["SHELF_ID"].Value.ToString(),
                                                 RadGridView_Show.Rows[i].Cells["ZONE_ID"].Value.ToString()));
                                        if (results.Length > 0)
                                        {
                                            //กำหนดค่าเพื่อเลือก checked==true
                                            RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "True";
                                        }
                                        else
                                        {
                                            RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "False";
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        RadGridView_Show.Rows[i].Cells["SHELF_EMPL"].Value = "False";
                                    }
                                }
                            }
                            break;
                        default:
                            if (TypeCondition == "Shelf")
                            {
                                //ตั้งค่าให้ตัวแปร เพื่อใช้สำหรับค้นหาวันที่ส่งสต๊อกตามบาร์โค๊ดที่เลือก 1 รายการ
                                shelfGetSetIndexClass.IndexRowId = RadGridView_ShelfOrEmpl.CurrentRow.Cells["shelf_id"].Value.ToString();
                                shelfGetSetIndexClass.IndexRowName = RadGridView_ShelfOrEmpl.CurrentRow.Cells["shelf_name"].Value.ToString();
                                shelfGetSetIndexClass.IndexRow = e.RowIndex;
                                shelfGetSetIndexClass.IndexEmplSetting = int.Parse(RadGridView_ShelfOrEmpl.CurrentRow.Cells["EMP_NUM"].Value.ToString());
                                RadGridView_ShelfOrEmpl.CurrentRow.Cells[e.ColumnIndex].Value = "True";
                                //ข้อมูลที่มีการตั้งค่าวันที่ดูแลชั้นวางของพนักงานในเชลล์แต่ละวัน
                                Condition = string.Format(@"and shelf_id='{0}'", shelfGetSetIndexClass.IndexRowId);
                                DataTable DtDateShelf = ShelfClass.GetShelfEmplDate(RadDropDownList_Branch.SelectedValue.ToString(),
                                     radDropDownList_Month.SelectedValue.ToString(),
                                     radDropDownList_Year.SelectedValue.ToString(),
                                     Condition);
                                if (DtDateShelf.Rows.Count > 0)
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        //ตั้งค่าข้อมูลพนักงานเป็น False ในแต่ละ column วันที่ ก่อนกำหนดค่าใหม่ ที่ได้จากการบันทึกก่อนหน้า
                                        for (int iCol = 4; iCol < RadGridView_Show.ColumnCount; iCol++)
                                        {
                                            RadGridView_Show.Rows[i].Cells[iCol].Value = "False";
                                        }
                                        //ค้นหาข้อมูลพนักงาน ตาม shelf และ zone
                                        DataRow[] results = DtDateShelf.Select(String.Format(@"empl_id = '{0}'  AND SHELF_ID = '{1}' AND ZONE_ID = '{2}' ",
                                                 RadGridView_Show.Rows[i].Cells["EMPL_ID"].Value.ToString(),
                                                 RadGridView_ShelfOrEmpl.CurrentRow.Cells["SHELF_ID"].Value.ToString(),
                                                 RadGridView_ShelfOrEmpl.CurrentRow.Cells["ZONE_ID"].Value.ToString()));

                                        if (results.Length > 0)
                                        {
                                            for (int r = 0; r < results.Length; r++)
                                            {
                                                int Columns = int.Parse(results[r].ItemArray[8].ToString());
                                                //กำหนดค่าเพื่อเลือก checked==true
                                                //ถ้าไม่มีข้อมูลวันลา ให้เลือก true
                                                if (!(results[r].ItemArray[10].ToString().Equals("")) && (!results[r].ItemArray[11].ToString().Equals("")))
                                                {
                                                    RadGridView_Show.Rows[i].Cells[Columns].Value = "False";
                                                }
                                                else
                                                {
                                                    RadGridView_Show.Rows[i].Cells[Columns].Value = "True";
                                                }
                                                //RadGridView_Show.Rows[i].Cells[Columns].Value = "True"; 
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        for (int iCol = 3; iCol < RadGridView_Show.ColumnCount; iCol++)
                                        {
                                            RadGridView_Show.Rows[i].Cells[iCol].Value = "False";
                                        }
                                    }

                                }
                            }
                            else if (TypeCondition == "Employee")
                            {
                                //ตั้งค่าให้ตัวแปร เพื่อใช้สำหรับค้นหาวันที่ส่งสต๊อกตามบาร์โค๊ดที่เลือก 1 รายการ
                                shelfGetSetIndexClass.IndexRowId = RadGridView_ShelfOrEmpl.CurrentRow.Cells["EMPL_ID"].Value.ToString();
                                shelfGetSetIndexClass.IndexRowName = RadGridView_ShelfOrEmpl.CurrentRow.Cells["SPC_NAME"].Value.ToString();
                                shelfGetSetIndexClass.IndexRow = e.RowIndex;

                                shelfGetSetIndexClass.IndexEmplSetting = 0;
                                RadGridView_ShelfOrEmpl.CurrentRow.Cells[e.ColumnIndex].Value = "True";

                                //ข้อมูลที่มีการ วันที่ดูแลเชลล์แต่ละวันของพนักงานแต่ลัคน
                                Condition = string.Format(@"and empl_id='{0}'", shelfGetSetIndexClass.IndexRowId);
                                DataTable DtDateShelf = ShelfClass.GetShelfEmplDate(RadDropDownList_Branch.SelectedValue.ToString(),
                                     radDropDownList_Month.SelectedValue.ToString(),
                                     radDropDownList_Year.SelectedValue.ToString(),
                                     Condition);
                                if (DtDateShelf.Rows.Count > 0)
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        for (int iCol = 4; iCol < RadGridView_Show.ColumnCount; iCol++)
                                        {
                                            RadGridView_Show.Rows[i].Cells[iCol].Value = "False";
                                        }

                                        DataRow[] results = DtDateShelf.Select(String.Format(@"shelf_id = '{0}' AND ZONE_ID = '{1}'  ",
                                                 RadGridView_Show.Rows[i].Cells["SHELF_ID"].Value.ToString(),
                                                 RadGridView_Show.Rows[i].Cells["ZONE_ID"].Value.ToString()));

                                        if (results.Length > 0)
                                        {
                                            for (int r = 0; r < results.Length; r++)
                                            {
                                                int Columns = int.Parse(results[r].ItemArray[7].ToString());
                                                //กำหนดค่าเพื่อเลือก checked==true
                                                if (!(results[r].ItemArray[11].ToString().Equals("")) && (!results[r].ItemArray[10].ToString().Equals("")))
                                                {
                                                    RadGridView_Show.Rows[i].Cells[Columns].Value = "False";
                                                }
                                                else
                                                {
                                                    RadGridView_Show.Rows[i].Cells[Columns].Value = "True";
                                                }

                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < RadGridView_Show.Rows.Count; i++)
                                    {
                                        for (int iCol = 4; iCol < RadGridView_Show.ColumnCount; iCol++)
                                        {
                                            RadGridView_Show.Rows[i].Cells[iCol].Value = "False";
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }

        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {

            if (RadGridView_Show.Rows.Count > 0)
            {
                //if (Permission)
                //{
                //    return;
                //}

                if (TypeCondition == "Shelf" && e.ColumnIndex < 2) return;
                if (TypeCondition == "Employee" && e.ColumnIndex < 4) return;


                if (TypeCondition == "Shelf" && (string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowId) || string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowName)))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุชั้นวางสินค้า ที่ต้องการตั้งค่า ก่อนทุกครั้ง");
                    return;
                }
                else if (TypeCondition == "Employee" && (string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowId) || string.IsNullOrEmpty(shelfGetSetIndexClass.IndexRowName)))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาระบุพนักงาน ที่ต้องการตั้งค่า ก่อนทุกครั้ง");
                    return;
                }

                shelfGetSetClass = new ShelfGetSetClass(RadDropDownList_Branch.SelectedValue.ToString(),
                  TypeSetting, TypeCondition,
                  RadGridView_Show, RadGridView_ShelfOrEmpl);
                //String Branch, Zone, Shelf, ShelfName, DateShelf, EMPL_ID, EMPL_NAME;

                switch (TypeSetting)
                {
                    case "EmplSetting":
                        if (shelfGetSetIndexClass.IndexRowId.Equals("")) return;

                        ArrayList SqlInsDel = new ArrayList();
                        String sqlInDel;
                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString() == "True")
                        {
                            //if (ShelfClass.CheckEmplShelf(SystemClass.SystemBranchID, DateTime.Now.Month.ToString(),
                            //    DateTime.Now.Year.ToString(), shelfGetSetClass.Zone, shelfGetSetClass.Shelf, shelfGetSetClass.EMPL_M))
                            if (ShelfClass.CheckEmplShelf(SystemClass.SystemBranchID, Month,
                             Years, shelfGetSetClass.Zone, shelfGetSetClass.Shelf, shelfGetSetClass.EMPL_M))
                            {
                                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("พนักงานมีการตั้งค่าประจำวันแล้ว ต้องการลบเพื่อตั้งค่าใหม่หรือไม่") == DialogResult.No)
                                {
                                    return;
                                }
                            }

                            sqlInDel = string.Format(@"delete from SHOP_SHELFEMPL
                                where BRANCH_ID = '{0}' and ZONE_ID = '{1}' 
                                and SHELF_ID = '{2}' and EMPL_ID = '{3}'",
                                    shelfGetSetClass.Branch,
                                    shelfGetSetClass.Zone,
                                    shelfGetSetClass.Shelf,
                                    shelfGetSetClass.EMPL_M);

                            SqlInsDel.Add(sqlInDel);

                            sqlInDel = string.Format(@"delete from SHOP_SHELFEMPLDATE  
                                WHERE EMPL_ID='{0}'
                                AND BRANCH_ID='{1}' 
                                AND MONTH(DATE_EMPL)='{2}' AND YEAR(DATE_EMPL)='{3}'
                                AND DATE_EMPL>'{4}'
                                AND ZONE_ID='{5}' AND SHELF_ID='{6}'",
                                    shelfGetSetClass.EMPL_M,
                                    shelfGetSetClass.Branch,
                                    DateTime.Now.Month.ToString(),
                                    DateTime.Now.Year.ToString(),
                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"),
                                    shelfGetSetClass.Zone,
                                    shelfGetSetClass.Shelf);
                            SqlInsDel.Add(sqlInDel);



                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "False";
                            shelfGetSetClass.CountEmpl -= 1;
                        }
                        else
                        {
                            if (shelfGetSetClass.EMP_NUM == shelfGetSetClass.CountEmpl.ToString())
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุพนักงานครบตามจำนวนที่กำหนดแล้ว");
                                return;
                            }

                            sqlInDel = string.Format(@"insert into SHOP_SHELFEMPL
                                (SHELF_ID, SHELF_NAME, ZONE_ID, BRANCH_ID, EMPL_ID, EMPL_NAME, 
                                REAMRK, WHOINS, WHONAMEINS, DATEINS, WHOUP, WHONAMEUP, DATEUP)
                                values
                                ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', 
                                '', '{6}', '{7}', convert(varchar,getdate(),25), '{6}', '{7}', convert(varchar,getdate(),25))",
                                shelfGetSetClass.Shelf,
                                shelfGetSetClass.ShelfName,
                                shelfGetSetClass.Zone,
                                shelfGetSetClass.Branch,
                                shelfGetSetClass.EMPL_M,
                                shelfGetSetClass.EMPL_NAME,
                                SystemClass.SystemUserID_M,
                                SystemClass.SystemUserName);
                            SqlInsDel.Add(sqlInDel);

                            RadGridView_Show.CurrentRow.Cells["SHELF_EMPL"].Value = "True";
                            shelfGetSetClass.CountEmpl += 1;
                        }

                        if (TypeCondition == "Shelf")
                        {
                            RadGridView_ShelfOrEmpl.CurrentRow.Cells["CountEmpl"].Value = shelfGetSetClass.CountEmpl;
                        }
                        else
                        {
                            RadGridView_Show.CurrentRow.Cells["CountEmpl"].Value = shelfGetSetClass.CountEmpl;
                        }
                        string TranctionsqlInsDel = ConnectionClass.ExecuteSQL_ArrayMain(SqlInsDel);
                        if (TranctionsqlInsDel != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(TranctionsqlInsDel);
                            RadButton_Search_Click(sender, e);
                        }
                        break;
                    default:

                        DateTime DateOld = DateTime.Now.AddDays(-125).Date;
                        DateTime DateSelect = DateTime.Parse(radDropDownList_Year.SelectedValue.ToString() + "-" +
                                 radDropDownList_Month.SelectedValue.ToString() + "-" +
                                 RadGridView_Show.CurrentColumn.Name.ToString());


                        TimeSpan DateTimespan = DateSelect - DateOld;
                        //String StartDateWork = DateSelect.ToString("yyyy-MM-dd");
                        if (DateTimespan.Days < 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถแก้ไขวันที่ส่งสต๊อกย้อนหลังได้ กรุณาเลือกวันที่ใหม่." + Environment.NewLine +
                                "วันที่ปัจจุบัน " + DateTime.Now.AddDays(0).ToString("dd-MM-yyyy"));
                            return;
                        }


                        String SqlEx;
                        shelfGetSetClass = new ShelfGetSetClass(RadDropDownList_Branch.SelectedValue.ToString(),
                         TypeSetting, TypeCondition,
                         RadGridView_Show, RadGridView_ShelfOrEmpl)
                        {
                            DateShelf = DateSelect.ToString("yyyy-MM-dd")
                        };
                        //ถ้าเป็นวันหยุด แก้อะไรไม่ได้เลย ต้องเลือกอย่างเดียว 
                        if (CheckEmplSettingWork(shelfGetSetClass.Branch,
                            shelfGetSetClass.Zone,
                            shelfGetSetClass.Shelf,
                            shelfGetSetClass.DateShelf,
                            shelfGetSetClass.EMPL_M,
                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString()))
                        {
                            return;
                        }





                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value == null)
                        {
                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "False";
                        }
                        if (RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString() == "True")
                        {
                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = false;
                            if (shelfGetSetClass.DATE_EVENTNAME.Equals(""))
                            {
                                shelfGetSetClass.DATE_EVENT = SystemClass.SystemUserID_M;
                                shelfGetSetClass.DATE_EVENTNAME = "ยกเลิกวันมาทำงาน";
                            }
                            SqlEx = string.Format(@"UPDATE SHOP_SHELFEMPLDATE set 
                                    DATE_EVENT='{5}' , DATE_EVENTNAME='{6}',
                                    WHOUP='{7}', WHONAMEUP='{8}', DATEUP=convert(varchar,getdate(),25) 
                                    WHERE BRANCH_ID ='{0}'
                                    AND  ZONE_ID ='{1}'
                                    AND  SHELF_ID ='{2}' 
                                    AND  EMPL_ID='{3}'
                                    AND  DATE_EMPL='{4}'",
                               shelfGetSetClass.Branch,
                               shelfGetSetClass.Zone,
                               shelfGetSetClass.Shelf,
                               shelfGetSetClass.EMPL_M,
                               shelfGetSetClass.DateShelf,
                               shelfGetSetClass.DATE_EVENT,
                               shelfGetSetClass.DATE_EVENTNAME,
                               SystemClass.SystemUserID_M, SystemClass.SystemUserName);
                            string returnstr = ConnectionClass.ExecuteSQL_Main(SqlEx);
                            if (returnstr != "") MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "False";
                        }
                        else
                        {
                            //DateTime.Now.ToString("yyyy-MM-dd")
                            DataTable dtEmplWork = ShelfClass.GetEmplWorkAllDate(shelfGetSetClass.EMPL_M, Years + "-" + Month + "-01");
                            String SHIFTID = "";
                            shelfGetSetClass.DATE_EVENT = "";
                            shelfGetSetClass.DATE_EVENTNAME = "";
                            if (dtEmplWork.Rows.Count > 0)
                            {
                                SHIFTID = dtEmplWork.Rows[int.Parse(RadGridView_Show.CurrentCell.Data.HeaderText) - 1]["SHIFTID"].ToString();
                                //dtEmplWork.Rows[0]["SHIFTID"].ToString();
                            }
                            //ต้องเช็ค update ก่อนเนื่องจาก PK ซ้ำ

                            SqlEx = string.Format(@"insert into SHOP_SHELFEMPLDATE
                                (SHELF_ID, SHELF_NAME, 
                                ZONE_ID, BRANCH_ID, 
                                EMPL_ID, EMPL_NAME, DATE_EMPL, REAMRK,
                                WHOINS, WHONAMEINS, DATEINS, 
                                WHOUP, WHONAMEUP, DATEUP,
                                DATE_EVENT, DATE_EVENTNAME, SHIFTID)
                                values
                                ('{0}', '{1}', 
                                '{2}', '{3}', 
                                '{4}', '{5}', '{6}', '{7}', 
                                '{8}', '{9}', convert(varchar,getdate(),25), 
                                '{8}', '{9}', convert(varchar,getdate(),25),'','','{10}')",
                                shelfGetSetClass.Shelf,
                                shelfGetSetClass.ShelfName,
                                shelfGetSetClass.Zone,
                                shelfGetSetClass.Branch,
                                shelfGetSetClass.EMPL_M,
                                shelfGetSetClass.EMPL_NAME,
                                shelfGetSetClass.DateShelf, "",
                                SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                                SHIFTID);

                            string returnstr = ConnectionClass.ExecuteSQL_Main(SqlEx);
                            if (returnstr != "")
                            {
                                //MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                                SqlEx = string.Format(@"update SHOP_SHELFEMPLDATE set 
                                SHIFTID='{0}',DATE_EVENT='{1}',DATE_EVENTNAME='{2}',
                                WHOUP='{3}', WHONAMEUP='{4}', DATEUP=convert(varchar,getdate(),25)
                                where EMPL_ID='{5}' AND BRANCH_ID='{6}' AND ZONE_ID='{7}' AND SHELF_ID='{8}' AND DATE_EMPL='{9}' ",
                                   SHIFTID,
                                   shelfGetSetClass.DATE_EVENT,
                                   shelfGetSetClass.DATE_EVENTNAME,
                                   SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                                   shelfGetSetClass.EMPL_M,
                                   RadDropDownList_Branch.SelectedValue.ToString(),
                                   shelfGetSetClass.Zone,
                                   shelfGetSetClass.Shelf,
                                   shelfGetSetClass.DateShelf);
                                returnstr = ConnectionClass.ExecuteSQL_Main(SqlEx);
                                if (returnstr != "")
                                {
                                    MsgBoxClass.MsgBoxShow_SaveStatus(returnstr);
                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                            }

                            RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value = "True";
                        }

                        break;
                }
            }
        }


        //ต้องการตรวจสอบคนทำงาน คนประจำล็อค วันหยุด ก่อนบันทึกค่า มาทำงานประจำล็อคแทนพนักงานประจำ 
        //True หยุด False ทำต่อ
        private bool CheckEmplSettingWork(String Branch, String Zone, String Shelf, String DateSelect, String EMPL_M, String StatusCheckbox)
        {
            bool Rsu = false;
            ////จำนวนพนักงานดูแลล็อค จากการตั้งค่าล็อค
            shelfGetSetIndexClass.IndexEmplSetting = ShelfClass.GetShelfSettingEmpl(
                Branch,
                Zone,
                Shelf);

            //int CountCheckBox = CountCheckboxTrue(RadGridView_Show, e.ColumnIndex);

            ////จำนวนพนักงานประจำล็อค ในแต่ละวัน
            int CountEmplShelf = ShelfClass.GetShelEmplCountDate(
                Branch,
                Zone,
                Shelf,
                DateSelect);

            ////จำนวนพนักงานแทนล็อค ในแต่ละวัน
            int CountEmplInDateShelf = ShelfClass.GetShelEmplReplaceCountDate(
                Branch,
                Zone,
                Shelf,
                DateSelect);


            bool StatusEmpl = ShelfClass.CheckShelEmplStatus(Branch, Zone, Shelf, EMPL_M);

            String EmplStatus = ShelfClass.GetDateSchedule(EMPL_M, DateSelect);




            //วันทำงาน
            if (EmplStatus.Equals("WORK"))
            {
                shelfGetSetClass.DATE_EVENT = SystemClass.SystemUserID_M;
                shelfGetSetClass.DATE_EVENTNAME = string.Empty;

                //เช็คพนักงาน เป็นพนักงานประจำล็อคไหม ถ้าไม่เป็น
                //if (ShelfClass.CheckEmplShelf(EMPL_M, Branch, Zone, Shelf))
                if (StatusEmpl && StatusCheckbox.ToUpper() == "FALSE")
                {
                    //มีพนักงานประจำล็อคในวันที่เลือก
                    if (CountEmplShelf > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                               "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                               "เนื่องจาก ยังมีพนักงานประจำล็อคดูแลในวันที่กำหนด มากกว่า 1 คน");
                        Rsu = true;
                    }
                    else
                    {
                        //มีพนักงาน แทน ล็อคในวันที่เลือก
                        if (CountEmplInDateShelf > 0)
                        {
                            //พนักงานที่เลือกเป็นพนักงานแทนอยู่แล้ว
                            if (ShelfClass.CheckShelEmplReplaceDate(Branch, Zone, Shelf, DateSelect, EMPL_M))
                            {
                                if (StatusCheckbox.ToUpper() == "TRUE")
                                {
                                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("พนักงาน " + EMPL_M + "เป็นพนักงานแทนล็อคอยู่แล้ว" +
                                        System.Environment.NewLine + " ต้องการเลือกพนักงานแทนในล็อคใหม่หรือไม่ ") == DialogResult.No)
                                    {
                                        Rsu = true;
                                    }
                                }
                            }
                            else
                            {

                                MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                                  "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                                  "เนื่องจาก ยังมีพนักงานแทน ดูแลในวันที่กำหนดแล้ว");
                                Rsu = true;
                            }
                        }

                    }


                    //////พนักงานประจำล็อคยังมีจำนวนคนมาทำงานมากกว่า 1 คน
                    //if (CountEmplInDateShelf > 0 && StatusCheckbox.ToUpper() == "FALSE")
                    //{
                    //    MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                    //        "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                    //        "เนื่องจาก ยังมีพนักงานดูแลล็อค มากกว่า 1 คน");
                    //    Rsu = true;
                    //}
                }
                else //ถ้าเป็นพนักงานประจำล็อคแต่ต้องการยกเลิก
                {
                    //if (!(StatusCheckbox.ToUpper() == "FALSE"))
                    if (StatusCheckbox.ToUpper() == "TRUE")
                    {
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("เป็นวันทำงานพนักงาน " + EMPL_M + System.Environment.NewLine +
                         "ต้องการยกเลิกพนักงานดูแลล็อค ในวันที่ " + DateSelect + " หรือไม่ ?") == DialogResult.No)
                        {
                            Rsu = true;
                        }
                    }

                }



            }
            //วันหยุด
            else if (EmplStatus.Equals("DAYOFF"))
            {
                shelfGetSetClass.DATE_EVENT = string.Empty;
                shelfGetSetClass.DATE_EVENTNAME = string.Empty;
                ////เช็คพนักงาน เป็นพนักงานประจำล็อคไหม ถ้าไม่เป็น
                //if (ShelfClass.CheckEmplShelf(EMPL_M, Branch, Zone, Shelf))
                if (StatusEmpl && StatusCheckbox.ToUpper() == "FALSE")
                {
                    ////พนักงานประจำล็อคยังมีจำนวนคนมาทำงานมากกว่า 1 คน
                    if (CountEmplShelf > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                               "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                               "เนื่องจาก ยังมีพนักงานประจำล็อคดูแลในวันที่กำหนด มากกว่า 1 คน");
                        Rsu = true;
                    }
                    else
                    {
                        if (StatusCheckbox.ToUpper() == "FALSE")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                                "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                                "เนื่องจาก เป็นวันหยุดพนักงาน");
                            Rsu = true;
                        }

                    }
                }
                else
                {
                    //if (StatusCheckbox.ToUpper().Equals("TRUE"))
                    //{
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                      "ไม่สามารถยกเลิกวันทำงานได้ เนื่องจากเป็นวันหยุดพนักงาน");
                    //}

                    Rsu = true;
                }
            }
            //วันลาทุกกรณี
            else
            {
                //สถานะเลือก ตั้งค่าวันมาทำงาน ให้เช็ควันลาก่อนเอาออก
                if (StatusCheckbox.ToUpper().Equals("TRUE"))
                {
                    String[] ResultArray;
                    ResultArray = EmplStatus.ToString().Split("|");

                    if (ResultArray.Length > 0)
                    {
                        //ลากิจครึ่งวัน ยังได้เงินแก้ไข ไม่ได้
                        if (!(ResultArray[2].ToString().Equals("FULL-DAY")))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                                "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                                "เนื่องจาก พนักงานไม่ได้ลากิจเต็มวัน " + EmplStatus);
                            Rsu = true;
                        }
                        else  //ลากิจทั้งวัน ส่งมาแก้ไขได้
                        {
                            shelfGetSetClass.DATE_EVENT = ResultArray[0].ToString();
                            shelfGetSetClass.DATE_EVENTNAME = ResultArray[1].ToString();

                        }
                    }
                }
                else // สถานะไม่เลือก ไม่ต้องเช็ตเพราะเป็นวันลา ไม่ต้องเลือก
                {
                    shelfGetSetClass.DATE_EVENT = SystemClass.SystemUserID_M;
                    shelfGetSetClass.DATE_EVENTNAME = EmplStatus;
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation(
                               "ไม่สามารถระบุพนักงานดูแลล็อคเพิ่มเติมได้" + System.Environment.NewLine +
                               "เนื่องจาก เป็นวันลาพนักงาน " + EmplStatus);
                    Rsu = true;
                }
            }
            return Rsu;
        }

        private void RadCheckBox_EmplHR_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {

        }

        private void RadDropDownList_Year_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Year.SelectedValue != null)
            {
                Years = radDropDownList_Year.SelectedValue.ToString();
            }
        }

        private void RadDropDownList_Month_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Month.SelectedValue != null)
            {
                Month = radDropDownList_Month.SelectedValue.ToString();
            }
        }

        private void RadButtonElement_WorkUser_Click(object sender, EventArgs e)
        {
            if (!(TypeSetting.Equals("EmplSetting")) && TypeCondition.Equals("Employee"))
            {
                if (shelfGetSetIndexClass.IndexRowId == null)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("เลือกพนักงานที่ต้องการตรวจสอบก่อนทุกครั้ง");
                    return;
                }

                if (!(ShelfClass.CheckEmplMonthSetting(RadDropDownList_Branch.SelectedValue.ToString(),
                 Month, Years, shelfGetSetIndexClass.IndexRowId)))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ยังไม่ได้ตั้งค่าพนักงานทำงานในเดือน ไม่สามารถแก้ไขกะได้");
                    return;
                }

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการตรวจสอบวันหยุดพนักงานใหม่ หรือไม่") == DialogResult.No)
                {
                    return;
                }

                DataTable DtEmpWorkTime = ShelfClass.GetEmplWorkAllDate(shelfGetSetIndexClass.IndexRowId,
                    radDropDownList_Year.SelectedValue.ToString() + "-" + radDropDownList_Month.SelectedValue.ToString() + "-01");

                if (DtEmpWorkTime.Rows.Count > 0)
                {
                    ArrayList SqlIns = new ArrayList();
                    string BranhID = RadDropDownList_Branch.SelectedValue.ToString(),
                         Month = radDropDownList_Month.SelectedValue.ToString(),
                         Year = radDropDownList_Year.SelectedValue.ToString();

                    //ข้อมูลที่มีการ วันที่ดูแลเชลล์แต่ละวันของพนักงานแต่ลัคน
                    string Condition = string.Format(@"and empl_id='{0}'", shelfGetSetIndexClass.IndexRowId);
                    DataTable DtDateShelf = ShelfClass.GetShelfEmplDate(BranhID,
                         Month,
                         Year,
                         Condition);
                    if (DtDateShelf.Rows.Count > 0)
                    {
                        for (int iRowEmpWork = 0; iRowEmpWork < DtEmpWorkTime.Rows.Count - 1; iRowEmpWork++)
                        {
                            string SHIFTDATE = DateTime.Parse(DtEmpWorkTime.Rows[iRowEmpWork]["SHIFTDATE"].ToString()).ToString("yyyy-MM-dd"),
                                SHIFTID = DtEmpWorkTime.Rows[iRowEmpWork]["SHIFTID"].ToString();
                            DataRow[] rowDateShelf = DtDateShelf.Select(string.Format(@"DATE_EMPL = '{0}'  AND SHIFTID <> '{1}'",
                               SHIFTDATE, SHIFTID));

                            if (rowDateShelf.Length == 0)
                            {
                                continue;
                            }
                            else
                            {
                                string sqlUp = string.Format(@"update SHOP_SHELFEMPLDATE set SHIFTID = '{0}'
                                        where BRANCH_ID = '{1}'
                                        AND EMPL_ID = '{2}'
                                        and DATE_EMPL = '{3}'",
                                    SHIFTID, BranhID, shelfGetSetIndexClass.IndexRowId, SHIFTDATE);
                                SqlIns.Add(sqlUp);
                            }

                        }
                        string Transction = ConnectionClass.ExecuteSQL_ArrayMain(SqlIns);
                        MsgBoxClass.MsgBoxShow_SaveStatus(Transction);
                    }
                }
            }
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Barcode_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion
    }


}
