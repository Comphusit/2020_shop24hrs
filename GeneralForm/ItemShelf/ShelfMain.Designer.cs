﻿namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    partial class ShelfMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShelfMain));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radGroupBox_Shelf = new Telerik.WinControls.UI.RadGroupBox();
            this.radTextBox_Image = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Emp = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Rate = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_statusshelf = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Shelf_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_UpdateShelf = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancelshelf = new Telerik.WinControls.UI.RadButton();
            this.radStatusStrip_Shelf = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Addshelf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Editshelf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radTextBox_Shelfname = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Zone = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Branch_shelf = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox_Zone = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel_statuszone = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Zone_id = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_UpdateZone = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancelzone = new Telerik.WinControls.UI.RadButton();
            this.radStatusStrip_Zone = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Addzone = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Editzone = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Document = new Telerik.WinControls.UI.RadButtonElement();
            this.radTextBox_Zonename = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Branch_zone = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Shelf = new Telerik.WinControls.UI.RadGridView();
            this.RadGridView_Zone = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Shelf)).BeginInit();
            this.radGroupBox_Shelf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Image)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Rate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_statusshelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Shelf_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_UpdateShelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancelshelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Shelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Shelfname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch_shelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Zone)).BeginInit();
            this.radGroupBox_Zone.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_statuszone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Zone_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_UpdateZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancelzone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Zonename)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch_zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Zone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Zone.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radGroupBox_Shelf, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.radGroupBox_Zone, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Shelf, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Zone, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(874, 663);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radGroupBox_Shelf
            // 
            this.radGroupBox_Shelf.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Shelf.Controls.Add(this.radTextBox_Image);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel9);
            this.radGroupBox_Shelf.Controls.Add(this.radTextBox_Emp);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel7);
            this.radGroupBox_Shelf.Controls.Add(this.radTextBox_Rate);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel8);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel_statusshelf);
            this.radGroupBox_Shelf.Controls.Add(this.radTextBox_Shelf_id);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel6);
            this.radGroupBox_Shelf.Controls.Add(this.radButton_UpdateShelf);
            this.radGroupBox_Shelf.Controls.Add(this.radButton_Cancelshelf);
            this.radGroupBox_Shelf.Controls.Add(this.radStatusStrip_Shelf);
            this.radGroupBox_Shelf.Controls.Add(this.radTextBox_Shelfname);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel4);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel2);
            this.radGroupBox_Shelf.Controls.Add(this.radDropDownList_Zone);
            this.radGroupBox_Shelf.Controls.Add(this.radDropDownList_Branch_shelf);
            this.radGroupBox_Shelf.Controls.Add(this.radLabel3);
            this.radGroupBox_Shelf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox_Shelf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_Shelf.HeaderText = "Shelf";
            this.radGroupBox_Shelf.Location = new System.Drawing.Point(577, 260);
            this.radGroupBox_Shelf.Name = "radGroupBox_Shelf";
            this.radGroupBox_Shelf.Size = new System.Drawing.Size(294, 379);
            this.radGroupBox_Shelf.TabIndex = 2;
            this.radGroupBox_Shelf.Text = "Shelf";
            // 
            // radTextBox_Image
            // 
            this.radTextBox_Image.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Image.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Image.Location = new System.Drawing.Point(106, 300);
            this.radTextBox_Image.MaxLength = 256;
            this.radTextBox_Image.Name = "radTextBox_Image";
            this.radTextBox_Image.Size = new System.Drawing.Size(60, 21);
            this.radTextBox_Image.TabIndex = 7;
            this.radTextBox_Image.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_Image.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Emp_KeyPress);
            this.radTextBox_Image.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Image_KeyUp);
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(8, 300);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(48, 19);
            this.radLabel9.TabIndex = 84;
            this.radLabel9.Text = "รูปถ่าย";
            // 
            // radTextBox_Emp
            // 
            this.radTextBox_Emp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Emp.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Emp.Location = new System.Drawing.Point(106, 270);
            this.radTextBox_Emp.MaxLength = 256;
            this.radTextBox_Emp.Name = "radTextBox_Emp";
            this.radTextBox_Emp.Size = new System.Drawing.Size(60, 21);
            this.radTextBox_Emp.TabIndex = 6;
            this.radTextBox_Emp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_Emp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Emp_KeyPress);
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(8, 270);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(92, 19);
            this.radLabel7.TabIndex = 82;
            this.radLabel7.Text = "จำนวนคนดูแล";
            // 
            // radTextBox_Rate
            // 
            this.radTextBox_Rate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Rate.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Rate.Location = new System.Drawing.Point(106, 236);
            this.radTextBox_Rate.MaxLength = 256;
            this.radTextBox_Rate.Name = "radTextBox_Rate";
            this.radTextBox_Rate.Size = new System.Drawing.Size(60, 21);
            this.radTextBox_Rate.TabIndex = 5;
            this.radTextBox_Rate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            this.radTextBox_Rate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Rate_KeyPress);
            this.radTextBox_Rate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Rate_KeyUp);
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(8, 238);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(78, 19);
            this.radLabel8.TabIndex = 80;
            this.radLabel8.Text = "ค่าคอม[%]";
            // 
            // radLabel_statusshelf
            // 
            this.radLabel_statusshelf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_statusshelf.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_statusshelf.Location = new System.Drawing.Point(207, 63);
            this.radLabel_statusshelf.Name = "radLabel_statusshelf";
            this.radLabel_statusshelf.Size = new System.Drawing.Size(29, 19);
            this.radLabel_statusshelf.TabIndex = 78;
            this.radLabel_statusshelf.Text = "เพิ่ม";
            // 
            // radTextBox_Shelf_id
            // 
            this.radTextBox_Shelf_id.Enabled = false;
            this.radTextBox_Shelf_id.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Shelf_id.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Shelf_id.Location = new System.Drawing.Point(106, 158);
            this.radTextBox_Shelf_id.Name = "radTextBox_Shelf_id";
            this.radTextBox_Shelf_id.Size = new System.Drawing.Size(60, 21);
            this.radTextBox_Shelf_id.TabIndex = 3;
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(8, 160);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(32, 19);
            this.radLabel6.TabIndex = 77;
            this.radLabel6.Text = "รหัส";
            // 
            // radButton_UpdateShelf
            // 
            this.radButton_UpdateShelf.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_UpdateShelf.Location = new System.Drawing.Point(11, 342);
            this.radButton_UpdateShelf.Name = "radButton_UpdateShelf";
            this.radButton_UpdateShelf.Size = new System.Drawing.Size(135, 32);
            this.radButton_UpdateShelf.TabIndex = 8;
            this.radButton_UpdateShelf.Text = "บันทึก";
            this.radButton_UpdateShelf.ThemeName = "Fluent";
            this.radButton_UpdateShelf.Click += new System.EventHandler(this.RadButton_AddShelf_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_UpdateShelf.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_UpdateShelf.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_UpdateShelf.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateShelf.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateShelf.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateShelf.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateShelf.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateShelf.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancelshelf
            // 
            this.radButton_Cancelshelf.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancelshelf.Location = new System.Drawing.Point(150, 342);
            this.radButton_Cancelshelf.Name = "radButton_Cancelshelf";
            this.radButton_Cancelshelf.Size = new System.Drawing.Size(135, 32);
            this.radButton_Cancelshelf.TabIndex = 9;
            this.radButton_Cancelshelf.Text = "ยกเลิก";
            this.radButton_Cancelshelf.ThemeName = "Fluent";
            this.radButton_Cancelshelf.Click += new System.EventHandler(this.RadButton_Cancelshelf_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancelshelf.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancelshelf.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelshelf.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelshelf.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelshelf.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelshelf.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelshelf.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radStatusStrip_Shelf
            // 
            this.radStatusStrip_Shelf.AutoSize = false;
            this.radStatusStrip_Shelf.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip_Shelf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip_Shelf.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Addshelf,
            this.commandBarSeparator3,
            this.radButtonElement_Editshelf,
            this.commandBarSeparator4});
            this.radStatusStrip_Shelf.Location = new System.Drawing.Point(2, 18);
            this.radStatusStrip_Shelf.Name = "radStatusStrip_Shelf";
            this.radStatusStrip_Shelf.Size = new System.Drawing.Size(290, 42);
            this.radStatusStrip_Shelf.TabIndex = 0;
            // 
            // radButtonElement_Addshelf
            // 
            this.radButtonElement_Addshelf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Addshelf.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Addshelf.Name = "radButtonElement_Addshelf";
            this.radStatusStrip_Shelf.SetSpring(this.radButtonElement_Addshelf, false);
            this.radButtonElement_Addshelf.Text = "radButtonElement1";
            this.radButtonElement_Addshelf.ToolTipText = "เพิ่ม";
            this.radButtonElement_Addshelf.UseCompatibleTextRendering = false;
            this.radButtonElement_Addshelf.Click += new System.EventHandler(this.RadButtonElement_AddShelf_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip_Shelf.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Editshelf
            // 
            this.radButtonElement_Editshelf.AutoSize = true;
            this.radButtonElement_Editshelf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Editshelf.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Editshelf.Name = "radButtonElement_Editshelf";
            this.radStatusStrip_Shelf.SetSpring(this.radButtonElement_Editshelf, false);
            this.radButtonElement_Editshelf.Text = "radButtonElement2";
            this.radButtonElement_Editshelf.ToolTipText = "แก้ไข";
            this.radButtonElement_Editshelf.UseCompatibleTextRendering = false;
            this.radButtonElement_Editshelf.Click += new System.EventHandler(this.RadButtonElement_EditShelf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip_Shelf.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radTextBox_Shelfname
            // 
            this.radTextBox_Shelfname.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Shelfname.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Shelfname.Location = new System.Drawing.Point(106, 197);
            this.radTextBox_Shelfname.Name = "radTextBox_Shelfname";
            this.radTextBox_Shelfname.Size = new System.Drawing.Size(173, 21);
            this.radTextBox_Shelfname.TabIndex = 4;
            this.radTextBox_Shelfname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(8, 199);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 19);
            this.radLabel4.TabIndex = 70;
            this.radLabel4.Text = "ชื่อชั้นวาง";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(8, 88);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(39, 19);
            this.radLabel2.TabIndex = 66;
            this.radLabel2.Text = "สาขา";
            // 
            // radDropDownList_Zone
            // 
            this.radDropDownList_Zone.DropDownAnimationEnabled = false;
            this.radDropDownList_Zone.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Zone.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Zone.Location = new System.Drawing.Point(106, 122);
            this.radDropDownList_Zone.Name = "radDropDownList_Zone";
            this.radDropDownList_Zone.Size = new System.Drawing.Size(173, 21);
            this.radDropDownList_Zone.TabIndex = 2;
            this.radDropDownList_Zone.Text = "โซน";
            // 
            // radDropDownList_Branch_shelf
            // 
            this.radDropDownList_Branch_shelf.DropDownAnimationEnabled = false;
            this.radDropDownList_Branch_shelf.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Branch_shelf.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Branch_shelf.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Branch_shelf.Location = new System.Drawing.Point(106, 88);
            this.radDropDownList_Branch_shelf.Name = "radDropDownList_Branch_shelf";
            this.radDropDownList_Branch_shelf.Size = new System.Drawing.Size(173, 21);
            this.radDropDownList_Branch_shelf.TabIndex = 1;
            this.radDropDownList_Branch_shelf.Text = "radDropDownList1";
            this.radDropDownList_Branch_shelf.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_BranchSh_SelectedValueChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(8, 122);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(33, 19);
            this.radLabel3.TabIndex = 68;
            this.radLabel3.Text = "โซน";
            // 
            // radGroupBox_Zone
            // 
            this.radGroupBox_Zone.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_Zone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.radGroupBox_Zone.Controls.Add(this.radLabel_statuszone);
            this.radGroupBox_Zone.Controls.Add(this.radTextBox_Zone_id);
            this.radGroupBox_Zone.Controls.Add(this.radLabel5);
            this.radGroupBox_Zone.Controls.Add(this.radButton_UpdateZone);
            this.radGroupBox_Zone.Controls.Add(this.radButton_Cancelzone);
            this.radGroupBox_Zone.Controls.Add(this.radStatusStrip_Zone);
            this.radGroupBox_Zone.Controls.Add(this.radTextBox_Zonename);
            this.radGroupBox_Zone.Controls.Add(this.radLabel_Date);
            this.radGroupBox_Zone.Controls.Add(this.radDropDownList_Branch_zone);
            this.radGroupBox_Zone.Controls.Add(this.radLabel1);
            this.radGroupBox_Zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_Zone.HeaderText = "Zone";
            this.radGroupBox_Zone.Location = new System.Drawing.Point(577, 3);
            this.radGroupBox_Zone.Name = "radGroupBox_Zone";
            this.radGroupBox_Zone.Size = new System.Drawing.Size(294, 251);
            this.radGroupBox_Zone.TabIndex = 1;
            this.radGroupBox_Zone.Text = "Zone";
            // 
            // radLabel_statuszone
            // 
            this.radLabel_statuszone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_statuszone.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_statuszone.Location = new System.Drawing.Point(237, 68);
            this.radLabel_statuszone.Name = "radLabel_statuszone";
            this.radLabel_statuszone.Size = new System.Drawing.Size(29, 19);
            this.radLabel_statuszone.TabIndex = 73;
            this.radLabel_statuszone.Text = "เพิ่ม";
            // 
            // radTextBox_Zone_id
            // 
            this.radTextBox_Zone_id.Enabled = false;
            this.radTextBox_Zone_id.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Zone_id.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Zone_id.Location = new System.Drawing.Point(106, 124);
            this.radTextBox_Zone_id.Name = "radTextBox_Zone_id";
            this.radTextBox_Zone_id.Size = new System.Drawing.Size(60, 21);
            this.radTextBox_Zone_id.TabIndex = 2;
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(11, 126);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(32, 19);
            this.radLabel5.TabIndex = 70;
            this.radLabel5.Text = "รหัส";
            // 
            // radButton_UpdateZone
            // 
            this.radButton_UpdateZone.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_UpdateZone.Location = new System.Drawing.Point(11, 205);
            this.radButton_UpdateZone.Name = "radButton_UpdateZone";
            this.radButton_UpdateZone.Size = new System.Drawing.Size(135, 32);
            this.radButton_UpdateZone.TabIndex = 4;
            this.radButton_UpdateZone.Text = "บันทึก";
            this.radButton_UpdateZone.ThemeName = "Fluent";
            this.radButton_UpdateZone.Click += new System.EventHandler(this.RadButton_AddZone_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_UpdateZone.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_UpdateZone.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_UpdateZone.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateZone.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateZone.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateZone.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateZone.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_UpdateZone.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancelzone
            // 
            this.radButton_Cancelzone.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancelzone.Location = new System.Drawing.Point(150, 205);
            this.radButton_Cancelzone.Name = "radButton_Cancelzone";
            this.radButton_Cancelzone.Size = new System.Drawing.Size(135, 32);
            this.radButton_Cancelzone.TabIndex = 5;
            this.radButton_Cancelzone.Text = "ยกเลิก";
            this.radButton_Cancelzone.ThemeName = "Fluent";
            this.radButton_Cancelzone.Click += new System.EventHandler(this.RadButton_Cancelzone_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancelzone.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancelzone.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelzone.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelzone.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelzone.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelzone.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancelzone.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radStatusStrip_Zone
            // 
            this.radStatusStrip_Zone.AutoSize = false;
            this.radStatusStrip_Zone.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip_Zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip_Zone.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Addzone,
            this.commandBarSeparator1,
            this.radButtonElement_Editzone,
            this.commandBarSeparator2,
            this.radButtonElement_Document});
            this.radStatusStrip_Zone.Location = new System.Drawing.Point(2, 18);
            this.radStatusStrip_Zone.Name = "radStatusStrip_Zone";
            this.radStatusStrip_Zone.Size = new System.Drawing.Size(290, 42);
            this.radStatusStrip_Zone.TabIndex = 0;
            // 
            // radButtonElement_Addzone
            // 
            this.radButtonElement_Addzone.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Addzone.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Addzone.Name = "radButtonElement_Addzone";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Addzone, false);
            this.radButtonElement_Addzone.Text = "radButtonElement1";
            this.radButtonElement_Addzone.UseCompatibleTextRendering = false;
            this.radButtonElement_Addzone.Click += new System.EventHandler(this.RadButtonElement_Addzone_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip_Zone.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Editzone
            // 
            this.radButtonElement_Editzone.AutoSize = true;
            this.radButtonElement_Editzone.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Editzone.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Editzone.Name = "radButtonElement_Editzone";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Editzone, false);
            this.radButtonElement_Editzone.Text = "radButtonElement2";
            this.radButtonElement_Editzone.UseCompatibleTextRendering = false;
            this.radButtonElement_Editzone.Click += new System.EventHandler(this.RadButtonElement_Editzone_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Zone.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Document
            // 
            this.radButtonElement_Document.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Document.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_Document.Name = "radButtonElement_Document";
            this.radStatusStrip_Zone.SetSpring(this.radButtonElement_Document, false);
            this.radButtonElement_Document.Text = "";
            this.radButtonElement_Document.ToolTipText = "Refresh";
            this.radButtonElement_Document.UseCompatibleTextRendering = false;
            this.radButtonElement_Document.Click += new System.EventHandler(this.RadButtonElement_Document_Click);
            // 
            // radTextBox_Zonename
            // 
            this.radTextBox_Zonename.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Zonename.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Zonename.Location = new System.Drawing.Point(106, 163);
            this.radTextBox_Zonename.Name = "radTextBox_Zonename";
            this.radTextBox_Zonename.Size = new System.Drawing.Size(170, 21);
            this.radTextBox_Zonename.TabIndex = 3;
            this.radTextBox_Zonename.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Key_Enter);
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(11, 93);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(39, 19);
            this.radLabel_Date.TabIndex = 60;
            this.radLabel_Date.Text = "สาขา";
            // 
            // radDropDownList_Branch_zone
            // 
            this.radDropDownList_Branch_zone.DropDownAnimationEnabled = false;
            this.radDropDownList_Branch_zone.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Branch_zone.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Branch_zone.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Branch_zone.Location = new System.Drawing.Point(106, 93);
            this.radDropDownList_Branch_zone.Name = "radDropDownList_Branch_zone";
            this.radDropDownList_Branch_zone.Size = new System.Drawing.Size(170, 21);
            this.radDropDownList_Branch_zone.TabIndex = 1;
            this.radDropDownList_Branch_zone.Text = "radDropDownList1";
            this.radDropDownList_Branch_zone.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Branch_SelectedValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(11, 165);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(51, 19);
            this.radLabel1.TabIndex = 63;
            this.radLabel1.Text = "ชื่อโซน";
            // 
            // RadGridView_Shelf
            // 
            this.RadGridView_Shelf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.RadGridView_Shelf.Cursor = System.Windows.Forms.Cursors.Default;
            this.RadGridView_Shelf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Shelf.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.RadGridView_Shelf.ForeColor = System.Drawing.Color.Black;
            this.RadGridView_Shelf.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.RadGridView_Shelf.Location = new System.Drawing.Point(3, 260);
            // 
            // 
            // 
            this.RadGridView_Shelf.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Shelf.Name = "RadGridView_Shelf";
            this.RadGridView_Shelf.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RadGridView_Shelf.Size = new System.Drawing.Size(568, 379);
            this.RadGridView_Shelf.TabIndex = 17;
            this.RadGridView_Shelf.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Shelf.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.RadGridView_Shelf_CurrentRowChanged);
            this.RadGridView_Shelf.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Shelf.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Shelf.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // RadGridView_Zone
            // 
            this.RadGridView_Zone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Zone.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Zone.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Zone.Name = "RadGridView_Zone";
            this.RadGridView_Zone.Size = new System.Drawing.Size(568, 251);
            this.RadGridView_Zone.TabIndex = 67;
            this.RadGridView_Zone.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Zone.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.RadGridView_Zone_CurrentRowChanged);
            this.RadGridView_Zone.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Zone.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Zone.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 645);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(568, 15);
            this.radLabel_Detail.TabIndex = 68;
            // 
            // ShelfMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 663);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "ShelfMain";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ตั้งค่าโซนสินค้า";
            this.Load += new System.EventHandler(this.ShelfMain_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Shelf)).EndInit();
            this.radGroupBox_Shelf.ResumeLayout(false);
            this.radGroupBox_Shelf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Image)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Rate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_statusshelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Shelf_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_UpdateShelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancelshelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Shelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Shelfname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch_shelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_Zone)).EndInit();
            this.radGroupBox_Zone.ResumeLayout(false);
            this.radGroupBox_Zone.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_statuszone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Zone_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_UpdateZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancelzone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Zonename)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch_zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Zone.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Zone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Branch_zone;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Shelf;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Zone;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Branch_shelf;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox_Zone;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Zonename;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Shelfname;
        private Telerik.WinControls.UI.RadGridView RadGridView_Shelf;
        private Telerik.WinControls.UI.RadGridView RadGridView_Zone;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Zone;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Addzone;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Editzone;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Document;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Shelf;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Addshelf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Editshelf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        protected Telerik.WinControls.UI.RadButton radButton_Cancelzone;
        protected Telerik.WinControls.UI.RadButton radButton_UpdateZone;
        protected Telerik.WinControls.UI.RadButton radButton_UpdateShelf;
        protected Telerik.WinControls.UI.RadButton radButton_Cancelshelf;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Zone_id;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Shelf_id;
        private Telerik.WinControls.UI.RadLabel radLabel_statuszone;
        private Telerik.WinControls.UI.RadLabel radLabel_statusshelf;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Rate;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Emp;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Image;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}
