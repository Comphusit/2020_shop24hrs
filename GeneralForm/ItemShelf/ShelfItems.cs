﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfItems : Telerik.WinControls.UI.RadForm
    {
        DataTable dtItems = new DataTable();
        string zoneID_old, zoneID;
        string sheftID_old, sheftID;
        string bchId, bchName;
        public ShelfItems()
        {
            InitializeComponent();
        }



        private void ShelfItems_Load(object sender, EventArgs e)
        {

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มข้อมูลสินค้าในชั้นวาง";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "ลบข้อมูลสินค้าในชั้นวาง";

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Bch);
            DatagridClass.SetDefaultRadGridView(RadGridView_Shelf);
            DatagridClass.SetDefaultRadGridView(radGridView_Item);

            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก"));
            RadGridView_Shelf.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัสโซน", 60)));
            RadGridView_Shelf.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 90)));
            RadGridView_Shelf.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "รหัสชั้นวาง", 80)));
            RadGridView_Shelf.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 150)));
            RadGridView_Shelf.MasterTemplate.Columns["C"].IsPinned = true;

            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            radGridView_Item.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            radGridView_Item.Columns["UNITID"].AllowFiltering = false;
            radGridView_Item.Columns["SPC_ITEMNAME"].AllowFiltering = false;
            //RadGridView_Shelf.Columns["ZONE_ID"].AllowFiltering = false;

            if (SystemClass.SystemBranchID == "MN000")
            {
                radLabel_Detail.Text = "ระบุสาขา | เลือก ชั้นวาง เพื่อดูสินค้าทั้งหมด | กด + >> เพิ่มสินค้า ในชั้นวางที่เลือก | กด / >> ลบสินค้า ในชั้นวางที่เลือก";
            }
            else
            {
                radLabel_Detail.Text = "เลือก ชั้นวาง เพื่อดูสินค้าทั้งหมด | กด + >> เพิ่มสินค้า ในชั้นวางที่เลือก | กด / >> ลบสินค้า ในชั้นวางที่เลือก";
            }

            SetBranch();
            SetSHELF();
            ClearTxt();
        }

        void ClearTxt()
        {

            zoneID_old = "";
            sheftID_old = "";
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Enabled = false;
            radLabel_Unit.Text = "";
            radLabel_Name.Text = "";
            Button_Save.Enabled = false;
            radTextBox_Barcode.Focus();
        }

        //Set Branch
        void SetBranch()
        {
            if (SystemClass.SystemBranchID == "MN000")
            {
                radDropDownList_Bch.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
            }
            else
            {
                radDropDownList_Bch.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
            }
            radDropDownList_Bch.DisplayMember = "NAME_BRANCH";
            radDropDownList_Bch.ValueMember = "BRANCH_ID";
            radDropDownList_Bch.SelectedIndex = 0;
        }
        //DataChange
        void SetSHELF()
        {
            string bch = radDropDownList_Bch.SelectedValue.ToString();
            string sql = string.Format(@"
                SELECT	'0' AS C,SHOP_SHELFZONE.ZONE_ID,SHOP_SHELFZONE.ZONE_NAME,SHOP_SHELF.SHELF_ID,SHOP_SHELF.SHELF_NAME
                FROM	SHOP_SHELF WITH (NOLOCK) 
		                INNER JOIN SHOP_SHELFZONE WITH (NOLOCK)  ON SHOP_SHELF.ZONE_ID =  SHOP_SHELFZONE.ZONE_ID
                WHERE	SHOP_SHELF.BRANCH_ID = '" + bch + @"' AND SHOP_SHELFZONE.BRANCH_ID = '" + bch + @"'
                ORDER BY SHOP_SHELFZONE.ZONE_ID,SHOP_SHELF.SHELF_ID");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_Shelf.DataSource = dt;
            dt.AcceptChanges();
        }
        //Shoe Items
        void SetItem(string pZoneID, string pShelfID)
        {
            if (dtItems.Rows.Count > 0)
            {
                dtItems.Rows.Clear(); dtItems.AcceptChanges();
            }
            string bch = radDropDownList_Bch.SelectedValue.ToString();
            string sql = $@"
                SELECT	INVENTITEMBARCODE.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME,INVENTITEMBARCODE.UNITID
                FROM	SHOP_SHELFITEMS	WITH (NOLOCK)
		                INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_SHELFITEMS.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE
                WHERE	SHELF_ID = '{pShelfID}' AND ZONE_ID = '{pZoneID}' AND BRANCH_ID = '{bch}'
                ORDER BY INVENTITEMBARCODE.ITEMBARCODE,INVENTITEMBARCODE.SPC_ITEMNAME ";
            dtItems = ItembarcodeClass.GetSHELF_GetItemAll(pShelfID, pZoneID, bch); //ConnectionClass.SelectSQL_Main(sql);
            radGridView_Item.DataSource = dtItems;
            dtItems.AcceptChanges();
        }
        //Check Items
        string CheckItem(string pItembarcode)
        {
            string bch = radDropDownList_Bch.SelectedValue.ToString();
            string sql = string.Format(@"
                SELECT	ITEMBARCODE,UNITID,SHELF_ID,ZONE_ID 
                FROM	SHOP_SHELFITEMS	WITH (NOLOCK)
                WHERE	ITEMBARCODE = '" + pItembarcode + @"'  AND BRANCH_ID = '" + bch + @"'
            ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0)
            {
                return "";
            }
            else
            {
                zoneID_old = dt.Rows[0]["ZONE_ID"].ToString();
                sheftID_old = dt.Rows[0]["SHELF_ID"].ToString();
                return "ชั้นวาง " + dt.Rows[0]["SHELF_ID"].ToString() + " โซน " + dt.Rows[0]["ZONE_ID"].ToString();
            }
        }
        //Select Change
        private void RadDropDownList_Type_SelectedValueChanged(object sender, EventArgs e)
        {
            SetSHELF();
            SetItem("", "");
            radStatusStrip1.Enabled = false;
            radTextBox_Barcode.Enabled = false;

            try
            {
                bchId = radDropDownList_Bch.SelectedValue.ToString();
                bchName = BranchClass.GetBranchNameByID(bchId);
            }
            catch (Exception)
            {
                bchId = "";
                bchName = "";

            }

        }

        #region "Formatting"
        private void RadGridView_Bch_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Bch_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Bch_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Bch_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Item_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Item_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Item_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Item_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        #endregion

        //ClaerZoneID
        void ClearZone(int rowsID)
        {
            zoneID = "";
            sheftID = "";
            RadGridView_Shelf.Rows[rowsID].Cells["C"].Value = "0";
            radStatusStrip1.Enabled = false;
            radTextBox_Barcode.Enabled = false;
            if (dtItems.Rows.Count > 0)
            {
                dtItems.Rows.Clear();
                radGridView_Item.DataSource = dtItems;
                dtItems.AcceptChanges();
            }

            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Enabled = false;
            radLabel_Unit.Text = "";
            radLabel_Name.Text = "";
            Button_Save.Enabled = false;
        }
        //เลือก
        private void RadGridView_Bch_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (e.Column.Name)
            {
                case "C":
                    try
                    {
                        if (RadGridView_Shelf.CurrentRow.Cells["C"].Value.ToString() == "0")
                        {
                            CheckUnSelectInGrid();
                            zoneID = RadGridView_Shelf.CurrentRow.Cells["ZONE_ID"].Value.ToString();
                            sheftID = RadGridView_Shelf.CurrentRow.Cells["SHELF_ID"].Value.ToString();

                            RadGridView_Shelf.CurrentRow.Cells["C"].Value = "1";
                            SetItem(zoneID, sheftID);
                            radStatusStrip1.Enabled = true;
                        }
                        else
                        {
                            ClearZone(RadGridView_Shelf.CurrentRow.Index);
                        }
                    }
                    catch (Exception)
                    {
                        ClearZone(RadGridView_Shelf.CurrentRow.Index);
                    }
                    break;
                default:
                    break;
            }
        }
        void CheckUnSelectInGrid()
        {
            for (int i = 0; i < RadGridView_Shelf.Rows.Count; i++)
            {
                RadGridView_Shelf.Rows[i].Cells["C"].Value = "0";
            }

        }
        //เชคบาร์โค้ด
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Barcode.Text == "")
                {
                    return;
                }
                //Check สินค้าในชั้นวาง
                string pStrCheckItem = CheckItem(radTextBox_Barcode.Text);
                if (pStrCheckItem != "")
                {
                    if (SystemClass.SystemBranchID == "MN000")//กรณี Center สามารถเปลี่ยนแปลงชั้นวางได้
                    {
                        if (MsgBoxClass.MsgBoxShow_ConfirmEdit("สินค้า " + radTextBox_Barcode.Text + " " + pStrCheckItem) == DialogResult.No)
                        {
                            ClearTxt();
                            radTextBox_Barcode.Enabled = true;
                            radTextBox_Barcode.Focus();
                            return;
                        }
                    }
                    else //กรณีสาขาไม่อนุญาติให้เปลี่ยนเเปลงชั้นวาง
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่อนุญาติให้สาขาเปลี่ยนชั้นวางของสินค้าทุกกรณี" + Environment.NewLine +
                            "ติดต่อ CenterShop Tel.1022 เพื่อแก้ไขเท่านั้น");
                        ClearTxt();
                        radTextBox_Barcode.Enabled = true;
                        radTextBox_Barcode.Focus();
                        return;
                    }
                }

                DataTable dtBarcode = ItembarcodeClass.GetItembarcode_ALLDeatil(radTextBox_Barcode.Text);
                if (dtBarcode.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลบาร์โค้ดที่ระบุ");
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Enabled = true;
                    radTextBox_Barcode.Focus();
                    return;
                }

                radLabel_Name.Text = dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString();
                radLabel_Unit.Text = dtBarcode.Rows[0]["UNITID"].ToString();
                radTextBox_Barcode.Enabled = true;
                Button_Save.Enabled = true;
                Button_Save.Focus();
            }
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            try
            {
                string barcode = radGridView_Item.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
                string name = radGridView_Item.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString();
                string unit = radGridView_Item.CurrentRow.Cells["UNITID"].Value.ToString();

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการลบข้อมูล " + Environment.NewLine +
                    barcode + @" " + name + @" " + unit + Environment.NewLine +
                    "ในชั้นวาง " + sheftID + @"[" + zoneID + @"]") == DialogResult.OK)
                {
                    return;
                }

                string strDel = string.Format(@" 
                    DELETE   
                    FROM    SHOP_SHELFITEMS 
                    WHERE   ITEMBARCODE = '" + barcode + @"' AND BRANCH_ID = '" + bchId + @"' ");

                string T = ConnectionClass.ExecuteSQL_Main(strDel);
                if (T == "")
                {
                    foreach (DataRow dr in dtItems.Rows)
                    {
                        if (dr["ITEMBARCODE"].ToString() == barcode)
                            dr.Delete();
                    }
                    radGridView_Item.DataSource = dtItems;
                    dtItems.AcceptChanges();
                    ClearTxt();
                }

                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
            catch (Exception)
            {
                return;
            }
        }

        //เพิ่มรายการ
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            if (bchId == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("การเลือกสาขาไม่ถูกต้อง ไม่สามารถเพิ่มข้อมูลใหม่ได้" + Environment.NewLine + "ลองใหม่อีกครั้ง.");
                return;
            }
            ClearTxt();
            radTextBox_Barcode.Enabled = true;
            radTextBox_Barcode.Focus();
        }
        //PDF
        private void Button_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
        //Save
        private void Button_Save_Click(object sender, EventArgs e)
        {
            ArrayList strIn = new ArrayList();
            if (zoneID_old != "")
            {
                //strIn.Add(string.Format(@" 
                //DELETE   
                //FROM    SHOP_SHELFITEMS 
                //WHERE   ITEMBARCODE = '" + radTextBox_Barcode.Text + @"' AND BRANCH_ID = '" + bchId + @"' "));
                strIn.Add(string.Format(@" 
                    UPDATE  SHOP_SHELFITEMS
                    SET     SHELF_ID = '" + sheftID + @"',ZONE_ID = '" + zoneID + @"',
                            WHOUP='" + SystemClass.SystemUserID_M + @"',WHONAMEUP='" + SystemClass.SystemUserName + @"',DATEUP = CONVERT(VARCHAR,GETDATE(),25)
                    WHERE   ITEMBARCODE = '" + radTextBox_Barcode.Text + @"' AND BRANCH_ID = '" + bchId + @"' "));
            }
            else
            {
                strIn.Add(string.Format(@"
                INSERT INTO [SHOP_SHELFITEMS]
                   ([SHELF_ID],[ZONE_ID],[BRANCH_ID],[BRANCH_NAME],
                    [ITEMBARCODE]
                   ,[WHOINS],[WHONAMEINS])
                VALUES (
                    '" + sheftID + @"','" + zoneID + @"','" + bchId + @"','" + bchName + @"',
                    '" + radTextBox_Barcode.Text + @"',
                    '" + SystemClass.SystemUserID_M + @"','" + SystemClass.SystemUserName + @"'
                )"));
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(strIn);
            if (T == "")
            {
                dtItems.Rows.Add(radTextBox_Barcode.Text, radLabel_Name.Text, radLabel_Unit.Text);
                radGridView_Item.DataSource = dtItems;
                dtItems.AcceptChanges();
                ClearTxt();
                radTextBox_Barcode.Enabled = true;
                radTextBox_Barcode.Focus();
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
    }
}
