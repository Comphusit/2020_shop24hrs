﻿//CheckOK
using PC_Shop24Hrs.Controllers;
using System;
using System.Collections.Generic;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
	class ShelfReportClass
	{
		public static List<SHELF> GetShef(string branchid)
		{
			string sql = string.Format(@"
                    SELECT	SHOP_SHELF.BRANCH_ID AS BRANCH_ID,SHOP_SHELFZONE.ZONE_ID,SHOP_SHELFZONE.ZONE_NAME,SHOP_SHELF.SHELF_ID,SHOP_SHELF.SHELF_NAME,RATE
                    FROM	SHOP_SHELF WITH (NOLOCK) INNER JOIN 
							SHOP_SHELFZONE WITH (NOLOCK)  ON SHOP_SHELF.ZONE_ID =  SHOP_SHELFZONE.ZONE_ID
                    WHERE	SHOP_SHELF.BRANCH_ID = '{0}' AND SHOP_SHELFZONE.BRANCH_ID = '{0}'
                    ORDER BY SHOP_SHELFZONE.ZONE_ID,SHOP_SHELF.SHELF_ID", branchid);
			DataTable dt = ConnectionClass.SelectSQL_Main(sql);
			var Listshelf = new List<SHELF>();
			foreach (DataRow row in dt.Rows)
			{
				SHELF shelf = new SHELF(row["BRANCH_ID"].ToString(), row["ZONE_ID"].ToString(), row["SHELF_ID"].ToString(), row["ZONE_NAME"].ToString(), row["SHELF_NAME"].ToString(), row["RATE"].ToString());
				Listshelf.Add(shelf);
			}
			return Listshelf;
		}
		public static DataTable GetBranch()
		{
			string sql = string.Format(@"
                    SELECT SHOP_BRANCH.BRANCH_ID,SHOP_BRANCH.BRANCH_NAME
					FROM SHOP_BRANCH INNER JOIN  (
							SELECT	SHOP_SHELF.BRANCH_ID AS BRANCH_ID,SHOP_SHELFZONE.ZONE_ID,SHOP_SHELFZONE.ZONE_NAME,SHOP_SHELF.SHELF_ID,SHOP_SHELF.SHELF_NAME,RATE
							FROM	SHOP_SHELF WITH (NOLOCK) INNER JOIN 
									SHOP_SHELFZONE WITH (NOLOCK)  ON SHOP_SHELF.ZONE_ID =  SHOP_SHELFZONE.ZONE_ID)S
									ON SHOP_BRANCH.BRANCH_ID = S.BRANCH_ID
					GROUP BY SHOP_BRANCH.BRANCH_ID,BRANCH_NAME
					ORDER BY BRANCH_ID");
			DataTable dt = ConnectionClass.SelectSQL_Main(sql);
			return dt;
		}
		public static DataTable GetDataSalebyBranch(string branchid, string datebegin, string dateend)
		{
			string sql = string.Format(@"
                    declare @statdate date
                    declare @enddate  date
                    declare @branchid VARCHAR(5) 
                    set @statdate = '{0}'
                    set @enddate = '{1}'
                    set @branchid = '{2}'

                    SELECT TMP.ZONE_ID, TMP.SHELF_ID
                        , SUM(SUM_QTY) AS QTY_TOTAL
                        , SUM(SUM_LINEAMOUNT) AS AMOUNT
                        , TMP.TimePeriod
                    FROM
                        (
                            SELECT ZONE_ID, SHELF_ID, ITEMBARCODE, T.SUM_QTY, T.SUM_LINEAMOUNT
                                    , T.TimePeriod
                            FROM   SHOP_SHELFITEMS WITH(NOLOCK) INNER JOIN (
                                    SELECT  BRANCH, PRODUCT, TimePeriod, SUM(QTY) AS SUM_QTY, SUM(LINEAMOUNT) AS SUM_LINEAMOUNT
                                    FROM    SPC705SRV.[Staging].[dbo].[VIEW_PT_RETAIL]
                                    WHERE   INVENTSITEID = N'SPC'
                                            AND SALESZONE = N'MN'
                                            AND TimePeriod between @statdate and @enddate
                                            AND BRANCH = @branchid
                                    GROUP BY BRANCH, PRODUCT, TimePeriod
                        ) T ON SHOP_SHELFITEMS.ITEMBARCODE = T.PRODUCT COLLATE database_default

                    WHERE BRANCH_ID = @branchid) TMP
                    GROUP BY TMP.ZONE_ID
                            , TMP.SHELF_ID
                            , TMP.TimePeriod"
					, datebegin, dateend, branchid);
			return ConnectionClass.SelectSQL_Main(sql);
		}
		#region PivotTable 
		public static DataTable GetDataSale(string branch, string datebegine, string dateend)
		{
			string sql = string.Format(@"
                    SELECT *
                              FROM (SELECT BRANCH
			                            ,ZONE_ID
		                                ,SHELF_ID
		                                ,SUM(SUM_LINEAMOUNT) AS AMOUNT 
			                            ,day(TimePeriod) day
                                FROM
	                                    (	
			
			                            SELECT BRANCH,ZONE_ID,SHELF_ID,ITEMBARCODE,T.SUM_QTY,T.SUM_LINEAMOUNT,TimePeriod
	                                        FROM   SHOP_SHELFITEMS INNER JOIN (
			                                        SELECT  BRANCH,PRODUCT,TimePeriod,SUM(QTY) AS SUM_QTY,SUM(LINEAMOUNT) AS SUM_LINEAMOUNT
			                                        FROM     spc705srv.[Staging].[dbo].[VIEW_PT_RETAIL]
			                                        WHERE  INVENTSITEID = N'SPC'
					                                        AND SALESZONE = N'MN'
					                                        AND TimePeriod between '{0}' and '{1}' 
					                                        AND BRANCH = '{2}'
					                                GROUP BY BRANCH,PRODUCT,TimePeriod 

			                                ) T ON SHOP_SHELFITEMS.ITEMBARCODE = T.PRODUCT COLLATE database_default

                                WHERE BRANCH_ID = '{2}' ) A
                                GROUP BY ZONE_ID,SHELF_ID,TimePeriod,BRANCH)
	                            TMP

                            PIVOT  
                            (  
                              SUM(AMOUNT) 
		                            FOR DAY IN ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],
					                            [11],[12],[13],[14],[15],[16],[17],[18],[19],[20],
					                            [21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31])  
                            ) AS PivotTable ", datebegine, dateend, branch);

			return ConnectionClass.SelectSQL_Main(sql);
		}
		public static DataTable GetCountEmp(string branchid, string datebegine, string dateend)
		{
			string sql = string.Format(@"
                    SELECT *
	                        FROM (
  		                        SELECT EMPL_ID 
			                       ,SHOP_SHELFEMPLDATE.BRANCH_ID
			                        ,SHOP_SHELFEMPLDATE.ZONE_ID
			                        ,SHOP_SHELFEMPLDATE.SHELF_ID
                                    ,ISNULL(RATE,0.01) AS RATE
			                        ,YEAR(DATE_EMPL) YEAR
			                        ,MONTH(DATE_EMPL) MONTH
			                        ,DAY(DATE_EMPL) DAY
		                        FROM SHOP_SHELFEMPLDATE WITH (NOLOCK) 
                                left join 
				                    SHOP_SHELF
		                                    on SHOP_SHELF.SHELF_ID = SHOP_SHELFEMPLDATE.SHELF_ID 
		                                    and SHOP_SHELF.BRANCH_ID = SHOP_SHELFEMPLDATE.BRANCH_ID
		                                    and SHOP_SHELF.ZONE_ID = SHOP_SHELFEMPLDATE.ZONE_ID 
	                          WHERE DATE_EMPL between '{0}' and '{1}' and SHOP_SHELFEMPLDATE.BRANCH_ID = '{2}' 
	                            group by DATE_EMPL
			                        ,SHOP_SHELFEMPLDATE.BRANCH_ID
			                        ,SHOP_SHELFEMPLDATE.SHELF_ID
			                        ,SHOP_SHELFEMPLDATE.ZONE_ID
			                        ,EMPL_ID
                                    ,RATE
                        ) TMP

                        PIVOT  
                        (  
                          COUNT (EMPL_ID)  
		                        FOR DAY IN ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],
					                        [11],[12],[13],[14],[15],[16],[17],[18],[19],[20],
					                        [21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31])  
                        ) AS PivotTable ", datebegine, dateend, branchid);
			DataTable dt = ConnectionClass.SelectSQL_Main(sql);
			return dt;
		}
		public static DataTable GetEmpAll(string branchid, string datebegine, string dateend)
		{
			string sql = string.Format(@"
                    SELECT * 
	                    FROM (
		                    SELECT BRANCH_ID
			                        ,ZONE_ID
			                        ,SHELF_ID
			                        ,EMPL_ID
			                        ,EMPL_NAME
			                        ,1 AS NUM 
			                        ,YEAR(DATE_EMPL) AS YEAR
			                        ,MONTH(DATE_EMPL) AS MONTH
			                        ,DAY(DATE_EMPL) AS DAY
		                    FROM    SHOP_SHELFEMPLDATE WITH (NOLOCK) 
		                    WHERE   DATE_EMPL 
				                    between '{0}' AND '{1}' 
				                    AND BRANCH_ID = '{2}'
                                    --AND EMPL_ID = 'M1008101'
		                    GROUP BY BRANCH_ID
				                    ,ZONE_ID
				                    ,SHELF_ID
				                    ,EMPL_ID
				                    ,EMPL_NAME
				                    ,DATE_EMPL
	                    )TMP

	                    PIVOT  
	                    (  
		                    SUM(NUM) 
			                    FOR DAY IN ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],
						                    [11],[12],[13],[14],[15],[16],[17],[18],[19],[20],
						                    [21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31])  
	                    ) AS PivotTable   order by EMPL_ID", datebegine, dateend, branchid);

			return ConnectionClass.SelectSQL_Main(sql);
		}
		#endregion
		public static List<EMPLDATE> GetEmployee(string branchid, string datebegine, string dateend)
		{
			string sql = string.Format(@"
		            SELECT EMPL_ID
							, SHOP_SHELFEMPLDATE.BRANCH_ID
							,SHOP_SHELFEMPLDATE.ZONE_ID
							,SHOP_SHELFEMPLDATE.SHELF_ID
							,SHOP_SHELF.SHELF_NAME
							,EMPL_ID
							,EMPL_NAME
							,YEAR(DATE_EMPL) YEAR
							,FORMAT(DATE_EMPL, 'MM') AS MONTH
							,FORMAT(DATE_EMPL, 'dd') AS DAY
							,ISNULL(RATE, 0.01) AS RATE
							,1 as CNT
							,EMP_NUM
							,DATE_EMPL
					FROM SHOP_SHELFEMPLDATE WITH(NOLOCK) left join
							SHOP_SHELF ON SHOP_SHELF.SHELF_ID = SHOP_SHELFEMPLDATE.SHELF_ID
							AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFEMPLDATE.BRANCH_ID
							AND SHOP_SHELF.ZONE_ID = SHOP_SHELFEMPLDATE.ZONE_ID
						WHERE DATE_EMPL   between '{0}' AND '{1}'
							AND SHOP_SHELFEMPLDATE.BRANCH_ID = '{2}'
							AND(DATE_EVENT = '' OR DATE_EVENT IS NULL)", datebegine, dateend, branchid);
			DataTable dt = ConnectionClass.SelectSQL_Main(sql);
			var list = new List<EMPLDATE>();
			foreach (DataRow dr in dt.Rows)
			{
				list.Add(new EMPLDATE()
				{
					BRANCH_ID = dr["BRANCH_ID"].ToString().Trim()
				,
					EMPL_ID = dr["EMPL_ID"].ToString().Trim()
				,
					EMPL_NAME = dr["EMPL_NAME"].ToString()
				,
					ZONE_ID = dr["ZONE_ID"].ToString().Trim()
				,
					SHELF_ID = dr["SHELF_ID"].ToString().Trim()
				,
					SHELF_NAME = dr["SHELF_NAME"].ToString()
				,
					YEAR = dr["YEAR"].ToString()
				,
					MONTH = dr["MONTH"].ToString()
				,
					DAY = dr["DAY"].ToString()
				,
					NUM = dr["EMP_NUM"].ToString()
				,
					RATE = Convert.ToDouble(dr["RATE"])
				,
					CNT = Convert.ToInt32(dr["CNT"])
					,
					DATE_EMPL = dr["DATE_EMPL"].ToString()
				});
			}
			return list;
		}
		public static List<SALE> GetSaleList(string branchid, string datebegine, string dateend)
		{
			string sql = string.Format(@"
                    SELECT BRANCH_ID
		                    ,UPPER(ZONE_ID) AS ZONE_ID
							,UPPER(SHELF_ID) AS SHELF_ID
		                    ,SUM(SUM_LINEAMOUNT) AS AMOUNT 
		                    ,FORMAT(TimePeriod, 'dd') AS DAY
							,Convert(varchar,TimePeriod,23) AS TimePeriod
                    FROM
	                        (		
		                    SELECT	BRANCH_ID
				                    ,ZONE_ID
				                    ,SHELF_ID
				                    ,ITEMBARCODE
				                    ,T.SUM_QTY
				                    ,T.SUM_LINEAMOUNT
				                    ,TimePeriod
	                            FROM   SHOP_SHELFITEMS INNER JOIN (
			                            SELECT  BRANCH,PRODUCT,TimePeriod,SUM(QTY) AS SUM_QTY,SUM(LINEAMOUNT) AS SUM_LINEAMOUNT
			                            FROM     spc705srv.[Staging].[dbo].[VIEW_PT_RETAIL]
			                            WHERE  INVENTSITEID = N'SPC'
					                            AND SALESZONE = N'MN'
					                            AND TimePeriod between '{0}' and '{1}' 
					                            AND BRANCH = '{2}'
					                    GROUP BY BRANCH,PRODUCT,TimePeriod 

			                    ) T ON SHOP_SHELFITEMS.ITEMBARCODE = T.PRODUCT COLLATE database_default

			                    WHERE BRANCH_ID = '{2}'
			                     ) A
                    GROUP BY ZONE_ID,SHELF_ID
		                    ,BRANCH_ID
		                    ,TimePeriod", datebegine, dateend, branchid);
			DataTable dt = ConnectionClass.SelectSQL_Main(sql);
			var list = new List<SALE>();
			foreach (DataRow dr in dt.Rows)
			{
				list.Add(new SALE()
				{
					BRANCH_ID = dr["BRANCH_ID"].ToString().Trim(),
					ZONE_ID = dr["ZONE_ID"].ToString().Trim(),
					SHELF_ID = dr["SHELF_ID"].ToString().Trim(),
					DAY = dr["DAY"].ToString().Trim(),
					AMOUNT = Convert.ToDouble(dr["AMOUNT"]),
					DATETIME = dr["TimePeriod"].ToString()
				});
			}
			return list;
		}
		public static DataTable GetSaleAll(string branchid, string datebegine, string dateend)
		{
			string sql = string.Format(@"
					DECLARE @statdate date
					DECLARE @enddate  date
					DECLARE @branchid VARCHAR(5) 
					set @statdate = '{0}'
					set @enddate = '{1}'
					set @branchid = '{2}' 
                    SELECT *
		                    FROM 
			                    (
			                    SELECT TMP.BRANCH AS BRANCH
				                    ,TMP.ZONE_ID AS ZONE_ID
                                    ,SHOP_SHELFZONE.ZONE_NAME AS ZONE_NAME
				                    ,TMP.SHELF_ID AS SHELF_ID
				                    ,SHOP_SHELF.SHELF_NAME AS SHELF_NAME
				                    ,TMP.TOTAL_LINEAMOUNT AS TOTAL_LINEAMOUNT
				                    ,day
				                    ,RATE
			                    FROM (	
					                    SELECT BRANCH
					                    ,SHOP_SHELFITEMS.ZONE_ID AS ZONE_ID
					                    ,SHOP_SHELFITEMS.SHELF_ID AS SHELF_ID
					                    ,SUM(T.SUM_LINEAMOUNT) AS TOTAL_LINEAMOUNT
					                    ,TimePeriod
					                    ,day(TimePeriod) day
					

				                    FROM   SHOP_SHELFITEMS WITH (NOLOCK) INNER JOIN (
						                    SELECT  BRANCH,PRODUCT,TimePeriod,SUM(QTY) AS SUM_QTY,SUM(LINEAMOUNT) AS SUM_LINEAMOUNT
						                    FROM     spc705srv.[Staging].[dbo].[VIEW_PT_RETAIL]  WITH (NOLOCK)
						                    WHERE  INVENTSITEID = N'SPC'
								                    AND SALESZONE = N'MN'
								                    AND TimePeriod between @statdate and @enddate
								                    AND BRANCH = @branchid
						                    GROUP BY BRANCH
								                    ,PRODUCT
								                    ,TimePeriod 

					                    ) T ON SHOP_SHELFITEMS.ITEMBARCODE = T.PRODUCT COLLATE database_default
				                    WHERE SHOP_SHELFITEMS.BRANCH_ID = @branchid
				                    GROUP BY BRANCH
						                    ,SHOP_SHELFITEMS.ZONE_ID
						                    ,SHOP_SHELFITEMS.SHELF_ID
						                    ,TimePeriod
						
						                    ) TMP   INNER JOIN  
								                    SHOP_SHELFZONE WITH (NOLOCK) ON SHOP_SHELFZONE.ZONE_ID = TMP.ZONE_ID AND SHOP_SHELFZONE.BRANCH_ID = @branchid  INNER JOIN  
								                    SHOP_SHELF WITH (NOLOCK) ON SHOP_SHELF.SHELF_ID = TMP.SHELF_ID AND SHOP_SHELF.BRANCH_ID = @branchid
								                    AND TMP.ZONE_ID = SHOP_SHELF.ZONE_ID
			                    )TMP2 

                    PIVOT  
                        (  
                            SUM(TOTAL_LINEAMOUNT) 
		                        FOR DAY IN ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],
					                        [11],[12],[13],[14],[15],[16],[17],[18],[19],[20],
					                        [21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31])  
                        ) AS PivotTable ", datebegine, dateend, branchid);
			return ConnectionClass.SelectSQL_Main(sql);
		}
	}
	#region Shelf
	class SHELF
	{
		public string BRANCH_ID { get; set; }
		public string ZONE_ID { get; set; }
		public string ZONE_NAME { get; set; }
		public string SHELF_ID { get; set; }
		public string SHELF_NAME { get; set; }
		public string RATE { get; set; }
		public SHELF(string branch, string zone, string shelf, string zonename, string shelfname, string rate)
		{
			this.BRANCH_ID = branch;
			this.ZONE_ID = zone;
			this.SHELF_ID = shelf;
			this.ZONE_NAME = zonename;
			this.SHELF_NAME = shelfname;
			this.RATE = rate;
		}
	}
	class EMPLDATE
    {
        public string BRANCH_ID { get; set; }
        public string EMPL_ID { get; set; }
        public string EMPL_NAME { get; set; }
        public string ZONE_ID { get; set; }
        public string SHELF_ID { get; set; }
		public string SHELF_NAME { get; set; }
		public string NUM { get; set; }
        public string YEAR { get; set; }
        public string MONTH { get; set; }
        public string DAY { get; set; }
		public double RATE { get; set; }
		public int CNT { get; set; }
		public string DATE_EMPL { get; set; }
	}
    class EMPCOUNT
    {
        public string YEAR { get; set; }
        public string MONTH { get; set; }
        public string DAY { get; set; }
        public string ZONE_ID { get; set; }
        public string SHELF_ID { get; set; }
        public int CNT { get; set; }
		public int EMP_NUM{ get; set; }
		public string BRANCH_ID { get; set; }
        public double RATE { get; set; }
    }
    class SALE
    {
        public string BRANCH_ID { get; set; }
        public string ZONE_ID { get; set; }
        public string SHELF_ID { get; set; }
        public double AMOUNT { get; set; }
        public string DAY { get; set; }
		public string  DATETIME { get; set; }
	}
	class EMPDETAIL
	{
		public string BRANCH_ID { get; set; }
		public string EMPL_ID { get; set; }
		public string EMPL_NAME { get; set; }
		public string STARTDATE { get; set; }
		public string ENDDATE { get; set; }
		public EMPDETAIL(string branch, string emplid, string emplname, string startdate, string enddate)
		{
			this.BRANCH_ID = branch;
			this.EMPL_ID = emplid;
			this.EMPL_NAME = emplname;
			this.STARTDATE = startdate;
			this.ENDDATE = enddate;
		}
	}
	#endregion
}
