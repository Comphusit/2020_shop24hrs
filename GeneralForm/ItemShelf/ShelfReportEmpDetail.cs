﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using System.Drawing;
using Telerik.Windows.Diagrams.Core;
using System.Collections.Generic;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfReportEmpDetail : Telerik.WinControls.UI.RadForm
    {
        #region Fields 
        readonly DataTable dt_Emp = new DataTable();
        readonly EMPDETAIL _pEmp;
        readonly string _pColumn;
        readonly string[] _pColumns;
        readonly ArrayList _pList;
        #endregion
        public ShelfReportEmpDetail(string pColumn, string[] pColumns, string pBranch, string pEmp, string pEmpName, string pStartDate, string pEndDate, ArrayList pList)
        {
            InitializeComponent();
            _pColumn = pColumn;
            _pColumns = pColumns;
            _pList = pList;
            _pEmp = new EMPDETAIL(pBranch, pEmp, pEmpName, pStartDate, pEndDate);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        private void ShelfReportEmpDetail_Load(object sender, EventArgs e)
        {
            radButtonElement_Pdf.ShowBorder = true;
            radButtonElement_Excel.ShowBorder = true;
            if (_pColumn.Contains("DATE"))
            {
                _pEmp.ENDDATE = _pEmp.STARTDATE;
            }
            this.Text = $"รายละเอียดค่าคอม >> {_pEmp.EMPL_ID}  {_pEmp.EMPL_NAME} วันที่ {_pEmp.STARTDATE} ถึง {_pEmp.ENDDATE} [ {_pEmp.BRANCH_ID} ]";

            toolStrip1.BackColor = Color.FromArgb(173, 244, 215);
            toolStrip1.Font = new Font(new FontFamily("Tahoma"), 9.75f);
            ToolStripLabel toolTiplalel1 = new ToolStripLabel
            {
                Text = @"รายละเอียด การแสดงผลตามวัน >> ยอดขาย * เรท / จำนวนคนดูแลต่อวัน = รายได้ต่อคน"
            };
            toolStrip1.Items.Add(toolTiplalel1);

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.EnableFiltering = false;
            RadGridView_Show.Visible = true;
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddVisible("BRANCH_ID", "รหัส", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddVisible("BRANCH_NAME", "สาขา", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัสโซน", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "รหัสชั้นวาง", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_NUM", "จำนวน พนง\n ตามตั้งค่า", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE", "เรท [%]", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALE", "ยอดขาย\n [บาท]", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CommisionTotal", "ค่าคอมทั้งหมด\n [บาท]", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CommisionEmp", "ค่าคอมที่ได้\n [บาท]", 100));

            //dt_Emp = this.AddNewColumn(new string[] { "BRANCH_ID", "BRANCH_NAME", "ZONE_ID", "SHELF_ID", "SHELF_NAME", "EMP_NUM", "RATE", "SALE", "CommisionTotal", "CommisionEmp" });
            RadGridView_Show.MasterTemplate.Columns.ForEach(radGridViewColmn => { radGridViewColmn.IsPinned = true; dt_Emp.Columns.Add(radGridViewColmn.FieldName); });
            List<string> newColmns = new List<string>
            {
                "CommisionTotal",
                "CommisionEmp",
                "SALE"
            };
            this.GetGridViewSummary(newColmns.ToArray(), GridAggregateFunction.Sum);

            if (_pColumn.Contains("DATE"))
            {
                ActionAddColumn(_pColumn);
                this.GetComByDay();
            }
            else
            {
                Action<string> action = new Action<string>(ActionAddColumn);
                Array.ForEach(_pColumns, action);
                this.GetCom();
            }
        }
        #region SetFontInRadGridview 
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //ToolTipText
        private void RadGridView_Show_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (_pColumn.Contains("DATE"))
                {
                    if (e.ColumnIndex == RadGridView_Show.Columns[_pColumn].Index)
                    {
                        if (e.Row is GridViewDataRowInfo)
                        {
                            if (!RadGridView_Show.Rows[e.RowIndex].Cells[_pColumn].Value.ToString().Equals("0"))
                            {
                                var value = RadGridView_Show.Rows[e.RowIndex].Cells[_pColumn].Value.ToString().Split('*', '/', '=');
                                e.CellElement.ToolTipText = $"ยอดขาย : {value[0]}\n" +
                                    $"เรท : {value[1]}\n" +
                                    $"จำนวนคนดูแลต่อวัน : {value[2]}\n" +
                                    $"รายได้ต่อคน: {value[3]}";
                            }
                        }
                    }
                }
                else
                {
                    _pColumns.ForEach(c =>
                    {
                        if (e.ColumnIndex == RadGridView_Show.Columns[c].Index)
                        {
                            if (e.Row is GridViewDataRowInfo)
                            {
                                if (!RadGridView_Show.Rows[e.RowIndex].Cells[c].Value.ToString().Equals("0"))
                                {
                                    var value = RadGridView_Show.Rows[e.RowIndex].Cells[c].Value.ToString().Split('*', '/', '=');
                                    e.CellElement.ToolTipText = $"ยอดขาย : {value[0]}\n" +
                                        $"เรท : {value[1]}\n" +
                                        $"จำนวนคนดูแลต่อวัน : {value[2]}\n" +
                                        $"รายได้ต่อคน: {value[3]}";
                                }
                            }
                        }
                    });
                }
            }
            if (e.CellElement.ColumnInfo.FieldName.Equals("CommisionEmp"))
            {
                e.CellElement.BackColor = Color.Khaki;
                e.CellElement.ForeColor = Color.Black;
            }
            else
            { 
                e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
            }
        }
        #endregion
        private void RadButtonElement_Pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, this.Text);
        }
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string strret = DatagridClass.ExportExcelGridView($"{ _pEmp.EMPL_ID}{ _pEmp.EMPL_NAME} วันที่ { _pEmp.STARTDATE} ถึง { _pEmp.ENDDATE} [ {_pEmp.BRANCH_ID} ]", RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(strret);
        }
        private void ActionAddColumn(string column)
        {
            dt_Emp.Columns.Add(new DataColumn(column, typeof(string)));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual(column, string.Format(@"{0}-{1}-{2}", column.Substring(4, 2), column.Substring(6, 2), column.Substring(8, 4)), 200));
        }
        private void GetCom()
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            var listEmployee = (List<EMPLDATE>)_pList[0];
            var ListSale = (List<SALE>)_pList[1];
            var emps = listEmployee.Where(e => e.EMPL_ID == _pEmp.EMPL_ID);
            foreach (var empgrp in emps.GroupBy(z => z.ZONE_ID).OrderBy(z => z.Key))   //Zone
            {
                foreach (var emp in empgrp.GroupBy(s => s.SHELF_ID).OrderBy(s => s.Key)) //shef
                {
                    DataRow row = dt_Emp.NewRow();
                    row["SHELF_ID"] = emp.Key;
                    double SumAmount = 0.0, ComTotal = 0.0, SumComtotal = 0.0;
                    foreach (var c in _pColumns)
                    {
                        string datecol = c.Substring(4, 2);
                        row[c] = 0;
                        foreach (var e in emp.Where(m => m.DAY == datecol))
                        {
                            row["ZONE_ID"] = e.ZONE_ID;
                            row["BRANCH_ID"] = e.BRANCH_ID;
                            row["SHELF_NAME"] = e.SHELF_NAME;
                            row["RATE"] = e.RATE;
                            row["EMP_NUM"] = e.NUM;
                            double total = 0.0;
                            int count = 0;
                            var sale = ListSale.Where(s => s.DAY == e.DAY && s.ZONE_ID == e.ZONE_ID && s.SHELF_ID == e.SHELF_ID);
                            count = listEmployee.Where(n => n.DAY == e.DAY && n.ZONE_ID == e.ZONE_ID && n.SHELF_ID == e.SHELF_ID).ToList().Count;
                            sale.ForEach(s =>
                            {
                                total = s.AMOUNT * e.RATE / count;
                                SumAmount += s.AMOUNT;
                                row[c] = $"{s.AMOUNT} * { e.RATE} / {count} = {total:#,##0.00}";
                            });
                            ComTotal += total;
                            SumComtotal = Convert.ToDouble(row["RATE"]) * SumAmount;
                        }
                    }
                    row["SALE"] = Math.Round(SumAmount).ToString("#,##0.00");
                    row["CommisionEmp"] = Math.Round(ComTotal).ToString("#,##0.00");
                    row["CommisionTotal"] = Math.Round(SumComtotal).ToString("#,##0.00");
                    dt_Emp.Rows.Add(row);
                }
            }
            RadGridView_Show.DataSource = dt_Emp;
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        private void GetComByDay()
        {
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            var listEmp = (List<EMPLDATE>)_pList[0];
            var ListSale = (List<SALE>)_pList[1];
            var emps = listEmp.Where(e => e.EMPL_ID == _pEmp.EMPL_ID && e.DAY == _pColumn.Substring(4, 2));
            foreach (var empgrp in emps.GroupBy(z => z.ZONE_ID).OrderBy(z => z.Key))   //Zone
            {
                foreach (var emp in empgrp.GroupBy(s => s.SHELF_ID).OrderBy(s => s.Key)) //shef
                {
                    DataRow row = dt_Emp.NewRow();
                    row["SHELF_ID"] = emp.Key;
                    foreach (var e in emp)
                    {
                        row["ZONE_ID"] = e.ZONE_ID;
                        row["BRANCH_ID"] = e.BRANCH_ID;
                        row["SHELF_NAME"] = e.SHELF_NAME;
                        row["RATE"] = e.RATE;
                        row["EMP_NUM"] = e.NUM;

                        int countemp = 0;
                        double SumAmount = 0.0, SumTotal = 0.0, total = 0.0, SumComtotal = 0.0;
                        var sale = ListSale.Where(s => s.DAY == e.DAY && s.ZONE_ID == e.ZONE_ID && s.SHELF_ID == e.SHELF_ID);
                        countemp = listEmp.Where(n => n.DAY == e.DAY && n.ZONE_ID == e.ZONE_ID && n.SHELF_ID == e.SHELF_ID).ToList().Count;
                        row[_pColumn] = 0;
                        sale.ForEach(s =>
                        {
                            total = s.AMOUNT * e.RATE / countemp;
                            SumAmount += s.AMOUNT;
                            row[_pColumn] = $"{s.AMOUNT} * {e.RATE} / {countemp} = {total:#,##0.00}";
                        });
                        SumComtotal = e.RATE * SumAmount;
                        SumTotal += total;
                        row["SALE"] = Math.Round(SumAmount).ToString("#,##0.00");
                        row["CommisionEmp"] = Math.Round(SumTotal).ToString("#,##0.00");
                        row["CommisionTotal"] = Math.Round(SumComtotal).ToString("#,##0.00");
                    }
                    dt_Emp.Rows.Add(row);
                }
            }
            RadGridView_Show.DataSource = dt_Emp;
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        //GetGridViewSummary
        private void GetGridViewSummary(string[] Colname, GridAggregateFunction gridAggregate)
        {
            GridViewSummaryItem[] items = new GridViewSummaryItem[Colname.Length];
            for (int i = 0; i < Colname.Length; i++)
            {
                GridViewSummaryItem summaryItem = new GridViewSummaryItem(Colname[i], "{0:0,0}", gridAggregate);
                items[i] = summaryItem;
            }
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(items);
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
    }
}