﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfMain : Telerik.WinControls.UI.RadForm
    {
        DataTable dtBranch = new DataTable();
        DataTable _dtBranch = new DataTable();

        DataTable dt_Zone = new DataTable();
        DataTable dt_Shelf = new DataTable();

        string statusupdate;
        readonly string _pUser;

        public ShelfMain(string pUser)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pUser = pUser;
        }
        private void ShelfMain_Load(object sender, EventArgs e)
        {
            this.SetProperty();

            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            _dtBranch = BranchClass.GetBranchAll("'1'", "'1'");

            this.BindGrid();
            this.SetDroupDownBrach(radDropDownList_Branch_zone);
            this.SetDroupDownBrach_Shelf(radDropDownList_Branch_shelf);
            this.SetTextEmpty();
        }

        #region ROWS DGV
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        #region Event
        private void RadButton_AddZone_Click(object sender, EventArgs e)
        {
            string branchid = radDropDownList_Branch_zone.SelectedValue.ToString();
            string zoneid = radTextBox_Zone_id.Text.Trim();
            if (statusupdate.Equals("add"))
            {
                if (!zoneid.Equals(""))
                {
                    string Ins = string.Format(@"
                        INSERT INTO     SHOP_SHELFZONE (ZONE_ID,ZONE_NAME,BRANCH_ID,REAMRK,WHOINS,WHONAMEINS) 
                        VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}')"
                                    , zoneid, radTextBox_Zonename.Text.ToString(), branchid
                                    , radTextBox_Zonename.Text.ToString(), SystemClass.SystemUserID_M, SystemClass.SystemUserName);
                    string ret = ConnectionClass.ExecuteSQL_Main(Ins);
                    MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                    if (ret.Equals(""))
                    {

                        string sql = string.Format(@"SELECT TOP 1 [ZONE_ID],[ZONE_NAME],SHOP_SHELFZONE.[BRANCH_ID],BRANCH_NAME,[REAMRK],[WHOINS],[WHONAMEINS],[DATEINS],WHOUP,WHONAMEUP,DATEUP 
                                    FROM    SHOP_SHELFZONE  WITH(NOLOCK) INNER JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_SHELFZONE.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                                    WHERE   ZONE_ID  =  '{0}' AND SHOP_SHELFZONE.BRANCH_ID  = '{1}'", zoneid, branchid);
                        DataTable dt = ConnectionClass.SelectSQL_Main(sql);
                        foreach (DataRow row in dt.Rows)
                        {
                            dt_Zone.Rows.Add(row["ZONE_ID"].ToString(),
                                row["ZONE_NAME"].ToString(),
                                row["BRANCH_ID"].ToString(),
                                row["BRANCH_NAME"].ToString(),
                                row["WHOINS"].ToString(),
                                row["WHONAMEINS"].ToString(),
                                row["DATEINS"].ToString());
                        }
                    }
                }
            }

            else if (statusupdate.Equals("update"))
            {
                DataRow[] rows = dt_Zone.Select("BRANCH_ID = '" + this.radDropDownList_Branch_zone.SelectedValue.ToString() + "' AND ZONE_ID = '" + radTextBox_Zone_id.Text.Trim() + "'");
                int iRow = dt_Zone.Rows.IndexOf(rows[0]);
                string sqlUp = string.Format(@"UPDATE SHOP_SHELFZONE SET 
                                        ZONE_NAME = '{0}'
                                        ,WHOUP = '{1}'
                                        ,WHONAMEUP = '{2}'
                                        ,DATEUP = GETDATE()
                                        WHERE ZONE_ID = '{3}' AND BRANCH_ID = '{4}'"
                                    , radTextBox_Zonename.Text, SystemClass.SystemUserID_M
                                    , SystemClass.SystemUserName
                                    , zoneid
                                    , branchid);
                string returnExc = ConnectionClass.ExecuteSQL_Main(sqlUp);
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                if (returnExc.Equals(""))
                {
                    dt_Zone.Rows[iRow]["ZONE_NAME"] = radTextBox_Zonename.Text;
                    dt_Zone.Rows[iRow]["WHOUP"] = SystemClass.SystemUserID_M;
                    dt_Zone.Rows[iRow]["DATEUP"] = DateTime.Now;
                }
            }

            dt_Zone.AcceptChanges();
            this.SetTextEmpty();
        }
        private void RadButton_AddShelf_Click(object sender, EventArgs e)
        {
            string branchid = radDropDownList_Branch_shelf.SelectedValue.ToString();
            string zoneid = radDropDownList_Zone.SelectedValue.ToString();
            string shefid = radTextBox_Shelf_id.Text.Trim();

            if (string.IsNullOrEmpty(radTextBox_Emp.Text)) radTextBox_Emp.Text = "1";

            if (string.IsNullOrEmpty(radTextBox_Rate.Text)) radTextBox_Rate.Text = "0.01";

            if (string.IsNullOrEmpty(radTextBox_Image.Text)) radTextBox_Image.Text = "5";

            if (string.IsNullOrEmpty(radTextBox_Shelfname.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ระบุชื่อชั้นสินค้าให้เรียบร้อย.");
                radTextBox_Shelfname.Focus();
                return;
            }

            if (statusupdate.Equals("add"))
            {
                if (!zoneid.Equals(""))
                {
                    string Ins = string.Format(@"
                        INSERT INTO   SHOP_SHELF (SHELF_ID,SHELF_NAME,ZONE_ID,BRANCH_ID,REAMRK,RATE,WHOINS,WHONAMEINS,EMP_NUM,IMAGE) 
                        VALUES  ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')"
                                    , shefid
                                    , radTextBox_Shelfname.Text.ToString()
                                    , zoneid, branchid
                                    , radTextBox_Shelfname.Text.ToString()
                                    , Convert.ToDouble(radTextBox_Rate.Text)
                                    , SystemClass.SystemUserID_M
                                    , SystemClass.SystemUserName
                                    , int.Parse(radTextBox_Emp.Text)
                                    , int.Parse(radTextBox_Image.Text));
                    string ret = ConnectionClass.ExecuteSQL_Main(Ins);
                    MsgBoxClass.MsgBoxShow_SaveStatus(ret);
                    if (ret.Equals(""))
                    {
                        string sql = string.Format(@" 
                                SELECT TOP 1 SHELF_ID,SHELF_NAME
				                        ,SHOP_SHELF.ZONE_ID AS ZONE_ID,ZONE_NAME
				                        ,SHOP_SHELF.BRANCH_ID AS BRANCH_ID,BRANCH_NAME
				                        ,SHOP_SHELF.REAMRK AS REAMRK,RATE
				                        ,SHOP_SHELF.WHOINS AS WHOINS
				                        ,SHOP_SHELF.WHONAMEINS AS WHONAMEINS
				                        ,SHOP_SHELF.DATEINS AS DATEINS
				                        ,SHOP_SHELF.WHOUP AS WHOUP,SHOP_SHELF.WHONAMEUP AS WHONAMEUP,SHOP_SHELF.DATEUP  AS DATEUP
                                        ,EMP_NUM,[IMAGE]
                                FROM    SHOP_SHELF  WITH(NOLOCK) INNER JOIN 
								        SHOP_BRANCH WITH(NOLOCK) ON SHOP_SHELF.BRANCH_ID = SHOP_BRANCH.BRANCH_ID  INNER JOIN 
								        SHOP_SHELFZONE  WITH(NOLOCK) ON SHOP_SHELF.ZONE_ID = SHOP_SHELFZONE.ZONE_ID
                                WHERE   SHELF_ID = '{0}' AND SHOP_SHELF.ZONE_ID  =  '{1}' AND SHOP_SHELF.BRANCH_ID  = '{2}'", shefid, zoneid, branchid);

                        DataTable dt = ConnectionClass.SelectSQL_Main(sql);
                        foreach (DataRow row in dt.Rows)
                        {
                            dt_Shelf.Rows.Add(row["SHELF_ID"].ToString(),
                                row["SHELF_NAME"].ToString(),
                                row["ZONE_ID"].ToString(),
                                row["ZONE_NAME"].ToString(),
                                row["BRANCH_ID"].ToString(),
                                row["BRANCH_NAME"].ToString(),
                                row["REAMRK"].ToString(),
                                row["RATE"].ToString(),
                                row["EMP_NUM"].ToString(),
                                row["IMAGE"].ToString(),
                                row["WHOINS"].ToString(),
                                row["WHONAMEINS"].ToString(),
                                row["DATEINS"].ToString());
                        }
                    }
                }
            }
            else if (statusupdate.Equals("update"))
            {
                DataRow[] rows = dt_Shelf.Select("BRANCH_ID = '" + branchid + "' AND ZONE_ID = '" + zoneid + "' AND SHELF_ID = '" + shefid + "'");
                int iRow = dt_Shelf.Rows.IndexOf(rows[0]);

                string sqlUp = string.Format(@"UPDATE SHOP_SHELF SET 
                                        SHELF_NAME = '{0}'
                                        ,RATE = '{1}'
                                        ,WHOUP = '{2}'
                                        ,WHONAMEUP = '{3}'
                                        ,DATEUP = GETDATE()
                                        ,EMP_NUM = '{4}'
                                        ,IMAGE = '{5}'
                                        WHERE SHELF_ID = '{6}' AND ZONE_ID = '{7}' AND BRANCH_ID = '{8}'"
                                    , radTextBox_Shelfname.Text
                                    , Convert.ToDouble(radTextBox_Rate.Text)
                                    , SystemClass.SystemUserID_M
                                    , SystemClass.SystemUserName
                                    , int.Parse(radTextBox_Emp.Text)
                                    , int.Parse(radTextBox_Image.Text)
                                    , shefid
                                    , zoneid
                                    , branchid);
                string returnExc = ConnectionClass.ExecuteSQL_Main(sqlUp);
                MsgBoxClass.MsgBoxShow_SaveStatus(returnExc);
                if (returnExc.Equals(""))
                {
                    dt_Shelf.Rows[iRow]["SHELF_NAME"] = radTextBox_Shelfname.Text;
                    dt_Shelf.Rows[iRow]["WHOUP"] = SystemClass.SystemUserID_M;
                    dt_Shelf.Rows[iRow]["DATEUP"] = DateTime.Now;
                    dt_Shelf.Rows[iRow]["RATE"] = Convert.ToDouble(radTextBox_Rate.Text);
                    dt_Shelf.Rows[iRow]["EMP_NUM"] = int.Parse(radTextBox_Emp.Text);
                    dt_Shelf.Rows[iRow]["IMAGE"] = int.Parse(radTextBox_Image.Text);
                }
            }
            dt_Shelf.AcceptChanges();
            this.SetTextEmpty();
        }
        private void RadDropDownList_BranchSh_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Branch_shelf.SelectedIndex > -1) this.SetDroupDownZone(radDropDownList_Zone, radDropDownList_Branch_shelf.SelectedValue.ToString());
        }
        private void RadGridView_Zone_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow != null && e.CurrentRow.Index > -1) this.Selectshelf_byzone(this.RadGridView_Zone.CurrentRow);
            else dt_Shelf.Rows.Clear();

            if (statusupdate != null && statusupdate.Equals("update")) this.SetTextEmpty();
        }
        private void RadButtonElement_Document_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, this.Name);
        }
        #endregion

        #region Method
        private void SetProperty()
        {
            radButton_UpdateZone.ButtonElement.ShowBorder = true;
            radButton_UpdateShelf.ButtonElement.ShowBorder = true;
            radButton_Cancelzone.ButtonElement.ShowBorder = true;
            radButton_Cancelshelf.ButtonElement.ShowBorder = true;

            //radStatusStrip_Zone.Enabled = false;
            radButtonElement_Addzone.Enabled = false; //ปิดใช้งานปุ่มเพิ่มโซน
            radButtonElement_Editzone.Enabled = false; //ปิดใช้งานปุ่มแก้ไขโซน
            radButtonElement_Addshelf.Enabled = false; //ปิดใช้งานปุ่มเพิ่มชั้นวาง
            //radStatusStrip_Shelf.Enabled = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_Zone);
            DatagridClass.SetDefaultRadGridView(RadGridView_Shelf);
            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Branch_zone);
            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Branch_shelf);
            DatagridClass.SetDefaultFontDropDownShot(radDropDownList_Zone);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Zone); radGroupBox_Zone.Text = "จัดการโซน";
            DatagridClass.SetDefaultFontGroupBox(radGroupBox_Shelf); radGroupBox_Shelf.Text = "จัดการชั้นวาง";

            radTextBox_Zonename.Font = new Font(new FontFamily("Tahoma"), 9.75f);
            radTextBox_Zonename.ForeColor = Color.Blue;
            radTextBox_Shelfname.Font = new Font(new FontFamily("Tahoma"), 9.75f);
            radTextBox_Shelfname.ForeColor = Color.Blue;

            radButtonElement_Addzone.ShowBorder = true; radButtonElement_Addzone.ToolTipText = "เพิ่มโซน";
            radButtonElement_Editzone.ShowBorder = true; radButtonElement_Editzone.ToolTipText = "แก้ไขโซน";
            radButtonElement_Addshelf.ShowBorder = true; radButtonElement_Addshelf.ToolTipText = "เพิ่มชั้นวาง";
            radButtonElement_Editshelf.ShowBorder = true; radButtonElement_Editshelf.ToolTipText = "แก้ไขชั้นวาง";
            radButtonElement_Document.ShowBorder = true; radButtonElement_Document.ToolTipText = "คู่มือการใช้งาน";

            this.radStatusStrip_Zone.SizingGrip = false;
            this.radStatusStrip_Shelf.SizingGrip = false;

            radLabel_Detail.Text = @"การเพิ่มชั้นวาง ให้เลือกแถวโซนที่ต้องการจากตารางบนก่อนเท่านั้น การการแก้ไขโซนหรือชั้นวางให้เลือกแถวที่ต้องการแก้ไข จากนั้นกดปุ่มแก้ไข";


        }
        private void BindGrid()
        {
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 100));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "โซน", 70));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อ", 200));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOUP", "แก้ไขโดย", 100));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEUP", "วันเวลาที่แก้ไข", 250));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOINS", "สร้างโดย", 100));
            RadGridView_Zone.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันเวลาที่สร้าง", 250));

            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 100));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "ชั้นวาง", 70));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อ", 200));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RATE", "RATE", 70));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_NUM", "จำนวนคนดูแล", 100));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("IMAGE", "รูปถ่าย", 100));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("ZONE_ID", "โซน"));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("ZONE_NAME", "คำอธิบาย"));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOUP", "แก้ไขโดย", 100));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEUP", "วันเวลาที่แก้ไข", 250));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOINS", "สร้างโดย", 100));
            RadGridView_Shelf.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันเวลาที่สร้าง", 250));

            if (_pUser.Equals("SHOP")) this.GetDataZone(SystemClass.SystemBranchID);
            else this.GetDataZone("");
        }
        void SetTextEmpty()
        {
            statusupdate = ""; radLabel_statuszone.Text = ""; radLabel_statusshelf.Text = "";
            radDropDownList_Branch_zone.Enabled = false;
            radDropDownList_Branch_zone.SelectedIndex = -1; radDropDownList_Branch_zone.Text = string.Empty;
            radTextBox_Zonename.Enabled = false;
            radTextBox_Zonename.Text = string.Empty;
            radButton_UpdateZone.Enabled = false;

            radDropDownList_Branch_shelf.Enabled = false;
            radDropDownList_Branch_shelf.SelectedIndex = -1; radDropDownList_Branch_shelf.Text = string.Empty;
            radDropDownList_Zone.Enabled = false;
            radDropDownList_Zone.SelectedIndex = -1; radDropDownList_Zone.Text = string.Empty;
            radTextBox_Shelfname.Enabled = false;
            radTextBox_Shelfname.Text = string.Empty;
            radButton_UpdateShelf.Enabled = false;
            radTextBox_Zone_id.Text = string.Empty;
            radTextBox_Shelf_id.Text = string.Empty;
            radTextBox_Rate.Enabled = false;
            radTextBox_Emp.Enabled = false;
            radTextBox_Image.Enabled = false;
            radTextBox_Rate.Text = "0.01";
            radTextBox_Emp.Text = "1";
            radTextBox_Image.Text = "5";
        }
        void GetDataZone(string branchid)
        {
            if (!branchid.Equals(""))
            {
                branchid = @"WHERE   SHOP_SHELFZONE.BRANCH_ID = '" + branchid + "'";
            }
            string sqlzone = string.Format(@"
                    SELECT  ZONE_ID,ZONE_NAME,SHOP_SHELFZONE.BRANCH_ID AS BRANCH_ID,BRANCH_NAME
                            ,WHOINS,WHONAMEINS,DATEINS,WHOUP,WHONAMEUP,DATEUP
                    FROM    SHOP_SHELFZONE WITH (NOLOCK) INNER JOIN 
		                    SHOP_BRANCH  WITH (NOLOCK) ON  SHOP_BRANCH.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID {0}
                    ORDER BY SHOP_SHELFZONE.BRANCH_ID,ZONE_ID", branchid);
            dt_Zone = ConnectionClass.SelectSQL_Main(sqlzone);
            RadGridView_Zone.DataSource = dt_Zone;
        }
        private string GetZoneID(string _branchid)
        {
            string sql = string.Format(@"SELECT CONCAT('Z',FORMAT((ISNULL(MAX(SUBSTRING(ZONE_ID,2,2)),0)+1),'00')) AS  MAXZONEID FROM SHOP_SHELFZONE WITH(NOLOCK) WHERE BRANCH_ID = '{0}' ", _branchid);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            return dt.Rows.Count > 0 ? dt.Rows[0]["MAXZONEID"].ToString() : "";
        }
        private string GetSheflID(string _branchid, string _zoneid)
        {
            string sql = string.Format(@"SELECT CONCAT('SH',FORMAT((ISNULL(MAX(SUBSTRING(SHELF_ID,3,3)),0)+1),'000')) AS  MAXSHELFID    FROM SHOP_SHELF  WITH(NOLOCK) WHERE BRANCH_ID = '{0}'AND ZONE_ID = '{1}'", _branchid, _zoneid);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            return dt.Rows.Count > 0 ? dt.Rows[0]["MAXSHELFID"].ToString() : "";
        }
        private DataTable GetZoneDropdown(string branchid)
        {
            string sql = string.Format(@"SELECT ZONE_ID,ZONE_NAME FROM SHOP_SHELFZONE  WITH(NOLOCK) WHERE BRANCH_ID = '{0}' GROUP BY ZONE_ID,ZONE_NAME,BRANCH_ID", branchid);
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetDataShelf_Byzone(string branchid, string zoneid)
        {
            string sqlshelf = string.Format(@"
                 SELECT     SHELF_ID,SHELF_NAME,SHOP_SHELF.ZONE_ID,ZONE_NAME,SHOP_SHELF.BRANCH_ID,BRANCH_NAME
				            ,SHOP_SHELF.REAMRK,RATE,EMP_NUM,[IMAGE]
                            ,SHOP_SHELF.WHOINS,SHOP_SHELF.WHONAMEINS,SHOP_SHELF.DATEINS,SHOP_SHELF.WHOUP
                            ,SHOP_SHELF.WHONAMEUP
                            ,SHOP_SHELF.DATEUP
                 FROM       SHOP_SHELF WITH (NOLOCK) INNER JOIN
		                    SHOP_SHELFZONE  WITH (NOLOCK) ON  SHOP_SHELFZONE.ZONE_ID = SHOP_SHELF.ZONE_ID AND SHOP_SHELF.BRANCH_ID = SHOP_SHELFZONE.BRANCH_ID INNER JOIN
						    SHOP_BRANCH WITH (NOLOCK) ON SHOP_SHELF.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                 WHERE      SHOP_SHELF.BRANCH_ID = '{0}' AND SHOP_SHELF.ZONE_ID = '{1}'
                 ORDER BY   SHOP_SHELF.BRANCH_ID,SHOP_SHELF.ZONE_ID,SHELF_ID", branchid, zoneid);
            return ConnectionClass.SelectSQL_Main(sqlshelf);
        }
        void SetDroupDownBrach(RadDropDownList DropDown)
        {
            DropDown.DataSource = dtBranch;
            DropDown.DisplayMember = "NAME_BRANCH";
            DropDown.ValueMember = "BRANCH_ID";
            //DropDown.SelectedIndex = 0;
        }
        void SetDroupDownBrach_Shelf(RadDropDownList DropDown)
        {
            DropDown.DataSource = _dtBranch;
            DropDown.DisplayMember = "NAME_BRANCH";
            DropDown.ValueMember = "BRANCH_ID";
            //DropDown.SelectedIndex = 0;
        }
        void SetDroupDownZone(RadDropDownList DropDown, string branchid)
        {

            DropDown.DataSource = GetZoneDropdown(branchid);
            DropDown.ValueMember = "ZONE_ID";
            DropDown.DisplayMember = "ZONE_NAME";
        }
        private void UpdateGroupInfo(GridViewRowInfo currentRow, string namegroup)
        {
            if (currentRow != null && !(currentRow is GridViewNewRowInfo))
            {
                switch (namegroup)
                {
                    case "zone":
                        this.radDropDownList_Branch_zone.SelectedValue = currentRow.Cells["BRANCH_ID"].Value.ToString();
                        this.radTextBox_Zone_id.Text = currentRow.Cells["ZONE_ID"].Value.ToString();
                        this.radTextBox_Zonename.Text = currentRow.Cells["ZONE_NAME"].Value.ToString();

                        radTextBox_Zonename.Enabled = true; radTextBox_Zonename.Focus();
                        radButton_UpdateZone.Enabled = true;
                        break;
                    case "shelf":
                        this.radDropDownList_Branch_shelf.SelectedValue = currentRow.Cells["BRANCH_ID"].Value.ToString();
                        this.radDropDownList_Zone.SelectedValue = currentRow.Cells["ZONE_ID"].Value.ToString();
                        radTextBox_Shelfname.Enabled = true; radTextBox_Shelfname.Focus();
                        radButton_UpdateShelf.Enabled = true;
                        radTextBox_Emp.Enabled = true;
                        radTextBox_Rate.Enabled = true;
                        radTextBox_Image.Enabled = true;
                        break;
                }
            }
            else
            {
                this.radTextBox_Zonename.Text = string.Empty;
                this.radTextBox_Shelfname.Text = string.Empty;
            }
        }
        void Selectshelf_byzone(GridViewRowInfo currentRow)
        {
            if (currentRow != null && !(currentRow is GridViewNewRowInfo))
            {
                dt_Shelf = this.GetDataShelf_Byzone(currentRow.Cells["BRANCH_ID"].Value.ToString(), currentRow.Cells["ZONE_ID"].Value.ToString());
                RadGridView_Shelf.DataSource = dt_Shelf;
            }
        }
        private void Key_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");

        }
        #endregion

        #region Zone
        private void RadButtonElement_Addzone_Click(object sender, EventArgs e)
        {
            statusupdate = "add"; radLabel_statuszone.Text = "เพิ่มโซน";
            radDropDownList_Branch_shelf.SelectedIndex = -1;
            radDropDownList_Branch_zone.Enabled = true; radDropDownList_Branch_zone.Focus(); radDropDownList_Branch_zone.SelectedIndex = 0;
            radTextBox_Zonename.Enabled = true;
            radButton_UpdateZone.Enabled = true;
            if (_pUser.Equals("SHOP"))
            {
                radDropDownList_Branch_zone.Enabled = false; radDropDownList_Branch_zone.SelectedValue = SystemClass.SystemBranchID;
            }
        }
        private void RadButtonElement_Editzone_Click(object sender, EventArgs e)
        {
            this.SetTextEmpty();

            if (RadGridView_Zone.CurrentRow.Index > -1 && this.RadGridView_Zone.CurrentRow != null && !(this.RadGridView_Zone.CurrentRow is GridViewNewRowInfo))
            {
                statusupdate = "update"; radLabel_statuszone.Text = "แก้ไขโซน";
                this.UpdateGroupInfo(this.RadGridView_Zone.CurrentRow, "zone");
            }
        }
        private void RadButton_Cancelzone_Click(object sender, EventArgs e)
        {
            statusupdate = ""; radLabel_statuszone.Text = statusupdate;
            this.radDropDownList_Branch_zone.SelectedIndex = -1; this.radDropDownList_Branch_zone.Enabled = false;
            this.radTextBox_Zone_id.Text = string.Empty;
            this.radTextBox_Zonename.Text = string.Empty; this.radTextBox_Zonename.Enabled = false;
            this.radButton_UpdateZone.Enabled = false;
        }
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Branch_zone.SelectedIndex > -1)
            {
                if (statusupdate != null && statusupdate.Equals("add")) radTextBox_Zone_id.Text = GetZoneID(radDropDownList_Branch_zone.SelectedValue.ToString());
                
                radTextBox_Zonename.Focus();
            }
        }
        #endregion

        #region Shelf
        private void RadButtonElement_AddShelf_Click(object sender, EventArgs e)
        {
            if (RadGridView_Zone.CurrentRow.Index > -1 && this.RadGridView_Zone.CurrentRow != null && !(this.RadGridView_Zone.CurrentRow is GridViewNewRowInfo))
            {
                this.SetTextEmpty();
                statusupdate = "add"; radLabel_statusshelf.Text = "เพิ่มชั้นวาง";
                this.UpdateGroupInfo(this.RadGridView_Zone.CurrentRow, "shelf");
                this.radTextBox_Shelf_id.Text = GetSheflID(radDropDownList_Branch_shelf.SelectedValue.ToString(), radDropDownList_Zone.SelectedValue.ToString());
            }
        }
        private void RadButtonElement_EditShelf_Click(object sender, EventArgs e)
        {
            this.SetTextEmpty();
            if (RadGridView_Shelf.Rows.Count > 0 && RadGridView_Shelf.CurrentRow.Index > -1 && this.RadGridView_Shelf.CurrentRow != null && !(this.RadGridView_Shelf.CurrentRow is GridViewNewRowInfo))
            {
                statusupdate = "update"; radLabel_statusshelf.Text = "แก้ไขชั้นวาง";
                this.UpdateGroupInfo(this.RadGridView_Shelf.CurrentRow, "shelf");
                this.radTextBox_Shelf_id.Text = this.RadGridView_Shelf.CurrentRow.Cells["SHELF_ID"].Value.ToString();
                this.radTextBox_Shelfname.Text = this.RadGridView_Shelf.CurrentRow.Cells["SHELF_NAME"].Value.ToString();
                this.radTextBox_Rate.Text = this.RadGridView_Shelf.CurrentRow.Cells["RATE"].Value.ToString();
                this.radTextBox_Emp.Text = this.RadGridView_Shelf.CurrentRow.Cells["EMP_NUM"].Value.ToString();
                this.radTextBox_Image.Text = this.RadGridView_Shelf.CurrentRow.Cells["IMAGE"].Value.ToString();
            }
        }
        private void RadButton_Cancelshelf_Click(object sender, EventArgs e)
        {
            statusupdate = ""; radLabel_statusshelf.Text = statusupdate;
            this.radDropDownList_Branch_shelf.SelectedIndex = -1; radDropDownList_Branch_shelf.Enabled = false;
            this.radDropDownList_Zone.SelectedIndex = -1;
            this.radTextBox_Shelf_id.Text = string.Empty; radTextBox_Rate.Text = string.Empty;
            this.radTextBox_Shelfname.Text = string.Empty; this.radTextBox_Shelfname.Enabled = false;
            this.radTextBox_Emp.Text = string.Empty;
            this.radTextBox_Image.Text = string.Empty;
        }
        private void RadGridView_Shelf_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            this.SetTextEmpty();
        }
        private void RadTextBox_Rate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as RadTextBox).Text.IndexOf('.') > -1)) e.Handled = true;

        }
        private void RadTextBox_Rate_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(radTextBox_Rate.Text)) if (Convert.ToDouble(radTextBox_Rate.Text) > 100) radTextBox_Rate.Text = "";

        }
        private void RadTextBox_Emp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) e.Handled = true;

        }
        private void RadTextBox_Image_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(radTextBox_Image.Text)) if (Convert.ToDouble(radTextBox_Image.Text) > 12) radTextBox_Image.Text = "12";

        }
        #endregion
    }
}