﻿using PC_Shop24Hrs.Controllers;
using System;
using System.Data;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    class ShelfClass
    {
        public static DataTable GetZone_ByBranch(string Branch)
        {
            string sql = $@"	
                select  ZONE_ID,ZONE_ID + ' ' + ZONE_NAME as ZONE_NAME 
				from    shop_shelfzone   with (nolock) 
                where   branch_id='{Branch}'";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable GetShelf_ByBranch(string Branch)
        {
            string sql = $@"
                    select      '' as CheckShelfEmpl,shop_shelfzone.zone_id, zone_name, shelf_id, shelf_name,EMP_NUM
                    from        shop_shelf with (nolock) 
                                inner join shop_shelfzone with (nolock) on shop_shelfzone.zone_id= shop_shelf.zone_id
                    where       shop_shelfzone.branch_id= '{Branch}' AND shop_shelf.branch_id= '{Branch}' 
                    order by    shop_shelfzone.zone_id, shelf_id, shelf_name ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable GetShelfEmplDate(string Branch, string month, string Year, string Condition)
        {
            string sql = $@"  
                select  SHELF_ID, SHELF_NAME, ZONE_ID, BRANCH_ID, EMPL_ID, EMPL_NAME,
                        DATE_EMPL,day(DATE_EMPL)+3  as DayDATE_EMPL,day(DATE_EMPL)+2  as DayDATE_SHELF, 
                        SHIFTID,DATE_EVENT,DATE_EVENTNAME
                from    SHOP_SHELFEMPLDATE  with (nolock)  
                where   BRANCH_ID = '{Branch}'  and month(DATE_EMPL)='{month}'
                        and year(DATE_EMPL)='{Year}'   {Condition} 
                order by DATE_EMPL ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable GetEmplWorkAllDate(string Empl, string Date)
        {
            string sql = $@"EXEC[dbo].[SPC_HROS_FINDEMPLSHIFTTABLEBYALTNUM]
                                @ALTNUM = N'{Empl}',
                                @TRANSDATE = N'{Date}'; ";

            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConHROS);
        }

        public static DataTable GetEmplEVENT(string Branch, string DateBegin, string DateEnd)
        {
            string sql = $@"
            DECLARE @BRANCH NVARCHAR(8) = N'{Branch}';
            DECLARE @TRANSDATEBEGIN DATETIME; 
            SET     @TRANSDATEBEGIN = CAST('{DateBegin}' AS DATE); 
            DECLARE @TRANSDATEEND DATETIME; 
            SET     @TRANSDATEEND = CAST('{DateEnd}' AS DATE); 

            SELECT  PWADJTIME.*,CONVERT(varchar,PWADJTIME.PWDATEADJ,23) as CVPWDATEADJ, 
                    IIF(PWUPDTYPE = '=', 'FULL-DAY', IIF(PWUPDTYPE = '<', 'FIRST-HAFT', 'SECOND-HALF')) AS PWUPDTYPEDESC, 
                    RTRIM(PWEVENT.PWDESC) AS PWEVENTNAME 
            FROM    PWADJTIME WITH(NOLOCK) LEFT JOIN PWEVENT ON PWADJTIME.PWEVENT = PWEVENT.PWEVENT 
            WHERE  PWDATEADJ BETWEEN @TRANSDATEBEGIN AND @TRANSDATEEND 
                     AND EXISTS
                     (
                      SELECT 'X'
                      FROM PWEMPLOYEE_V_SPC E WITH(NOLOCK)
                      WHERE PWADJTIME.PWEMPLOYEE = E.PWEMPLOYEE
                       AND E.PWEDESC = @BRANCH  AND E.PWDEPTNAME not like '%ผู้จัดการ%'
                     ) ";
            return ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConHROS);
        }


        public static String GetDateSchedule(string Empl, string Date)
        {
            string sql = $@"
                DECLARE @ID NVARCHAR(8) = N'{Empl}';
                DECLARE @TRANSDATE DATETIME;  
                SET @TRANSDATE = CAST('{Date}' AS DATE);

                IF EXISTS (SELECT 'X' FROM PWADJTIME WHERE PWEMPLOYEE = @ID AND PWDATEADJ = @TRANSDATE)
                 BEGIN 
                  SELECT PWADJTIME.*
                   , IIF(PWUPDTYPE = '=', 'FULL-DAY', IIF(PWUPDTYPE = '<', 'FIRST-HAFT', 'SECOND-HALF')) AS PWUPDTYPEDESC
                   , RTRIM(PWEVENT.PWDESC) AS PWEVENTNAME 
                  FROM PWADJTIME WITH(NOLOCK) LEFT JOIN PWEVENT ON PWADJTIME.PWEVENT = PWEVENT.PWEVENT
                  WHERE PWEMPLOYEE = @ID  AND PWDATEADJ = @TRANSDATE
                 END
                ELSE
                 BEGIN
                  SELECT  [dbo].[SPC_FINDEMPLSHIFTIDBYDATE] (@ID, @TRANSDATE) AS SHIFTID
                 END ";

            DataTable dt = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConHROS);
            //DataRow[] rowResult;
            string Result = "WORK";
            if (dt.Rows.Count > 0)
            {
                DataColumnCollection dataColumn = dt.Columns;
                if (dataColumn.Contains("PWEVENTNAME"))
                {
                    Result = dt.Rows[0]["PWEVENT"].ToString() + "|" + dt.Rows[0]["PWEVENTNAME"].ToString() + "|" + dt.Rows[0]["PWUPDTYPEDESC"].ToString();
                }
                else
                {
                    if (dt.Rows[0]["SHIFTID"].ToString().Replace(" ", "").Equals("")) Result = "DAYOFF";
                }
            }
            return Result;
        }
        //ค้นหาชื่อ Zone Shelf
        public static DataTable GetInformationShelfName_ByShelfID(string pBchID, string pZoneID, string pShelfID)
        {
            string sql = $@"
                SELECT	SHOP_SHELFZONE.ZONE_ID,ZONE_NAME, SHELF_ID, SHELF_NAME
                FROM	SHOP_SHELF with (nolock)
                        INNEr JOIN SHOP_SHELFZONE with (nolock)	ON SHOP_SHELF.ZONE_ID = SHOP_SHELFZONE.ZONE_ID
                WHERE	SHOP_SHELFZONE.BRANCH_ID= '{pBchID}'
		                AND SHOP_SHELF.BRANCH_ID= '{pBchID}'
                        AND SHOP_SHELFZONE.ZONE_ID='{pZoneID}'
		                AND SHOP_SHELF.SHELF_ID = '{pShelfID}'
                ORDER BY SHOP_SHELFZONE.ZONE_ID, SHELF_ID, SHELF_NAME ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาข้อมูลสินค้า
        public static DataTable GetInformationItembarcode_ByItembarcode(string pBchID, string pItembarcode)
        {
            string sql = $@"
                SELECT	ITEMBARCODE,'' AS SPC_ITEMNAME,SHELF_ID,ZONE_ID,
		                CASE ISNULL(SHOP_SHELFITEMS.ITEMBARCODE,'0') WHEN '0' THEN '0' ELSE '1' END  AS STAUP 
                FROM	SHOP_SHELFITEMS	WITH (NOLOCK)  
                WHERE	ITEMBARCODE = '{pItembarcode}'  AND BRANCH_ID = '{pBchID}'
                ORDER BY ITEMBARCODE ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
 
        //จำนวนพนักงานประจำล็อค
        public static int GetShelfSettingEmpl(string Branch, string Zone, string Shelf)
        {
            int EmplSetting = 0;
            string sql = $@"
                SELECT  ISNULL(EMP_NUM,0) AS EMP_NUM 
                FROM    SHOP_SHELF WITH(NOLOCK) 
                WHERE   BRANCH_ID='{Branch}' AND ZONE_ID='{Zone}' AND SHELF_ID='{Shelf}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) EmplSetting = int.Parse(dt.Rows[0]["EMP_NUM"].ToString());
            return EmplSetting;
        }

        //จำนวนพนักงานดูแลล็อคประจำในแต่ละวัน
        public static int GetShelEmplCountDate(string Branch, string Zone, string Shelf, string DateEmpl)
        {
            int CountDate = 0;
            string sql = $@"
                SELECT   count(SHOP_SHELFEMPLDATE.EMPL_ID) as EMPL_ID 
                FROM    SHOP_SHELFEMPLDATE WITH(NOLOCK)   
                        INNER JOIN SHOP_SHELFEMPL WITH(NOLOCK) on SHOP_SHELFEMPLDATE.EMPL_ID = SHOP_SHELFEMPL.EMPL_ID
                            AND SHOP_SHELFEMPLDATE.BRANCH_ID = SHOP_SHELFEMPL.BRANCH_ID
                            AND SHOP_SHELFEMPLDATE.ZONE_ID = SHOP_SHELFEMPL.ZONE_ID
                            AND SHOP_SHELFEMPLDATE.SHELF_ID = SHOP_SHELFEMPL.SHELF_ID
                WHERE   SHOP_SHELFEMPLDATE.BRANCH_ID = '{Branch}' AND SHOP_SHELFEMPLDATE.ZONE_ID = '{Zone}'
                        AND SHOP_SHELFEMPLDATE.SHELF_ID = '{Shelf}' AND DATE_EMPL = '{DateEmpl}' AND DATE_EVENT = '' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) CountDate = int.Parse(dt.Rows[0]["EMPL_ID"].ToString());
            return CountDate;
        }

        //จำนวนพนักงานดูแลล็อคแทนในแต่ละวัน
        public static int GetShelEmplReplaceCountDate(string Branch, string Zone, string Shelf, string DateEmpl)
        {
            int CountDate = 0;
            string sql = $@"
                SELECT  count(SHOP_SHELFEMPLDATE.EMPL_ID) as EMPL_ID 
                FROM    SHOP_SHELFEMPLDATE WITH(NOLOCK)  
                        LEFT OUTER JOIN SHOP_SHELFEMPL WITH(NOLOCK) ON SHOP_SHELFEMPLDATE.EMPL_ID = SHOP_SHELFEMPL.EMPL_ID
                            AND SHOP_SHELFEMPLDATE.BRANCH_ID = SHOP_SHELFEMPL.BRANCH_ID
                            AND SHOP_SHELFEMPLDATE.ZONE_ID = SHOP_SHELFEMPL.ZONE_ID
                            AND SHOP_SHELFEMPLDATE.SHELF_ID = SHOP_SHELFEMPL.SHELF_ID
                WHERE   SHOP_SHELFEMPLDATE.BRANCH_ID = '{Branch}' AND SHOP_SHELFEMPLDATE.ZONE_ID = '{Zone}'
                        AND SHOP_SHELFEMPLDATE.SHELF_ID = '{Shelf}' AND DATE_EMPL = '{DateEmpl}' AND DATE_EVENT = '' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) CountDate = int.Parse(dt.Rows[0]["EMPL_ID"].ToString());
            return CountDate;
        }

        // พนักงานที่เลือกเป็นพนักงานดูแลล็อคแทนในวันนั้นๆ หรือไม่
        public static bool CheckShelEmplReplaceDate(string Branch, string Zone, string Shelf, string DateEmpl, string Empl_ID)
        {
            bool StatusEmplReplace = false;
            string sql = $@"
                SELECT  SHOP_SHELFEMPLDATE.EMPL_ID 
                FROM    SHOP_SHELFEMPLDATE WITH(NOLOCK)  
                        LEFT OUTER JOIN SHOP_SHELFEMPL WITH(NOLOCK) ON SHOP_SHELFEMPLDATE.EMPL_ID = SHOP_SHELFEMPL.EMPL_ID
                            AND SHOP_SHELFEMPLDATE.BRANCH_ID = SHOP_SHELFEMPL.BRANCH_ID
                            AND SHOP_SHELFEMPLDATE.ZONE_ID = SHOP_SHELFEMPL.ZONE_ID
                            AND SHOP_SHELFEMPLDATE.SHELF_ID = SHOP_SHELFEMPL.SHELF_ID
                WHERE   SHOP_SHELFEMPLDATE.BRANCH_ID = '{Branch}' AND SHOP_SHELFEMPLDATE.ZONE_ID = '{Zone}'
                        AND SHOP_SHELFEMPLDATE.SHELF_ID = '{Shelf}' AND DATE_EMPL = '{DateEmpl}' AND SHOP_SHELFEMPLDATE.EMPL_ID = '{Empl_ID}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) StatusEmplReplace = true;
            return StatusEmplReplace;
        }

        //สถานะพนักงาน และล็อค เป็นพนักงานประจำ (True) หรือ พนักงานแทน(False)
        public static bool CheckShelEmplStatus(string Branch, string Zone, string Shelf, string Empl)
        {
            bool StatusEmpl = true;
            string sql = $@"
                SELECT  EMPL_ID 
                FROM    SHOP_SHELFEMPL WITH(NOLOCK)  
                WHERE   BRANCH_ID='{Branch}' AND ZONE_ID='{Zone}' AND SHELF_ID='{Shelf}' AND EMPL_ID='{Empl}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) StatusEmpl = false;
            return StatusEmpl;
        }

        public static bool CheckEmplMonthSetting(string Branch, string month, string years, string emplid)
        {
            bool CountDate = false;
            string sql = $@"     
                        SELECT  * 
                        FROM    SHOP_SHELFEMPLDATE WITH(NOLOCK)
                        WHERE   BRANCH_ID='{Branch}'
                                AND MONTH(DATE_EMPL)='{month}'
                                AND YEAR(DATE_EMPL)='{years}'
                                AND EMPL_ID = '{emplid}' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) CountDate = true;
            return CountDate;
        }

        public static bool CheckEmplShelf(string Branch, string month, string years, string Zone, string Shelf, string emplid)
        {
            bool CountDate = false;
            string sql = $@" 
                        SELECT  * 
                        FROM    SHOP_SHELFEMPLDATE WITH(NOLOCK)
                        WHERE   BRANCH_ID='{Branch}'
                                AND MONTH(DATE_EMPL)='{month}'
                                AND YEAR(DATE_EMPL)='{years}'
                                AND ZONE_ID='{Zone}'
                                AND SHELF_ID ='{Shelf}'
                                AND EMPL_ID = '{emplid}' ";
            DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) CountDate = true;
            return CountDate;
        }

        public static DataTable GetEmplShelf(string Branch, string month, string years, string emplid)
        {
            string sql = $@" 
                        SELECT  SHELF_ID,SHELF_NAME,ZONE_ID,BRANCH_ID,EMPL_ID,
                                EMPL_NAME,convert(varchar,DATE_EMPL,23) as DATE_EMPL,SHIFTID,DATE_EVENT,DATE_EVENTNAME 
                        FROM    SHOP_SHELFEMPLDATE WITH(NOLOCK)
                        WHERE   BRANCH_ID='{Branch}'
                                AND MONTH(DATE_EMPL)='{month}'
                                AND YEAR(DATE_EMPL)='{years}' 
                                AND EMPL_ID = '{emplid}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #region setting 

        public static DataTable GetShelfEmplSetting(string Branch, string Condition)
        {
            string sql = $@"  
                select  SHELF_ID, SHELF_NAME, ZONE_ID, BRANCH_ID, EMPL_ID, EMPL_NAME 
                from    SHOP_SHELFEMPL 
                where   BRANCH_ID = '{Branch}'   {Condition} 
                order by ZONE_ID,SHELF_ID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable GetShelfSettingColumns(string Branch)
        {
            string sql = $@"  
                select  '' as CheckShelfEmpl,SHOP_SHELF.ZONE_ID, SHOP_SHELFZONE.ZONE_NAME,SHOP_SHELF.SHELF_ID,SHELF_NAME,EMP_NUM,isnull(CountEmpl,0) as CountEmpl 
                from    SHOP_SHELF WITH (NOLOCK)
                        left outer join 
                        (
                            select ZONE_ID, SHELF_ID, count(EMPL_ID) as CountEmpl
                            from SHOP_SHELFEMPL WITH (NOLOCK)
                            where  BRANCH_ID = '{Branch}' 
                            group by ZONE_ID, SHELF_ID)SHELFEMPL on  SHOP_SHELF.ZONE_ID=SHELFEMPL.ZONE_ID AND SHOP_SHELF.SHELF_ID=SHELFEMPL.SHELF_ID
			            inner join SHOP_SHELFZONE WITH (NOLOCK)  on  SHOP_SHELF.ZONE_ID=SHOP_SHELFZONE.ZONE_ID 
                where   SHOP_SHELF.BRANCH_ID = '{Branch}' 
                        AND SHOP_SHELFZONE.BRANCH_ID = '{Branch}' 
                Order by  SHOP_SHELF.ZONE_ID,SHOP_SHELF.SHELF_ID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        public static DataTable GetEmplShelf(string Branch)
        {
            string sql = $@"
                    select  DISTINCT EMPL_ID as EMPLID,EMPL_NAME as SPC_NAME,Shop_Branch.BRANCH_ID AS NUM,BRANCH_NAME AS  DESCRIPTION,
                            Shop_Branch.BRANCH_ID + '-' + BRANCH_NAME AS DeptDesc, '' AS POSSITION
                    from    SHOP_SHELFEMPL WITH (NOLOCK)
                            INNER JOIN SHOP_BRANCH WITH (NOLOCK)  ON SHOP_SHELFEMPL.BRANCH_ID = Shop_Branch.BRANCH_ID
                    where   SHOP_SHELFEMPL.BRANCH_ID = '{Branch}' ";

            return ConnectionClass.SelectSQL_Main(sql);
        }



        #endregion


        #region Process Shelf Empl

        public static DataTable GetShelfEmplID(string EmplID, string Branch)
        {
            string sql = $@" 	
                select  SHELF_ID, SHELF_NAME, ZONE_ID, BRANCH_ID, EMPL_ID, EMPL_NAME, REAMRK
                from    shop_shelfempl WITH (NOLOCK)  
	            where   empl_id='{EmplID}'
	                    and branch_id='{Branch}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }

        #endregion
    }

    class ShelfGetSetClass
    {
        public string Branch { get; set; }
        public string Zone { get; set; }
        public string Shelf { get; set; }
        public string ShelfName { get; set; }
        public string EMPL_ID { get; set; }
        public string EMPL_M { get; set; }
        public string EMPL_NAME { get; set; }
        public string EMP_NUM { get; set; }
        public int CountEmpl { get; set; }

        public string DateShelf { get; set; }
        public string DATE_EVENT { get; set; }
        public string DATE_EVENTNAME { get; set; }

        public ShelfGetSetClass()
        {

        }
        public ShelfGetSetClass(String branch, String TypeSetting, String TypeCondition, RadGridView gridViewShow, RadGridView gridViewShelfOrEmpl)
        {
            if (TypeSetting.Equals("EmplSetting"))
            {
                switch (TypeCondition)
                {
                    case "Shelf":
                        EMP_NUM = gridViewShelfOrEmpl.CurrentRow.Cells["EMP_NUM"].Value.ToString();
                        CountEmpl = int.Parse(gridViewShelfOrEmpl.CurrentRow.Cells["CountEmpl"].Value.ToString());
                        break;
                    default:
                        EMP_NUM = gridViewShow.CurrentRow.Cells["EMP_NUM"].Value.ToString();
                        CountEmpl = int.Parse(gridViewShow.CurrentRow.Cells["CountEmpl"].Value.ToString());
                        break;
                }
            }
            else
            {
                EMP_NUM = "0";
                CountEmpl = 0;
            }

            switch (TypeCondition)
            {
                case "Shelf":
                    Branch = branch;
                    Zone = gridViewShelfOrEmpl.CurrentRow.Cells["ZONE_ID"].Value.ToString();
                    Shelf = gridViewShelfOrEmpl.CurrentRow.Cells["Shelf_ID"].Value.ToString();
                    ShelfName = gridViewShelfOrEmpl.CurrentRow.Cells["Shelf_NAME"].Value.ToString();

                    EMPL_M = gridViewShow.CurrentRow.Cells["EMPL_ID"].Value.ToString();
                    EMPL_ID = EMPL_M.Substring(1, 7);
                    EMPL_NAME = gridViewShow.CurrentRow.Cells["EMPL_NAME"].Value.ToString();

                    break;
                default:
                    Branch = branch;
                    Zone = gridViewShow.CurrentRow.Cells["ZONE_ID"].Value.ToString();
                    Shelf = gridViewShow.CurrentRow.Cells["SHELF_ID"].Value.ToString();
                    ShelfName = gridViewShow.CurrentRow.Cells["SHELF_NAME"].Value.ToString();

                    EMPL_M = gridViewShelfOrEmpl.CurrentRow.Cells["EMPL_ID"].Value.ToString();
                    EMPL_ID = EMPL_M.Substring(1, 7);
                    EMPL_NAME = gridViewShelfOrEmpl.CurrentRow.Cells["SPC_NAME"].Value.ToString();
                    break;
            }
        }
    }

    class ShelfGetSetIndexClass
    {
        public string IndexRowId { get; set; }
        public string IndexRowName { get; set; }
        public int IndexRow { get; set; }
        public int IndexEmplSetting { get; set; }
    }
}
