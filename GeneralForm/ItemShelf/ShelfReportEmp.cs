﻿//CheckOK
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.Windows.Diagrams.Core;
using PC_Shop24Hrs.Controllers;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    public partial class ShelfReportEmp : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeUser;
        DataTable dtBranch;
        DataTable dt = new DataTable();
        DataRow[] drBranchs;
        string tiitle;
        string[] colmnDate;
        List<string> ListColmns;
        #region list 
        readonly ArrayList ListExt = new ArrayList();
        #endregion
        public ShelfReportEmp(string pTypeUser)
        {
            InitializeComponent();
            _pTypeUser = pTypeUser;
        }
        private void ShelfReportEmp_Load(object sender, EventArgs e)
        {
            tiitle = @"ค่าคอมพนักงานดูแลชั้นวาง";

            this.SetProperty();
            dtBranch = ShelfReportClass.GetBranch();

            this.SetDropdownlistBranch(dtBranch);
            this.ClearData();
            if (_pTypeUser.Equals("SHOP"))
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                RadDropDownList_Branch.ReadOnly = true;
                radCheckBox_Branch.Checked = true;
                radCheckBox_Branch.ReadOnly = true;
            }
            else
            {
                radCheckBox_Branch.Checked = true;
                RadDropDownList_Branch.SelectedValue = "MN063";
            }
            this.BranchSelect();
        }

        #region Dropdownlist
        void SetDropdownlistBranch(DataTable _dtBranch)
        {
            RadDropDownList_Branch.DataSource = _dtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            if (SystemClass.SystemBranchID != "MN000")
            {
                RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                RadDropDownList_Branch.Enabled = false;
            }
            else
            {
                RadDropDownList_Branch.Enabled = true;
            }
        }
        void BranchSelect()
        {
            drBranchs = radCheckBox_Branch.Checked ? drBranchs = dtBranch.Select("BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + "'") : drBranchs = dtBranch.Select();
        }

        #endregion


        #region InitializeGrid
        private void ActionColumnAdd(string column)
        {
            dt.Columns.Add(new DataColumn(column, typeof(string)));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight(column, string.Format(@"{0}-{1}-{2}" + Environment.NewLine + @"(ยอดค่าคอม)"
                , column.Substring(4, 2), column.Substring(6, 2), column.Substring(8, 4)), 100));
            ListColmns.Add(column);
        }
        private void BindToDataShelf()
        {
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "รหัสโซน", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "รหัสชั้นวาง", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("RATE", "เรท [%]", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALE", "ยอดขาย [บาท]", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Commision", "ค่าคอม", 100));
            RadGridView_Show.MasterTemplate.Columns.ForEach(radGridViewColmn => { radGridViewColmn.IsPinned = true; dt.Columns.Add(radGridViewColmn.FieldName); });
            colmnDate = this.GetColumnsDate(radDateTimePicker_DateEnd.Value.Subtract(radDateTimePicker_DateBegin.Value).Days + 1);

            ListColmns = new List<string>();
            Action<string> action = new Action<string>(ActionColumnAdd);
            Array.ForEach(colmnDate.ToArray(), action);
            ListColmns.Add("SALE");
            ListColmns.Add("Commision");
            this.GetGridViewSummary(ListColmns.ToArray(), GridAggregateFunction.Sum);

            dt = this.GetDataComByShelf(radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd"), colmnDate);
            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ข้อมูลสินค้ายังไม่ถูกกำหนดในชั้นวาง.");
                return;
            }
            else
            {
                RadGridView_Show.DataSource = dt;
            }
        }
        private void BindToEmp()
        {
            toolStrip_Label.Items.Clear();
            ToolStripLabel toolTiplalel1 = new ToolStripLabel
            {
                Text = "ดับเบิ้ลคลิก >> ดูรายละเอียด"
            };
            toolStrip_Label.Items.Add(toolTiplalel1);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_ID", "รหัสพนักงาน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMP_NAME", "ชื่อพนักงาน", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Commision", "ค่าคอม", 100));
            RadGridView_Show.MasterTemplate.Columns.ForEach(radGridViewColmn => { radGridViewColmn.IsPinned = true; dt.Columns.Add(radGridViewColmn.FieldName); });
            colmnDate = this.GetColumnsDate(radDateTimePicker_DateEnd.Value.Subtract(radDateTimePicker_DateBegin.Value).Days + 1);

            ListColmns = new List<string>();
            Action<string> action = new Action<string>(ActionColumnAdd);
            Array.ForEach(colmnDate.ToArray(), action);
            ListColmns.Add("Commision");
            this.GetGridViewSummary(ListColmns.ToArray(), GridAggregateFunction.Sum);

            dt = this.GetDataCommisByEmployee(radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd"), colmnDate);
            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ข้อมูลพนักงานดูแลชั้นวางยังไม่ถูกกำหนดในวันที่." + Environment.NewLine + radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd") + " ถึง " + radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd"));
                return;
            }
            else
            {
                RadGridView_Show.DataSource = dt;
            }
        }
        private void BindToBranch()
        {
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 70));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALE", "ยอดขาย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Commision", "ค่าคอม", 100));
            RadGridView_Show.MasterTemplate.Columns.ForEach(radGridViewColmn => { radGridViewColmn.IsPinned = true; dt.Columns.Add(radGridViewColmn.FieldName); });
            colmnDate = this.GetColumnsDate(radDateTimePicker_DateEnd.Value.Subtract(radDateTimePicker_DateBegin.Value).Days + 1);

            ListColmns = new List<string>();
            Action<string> action = new Action<string>(ActionColumnAdd);
            Array.ForEach(colmnDate.ToArray(), action);

            ListColmns.Add("SALE");
            ListColmns.Add("Commision");
            this.GetGridViewSummary(ListColmns.ToArray(), GridAggregateFunction.Sum);

            foreach (DataRow drBranch in drBranchs)
            {

                DataRow _dr = this.GetDataCommisByBranch(colmnDate, drBranch["BRANCH_ID"].ToString(), drBranch["BRANCH_NAME"].ToString()
                    , ShelfReportClass.GetSaleAll(drBranch["BRANCH_ID"].ToString(), radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd"))
                    , ShelfReportClass.GetEmployee(drBranch["BRANCH_ID"].ToString(), radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd")));

                if (!_dr.IsNull("BRANCH_ID"))
                    dt.Rows.Add(_dr);
            }

            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ข้อมูลสินยังไม่ถูกกำหนดในชั้นวาง.");
                return;
            }
            else
            {
                RadGridView_Show.DataSource = dt;
            }
        }
        #endregion

        #region Methode
        private void SetProperty()
        {
            radButtonElement_Pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_Pdf.ShowBorder = true;
            radButtonElement_Excel.ToolTipText = "บันทึก excel"; radButtonElement_Excel.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_DateEnd, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_DateBegin, DateTime.Now.AddDays(-7), DateTime.Now.AddDays(-1));

            radDateTimePicker_DateBegin.Value = new DateTime(radDateTimePicker_DateBegin.Value.Year, radDateTimePicker_DateBegin.Value.Month, 1);
            radDateTimePicker_DateEnd.Value = radDateTimePicker_DateBegin.Value.AddDays(7);

            radCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadGridView_Show.Visible = true;
            RadGridView_Show.EnableFiltering = false;
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontGroupBox(radGroupBox1);
            this.toolStrip_Label.BackColor = Color.FromArgb(173, 244, 215);
            this.toolStrip_Label.Font = new Font(new FontFamily("Tahoma"), 9.75f);

        }
        void ClearData()
        {
            this.RadGridView_Show.MasterTemplate.Columns.Clear();
            this.RadGridView_Show.GroupDescriptors.Clear();
            this.RadGridView_Show.SortDescriptors.Clear();
            this.RadGridView_Show.MasterTemplate.AutoGenerateColumns = false;
            this.RadGridView_Show.SummaryRowsTop.Clear();
            this.RadGridView_Show.DataSource = null;

            if (dt != null) dt.Rows.Clear();
            if (dt.Columns != null) dt.Columns.Clear();
            if (colmnDate != null) colmnDate = null;
        }
        private DataTable GetDataComByShelf(string datebegine, string dateend, string[] colm)
        {
            DataTable _dt_Shelf = dt;
            foreach (DataRow drBranch in drBranchs)
            {
                DataTable dtsale = ShelfReportClass.GetSaleAll(drBranch["BRANCH_ID"].ToString(), datebegine, dateend);
                List<EMPLDATE> ListEmp = ShelfReportClass.GetEmployee(drBranch["BRANCH_ID"].ToString(), datebegine, dateend);
                for (int i = 0; i < dtsale.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["BRANCH_ID"] = dtsale.Rows[i]["BRANCH"].ToString();
                    dr["BRANCH_NAME"] = drBranch["BRANCH_NAME"].ToString();
                    dr["ZONE_ID"] = dtsale.Rows[i]["ZONE_ID"].ToString();
                    dr["ZONE_NAME"] = dtsale.Rows[i]["ZONE_NAME"].ToString();
                    dr["SHELF_ID"] = dtsale.Rows[i]["SHELF_ID"].ToString();
                    dr["SHELF_NAME"] = dtsale.Rows[i]["SHELF_NAME"].ToString();
                    dr["RATE"] = dtsale.Rows[i]["RATE"].ToString();
                    double SumCom = 0.0, Sum = 0.0;
                    colm.ForEach(col =>
                    {
                        string datecolumn = col.Substring(4, 2);
                        double totol = 0.0;
                        var cnt = ListEmp.Where(c => c.ZONE_ID.Equals(dr["ZONE_ID"])).Where(c => c.SHELF_ID.Equals(dr["SHELF_ID"])).Where(c => c.DAY.Equals(datecolumn));
                        if (cnt.ToList().Count > 0)
                        {
                            if (!string.IsNullOrEmpty(dtsale.Rows[i][datecolumn].ToString()))
                            {
                                Sum += Convert.ToDouble(dtsale.Rows[i][datecolumn]);
                                totol = Convert.ToDouble(dr["RATE"]) * Convert.ToDouble(dtsale.Rows[i][datecolumn]);
                            }
                        }
                        SumCom += totol;
                        dr[col] = Math.Round(totol, 4).ToString("#,##0.0000");
                    });
                    dr["SALE"] = Math.Round(Sum, 4).ToString("#,##0.00");
                    dr["Commision"] = Math.Round(SumCom, 4).ToString("#,##0.00");
                    _dt_Shelf.Rows.Add(dr);
                }
            }
            return _dt_Shelf;
        }
        private DataRow GetDataCommisByBranch(string[] colm, string Branch, string BranchName, DataTable dtsale, List<EMPLDATE> _ListEmp)
        {
            DataRow dr = dt.NewRow();
            if (dtsale.Rows.Count > 0)
            {
                double SumCom = 0.0;
                dr["BRANCH_ID"] = Branch;
                dr["BRANCH_NAME"] = BranchName;
                double Sum = 0.0;
                colm.ForEach(col =>
                {
                    double total = 0;
                    foreach (DataRow rows in dtsale.Rows)
                    {
                        var cnt = _ListEmp.Where(e => e.ZONE_ID.Equals(rows["ZONE_ID"])).Where(e => e.SHELF_ID.Equals(rows["SHELF_ID"])).Where(e => e.DAY.Equals(col.Substring(4, 2)));
                        if (cnt.ToList().Count > 0)
                        {
                            if (!rows.IsNull(col.Substring(4, 2)))
                            {
                                total += Convert.ToDouble(rows[col.Substring(4, 2)]) * Convert.ToDouble(rows["RATE"]);
                                Sum += Convert.ToDouble(rows[col.Substring(4, 2)]);
                            }
                        }
                    }
                    dr[col] = Math.Round(total, 4).ToString("#,##0.00");
                    SumCom += Convert.ToDouble(dr[col]);
                });
                dr["SALE"] = Math.Round(Sum, 4).ToString("#,##0.00");
                dr["Commision"] = Math.Round(SumCom, 4).ToString("#,##0.00");
            }
            return dr;
        }
        private DataTable GetDataCommisByEmployee(string datebegine, string dateend, string[] colm)
        {
            DataTable _dt_Emp = dt;
            if (ListExt.Count > 0) ListExt.Clear();
            foreach (DataRow drBranch in drBranchs)
            {

                List<SALE> ListSale = ShelfReportClass.GetSaleList(drBranch["BRANCH_ID"].ToString(), datebegine, dateend);
                List<EMPLDATE> ListEmp = ShelfReportClass.GetEmployee(drBranch["BRANCH_ID"].ToString(), datebegine, dateend);

                ListExt.Add(ListEmp);
                ListExt.Add(ListSale);
                if (ListEmp.Count > 0)
                {
                    var employees = (from e in ListEmp group e by e.EMPL_ID).OrderBy(k => k.Key);
                    foreach (var emps in employees)
                    {
                        DataRow dr = dt.NewRow();
                        dr["EMP_ID"] = emps.Key;
                        double ComTotal = 0.0;
                        foreach (var c in colm)
                        {
                            string datecol = c.Substring(4, 2);
                            double sum = 0.0;
                            var emp = from s in emps
                                      where s.DAY == datecol
                                      orderby s.ZONE_ID
                                      select s;
                            foreach (var e in emp) // Each group has inner collection
                            {
                                dr["EMP_NAME"] = e.EMPL_NAME;
                                dr["BRANCH_ID"] = e.BRANCH_ID;
                                dr["BRANCH_NAME"] = drBranch["BRANCH_NAME"].ToString();

                                var s = ListSale.Where(cnt => cnt.ZONE_ID.Equals(e.ZONE_ID)).Where(cnt => cnt.SHELF_ID.Equals(e.SHELF_ID)).Where(cnt => cnt.DAY.Equals(datecol)).FirstOrDefault();
                                double cal = 0.0;
                                if (s != null)
                                {
                                    double sale = s.AMOUNT;
                                    double rate = ListEmp.Where(cnt => cnt.ZONE_ID.Equals(e.ZONE_ID)).Where(cnt => cnt.SHELF_ID.Equals(e.SHELF_ID)).Where(cnt => cnt.DAY.Equals(datecol)).FirstOrDefault().RATE;
                                    int count = ListEmp.Where(cnt => cnt.ZONE_ID.Equals(e.ZONE_ID)).Where(cnt => cnt.SHELF_ID.Equals(e.SHELF_ID)).Where(cnt => cnt.DAY.Equals(datecol)).ToList().Count;
                                    cal = sale * rate / count;
                                }

                                sum += cal;
                            }
                            dr[c] = Math.Round(sum).ToString("#,##0.00");
                            ComTotal += Math.Round(sum);
                        }
                        dr["Commision"] = Math.Round(ComTotal).ToString("#,##0.00");
                        _dt_Emp.Rows.Add(dr);
                    }
                }
            }
            return _dt_Emp;
        }
        private string[] GetColumnsDate(int count)
        {
            string[] columnName = new string[count];
            for (int i = 0; i < count; i++)
            {
                columnName[i] = "DATE" + radDateTimePicker_DateBegin.Value.AddDays(i).ToString("ddMMyyyy");
            }
            return columnName;

        }
        private void GetGridViewSummary(string[] Col, GridAggregateFunction gridAggregate)
        {
            GridViewSummaryItem[] items = new GridViewSummaryItem[Col.Length];
            int i = 0;
            foreach (var c in Col)
            {
                GridViewSummaryItem summaryItem = new GridViewSummaryItem(c, "{0:0,0}", gridAggregate);
                items[i] = summaryItem;
                i++;
            }
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(items);
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        #endregion

        #region SetFontInRadGridview 
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        #endregion

        #region Event
        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearData();
            BranchSelect();
        }
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (!radCheckBox_Branch.Checked)
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("การดึงข้อมูลทุกสาขา อาจใช้เวลาค้นหามากกว่า 1 นาที คุณต้องการทำต่อหรือไม่.") == DialogResult.No)
                {
                    return;
                }
            }

            this.ClearData();
            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.WaitCursor; });
            //System.Diagnostics.Stopwatch objStopWatchAuto = new System.Diagnostics.Stopwatch();
            //objStopWatchAuto.Start();

            if (radRadioButton_Shelf.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                BindToDataShelf();
            }
            else if (radRadioButton_Emp.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                BindToEmp();
            }
            else if (radRadioButton_Branch.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                BindToBranch();
            }

            //objStopWatchAuto.Stop();
            //radLabel_time.Text = $"Time : {(objStopWatchAuto.ElapsedMilliseconds / 1000).ToString()} ss";

            //if (SystemClass.SystemComProgrammer.Equals("1")) radLabel_time.Visible = true;

            this.Invoke((MethodInvoker)delegate { this.Cursor = Cursors.Default; });
        }
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView(this.tiitle, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (radRadioButton_Emp.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ShelfReportEmpDetail empDetail = new ShelfReportEmpDetail(e.Column.Name, colmnDate
                        , this.RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString()
                        , this.RadGridView_Show.CurrentRow.Cells["EMP_ID"].Value.ToString()
                        , this.RadGridView_Show.CurrentRow.Cells["EMP_NAME"].Value.ToString()
                        , radDateTimePicker_DateBegin.Value.ToString("yyyy-MM-dd")
                        , radDateTimePicker_DateEnd.Value.ToString("yyyy-MM-dd")
                        , ListExt);
                    empDetail.Show(this);
                    empDetail.ShowInTaskbar = true;
                }
            }
        }
        private void RadButtonElement_Pdf_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pTypeUser);
        }
        private void RadDateTimePicker_DateBegin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_DateBegin, radDateTimePicker_DateEnd);
        }
        private void RadDateTimePicker_DateEnd_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_DateBegin, radDateTimePicker_DateEnd);
        }
        private void RadCheckBox_Branch_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            BranchSelect();
        }
        private void RadRadio_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {

        }

        #endregion
    }
}