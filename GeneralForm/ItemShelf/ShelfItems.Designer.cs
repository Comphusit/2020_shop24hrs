﻿namespace PC_Shop24Hrs.GeneralForm.ItemShelf
{
    partial class ShelfItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShelfItems));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button_pdt = new System.Windows.Forms.Button();
            this.radDropDownList_Bch = new Telerik.WinControls.UI.RadDropDownList();
            this.RadGridView_Shelf = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Camall = new Telerik.WinControls.UI.RadLabelElement();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_Add = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radGridView_Item = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.Button_Save = new System.Windows.Forms.Button();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Unit = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Bch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_Add)).BeginInit();
            this.panel_Add.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item.MasterTemplate)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.RadGridView_Shelf, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(385, 659);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.Controls.Add(this.button_pdt, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radDropDownList_Bch, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(379, 42);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // button_pdt
            // 
            this.button_pdt.BackColor = System.Drawing.Color.Transparent;
            this.button_pdt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_pdt.FlatAppearance.BorderSize = 0;
            this.button_pdt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_pdt.ForeColor = System.Drawing.Color.Black;
            this.button_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.button_pdt.Location = new System.Drawing.Point(347, 3);
            this.button_pdt.Name = "button_pdt";
            this.button_pdt.Size = new System.Drawing.Size(29, 36);
            this.button_pdt.TabIndex = 78;
            this.button_pdt.UseVisualStyleBackColor = false;
            this.button_pdt.Click += new System.EventHandler(this.Button_pdt_Click);
            // 
            // radDropDownList_Bch
            // 
            this.radDropDownList_Bch.AutoSize = false;
            this.radDropDownList_Bch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList_Bch.DropDownHeight = 150;
            this.radDropDownList_Bch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Bch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Bch.Location = new System.Drawing.Point(3, 3);
            this.radDropDownList_Bch.Name = "radDropDownList_Bch";
            this.radDropDownList_Bch.Size = new System.Drawing.Size(338, 36);
            this.radDropDownList_Bch.TabIndex = 0;
            this.radDropDownList_Bch.Text = "radDropDownList1";
            this.radDropDownList_Bch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Type_SelectedValueChanged);
            // 
            // RadGridView_Shelf
            // 
            this.RadGridView_Shelf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Shelf.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Shelf.Location = new System.Drawing.Point(3, 51);
            // 
            // 
            // 
            this.RadGridView_Shelf.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Shelf.Name = "RadGridView_Shelf";
            // 
            // 
            // 
            this.RadGridView_Shelf.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Shelf.RootElement.RippleAnimationColor = System.Drawing.Color.Red;
            this.RadGridView_Shelf.Size = new System.Drawing.Size(379, 605);
            this.RadGridView_Shelf.TabIndex = 18;
            this.RadGridView_Shelf.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Bch_ViewCellFormatting);
            this.RadGridView_Shelf.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Bch_CellClick);
            this.RadGridView_Shelf.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Bch_ConditionalFormattingFormShown);
            this.RadGridView_Shelf.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Bch_FilterPopupRequired);
            this.RadGridView_Shelf.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Bch_FilterPopupInitialized);
            // 
            // radLabel_Camall
            // 
            this.radLabel_Camall.AutoSize = false;
            this.radLabel_Camall.Bounds = new System.Drawing.Rectangle(0, 0, 230, 32);
            this.radLabel_Camall.Name = "radLabel_Camall";
            this.radLabel_Camall.Text = "กล้อง :";
            this.radLabel_Camall.TextWrap = true;
            this.radLabel_Camall.UseCompatibleTextRendering = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.panel_Add);
            this.splitContainer1.Size = new System.Drawing.Size(909, 659);
            this.splitContainer1.SplitterDistance = 385;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel_Add
            // 
            this.panel_Add.BackColor = System.Drawing.Color.Transparent;
            this.panel_Add.Controls.Add(this.tableLayoutPanel1);
            this.panel_Add.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Add.Location = new System.Drawing.Point(0, 0);
            this.panel_Add.Name = "panel_Add";
            this.panel_Add.Size = new System.Drawing.Size(519, 659);
            this.panel_Add.TabIndex = 9;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radStatusStrip1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Item, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(519, 659);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 637);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(513, 19);
            this.radLabel_Detail.TabIndex = 55;
            this.radLabel_Detail.Text = "<html>ระบุสาขา | เลือก ชั้นวาง เพื่อดูสินค้าทั้งหมด | กด + &gt;&gt; เพิ่มสินค้า |" +
    " กด / &gt;&gt; ลบสินค้า</html>";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.radButtonElement_Edit,
            this.commandBarSeparator2});
            this.radStatusStrip1.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(513, 42);
            this.radStatusStrip1.TabIndex = 21;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่ม";
            this.radButtonElement_Add.UseCompatibleTextRendering = false;
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.AutoSize = true;
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.UseCompatibleTextRendering = false;
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radGridView_Item
            // 
            this.radGridView_Item.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Item.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Item.Location = new System.Drawing.Point(3, 96);
            // 
            // 
            // 
            this.radGridView_Item.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView_Item.Name = "radGridView_Item";
            // 
            // 
            // 
            this.radGridView_Item.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Item.Size = new System.Drawing.Size(513, 535);
            this.radGridView_Item.TabIndex = 18;
            this.radGridView_Item.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Item_ViewCellFormatting);
            this.radGridView_Item.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Item_ConditionalFormattingFormShown);
            this.radGridView_Item.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Item_FilterPopupRequired);
            this.radGridView_Item.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Item_FilterPopupInitialized);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel6.Controls.Add(this.Button_Save, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.radTextBox_Barcode, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.radLabel_Unit, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.radLabel_Name, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 51);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(513, 39);
            this.tableLayoutPanel6.TabIndex = 20;
            // 
            // Button_Save
            // 
            this.Button_Save.BackColor = System.Drawing.Color.Transparent;
            this.Button_Save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Save.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Save.ForeColor = System.Drawing.Color.Black;
            this.Button_Save.Location = new System.Drawing.Point(455, 3);
            this.Button_Save.Name = "Button_Save";
            this.Button_Save.Size = new System.Drawing.Size(55, 33);
            this.Button_Save.TabIndex = 77;
            this.Button_Save.Text = "บันทึก";
            this.Button_Save.UseVisualStyleBackColor = false;
            this.Button_Save.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.AutoSize = false;
            this.radTextBox_Barcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 10F);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(3, 3);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(131, 33);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            // 
            // radLabel_Unit
            // 
            this.radLabel_Unit.AutoSize = false;
            this.radLabel_Unit.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_Unit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Unit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Unit.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Unit.Location = new System.Drawing.Point(395, 3);
            this.radLabel_Unit.Name = "radLabel_Unit";
            this.radLabel_Unit.Size = new System.Drawing.Size(54, 33);
            this.radLabel_Unit.TabIndex = 2;
            this.radLabel_Unit.Text = "หน่วย";
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Name.Location = new System.Drawing.Point(140, 3);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(249, 33);
            this.radLabel_Name.TabIndex = 1;
            this.radLabel_Name.Text = "ชื่อสินค้า";
            // 
            // ShelfItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 659);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShelfItems";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "สินค้าประจำชั้นวาง";
            this.Load += new System.EventHandler(this.ShelfItems_Load);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Bch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Shelf)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_Add)).EndInit();
            this.panel_Add.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Item)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabelElement radLabel_Camall;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Bch;
        private Telerik.WinControls.UI.RadGridView RadGridView_Shelf;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Telerik.WinControls.UI.RadPanel panel_Add;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadGridView radGridView_Item;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadLabel radLabel_Unit;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private System.Windows.Forms.Button Button_Save;
        private System.Windows.Forms.Button button_pdt;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
    }
}
