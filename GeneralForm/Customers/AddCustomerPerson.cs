﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System.IO;
using System.Drawing;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Customers
{
    public partial class AddCustomerPerson : Telerik.WinControls.UI.RadForm
    {
        string IDCard, CustID;
        DataTable dtCityAll = new DataTable();
        readonly string _TypeOpen, _CustGroup;
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        public AddCustomerPerson(string TypeOpen, string CustGroup)
        {
            InitializeComponent();

            _TypeOpen = TypeOpen;
            _CustGroup = CustGroup;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_State);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Country);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_city);

            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_Scan.ButtonElement.ShowBorder = true;

            RadButton_pdt.ButtonElement.ShowBorder = true; RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

        }

        #region "Function"
        void ClearDefault()
        {
            RadTextBox_ID1.Enabled = true; RadTextBox_ID1.Text = string.Empty; RadTextBox_ID1.SelectAll(); RadTextBox_ID1.Focus();
            RadTextBox_ID2.Enabled = true; RadTextBox_ID2.Text = string.Empty;
            RadTextBox_ID3.Enabled = true; RadTextBox_ID3.Text = string.Empty;
            RadTextBox_ID4.Enabled = true; RadTextBox_ID4.Text = string.Empty;
            RadTextBox_ID5.Enabled = true; RadTextBox_ID5.Text = string.Empty;


            RadTextBox_Cust.Enabled = false; RadTextBox_Cust.Text = string.Empty; RadTextBox_Cust.Visible = false;
            RadTextBox_Ramark.Enabled = false; RadTextBox_Ramark.Text = "";

            RadTextBox_Name.Enabled = false; RadTextBox_Name.Text = string.Empty;
            RadTextBox_Number.Enabled = false; RadTextBox_Number.Text = string.Empty;
            RadTextBox_Address.Enabled = false; RadTextBox_Address.Text = string.Empty;

            RadDropDownList_State.Enabled = false;
            RadDropDownList_Country.Enabled = false;
            RadDropDownList_city.Enabled = false;
            RadDropDownList_Zipcode.Enabled = false;

            pictureBox_ThaiID.Image = null; this.pictureBox_ThaiID.Refresh();

            RadTextBox_Tel.Enabled = false; RadTextBox_Tel.Text = string.Empty;
            radTextBox_NameYa.Enabled = false; radTextBox_NameYa.Text = ""; radTextBox_NameYa.Visible = false; radLabel_NameYa.Visible = false;

            RadButton_Save.Enabled = false;
            RadButton_Cancel.Enabled = true;

            SetDropdownlist_state(Models.CustomerClass.Get_State());
            if (_CustGroup == "CPHA")
            {
                radTextBox_NameYa.Visible = true;
                radLabel_NameYa.Visible = true;
            }
        }

        //Get Add
        private void GetAddress()
        {
            if (RadTextBox_Address.Text != "" ||
              RadDropDownList_State.SelectedItem.Text != "")
            {
                RadTextBox_Address.Text = RadTextBox_Number.Text + "," +
                    RadDropDownList_city.SelectedItem.Text + "," +
                    RadDropDownList_Country.SelectedItem.Text + "," +
                    RadDropDownList_State.SelectedItem.Text + "," +
                    RadDropDownList_Zipcode.SelectedItem.Text;
            }
        }

        #endregion

        private void AddCustomerPerson_Load(object sender, EventArgs e)
        {
            dtCityAll = Models.CustomerClass.Get_CityAll();

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_State);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_city);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Country);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Zipcode);

            SetDropdownlist_state(Models.CustomerClass.Get_State());
            if (_TypeOpen == "SUPC")
            {
                SetDropdownlist_state(Models.CustomerClass.Get_State());
                ClearDefault();
            }
            else
            {
                ClearDefault();
            }

        }
        Boolean CheckCust(string idCard)
        {
            //เช็ครหัสลูกค้า
            DataTable dtCust = Models.CustomerClass.FindCust_ByCustID("", $@" AND IDENTIFICATIONNUMBER = '{idCard}' "); //CustomerClass.GetCUSTTABLE_ByIDENTIFICATIONNUMBER(idCard);
            if (dtCust.Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้ามีรหัสสมาชิกเรียบร้อยแล้ว ไม่สามารถสมัครซ้ำได้อีก" + System.Environment.NewLine +
                                        @"รหัส : " + dtCust.Rows[0]["ACCOUNTNUM"].ToString() + System.Environment.NewLine +
                                        @"ชื่อ-สกุล : " + dtCust.Rows[0]["NAME"].ToString() + System.Environment.NewLine +
                                        @"เบอร์โทร : " + dtCust.Rows[0]["PHONE"].ToString() + System.Environment.NewLine +
                                        @"ต้องการแก้ไขข้อมูลลูกค้า โทร 1602");
                RadTextBox_Cust.Text = dtCust.Rows[0]["ACCOUNTNUM"].ToString();
                RadTextBox_Cust.Visible = true;
                return true;
            }
            else
            {  //เช็ค SPC_EXTERNALLIST
                if (Models.CustomerClass.GetEXTERNALLIST_ByIDENTIFICATIONNUMBER("0", idCard))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำลังโอนข้อมูลลูกค้าเข้าระบบหลัก{Environment.NewLine}กรุณารอสักครู่ ไม่ต้องบันทึกซ้ำ");
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        //scan
        private void RadButton_Scan_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            ClearDefault();//"Scan"
            ArrayList arrayListIDCARD = Models.CustomerClass.Get_IdCardReader();  //ScanIDCARDClass.GetIdCard();
            this.Cursor = Cursors.Default;

            if (arrayListIDCARD.Count > 0)
            {

                RadTextBox_ID1.Enabled = false;
                RadTextBox_ID2.Enabled = false;
                RadTextBox_ID3.Enabled = false;
                RadTextBox_ID4.Enabled = false;
                RadTextBox_ID5.Enabled = false;

                pictureBox_ThaiID.SizeMode = PictureBoxSizeMode.StretchImage;
                FileStream fs;
                fs = new FileStream(@"C:\MyFolder\Data\NIDCard.JPG", FileMode.Open, FileAccess.Read);
                pictureBox_ThaiID.Image = System.Drawing.Image.FromStream(fs);
                fs.Close();

                RadTextBox_ID1.Text = arrayListIDCARD[6].ToString().Substring(0, 1);
                RadTextBox_ID2.Text = arrayListIDCARD[6].ToString().Substring(1, 4);
                RadTextBox_ID3.Text = arrayListIDCARD[6].ToString().Substring(5, 5);
                RadTextBox_ID4.Text = arrayListIDCARD[6].ToString().Substring(10, 2);
                RadTextBox_ID5.Text = arrayListIDCARD[6].ToString().Substring(12, 1);

                IDCard = RadTextBox_ID1.Text + RadTextBox_ID2.Text + RadTextBox_ID3.Text + RadTextBox_ID4.Text + RadTextBox_ID5.Text;

                RadTextBox_Name.Text = arrayListIDCARD[7].ToString() + @" " +
                    arrayListIDCARD[8].ToString() + @" " +
                    arrayListIDCARD[9].ToString() + @" " +
                    arrayListIDCARD[10].ToString();


                RadTextBox_Number.Text = arrayListIDCARD[15].ToString() + @" " +
                    arrayListIDCARD[16].ToString() + @" " +
                    arrayListIDCARD[17].ToString();


                //แก้ไขที่อยู่ให้ค้นหาใน database ax ได้
                String Changwath, Aumpor, Tumbol;
                Changwath = arrayListIDCARD[22].ToString().Replace("จังหวัด", "");
                if (Changwath.Length > 10) Changwath = Changwath.Substring(0, 10);

                RadDropDownList_State.SelectedValue = Changwath;

                Aumpor = arrayListIDCARD[21].ToString().Replace("อำเภอ", "").Replace("เขต", "");

                if ((Aumpor.Length > 10) && (SystemClass.SystemAX2012 == "0")) Aumpor = Aumpor.Substring(0, 10);

                Tumbol = arrayListIDCARD[20].ToString().Replace("ตำบล", "").Replace("แขวง", "");


                if (RadDropDownList_State.SelectedItem != null)
                {
                    SetDropdownlist_Country(Models.CustomerClass.Get_Country(RadDropDownList_State.SelectedValue.ToString()));
                    RadDropDownList_Country.SelectedValue = Aumpor;
                }


                if (RadDropDownList_Country.SelectedItem != null && RadDropDownList_State.SelectedItem != null)
                {
                    SetDropdownlist_City(Models.CustomerClass.Get_City(RadDropDownList_State.SelectedValue.ToString(),
                        RadDropDownList_Country.SelectedValue.ToString()));
                    DataRow[] tumbolRecid = dtCityAll.Select($@" CITYALIAS = '{Tumbol}' AND STATEID = '{Changwath}' AND COUNTY = '{Aumpor}' ");
                    RadDropDownList_city.SelectedValue = tumbolRecid[0]["RECID"];
                }

                if (RadDropDownList_city.SelectedItem != null && RadDropDownList_Country.SelectedItem != null && RadDropDownList_State.SelectedItem != null)
                {
                    SetDropdownlist_Zipcode(Models.CustomerClass.Get_Zipcode(RadDropDownList_State.SelectedValue.ToString(),
                        RadDropDownList_Country.SelectedValue.ToString(),
                        RadDropDownList_city.SelectedValue.ToString()));
                }
                GetAddress();

                if (CheckCust(arrayListIDCARD[6].ToString())) { }
                else
                {
                    RadTextBox_Tel.Enabled = true;
                    RadTextBox_Tel.Text = "";
                    RadTextBox_Tel.Focus();
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถสแกนบัตรประชาชนได้{Environment.NewLine}ตรวจสอบบัตรและเครื่องสแกน โปรแกรม ThaiID");
            }
        }

        #region "DropDownList"

        void SetDropdownlist_state(DataTable DtState)
        {
            RadDropDownList_State.DataSource = DtState;
            RadDropDownList_State.ValueMember = "STATEID";
            RadDropDownList_State.DisplayMember = "STATENAME";
        }
        void SetDropdownlist_Country(DataTable DtCountry)
        {
            RadDropDownList_Country.DataSource = DtCountry;
            RadDropDownList_Country.ValueMember = "COUNTYID";
            RadDropDownList_Country.DisplayMember = "NAME";
        }
        void SetDropdownlist_City(DataTable DtCity)
        {
            RadDropDownList_city.DataSource = DtCity;
            RadDropDownList_city.ValueMember = "RECID";
            RadDropDownList_city.DisplayMember = "CITYDESC";
        }
        void SetDropdownlist_Zipcode(DataTable DtZipcode)
        {
            RadDropDownList_Zipcode.DataSource = DtZipcode;
            RadDropDownList_Zipcode.ValueMember = "ZIPCODE";
            RadDropDownList_Zipcode.DisplayMember = "ZIPCODE";
        }
        //จังหวัด
        private void RadDropDownList_Country_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Country.SelectedItem != null && RadDropDownList_State.SelectedItem != null)
            {
                SetDropdownlist_City(Models.CustomerClass.Get_City(RadDropDownList_State.SelectedValue.ToString(),
                    RadDropDownList_Country.SelectedValue.ToString()));
                if (RadDropDownList_city.SelectedValue != null)
                {
                    GetAddress();
                }
            }
        }
        //เมือง
        private void RadDropDownList_city_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_city.SelectedValue != null
                && RadDropDownList_Country.SelectedValue != null
                && RadDropDownList_State.SelectedValue != null)
            {
                SetDropdownlist_Zipcode(Models.CustomerClass.Get_Zipcode(RadDropDownList_State.SelectedValue.ToString(),
                    RadDropDownList_Country.SelectedValue.ToString(),
                    RadDropDownList_city.SelectedValue.ToString()));
                if (RadDropDownList_Zipcode.SelectedValue != null && RadDropDownList_city.SelectedValue != null)
                {
                    GetAddress();
                }
            }
        }

        #endregion

        #region"KeyDown KeyUP IDCard"
        private void RadTextBox_ID1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                RadTextBox_ID2.SelectAll();
                RadTextBox_ID2.Focus();
            }
        }
        private void RadTextBox_ID2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                RadTextBox_ID3.SelectAll();
                RadTextBox_ID3.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                RadTextBox_ID1.SelectAll();
                RadTextBox_ID1.Focus();
            }
        }
        private void RadTextBox_ID3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                RadTextBox_ID4.SelectAll();
                RadTextBox_ID4.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                RadTextBox_ID2.SelectAll();
                RadTextBox_ID2.Focus();
            }
        }
        private void RadTextBox_ID4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                RadTextBox_ID5.SelectAll();
                RadTextBox_ID5.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                RadTextBox_ID3.SelectAll();
                RadTextBox_ID3.Focus();
            }
        }
        private void RadTextBox_ID5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                IDCard = RadTextBox_ID1.Text + RadTextBox_ID2.Text + RadTextBox_ID3.Text + RadTextBox_ID4.Text + RadTextBox_ID5.Text;
                if (IDCard.Length != 13)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"เลขบัตรประชาชนไม่ถูกต้อง{Environment.NewLine}ตรวจสอบใหม่อีกครั้ง");
                    RadTextBox_ID5.SelectAll();
                    return;
                }

                if (CheckCust(IDCard)) { ClearDefault(); return; }
                else
                {
                    RadTextBox_ID1.Enabled = false;
                    RadTextBox_ID2.Enabled = false;
                    RadTextBox_ID3.Enabled = false;
                    RadTextBox_ID4.Enabled = false;
                    RadTextBox_ID5.Enabled = false;
                    RadTextBox_Name.Enabled = true;
                    RadTextBox_Name.Focus();
                    return;
                }
                //DataTable dtCust = CustomerClass.GetCUSTTABLE_ByIDENTIFICATIONNUMBER(IDCard);
                //if (dtCust.Rows.Count > 0)
                //{
                //    MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้ามีรหัสสมาชิกเรียบร้อยแล้ว ไม่สามารถสมัครซ้ำได้อีก" + System.Environment.NewLine +
                //                            @"รหัส : " + dtCust.Rows[0]["ACCOUNTNUM"].ToString() + System.Environment.NewLine +
                //                            @"ชื่อ-สกุล : " + dtCust.Rows[0]["NAME"].ToString() + System.Environment.NewLine +
                //                            @"เบอร์โทร : " + dtCust.Rows[0]["PHONE"].ToString());
                //    return;
                //}
                //if (CustomerClass.GetEXTERNALLIST_ByIDENTIFICATIONNUMBER(IDCard))
                //{
                //    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำลังโอนข้อมูลลูกค้าเข้าระบบหลัก{Environment.NewLine}กรุณารอสักครู่ ไม่ต้องบันทึกซ้ำ");
                //    return;
                //}

                ////เช็คบันทึกซ้ำ
                //if (CustomerClass.GetCustRegist(IDCard))
                //{
                //    DataTable DtIdCard = CustomerClass.GetCUSTTABLE_ByCustId(IDCard);
                //    if (DtIdCard.Rows.Count > 0)
                //    {
                //        MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้าสมัครสมาชิกแล้ว !!" + System.Environment.NewLine +
                //            @"รหัส : " + DtIdCard.Rows[0]["ACCOUNTNUM"].ToString() + System.Environment.NewLine +
                //            @"ชื่อ-สกุล : " + DtIdCard.Rows[0]["NAME"].ToString() + System.Environment.NewLine +
                //            @"เบอร์โทร : " + DtIdCard.Rows[0]["PHONE"].ToString() + System.Environment.NewLine +
                //            @"ที่อยู่ : " + DtIdCard.Rows[0]["ADDRESS"].ToString());
                //    }
                //    ClearDefault("Complete");
                //    return;
                //}
                //if (CheckCUST_Detail("KeyIDCARD") == "Cencel")
                //{
                //    return;
                //}


            }
        }

        private void RadTextBox_ID1_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_ID1.Text.Length == 1)
            {
                RadTextBox_ID2.SelectAll();
                RadTextBox_ID2.Focus();
            }
        }

        private void RadTextBox_ID2_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_ID2.Text.Length == 4)
            {
                RadTextBox_ID3.SelectAll();
                RadTextBox_ID3.Focus();
            }
        }

        private void RadTextBox_ID3_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_ID3.Text.Length == 5)
            {
                RadTextBox_ID4.SelectAll();
                RadTextBox_ID4.Focus();
            }
        }

        private void RadTextBox_ID4_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_ID4.Text.Length == 2)
            {
                RadTextBox_ID5.SelectAll();
                RadTextBox_ID5.Focus();
            }
        }

        private void RadTextBox_Tel_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        #endregion

        private void RadTextBox_Number_KeyUp(object sender, KeyEventArgs e)
        {
            GetAddress();
        }

        private void RadDropDownList_State_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_State.SelectedValue != null)
            {
                SetDropdownlist_Country(Models.CustomerClass.Get_Country(RadDropDownList_State.SelectedValue.ToString()));
                if (RadDropDownList_Country.SelectedValue != null)
                {
                    GetAddress();
                }
            }
        }
        //SaveData
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (IDCard.Length != 13)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ รหัสบัตรประชาชน ให้ถูกต้อง");
                return;
            }

            if (RadTextBox_Tel.Text.Length != 10)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ เบอร์โทร ให้ถูกต้อง");
                RadTextBox_Tel.Focus();
                RadTextBox_Tel.SelectAll();
                return;
            }
            if (RadTextBox_Name.Text.Length < 5)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อ -นามสกุล ให้ถูกต้อง");
                RadTextBox_Name.Focus();
                RadTextBox_Name.SelectAll();
                return;
            }
            if (RadTextBox_Number.Text.Length < 1)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ที่อยู่ ให้ถูกต้อง");
                RadTextBox_Number.Focus();
                RadTextBox_Number.SelectAll();
                return;
            }
            if (RadDropDownList_Zipcode.SelectedValue.ToString().Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ รหัสไปรษณีย์ ให้ถูกต้อง");
                RadDropDownList_Zipcode.Focus();
                RadDropDownList_Zipcode.SelectAll();
                return;
            }
            if (_CustGroup == "CPHA")
            {
                if (radTextBox_NameYa.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อร้านยา ให้เรียบร้อย");
                    radTextBox_NameYa.Focus();
                    radTextBox_NameYa.SelectAll();
                    return;
                }
            }
            //ลูกค้าที่สมัครสาขา ขึ้นต้นด้วย 7 สมัครสาขาใหญ่ขึ้นต้นด้วย5
            string TextNumber = "7"; if (_TypeOpen == "SUPC") { TextNumber = "5"; }

            CustID = Class.ConfigClass.GetMaxINVOICEID("C", "", "C" + TextNumber, "6");
            string RecId;
            RecId = CustID.Substring(1, CustID.Length - 1);

            string fNameAX = "Name";
            string vNameAX = RadTextBox_Name.Text;
            string priceAX = "", priceAXvalue = "";
            if (_CustGroup == "CPHA")
            {
                fNameAX = "NameAlias|Name";
                vNameAX = RadTextBox_Name.Text + @"|" + radTextBox_NameYa.Text;
                priceAX = "|SPC_PRICEGROUPRT";
                priceAXvalue = "|RT02";
            }

            string sqlAxColumns = $@"CUSTGROUP|ACCOUNTNUM|PartyType|IdentificationNumber|{fNameAX}|PaymTermId|PHONE|PhoneLocal|CellularPhone|TeleFax|Email|Dimension|ZIPCODE|STATE|COUNTY|CITY|STREET{priceAX}";
            string sqlAxValues = $@"{_CustGroup}|{CustID}| 1 |{IDCard}|{vNameAX}|COD|{RadTextBox_Tel.Text}||||||{RadDropDownList_Zipcode.SelectedValue}|{RadDropDownList_State.SelectedValue}|{RadDropDownList_Country.SelectedValue}|{RadDropDownList_city.SelectedItem.Text}|{RadTextBox_Number.Text}{priceAXvalue}";

            ArrayList sqlAX = new ArrayList
            {
                AX_SendData.Save_EXTERNALLIST_CUSTTABLE(CustID,sqlAxColumns,sqlAxValues,RecId)
            };

            ArrayList sql24 = new ArrayList
            {
                Models.CustomerClass.Save_CUSTTABLE(CustID,RadTextBox_Name.Text,IDCard,RadTextBox_Tel.Text,RadTextBox_Address.Text,RecId,radTextBox_NameYa.Text)
            };

            string resault = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "")
            {
                PrintDocument();
                ClearDefault();//"Complete"
                RadTextBox_Cust.Text = CustID;
                return;
            }
        }

        //print
        void PrintDocument()
        {
            DialogResult dr = printDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                printDocument1.Print();

            }
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int Y = 0;
            if (_CustGroup == "CPHA")
                e.Graphics.DrawString("สมาชิกลูกค้าร้านยา[บัตรเหลือง]", SystemClass.SetFont12, Brushes.Black, 0, Y);
            else
                e.Graphics.DrawString("สมาชิกลูกค้า", SystemClass.SetFont12, Brushes.Black, 50, Y);

            Y += 25;
            e.Graphics.DrawString("รหัส : " + CustID, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("-------------------------", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);

            Y += 25;
            e.Graphics.DrawString("ชื่อสมาชิก : ", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadTextBox_Name.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("โทร : " + RadTextBox_Tel.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("บ้านเลขที่ : " + RadTextBox_Number.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_city.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_Country.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_State.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("รหัสไปรษณีย์ : " + RadDropDownList_Zipcode.SelectedValue.ToString(), SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("-------------------------", SystemClass.SetFont12, Brushes.Black, 60, Y);

            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemBranchID + ":" + SystemClass.SystemBranchName, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(".สามารถใช้งานรหัสสมาชิกได้. : ", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("...หลังจากสมัคร 5 นาที...", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);

        }

        //dpcument
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _CustGroup);
        }

        private void RadTextBox_Tel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_Tel.Text.Length != 10)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ เบอร์โทร ให้ถูกต้อง");
                    RadTextBox_Tel.Focus();
                    RadTextBox_Tel.SelectAll();
                    return;
                }
                else
                {
                    RadTextBox_Tel.Enabled = false;
                    if (_CustGroup == "CPHA")
                    {
                        radTextBox_NameYa.Enabled = true;
                        radTextBox_NameYa.Focus();
                    }
                    else
                    {
                        RadButton_Save.Enabled = true;
                        RadButton_Save.Focus();
                    }
                }
            }
        }

        private void RadTextBox_NameYa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_NameYa.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อร้านยา ให้เรียบร้อย");
                    radTextBox_NameYa.Focus();
                    radTextBox_NameYa.SelectAll();
                    return;
                }
                else
                {
                    RadButton_Save.Enabled = true;
                }
            }
        }

        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_Name.Text.Length < 5)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อ -นามสกุล ให้ถูกต้อง");
                    RadTextBox_Name.Focus();
                    RadTextBox_Name.SelectAll();
                    return;
                }
                else
                {
                    RadTextBox_Name.Enabled = false;
                    RadTextBox_Number.Enabled = true;
                    RadTextBox_Number.Focus();
                }
            }

        }

        private void RadTextBox_Number_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_Number.Text.Length < 1)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ที่อยู่ ให้ถูกต้อง");
                    RadTextBox_Number.Focus();
                    RadTextBox_Number.SelectAll();
                    return;
                }
                else
                {
                    RadTextBox_Number.Enabled = false;
                    RadDropDownList_State.Enabled = true;
                    RadDropDownList_city.Enabled = true;
                    RadDropDownList_Country.Enabled = true;
                    RadDropDownList_Zipcode.Enabled = true;
                    RadTextBox_Tel.Enabled = true;
                    RadTextBox_Tel.Focus();
                }
            }
        }

        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearDefault();//"Default"
        }


    }
}
