﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;

namespace PC_Shop24Hrs.GeneralForm.Customers
{
    public partial class Customer_Report : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeReport;
        readonly string _pPermission;
        //0 - บันทึกการโทรลูกค้า
        //1 - บันทึกการสั่งของลูกค้า
        //2 - จำนวนลูกค้าที่โทร
        //3 - รายงานรายละเอียดการโทรต่อการสั่งของลูกค้า
        //4 - รายงานรายละเอียดการแลกแต้มของลูกค้ามินิมาร์ท
        //5 - รายงานจำนวนแต้มที่แลกไปทั้งหมดของมินิมาร์ท
        //6 - รายงานจำนวนรับออเดอร์ลูกค้าของมินิมาร์ท
        //7 - รายงานรายละเอียกลูกค้าแต่ละคนที่สมัครแต่ละสาขา
        //8 - รายงานสรุปจำนวนลูกค้าสมัครสมาชิกแต่ละสาขา
        //Load
        public Customer_Report(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void Customer_CenterReport_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;

            switch (_pTypeReport)
            {
                case "0":  //0 - บันทึกการโทรลูกค้า
                    radLabel_Detail.Text = "สีฟ้า >> จำนวนครั้งที่โทรหาลูกค้า"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่โทรหาลูกค้า";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "1": //1 - บันทึกการสั่งของลูกค้า
                    radLabel_Detail.Text = "สีฟ้า >> จำนวนครั้งที่ลูกค้าสั่งสินค้า"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่ลูกค้าสั่งสินค้า";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "2"://2 - จำนวนลูกค้าที่โทร
                    radLabel_Detail.Text = "สีฟ้า >> จำนวนครั้งที่โทรหาลูกค้า"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่โทรหาลูกค้า";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "3"://3 - รายงานรายละเอียดการโทรต่อการสั่งของลูกค้า
                    radLabel_Detail.Text = "สีฟ้า >> จำนวนครั้งที่โทรหาลูกค้า|จำนวนออร์เดอร์"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่โทรหาลูกค้า";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "4":  //4 - รายงานรายละเอียดการแลกแต้มของลูกค้ามินิมาร์ท
                    //radLabel_Detail.Text = "สีฟ้า >> จำนวนครั้งที่โทรหาลูกค้า|จำนวนออร์เดอร์"; 
                    radLabel_Detail.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONNAME", "ชื่อสาขา", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REDEMPID", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "รหัสลูกค้า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("IDENTIFICATIONNUMBER", "รหัสบัตรประชาชน", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("IDENTIFICATIONNAME", "ชื่อเจ้าของบัตร", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("POINTTOTAL", "แต้มทั้งหมด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("POINTCUR", "แต้มที่ใช้", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("POINTRemain", "แต้มหลังแลก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDBY", "ผู้แลก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDBYNAME", "ชื่อผู้แลก", 200)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("ITEMBARCODETOTALPOINT", "แต้มที่ใช้ไป", 100)));

                    RadGridView_ShowHD.Columns["INVENTLOCATIONID"].IsPinned = true;
                    RadGridView_ShowHD.Columns["INVENTLOCATIONNAME"].IsPinned = true;
                                        

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("REDEMPID", System.ComponentModel.ListSortDirection.Descending);
                    RadGridView_ShowHD.GroupDescriptors.Add(descriptor);

                    radLabel_Date.Text = "ระบุวันที่แลกแต้ม";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "5":   //5 - รายงานจำนวนแต้มที่แลกไปทั้งหมดของมินิมาร์ท
                    radLabel_Detail.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMPOINT", "แต้มทั้งหมด", 150)));

                    GridViewSummaryItem summaryItem6 = new GridViewSummaryItem
                    {
                        Name = "SUMPOINT",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem6 = new GridViewSummaryRowItem { summaryItem6 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem6);

                    radLabel_Date.Text = "ระบุวันที่แลกแต้ม";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "6": //6 - รายงานจำนวนรับออเดอร์ลูกค้าของมินิมาร์ท
                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                    }

                    DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, "1");

                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-10);
                    radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
                    radLabel_Detail.Text = "สีฟ้า >> มีรายการรับออเดอร์"; radLabel_Detail.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่รับออเดอร์ลูกค้า";
                    break;
                case "7": //7 - รายงานรายละเอียกลูกค้าแต่ละคนที่สมัครแต่ละสาขา
                    radLabel_Detail.Text = "รายละเอียดลูกค้าที่สมัครสมาชิกที่สาขา";
                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                    }

                    DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, "1");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PartyType", "ประเภท", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "รหัส", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CUSTOMERNAME", "ชื่อ", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEIN", "วันที่", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOIN", "ผู้สมัคร", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAMEIN", "ชื่อผู้สมัคร", 300));
                    break;
                case "8"://8 - รายงานสรุปจำนวนลูกค้าสมัครสมาชิกแต่ละสาขา
                    radLabel_Detail.Text = "ยอดรวมลูกค้าที่สมัครสมาชิกที่สาขา";
                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                    }

                    DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, "1");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("ACCOUNTNUM", "จำนวนลูกค้าใหม่", 150));
                    break;
                default:
                    break;
            }
 
        }

        //Set HD
        void SetDGV_HD()
        {
            switch (_pTypeReport)
            {
                case "0"://บันทึกการโทรลูกค้า
                    LoadDataTelCount();
                    break;
                case "1"://บันทึกการสั่ง
                    LoadDataOrderCount();
                    break;
                case "2":
                    LoadDataEmpCount();
                    break;
                case "3":
                    LoadDataEmpOrderCount();
                    break;
                case "4":
                    LoadDataMNFR();
                    break;
                case "5":
                    LoadDataMNFRSUM();
                    break;
                case "6":
                    LoadDataOrdetCustomerByCash();
                    break;
                case "7"://7 - รายงานรายละเอียกลูกค้าแต่ละคนที่สมัครแต่ละสาขา
                    string DropdownBranch = "";//, RadioType = "0";
                    if (RadCheckBox_1.Checked == true) DropdownBranch = RadDropDownList_1.SelectedValue.ToString();

                    DataTable dtCustomerRegis = Models.CustomerClass.Get_CutomerRegister("0", DropdownBranch,
                        radDateTimePicker_D1.Value.Date.ToString("yyyy-MM-dd"),
                        radDateTimePicker_D2.Value.Date.ToString("yyyy-MM-dd"));

                    RadGridView_ShowHD.DataSource = dtCustomerRegis;
                    break;
                case "8":
                    string Branch = "";
                    if (RadCheckBox_1.Checked == true) Branch = RadDropDownList_1.SelectedValue.ToString();
                    DataTable dtCountCustomerRegis = Models.CustomerClass.Get_CutomerRegister("1", Branch,
                                    radDateTimePicker_D1.Value.Date.ToString("yyyy-MM-dd"),
                                    radDateTimePicker_D2.Value.Date.ToString("yyyy-MM-dd"));
                    RadGridView_ShowHD.DataSource = dtCountCustomerRegis;
                    break;
                default:
                    break;
            }
        }

        //LoadData0 บันทึกการโทรลูกค้า
        void LoadDataTelCount()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstID", "รหัสลูกค้า", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstNAME", "ชื่อรหัสลูกค้า", 300)));
            RadGridView_ShowHD.Columns["CstID"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0)
            {
                intDay = 1;
            }

            //เพิ่มคอลัม
            int i_Column = 0;

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                string HeadColumnCount = "D" + pDate.Replace("-", "V");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, pDate, 120)));

                this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                   new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 ", false)
                   { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                i_Column += 1;
            }

            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการโทรของลูกค้า");
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable dtCst = Models.CustomerClass.Get_TelOrder("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlCstSum);
            DataTable dtTel = Models.CustomerClass.Get_TelOrder("1", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sqlCstDetail);

            ////Add Data    //จำนวนวันที่ดึง
            for (int iCst = 0; iCst < dtCst.Rows.Count; iCst++)
            {
                string cstID = dtCst.Rows[iCst]["CustID"].ToString();
                string cstName = dtCst.Rows[iCst]["CustName"].ToString();
                RadGridView_ShowHD.Rows.Add(cstID, cstName);

                int iCountColume = 2;
                for (int iD = 0; iD < intDay; iD++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");
                    DataRow[] dr = dtTel.Select(" CustID = '" + cstID + @"' AND  DATEIN = '" + pDate + "' ");
                    int cTel = 0;
                    if (dr.Length > 0)
                    {
                        cTel = int.Parse(dr[0]["SumCount"].ToString());
                    }
                    RadGridView_ShowHD.Rows[iCst].Cells[iCountColume].Value = cTel.ToString();
                    iCountColume++;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //LoadData 1  บันทึกการสั่ง
        void LoadDataOrderCount()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstID", "รหัสลูกค้า", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstNAME", "ชื่อรหัสลูกค้า", 300)));
            RadGridView_ShowHD.Columns["CstID"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0)
            {
                intDay = 1;
            }

            //เพิ่มคอลัม
            int i_Column = 0;

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                string HeadColumnCount = "D" + pDate.Replace("-", "V");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, pDate, 120)));

                this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                   new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 ", false)
                   { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                i_Column += 1;
            }

            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการสั่งของลูกค้า");
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable dtCst = Models.CustomerClass.Get_OrderCust("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), ""); //ConnectionClass.SelectSQL_Main(sqlCstSum);
            DataTable dtTel = Models.CustomerClass.Get_OrderCust("1", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), ""); //CountOrderCst(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "0");
            ////Add Data    //จำนวนวันที่ดึง
            for (int iCst = 0; iCst < dtCst.Rows.Count; iCst++)
            {
                string cstID = dtCst.Rows[iCst]["CustID"].ToString();
                string cstName = dtCst.Rows[iCst]["CustName"].ToString();
                RadGridView_ShowHD.Rows.Add(cstID, cstName);

                int iCountColume = 2;
                for (int iD = 0; iD < intDay; iD++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");
                    DataRow[] dr = dtTel.Select(" CustID = '" + cstID + @"' AND  DATEIN = '" + pDate + "' ");
                    int cTel = 0;
                    if (dr.Length > 0)
                    {
                        cTel = int.Parse(dr[0]["SumCount"].ToString());
                    }
                    RadGridView_ShowHD.Rows[iCst].Cells[iCountColume].Value = cTel.ToString();
                    iCountColume++;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //LoadData 2 จำนวนที่โทรหาลูกค้าตามพนักงาน
        void LoadDataEmpCount()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstID", "รหัสพนักงาน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstNAME", "ชื่อพนักงาน", 300)));
            RadGridView_ShowHD.Columns["CstID"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0)
            {
                intDay = 1;
            }

            //เพิ่มคอลัม
            int i_Column = 0;

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                string HeadColumnCount = "D" + pDate.Replace("-", "V");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, pDate, 120)));

                this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                   new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 ", false)
                   { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                i_Column += 1;
            }

            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการสั่งของลูกค้า");
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable dtCst = Models.CustomerClass.Get_TelOrder("2", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));//ConnectionClass.SelectSQL_Main(sqlCstSum);

            DataTable dtTel = Models.CustomerClass.Get_TelOrder("3", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); // ConnectionClass.SelectSQL_Main(sqlCstDetail);
            ////Add Data    //จำนวนวันที่ดึง
            for (int iEmp = 0; iEmp < dtCst.Rows.Count; iEmp++)
            {
                string empID = dtCst.Rows[iEmp]["WHOIN"].ToString();
                string empName = dtCst.Rows[iEmp]["SPC_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(empID, empName);

                int iCountColume = 2;
                for (int iD = 0; iD < intDay; iD++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");
                    DataRow[] dr = dtTel.Select(" WHOIN = '" + empID + @"' AND  DATEIN = '" + pDate + "' ");
                    int cTel = 0;
                    if (dr.Length > 0)
                    {
                        cTel = int.Parse(dr[0]["SumCount"].ToString());
                    }
                    RadGridView_ShowHD.Rows[iEmp].Cells[iCountColume].Value = cTel.ToString();
                    iCountColume++;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //LoadData 3 จำนวนที่โทรหาลูกค้าตามพนักงาน ต่อ การสั่ง
        void LoadDataEmpOrderCount()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstID", "รหัสพนักงาน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CstNAME", "ชื่อพนักงาน", 300)));
            RadGridView_ShowHD.Columns["CstID"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0)
            {
                intDay = 1;
            }

            //เพิ่มคอลัม
            int i_Column = 0;

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                string HeadColumnCount = "D" + pDate.Replace("-", "V");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter(HeadColumnCount, pDate, 130)));

                this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                   new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " <> '0 | 0' ", false)
                   { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                i_Column += 1;
            }

            //Check กลุ่ม
            if (i_Column == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการสั่งของลูกค้า");
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable dtCst = Models.CustomerClass.Get_TelOrder("2", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlCstSum);

            DataTable dtTel = Models.CustomerClass.Get_TelOrder("3", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlCstDetail);
            DataTable dtTelOrder = Models.CustomerClass.Get_OrderCust("2", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "");  //CountOrderCst(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "1");

            ////Add Data    //จำนวนวันที่ดึง
            for (int iEmp = 0; iEmp < dtCst.Rows.Count; iEmp++)
            {
                string empID = dtCst.Rows[iEmp]["WHOIN"].ToString();
                string empName = dtCst.Rows[iEmp]["SPC_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(empID, empName);

                int iCountColume = 2;
                for (int iD = 0; iD < intDay; iD++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");
                    DataRow[] dr = dtTel.Select(" WHOIN = '" + empID + @"' AND  DATEIN = '" + pDate + "' ");
                    DataRow[] drOrder = dtTelOrder.Select(" UserCode = '" + empID + @"' AND   DATEIN = '" + pDate + "' ");
                    int cTel = 0;
                    if (dr.Length > 0)
                    {
                        cTel = int.Parse(dr[0]["SumCount"].ToString());
                    }
                    int cOrder = 0;
                    if (drOrder.Length > 0)
                    {
                        cOrder = int.Parse(drOrder[0]["SumCount"].ToString());
                    }

                    RadGridView_ShowHD.Rows[iEmp].Cells[iCountColume].Value = cTel.ToString() + " | " + cOrder.ToString();
                    iCountColume++;
                }
            }

            this.Cursor = Cursors.Default;
        }

        //แลกแต้ม
        void LoadDataMNFR()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dtCst = Models.CustomerClass.Get_CustRedim("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlCstSum);
            RadGridView_ShowHD.DataSource = dtCst;
            dtCst.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //จำนวนแต้มที่แลก
        void LoadDataMNFRSUM()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dtBch;
            //string pBch = "";
            if (SystemClass.SystemBranchID != "MN000")
            {
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
            }
            else
            {
                dtBch = BranchClass.GetBranchAll("'1'", "'1'");
            }

            DataTable dt = Models.CustomerClass.Get_CustRedim("1", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_MainAX(sqlCstSum);

            for (int i = 0; i < dtBch.Rows.Count; i++)
            {
                DataRow[] dr = dt.Select(" INVENTLOCATIONID = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' ");
                double iSum = 0;
                if (dr.Length > 0)
                {
                    iSum = double.Parse(dr[0]["SUMPOINT"].ToString());
                }
                RadGridView_ShowHD.Rows.Add(dtBch.Rows[i]["BRANCH_ID"].ToString(), dtBch.Rows[i]["BRANCH_NAME"].ToString(), iSum.ToString("#,#0.00"));
            }

            this.Cursor = Cursors.Default;
        }
        //จำนวนการรับออเดอร์ลูกค้า
        void LoadDataOrdetCustomerByCash()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }

            if (RadGridView_ShowHD.RowCount > 0) RadGridView_ShowHD.Rows.Clear();
            if (RadGridView_ShowHD.ColumnCount > 0) RadGridView_ShowHD.Columns.Clear();

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสแคชเชียร์", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อแคชเชียร์", 250)));

            RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            RadGridView_ShowHD.Columns["EMPLNAME"].IsPinned = true;
            double intDay = Math.Ceiling((radDateTimePicker_D2.Value - radDateTimePicker_D1.Value).TotalDays);
            if (intDay == 0) intDay = 1;
            for (int iDay = 0; iDay < intDay + 1; iDay++)
            {
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                string HeadColumnCount = "D" + pDate.Replace("-", "V");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(HeadColumnCount, pDate, 120)));

                this.RadGridView_ShowHD.Columns[HeadColumnCount].ConditionalFormattingObjectList.Add(
                   new ExpressionFormattingObject("MyCondition2", HeadColumnCount + " > 0 ", false)
                   { CellBackColor = ConfigClass.SetColor_SkyPastel() });

            }
            //ดึงข้อมูลใส่ grid MaaMiaw
            string BranchSql = "";
            DataTable dtEmp;
            if (RadCheckBox_1.Checked == true)
            {
                BranchSql = RadDropDownList_1.SelectedValue.ToString();
                dtEmp = Models.EmplClass.GetEmployeeAll_ByDptID("0", "0", $@" = '{BranchSql}' ", " LIKE '%แคชเชียร์%' ", "");
            }
            else
            {
                dtEmp = Models.EmplClass.GetEmployeeAll_ByDptID("0", "0", "", " LIKE '%แคชเชียร์%' ", "");
            }

            DataTable DtOrderCust = Models.CustomerClass.Get_OrderCust("3", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), BranchSql);

            for (int i = 0; i < dtEmp.Rows.Count; i++)
            {
                string bchID = dtEmp.Rows[i]["NUM"].ToString();
                string bchName = dtEmp.Rows[i]["DESCRIPTION"].ToString();
                string empID = dtEmp.Rows[i]["EMPLID"].ToString();
                string empName = dtEmp.Rows[i]["SPC_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(bchID, bchName, empID, empName);

                int iCountColume = 4;
                for (int iD = 0; iD <= intDay; iD++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iD).ToString("yyyy-MM-dd");
                    DataRow[] dr = DtOrderCust.Select(" dayDocdate = '" + pDate + @"' AND  Cash = '" + empID + "' ");

                    int cR = 0;
                    if (dr.Length > 0)
                    {
                        cR = int.Parse(dr[0]["Items"].ToString());
                    }

                    RadGridView_ShowHD.Rows[i].Cells[iCountColume].Value = cR.ToString();
                    iCountColume++;
                }
            }
            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }


            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T;
            if (_pTypeReport == "1") T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "2");
            else T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            return;
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true;
            else RadDropDownList_1.Enabled = false;
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (_pTypeReport)
            {
                case "1":
                    break;
                default:
                    break;
            }
        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
    }
}
