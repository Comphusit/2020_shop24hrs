﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;
using System.Data;
using PC_Shop24Hrs.Class;
using System.Collections;
using PC_Shop24Hrs.GeneralForm.MNOR;

namespace PC_Shop24Hrs.GeneralForm.Customers
{
    public partial class Customer_Detail : Telerik.WinControls.UI.RadForm
    {
        string pTypeScan;
        string pImage;
        string pPartyType;
        Data_CUSTOMER dt_Cust;
        readonly DataTable dt_Grid = new DataTable();
        readonly string _pTypeOpen;
        readonly string _pPermission;
        //Load
        public Customer_Detail(string pTypeOpen, string pPermission = "0")
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
            _pPermission = pPermission;
        }
        //Load Main
        private void Customer_Detail_Load(object sender, EventArgs e)
        {
            RadButton_Find.ButtonElement.ShowBorder = true; RadButton_Find.ButtonElement.ToolTipText = "ค้นหาลูกค้า";
            radButton_Scan.ButtonElement.ShowBorder = true; radButton_Scan.ButtonElement.ToolTipText = "สแกนบัตรประชาชนลูกค้า";
            radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_Scan.ButtonElement.Font = SystemClass.SetFontGernaral;
            radButton_Refresh.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_History);
            radListView_Case.ListViewElement.Font = SystemClass.SetFontGernaral;

            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("C", "สถานะ", 120)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH", "สาขา", 110)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เอกสาร", 140)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocDate", "วันที่", 90)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 150)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemName", "ชื่อสินค้า", 250)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Qty", "จำนวน", 80)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 80)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 200)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OpenBY", "เปิดโดย", 200)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("mnpoax", "สถานะการตาม", 200)));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SUMDAY", "เวลารวม")));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "สถานะรับ")));
            RadGridView_History.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("RECIVE_STA", "สถานะรับ")));

            DatagridClass.SetCellBackClolorByExpression("DOCNO", "STA = '0' ", ConfigClass.SetColor_Red(), RadGridView_History);

            dt_Grid.Columns.Add("C");
            dt_Grid.Columns.Add("BRANCH");
            dt_Grid.Columns.Add("DOCNO");
            dt_Grid.Columns.Add("DocDate");
            dt_Grid.Columns.Add("Barcode");
            dt_Grid.Columns.Add("ItemName");
            dt_Grid.Columns.Add("Qty");
            dt_Grid.Columns.Add("UnitID");
            dt_Grid.Columns.Add("REMARK");
            dt_Grid.Columns.Add("OpenBY");
            dt_Grid.Columns.Add("mnpoax");
            dt_Grid.Columns.Add("SUMDAY");
            dt_Grid.Columns.Add("STA");
            dt_Grid.Columns.Add("RECIVE_STA");

            ClearData();

        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        void ClearData()
        {
            pTypeScan = "0"; pImage = "0";
            if (pictureBox_Cst.Image != null)
            {
                pictureBox_Cst.Image.Dispose();
                pictureBox_Cst.Image = null;
            }
            pPartyType = "0";
            radListView_Case.Enabled = false;

            for (int i = 0; i < radListView_Case.Items.Count; i++)
            {
                radListView_Case.Items[i].CheckState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }

            if (dt_Grid.Rows.Count > 0)
            {
                dt_Grid.Rows.Clear(); RadGridView_History.DataSource = dt_Grid; dt_Grid.AcceptChanges();
            }
            radLabel_CstID.Text = "";

            radLabel_PartyTypeName.Text = "";
            radLabel_CstName.Text = "";
            radLabel_CstIDCard.Text = "";
            radLabel_CstNameAlias.Text = "";
            radLabel_CstTel.Text = "";
            radLabel_CstPoint.Text = "";
            radLabel_CstAddress.Text = "";

            button_MNOG.Enabled = false; Button_MNPD.Enabled = false; button_point.Enabled = false; button_tel.Enabled = false;
            radButton_Scan.Enabled = true; RadButton_Find.Enabled = true;

            foreach (ListViewDataItem item in radListView_Case.Items)
            {
                item.CheckState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }
            radTextBox_cst.Enabled = true;
            radTextBox_cst.Text = "";
            radTextBox_cst.Focus();

        }

        void SetFindEnter()
        {
            radLabel_CstID.Text = this.dt_Cust.Customer_ACCOUNTNUM;
            radLabel_CstAddress.Text = this.dt_Cust.Customer_ADDRESS;
            radLabel_CstIDCard.Text = this.dt_Cust.Customer_IDENTIFICATIONNUMBER;
            radLabel_CstName.Text = this.dt_Cust.Customer_NAME;
            radLabel_CstNameAlias.Text = this.dt_Cust.Customer_NAMEALIAS;
            radLabel_CstTel.Text = this.dt_Cust.Customer_PHONE;
            radLabel_CstPoint.Text = string.Format("{0:0.00}", double.Parse(this.dt_Cust.Customer_SPC_REMAINPOINT));
            radLabel_PartyTypeName.Text = this.dt_Cust.Customer_PartyTypeName;
            pPartyType = this.dt_Cust.Customer_PartyType;

            try
            {
                DataTable dtPathImage = Models.CustomerClass.GetImage_ByCustID(this.dt_Cust.Customer_ACCOUNTNUM); //ConnectionClass.SelectSQL_MainAX(sql);
                if (dtPathImage.Rows.Count > 0)
                {
                    pImage = "1";
                    pictureBox_Cst.Image = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dtPathImage.Rows[0]["SPC_IMAGEPATH"].ToString());
                }
            }

            catch (Exception)
            {
                pImage = "0";
            }

            if (_pTypeOpen == "SUPC")
            {
                button_MNOG.Enabled = true; Button_MNPD.Enabled = true; button_point.Enabled = false; button_tel.Enabled = true;
            }
            else
            {
                button_MNOG.Enabled = true; Button_MNPD.Enabled = true; button_tel.Enabled = false;
                if (pPartyType == "1")
                {
                    button_point.Enabled = true;
                    radLabel_PartyTypeName.ForeColor = ConfigClass.SetColor_Blue();
                }
                else
                {
                    radLabel_PartyTypeName.Text += " [ไม่สามารถแลกแต้มที่สาขาได้]";
                    button_point.Enabled = false;
                    radLabel_PartyTypeName.ForeColor = ConfigClass.SetColor_Red();
                }
            }

            if (pPartyType == "2")
            {
                //ลูกค้าองค์กร
                if (CheckstatusCustomer(radLabel_CstID.Text).Equals("1"))
                {
                    radListView_Case.Items[5].CheckState = Telerik.WinControls.Enumerations.ToggleState.On;
                    radLabel_PartyTypeName.Text = radLabel_PartyTypeName.Text.Replace("[ไม่สามารถแลกแต้มที่สาขาได้]", "");
                    if (_pTypeOpen == "SUPC")
                    { button_point.Enabled = false; }
                    else { button_point.Enabled = true; }

                    radLabel_PartyTypeName.ForeColor = ConfigClass.SetColor_Blue();
                }
                else
                {
                    radListView_Case.Items[5].CheckState = Telerik.WinControls.Enumerations.ToggleState.Off;
                    radLabel_PartyTypeName.Text += " [ไม่สามารถแลกแต้มที่สาขาได้]";
                    button_point.Enabled = false;
                    radLabel_PartyTypeName.ForeColor = ConfigClass.SetColor_Red();
                    if (_pPermission.Equals("1"))
                    {
                        radListView_Case.Items[5].Enabled = true;
                    }
                    else
                    {
                        radListView_Case.Items[5].Enabled = false;
                    }
                }

            }
            else
            {
                radListView_Case.Items[5].Enabled = false;
                radListView_Case.Items[5].CheckState = Telerik.WinControls.Enumerations.ToggleState.Off;
            }

            radButton_Scan.Enabled = false; RadButton_Find.Enabled = false;
            radListView_Case.Enabled = true;
            radTextBox_cst.Enabled = false;
        }

        //refresh
        private void RadButton_Refresh_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Find
        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            SetData("");
        }

        //setData For Find
        void SetData(string pCon)
        {
            ShowDataDGV_Customer _showCustomer = new ShowDataDGV_Customer(pCon);
            if (_showCustomer.ShowDialog() == DialogResult.Yes)
            {
                this.dt_Cust = _showCustomer.cust;
                SetFindEnter();
            }
            else
            {
                radTextBox_cst.SelectAll();
                radTextBox_cst.Text = "";
            }
        }
        //Enter
        private void RadTextBox_cst_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    FormShare.ShowData.Data_CUSTOMER dtCst = new FormShare.ShowData.Data_CUSTOMER(radTextBox_cst.Text);
                    this.dt_Cust = dtCst;
                    if ((dt_Cust.Customer_ACCOUNTNUM == "") || (dt_Cust.Customer_ACCOUNTNUM is null))
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ลูกค้าที่ระบุ");
                        ClearData();
                    }
                    else
                    {
                        SetFindEnter();
                    }
                    break;
                case Keys.F1:
                    string condition;
                    if (radTextBox_cst.Text != "")
                    {
                        condition = "AND NAME LIKE '%" + radTextBox_cst.Text.Replace(" ", "%") + @"%' ";
                    }
                    else { condition = ""; }
                    SetData(condition);
                    break;
                default:
                    break;
            }
        }
        //ดึงข้อมูลประวัติต่างๆ
        private void RadListView_Case_ItemCheckedChanged(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.CheckState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                switch (e.Item.Value)
                {
                    case "ประวัติการโทร":
                        FindHistoryTel();
                        break;
                    case "ออเดอร์ของสด":
                        FindHistoryMNOG();
                        break;
                    case "ประวัติการขาย":
                        FindHistorySale();
                        break;
                    case "ออเดอร์ทั่วไป":
                        FindHistoryMNPD();
                        break;
                    case "ประวัติแลกแต้ม":
                        FindHistoryChangePoint();
                        break;
                    case "อนุญาติให้แลกแต้ม":
                        if (_pPermission.Equals("1"))
                        {
                            if (CheckstatusCustomer(radTextBox_cst.Text.Trim()).Equals(""))
                            {
                                UpdateStatusCustomer();
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (e.Item.Value.ToString() == "อนุญาติให้แลกแต้ม")
                {
                    if (radTextBox_cst.Text.Trim() == "")
                    {
                        e.Item.CheckState = Telerik.WinControls.Enumerations.ToggleState.Off;
                        return;
                    }
                    if (CheckstatusCustomer(radTextBox_cst.Text.Trim()).Equals(""))
                    { e.Item.CheckState = Telerik.WinControls.Enumerations.ToggleState.Off; }
                    else
                    {
                        e.Item.CheckState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                }
                else
                {
                    DeleteRows(e.Item.Value.ToString());
                }
            }
        }
        //Delete Rows In Datarow
        void DeleteRows(string pCon)
        {
            this.Cursor = Cursors.WaitCursor;
            for (int i = 0; i < dt_Grid.Rows.Count; i++)
            {
                if (dt_Grid.Rows[i]["C"].ToString() == pCon)
                {
                    dt_Grid.Rows[i].Delete();
                }
            }
            dt_Grid.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Tel 
        private void Button_tel_Click(object sender, EventArgs e)
        {
            FormShare.InputData _inputData = new FormShare.InputData("1", radLabel_CstID.Text + " - " + radLabel_CstName.Text, "บันทึกการโทร", "");
            if (_inputData.ShowDialog(this) == DialogResult.Yes)
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(Models.CustomerClass.Save_CUSTOMER_TELORDER("0", radLabel_CstID.Text, radLabel_CstName.Text, _inputData.pInputData)));
                return;
            }
        }
        //History
        void FindHistoryTel()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dt = Models.CustomerClass.Get_TelOrder("4", radLabel_CstID.Text, "");// ConnectionClass.SelectSQL_Main(sqlTel);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt_Grid.Rows.Add(dt.Rows[i]["C"].ToString(), "CenterShop", "",
                    dt.Rows[i]["DATEIN"].ToString(), "", "", "", "",
                    dt.Rows[i]["Remark"].ToString(), dt.Rows[i]["WHOIN"].ToString() + Environment.NewLine + dt.Rows[i]["WHOINNAME"].ToString(), "", ""
                    , dt.Rows[i]["STA"].ToString());
            }
            RadGridView_History.DataSource = dt_Grid;
            dt_Grid.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Sale
        void FindHistorySale()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dt = PosSaleClass.GetXXX_DetailByCstID("1", radLabel_CstID.Text, "");// ConnectionClass.SelectSQL_POSRetail707(sqlSale);

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                dt_Grid.Rows.Add(dt.Rows[i]["C"].ToString(),
                    dt.Rows[i]["BRANCH"].ToString() + Environment.NewLine + dt.Rows[i]["BRANCH_NAME"].ToString(),
                    dt.Rows[i]["DOCNO"].ToString(),
                    dt.Rows[i]["DATEIN"].ToString(),
                    dt.Rows[i]["Barcode"].ToString(),
                    dt.Rows[i]["ItemName"].ToString(),
                    string.Format("{0:0.00}", double.Parse(dt.Rows[i]["Qty"].ToString())),
                    dt.Rows[i]["UnitID"].ToString(),
                    string.Format("{0:0.00}", double.Parse(dt.Rows[i]["POINT"].ToString())) + " แต้ม",
                    "",
                    "", "", dt.Rows[i]["STA"].ToString());
            }
            this.Cursor = Cursors.Default;
            RadGridView_History.DataSource = dt_Grid;
            dt_Grid.AcceptChanges();
        }
        //MNPD
        void FindHistoryMNPD()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dt = Models.CustomerClass.Get_OrderCust("4", radLabel_CstID.Text, "", ""); //ConnectionClass.SelectSQL_Main(sqlPD);
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                string str = "เอาสินค้าจากสาขาให้";
                if (dt.Rows[i]["TYPEPRODUCT"].ToString() == "2")
                {
                    if (dt.Rows[i]["StaPrcDoc"].ToString() == "0")
                    {
                        str = "บิลยังไม่ยืนยัน";
                    }
                    else
                    {
                        if (dt.Rows[i]["StatusShop"].ToString() == "0")
                        {
                            DataTable dtPD = POClass.FindStaOrderINAX_ByTranID(dt.Rows[i]["DOCNO"].ToString() + "-" + dt.Rows[i]["SeqNo"].ToString());
                            str = "ไม่พบข้อมูล";
                            if (dtPD.Rows.Count > 0)
                            {
                                str = dtPD.Rows[0]["STA"].ToString();
                            }
                        }
                        else
                        {
                            str = "ลูกค้ารับสินค้าแล้ว";
                        }
                    }
                }

                //double DD = double.Parse(dt.Rows[i]["SUMDAY"].ToString()) / 24;
                dt_Grid.Rows.Add(dt.Rows[i]["C"].ToString(),
                    dt.Rows[i]["Branch"].ToString() + Environment.NewLine + dt.Rows[i]["BRANCH_NAME"].ToString(),
                    dt.Rows[i]["DOCNO"].ToString(),
                    dt.Rows[i]["DocDate"].ToString(),
                    dt.Rows[i]["Barcode"].ToString(),
                    dt.Rows[i]["ItemName"].ToString(),
                    string.Format("{0:0.00}", double.Parse(dt.Rows[i]["Qty"].ToString())),
                    dt.Rows[i]["UnitID"].ToString(),
                    dt.Rows[i]["REMARK"].ToString(),
                    dt.Rows[i]["OpenBY"].ToString(),
                    str, "", dt.Rows[i]["STA"].ToString());
            }
            RadGridView_History.DataSource = dt_Grid;
            dt_Grid.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //MNOG
        void FindHistoryMNOG()
        {
            this.Cursor = Cursors.WaitCursor;

            DataTable dt = Models.CustomerClass.Get_OrderCust("5", radLabel_CstID.Text, "", "");// ConnectionClass.SelectSQL_Main(sqlOG);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string str = "";
                if (dt.Rows[i]["MNPOINVENT"].ToString() == "1")
                {
                    str = "ลูกค้ารับสินค้าแล้ว";
                }
                // double DD = double.Parse(dt.Rows[i]["SUMDAY"].ToString()) / 24;
                dt_Grid.Rows.Add(dt.Rows[i]["C"].ToString(),
                    dt.Rows[i]["BRANCH"].ToString(),
                    dt.Rows[i]["DOCNO"].ToString(),
                    dt.Rows[i]["DocDate"].ToString(),
                    dt.Rows[i]["Barcode"].ToString(),
                    dt.Rows[i]["ItemName"].ToString(),
                    string.Format("{0:0.00}", double.Parse(dt.Rows[i]["Qty"].ToString())),
                    dt.Rows[i]["UnitID"].ToString(),
                    dt.Rows[i]["REMARK"].ToString(),
                    dt.Rows[i]["OpenBY"].ToString(),
                    dt.Rows[i]["mnpoax"].ToString(), str, dt.Rows[i]["STA"].ToString(), dt.Rows[i]["RECIVE_STA"].ToString());
            }
            RadGridView_History.DataSource = dt_Grid;
            dt_Grid.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Change Point
        void FindHistoryChangePoint()
        {
            this.Cursor = Cursors.WaitCursor;

            DataTable dt = Models.CustomerClass.Get_CustRedim("2", radLabel_CstID.Text, ""); //ConnectionClass.SelectSQL_SentServer(sqlpoint, IpServerConnectClass.ConMainReportAX);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt_Grid.Rows.Add(dt.Rows[i]["C"].ToString(),
                    dt.Rows[i]["INVENTLOCATIONID"].ToString(),
                    dt.Rows[i]["REDEMPID"].ToString(),
                    dt.Rows[i]["DOCDATE"].ToString(),
                    dt.Rows[i]["ITEMBARCODE"].ToString(),
                    dt.Rows[i]["NAME"].ToString(),
                   string.Format("{0:0.00}", double.Parse(dt.Rows[i]["Qty"].ToString())),
                    dt.Rows[i]["UnitID"].ToString(),
                    dt.Rows[i]["IDENTIFICATIONNUMBER"].ToString(),
                    dt.Rows[i]["IDENTIFICATIONNAME"].ToString(),
                    "", "", dt.Rows[i]["STA"].ToString());
            }
            RadGridView_History.DataSource = dt_Grid;
            dt_Grid.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //recive Item only
        private void RadGridView_History_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (e.Column.Name)
            {
                case "DOCNO":
                    if (RadGridView_History.CurrentRow.Cells["STA"].Value.ToString() != "0") return;
                    if ((RadGridView_History.CurrentRow.Cells["C"].Value.ToString() == "ออเดอร์ของสด") || (RadGridView_History.CurrentRow.Cells["C"].Value.ToString() == "ออเดอร์ทั่วไป"))
                    {
                        string docno = RadGridView_History.CurrentRow.Cells["DOCNO"].Value.ToString();
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกลูกค้ารับสินค้าเรียบร้อยแล้ว" + Environment.NewLine +
                            "เลขที่บิล " + docno + " ?") == DialogResult.No) return;

                        string strUser = "ลูกค้ารับเรียบร้อยแล้ว -> BY " + SystemClass.SystemUserName + Environment.NewLine + " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

                        string barcode = RadGridView_History.CurrentRow.Cells["Barcode"].Value.ToString();
                        string upSql;

                        if (RadGridView_History.CurrentRow.Cells["C"].Value.ToString() == "ออเดอร์ทั่วไป")
                        {
                            upSql = GeneralForm.MNOG_PD.MNOG_MNPD_Class.Update_OrderCust("0", docno, barcode, strUser);
                        }
                        else
                        {

                            if (RadGridView_History.CurrentRow.Cells["RECIVE_STA"].Value.ToString() == "2")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกลูกค้ารับออเดอร์ได้{Environment.NewLine}เนื่องจากจัดซื้อไม่ได้รับออเดอร์{Environment.NewLine}เช็คข้อมูลการรับออเดอร์ของจัดซื้ออีกครั้ง.");
                                return;
                            }

                            upSql = GeneralForm.MNOG_PD.MNOG_MNPD_Class.Update_OrderCust("1", docno, barcode, strUser);
                        }


                        string T = ConnectionClass.ExecuteSQL_Main(upSql);
                        if (T == "")
                        {
                            RadGridView_History.CurrentRow.Cells["STA"].Value = "1";
                            RadGridView_History.CurrentRow.Cells["SUMDAY"].Value = "ลูกค้ารับสินค้าแล้ว";
                            RadGridView_History.CurrentRow.Cells["REMARK"].Value = strUser;
                        }
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        return;
                    }

                    break;
                default:
                    break;
            }



        }
        //open mnog
        private void Button_MNOG_Click(object sender, EventArgs e)
        {
            if (radLabel_CstID.Text == "") { return; }
            string openBy = "Minimart";
            if (_pTypeOpen == "SUPC")
            {
                openBy = "CenterShop";
            }
            MNOG_PD.MNOG_Order _mnogOrder = new MNOG_PD.MNOG_Order(openBy)
            {
                pCstID = radLabel_CstID.Text,
                pCstName = radLabel_CstName.Text,
                pCstTel = radLabel_CstTel.Text
            };
            if (_mnogOrder.ShowDialog(this) == DialogResult.Yes)
            {

            }

        }

        private void Button_MNPD_Click(object sender, EventArgs e)
        {
            //open mnpd
            if (radLabel_CstID.Text == "") { return; }
            string openBy = "Minimart";
            if (_pTypeOpen == "SUPC")
            {
                openBy = "CenterShop";
            }
            MNOG_PD.MNPD_Order _mnpdOrder = new MNOG_PD.MNPD_Order(openBy)
            {
                pCstID = radLabel_CstID.Text,
                pCstName = radLabel_CstName.Text,
                pCstTel = radLabel_CstTel.Text
            };
            if (_mnpdOrder.ShowDialog(this) == DialogResult.Yes)
            {

            }


        }
        //แลกแต้ม
        private void Button_point_Click(object sender, EventArgs e)
        {
            if (radLabel_CstIDCard.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้ายังไม่ได้ระบุรหัสบัตรประชาชนหรือเลขที่พาสปอร์ต" + Environment.NewLine +
                    "ไม่สามารถแลกแต้มได้ ให้ดำเนินการให้เรียบร้อยก่อนแลกแต้มอีกครั้ง" + Environment.NewLine +
                    "ติดต่อแผนกโปรโมชั่นเพื่อแก้ไขข้อมูลลูกค้า Tel.1600,1602");
                return;
            }

            if (double.Parse(radLabel_CstPoint.Text) <= 2)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้ามีแต้มสะสมไม่ถึงขั้นต่ำในการใช้แลกแต้ม [2 แต้ม] " + Environment.NewLine +
                   "ไม่สามารถแลกแต้มได้");
                return;
            }

            if ((pPartyType != "2"))
            {
                if (pImage == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้ายังไม่มีรูปถ่ายบัตรประชาชนหรือพาสปอร์ต" + Environment.NewLine +
                        "ให้ถ่ายรูปแล้ว Upload ผ่านทาง PDA ให้เรียบร้อยก่อนเท่านั้น");
                    return;
                }
            }

            this.Cursor = Cursors.WaitCursor;

            if (Models.CustomerClass.GetEXTERNALLIST_ByIDENTIFICATIONNUMBER("1", radLabel_CstID.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ลูกค้าอยู่ในระบบที่กำลังอัพเดตแต้ม ไม่สามารถแลกแต้มได้ตอนนี้{Environment.NewLine}เช็คข้อมูลลูกค้าใหม่และลองอีกครั้ง.");
                this.Cursor = Cursors.Default;
                return;
            }

            string strCheckMNFR = $@"
                SELECT	*	FROM	SHOP_MNFR_REDEMPTABLE WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,TRANSDATE,23) = CONVERT(VARCHAR,GETDATE(),23)
		                AND ACCOUNTNUM = '{radLabel_CstID.Text}'
                ";
            DataTable dtCheckMNFR = ConnectionClass.SelectSQL_Main(strCheckMNFR);
            if (dtCheckMNFR.Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"แจ้งปรับปรุงระบบ{Environment.NewLine}กำหนดลูกค้าแลกแต้มได้วันละ 1 ครั้งเท่านั้น{Environment.NewLine}เนื่องจากมีปัญหาในส่วนของการปรับปรุงแต้มที่แลกไปก่อนหน้า.");
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable dtDGV = ItembarcodeClass.ItemGroup_GetItemBarcode("D038", "02", "D038_01", $@" AND SPC_REDEMPPOINT > 0  AND SPC_REDEMPPOINT  <= {double.Parse(radLabel_CstPoint.Text)} "); //ConnectionClass.SelectSQL_Main(sql);
            if (dtDGV.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีสินค้ารายการใดที่แต้มของลูกค้าสามารถแลกได้" + Environment.NewLine +
                    "เช็คข้อมูลลูกค้าใหม่และลองอีกครัง.");
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable dtDGVSend = new DataTable();
            dtDGVSend.Columns.Add("ITEMID");
            dtDGVSend.Columns.Add("INVENTDIMID");
            dtDGVSend.Columns.Add("ITEMBARCODE");
            dtDGVSend.Columns.Add("SPC_ITEMNAME");
            dtDGVSend.Columns.Add("STA");
            dtDGVSend.Columns.Add("QTY");
            dtDGVSend.Columns.Add("UNITID");
            dtDGVSend.Columns.Add("SPC_REDEMPPOINT");
            dtDGVSend.Columns.Add("STOCK");
            dtDGVSend.Columns.Add("STA_CHANGE");

            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                dtDGVSend.Rows.Add(
                   dtDGV.Rows[i]["ITEMID"].ToString(), dtDGV.Rows[i]["INVENTDIMID"].ToString(),
                   dtDGV.Rows[i]["ITEMBARCODE"].ToString(), dtDGV.Rows[i]["SPC_ITEMNAME"].ToString(),
                   dtDGV.Rows[i]["STA"].ToString(), dtDGV.Rows[i]["QTYORDER"].ToString(),
                   dtDGV.Rows[i]["ITEMUNIT"].ToString(), double.Parse(dtDGV.Rows[i]["SPC_REDEMPPOINT"].ToString()).ToString("N2"), "0.00", "2");
            }
            this.Cursor = Cursors.Default;
            MNFR.MNFR_MAIN _mnfr = new MNFR.MNFR_MAIN(radLabel_CstIDCard.Text, radLabel_CstName.Text,
                radLabel_CstID.Text, pTypeScan, dtDGVSend);
            radLabel_CstIDCard.Visible = false;
            if (_mnfr.ShowDialog(this) == DialogResult.Yes)
            {

            }
            radLabel_CstIDCard.Visible = true;

            FormShare.ShowData.Data_CUSTOMER dtCst = new FormShare.ShowData.Data_CUSTOMER(radTextBox_cst.Text);
            this.dt_Cust = dtCst;

            SetFindEnter();


        }
        //สแกนบัตร ปชช  //ถ้าสแกนผ่าน return 1 ให้ด้วย
        private void RadButton_Scan_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            ArrayList arrayListIDCARD = Models.CustomerClass.Get_IdCardReader(); //ScanIDCARDClass.GetIdCard();

            if (arrayListIDCARD.Count > 0)
            {
                FormShare.ShowData.Data_CUSTOMER dtCst = new FormShare.ShowData.Data_CUSTOMER(arrayListIDCARD[6].ToString());
                this.dt_Cust = dtCst;
                if ((dt_Cust.Customer_ACCOUNTNUM == "") || (dt_Cust.Customer_ACCOUNTNUM is null))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("ลูกค้าที่ระบุ");
                    ClearData();
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    pTypeScan = "1";
                    SetFindEnter();
                    radButton_Scan.Enabled = false;
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error(" ไม่สามารถสแกนบัตรประชาชนได้ " + System.Environment.NewLine +
                                                "ตรวจสอบบัตรและเครื่องสแกน โปรแกรม ThaiID ");
                ClearData();
                this.Cursor = Cursors.Default;
            }

        }
        //Doc
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //เช็คลูกค้าองค์กรDATEEND
        string CheckstatusCustomer(string cus_id)
        {
            DataTable dt = Models.CustomerClass.Get_CustRedim("3", cus_id, "");// ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) return "1";
            else return "";
        }
        void UpdateStatusCustomer()
        {
            string ret = ConnectionClass.ExecuteSQL_Main(Models.CustomerClass.Save_CUSTOMER_TELORDER("1", radLabel_CstID.Text, radLabel_CstName.Text, radLabel_CstIDCard.Text));
            if (ret.Equals(""))
            {
                SetFindEnter();
            }
        }
    }
}
