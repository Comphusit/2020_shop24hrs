﻿namespace PC_Shop24Hrs.GeneralForm.Customers
{
    partial class AddCustomerPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCustomerPerson));
            this.RadGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radTextBox_NameYa = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_NameYa = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.RadDropDownList_State = new Telerik.WinControls.UI.RadDropDownList();
            this.RadDropDownList_Zipcode = new Telerik.WinControls.UI.RadDropDownList();
            this.RadDropDownList_city = new Telerik.WinControls.UI.RadDropDownList();
            this.RadDropDownList_Country = new Telerik.WinControls.UI.RadDropDownList();
            this.RadTextBox_Ramark = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Address = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Tel = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Name = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Cust = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Number = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_ID5 = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_ID4 = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_ID3 = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_ID2 = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_ID1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Cust = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_Scan = new Telerik.WinControls.UI.RadButton();
            this.pictureBox_ThaiID = new System.Windows.Forms.PictureBox();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.RadLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox1)).BeginInit();
            this.RadGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_NameYa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NameYa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_State)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Zipcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_city)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Country)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Ramark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Address)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Number)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Scan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ThaiID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGroupBox1
            // 
            this.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.RadGroupBox1.Controls.Add(this.radTextBox_NameYa);
            this.RadGroupBox1.Controls.Add(this.radLabel_NameYa);
            this.RadGroupBox1.Controls.Add(this.RadButton_pdt);
            this.RadGroupBox1.Controls.Add(this.RadDropDownList_State);
            this.RadGroupBox1.Controls.Add(this.RadDropDownList_Zipcode);
            this.RadGroupBox1.Controls.Add(this.RadDropDownList_city);
            this.RadGroupBox1.Controls.Add(this.RadDropDownList_Country);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_Ramark);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_Address);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_Tel);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_Name);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_Cust);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_Number);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_ID5);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_ID4);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_ID3);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_ID2);
            this.RadGroupBox1.Controls.Add(this.RadTextBox_ID1);
            this.RadGroupBox1.Controls.Add(this.radLabel11);
            this.RadGroupBox1.Controls.Add(this.radLabel12);
            this.RadGroupBox1.Controls.Add(this.radLabel9);
            this.RadGroupBox1.Controls.Add(this.radLabel8);
            this.RadGroupBox1.Controls.Add(this.radLabel10);
            this.RadGroupBox1.Controls.Add(this.radLabel7);
            this.RadGroupBox1.Controls.Add(this.radLabel6);
            this.RadGroupBox1.Controls.Add(this.radLabel5);
            this.RadGroupBox1.Controls.Add(this.radLabel_Cust);
            this.RadGroupBox1.Controls.Add(this.radLabel3);
            this.RadGroupBox1.Controls.Add(this.radLabel1);
            this.RadGroupBox1.Controls.Add(this.RadButton_Scan);
            this.RadGroupBox1.Controls.Add(this.pictureBox_ThaiID);
            this.RadGroupBox1.Controls.Add(this.RadButton_Cancel);
            this.RadGroupBox1.Controls.Add(this.RadButton_Save);
            this.RadGroupBox1.Controls.Add(this.RadLabel2);
            this.RadGroupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadGroupBox1.GroupBoxStyle = Telerik.WinControls.UI.RadGroupBoxStyle.Office;
            this.RadGroupBox1.HeaderText = "";
            this.RadGroupBox1.Location = new System.Drawing.Point(2, 4);
            this.RadGroupBox1.Name = "RadGroupBox1";
            this.RadGroupBox1.Size = new System.Drawing.Size(540, 534);
            this.RadGroupBox1.TabIndex = 20;
            this.RadGroupBox1.ThemeName = "VisualStudio2012Light";
            // 
            // radTextBox_NameYa
            // 
            this.radTextBox_NameYa.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_NameYa.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_NameYa.Location = new System.Drawing.Point(21, 478);
            this.radTextBox_NameYa.Name = "radTextBox_NameYa";
            this.radTextBox_NameYa.Size = new System.Drawing.Size(163, 21);
            this.radTextBox_NameYa.TabIndex = 2;
            this.radTextBox_NameYa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_NameYa_KeyDown);
            // 
            // radLabel_NameYa
            // 
            this.radLabel_NameYa.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_NameYa.Location = new System.Drawing.Point(22, 455);
            this.radLabel_NameYa.Name = "radLabel_NameYa";
            this.radLabel_NameYa.Size = new System.Drawing.Size(117, 19);
            this.radLabel_NameYa.TabIndex = 74;
            this.radLabel_NameYa.Text = "ชื่อร้านค้า [Enter]";
            this.radLabel_NameYa.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_NameYa.ThemeName = "VisualStudio2012Light";
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(505, 14);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 73;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // RadDropDownList_State
            // 
            this.RadDropDownList_State.DropDownAnimationEnabled = false;
            this.RadDropDownList_State.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_State.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_State.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_State.Location = new System.Drawing.Point(210, 199);
            this.RadDropDownList_State.Name = "RadDropDownList_State";
            this.RadDropDownList_State.Size = new System.Drawing.Size(141, 21);
            this.RadDropDownList_State.TabIndex = 3;
            this.RadDropDownList_State.Text = "จังหวัด";
            this.RadDropDownList_State.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_State_SelectedValueChanged);
            // 
            // RadDropDownList_Zipcode
            // 
            this.RadDropDownList_Zipcode.DropDownAnimationEnabled = false;
            this.RadDropDownList_Zipcode.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Zipcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Zipcode.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Zipcode.Location = new System.Drawing.Point(211, 308);
            this.RadDropDownList_Zipcode.Name = "RadDropDownList_Zipcode";
            this.RadDropDownList_Zipcode.Size = new System.Drawing.Size(141, 21);
            this.RadDropDownList_Zipcode.TabIndex = 6;
            this.RadDropDownList_Zipcode.Text = "รหัสไปรษณีย์";
            // 
            // RadDropDownList_city
            // 
            this.RadDropDownList_city.DropDownAnimationEnabled = false;
            this.RadDropDownList_city.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_city.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_city.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_city.Location = new System.Drawing.Point(210, 256);
            this.RadDropDownList_city.Name = "RadDropDownList_city";
            this.RadDropDownList_city.Size = new System.Drawing.Size(288, 21);
            this.RadDropDownList_city.TabIndex = 5;
            this.RadDropDownList_city.Text = "ตำบล";
            this.RadDropDownList_city.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_city_SelectedValueChanged);
            // 
            // RadDropDownList_Country
            // 
            this.RadDropDownList_Country.DropDownAnimationEnabled = false;
            this.RadDropDownList_Country.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Country.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Country.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Country.Location = new System.Drawing.Point(357, 198);
            this.RadDropDownList_Country.Name = "RadDropDownList_Country";
            this.RadDropDownList_Country.Size = new System.Drawing.Size(141, 21);
            this.RadDropDownList_Country.TabIndex = 4;
            this.RadDropDownList_Country.Text = "อำเภอ";
            this.RadDropDownList_Country.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Country_SelectedValueChanged);
            // 
            // RadTextBox_Ramark
            // 
            this.RadTextBox_Ramark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_Ramark.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Ramark.Location = new System.Drawing.Point(45, 342);
            this.RadTextBox_Ramark.Name = "RadTextBox_Ramark";
            this.RadTextBox_Ramark.Size = new System.Drawing.Size(88, 21);
            this.RadTextBox_Ramark.TabIndex = 14;
            this.RadTextBox_Ramark.Visible = false;
            // 
            // RadTextBox_Address
            // 
            this.RadTextBox_Address.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_Address.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Address.Location = new System.Drawing.Point(210, 360);
            this.RadTextBox_Address.Multiline = true;
            this.RadTextBox_Address.Name = "RadTextBox_Address";
            // 
            // 
            // 
            this.RadTextBox_Address.RootElement.StretchVertically = true;
            this.RadTextBox_Address.Size = new System.Drawing.Size(280, 45);
            this.RadTextBox_Address.TabIndex = 59;
            // 
            // RadTextBox_Tel
            // 
            this.RadTextBox_Tel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_Tel.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Tel.Location = new System.Drawing.Point(210, 442);
            this.RadTextBox_Tel.MaxLength = 10;
            this.RadTextBox_Tel.Name = "RadTextBox_Tel";
            this.RadTextBox_Tel.Size = new System.Drawing.Size(132, 21);
            this.RadTextBox_Tel.TabIndex = 1;
            this.RadTextBox_Tel.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.RadTextBox_Tel_TextChanging);
            this.RadTextBox_Tel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Tel_KeyDown);
            // 
            // RadTextBox_Name
            // 
            this.RadTextBox_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_Name.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Name.Location = new System.Drawing.Point(210, 96);
            this.RadTextBox_Name.Name = "RadTextBox_Name";
            this.RadTextBox_Name.Size = new System.Drawing.Size(288, 21);
            this.RadTextBox_Name.TabIndex = 0;
            this.RadTextBox_Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Name_KeyDown);
            // 
            // RadTextBox_Cust
            // 
            this.RadTextBox_Cust.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_Cust.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Cust.Location = new System.Drawing.Point(24, 245);
            this.RadTextBox_Cust.Name = "RadTextBox_Cust";
            this.RadTextBox_Cust.Size = new System.Drawing.Size(160, 21);
            this.RadTextBox_Cust.TabIndex = 51;
            this.RadTextBox_Cust.Visible = false;
            // 
            // RadTextBox_Number
            // 
            this.RadTextBox_Number.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_Number.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Number.Location = new System.Drawing.Point(210, 146);
            this.RadTextBox_Number.MaxLength = 50;
            this.RadTextBox_Number.Name = "RadTextBox_Number";
            this.RadTextBox_Number.Size = new System.Drawing.Size(288, 21);
            this.RadTextBox_Number.TabIndex = 2;
            this.RadTextBox_Number.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Number_KeyDown);
            this.RadTextBox_Number.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Number_KeyUp);
            // 
            // RadTextBox_ID5
            // 
            this.RadTextBox_ID5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_ID5.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_ID5.Location = new System.Drawing.Point(436, 48);
            this.RadTextBox_ID5.MaxLength = 1;
            this.RadTextBox_ID5.Name = "RadTextBox_ID5";
            this.RadTextBox_ID5.Size = new System.Drawing.Size(29, 21);
            this.RadTextBox_ID5.TabIndex = 11;
            this.RadTextBox_ID5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_ID5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID5_KeyDown);
            // 
            // RadTextBox_ID4
            // 
            this.RadTextBox_ID4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_ID4.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_ID4.Location = new System.Drawing.Point(387, 47);
            this.RadTextBox_ID4.MaxLength = 2;
            this.RadTextBox_ID4.Name = "RadTextBox_ID4";
            this.RadTextBox_ID4.Size = new System.Drawing.Size(36, 21);
            this.RadTextBox_ID4.TabIndex = 10;
            this.RadTextBox_ID4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_ID4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID4_KeyDown);
            this.RadTextBox_ID4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID4_KeyUp);
            // 
            // RadTextBox_ID3
            // 
            this.RadTextBox_ID3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_ID3.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_ID3.Location = new System.Drawing.Point(311, 47);
            this.RadTextBox_ID3.MaxLength = 5;
            this.RadTextBox_ID3.Name = "RadTextBox_ID3";
            this.RadTextBox_ID3.Size = new System.Drawing.Size(70, 21);
            this.RadTextBox_ID3.TabIndex = 9;
            this.RadTextBox_ID3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_ID3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID3_KeyDown);
            this.RadTextBox_ID3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID3_KeyUp);
            // 
            // RadTextBox_ID2
            // 
            this.RadTextBox_ID2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_ID2.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_ID2.Location = new System.Drawing.Point(245, 47);
            this.RadTextBox_ID2.MaxLength = 4;
            this.RadTextBox_ID2.Name = "RadTextBox_ID2";
            this.RadTextBox_ID2.Size = new System.Drawing.Size(60, 21);
            this.RadTextBox_ID2.TabIndex = 8;
            this.RadTextBox_ID2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_ID2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID2_KeyDown);
            this.RadTextBox_ID2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID2_KeyUp);
            // 
            // RadTextBox_ID1
            // 
            this.RadTextBox_ID1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadTextBox_ID1.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_ID1.Location = new System.Drawing.Point(210, 47);
            this.RadTextBox_ID1.MaxLength = 1;
            this.RadTextBox_ID1.Name = "RadTextBox_ID1";
            this.RadTextBox_ID1.Size = new System.Drawing.Size(29, 21);
            this.RadTextBox_ID1.TabIndex = 7;
            this.RadTextBox_ID1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_ID1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID1_KeyDown);
            this.RadTextBox_ID1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ID1_KeyUp);
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel11.Location = new System.Drawing.Point(24, 320);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(73, 19);
            this.radLabel11.TabIndex = 49;
            this.radLabel11.Text = "หมายเหตุ :";
            this.radLabel11.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel11.ThemeName = "VisualStudio2012Light";
            this.radLabel11.Visible = false;
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel12.Location = new System.Drawing.Point(209, 338);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(43, 19);
            this.radLabel12.TabIndex = 48;
            this.radLabel12.Text = "ที่อยู่ :";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel12.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel9.Location = new System.Drawing.Point(208, 286);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(96, 19);
            this.radLabel9.TabIndex = 30;
            this.radLabel9.Text = "รหัสไปรษณีย์ :";
            this.radLabel9.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel9.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel8.Location = new System.Drawing.Point(354, 176);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(54, 19);
            this.radLabel8.TabIndex = 28;
            this.radLabel8.Text = "อำเภอ :";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel8.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel10.Location = new System.Drawing.Point(208, 233);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(50, 19);
            this.radLabel10.TabIndex = 29;
            this.radLabel10.Text = "ตำบล :";
            this.radLabel10.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel10.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel7.Location = new System.Drawing.Point(208, 177);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(57, 19);
            this.radLabel7.TabIndex = 27;
            this.radLabel7.Text = "จังหวัด :";
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel7.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel6.Location = new System.Drawing.Point(209, 124);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(121, 19);
            this.radLabel6.TabIndex = 47;
            this.radLabel6.Text = "บ้านเลขที่  [Enter]";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel6.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(208, 74);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(135, 19);
            this.radLabel5.TabIndex = 27;
            this.radLabel5.Text = "ชื่อ-นามสกุล [Enter]";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel5.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel_Cust
            // 
            this.radLabel_Cust.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Cust.Location = new System.Drawing.Point(22, 295);
            this.radLabel_Cust.Name = "radLabel_Cust";
            this.radLabel_Cust.Size = new System.Drawing.Size(75, 19);
            this.radLabel_Cust.TabIndex = 27;
            this.radLabel_Cust.Text = "รหัสลูกค้า :";
            this.radLabel_Cust.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_Cust.ThemeName = "VisualStudio2012Light";
            this.radLabel_Cust.Visible = false;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(209, 24);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(116, 19);
            this.radLabel3.TabIndex = 26;
            this.radLabel3.Text = "รหัสบัตรประชาชน";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel3.ThemeName = "VisualStudio2012Light";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(24, 379);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(92, 70);
            this.radLabel1.TabIndex = 46;
            this.radLabel1.Text = "ต้องการแก้ไขข้อมูลลูกค้า โทร 1602 เท่านั้น";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel1.ThemeName = "VisualStudio2012Light";
            this.radLabel1.Visible = false;
            // 
            // RadButton_Scan
            // 
            this.RadButton_Scan.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Scan.Location = new System.Drawing.Point(22, 207);
            this.RadButton_Scan.Name = "RadButton_Scan";
            this.RadButton_Scan.Size = new System.Drawing.Size(162, 32);
            this.RadButton_Scan.TabIndex = 15;
            this.RadButton_Scan.Text = "สแกน";
            this.RadButton_Scan.ThemeName = "Fluent";
            this.RadButton_Scan.Click += new System.EventHandler(this.RadButton_Scan_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Scan.GetChildAt(0))).Text = "สแกน";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Scan.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Scan.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Scan.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pictureBox_ThaiID
            // 
            this.pictureBox_ThaiID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_ThaiID.Location = new System.Drawing.Point(22, 21);
            this.pictureBox_ThaiID.Name = "pictureBox_ThaiID";
            this.pictureBox_ThaiID.Size = new System.Drawing.Size(162, 180);
            this.pictureBox_ThaiID.TabIndex = 44;
            this.pictureBox_ThaiID.TabStop = false;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(348, 485);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(103, 32);
            this.RadButton_Cancel.TabIndex = 13;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(247, 485);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(95, 32);
            this.RadButton_Save.TabIndex = 12;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadLabel2
            // 
            this.RadLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadLabel2.Location = new System.Drawing.Point(210, 419);
            this.RadLabel2.Name = "RadLabel2";
            this.RadLabel2.Size = new System.Drawing.Size(111, 19);
            this.RadLabel2.TabIndex = 25;
            this.RadLabel2.Text = "เบอร์โทร [Enter]";
            this.RadLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.RadLabel2.ThemeName = "VisualStudio2012Light";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // AddCustomerPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 546);
            this.Controls.Add(this.RadGroupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddCustomerPerson";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "AddCustomer";
            this.Load += new System.EventHandler(this.AddCustomerPerson_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGroupBox1)).EndInit();
            this.RadGroupBox1.ResumeLayout(false);
            this.RadGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_NameYa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NameYa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_State)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Zipcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_city)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Country)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Ramark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Address)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Number)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_ID1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Scan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ThaiID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal Telerik.WinControls.UI.RadGroupBox RadGroupBox1;
        protected Telerik.WinControls.UI.RadButton RadButton_Scan;
        private System.Windows.Forms.PictureBox pictureBox_ThaiID;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        internal Telerik.WinControls.UI.RadLabel RadLabel2;
        internal Telerik.WinControls.UI.RadLabel radLabel11;
        internal Telerik.WinControls.UI.RadLabel radLabel12;
        internal Telerik.WinControls.UI.RadLabel radLabel9;
        internal Telerik.WinControls.UI.RadLabel radLabel8;
        internal Telerik.WinControls.UI.RadLabel radLabel10;
        internal Telerik.WinControls.UI.RadLabel radLabel7;
        internal Telerik.WinControls.UI.RadLabel radLabel6;
        internal Telerik.WinControls.UI.RadLabel radLabel5;
        internal Telerik.WinControls.UI.RadLabel radLabel_Cust;
        internal Telerik.WinControls.UI.RadLabel radLabel3;
        internal Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Ramark;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Address;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Tel;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Name;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Cust;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Number;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_ID5;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_ID4;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_ID3;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_ID2;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_ID1;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Zipcode;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_city;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Country;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_State;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadTextBox radTextBox_NameYa;
        internal Telerik.WinControls.UI.RadLabel radLabel_NameYa;
    }
}
