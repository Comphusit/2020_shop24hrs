﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Customers
{
    public partial class ChangePoint_BillSale : Telerik.WinControls.UI.RadForm
    {
        string cstOld;
        DataTable dt = new DataTable();
        readonly string _pPermission; //0 สิดที่กำหนดเฉพาะบิลวันนี้ 1 ทำได้หมดทุกบิล
        public ChangePoint_BillSale(string pPermission)
        {
            InitializeComponent();

            _pPermission = pPermission;
        }

        //Load
        private void ChangePoint_BillSale_Load(object sender, EventArgs e)
        {
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            radButton_Save.ButtonElement.ShowBorder = true; radButton_Cancel.ButtonElement.ShowBorder = true;
            radGroupBox_DB.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;

            ClearTxt();

            radTextBox_BilllID.Focus();
        }
        //ClearData
        void ClearTxt()
        {
            cstOld = "";
            radLabel_bch.Text = "";
            radLabel_Cst.Text = "";
            radLabel_CstName.Text = "";
            radLabel_Name.Text = "";
            radLabel_Grand.Text = "";
            radLabel_point.Text = "";
            radTextBox_InputData.Text = "";
            radLabel_Grand.Text = "";
            radLabel_point.Text = "";
            radTextBox_Remaek.Text = "";
            radTextBox_BilllID.Text = "";
            radLabel_Date.Text = "";
            radButton_Save.Enabled = false;
            radTextBox_InputData.Enabled = false;
            radTextBox_Remaek.Enabled = false;
            radTextBox_BilllID.Enabled = true;
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            radTextBox_BilllID.Focus();
        }
        //ยกเลิก
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radTextBox_Remaek.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ระบุ หมายเหตุสำหรับการแก้ไขรหัสลูกค้าในบิล ทุกครั้งก่อนการแก้ไข.");
                radTextBox_Remaek.Focus();
                return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการแก้ไขรหัสลูกค้าในบิล {radTextBox_BilllID.Text} ?") == DialogResult.No) return;

            ArrayList sql24 = new ArrayList();

            string cstNew = radTextBox_InputData.Text;
            string invoice = radTextBox_BilllID.Text;
            double point = Convert.ToDouble(radLabel_point.Text);

            string recId = (DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")).Replace("-", "").ToString().Replace(":", "").Replace(" ", "");
            string methodName = "CREATE";
            string tblName = "SPC_CustPointTransOther";
            string pkField = "DATAAREAID|CUSTACCOUNT|RECID";
            string pkValues = $@"SPC|{cstNew}|{recId}";
            string fieldName = "DATAAREAID|CUSTACCOUNT|INVOICEID|UPDATEPOINT|EMPLUPDATE";
            string fieldValues = $@"SPC|{cstNew}|{invoice}|{point}|{SystemClass.SystemUserID_M}";

            ArrayList sql708 = new ArrayList
            {
                AX_SendData.Save_EXTERNALLIST(methodName,tblName,pkField,pkValues,fieldName,fieldValues,recId)
            };

            if (cstOld != "")
            {
                pkValues = $@"SPC|{cstOld}|{recId}1";
                fieldValues = $@"SPC|{cstOld}|{invoice}|{point}*-1|{SystemClass.SystemUserID_M}";
                AX_SendData.Save_EXTERNALLIST(methodName, tblName, pkField, pkValues, fieldName, fieldValues, recId);
            }

            sql24.Add(@"
                INSERT INTO [dbo].[SHOP_SPC_CUSTPOINTHISTORY]
                    ([INVOICEID],[ACCOUNTNUM],[ACCOUNTNUMNAME],[ACCOUNTNUMOLD],[ACCOUNTNUMOLDNAME],[TRANSDATE],[INVOICEAMOUNT],[NEWPOINT],
                    [POSGROUP],[POSGROUPNAME],[CASHIERID],[CASHIERIDNAME],[WHOID],[WHONAME],[RECID],[REMARK])
                VALUES ('" + invoice + @"','" + cstNew + @"','" + radLabel_CstName.Text + @"',
                    '" + cstOld + @"','" + dt.Rows[0]["ACCOUNTNAME"].ToString() + @"',
                    '" + radLabel_Date.Text + @"','" + radLabel_Grand.Text + @"','" + point + @"',
                    '" + dt.Rows[0]["POSGROUP"].ToString() + @"','" + dt.Rows[0]["POSGROUPNAME"].ToString() + @"',
                    '" + dt.Rows[0]["CASHIERID"].ToString() + @"','" + dt.Rows[0]["CASHIERNAME"].ToString() + @"',
                    '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + recId + @"','" + radTextBox_Remaek.Text + @"')
            ");

            string result = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sql708);
            if (result == "") ClearTxt();
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
        }
        //Input Account
        private void RadTextBox_InputData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_InputData.Text == "") return;

                DataTable dt = Models.CustomerClass.FindCust_ByCustID("", $@" AND ACCOUNTNUM = '{radTextBox_InputData.Text}'  ");
                if (dt.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลลูกค้าที่ระบุ ลองใหม่อีกครั้ง.");
                    radTextBox_InputData.SelectAll();
                    radTextBox_InputData.Focus();
                    return;
                }

                if (cstOld == dt.Rows[0]["ACCOUNTNUM"].ToString())
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("รหัสลูกค้าในบิลและรหัสลูกค้าใหม่ เหมือนกัน ไม่จำเป็นต้องแก้ไข.");
                    radTextBox_InputData.SelectAll();
                    radTextBox_InputData.Focus();
                    return;
                }

                radTextBox_InputData.Text = dt.Rows[0]["ACCOUNTNUM"].ToString();
                radLabel_CstName.Text = dt.Rows[0]["NAME"].ToString();
                radTextBox_InputData.Enabled = false;
                radTextBox_Remaek.Enabled = true;
                radButton_Save.Enabled = true;
                radTextBox_Remaek.Focus();
            }
        }
        //Document
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pPermission);
        }
        //Enter Input Bill
        private void RadTextBox_BilllID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
             
                if (radTextBox_BilllID.Text == "") return;

                int lenBill = radTextBox_BilllID.Text.Length;
                if (lenBill == 16)
                {
                    CheckBill(radTextBox_BilllID.Text);
                }
                else
                {
                    try
                    {
                        string[] noCheckBill = radTextBox_BilllID.Text.Split('-');
                        if (noCheckBill[0].Length != 4)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"ข้อมูลบิลที่ระบุไม่ถูกต้อง ลองใหม่อีกครั้ง{Environment.NewLine}{radTextBox_BilllID.Text}");
                            radTextBox_BilllID.Focus(); radTextBox_BilllID.SelectAll();
                            return;
                        }

                        try
                        {
                            string bill7 = Convert.ToDouble(noCheckBill[1]).ToString().PadLeft(7, '0');
                            string billM = Convert.ToDouble(DateTime.Now.Month.ToString()).ToString().PadLeft(2, '0');
                            string noBill = $@"{noCheckBill[0].Substring(0, 1)}{DateTime.Now.Year.ToString().Substring(2, 2)}{billM}{noCheckBill[0].Substring(1, 3)}-{bill7}";
                            CheckBill(noBill);
                        }
                        catch (Exception exA)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error($@"ข้อมูลบิลที่ระบุไม่ถูกต้อง ลองใหม่อีกครั้ง{Environment.NewLine}{exA.Message}");
                            radTextBox_BilllID.Focus(); radTextBox_BilllID.SelectAll();
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ข้อมูลบิลที่ระบุไม่ถูกต้อง ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
                        radTextBox_BilllID.Focus(); radTextBox_BilllID.SelectAll();
                        return;
                    }
                }
            }

        }

        //ค้นหาบิล Vat
        private void CheckBill(string noBill)//, string table
        {
           
            if (Models.CustomerClass.Get_CustRedim("4", noBill, "").Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"บิลขายเลขที่ {noBill} มีการเปลี่ยนแปลงลูกค้าแล้ว{Environment.NewLine}ไม่สามารถแก้ไขซ้ำได้อีก.");
                radTextBox_BilllID.Focus();
                radTextBox_BilllID.SelectAll();
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            dt = PosSaleClass.GetXXX_DetailByCstID("2", noBill, "");
            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบเลขที่บิลที่ระบุ ลองใหม่อีกครั้ง{Environment.NewLine}[{noBill}]");
                radTextBox_BilllID.Focus();
                radTextBox_BilllID.SelectAll();
            }
            else
            {
                SetForChange();
            }
            Cursor.Current = Cursors.Default;
        }

        //Set For Change
        void SetForChange()
        {

            radTextBox_BilllID.Text = dt.Rows[0]["INVOICEID"].ToString();
            cstOld = dt.Rows[0]["INVOICEACCOUNT"].ToString();
            radLabel_Date.Text = dt.Rows[0]["INVOICEDATE"].ToString();
            radLabel_Name.Text = dt.Rows[0]["CASHIERID"].ToString() + "-" + dt.Rows[0]["CASHIERNAME"].ToString();
            radLabel_Grand.Text = dt.Rows[0]["INVOICEAMOUNT"].ToString();
            radLabel_Cst.Text = dt.Rows[0]["INVOICEACCOUNT"].ToString() + "-" + dt.Rows[0]["ACCOUNTNAME"].ToString();
            radLabel_point.Text = dt.Rows[0]["POINT"].ToString();
            radLabel_bch.Text = dt.Rows[0]["POSGROUP"].ToString() + " " + dt.Rows[0]["POSGROUPNAME"].ToString();

            radTextBox_BilllID.Enabled = false;
            if (_pPermission == "0")
            {
                if (dt.Rows[0]["STA"].ToString() == "1")
                {
                    radLabel_Date.ForeColor = ConfigClass.SetColor_Red();
                    radTextBox_InputData.Enabled = false;
                    radButton_Cancel.Focus();
                }
                else
                {
                    radTextBox_InputData.Enabled = true; radLabel_Date.ForeColor = ConfigClass.SetColor_Black(); radTextBox_InputData.Focus();
                }
            }
            else
            {
                radTextBox_InputData.Enabled = true; radTextBox_InputData.Focus();
            }

        }
    }
}