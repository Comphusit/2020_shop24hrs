﻿
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System.Drawing;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Customers
{
    public partial class AddCustomerOther : Telerik.WinControls.UI.RadForm
    {
        string IDCard, CustID; 
        readonly string _TypeCustomer, _TypeOpen;
        public AddCustomerOther(string TypeCustomer, string TypeOpen)
        {
            //TypeCustomer -- Company ลูกค้าองค์กรณ์ ,ถ้าไม่ใช่ จะเป็นลูกค้าต่างชาติ
            _TypeCustomer = TypeCustomer;
            _TypeOpen = TypeOpen;
            InitializeComponent();
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_State);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Country);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_city);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Zipcode);
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
        }
        #region "Function"
        void ClearDefault()
        {
            RadTextBox_Organization.Enabled = true; RadTextBox_Organization.Text = string.Empty; RadTextBox_Organization.SelectAll(); RadTextBox_Organization.Focus();

            RadTextBox_Cust.Enabled = false; RadTextBox_Cust.Text = "";
            RadTextBox_Name.Enabled = false; RadTextBox_Name.Text = "";

            RadTextBox_Tel.Enabled = false; RadTextBox_Tel.Text = "";
            RadTextBox_Number.Enabled = false; RadTextBox_Number.Text = "";
            RadTextBox_Address.Enabled = false; RadTextBox_Address.Text = "";
            RadTextBox_Ramark.Enabled = false; RadTextBox_Ramark.Text = "";

            RadDropDownList_State.Enabled = false;
            RadDropDownList_Country.Enabled = false;
            RadDropDownList_city.Enabled = false;

            RadButton_Save.Enabled = false;
            RadButton_Cancel.Enabled = true;
                        
            SetDropdownlist_state(Models.CustomerClass.Get_State());

            RadTextBox_Organization.Focus();


        }
        private void GetAddress()
        {
            if (RadTextBox_Address.Text != "" ||
              RadDropDownList_State.SelectedItem.Text != "")
            {
                RadTextBox_Address.Text = RadTextBox_Number.Text + "," +
                    RadDropDownList_city.SelectedItem.Text + "," +
                    RadDropDownList_Country.SelectedItem.Text + "," +
                    RadDropDownList_State.SelectedItem.Text + "," +
                    RadDropDownList_Zipcode.SelectedItem.Text;
            }

        }

        Boolean CheckCUST_Detail()
        {
            IDCard = RadTextBox_Organization.Text;
            if (IDCard.Length < 6)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("หมายเลขผู้เสียภาษีไม่ถูกต้อง กรุณาตรวจสอบ");
                RadTextBox_Organization.SelectAll();
                return true;
            }
            DataTable dtCust = Models.CustomerClass.FindCust_ByCustID("", $@" AND IDENTIFICATIONNUMBER = '{IDCard}' "); //CustomerClass.GetCUSTTABLE_ByIDENTIFICATIONNUMBER(IDCard);
            if (dtCust.Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ลูกค้ามีรหัสสมาชิกเรียบร้อยแล้ว ไม่สามารถสมัครซ้ำได้อีก" + System.Environment.NewLine +
                                       @"รหัส : " + dtCust.Rows[0]["ACCOUNTNUM"].ToString() + System.Environment.NewLine +
                                       @"ชื่อ-สกุล : " + dtCust.Rows[0]["NAME"].ToString() + System.Environment.NewLine +
                                       @"เบอร์โทร : " + dtCust.Rows[0]["PHONE"].ToString() + System.Environment.NewLine +
                                       @"ต้องการแก้ไขข้อมูลลูกค้า โทร 1602");
                return true;
            }
            else
            {
                if (Models.CustomerClass.GetEXTERNALLIST_ByIDENTIFICATIONNUMBER("0",IDCard))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำลังโอนข้อมูลลูกค้าเข้าระบบหลัก{Environment.NewLine}กรุณารอสักครู่ ไม่ต้องบันทึกซ้ำ");
                    return true;
                }
                else
                {
                    return false;
                }
            }


        }

        #endregion

        private void AddCustomerOther_Load(object sender, EventArgs e)
        {
            RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";
            RadButton_pdt.ButtonElement.ShowBorder = true;

            SetDropdownlist_state(Models.CustomerClass.Get_State());
            if (_TypeCustomer == "Company") radLabel_TypeCustomer.Text = "หมายเลขผู้เสียภาษี [ENTER]";
            else radLabel_TypeCustomer.Text = "หมายเลขพาสปอร์ต [ENTER]";

            ClearDefault();
        }


        #region "DropDownList"

        void SetDropdownlist_state(DataTable DtState)
        {
            RadDropDownList_State.DataSource = DtState;
            RadDropDownList_State.ValueMember = "STATEID";
            RadDropDownList_State.DisplayMember = "STATENAME";
        }
        void SetDropdownlist_Country(DataTable DtCountry)
        {
            RadDropDownList_Country.DataSource = DtCountry;
            RadDropDownList_Country.ValueMember = "COUNTYID";
            RadDropDownList_Country.DisplayMember = "NAME";
        }
        void SetDropdownlist_City(DataTable DtCity)
        {
            RadDropDownList_city.DataSource = DtCity;
            RadDropDownList_city.ValueMember = "RECID";
            RadDropDownList_city.DisplayMember = "CITYDESC";
        }
        void SetDropdownlist_Zipcode(DataTable DtZipcode)
        {
            RadDropDownList_Zipcode.DataSource = DtZipcode;
            RadDropDownList_Zipcode.ValueMember = "ZIPCODE";
            RadDropDownList_Zipcode.DisplayMember = "ZIPCODE";
        }
        private void RadDropDownList_Country_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Country.SelectedItem != null && RadDropDownList_State.SelectedItem != null)
            {
                SetDropdownlist_City(Models.CustomerClass.Get_City(RadDropDownList_State.SelectedValue.ToString(),
                    RadDropDownList_Country.SelectedValue.ToString()));
                if (RadDropDownList_city.SelectedValue != null) GetAddress();
            }
        }
        private void RadDropDownList_city_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_city.SelectedValue != null
                && RadDropDownList_Country.SelectedValue != null
                && RadDropDownList_State.SelectedValue != null)
            {
                SetDropdownlist_Zipcode(Models.CustomerClass.Get_Zipcode(RadDropDownList_State.SelectedValue.ToString(),
                    RadDropDownList_Country.SelectedValue.ToString(),
                    RadDropDownList_city.SelectedValue.ToString()));
                if (RadDropDownList_Zipcode.SelectedValue != null && RadDropDownList_city.SelectedValue != null) GetAddress();
            }
        }

        #endregion

        #region"KeyDown KeyUP IDCard"
        private void RadTextBox_Organization_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                IDCard = RadTextBox_Organization.Text;
                
                if (CheckCUST_Detail()) { RadTextBox_Organization.Text = ""; RadTextBox_Organization.Focus(); return; }

                RadTextBox_Organization.Enabled = false;
                RadTextBox_Name.Enabled = true;
                RadTextBox_Name.Focus();
            }
        }

        private void RadTextBox_Tel_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        private void RadTextBox_Organization_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            //e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        #endregion

        private void RadTextBox_Number_KeyUp(object sender, KeyEventArgs e)
        {
            GetAddress();
        }

        private void RadDropDownList_State_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_State.SelectedValue != null)
            {
                SetDropdownlist_Country(Models.CustomerClass.Get_Country(RadDropDownList_State.SelectedValue.ToString()));
                if (RadDropDownList_Country.SelectedValue != null) GetAddress();
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (IDCard.Length < 5)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ รหัสบัตรประชาชน ให้ถูกต้อง");
                return;
            }

            if (RadTextBox_Tel.Text.Length < 9)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ เบอร์โทร ให้ถูกต้อง");
                RadTextBox_Tel.Focus();
                RadTextBox_Tel.SelectAll();
                return;
            }
            if (RadTextBox_Name.Text.Length < 5)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อ -นามสกุล ให้ถูกต้อง");
                RadTextBox_Name.Focus();
                RadTextBox_Name.SelectAll();
                return;
            }
            if (RadTextBox_Number.Text.Length < 1)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ที่อยู่ ให้ถูกต้อง");
                RadTextBox_Number.Focus();
                RadTextBox_Number.SelectAll();
                return;
            }
            if (RadDropDownList_Zipcode.SelectedValue.ToString().Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ รหัสไปรษณีย์ ให้ถูกต้อง");
                RadDropDownList_Zipcode.Focus();
                RadDropDownList_Zipcode.SelectAll();
                return;
            }
 
            string TextNumber = "7";
            if (_TypeOpen == "SUPC")
            {
                TextNumber = "5";
            }

            CustID = Class.ConfigClass.GetMaxINVOICEID("C", "", "C" + TextNumber, "6");
            string RecId; 
            RecId = CustID.Substring(1, CustID.Length - 1);

            string sqlAxColumns = @"CUSTGROUP|ACCOUNTNUM|PartyType|IdentificationNumber|Name|PaymTermId|PHONE|PhoneLocal|CellularPhone|TeleFax|Email|Dimension|ZIPCODE|STATE|COUNTY|CITY|STREET";
            string sqlAxValues = @"CRET_MN|" + CustID + @"|1|" + IDCard +
                @"|" + RadTextBox_Name.Text +
                @"|COD|" + RadTextBox_Tel.Text + @"|" +
                @"|||||" + RadDropDownList_Zipcode.SelectedValue +
                @"|" + RadDropDownList_State.SelectedValue +
                @"|" + RadDropDownList_Country.SelectedValue +
                @"|" + RadDropDownList_city.SelectedItem.Text +
                @"|" + RadTextBox_Number.Text;

            ArrayList sqlAX = new ArrayList
            {
                  AX_SendData.Save_EXTERNALLIST_CUSTTABLE(CustID,sqlAxColumns,sqlAxValues,RecId)
            };

            ArrayList sql24 = new ArrayList {
                 Models.CustomerClass.Save_CUSTTABLE(CustID,RadTextBox_Name.Text,IDCard,RadTextBox_Tel.Text,RadTextBox_Address.Text,RecId,RadTextBox_Ramark.Text)
            };

            string resault = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "")
            {
                PrintDocument();
                ClearDefault();//"Complete"
                RadTextBox_Cust.Text = CustID;
                return;
            }
        }
        void PrintDocument()
        {
            DialogResult dr = printDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                printDocument1.Print();

            }
        }

        //Doc
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _TypeCustomer);
        }

        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_Name.Text.Length < 5)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อ -นามสกุล ให้ถูกต้อง");
                    RadTextBox_Name.Focus();
                    RadTextBox_Name.SelectAll();
                    return;
                }

                RadTextBox_Name.Enabled = false;
                RadTextBox_Tel.Enabled = true;
                RadTextBox_Tel.Focus();
            }
        }

        private void RadTextBox_Tel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_Tel.Text.Length < 10)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ เบอร์โทร ให้ถูกต้อง");
                    RadTextBox_Tel.Focus();
                    RadTextBox_Tel.SelectAll();
                    return;
                }

                RadTextBox_Tel.Enabled = false;
                RadTextBox_Number.Enabled = true;
                RadTextBox_Number.Focus();
            }
        }

        private void RadTextBox_Number_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_Number.Text.Length < 1)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ที่อยู่ ให้ถูกต้อง");
                    RadTextBox_Number.Focus();
                    RadTextBox_Number.SelectAll();
                    return;
                }
                RadTextBox_Number.Enabled = false;
                RadDropDownList_city.Enabled = true;
                RadDropDownList_Country.Enabled = true;
                RadDropDownList_State.Enabled = true;
                RadDropDownList_Zipcode.Enabled = true;

                RadButton_Save.Enabled = true;
                RadButton_Save.Focus();
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearDefault(); 
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            int Y = 0;
            e.Graphics.DrawString("สมาชิกลูกค้า", SystemClass.SetFont12, Brushes.Black, 50, Y);
            Y += 25;
            e.Graphics.DrawString("รหัส : " + CustID, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("-------------------------", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);

            Y += 25;
            e.Graphics.DrawString("ชื่อสมาชิก : ", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadTextBox_Name.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("โทร : " + RadTextBox_Tel.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("บ้านเลขที่ : " + RadTextBox_Number.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_city.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_Country.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(RadDropDownList_State.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("รหัสไปรษณีย์ : " + RadDropDownList_Zipcode.SelectedItem.Text, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("-------------------------", SystemClass.SetFont12, Brushes.Black, 60, Y);

            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemBranchID + ":" + SystemClass.SystemBranchName, SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString(".สามารถใช้งานรหัสสมาชิกได้. : ", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);
            Y += 25;
            e.Graphics.DrawString("...หลังจากสมัคร 5 นาที...", SystemClass.SetFontGernaral, Brushes.Black, 5, Y);

        }

    }
}
