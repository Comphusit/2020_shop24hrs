﻿namespace PC_Shop24Hrs.GeneralForm.Customers
{
    partial class ChangePoint_BillSale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePoint_BillSale));
            this.radGroupBox_DB = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_bch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Cst = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remaek = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_InputData = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_ForSave = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Grand = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_point = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_BilllID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).BeginInit();
            this.radGroupBox_DB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_bch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_InputData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ForSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Grand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_point)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BilllID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox_DB
            // 
            this.radGroupBox_DB.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox_DB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox_DB.Controls.Add(this.radLabel4);
            this.radGroupBox_DB.Controls.Add(this.radLabel_bch);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Date);
            this.radGroupBox_DB.Controls.Add(this.radLabel_CstName);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Cst);
            this.radGroupBox_DB.Controls.Add(this.radLabel5);
            this.radGroupBox_DB.Controls.Add(this.radLabel1);
            this.radGroupBox_DB.Controls.Add(this.RadButton_pdt);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Name);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_Remaek);
            this.radGroupBox_DB.Controls.Add(this.radLabel3);
            this.radGroupBox_DB.Controls.Add(this.radLabel2);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_InputData);
            this.radGroupBox_DB.Controls.Add(this.radLabel_ForSave);
            this.radGroupBox_DB.Controls.Add(this.radLabel_Grand);
            this.radGroupBox_DB.Controls.Add(this.radButton_Save);
            this.radGroupBox_DB.Controls.Add(this.radButton_Cancel);
            this.radGroupBox_DB.Controls.Add(this.radLabel_point);
            this.radGroupBox_DB.Controls.Add(this.radLabel_2);
            this.radGroupBox_DB.Controls.Add(this.radTextBox_BilllID);
            this.radGroupBox_DB.Controls.Add(this.radLabel_1);
            this.radGroupBox_DB.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox_DB.FooterTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox_DB.HeaderText = "ข้อมูล";
            this.radGroupBox_DB.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox_DB.Name = "radGroupBox_DB";
            this.radGroupBox_DB.Size = new System.Drawing.Size(448, 437);
            this.radGroupBox_DB.TabIndex = 25;
            this.radGroupBox_DB.Text = "ข้อมูล";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(13, 85);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(74, 19);
            this.radLabel4.TabIndex = 79;
            this.radLabel4.Text = "สาขา";
            // 
            // radLabel_bch
            // 
            this.radLabel_bch.AutoSize = false;
            this.radLabel_bch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_bch.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_bch.Location = new System.Drawing.Point(102, 82);
            this.radLabel_bch.Name = "radLabel_bch";
            this.radLabel_bch.Size = new System.Drawing.Size(328, 23);
            this.radLabel_bch.TabIndex = 78;
            this.radLabel_bch.Text = ":";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.AutoSize = false;
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Date.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Date.Location = new System.Drawing.Point(177, 51);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(253, 23);
            this.radLabel_Date.TabIndex = 77;
            this.radLabel_Date.Text = ":";
            // 
            // radLabel_CstName
            // 
            this.radLabel_CstName.AutoSize = false;
            this.radLabel_CstName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstName.Location = new System.Drawing.Point(102, 264);
            this.radLabel_CstName.Name = "radLabel_CstName";
            this.radLabel_CstName.Size = new System.Drawing.Size(328, 23);
            this.radLabel_CstName.TabIndex = 76;
            this.radLabel_CstName.Text = ":";
            // 
            // radLabel_Cst
            // 
            this.radLabel_Cst.AutoSize = false;
            this.radLabel_Cst.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Cst.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Cst.Location = new System.Drawing.Point(102, 167);
            this.radLabel_Cst.Name = "radLabel_Cst";
            this.radLabel_Cst.Size = new System.Drawing.Size(328, 23);
            this.radLabel_Cst.TabIndex = 75;
            this.radLabel_Cst.Text = ":";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(12, 168);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(74, 19);
            this.radLabel5.TabIndex = 74;
            this.radLabel5.Text = "รหัสลูกค้า";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(13, 111);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(74, 19);
            this.radLabel1.TabIndex = 73;
            this.radLabel1.Text = "แคชเชียร์";
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(417, 12);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 72;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(102, 108);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(328, 23);
            this.radLabel_Name.TabIndex = 70;
            this.radLabel_Name.Text = ":";
            // 
            // radTextBox_Remaek
            // 
            this.radTextBox_Remaek.AcceptsReturn = true;
            this.radTextBox_Remaek.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Remaek.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remaek.Location = new System.Drawing.Point(13, 328);
            this.radTextBox_Remaek.Multiline = true;
            this.radTextBox_Remaek.Name = "radTextBox_Remaek";
            // 
            // 
            // 
            this.radTextBox_Remaek.RootElement.StretchVertically = true;
            this.radTextBox_Remaek.Size = new System.Drawing.Size(417, 50);
            this.radTextBox_Remaek.TabIndex = 3;
            this.radTextBox_Remaek.Tag = "";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(12, 303);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(418, 19);
            this.radLabel3.TabIndex = 68;
            this.radLabel3.Text = "หมายเหตุสำหรับการแก้ไขรหัสลูกค้าในบิล";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(12, 30);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(367, 19);
            this.radLabel2.TabIndex = 66;
            this.radLabel2.Text = "เลขที่บิล [Enter]";
            // 
            // radTextBox_InputData
            // 
            this.radTextBox_InputData.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_InputData.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_InputData.Location = new System.Drawing.Point(13, 263);
            this.radTextBox_InputData.MaxLength = 8;
            this.radTextBox_InputData.Name = "radTextBox_InputData";
            this.radTextBox_InputData.Size = new System.Drawing.Size(83, 25);
            this.radTextBox_InputData.TabIndex = 2;
            this.radTextBox_InputData.Text = "C0000430";
            this.radTextBox_InputData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_InputData_KeyDown);
            // 
            // radLabel_ForSave
            // 
            this.radLabel_ForSave.AutoSize = false;
            this.radLabel_ForSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_ForSave.Location = new System.Drawing.Point(12, 234);
            this.radLabel_ForSave.Name = "radLabel_ForSave";
            this.radLabel_ForSave.Size = new System.Drawing.Size(192, 19);
            this.radLabel_ForSave.TabIndex = 64;
            this.radLabel_ForSave.Text = "รหัสลูกค้าใหม่ [Enter]";
            // 
            // radLabel_Grand
            // 
            this.radLabel_Grand.AutoSize = false;
            this.radLabel_Grand.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Grand.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Grand.Location = new System.Drawing.Point(102, 137);
            this.radLabel_Grand.Name = "radLabel_Grand";
            this.radLabel_Grand.Size = new System.Drawing.Size(328, 23);
            this.radLabel_Grand.TabIndex = 62;
            this.radLabel_Grand.Text = ":";
            // 
            // radButton_Save
            // 
            this.radButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(104, 396);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(113, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(230, 396);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(113, 32);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_point
            // 
            this.radLabel_point.AutoSize = false;
            this.radLabel_point.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_point.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_point.Location = new System.Drawing.Point(102, 193);
            this.radLabel_point.Name = "radLabel_point";
            this.radLabel_point.Size = new System.Drawing.Size(328, 23);
            this.radLabel_point.TabIndex = 63;
            this.radLabel_point.Text = ":";
            // 
            // radLabel_2
            // 
            this.radLabel_2.AutoSize = false;
            this.radLabel_2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_2.Location = new System.Drawing.Point(12, 194);
            this.radLabel_2.Name = "radLabel_2";
            this.radLabel_2.Size = new System.Drawing.Size(74, 19);
            this.radLabel_2.TabIndex = 60;
            this.radLabel_2.Text = "จำนวนแต้ม";
            // 
            // radTextBox_BilllID
            // 
            this.radTextBox_BilllID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_BilllID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_BilllID.Location = new System.Drawing.Point(12, 51);
            this.radTextBox_BilllID.MaxLength = 16;
            this.radTextBox_BilllID.Name = "radTextBox_BilllID";
            this.radTextBox_BilllID.Size = new System.Drawing.Size(159, 25);
            this.radTextBox_BilllID.TabIndex = 0;
            this.radTextBox_BilllID.Text = "S2103A01-0000001";
            this.radTextBox_BilllID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BilllID_KeyDown);
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(12, 138);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(74, 19);
            this.radLabel_1.TabIndex = 39;
            this.radLabel_1.Text = "ยอดเงิน";
            // 
            // ChangePoint_BillSale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(448, 437);
            this.Controls.Add(this.radGroupBox_DB);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePoint_BillSale";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "บันทึกข้อมูล";
            this.Load += new System.EventHandler(this.ChangePoint_BillSale_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox_DB)).EndInit();
            this.radGroupBox_DB.ResumeLayout(false);
            this.radGroupBox_DB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_bch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_InputData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ForSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Grand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_point)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_BilllID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox_DB;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_BilllID;
        private Telerik.WinControls.UI.RadLabel radLabel_2;
        private Telerik.WinControls.UI.RadLabel radLabel_Grand;
        private Telerik.WinControls.UI.RadLabel radLabel_point;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_ForSave;
        private Telerik.WinControls.UI.RadTextBox radTextBox_InputData;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remaek;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Cst;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel_CstName;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_bch;
    }
}
