﻿namespace PC_Shop24Hrs.GeneralForm.Customers
{
    partial class Customer_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.ListViewDataItem listViewDataItem1 = new Telerik.WinControls.UI.ListViewDataItem("ออเดอร์ทั่วไป");
            Telerik.WinControls.UI.ListViewDataItem listViewDataItem2 = new Telerik.WinControls.UI.ListViewDataItem("ออเดอร์ของสด");
            Telerik.WinControls.UI.ListViewDataItem listViewDataItem3 = new Telerik.WinControls.UI.ListViewDataItem("ประวัติการขาย");
            Telerik.WinControls.UI.ListViewDataItem listViewDataItem4 = new Telerik.WinControls.UI.ListViewDataItem("ประวัติการโทร");
            Telerik.WinControls.UI.ListViewDataItem listViewDataItem5 = new Telerik.WinControls.UI.ListViewDataItem("ประวัติแลกแต้ม");
            Telerik.WinControls.UI.ListViewDataItem listViewDataItem6 = new Telerik.WinControls.UI.ListViewDataItem("อนุญาติให้แลกแต้ม");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customer_Detail));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_History = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.radListView_Case = new Telerik.WinControls.UI.RadListView();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radLabel_PartyTypeName = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Scan = new Telerik.WinControls.UI.RadButton();
            this.button_point = new System.Windows.Forms.Button();
            this.button_tel = new System.Windows.Forms.Button();
            this.button_MNOG = new System.Windows.Forms.Button();
            this.Button_MNPD = new System.Windows.Forms.Button();
            this.radButton_Refresh = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Find = new Telerik.WinControls.UI.RadButton();
            this.radLabel_CstAddress = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstPoint = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstNameAlias = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstTel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstIDCard = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_cst = new Telerik.WinControls.UI.RadTextBox();
            this.pictureBox_Cst = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_History)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_History.MasterTemplate)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radListView_Case)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PartyTypeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Scan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Refresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Find)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstNameAlias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstIDCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_cst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Cst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.tableLayoutPanel3);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(3, 333);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(813, 280);
            this.radPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.RadGridView_History, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(813, 280);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // RadGridView_History
            // 
            this.RadGridView_History.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_History.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_History.Location = new System.Drawing.Point(133, 3);
            // 
            // 
            // 
            this.RadGridView_History.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_History.Name = "RadGridView_History";
            // 
            // 
            // 
            this.RadGridView_History.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_History.Size = new System.Drawing.Size(677, 274);
            this.RadGridView_History.TabIndex = 0;
            this.RadGridView_History.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_History.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_History_CellDoubleClick);
            this.RadGridView_History.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_History.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_History.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.radListView_Case, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(124, 274);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // radListView_Case
            // 
            this.radListView_Case.AllowColumnResize = false;
            this.radListView_Case.AllowEdit = false;
            this.radListView_Case.AllowRemove = false;
            this.radListView_Case.Dock = System.Windows.Forms.DockStyle.Fill;
            listViewDataItem1.ImageKey = "";
            listViewDataItem1.Text = "ออเดอร์ทั่วไป";
            listViewDataItem2.Text = "ออเดอร์ของสด";
            listViewDataItem3.Text = "ประวัติการขาย";
            listViewDataItem4.Text = "ประวัติการโทร";
            listViewDataItem5.Text = "ประวัติแลกแต้ม";
            listViewDataItem6.Text = "อนุญาติให้แลกแต้ม";
            this.radListView_Case.Items.AddRange(new Telerik.WinControls.UI.ListViewDataItem[] {
            listViewDataItem1,
            listViewDataItem2,
            listViewDataItem3,
            listViewDataItem4,
            listViewDataItem5,
            listViewDataItem6});
            this.radListView_Case.Location = new System.Drawing.Point(3, 3);
            this.radListView_Case.Name = "radListView_Case";
            this.radListView_Case.ShowCheckBoxes = true;
            this.radListView_Case.ShowGridLines = true;
            this.radListView_Case.Size = new System.Drawing.Size(118, 168);
            this.radListView_Case.TabIndex = 2;
            this.radListView_Case.ItemCheckedChanged += new Telerik.WinControls.UI.ListViewItemEventHandler(this.RadListView_Case_ItemCheckedChanged);
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.BorderVisible = true;
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 177);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(118, 94);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>DoubleClick เลขที่บิล &gt; <br />บันทึกลูกค้ารับแล้ว<br /><br />สีแดง เลขที" +
    "่บิล &gt; <br />ลูกค้ายังไม่รับสินค้า</html>";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 330F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(819, 616);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.68421F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.31579F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox_Cst, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(813, 324);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radButton_pdt);
            this.panel1.Controls.Add(this.radLabel_PartyTypeName);
            this.panel1.Controls.Add(this.radButton_Scan);
            this.panel1.Controls.Add(this.button_point);
            this.panel1.Controls.Add(this.button_tel);
            this.panel1.Controls.Add(this.button_MNOG);
            this.panel1.Controls.Add(this.Button_MNPD);
            this.panel1.Controls.Add(this.radButton_Refresh);
            this.panel1.Controls.Add(this.RadButton_Find);
            this.panel1.Controls.Add(this.radLabel_CstAddress);
            this.panel1.Controls.Add(this.radLabel_CstPoint);
            this.panel1.Controls.Add(this.radLabel_CstNameAlias);
            this.panel1.Controls.Add(this.radLabel_CstTel);
            this.panel1.Controls.Add(this.radLabel_CstName);
            this.panel1.Controls.Add(this.radLabel_CstIDCard);
            this.panel1.Controls.Add(this.radLabel10);
            this.panel1.Controls.Add(this.radLabel_CstID);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_1);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radTextBox_cst);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(593, 318);
            this.panel1.TabIndex = 0;
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(563, 4);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 82;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radLabel_PartyTypeName
            // 
            this.radLabel_PartyTypeName.AutoSize = false;
            this.radLabel_PartyTypeName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_PartyTypeName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_PartyTypeName.Location = new System.Drawing.Point(343, 31);
            this.radLabel_PartyTypeName.Name = "radLabel_PartyTypeName";
            this.radLabel_PartyTypeName.Size = new System.Drawing.Size(247, 23);
            this.radLabel_PartyTypeName.TabIndex = 81;
            this.radLabel_PartyTypeName.Text = ":";
            // 
            // radButton_Scan
            // 
            this.radButton_Scan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radButton_Scan.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radButton_Scan.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Scan.ForeColor = System.Drawing.Color.Blue;
            this.radButton_Scan.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Scan.Location = new System.Drawing.Point(262, 29);
            this.radButton_Scan.Name = "radButton_Scan";
            this.radButton_Scan.Size = new System.Drawing.Size(75, 26);
            this.radButton_Scan.TabIndex = 80;
            this.radButton_Scan.Text = "สแกนบัตร";
            this.radButton_Scan.Click += new System.EventHandler(this.RadButton_Scan_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Scan.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Scan.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Scan.GetChildAt(0))).Text = "สแกนบัตร";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Scan.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Scan.GetChildAt(0).GetChildAt(0))).AutoSize = false;
            // 
            // button_point
            // 
            this.button_point.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.button_point.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_point.ForeColor = System.Drawing.Color.Black;
            this.button_point.Location = new System.Drawing.Point(13, 272);
            this.button_point.Name = "button_point";
            this.button_point.Size = new System.Drawing.Size(115, 42);
            this.button_point.TabIndex = 79;
            this.button_point.Text = "บันทึก\r\nแลกแต้ม";
            this.button_point.UseVisualStyleBackColor = false;
            this.button_point.Click += new System.EventHandler(this.Button_point_Click);
            // 
            // button_tel
            // 
            this.button_tel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.button_tel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_tel.ForeColor = System.Drawing.Color.Black;
            this.button_tel.Location = new System.Drawing.Point(368, 272);
            this.button_tel.Name = "button_tel";
            this.button_tel.Size = new System.Drawing.Size(115, 42);
            this.button_tel.TabIndex = 78;
            this.button_tel.Text = "บันทึก\r\nการโทร";
            this.button_tel.UseVisualStyleBackColor = false;
            this.button_tel.Click += new System.EventHandler(this.Button_tel_Click);
            // 
            // button_MNOG
            // 
            this.button_MNOG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.button_MNOG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_MNOG.ForeColor = System.Drawing.Color.Black;
            this.button_MNOG.Location = new System.Drawing.Point(250, 272);
            this.button_MNOG.Name = "button_MNOG";
            this.button_MNOG.Size = new System.Drawing.Size(115, 42);
            this.button_MNOG.TabIndex = 77;
            this.button_MNOG.Text = "เปิดออเดอร์\r\nของสด [MNOG]";
            this.button_MNOG.UseVisualStyleBackColor = false;
            this.button_MNOG.Click += new System.EventHandler(this.Button_MNOG_Click);
            // 
            // Button_MNPD
            // 
            this.Button_MNPD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.Button_MNPD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_MNPD.ForeColor = System.Drawing.Color.Black;
            this.Button_MNPD.Location = new System.Drawing.Point(132, 272);
            this.Button_MNPD.Name = "Button_MNPD";
            this.Button_MNPD.Size = new System.Drawing.Size(115, 42);
            this.Button_MNPD.TabIndex = 76;
            this.Button_MNPD.Text = "เปิดออเดอร์\r\nทั่วไป [MNPD]";
            this.Button_MNPD.UseVisualStyleBackColor = false;
            this.Button_MNPD.Click += new System.EventHandler(this.Button_MNPD_Click);
            // 
            // radButton_Refresh
            // 
            this.radButton_Refresh.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Refresh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_Refresh.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Refresh.Location = new System.Drawing.Point(13, 29);
            this.radButton_Refresh.Name = "radButton_Refresh";
            this.radButton_Refresh.Size = new System.Drawing.Size(26, 26);
            this.radButton_Refresh.TabIndex = 71;
            this.radButton_Refresh.Text = "radButton3";
            this.radButton_Refresh.Click += new System.EventHandler(this.RadButton_Refresh_Click);
            // 
            // RadButton_Find
            // 
            this.RadButton_Find.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_Find.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_Find.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_Find.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_Find.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_Find.Location = new System.Drawing.Point(230, 29);
            this.RadButton_Find.Name = "RadButton_Find";
            this.RadButton_Find.Size = new System.Drawing.Size(26, 26);
            this.RadButton_Find.TabIndex = 70;
            this.RadButton_Find.Text = "radButton3";
            this.RadButton_Find.Click += new System.EventHandler(this.RadButton_Find_Click);
            // 
            // radLabel_CstAddress
            // 
            this.radLabel_CstAddress.AutoSize = false;
            this.radLabel_CstAddress.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstAddress.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstAddress.Location = new System.Drawing.Point(142, 219);
            this.radLabel_CstAddress.Name = "radLabel_CstAddress";
            this.radLabel_CstAddress.Size = new System.Drawing.Size(428, 47);
            this.radLabel_CstAddress.TabIndex = 69;
            this.radLabel_CstAddress.Text = ":";
            this.radLabel_CstAddress.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel_CstPoint
            // 
            this.radLabel_CstPoint.AutoSize = false;
            this.radLabel_CstPoint.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstPoint.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstPoint.Location = new System.Drawing.Point(142, 190);
            this.radLabel_CstPoint.Name = "radLabel_CstPoint";
            this.radLabel_CstPoint.Size = new System.Drawing.Size(184, 23);
            this.radLabel_CstPoint.TabIndex = 68;
            this.radLabel_CstPoint.Text = ":";
            // 
            // radLabel_CstNameAlias
            // 
            this.radLabel_CstNameAlias.AutoSize = false;
            this.radLabel_CstNameAlias.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstNameAlias.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstNameAlias.Location = new System.Drawing.Point(142, 140);
            this.radLabel_CstNameAlias.Name = "radLabel_CstNameAlias";
            this.radLabel_CstNameAlias.Size = new System.Drawing.Size(409, 23);
            this.radLabel_CstNameAlias.TabIndex = 66;
            this.radLabel_CstNameAlias.Text = ":";
            // 
            // radLabel_CstTel
            // 
            this.radLabel_CstTel.AutoSize = false;
            this.radLabel_CstTel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstTel.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstTel.Location = new System.Drawing.Point(142, 166);
            this.radLabel_CstTel.Name = "radLabel_CstTel";
            this.radLabel_CstTel.Size = new System.Drawing.Size(409, 23);
            this.radLabel_CstTel.TabIndex = 65;
            this.radLabel_CstTel.Text = ":";
            // 
            // radLabel_CstName
            // 
            this.radLabel_CstName.AutoSize = false;
            this.radLabel_CstName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstName.Location = new System.Drawing.Point(142, 89);
            this.radLabel_CstName.Name = "radLabel_CstName";
            this.radLabel_CstName.Size = new System.Drawing.Size(409, 23);
            this.radLabel_CstName.TabIndex = 64;
            this.radLabel_CstName.Text = ":";
            // 
            // radLabel_CstIDCard
            // 
            this.radLabel_CstIDCard.AutoSize = false;
            this.radLabel_CstIDCard.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstIDCard.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstIDCard.Location = new System.Drawing.Point(142, 115);
            this.radLabel_CstIDCard.Name = "radLabel_CstIDCard";
            this.radLabel_CstIDCard.Size = new System.Drawing.Size(409, 23);
            this.radLabel_CstIDCard.TabIndex = 63;
            this.radLabel_CstIDCard.Text = ":";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(13, 219);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(34, 19);
            this.radLabel10.TabIndex = 62;
            this.radLabel10.Text = "ที่อยู่";
            // 
            // radLabel_CstID
            // 
            this.radLabel_CstID.AutoSize = false;
            this.radLabel_CstID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstID.Location = new System.Drawing.Point(142, 63);
            this.radLabel_CstID.Name = "radLabel_CstID";
            this.radLabel_CstID.Size = new System.Drawing.Size(184, 23);
            this.radLabel_CstID.TabIndex = 61;
            this.radLabel_CstID.Text = ":";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(13, 191);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(74, 19);
            this.radLabel7.TabIndex = 59;
            this.radLabel7.Text = "จำนวนแต้ม";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(13, 166);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(59, 19);
            this.radLabel6.TabIndex = 58;
            this.radLabel6.Text = "เบอร์โทร";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(13, 141);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(60, 19);
            this.radLabel5.TabIndex = 57;
            this.radLabel5.Text = "ชื่อค้นหา";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(13, 116);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(91, 19);
            this.radLabel3.TabIndex = 56;
            this.radLabel3.Text = "บัตรประชาชน";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(13, 91);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(59, 19);
            this.radLabel2.TabIndex = 55;
            this.radLabel2.Text = "ชื่อลูกค้า";
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(13, 1);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(487, 19);
            this.radLabel_1.TabIndex = 53;
            this.radLabel_1.Text = "<html>ป้อนรหัสลูกค้า,เบอร์โทร,บัตรประชาชน Enter : ชื่อลูกค้า F1</html>";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(13, 65);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 19);
            this.radLabel4.TabIndex = 51;
            this.radLabel4.Text = "รหัสลูกค้า";
            // 
            // radTextBox_cst
            // 
            this.radTextBox_cst.AutoSize = false;
            this.radTextBox_cst.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_cst.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_cst.Location = new System.Drawing.Point(49, 25);
            this.radTextBox_cst.Name = "radTextBox_cst";
            this.radTextBox_cst.Size = new System.Drawing.Size(175, 33);
            this.radTextBox_cst.TabIndex = 0;
            this.radTextBox_cst.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_cst_KeyDown);
            // 
            // pictureBox_Cst
            // 
            this.pictureBox_Cst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_Cst.Location = new System.Drawing.Point(602, 3);
            this.pictureBox_Cst.Name = "pictureBox_Cst";
            this.pictureBox_Cst.Size = new System.Drawing.Size(208, 318);
            this.pictureBox_Cst.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Cst.TabIndex = 1;
            this.pictureBox_Cst.TabStop = false;
            // 
            // Customer_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 616);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Customer_Detail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "ลูกค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Customer_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_History.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_History)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radListView_Case)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PartyTypeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Scan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Refresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Find)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstNameAlias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstIDCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_cst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Cst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_History;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_cst;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel_CstID;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel_CstPoint;
        private Telerik.WinControls.UI.RadLabel radLabel_CstNameAlias;
        private Telerik.WinControls.UI.RadLabel radLabel_CstTel;
        private Telerik.WinControls.UI.RadLabel radLabel_CstName;
        private Telerik.WinControls.UI.RadLabel radLabel_CstIDCard;
        private Telerik.WinControls.UI.RadLabel radLabel_CstAddress;
        private System.Windows.Forms.PictureBox pictureBox_Cst;
        private Telerik.WinControls.UI.RadButton RadButton_Find;
        private Telerik.WinControls.UI.RadButton radButton_Refresh;
        private System.Windows.Forms.Button Button_MNPD;
        private System.Windows.Forms.Button button_MNOG;
        private System.Windows.Forms.Button button_tel;
        private System.Windows.Forms.Button button_point;
        private Telerik.WinControls.UI.RadListView radListView_Case;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadButton radButton_Scan;
        private Telerik.WinControls.UI.RadLabel radLabel_PartyTypeName;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
    }
}
