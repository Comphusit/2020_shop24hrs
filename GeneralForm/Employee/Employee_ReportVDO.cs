﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.Windows.Diagrams.Core;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.Data;

namespace PC_Shop24Hrs.GeneralForm.Employee
{
    public partial class Employee_ReportVDO : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable("dt_Data");
        DataTable dtBranch = new DataTable("dtBranch");
        DataTable dt = new DataTable("dt");
        DataTable dtEmpTimekeep = new DataTable("dtEmpTimekeep");
        DataTable Branch = new DataTable();
        readonly string pPathEmpVDO = PathImageClass.pPathEmpVDO;
        readonly string _pTypeReport, _pTypeOpen;
        string dir;
        string[] Colmn;
        enum EmployeeVDO
        {
            EmployeeCheckVDO,           //0
            EmployeeCheckVDOSum,        //1
            EmployeeCheckVDOD054,       //2
            EmployeeCheckVDOD88,        //3 เลิกใช้แล้ว พนักงานย้ายไปแผนก แคชเชียร์เงินสดแล้ว
            EmployeeCheckVDOOther,      //4
            EmployeeCheckVDOBill,       //5
            EmployeeCheckVDOCar,        //6
            EmployeeCheckVDOPCA,         //7
            EmployeeCheckVDOPartTime,    //8
            EmployeeCheckVDOOT    //9
        }
        /// <summary>
        /// DirVDO Foldervdo
        /// </summary>
        enum DirVDO
        {
            VDO,            //0
            Bill,           //1
            Motorcycle,     //2
            PC              //3
        }
        //Load
        public Employee_ReportVDO(string pTypeReport, string pTypeOpen)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pTypeReport = ((EmployeeVDO)int.Parse(pTypeReport)).ToString();
            _pTypeOpen = pTypeOpen;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        //Load
        private void Employee_ReportVDO_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButton_Search.ButtonElement.ShowBorder = true;


            radStatusStrip1.SizingGrip = false;
            radGridView_Branch.Visible = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Beg, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            radDropDownList_Condition.DropDownListElement.ListElement.Font = SystemClass.SetFontGernaral;
            radDropDownList_Condition.DropDownListElement.Font = SystemClass.SetFontGernaral;
            radDropDownList_Condition.DropDownListElement.TextBox.ForeColor = Color.Blue;
            radDropDownList_Condition.DropDownListElement.TextBox.Font = SystemClass.SetFontGernaral;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_group.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadDropDownList_Branch.Visible = false; RadCheckBox_Branch.Visible = false; radDropDownList_Condition.Visible = false;
            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");

            RadDropDownList_Branch.DataSource = dtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            radLabel_Detail.Text = "สีฟ้า >> มีวีดีโอ | DoubleClick ดูวีดีโอ.";

            switch (_pTypeReport)
            {
                case "EmployeeCheckVDO":
                    RadCheckBox_Branch.Text = "ทุกสาขา";
                    RadCheckBox_Branch.Visible = true; radGridView_Branch.Visible = true;
                    radDateTimePicker_End.Visible = false; radDateTimePicker_Beg.Value = DateTime.Now.AddDays(-1);
                    dt_Data = Employee_Class.AddNewColumn(new string[] { "EMPLID", "EMPLNAME", "BRANCH_ID", "BRANCH_NAME", "DIMENSION", "DESCRIPTION"
                        , "VDOSTATUS", "PW_DATETIME_IN", "PW_DATETIME_OUT", "TIME_IN", "TIME_OUT", "VDOTIMELAP", "SUMTIME", "PATHVDO" });
                    Branch = Employee_Class.AddNewColumn(new string[] { "CHECK", "BRANCH_ID", "BRANCH_NAME" });
                    foreach (DataRow row in dtBranch.Rows)
                    {
                        Branch.Rows.Add("0", row["BRANCH_ID"], row["NAME_BRANCH"]);
                    }
                    dir = DirVDO.VDO.ToString();
                    radGridView_Branch.Columns.Clear();
                    radGridView_Branch.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_CheckBox("CHECK", "/", 50));
                    radGridView_Branch.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID"));
                    radGridView_Branch.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 170));
                    radGridView_Branch.DataSource = Branch;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "สาขา"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DIMENSION", "แผนก"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("VDOSTATUS", "วีดีโอ", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้างาน", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TIME_IN", "TIME_IN"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("TIME_OUT", "TIME_OUT"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VDOTIMELAP", "เวลาถ่ายวีดีโอ", 180));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SUMTIME", "เวลารวม", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddHyperlinkColumn_AddManual("PATHVDO", "PATHVDO", 150));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PATHVDO", "PATHVDO"));//AddHyperlinkColumn_AddManual

                    SetConditions("VDOSTATUS = '1'", "VDOSTATUS", true, Color.LightSkyBlue, "VDOSTATUS");
                    SetConditions("VDOSTATUS = 'ไม่ตรวจตัว'", "VDOSTATUS", true, Color.Red, "VDOSTATUS");
                    SetConditions("PW_DATETIME_IN = 'N/A'", "PW_DATETIME_IN", true, Color.Yellow, "PW_DATETIME_IN");
                    SetConditions("VDOSTATUS = '0' AND TIME_IN = '1' AND PW_DATETIME_IN <> ''", "PW_DATETIME_IN", true, Color.Yellow, "PW_DATETIME_IN");
                    SetConditions("VDOSTATUS = '0' AND TIME_OUT = '0' AND PW_DATETIME_OUT <> ''", "PW_DATETIME_OUT", true, Color.Yellow, "PW_DATETIME_OUT");

                    radLabel_Detail.Text = @"สีฟ้า >> มีวีดีโอ | DoubleClick ดูวีดีโอ | สีแดง >> ไม่มีวีดีโอตรวจตัว แต่มีลงเวลาออกงาน | สีขาว >> ไม่มีวีดีโอตรวจตัว และไม่มีลงเวลาออกงาน";
                    break;

                case "EmployeeCheckVDOSum":
                    FitLayoutPanel();
                    RadCheckBox_Branch.Visible = true;
                    RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.Enabled = false;
                    radDateTimePicker_Beg.Value = DateTime.Now.AddDays(-7);
                    dir = DirVDO.VDO.ToString();
                    break;

                case "EmployeeCheckVDOBill":
                    FitLayoutPanel();
                    RadCheckBox_Branch.Visible = true;
                    RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.Enabled = false;
                    radDateTimePicker_Beg.Value.AddDays(-7);
                    radDateTimePicker_End.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("VDOSTATUS", "วีดีโอ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_QTYBill", "PW_QTYBill"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("VDOBill", "VDOBill"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้างาน", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_BRANCH_OUT", "PW_BRANCH_OUT"));

                    dt_Data = Employee_Class.AddNewColumn(new string[] { "EMPLID", "EMPLNAME", "BRANCH_ID", "BRANCH_NAME", "DESCRIPTION"
                        , "VDOSTATUS","PW_QTYBill",  "VDOBill", "PW_DATETIME_IN", "PW_DATETIME_OUT","PW_BRANCH_OUT" });

                    SetConditions("VDOSTATUS <> '0'", "VDOSTATUS", true, Color.LightSkyBlue, "VDOSTATUS");
                    SetConditions("VDOBill < PW_QTYBill", "VDOSTATUS", true, Color.Red, "VDOSTATUS");
                    dir = DirVDO.Bill.ToString();
                    radLabel_Detail.Text = @"สีฟ้า >> มีวีดีโอ | DoubleClick ดูวีดีโอ | สีแดง >> มีวีดีโอน้อยกว่าจำนวนบิล.";
                    break;

                case "EmployeeCheckVDOCar":
                    FitLayoutPanel();
                    RadCheckBox_Branch.Visible = true;
                    RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.Enabled = false;
                    radDateTimePicker_Beg.Value = DateTime.Now.AddDays(-7);
                    dir = DirVDO.Motorcycle.ToString();
                    radLabel_Detail.Text = @"สีฟ้า >> มีวีดีโอ | DoubleClick ดูวีดีโอ | N / A >> ไม่มีข้อมูลเข้างาน.";

                    DataTable table = new DataTable();
                    table.Columns.Add("value");
                    table.Columns.Add("text");

                    table.Rows.Add("01", "ทั้งหมด");
                    table.Rows.Add("02", "ไม่มี VDO");
                    table.Rows.Add("03", "มี VDO");

                    radDropDownList_Condition.DataSource = table;
                    radDropDownList_Condition.ValueMember = "value";
                    radDropDownList_Condition.DisplayMember = "text";
                    radDropDownList_Condition.SelectedIndex = 0;
                    break;

                case "EmployeeCheckVDOPCA":
                    FitLayoutPanel();
                    radDateTimePicker_Beg.Value.AddDays(-7);
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "บริษัท", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("VDOSTATUS", "วีดีโอ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขาเข้างาน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้างาน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMEOUT", "สาขาออกงาน", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PW_SUMDATE", "วันรวม", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PW_SUMTIME", "เวลารวม", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PW_STATUS", "สถานะ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CHECKNAME", "ผู้ถ่ายรูปตรวจตัว PC", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddHyperlinkColumn_AddManual("PATHVDO", "PATHVDO", 150));//AddHyperlinkColumn_AddManual

                    dt_Data = Employee_Class.AddNewColumn(new string[] { "DIMENSION", "EMPLID", "EMPLNAME", "DESCRIPTION", "VDOSTATUS", "BRANCH_NAME", "PW_DATETIME_IN", "NAMEOUT",
                    "PW_DATETIME_OUT", "PW_SUMDATE", "PW_SUMTIME", "PW_STATUS", "PW_MNEP_CHECKNAME", "PW_MNEP_CHECKDATETIME", "PATHVDO" });
                    SetConditions("VDOSTATUS = '1'", "VDOSTATUS", true, Color.LightSkyBlue, "VDOSTATUS");
                    SetConditions("VDOSTATUS = 'ไม่ตรวจตัว'", "VDOSTATUS", true, Color.Red, "VDOSTATUS");
                    dir = DirVDO.PC.ToString();
                    break;

                default: //EmployeeCheckVDOD88-EmployeeCheckVDOD054-EmployeeCheckVDOOther--EmployeeCheckVDOPartTime
                    FitLayoutPanel();
                    SetDataGrideHead();
                    radDateTimePicker_Beg.Value = DateTime.Now.AddDays(-7);
                    dir = DirVDO.VDO.ToString();
                    break;
            }
            this.GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
            switch (_pTypeOpen)
            {
                case "SHOP":
                    FitLayoutPanel();
                    RadCheckBox_Branch.Checked = true;
                    RadCheckBox_Branch.Visible = false;
                    RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                    RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.Enabled = false;
                    break;
                case "SUPC":
                    if (_pTypeReport.Equals("EmployeeCheckVDO") || _pTypeReport.Equals("EmployeeCheckVDOSum")
                        || _pTypeReport.Equals("EmployeeCheckVDOBill") || _pTypeReport.Equals("EmployeeCheckVDOCar"))
                    {
                        radLabel_Detail.Text = @"สีฟ้า >> มีวีดีโอ | DoubleClick ดูวีดีโอ | * การระบุสาขาจะทำให้ดึงข้อมูลได้เร็วขึ้น.";
                    }
                    break;
                default:
                    break;
            }
            this.ClearTxt();
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //Begin Edit
        private void RadGridView_Branch_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Column == radGridView_Branch.Columns[2]) e.Cancel = true;
            if (RadCheckBox_Branch.Checked == true) e.Cancel = true;
        }
        //End Edit
        private void RadGridView_Branch_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            string headerText = radGridView_Branch.Columns[e.ColumnIndex].HeaderText;
            if (!headerText.Equals("/")) return;
            if (radGridView_Branch.CurrentRow.Cells["CHECK"].Value.ToString() == "On")
            {
                DataRow[] rows = Branch.Select("BRANCH_ID = '" + radGridView_Branch.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + "' ");
                int iRow = Branch.Rows.IndexOf(rows[0]);
                Branch.Rows[iRow]["CHECK"] = "1";
                Branch.AcceptChanges();
            }
        }
        #endregion
        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            RadButton_Search.Focus();
        }
        //GetGridViewSummary
        private void GetGridViewSummary(string Col, GridAggregateFunction gridAggregate)
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem(Col, "ทั้งหมด : {0:0,0}", gridAggregate);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
        }
        private void SetConditions(string Expression, string ColName, bool applyToRow, Color color, string array)
        {
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject(ColName, Expression, applyToRow)
            {
                CellBackColor = color
            };
            this.RadGridView_ShowHD.Columns[array].ConditionalFormattingObjectList.Add(obj1);
        }
        #region Even
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
            this.Cursor = Cursors.WaitCursor;
            this.SetDataGridShow();
            this.Cursor = Cursors.Default;
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            this.ClearTxt();
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            this.SelectRow(this.RadCheckBox_Branch.Checked);
            if (RadCheckBox_Branch.CheckState == CheckState.Checked) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }
        //CellDoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                switch (_pTypeReport)
                {
                    case "EmployeeCheckVDO":
                        if (e.Value.Equals("1") && e.Column.FieldName == "VDOSTATUS") System.Diagnostics.Process.Start(this.RadGridView_ShowHD.CurrentRow.Cells["PATHVDO"].Value.ToString());
                        break;
                    case "EmployeeCheckVDOSum":
                        if (e.Value.Equals("1"))
                        {
                            string date = string.Format(@"{0}-{1}-{2}", e.Column.FieldName.Substring(8, 4), e.Column.FieldName.Substring(6, 2), e.Column.FieldName.Substring(4, 2));
                            DataRow[] row = dtEmpTimekeep.Select("EMPLID = '" + RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString() + "' AND PW_DATETIME_OUT = '" + date + "'");

                            foreach (var r in row)
                            {
                                System.Diagnostics.Process.Start(Employee_Class.FindVDOEmp(RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString()
                                    , date
                                    , r["PW_BRANCH_OUT"].ToString(), "1", pPathEmpVDO, dir));
                            }
                        }
                        break;
                    case "EmployeeCheckVDOBill":
                        if (!e.Value.Equals("0") && e.Column.FieldName == "VDOSTATUS")
                        {
                            Employee employee = new Employee()
                            {
                                EMPLID = RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString()
                            ,
                                EMPLNAME = RadGridView_ShowHD.CurrentRow.Cells["EMPLNAME"].Value.ToString()
                            ,
                                BRANCH_ID = RadGridView_ShowHD.CurrentRow.Cells["PW_BRANCH_OUT"].Value.ToString()
                            ,
                                PW_DATETIME_OUT = RadGridView_ShowHD.CurrentRow.Cells["PW_DATETIME_OUT"].Value.ToString()
                            ,
                                PW_DATETIME_IN = RadGridView_ShowHD.CurrentRow.Cells["PW_DATETIME_IN"].Value.ToString()
                            ,
                                VDOBILL = int.Parse(RadGridView_ShowHD.CurrentRow.Cells["VDOBILL"].Value.ToString())
                            };

                            Employee_ReportVDODetail frm = new Employee_ReportVDODetail(employee, pPathEmpVDO);
                            frm.ShowDialog();
                        }
                        break;
                    case "EmployeeCheckVDOCar":
                        if (e.Value.Equals("1"))
                        {
                            string date = string.Format(@"{0}-{1}-{2}", e.Column.FieldName.Substring(8, 4), e.Column.FieldName.Substring(6, 2), e.Column.FieldName.Substring(4, 2));
                            DataRow[] row = dtEmpTimekeep.Select("EMPLID = '" + RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString() + "' AND PW_DATETIME_OUT = '" + date + "'");

                            foreach (var r in row)
                            {
                                System.Diagnostics.Process.Start(Employee_Class.FindVDOEmp(RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString()
                                    , date
                                    , r["PW_BRANCH_OUT"].ToString(), "1", pPathEmpVDO, DirVDO.Motorcycle.ToString()));
                            }
                        }
                        break;
                    default:
                        if (e.Value.Equals("1") && e.Column.FieldName == "VDOSTATUS") System.Diagnostics.Process.Start(this.RadGridView_ShowHD.CurrentRow.Cells["PATHVDO"].Value.ToString());
                        break;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
        }
        private void RadCheckBox_group_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            if (radCheckBox_group.Checked == true)
            {
                this.GroupByBranchID();
                this.GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
            }
            else
            {
                this.RadGridView_ShowHD.GroupDescriptors.Clear();
                this.GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
            }
        }
        private void RadDateTimePicker_Beg_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Beg, radDateTimePicker_End);
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        private void RadDropDownList_Condition_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Condition.SelectedIndex > -1)
            {
                RadGridView_ShowHD.FilterDescriptors.Clear();
                this.GridViewfilter(RadGridView_ShowHD.Columns["VDOSTATUS"], radDropDownList_Condition.SelectedValue.ToString());
            }
        }
        #endregion

        #region Method
        private void SetDataGridShow()
        {
            switch (_pTypeReport)
            {
                case "EmployeeCheckVDO":
                    try
                    {
                        dt = Employee_Class.GetEmployeeCheckVDO(EmployeeClass.EmployeeVDO(radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"), GetBranchSelect(""))
                            , Employee_Class.ShiftTime0()
                            , radDateTimePicker_Beg.Value, pPathEmpVDO, dir, dt_Data); //DirVDO.VDO.ToString()
                    }
                    catch (Exception ex)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("เลือกสาขาก่อนเท่านั้น." + ex);
                    }
                    break;
                case "EmployeeCheckVDOSum":
                    if (RadGridView_ShowHD.MasterTemplate.Columns.Count > 0) RadGridView_ShowHD.MasterTemplate.Columns.Clear();
                    if (dt_Data.Columns.Count > 0) dt_Data.Columns.Clear();

                    dt_Data = Employee_Class.AddNewColumn(new string[] { "EMPLID", "EMPLNAME", "BRANCH_ID", "BRANCH_NAME", "DIMENSION", "DESCRIPTION" }
                  , new string[] { "EMPLID" });

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DIMENSION", "แผนก"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));

                    Colmn = GetColumnsDate();
                    AppendNewColumn(dt_Data, Colmn);  //เพิ่ม new column 
                    dtEmpTimekeep = EmployeeClass.EmployeeTimeKeeper(radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                        GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), 1);
                    dt = Employee_Class.GetEmployeeCheckVDOSum(EmployeeClass.EmployeeBranch(GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()))
                        , dtEmpTimekeep
                        , pPathEmpVDO, dir, Colmn, dt_Data);
                    Colmn.ForEach(colNameDate => SetConditions(string.Format(@"{0} = '1'", colNameDate), colNameDate, true, Color.LightSkyBlue, colNameDate));
                    break;

                case "EmployeeCheckVDOD054":
                    try
                    {
                        dt = Employee_Class.GetEmployeeCheckVDOByDept(EmployeeClass.EmployeeDept("D054"
                        , GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"),
                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd")), pPathEmpVDO, dir, dt_Data); // DirVDO.VDO.ToString()

                    }
                    catch (Exception)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ค้นหา เช็ควันที่ต้องการอีกครั้ง.");
                    }

                    break;
                case "EmployeeCheckVDOD88":
                    try
                    {
                        dt = Employee_Class.GetEmployeeCheckVDOByDept(EmployeeClass.EmployeeDept("D088"
                            , GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd")), pPathEmpVDO, dir, dt_Data); // DirVDO.VDO.ToString()

                    }
                    catch (Exception)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ค้นหา เช็ควันที่ต้องการอีกครั้ง.");
                    }
                    break;
                case "EmployeeCheckVDOOther":
                    try
                    {
                        dt = Employee_Class.GetEmployeeCheckVDOByDept(EmployeeClass.EmployeeDept(""
                            , GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"),
                            radDateTimePicker_End.Value.ToString("yyyy-MM-dd")), pPathEmpVDO, dir, dt_Data); // DirVDO.VDO.ToString()
                    }
                    catch (Exception)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ค้นหา เช็ควันที่ต้องการอีกครั้ง.");
                    }
                    break;
                case "EmployeeCheckVDOBill":
                    dt = Employee_Class.GetEmployeeCheckVDOBill(EmployeeClass.EmployeeVDO(radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"),
                        GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString())), pPathEmpVDO, dir, dt_Data); // DirVDO.Bill.ToString()
                    break;
                case "EmployeeCheckVDOCar":
                    if (RadGridView_ShowHD.MasterTemplate.Columns.Count > 0) RadGridView_ShowHD.MasterTemplate.Columns.Clear();
                    if (dt_Data.Columns.Count > 0) dt_Data.Columns.Clear();
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_BRANCH_OUT", "PW_BRANCH_OUT"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_DATETIME_OUT", "PW_DATETIME_OUT"));
                    dt_Data = Employee_Class.AddNewColumn(new string[] { "EMPLID", "EMPLNAME", "BRANCH_ID", "BRANCH_NAME", "DESCRIPTION", "PW_BRANCH_OUT", "PW_DATETIME_OUT" });

                    Colmn = GetColumnsDate();
                    AppendNewColumn(dt_Data, Colmn);  //เพิ่ม new column 
                    dtEmpTimekeep = EmployeeClass.EmployeeTimeKeeper(radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), 1);
                    dt = Employee_Class.GetEmployeeCheckVDOMotorcycle(EmployeeClass.EmployeeTimeKeeper(radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), 0)
                        , dtEmpTimekeep
                        , pPathEmpVDO, dir, Colmn, dt_Data); // DirVDO.Motorcycle.ToString()

                    Colmn.ForEach(colName => SetConditions(string.Format(@"{0} = '1'", colName), colName, true, Color.LightSkyBlue, colName));
                    Colmn.ForEach(colName => SetConditions(string.Format(@"{0} = 'ไม่มีวีดีโอ'", colName), colName, true, Color.Red, colName));
                    break;

                case "EmployeeCheckVDOPCA":
                    dt = Employee_Class.GetEmployeeCheckVDOPCA(Employee_Class.EmployeePCA(radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd")
                        , GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString())), pPathEmpVDO, dir, dt_Data); //DirVDO.PC.ToString()
                    break;

                case "EmployeeCheckVDOPartTime":
                    try
                    {
                        dt = Employee_Class.GetEmployeeCheckVDOByDept(EmployeeClass.EmployeeDept(""
                        , GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"),
                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), "", "2"), pPathEmpVDO, dir, dt_Data); // DirVDO.VDO.ToString()
                    }
                    catch (Exception)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ค้นหา เช็ควันที่ต้องการอีกครั้ง.");
                    }

                    break;
                case "EmployeeCheckVDOOT":
                    try
                    {
                        dt = Employee_Class.GetEmployeeCheckVDOByDept(EmployeeClass.EmployeeDept(""
                        , GetBranchSelect(RadDropDownList_Branch.SelectedValue.ToString()), radDateTimePicker_Beg.Value.ToString("yyyy-MM-dd"),
                        radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), "", "3"), pPathEmpVDO, dir, dt_Data); // DirVDO.VDO.ToString()
                    }
                    catch (Exception)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ค้นหา เช็ควันที่ต้องการอีกครั้ง.");
                    }
                    break;
            }

            RadGridView_ShowHD.DataSource = dt;
            dt.AcceptChanges();
            if (dt.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีพนักงานเข้างานในวันที่กำหนด.");
        }
        private string GetBranchSelect(string branchid = "", string strBranch = "")
        {
            if (_pTypeReport.Equals("EmployeeCheckVDO") && _pTypeOpen.Equals("SHOP"))
            {
                strBranch += string.Format(@" = '{0}' ", SystemClass.SystemBranchID);
            }
            else
            {
                if (branchid.Equals("") && RadCheckBox_Branch.Checked == false)
                {
                    foreach (DataRow row in Branch.Select("CHECK = '1'"))
                    {
                        if (row["CHECK"].ToString().Equals("1"))
                        {
                            strBranch += string.Format(@"'{0}',", row["BRANCH_ID"].ToString());
                        }
                    }
                    strBranch = string.Format(@"IN ({0})", strBranch.Substring(0, strBranch.Length - 1));
                }
                else if (!branchid.Equals("") && RadCheckBox_Branch.Checked == true)
                {

                    strBranch += string.Format(@" = '{0}' ", branchid);
                }
                else
                {
                    strBranch = "LIKE 'MN%'";
                }
            }
            return strBranch;
        }
        private void GroupByBranchID()
        {
            this.RadGridView_ShowHD.GroupDescriptors.Clear();
            this.RadGridView_ShowHD.GroupDescriptors.Add(new GridGroupByExpression("BRANCH_NAME as BRANCH_NAME format \"{0}\" Group By BRANCH_NAME"));
        }
        private void AppendNewColumn(DataTable table, string[] colNames)
        {
            colNames.ForEach(colName => table.Columns.Add(new DataColumn(colName, typeof(string))));
            colNames.ForEach(colNameGrid => RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight(colNameGrid
                , string.Format(@"วันที่ {0}-{1}-{2}", colNameGrid.Substring(4, 2), colNameGrid.Substring(6, 2), colNameGrid.Substring(8, 4)), 120)));
        }
        private string[] GetColumnsDate()
        {
            int count = (radDateTimePicker_End.Value.Date - radDateTimePicker_Beg.Value.Date).Days + 1;
            EmployeeDate date = new EmployeeDate()
            {
                DATES = "DATE",
                DATETIME = radDateTimePicker_Beg.Value,
                LASTDATES = DateTime.DaysInMonth(radDateTimePicker_Beg.Value.Year, radDateTimePicker_Beg.Value.Month)
            };

            string[] columnName = new string[count];
            for (int i = 0; i < count; i++)
            {
                if (int.Parse(date.DATETIME.AddDays(i).Date.ToString("dd")) <= date.LASTDATES)
                {
                    date.DATETIMES = date.DATETIME.AddDays(i);
                }
                else
                {
                    date.DATETIME.AddMonths(1);
                    date.DATETIMES = date.DATETIME.AddDays(i);
                }
                columnName[i] = date.DATES + date.DATETIMES.ToString("ddMMyyyy");
            }
            return columnName;
        }
        private void SelectRow(bool boChk)
        {
            foreach (GridViewRowInfo dgv in this.radGridView_Branch.Rows)
            {
                dgv.Cells["CHECK"].Value = boChk;
            }
        }
        private void FitLayoutPanel()
        {
            radGridView_Branch.Visible = false;
            TableLayoutRowStyleCollection styles = this.tableLayoutPanel3.RowStyles;
            TableLayoutColumnStyleCollection stylesc = this.tableLayoutPanel1.ColumnStyles;
            foreach (RowStyle style in styles)
            {
                if (style.SizeType == SizeType.AutoSize)
                {
                    style.SizeType = SizeType.Percent;
                    style.Height = 0;
                }
                else
                {
                    style.SizeType = SizeType.Absolute;
                    style.Height = 40;
                }
            }
            foreach (ColumnStyle style in stylesc)
            {
                if (style.SizeType == SizeType.Absolute)
                {
                    style.Width = 200;
                }
            }
            RadButton_Search.Width = 175;
        }
        private void SetDataGrideHead()
        {

            radDateTimePicker_Beg.Value.AddDays(-7);
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนก", 250));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 170));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("VDOSTATUS", "วีดีโอ", 70));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขาเข้างาน", 150));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้างาน", 150));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMEOUT", "สาขาออกงาน", 200));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 150));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PW_SUMDATE", "วันรวม", 70));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PW_SUMTIME", "เวลารวม", 100));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PW_STATUS", "สถานะ", 70));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CHECKNAME", "ผู้ตรวจ", 200));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CHECKDATETIME", "เวลาตรวจ", 150));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddHyperlinkColumn_AddManual("PATHVDO", "PATHVDO", 50));

            dt_Data = Employee_Class.AddNewColumn(new string[] { "DIMENSION", "EMPLID", "EMPLNAME", "DESCRIPTION", "VDOSTATUS", "BRANCH_NAME", "PW_DATETIME_IN", "NAMEOUT",
                    "PW_DATETIME_OUT", "PW_SUMDATE", "PW_SUMTIME", "PW_STATUS", "PW_MNEP_CHECKNAME", "PW_MNEP_CHECKDATETIME", "PATHVDO" });
            SetConditions("VDOSTATUS = '1'", "VDOSTATUS", true, Color.LightSkyBlue, "VDOSTATUS");
            SetConditions("VDOSTATUS = 'ไม่ตรวจตัว'", "VDOSTATUS", true, Color.Red, "VDOSTATUS");
        }
        private void GridViewfilter(GridViewDataColumn ColName, string Value)
        {
            if (Value == "02")
            {
                FilterDescriptor filter = new FilterDescriptor(ColName.Name, FilterOperator.IsEqualTo, "ไม่ตรวจตัวก่อนกลับบ้าน");
                ColName.FilterDescriptor = filter;
            }
            else if (Value == "03")
            {
                FilterDescriptor filter = new FilterDescriptor(ColName.Name, FilterOperator.IsNotEqualTo, "ไม่ตรวจตัวก่อนกลับบ้าน");
                ColName.FilterDescriptor = filter;
            }
            else
            {

                RadGridView_ShowHD.FilterDescriptors.Clear();
            }
        }
        #endregion
    }
}