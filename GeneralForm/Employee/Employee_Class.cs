﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using System.IO;
using System.Linq;
using System.Collections.Generic;
 
namespace PC_Shop24Hrs.GeneralForm.Employee
{
    #region "VarEmployee"
    public class Employee
    {
        public string EMPLID { get; set; } = "";
        public string EMPLNAME { get; set; } = "";
        public string BRANCH_ID { get; set; } = "";
        public string BRANCH_NAME { get; set; } = "";
        public string DIMENSION { get; set; } = "";
        public string DESCRIPTION { get; set; } = "";
        public string PW_DATETIME_IN { get; set; } = "";
        public string PW_DATETIME_OUT { get; set; } = "";
        public string PW_BRANCH_IN { get; set; } = "";
        public string PW_BRANCH_OUT { get; set; } = "";
        public string TIME_IN { get; set; } = "";
        public string TIME_OUT { get; set; } = "";
        public string VDOSTATUS { get; set; } = "0";
        public string SUMTIME { get; set; } = "";
        public string PATHVDO { get; set; } = "";
        public string CREATEVDO { get; set; } = "";
        public string PW_STATUS { get; set; } = "";
        public string SUMDATE { get; set; } = "";
        public string PW_MNEP_CHECKNAME { get; set; } = "";
        public string PW_MNEP_CHECKDATETIME { get; set; } = "";
        public string NAMEOUT { get; set; } = "";
        public int PW_QTYBill { get; set; } = 0;
        public int VDOBILL { get; set; } = 0;
    }
    class EmployeeEvent
    {
        public string EMPDATE1 { get; set; }
        public string EMPTIME1 { get; set; }
        public string EMPDATE2 { get; set; }
        public string EMPTIME2 { get; set; }
    }
    class EmployeeDate
    {
        public string DATES { get; set; }
        public DateTime DATETIME { get; set; }
        public DateTime DATETIMES { get; set; }
        public int LASTDATES { get; set; }
    }
    class EmployeeVDOFomat
    {
        public DateTime DateCreate { get; set; }
        public string FileName { get; set; }
    }
    class LogUse
    {
        public string LOGBRANCH { get; set; }
        public string LOGDATETIME { get; set; }
        public string LOGDATE { get; set; }
        public string LOGMONTH { get; set; }
        public double LOGROW { get; set; }
        public double LOGCOUNT { get; set; }
    }
    class BranchServerIp
    {
        public string BRANCH_ID { get; set; }
        public string BRANCH_SERVER_IP { get; set; }
        public string BRANCH_USERNAME { get; set; }
        public string BRANCH_PASSWORDNAME { get; set; }
    }
    #endregion

    class Employee_Class
    {
        
        #region Employee

        public static DataTable StandardEmployee(string DateBegin, string nameColumn, string branch, string pwType = "1")
        {
            string condition = "";
            if (!branch.Equals("MN000")) condition = $@"AND BRANCH_ID = '{branch}' ";

            string sql = $@"
                SELECT	    BRANCH_ID,BRANCH_NAME, COUNT(BRANCH_ID) AS COUNTPEOPLE
                            , ISNULL(BRANCH_MAXPEOPLE1,'0') AS BRANCH_MAXPEOPLE1
                            , ISNULL(BRANCH_MAXPEOPLE2,'0') AS BRANCH_MAXPEOPLE2, ISNULL(BRANCH_SIZE, 'M') AS BRANCH_SIZE
                            , (BRANCH_SIZE + '-' +SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC) AS SHOW_DESC,SHOW_ID,'' AS PEOPLE
                FROM        SHOP_TIMEKEEPER WITH (NOLOCK) 
                            INNER JOIN SHOP_BRANCH WITH(NOLOCK)  ON SHOP_TIMEKEEPER.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID 
                            LEFT OUTER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH(NOLOCK) ON SHOP_BRANCH.BRANCH_SIZE  = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_NAME AND TYPE_CONFIG = '11'
                WHERE       PW_DATE_IN = '{DateBegin}'  AND PW_STATUS = '1' AND BRANCH_STA = '1' AND PW_TYPE = '{pwType}' {condition} 
                GROUP BY    BRANCH_ID, BRANCH_NAME, BRANCH_MAXPEOPLE1, BRANCH_MAXPEOPLE2, BRANCH_SIZE, SHOW_DESC, SHOW_ID  
                ORDER BY    BRANCH_ID ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row["PEOPLE"] = string.Format(@"{0} / {1}", row["COUNTPEOPLE"], row[nameColumn]);
                }
            }
            return dt;
        }

        public static DataTable EmployeeTimeStamp(string DateBegin, DataTable dt_Branch, string branchid, string pwType = "1")
        {
            DataTable data_Count = new DataTable();
            DataTable data_branch = new DataTable();

            data_branch.Columns.Add("BRANCH_ID");
            data_branch.Columns.Add("BRANCH_NAME");

            data_Count.Columns.Add("BRANCH_ID");
            data_Count.Columns.Add("BRANCH_NAME");
            data_Count.Columns.Add("COUNT_OUT");
            data_Count.Columns.Add("COUNT_IN");
            data_Count.Columns.Add("COUNT_IN12");

            if (!branchid.Equals(""))
            {
                foreach (DataRow row in dt_Branch.Select("BRANCH_ID = '" + branchid + "'"))
                {
                    data_branch.Rows.Add(row["BRANCH_ID"].ToString(), row["BRANCH_NAME"].ToString());
                }
            }
            else
            {
                for (int j = 0; j < dt_Branch.Rows.Count; j++)
                {
                    data_branch.Rows.Add(dt_Branch.Rows[j]["BRANCH_ID"].ToString(), dt_Branch.Rows[j]["BRANCH_NAME"].ToString());
                }
            }

            string sql = $@"
                Select  PW_BRANCH_IN, PW_DATETIME_IN, ISNULL(PW_DATETIME_OUT, '1900-01-01') as PW_DATETIME_OUT
                        ,ISNULL(PW_SUMDATE, '') as PW_SUMDATE,ISNULL(PW_SUMTIME, '') as PW_SUMTIME,PW_STATUS
                FROM    SHOP_TIMEKEEPER WITH(NOLOCK)
                WHERE   PW_DATE_IN = '{DateBegin}'  AND PW_TYPE = '{pwType}'
                ORDER BY  PW_BRANCH_IN ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            for (int i = 0; i < data_branch.Rows.Count; i++)
            {
                double Count_Ount = 0, Count_IN = 0, Count_IN12 = 0;
                string SumTime, SumDay;
                foreach (DataRow row in dt.Select(string.Format(@"PW_BRANCH_IN = '{0}'", data_branch.Rows[i]["BRANCH_ID"].ToString())))
                {
                    if (row["PW_STATUS"].ToString().Equals("0"))
                    {
                        Count_Ount += 1;
                    }
                    else
                    {
                        TimeSpan Span = DateTime.Now.Subtract(Convert.ToDateTime(row["PW_DATETIME_IN"].ToString()));
                        SumDay = Span.Days.ToString();

                        if (Span.Hours.ToString().Length == 1) SumTime = "0" + Span.Hours.ToString();
                        else SumTime = Span.Hours.ToString();
                        if (double.Parse(SumDay) > 0) Count_IN12 += 1;
                        else if (double.Parse(SumTime) > 12) Count_IN12 += 1;
                        else Count_IN += 1;
                    }
                }

                data_Count.Rows.Add(data_branch.Rows[i]["BRANCH_ID"].ToString(), data_branch.Rows[i]["BRANCH_NAME"].ToString(), Count_Ount, Count_IN, Count_IN12);
            }
            return data_Count;
        }
        #endregion

        #region VDO

        public static DataTable EmployeePCA(string DateBegin, string DateEnd, string Branch = "")
        {
            if (!Branch.Equals("")) Branch = $@"AND SHOP_BRANCH.BRANCH_ID {Branch} ";

            string sql = string.Format($@"
                     SELECT	TPC.DESCRIPTION AS [DESCRIPTION],PW_IDCARD,PW_NAME,SHOP_BRANCH.BRANCH_ID AS BRANCH_ID,SHOP_BRANCH.BRANCH_NAME AS BRANCH_NAME
		                    ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_IN,120),'1900-01-01') as PW_DATETIME_IN,   
                            TMP.Branch_ID AS IDOUT,TMP.Branch_Name AS NAMEOUT,ISNULL(CONVERT(VARCHAR,PW_DATETIME_OUT,120),'1900-01-01') as PW_DATETIME_OUT,   
                            ISNULL(CONVERT(VARCHAR,PW_SUMDATE,120),'') as PW_SUMDATE,ISNULL(PW_SUMTIME,'') as PW_SUMTIME,  
		                    PW_STATUS  ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_OUT,23),'1900-01-01') AS DATEOUTT   
                            ,ISNULL(CONVERT(VARCHAR,PW_DATETIME_IN,23),'1900-01-01') AS DATEIN  
                     FROM	SHOP_TIMEKEEPERPC  WITH (NOLOCK) 
                            LEFT OUTER JOIN  SHOP_BRANCH  WITH (NOLOCK)  ON   Shop_TimeKeeperPC.PW_BRANCH_IN = SHOP_BRANCH.BRANCH_ID   
                            LEFT OUTER JOIN 
                                (   SELECT	BRANCH_ID,BRANCH_NAME 
                                    FROM SHOP_BRANCH WITH (NOLOCK) )TMP ON Shop_TimeKeeperPC.PW_BRANCH_OUT  = TMP.BRANCH_ID  INNER JOIN 
							        (select  SHOW_ID as EMPLID,SHOW_NAME as SPC_NAME,
									        SHOP_CONFIGBRANCH_GenaralDetail.SHOW_DESC as DESCRIPTION ,
									        SHOP_CONFIGBRANCH_GenaralDetail.STA
								        from   SHOP_CONFIGBRANCH_GenaralDetail  WITH (NOLOCK) 
								         where TYPE_CONFIG='31'  AND STA = '1') TPC ON  TPC.EMPLID = Shop_TimeKeeperPC.PW_IDCARD 
                     WHERE   PW_DATE_IN BETWEEN '{DateBegin}' AND '{DateEnd}' {Branch}
		             ORDER BY PW_IDCARD,PW_BRANCH_IN,PW_DATETIME_IN ");

            return ConnectionClass.SelectSQL_Main(sql);
        }
        public static DataTable ShiftTime0()
        {
            string sql = $@"
                    SELECT	    PWTIME0, PWHOUR_D, PWTIMEIN1, PWTIMEOUT2  
                                ,CASE    WHEN CAST(PWTIMEOUT2 AS FLOAT) < CAST(PWTIMEIN1 AS FLOAT) THEN 'เลิกงานพรุ่งนี้' ELSE 'เลิกงานวันนี้'  END  AS DDOUT  
                                ,CASE	WHEN CAST(PWTIMEOUT2 AS FLOAT) < CAST(PWTIMEIN1 AS FLOAT) THEN '1' ELSE '0' END  AS STA  
                    FROM	    PWTIME1 WITH(NOLOCK)  
                    WHERE	    (CAST(PWHOUR_D AS FLOAT)) > 0  AND	(PWTIME0 LIKE '[PT][A-Z]%[0-9]%')    
                    GROUP BY    PWTIME0, PWHOUR_D, PWTIMEIN1, PWTIMEOUT2  
                                ,CASE	WHEN CAST(PWTIMEOUT2 AS FLOAT) < CAST(PWTIMEIN1 AS FLOAT) THEN 'เลิกงานพรุ่งนี้' ELSE 'เลิกงานวันนี้'  END   
                                ,CASE	WHEN CAST(PWTIMEOUT2 AS FLOAT) < CAST(PWTIMEIN1 AS FLOAT) THEN '1' ELSE '0' END ";

            return ConnectionClass.SelectSQL_SentServer(sql, Controllers.IpServerConnectClass.ConHROS);
        }
        public static DataTable ShiftTime1(string EmpID, string DateBegin, string DateEnd)
        {
            string sql = $@"
                    SELECT	 PWEMPLOYEE, PWCARD, PWFNAME, PWLNAME
                            ,CASE WHEN ISNULL(DAY1,'') = '' THEN  dbo.SPC_FINDEMPLSHIFTIDBYDATE (PWCARD,'{DateBegin}') ELSE ISNULL(DAY1,'') END AS DATE1  
                            ,CASE WHEN ISNULL(DAY2,'') = '' THEN  dbo.SPC_FINDEMPLSHIFTIDBYDATE (PWCARD,'{DateEnd}') ELSE ISNULL(DAY2,'') END AS DATE2  
                            ,CASE WHEN ISNULL(DAY1,'') = '' THEN  '0' ELSE '1' END AS DATE11  
                            ,CASE WHEN ISNULL(DAY2,'') = '' THEN  '0' ELSE '1' END AS DATE21  
                    FROM	PWEMPLOYEE WITH(NOLOCK)   LEFT OUTER JOIN  
                            (   SELECT	PWADJTIME.PWEMPLOYEE AS PWID,PWDESC AS DAY1 
                                FROM    dbo.PWADJTIME WITH (NOLOCK) INNER JOIN 
                                        dbo.PWEVENT WITH (NOLOCK) ON PWADJTIME.PWEVENT = PWEVENT.PWEVENT  INNER JOIN 
                                        dbo.PWSTOPWORK1 WITH (NOLOCK) ON PWADJTIME.PWADJTIME = PWSTOPWORK1.PWSTOPWORK  
                                WHERE	PWADJTIME.PWEMPLOYEE LIKE '%{EmpID.Substring(1, 7)}' AND PWSTOPWORK1.PWCANCEL = '' AND CONVERT(VARCHAR,PWDATEADJ,23) = '{DateBegin}'  
                            )TMP1 ON PWEMPLOYEE.PWEMPLOYEE = TMP1.PWID  LEFT OUTER JOIN   
                            (SELECT	PWADJTIME.PWEMPLOYEE AS PWID,PWDESC AS DAY2 
                            FROM	dbo.PWADJTIME WITH (NOLOCK) INNER JOIN 
                                    dbo.PWEVENT WITH (NOLOCK) ON PWADJTIME.PWEVENT = PWEVENT.PWEVENT  INNER JOIN 
                                    dbo.PWSTOPWORK1 WITH (NOLOCK) ON PWADJTIME.PWADJTIME = PWSTOPWORK1.PWSTOPWORK  
                            WHERE	PWADJTIME.PWEMPLOYEE LIKE '%{EmpID.Substring(1, 7)}' AND PWSTOPWORK1.PWCANCEL = '' AND CONVERT(VARCHAR,PWDATEADJ,23) = '{DateEnd}'  
                            )TMP2 ON PWEMPLOYEE.PWEMPLOYEE = TMP2.PWID  
                    WHERE	PWCARD IN ('{EmpID.Substring(1, 7)}') ";

            return ConnectionClass.SelectSQL_SentServer(sql, Controllers.IpServerConnectClass.ConHROS);
        }
        public static string FindVDOEmp(string empID, string columnName, string branchID, string status, string PathEmpVDO, string dir, string returnValue = "0")
        {
            DirectoryInfo DirInfo = new DirectoryInfo(string.Format(@"{0}\{1}\{2:yyyy-MM-dd}\{3}"
                        , PathEmpVDO, dir
                        , Convert.ToDateTime(columnName)
                        , branchID));
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(string.Format(@"*_{0}.MP4", empID), SearchOption.AllDirectories);
                if (Files.Length > 0)
                {
                    if (status == "0") returnValue = "1"; else returnValue = Files[0].FullName;
                }
                else
                {
                    //ofline
                    FileInfo[] _Files = DirInfo.GetFiles(string.Format(@"*_{0}.MP4", empID.Substring(1, 7)), SearchOption.AllDirectories);
                    if (_Files.Length > 0)
                    {
                        if (status == "0") returnValue = "1"; else returnValue = _Files[0].FullName;
                    }
                }
            }
            catch (Exception)
            {
                // MessageBox.Show(ex.Message, SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return returnValue;
        }
        public static string FindVDOEmpCar(string empID, string columnName, string branchID, string PathEmpVDO, string dir, string returnValue = "0")
        {
            DirectoryInfo DirInfo = new DirectoryInfo(string.Format(@"{0}\{1}\{2:yyyy-MM-dd}\{3}"
                        , PathEmpVDO, dir
                        , Convert.ToDateTime(columnName)
                        , branchID));
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(string.Format(@"*_{0}.MP4", empID), SearchOption.AllDirectories);
                if (Files.Length > 0)
                {
                    returnValue = "1";
                }
                else
                {
                    //offline
                    FileInfo[] _Files = DirInfo.GetFiles(string.Format(@"*_{0}.MP4", empID.Substring(1, 7)), SearchOption.AllDirectories);
                    if (_Files.Length > 0) returnValue = "1"; else returnValue = "ไม่มีวีดีโอ";
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return returnValue;
        }
        public static List<EmployeeVDOFomat> FindVDOEmp(string empID, string columnName, string branchID, string PathEmpVDO, string dir)
        {
            List<EmployeeVDOFomat> list = new List<EmployeeVDOFomat>();
            if (dir.Equals("PC")) PathEmpVDO = string.Format(@"{0}\{1}\{2:yyyy-MM-dd}\", PathEmpVDO, dir, Convert.ToDateTime(columnName));
            else PathEmpVDO = string.Format(@"{0}\{1}\{2:yyyy-MM-dd}\{3}", PathEmpVDO, dir, Convert.ToDateTime(columnName), branchID);

            DirectoryInfo DirInfo = new DirectoryInfo(PathEmpVDO);
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(string.Format(@"*{0}*_{1}.MP4", branchID, empID), SearchOption.AllDirectories);
                if (Files.Length > 0)
                {
                    foreach (var File in Files)
                    {
                        EmployeeVDOFomat vdo = new EmployeeVDOFomat()
                        {
                            DateCreate = File.CreationTime,
                            FileName = File.FullName
                        };
                        list.Add(vdo);
                    }
                }
            }
            catch (Exception)
            {
                // MessageBox.Show(ex.Message, SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return list;
        }
        static EmployeeEvent FindTMP(string EmpID, string date1, string date2, DataTable dtShift)
        {
            DataTable dtShiftTime = ShiftTime1(EmpID, date1, date2);
            EmployeeEvent employee = new EmployeeEvent();
            if (dtShiftTime.Rows.Count > 0)
            {
                if (dtShiftTime.Rows[0]["DATE11"].ToString().ToUpper().Trim().Equals("0") &&
                    !dtShiftTime.Rows[0]["DATE1"].ToString().Trim().Equals("") &&
                    !dtShiftTime.Rows[0]["DATE1"].ToString().ToUpper().Trim().Equals("N/A"))
                {
                    DataRow[] resulttime = dtShift.Select(string.Format(@"PWTIME0 ='{0}'", dtShiftTime.Rows[0]["DATE1"].ToString().Trim()));
                    if (resulttime.Length > 0)
                    {
                        try
                        {
                            foreach (DataRow row in resulttime)
                            {
                                employee.EMPDATE1 = string.Format(@"{0} - {1} [ {2} ]", row["PWTIMEIN1"].ToString(), row["PWTIMEOUT2"].ToString(), row["DDOUT"].ToString());
                                employee.EMPTIME1 = row["STA"].ToString();
                            }
                        }
                        catch (Exception)
                        {
                            employee.EMPDATE1 = dtShiftTime.Rows[0]["DATE1"].ToString().Trim();
                            employee.EMPTIME1 = "1";
                        }
                    }
                    else
                    {
                        employee.EMPDATE1 = dtShiftTime.Rows[0]["DATE1"].ToString().Trim();
                        employee.EMPTIME1 = "1";
                    }
                }
                else
                {
                    employee.EMPDATE1 = dtShiftTime.Rows[0]["DATE1"].ToString().Trim();
                    employee.EMPTIME1 = "1";
                }

                if (dtShiftTime.Rows[0]["DATE21"].ToString().Trim().ToUpper().Equals("0") &&
                   !dtShiftTime.Rows[0]["DATE2"].ToString().ToUpper().Trim().Equals("") &&
                   !dtShiftTime.Rows[0]["DATE2"].ToString().Trim().Equals("N/A"))
                {

                    DataRow[] resulttime = dtShift.Select(string.Format(@"PWTIME0 ='{0}'", dtShiftTime.Rows[0]["DATE2"].ToString().Trim()));
                    if (resulttime.Length > 0)
                    {
                        try
                        {
                            foreach (DataRow row in resulttime)
                            {
                                employee.EMPDATE2 = string.Format(@"{0} - {1} [ {2} ]", row["PWTIMEIN1"].ToString(), row["PWTIMEOUT2"].ToString(), row["DDOUT"].ToString());
                                employee.EMPTIME2 = row["STA"].ToString();
                            }
                        }
                        catch (Exception)
                        {
                            employee.EMPDATE2 = dtShiftTime.Rows[0]["DATE2"].ToString().Trim();
                            employee.EMPTIME2 = "1";
                        }
                    }
                    else
                    {
                        employee.EMPDATE2 = dtShiftTime.Rows[0]["DATE2"].ToString().Trim();
                        employee.EMPTIME2 = "1";
                    }
                }
                else
                {
                    employee.EMPDATE2 = dtShiftTime.Rows[0]["DATE2"].ToString().Trim();
                    employee.EMPTIME2 = "1";
                }
            }
            else
            {
                employee.EMPDATE1 = "-";
                employee.EMPDATE2 = "-";
                employee.EMPTIME1 = "0";
                employee.EMPTIME2 = "0";
            }
            return employee;
        }
        public static DataTable GetEmployeeCheckVDO(DataTable dtEmployee, DataTable dtShift, DateTime DateBegin, string pPathEmpVDO, string dir, DataTable dt_Data)
        {
            List<Employee> EmployeeTimeOutList = new List<Employee>();
            EmployeeTimeOutList = (from DataRow dr in dtEmployee.Select("PW_DATETIME_OUT <> ''")   //dtEmployee.Rows 
                                   select new Employee()
                                   {
                                       EMPLID = dr["EMPLID"].ToString(),
                                       EMPLNAME = dr["EMPLNAME"].ToString(),
                                       BRANCH_ID = dr["BRANCH_ID"].ToString(),
                                       BRANCH_NAME = dr["BRANCH_NAME"].ToString(),
                                       DIMENSION = dr["DIMENSION"].ToString(),
                                       DESCRIPTION = dr["DESCRIPTION"].ToString(),
                                       PW_DATETIME_IN = dr["PW_DATETIME_IN"].ToString(),
                                       PW_DATETIME_OUT = dr["PW_DATETIME_OUT"].ToString(),
                                       PW_BRANCH_IN = dr["PW_BRANCH_IN"].ToString(),
                                       PW_BRANCH_OUT = dr["PW_BRANCH_OUT"].ToString()
                                   }).ToList();

            for (int i = 0; i < dtEmployee.Rows.Count; i++)
            {
                Employee employee = new Employee();
                List<Employee> newList = EmployeeTimeOutList.Where(m => m.EMPLID == dtEmployee.Rows[i]["EMPLID"].ToString()).ToList();
                if (newList.Count > 0)
                {
                    foreach (var lists in newList)
                    {
                        List<EmployeeVDOFomat> listvdos = FindVDOEmp(lists.EMPLID, lists.PW_DATETIME_OUT, lists.PW_BRANCH_IN, pPathEmpVDO, dir);
                        if (listvdos.Count == 0)
                        {
                            lists.VDOSTATUS = "ไม่ตรวจตัว";
                        }
                        else
                        {
                            lists.VDOSTATUS = listvdos.Count.ToString();
                            listvdos.ForEach(v =>
                            {
                                lists.PATHVDO = v.FileName;
                                lists.CREATEVDO = v.DateCreate.ToString("yyyy-MM-dd HH:mm:ss");
                                TimeSpan span = Convert.ToDateTime(lists.CREATEVDO).Subtract(Convert.ToDateTime(lists.PW_DATETIME_OUT));
                                lists.SUMTIME = span.ToString(@"hh\:mm\:ss");
                            });
                        }
                        employee = new Employee()
                        {
                            EMPLID = lists.EMPLID,
                            EMPLNAME = lists.EMPLNAME,
                            BRANCH_ID = lists.BRANCH_ID,
                            BRANCH_NAME = lists.BRANCH_NAME,
                            DIMENSION = lists.DIMENSION,
                            DESCRIPTION = lists.DESCRIPTION,
                            VDOSTATUS = lists.VDOSTATUS,
                            PW_DATETIME_IN = lists.PW_DATETIME_IN,
                            PW_DATETIME_OUT = lists.PW_DATETIME_OUT,
                            SUMTIME = lists.SUMTIME,
                            CREATEVDO = lists.CREATEVDO,
                            PATHVDO = lists.PATHVDO
                        };
                    }
                }
                else
                {
                    EmployeeEvent EmpEvent = FindTMP(dtEmployee.Rows[i]["EMPLID"].ToString(), DateBegin.AddDays(-1).ToString("yyyy-MM-dd"), DateBegin.ToString("yyyy-MM-dd"), dtShift);
                    employee.EMPLID = dtEmployee.Rows[i]["EMPLID"].ToString();
                    employee.EMPLNAME = dtEmployee.Rows[i]["EMPLNAME"].ToString();
                    employee.BRANCH_ID = dtEmployee.Rows[i]["BRANCH_ID"].ToString();
                    employee.BRANCH_NAME = dtEmployee.Rows[i]["BRANCH_NAME"].ToString();
                    employee.DIMENSION = dtEmployee.Rows[i]["DIMENSION"].ToString();
                    employee.DESCRIPTION = dtEmployee.Rows[i]["DESCRIPTION"].ToString();
                    employee.VDOSTATUS = dtEmployee.Rows[i]["PW"].ToString();
                    employee.PW_DATETIME_IN = EmpEvent.EMPDATE1;
                    employee.PW_DATETIME_OUT = EmpEvent.EMPDATE2;
                    employee.TIME_IN = EmpEvent.EMPTIME1;
                    employee.TIME_OUT = EmpEvent.EMPTIME2;
                    employee.SUMTIME = "";
                    employee.CREATEVDO = "";
                    employee.PATHVDO = "";
                }
                if (!(employee.PW_DATETIME_IN.Equals("N/A") && employee.PW_DATETIME_OUT.Equals("N/A")))
                {
                    dt_Data.Rows.Add(employee.EMPLID,
                        employee.EMPLNAME,
                        employee.BRANCH_ID,
                        employee.BRANCH_NAME,
                        employee.DIMENSION,
                        employee.DESCRIPTION,
                        employee.VDOSTATUS,
                        employee.PW_DATETIME_IN,
                        employee.PW_DATETIME_OUT,
                        employee.TIME_IN,
                        employee.TIME_OUT,
                        employee.CREATEVDO,
                        employee.SUMTIME,
                        employee.PATHVDO);
                }
            }
            return dt_Data;
        }
        public static DataTable GetEmployeeCheckVDOSum(DataTable dtEmployee, DataTable dtEmployeeTimekeep, string _pPathEmpVDO, string _dir, string[] columns, DataTable _dt_Data)
        {
            List<Employee> EmployeeList = (from DataRow dr in dtEmployee.Rows   //dtEmployee.Rows 
                                           select new Employee()
                                           {
                                               EMPLID = dr["EMPLID"].ToString(),
                                               EMPLNAME = dr["EMPLNAME"].ToString(),
                                               BRANCH_ID = dr["BRANCH_ID"].ToString(),
                                               BRANCH_NAME = dr["BRANCHNAME"].ToString(),
                                               DIMENSION = dr["DIMENSION"].ToString(),
                                               DESCRIPTION = dr["DESCRIPTION"].ToString()
                                           }).ToList();

            foreach (var Emp in EmployeeList)
            {
                DataRow dr = _dt_Data.NewRow();
                dr["EMPLID"] = Emp.EMPLID;
                dr["EMPLNAME"] = Emp.EMPLNAME;
                dr["BRANCH_ID"] = Emp.BRANCH_ID;
                dr["BRANCH_NAME"] = Emp.BRANCH_NAME;
                dr["DESCRIPTION"] = Emp.DESCRIPTION;

                List<Employee> EmployeeTimekeepList = (from DataRow d in dtEmployeeTimekeep.Select("EMPLID = '" + Emp.EMPLID + "'")   //dtEmployee.Rows 
                                                       select new Employee()
                                                       {
                                                           EMPLID = d["EMPLID"].ToString(),
                                                           EMPLNAME = d["EMPLNAME"].ToString(),
                                                           BRANCH_ID = d["BRANCH_ID"].ToString(),
                                                           DESCRIPTION = d["DESCRIPTION"].ToString(),
                                                           PW_DATETIME_IN = d["PW_DATETIME_IN"].ToString(),
                                                           PW_DATETIME_OUT = d["PW_DATETIME_OUT"].ToString(),
                                                           PW_BRANCH_IN = d["PW_BRANCH_IN"].ToString(),
                                                           PW_BRANCH_OUT = d["PW_BRANCH_OUT"].ToString()
                                                       }).ToList();
                foreach (var c in columns)
                {
                    DateTime dateTime = Convert.ToDateTime(string.Format(@"{0}-{1}-{2}", c.Substring(8, 4), c.Substring(6, 2), c.Substring(4, 2)));
                    foreach (var timekeep in EmployeeTimekeepList)
                    {
                        dr[c] = FindVDOEmp(Emp.EMPLID, dateTime.ToString("yyyy-MM-dd"), timekeep.PW_BRANCH_OUT, "0", _pPathEmpVDO, _dir);
                    }
                }
                _dt_Data.Rows.Add(dr);
            }
            return _dt_Data;
        }
        public static DataTable GetEmployeeCheckVDOByDept(DataTable dtEmployee, string pPathEmpVDO, string dir, DataTable dt_Data)
        {
            List<Employee> EmployeeTimeOutList = new List<Employee>();
            EmployeeTimeOutList = (from DataRow dr in dtEmployee.Select("PW_STATUS = '0'")   //dtEmployee.Rows 
                                   select new Employee()
                                   {
                                       EMPLID = dr["PW_IDCARD"].ToString(),
                                       EMPLNAME = dr["PW_NAME"].ToString(),
                                       BRANCH_ID = dr["BRANCH_ID"].ToString(),
                                       BRANCH_NAME = dr["BRANCH_ID"].ToString() + " - " + dr["BRANCH_NAME"].ToString(),
                                       DIMENSION = dr["DIMENSIONS"].ToString() + " - " + dr["DIMENSIONSNAME"].ToString(),
                                       DESCRIPTION = dr["DESCRIPTION"].ToString(),
                                       PW_DATETIME_IN = dr["PW_DATETIME_IN"].ToString(),
                                       PW_DATETIME_OUT = dr["PW_DATETIME_OUT"].ToString(),
                                       PW_MNEP_CHECKNAME = dr["PW_MNEP_CHECKNAME"].ToString(),
                                       PW_MNEP_CHECKDATETIME = dr["PW_MNEP_CHECKDATE"].ToString() + " " + dr["PW_MNEP_CHECKTIME"].ToString(),
                                       SUMTIME = dr["PW_SUMTIME"].ToString(),
                                       SUMDATE = dr["PW_SUMDATE"].ToString(),
                                       NAMEOUT = dr["IDOUT"].ToString() + " - " + dr["NAMEOUT"].ToString(),
                                       TIME_OUT = dr["IDOUT"].ToString(),
                                       PW_STATUS = "ไม่อยู่"
                                   }).ToList();

            for (int i = 0; i < dtEmployee.Rows.Count; i++)
            {
                Employee employee = new Employee();
                List<Employee> newList = EmployeeTimeOutList.Where(m => m.EMPLID == dtEmployee.Rows[i]["PW_IDCARD"].ToString() && m.PW_DATETIME_IN == dtEmployee.Rows[i]["PW_DATETIME_IN"].ToString()).ToList();
                if (newList.Count > 0)
                {
                    foreach (var lists in newList)
                    {
                        List<EmployeeVDOFomat> listvdos = FindVDOEmp(lists.EMPLID, lists.PW_DATETIME_OUT, lists.BRANCH_ID, pPathEmpVDO, dir);
                        if (listvdos.Count == 0)
                        {
                            lists.VDOSTATUS = "ไม่ตรวจตัว";
                        }
                        else
                        {
                            foreach (var listvdo in listvdos)
                            {
                                lists.VDOSTATUS = "1";
                                lists.PATHVDO = listvdo.FileName;
                                lists.CREATEVDO = listvdo.DateCreate.ToString("yyyy-MM-dd HH:mm:ss");
                            }
                        }
                        employee = new Employee()
                        {
                            EMPLID = lists.EMPLID,
                            EMPLNAME = lists.EMPLNAME,
                            BRANCH_ID = lists.BRANCH_ID,
                            BRANCH_NAME = lists.BRANCH_NAME,
                            DIMENSION = lists.DIMENSION,
                            DESCRIPTION = lists.DESCRIPTION,
                            VDOSTATUS = lists.VDOSTATUS,
                            PW_DATETIME_IN = lists.PW_DATETIME_IN,
                            PW_DATETIME_OUT = lists.PW_DATETIME_OUT,
                            PW_MNEP_CHECKNAME = lists.PW_MNEP_CHECKNAME,
                            PW_MNEP_CHECKDATETIME = lists.PW_MNEP_CHECKDATETIME,
                            PW_STATUS = lists.PW_STATUS,
                            SUMTIME = lists.SUMTIME,
                            SUMDATE = lists.SUMDATE,
                            NAMEOUT = lists.NAMEOUT,
                            PATHVDO = lists.PATHVDO
                        };
                    }
                }
                else
                {
                    TimeSpan span = DateTime.Now.Subtract(Convert.ToDateTime(dtEmployee.Rows[i]["PW_DATETIME_IN"].ToString()));
                    employee.EMPLID = dtEmployee.Rows[i]["PW_IDCARD"].ToString();
                    employee.EMPLNAME = dtEmployee.Rows[i]["PW_NAME"].ToString();
                    employee.BRANCH_ID = dtEmployee.Rows[i]["BRANCH_ID"].ToString();
                    employee.BRANCH_NAME = dtEmployee.Rows[i]["BRANCH_ID"].ToString() + " - " + dtEmployee.Rows[i]["BRANCH_NAME"].ToString();
                    employee.DIMENSION = dtEmployee.Rows[i]["DIMENSIONS"].ToString() + " - " + dtEmployee.Rows[i]["DIMENSIONSNAME"].ToString();
                    employee.DESCRIPTION = dtEmployee.Rows[i]["DESCRIPTION"].ToString();
                    employee.PW_DATETIME_IN = dtEmployee.Rows[i]["PW_DATETIME_IN"].ToString();
                    employee.PW_DATETIME_OUT = dtEmployee.Rows[i]["PW_DATETIME_OUT"].ToString();
                    employee.SUMTIME = span.ToString(@"hh\:mm");
                    employee.SUMDATE = span.Days.ToString(@"0");
                    employee.NAMEOUT = dtEmployee.Rows[i]["IDOUT"].ToString() + " - " + dtEmployee.Rows[i]["NAMEOUT"].ToString();
                    employee.PW_STATUS = "อยู่";
                    employee.VDOSTATUS = "0";
                    employee.PW_MNEP_CHECKNAME = dtEmployee.Rows[i]["PW_MNEP_CHECKNAME"].ToString();
                    employee.PW_MNEP_CHECKDATETIME = dtEmployee.Rows[i]["PW_MNEP_CHECKDATE"].ToString() + " " + dtEmployee.Rows[i]["PW_MNEP_CHECKTIME"].ToString();
                }

                dt_Data.Rows.Add(employee.DIMENSION,
                       employee.EMPLID,
                       employee.EMPLNAME,
                       employee.DESCRIPTION,
                       employee.VDOSTATUS,
                       employee.BRANCH_NAME,
                       employee.PW_DATETIME_IN,
                       employee.NAMEOUT,
                       employee.PW_DATETIME_OUT,
                       employee.SUMDATE,
                       employee.SUMTIME,
                       employee.PW_STATUS,
                       employee.PW_MNEP_CHECKNAME,
                       employee.PW_MNEP_CHECKDATETIME,
                       employee.PATHVDO);
            }
            return dt_Data;
        }
        public static DataTable GetEmployeeCheckVDOBill(DataTable dtEmployee, string pPathEmpVDO, string dir, DataTable dt_Data)
        {
            List<Employee> List = new List<Employee>();
            List = (from DataRow dr in dtEmployee.Select("PW_QTYBill <> '0'")
                    select new Employee()
                    {
                        EMPLID = dr["EMPLID"].ToString(),
                        EMPLNAME = dr["EMPLNAME"].ToString(),
                        BRANCH_ID = dr["BRANCH_ID"].ToString(),
                        BRANCH_NAME = dr["BRANCH_NAME"].ToString(),
                        DIMENSION = dr["BRANCH_ID"].ToString(),
                        DESCRIPTION = dr["DESCRIPTION"].ToString(),
                        PW_QTYBill = int.Parse(dr["PW_QTYBill"].ToString()),
                        PW_DATETIME_IN = dr["PW_DATETIME_IN"].ToString(),
                        PW_DATETIME_OUT = dr["PW_DATETIME_OUT"].ToString(),
                        PW_BRANCH_IN = dr["PW_BRANCH_IN"].ToString(),
                        PW_BRANCH_OUT = dr["PW_BRANCH_OUT"].ToString()
                    }).ToList();

            List<EmployeeVDOFomat> listVDO = new List<EmployeeVDOFomat>();
            foreach (var Emp in List)
            {
                List<EmployeeVDOFomat> initialList = new List<EmployeeVDOFomat>();
                if (Convert.ToDateTime(Emp.PW_DATETIME_IN).ToShortDateString() == Convert.ToDateTime(Emp.PW_DATETIME_OUT).ToShortDateString())
                {
                    listVDO = FindVDOEmp(Emp.EMPLID, Emp.PW_DATETIME_IN, Emp.PW_BRANCH_OUT, pPathEmpVDO, dir);
                    initialList.AddRange(listVDO);
                }
                else
                {

                    listVDO = FindVDOEmp(Emp.EMPLID, Emp.PW_DATETIME_IN, Emp.PW_BRANCH_OUT, pPathEmpVDO, dir);
                    if (listVDO.Count > 0) initialList.AddRange(listVDO);

                    listVDO = FindVDOEmp(Emp.EMPLID, Emp.PW_DATETIME_OUT, Emp.PW_BRANCH_OUT, pPathEmpVDO, dir);
                    if (listVDO.Count > 0) initialList.AddRange(listVDO);
                }

                foreach (var lists in initialList)
                {
                    if (lists.DateCreate >= Convert.ToDateTime(Emp.PW_DATETIME_IN) && lists.DateCreate <= Convert.ToDateTime(Emp.PW_DATETIME_OUT)) Emp.VDOBILL += 1;
                }
            }

            DataRow[] rowemp = dtEmployee.Select("PW_DATETIME_IN <> ''", "BRANCH_ID ASC");
            foreach (DataRow row in rowemp)
            {
                Employee employee = new Employee();
                List<Employee> newList = List.Where(m => m.EMPLID == row["EMPLID"].ToString() && m.PW_DATETIME_IN == row["PW_DATETIME_IN"].ToString())
                    .Select(m => new Employee
                    {
                        EMPLID = m.EMPLID,
                        EMPLNAME = m.EMPLNAME,
                        BRANCH_ID = m.BRANCH_ID,
                        BRANCH_NAME = m.BRANCH_NAME,
                        DESCRIPTION = m.DESCRIPTION,
                        VDOSTATUS = m.PW_QTYBill + " | " + m.VDOBILL,
                        PW_QTYBill = m.PW_QTYBill,
                        VDOBILL = m.VDOBILL,
                        PW_DATETIME_IN = m.PW_DATETIME_IN,
                        PW_DATETIME_OUT = m.PW_DATETIME_OUT,
                        PW_BRANCH_OUT = m.PW_BRANCH_OUT
                    }).ToList();

                if (newList.Count > 0)
                {
                    foreach (var lists in newList)
                    {
                        employee = new Employee()
                        {
                            EMPLID = lists.EMPLID,
                            EMPLNAME = lists.EMPLNAME,
                            BRANCH_ID = lists.BRANCH_ID,
                            BRANCH_NAME = lists.BRANCH_NAME,
                            DESCRIPTION = lists.DESCRIPTION,
                            VDOSTATUS = lists.VDOSTATUS,
                            PW_QTYBill = lists.PW_QTYBill,
                            VDOBILL = lists.VDOBILL,
                            PW_DATETIME_IN = lists.PW_DATETIME_IN,
                            PW_DATETIME_OUT = lists.PW_DATETIME_OUT,
                            PW_BRANCH_OUT = lists.PW_BRANCH_OUT
                        };
                    }
                }
                else
                {
                    employee.EMPLID = row["EMPLID"].ToString();
                    employee.EMPLNAME = row["EMPLNAME"].ToString();
                    employee.BRANCH_ID = row["BRANCH_ID"].ToString();
                    employee.BRANCH_NAME = row["BRANCH_NAME"].ToString();
                    employee.DESCRIPTION = row["DESCRIPTION"].ToString();
                    employee.PW_DATETIME_IN = row["PW_DATETIME_IN"].ToString();
                    employee.PW_DATETIME_OUT = row["PW_DATETIME_OUT"].ToString();
                    employee.PW_BRANCH_OUT = row["PW_BRANCH_OUT"].ToString();
                }

                dt_Data.Rows.Add(employee.EMPLID,
                    employee.EMPLNAME,
                    employee.BRANCH_ID,
                    employee.BRANCH_NAME,
                    employee.DESCRIPTION,
                    employee.VDOSTATUS,
                    employee.PW_QTYBill,
                    employee.VDOBILL,
                    employee.PW_DATETIME_IN,
                    employee.PW_DATETIME_OUT,
                    employee.PW_BRANCH_OUT);
            }

            return dt_Data;
        }
        public static DataTable GetEmployeeCheckVDOMotorcycle(DataTable dtEmployee, DataTable dtEmployeeTimekeep, string _pPathEmpVDO, string _dir, string[] columns, DataTable dt_Data)
        {
            List<Employee> List = (from DataRow dr in dtEmployee.Rows   //dtEmployee 
                                   select new Employee()
                                   {
                                       EMPLID = dr["EMPLID"].ToString(),
                                       EMPLNAME = dr["EMPLNAME"].ToString(),
                                       BRANCH_ID = dr["BRANCH_ID"].ToString(),
                                       BRANCH_NAME = dr["BRANCH_ID"].ToString() + " - " + dr["BRANCH_NAME"].ToString(),
                                       DESCRIPTION = dr["DESCRIPTION"].ToString()
                                   }).ToList();

            foreach (var Emp in List)
            {
                List<Employee> _List = (from DataRow dr in dtEmployeeTimekeep.Select("EMPLID = '" + Emp.EMPLID + "'") //dtEmployeeTimekeep
                                        select new Employee()
                                        {
                                            EMPLID = dr["EMPLID"].ToString(),
                                            EMPLNAME = dr["EMPLNAME"].ToString(),
                                            BRANCH_ID = dr["BRANCH_ID"].ToString(),
                                            BRANCH_NAME = dr["BRANCH_ID"].ToString() + " - " + dr["BRANCH_NAME"].ToString(),
                                            DESCRIPTION = dr["DESCRIPTION"].ToString(),
                                            PW_DATETIME_IN = dr["PW_DATETIME_IN"].ToString(),
                                            PW_DATETIME_OUT = dr["PW_DATETIME_OUT"].ToString(),
                                            PW_BRANCH_IN = dr["PW_BRANCH_IN"].ToString(),
                                            PW_BRANCH_OUT = dr["PW_BRANCH_OUT"].ToString()
                                        }).ToList();

                DataRow r = dt_Data.NewRow();
                r["EMPLID"] = Emp.EMPLID;
                r["EMPLNAME"] = Emp.EMPLNAME;
                r["BRANCH_ID"] = Emp.BRANCH_ID;
                r["BRANCH_NAME"] = Emp.BRANCH_NAME;
                r["DESCRIPTION"] = Emp.DESCRIPTION;

                foreach (var c in columns)
                {
                    DateTime dateTime = Convert.ToDateTime(string.Format(@"{0}-{1}-{2}", c.Substring(8, 4), c.Substring(6, 2), c.Substring(4, 2)));
                    var query = _List.Where(person => person.PW_DATETIME_OUT == dateTime.ToString("yyyy-MM-dd")).ToList();
                    if (query.Count == 0) r[c] = "N/A";
                    else r[c] = FindVDOEmpCar(Emp.EMPLID, dateTime.ToString("yyyy-MM-dd"), Emp.PW_BRANCH_OUT, _pPathEmpVDO, _dir);
                }
                dt_Data.Rows.Add(r);
            }
            return dt_Data;
        }
        public static DataTable GetEmployeeCheckVDOPCA(DataTable dtEmployee, string pPathEmpVDO, string dir, DataTable dt_Data)
        {
            for (int i = 0; i < dtEmployee.Rows.Count; i++)
            {
                Employee employee = new Employee();
                DataRow[] row = dtEmployee.Select($@"PW_IDCARD = '{dtEmployee.Rows[i]["PW_IDCARD"]}' AND BRANCH_ID = '{dtEmployee.Rows[i]["BRANCH_ID"]}' AND PW_STATUS = '0'");
                if (row.Length > 0)
                {
                    foreach (var rows in row)
                    {
                        List<EmployeeVDOFomat> listvdos = FindVDOEmp(rows["PW_IDCARD"].ToString(), rows["PW_DATETIME_OUT"].ToString(), rows["BRANCH_ID"].ToString(), pPathEmpVDO, dir);
                        if (listvdos.Count == 0) employee.VDOSTATUS = "ไม่ตรวจตัว";
                        else
                        {
                            listvdos.ForEach(v =>
                            {
                                employee.VDOSTATUS = "1";
                                employee.PATHVDO = v.FileName;
                                employee.CREATEVDO = v.DateCreate.ToString("yyyy-MM-dd HH:mm:ss");
                                employee.PW_MNEP_CHECKNAME = v.FileName.Substring(v.FileName.LastIndexOf(@"-") + 2, 7);
                                employee.PW_MNEP_CHECKNAME = EmpName(employee.PW_MNEP_CHECKNAME);
                            });
                        }
                        employee.EMPLID = dtEmployee.Rows[i]["PW_IDCARD"].ToString();
                        employee.EMPLNAME = dtEmployee.Rows[i]["PW_NAME"].ToString();
                        employee.BRANCH_ID = dtEmployee.Rows[i]["BRANCH_ID"].ToString();
                        employee.BRANCH_NAME = dtEmployee.Rows[i]["BRANCH_ID"].ToString() + " - " + dtEmployee.Rows[i]["BRANCH_NAME"].ToString();
                        employee.DESCRIPTION = dtEmployee.Rows[i]["DESCRIPTION"].ToString();
                        employee.PW_DATETIME_IN = dtEmployee.Rows[i]["PW_DATETIME_IN"].ToString();
                        employee.PW_DATETIME_OUT = dtEmployee.Rows[i]["PW_DATETIME_OUT"].ToString();
                        employee.SUMTIME = dtEmployee.Rows[i]["PW_SUMTIME"].ToString();
                        employee.SUMDATE = dtEmployee.Rows[i]["PW_SUMDATE"].ToString();
                        employee.NAMEOUT = dtEmployee.Rows[i]["IDOUT"].ToString() + " - " + dtEmployee.Rows[i]["NAMEOUT"].ToString();
                        employee.PW_STATUS = "ไม่อยู่";
                    }
                }
                else
                {
                    TimeSpan span = DateTime.Now.Subtract(Convert.ToDateTime(dtEmployee.Rows[i]["PW_DATETIME_IN"].ToString()));
                    employee.EMPLID = dtEmployee.Rows[i]["PW_IDCARD"].ToString();
                    employee.EMPLNAME = dtEmployee.Rows[i]["PW_NAME"].ToString();
                    employee.BRANCH_ID = dtEmployee.Rows[i]["BRANCH_ID"].ToString();
                    employee.BRANCH_NAME = dtEmployee.Rows[i]["BRANCH_ID"].ToString() + " - " + dtEmployee.Rows[i]["BRANCH_NAME"].ToString();
                    employee.DESCRIPTION = dtEmployee.Rows[i]["DESCRIPTION"].ToString();
                    employee.PW_DATETIME_IN = dtEmployee.Rows[i]["PW_DATETIME_IN"].ToString();
                    employee.PW_DATETIME_OUT = dtEmployee.Rows[i]["PW_DATETIME_OUT"].ToString();
                    employee.SUMTIME = span.ToString(@"hh\:mm");
                    employee.SUMDATE = span.Days.ToString(@"0");
                    employee.NAMEOUT = dtEmployee.Rows[i]["IDOUT"].ToString() + " - " + dtEmployee.Rows[i]["NAMEOUT"].ToString();
                    employee.PW_STATUS = "อยู่";
                    employee.VDOSTATUS = "0";
                }

                dt_Data.Rows.Add(employee.DESCRIPTION,
                       employee.EMPLID,
                       employee.EMPLNAME,
                       employee.DESCRIPTION,
                       employee.VDOSTATUS,
                       employee.BRANCH_NAME,
                       employee.PW_DATETIME_IN,
                       employee.NAMEOUT,
                       employee.PW_DATETIME_OUT,
                       employee.SUMDATE,
                       employee.SUMTIME,
                       employee.PW_STATUS,
                       employee.PW_MNEP_CHECKNAME,
                       employee.PW_MNEP_CHECKDATETIME,
                       employee.PATHVDO);
            }
            return dt_Data;
        }
        #endregion
        public static DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null) return newColumn;

            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];

                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }
            return newColumn;
        }
        private static string EmpName(string EmpID, string EmpName = "")
        {
            DataTable dt = Class.Models.EmplClass.GetEmployeeDetail_ByEmplID(EmpID);
            if (dt.Rows.Count > 0) EmpName = dt.Rows[0]["EMPLID"].ToString() + " " + dt.Rows[0]["SPC_NAME"].ToString();
            return EmpName;
        }
    }
}
