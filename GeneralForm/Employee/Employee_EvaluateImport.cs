﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.Employee
{
    public partial class Employee_EvaluateImport : Telerik.WinControls.UI.RadForm
    {
        //Load
        public Employee_EvaluateImport()
        {
            InitializeComponent();
        }
        //Load
        private void Employee_EvaluateImport_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_MM);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Head);

            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;

            radLabel_Detail.Visible = true;

            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EVALUATE_HEAD", "หัวข้อ", 80));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EVALUATE_HEADDESC", "ชื่อหัวข้อ", 100));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EVALUATE_MONTH", "รอบเดือน", 80));

            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EVALUATE_EMPLID", "รหัสพนักงาน", 80));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RateEmpName", "ชื่อ-นามสกุล", 250));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("EVALUATE_RATE", "ค่าข้อมูล", 80));

            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PWSECTION", "รหัสแผนก", 80));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PWSECTIONNAME", "แผนก", 300));

            radDropDownList_MM.DataSource = DateTimeSettingClass.GetMonthYearByDate(DateTime.Now.AddDays(-180).ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"));
            radDropDownList_MM.DisplayMember = "ThaiMonth";
            radDropDownList_MM.ValueMember = "MMYY";

            RadDropDownList_Head.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("63", "", "ORDER BY SHOW_ID", "1");
            RadDropDownList_Head.DisplayMember = "SHOW_NAME";
            RadDropDownList_Head.ValueMember = "SHOW_ID";

            ClearTxt();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            radBrowseEditor_choose.Enabled = true;
            RadButton_Save.Enabled = false;
            radBrowseEditor_choose.Value = "";
            radLabel_Detail.Text = "";
            radDropDownList_MM.Enabled = true;
            RadDropDownList_Head.Enabled = true;
            if (RadGridView_ShowHD.Rows.Count > 0)
            {
                RadGridView_ShowHD.Rows.Clear();
            }
        }

        //Choose File
        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            //Check ค่าว่างใน File ที่เลือก
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;

            //Check File Excel
            if ((radBrowseEditor_choose.Value.Contains(".xls")) == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"File ที่ระบุไม่ใช่ File Excel เช็คใหม่อีกครั้ง {Environment.NewLine} { radBrowseEditor_choose.Value}");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }
            DataTable DtSet = new DataTable();
            radBrowseEditor_choose.Enabled = false;
            radDropDownList_MM.Enabled = false;
            RadDropDownList_Head.Enabled = false;


            this.Cursor = Cursors.WaitCursor;
            try
            {
                System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
               ($@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{radBrowseEditor_choose.Value}';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                MyConnection.Open();
                string myTableName = MyConnection.GetSchema("Tables").Rows[0]["TABLE_NAME"].ToString();

                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{myTableName}] ", MyConnection);
                MyCommand.TableMappings.Add("Table", "Net-informations.com");
                MyCommand.Fill(DtSet);

                int countError = 0;
                DataTable dtEmp;
                string fEmp, FSecID, FSecName, fWhere;

                if (RadDropDownList_Head.SelectedValue.ToString() == "HH03")
                {
                    RadGridView_ShowHD.Columns["EVALUATE_EMPLID"].HeaderText = "สาขา";
                    RadGridView_ShowHD.Columns["RateEmpName"].HeaderText = "ชื่อสาขา";
                    RadGridView_ShowHD.Columns["PWSECTION"].IsVisible = false;
                    RadGridView_ShowHD.Columns["PWSECTIONNAME"].IsVisible = false;
                    dtEmp = BranchClass.GetBranchAll(" '1','2','3','4' ", " '0','1' ");
                    fWhere = " BRANCH_ID ";
                    fEmp = "BRANCH_NAME"; FSecID = "BRANCH_ID"; FSecName = "BRANCH_NAME";
                }
                else
                {
                    RadGridView_ShowHD.Columns["EVALUATE_EMPLID"].HeaderText = "รหัสพนักงาน";
                    RadGridView_ShowHD.Columns["RateEmpName"].HeaderText = "ชื่อ-นามสกุล";
                    RadGridView_ShowHD.Columns["PWSECTION"].IsVisible = true;
                    RadGridView_ShowHD.Columns["PWSECTIONNAME"].IsVisible = true;
                    dtEmp = Models.EmplClass.RateEmp_EVALUATE("1", "", "", "");
                    fWhere = " PWEMPLOYEE ";
                    fEmp = "RateEmpName"; FSecID = "PWSECTION"; FSecName = "PWSECTIONNAME";
                }

                foreach (DataRow row_Excel in DtSet.Rows)
                {
                    string Emp = row_Excel[0].ToString();
                    double values = Convert.ToDouble(row_Excel[1].ToString());

                    string empName = "", empDpt = "", empDptName = "";
                    if (dtEmp.Rows.Count > 0)
                    {
                        DataRow[] iCheck = dtEmp.Select($@" {fWhere} = '{Emp}' ");
                        if (iCheck.Length > 0)
                        {
                            empName = iCheck[0][$@"{fEmp}"].ToString();
                            empDpt = iCheck[0][$@"{FSecID}"].ToString();
                            empDptName = iCheck[0][$@"{FSecName}"].ToString();
                        }
                        else countError++;
                    }
                    else countError++;

                    RadGridView_ShowHD.Rows.Add(RadDropDownList_Head.SelectedValue.ToString(),
                        RadDropDownList_Head.SelectedItem[0].ToString(), radDropDownList_MM.SelectedValue.ToString(),
                        Emp, empName, values, empDpt, empDptName);
                }

                MyConnection.Close();

                radLabel_Detail.Text = "จำนวนแถว Excel >> " + DtSet.Rows.Count.ToString() + " | จำนวนแถว หน้าจอ >> " + RadGridView_ShowHD.Rows.Count.ToString();

                if (RadGridView_ShowHD.Rows.Count == DtSet.Rows.Count)
                {
                    if (countError != 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"ข้อมูล Import ไม่ถูกต้อง{Environment.NewLine}เช็คข้อมูลรหัสพนักงานอีกครั้ง");
                        RadButton_Save.Enabled = false;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("Import ข้อมูลจาก Excel เข้าเรียบร้อย.");
                        RadButton_Save.Enabled = true;
                        RadButton_Save.Focus();
                    }
                }
                else
                {
                    RadButton_Save.Enabled = false;
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูล หน้าจอ กับ excel มีจำนวนไม่เท่ากัน ลองใหม่อีกครั้ง");
                }


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถดำเนินการ Import Excel ได้ ลองใหม่อีกครั้ง.{ Environment.NewLine}{ex.Message}");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }

        }

        //ยกเลิก
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        //Save To DB
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count < 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกได้ เนื่องจากไม่มีรายการ"); return;
            }

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert(" Import ข้อมูลตามหน้าจอ ") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;
            ArrayList sql24 = new ArrayList();
            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                sql24.Add($@"
                INSERT INTO [dbo].[SHOP_EVALUATE_EMP]
                           ([EVALUATE_HEAD],[EVALUATE_HEADDESC]
                           ,[EVALUATE_MONTH]
                           ,[EVALUATE_EMPLID],[EVALUATE_RATE]
                           ,[EVALUATE_WHOIDINS],[EVALUATE_WHONAMEINS])
                VALUES		('{RadGridView_ShowHD.Rows[i].Cells["EVALUATE_HEAD"].Value}','{RadGridView_ShowHD.Rows[i].Cells["EVALUATE_HEADDESC"].Value}',
                            '{RadGridView_ShowHD.Rows[i].Cells["EVALUATE_MONTH"].Value}',
                            '{RadGridView_ShowHD.Rows[i].Cells["EVALUATE_EMPLID"].Value}','{RadGridView_ShowHD.Rows[i].Cells["EVALUATE_RATE"].Value}',
                            '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}') ");
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_ArrayMain(sql24));
            RadButton_Save.Enabled = false;
            this.Cursor = Cursors.Default;
        }
    }
}
