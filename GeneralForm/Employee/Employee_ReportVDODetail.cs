﻿//CheckOK
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Employee
{
    public partial class Employee_ReportVDODetail : Telerik.WinControls.UI.RadForm
    {
        DataTable dt = new DataTable("dt");
        readonly Employee _employee = new Employee();
        readonly string _path;

        public Employee_ReportVDODetail(Employee employee, string path)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _employee = employee;
            _path = path;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        //load
        private void Employee_ReportVDODetail_Load(object sender, EventArgs e)
        {
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VDOSTATUS", "วันที่", 200));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("EMPLID", "รหัส"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 170));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BRANCH_ID", "BRANCH_ID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEVDO", "เวลา up รูป", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PATHVDO", "PATHVDO"));
            dt = Employee_Class.AddNewColumn(new string[] { "VDOSTATUS", "EMPLID", "EMPLNAME", "BRANCH_ID", "DATEVDO", "PATHVDO" });
            Getdata();

        }
        private void Getdata()
        {

            List<EmployeeVDOFomat> listVDO, initialList = new List<EmployeeVDOFomat>();
            listVDO = Employee_Class.FindVDOEmp(_employee.EMPLID, _employee.PW_DATETIME_OUT, _employee.BRANCH_ID, _path, "Bill");
            initialList.AddRange(listVDO);
            if (!DateTime.Parse(_employee.PW_DATETIME_OUT).ToShortDateString().Equals(DateTime.Parse(_employee.PW_DATETIME_IN).ToShortDateString()))
            {
                listVDO = Employee_Class.FindVDOEmp(_employee.EMPLID, _employee.PW_DATETIME_IN, _employee.BRANCH_ID, _path, "Bill");
                initialList.AddRange(listVDO);
            }
            foreach (var lists in initialList)
            {
                if (lists.DateCreate >= Convert.ToDateTime(_employee.PW_DATETIME_IN) && lists.DateCreate <= Convert.ToDateTime(_employee.PW_DATETIME_OUT))
                {
                    _employee.VDOSTATUS = "เปิด VDO";
                    _employee.PATHVDO = lists.FileName;
                    _employee.CREATEVDO = lists.DateCreate.ToString("yyyy-MM-dd HH:mm:ss");

                    dt.Rows.Add(_employee.VDOSTATUS, _employee.EMPLID, _employee.EMPLNAME, _employee.BRANCH_ID, _employee.CREATEVDO, _employee.PATHVDO);
                }
            }
            radGridView_Show.DataSource = dt;
        }
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                if (e.Value.Equals("เปิด VDO") && e.Column.FieldName == "VDOSTATUS") System.Diagnostics.Process.Start(this.radGridView_Show.CurrentRow.Cells["PATHVDO"].Value.ToString());
            }
            catch (Exception) { }
        }
    }
}
