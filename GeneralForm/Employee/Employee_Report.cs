﻿//CheckOK
using System;
using System.Data;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.Data;
using PC_Shop24Hrs.GeneralForm.TimeKeeper;
using PC_Shop24Hrs.FormShare;

namespace PC_Shop24Hrs.GeneralForm.Employee
{
    public partial class Employee_Report : Telerik.WinControls.UI.RadForm
    {
        #region Fields 

        DataTable dt_Data = new DataTable();
        DataTable dtBranch = new DataTable();
        readonly string _pTypeOpen;
        readonly string _pTypeBranch;
        readonly DateTime _pTypeDate;
        readonly Employee _pTypeReport;
        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        enum Employee
        {
            TimeStamp,          //0
            TimeStampDetail,    //1
            CheckQuantity,      //2
            StandardEmployee,   //3
            TimeBreakDetail,    //4
            ScanInSPC,          //5
            PartTime,           //6
            OT,                 //7
            Rate                //8
        }

        #endregion
        public Employee_Report(string pTypeReport, string pTypeOpen, string pTypeBranch, DateTime pTypeDate)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pTypeReport = (Employee)int.Parse(pTypeReport);
            _pTypeOpen = pTypeOpen;
            _pTypeBranch = pTypeBranch;
            _pTypeDate = pTypeDate;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }

        private void Employee_Report_Load(object sender, EventArgs e)
        {

            this.FormProperty();
            this.SetButton();
            this.SetDropDownList();
            this.CheckUser();
            this.InitializeGrid();
            this.ClearTxt();
            this.CheckTimeStamByBranch();

            if (_pTypeReport == Employee.ScanInSPC || _pTypeReport == Employee.Rate) radDateTimePicker_D1.Value = DateTime.Now.AddDays(-30); else this.GetEmployeeReport();

        }

        #region ROWSDGV
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion
        #region Method
        void FormProperty()
        {
            radStatusStrip1.SizingGrip = false;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Grp.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Branch.Visible = false;
            radCheckBox_Grp.Visible = false;
            radRadioButton_P1.Visible = false;
            radRadioButton_P2.Visible = false;
        }
        void SetButton()
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButton_Search.ButtonElement.ShowBorder = true;
        }
        void SetDropDownList()
        {
            radDropDownList_Grp.Visible = false;
            RadDropDownList_Branch.Visible = false;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);
            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            RadDropDownList_Branch.DataSource = dtBranch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
        }
        void InitializeGrid()
        {
            string[] ColumnCondition;
            switch (_pTypeReport)
            {
                case Employee.CheckQuantity:
                    radDateTimePicker_D2.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "ลงเวลาเข้างาน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 60));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "เข้างานที่", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัส", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อพนักงาน", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("COUNTSCN", "จำนวนครั้งที่แสกน", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIMESCAN", "วันที่", 200));
                    GetGridViewSummary("PW_NAME", GridAggregateFunction.Count);
                    ColumnCondition = new string[] { "PW_IDCARD", "PW_NAME" };
                    this.SetConditions("DATETIMESCAN <> ''", "DATETIMESCAN", true, Color.Salmon, ColumnCondition);
                    radLabel_Detail.Text = "สีส้ม >> มีการแสกนนิ้ว";
                    break;

                case Employee.TimeStamp:
                    radDateTimePicker_D2.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 60));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("COUNT_OUT", "เลิกงานแล้ว", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("COUNT_IN", "อยู่ = 12 ชั่วโมง", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("COUNT_IN12", "อยู่ > 12 ชั่วโมง", 100));

                    GridViewSummaryItem summaryItemOut = new GridViewSummaryItem("COUNT_OUT", "{0}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemIN = new GridViewSummaryItem("COUNT_IN", "{0}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemIN12 = new GridViewSummaryItem("COUNT_IN12", "{0}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(
                        new GridViewSummaryItem[] { summaryItemOut, summaryItemIN, summaryItemIN12 });
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

                    radLabel_Detail.Text = "ดับเบิ้ลคลิกที่สาขา >> ดูรายละเอียด";

                    break;

                case Employee.TimeStampDetail:
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP", "เลขตรวจตัว", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัสพนักงาน", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อ-นามสกุล", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_PhoneMobile", "เบอร์โทร", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้า", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PW_SUMDATE", "รวมวัน", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PW_SUMTIME", "รวมเวลา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_STATUS", "สถานะ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CheckName", "ผู้ตรวจตัว", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CheckDateTime", "วันเวลาตรวจ", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_MNEP_CHECK", "PW_MNEP_CHECK"));
                    GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
                    ColumnCondition = new string[] { "PW_IDCARD", "PW_NAME" };
                    this.SetConditions("PW_STATUS = 'ไม่อยู่' AND PW_MNEP_CHECK = '0'", "PW_STATUS", true, Color.Salmon, ColumnCondition);

                    if (SystemClass.SystemBranchID != "MN000") radLabel_Detail.Text = "DoubleClick รหัสพนักงาน >> ดูวีดีโอ | DoubleClick เลขที่ตรวจตัว >> บันทึกตรวจ";
                    else radLabel_Detail.Text = "รายชื่อพนักงานประจำกะ ที่ลงเวลาเข้างานที่สาขาอยู่ ณ ปัจจุบัน";

                    break;

                case Employee.StandardEmployee:
                    radRadioButton_P1.Visible = true; radRadioButton_P2.Visible = true;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_SIZE", "SIZE", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_DESC", "คำอธิบาย", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTPEOPLE", "จำนวนพนักงานมาทำงาน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("BRANCH_MAXPEOPLE1", "มาตรฐานกะเช้า", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("BRANCH_MAXPEOPLE2", "มาตรฐานกะดึก", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("PEOPLE", "มาตรฐานพนักงาน", 150));

                    GridViewSummaryItem summaryItemEmp = new GridViewSummaryItem("COUNTPEOPLE", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemPeople1 = new GridViewSummaryItem("BRANCH_MAXPEOPLE1", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemPeople2 = new GridViewSummaryItem("BRANCH_MAXPEOPLE2", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemEmp = new GridViewSummaryRowItem(
                        new GridViewSummaryItem[] { summaryItemEmp, summaryItemPeople1, summaryItemPeople2 });
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemEmp);

                    radLabel_Detail.Text = "สีแดง >> น้อยกว่ามาตรฐาน";

                    break;

                case Employee.TimeBreakDetail:
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัสพนักงาน", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อ-นามสกุล", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 230));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "วันเวลาเข้างาน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "วันเวลาออกงาน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATE_IN", "วันที่ลงพัก", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_COUNT", "ครั้งที่", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_TIME_IN", "ลงเวลาพัก:ออก", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_TIME_OUT", "ลงเวลาพัก:เข้า", 170));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_SUMTIME", "เวลาเข้า"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_SUMDATETIME", "รวมเวลา (วัน ชม นาที)", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_REMARK", "หมายเหตุพักเกิน", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ผู้ลงเวลาเข้า", 200));

                    GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
                    ColumnCondition = new string[] { "PW_IDCARD", "PW_NAME" };
                    this.SetConditions("PW_SUMTIME > '00:40'", "PW_SUMTIME", true, Color.Red, ColumnCondition);

                    string[] ColumnConditionOutBreak = new string[] { "PW_TIME_OUT" };
                    this.SetConditions("PW_TIME_OUT = ''", "PW_TIME_OUT", true, Color.Green, ColumnConditionOutBreak);

                    string[] ColumnConditionOut = new string[] { "PW_DATETIME_OUT" };
                    this.SetConditions("PW_DATETIME_OUT = '1900-01-01'", "PW_DATETIME_OUT", true, Color.Red, ColumnConditionOut);

                    radLabel_Detail.Text = "สีแดง รหัส/ชื่อ >> พักเกิน | สีแดง ลงเวลาออก >> ยังไม่ได้ลงเวลาออกงาน | สีเขียว ลงเวลาพัก:เข้า >> อยู่ระหว่างพัก";

                    DataTable table = new DataTable();
                    table.Columns.Add("value");
                    table.Columns.Add("text");

                    table.Rows.Add("01", "ทั้งหมด");
                    table.Rows.Add("02", "เกินเวลา");
                    table.Rows.Add("03", "ไม่เกินเวลา");
                    table.Rows.Add("04", "ไม่ลงพัก");

                    radDropDownList_Grp.DataSource = table;
                    radDropDownList_Grp.ValueMember = "value";
                    radDropDownList_Grp.DisplayMember = "text";
                    radDropDownList_Grp.SelectedIndex = 0;

                    radLabel_Date.Text = "วันที่ลงเวลาเข้างาน";
                    radDateTimePicker_D1.Visible = true;
                    radDateTimePicker_D2.Visible = false;
                    break;

                case Employee.ScanInSPC:
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
                    RadDropDownList_Branch.Visible = false;
                    RadCheckBox_Branch.Visible = false;
                    radLabel_Date.Text = "วันที่ลงเวลาเข้าสาขาใหญ่";
                    radDropDownList_Grp.Visible = false;
                    radCheckBox_Grp.Visible = false;
                    radDateTimePicker_D1.Visible = true;
                    radDateTimePicker_D2.Visible = true;

                    radLabel_Detail.Text = "สีม่วง >> รวมจำนวนครั้งที่มาสาขาใหญ่ | สีฟ้า >> จำนวนครั้งที่มาสาขาใหญ่ในแต่ละวัน";

                    break;
                case Employee.PartTime:
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP", "เลขตรวจตัว", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัสพนักงาน", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อ-นามสกุล", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_PhoneMobile", "เบอร์โทร", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้า", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PW_SUMDATE", "รวมวัน", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PW_SUMTIME", "รวมเวลา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_STATUS", "สถานะ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CheckName", "ผู้ตรวจตัว", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CheckDateTime", "วันเวลาตรวจ", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_MNEP_CHECK", "PW_MNEP_CHECK"));
                    GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
                    ColumnCondition = new string[] { "PW_IDCARD", "PW_NAME" };
                    this.SetConditions("PW_STATUS = 'ไม่อยู่' AND PW_MNEP_CHECK = '0'", "PW_STATUS", true, Color.Salmon, ColumnCondition);

                    if (SystemClass.SystemBranchID != "MN000") radLabel_Detail.Text = "DoubleClick รหัสพนักงาน >> ดูวีดีโอ | DoubleClick เลขที่ตรวจตัว >> บันทึกตรวจ";
                    else radLabel_Detail.Text = "รายชื่อพนักงาน PartTime ที่ลงเวลาเข้างานที่สาขาอยู่ ณ ปัจจุบัน";
                    break;
                case Employee.OT:
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP", "เลขตรวจตัว", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_IDCARD", "รหัสพนักงาน", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_NAME", "ชื่อ-นามสกุล", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ตำแหน่ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_PhoneMobile", "เบอร์โทร", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_IN", "เวลาเข้า", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_DATETIME_OUT", "เวลาออก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PW_SUMDATE", "รวมวัน", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PW_SUMTIME", "รวมเวลา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_STATUS", "สถานะ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CheckName", "ผู้ตรวจตัว", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PW_MNEP_CheckDateTime", "วันเวลาตรวจ", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PW_MNEP_CHECK", "PW_MNEP_CHECK"));
                    GetGridViewSummary("BRANCH_NAME", GridAggregateFunction.Count);
                    ColumnCondition = new string[] { "PW_IDCARD", "PW_NAME" };
                    this.SetConditions("PW_STATUS = 'ไม่อยู่' AND PW_MNEP_CHECK = '0'", "PW_STATUS", true, Color.Salmon, ColumnCondition);

                    if (SystemClass.SystemBranchID != "MN000") radLabel_Detail.Text = "DoubleClick รหัสพนักงาน >> ดูวีดีโอ | DoubleClick เลขที่ตรวจตัว >> บันทึกตรวจ";
                    else radLabel_Detail.Text = "รายชื่อพนักงาน OT วันหยุด ที่ลงเวลาเข้างานที่สาขาอยู่ ณ ปัจจุบัน";
                    break;
                case Employee.Rate:

                    RadCheckBox_Branch.Visible = true;
                    RadDropDownList_Branch.Visible = true;
                    radRadioButton_P1.Visible = true; radRadioButton_P1.Text = "รอบ 7-15"; radRadioButton_P1.IsChecked = true;
                    radRadioButton_P2.Visible = true; radRadioButton_P2.Text = "รอบ 24-31";
                    radCheckBox_Grp.Visible = true; radCheckBox_Grp.Enabled = false; radCheckBox_Grp.Checked = true; radCheckBox_Grp.Text = "ระบุเดือน";
                    radDropDownList_Grp.Visible = true;
                    DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);
                    radLabel_Date.Visible = false; radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    string D1, D2;
                    D1 = DateTime.Now.AddDays(-180).ToString("yyyy-MM-dd");
                    D2 = DateTime.Now.ToString("yyyy-MM-dd");
                    DataTable dtMonth = DateTimeSettingClass.GetMonthYearByDate($@"{D1}", $@"{D2}");
                    radDropDownList_Grp.DataSource = dtMonth;
                    radDropDownList_Grp.DisplayMember = "ThaiMonth";
                    radDropDownList_Grp.ValueMember = "MMYY";


                    break;
            }
        }
        void CheckUser()
        {
            switch (_pTypeOpen)
            {
                case "SUPC":
                    RadCheckBox_Branch.Visible = true;
                    RadDropDownList_Branch.Visible = true;
                    break;
                case "SHOP":
                    RadCheckBox_Branch.Visible = true;
                    RadCheckBox_Branch.Checked = true;
                    RadCheckBox_Branch.ReadOnly = true;
                    RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.Enabled = false;
                    RadDropDownList_Branch.ReadOnly = true;
                    RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID;
                    break;
            }
        }
        void CheckTimeStamByBranch()
        {
            if (_pTypeBranch != "") //เรียกมาจากหน้าสรุป 
            {
                for (int i = 0; i < tableLayoutPanel1.ColumnCount; i++)
                {
                    if (i == 1) { tableLayoutPanel1.ColumnStyles[i].Width = 0; }
                }
                this.Text = "ลงเวลาเข้า-ออกงาน [ละเอียด].";
                RadDropDownList_Branch.SelectedValue = _pTypeBranch;
                RadCheckBox_Branch.Checked = true;
                RadDropDownList_Branch.Enabled = false;
                radDateTimePicker_D1.Value = _pTypeDate;
                radDateTimePicker_D2.Value = _pTypeDate;
                dt_Data = TimeStampDetail(EmployeeClass.EmployeeTimeStampDetail(
                               radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                               string.Format(@" AND PW_BRANCH_IN = '{0}'", RadDropDownList_Branch.SelectedValue)));

                dt_Data.AcceptChanges();
                RadGridView_ShowHD.FilterDescriptors.Clear();
                RadGridView_ShowHD.DataSource = dt_Data;
            }
        }
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            if (RadGridView_ShowHD.MasterTemplate.Rows.Count > 0)
            {
                RadGridView_ShowHD.DataSource = null;

                switch (_pTypeReport)
                {
                    case Employee.TimeStamp:
                        break;
                    case Employee.TimeStampDetail:
                        break;
                    case Employee.CheckQuantity:
                        break;
                    case Employee.StandardEmployee:
                        break;
                    case Employee.TimeBreakDetail:
                        break;
                    case Employee.ScanInSPC:
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                        break;
                    case Employee.Rate:
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                        break;
                    default:
                        break;
                }

            }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        void GetEmployeeReport()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }
            string pwBranch = "";
            switch (_pTypeReport)
            {
                case Employee.CheckQuantity:
                    if (SystemClass.SystemBranchID.Equals("MN000"))
                    {
                        if (RadCheckBox_Branch.Checked == true) pwBranch = $@" AND PW_BRANCH_IN = '{RadDropDownList_Branch.SelectedValue}'";
                    }
                    else pwBranch = $@" AND PW_BRANCH_IN = '{SystemClass.SystemBranchID}' ";
                    dt_Data = EmployeeClass.EmployeeCheckQuantity(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                        radDateTimePicker_D1.Value.AddDays(1).ToString("yyyy-MM-dd"), pwBranch);

                    break;
                case Employee.TimeStamp:
                    if (_pTypeOpen.Equals("SHOP")) pwBranch = SystemClass.SystemBranchID;
                    dt_Data = Employee_Class.EmployeeTimeStamp(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), dtBranch, pwBranch);

                    break;
                case Employee.TimeStampDetail:
                    if (SystemClass.SystemBranchID.Equals("MN000"))
                    {
                        if (RadCheckBox_Branch.Checked == true) pwBranch = $@" AND PW_BRANCH_IN = '{RadDropDownList_Branch.SelectedValue}' ";
                    }
                    else pwBranch = $@" AND PW_BRANCH_IN = '{SystemClass.SystemBranchID}' ";
                    dt_Data = TimeStampDetail(EmployeeClass.EmployeeTimeStampDetail(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pwBranch));
                    break;
                case Employee.StandardEmployee:
                    this.RadGridView_ShowHD.Columns["PEOPLE"].ConditionalFormattingObjectList.Clear();
                    if (radRadioButton_P1.IsChecked == true)
                    {
                        dt_Data = Employee_Class.StandardEmployee(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), "BRANCH_MAXPEOPLE1", SystemClass.SystemBranchID);
                        RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_MAXPEOPLE1"].IsVisible = true;
                        RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_MAXPEOPLE2"].IsVisible = false;

                        this.RadGridView_ShowHD.Columns["PEOPLE"].ConditionalFormattingObjectList.Add(
                         new ExpressionFormattingObject("MyCondition2", "COUNTPEOPLE" + " < " + "BRANCH_MAXPEOPLE1", false)
                         { CellBackColor = ConfigClass.SetColor_Red() });
                    }
                    else
                    {
                        dt_Data = Employee_Class.StandardEmployee(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), "BRANCH_MAXPEOPLE2", SystemClass.SystemBranchID);
                        RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_MAXPEOPLE1"].IsVisible = false;
                        RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_MAXPEOPLE2"].IsVisible = true;

                        this.RadGridView_ShowHD.Columns["PEOPLE"].ConditionalFormattingObjectList.Add(
                          new ExpressionFormattingObject("MyCondition2", "COUNTPEOPLE" + " < " + "BRANCH_MAXPEOPLE2", false)
                          { CellBackColor = ConfigClass.SetColor_Red() });
                    }
                    break;

                case Employee.TimeBreakDetail:
                    if (SystemClass.SystemBranchID.Equals("MN000"))
                    {
                        if (RadCheckBox_Branch.Checked == true) pwBranch = $@" AND PW_BRANCH_IN = '{RadDropDownList_Branch.SelectedValue}' ";
                    }
                    else pwBranch = $@" AND PW_BRANCH_IN = '{SystemClass.SystemBranchID}' ";

                    dt_Data = EmployeeClass.EmployeeTimeBreak(radDateTimePicker_D1.Value, pwBranch);
                    radDropDownList_Grp.Visible = true;
                    break;
                case Employee.PartTime:
                    if (SystemClass.SystemBranchID.Equals("MN000"))
                    {
                        if (RadCheckBox_Branch.Checked == true) pwBranch = $@" AND PW_BRANCH_IN = '{RadDropDownList_Branch.SelectedValue}' ";
                    }
                    else pwBranch = $@" AND PW_BRANCH_IN = '{SystemClass.SystemBranchID}' ";

                    dt_Data = TimeStampDetail(EmployeeClass.EmployeeTimeStampDetail(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pwBranch, "2"));

                    break;
                case Employee.OT:
                    if (SystemClass.SystemBranchID.Equals("MN000"))
                    {
                        if (RadCheckBox_Branch.Checked == true) pwBranch = $@" AND PW_BRANCH_IN = '{RadDropDownList_Branch.SelectedValue}' ";
                    }
                    else pwBranch = $@" AND PW_BRANCH_IN = '{SystemClass.SystemBranchID}' ";

                    dt_Data = TimeStampDetail(EmployeeClass.EmployeeTimeStampDetail(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pwBranch, "3"));

                    break;
                case Employee.ScanInSPC:


                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                        summaryRowItem.Clear();
                        RadGridView_ShowHD.SummaryRowsTop.Clear();
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));

                    double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SumRows", "รวม", 85)));

                    DatagridClass.SetCellBackClolorByExpression("SumRows", "SumRows <> '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                    //เพิ่มคอลัม
                    int i_Column = 0;
                    for (int iDay = 0; iDay < intDay; iDay++)
                    {
                        string headColume = "A" + Convert.ToString(iDay);
                        string pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");

                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headColume, pDate, 85)));

                        this.RadGridView_ShowHD.Columns[headColume].ConditionalFormattingObjectList.Add(
                                  new ExpressionFormattingObject("MyCondition2", headColume + " > 0 ", false)
                                  { CellBackColor = ConfigClass.SetColor_SkyPastel() });

                        GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum)
                        {
                            FormatString = "{0:n2}"
                        };
                        summaryRowItem.Add(summaryItem);

                        i_Column += 1;
                    }

                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    //Data
                    DataTable dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                    DataTable dtData = TimeKeeperClass.ScanInSPC_Detail("1", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));


                    for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
                    {
                        int iC = 0;

                        string branchID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                        string branchNAME = dtBch.Rows[iBch]["BRANCH_NAME"].ToString();

                        this.RadGridView_ShowHD.Rows.Add(branchID, branchNAME, "2");

                        for (int iDay = 0; iDay < intDay; iDay++)
                        {
                            string headColume = "A" + Convert.ToString(iDay);
                            var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");

                            DataRow[] dr = dtData.Select($@" BRANCHID = '{branchID}' AND TRANDATE = '{pDate}' ");

                            int cc = 0;
                            if (dr.Length > 0)
                            {

                                cc = int.Parse(dr[0]["C_COUNT"].ToString());
                                iC += cc;
                            }


                            RadGridView_ShowHD.Rows[iBch].Cells[headColume].Value = cc.ToString();
                        }
                        RadGridView_ShowHD.Rows[iBch].Cells["SumRows"].Value = iC;
                    }
                    this.Cursor = Cursors.Default;
                    break;
                case Employee.Rate:
                    //DataTable dtCoinConfig = new DataTable();
                    string rateRound = "2";
                    string bch = "BRANCH_ID LIKE 'MN%' AND BRANCH_ID NOT LIKE 'MN998'";
                    string rateBch = "";
                    string Month = $@"RateMonth = '{radDropDownList_Grp.SelectedValue}'";

                    DataTable dtPerComAddOn = BranchClass.GetBranchAll(" '1','2','3','4' ", " '0','1' ");

                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.DataSource = null;
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                    }

                    if (radRadioButton_P1.IsChecked == true) rateRound = "1";

                    if (RadCheckBox_Branch.Checked == true)
                    {
                        bch = $@" BRANCH_ID = '{RadDropDownList_Branch.SelectedValue}' ";
                        rateBch = $@" AND RateBch = '{RadDropDownList_Branch.SelectedValue}' ";
                    }

                    radLabel_Detail.Text = "ชื่อ-นามสกุล : สีชมพู >> ยังไม่ได้ประเมิน | สีม่วง >> ช่องที่ไม่น่าจะเป็นต้องสนใจ เพราะข้อมูลอาจจะเข้าไม่ถูกต้อง | สีแดง รวมรายได้ >> รายได้เป็น 0 ";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("RateEmpId", "รหัสพนักงาน"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PWEMPLOYEE", "รหัสพนักงาน", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RateEmpName", "ชื่อ-นามสกุล", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PWSECTION", "รหัสแผนก"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PWSECTIONNAME", "แผนก"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PWDEPTNAME", "ตำแหน่ง", 300));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RateMonth", "รอบเดือน", 80));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SumRate", "คะแนนรวม", 80));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SumBath", "ยอดเงินรวม", 80));

                    //Freeze Column
                    for (int i = 0; i < 5; i++)
                    {
                        this.RadGridView_ShowHD.MasterTemplate.Columns[i].IsPinned = true;
                    }

                    DatagridClass.SetCellBackClolorByExpression("RateEmpName", "RateMonth = '' ", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD);

                    string H13Desc = "", H13ID = "", HH06Desc = "", HH06ID = "";

                    DataTable dtColumn = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("62", "", "ORDER BY SHOW_ID", "1");
                    for (int i = 0; i < dtColumn.Rows.Count; i++)
                    {
                        string headText = $@"{dtColumn.Rows[i]["SHOW_NAME"]} {Environment.NewLine} {dtColumn.Rows[i]["SHOW_ID"]}";
                        if (dtColumn.Rows[i]["SHOW_ID"].ToString() == "H13")
                        {
                            H13ID = dtColumn.Rows[i]["SHOW_ID"].ToString();
                            H13Desc = headText;
                        }
                        else
                        {
                            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(dtColumn.Rows[i]["SHOW_ID"].ToString(), headText, 120)));
                        }
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SumRate", "คะแนนรวม", 80));

                    //เพิ่มการแสดงผลหัวข้อประเมินจากการ Import
                    DataTable dtColumnHH = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("63", "", "ORDER BY SHOW_ID", "1");
                    for (int iHH = 0; iHH < dtColumnHH.Rows.Count; iHH++)
                    {
                        string ID = dtColumnHH.Rows[iHH]["SHOW_ID"].ToString();
                        string headTextHH = $@"{dtColumnHH.Rows[iHH]["SHOW_NAME"]} {Environment.NewLine} {ID}";
                        if (ID == "HH06")
                        {
                            HH06Desc = headTextHH;
                            HH06ID = ID;
                        }
                        else
                        {
                            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(ID, headTextHH, 120)));


                            //if (dtColumnHH.Rows[iHH]["SHOW_DESC"].ToString() == "1")
                            //{
                            //    DatagridClass.SetCellBackClolorByExpression(ID, $@"{ID} <> 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                            //}
                            //else
                            //{
                            //    DatagridClass.SetCellBackClolorByExpression(ID, $@"{ID} = 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                            //}

                            string sing = $@" = ";
                            if (dtColumnHH.Rows[iHH]["SHOW_DESC"].ToString() == "1") sing = $@" <> ";

                            DatagridClass.SetCellBackClolorByExpression(ID, $@"{ID} {sing} 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                        }


                        if (ID == "HH02")
                        {
                            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH1", $@"เงินเพิ่ม{Environment.NewLine}วันทำงาน", 100));
                            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH2", $@"เงินเพิ่ม{Environment.NewLine}ค่าข้าว", 100));
                            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH3", $@"เงินเพิ่ม{Environment.NewLine}ค่ากันดาร", 100));
                            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH_SUM", $@"เงินเพิ่ม{Environment.NewLine}รวม", 130));
                        }
                        if (ID == "HH03")
                        {
                            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SumWork", "เบี้ยขยัน", 130));
                        }


                    }

                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH1", $@"เงินเพิ่ม{Environment.NewLine}วันทำงาน", 100));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH2", $@"เงินเพิ่ม{Environment.NewLine}ค่าข้าว", 100));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH3", $@"เงินเพิ่ม{Environment.NewLine}ค่ากันดาร", 100));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("HHH_SUM", $@"เงินเพิ่ม{Environment.NewLine}รวม", 130));

                    //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SumWork", "เบี้ยขยัน", 130));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SumAll", "รวมรายได้", 130));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(HH06ID, HH06Desc, 120)));
                    DatagridClass.SetCellBackClolorByExpression(HH06ID, $@"{HH06ID} <> 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                    DatagridClass.SetCellBackClolorByExpression("SumAll", $@"SumAll = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    //หมายเหตุ ให้อยู่หลังสุด
                    if (H13ID != "")
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(H13ID, H13Desc, 500)));
                    }

                    dt_Data = Models.EmplClass.RateEmp_EVALUATE("0", bch, Month, rateRound); //ConnectionClass.SelectSQL_Main(sql);
                    RadGridView_ShowHD.DataSource = dt_Data;

                    string sqlCaseElse = $@"
                        SELECT	RateEmpId, RateEmpName, SHOW_ID, SHOW_NAME, RateRound, RateNumber
                        FROM	SHOP_RATEEMP WITH (NOLOCK)
                        WHERE	RateMonth = '{radDropDownList_Grp.SelectedValue}'
		                        {rateBch}
                        ORDER BY RateEmpId,SHOW_ID
                    ";
                    DataTable dtElse = ConnectionClass.SelectSQL_Main(sqlCaseElse);

                    string sqlEvaluate = $@"
                        SELECT	EVALUATE_HEAD,EVALUATE_EMPLID,EVALUATE_RATE
                        FROM	SHOP_EVALUATE_EMP WITH (NOLOCK)
                        WHERE	EVALUATE_MONTH = '{radDropDownList_Grp.SelectedValue}' ";
                    DataTable dtEvaluate = ConnectionClass.SelectSQL_Main(sqlEvaluate);


                    //for (int i = 0; i < dt_Bch.Rows.Count; i++)
                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        double sumRate = 0;//, sumBath = 0;
                        double sumAll = 0;

                        for (int iC = 0; iC < dtColumn.Rows.Count; iC++)
                        {
                            string ShowID = dtColumn.Rows[iC]["SHOW_ID"].ToString();
                            string empID = RadGridView_ShowHD.Rows[i].Cells["RateEmpId"].Value.ToString();
                            DataRow[] dr = dtElse.Select($@" RateEmpId = '{empID}' AND SHOW_ID = '{ShowID}' ");

                            if (ShowID != "H13")
                            {
                                double Rate = 0;
                                if (dr.Length > 0) Rate = double.Parse(dr[0]["RateNumber"].ToString());
                                RadGridView_ShowHD.Rows[i].Cells[ShowID].Value = Rate.ToString("N2");

                                if (dtColumn.Rows[iC]["REMARK"].ToString() == "10") sumRate += Rate;
                                if (dtColumn.Rows[iC]["LOCK"].ToString() == "1") sumAll += Rate;

                            }
                            else
                            {
                                string remark = "";
                                if (dr.Length > 0) remark = dr[0]["RateNumber"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells[ShowID].Value = remark;
                            }
                        }

                        RadGridView_ShowHD.Rows[i].Cells["SumRate"].Value = sumRate.ToString("N2");

                        //CheckFileImport
                        for (int iEvaluate = 0; iEvaluate < dtColumnHH.Rows.Count; iEvaluate++)
                        {
                            string empM = RadGridView_ShowHD.Rows[i].Cells["PWEMPLOYEE"].Value.ToString();
                            string ShowIDE = dtColumnHH.Rows[iEvaluate]["SHOW_ID"].ToString();
                            if (ShowIDE == "HH03") empM = RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString();

                            DataRow[] drE = dtEvaluate.Select($@" EVALUATE_EMPLID = '{empM}' AND EVALUATE_HEAD = '{ShowIDE}' ");
                            double rateEvaluate = 0;
                            if (drE.Length > 0) rateEvaluate = double.Parse(drE[0]["EVALUATE_RATE"].ToString());
                            RadGridView_ShowHD.Rows[i].Cells[ShowIDE].Value = rateEvaluate.ToString("N2");

                            if (dtColumnHH.Rows[iEvaluate]["LOCK"].ToString() == "1")
                            {
                                if ((ShowIDE == "HH05") && (RadGridView_ShowHD.Rows[i].Cells["PWEMPLOYEE"].Value.ToString().Contains("M") == false))
                                {
                                    sumAll += (rateEvaluate * double.Parse(RadGridView_ShowHD.Rows[i].Cells["HH01"].Value.ToString()));
                                }
                                else sumAll += rateEvaluate;
                            }
                        }

                        double hourAddOn = 1.2;
                        if (RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString() == "MN042") hourAddOn = 1;
                        //เงินเพิ่มอื่นๆ 
                        double SumDay = (double.Parse(RadGridView_ShowHD.Rows[i].Cells["HH01"].Value.ToString()) * hourAddOn);
                        double SumHHH1, SumHHH2;

                        if (SumDay > 27) SumHHH1 = SumDay * 50;
                        else if (SumDay < 24) SumHHH1 = 0;
                        else SumHHH1 = SumDay * 30;
                        RadGridView_ShowHD.Rows[i].Cells["HHH1"].Value = SumHHH1.ToString("N2");

                        SumHHH2 = (double.Parse(RadGridView_ShowHD.Rows[i].Cells["HH01"].Value.ToString()) * 35);
                        RadGridView_ShowHD.Rows[i].Cells["HHH2"].Value = SumHHH2.ToString("N2");

                        DataRow[] drCheckComPer = dtPerComAddOn.Select($@" BRANCH_ID = '{RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value}' ");
                        double SumHHH3 = 0;
                        if ((drCheckComPer.Length > 0) && (double.Parse(drCheckComPer[0]["BRANCH_CONFIGCOMMISSION_PerAddON"].ToString()) > 0))
                        {
                            double ratePer = double.Parse(drCheckComPer[0]["BRANCH_CONFIGCOMMISSION_PerAddON"].ToString());
                            if (RadGridView_ShowHD.Rows[i].Cells["PWEMPLOYEE"].Value.ToString().Contains("P"))
                                SumHHH3 = (double.Parse(RadGridView_ShowHD.Rows[i].Cells["HH01"].Value.ToString()) * (ratePer - 20));
                            else SumHHH3 = (double.Parse(RadGridView_ShowHD.Rows[i].Cells["HH01"].Value.ToString()) * ratePer);
                        }
                        RadGridView_ShowHD.Rows[i].Cells["HHH3"].Value = SumHHH3.ToString("N2");

                        double SumAllAddOn = SumHHH1 + SumHHH2 + SumHHH3;
                        RadGridView_ShowHD.Rows[i].Cells["HHH_SUM"].Value = SumAllAddOn.ToString("N2");
                        //**เงินเพิ่ม

                        //เบี้ยขยัน
                        double rate = double.Parse(RadGridView_ShowHD.Rows[i].Cells["SumRate"].Value.ToString());
                        double rateCenter = double.Parse(RadGridView_ShowHD.Rows[i].Cells["HH03"].Value.ToString());
                        double sumWork = ((6000 * rate) / 100) + rateCenter;
                        RadGridView_ShowHD.Rows[i].Cells["SumWork"].Value = sumWork.ToString("N2");
                        //**เบี้ยขยัน

                        RadGridView_ShowHD.Rows[i].Cells["SumAll"].Value = (sumAll + SumAllAddOn + sumWork).ToString("N2");
                    }
                    //RadGridView_ShowHD.DataSource = dt_Bch;drE
                    this.Cursor = Cursors.Default;
                    return;
            }

            if (_pTypeReport != Employee.ScanInSPC)
            {
                RadGridView_ShowHD.FilterDescriptors.Clear();
                RadGridView_ShowHD.DataSource = dt_Data;
                dt_Data.AcceptChanges();

                if (dt_Data.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีพนักงานเข้างานในวันที่กำหนด.");

                this.Cursor = Cursors.Default;
            }

        }
        DataTable TimeStampDetail(DataTable _dt)
        {
            if (_dt.Rows.Count > 0)
            {
                string TimeOut, SumTime, SumDay;
                //อยู่
                foreach (DataRow rows in _dt.Rows)
                {
                    int iRow = _dt.Rows.IndexOf(rows);
                    if (_dt.Rows[iRow]["PW_STATUS"].ToString().Equals("1"))
                    {
                        _dt.Rows[iRow]["PW_STATUS"] = "อยู่";

                        TimeOut = "";
                        TimeSpan Span = DateTime.Now.Subtract(Convert.ToDateTime(_dt.Rows[iRow]["PW_DATETIME_IN"].ToString()));
                        SumTime = Span.Hours.ToString().Length == 1 ? "0" + Span.Hours.ToString() : Span.Hours.ToString();
                        SumTime = Span.Minutes.ToString().Length == 1 ? SumTime + ":0" + Span.Minutes.ToString() : SumTime + ":" + Span.Minutes.ToString();
                        SumDay = Span.Days.ToString();

                        _dt.Rows[iRow]["PW_SUMDATE"] = SumDay;
                        _dt.Rows[iRow]["PW_SUMTIME"] = SumTime;
                        _dt.Rows[iRow]["PW_DATETIME_OUT"] = TimeOut;
                    }
                    else _dt.Rows[iRow]["PW_STATUS"] = "ไม่อยู่";


                    if (_dt.Rows[iRow]["PW_CAR"].ToString().Equals("1")) _dt.Rows[iRow]["PW_CAR"] = "เอารถมา"; else _dt.Rows[iRow]["PW_CAR"] = "ไม่เอารถมา";
                }
            }
            return _dt;
        }
        private void GetGridViewSummary(string Col, GridAggregateFunction gridAggregate)
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem(Col, "ทั้งหมด : {0}", gridAggregate);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
        }
        private void SetConditions(string Expression, string ColName, bool applyToRow, Color color, string[] array)
        {
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject(ColName, Expression, applyToRow)
            {
                CellBackColor = color
            };
            foreach (string str in array)
            {
                this.RadGridView_ShowHD.Columns[str].ConditionalFormattingObjectList.Add(obj1);
            }
        }
        private string SaveDataCheck(string pPWDoc, string pEmpID, string pEmpName)
        {
            string sql = string.Format($@"
            UPDATE SHOP_TIMEKEEPER 
            SET
                PW_MNEP_Check = '1', 
                PW_MNEP_CheckWho = '{pEmpID}',
                PW_MNEP_CheckName = '{pEmpName}',
                PW_MNEP_CheckDate = convert(varchar,getdate(),25),
                PW_MNEP_CheckTime = convert(varchar,getdate(),24)  
            WHERE PW_MNEP = '{pPWDoc}'");
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        #endregion
        #region Event
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (_pTypeReport != Employee.Rate) radDropDownList_Grp.SelectedIndex = 0;
            this.GetEmployeeReport();
        }
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            this.ClearTxt();
        }
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = "";

            if (_pTypeReport == Employee.Rate)
            {
                InputDate frmDate = new InputDate("กำหนดเอกสารที่ต้องการพิมพ์", "บันทึก", "ยกเลิก", "dd-MM-yyyy", "EMP");
                //InputChooseData frm = new InputChooseData("0");
                DialogResult it = frmDate.ShowDialog();
                if (it != DialogResult.Yes) return;
                string select = frmDate.pInputDate;
                DataTable DtHead = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("62", "", "ORDER BY SHOW_ID", "'1'");

                if (select == "ALL-1")
                {
                    T = DatagridClass.ExportExcelGridView($@"รายงานประเมิณพนักงาน_{radDropDownList_Grp.SelectedValue}", RadGridView_ShowHD, "1");
                }
                else
                {
                    for (int Ex = 0; Ex < DtHead.Rows.Count; Ex++)
                    {
                        if (RadGridView_Excel.Columns.Count > 0)
                        {
                            RadGridView_Excel.DataSource = null;
                            RadGridView_Excel.Columns.Clear();
                            RadGridView_Excel.Rows.Clear();
                        }
                        DatagridClass.SetDefaultRadGridView(RadGridView_Excel);

                        if (DtHead.Rows[Ex]["SHOW_ID"].ToString() == select || select == "ALL-2")
                        {
                            string rateBch = "";

                            if (RadCheckBox_Branch.Checked == true) rateBch = $@" AND RateBch = '{RadDropDownList_Branch.SelectedValue}' ";

                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PWEMPLOYEE", "รหัสพนักงาน", 80));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("RateEmpId", "รหัสพนักงาน"));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RateEmpName", "ชื่อ-นามสกุล", 180));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PWSECTION", "รหัสแผนก"));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PWSECTIONNAME", "แผนก"));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PWDEPTNAME", "ตำแหน่ง", 250));
                            RadGridView_Excel.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RateMonth", "รอบเดือน", 150));

                            string headText = $@"{DtHead.Rows[Ex]["SHOW_NAME"]} {Environment.NewLine} {DtHead.Rows[Ex]["SHOW_ID"]}";
                            if (DtHead.Rows[Ex]["SHOW_ID"].ToString() == "H13") RadGridView_Excel.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(DtHead.Rows[Ex]["SHOW_ID"].ToString(), headText, 700)));
                            else RadGridView_Excel.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(DtHead.Rows[Ex]["SHOW_ID"].ToString(), headText, 120)));
                            RadGridView_Excel.DataSource = dt_Data;

                            string sqlCaseElse = $@"
                        SELECT	RateEmpId, RateEmpName, SHOW_ID, SHOW_NAME, RateRound, RateNumber
                        FROM	SHOP_RATEEMP WITH (NOLOCK)
                        WHERE	RateMonth = '{radDropDownList_Grp.SelectedValue}'
		                        {rateBch}
                        ORDER BY RateEmpId,SHOW_ID ";
                            DataTable dtElse = ConnectionClass.SelectSQL_Main(sqlCaseElse);

                            for (int i = 0; i < dt_Data.Rows.Count; i++)
                            {
                                string ShowID = DtHead.Rows[Ex]["SHOW_ID"].ToString();
                                string empID = RadGridView_Excel.Rows[i].Cells["RateEmpId"].Value.ToString();
                                DataRow[] dr = dtElse.Select($@" RateEmpId = '{empID}' AND SHOW_ID = '{ShowID}' ");

                                if (ShowID != "H13")
                                {
                                    double Rate = 0;
                                    if (dr.Length > 0) Rate = double.Parse(dr[0]["RateNumber"].ToString());
                                    RadGridView_Excel.Rows[i].Cells[ShowID].Value = Rate.ToString("N2");
                                }
                                else
                                {
                                    string remark = "";
                                    if (dr.Length > 0) remark = dr[0]["RateNumber"].ToString();
                                    RadGridView_Excel.Rows[i].Cells[ShowID].Value = remark;
                                }
                            }
                            T = DatagridClass.ExportExcelGridView($@"รายงานประเมิณพนักงาน_{DtHead.Rows[Ex]["SHOW_ID"]}_{DtHead.Rows[Ex]["SHOW_NAME"]}", RadGridView_Excel, "1");
                        }
                    }
                }
            }
            else T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            RadDropDownList_Branch.Enabled = RadCheckBox_Branch.CheckState == CheckState.Checked;
        }
        void CheckEmp()
        {
            if (SystemClass.SystemBranchID == "MN000") return;
            DataRow[] rows = dt_Data.Select("PW_MNEP = '" + this.RadGridView_ShowHD.CurrentRow.Cells["PW_MNEP"].Value.ToString() + "' ");
            int iRows = dt_Data.Rows.IndexOf(rows[0]);
            if (SystemClass.SystemBranchID == "MN000" && SystemClass.SystemComMinimart != "1") return;

            if (this.RadGridView_ShowHD.CurrentRow.Cells["PW_MNEP_CHECK"].Value.ToString() == "1") return;

            if (this.RadGridView_ShowHD.CurrentRow.Cells["PW_DATETIME_OUT"].Value.ToString().Equals(""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานยังไม่ลงเวลาออก ไม่สามารถบันทึกตรวจตัวได้.");
                return;
            }

            string EmpID = SystemClass.SystemUserID, EmpName = SystemClass.SystemUserName;

            //if (MessageBox.Show("ยืนยันบันทึกตวรจตัว.", SystemClass.SystemHeadprogram, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันบันทึกตวรจตัว ?.") == DialogResult.Yes)
            {
                if (SystemClass.SystemUserPositionID == "03")
                {
                    ConfirmCheckTimekeep frmConfirm = new ConfirmCheckTimekeep("0");
                    if (frmConfirm.ShowDialog(this) == DialogResult.OK)
                    {
                        EmpID = frmConfirm.EmpID;
                        EmpName = frmConfirm.EmpName;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถบันทึกตรวจตัวได้เนื่องจากไม่มีสิทธิ์ในการบันทึก.[ต้องยืนยันด้วยตำแหน่งผู้จัดการหรือผู้ช่วยเท่านั้น");
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }

                string ReturnExc = SaveDataCheck(this.RadGridView_ShowHD.CurrentRow.Cells["PW_MNEP"].Value.ToString(), EmpID, EmpName);
                if (ReturnExc == "")
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(ReturnExc);
                    dt_Data.Rows[iRows]["PW_MNEP_CHECK"] = "1";
                    dt_Data.Rows[iRows]["PW_MNEP_CHECKWHO"] = EmpID;
                    dt_Data.Rows[iRows]["PW_MNEP_CHECKNAME"] = EmpName;
                    dt_Data.Rows[iRows]["PW_MNEP_CheckDateTime"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    dt_Data.AcceptChanges();
                }
                else
                {
                    MsgBoxClass.MsgBoxShow_SaveStatus(ReturnExc);
                    return;
                }
            }

        }

        void CheckVDO()
        {
            if (this.RadGridView_ShowHD.CurrentRow.Cells["PW_DATETIME_OUT"].Value.ToString().Equals(""))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("พนักงานยังไม่ลงเวลาออก จึงไม่สามารถดู VDO ตรวจตัวได้.");
                return;
            }
            string MP4 = ".MP4";
            string pPathEmpVDO = string.Format(@"\\192.168.100.77\ImageMinimark\CheckEmployee\VDO");
            string pPathEmpVDOName = string.Format(@"{0}{1}", RadGridView_ShowHD.CurrentRow.Cells["PW_IDCARD"].Value.ToString(), MP4);

            DirectoryInfo DirInfo = new DirectoryInfo(string.Format(@"{0}\{1:yyyy-MM-dd}\{2}"
                    , pPathEmpVDO
                    , Convert.ToDateTime(this.RadGridView_ShowHD.CurrentRow.Cells["PW_DATETIME_OUT"].Value)
                    , this.RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString()));
            try
            {
                FileInfo[] Files = DirInfo.GetFiles(string.Format(@"*{0}", pPathEmpVDOName), SearchOption.AllDirectories);
                if (Files.Length > 0)
                {
                    System.Diagnostics.Process.Start(Files[0].FullName);
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีข้อมูลพนักงานถ่าย VDO ที่ต้องการ.");
                }
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error(ex.Message);
            }
        }
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (_pTypeReport)
            {
                case Employee.TimeStampDetail:

                    switch (e.Column.Name)
                    {
                        case "PW_MNEP"://ตรวจตัว
                            CheckEmp();
                            break;
                        case "PW_IDCARD"://ดูวีดีโอ
                            CheckVDO();
                            break;
                    }
                    break;
                case Employee.TimeStamp:
                    switch (e.Column.Name)
                    {
                        case "BRANCH_ID"://ลงเวลาละเอียด
                            Employee_Report report_Employee = new Employee_Report("1", "SUPC"
                                , RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), radDateTimePicker_D1.Value);
                            if (report_Employee.ShowDialog(this) == DialogResult.OK) return;

                            break;
                    }
                    break;
                case Employee.PartTime:

                    switch (e.Column.Name)
                    {
                        case "PW_MNEP"://ตรวจตัว
                            CheckEmp();
                            break;
                        case "PW_IDCARD"://ดูวีดีโอ
                            CheckVDO();
                            break;
                    }
                    break;
                case Employee.OT:

                    switch (e.Column.Name)
                    {
                        case "PW_MNEP"://ตรวจตัว
                            CheckEmp();
                            break;
                        case "PW_IDCARD"://ดูวีดีโอ
                            CheckVDO();
                            break;
                    }
                    break;
            }
        }
        private void RadDropDownList_Grp_SelectedValueChanged(object sender, EventArgs e)
        {
            if (radDropDownList_Grp.SelectedIndex > -1)
            {
                RadGridView_ShowHD.FilterDescriptors.Clear();
                this.GridViewfilter(RadGridView_ShowHD.Columns["PW_SUMDATETIME"], radDropDownList_Grp.SelectedValue.ToString());
            }
        }
        private void GridViewfilter(GridViewDataColumn ColName, string Value)
        {
            FilterDescriptor filter;
            switch (Value)
            {
                case "02":
                    filter = new FilterDescriptor(ColName.Name, FilterOperator.IsGreaterThan, "0:00:40");
                    ColName.FilterDescriptor = filter;
                    break;
                case "03":
                    filter = new FilterDescriptor(ColName.Name, FilterOperator.IsLessThan, "0:00:40");
                    ColName.FilterDescriptor = filter;
                    break;
                case "04":
                    filter = new FilterDescriptor(ColName.Name, FilterOperator.IsNull, null);
                    ColName.FilterDescriptor = filter;
                    break;
                default:
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    break;
            }
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport.ToString());
        }
        #endregion
    }
}