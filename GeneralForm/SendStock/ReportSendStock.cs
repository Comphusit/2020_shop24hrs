﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.SendStock
{
    public partial class ReportSendStock : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTypeOpen;
        string Dept;
        DataTable DtBranch;

        public ReportSendStock(string pTypeOpen)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pTypeOpen = pTypeOpen;
        }

        #region Function
        void ClearDefualt()
        {
            RadGridView_Show.DataSource = null;
            if (RadGridView_Show.RowCount > 0)
            {
                this.RadGridView_Show.Rows.Clear();
                this.RadGridView_Show.SummaryRowsTop.Clear();
            }
            if (RadGridView_Show.ColumnCount > 0)
            {
                this.RadGridView_Show.Columns.Clear();
                this.RadGridView_Show.SummaryRowsTop.Clear();
                this.RadGridView_Show.FilterDescriptors.Clear();
            }
        }

        void SetDropdownlistDept(DataTable DtDimension)
        {
            RadDropDownList_Dept.DataSource = DtDimension;
            RadDropDownList_Dept.DisplayMember = "DeptName";
            RadDropDownList_Dept.ValueMember = "NUM";
        }

        void SetDropdown_Branch(DataTable DtReason)
        {
            RadDropDownList_Branch.DataSource = DtReason;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
        }

        #endregion

        #region ROWSNUMBER 
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_Show_CustomFiltering(object sender, GridViewCustomFilteringEventArgs e)
        {
            try
            {
                e.Visible = Convert.ToDouble(e.Row.Cells["SUM"].Value.ToString()) > 0;
            }
            catch (Exception)
            {
                return;
            }
        }
        #endregion

        #region EVEN 

        private void ReportSendStock_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_Begin, DateTime.Now, DateTime.Now);

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadButton_Search.ButtonElement.ShowBorder = true;

            RadButtonElement_add.ShowBorder = true; RadButtonElement_add.ToolTipText = "เริ่มต้น";
            RadButtonElement_excel.ShowBorder = true; RadButtonElement_excel.ToolTipText = "Expor To Excel";
            RadButtonElement_Pdf.ShowBorder = true; RadButtonElement_Pdf.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            if (_pTypeOpen == "SHOP")
            {
                SetDropdownlistDept(MRTClass.GetDept("D054"));
                DtBranch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Checked = true;
                RadDropDownList_Branch.Enabled = false;
            }
            else
            {
                SetDropdownlistDept(MRTClass.GetDept(SystemClass.SystemDptID));
                DtBranch = BranchClass.GetBranchAll(" '1' ", " '1' ");
                RadCheckBox_Branch.Checked = false;
                RadDropDownList_Branch.Enabled = false;
            }

            SetDropdown_Branch(DtBranch);
            RadioButton_Round2.Checked = true;
        }

        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedValue != null)
            {
                ClearDefualt();

                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ItemBarcode", "บาร์โค๊ด", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 200));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM", "รวม", 100));

                //Freeze Column
                this.RadGridView_Show.MasterTemplate.Columns["ItemBarcode"].IsPinned = true;
                this.RadGridView_Show.MasterTemplate.Columns["SPC_ITEMNAME"].IsPinned = true;
                this.RadGridView_Show.MasterTemplate.Columns["UNITID"].IsPinned = true;
                this.RadGridView_Show.MasterTemplate.Columns["SUM"].IsPinned = true;

                DatagridClass.SetCellBackClolorByExpression("SUM", "SUM <> 0 ", ConfigClass.SetColor_PurplePastel(), RadGridView_Show);

                this.Cursor = Cursors.WaitCursor;
                DataTable Dtitem = ItembarcodeClass.MNOS_GetItems_ByDept(RadDropDownList_Dept.SelectedValue.ToString());
                RadGridView_Show.DataSource = Dtitem;

                if (DtBranch.Rows.Count > 0)
                {
                    string BranchID, BranchName;
                    foreach (DataRow row in DtBranch.Rows)
                    {
                        BranchID = row["BRANCH_ID"].ToString();
                        BranchName = row["BRANCH_ID"].ToString() + Environment.NewLine + row["BRANCH_NAME"].ToString();

                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight(BranchID, BranchName, 80));
                        DatagridClass.SetCellBackClolorByExpression(BranchID, $@"{BranchID} <> '-' ", ConfigClass.SetColor_SkyPastel(), RadGridView_Show);
                    }

                    DataTable DtSendStockDT = OrderToAx.MRTMainClass.GetItemSendStockAll(RadDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), "1");

                    for (int iRow = 0; iRow < RadGridView_Show.Rows.Count; iRow++)
                    {
                        double SUMITEMQTY = 0;
                        for (int iColumn = 4; iColumn < RadGridView_Show.ColumnCount; iColumn++)
                        {
                            DataRow[] results = DtSendStockDT.Select($@"BARCODE = '{RadGridView_Show.Rows[iRow].Cells["ItemBarcode"].Value}' AND BRANCH_ID = '{RadGridView_Show.Columns[iColumn].Name}' ");

                            double Qty = 0;
                            if (results.Length > 0)
                            {
                                Qty = double.Parse(results[0]["QTYSTOCK"].ToString());
                                SUMITEMQTY += Qty;
                            }
                            if (Qty == 0) RadGridView_Show.Rows[iRow].Cells[iColumn].Value = "-"; else RadGridView_Show.Rows[iRow].Cells[iColumn].Value = Qty.ToString("N2");
                        }
                        RadGridView_Show.Rows[iRow].Cells[3].Value = SUMITEMQTY.ToString("N2");
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedValue != null)
            {
                Dept = RadDropDownList_Dept.SelectedValue.ToString();
                switch (Dept)
                {
                    case "D032":
                        RadioButton_Round1.Enabled = false;
                        RadioButton_Round1.Text = "ไม่มี";
                        RadioButton_Round2.Enabled = true;
                        RadioButton_Round2.Text = "05.00-10.00";
                        RadioButton_Round2.Checked = true;
                        break;
                    case "D110":
                        RadioButton_Round2.Enabled = true;
                        RadioButton_Round2.Text = "10.00-12.00";
                        RadioButton_Round1.Visible = false;
                        break;
                    case "D164":
                        RadioButton_Round2.Enabled = true;
                        RadioButton_Round2.Text = "05.00-07.00";
                        RadioButton_Round1.Visible = false;
                        break;
                    default:
                        RadioButton_Round1.Enabled = false;
                        RadioButton_Round1.Text = "ยังไม่ได้ตั้งค่า";
                        RadioButton_Round2.Enabled = false;
                        RadioButton_Round2.Text = "ยังไม่ได้ตั้งค่า";
                        break;
                }

            }
        }


        private void RadButtonElement_Pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearDefualt();
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_Show, "1");

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        #endregion
    }
}