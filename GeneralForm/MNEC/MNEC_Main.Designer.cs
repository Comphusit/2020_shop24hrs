﻿namespace PC_Shop24Hrs.GeneralForm.MNEC
{
    partial class MNEC_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNEC_Main));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadDateTimePicker_1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_D2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radCheckBox_Grp = new Telerik.WinControls.UI.RadCheckBox();
            this.radDropDownList_Grp = new Telerik.WinControls.UI.RadDropDownList();
            this.radRadioButton_P2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_P1 = new Telerik.WinControls.UI.RadRadioButton();
            this.RadCheckBox_1 = new Telerik.WinControls.UI.RadCheckBox();
            this.RadDropDownList_1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_saveMN = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_save = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDocument2 = new System.Drawing.Printing.PrintDocument();
            this.printDialog2 = new System.Windows.Forms.PrintDialog();
            this.printDialog3 = new System.Windows.Forms.PrintDialog();
            this.printDocument3 = new System.Drawing.Printing.PrintDocument();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Grp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Grp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_P2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_P1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 19);
            this.radLabel_Detail.TabIndex = 53;
            this.radLabel_Detail.Text = "<html>สีเขียว &gt;&gt; สร้างแล้ว | สีม่วง &gt;&gt; จัดส่งสินค้า | สีฟ้า &gt;&gt; " +
    "ได้รับแล้ว</html>";
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 605);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowHD_ViewCellFormatting);
            this.RadGridView_ShowHD.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridView_ShowHD_CellBeginEdit);
            this.RadGridView_ShowHD.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellEndEdit);
            this.RadGridView_ShowHD.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellDoubleClick);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_ShowHD_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_ShowHD_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_ShowHD_FilterPopupInitialized);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadDateTimePicker_1);
            this.panel1.Controls.Add(this.radDateTimePicker_D2);
            this.panel1.Controls.Add(this.radCheckBox_Grp);
            this.panel1.Controls.Add(this.radDropDownList_Grp);
            this.panel1.Controls.Add(this.radRadioButton_P2);
            this.panel1.Controls.Add(this.radRadioButton_P1);
            this.panel1.Controls.Add(this.RadCheckBox_1);
            this.panel1.Controls.Add(this.RadDropDownList_1);
            this.panel1.Controls.Add(this.radLabel_Date);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // RadDateTimePicker_1
            // 
            this.RadDateTimePicker_1.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_1.ForeColor = System.Drawing.Color.Blue;
            this.RadDateTimePicker_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_1.Location = new System.Drawing.Point(12, 207);
            this.RadDateTimePicker_1.Name = "RadDateTimePicker_1";
            this.RadDateTimePicker_1.Size = new System.Drawing.Size(174, 21);
            this.RadDateTimePicker_1.TabIndex = 69;
            this.RadDateTimePicker_1.TabStop = false;
            this.RadDateTimePicker_1.Text = "18/05/2023";
            this.RadDateTimePicker_1.Value = new System.DateTime(2023, 5, 18, 0, 0, 0, 0);
            // 
            // radDateTimePicker_D2
            // 
            this.radDateTimePicker_D2.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_D2.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D2.Location = new System.Drawing.Point(11, 234);
            this.radDateTimePicker_D2.Name = "radDateTimePicker_D2";
            this.radDateTimePicker_D2.Size = new System.Drawing.Size(174, 21);
            this.radDateTimePicker_D2.TabIndex = 68;
            this.radDateTimePicker_D2.TabStop = false;
            this.radDateTimePicker_D2.Text = "18/05/2023";
            this.radDateTimePicker_D2.Value = new System.DateTime(2023, 5, 18, 0, 0, 0, 0);
            // 
            // radCheckBox_Grp
            // 
            this.radCheckBox_Grp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Grp.Location = new System.Drawing.Point(11, 129);
            this.radCheckBox_Grp.Name = "radCheckBox_Grp";
            this.radCheckBox_Grp.Size = new System.Drawing.Size(70, 19);
            this.radCheckBox_Grp.TabIndex = 66;
            this.radCheckBox_Grp.Text = "ระบุกลุ่ม";
            // 
            // radDropDownList_Grp
            // 
            this.radDropDownList_Grp.DropDownAnimationEnabled = false;
            this.radDropDownList_Grp.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_Grp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Grp.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Grp.Location = new System.Drawing.Point(10, 154);
            this.radDropDownList_Grp.Name = "radDropDownList_Grp";
            this.radDropDownList_Grp.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_Grp.TabIndex = 65;
            // 
            // radRadioButton_P2
            // 
            this.radRadioButton_P2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_P2.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_P2.Location = new System.Drawing.Point(113, 100);
            this.radRadioButton_P2.Name = "radRadioButton_P2";
            this.radRadioButton_P2.Size = new System.Drawing.Size(46, 19);
            this.radRadioButton_P2.TabIndex = 64;
            this.radRadioButton_P2.TabStop = false;
            this.radRadioButton_P2.Text = "สรุป";
            this.radRadioButton_P2.CheckStateChanged += new System.EventHandler(this.RadRadioButton_P2_CheckStateChanged);
            // 
            // radRadioButton_P1
            // 
            this.radRadioButton_P1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_P1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_P1.ForeColor = System.Drawing.Color.Blue;
            this.radRadioButton_P1.Location = new System.Drawing.Point(21, 100);
            this.radRadioButton_P1.Name = "radRadioButton_P1";
            this.radRadioButton_P1.Size = new System.Drawing.Size(90, 19);
            this.radRadioButton_P1.TabIndex = 63;
            this.radRadioButton_P1.Text = "รายละเอียด";
            this.radRadioButton_P1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // RadCheckBox_1
            // 
            this.RadCheckBox_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_1.Location = new System.Drawing.Point(11, 44);
            this.RadCheckBox_1.Name = "RadCheckBox_1";
            this.RadCheckBox_1.Size = new System.Drawing.Size(76, 19);
            this.RadCheckBox_1.TabIndex = 62;
            this.RadCheckBox_1.Text = "ระบุสาขา";
            this.RadCheckBox_1.CheckStateChanged += new System.EventHandler(this.RadCheckBox_1_CheckStateChanged);
            // 
            // RadDropDownList_1
            // 
            this.RadDropDownList_1.DropDownAnimationEnabled = false;
            this.RadDropDownList_1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_1.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_1.Location = new System.Drawing.Point(10, 69);
            this.RadDropDownList_1.Name = "RadDropDownList_1";
            this.RadDropDownList_1.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_1.TabIndex = 61;
            this.RadDropDownList_1.Text = "radDropDownList1";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(11, 185);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(55, 19);
            this.radLabel_Date.TabIndex = 60;
            this.radLabel_Date.Text = "ระบุวันที่";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.RadButtonElement_saveMN,
            this.commandBarSeparator1,
            this.RadButtonElement_save,
            this.commandBarSeparator4,
            this.radButtonElement_Print,
            this.commandBarSeparator5,
            this.radButtonElement_excel,
            this.commandBarSeparator6,
            this.RadButtonElement_pdt,
            this.commandBarSeparator2});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 36);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_saveMN
            // 
            this.RadButtonElement_saveMN.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_saveMN.Enabled = false;
            this.RadButtonElement_saveMN.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.RadButtonElement_saveMN.Name = "RadButtonElement_saveMN";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_saveMN, false);
            this.RadButtonElement_saveMN.Text = "RadButtonElement_saveMN";
            this.RadButtonElement_saveMN.Click += new System.EventHandler(this.RadButtonElement_saveMN_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_save
            // 
            this.RadButtonElement_save.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_save.Image = global::PC_Shop24Hrs.Properties.Resources.FileCheck;
            this.RadButtonElement_save.Name = "RadButtonElement_save";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_save, false);
            this.RadButtonElement_save.Text = "RadButtonElement_save";
            this.RadButtonElement_save.Click += new System.EventHandler(this.RadButtonElement_save_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Print
            // 
            this.radButtonElement_Print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_Print.Name = "radButtonElement_Print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Print, false);
            this.radButtonElement_Print.Text = "radButtonElement_Print";
            this.radButtonElement_Print.ToolTipText = "Export To Excel";
            this.radButtonElement_Print.UseCompatibleTextRendering = false;
            this.radButtonElement_Print.Click += new System.EventHandler(this.RadButtonElement_Print_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(11, 264);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 0;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDocument2
            // 
            this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument2_PrintPage);
            // 
            // printDialog2
            // 
            this.printDialog2.UseEXDialog = true;
            // 
            // printDialog3
            // 
            this.printDialog3.UseEXDialog = true;
            // 
            // printDocument3
            // 
            this.printDocument3.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument3_PrintPage);
            // 
            // MNEC_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "MNEC_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายงานต่างๆ แผนกดูแลระบบมินิมาร์ท";
            this.Load += new System.EventHandler(this.MNEC_Main_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Grp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Grp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_P2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_P1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_1;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_P2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_P1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Grp;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Grp;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_saveMN;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private System.Windows.Forms.PrintDialog printDialog2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_save;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private System.Windows.Forms.PrintDialog printDialog3;
        private System.Drawing.Printing.PrintDocument printDocument3;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D2;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_1;
    }
}
