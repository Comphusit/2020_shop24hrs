﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;
using PC_Shop24Hrs.GeneralForm.MNPM;

namespace PC_Shop24Hrs.GeneralForm.MNEC
{
    public partial class MNEC_Main : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        DataTable dtColumn = new DataTable();
        DataTable dtCoinConfig = new DataTable();
        double QtyCell;
        string CarId;
        string RouteId;
        int iRowDT = 0;
        string _pTypeReport;
        string MNECDocNo;

        Double Total { get; set; } = 0;
        Double Bank1000 { get; set; } = 0;
        Double Bank500 { get; set; } = 0;
        Double Bank100 { get; set; } = 0;
        Double Bank50 { get; set; } = 0;
        Double Bank20 { get; set; } = 0;
        Double Coins { get; set; } = 0;
        string PCase { get; set; } = "";
        string Sum { get; set; } = "";

        //22 - รายงานแลกเหรียญ [Center].
        //23 - รายงานรวมยอดเงินแลกเหรียญ.
        //24 - รายงานแลกเหรียญ [จัดส่งสินค้ามินิมาร์ท].

        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        //Load
        public MNEC_Main(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load
        private void MNEC_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 40;
            barcode.LeftMargin = 10;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_Print.ShowBorder = true; radButtonElement_Print.ToolTipText = "พิมพ์บิล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButtonElement_saveMN.ShowBorder = true; RadButtonElement_saveMN.ToolTipText = "บันทึกบิล MNEC";
            RadButtonElement_save.ShowBorder = true; RadButtonElement_save.ToolTipText = "บันทึกบิลให้บัญชี";
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Grp.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadDropDownList_1.Visible = false; RadCheckBox_1.Visible = false;
            radLabel_Detail.Visible = false;
            radRadioButton_P1.Visible = false; radRadioButton_P2.Visible = false;
            radDropDownList_Grp.Visible = false; radCheckBox_Grp.Visible = false;

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_1, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));

            if (SystemClass.SystemBranchID != "MN000")
            {
                DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_1, DateTime.Now, DateTime.Now.AddDays(1));
                RadDateTimePicker_1.MinDate = DateTime.Now.AddDays(-30);
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2));
            }

            switch (_pTypeReport)
            {
                case "22":
                    RadCheckBox_1.Visible = true; RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = false;
                    RadDropDownList_1.Visible = true; RadDropDownList_1.Enabled = true;
                    BranchClass.GetDataRouteSendCoin(RadDropDownList_1);
                    RadButtonElement_saveMN.Enabled = false;
                    RadButtonElement_save.Enabled = false;
                    radButtonElement_Print.Enabled = false;
                    radRadioButton_P1.Visible = true;
                    radRadioButton_P2.Visible = true;
                    radDateTimePicker_D2.Visible = false;
                    radLabel_Detail.Text = "";

                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        radRadioButton_P1.Visible = false;
                        radRadioButton_P2.Visible = false;
                        RadCheckBox_1.Visible = false;
                        RadDropDownList_1.Visible = false;
                        RadGridView_ShowHD.ReadOnly = true;

                        radDateTimePicker_D2.Visible = true;
                        RadButtonElement_save.Enabled = false;
                        RadButtonElement_saveMN.Enabled = false;
                        radButtonElement_Print.Enabled = false;
                        RadButtonElement_save.ToolTipText = "ยืนยันการรับเงินแลกเหรียญ";
                        radLabel_Date.Text = "ระบุวันที่เอกสาร";
                        SetDGV_HD();
                    }
                    break;
                case "23":
                    Report23("0");
                    break;
                case "24":
                    Report24("0");
                    break;
                case "25":
                    Report25("0");
                    break;
                case "26":
                    Report26("0");
                    break;
                default:
                    break;
            }
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;

            switch (_pTypeReport)
            {
                case "22":
                    Report22("0");
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        Report22("1");
                        return;
                    }
                    Report22("1");
                    break;
                case "23":
                    Report23("1");
                    break;
                case "24":
                    Report24("1");
                    break;
                case "25":
                    Report25("1");
                    break;
                case "26":
                    Report26("1");
                    break;
                default:
                    break;
            }

        }

        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }

        //22 - รายงานแลกเหรียญ [Center].
        void Report22(string pType)
        {
            //051001 : เหรียญ 1 บาท, 051002 : เหรียญ 2 บาท, 051005 : เหรียญ 5 บาท
            //051010 : เหรียญ 10 บาท, 051025 : เหรียญ 0.25 บาท, 051050 : เหรียญ 0.50 บาท

            DataTable dt_DataAll = new DataTable();
            if (pType == "0")
            {
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear();

                    RadGridView_ShowHD.DataSource = null;
                    this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                    summaryRowItem.Clear();
                }

                radRadioButton_P1.Visible = true;
                radRadioButton_P2.Visible = true;

                RadButtonElement_saveMN.ShowBorder = true; RadButtonElement_saveMN.ToolTipText = "บันทึกบิลแลกเหรียญ";
                RadButtonElement_saveMN.Enabled = true;
                RadCheckBox_1.Text = "เลือกสายรถ";



                RadCheckBox_1.Visible = true; RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = false;
                RadDropDownList_1.Visible = true;
                RadDropDownList_1.Enabled = true;

                radLabel_Detail.Visible = true;
                radLabel_Date.Text = "ระบุวันที่ส่งเหรียญ";
                dtCoinConfig = ConfigClass.FindData_CONFIGBRANCH_DETAIL("59");
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MNECDateDoc", "วันที่เอกสาร", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECDocno", "เลขที่เอกสาร", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ROUTEID", "สายรถ")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("NAME", "ชื่อสายรถ")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BRANCH_RouteSendCoin", "กลุ่มสายรถ")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNECStaDoc", "สถานะเอกสาร")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNECStaApv", "สาถนะสาขายืนยัน")));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECCarID", "ทะเบียนรถ", 130)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MNECSum", "รวม [บาท]", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("MNECRef", "อ้างอิง"));
                //RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("MNECDateDoc", "วันที่เอกสาร"));


                DatagridClass.SetCellBackClolorByExpression("MNECDocno", $@" MNECStaDoc = '-'", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD); //ยังไม่ได้ออกบิล
                DatagridClass.SetCellBackClolorByExpression("MNECDocno", $@" MNECStaDoc = '0'", ConfigClass.SetColor_Red(), RadGridView_ShowHD); //ยกเลิกเอกสาร
                DatagridClass.SetCellBackClolorByExpression("MNECDocno", $@" MNECStaApv = '0'", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD); //เปิดบิลแล้ว
                DatagridClass.SetCellBackClolorByExpression("MNECDocno", $@" MNECStaApv = '1'", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD); //เปิดบิลแล้ว
                DatagridClass.SetCellBackClolorByExpression("MNECDocno", $@" MNECStaApv = '2'", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD); //ส่งให้บัญชีแล้ว
                DatagridClass.SetCellBackClolorByExpression("MNECDocno", $@" MNECStaApv = '3'", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD); //ยืนยันรับเหรียญ

                DatagridClass.SetCellBackClolorByExpression("MNECCarID", $@" MNECCarID = '-'", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("MNECSum", $@" MNECSum = '0.00'", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                radLabel_Detail.Text = "เลขที่เอกสาร : สีชมพู >> ยังไม่ได้บันทึกบิลแลกเหรียญ | สีเขียว >> บันทึกแลกเหรียญแล้ว | สีฟ้า >> สาขายืนยันแลกเหรียญ | สีม่วง >> บันทึกให้บัญชีแล้ว | สีเหลือง >> สาขายืนยันรับเงินแล้ว";

                RadGridView_ShowHD.Columns["MNECDateDoc"].IsPinned = true;
                RadGridView_ShowHD.Columns["MNECDocno"].IsPinned = true;
                RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;

                dtColumn = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("59", "", "  ORDER BY CONVERT(decimal,REMARK) ", "1");
                for (int i = 0; i < dtColumn.Rows.Count; i++)
                {
                    string headText = dtColumn.Rows[i]["SHOW_NAME"].ToString();
                    string idCoin = dtColumn.Rows[i]["SHOW_ID"].ToString();
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight($@"{idCoin}", $@"{headText}{Environment.NewLine}[เหรียญ]", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight($@"{idCoin}B", $@"รวมบาท", 80)));

                    DatagridClass.SetCellBackClolorByExpression(dtColumn.Rows[i]["SHOW_ID"].ToString(), $@"{dtColumn.Rows[i]["SHOW_ID"]} = 0.00 ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);
                    dt_DataAll.Columns.Add(idCoin);
                    dt_DataAll.Columns.Add($@"{idCoin}B");
                }

                if (SystemClass.SystemBranchID != "MN000")
                {
                    radRadioButton_P1.Visible = false;
                    radRadioButton_P2.Visible = false;

                    RadCheckBox_1.Visible = false;
                    RadDropDownList_1.Visible = false;
                    RadGridView_ShowHD.ReadOnly = true;

                    radDateTimePicker_D2.Visible = true;

                    radLabel_Detail.Text = "เลขที่เอกสาร : สีฟ้า >> ยืนยันแลกเหรียญแล้ว, สีเหลือง >> ยืนยันรับเงินแลกเหรียญ, สีแดง >> เอกสารถูกยกเลิก || DoubleClick (สีเขียว) >> เพื่อยืนยันแลกเหรียญ, DoubleClick (สีฟ้า) >> พิมพ์บิลซ้ำ";
                    RadButtonElement_save.Enabled = false;
                    RadButtonElement_saveMN.Enabled = false;
                    radButtonElement_Print.Enabled = false;
                    RadButtonElement_save.ToolTipText = "ยืนยันการรับเงินแลกเหรียญ";
                    radLabel_Date.Text = "ระบุวันที่เอกสาร";
                }
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;

                RadGridView_ShowHD.ReadOnly = false;
                RadGridView_ShowHD.MasterTemplate.AllowAddNewRow = false;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"MNECDateDoc"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"MNECDocno"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"BRANCH_ID"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"BRANCH_NAME"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"ROUTEID"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"NAME"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"MNECCarID"].ReadOnly = true;
                RadGridView_ShowHD.MasterTemplate.Columns[$@"MNECSum"].ReadOnly = true;

                for (int iReadOnly = 0; iReadOnly < RadGridView_ShowHD.Columns.Count; iReadOnly++)
                {
                    if (RadGridView_ShowHD.Columns[iReadOnly].Name.ToString().Contains("B")) RadGridView_ShowHD.Columns[iReadOnly].ReadOnly = true;
                }

                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                summaryRowItem.Clear();

                GridViewSummaryItem summary051001 = new GridViewSummaryItem("051001", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051002 = new GridViewSummaryItem("051002", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051005 = new GridViewSummaryItem("051005", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051010 = new GridViewSummaryItem("051010", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051025 = new GridViewSummaryItem("051025", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051050 = new GridViewSummaryItem("051050", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryItem summary051001B = new GridViewSummaryItem("051001B", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051002B = new GridViewSummaryItem("051002B", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051005B = new GridViewSummaryItem("051005B", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051010B = new GridViewSummaryItem("051010B", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051025B = new GridViewSummaryItem("051025B", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summary051050B = new GridViewSummaryItem("051050B", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryItem sum = new GridViewSummaryItem("MNECSum", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { sum,summary051001, summary051002, summary051005, summary051010, summary051025, summary051050,
                summary051001B, summary051002B, summary051005B, summary051010B, summary051025B, summary051050B};
                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
                string BCH = $@"AND  BRANCH_RouteSendCoin = '{RadDropDownList_1.SelectedValue}' ";
                string DateDoc = $@" AND MNECDateDoc = '{RadDateTimePicker_1.Value:yyyy-MM-dd}' ";
                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                if (SystemClass.SystemBranchID != "MN000")
                {
                    BCH = $@"AND BRANCH_ID = '{SystemClass.SystemBranchID}' ";
                    DateDoc = $@" AND MNECDateDoc BETWEEN '{RadDateTimePicker_1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
                }

                string sql = $@"
                        SELECT	ISNULL(MNECDocno,'') AS MNECDocno,BRANCH_ID, BRANCH_NAME, ROUNT.ROUTEID AS ROUTEID, ROUTENAME AS NAME, 
		                        BRANCH_RouteSendCoin, ISNULL(MNECStaDoc,'-') AS MNECStaDoc, ISNULL(MNECStaApv,'-') AS MNECStaApv, 
		                        ISNULL(MNECSum,'') AS MNECSum, MNECRef, MNECCarID, CONVERT(NVARCHAR,MNECDateDoc,23) AS MNECDateDoc
                        FROM	SHOP_BRANCH WITH (NOLOCK)
                        INNER JOIN	( {LogisticClass.GetRouteAllByBranch() } ) ROUNT ON SHOP_BRANCH.BRANCH_ID = ROUNT.ACCOUNTNUM
                        LEFT JOIN SHOP_MNEC_HD WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = SHOP_MNEC_HD.MNECBranch {DateDoc}
                        WHERE	BRANCH_STAOPEN = '1' 
                                {BCH}
                                
                    ";
                DataTable dt_Bch = ConnectionClass.SelectSQL_Main(sql);
                RadGridView_ShowHD.DataSource = dt_Bch;

                if (SystemClass.SystemBranchID != "MN000")
                {
                    if (dt_Bch.Rows[0]["MNECDocno"].ToString() == "")
                    {
                        RadGridView_ShowHD.DataSource = null;
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }

                string Docdate = $@" WHERE	MNECDateDoc = '{RadDateTimePicker_1.Value:yyyy-MM-dd}'";
                if (SystemClass.SystemBranchID != "MN000") Docdate = $@" WHERE  MNECDateDoc BETWEEN '{RadDateTimePicker_1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";

                string sqlMNEC = $@"
                SELECT	SHOP_MNEC_HD.MNECDocno, SHOP_MNEC_HD.MNECDateDoc, MNECBarcode AS SHOW_ID, MNECName, MNECQty AS REMARK, MNECUnitID,MNECBranch AS BRANCH_ID,SHOP_CONFIGBRANCH_GenaralDetail.REMARK AS SHOW_NAME 
                FROM	SHOP_MNEC_HD WITH (NOLOCK)  
                        INNER JOIN SHOP_MNEC_DT WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNEC_DT.MNECDocno
                        INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_MNEC_DT.MNECBarcode =  SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND TYPE_CONFIG = '59' AND STA IN (1)  
                {Docdate}";
                DataTable dtMNEC = ConnectionClass.SelectSQL_Main(sqlMNEC);


                for (int i = 0; i < dt_Bch.Rows.Count; i++)
                {
                    for (int iC = 0; iC < dtColumn.Rows.Count; iC++)
                    {
                        string cName = dtColumn.Rows[iC]["SHOW_ID"].ToString();
                        string mnec = RadGridView_ShowHD.Rows[i].Cells["MNECDocno"].Value.ToString();
                        DataRow[] dr;

                        if (mnec == "") dr = dtCoinConfig.Select($@" BRANCH_ID = '{dt_Bch.Rows[i]["BRANCH_ID"]}' AND SHOW_ID = '{cName}' ");
                        else dr = dtMNEC.Select($@" MNECDocno = '{mnec}' AND SHOW_ID = '{cName}' ");

                        double coinValues = 0, coinM = 0;
                        if (dr.Length > 0)
                        {
                            coinValues = double.Parse(dr[0]["REMARK"].ToString());
                            coinM = double.Parse(dr[0]["SHOW_NAME"].ToString());
                        }
                        RadGridView_ShowHD.Rows[i].Cells[cName].Value = coinValues.ToString("N2");
                        RadGridView_ShowHD.Rows[i].Cells[$@"{cName}B"].Value = (coinValues * coinM).ToString("N2");
                    }
                    RadGridView_ShowHD.Rows[i].Cells["MNECSum"].Value = SumQtyAllBranch(i);
                }

                if (RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString() != "")
                {
                    RadButtonElement_saveMN.Enabled = false; RadButtonElement_save.Enabled = true;
                }
                else
                {
                    RadButtonElement_saveMN.Enabled = true; RadButtonElement_save.Enabled = false;
                }

                DataTable Dt = MNECSelect(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), $@"AND MNECBranchRoute = '{RadDropDownList_1.SelectedValue}'", "MN000");
                if (Dt.Rows.Count > 0)
                {
                    RadGridView_ShowHD.ReadOnly = true;
                    RadButtonElement_saveMN.Enabled = false;
                    RadButtonElement_save.Enabled = false;
                    radButtonElement_Print.Enabled = true;
                }

                if (SystemClass.SystemBranchID != "MN000")
                {
                    RadGridView_ShowHD.ReadOnly = true;
                    if (RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString() != "")
                    {
                        RadButtonElement_saveMN.Enabled = false; RadButtonElement_save.Enabled = true;
                    }
                    else
                    {
                        RadButtonElement_saveMN.Enabled = false; RadButtonElement_save.Enabled = false;
                    }
                }

                this.Cursor = Cursors.Default;
            }
        }

        //23 - รายงานสรุปยอดเหรียญ.
        void Report23(string pType)
        {
            if (pType == "0") //Set Column
            {
                //Clear GridView
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear();

                    RadGridView_ShowHD.DataSource = null;
                    this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                    summaryRowItem.Clear();
                }
                //Clear GridView

                RadDateTimePicker_1.Visible = true;
                radDateTimePicker_D2.Visible = true;

                radLabel_Date.Text = "ระบุวันที่เอกสาร";
                RadCheckBox_1.Visible = false; RadCheckBox_1.Visible = false;
                radLabel_Detail.Visible = true; radLabel_Detail.Text = "";
                RadDropDownList_1.Visible = false;
                RadButtonElement_saveMN.Enabled = false; RadButtonElement_save.Enabled = false; radButtonElement_Print.Enabled = false;
            }
            else //Set Row
            {
                this.Cursor = Cursors.WaitCursor;
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear(); RadGridView_ShowHD.DataSource = null;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECDateDoc", "วันที่ส่งเหรียญ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MNECSum", "รวม [บาท]", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("MNECBranchRoute", "สายส่งเหรียญ", 100)));

                string sqlHD = $@"  SELECT    CONVERT(NVARCHAR,MNECDateDoc,23) AS MNECDateDoc, MNECSum, MNECBranchRoute
                                    FROM	SHOP_MNEC_HD WITH (NOLOCK)
                                    WHERE	MNECDateDoc BETWEEN '{RadDateTimePicker_1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
		                                    AND MNECBranch = '{SystemClass.SystemBranchID}' 
                                    ORDER BY MNECDateDoc DESC ";

                DataTable Dt_MNEC = ConnectionClass.SelectSQL_Main(sqlHD);

                if (Dt_MNEC.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูล ลองใหม่อีกครั้ง.");
                    this.Cursor = Cursors.Default;
                    return;
                }

                DataTable dtMNEC = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("59", "", "ORDER BY CONVERT(float,REMARK)", "1");
                //Add Column Coins & Bag
                for (int iC = 0; iC < dtMNEC.Rows.Count; iC++)
                {
                    string headText = dtMNEC.Rows[iC]["SHOW_NAME"].ToString();
                    string conisID = dtMNEC.Rows[iC]["SHOW_ID"].ToString();
                    string CoinsBagName = $@"{headText}{Environment.NewLine}ถุงใหญ่ | ถุงเล็ก";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(conisID, headText, 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight($@"B{conisID}", CoinsBagName, 110)));

                }
                //Add Column Coins & Bag

                RadGridView_ShowHD.DataSource = Dt_MNEC;

                DataTable dtDetail = ConnectionClass.SelectSQL_Main($@"
                    SELECT	CONVERT(NVARCHAR,MNECDateDoc,23) AS MNECDateDoc,MNECQty, MNECBarcode,TMP.MaxImage,MNECQty/CONVERT(decimal,TMP.MaxImage) AS SUMALL,MNECBranchRoute,
		                    (MNECQty/CONVERT(decimal,TMP.MaxImage)/10) AS SUMBAG
                    FROM	SHOP_MNEC_HD WITH (NOLOCK) 
		                    INNER JOIN SHOP_MNEC_DT WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNEC_DT.MNECDocno
		                    INNER JOIN (SELECT	*	FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)  WHERE	TYPE_CONFIG = '59' AND STA = '1' )TMP ON SHOP_MNEC_DT.MNECBarcode = TMP.SHOW_ID
                    WHERE	MNECDateDoc BETWEEN '{RadDateTimePicker_1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
		                    AND MNECBranch = '{SystemClass.SystemBranchID}' 
                    ORDER BY MNECDateDoc DESC ");
                if (dtDetail.Rows.Count == 0) return;

                //Set Coins & Bag to GridView
                for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
                {
                    for (int i = 0; i < dtMNEC.Rows.Count; i++)
                    {
                        DataRow[] dr = dtDetail.Select($@" MNECDateDoc = '{RadGridView_ShowHD.Rows[iR].Cells["MNECDateDoc"].Value}' AND MNECBarcode = '{dtMNEC.Rows[i]["SHOW_ID"]}' 
                                                            AND MNECBranchRoute = '{RadGridView_ShowHD.Rows[iR].Cells["MNECBranchRoute"].Value}' ");

                        double AddData = 0, AddBag = 0;
                        if (dr.Length > 0)
                        {
                            AddData = double.Parse(dr[0]["MNECQty"].ToString());
                            AddBag = double.Parse(dr[0]["SUMBAG"].ToString());

                        }
                        string[] AA = AddBag.ToString().Split('.');
                        RadGridView_ShowHD.Rows[iR].Cells[dtMNEC.Rows[i]["SHOW_ID"].ToString()].Value = AddData.ToString("N2");

                        string bagSmall = AA[0] + " | 0";
                        if (AA.Length > 1) bagSmall = AA[0] + " | " + AA[1];

                        RadGridView_ShowHD.Rows[iR].Cells[$@"B{dtMNEC.Rows[i]["SHOW_ID"]}"].Value = bagSmall;
                    }
                }
                //Set Coins & Bag to GridView

                this.Cursor = Cursors.Default;
            }
        }

        //24 - รายงานแลกเหรียญ [จัดส่งสินค้ามินิมาร์ท].
        void Report24(string pType)
        {
            if (pType == "0")
            {
                DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_1, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));
                DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now.AddDays(2));
                RadDateTimePicker_1.Visible = true;
                radDateTimePicker_D2.Visible = false;

                radLabel_Date.Text = "ระบุวันที่ส่งเหรียญ";
                RadCheckBox_1.Visible = false; RadCheckBox_1.Visible = false;
                radLabel_Detail.Visible = true; radLabel_Detail.Text = "ป้ายทะเบียน || สีแดง >> ยังไม่ระบุเลขป้ายทะเบียน";
                RadDropDownList_1.Visible = false;
                radButtonElement_Print.Enabled = false;

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECDateDoc", "วันที่", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECRouteID", "สายรถ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECRouteName", "ชื่อสายรถ", 350)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MNECCarID", "ทะเบียนรถ", 200));

                DatagridClass.SetCellBackClolorByExpression("MNECCarID", $@" MNECCarID = ''", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                RadGridView_ShowHD.ReadOnly = false;
                RadGridView_ShowHD.MasterTemplate.AllowAddNewRow = false;
                RadGridView_ShowHD.Columns["MNECDateDoc"].ReadOnly = true;
                RadGridView_ShowHD.Columns["MNECRouteID"].ReadOnly = true;
                RadGridView_ShowHD.Columns["MNECRouteName"].ReadOnly = true;
                RadButtonElement_save.Enabled = false;
                RadButtonElement_saveMN.Enabled = false;
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                string sql = $@"SELECT	CONVERT(nvarchar,MNECDateDoc,23) AS MNECDateDoc, MNECRouteID, MNECRouteName, COUNT(MNECRouteID) AS CountRoute, ISNULL(MNECCarID,'') AS MNECCarID
                                FROM	SHOP_MNEC_HD
                                WHERE	MNECDateDoc = '{RadDateTimePicker_1.Value:yyyy-MM-dd}'
		                                AND MNECStaApv = '2'
                                GROUP BY MNECDateDoc, MNECRouteID, MNECRouteName,MNECCarID";

                DataTable DtCar = ConnectionClass.SelectSQL_Main(sql);

                RadGridView_ShowHD.DataSource = DtCar;

                if (DtCar.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูล ลองใหม่อีกครั้ง.");

                this.Cursor = Cursors.Default;
            }
        }

        void Report25(string pType)
        {
            if (pType == "0")
            {
                RadButtonElement_pdt.Enabled = false;
                radButtonElement_Print.Enabled = false;
                RadButtonElement_saveMN.Enabled = false;
                RadButtonElement_save.Enabled = false;
                radLabel_Date.Text = "ระบุวันที่ส่งซองเงิน";
                RadDateTimePicker_1.Value = DateTime.Now; radDateTimePicker_D2.Value = DateTime.Now;
                radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีแดง >> จัดส่งยังไม่ได้รับ[ถ้าบัญชีรับแล้วก็เป็นสีปกติ] | สีฟ้า >> บัญชียังไม่ได้รับ | สีเหลือง ช่อง ยอดรับเงิน/รวม[บาท] >> ยอดเงินที่ส่ง-รับ ไม่เท่ากัน | สีม่วง ช่อง สถานะรับเงิน >> ยังไม่สร้าง SL | สีฟ้า ช่อง สถานะรับเงิน >> การเงินยังไม่บันทึกรับ ";

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SLROUND", "รอบรับชำระเงิน", 120));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("POSTED", $@"ลงรายการ{Environment.NewLine}บัญชี"));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SLId", "ใบรับชำระเงิน", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("STA", "สถานะรับเงิน", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CASHAMOUNT", "ยอดรับเงิน", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("DAYS", $@"จน.วัน{Environment.NewLine}การเงินค้างรับ", 100));

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MNECDateDoc", "วันที่เอกสาร", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECDocno", "เลขที่เอกสาร", 140)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECBranch", "สาขา", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECBranchName", "ชื่อสาขา", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNECBranchRoute", "สายรถ", 50)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUNDSEND", "รอบส่ง", 60)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MNECSum", "รวม [บาท]", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPMDateIn", "เวลาส่งซอง", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNWhoSend", "ผู้ส่งซอง [สาขา]", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StaAccDESC", "สถานะ [บัญชี]", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoAcc", "ผู้รับ [บัญชี]", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateAcc", "เวลารับ [บัญชี]", 150)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StaTransDESC", "สถานะ [จัดส่ง]", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoTrans", "ผู้รับ [จัดส่ง]", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateTrans", "เวลารับ [จัดส่ง]", 150)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STATUSFINANCE", "STATUSFINANCE")));

                DatagridClass.SetCellBackClolorByExpression("STA", "STA = 'ยังไม่สร้าง SL' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("STA", "STA = 'เปิดค้างไว้' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                DatagridClass.SetCellBackClolorByExpression("MNECDocno", "StaTransDESC = 'ไม่ได้รับ' AND StaAccDESC = 'ไม่ได้รับ' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("MNECDocno", "StaAccDESC = 'ไม่ได้รับ' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                DatagridClass.SetCellBackClolorByExpression("CASHAMOUNT", $@" CASHAMOUNT <> MNECSum AND CASHAMOUNT > 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("MNECSum", $@" CASHAMOUNT <> MNECSum AND CASHAMOUNT > 0  ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);

                GridViewSummaryItem summaryItem = new GridViewSummaryItem("MNECSum", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemA1201 = new GridViewSummaryItem("CASHAMOUNT", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem, summaryItemA1201 };

                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;

                //string sql = $@"
                //SELECT	CONVERT(NVARCHAR,MNECDateDoc,23) AS MNECDateDoc, MNECDocno, MNECBranch, MNECBranchName, MNECBranchRoute, MNECSum,
                //  ISNULL(MNPMLO,'') AS LOGISTICID, ISNULL(MNPMCar,'') AS VEHICLEID,
                //  ISNULL(CONVERT(NVARCHAR,MNPMDateIn,23) + ' ' + MNPMTimeIn,'') AS MNPMDateIn,
                //  ISNULL(SHOP_MNPM_DT.MNPMDRAWERID,'') AS MNPMDRAWERID, 
                //  ISNULL(MNPMUserCode,'')+'-'+ISNULL(EMPID.SPC_NAME,'') AS MNWhoSend,
                //  CASE WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') = '' THEN ' ' 
                //    WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') < (CONVERT(VARCHAR,DateAcc,23) +' 08:30:59') THEN '1'
                //    WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') < (CONVERT(VARCHAR,DateAcc,23) +' 15:00:59') THEN '2'
                //    WHEN ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'') < (CONVERT(VARCHAR,DateAcc,23) +' 19:30:59') THEN '3'
                //  END AS ROUNDSEND,
                //  ISNULL(StaTrans,'') AS StaTrans,CASE ISNULL(StaTrans,'') WHEN '1' THEN 'รับแล้ว' ELSE 'ไม่ได้รับ' END AS StaTransDESC,
                //  ISNULL(CONVERT(VARCHAR,DateTrans,23) +' '+TimeTrans,'')  AS DateTrans,
                //  ISNULL(WhoTrans,'')+'-'+ISNULL(TMPTRANS.SPC_NAME,'') AS WhoTrans,
                //  ISNULL(StaAcc,'') AS StaAcc,CASE ISNULL(StaAcc,'') WHEN '1' THEN 'รับแล้ว' ELSE 'ไม่ได้รับ' END AS StaAccDESC ,
                //  ISNULL(CONVERT(VARCHAR,DateAcc,23)+' '+TimeAcc,'')  AS DateAcc,
                //  ISNULL(WhoAcc,'')+ '-' +ISNULL(TMPACC.SPC_NAME,'') AS WhoAcc

                //FROM	SHOP_MNEC_HD WITH (NOLOCK)
                //  LEFT JOIN SHOP_MNPM_DT WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNPM_DT.MNPMDRAWERID AND SHOP_MNPM_DT.MNPMDRAWERID LIKE 'MNEC%'
                //  LEFT JOIN SHOP_MNPM_HD WITH (NOLOCK) ON SHOP_MNPM_DT.MNPMDocno = SHOP_MNPM_HD.MNPMDocno
                //  LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE TMPACC WITH (NOLOCK)	 ON SHOP_MNPM_DT.WhoAcc = TMPACC.EMPLID AND TMPACC.DATAAREAID = N'SPC'   
                //  LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE TMPTRANS WITH (NOLOCK)  ON SHOP_MNPM_DT.WhoTrans = TMPTRANS.EMPLID AND TMPTRANS.DATAAREAID = N'SPC'
                //        LEFT OUTER JOIN SHOP2013TMP.dbo.EMPLTABLE EMPID WITH (NOLOCK)	 ON SHOP_MNPM_HD.MNPMUserCode = EMPID.EMPLID AND EMPID.DATAAREAID = N'SPC'
                //WHERE	MNECDateDoc BETWEEN '{RadDateTimePicker_1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                //  AND MNECStaApv IN ('1','2','3')
                //ORDER BY SHOP_MNEC_HD.MNECDocno ";

                DataTable DtCar = Models.EmplClass.MNEC_ReportMain(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); // ConnectionClass.SelectSQL_Main(sql);

                RadGridView_ShowHD.DataSource = DtCar;

                if (DtCar.Rows.Count == 0)
                { MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูล ลองใหม่อีกครั้ง."); this.Cursor = Cursors.Default; return; }

                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    int diffDay = 0;
                    string sta = "ยังไม่สร้าง SL", slID = "", slround = "";
                    Boolean slPoste = false;
                    double cashAmount = 0;

                    if (RadGridView_ShowHD.Rows[i].Cells["SLROUND"].Value.ToString() == "")
                    {
                        DataTable dtSL = Models.ARClass.GetSLByMNEC(RadGridView_ShowHD.Rows[i].Cells["MNECDocno"].Value.ToString());
                        if (dtSL.Rows.Count > 0)
                        {
                            sta = dtSL.Rows[0]["STA"].ToString();
                            slID = dtSL.Rows[0]["SLId"].ToString();
                            cashAmount = double.Parse(dtSL.Rows[0]["CASHAMOUNT"].ToString());
                            slround = dtSL.Rows[0]["SLROUND"].ToString();
                            if (dtSL.Rows[0]["POSTED"].ToString() == "1")
                            {
                                slPoste = true;
                                string sqlUp = $@"
                                UPDATE  SHOP_MNEC_HD
                                SET     RSL_DOCNO = '{slround}',SL_DOCNO = '{slID}',SL_NET = '{cashAmount}'
                                WHERE   MNECDocno = '{RadGridView_ShowHD.Rows[i].Cells["MNECDocno"].Value}'   ";
                                ConnectionClass.ExecuteSQL_Main(sqlUp);
                            }

                            if (dtSL.Rows[0]["STATUSFINANCE"].ToString() == "0")
                            {
                                TimeSpan Span = DateTime.Now - DateTime.Parse(dtSL.Rows[0]["SLDATE"].ToString());
                                diffDay = Span.Days;
                            }
                        }

                        RadGridView_ShowHD.Rows[i].Cells["SLROUND"].Value = slround;
                        RadGridView_ShowHD.Rows[i].Cells["POSTED"].Value = slPoste;
                        RadGridView_ShowHD.Rows[i].Cells["SLId"].Value = slID;
                        RadGridView_ShowHD.Rows[i].Cells["STA"].Value = sta;
                        RadGridView_ShowHD.Rows[i].Cells["CASHAMOUNT"].Value = cashAmount;
                        RadGridView_ShowHD.Rows[i].Cells["DAYS"].Value = diffDay.ToString("N");
                    }

                }

                this.Cursor = Cursors.Default;
            }

        }
        //สรุประบบเงินแลกเหรียญ
        void Report26(string pType)
        {
            if (pType == "0")
            {
                RadButtonElement_pdt.Enabled = false;
                radButtonElement_Print.Enabled = false;
                RadButtonElement_saveMN.Enabled = false;
                RadButtonElement_save.Enabled = false;
                radLabel_Date.Text = "ระบุวันที่ส่งซองเงิน";
                RadDateTimePicker_1.Visible = false; radDateTimePicker_D2.Visible = false; radLabel_Date.Visible = false;
                radLabel_Detail.Visible = true; radLabel_Detail.Text = "";


                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 120));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ชื่อเหรียญ", 140));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("REMARK", "ราคาเหรียญ"));

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("STOCK", "จำนวนเหรียญในสต็อก", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("STOCKSUM", "จำนวนเงินในสต็อก", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MNEC_SHOP", "เงินอยู่ที่สาขา", 120));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MNEC_CAR", "เงินอยู่ในรถ", 120));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MNEC_ACC", "เงินอยู่ที่บัญชี", 120));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PO_1999", "จำนวนเงินที่สั่งขนส่ง กทม", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PO_ACC", "จำนวนเงินจากบัญชี", 120));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM", "รวมทั้งหมด", 130));
                
                GridViewSummaryItem summaryItemA = new GridViewSummaryItem("STOCK", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemB = new GridViewSummaryItem("MNEC_SHOP", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemC = new GridViewSummaryItem("MNEC_CAR", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemD = new GridViewSummaryItem("MNEC_ACC", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemE = new GridViewSummaryItem("PO_1999", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemF = new GridViewSummaryItem("PO_ACC", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemG = new GridViewSummaryItem("STOCKSUM", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemH = new GridViewSummaryItem("SUM", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC, summaryItemD, summaryItemE, summaryItemF, summaryItemG, summaryItemH };

                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;

                DataTable DtCoin = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("59", "", "  ORDER BY CONVERT(decimal,REMARK) ", "1");

                RadGridView_ShowHD.DataSource = DtCoin;

                if (DtCoin.Rows.Count == 0)
                { MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูล ลองใหม่อีกครั้ง."); this.Cursor = Cursors.Default; return; }

                DataTable dtPo1999 = PurchClass.ProductRecive_GetPurchPO("2", "V008950", "");
                DataTable dtPoACC = PurchClass.ProductRecive_GetPurchPO("2", "V008951", "");
                               
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    double sumNet = 0;
                    string coin = DtCoin.Rows[i]["SHOW_ID"].ToString();
                    double coinPrice = double.Parse(RadGridView_ShowHD.Rows[i].Cells["REMARK"].Value.ToString());
                    double stock = ItembarcodeClass.FindStock_ByBarcode(coin, "RETAILAREA");
                    double stockNet = stock * coinPrice;
                    RadGridView_ShowHD.Rows[i].Cells["STOCK"].Value = stock.ToString("N2");
                    RadGridView_ShowHD.Rows[i].Cells["STOCKSUM"].Value = stockNet.ToString("N2");

                    sumNet += stockNet;
                    double po1999 = 0;
                    if (dtPo1999.Rows.Count > 0)
                    {
                        DataRow[] drPO1999 = dtPo1999.Select($@" BARCODE = '{coin}' ");
                        if (drPO1999.Length > 0) po1999 = Double.Parse(drPO1999[0]["PURCHQTY"].ToString());
                    }

                    double poAcc = 0;
                    if (dtPoACC.Rows.Count > 0)
                    {
                        DataRow[] drPOAcc = dtPoACC.Select($@" BARCODE = '{coin}' ");
                        if (drPOAcc.Length > 0) poAcc = Double.Parse(drPOAcc[0]["PURCHQTY"].ToString());
                    }

                    RadGridView_ShowHD.Rows[i].Cells["PO_1999"].Value = (po1999 * coinPrice).ToString("N2");
                    RadGridView_ShowHD.Rows[i].Cells["PO_ACC"].Value = (poAcc * coinPrice).ToString("N2");
                    sumNet += po1999;
                    sumNet += poAcc;

                    if (i == 0)
                    {
                        string sql1 = " AND ISNULL(MNPMLO,'') = '' ";//---สาขายังไม่ส่งซอง
                        string sql2 = " AND ISNULL(MNPMLO,'') != '' AND ISNULL(StaAcc,'0') = '0'  ";//--- ส่งแล้ว บัญชียังไม่รับ เงินอยู่ในรถ
                        string sql3 = " AND ISNULL(MNPMLO,'') != '' AND ISNULL(StaAcc,'0') = '1' AND SL_NET = 0 "; //--- ส่งแล้ว รับแล้วแต่ยังไม่ทำ SL
                        string sqlSelect = $@"
                            SELECT	ISNULL(SUM(MNECSum),0) AS SUMGRAND
                            FROM	SHOP_MNEC_HD WITH (NOLOCK)
		                            LEFT JOIN SHOP_MNPM_DT WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNPM_DT.MNPMDRAWERID AND SHOP_MNPM_DT.MNPMDRAWERID LIKE 'MNEC%'
		                            LEFT JOIN SHOP_MNPM_HD WITH (NOLOCK) ON SHOP_MNPM_DT.MNPMDocno = SHOP_MNPM_HD.MNPMDocno
                            WHERE	MNECDateDoc BETWEEN '2023-07-01'  AND DATEADD(DAY,10,GETDATE())--GETDATE()-30
		                            AND MNECStaApv IN ('1','2','3')  ";
                        double sum_NotSend = double.Parse(ConnectionClass.SelectSQL_Main($@" {sqlSelect} {sql1} ").Rows[0]["SUMGRAND"].ToString());
                        double sum_Send = double.Parse(ConnectionClass.SelectSQL_Main($@" {sqlSelect} {sql2} ").Rows[0]["SUMGRAND"].ToString());
                        double sum_NotSL = double.Parse(ConnectionClass.SelectSQL_Main($@" {sqlSelect} {sql3} ").Rows[0]["SUMGRAND"].ToString());
                        RadGridView_ShowHD.Rows[i].Cells["MNEC_SHOP"].Value = sum_NotSend.ToString("N2");
                        RadGridView_ShowHD.Rows[i].Cells["MNEC_CAR"].Value = sum_Send.ToString("N2");
                        RadGridView_ShowHD.Rows[i].Cells["MNEC_ACC"].Value = sum_NotSL.ToString("N2");
                        sumNet = sumNet + sum_NotSend + sum_Send + sum_NotSL;
                    }

                    RadGridView_ShowHD.Rows[i].Cells["SUM"].Value = sumNet;
                }

                this.Cursor = Cursors.Default;
            }

        }
        #region "ROWS DGV"
        private void RadGridView_ShowHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_ShowHD_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowHD_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_ShowHD_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        #region Event Click
        //SaveMN : บันทึกบิลให้สาขา
        private void RadButtonElement_saveMN_Click(object sender, EventArgs e)
        {
            if (_pTypeReport != "22") return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (SystemClass.SystemBranchID != "MN000") return;

            string BranchId, BranchName, RouteId, RouteName, DateDoc;

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกเหรียญส่งมินิมาร์ททั้งหมด ?") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;


            if (RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString() != "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"มีการบันทึกบิลแลกเหรียญมินิมาร์ทของวันที่ {RadDateTimePicker_1.Value:yyyy-MM-dd} เรียบร้อยแล้วไม่สามารถบันทึกบิลซ้ำได้.");
                this.Cursor = Cursors.Default;
                return;
            }

            ArrayList sql24 = new ArrayList();
            ArrayList sqlAx = new ArrayList();

            string ordernum = POClass.MNEC_CheckMNPT();
            if (ordernum == "")
            {
                ordernum = Class.ConfigClass.GetMaxINVOICEID("MNPT", "-", "MNPT", "1");
                sqlAx.Add(TimeKeeper.ScanDptTime.SqlSendAXHD("AX", ordernum, "RETAILAREA", SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                   RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString(), "สำหรับแลกเหรียญ", ""));

                sqlAx.Add(TimeKeeper.ScanDptTime.SqlSendAXDT("AX", ordernum, "RETAILAREA", SystemClass.SystemUserID_M));
            }

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
            {
                BranchId = RadGridView_ShowHD.Rows[iR].Cells["BRANCH_ID"].Value.ToString();
                BranchName = RadGridView_ShowHD.Rows[iR].Cells["BRANCH_NAME"].Value.ToString();
                RouteId = RadGridView_ShowHD.Rows[iR].Cells["ROUTEID"].Value.ToString();
                RouteName = RadGridView_ShowHD.Rows[iR].Cells["NAME"].Value.ToString();
                DateDoc = RadDateTimePicker_1.Value.ToString("yyyy - MM - dd");

                for (int iC = 12; iC < RadGridView_ShowHD.Columns.Count; iC++)
                {
                    if (RadGridView_ShowHD.Columns[iC].Name.ToString().Contains("B")) continue;

                    string Barcode, Name;
                    double QTY, Sum;
                    Name = RadGridView_ShowHD.Columns[iC].HeaderText.ToString();
                    Barcode = RadGridView_ShowHD.Columns[iC].Name.ToString();

                    QTY = double.Parse(RadGridView_ShowHD.Rows[iR].Cells[iC].Value.ToString());
                    Sum = double.Parse(RadGridView_ShowHD.Rows[iR].Cells["MNECSum"].Value.ToString());

                    if (iRowDT == 0)
                    {
                        iRowDT = 1;
                        MNECDocNo = ConfigClass.GetMaxINVOICEID("MNEC", "-", "MNEC", "1");
                        sql24.Add(SaveHD(MNECDocNo, BranchId, BranchName, RouteId, RouteName, DateDoc, Sum, ordernum, RadDropDownList_1.SelectedValue.ToString()));
                        sql24.Add(SaveDT(iRowDT, MNECDocNo, Barcode, Name, QTY));
                    }
                    else
                    {
                        iRowDT += 1;
                        sql24.Add(SaveDT(iRowDT, MNECDocNo, Barcode, Name, QTY));
                    }
                }
                iRowDT = 0;
            }
            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.Execute_SameTime_2Server(sql24, IpServerConnectClass.ConSelectMain, sqlAx, IpServerConnectClass.ConMainAX));
            Report22("1");
            this.Cursor = Cursors.Default;
        }

        //Save : บันทึกบิลให้บัญชี
        private void RadButtonElement_save_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (SystemClass.SystemBranchID == "MN000")
            {
                DataTable DtCheck = CheckSTA(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), RadDropDownList_1.SelectedValue.ToString(), "'0'");

                if (DtCheck.Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("มีรายการแลกเหรียญที่สาขายังไม่ยืนยัน ตรวจเช็คอีกครั้งหรือยกเลิกเอกสารก่อนบันทึกบิลส่งบัญชี.");
                    return;
                }


                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกรายการแลกเหรียญส่งบัญชี ?") == DialogResult.No) return;

                if (RadGridView_ShowHD.Rows.Count == 0) return;

                ArrayList sql24 = new ArrayList();
                DataTable Dt = MNECSelect(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), $@"AND MNECBranchRoute = '{RadDropDownList_1.SelectedValue}'", "MN000");
                if (Dt.Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("มีการบันทึกบิลให้บัญชีแล้ว ไม่ต้องบันทึกซ้ำ.");
                    return;
                }

                this.Cursor = Cursors.WaitCursor;

                DataTable DtSum = ItembarcodeClass.MNEC_FindSumForSendAR(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), RadDropDownList_1.SelectedValue.ToString());
                double MNECSUM = 0;

                string MNECDocNoNew = ConfigClass.GetMaxINVOICEID("MNEC", "-", "MNEC", "1");
                for (int i = 0; i < DtSum.Rows.Count; i++)
                {
                    sql24.Add(SaveDT(i + 1, MNECDocNoNew, DtSum.Rows[i]["ITEMBARCODE"].ToString(), DtSum.Rows[i]["SPC_ITEMNAME"].ToString(),
                                                     double.Parse(DtSum.Rows[i]["MNECQty"].ToString())));
                    MNECSUM += double.Parse(DtSum.Rows[i]["MNECQTYALL"].ToString());
                }
                sql24.Add(SaveHD(MNECDocNoNew, SystemClass.SystemBranchID, SystemClass.SystemBranchName,
                                                          "", "", RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), MNECSUM, "", RadDropDownList_1.SelectedValue.ToString()));

                sql24.Add(UpdateSta("", "2", RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"),
                                                              RadDropDownList_1.SelectedValue.ToString())); //ส่งให้บัญชี
                ArrayList sqlAX = AX_SendData.SAVE_SPC_POSWSTABLE20_MNEC(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), MNECDocNoNew, DtSum);

                string T = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAX); //ออกบิล MNEC รวมจำนวนเงินแต่ละสาขา
                this.Cursor = Cursors.Default;
                if (T == "") MsgBoxClass.MsgBoxShow_SaveStatus(T);
                Report22("1");
            }
            else
            {

                if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaApv"].Value.ToString() != "2")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ยังไม่มีการส่งเหรียญ ไม่สามารถกดยืนยันรับเงินแลกเหรียญได้."); return;
                }

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการรับเงินแลกเหรียญ ?") == DialogResult.No) return;

                string sql = UpdateSta(RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString(), "3",
                                                         RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"),
                                                         RadDropDownList_1.SelectedValue.ToString()); //ยืนยันรับเงิน
                MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sql));
                Report22("1");
            }
        }

        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            string Name = this.Text;
            string T = "";
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            if (_pTypeReport == "22")
            {
                Name = "รายละเอียด แลกเหรียญมินิมาร์ท";
                try
                {
                    Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                    app.Visible = true;
                    worksheet = workbook.Sheets["Sheet1"];
                    worksheet = workbook.ActiveSheet;
                    worksheet.Name = "ออร์เดอร์";

                    int iColumns = 1; //Set Column
                    worksheet.Columns[1].ColumnWidth = 20;
                    worksheet.Columns[2].ColumnWidth = 30;
                    worksheet.Columns[3].ColumnWidth = 19;
                    worksheet.Columns[3].ColumnWidth = 19;
                    for (int iH = 0; iH < RadGridView_ShowHD.Columns.Count; iH++) //set column : เฉพาะสาขาที่มีการสั่งสินค้า
                    {
                        if (RadGridView_ShowHD.Columns[iH].IsVisible == true)
                        {
                            if (iH > 4) worksheet.Columns[iH].ColumnWidth = 15;
                            worksheet.Cells[1, iColumns].value = RadGridView_ShowHD.Columns[iH].HeaderText.ToString();
                            iColumns++;
                        }
                    }

                    int iRows = 1; //Set Row
                    for (int iItem = 0; iItem < RadGridView_ShowHD.Rows.Count; iItem++)
                    {
                        if (RadGridView_ShowHD.Rows[iItem].Cells["MNECStaApv"].Value.ToString() != "")
                        {
                            int iColumnsR = 1;
                            for (int i = 0; i < RadGridView_ShowHD.Columns.Count; i++) //set row : เฉพาะสินค้าที่มีรายการสั่ง
                            {
                                if (RadGridView_ShowHD.Columns[i].IsVisible == true)
                                {
                                    worksheet.Cells[iRows + 1, 1].NumberFormat = "@";
                                    string vv = RadGridView_ShowHD.Rows[iItem].Cells[i].Value.ToString();
                                    worksheet.Cells[iRows + 1, iColumnsR].value = vv;
                                    if (vv == "0.00") worksheet.Cells[iRows + 1, iColumnsR].value = "";
                                    iColumnsR++;
                                }
                            }
                            iRows++;
                        }
                    }

                }
                catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }
            }
            T = DatagridClass.ExportExcelGridView(Name, RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        //Print
        private void RadButtonElement_Print_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            DataTable DtCheck = CheckSTA(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), RadDropDownList_1.SelectedValue.ToString(), "'2'");

            if (DtCheck.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ยังไม่มีการบันทึกบิลให้บัญชี ไม่สามารถพิมพ์บิลได้.");
                return;
            }

            string sqlPrint = $@"   SELECT	MNECRouteID, MNECRouteName, COUNT(MNECRouteID) AS CRoute
                                    FROM	SHOP_MNEC_HD
                                    WHERE	MNECDateDoc = '{RadDateTimePicker_1.Value:yyyy-MM-dd}'
		                                    AND MNECBranchRoute = '{RadDropDownList_1.SelectedValue}'
		                                    AND MNECStaApv = '2'
                                    AND MNECCarID != ''
                                    GROUP BY MNECRouteID, MNECRouteName";

            DataTable DtPrint = ConnectionClass.SelectSQL_Main(sqlPrint);
            if (DtPrint.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ยังไม่มีการระบุเลขทะเบียนรถ ไม่สามารถพิมพ์บิลได้.");
                return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการพิมพ์บิลส่งเหรียญทุกสายรถ ?") == DialogResult.No) return;

            for (int i = 0; i < DtPrint.Rows.Count; i++)
            {
                RouteId = $@"{DtPrint.Rows[i]["MNECRouteID"]}";
                PrintDocument2();
            }

            SetDGV_HD();
            return;
        }

        //Cell DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pTypeReport == "22")
            {
                if (SystemClass.SystemBranchID == "MN000") //ยกเลิกเอกสาร
                {
                    if (e.Column.Name != "MNECDocno") return;
                    if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaDoc"].Value.ToString() == "0" ||
                        RadGridView_ShowHD.CurrentRow.Cells["MNECStaApv"].Value.ToString() == "1") return;
                    if (RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString() == "") return;

                    DataTable Dt = MNECSelect(RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), $@"AND MNECBranchRoute = '{RadDropDownList_1.SelectedValue}'", "MN000");
                    if (Dt.Rows.Count > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("มีการบันทึกบิลให้บัญชีแล้ว ไม่สามารถยกเลิกเอกสารได้."); return;
                    }

                    DataTable DtSTA = CheckSTA(RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString(),
                                              RadGridView_ShowHD.CurrentRow.Cells["BRANCH_RouteSendCoin"].Value.ToString(),
                                              "'1','2','3'", RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString()); ;
                    if (DtSTA.Rows.Count > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่สามารถทำการยกเลิกเอกสารได้ {Environment.NewLine}เนื่องจากเอกสารไม่อยู่ในสถานะที่สามารถยกเลิกได้ {Environment.NewLine}รีเฟรชรายงานอีกครั้งเพื่อตรวจสอบสถานะของเอกสาร.");
                        return;
                    }


                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกเอกสาร ?") == DialogResult.No) return;

                    string rmk;
                    FormShare.ShowRemark _showRemark = new FormShare.ShowRemark("1")
                    {
                        pDesc = "หมายเหตุ : การยกเลิกเอกสารแลกเหรียญมินิมาร์ท"
                    };

                    if (_showRemark.ShowDialog(this) == DialogResult.Yes)
                    {
                        rmk = _showRemark.pRmk +
                                Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                                " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("บังคับระบุหมายเหตุก่อนการยกเลิกทุกครั้ง"); return;
                    }

                    if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaApv"].Value.ToString() == "0")
                    {
                        string sql = UpdateSta(RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString(), "0",
                                                            RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"), RadDropDownList_1.SelectedValue.ToString(), rmk); //ยกเลิก
                        string T = ConnectionClass.ExecuteSQL_Main(sql);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "") Report22("1");
                        return;
                    }
                }


                if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaDoc"].Value.ToString() == "0") return;
                if (RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString() == "") return;
                if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaApv"].Value.ToString() == "1") //RE Print
                {
                    if (e.Column.Name != "MNECDocno") return;
                    MNECDocNo = RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString();
                    string datedoc = RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString();

                    DataTable dtPrint = AR_Class.GetMNAR(datedoc,
                                                        RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString(),
                                                        MNECDocNo,
                                                        "4");

                    this.Bank1000 = double.Parse(dtPrint.Rows[0]["BANK1000"].ToString());
                    this.Bank500 = double.Parse(dtPrint.Rows[0]["BANK500"].ToString()); ;
                    this.Bank100 = double.Parse(dtPrint.Rows[0]["BANK100"].ToString()); ;
                    this.Bank50 = double.Parse(dtPrint.Rows[0]["BANK50"].ToString());
                    this.Bank20 = double.Parse(dtPrint.Rows[0]["BANK20"].ToString());
                    this.Coins = double.Parse(dtPrint.Rows[0]["COINS"].ToString());
                    this.Total = double.Parse(dtPrint.Rows[0]["NET"].ToString());

                    PrintDocument();
                    SetDGV_HD();
                    return;
                }


                ArrayList sql24 = new ArrayList();
                this.PCase = "1";
                this.Sum = RadGridView_ShowHD.CurrentRow.Cells["MNECSum"].Value.ToString();

                if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaApv"].Value.ToString() != "0") return;

                DataTable dtSTA = CheckSTA(RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString(),
                                              RadGridView_ShowHD.CurrentRow.Cells["BRANCH_RouteSendCoin"].Value.ToString(),
                                              "'','1','2','3'", RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());

                if (dtSTA.Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่สามารถทำการยืนยันแลกเหรียญได้ {Environment.NewLine}เนื่องจากเอกสารไม่อยู่ในสถานะที่สามารถยกเลิกได้ {Environment.NewLine}รีเฟรชรายงานอีกครั้งเพื่อตรวจสอบสถานะของเอกสาร.");
                    return;
                }


                this.Cursor = Cursors.WaitCursor;
                using (ARSendMoney_Type typeSendMoney = new ARSendMoney_Type(
                    this.Bank1000,
                    this.Bank500,
                    this.Bank100,
                    this.Bank50,
                    this.Bank20,
                    this.Coins,
                    this.PCase,
                    this.Sum
                    ))
                {
                    DialogResult dr = typeSendMoney.ShowDialog();
                    if (dr == DialogResult.Yes)
                    {
                        this.Bank1000 = typeSendMoney.Bank1000;
                        this.Bank500 = typeSendMoney.Bank500;
                        this.Bank100 = typeSendMoney.Bank100;
                        this.Bank50 = typeSendMoney.Bank50;
                        this.Bank20 = typeSendMoney.Bank20;
                        this.Coins = typeSendMoney.Coins;
                        this.Total = double.Parse(RadGridView_ShowHD.CurrentRow.Cells["MNECSum"].Value.ToString());
                        MNECDocNo = RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString();

                        Var_SendMoneyToFinance var_SendMoneyToFinance = new Var_SendMoneyToFinance
                        {
                            DATESENDMONEY = RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString(),
                            DATETRANSMONEY = RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString(),
                            Bank1000 = typeSendMoney.Bank1000,
                            Bank500 = typeSendMoney.Bank500,
                            Bank100 = typeSendMoney.Bank100,
                            Bank50 = typeSendMoney.Bank50,
                            Bank20 = typeSendMoney.Bank20,
                            Coins = typeSendMoney.Coins,
                            Total = typeSendMoney.Total,
                            Seq = 1,
                            Round = "4",
                            EMPLID = SystemClass.SystemUserName
                        };

                        sql24.Add(AR_Class.Save_SendMoneyToFinance(MNECDocNo, var_SendMoneyToFinance));

                        sql24.Add(UpdateSta(RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString(), "1")); //สาขา
                        ArrayList sqlAX = AX_SendData.SAVE_SPC_TMSLOADINGTABLE_MNEC(MNECDocNo, RadGridView_ShowHD.CurrentRow.Cells["MNECRef"].Value.ToString());
                        sqlAX.Add(AX_SendData.Save_EXTERNALLIST_SPC_SHIPMENTINVOICE(MNECDocNo, DateTime.Now.ToString("yyyy-MM-dd"),
                                                                                    SystemClass.SystemBranchID, SystemClass.SystemBranchName));

                        string T = ConnectionClass.Execute_SameTime_2Server(sql24, IpServerConnectClass.ConSelectMain, sqlAX, IpServerConnectClass.ConMainAX); //ออกบิล MNEC รวมจำนวนเงินแต่ละสาขา //ConDev
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "") PrintDocument();
                        SetDGV_HD();
                    }
                }
                this.Cursor = Cursors.Default;
            }

        }

        private void RadRadioButton_P2_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();

                RadGridView_ShowHD.DataSource = null;
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                summaryRowItem.Clear();
            }


            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_1, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));
            RadCheckBox_1.Visible = true; RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = false;
            RadDropDownList_1.Visible = true; RadDropDownList_1.Enabled = true;
            BranchClass.GetDataRouteSendCoin(RadDropDownList_1);
            radDateTimePicker_D2.Visible = false;
            if (radRadioButton_P2.CheckState == CheckState.Checked)
            {
                RadButtonElement_saveMN.Enabled = false;
                RadButtonElement_save.Enabled = false;
                radButtonElement_Print.Enabled = false;
                RadCheckBox_1.Visible = false;
                RadDropDownList_1.Visible = false;
                radDateTimePicker_D2.Visible = true;
                _pTypeReport = "23";
            }
            else _pTypeReport = "22";

        }
        #endregion

        #region Print Bill
        void PrintDocument()
        {
            DialogResult dr = printDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                printDocument1.Print();

                System.Drawing.Printing.PaperSize ps2 = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog2.PrinterSettings.DefaultPageSettings.PaperSize = ps2;
                printDocument2.PrinterSettings = printDialog2.PrinterSettings;
                printDocument2.Print();
            }
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int Y = 0;

            barcode.Data = MNECDocNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            e.Graphics.DrawString("ซองเงินแลกเหรียญ", new Font(new FontFamily("Tahoma"), 20), Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString(DateTime.Now.ToString("yyyy-MM-dd"), new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString(SystemClass.SystemUserID_M, new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 15, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 15;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 60;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 15;
            e.Graphics.DrawString("พิมพ์" + DateTime.Now, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 15;
            e.Graphics.DrawString("ธนบัตร 1000 : " + this.Bank1000 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 500  : " + this.Bank500 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 100  : " + this.Bank100 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 50   : " + this.Bank50 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ธนบัตร 20   : " + this.Bank20 + "  ใบ", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("เหรียญ        : " + this.Coins + "  บาท", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("ยอดรวม  " + this.Total.ToString("N2") + " บาท", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 20;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(SystemClass.SystemUserName, new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            //Y += 20;
            e.Graphics.PageUnit = GraphicsUnit.Inch;

        }

        private void PrintDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = MNECDocNo;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            e.Graphics.DrawString("ซองเงินแลกเหรียญ", new Font(new FontFamily("Tahoma"), 20), Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString(DateTime.Now.ToString("yyyy-MM-dd"), new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString(SystemClass.SystemUserID_M, new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString(SystemClass.SystemBranchID + " - " + SystemClass.SystemBranchName, SystemClass.printFont15, Brushes.Black, 15, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 60;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;// DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss")
            e.Graphics.DrawString("ซองรอบที่ _____ วันที่ส่ง " + DateTime.Now.ToString("yyyy-MM-dd"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("เวลาส่งเงิน ___________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 10;
            e.Graphics.DrawString(SystemClass.SystemUserName, new Font(new FontFamily("Tahoma"), 25), Brushes.Black, 0, Y);
            Y += 40;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }

        void PrintDocument2()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
            printDialog3.PrinterSettings.DefaultPageSettings.PaperSize = ps;
            printDocument3.PrinterSettings = printDialog1.PrinterSettings;
            printDocument3.Print();
        }

        private void PrintDocument3_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string sqlPrint = $@"   SELECT	MNECDocno, MNECDateDoc, MNECBranch, MNECBranchName, MNECSum, MNECRouteID, MNECRouteName, MNECCarID
                                    FROM	SHOP_MNEC_HD WITH (NOLOCK)
                                    WHERE	MNECDateDoc = '{RadDateTimePicker_1.Value:yyyy-MM-dd}'
		                                    AND MNECStaDoc = '1'
		                                    AND MNECStaApv = '2'
		                                    AND MNECRouteID = '{RouteId}'
		                                    AND MNECCarID != ''";

            DataTable DtPrint = ConnectionClass.SelectSQL_Main(sqlPrint);

            int Y = 0;

            e.Graphics.DrawString("สายส่งเหรียญ", new Font(new FontFamily("Tahoma"), 20), Brushes.Black, 0, Y);
            Y += 30;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 5, Y);
            Y += 15;
            e.Graphics.DrawString("สายรถ : " + RouteId + " - " + DtPrint.Rows[0]["MNECRouteName"], SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("ป้ายทะเบียน : " + DtPrint.Rows[0]["MNECCarID"].ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่ส่ง : " + DateTime.Now.ToString("yyyy-MM-dd"), SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------", SystemClass.printFont, Brushes.Black, 5, Y);
            for (int i = 0; i < DtPrint.Rows.Count; i++)
            {
                Y += 15;
                e.Graphics.DrawString(DtPrint.Rows[i]["MNECBranch"].ToString() + " - " + DtPrint.Rows[i]["MNECBranchName"].ToString(), SystemClass.printFont, Brushes.Black, 0, Y);
            }
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        #endregion

        #region Edit Data RadgridView 
        private void RadGridView_ShowHD_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pTypeReport == "22")
            {
                if (RadGridView_ShowHD.CurrentCell.Name.ToString().Contains("B")) return;

                string Qty = RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                if (Qty.Equals(""))
                {
                    QtyCell = 0;
                    return;
                }
                QtyCell = double.Parse(Qty);
            }

            if (_pTypeReport == "25")
            {
                string Carid = RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                if (Carid.Equals(""))
                {
                    CarId = ""; return;
                }
                else CarId = RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
            }
        }

        private void RadGridView_ShowHD_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (SystemClass.SystemBranchID != "MN000") return;

            if (_pTypeReport == "22")
            {
                if (RadGridView_ShowHD.CurrentCell.Name.ToString().Contains("B")) return;

                string StaApv = RadGridView_ShowHD.CurrentRow.Cells["MNECStaApv"].Value.ToString();

                if (RadGridView_ShowHD.CurrentRow.Cells["MNECStaDoc"].Value.ToString() == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถทำการแก้ไขจำนวนเหรียญได้ เนื่องจากเอกสารถูกยกเลิกแล้ว.");
                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = QtyCell.ToString("N2"); return;
                }

                if (StaApv == "1" || StaApv == "2")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถทำการแก้ไขจำนวนเหรียญได้ เนื่องจากสาขายืนยันการแลกเหรียญแล้ว.");
                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = QtyCell.ToString("N2"); return;
                }

                DataTable DtSTA = CheckSTA(RadGridView_ShowHD.CurrentRow.Cells["MNECDateDoc"].Value.ToString(),
                                              RadGridView_ShowHD.CurrentRow.Cells["BRANCH_RouteSendCoin"].Value.ToString(),
                                              "'1','2','3'", RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());

                if (DtSTA.Rows.Count != 0) //เช็คสถานะเอกสาร
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่สามารถทำการแก้ไขจำนวนเหรียญได้ {Environment.NewLine}เนื่องจากเอกสารไม่อยู่ในสถานะที่แก้ไขได้ {Environment.NewLine}รีเฟรชรายงานอีกครั้งเพื่อตรวจสอบสถานะของเอกสาร.");
                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = QtyCell.ToString("N2"); return;
                }

                if (RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString() == "")
                {
                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = "0.00"; return;
                } //ค่าที่ใส่ไม่เท่ากับ ""

                bool isNumerical = double.TryParse(RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString(), out _); //out double number
                if (isNumerical == false)
                {
                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = "0.00"; return;
                } //ค่าที่ใส่ไม่เท่ากับตัวเลข 

                string QtyInput = RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString();
                int QTYIn = Convert.ToInt32(double.Parse(RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString()));

                if (double.TryParse(QtyInput, out _))
                {
                    string MNECSum = SumQtyAllBranch(RadGridView_ShowHD.CurrentRow.Index);
                    ////แก้ไขการแสดงผลของยอดรวม
                    //RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = QTYIn.ToString("N2");
                    //RadGridView_ShowHD.CurrentRow.Cells["MNECSum"].Value =
                    //    SumQtyAllBranch(RadGridView_ShowHD.CurrentRow.Index);
                    string ColumnName = RadGridView_ShowHD.Columns[RadGridView_ShowHD.CurrentCell.ColumnIndex].Name.ToString();

                    if (RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString() != "")
                    {
                        if (SystemClass.SystemDptID == "D054")
                        {

                            string T = UPDATEQTY(float.Parse(MNECSum).ToString(),
                                                               RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value.ToString(),
                                                               RadGridView_ShowHD.CurrentRow.Cells["MNECDocno"].Value.ToString(),
                                                               ColumnName);
                            if (T != "") MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        }
                        else
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning("การแก้ไขจำนวนเหรียญ อนุญาตให้เฉพาะ Center แก้ไขเท่านั้น.");
                            RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = QtyCell.ToString("N2"); return;
                        }
                    }

                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex].Value = QTYIn.ToString("N2");
                    RadGridView_ShowHD.CurrentRow.Cells["MNECSum"].Value = MNECSum;
                    double cc = Double.Parse(RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex + 1].Value.ToString());
                    RadGridView_ShowHD.CurrentRow.Cells[RadGridView_ShowHD.CurrentCell.ColumnIndex + 1].Value = QTYIn * (cc / QtyCell);
                }
            }

            if (_pTypeReport == "24")
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["MNECCarID"].Value.ToString() == "")
                {
                    RadGridView_ShowHD.CurrentRow.Cells["MNECCarID"].Value = CarId;
                    return;
                }
                string carid = RadGridView_ShowHD.CurrentRow.Cells["MNECCarID"].Value.ToString();
                //DataTable CarDt = Models.AssetClass.FindDataVEHICLE(carid, "");
                //if (CarDt.Rows.Count == 0)
                //{
                //    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่พบข้อมูลรถทะเบียน {carid} ตรวจสอบข้อมูลอีกครั้ง.");
                //    RadGridView_ShowHD.CurrentRow.Cells["MNECCarID"].Value = "";
                //    return;
                //}

                SaveCarID(carid, RadDateTimePicker_1.Value.ToString("yyyy-MM-dd"),
                                            RadGridView_ShowHD.CurrentRow.Cells["MNECRouteID"].Value.ToString());

                SetDGV_HD();
            }
        }

        //Sum 
        private string SumQtyAllBranch(int curRow)//string SumQty, List<Qty> Qty
        {
            double sumGrand = 0;
            for (int i = 12; i < RadGridView_ShowHD.Columns.Count; i++)
            {
                string cName = RadGridView_ShowHD.Columns[i].Name.ToString();
                if (cName.Contains("B")) continue;

                DataRow[] dr = dtCoinConfig.Select($@"SHOW_ID = '{cName}'");
                double bath = double.Parse(dr[0]["SHOW_NAME"].ToString());


                double sum = bath * double.Parse(RadGridView_ShowHD.Rows[curRow].Cells[cName].Value.ToString());
                sumGrand += sum;

            }
            return sumGrand.ToString("N2");
        }

        #endregion

        #region Query
        DataTable CheckSTA(string date, string bchRoute, string sta, string bch = "")
        {
            string BchID = "";
            if (bch != "") BchID = $@"AND MNECBranch = '{bch}'";
            string sql = $@"    SELECT	MNECStaApv, COUNT(MNECStaApv) AS CountApv
                                FROM    SHOP_MNEC_HD  WITH (NOLOCK)
                                WHERE   MNECDateDoc = '{date}'
                                        AND MNECBranchRoute = '{bchRoute}'
                                        AND MNECStaApv IN ({sta})
                                        {BchID}
                                GROUP BY MNECStaApv ";

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable MNECSelect(string date, string CON, string bchID = "")
        {
            string BCH = "";
            if (bchID != "") BCH = $@"AND SHOP_MNEC_HD.MNECBranch = '{bchID}'";
            string sql = $@"
                                SELECT	SHOP_MNEC_HD.MNECDocno,
						CONVERT(NVARCHAR,SHOP_MNEC_HD.MNECDateDoc,23) AS MNECDateDoc, MNECStaDoc, 
		                MNECBranch,MNECBranchName,MNECRouteID,MNECRouteName,MNECStaApv
                FROM	SHOP_MNEC_HD WITH (NOLOCK)
		                INNER JOIN SHOP_MNEC_DT WITH (NOLOCK) ON SHOP_MNEC_HD.MNECDocno = SHOP_MNEC_DT.MNECDocno
                WHERE	CONVERT(nvarchar,SHOP_MNEC_HD.MNECDateDoc) = '{date}'
		                {BCH}
                        {CON}";


            return ConnectionClass.SelectSQL_Main(sql);
        }

        string UPDATEQTY(string SUM, string qty, string docno, string remark)
        {
            ArrayList sql = new ArrayList
            {
                $@"UPDATE  SHOP_MNEC_HD
                          SET     MNECSum = '{SUM}'    
                          WHERE   MNECDocno = '{docno}'",

                $@" UPDATE	SHOP_MNEC_DT
                        SET     MNECQty = '{double.Parse(qty)}',
                                MNECDateUp = '{DateTime.Now:yyyy-MM-HH hh:mm:ss}',
                                MNECWhoUp = '{SystemClass.SystemUserID_M}',
                                MNECWhoNameUp = '{SystemClass.SystemUserName}'
                        WHERE   MNECDocno = '{docno}'
                                AND MNECBarcode = '{remark}' "
            };


            return ConnectionClass.ExecuteSQL_ArrayMain(sql);
        }

        string SaveCarID(string carid, string Date, string routeID)
        {
            string sqlUp = $@"
                UPDATE	SHOP_MNEC_HD
                SET		MNECCarID = '{carid}',
		                MNECCarDateIns = GETDATE(),
		                MNECCarWhoIns = '{SystemClass.SystemUserID_M}',
		                MNECCarWhoNameIns = '{SystemClass.SystemUserName}'
                WHERE	MNECDateDoc = '{Date}'
		                AND MNECStaApv = '2'
		                AND MNECRouteID = '{routeID}' ";

            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }

        string UpdateSta(string docno, string sta, string datedoc = "", string BranchRoute = "", string remark = "")
        {
            //sta : 0 >> ยกลเลิกเอกสาร, 1 >> อัพสเตตัสสาขา, 2 >> อัพสเตตัสบัญชี, 3 >> ยืนยันรับเหรียญ
            string WHERE = $@"WHERE MNECDocno = '{docno}'";
            if (docno == "") WHERE = $@"WHERE   MNECDateDoc = '{datedoc}' 
                                                AND MNECBranchRoute = '{BranchRoute}'
                                                AND MNECStaApv = '1'";

            string sqlUp = $@"  UPDATE  SHOP_MNEC_HD
                                SET     MNECStaDoc = '{sta}',
                                        MNECStaApv = '',
                                        MNECWhoUp = '{SystemClass.SystemUserID}',
		                                MNECDateUp = CONVERT(NVARCHAR,GETDATE(),25),
                                        MNECRemark = '{remark}'
                                {WHERE} ";

            if (sta != "0")
            {
                sqlUp = $@" UPDATE	SHOP_MNEC_HD
                            SET		MNECStaApv = '{sta}',
                                    MNECApvDate = '{DateTime.Now:yyyy-MM-dd HH:mm:ss}',
		                            MNECApvId = '{SystemClass.SystemUserID}',
                                    MNECApvName = '{SystemClass.SystemUserName}'
                            {WHERE} ";
            }
            return sqlUp;
        }

        string SaveDT(int iRows, string docno, string Barcode, string Name, double Qty)
        {
            string sqlDT = $@"
                INSERT INTO SHOP_MNEC_DT(MNECDocno, MNECSeqNo, MNECBarcode, MNECName, MNECQty, MNECUnitID)
                VALUES('{docno}', '{iRows}', '{Barcode}', '{Name}', '{Qty}', 'เหรียญ') ";

            return sqlDT;
        }

        public static string SaveHD(string docno, string bchID, string bchName, string route, string routename, string DateDoc, double Sum, string MNPT, string BranchRoute)
        {
            string sql = $@"
                INSERT INTO SHOP_MNEC_HD
		                (MNECDocno, MNECWhoIns, MNECWhoNameIns,MNECBranch, MNECBranchName,MNECRouteID,MNECRouteName,MNECDateDoc,MNECSum,MNECRef,MNECBranchRoute)
                VALUES	('{docno}','{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}','{bchID}','{bchName}','{route}','{routename}','{DateDoc}','{Sum}','{MNPT}', '{BranchRoute}') ";
            return sql;
        }
        #endregion
    }
}

