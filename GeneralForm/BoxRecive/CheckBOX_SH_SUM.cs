﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class CheckBOX_SH_SUM : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pPermission; // 0 ไม่มีสิด 1 มีสิด
        //Load
        public CheckBOX_SH_SUM(string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load
        private void CheckBOX_SH_SUM_Load(object sender, EventArgs e)
        {

            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Date, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radCheckBox_SH.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_date.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("Click", "เช็ค", 80)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 110)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พขร", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ พขร.", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VEHICLEID", "ทะเบียนรถ", 160)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Shop_StatypeDesc", "สถานะเอกสาร", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTBOX", "จำนวนลัง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTBOXRECIVE", "จำนวนรับ", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("BOXDIFF", "จำนวนค้าง", 80)));

            RadGridView_ShowHD.Columns["Click"].IsPinned = true;
            RadGridView_ShowHD.Columns["MN"].IsPinned = true;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "BOXDIFF = COUNTBOX ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["LOGISTICID"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["BOXDIFF"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "BOXDIFF < COUNTBOX AND BOXDIFF <> 0 ", false)
            { CellBackColor = ConfigClass.SetColor_DarkPurplePastel() };
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["LOGISTICID"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["BOXDIFF"].ConditionalFormattingObjectList.Add(obj2);

            if (SystemClass.SystemBranchID == "MN000")
            {
                RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll("1", "1");
            }
            else
            {
                RadDropDownList_Branch.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.CheckState = CheckState.Checked;
                RadCheckBox_Branch.Enabled = false;
            }

            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            RadButton_Search.ButtonElement.ShowBorder = true;

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD(string pSH, string pDate, string pBch)
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

                string conDate = "", conSH = "", conBch = "";
            if (radCheckBox_date.Checked == true) conDate = pDate;
            if (radCheckBox_SH.Checked == true) conSH = pSH;
            if (RadCheckBox_Branch.Checked == true) conBch = pBch;

            dt_Data = BOXClass.SumBoxAll_ByCondition(conDate, conSH, conBch);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_Date.Value = DateTime.Now;
            radCheckBox_date.CheckState = CheckState.Checked;
            if (SystemClass.SystemBranchID == "MN000") RadCheckBox_Branch.CheckState = CheckState.Unchecked;
            radCheckBox_SH.CheckState = CheckState.Unchecked;
            radTextBox_SH1.Text = ""; radTextBox_SH2.Text = "";
            radTextBox_SH1.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetFind();
        }
        //Check Numbers
        private void RadTextBox_SH1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Check Numbers
        private void RadTextBox_SH2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Set Focus
        private void RadTextBox_SH1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_SH2.Focus();
                radTextBox_SH2.SelectAll();
            }
        }
        //Set Enter
        private void RadTextBox_SH2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetFind();
        }
        //Find
        void SetFind()
        {
            if ((radCheckBox_SH.Checked == false) && (RadCheckBox_Branch.Checked == false) && (radCheckBox_date.Checked == false)) return;

            string sh = "";
            if (radCheckBox_SH.Checked == true)
            {
                if (radTextBox_SH1.Text == "") return;
                if (radTextBox_SH2.Text == "") return;

                sh = radTextBox_SH1.Text + "-" + String.Format("{0:0000000}", Convert.ToUInt64(radTextBox_SH2.Text));
            }

            SetDGV_HD(sh, radDateTimePicker_Date.Value.ToString("yyyy-MM-dd"), RadDropDownList_Branch.SelectedValue.ToString());

        }
        //Recive Box
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "Click":
                    string pSH = RadGridView_ShowHD.MasterView.CurrentRow.Cells["SHIPMENTID"].Value.ToString();
                    if (pSH == "") return;

                    CheckBOX_Detail _checkBox_Detail = new CheckBOX_Detail(_pPermission, pSH, "0", RadGridView_ShowHD.MasterView.CurrentRow.Cells["MN"].Value.ToString());
                    if (_checkBox_Detail.ShowDialog(this) == DialogResult.OK)
                    {
                    }

                    if (_checkBox_Detail.pChange == "1") SetFind();

                    break;
                default:
                    break;
            }
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true)
            { RadDropDownList_Branch.Enabled = true; radCheckBox_date.CheckState = CheckState.Checked; }
            else
            { RadDropDownList_Branch.Enabled = false; }
        }
        //Date  Change
        private void RadCheckBox_date_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_date.Checked == true) radDateTimePicker_Date.Enabled = true; else radDateTimePicker_Date.Enabled = false;
        }
        //SH Change
        private void RadCheckBox_SH_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_SH.Checked == true)
            {
                radTextBox_SH1.Enabled = true; radTextBox_SH2.Enabled = true;
                radTextBox_SH1.Text = ""; radTextBox_SH2.Text = "";
                radTextBox_SH1.Focus();
            }
            else
            { radTextBox_SH1.Enabled = false; radTextBox_SH2.Enabled = false; }
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียด", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pPermission);
        }
    }
}
