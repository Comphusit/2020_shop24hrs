﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class ReciveBox_STA : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();

        readonly string _pType; // 0 = 'XY'  1 = P  2 = A .3 = ลังที่ไม่ได้สแกนรับ
        string sta;
        //Load
        public ReciveBox_STA(string pType)
        {
            InitializeComponent();
            _pType = pType;
        }
        //Load
        private void ReciveBox_STA_Load(object sender, EventArgs e)
        {
            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_Box.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEBILL", "สาขาเจ้าของ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMERECIVE", "สาขาสแกน", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME_RECIVE", "สาขารับ", 200)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEIN", "วันที่รับ", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Lo_Scan", "LO Scan", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO ลัง", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATE_Recive", "เวลาที่ยิง", 200)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXNUMBER", "เลขที่ลัง", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STA_ReciveTxt", "สถานะ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MACHINENAME", "PDA", 130)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCNAME", "ชื่อสินค้า", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

            switch (_pType)
            {
                case "0":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));
                    RadGridView_ShowHD.TableElement.RowHeight = 120;
                    radLabel_Detail.Text = "สีฟ้า >> รับลังสาขาอื่น | สีแดง >> ยังไม่มีสาขาไหนรับลัง | DoubleClick ช่องรูป >> ดูรูปขนาดใหญ่";
                    break;
                case "1":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));
                    RadGridView_ShowHD.TableElement.RowHeight = 120;
                    radLabel_Detail.Text = "สีฟ้า >> รับลังสาขาอื่น | สีแดง >> ยังไม่มีสาขาไหนรับลัง | DoubleClick ช่องรูป >> ดูรูปขนาดใหญ่";
                    break;
                case "2":
                    RadGridView_ShowHD.TableElement.RowHeight = 50;
                    radLabel_Detail.Text = "สีฟ้า >> รับลังสาขาอื่น | สีแดง >> ยังไม่มีสาขาไหนรับลัง";
                    break;
                case "3":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));
                    RadGridView_ShowHD.TableElement.RowHeight = 120;

                    RadGridView_ShowHD.Columns["NAMEBILL"].IsVisible = false;
                    RadGridView_ShowHD.Columns["NAMERECIVE"].IsVisible = false;
                    RadGridView_ShowHD.Columns["LOGISTICID"].IsVisible = false;
                    RadGridView_ShowHD.Columns["ITEMBARCODE"].IsVisible = false;
                    RadGridView_ShowHD.Columns["QTY"].IsVisible = false;
                    RadGridView_ShowHD.Columns["UNITID"].IsVisible = false;

                    radLabel_Detail.Text = "สีฟ้า >> รับลังสาขาอื่น | สีแดง >> ยังไม่มีสาขาไหนรับลัง | DoubleClick ช่องรูป >> ดูรูปขนาดใหญ่";
                    break;
                default:
                    break;
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME_EMPLDRIVER", "พขร.", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME_RECIVE", "ผู้รับลัง.", 160)));

            RadGridView_ShowHD.Columns["NAMEBILL"].IsPinned = true;
            RadGridView_ShowHD.Columns["NAMERECIVE"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCHNAME_RECIVE"].IsPinned = true;


            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "NAMEBILL <> BRANCHNAME_RECIVE ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            RadGridView_ShowHD.Columns["NAMEBILL"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["BRANCHNAME_RECIVE"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["STA_ReciveTxt"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "BRANCHNAME_RECIVE = '-' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["NAMEBILL"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["BRANCHNAME_RECIVE"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["STA_ReciveTxt"].ConditionalFormattingObjectList.Add(obj1);

            RadButton_Search.ButtonElement.ShowBorder = true;

            GroupDescriptor descriptor = new GroupDescriptor();
            descriptor.GroupNames.Add("BRANCHNAME_RECIVE", System.ComponentModel.ListSortDirection.Ascending);
            descriptor.GroupNames.Add("BOXNUMBER", System.ComponentModel.ListSortDirection.Ascending);
            RadGridView_ShowHD.GroupDescriptors.Add(descriptor);

            switch (_pType)
            {
                case "0"://รับลังสาขาอื่น
                    sta = "'N','Y'"; break;
                case "1": //ลังค้างรับ
                    sta = "'P'"; break;
                case "2": // สแกนลังไม่ตรง LO
                    sta = "'A'"; break;
                case "3":
                    RadGridView_ShowHD.Columns["NAME_EMPLDRIVER"].IsVisible = false;
                    break;
                default:
                    break;
            }
            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
             string pBox = "";
            if ((RadCheckBox_Box.CheckState == CheckState.Checked) && (RadTextBox_BoxGroup.Text != "")) pBox = RadTextBox_BoxGroup.Text;

            string pBch1 = "";
            if (SystemClass.SystemBranchID != "MN000") pBch1 = SystemClass.SystemBranchID;

            dt_Data = BOXClass.Report_ReciveBoxSta(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                pBox, pBch1, sta);//ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string wantFile = "";
                string pCount = "0";
                string pathFullName = PathImageClass.pImageEmply;

                string pathFileName;
                DirectoryInfo DirInfo;

                switch (_pType)
                {
                    case "0":
                        pathFileName = PathImageClass.pPathBOXBillEdit + dt_Data.Rows[i]["DATEIN"].ToString();
                        DirInfo = new DirectoryInfo(pathFileName);
                        if (DirInfo.Exists)
                        {
                            wantFile = "Edit-" +
                                dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString() + @"*_" +
                                dt_Data.Rows[i]["SHIPMENTID"].ToString() + @"," +
                                dt_Data.Rows[i]["BOXNUMBER"].ToString() + @"," +
                                dt_Data.Rows[i]["DOCUMENTNUM"].ToString() +
                                @".JPG";

                            FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                            if (Files.Length > 0)
                            {
                                pathFullName = Files[0].FullName;
                                pCount = "1";
                            }
                        }
                        if (pCount != "0")
                        {
                            RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                        }

                        RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFileName + @"|" + wantFile;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;
                        break;

                    case "1": //P - ลังค้างรับ
                        pathFileName = PathImageClass.pPathBOXNotScan + dt_Data.Rows[i]["DATEIN"].ToString() + @"\" + dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString();
                        DirInfo = new DirectoryInfo(pathFileName);
                        if (DirInfo.Exists)
                        {
                            wantFile = "RBox-" +
                                dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString() + @"*" +
                                dt_Data.Rows[i]["BOXNUMBER"].ToString() + @"*" +
                                @"A*.JPG";

                            FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                            if (Files.Length > 0)
                            {
                                pathFullName = Files[0].FullName;
                                pCount = "1";
                            }

                        }

                        if (pCount == "0")
                        {
                            string[] A = dt_Data.Rows[i]["PathImageTag"].ToString().Split('_');
                            string[] B = A[0].Split('-');
                            pathFileName = PathImageClass.pPathBOXNotScan + B[2] + "-" + B[3] + "-" + B[4] + @"\" + dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString();
                            DirInfo = new DirectoryInfo(pathFileName);
                            if (DirInfo.Exists)
                            {
                                wantFile = "RBox-" +
                                      dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString() + @"*" +
                                      dt_Data.Rows[i]["BOXNUMBER"].ToString() + @"*" +
                                      @"A*.JPG";

                                FileInfo[] Files1 = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);
                                if (Files1.Length > 0)
                                {
                                    pathFullName = Files1[0].FullName;
                                    pCount = "1";
                                }
                            }
                        }
                        if (pCount != "0")
                        {
                            RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                        }

                        RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFileName + @"|" + wantFile;
                        RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;
                        break;
                    default:
                        break;
                }
            }

            this.Cursor = Cursors.Default;
        }

        void SetDGV_BoxNotScan()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            
            string pBox = "";
            if ((RadCheckBox_Box.CheckState == CheckState.Checked) && (RadTextBox_BoxGroup.Text != "")) pBox = RadTextBox_BoxGroup.Text;

            string pBch1 = "";
            if (SystemClass.SystemBranchID != "MN000") pBch1 = SystemClass.SystemBranchID;

            dt_Data = BOX_Class.Repot_ReciveBoxNotScan(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pBox, pBch1);// ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string wantFile = "";
                string pCount = "0";
                string pathFullName = PathImageClass.pImageEmply;

                string pathFileName;
                DirectoryInfo DirInfo;


                pathFileName = PathImageClass.pPathBOXNotScan + dt_Data.Rows[i]["DATEIN"].ToString() + @"\" + dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString();
                DirInfo = new DirectoryInfo(pathFileName);
                if (DirInfo.Exists)
                {
                    wantFile = "RBox-" +
                        dt_Data.Rows[i]["BOXBRANCH_Recive"].ToString() + @"*" +
                        dt_Data.Rows[i]["BOXNUMBER"].ToString() + @"*" +
                        @".JPG";

                    FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                    if (Files.Length > 0)
                    {
                        pathFullName = Files[0].FullName;
                        pCount = "1";
                    }

                }

                if (pCount != "0")
                {
                    RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                }

                RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFileName + @"|" + wantFile;
                RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;
            }

            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now;
            RadCheckBox_Box.CheckState = CheckState.Unchecked;
            RadTextBox_BoxGroup.Text = "";
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            if (_pType == "3") SetDGV_BoxNotScan(); else SetDGV_HD();
        }


        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Box.Checked == true)
            { RadTextBox_BoxGroup.Enabled = true; RadTextBox_BoxGroup.Text = ""; RadTextBox_BoxGroup.Focus(); }
            else
            { RadTextBox_BoxGroup.Enabled = false; RadTextBox_BoxGroup.Text = ""; }
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (e.Column.Name)
            {
                case "Image":
                    ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage"].Value.ToString(),
                                                        RadGridView_ShowHD.CurrentRow.Cells["PathImage"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดการรับลังสาขาอื่น", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pType);
        }
    }
}
