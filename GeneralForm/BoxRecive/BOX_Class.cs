﻿//CheckOK
using System.Data;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    class BOX_Class
    {
        //Update BoxQtyNull
        public static string SHOP_RECIVEBOX_DeleteNull()
        {
            return " UPDATE	SHOP_RECIVEBOX Set BOXUITID = BOXQTY WHERE	BOXUITID = '' ";
        }
        //ค้นหาข้อมูลลัง
        public static DataTable CheckBox_FindData(string date1, string date2, string bchID)
        {
            string conditionBchID = "";
            if (bchID != "") conditionBchID = $@" AND BRANCH = '{bchID}' ";

            string sqlSelect = $@"
            SELECT	BRANCH,BRANCH_NAME,SHIPMENTID,SHOP_CHECKBOX_HD.DOCUMENTNUM AS DOCUMENTNUM , 
		            VEHICLEID +CHAR(10) + EMPLDRIVER+char(10)+SPC_NAME AS EMPLDRIVER ,VEHICLEID,WhoIns +char(10) +  WhoNameIns AS WhoIns,CONVERT(VARCHAR,DateIns,23) AS DateIns ,TimeIns,  
		            LINENUM, BOXGROUP, STABOX, Type, BARCODE, Name, QtyRecive, QtySend, Sign, Unit, Reson,LINEBILL  ,
		            CASE  WHEN (QtySend-QtyRecive) > 0 THEN '0' WHEN (QtySend-QtyRecive) < 0 THEN '1' ELSE '2' END AS Diff
 
            FROM	SHOP_CHECKBOX_HD WITH (NOLOCK) 
		            INNER JOIN  SHOP_CHECKBOX_DT WITH (NOLOCK)  ON SHOP_CHECKBOX_HD.DOCUMENTNUM = SHOP_CHECKBOX_DT.DOCUMENTNUM  
		            INNER JOIN Shop_Branch WITH (NOLOCK) ON SHOP_CHECKBOX_HD.BRANCH = Shop_Branch.BRANCH_ID  

            WHERE	DATEINS  BETWEEN '{date1}' AND '{date2}'  {conditionBchID}
            
            ORDER BY CONVERT(VARCHAR,DateIns,23),BRANCH,SHOP_CHECKBOX_HD.DOCUMENTNUM,LINENUM   ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //การ recheck Box Recive
        public static DataTable Shop_RecheckReciveItem_FindData(string date)
        {
            string sql = $@"
                SELECT	INVOICEID,ITEMBARCODE,DESCRIPTION  AS RMK
                FROM	Shop_RecheckReciveItem WITH (NOLOCK) 
			    WHERE	DATEBILL = '{date}' AND INVOICEID LIKE 'T%'  ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Update Recheck Recive
        public static string Shop_RecheckReciveItem_Update(string desc, string invoiceID, string itembarcode)
        {
            string UpStr = $@"
            Update  Shop_RecheckReciveItem  
            SET     DESCRIPTION = '{desc}'+CONVERT(VARCHAR,GETDATE(),23)+' ['+CONVERT(VARCHAR,GETDATE(),24)+']' 
            WHERE   INVOICEID = '{invoiceID}'  AND  ITEMBARCODE = '{itembarcode}' ";
            return ConnectionClass.ExecuteSQL_Main(UpStr);
        }
        //Recheck Recive Insert
        public static string Shop_RecheckReciveItem_Insert(string invoiceID, string date, string van, string bch, string itembarcode,
            string name, string unit, string qtySend, string qtyRecive, string shipment)
        {
            string sql = $@"
            INSERT INTO Shop_RecheckReciveItem (INVOICEID,DATEBILL,VAN,BRANCH,ITEMBARCODE,
                        SPC_ITEMNAME,UNITID,QtySend,QtyRecive,
                        SHIPMENTID,WHOINS,WHOINSNAME) 
            VALUES  ('{invoiceID}','{date}', '{van}','{bch}','{itembarcode}',
                    '{ConfigClass.ChecKStringForImport(name)}','{unit}','{qtySend}','{qtyRecive}',
                    '{shipment}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')
            ";
            return sql;
        }
        //Insert ReciveBox
        public static string SHOP_RECIVEBOX_Insert(string bchIDRecive, string boxID, string documentNum, string staRecive, string staProcess, string staAX,
            string boxType, string itembarcode, string itemName, double boxQty, string qtyBox, string rmk, 
            string bchIDBill, string shBill, string shRecive, string lo)
        {
            string sqlIn = $@"
                    INSERT INTO  SHOP_RECIVEBOX (BOXBRANCH,BOXNUMBER,DOCUMENTNUM,
                               MACHINENAME, Stadoc_Recive, Stadoc_Process,Stadoc_AX, 
                               BOXTYPE,BOXBARCODE,BOXSPCITEMNAME,BOXQTY,BOXUITID,REMARK,
                               DATEIN,TIMEIN,WHOIN,WHONAME,BOXBRANCHBILL,SHIPMENTID,SHIPID_Recive,LO_Recive ) 
                    values (
                               '{bchIDRecive}','{boxID}','{documentNum}',
                               '{SystemClass.SystemPcName}','{staRecive}','{staProcess}','{staAX}',
                               '{boxType}','{itembarcode}',
                               '{ConfigClass.ChecKStringForImport(itemName)}','{boxQty}','{qtyBox}',
                               '{rmk}',CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                               '{bchIDBill}','{shBill}','{shRecive}','{lo}'  ) ";

            return sqlIn;
        }
        //ReciveBox Other
        public static string SHOP_RECIVEBOX_OTHER_Insert(string bchID,string boxID,string documentNum,string shID,string bchIDBill)
        {
            string sql = $@"
                        INSERT INTO  SHOP_RECIVEBOX_OTHER (
                        BOXBRANCH_Recive,BOXNUMBER,DOCUMENTNUM,SHIPMENTID,MACHINENAME,
                        DATEIN,TIMEIN,WHOIN,WHONAME,BOXBRANCH_BILL,STA_Recive,ImageSta,LO_Scan,TagNumber ) values (
                        '{bchID}','{boxID}','{documentNum}',
                        '{shID}','{SystemClass.SystemPcName}',
                        CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                        '{bchIDBill}','Y','0','','' ) ";
            return sql;
        }
        //ค้นหาการรับสินค้า
        public static DataTable FindDetail_BoxReciveByBoxID(string pBoxID)
        {
            string sql = $@"
                SELECT	TOP 1 DOCUMENTNUM,BOXNUMBER,MACHINENAME,ISNULL(WHOIN + ' ' + WHONAME,'') AS WHOIN,
                        ISNULL(CONVERT(varchar,DATEIN,23),'') AS DATERECIVE,ISNULL(CONVERT(varchar,DATEIN,23) + ' ' + SUBSTRING(TIMEIN,0,6),'') AS DATEIN,
                        ISNULL(CONVERT(varchar,ProcessDATEIN,23) + ' ' + SUBSTRING(ProcessTIMEIN,0,6),'') AS ProcessDATEIN
                FROM	SHOP_RECIVEBOX  WITH (NOLOCK)
                WHERE	BOXNUMBER = '{pBoxID}' ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาลังค้างรับ
        public static DataTable Find_Shop_ReciveBOX_OtherP(string pBoxID)
        {
            string sql = $@"
                    SELECT	TOP 1 MACHINENAME,WHOIN+'-'+WHONAME AS WHOIN,CONVERT(varchar,DATEIN,23) + ' ' + TIMEIN AS DATEINS,BOXBRANCH_BILL,
                            BOXBRANCH_Recive,CASE WHEN BOXBRANCH_BILL=BOXBRANCH_Recive THEN '1' ELSE '2' END AS STA_RECIVE,PathImageItem,PathImageTag
                    FROM	SHOP_RECIVEBOX_OTHER WITH (NOLOCK)   
                    WHERE	[BOXNUMBER] = '{pBoxID}' AND STA_Recive = 'P'
                    ORDER BY DATEIN DESC,TIMEIN DESC ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //รับลังไม่ผ่านการสแกน
        public static DataTable Repot_ReciveBoxNotScan(string date1, string date2, string boxID, string bchID)
        {
            string con_BoxID = "";
            if (boxID != "") con_BoxID = $@" AND BOXNUMBER = '{boxID}' ";

            string con_BchID = "";
            if (bchID != "") con_BchID = $@"  AND  BOXBRANCH = '{bchID}' ";

            string sqlSelect = $@"
                    SELECT	BOXBRANCHBILL+'-'+BCHRECIVE.BRANCH_NAME AS NAMEBILL,BOXBRANCH+'-'+BCHBILL.BRANCH_NAME AS NAMERECIVE,
		                    BOXBRANCH+'-'+BCHBILL.BRANCH_NAME AS BRANCHNAME_RECIVE,BOXBRANCH AS BOXBRANCH_Recive,
		                    CONVERT(VARCHAR,DATEIN,23) AS DATEIN,LO_Recive AS Lo_Scan,LO_Recive AS LOGISTICID,SHIPMENTID,
		                    CONVERT(VARCHAR,DATEIN,23) + ' ' + TIMEIN AS DATE_Recive,WHOIN + CHAR(10) + WHONAME AS SPC_NAME_RECIVE,
		                    SHOP_RECIVEBOX.DOCUMENTNUM,BOXNUMBER,'ลังไม่ผ่านการสแกน' AS STA_ReciveTxt,MACHINENAME,
		                    '' AS ITEMBARCODE,SHOP_RECIVEBOX.BOXSPCITEMNAME AS SPCNAME,'' AS QTY,'' AS UNITID

                    FROM	SHOP_RECIVEBOX WITH (NOLOCK)
			                    INNER JOIN	SHOP_BRANCH BCHBILL WITH (NOLOCK) ON SHOP_RECIVEBOX.BOXBRANCH = BCHBILL.BRANCH_ID
			                    INNER JOIN	SHOP_BRANCH BCHRECIVE WITH (NOLOCK) ON SHOP_RECIVEBOX.BOXBRANCHBILL = BCHRECIVE.BRANCH_ID
	                   
                    WHERE	CONVERT(VARCHAR,DATEIN,23) BETWEEN '{date1}'  AND '{date2}'
		                    AND BOXSCN = '0' AND Stadoc_Recive = 'X' 
                            {con_BoxID} {con_BchID}
                    ORDER BY BOXBRANCH,DATEIN,TIMEIN ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
    }
}
