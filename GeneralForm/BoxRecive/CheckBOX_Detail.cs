﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class CheckBOX_Detail : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pPermission; // 0 ไม่มีสิด 1 มีสิด
        readonly string _pSH;
        readonly string _pTypeOpen;//0 เป็นการค้นหาจาก SH 1 เปนการค้นหาผ่าน หน้าจอลังค้างรับ   
        readonly string _pBchID;
        string lo;

        public string pChange;
        public CheckBOX_Detail(string pPermission, string pSH, string pTypeOpen, string pBchID)
        {
            InitializeComponent();
            _pPermission = pPermission;
            _pSH = pSH;
            _pTypeOpen = pTypeOpen;
            _pBchID = pBchID;
        }
        //Load
        private void CheckBOX_Detail_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_Recive.ShowBorder = true; radButtonElement_Recive.ToolTipText = "รับลังตามเลขที่ลัง";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export To Excel";

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "รับ")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("STA", "สถานะ", 50)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่บิล", 120)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 160)));

            if ((SystemClass.SystemBranchID == "MN000") && (_pPermission == "1")) RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 160)));
            else RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BOXGROUP", "เลขที่ลัง")));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCNAME", "ชื่อสินค้า", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXBRANCH_Recive", "สาขาที่รับ", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CheckBranch", "รับลังต่างสาขา")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BOXTYPE", "ประเภทลัง")));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTYINBOX", "จำนวนลังส่ง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("BOXQTY", "จำนวนลังรับ", 80)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MACHINENAME", "เครื่องที่รับ", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIN", "ผู้รับ", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEIN", "วันที่รับ", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ProcessDATEIN", "ส่งเข้า AX", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("p", "p")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImageItem", "PathImageItem")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImageTag", "PathImageTag")));

            RadGridView_ShowHD.Columns["C"].IsPinned = true;
            RadGridView_ShowHD.Columns["STA"].IsPinned = true;
            RadGridView_ShowHD.Columns["MN"].IsPinned = true;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "C = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            RadGridView_ShowHD.Columns["STA"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["MACHINENAME"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["WHOIN"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["ProcessDATEIN"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", " CheckBranch = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["STA"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["MACHINENAME"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["WHOIN"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["ProcessDATEIN"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition1", " p = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_PinkPastel() };
            RadGridView_ShowHD.Columns["STA"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["MACHINENAME"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["WHOIN"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["ProcessDATEIN"].ConditionalFormattingObjectList.Add(obj3);


            ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition1", " p = '2' ", false)
            { CellBackColor = ConfigClass.SetColor_YellowPastel() };
            RadGridView_ShowHD.Columns["STA"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["MACHINENAME"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["WHOIN"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["ProcessDATEIN"].ConditionalFormattingObjectList.Add(obj4);


            if ((SystemClass.SystemBranchID == "MN000") && (_pPermission == "1"))
            {
                GroupDescriptor descriptor = new GroupDescriptor();
                descriptor.GroupNames.Add("MN", System.ComponentModel.ListSortDirection.Ascending);
                descriptor.GroupNames.Add("BOXGROUP", System.ComponentModel.ListSortDirection.Ascending);
                RadGridView_ShowHD.GroupDescriptors.Add(descriptor);
                radLabel_Detail.Text = "Click รับ >> รับลัง | สีแดง >> รับลังสาขาอื่น | สีฟ้า >> ยังไม่ได้รับของ | สีชมพู >> สาขารับลังค้างรับ | สีเหลือง >> รับลังค้างรับสาขาอื่น | DoubleClick LO เพื่อดูรูป | DoubleClick ลัง เพื่อดูรูปค้างรับ | หน้าจอนี้ไม่สามารถรับลังต่างสาขาได้";
            }

            else
            {
                GroupDescriptor descriptor = new GroupDescriptor();
                descriptor.GroupNames.Add("MN", System.ComponentModel.ListSortDirection.Ascending);
                descriptor.GroupNames.Add("DOCUMENTNUM", System.ComponentModel.ListSortDirection.Ascending);
                RadGridView_ShowHD.GroupDescriptors.Add(descriptor);
                radLabel_Detail.Text = "สีแดง >> รับลังสาขาอื่น | สีฟ้า >> ยังไม่ได้รับของ | สีชมพู >> สาขารับลังค้างรับ | สีเหลือง >> รับลังค้างรับสาขาอื่น | DoubleClick LO เพื่อดูรูป | DoubleClick ลัง เพื่อดูรูปค้างรับ | หน้าจอนี้ไม่สามารถรับลังต่างสาขาได้";
            }

            RadButton_Search.ButtonElement.ShowBorder = true;

            ConnectionClass.ExecuteSQL_Main(BOX_Class.SHOP_RECIVEBOX_DeleteNull());

            ClearTxt();

            if (_pPermission == "0") radButtonElement_Recive.Enabled = false; else radButtonElement_Recive.Enabled = true;

            if (_pTypeOpen == "1")
            {
                radButtonElement_add.Enabled = false;
                RadButton_Search.Enabled = false;
                radRadioButton_SH.Enabled = false;
                radRadioButton_LO.Enabled = false;
                radRadioButton_Box.Enabled = false;
                radRadioButton_Bill.Enabled = false;
                SetDGV_ByBoxID();
            }
            else
            {
                if (_pSH != "")
                {
                    string[] p = _pSH.Split('-');
                    radTextBox_SH1.Text = p[0]; radTextBox_SH1.Enabled = false;
                    radTextBox_SH2.Text = p[1]; radTextBox_SH2.Enabled = false;
                    radButtonElement_add.Enabled = false;
                    RadButton_Search.Enabled = false;
                    radRadioButton_SH.CheckState = CheckState.Checked;
                    radRadioButton_SH.Enabled = false;
                    radRadioButton_LO.Enabled = false;
                    radRadioButton_Box.Enabled = false;
                    radRadioButton_Bill.Enabled = false;
                    SetFind();
                }
                else
                {
                    radRadioButton_SH.CheckState = CheckState.Checked;
                }
            }

        }
        //Set HD
        void SetDGV_BySH_LO(string pCondition)
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
            dt_Data = BOXClass.FindDetail_BillBySHLO(pCondition, _pBchID);// ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            ///Check คนรับลัง
            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pc = "", who = "", date = "", process = "";
                DataTable dtRecive = BOX_Class.FindDetail_BoxReciveByBoxID(dt_Data.Rows[i]["BOXGROUP"].ToString());
                if (dtRecive.Rows.Count > 0)
                {
                    pc = dtRecive.Rows[0]["MACHINENAME"].ToString();
                    who = dtRecive.Rows[0]["WHOIN"].ToString();
                    date = dtRecive.Rows[0]["DATEIN"].ToString();
                    process = dtRecive.Rows[0]["ProcessDATEIN"].ToString();
                }

                //ลังค้างรับ
                int p = 0;
                DataTable dtP = BOX_Class.Find_Shop_ReciveBOX_OtherP(dt_Data.Rows[i]["BOXGROUP"].ToString());
                if (dtP.Rows.Count > 0)
                {
                    p = Convert.ToInt32(dtP.Rows[0]["STA_RECIVE"].ToString());
                    pc = dtP.Rows[0]["MACHINENAME"].ToString();
                    who = dtP.Rows[0]["WHOIN"].ToString();
                    date = dtP.Rows[0]["DATEINS"].ToString();
                    process = "";
                    RadGridView_ShowHD.Rows[i].Cells["PathImageItem"].Value = dtP.Rows[0]["PathImageItem"].ToString();
                    RadGridView_ShowHD.Rows[i].Cells["PathImageTag"].Value = dtP.Rows[0]["PathImageTag"].ToString();
                }

                RadGridView_ShowHD.Rows[i].Cells["p"].Value = p;
                RadGridView_ShowHD.Rows[i].Cells["MACHINENAME"].Value = pc;
                RadGridView_ShowHD.Rows[i].Cells["WHOIN"].Value = who;
                RadGridView_ShowHD.Rows[i].Cells["DATEIN"].Value = date;
                RadGridView_ShowHD.Rows[i].Cells["ProcessDATEIN"].Value = process;
            }

            if (dt_Data.Rows.Count > 0)
            {
                lo = dt_Data.Rows[0]["LOGISTICID"].ToString();
                SetDescLO(lo);
            }

            this.Cursor = Cursors.Default;
        }

        //ค้นหาข้อมูลจากลังค้างรับ ตามเลขที่ลัง
        void SetDGV_ByBoxID()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
             
            dt_Data = BOXClass.FindDetail_BillByฺBillBoxID($@" AND SPC_BOXFACESHEET.BOXGROUP IN   ( {_pSH} ) ");// ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                DataTable dtRecive = BOX_Class.FindDetail_BoxReciveByBoxID(dt_Data.Rows[i]["BOXGROUP"].ToString());
                string pc = "", who = "", date = "", process = "";
                if (dtRecive.Rows.Count > 0)
                {
                    pc = dtRecive.Rows[0]["MACHINENAME"].ToString();
                    who = dtRecive.Rows[0]["WHOIN"].ToString();
                    date = dtRecive.Rows[0]["DATEIN"].ToString();
                    process = dtRecive.Rows[0]["ProcessDATEIN"].ToString();
                }

                int p = 0;
                DataTable dtP = BOX_Class.Find_Shop_ReciveBOX_OtherP(dt_Data.Rows[i]["BOXGROUP"].ToString());
                if (dtP.Rows.Count > 0)
                {
                    p = Convert.ToInt32(dtP.Rows[0]["STA_RECIVE"].ToString());
                    pc = dtP.Rows[0]["MACHINENAME"].ToString();
                    who = dtP.Rows[0]["WHOIN"].ToString();
                    date = dtP.Rows[0]["DATEINS"].ToString();
                    process = "";
                    RadGridView_ShowHD.Rows[i].Cells["PathImageItem"].Value = dtP.Rows[0]["PathImageItem"].ToString();
                    RadGridView_ShowHD.Rows[i].Cells["PathImageTag"].Value = dtP.Rows[0]["PathImageTag"].ToString();
                }

                RadGridView_ShowHD.Rows[i].Cells["MACHINENAME"].Value = pc;
                RadGridView_ShowHD.Rows[i].Cells["WHOIN"].Value = who;
                RadGridView_ShowHD.Rows[i].Cells["DATEIN"].Value = date;
                RadGridView_ShowHD.Rows[i].Cells["ProcessDATEIN"].Value = process;
                RadGridView_ShowHD.Rows[i].Cells["p"].Value = p;
            }

            SetEnableTxt();
            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            lo = "";
            radRadioButton_Bill.CheckState = CheckState.Unchecked;
            radRadioButton_SH.CheckState = CheckState.Checked;
            radRadioButton_Box.CheckState = CheckState.Unchecked;
            radRadioButton_LO.CheckState = CheckState.Unchecked;

            radTextBox_LO1.Text = ""; radTextBox_LO2.Text = "";
            RadTextBox_BoxGroup.Text = ""; radTextBox_Bill.Text = "";
            radTextBox_SH1.Text = ""; radTextBox_SH2.Text = "";

            radTextBox_SH1.Enabled = true; radTextBox_SH2.Enabled = true;
            radTextBox_LO1.Enabled = true; radTextBox_LO2.Enabled = true;
            RadTextBox_BoxGroup.Enabled = true; radTextBox_Bill.Enabled = true;

            radButtonElement_add.Enabled = true;
            radButtonElement_Recive.Enabled = true;
            RadButton_Search.Enabled = true;
            pChange = "0";
            radLabel_Drive.Text = "";

            radTextBox_SH1.Focus();
        }
        void SetEnableTxt()
        {
            lo = "";
            radRadioButton_Bill.CheckState = CheckState.Unchecked;
            radRadioButton_SH.CheckState = CheckState.Unchecked;
            radRadioButton_Box.CheckState = CheckState.Unchecked;
            radRadioButton_LO.CheckState = CheckState.Unchecked;

            radTextBox_LO1.Text = ""; radTextBox_LO2.Text = "";
            RadTextBox_BoxGroup.Text = ""; radTextBox_Bill.Text = "";
            radTextBox_SH1.Text = ""; radTextBox_SH2.Text = "";

            radTextBox_SH1.Enabled = false; radTextBox_SH2.Enabled = false;
            radTextBox_LO1.Enabled = false; radTextBox_LO2.Enabled = false;
            RadTextBox_BoxGroup.Enabled = false; radTextBox_Bill.Enabled = false;

            radButtonElement_add.Enabled = false;
            radButtonElement_Recive.Enabled = false;
            RadButton_Search.Enabled = false;

            radTextBox_SH1.Focus();
        }

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetFind();
        }
        //Check Numbers
        private void RadTextBox_SH1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Check Numbers
        private void RadTextBox_SH2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Set Focus
        private void RadTextBox_SH1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_SH2.Focus();
                radTextBox_SH2.SelectAll();
            }
        }
        //Set Enter
        private void RadTextBox_SH2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetFind();
        }
        //Find
        void SetFind()
        {
            //ค้นหาตาม SH
            if (radRadioButton_SH.CheckState == CheckState.Checked)
            {
                if (radTextBox_SH1.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลใบจัดส่งที่ต้องการค้นหาให้เรียบร้อย ก่อนกดค้นหา");
                    radTextBox_SH1.Focus();
                    return;
                }

                if (radTextBox_SH2.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลใบจัดส่งที่ต้องการค้นหาให้เรียบร้อย ก่อนกดค้นหา");
                    radTextBox_SH2.Focus();
                    return;
                }

                string sh = radTextBox_SH1.Text + "-" + String.Format("{0:0000000}", Convert.ToUInt32(radTextBox_SH2.Text));

                SetDGV_BySH_LO("SHIPMENTID = '" + sh + @"' ");
                return;
            }
            //ค้นหาตามบิล
            if (radRadioButton_Bill.CheckState == CheckState.Checked)
            {
                if (radTextBox_Bill.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลเลขที่บิลที่ต้องการค้นหาให้เรียบร้อย ก่อนกดค้นหา");
                    radTextBox_Bill.SelectAll();
                    radTextBox_Bill.Focus();
                    return;
                }

                SetDGV_ByBoxBill_ID("AND SPC_BOXFACESHEET.DOCUMENTNUM  = '" + radTextBox_Bill.Text + @"' ");
                return;
            }
            //ค้นหาตามเลขที่ลัง
            if (radRadioButton_Box.CheckState == CheckState.Checked)
            {
                if (RadTextBox_BoxGroup.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลเลขที่ลังที่ต้องการค้นหาให้เรียบร้อย ก่อนกดค้นหา");
                    RadTextBox_BoxGroup.SelectAll();
                    RadTextBox_BoxGroup.Focus();
                    return;
                }
                SetDGV_ByBoxBill_ID("AND SPC_BOXFACESHEET.BOXGROUP  = '" + RadTextBox_BoxGroup.Text + @"' ");
                return;
            }
            //ค้นหาตามเลข LO
            if (radRadioButton_LO.CheckState == CheckState.Checked)
            {
                if (radTextBox_LO1.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลใบส่งที่ต้องการค้นหาให้เรียบร้อย ก่อนกดค้นหา");
                    radTextBox_LO1.Focus();
                    return;
                }

                if (radTextBox_LO2.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ระบุข้อมูลใบส่งที่ต้องการค้นหาให้เรียบร้อย ก่อนกดค้นหา");
                    radTextBox_LO2.Focus();
                    return;
                }

                string lo = radTextBox_LO1.Text + "-" + String.Format("{0:0000000}", Convert.ToUInt32(radTextBox_LO2.Text));

                SetDGV_BySH_LO("LOGISTICID = '" + lo + @"' ");
            }


        }
        //Recive Box
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pPermission == "0") return;

            switch (e.Column.Name)
            {
                case "C":
                    if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "1") return;

                    string rmk = "PC-SUPC-SH";
                    string boxID = RadGridView_ShowHD.MasterView.CurrentRow.Cells["BOXGROUP"].Value.ToString();
                    string mnRecvice = RadGridView_ShowHD.MasterView.CurrentRow.Cells["MN"].Value.ToString();

                    double q = Convert.ToDouble(RadGridView_ShowHD.MasterView.CurrentRow.Cells["QTYINBOX"].Value.ToString()) -
                        Convert.ToDouble(RadGridView_ShowHD.MasterView.CurrentRow.Cells["BOXQTY"].Value.ToString());

                   
                    string staAX = "0", staProcess = "0";
                    if (AX_SendData.SPC_TRANSFERUPDRECEIVE_Insert(mnRecvice, boxID, RadGridView_ShowHD.MasterView.CurrentRow.Cells["DOCUMENTNUM"].Value.ToString(), rmk) == "")
                    {
                        staAX = "1";
                        staProcess = "1";
                        rmk += "-AX";
                    }
                     
                    string T = ConnectionClass.ExecuteSQL_Main(BOX_Class.SHOP_RECIVEBOX_Insert(
                        RadGridView_ShowHD.MasterView.CurrentRow.Cells["MN"].Value.ToString(),
                               boxID,
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["DOCUMENTNUM"].Value.ToString(),
                               "H", staProcess, staAX,
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["BOXTYPE"].Value.ToString(),
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["SPCNAME"].Value.ToString(),
                               q,
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["QTYINBOX"].Value.ToString(),
                               rmk, mnRecvice,
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["SHIPMENTID"].Value.ToString(),
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["SHIPMENTID"].Value.ToString(),
                               RadGridView_ShowHD.MasterView.CurrentRow.Cells["LOGISTICID"].Value.ToString()));

                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        //SetFind();
                        for (int ii = 0; ii < RadGridView_ShowHD.Rows.Count; ii++)
                        {
                            if (RadGridView_ShowHD.Rows[ii].Cells["BOXGROUP"].Value.ToString() == boxID)
                            {
                                RadGridView_ShowHD.Rows[ii].Cells["C"].Value = "1";
                                RadGridView_ShowHD.Rows[ii].Cells["BOXBRANCH_Recive"].Value = mnRecvice;
                                RadGridView_ShowHD.Rows[ii].Cells["CheckBranch"].Value = "0";
                            }
                        }

                        pChange = "1";
                        dt_Data.AcceptChanges();
                    }
                    break;
                default:
                    break;
            }
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Recive By BOX
        private void RadButtonElement_Recive_Click(object sender, EventArgs e)
        {
            ReciveBOX_BoxGroup _ReciveBOX_BoxGroup = new ReciveBOX_BoxGroup("0");
            _ReciveBOX_BoxGroup.Show();
        }
        //Set DGV ตามข้อมูล เลขที่บิลหรือเลขที่ลัง
        void SetDGV_ByBoxBill_ID(string pCondition)
        {
            this.Cursor = Cursors.WaitCursor;
            RadGridView_ShowHD.FilterDescriptors.Clear();

            if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();


            dt_Data = BOXClass.FindDetail_BillByฺBillBoxID(pCondition);//ConnectionClass.SelectSQL_Main(sql);
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                DataTable dtRecive = BOX_Class.FindDetail_BoxReciveByBoxID(dt_Data.Rows[i]["BOXGROUP"].ToString());
                string pc = "", who = "", date = "", process = "";
                if (dtRecive.Rows.Count > 0)
                {
                    pc = dtRecive.Rows[0]["MACHINENAME"].ToString();
                    who = dtRecive.Rows[0]["WHOIN"].ToString();
                    date = dtRecive.Rows[0]["DATEIN"].ToString();
                    process = dtRecive.Rows[0]["ProcessDATEIN"].ToString();
                }

                int p = 0;
                DataTable dtP = BOX_Class.Find_Shop_ReciveBOX_OtherP(dt_Data.Rows[i]["BOXGROUP"].ToString());
                if (dtP.Rows.Count > 0)
                {
                    p = Convert.ToInt32(dtP.Rows[0]["STA_RECIVE"].ToString());
                    pc = dtP.Rows[0]["MACHINENAME"].ToString();
                    who = dtP.Rows[0]["WHOIN"].ToString();
                    date = dtP.Rows[0]["DATEINS"].ToString();
                    process = "";
                    RadGridView_ShowHD.Rows[i].Cells["PathImageItem"].Value = dtP.Rows[0]["PathImageItem"].ToString();
                    RadGridView_ShowHD.Rows[i].Cells["PathImageTag"].Value = dtP.Rows[0]["PathImageTag"].ToString();
                }

                RadGridView_ShowHD.Rows[i].Cells["p"].Value = p;
                RadGridView_ShowHD.Rows[i].Cells["MACHINENAME"].Value = pc;
                RadGridView_ShowHD.Rows[i].Cells["WHOIN"].Value = who;
                RadGridView_ShowHD.Rows[i].Cells["DATEIN"].Value = date;
                RadGridView_ShowHD.Rows[i].Cells["ProcessDATEIN"].Value = process;
            }

            if (dt_Data.Rows.Count > 0)
            {
                lo = dt_Data.Rows[0]["LOGISTICID"].ToString();
                SetDescLO(lo);
            }
            this.Cursor = Cursors.Default;
        }
        //Desc
        void SetDescLO(string pLO)
        {
            if (pLO == "")
            {
                radLabel_Drive.Text = "ไม่มีข้อมูล LO";
                return;
            }
            DataTable dtLO = LogisticClass.GetLogisticDetail_ByLO(pLO);
            if (dtLO.Rows.Count > 0)
            {
                radLabel_Drive.Text = pLO + Environment.NewLine +
                  "ทะเบียนรถ   " + dtLO.Rows[0]["VEHICLEID"].ToString() + Environment.NewLine +
                  "พขร   " + dtLO.Rows[0]["EMPLDRIVER"].ToString() + Environment.NewLine +
                  "" + dtLO.Rows[0]["SPC_NAME"].ToString() + Environment.NewLine +
                  "" + dtLO.Rows[0]["DEPTLO"].ToString() + Environment.NewLine +
                  "" + dtLO.Rows[0]["SHIPPINGDATETIME"].ToString() + Environment.NewLine +
                  "" + dtLO.Rows[0]["ARRIVALDATETIME"].ToString() + Environment.NewLine +
                  "" + dtLO.Rows[0]["ROUTEID"].ToString() + "-" + dtLO.Rows[0]["ROUTENAME"].ToString();
            }
            else
            {
                radLabel_Drive.Text = "ไม่มีข้อมูล LO";
            }
        }
        //Change
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["LOGISTICID"].Value.ToString() != lo)
                {
                    lo = RadGridView_ShowHD.CurrentRow.Cells["LOGISTICID"].Value.ToString();
                    SetDescLO(lo);
                    return;
                }
                else return;
            }
            catch (Exception)
            {
                return;
            }

        }
        //Check Numbers
        private void RadTextBox_BoxGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Check Numbers
        private void RadTextBox_LO2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //SH
        private void RadRadioButton_SH_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_SH.CheckState == CheckState.Checked)
            {
                if (_pSH == "")
                {
                    radTextBox_SH2.Enabled = true; radTextBox_SH2.Text = "";
                    radTextBox_SH1.Enabled = true; radTextBox_SH1.Text = "20";
                    radTextBox_SH1.SelectionStart = radTextBox_SH1.Text.Length;
                    radTextBox_SH1.Focus();
                }

            }
            else
            {
                radTextBox_SH1.Enabled = false; radTextBox_SH1.Text = "";
                radTextBox_SH2.Enabled = false; radTextBox_SH2.Text = "";
            }
        }
        //Box
        private void RadRadioButton_Box_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_Box.CheckState == CheckState.Checked)
            {
                RadTextBox_BoxGroup.Enabled = true;
                RadTextBox_BoxGroup.Text = "";
                RadTextBox_BoxGroup.Focus();
            }
            else
            {
                RadTextBox_BoxGroup.Enabled = false;
                RadTextBox_BoxGroup.Text = "";
            }
        }
        //Bill
        private void RadRadioButton_Bill_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_Bill.CheckState == CheckState.Checked)
            {
                radTextBox_Bill.Enabled = true;
                radTextBox_Bill.Text = "";
                radTextBox_Bill.Focus();
            }
            else
            {
                radTextBox_Bill.Enabled = false;
                radTextBox_Bill.Text = "";
            }
        }
        //LO
        private void RadRadioButton_LO_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_LO.CheckState == CheckState.Checked)
            {
                radTextBox_LO2.Enabled = true; radTextBox_LO2.Text = ""; radTextBox_LO2.Focus();
                radTextBox_LO1.Enabled = true; radTextBox_LO1.Text = "LO" + DateTime.Now.Year.ToString().Substring(2, 2);
            }
            else
            {
                radTextBox_LO1.Enabled = false; radTextBox_LO1.Text = "";
                radTextBox_LO2.Enabled = false; radTextBox_LO2.Text = "";
            }
        }
        //SetEnter
        private void RadTextBox_BoxGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetFind();
        }
        //SetEnter
        private void RadTextBox_Bill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetFind();
        }
        //SetEnter
        private void RadTextBox_LO2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetFind();
        }
        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดการส่งของ", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "LOGISTICID":

                    string date = RadGridView_ShowHD.CurrentRow.Cells["DATEIN"].Value.ToString();
                    string bch = RadGridView_ShowHD.CurrentRow.Cells["MN"].Value.ToString();
                    string lo = RadGridView_ShowHD.CurrentRow.Cells["LOGISTICID"].Value.ToString();
                    if (date == "") { return; }

                    date = date.Substring(0, 10);
                    string pathFileName = PathImageClass.pPathSupc + date + @"\" + bch;

                    string pCount = "0";
                    string wantFile = "";

                    DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                    if (DirInfo.Exists)
                    {
                        wantFile = "SUPC-*" + lo + @".JPG";

                        FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                        if (Files.Length > 0) pCount = "1";
                    }

                    if (pCount == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรูปถ่ายการรับสินค้าของ " + lo + @" สำหรับสาขา " + bch + @" ในระบบ");
                        return;
                    }

                    ImageClass.OpenShowImage(pCount, pathFileName + @"|" + wantFile);

                    break;

                case "BOXGROUP":
                    if (RadGridView_ShowHD.CurrentRow.Cells["p"].Value.ToString() == "0")
                    { return; }

                    string pathFileNameP = PathImageClass.pPathBOXNotScan + RadGridView_ShowHD.CurrentRow.Cells["PathImageItem"].Value.ToString().Substring(11, 10) +
                        @"\" + RadGridView_ShowHD.CurrentRow.Cells["PathImageItem"].Value.ToString().Substring(5, 5) + @"\";

                    string pCountP = "0";

                    string wantFileP = "RBox-*" + RadGridView_ShowHD.CurrentRow.Cells["BOXGROUP"].Value.ToString() + "-" +
                        RadGridView_ShowHD.CurrentRow.Cells["DOCUMENTNUM"].Value.ToString() + "-A*.jpg";

                    DirectoryInfo DirInfoP = new DirectoryInfo(pathFileNameP);

                    if (DirInfoP.Exists)
                    {
                        FileInfo[] FilesP = DirInfoP.GetFiles(wantFileP, SearchOption.AllDirectories);

                        if (FilesP.Length > 0) pCountP = "1";
                    }

                    if (pCountP == "0")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่เจอรูปในระบบ ลองใหม่อีกครั้ง.");
                        return;
                    }

                    ImageClass.OpenShowImage(pCountP, pathFileNameP + @"|" + wantFileP);
                    break;
                default:
                    break;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeOpen);
        }
    }
}
