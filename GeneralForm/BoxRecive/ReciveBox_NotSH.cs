﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class ReciveBox_NotSH : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        //Load
        public ReciveBox_NotSH()
        {
            InitializeComponent();
        }
        //Load
        private void ReciveBox_NotSH_Load(object sender, EventArgs e)
        {
            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_Box.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO Scan รับ", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH Scan รับ", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่บิล", 120)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 160)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCNAME", "ชื่อสินค้า", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXBRANCH_Recive", "สาขาที่รับ", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CheckBranch", "รับลังต่างสาขา")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BOXTYPE", "ประเภทลัง")));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTYINBOX", "จำนวนลังส่ง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("BOXQTY", "จำนวนลังรับ", 80)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MACHINENAME", "เครื่องที่รับ", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIN", "ผู้รับ", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEIN", "วันที่รับ", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ProcessDATEIN", "ส่งเข้า AX", 150)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("p", "p")));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));


            RadGridView_ShowHD.Columns["MN"].IsPinned = true;
            RadGridView_ShowHD.Columns["MN_NAME"].IsPinned = true;
            RadGridView_ShowHD.Columns["LOGISTICID"].IsPinned = true;

            RadGridView_ShowHD.TableElement.RowHeight = 120;

            ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition1", " p = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_PurplePastel() };
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["MACHINENAME"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["WHOIN"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj3);
            RadGridView_ShowHD.Columns["ProcessDATEIN"].ConditionalFormattingObjectList.Add(obj3);


            ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition1", " p = '2' ", false)
            { CellBackColor = ConfigClass.SetColor_YellowPastel() };
            RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["MACHINENAME"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["WHOIN"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["DATEIN"].ConditionalFormattingObjectList.Add(obj4);
            RadGridView_ShowHD.Columns["ProcessDATEIN"].ConditionalFormattingObjectList.Add(obj4);

            GroupDescriptor descriptor = new GroupDescriptor();
            descriptor.GroupNames.Add("MN", System.ComponentModel.ListSortDirection.Ascending);
            descriptor.GroupNames.Add("BOXGROUP", System.ComponentModel.ListSortDirection.Ascending);
            RadGridView_ShowHD.GroupDescriptors.Add(descriptor);

            RadButton_Search.ButtonElement.ShowBorder = true;

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string pBch = "";
            if (SystemClass.SystemBranchID != "MN000") pBch = SystemClass.SystemBranchID;
             
            dt_Data = BOXClass.Report_ReciveBoxNotSH_ByDateRecive(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pBch);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string wantFile = "";
                string pCount = "0";
                string pathFullName = PathImageClass.pImageEmply;
                string pathFileName = PathImageClass.pPathBOXNotScan;
                DirectoryInfo DirInfo;

                string pc = "", who = "", date = "", process = "", dateRecive;
                DataTable dtRecive = BOX_Class.FindDetail_BoxReciveByBoxID(dt_Data.Rows[i]["BOXGROUP"].ToString());
                if (dtRecive.Rows.Count > 0)
                {
                    pc = dtRecive.Rows[0]["MACHINENAME"].ToString();
                    who = dtRecive.Rows[0]["WHOIN"].ToString();
                    date = dtRecive.Rows[0]["DATEIN"].ToString();
                    process = dtRecive.Rows[0]["ProcessDATEIN"].ToString();
                    dateRecive = dtRecive.Rows[0]["DATERECIVE"].ToString();

                    pathFileName += dateRecive + @"\" + dt_Data.Rows[i]["MN"].ToString(); ;
                    DirInfo = new DirectoryInfo(pathFileName);
                    if (DirInfo.Exists)
                    {
                        wantFile = "RBox-" +
                            dt_Data.Rows[i]["MN"].ToString() + @"*" +
                            dt_Data.Rows[i]["BOXGROUP"].ToString() + @"" +
                            @"*N.JPG";

                        FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                        if (Files.Length > 0)
                        {
                            pathFullName = Files[0].FullName;
                            pCount = "1";
                        }
                    }
                }


                int p = 0;
                DataTable dtP = BOX_Class.Find_Shop_ReciveBOX_OtherP(dt_Data.Rows[i]["BOXGROUP"].ToString());
                if (dtP.Rows.Count > 0)
                {
                    p = Convert.ToInt32(dtP.Rows[0]["STA_RECIVE"].ToString());
                    pc = dtP.Rows[0]["MACHINENAME"].ToString();
                    who = dtP.Rows[0]["WHOIN"].ToString();
                    date = dtP.Rows[0]["DATEINS"].ToString();
                    process = "";
                }

                RadGridView_ShowHD.Rows[i].Cells["p"].Value = p;
                RadGridView_ShowHD.Rows[i].Cells["MACHINENAME"].Value = pc;
                RadGridView_ShowHD.Rows[i].Cells["WHOIN"].Value = who;
                RadGridView_ShowHD.Rows[i].Cells["DATEIN"].Value = date;
                RadGridView_ShowHD.Rows[i].Cells["ProcessDATEIN"].Value = process;

                RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFileName + @"|" + wantFile;
                RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;

            }

            this.Cursor = Cursors.Default;
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now;
            RadCheckBox_Box.CheckState = CheckState.Unchecked;
            RadTextBox_BoxGroup.Text = "";
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Box.Checked == true) { RadTextBox_BoxGroup.Enabled = true; RadTextBox_BoxGroup.Text = ""; RadTextBox_BoxGroup.Focus(); }
            else { RadTextBox_BoxGroup.Enabled = false; RadTextBox_BoxGroup.Text = ""; }
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement))   return;  

            switch (e.Column.Name)
            {
                case "Image":
                    ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage"].Value.ToString(),
                                                        RadGridView_ShowHD.CurrentRow.Cells["PathImage"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return; 
            string T = DatagridClass.ExportExcelGridView("รายละเอียดการรับลังไม่ขึ้นทะเบียน", RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
