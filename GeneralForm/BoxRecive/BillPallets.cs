﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class BillPallets : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pPermission; // 0 ไม่มีสิด 1 มีสิด

        int pInsert;
        //Load
        public BillPallets(string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load
        private void BillPallets_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_Clear.ToolTipText = "ล้างข้อมูล"; radButtonElement_Clear.ShowBorder = true;
            radButtonElement_add.ToolTipText = "บันทึกบิล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadCheckBox_Diff.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่บิล", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEIN", "วันที่รับ", 130)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 120)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวนส่ง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("BOXQTY", "จำนวนรับ", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("BOXDIFF", "สรุป", 80)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESC_RMK", "คำอธิบาย", 350)));

            RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
            RadGridView_ShowHD.TableElement.RowHeight = 150;

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "BOXDIFF > 0 ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            RadGridView_ShowHD.Columns["BOXQTY"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["QTY"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "BOXDIFF < 0 ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["BOXQTY"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["QTY"].ConditionalFormattingObjectList.Add(obj1);

            RadButton_Search.ButtonElement.ShowBorder = true;

            radButtonElement_add.Enabled = false;
            if (_pPermission == "1") radLabel_Detail.Text = "สีฟ้า >> รับสินค้าเกิน | สีแดง >> รับสินค้าขาด | DoubleClick ช่องคำอธิบาย >> ตอบเหตุผล | DoubleClick ช่องรูป >> ดูรูปขนาดใหญ่";
            else radLabel_Detail.Text = "สีฟ้า >> รับสินค้าเกิน | สีแดง >> รับสินค้าขาด | DoubleClick ช่องรูป >> ดูรูปขนาดใหญ่";

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string p1 = "", p2 = "ORDER BY BRANCH_ID,SPC_BOXFACESHEET.BOXGROUP";
            if (RadCheckBox_Diff.CheckState == CheckState.Checked)
            {
                p1 = "SELECT	*	FROM	(";
                p2 = ")TMP WHERE BOXDIFF != '0'   ORDER BY BRANCH_ID,BOXGROUP ";
            }

            string pBch = "";
            if (SystemClass.SystemBranchID != "MN000") pBch = SystemClass.SystemBranchID;
  
            dt_Data = BOXClass.Report_BillPallets(p1, p2, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), pBch);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string dateIns = dt_Data.Rows[i]["DATEIN"].ToString();
                string pathFullName = PathImageClass.pImageEmply;
                string pathFileName = PathImageClass.pPathBOXNotScan + dateIns + @"\" + dt_Data.Rows[i]["BRANCH_ID"].ToString();
                string wantFile = "";
                string pCount = "0";
                if (dateIns != "")
                {
                    DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                    if (DirInfo.Exists)
                    {
                        wantFile = "RBox-" +
                            dt_Data.Rows[i]["BRANCH_ID"].ToString() + @"*" +
                            dt_Data.Rows[i]["BOXGROUP"].ToString() + @"" +
                            @"*Q.JPG";

                        FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                        if (Files.Length > 0)
                        {
                            pathFullName = Files[0].FullName;
                            pCount = "1";
                        }
                    }
                }

                RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFileName + @"|" + wantFile;
                RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;

            }

            SetStaInsert();
            if (pInsert > 0) radButtonElement_add.Enabled = false;
            else
            {
                if (_pPermission == "1") radButtonElement_add.Enabled = true;
            }

            this.Cursor = Cursors.Default;
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        void SetStaInsert()
        {
            if (_pPermission == "0")
            {
                pInsert = 1;
                return;
            }
            
            DataTable dt = BOX_Class.Shop_RecheckReciveItem_FindData(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sql);
            if (dt_Data.Rows.Count > 0) pInsert = dt.Rows.Count;
            else
            {
                int r = 0;
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["BOXDIFF"].Value.ToString() != "0") r += 1;
                }
                pInsert = r;
            }
        }
        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            pInsert = 0;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการรับลังพาเลท") == DialogResult.Yes)
            {
                ArrayList sql = new ArrayList();
                for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                {
                    if (RadGridView_ShowHD.Rows[i].Cells["BOXDIFF"].Value.ToString() != "0")
                    {
                         
                        sql.Add(BOX_Class.Shop_RecheckReciveItem_Insert(RadGridView_ShowHD.Rows[i].Cells["DOCUMENTNUM"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["TRANSDATE"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["BOXGROUP"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["BOXGROUP"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["NAME"].Value.ToString(), "",
                                RadGridView_ShowHD.Rows[i].Cells["QTY"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["BOXQTY"].Value.ToString(),
                                RadGridView_ShowHD.Rows[i].Cells["SHIPMENTID"].Value.ToString()));
                    }
                }

                string T = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T == "")
                {
                    pInsert = sql.Count;
                    radButtonElement_add.Enabled = false;
                }

            }
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (e.Column.Name)
            {
                case "Image":
                    ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage"].Value.ToString(), RadGridView_ShowHD.CurrentRow.Cells["PathImage"].Value.ToString());
                    break;
                case "DESC_RMK":
                    if (_pPermission == "0") { return; }
                    if (RadGridView_ShowHD.CurrentRow.Cells["BOXDIFF"].Value.ToString() == "0") return;
                    if (pInsert == 0) return;
                    if (RadGridView_ShowHD.CurrentRow.Cells["DESC_RMK"].Value.ToString() != "") return;

                    FormShare.InputData _inputData = new FormShare.InputData("1", RadGridView_ShowHD.CurrentRow.Cells["NAME"].Value.ToString(), "หมายเหตุการตาม", "");
                    if (_inputData.ShowDialog(this) == DialogResult.Yes)
                    {
                        string strUp = _inputData.pInputData + Environment.NewLine + "-> BY " + SystemClass.SystemUserName + Environment.NewLine +
                                        " [" + SystemClass.SystemDptName + "/" + SystemClass.SystemUserNameShort + "] ";

                        
                        string T = BOX_Class.Shop_RecheckReciveItem_Update(strUp, RadGridView_ShowHD.CurrentRow.Cells["DOCUMENTNUM"].Value.ToString(),
                            RadGridView_ShowHD.CurrentRow.Cells["BOXGROUP"].Value.ToString());// ConnectionClass.ExecuteSQL_Main(UpStr);
                        if (T == "") RadGridView_ShowHD.CurrentRow.Cells["DESC_RMK"].Value = _inputData.pInputData;

                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    }
                    break;
                default:
                    break;
            }

        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดบิลพาเลท " + RadGridView_ShowHD.Rows[0].Cells["TRANSDATE"].Value.ToString(), RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Clear
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pPermission);
        }
    }
}
