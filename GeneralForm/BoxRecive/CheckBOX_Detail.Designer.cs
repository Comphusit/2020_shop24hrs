﻿namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    partial class CheckBOX_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckBOX_Detail));
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radTextBox_LO2 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_LO1 = new Telerik.WinControls.UI.RadTextBox();
            this.radRadioButton_LO = new Telerik.WinControls.UI.RadRadioButton();
            this.radTextBox_Bill = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_BoxGroup = new Telerik.WinControls.UI.RadTextBox();
            this.radRadioButton_Box = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Bill = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_SH = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel_Drive = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_SH2 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_SH1 = new Telerik.WinControls.UI.RadTextBox();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Recive = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LO2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LO1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_LO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BoxGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Bill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_SH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Drive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SH2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SH1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 605);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ShowHD.SelectionChanged += new System.EventHandler(this.RadGridView_ShowHD_SelectionChanged);
            this.RadGridView_ShowHD.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellClick);
            this.RadGridView_ShowHD.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ShowHD_CellDoubleClick);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 19);
            this.radLabel_Detail.TabIndex = 53;
            this.radLabel_Detail.Text = resources.GetString("radLabel_Detail.Text");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radTextBox_LO2);
            this.panel1.Controls.Add(this.radTextBox_LO1);
            this.panel1.Controls.Add(this.radRadioButton_LO);
            this.panel1.Controls.Add(this.radTextBox_Bill);
            this.panel1.Controls.Add(this.RadTextBox_BoxGroup);
            this.panel1.Controls.Add(this.radRadioButton_Box);
            this.panel1.Controls.Add(this.radRadioButton_Bill);
            this.panel1.Controls.Add(this.radRadioButton_SH);
            this.panel1.Controls.Add(this.radLabel_Drive);
            this.panel1.Controls.Add(this.radTextBox_SH2);
            this.panel1.Controls.Add(this.radTextBox_SH1);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // radTextBox_LO2
            // 
            this.radTextBox_LO2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_LO2.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_LO2.Location = new System.Drawing.Point(76, 399);
            this.radTextBox_LO2.MaxLength = 7;
            this.radTextBox_LO2.Name = "radTextBox_LO2";
            this.radTextBox_LO2.Size = new System.Drawing.Size(109, 25);
            this.radTextBox_LO2.TabIndex = 5;
            this.radTextBox_LO2.Text = "0054450";
            this.radTextBox_LO2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_LO2_KeyDown);
            this.radTextBox_LO2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_LO2_KeyPress);
            // 
            // radTextBox_LO1
            // 
            this.radTextBox_LO1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_LO1.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_LO1.Location = new System.Drawing.Point(10, 399);
            this.radTextBox_LO1.MaxLength = 4;
            this.radTextBox_LO1.Name = "radTextBox_LO1";
            this.radTextBox_LO1.Size = new System.Drawing.Size(57, 25);
            this.radTextBox_LO1.TabIndex = 4;
            this.radTextBox_LO1.Text = "LO20";
            // 
            // radRadioButton_LO
            // 
            this.radRadioButton_LO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_LO.Location = new System.Drawing.Point(10, 376);
            this.radRadioButton_LO.Name = "radRadioButton_LO";
            this.radRadioButton_LO.Size = new System.Drawing.Size(103, 19);
            this.radRadioButton_LO.TabIndex = 66;
            this.radRadioButton_LO.TabStop = false;
            this.radRadioButton_LO.Text = "เลขที่ส่ง [LO]";
            this.radRadioButton_LO.CheckStateChanged += new System.EventHandler(this.RadRadioButton_LO_CheckStateChanged);
            // 
            // radTextBox_Bill
            // 
            this.radTextBox_Bill.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_Bill.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Bill.Location = new System.Drawing.Point(10, 337);
            this.radTextBox_Bill.MaxLength = 20;
            this.radTextBox_Bill.Name = "radTextBox_Bill";
            this.radTextBox_Bill.Size = new System.Drawing.Size(178, 25);
            this.radTextBox_Bill.TabIndex = 3;
            this.radTextBox_Bill.Text = "T2007184-000000";
            this.radTextBox_Bill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Bill_KeyDown);
            // 
            // RadTextBox_BoxGroup
            // 
            this.RadTextBox_BoxGroup.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadTextBox_BoxGroup.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_BoxGroup.Location = new System.Drawing.Point(10, 280);
            this.RadTextBox_BoxGroup.MaxLength = 20;
            this.RadTextBox_BoxGroup.Name = "RadTextBox_BoxGroup";
            this.RadTextBox_BoxGroup.Size = new System.Drawing.Size(178, 25);
            this.RadTextBox_BoxGroup.TabIndex = 2;
            this.RadTextBox_BoxGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_BoxGroup_KeyDown);
            this.RadTextBox_BoxGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_BoxGroup_KeyPress);
            // 
            // radRadioButton_Box
            // 
            this.radRadioButton_Box.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Box.Location = new System.Drawing.Point(10, 258);
            this.radRadioButton_Box.Name = "radRadioButton_Box";
            this.radRadioButton_Box.Size = new System.Drawing.Size(92, 19);
            this.radRadioButton_Box.TabIndex = 65;
            this.radRadioButton_Box.TabStop = false;
            this.radRadioButton_Box.Text = "ระบุเลขที่ลัง";
            this.radRadioButton_Box.CheckStateChanged += new System.EventHandler(this.RadRadioButton_Box_CheckStateChanged);
            // 
            // radRadioButton_Bill
            // 
            this.radRadioButton_Bill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Bill.Location = new System.Drawing.Point(10, 314);
            this.radRadioButton_Bill.Name = "radRadioButton_Bill";
            this.radRadioButton_Bill.Size = new System.Drawing.Size(93, 19);
            this.radRadioButton_Bill.TabIndex = 64;
            this.radRadioButton_Bill.TabStop = false;
            this.radRadioButton_Bill.Text = "ระบุเลขที่บิล";
            this.radRadioButton_Bill.CheckStateChanged += new System.EventHandler(this.RadRadioButton_Bill_CheckStateChanged);
            // 
            // radRadioButton_SH
            // 
            this.radRadioButton_SH.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_SH.Location = new System.Drawing.Point(10, 196);
            this.radRadioButton_SH.Name = "radRadioButton_SH";
            this.radRadioButton_SH.Size = new System.Drawing.Size(137, 19);
            this.radRadioButton_SH.TabIndex = 61;
            this.radRadioButton_SH.TabStop = false;
            this.radRadioButton_SH.Text = "เลขที่ใบจัดส่ง [SH]";
            this.radRadioButton_SH.CheckStateChanged += new System.EventHandler(this.RadRadioButton_SH_CheckStateChanged);
            // 
            // radLabel_Drive
            // 
            this.radLabel_Drive.AutoSize = false;
            this.radLabel_Drive.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radLabel_Drive.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Drive.Location = new System.Drawing.Point(3, 40);
            this.radLabel_Drive.Name = "radLabel_Drive";
            this.radLabel_Drive.Size = new System.Drawing.Size(188, 144);
            this.radLabel_Drive.TabIndex = 52;
            this.radLabel_Drive.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radTextBox_SH2
            // 
            this.radTextBox_SH2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_SH2.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SH2.Location = new System.Drawing.Point(76, 221);
            this.radTextBox_SH2.MaxLength = 7;
            this.radTextBox_SH2.Name = "radTextBox_SH2";
            this.radTextBox_SH2.Size = new System.Drawing.Size(109, 25);
            this.radTextBox_SH2.TabIndex = 1;
            this.radTextBox_SH2.Text = "0054450";
            this.radTextBox_SH2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_SH2_KeyDown);
            this.radTextBox_SH2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_SH2_KeyPress);
            // 
            // radTextBox_SH1
            // 
            this.radTextBox_SH1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_SH1.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_SH1.Location = new System.Drawing.Point(10, 221);
            this.radTextBox_SH1.MaxLength = 5;
            this.radTextBox_SH1.Name = "radTextBox_SH1";
            this.radTextBox_SH1.Size = new System.Drawing.Size(57, 25);
            this.radTextBox_SH1.TabIndex = 0;
            this.radTextBox_SH1.Text = "20353";
            this.radTextBox_SH1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_SH1_KeyDown);
            this.radTextBox_SH1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_SH1_KeyPress);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_add,
            this.commandBarSeparator2,
            this.radButtonElement_Recive,
            this.commandBarSeparator1,
            this.radButtonElement_Excel,
            this.commandBarSeparator4,
            this.RadButtonElement_pdt,
            this.commandBarSeparator5});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "Export To Excel";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Recive
            // 
            this.radButtonElement_Recive.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Recive.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButtonElement_Recive.Name = "radButtonElement_Recive";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Recive, false);
            this.radButtonElement_Recive.Text = "radButtonElement1";
            this.radButtonElement_Recive.Click += new System.EventHandler(this.RadButtonElement_Recive_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 448);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 6;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // CheckBOX_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "CheckBOX_Detail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ข้อมูลการรับลังสินค้า ตามใบขนส่ง";
            this.Load += new System.EventHandler(this.CheckBOX_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LO2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_LO1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_LO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_BoxGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Bill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_SH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Drive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SH2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_SH1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SH1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_SH2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Recive;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadLabel radLabel_Drive;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_SH;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Bill;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_BoxGroup;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Box;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Bill;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_LO;
        private Telerik.WinControls.UI.RadTextBox radTextBox_LO2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_LO1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
    }
}
