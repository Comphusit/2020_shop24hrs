﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class CheckBOX_All : Telerik.WinControls.UI.RadForm
    {
        readonly string _pPermission; // 0 ไม่มีสิด 1 มีสิด
        readonly string _pTypeReport;// 0 ลังค้างรับ , 1 รายละเอียดลังค้างรับ
        string pTxtCondition;

        DataTable dtData = new DataTable();
        //Load
        public CheckBOX_All(string pPermission, string pTypeReport)
        {
            InitializeComponent();
            _pPermission = pPermission;
            _pTypeReport = pTypeReport;
        }
        //Load
        private void CheckBOX_All_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButton_Search.ButtonElement.ShowBorder = true;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_SH, DateTime.Now.AddDays(-1), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_LO, DateTime.Now.AddDays(-1), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Bill, DateTime.Now.AddDays(-1), DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radRadioButton_Bill.ButtonElement.Font = SystemClass.SetFontGernaral;
            radRadioButton_Lo.ButtonElement.Font = SystemClass.SetFontGernaral;
            radRadioButton_SH.ButtonElement.Font = SystemClass.SetFontGernaral;

            switch (_pTypeReport)
            {
                case "0"://ลังค้างรับ
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("Click", "เช็ค", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATE", "วันที่", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN_NAME", "ชื่อสาขา", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTBOX", "จำนวนลัง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTBOXRECIVE", "จำนวนรับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("BOXDIFF", "จำนวนค้าง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("boxSta", "ค้างเกินครึ่ง")));

                    RadGridView_ShowHD.Columns["Click"].IsPinned = true;
                    RadGridView_ShowHD.Columns["DATE"].IsPinned = true;
                    RadGridView_ShowHD.Columns["MN"].IsPinned = true;

                    ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "BOXDIFF <> 0 ", false)
                    { CellBackColor = ConfigClass.SetColor_SkyPastel() };
                    RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj1);
                    RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj1);
                    RadGridView_ShowHD.Columns["BOXDIFF"].ConditionalFormattingObjectList.Add(obj1);
                    RadGridView_ShowHD.Columns["Click"].ConditionalFormattingObjectList.Add(obj1);

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "boxSta = '1' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["MN"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["MN_NAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["BOXDIFF"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["Click"].ConditionalFormattingObjectList.Add(obj2);
                    break;
                case "1"://รายละเอียดลังค้างรับ
                    radRadioButton_Lo.Visible = false; radDateTimePicker_LO.Visible = false;
                    radRadioButton_Bill.CheckState = CheckState.Checked;
                    radLabel_Detail.Visible = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("C", "รับ")));
                    
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MN_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOGISTICID", "LO", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่บิล", 120)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 160)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPCNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXTYPE_DESC", "ประเภทลัง", 80)));

                    RadGridView_ShowHD.Columns["INVOICEACCOUNT"].IsPinned = true;
                    RadGridView_ShowHD.Columns["MN_NAME"].IsPinned = true;

                    break;
                default:
                    break;
            }

            ConnectionClass.ExecuteSQL_Main(BOX_Class.SHOP_RECIVEBOX_DeleteNull());

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;

            switch (_pTypeReport)
            {
                case "0":
                    RadGridView_ShowHD.FilterDescriptors.Clear();

                    if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();

                    DataTable dtBch = Class.BranchClass.GetBranchAll("'1','4'", "'1'");
                    DataTable dtRecivce = new DataTable();
                    DataTable dtSend = new DataTable();
                    string date = "";

                    if (radRadioButton_Bill.CheckState == CheckState.Checked)
                    {
                        dtRecivce = BOXClass.FindBoxRecive_Bill(radDateTimePicker_Bill.Value.ToString("yyyy-MM-dd"));
                        dtSend = BOXClass.FindBoxSend_Bill(radDateTimePicker_Bill.Value.ToString("yyyy-MM-dd"));
                        date = radDateTimePicker_Bill.Value.ToString("yyyy-MM-dd");
                    }
                    if (radRadioButton_SH.CheckState == CheckState.Checked)
                    {
                        dtRecivce = BOXClass.FindBoxRecive_SH(radDateTimePicker_SH.Value.ToString("yyyy-MM-dd"));
                        dtSend = BOXClass.FindBoxSend_SH(radDateTimePicker_SH.Value.ToString("yyyy-MM-dd"));
                        date = radDateTimePicker_SH.Value.ToString("yyyy-MM-dd");
                    }
                    if (radRadioButton_Lo.CheckState == CheckState.Checked)
                    {
                        dtRecivce = BOXClass.FindBoxRecive_LO(radDateTimePicker_LO.Value.ToString("yyyy-MM-dd"));
                        dtSend = BOXClass.FindBoxSend_LO(radDateTimePicker_LO.Value.ToString("yyyy-MM-dd"));
                        date = radDateTimePicker_LO.Value.ToString("yyyy-MM-dd");
                    }
                    if (date == "") { this.Cursor = Cursors.Default; return; }

                    for (int i = 0; i < dtBch.Rows.Count; i++)
                    {
                        int boxSend = 0, boxRecive = 0, boxDiff;
                        DataRow[] resultRecive = dtRecivce.Select("MN = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' ");
                        DataRow[] resultSend = dtSend.Select("MN = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' ");
                        if (resultRecive.Length > 0) boxRecive = Convert.ToInt16(resultRecive[0]["BoxQtyRecive"]);
                        if (resultSend.Length > 0) boxSend = Convert.ToInt16(resultSend[0]["BoxQtySend"]);

                        boxDiff = boxSend - boxRecive;

                        string boxSta = "0";
                        if (boxDiff > (boxSend / 2)) boxSta = "1";

                        string desc = "";
                        if (boxDiff > 0) desc = "Click";

                        RadGridView_ShowHD.Rows.Add(desc, date,
                            dtBch.Rows[i]["BRANCH_ID"].ToString(), dtBch.Rows[i]["BRANCH_NAME"].ToString(),
                            boxSend, boxRecive, boxDiff, boxSta);
                    }
                    break;
                case "1":
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    if (dtData.Rows.Count > 0) { dtData.Rows.Clear(); RadGridView_ShowHD.DataSource = dtData; }

                    if (radRadioButton_Bill.CheckState == CheckState.Checked) dtData =BOXClass.Report_BoxNotReciveByDateBill(radDateTimePicker_Bill.Value.ToString("yyyy-MM-dd"));
                    if (radRadioButton_SH.CheckState == CheckState.Checked) dtData = BOXClass.Report_BoxNotReciveByDateSH(radDateTimePicker_SH.Value.ToString("yyyy-MM-dd"));

                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dtData;
                    dtData.AcceptChanges();

                    break;
                default:
                    break;
            }
            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            radDateTimePicker_SH.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_LO.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_Bill.Value = DateTime.Now.AddDays(-1);
            switch (_pTypeReport)
            {
                case "0":
                    if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();
                    break;
                case "1":
                    if (dtData.Rows.Count > 0) { dtData.Rows.Clear(); RadGridView_ShowHD.DataSource = dtData; }
                    break;
                default:
                    break;
            }
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Recive Box
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (_pTypeReport)
            {
                case "0":
                    switch (e.Column.Name)
                    {
                        case "Click":
                            if (RadGridView_ShowHD.MasterView.CurrentRow.Cells["BOXDIFF"].Value.ToString() == "0")
                            { return; }
                            if (RadGridView_ShowHD.MasterView.CurrentRow.Cells["DATE"].Value.ToString() == "")
                            {
                                return;
                            }

                            SetpTxtCondition(RadGridView_ShowHD.MasterView.CurrentRow.Cells["DATE"].Value.ToString(),
                                RadGridView_ShowHD.MasterView.CurrentRow.Cells["MN"].Value.ToString());


                            CheckBOX_Detail _checkBox_Detail = new CheckBOX_Detail(_pPermission, pTxtCondition, "1", "");
                            if (_checkBox_Detail.ShowDialog(this) == DialogResult.OK)
                            { }
                            if (_checkBox_Detail.pChange == "1")
                            {
                                SetDGV_HD();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "1":
                    if (_pPermission == "0") { return; }
                    break;
                default:
                    break;
            }


        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        #region "SQL_ReportType0"
        void SetpTxtCondition(string pDate, string pBchId)
        {
            if (radRadioButton_Bill.CheckState == CheckState.Checked)
            {
                pTxtCondition = BOXClass.FindBoxID_NotReciveByDate("0", pDate, pBchId);
            }
            if (radRadioButton_SH.CheckState == CheckState.Checked)
            {
                pTxtCondition = BOXClass.FindBoxID_NotReciveByDate("1", pDate, pBchId);
            }
            if (radRadioButton_Lo.CheckState == CheckState.Checked)
            {
                pTxtCondition = BOXClass.FindBoxID_NotReciveByDate("2", pDate, pBchId);
            }
        }
     
        #endregion

        #region "SQL_ReportType1"

       
        #endregion
        //Export Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = "ไม่เข้าเงื่อนไขในการ Export Excel";
            switch (_pTypeReport)
            {
                case "0":
                    T = DatagridClass.ExportExcelGridView("จำนวนลังค้างรับ", RadGridView_ShowHD, "1");
                    break;
                case "1":
                    T = DatagridClass.ExportExcelGridView("รายละเอียดลังค้างรับ", RadGridView_ShowHD, "1");
                    break;
                default:
                    break;
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pPermission);
        }
    }
}
