﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class BillEdit : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pPermission; // 0 ไม่มีสิด 1 มีสิด
        //Load
        public BillEdit(string pPermission)
        {
            InitializeComponent();
            _pPermission = pPermission;
        }
        //Load
        private void BillEdit_Load(object sender, EventArgs e)
        {
            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateIns", "วันที่", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "SH", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Type", "สถานะ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค้ด", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินค้า", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QtySend", "จำนวนส่ง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QtyRecive", "จำนวนรับ", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Unit", "หน่วย", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Reson", "หมายเหตุ", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoIns", "ผู้บันทึก", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLDRIVER", "พนักงานขับรถ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Diff", "Diff")));

            RadGridView_ShowHD.Columns["BRANCH"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;

            RadGridView_ShowHD.TableElement.RowHeight = 150;

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "Diff = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            RadGridView_ShowHD.Columns["QtySend"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["QtyRecive"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "Diff = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["QtySend"].ConditionalFormattingObjectList.Add(obj1);
            RadGridView_ShowHD.Columns["QtyRecive"].ConditionalFormattingObjectList.Add(obj1);

            if (SystemClass.SystemBranchID == "MN000")
            {
                RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll("1,4", "1");
            }
            else
            {
                RadCheckBox_Branch.CheckState = CheckState.Checked;
                RadDropDownList_Branch.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Enabled = false;
            }

            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            RadButton_Search.ButtonElement.ShowBorder = true;

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string pBch = "";
            if (RadCheckBox_Branch.CheckState == CheckState.Checked) pBch = RadDropDownList_Branch.SelectedValue.ToString();

            dt_Data = BOX_Class.CheckBox_FindData(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pBch);// ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pathFullName = PathImageClass.pImageEmply;
                string pathFileName = PathImageClass.pPathBOXBillEdit + dt_Data.Rows[i]["DateIns"].ToString();
                string wantFile = "";
                string pCount = "0";
                DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                if (DirInfo.Exists)
                {
                    wantFile = "Edit-" +
                        dt_Data.Rows[i]["BRANCH"].ToString() + @"*_" +
                        dt_Data.Rows[i]["LINEBILL"].ToString() + @"," +
                        dt_Data.Rows[i]["BARCODE"].ToString() + @"," +
                        dt_Data.Rows[i]["DOCUMENTNUM"].ToString() +
                        @".JPG";

                    FileInfo[] Files = DirInfo.GetFiles(wantFile, SearchOption.AllDirectories);

                    if (Files.Length > 0)
                    {
                        pathFullName = Files[0].FullName;
                        pCount = "1";
                    }
                }

                RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFileName + @"|" + wantFile;
                RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;

            }

            this.Cursor = Cursors.Default;
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now;
            if (SystemClass.SystemBranchID == "MN000") RadCheckBox_Branch.CheckState = CheckState.Unchecked;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            ImageClass.OpenShowImage(RadGridView_ShowHD.CurrentRow.Cells["CountImage"].Value.ToString(),
                            RadGridView_ShowHD.CurrentRow.Cells["PathImage"].Value.ToString());
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดบิลแก้ไข", RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //openDoc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pPermission);
        }
    }
}
