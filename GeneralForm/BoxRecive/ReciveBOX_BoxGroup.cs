﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.BoxRecive
{
    public partial class ReciveBOX_BoxGroup : Telerik.WinControls.UI.RadForm
    {
        readonly string _pType;
        //0 การรับลัง
        //1 การลบลัง ของ MRT

        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        public ReciveBOX_BoxGroup(string pType)
        {
            InitializeComponent();
            _pType = pType;
        }

        //Load
        private void ReciveBOX_BoxGroup_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radLabel_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            switch (_pType)
            {
                case "0":
                    radLabel_Branch.Visible = true; RadDropDownList_Branch.Visible = true;
                    RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll("1", "1");
                    RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_Branch.ValueMember = "BRANCH_ID";

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 130));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่บิล", 150));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHIPMENTID", "เลขที่ใบจัดส่ง", 130));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LO", "LO", 120));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่บิล", 120));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "สาขาเจ้าของ", 100));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CUSTNAME", "ชื่อสาขา", 150));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MN_RECIVE", "สาขารับของ", 170));

                    radGridView_Show.EnableFiltering = false;
                    break;
                case "1":

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "เลขที่ลัง", 130));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VOUCHERID", "เลขที่บิล", 150));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขาเจ้าของ", 100));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATIONNAMETO", "ชื่อสาขา", 150));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SHIPDATE", "วันที่บิล", 120));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NUMBERLABEL", "Label", 100));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ROUTENAME", "สาย", 100));

                    radGridView_Show.EnableFiltering = false;
                    break;
                default:
                    break;
            }

        }

        #region "CheckNumOnly"

        private void RadTextBox_Tag_TextChanging(object sender, TextChangingEventArgs e)
        {
            e.Cancel = !ConfigClass.IsNumber(e.NewValue);
        }

        #endregion
        //Input ลัง
        private void RadTextBox_BoxGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (RadTextBox_BoxGroup.Text == "") return;

                switch (_pType)
                {
                    case "0":
                        CheckBOX(RadTextBox_BoxGroup.Text);
                        break;
                    case "1":
                        DeleteBOX(RadTextBox_BoxGroup.Text);
                        break;
                    default:
                        break;
                }

            }
        }

        void CheckBOX(string pBoxID)
        {
              DataTable dt = BOXClass.CheckBOXFACESHEET_ByBoxID(pBoxID); // ConnectionClass.SelectSQL_Main(sql);

            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ลัง");
                return;
            }

            if (dt.Rows[0]["Sta_IN"].ToString() != "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลลังเลขที่ " + pBoxID + " ได้รับเข้าระบบเรียบร้อยแล้ว ไม่สามารถรับซ้ำได้.");
                RadTextBox_BoxGroup.SelectAll();
                RadTextBox_BoxGroup.Focus();
                return;
            }

            string bchID = RadDropDownList_Branch.SelectedValue.ToString();
            string bchIDDiff = "0";
            if (RadDropDownList_Branch.SelectedValue.ToString() != dt.Rows[0]["INVOICEACCOUNT"].ToString())
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmInsert(pBoxID + " เข้าสาขา " + RadDropDownList_Branch.SelectedItem[0].ToString()) == DialogResult.No)
                {
                    RadTextBox_BoxGroup.SelectAll();
                    RadTextBox_BoxGroup.Focus();
                    return;
                }
                else
                {
                    bchIDDiff = "1";
                }
            }

               string staAX = "0";
            string staProcess = "0";
            string rmk = "PC-SUPC";
            string tAX = ConnectionClass.ExecuteSQL_MainAX(AX_SendData.SPC_TRANSFERUPDRECEIVE_Insert(bchID, dt.Rows[0]["BOXGROUP"].ToString(), dt.Rows[0]["DOCUMENTNUM"].ToString(), "PC-SUPC"));
            if (tAX == "")
            {
                staAX = "1";
                staProcess = "1";
                rmk += "-AX";
            }
            ArrayList sqlIn = new ArrayList()
            {
                BOX_Class.SHOP_RECIVEBOX_Insert(bchID, dt.Rows[0]["BOXGROUP"].ToString(), dt.Rows[0]["DOCUMENTNUM"].ToString(), "H",
                    staProcess, staAX, dt.Rows[0]["BOXTYPE"].ToString(), "", dt.Rows[0]["NAME"].ToString(),
                    Double.Parse(dt.Rows[0]["QTY"].ToString()), dt.Rows[0]["QTY"].ToString(),
                    rmk, dt.Rows[0]["INVOICEACCOUNT"].ToString(),
                    dt.Rows[0]["SHIPMENTID"].ToString(), dt.Rows[0]["SHIPMENTID"].ToString(), dt.Rows[0]["LO"].ToString())
            };

            if (bchIDDiff == "1")
            {
                sqlIn.Add(BOX_Class.SHOP_RECIVEBOX_OTHER_Insert(bchID, dt.Rows[0]["BOXGROUP"].ToString(),
                    dt.Rows[0]["DOCUMENTNUM"].ToString(), dt.Rows[0]["SHIPMENTID"].ToString(), dt.Rows[0]["INVOICEACCOUNT"].ToString()));
               
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radGridView_Show.Rows.Add(dt.Rows[0]["BOXGROUP"].ToString(), dt.Rows[0]["DOCUMENTNUM"].ToString(),
                    dt.Rows[0]["SHIPMENTID"].ToString(), dt.Rows[0]["LO"].ToString(), dt.Rows[0]["TRANSDATE"].ToString(),
                    dt.Rows[0]["INVOICEACCOUNT"].ToString(), dt.Rows[0]["CUSTNAME"].ToString(),
                    RadDropDownList_Branch.SelectedItem[0].ToString());
                RadTextBox_BoxGroup.SelectAll();
                RadTextBox_BoxGroup.Focus();
                return;
            }

        }
        // ลบลังใน MRT แต่ไม่ใช้งานแล้ว
        void DeleteBOX(string pBoxID)
        {
            string sql = $@"
                SELECT	BOXGROUP,VOUCHERID,INVENTLOCATIONIDTO,LOCATIONNAMETO,CONVERT(VARCHAR,SHIPDATE,23) AS SHIPDATE,NUMBERLABEL,ROUTENAME,StaAx,REFBOXTRANS
                FROM    ANDROID_POSTOBOXFACESHEET WITH (NOLOCK) 
		                INNER JOIN ANDROID_ORDERTOHD WITH (NOLOCK) ON ANDROID_POSTOBOXFACESHEET.VOUCHERID = ANDROID_ORDERTOHD.DocNo
                WHERE   BOXGROUP='{pBoxID}' AND [STATUS] = '0'
            ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            if (dt.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("เลขที่ลัง");
                return;
            }

            if (dt.Rows[0]["StaAx"].ToString() == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ข้อมูลลังเลขที่  {pBoxID}{Environment.NewLine}{dt.Rows[0]["VOUCHERID"]}{Environment.NewLine}ได้รับเข้าระบบ AX เรียบร้อยแล้ว{Environment.NewLine}ไม่สามารถลบลังได้.");
                RadTextBox_BoxGroup.SelectAll();
                RadTextBox_BoxGroup.Focus();
                return;
            }

            ArrayList sqlDel = new ArrayList
            {
                $@"DELETE	ANDROID_POSTOBOXFACESHEET   WHERE	BOXGROUP = '{pBoxID}' ",
                $@"DELETE	ANDROID_POSTOBOXTRANS   WHERE	REFBOXTRANS = '{dt.Rows[0]["REFBOXTRANS"]}' ",
                $@"UPDATE	ANDROID_ORDERTOHD SET Box = Box-1   WHERE	DocNo = '{dt.Rows[0]["VOUCHERID"]}' ",
            };

            string resault = ConnectionClass.ExecuteSQL_ArrayMain(sqlDel);
            MsgBoxClass.MsgBoxShow_SaveStatus(resault);
            if (resault == "")
            {
                radGridView_Show.Rows.Add(
                    dt.Rows[0]["BOXGROUP"].ToString(), dt.Rows[0]["VOUCHERID"].ToString(),
                    dt.Rows[0]["INVENTLOCATIONIDTO"].ToString(), dt.Rows[0]["LOCATIONNAMETO"].ToString(), dt.Rows[0]["SHIPDATE"].ToString(),
                    dt.Rows[0]["NUMBERLABEL"].ToString(), dt.Rows[0]["ROUTENAME"].ToString());

                RadTextBox_BoxGroup.SelectAll();
                RadTextBox_BoxGroup.Focus();
                return;
            }

        }
        //Set Focus
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            RadTextBox_BoxGroup.SelectAll();
            RadTextBox_BoxGroup.Focus();
        }
    }
}